object CertDM: TCertDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 298
  Width = 215
  object ADOConn: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=F:\Gitee\VonWindows' +
      '\Published\Certification\Certification.mdb;Persist Security Info' +
      '=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 88
    Top = 64
  end
  object DBTemplate: TADOTable
    Connection = ADOConn
    CursorType = ctStatic
    TableName = 'Template'
    Left = 88
    Top = 128
  end
  object DQCertification: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM Certification WHERE TempIdx=:ID')
    Left = 88
    Top = 192
  end
end
