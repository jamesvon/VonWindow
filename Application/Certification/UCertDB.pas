unit UCertDB;

interface

uses
  System.SysUtils, System.Classes, System.Types, Data.DB, Data.Win.ADODB,
  Winapi.Windows, Winapi.Messages, UVonSystemFuns, UVonCrypt;

type
  TCertDM = class(TDataModule)
    ADOConn: TADOConnection;
    DBTemplate: TADOTable;
    DQCertification: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    procedure InitEnvironment;
  public
    { Public declarations }
  end;

var
  CertDM: TCertDM;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TCertDM }

procedure TCertDM.InitEnvironment;
var
  p: DWORD;
begin
  with FormatSettings do
  begin
    CurrencyString := '￥';
    CurrencyFormat := 0;
    CurrencyDecimals := 2;
    DateSeparator := '-';
    TimeSeparator := ':';
    ListSeparator := ',';
    ShortDateFormat := 'yyyy-M-d';
    LongDateFormat := 'yyyy年M月d日';
    TimeAMString := '上午';
    TimePMString := '下午';
    ShortTimeFormat := 'h:mm';
    LongTimeFormat := 'h:mm:ss';
    ShortMonthNames[1] := '一月';
    ShortMonthNames[2] := '二月';
    ShortMonthNames[3] := '三月';
    ShortMonthNames[4] := '四月';
    ShortMonthNames[5] := '五月';
    ShortMonthNames[6] := '六月';
    ShortMonthNames[7] := '七月';
    ShortMonthNames[8] := '八月';
    ShortMonthNames[9] := '九月';
    ShortMonthNames[10] := '十月';
    ShortMonthNames[11] := '十一月';
    ShortMonthNames[12] := '十二月';
    LongMonthNames[1] := '一月';
    LongMonthNames[2] := '二月';
    LongMonthNames[3] := '三月';
    LongMonthNames[4] := '四月';
    LongMonthNames[5] := '五月';
    LongMonthNames[6] := '六月';
    LongMonthNames[7] := '七月';
    LongMonthNames[8] := '八月';
    LongMonthNames[9] := '九月';
    LongMonthNames[10] := '十月';
    LongMonthNames[11] := '十一月';
    LongMonthNames[12] := '十二月';
    ShortDayNames[1] := '星期日';
    ShortDayNames[2] := '星期一';
    ShortDayNames[3] := '星期二';
    ShortDayNames[4] := '星期三';
    ShortDayNames[5] := '星期四';
    ShortDayNames[6] := '星期五';
    ShortDayNames[7] := '星期六';
    LongDayNames[1] := '星期日';
    LongDayNames[2] := '星期一';
    LongDayNames[3] := '星期二';
    LongDayNames[4] := '星期三';
    LongDayNames[5] := '星期四';
    LongDayNames[6] := '星期五';
    LongDayNames[7] := '星期六';
    ThousandSeparator := ',';
    DecimalSeparator := '.';
    TwoDigitYearCenturyWindow := $32;
    NegCurrFormat := $02;
  end;
  SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_SSHORTDATE,pchar('yyyy/MM/dd')); //设置短日期格式
  SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_SLONGDATE,pchar('yyyy''年''M''月''d''日''')); //设置长日期格式为 yyyy'年'M'月'd'日'，“年月日”字符必须用单引号括起来。Delphi字符串里必须用两个单引号。
  SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_STIMEFORMAT,pchar('HH:mm:ss')); //设置时间格式，24小时制
  SendMessageTimeOut(HWND_BROADCAST,WM_SETTINGCHANGE,0,0,SMTO_ABORTIFHUNG,10,p);//设置完成后必须调用，通知其他程序格式已经更改，否则即使是程序自身也不能使用新设置的格式
end;

procedure TCertDM.DataModuleCreate(Sender: TObject);
begin
  InitEnvironment;
  CertDM.DBTemplate.Open;
end;

end.
