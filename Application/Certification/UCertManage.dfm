object FCertification: TFCertification
  Left = 0
  Top = 0
  Caption = 'VonWindows'#35777#20070#31649#29702
  ClientHeight = 636
  ClientWidth = 1012
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object tabCtrl: TTabControl
    Left = 0
    Top = 0
    Width = 880
    Height = 636
    Align = alClient
    TabOrder = 0
    OnChange = tabCtrlChange
    object Splitter2: TSplitter
      Left = 473
      Top = 6
      Height = 626
      Align = alRight
      ExplicitLeft = 137
      ExplicitTop = -18
    end
    inline FrameCertification: TFramePropertyList
      Left = 476
      Top = 6
      Width = 400
      Height = 626
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 476
      ExplicitTop = 6
      ExplicitWidth = 400
      ExplicitHeight = 626
      inherited lstValues: TValueListEditor
        Width = 400
        Height = 626
        ExplicitWidth = 400
        ExplicitHeight = 626
        ColWidths = (
          150
          244)
      end
    end
    object DBGrid1: TDBGrid
      Left = 4
      Top = 6
      Width = 469
      Height = 626
      Align = alClient
      DataSource = DSCertification
      ReadOnly = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnCellClick = DBGrid1CellClick
      Columns = <
        item
          Expanded = False
          FieldName = 'CertName'
          Title.Caption = #35777#20070#21517#31216
          Width = 181
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CreateDate'
          Title.Caption = #24314#31435#26085#26399
          Width = 130
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Publishedate'
          Title.Caption = #21457#24067#26085#26399
          Width = 134
          Visible = True
        end>
    end
  end
  object Panel2: TPanel
    Left = 880
    Top = 0
    Width = 132
    Height = 636
    Align = alRight
    Caption = 'Panel2'
    ShowCaption = False
    TabOrder = 1
    object Label5: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 124
      Height = 13
      Align = alTop
      Caption = #35777#20070#21517#31216
      Layout = tlCenter
      ExplicitWidth = 48
    end
    object btnSave: TButton
      AlignWithMargins = True
      Left = 4
      Top = 50
      Width = 124
      Height = 25
      Align = alTop
      Caption = #20445#23384
      TabOrder = 0
      OnClick = btnSaveClick
    end
    object btnRead: TButton
      AlignWithMargins = True
      Left = 4
      Top = 112
      Width = 124
      Height = 25
      Align = alTop
      Caption = #35835#21462
      TabOrder = 1
      OnClick = btnReadClick
    end
    object btnPublished: TButton
      AlignWithMargins = True
      Left = 4
      Top = 143
      Width = 124
      Height = 25
      Align = alTop
      Caption = #21457#24067
      TabOrder = 2
      OnClick = btnPublishedClick
    end
    object ECertName: TEdit
      AlignWithMargins = True
      Left = 4
      Top = 23
      Width = 124
      Height = 21
      Align = alTop
      TabOrder = 3
    end
    object btnDelete: TButton
      AlignWithMargins = True
      Left = 4
      Top = 174
      Width = 124
      Height = 25
      Align = alTop
      Caption = #21024#38500
      TabOrder = 4
      OnClick = btnDeleteClick
    end
    object btnClone: TButton
      AlignWithMargins = True
      Left = 4
      Top = 81
      Width = 124
      Height = 25
      Align = alTop
      Caption = #20811#38534
      TabOrder = 5
      OnClick = btnCloneClick
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 740
    Top = 176
  end
  object SaveDialog1: TSaveDialog
    Left = 740
    Top = 224
  end
  object DSCertification: TDataSource
    DataSet = CertDM.DQCertification
    Left = 208
    Top = 144
  end
end
