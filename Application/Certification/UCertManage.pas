unit UCertManage;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.ValEdit, Vcl.Grids, IniFiles,
  UPropertyList, Data.DB, Vcl.DBGrids, UVonCrypt, UVonSystemFuns, UCertDB;

type
  /// <summary>证书内容信息</summary>
  TCertData = class(TCryptItem)
  private
    FSubjectValue: string;
    FSubjectData: TBytes;
  public
    procedure ReadFromStream(Stream: TStream); override;
    procedure WriteToStream(Stream: TStream); override;
    procedure ResizeData(size: Integer);
  published
    property SubjectData: TBytes read FSubjectData write FSubjectData;
    property SubjectValue: string read FSubjectValue write FSubjectValue;
  end;

  TFCertification = class(TForm)
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    tabCtrl: TTabControl;
    Splitter2: TSplitter;
    FrameCertification: TFramePropertyList;
    Panel2: TPanel;
    Label5: TLabel;
    btnSave: TButton;
    btnRead: TButton;
    btnPublished: TButton;
    ECertName: TEdit;
    DBGrid1: TDBGrid;
    DSCertification: TDataSource;
    btnDelete: TButton;
    btnClone: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnReadClick(Sender: TObject);
    procedure tabCtrlChange(Sender: TObject);
    procedure btnPublishedClick(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormDestroy(Sender: TObject);
    procedure btnCloneClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
  private
    { Private declarations }
    FCurrentCipher, FCurrentHash, FCurrentKey: string;
    FCurrentCert: TCryptCollection<TCertData>;
    procedure DisplayCurrentCertification;
  public
    { Public declarations }
  end;

var
  FCertification: TFCertification;

implementation

{$R *.dfm}
    
procedure TFCertification.FormCreate(Sender: TObject);
begin
  FCurrentCert:= TCryptCollection<TCertData>.Create;
  with CertDM.DBTemplate do begin
    First;
    while not EOF do begin
      tabCtrl.tabs.AddObject(FieldByName('TempName').AsString, TObject(FieldByName('ID').AsInteger));
      Next;
    end;
  end;
  if tabCtrl.tabs.Count > 0 then begin
    tabCtrl.tabindex:= 0;
    tabCtrlChange(nil);
  end;
end;

procedure TFCertification.FormDestroy(Sender: TObject);
begin
  FCurrentCert.Free;
end;

{$region 'Methods of certification file'}
procedure TFCertification.tabCtrlChange(Sender: TObject);
var
  lst: TStringList;
  I: Integer;
begin     //提取已发证书列表
  if tabCtrl.TabIndex < 0 then Exit;
  FCurrentCipher:= CertDM.DBTemplate.FieldByName('CipherName').AsString;
  FCurrentHash:= CertDM.DBTemplate.FieldByName('HashName').AsString;
  FCurrentKey:= CertDM.DBTemplate.FieldByName('Key').AsString;
  lst:= TStringList.Create;
  lst.Assign(TBlobField(CertDM.DBTemplate.FieldByName('Content')));
  FrameCertification.Clear;
  FrameCertification.Settings:= lst;
  lst.Free;
 // FrameCertification.Refresh;
  with CertDM.DQCertification do begin
    Close;
    Parameters[0].Value:= Integer(tabCtrl.Tabs.Objects[tabCtrl.TabIndex]);
    Open;
    if RecordCount > 0 then DBGrid1CellClick(nil);
  end;
end;

procedure TFCertification.DBGrid1CellClick(Column: TColumn);
var
  ms: TMemoryStream;
begin
  FrameCertification.Refresh;
  ECertName.Text:= CertDM.DQCertification.FieldByName('CertName').AsString;
  ms:= TMemoryStream.Create;
  TBlobField(CertDM.DQCertification.FieldByName('Content')).SaveToStream(ms);
  ms.Position:= 0;
  FCurrentCert.LoadFromStream(ms);
  ms.Free;
  DisplayCurrentCertification;
end;

procedure TFCertification.DisplayCurrentCertification;
var
  I: Integer;
  data: TData;
begin
  for I := 0 to FCurrentCert.Count - 1 do
    with FCurrentCert.ItemByIndex[I] do begin
      FrameCertification.Values[Name]:= SubjectValue;
      data:= TData.Create;
      data.Length:= System.Length(SubjectData);
      Move(SubjectData[0], data.Datas[0], data.Length);
      FrameCertification.Datas[Name]:= data;
    end;
end;

procedure TFCertification.btnCloneClick(Sender: TObject);
var
  S: string;
begin
  S:= ECertName.Text;
  if InputQuery('录入', '录入新的证书名称', S) then begin
    ECertName.Text:= S;
    btnSaveClick(nil);
  end;
end;

procedure TFCertification.btnDeleteClick(Sender: TObject);
begin
  if CertDM.DQCertification.RecordCount = 0 then Exit;
  if CertDM.DQCertification.FieldByName('CertName').AsString <> ECertName.Text then
    raise Exception.Create('尚未提取，不允许删除！');
  if MessageDlg('是否确认要删除？', mtInformation, [mbOK, mbCancel], 0) <> mrOK then
    Exit;
  if MessageDlg('一旦删除系统将无法回复，是否确认？', mtInformation, [mbOK, mbCancel], 0) <> mrOK then
    Exit;
  CertDM.DQCertification.Delete();
  DBGrid1CellClick(nil);
end;

procedure TFCertification.btnPublishedClick(Sender: TObject);
var
  I: Integer;
  item: TCertData;
begin
  with SaveDialog1 do
    if Execute then begin
      btnSaveClick(nil);
      item:= TCertData.Create;
      item.Name:= 'Publishedate';
      item.SubjectValue:= DatetimeToStr(Now);
      FCurrentCert.Add(item);
      FCurrentCert.SaveToFile(Filename, FCurrentKey, FCurrentCipher, FCurrentHash);
    end;
end;

procedure TFCertification.btnReadClick(Sender: TObject);
begin
  with OpenDialog1 do
    if Execute then begin
      FCurrentCert.LoadFromFile(Filename, FCurrentKey, FCurrentCipher, FCurrentHash);
      DisplayCurrentCertification;
    end;
end;

procedure TFCertification.btnSaveClick(Sender: TObject);
var
  s: TMemoryStream;
  I: Integer;
  bs: TData;
  item: TCertData;
begin
  with CertDM.DQCertification do begin
    if(RecordCount > 0)and(FieldByName('CertName').AsString = ECertName.Text)then Edit
    else begin
      Append;
      FieldByName('CertName').AsString:= ECertName.Text;
      FieldByName('TempIdx').AsInteger:= Integer(tabCtrl.Tabs.Objects[tabCtrl.TabIndex]);
      FieldByName('CreateDate').AsDateTime:= Now;
    end;
    s:= TMemoryStream.Create;
    FCurrentCert.Clear;
    for I := 0 to FrameCertification.Count - 1 do begin
      item:= TCertData.Create;
      item.Name:= FrameCertification.Names[I];
      item.SubjectValue:= FrameCertification.ValueByIndex[I];
      bs:= FrameCertification.DataByIndex[I];
      if Assigned(bs) then begin
        item.ResizeData(bs.Length);
        Move(bs.Datas[0], item.SubjectData[0], bs.Length);
      end else item.ResizeData(0);
      FCurrentCert.Add(item);
    end;
    FCurrentCert.SaveToStream(S);
    S.Position:= 0;
    TBlobField(FieldByName('Content')).LoadFromStream(S);
    Post;
    S.Free;
  end;
end;
{$endregion}

{ TCertData }

procedure TCertData.ReadFromStream(Stream: TStream);
var
  FSubjectValueSize: Int64;
begin
  inherited;
  Name:= ReadStringFromStream(Stream);
  FSubjectValue:= ReadStringFromStream(Stream);
  FSubjectValueSize:= ReadInt64FromStream(Stream);
  SetLength(FSubjectData, FSubjectValueSize);
  Stream.Read(FSubjectData, FSubjectValueSize);
end;

procedure TCertData.ResizeData(size: Integer);
begin
  SetLength(FSubjectData, size);
end;

procedure TCertData.WriteToStream(Stream: TStream);
begin
  inherited;
  WriteStringToStream(Name, Stream);
  WriteStringToStream(FSubjectValue, Stream);
  WriteInt64ToStream(Length(FSubjectData), Stream);
  Stream.Write(FSubjectData, Length(FSubjectData));
end;

end.
