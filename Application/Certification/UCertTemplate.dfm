object FCertificationTemplate: TFCertificationTemplate
  Left = 0
  Top = 0
  Caption = 'VonWindows'#35777#20070#27169#26495#31649#29702
  ClientHeight = 636
  ClientWidth = 1012
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 209
    Top = 0
    Height = 636
    ExplicitLeft = 201
  end
  object Panel1: TPanel
    Left = 888
    Top = 0
    Width = 124
    Height = 636
    Align = alRight
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 124
      Height = 289
      Align = alTop
      Caption = #27169#26495#20449#24687
      TabOrder = 0
      object Label4: TLabel
        AlignWithMargins = True
        Left = 5
        Top = 18
        Width = 114
        Height = 13
        Align = alTop
        Caption = #27169#26495#21517#31216
        Layout = tlCenter
        ExplicitWidth = 48
      end
      object Label1: TLabel
        AlignWithMargins = True
        Left = 5
        Top = 156
        Width = 114
        Height = 13
        Align = alTop
        Caption = #35777#20070#31192#38053
        Layout = tlCenter
        ExplicitWidth = 48
      end
      object Label2: TLabel
        AlignWithMargins = True
        Left = 5
        Top = 110
        Width = 114
        Height = 13
        Align = alTop
        Caption = #25688#35201#31639#27861
        Layout = tlCenter
        ExplicitWidth = 48
      end
      object Label3: TLabel
        AlignWithMargins = True
        Left = 5
        Top = 64
        Width = 114
        Height = 13
        Align = alTop
        Caption = #21152#23494#26041#27861
        Layout = tlCenter
        ExplicitWidth = 48
      end
      object ETemplateName: TEdit
        AlignWithMargins = True
        Left = 5
        Top = 37
        Width = 114
        Height = 21
        Align = alTop
        TabOrder = 0
      end
      object btnTempSave: TButton
        AlignWithMargins = True
        Left = 5
        Top = 202
        Width = 114
        Height = 23
        Align = alTop
        Caption = #20445#23384
        TabOrder = 1
        OnClick = btnTempSaveClick
      end
      object btnTempDel: TButton
        AlignWithMargins = True
        Left = 5
        Top = 231
        Width = 114
        Height = 23
        Align = alTop
        Caption = #21024#38500
        TabOrder = 2
        OnClick = btnTempDelClick
      end
      object btnDefault: TButton
        AlignWithMargins = True
        Left = 5
        Top = 260
        Width = 114
        Height = 23
        Align = alTop
        Cancel = True
        Caption = #40664#35748#27169#26495
        TabOrder = 3
        OnClick = btnDefaultClick
      end
      object EHashName: TComboBox
        AlignWithMargins = True
        Left = 5
        Top = 129
        Width = 114
        Height = 21
        Align = alTop
        Style = csDropDownList
        TabOrder = 4
      end
      object ECipherName: TComboBox
        AlignWithMargins = True
        Left = 5
        Top = 83
        Width = 114
        Height = 21
        Align = alTop
        Style = csDropDownList
        TabOrder = 5
      end
      object ECertKey: TEdit
        AlignWithMargins = True
        Left = 5
        Top = 175
        Width = 114
        Height = 21
        Align = alTop
        TabOrder = 6
      end
    end
    object lstTypes: TListBox
      Left = 0
      Top = 289
      Width = 124
      Height = 347
      Align = alClient
      ItemHeight = 13
      Items.Strings = (
        'String'
        'Enum')
      TabOrder = 1
      OnDblClick = lstTypesDblClick
    end
  end
  object lstTemplate: TListBox
    Left = 0
    Top = 0
    Width = 209
    Height = 636
    Align = alLeft
    ItemHeight = 13
    TabOrder = 1
    OnClick = lstTemplateClick
    ExplicitLeft = -3
  end
  object lstTempItems: TMemo
    Left = 212
    Top = 0
    Width = 676
    Height = 636
    Align = alClient
    TabOrder = 2
  end
  object OpenDialog1: TOpenDialog
    Left = 740
    Top = 176
  end
  object SaveDialog1: TSaveDialog
    Left = 740
    Top = 224
  end
end
