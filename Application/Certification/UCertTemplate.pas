unit UCertTemplate;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.ValEdit, Vcl.Grids, IniFiles,
  UPropertyList, UCertDB;

type
  TFCertificationTemplate = class(TForm)
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    Splitter1: TSplitter;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ETemplateName: TEdit;
    btnTempSave: TButton;
    btnTempDel: TButton;
    btnDefault: TButton;
    EHashName: TComboBox;
    ECipherName: TComboBox;
    ECertKey: TEdit;
    lstTemplate: TListBox;
    lstTempItems: TMemo;
    lstTypes: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure btnTempSaveClick(Sender: TObject);
    procedure btnTempDelClick(Sender: TObject);
    procedure btnDefaultClick(Sender: TObject);
    procedure lstTypesDblClick(Sender: TObject);
    procedure lstTemplateClick(Sender: TObject);
  private
    { Private declarations }
    FCipherName, FHashName, FCertKey, FCertFilename: string;
  public
    { Public declarations }
  end;

var
  FCertificationTemplate: TFCertificationTemplate;

implementation

uses UVonCrypt;

{$R *.dfm}
    
procedure TFCertificationTemplate.FormCreate(Sender: TObject);
begin
  GetCipherList(ECipherName.Items);
  ECipherName.ItemIndex:= 0;
  GetHashList(EHashName.Items);
  EHashName.ItemIndex:= 0;
  GetPropertyTypes(lstTypes.Items);
  with CertDM.DBTemplate do begin
    First;
    while not EOF do begin
      lstTemplate.Items.AddObject(FieldByName('TempName').AsString, TObject(FieldByName('ID').AsInteger));
      Next;
    end;
  end;
end;

{$region 'Methods of template file'}
procedure TFCertificationTemplate.lstTemplateClick(Sender: TObject);
var
  szID: Integer;
begin
  if lstTemplate.ItemIndex < 0 then Exit;
  szID:= Integer(lstTemplate.Items.Objects[lstTemplate.ItemIndex]);
  with CertDM.DBTemplate do
    if Locate('ID', szID, []) then begin
      ETemplateName.Text:= FieldByName('TempName').AsString;
      ECipherName.ItemIndex:= ECipherName.Items.IndexOf(FieldByName('CipherName').AsString);
      EHashName.ItemIndex:= EHashName.Items.IndexOf(FieldByName('HashName').AsString);
      ECertKey.Text:= FieldByName('Key').AsString;
      lstTempItems.Text:= CertDM.DBTemplate.FieldByName('Content').AsString;
    end;
end;

procedure TFCertificationTemplate.lstTypesDblClick(Sender: TObject);
begin
  lstTempItems.SelText := lstTypes.Items[lstTypes.ItemIndex] + '()';
end;

procedure TFCertificationTemplate.btnDefaultClick(Sender: TObject);
begin
  lstTempItems.Lines.Clear;
  lstTempItems.Lines.Add('系统标题=string()');
  lstTempItems.Lines.Add('系统码=string()');
  lstTempItems.Lines.Add('当前版本=string()');
  lstTempItems.Lines.Add('版本绑定=enum(true,false)');
  lstTempItems.Lines.Add('绑卡绑定=enum(true,false)');
  lstTempItems.Lines.Add('服务绑定=enum(true,false)');
  lstTempItems.Lines.Add('CPU绑定=enum(true,false)');
  lstTempItems.Lines.Add('数据库绑定=enum(true,false)');
  lstTempItems.Lines.Add('显卡绑定=enum(true,false)');
  lstTempItems.Lines.Add('开机检查网络=enum(true,false)');
  lstTempItems.Lines.Add('支持版本=string()');
  lstTempItems.Lines.Add('创建日期=date()');
  lstTempItems.Lines.Add('有效日期=date()');
  lstTempItems.Lines.Add('注册方式=enum(人工注册,自动注册,互联网注册,服务注册)');
end;

procedure TFCertificationTemplate.btnTempDelClick(Sender: TObject);
begin
  if lstTemplate.ItemIndex < 0 then Exit;
  if lstTemplate.Items[lstTemplate.ItemIndex] <> ETemplateName.Text then
    raise Exception.Create('尚未提取，不允许删除！');
  if MessageDlg('是否确认要删除？', mtInformation, [mbOK, mbCancel], 0) <> mrOK then
    Exit;
  if MessageDlg('一旦删除系统将无法回复，是否确认？', mtInformation, [mbOK, mbCancel], 0) <> mrOK then
    Exit;
  CertDM.DBTemplate.Locate('ID', Integer(lstTemplate.Items.Objects[lstTemplate.ItemIndex]), []);
  CertDM.DBTemplate.Delete();
  lstTemplate.DeleteSelected;
end;

procedure TFCertificationTemplate.btnTempSaveClick(Sender: TObject);
var
  Idx: Integer;
begin     //存储证书方案
  Idx:= lstTemplate.Items.IndexOf(ETemplateName.Text);
  if Idx < 0 then CertDM.DBTemplate.Append
  else begin
    CertDM.DBTemplate.Locate('ID', Integer(lstTemplate.Items.Objects[Idx]), []);
    CertDM.DBTemplate.Edit;
  end;
  CertDM.DBTemplate.FieldByName('TempName').AsString:= ETemplateName.Text;
  CertDM.DBTemplate.FieldByName('CipherName').AsString:= ECipherName.Text;
  CertDM.DBTemplate.FieldByName('HashName').AsString:= EHashName.Text;
  CertDM.DBTemplate.FieldByName('Key').AsString:= ECertKey.Text;
  CertDM.DBTemplate.FieldByName('Content').AsString:= lstTempItems.Text;
  CertDM.DBTemplate.Post;
  if Idx < 0 then
    lstTemplate.Items.AddObject(ETemplateName.Text, TObject(CertDM.DBTemplate.FieldByName('ID').AsInteger));
end;
{$endregion}

end.
