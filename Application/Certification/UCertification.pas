unit UCertification;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  Vcl.ExtCtrls;

type
  TFCertMain = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCertMain: TFCertMain;

implementation

uses UCertManage, UCertTemplate;

{$R *.dfm}

procedure TFCertMain.Button1Click(Sender: TObject);
begin
  with TFCertificationTemplate.Create(nil) do try
    ShowModal;
  finally
    Free;
  end;
end;

procedure TFCertMain.Button2Click(Sender: TObject);
begin
  with TFCertification.Create(nil) do try
    ShowModal;
  finally
    Free;
  end;
end;

end.
