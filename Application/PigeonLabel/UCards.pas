unit UCards;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, UFlyLabelDB, DB, ADODB, UComm,
  IniFiles;

type
  TFCardNo = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    EMemName: TEdit;
    Label2: TLabel;
    ECardNo: TEdit;
    btnAdd: TBitBtn;
    btnEdit: TBitBtn;
    btnDel: TBitBtn;
    BitBtn4: TBitBtn;
    DBMember: TADOTable;
    DataSource1: TDataSource;
    BitBtn1: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    FComm: TComm;
    FComMID: Integer;
  public
    { Public declarations }
  end;

var
  FCardNo: TFCardNo;

  function GetNewCardNO: string;

implementation
       
function GetNewCardNO: string;
begin

end;

{$R *.dfm}

procedure TFCardNo.FormCreate(Sender: TObject);
begin
  DBMember.Open;
  FComm:= TComm.Create;
  with TIniFile.Create(ExtractFilePath(Application.ExeName) + 'FlyLabel.INI')do try
    FComMID:= ReadInteger('SYSTEM', 'COMM', 3);
  finally
    Free;
  end;
end;

procedure TFCardNo.btnAddClick(Sender: TObject);
var
  Idx: Integer;

  function ReadCard: Boolean;
  begin
    Result:= FComm.Open('COM' + IntToStr(FComMID), 9600);
    if Result then FComm.Start
    else begin
      Inc(FComMID);
      if FComMID < 10 then Result:= ReadCard;
    end;
  end;
begin     //添加
  if FComMID = 0 then FComMID:= 1;
  if not ReadCard then begin
    FComMID:= 0;
    raise Exception.Create('Can not found card reader.');
  end;
  ECardNo.Text:= Format('%.6d', [DBMember.RecordCount + 1]);
  FComm.WriteCardNO(ECardNo.Text);     
  FComm.Close;
  with DBMember do begin
    Append;
    FieldByName('MemName').AsString:= EMemName.Text;
    FieldByName('CardNo').AsString:= ECardNo.Text;
    FieldByName('LastData').AsString:= '';
    FieldByName('LastAccess').AsDateTime:= Now;
    FieldByName('CreateDate').AsDateTime:= Now;
    Post;
  end;
end;

procedure TFCardNo.btnEditClick(Sender: TObject);
var
  Idx: Integer;

  function ReadCard: Boolean;
  begin
    Result:= FComm.Open('COM' + IntToStr(FComMID), 9600);
    if Result then FComm.Start
    else begin
      Inc(FComMID);
      if FComMID < 10 then Result:= ReadCard;
    end;
  end;
begin     //修改
  if FComMID = 0 then FComMID:= 1;
  if not ReadCard then begin
    FComMID:= 0;
    raise Exception.Create('Can not found card reader.');
  end;
  FComm.WriteCardNO(ECardNo.Text);   
  FComm.Close;
  with DBMember do begin
    Edit;
    FieldByName('MemName').AsString:= EMemName.Text;
    FieldByName('CardNo').AsString:= ECardNo.Text;
    FieldByName('LastAccess').AsDateTime:= Now;
    Post;
  end;
end;

procedure TFCardNo.btnDelClick(Sender: TObject);
begin
  if DBMember.RecordCount < 1 then Exit;
  if MessageDlg('是否确定要删除这个人员吗?', mtWarning, mbOKCancel, 0) = mrOK then begin
//    FFlyLabelDB.AddTask(DBMember.FieldByName('ID').AsInteger, 2, FFlyLabelDB.LogonName, DBMember.FieldByName('memName').AsString);
    DBMember.Delete;
  end;
end;

procedure TFCardNo.DBGrid1DblClick(Sender: TObject);
begin
  with DBMember do begin
    EMemName.Text:= FieldByName('MemName').AsString;
    ECardNo.Text:= FieldByName('CardNo').AsString;
  end;
end;

procedure TFCardNo.BitBtn1Click(Sender: TObject);
begin     //清除
  if FComMID = 0 then FComMID:= 1;            
  if FComm.Open('COM' + IntToStr(FComMID), 9600) then begin
    FComm.Clear;
    FComm.Close;
  end else
  if FComMID < 10 then Inc(FComMID)
  else begin
    FComMID:= 0;
    raise Exception.Create('Can not found card reader.');
  end;
end;

end.
