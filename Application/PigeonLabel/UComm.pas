unit UComm;

interface

uses
  Windows, SysUtils, Classes, Forms, StdCtrls, DateUtils, IniFiles, UFlyLabelDB,
  Dialogs, ExtCtrls;

const
  DEFAULR_INTERVALUE = 100;
  SYSTEM_VERSION = '1.0.0.11';
  CONFIGFILENANE = 'PIGEON.INI';
  NOJUNZHUORING = '非君卓信鸽脚环';

type
  EUart = Class(Exception);

  TUart = class(TObject)
  private
    OpenFlag: boolean;      //打开标志
    handle  : THandle;      //串口句柄
  public
   constructor Create;
   destructor  Destroy; override;
   //打开串口，例：Open('COM1',CBR_9600,'n',8,1)
   function Open(com: string; bps: longint; par: char; dbit, sbit: byte): Boolean;
   //关闭串口
   procedure Close;
   //返回输入缓冲区中的字符数
   function InbufChars: DWord;
   //返回输出缓冲区中的字符数
   function OutBufChars: Longint;
   //写串口 buf:存放要写的数据，len:要写的长度
   procedure Send(var buf; len: DWord);
   //读串口 buf:存放读到的数据，len:要读的长度，tmout:超时值(ms)
   //返回: 实际读到的字符数
   function Receive(var buf; len: DWord; tmout: integer): integer;
   //向串口发一个字符
   procedure PutChar(ch: char);
   //向串口发一个字符串
   procedure Puts(s:string);
   //从串口读一个字符，未收到字符则返回#0
   function GetChar: char;
  //从串口取一个字符串，忽略不可见字符,收到#13或255个字符结束，tmout:超时值(ms)
   //返回:true表示收到完整字符串，false表示超时退出
   function Gets(var s: string; tmout: integer): boolean;
   //清接收缓冲区
   procedure ClearInBuf;
   //等待一个字符串,tmout:超时值(ms),返回false表示超时
   function Wait(s: String; tmout: integer): boolean;
   //执行一个AT命令，返回true表示成功
   function ExecAtComm(s: string): boolean;
   //挂机，返回true表示成功
   function Hangup: boolean;
   //应答，返回true表示成功
   function Answer: boolean;

   property Opened: Boolean read OpenFlag;
  end;

type
  TArrayBool = array of Boolean;

  TComm = class
  private
    FComm: TUart;
    FSend_State: Integer;
    FLabelCount: Integer;
    FCurrentID: Integer;
    FMachineID: string;
    FLabels: TStringList;
    FTimer: TTimer;
    procedure SetMachineID(const Value: string);
    procedure SetLabelCount(const Value: Integer);
    procedure SetLabels(const Value: TStringList);
    procedure ReadComm(Sender: TObject);
    function GetOpened: Boolean;
  public
    constructor Create(); overload;
    constructor Create(com: string; bps: longint; par: char = 'n'; dbit: byte = 8; sbit: byte = 1); overload;
    destructor  Destroy; override;
    function Open(com: string; bps: longint; par: char = 'n'; dbit: byte = 8; sbit: byte = 1): Boolean;
    procedure Start;
    procedure Close;
    procedure Clear;
    procedure WriteCardNO(CardNo: string);
    procedure WriteCommBufToLog(Buf: array of byte; Count: Integer; Processor: string);
  published
    property Labels: TStringList read FLabels write SetLabels;
    property MachineID: string read FMachineID write SetMachineID;
    property LabelCount: Integer read FLabelCount write SetLabelCount;
    property Opened: Boolean read GetOpened;
  end;

  procedure WriteLog(InfoClass: Integer; Processor, Info: string);
  
var
  LogClass: Integer;

implementation

procedure WriteLog(InfoClass: Integer; Processor, Info: string);
var
  F: TextFile;
  Filename: string;
begin
  if LogClass < 1 then
    with TIniFile.Create(ExtractFilePath(Application.ExeName) + CONFIGFILENANE)do try
      LogClass:= ReadInteger('SYSTEM', 'LogClass', 9);
    finally
      Free;
    end;
  if InfoClass > LogClass then Exit;
  Filename:= ExtractFilePath(Application.ExeName) + DateToStr(Now) + '.LOG';
  AssignFile(F, Filename);
  if FileExists(Filename) then Append(F) else Rewrite(F);
  WriteLn(F, DateTimeToStr(Now) + ':[' + IntToStr(InfoClass) + ']' + Processor +
    char(9) + Info);
  CloseFile(F);
end;

{$H+}

const
  CommInQueSize  = 4096;    //输入缓冲区大小
  CommOutQueSize = 4096;    //输出缓冲区大小

{**********************
*      计时器        *
**********************}
type
  Timers=Class
  private
    StartTime: TTimeStamp;
  public
    constructor Create;
    Procedure Start;            {开始计时}
    Function  Get:LongInt;      {返回计时值(单位:ms)}
  end;

//构造函数
constructor Timers.Create;
begin
  inherited Create;
  Start;
end;

//开始计时
Procedure Timers.Start;
begin
  StartTime:=DateTimeToTimeStamp(Now);
end;

//返回计时值(单位:ms)
Function Timers.Get:LongInt;
var
  CurTime: TTimeStamp;
begin
  CurTime:=DateTimeToTimeStamp(Now);
  result:=(CurTime.Date-StartTime.Date)*24*3600000+(CurTime.Time-StartTime.Time);
end;


{******************
*      TUart     *
******************}

//构造函数
constructor TUart.Create;
begin
  inherited Create;
  OpenFlag:=False;
end;

//析构函数
Destructor TUart.Destroy;
begin
  Close;
  inherited Destroy;
end;

{*****
打开串口，例：Open('COM1',CBR_9600,'n',8,1)
波特率：
  CBR_110       CBR_19200
  CBR_300     CBR_38400
  CBR_600     CBR_56000
  CBR_1200    CBR_57600
  CBR_2400    CBR_115200
  CBR_4800    CBR_128000
      CBR_256000
  CBR_14400
校验位:
  'n'=no,'o'=odd,'e'=even,'m'=mark,'s'=space
数据位： 4-8 (other=8)
停止位： 1-2 (other=1.5)
****}
function TUart.Open(com: string; bps: longint; par: char; dbit, sbit: byte): Boolean;
var
  dcb   : TDCB;
begin
  OpenFlag := false;
  //初始化串口
  handle:= CreateFile(PChar(com), GENERIC_READ + GENERIC_WRITE, 0, nil, OPEN_EXISTING, 0, 0 );
  Result:= not(handle = INVALID_HANDLE_VALUE);
  if not Result then Exit;
  GetCommState(handle, dcb);
  with dcb do
  begin
    BaudRate:= bps;               // 波特率
    if dbit in [4,5,6,7,8] then   // 数据位
      ByteSize:= dbit
    else
      ByteSize:= 8;
    case sbit of                  // 停止位
      1: StopBits:= 0;            // 1
      2: StopBits:= 2;            // 2
      else StopBits:= 0;          //1
    end;
    case par of                   //校验
      'n', 'N': Parity:= 0;       //no
      'o', 'O': Parity:= 1;       //odd
      'e', 'E': Parity:= 2;       //even
      'm', 'M': Parity:= 3;       //mark
      's', 'S': Parity:= 4;       //space
      else Parity:= 0;            //no
    end;
  end;
  SetCommState(handle, dcb);
  SetupComm(handle, CommOutQueSize, CommInQueSize);
  OpenFlag := True;
  Result:= OpenFlag;
end;

//关闭串口
procedure TUart.Close;
begin
   if OpenFlag then
   begin
     CloseHandle(handle);
     handle := 0;
   end;
   OpenFlag:=false;
end;

//检测输入缓冲区中的字符数
function TUart.InbufChars:DWord;
var
  ErrCode : DWord;
  Stat    : TCOMSTAT;
begin
  result:=0;
  if not OpenFlag then exit;
  ClearCommError(handle,ErrCode,@Stat);
  result:=stat.cbInQue;
end;

//检测输出缓冲区中的字符数
function TUart.OutBufChars:Longint;
var
  ErrCode : DWord;
  Stat    : TCOMSTAT;
begin
  result:=0;
  if not OpenFlag then exit;
  ClearCommError(handle,ErrCode,@Stat);
  result:=stat.cbOutQue;
end;

//写串口 buf:存放要写的数据，len:要写的长度
procedure TUart.Send(var buf; len:DWord);
var
  i : DWord;
begin
  WriteFile(handle,Buf,len,i,nil);  //写串口
end;


//读串口 buf:存放读到的数据，len:要读的长度，tmout:超时值(ms)
//返回: 实际读到的字符数
function TUart.Receive(var buf;len:DWord;tmout:integer):integer;
var
  Timer   : Timers;
  i       : DWord;
  BufChs  : DWord;
begin
  Timer:=Timers.Create;
  Timer.Start;
  repeat until (InBufChars>=len) or (Timer.Get>tmout); //收到指定长度数据或超时
  BufChs:=InBufChars;
  if len>BufChs then len:=BufChs;
  ReadFile(handle,Buf,len,i,nil);  //读串口
  result:=i;
  Timer.free;
end;

//向串口发一个字符
procedure TUart.PutChar(ch:char);
var
  i : DWord;
begin
  WriteFile(handle,ch,1,i,nil);  //写串口
end;

//向串口发一个字符串
procedure TUart.Puts(s:string);
var
  i : integer;
begin
  for i:=1 to length(s) do Putchar(s[i]);
end;

//从串口读一个字符，未收到字符则返回#0
function TUart.GetChar:char;
var
  i: DWord;
begin
  result:=#0;
  if InBufChars>0 then
    ReadFile(handle,result,1,i,nil);  //读串口
end;

//从串口取一个字符串，忽略不可见字符,收到#13或255个字符结束，tmout:超时值(ms)
//返回:true表示收到完整字符串，false表示超时退出
function TUart.Gets(var s:string;tmout:integer):boolean;
var
  Timer   : Timers;
  ch      : char;
begin
  Timer:=Timers.Create;
  Timer.Start;
  s:='';
  result:=false;
  repeat
    ch:=GetChar;
    if ch<>#0 then Timer.Start;   //如收到字符则清定时器
    if ch>=#32 then s:=s+ch;
    if (ch=#13) or (length(s)>=255) then
    begin
     result:=true;
     break;
    end;
  until Timer.Get>tmout; //超时
  Timer.free;
end;


//清接收缓冲区
procedure TUart.ClearInBuf;
begin
  if not OpenFlag then exit;
  PurgeComm(handle,PURGE_RXCLEAR);
end;

//等待一个字符串,tmout:超时值(ms),返回false表示超时
function TUart.Wait(s:String;tmout:integer):boolean;
var
  s1    : string;
  timer : Timers;
begin
  timer:=Timers.Create;
  timer.Start;
  result:=false;
  repeat
    Gets(s1,tmout);
    if pos(s,s1)>0 then
    begin
      result:=true;
      break
    end;
  until timer.Get>tmout;
  timer.Free;
end;

//执行一个AT命令，返回true表示成功
function TUart.ExecAtComm(s:string):boolean;
begin
  ClearInBuf;
  Puts(s);
  result:=false;
  if Wait('OK',3000) then
    result:=true;
end;

//挂机，返回true表示成功
function TUart.Hangup:boolean;
begin
  result:=false;
  ExecAtComm('+++');
  if not ExecAtComm('ATH'#13) then exit;
  result:=true;
end;

//应答，返回true表示成功
function TUart.Answer:boolean;
begin
  ClearInBuf;
  Puts('ATA'#13);
  result:=false;
  if Wait('CONNECT',30000) then
    result:=true;
end;

{ TComm }

procedure TComm.Close;
begin
  FTimer.Enabled:= False;
  FComm.Close;
end;

constructor TComm.Create();
begin
  FComm:= TUart.Create;
  FTimer:= TTimer.Create(nil);
  FTimer.Enabled:= False;
  FTimer.Interval:= DEFAULR_INTERVALUE;
  FTimer.OnTimer:= ReadComm;
  FCurrentID:= 0;
end;

constructor TComm.Create(com: string; bps: Integer; par: char; dbit,
  sbit: byte);
begin
  Create();
  Open(com, bps, par, dbit, sbit);
end;

destructor TComm.Destroy;
begin
  FTimer.Free;
  FComm.Close;
  FComm.Free;
  inherited;
end;

function TComm.Open(com: string; bps: Integer; par: char; dbit,
  sbit: byte): Boolean;
begin
  Result:= FComm.Open(com, bps, par, dbit, sbit);
end;

procedure TComm.ReadComm(Sender: TObject);
var
  Buf: array[0..26] of byte;
  szBuf: array[0..26] of byte;
  BufCount, BufSize: Integer;
  S: string;

  function UnPackage: Integer;
  var
    i, Idx: Integer;
    isFlag: Boolean;
  begin
    FillChar(szBuf, SizeOf(szBuf), 0);
    isFlag:= False;
    Idx:= 0;
    for i:= 2 to BufSize - 2 do
      if(Buf[i] = $7D)and(not isFlag)then isFlag:= true
      else begin
        if isFlag then szBuf[Idx]:= Buf[i] or $70
        else szBuf[Idx]:= Buf[i];
        isFlag:= False;
        Inc(idx);
      end;
    Result:= Idx;                        
  end;       

  function Packaged: Integer;
  var      //就是把数据中的7e,7d,7f,转换成7d5d,7d5e,7d5f
    i, Idx: Integer;
  begin
    Buf[12]:= not (Buf[1] + Buf[2] + Buf[3] + Buf[4] + Buf[5] + Buf[6] +
      Buf[7] + Buf[8] + Buf[9] + Buf[10] + Buf[11]);
    FillChar(szBuf, SizeOf(szBuf), 0);
    Idx:= 1;
    for i:= 1 to 12 do begin
      case Buf[i] of
      $7E, $7D, $7F: begin
          szBuf[Idx]:= $7D;
          Inc(idx);
          szBuf[Idx]:= Buf[i] and $5F;
          Inc(idx);
        end;
      else begin
          szBuf[Idx]:= Buf[i];
          Inc(idx);
        end;
      end;
    end;
    szBuf[0]:= $7E;
    szBuf[Idx]:= $7F;
    Result:= Idx + 1;
  end;

  function ReceverData: Boolean;
  var i: Integer;
  begin
    Result:= False;
    FillChar(Buf, SizeOf(Buf), 0);
    BufSize:= FComm.Receive(Buf, 255, 50);
    (* Receive data *)
    case FSend_State of
    1: begin     //Receive data after send read card(Read card No)
        FSend_State:= 1;       //To read label Nos
        if Buf[1] = $00 then Exit;
        if Buf[BufSize - 1] <> $7F then Exit;
        BufCount:= UnPackage;
        if BufCount < 8 then Exit;
        FLabelCount:= szBuf[2] * 256 + szBuf[3];
        FMachineID:= Char(szBuf[4]) + Char(szBuf[5]) + Char(szBuf[6]) +
          Char(szBuf[7]) + Char(szBuf[8]) + Char(szBuf[9]);
        FLabels.Clear;
        if FLabelCount = 0 then FSend_State:= 0
        else FSend_State:= 2;       //To read label Nos
        FCurrentID:= 1;
      end;
    2: begin     //Receive data after send read labels(Get label No)
        FSend_State:= 2;
        if Buf[1] = $00 then Exit;
        if Buf[BufSize - 1] <> $7F then Exit;
        BufCount:= UnPackage;
        S:= '';
        for i:= 2 to BufCount - 1 do
          if szBuf[i] = $FF then Break
          else begin
            S:= S + Char((szBuf[i]shr 4) + $30);
            if szBuf[i]and $0F < $0A then S:= S + Char((szBuf[i]and $0F) + $30);
          end;
        FLabels.Add(S);
        if FCurrentID < FLabelCount then begin
          FSend_State:= 2;  //Read next card No
          Inc(FCurrentID);
        end else begin      //To clare all card No
          FSend_State:= 0;
          FCurrentID:= 1;
        end;
      end;
    end;
    Result:= True;
  end;
begin
  ReceverData;
  (* Send data *)
  case FSend_State of
  0: Close;
  1: begin    //Read card No
      Buf[0]:= $7E;
      Buf[1]:= $55;
      Buf[2]:= $00;
      Buf[3]:= $00;
      Buf[4]:= $00;
      Buf[5]:= $00;
      Buf[6]:= $00;
      Buf[7]:= $00;
      Buf[8]:= $00;
      Buf[9]:= $00;
      Buf[10]:= $00;
      Buf[11]:= $00;
      FComm.Send(szBuf, Packaged);
    end;
  2:  begin    //Read label No
      Buf[0]:= $7E;
      Buf[1]:= $55;
      Buf[2]:= (FCurrentID * 8) shr 8;
      Buf[3]:= FCurrentID * 8;
      Buf[4]:= $00;
      Buf[5]:= $00;
      Buf[6]:= $00;
      Buf[7]:= $00;
      Buf[8]:= $00;
      Buf[9]:= $00;
      Buf[10]:= $00;
      Buf[11]:= $00;
      Buf[12]:= Buf[1] + Buf[2] + Buf[3];
      FComm.Send(szBuf, Packaged);
    end;
  3: begin    //Delete data just received
      Buf[0]:= $7E;
      Buf[1]:= $54;
      Buf[2]:= 0;
      Buf[3]:= 0;    
      Buf[4]:= 0;
      Buf[5]:= 0;
      if Length(FMachineID) > 0 then Buf[6]:= Ord(FMachineID[1]);
      if Length(FMachineID) > 1 then Buf[7]:= Ord(FMachineID[2]);
      if Length(FMachineID) > 2 then Buf[8]:= Ord(FMachineID[3]);
      if Length(FMachineID) > 3 then Buf[9]:= Ord(FMachineID[4]);
      if Length(FMachineID) > 4 then Buf[10]:= Ord(FMachineID[5]);
      if Length(FMachineID) > 5 then Buf[11]:= Ord(FMachineID[6]);
      FComm.Send(szBuf, Packaged);
      FCurrentID:= 1;
      FSend_State:= 4;
    end;        
  4: begin    //Delete data just received  7E,54,00,08,FF,FF,FF,FF,FF,FF,FF,FF,CRC,7F 
      Buf[0]:= $7E;
      Buf[1]:= $54;
      Buf[2]:= $00;
      Buf[3]:= $08;
      Buf[4]:= $FF;
      Buf[5]:= $FF;
      Buf[6]:= $FF;
      Buf[7]:= $FF;
      Buf[8]:= $FF;
      Buf[9]:= $FF;
      Buf[10]:= $FF;
      Buf[11]:= $FF;
      FComm.Send(szBuf, Packaged);
      Inc(FCurrentID);
      if FCurrentID > FLabelCount then FSend_State:= 0;
    end;
  end;
end;

procedure TComm.SetMachineID(const Value: string);
begin
  FMachineID := Value;
end;

procedure TComm.WriteCommBufToLog(Buf: array of byte; Count: Integer; Processor: string);
var
  i: Integer;
  S: string;
begin
  S:= '';
  for i:= 0 to 255 - 1 do begin
    S:= S + '$' + IntToHex(Buf[i], 2);
  end;
  WriteLog(8, 'Clock_' + Processor, S);
end;

procedure TComm.SetLabelCount(const Value: Integer);
begin
  FLabelCount := Value;
end;

procedure TComm.SetLabels(const Value: TStringList);
begin
  FLabels := Value;
end;

procedure TComm.Start;
begin
  FTimer.Enabled:= True;
  FSend_State:= 1;
end;

function TComm.GetOpened: Boolean;
begin
  Result:= FComm.OpenFlag;
end;

procedure TComm.WriteCardNO(CardNo: string);
begin
  FMachineID:= CardNo;
  FSend_State:= 3;
  FLabelCount:= 1;
  ReadComm(nil);
end;

procedure TComm.Clear;
begin
  FMachineID:= FMachineID;
  FSend_State:= 3;
  FLabelCount:= 1;
  ReadComm(nil);
end;

end.
