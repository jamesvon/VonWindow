object DM: TDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 360
  Top = 152
  Height = 150
  Width = 215
  object ADOC: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ' +
      'ID=sa;Initial Catalog=FlyLabel'
    ConnectOptions = coAsyncConnect
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'SQLOLEDB.1'
    Left = 32
    Top = 8
  end
  object DCAddTask: TADOCommand
    CommandText = 
      'INSERT INTO FL_Made (LabelIDX, TaskKind, ItemCount, TaskContent,' +
      ' CreateDate)'#13#10'VALUES(:LID, :TKD, :CNT, :TCT, :CDT)'
    Connection = ADOC
    Parameters = <
      item
        Name = 'LID'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'TKD'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'CNT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'TCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = 'CDT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    Left = 104
    Top = 8
  end
end
