unit UDB;

interface

uses
  SysUtils, Classes, DB, ADODB, Forms, IniFiles, Types, Windows, DateUtils;

type
  TDM = class(TDataModule)
    ADOC: TADOConnection;
    DCAddTask: TADOCommand;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    LogonName: string;
    procedure AddTask(ALabelIDX, ATaskKind: Integer; ACardNo, ATaskContent: string);
  end;

  function GetApplicationVersion: string;

var
  DM: TDM;

implementation

function GetApplicationVersion: string;
var
  S: string;  
  n, Len: DWORD;
  Buf: PChar;
  Vervalue:  PVSFixedFileInfo;
  V1, V2, V3, V4: Word;
begin
  Result:= '';
  S := Application.ExeName;
  n := GetFileVersionInfoSize(PChar(S), n);
  if n > 0 then begin
    Buf := AllocMem(n);
    GetFileVersionInfo(PChar(S), 0, n, Buf);
    if VerQueryValue(Buf, '\', Pointer(Vervalue), Len) then
      with Vervalue^ do begin
        V1:=  dwFileVersionMS  shr  16;
        V2:=  dwFileVersionMS  and  $FFFF;
        V3:=  dwFileVersionLS  shr  16;
        V4:=  dwFileVersionLS  and  $FFFF;
      end;
    Result:= IntToStr(V1) + '.' + IntToStr(V2) + '.' + IntToStr(V3) + '.' + IntToStr(V4);   //040904E4
    FreeMem(Buf, n);
  end;
end;

{$R *.dfm}

(************************************************
TaskKind
   1     添加、修改一个标签
   2     删除一个标签
   3     从补录读卡其中读取一个标签
   4     删除一个补录标签
   5     打印一个标签
*************************************************)
procedure TDM.AddTask(ALabelIDX, ATaskKind: Integer; ACardNo, ATaskContent: string);
begin
  with DCAddTask do begin
    Parameters[0].Value:= ALabelIDX;
    Parameters[1].Value:= ATaskKind;
    Parameters[2].Value:= ACardNo;
    Parameters[3].Value:= ATaskContent;   
    Parameters[4].Value:= Now;
    Execute;
  end;
end;

procedure TDM.DataModuleCreate(Sender: TObject);
var
  tbs: TStringList;
begin
  with TIniFile.Create(ExtractFilePath(Application.ExeName) + 'FlyLabel.INI')do try
    ADOC.ConnectionString:= ReadString('SYSTEM', 'CONN', 'Provider=SQLOLEDB.1;Password=sa;Persist Security Info=True;User ID=sa;Initial Catalog=FlyLabel');
    //ADOC.ConnectionString:= ReadString('SYSTEM', 'CONN', 'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=FlyLabels.mdb');
    //Provider=Microsoft.Jet.OLEDB.4.0;Data Source=F:\MyPrograms\FlyLabels\FlyLabels.mdb;Persist Security Info=False
  finally
    Free;
  end;
  ADOC.Open;
  tbs:= TStringList.Create;
  ADOC.GetTableNames(tbs);
  if tbs.IndexOf('FL_Made_' + IntToStr(YearOf(Now))) < 0 then
    ADOC.Execute('CREATE TABLE FL_Made_' + IntToStr(YearOf(Now)) +
      '(ID int IDENTITY (1, 1) NOT NULL , LabelIDX int, TaskKind int, ' +
      'Procesor varchar(6), TaskContent varchar(20), CreateDate datetime)');
  DCAddTask.CommandText:= 'INSERT INTO FL_Made_' + IntToStr(YearOf(Now)) +
    '(LabelIDX, TaskKind, Procesor, TaskContent, CreateDate)VALUES(:LID, :TKD, :PRC, :TCT, :CDT)';
end;

end.
