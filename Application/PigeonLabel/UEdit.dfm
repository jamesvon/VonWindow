object FEdit: TFEdit
  Left = 104
  Top = 60
  Width = 696
  Height = 480
  Caption = #26631#31614#32534#36753
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 15
  object Panel1: TPanel
    Left = 0
    Top = 405
    Width = 688
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Label10: TLabel
      Left = 8
      Top = 18
      Width = 48
      Height = 15
      Caption = #26679#24335#21517#31216
    end
    object ELabelName: TEdit
      Left = 64
      Top = 13
      Width = 233
      Height = 23
      TabOrder = 0
    end
    object Panel5: TPanel
      Left = 440
      Top = 0
      Width = 248
      Height = 41
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtnSave: TBitBtn
        Left = 5
        Top = 8
        Width = 75
        Height = 25
        Caption = #20445#23384
        Default = True
        TabOrder = 0
        OnClick = BtnSaveClick
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
      object BtnSaveAs: TBitBtn
        Left = 85
        Top = 8
        Width = 75
        Height = 25
        Caption = #21478#20445#23384
        Default = True
        TabOrder = 1
        OnClick = BtnSaveAsClick
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
      object BtnCancel: TBitBtn
        Left = 165
        Top = 8
        Width = 75
        Height = 25
        Caption = #25918#24323
        TabOrder = 2
        Kind = bkCancel
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 224
    Width = 688
    Height = 181
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Label14: TLabel
      Left = 448
      Top = 120
      Width = 48
      Height = 15
      Caption = #24207#21495#20301#25968
    end
    object Label15: TLabel
      Left = 504
      Top = 120
      Width = 48
      Height = 15
      Caption = #26174#31034#23545#40784
    end
    object GpCopyMode: TRadioGroup
      Left = 304
      Top = 0
      Width = 377
      Height = 113
      Caption = #21472#21152#27169#24335
      Columns = 3
      ItemIndex = 9
      Items.Strings = (
        #20840#40657
        #21453#30333
        #21512#24182#22797#21046
        #21512#24182#30011#22270
        #21453#33394#22797#21046
        #21453#33394#21024#38500
        #22270#26696#22797#21046
        #22270#26696#21453#33394
        #22270#26696#21076#38500
        #22270#29255#21472#21152
        #22270#29255#22797#21046
        #22270#29255#25830#38500
        #22270#29255#21453#33394
        #22270#29255#32472#21046
        #20840#30333)
      TabOrder = 0
      OnClick = ETextTopChange
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 177
      Height = 181
      Align = alLeft
      Caption = #32534#21495#25511#21046
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 16
        Width = 24
        Height = 15
        Caption = #23383#20363
      end
      object Label3: TLabel
        Left = 8
        Top = 56
        Width = 48
        Height = 15
        Caption = #19978#36793#20301#32622
      end
      object Label2: TLabel
        Left = 64
        Top = 56
        Width = 48
        Height = 15
        Caption = #24038#36793#20301#32622
      end
      object Label5: TLabel
        Left = 8
        Top = 104
        Width = 24
        Height = 15
        Caption = #23383#20307
      end
      object Label6: TLabel
        Left = 120
        Top = 16
        Width = 36
        Height = 15
        Caption = #23383#22823#23567
      end
      object BtnBackColor: TSpeedButton
        Left = 64
        Top = 32
        Width = 23
        Height = 22
        Hint = #36873#25321#32972#26223#33394
        Flat = True
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00559999999995
          5555557777777775F5555559999999505555555777777757FFF5555555555550
          0955555555555FF7775F55555555995501955555555577557F75555555555555
          01995555555555557F5755555555555501905555555555557F57555555555555
          0F905555555555557FF75555555555500005555555555557777555555555550F
          F05555555555557F57F5555555555008F05555555555F775F755555555570000
          05555555555775577555555555700007555555555F755F775555555570000755
          55555555775F77555555555700075555555555F75F7755555555570007555555
          5555577F77555555555500075555555555557777555555555555}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = BtnBackColorClick
      end
      object BtnFontColor: TSpeedButton
        Left = 88
        Top = 32
        Width = 23
        Height = 22
        Hint = #36873#25321#23383#20307#39068#33394
        Flat = True
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00559999999995
          5555557777777775F5555559999999505555555777777757FFF5555555555550
          0955555555555FF7775F55555555995501955555555577557F75555555555555
          01995555555555557F5755555555555501905555555555557F57555555555555
          0F905555555555557FF75555555555500005555555555557777555555555550F
          F05555555555557F57F5555555555008F05555555555F775F755555555570000
          05555555555775577555555555700007555555555F755F775555555570000755
          55555555775F77555555555700075555555555F75F7755555555570007555555
          5555577F77555555555500075555555555557777555555555555}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = BtnFontColorClick
      end
      object Label13: TLabel
        Left = 120
        Top = 56
        Width = 36
        Height = 15
        Caption = #23383#38388#36317
      end
      object EExampleText: TEdit
        Left = 8
        Top = 32
        Width = 57
        Height = 23
        TabOrder = 0
        Text = '000000'
      end
      object ETextTop: TSpinEdit
        Left = 8
        Top = 72
        Width = 49
        Height = 24
        MaxValue = 0
        MinValue = 0
        TabOrder = 1
        Value = 0
        OnChange = ETextTopChange
      end
      object ETextLeft: TSpinEdit
        Left = 64
        Top = 72
        Width = 49
        Height = 24
        MaxValue = 0
        MinValue = 0
        TabOrder = 2
        Value = 216
        OnChange = ETextTopChange
      end
      object EFontName: TComboBox
        Left = 8
        Top = 120
        Width = 161
        Height = 23
        ItemHeight = 15
        TabOrder = 3
        Text = 'EFontName'
        OnChange = ETextTopChange
      end
      object EFontSize: TSpinEdit
        Left = 120
        Top = 32
        Width = 49
        Height = 24
        MaxValue = 0
        MinValue = 0
        TabOrder = 4
        Value = 68
        OnChange = ETextTopChange
      end
      object ChkBold: TCheckBox
        Left = 8
        Top = 144
        Width = 41
        Height = 17
        Caption = #31895#20307
        TabOrder = 5
        OnClick = ETextTopChange
      end
      object ChkItalic: TCheckBox
        Left = 8
        Top = 160
        Width = 41
        Height = 17
        Caption = #26012#20307
        TabOrder = 6
        OnClick = ETextTopChange
      end
      object ChkUnderline: TCheckBox
        Left = 56
        Top = 144
        Width = 57
        Height = 17
        Caption = #19979#21010#32447
        TabOrder = 7
        OnClick = ETextTopChange
      end
      object ChkStrikeOut: TCheckBox
        Left = 56
        Top = 160
        Width = 57
        Height = 17
        Caption = #21024#38500#32447
        TabOrder = 8
        OnClick = ETextTopChange
      end
      object EWordWidth: TSpinEdit
        Left = 120
        Top = 72
        Width = 49
        Height = 24
        MaxValue = 0
        MinValue = 0
        TabOrder = 9
        Value = 68
        OnChange = ETextTopChange
      end
    end
    object RGSizeOption: TGroupBox
      Left = 177
      Top = 0
      Width = 120
      Height = 181
      Align = alLeft
      Caption = #26631#31614#25511#21046
      TabOrder = 2
      object Label4: TLabel
        Left = 8
        Top = 24
        Width = 36
        Height = 15
        Caption = #22270#29255#38271
      end
      object Label7: TLabel
        Left = 64
        Top = 24
        Width = 36
        Height = 15
        Caption = #22270#29255#39640
      end
      object Label8: TLabel
        Left = 8
        Top = 72
        Width = 36
        Height = 15
        Caption = #26631#31614#38271
      end
      object Label9: TLabel
        Left = 64
        Top = 72
        Width = 36
        Height = 15
        Caption = #26631#31614#39640
      end
      object EImgWidth: TSpinEdit
        Left = 8
        Top = 40
        Width = 49
        Height = 24
        MaxValue = 0
        MinValue = 0
        TabOrder = 0
        Value = 0
        OnChange = EImgWidthChange
      end
      object EImgHeight: TSpinEdit
        Left = 64
        Top = 40
        Width = 49
        Height = 24
        MaxValue = 0
        MinValue = 0
        TabOrder = 1
        Value = 0
        OnChange = EImgWidthChange
      end
      object ELabelWidth: TSpinEdit
        Left = 8
        Top = 88
        Width = 49
        Height = 24
        MaxValue = 0
        MinValue = 0
        TabOrder = 2
        Value = 0
        OnChange = ELabelWidthChange
      end
      object ELabelHeight: TSpinEdit
        Left = 64
        Top = 88
        Width = 49
        Height = 24
        MaxValue = 0
        MinValue = 0
        TabOrder = 3
        Value = 0
        OnChange = ELabelWidthChange
      end
      object RGSynSize: TRadioGroup
        Left = 8
        Top = 112
        Width = 105
        Height = 65
        Caption = #23610#23544#21516#27493
        ItemIndex = 0
        Items.Strings = (
          #19981#21516#27493
          #22270#29255#21516#27493#26631#31614
          #26631#31614#21516#27493#22270#29255)
        TabOrder = 4
      end
    end
    object BtnLoadDefault: TBitBtn
      Left = 304
      Top = 120
      Width = 107
      Height = 25
      Caption = #25552#21462#40664#35748#23646#24615
      TabOrder = 3
      OnClick = BtnLoadDefaultClick
    end
    object BtnSaveDefault: TBitBtn
      Left = 304
      Top = 152
      Width = 107
      Height = 25
      Caption = #20445#23384#20026#40664#35748#23646#24615
      TabOrder = 4
      OnClick = BtnSaveDefaultClick
    end
    object EWordLength: TSpinEdit
      Left = 448
      Top = 136
      Width = 49
      Height = 24
      MaxValue = 0
      MinValue = 0
      TabOrder = 5
      Value = 6
      OnChange = EWordLengthChange
    end
    object EAlign: TComboBox
      Left = 504
      Top = 136
      Width = 73
      Height = 23
      ItemHeight = 15
      ItemIndex = 0
      TabOrder = 6
      Text = #24038#23545#40784
      OnChange = EAlignChange
      Items.Strings = (
        #24038#23545#40784
        #23621#20013
        #21491#23545#40784)
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 184
    Width = 688
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Label11: TLabel
      Left = 8
      Top = 16
      Width = 60
      Height = 15
      Caption = #40736#26631#20301#32622#65306
    end
    object Label12: TLabel
      Left = 96
      Top = 16
      Width = 12
      Height = 15
      Caption = #65306
    end
    object LMouseX: TLabel
      Left = 72
      Top = 16
      Width = 25
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object LMouseY: TLabel
      Left = 104
      Top = 16
      Width = 25
      Height = 13
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Panel3: TPanel
      Left = 296
      Top = 0
      Width = 392
      Height = 40
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtnLoadBMP: TBitBtn
        Left = 7
        Top = 8
        Width = 89
        Height = 25
        Caption = #25552#21462#32972#26223#22270
        TabOrder = 0
        OnClick = BtnLoadBMPClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00303333333333
          333337F3333333333333303333333333333337F33FFFFF3FF3FF303300000300
          300337FF77777F77377330000BBB0333333337777F337F33333330330BB00333
          333337F373F773333333303330033333333337F3377333333333303333333333
          333337F33FFFFF3FF3FF303300000300300337FF77777F77377330000BBB0333
          333337777F337F33333330330BB00333333337F373F773333333303330033333
          333337F3377333333333303333333333333337FFFF3FF3FFF333000003003000
          333377777F77377733330BBB0333333333337F337F33333333330BB003333333
          333373F773333333333330033333333333333773333333333333}
        NumGlyphs = 2
      end
      object BtnEditBMP: TBitBtn
        Left = 199
        Top = 7
        Width = 89
        Height = 25
        Caption = #32534#36753#32972#26223#22270
        TabOrder = 1
        OnClick = BtnEditBMPClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555775777777
          57705557757777775FF7555555555555000755555555555F777F555555555550
          87075555555555F7577F5555555555088805555555555F755F75555555555033
          805555555555F755F75555555555033B05555555555F755F75555555555033B0
          5555555555F755F75555555555033B05555555555F755F75555555555033B055
          55555555F755F75555555555033B05555555555F755F75555555555033B05555
          555555F75FF75555555555030B05555555555F7F7F75555555555000B0555555
          5555F777F7555555555501900555555555557777755555555555099055555555
          5555777755555555555550055555555555555775555555555555}
        NumGlyphs = 2
      end
      object BtnSaveBMP: TBitBtn
        Left = 295
        Top = 7
        Width = 89
        Height = 25
        Caption = #23384#20648#32972#26223#22270
        TabOrder = 2
        OnClick = BtnSaveBMPClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333FFFFFFFFFFFFF33000077777770033377777777777773F000007888888
          00037F3337F3FF37F37F00000780088800037F3337F77F37F37F000007800888
          00037F3337F77FF7F37F00000788888800037F3337777777337F000000000000
          00037F3FFFFFFFFFFF7F00000000000000037F77777777777F7F000FFFFFFFFF
          00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
          00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
          00037F7F333333337F7F000FFFFFFFFF07037F7F33333333777F000FFFFFFFFF
          0003737FFFFFFFFF7F7330099999999900333777777777777733}
        NumGlyphs = 2
      end
      object BitBtn1: TBitBtn
        Left = 103
        Top = 7
        Width = 89
        Height = 25
        Caption = #25552#21462#40664#35748#22270
        TabOrder = 3
        OnClick = BitBtn1Click
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00303333333333
          333337F3333333333333303333333333333337F33FFFFF3FF3FF303300000300
          300337FF77777F77377330000BBB0333333337777F337F33333330330BB00333
          333337F373F773333333303330033333333337F3377333333333303333333333
          333337F33FFFFF3FF3FF303300000300300337FF77777F77377330000BBB0333
          333337777F337F33333330330BB00333333337F373F773333333303330033333
          333337F3377333333333303333333333333337FFFF3FF3FFF333000003003000
          333377777F77377733330BBB0333333333337F337F33333333330BB003333333
          333373F773333333333330033333333333333773333333333333}
        NumGlyphs = 2
      end
    end
    object BitBtn2: TBitBtn
      Left = 216
      Top = 8
      Width = 75
      Height = 25
      Caption = #21047#26032
      TabOrder = 1
      OnClick = BitBtn2Click
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
        33333333333F8888883F33330000324334222222443333388F3833333388F333
        000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
        F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
        223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
        3338888300003AAAAAAA33333333333888888833333333330000333333333333
        333333333333333333FFFFFF000033333333333344444433FFFF333333888888
        00003A444333333A22222438888F333338F3333800003A2243333333A2222438
        F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
        22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
        33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
        3333333333338888883333330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 688
    Height = 184
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 3
    object TabSheet1: TTabSheet
      Caption = #22270#29255
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 0
        Width = 680
        Height = 154
        Align = alClient
        TabOrder = 0
        object ImageEdit: TImage
          Left = 0
          Top = 0
          Width = 684
          Height = 137
          AutoSize = True
          OnMouseDown = ImageEditMouseDown
          OnMouseMove = ImageEditMouseMove
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #26631#31614
      ImageIndex = 1
      object ScrollBox2: TScrollBox
        Left = 0
        Top = 0
        Width = 680
        Height = 154
        Align = alClient
        TabOrder = 0
        object ImagePreview: TImage
          Left = 0
          Top = 0
          Width = 680
          Height = 137
          AutoSize = True
        end
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 
      #25152#26377#22270#24418#25991#20214'|*.BMP;*.JPG;*.JPEG;*.JPE;*.JEIF|'#20301#22270#25991#20214'|*.BMP|JPEG|*.JPG;*.J' +
      'PEG;*.JPE;*.JEIF|'#25152#26377#25991#20214'|*.*'
    Left = 368
    Top = 233
  end
  object ColorDialog1: TColorDialog
    Left = 400
    Top = 232
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '.BMP'
    Filter = #20301#22270#25991#20214'|*.bmp'
    Left = 368
    Top = 264
  end
end
