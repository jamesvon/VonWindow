unit UEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, Spin, JPEG, ComCtrls, IniFiles, ADODB,
  OleCtnrs, Menus, ShellAPI, ULabelPrint;

type  
  TFEdit = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel4: TPanel;
    GpCopyMode: TRadioGroup;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    EExampleText: TEdit;
    Label3: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    ETextTop: TSpinEdit;
    ETextLeft: TSpinEdit;
    EFontName: TComboBox;
    EFontSize: TSpinEdit;
    ChkBold: TCheckBox;
    ChkItalic: TCheckBox;
    ChkUnderline: TCheckBox;
    ChkStrikeOut: TCheckBox;
    RGSizeOption: TGroupBox;
    Label4: TLabel;
    EImgWidth: TSpinEdit;
    Label7: TLabel;
    EImgHeight: TSpinEdit;
    Label8: TLabel;
    ELabelWidth: TSpinEdit;
    Label9: TLabel;
    ELabelHeight: TSpinEdit;
    Label10: TLabel;
    ELabelName: TEdit;
    Label11: TLabel;
    Label12: TLabel;
    LMouseX: TLabel;
    LMouseY: TLabel;
    OpenDialog1: TOpenDialog;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    ScrollBox1: TScrollBox;
    ImageEdit: TImage;
    ScrollBox2: TScrollBox;
    ImagePreview: TImage;
    BtnLoadDefault: TBitBtn;
    BtnSaveDefault: TBitBtn;
    ColorDialog1: TColorDialog;
    BtnBackColor: TSpeedButton;
    BtnFontColor: TSpeedButton;
    EWordWidth: TSpinEdit;
    Label13: TLabel;
    SaveDialog1: TSaveDialog;
    Panel3: TPanel;
    BtnLoadBMP: TBitBtn;
    BtnEditBMP: TBitBtn;
    BtnSaveBMP: TBitBtn;
    BitBtn1: TBitBtn;
    Panel5: TPanel;
    BtnSave: TBitBtn;
    BtnSaveAs: TBitBtn;
    BtnCancel: TBitBtn;
    RGSynSize: TRadioGroup;
    BitBtn2: TBitBtn;
    Label14: TLabel;
    EWordLength: TSpinEdit;
    Label15: TLabel;
    EAlign: TComboBox;
    procedure ImageEditMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImageEditMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnLoadBMPClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnLoadDefaultClick(Sender: TObject);
    procedure BtnSaveDefaultClick(Sender: TObject);
    procedure ETextTopChange(Sender: TObject);
    procedure EImgWidthChange(Sender: TObject);
    procedure ELabelWidthChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtnBackColorClick(Sender: TObject);
    procedure BtnFontColorClick(Sender: TObject);
    procedure BtnSaveBMPClick(Sender: TObject);
    procedure BtnEditBMPClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure BtnSaveAsClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure EWordLengthChange(Sender: TObject);
    procedure EAlignChange(Sender: TObject);
  private
    { Private declarations }
    //CanDrew: boolean;
    LabelImage: TLabelImage;
    procedure LoadBackground(Filename: string);
    procedure SetLabelValue;
    procedure Redrew;
  public
    { Public declarations }
    IsEdit: Boolean;
    DQLabels: TADOQuery;
    ParentID: Integer;
    procedure LoadData;
    procedure SaveData;
  end;

  procedure EditLabel(Sender: TObject; DQLabel: TADOQuery);
  procedure AppendLabel(Sender: TObject; DQLabel: TADOQuery; Pid: Integer);
  procedure CloneLabel(Sender: TObject; DQLabel: TADOQuery; Pid: Integer);

var
  FEdit: TFEdit;

implementation

uses UDB, DB;

{$R *.dfm}

procedure EditLabel(Sender: TObject; DQLabel: TADOQuery);
begin     //修改一个标签
  with TFEdit.Create(Application) do try
    DQLabels:= DQLabel;
    IsEdit:= true;
    BtnSaveAs.Visible:= true;
    ParentID:= DQLabel.FieldByName('CategoryIDX').AsInteger;
    LoadData;
    ShowModal();
    (Sender as TForm).Hide;
  finally
    Free;
    (Sender as TForm).Show;
  end;
end;

procedure AppendLabel(Sender: TObject; DQLabel: TADOQuery; Pid: Integer);
begin     //添加一个标签
  with TFEdit.Create(Application) do try
    DQLabels:= DQLabel;
    IsEdit:= False;
    BtnSaveAs.Visible:= False;
    ParentID:= Pid;
    ShowModal();
    (Sender as TForm).Hide;
  finally
    Free;
    (Sender as TForm).Show;
  end;
end;

procedure CloneLabel(Sender: TObject; DQLabel: TADOQuery; Pid: Integer);
begin     //克隆一个标签
  with TFEdit.Create(Application) do try
    DQLabels:= DQLabel;
    IsEdit:= False;
    BtnSaveAs.Visible:= False;
    ParentID:= Pid;
    LoadData;
    ELabelName.Text:= '';
    ShowModal();
    (Sender as TForm).Hide;
  finally
    Free;
    (Sender as TForm).Show;
  end;
end;


{ TFEdit }  

procedure TFEdit.FormCreate(Sender: TObject);
begin     //初始化
  EFontName.Items:= Screen.Fonts;
  LabelImage:= TLabelImage.Create;
  //CanDrew:= false;
  BtnLoadDefaultClick(self);
  Redrew;
end;

procedure TFEdit.FormDestroy(Sender: TObject);
begin     //卸载
  LabelImage.Free;
end;

procedure TFEdit.BtnLoadDefaultClick(Sender: TObject);
begin     //提取缺省图片参数
  with TIniFile.Create(ExtractFilePath(Application.ExeName) + 'FlyLabel.INI')do try
    EExampleText.Text:= ReadString('DEFAULT', 'Example', '000000');
    EWordWidth.Value:= ReadInteger('DEFAULT', 'WordWidth', 0);
    ETextTop.Value:= ReadInteger('DEFAULT', 'TextTop', 0);
    ETextLeft.Value:= ReadInteger('DEFAULT', 'TextLeft', 0);
    EFontSize.Value:= ReadInteger('DEFAULT', 'FontSize', 9);
    EFontName.ItemIndex:= EFontName.Items.IndexOf(ReadString('DEFAULT', 'FontName', 'Arial'));
    ChkBold.Checked:= ReadBool('DEFAULT', 'FontBold', false);
    ChkUnderline.Checked:= ReadBool('DEFAULT', 'FontUnderline', false);
    ChkItalic.Checked:= ReadBool('DEFAULT', 'FontItalic', false);
    ChkStrikeOut.Checked:= ReadBool('DEFAULT', 'FontStrikeOut', false);
    ELabelHeight.Value:= ReadInteger('DEFAULT', 'LabelHeight', 100);
    ELabelWidth.Value:= ReadInteger('DEFAULT', 'LabelWidth', 100);
  finally
    Free;
  end;
end;

procedure TFEdit.BtnSaveDefaultClick(Sender: TObject);
begin     //将当前参数保存为缺省图片参数
  with TIniFile.Create(ExtractFilePath(Application.ExeName) + 'FlyLabel.INI')do try
    WriteString('DEFAULT', 'Example', EExampleText.Text);
    WriteInteger('DEFAULT', 'WordWidth', EWordWidth.Value);
    WriteInteger('DEFAULT', 'TextTop', ETextTop.Value);
    WriteInteger('DEFAULT', 'TextLeft', ETextLeft.Value);
    WriteInteger('DEFAULT', 'FontSize', EFontSize.Value);
    WriteString('DEFAULT', 'FontName', EFontName.Text);
    WriteBool('DEFAULT', 'FontBold', ChkBold.Checked);
    WriteBool('DEFAULT', 'FontUnderline', ChkUnderline.Checked);
    WriteBool('DEFAULT', 'FontItalic', ChkItalic.Checked);
    WriteBool('DEFAULT', 'FontStrikeOut', ChkStrikeOut.Checked);
    WriteInteger('DEFAULT', 'LabelHeight', ELabelHeight.Value);
    WriteInteger('DEFAULT', 'LabelWidth', ELabelWidth.Value);
  finally
    Free;
  end;
end;   

procedure TFEdit.ImageEditMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin     //鼠标跟踪
  LMouseX.Caption:= IntToStr(X);
  LMouseY.Caption:= IntToStr(Y);
end;

procedure TFEdit.ImageEditMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin     //鼠标在图片上定点
  ETextTop.Value:= Y;
  ETextLeft.Value:= X;
  Redrew;
end;

procedure TFEdit.LoadBackground(Filename: string);
var
  FileExt: string;
  szJPEG: TJPEGImage;
begin     //提取背景图片
  //CanDrew:= false;
  FileExt:= UpperCase(ExtractFileExt(Filename));
  if FileExt = '.BMP' then
    ImageEdit.Picture.LoadFromFile(Filename);
  if(FileExt = '.JPEG')or(FileExt = '.JPG')or(FileExt = '.JPE')or(FileExt = '.JEIF')then begin
    szJPEG:= TJPEGImage.Create;
    szJPEG.LoadFromFile(Filename);
    ImageEdit.Picture.Assign(szJPEG);
    szJPEG.Free;
  end;
  EImgWidth.Value:= ImageEdit.Picture.Width;        //430
  EImgHeight.Value:= ImageEdit.Picture.Height;
  LabelImage.BackBmp.Assign(ImageEdit.Picture.Bitmap);
  //CanDrew:= true;
end;

procedure TFEdit.BtnLoadBMPClick(Sender: TObject);
begin     //提取指定的背景图
  with OpenDialog1 do
    if Execute then LoadBackground(FileName);
end;

procedure TFEdit.BitBtn1Click(Sender: TObject);
begin     //提取缺省的背景图
  LoadBackground(ExtractFilePath(Application.ExeName) + 'Deflaut.BMP');
end;

procedure TFEdit.ETextTopChange(Sender: TObject);
begin     //位置及相关参数调整
  if not (Sender as TWinControl).Focused then Exit;
  Redrew;
end;

procedure TFEdit.SetLabelValue;
begin     //向画图类付值
  LabelImage.WordWidth:= EWordWidth.Value;
  LabelImage.TextTop:= ETextTop.Value;
  LabelImage.TextLeft:= ETextLeft.Value;
  LabelImage.FontSize:= EFontSize.Value;
  LabelImage.FontName:= EFontName.Text;
  LabelImage.FontColor:= EExampleText.Font.Color;
  LabelImage.Background:= EExampleText.Color;
  LabelImage.IsStrikeOut:= ChkStrikeOut.Checked;
  LabelImage.IsItalic:= ChkItalic.Checked;
  LabelImage.IsBold:= ChkBold.Checked;
  LabelImage.IsUnderline:= ChkUnderline.Checked;
  LabelImage.CopyMode:= GpCopyMode.ItemIndex;
  LabelImage.No:= EExampleText.Text;   
  LabelImage.Align:= EAlign.ItemIndex;
  LabelImage.WordLength:= EWordLength.Value;
end;

procedure TFEdit.Redrew;
begin     //刷新图片
  SetLabelValue;
  //if not CanDrew then Exit;
  LabelImage.Drew(ImageEdit.Picture.Bitmap);   
  LabelImage.Drew(ImagePreview.Picture.Bitmap);
end;

procedure TFEdit.EImgWidthChange(Sender: TObject);
begin     //更改图片尺寸
  if(Sender <> nil)and(not (Sender as TWinControl).Focused)then Exit;
  if RGSynSize.ItemIndex = 2 then begin          //标签同步图片
    ELabelWidth.Value:= EImgWidth.Value;
    ELabelHeight.Value:= EImgHeight.Value;
    ELabelWidthChange(nil);
  end;
  ImageEdit.Picture.Bitmap.Width:= EImgWidth.Value;
  ImageEdit.Picture.Bitmap.Height:= EImgHeight.Value;
  LabelImage.BackBmp.Width:= EImgWidth.Value;
  LabelImage.BackBmp.Height:= EImgHeight.Value;
  Redrew;
end;

procedure TFEdit.ELabelWidthChange(Sender: TObject);
begin     //更改标签尺寸
  if(Sender <> nil)and(not (Sender as TWinControl).Focused)then Exit;
  if RGSynSize.ItemIndex = 1 then begin         //图片同步标签
    EImgWidth.Value:= ELabelWidth.Value;
    EImgHeight.Value:= ELabelHeight.Value;
    EImgWidthChange(nil);
  end;
  ImagePreview.Picture.Bitmap.Width:= ELabelWidth.Value;
  ImagePreview.Picture.Bitmap.Height:= ELabelHeight.Value;
  Redrew;
end;

procedure TFEdit.BtnBackColorClick(Sender: TObject);
begin     //选择背景色
  with ColorDialog1 do begin
    Color:= LabelImage.BackBmp.Canvas.Brush.Color;
    if Execute then begin
      LabelImage.BackBmp.Canvas.Brush.Color:= ColorDialog1.Color;
      EExampleText.Color:= ColorDialog1.Color;
    end;
  end;
end;

procedure TFEdit.BtnFontColorClick(Sender: TObject);
begin     //选择字体颜色
  with ColorDialog1 do begin
    Color:= LabelImage.BackBmp.Canvas.Pen.Color;
    if Execute then begin
      LabelImage.BackBmp.Canvas.Font.Color:= ColorDialog1.Color;
      EExampleText.Font.Color:= ColorDialog1.Color;
    end;
  end;
end;

procedure TFEdit.BtnSaveBMPClick(Sender: TObject);
begin
  with SaveDialog1 do
    if Execute then
      ImageEdit.Picture.SaveToFile(Filename);
end;

procedure TFEdit.BtnEditBMPClick(Sender: TObject);
begin
  LabelImage.BackBmp.SaveToFile('Deflaut.bmp');            //保存现图形
  ShellExecute(0,nil,'MSPAINT.EXE','Deflaut.bmp',nil,sw_shownormal);//画板
end;

procedure TFEdit.LoadData;
begin     //数据库读取函数
  with DQLabels do begin
    ELabelName.Text:= FieldByName('LabelName').AsString;
    //ECategoryIDX.Value:= FieldByName('CategoryIDX').AsInteger;
    LabelImage.BackBmp.Assign(FieldByName('Picture'));
    ImageEdit.Picture.Bitmap.Width:= LabelImage.BackBmp.Width; 
    ImageEdit.Picture.Bitmap.Height:= LabelImage.BackBmp.Height;
    EImgWidth.Value:= FieldByName('ImageWidth').AsInteger;
    EImgHeight.Value:= FieldByName('ImageLength').AsInteger;
    EExampleText.Color:= FieldByName('BaseColor').AsInteger;
    GpCopyMode.ItemIndex:= FieldByName('CopyStyle').AsInteger;
    //ELastNO.Value:= FieldByName('LastNO').AsInteger;
    ELabelWidth.Value:= FieldByName('LabelWidth').AsInteger;
    ELabelHeight.Value:= FieldByName('LabelHeight').AsInteger;
    ImagePreview.Picture.Bitmap.Width:= ELabelWidth.Value;    
    ImagePreview.Picture.Bitmap.Height:= ELabelHeight.Value;
    EFontName.ItemIndex:= EFontName.Items.IndexOf(FieldByName('FontName').AsString);
    EFontSize.Value:= FieldByName('FontSize').AsInteger;
    EExampleText.Font.Color:= FieldByName('FontColor').AsInteger;
    EWordWidth.Value:= FieldByName('WordWidth').AsInteger;
    ETextLeft.Value:= FieldByName('WordLeft').AsInteger;
    ETextTop.Value:= FieldByName('WordTop').AsInteger;
    EWordLength.Value:= FieldByName('WordLength').AsInteger;
    EAlign.ItemIndex:= FieldByName('Align').AsInteger;
    ChkBold.Checked:= FieldByName('IsBold').AsBoolean;
    ChkItalic.Checked:= FieldByName('IsItalic').AsBoolean;
    ChkStrikeOut.Checked:= FieldByName('IsStrikeOut').AsBoolean;
    ChkUnderline.Checked:= FieldByName('IsUnderline').AsBoolean;
  end;
  ETextTopChange(self);
end;

procedure TFEdit.SaveData;
begin     //数据库存储函数
  with DQLabels do begin
    if IsEdit then Edit
    else Append;
    FieldByName('LabelName').AsString:= ELabelName.Text;
    FieldByName('CategoryIDX').AsInteger:= ParentID;
    FieldByName('Picture').Assign(LabelImage.BackBmp);
    FieldByName('ImageWidth').AsInteger:= EImgWidth.Value;
    FieldByName('ImageLength').AsInteger:= EImgHeight.Value;
    FieldByName('BaseColor').AsInteger:= EExampleText.Color;
    FieldByName('CopyStyle').AsInteger:= GpCopyMode.ItemIndex;
    FieldByName('LabelWidth').AsInteger:= ELabelWidth.Value;
    FieldByName('LabelHeight').AsInteger:= ELabelHeight.Value;
    FieldByName('FontName').AsString:= EFontName.Text;
    FieldByName('FontSize').AsInteger:= EFontSize.Value;
    FieldByName('FontColor').AsInteger:= EExampleText.Font.Color;
    FieldByName('WordWidth').AsInteger:= EWordWidth.Value;
    FieldByName('WordLeft').AsInteger:= ETextLeft.Value;
    FieldByName('WordTop').AsInteger:= ETextTop.Value;      
    FieldByName('WordLength').AsInteger:= EWordLength.Value;
    FieldByName('Align').AsInteger:= EAlign.ItemIndex;
    FieldByName('IsBold').AsBoolean:= ChkBold.Checked;
    FieldByName('IsItalic').AsBoolean:= ChkItalic.Checked;
    FieldByName('IsStrikeout').AsBoolean:= ChkStrikeout.Checked;
    FieldByName('IsUnderline').AsBoolean:= ChkUnderline.Checked;
    Post;
    DM.AddTask(FieldByName('ID').AsInteger, 1, DM.LogonName, ELabelName.Text);
  end;
end;

procedure TFEdit.BtnSaveClick(Sender: TObject);
begin     //保存
  SaveData;
  Close;
end;

procedure TFEdit.BtnSaveAsClick(Sender: TObject);
begin     //另存为
  IsEdit:= False;
  SaveData;
  Close;
end;

procedure TFEdit.BitBtn2Click(Sender: TObject);
begin     //刷新
  Redrew;
end;

procedure TFEdit.EWordLengthChange(Sender: TObject);
begin
  EExampleText.Text:= Copy(EExampleText.Text + StringOfChar('0', EWordLength.Value), 1, EWordLength.Value);
  Redrew;
end;

procedure TFEdit.EAlignChange(Sender: TObject);
begin
  Redrew;
end;

end.
