object FFastPrinter: TFFastPrinter
  Left = 0
  Top = 0
  Caption = 'FFastPrinter'
  ClientHeight = 685
  ClientWidth = 899
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCanResize = FormCanResize
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 0
    Top = 0
    Width = 899
    Height = 217
    Align = alTop
    ExplicitWidth = 658
  end
  object Panel1: TPanel
    Left = 624
    Top = 217
    Width = 90
    Height = 468
    Align = alRight
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    object Label1: TLabel
      Left = 6
      Top = 375
      Width = 72
      Height = 13
      Caption = #22686#34917#20445#30041#20301#25968
    end
    object lbCount: TLabel
      Left = 6
      Top = 422
      Width = 6
      Height = 13
      Caption = '0'
    end
    object btnPrint: TBitBtn
      Left = 6
      Top = 180
      Width = 75
      Height = 25
      Caption = #25171#21360
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 2
      OnClick = btnPrintClick
    end
    object btnPrinter: TBitBtn
      Left = 6
      Top = 94
      Width = 75
      Height = 25
      Caption = #25171#21360#26426
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 0
      OnClick = btnPrinterClick
    end
    object ELostNo: TLabeledEdit
      Left = 6
      Top = 278
      Width = 75
      Height = 21
      EditLabel.Width = 24
      EditLabel.Height = 13
      EditLabel.Caption = #34917#21495
      MaxLength = 7
      TabOrder = 4
      Text = '1'
      OnKeyDown = ELostNoKeyDown
    end
    object btnMake: TBitBtn
      Left = 6
      Top = 149
      Width = 75
      Height = 25
      Caption = #29983#25104
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 1
      OnClick = btnMakeClick
    end
    object BtnDeleteRecord: TBitBtn
      Left = 6
      Top = 336
      Width = 75
      Height = 25
      Caption = #21024#38500#35760#24405
      DoubleBuffered = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333303
        333333333333337FF3333333333333903333333333333377FF33333333333399
        03333FFFFFFFFF777FF3000000999999903377777777777777FF0FFFF0999999
        99037F3337777777777F0FFFF099999999907F3FF777777777770F00F0999999
        99037F773777777777730FFFF099999990337F3FF777777777330F00FFFFF099
        03337F773333377773330FFFFFFFF09033337F3FF3FFF77733330F00F0000003
        33337F773777777333330FFFF0FF033333337F3FF7F3733333330F08F0F03333
        33337F7737F7333333330FFFF003333333337FFFF77333333333000000333333
        3333777777333333333333333333333333333333333333333333}
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 6
      OnClick = BtnDeleteRecordClick
    end
    object BtnClearRecords: TBitBtn
      Left = 6
      Top = 211
      Width = 75
      Height = 25
      Caption = #28165#38500#35760#24405
      DoubleBuffered = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
        333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
        0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
        07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
        07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
        0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
        33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
        B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
        3BB33773333773333773B333333B3333333B7333333733333337}
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 3
      OnClick = BtnClearRecordsClick
    end
    object BtnAddRecord: TBitBtn
      Left = 6
      Top = 305
      Width = 75
      Height = 25
      Caption = #22686#34917
      DoubleBuffered = False
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333333FFFFFFFFF333333000000000033333377777777773333330FFFFF
        FFF03333337F333333373333330FFFFFFFF03333337F3FF3FFF73333330F00F0
        00F03333F37F773777373330330FFFFFFFF03337FF7F3F3FF3F73339030F0800
        F0F033377F7F737737373339900FFFFFFFF03FF7777F3FF3FFF70999990F00F0
        00007777777F7737777709999990FFF0FF0377777777FF37F3730999999908F0
        F033777777777337F73309999990FFF0033377777777FFF77333099999000000
        3333777777777777333333399033333333333337773333333333333903333333
        3333333773333333333333303333333333333337333333333333}
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 5
      OnClick = BtnAddRecordClick
    end
    object EHeaderBit: TSpinEdit
      Left = 6
      Top = 394
      Width = 75
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 7
      Value = 4
    end
    object EStartNo: TLabeledEdit
      Left = 6
      Top = 27
      Width = 75
      Height = 21
      EditLabel.Width = 36
      EditLabel.Height = 13
      EditLabel.Caption = #36215#22987#21495
      TabOrder = 8
      Text = '1'
    end
    object ENum: TLabeledEdit
      Left = 6
      Top = 67
      Width = 45
      Height = 21
      EditLabel.Width = 24
      EditLabel.Height = 13
      EditLabel.Caption = #25968#37327
      TabOrder = 9
      Text = '60'
    end
    object EBit: TLabeledEdit
      Left = 55
      Top = 67
      Width = 26
      Height = 21
      EditLabel.Width = 24
      EditLabel.Height = 13
      EditLabel.Caption = #20301#25968
      TabOrder = 10
      Text = '7'
      OnChange = EBitChange
    end
    object chkFullBit: TCheckBox
      Left = 6
      Top = 126
      Width = 78
      Height = 17
      Caption = #19981#36275#20301#34917#38646
      Checked = True
      State = cbChecked
      TabOrder = 11
    end
  end
  object lstCode: TListBox
    Left = 0
    Top = 217
    Width = 624
    Height = 468
    Align = alClient
    Columns = 1
    ItemHeight = 13
    TabOrder = 1
  end
  object Panel2: TPanel
    Left = 714
    Top = 217
    Width = 185
    Height = 468
    Align = alRight
    BevelOuter = bvNone
    Caption = 'Panel2'
    ShowCaption = False
    TabOrder = 2
    object lstValues: TValueListEditor
      Left = 0
      Top = 0
      Width = 185
      Height = 139
      Align = alTop
      TabOrder = 0
      TitleCaptions.Strings = (
        #21442#25968
        #20540)
      ColWidths = (
        70
        109)
    end
    object mLog: TMemo
      Left = 0
      Top = 139
      Width = 185
      Height = 329
      Align = alClient
      Lines.Strings = (
        #25171#21360#35760#24405)
      ScrollBars = ssBoth
      TabOrder = 1
    end
  end
  object PrintDialog1: TPrintDialog
    Left = 288
    Top = 72
  end
  object PrinterSetupDialog1: TPrinterSetupDialog
    Left = 368
    Top = 72
  end
  object DQLabels: TADOQuery
    Connection = FFlyLabelDB.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT ID,LabelName FROM FL_Label'
      'WHERE CategoryIDX=:CID')
    Left = 96
    Top = 72
  end
  object DQLB: TADOQuery
    Connection = FFlyLabelDB.ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Content,Picture,Pixel  FROM FL_Label'
      'WHERE ID=:ID')
    Left = 168
    Top = 72
  end
  object DSLB: TDataSource
    DataSet = DQLB
    Left = 224
    Top = 72
  end
end
