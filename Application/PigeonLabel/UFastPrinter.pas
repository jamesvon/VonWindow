unit UFastPrinter;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, Buttons, ExtCtrls, IniFiles, UVonQRCode, Grids,
  DBCtrls, ValEdit, ULabelPrint, UFlyLabelDB, DB, ADODB, ComCtrls, Printers,
  UFrameGridBar, UZXIngQRCode, UVonXorCrypt, UVonGraphicAPI;

type
  TFFastPrinter = class(TForm)
    PrintDialog1: TPrintDialog;
    PrinterSetupDialog1: TPrinterSetupDialog;
    DQLabels: TADOQuery;
    DQLB: TADOQuery;
    DSLB: TDataSource;
    Panel1: TPanel;
    Label1: TLabel;
    lbCount: TLabel;
    btnPrint: TBitBtn;
    btnPrinter: TBitBtn;
    ELostNo: TLabeledEdit;
    btnMake: TBitBtn;
    BtnDeleteRecord: TBitBtn;
    BtnClearRecords: TBitBtn;
    BtnAddRecord: TBitBtn;
    EHeaderBit: TSpinEdit;
    EStartNo: TLabeledEdit;
    ENum: TLabeledEdit;
    lstCode: TListBox;
    EBit: TLabeledEdit;
    Panel2: TPanel;
    lstValues: TValueListEditor;
    mLog: TMemo;
    chkFullBit: TCheckBox;
    Image1: TImage;
    procedure btnPrinterClick(Sender: TObject);
    procedure btnMakeClick(Sender: TObject);
    procedure BtnClearRecordsClick(Sender: TObject);
    procedure BtnAddRecordClick(Sender: TObject);
    procedure BtnDeleteRecordClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure btnPrintClick(Sender: TObject);
    procedure ELostNoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EBitChange(Sender: TObject);
  private
    { Private declarations }
    numBmp: array[0..9]of TBitmap;
    FP: TLabelImage;
    FQR: TVonQRCode;
    procedure DrewNo(Canvas: TCanvas; DrewMode, WordLeft, WordTop, WordAlign,
      WordWidth, LEN: Integer; Code: string);
    procedure DrewQRCode(Canvas: TCanvas; DrewMode, CodeLeft, CodeTop: Integer; CodeBmp: TBitmap);
//      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'BKCOLOR', plCodeColor.Color);
//      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'FONTCOLOR', plCodeColor.Font.Color);
//      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Top', ECodeTop.Value);
//      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Left', ECodeLeft.Value);
//      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'CODETYPE', ECodeType.ItemIndex);
//      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'CODESIZE', ECodeSize.Value);
//      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'CODELEVEL', ECodeLevel.ItemIndex);
//      FCurrentLabel.Content.WriteString(EContentName.Text, 'Diameter', ECodeDiameter.Text);
//      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Rotate', ECodeRotate.ItemIndex);
//      FCurrentLabel.Content.WriteString(EContentName.Text, 'Version', ECodeVersion.Text);
//      FCurrentLabel.Content.WriteString(EContentName.Text, 'PrnDpi', ECodePrnDpi.Text);
//      FCurrentLabel.Content.WriteString(EContentName.Text, 'CodeValue', ECodeValue.Text);
//  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'CopyMode', GpCopyMode.ItemIndex);
//  FCurrentLabel.Content.WriteBool(EContentName.Text, 'SerialNo', chkSerialNo.Checked);
//  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'VRate', EVRate.Value);
//  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'HRate', EHRate.Value);
    function Roate(Source: TBitmap): TBitmap;
  public
    { Public declarations }
  end;

var
  FFastPrinter: TFFastPrinter;

implementation

{$R *.dfm}

procedure TFFastPrinter.FormCreate(Sender: TObject);
var
  szID: Integer;
  I: Integer;
  IniFile: TIniFile;
begin
  FQR:= TVonQRCode.Create;
  FP:= TLabelImage.Create;
  IniFile:= TIniFile.Create(ExtractFilePath(Application.ExeName) + 'FlyLabel.INI');
  with IniFile do begin
    EStartNo.Text:= ReadString('GBCode', 'START', '1');
    ENum.Text:= ReadString('GBCode', 'NUM', '1600');
    EHeaderBit.Value:= ReadInteger('GBCode', 'HeaderBit', 4);
  end;
  if TryStrToInt(FFlyLabelDB.ExecParams, szID) then
    DQLB.Parameters[0].Value:= szID
  else raise Exception.Create('Not found label to fast print');
  DQLB.Open;
  FP.Content.Text:= DQLB.FieldByName('Content').AsString;
  lstValues.Strings.Clear;
  for I := 0 to FP.Content.ReadSectionCount - 1 do
    with FP.Content.GetDirectionSection(I) do
      if Values['SerialNo'] = '0' then
        lstValues.InsertRow(FP.Content.SectionNames[I],
          IniFile.ReadString('LABEL', FP.Content.SectionNames[I], ''), True);
  IniFile.Free;
  FP.Pixel:= DQLB.FieldByName('Pixel').AsInteger;
  FP.OrgBmp.Assign(DQLB.FieldByName('Picture'));
end;

procedure TFFastPrinter.FormDestroy(Sender: TObject);
var
  I: Integer;
begin
  FP.Free;
  FQR.Free;
  with TIniFile.Create(ExtractFilePath(Application.ExeName) + 'FlyLabel.INI') do try
    WriteString('GBCode', 'START', EStartNo.Text);
    WriteString('GBCode', 'NUM', ENum.Text);
    for I := 1 to lstValues.RowCount - 1 do
      WriteString('LABEL', lstValues.Cells[0, I], lstValues.Cells[1, I]);
  finally
    Free;
  end;
end;

procedure TFFastPrinter.BtnAddRecordClick(Sender: TObject);
begin
  lstCode.Items.Add(Format('%.' + EBit.Text + 'd', [StrToInt(ELostNo.Text)]));
  ELostNo.Text:= Copy(ELostNo.Text, 1, EHeaderBit.Value);
  ELostNo.SelStart:= 20;
  lbCount.Caption:= Format('共有%d个标签', [lstCode.Count]);
end;

procedure TFFastPrinter.BtnClearRecordsClick(Sender: TObject);
begin
  lstCode.Items.Clear;
  btnMake.SetFocus;
end;

procedure TFFastPrinter.BtnDeleteRecordClick(Sender: TObject);
begin
  if lstCode.ItemIndex < 0 then Exit;
  lstCode.Items.Delete(lstCode.ItemIndex);
  lbCount.Caption:= Format('共有%d个标签', [lstCode.Count]);
end;

procedure TFFastPrinter.btnMakeClick(Sender: TObject);
var
  I, sNo, sNum: Integer;
begin
  sNo:= StrToInt(EStartNo.Text);
  sNum:= StrToInt(ENum.Text);
  for I := 0 to sNum - 1 do
    if chkFullBit.Checked then
      lstCode.Items.Add(Format('%.' + EBit.Text + 'd', [sNo + I]))
    else lstCode.Items.Add(IntToStr(sNo + I));
  EStartNo.Text:= IntToStr(sNo + sNum);
  btnPrint.SetFocus;
  lbCount.Caption:= Format('共有%d个标签', [lstCode.Count]);
end;

procedure TFFastPrinter.btnPrinterClick(Sender: TObject);
begin
  PrinterSetupDialog1.Execute();
end;

procedure TFFastPrinter.DrewNo(Canvas: TCanvas; DrewMode, WordLeft, WordTop, WordAlign, WordWidth, LEN: Integer; Code: string);
const
  numS = '0123456789';
var
  I: Integer;
begin
  with Canvas do begin
    case DrewMode of
    0: CopyMode:= cmBlackness;  //Fills the destination rectangle on the canvas with black.
    1: CopyMode:= cmDstInvert;  //Inverts the image on the canvas and ignores the source.
    2: CopyMode:= cmMergeCopy;  //Combines the image on the canvas and the source bitmap by using the Boolean AND operator.
    3: CopyMode:= cmMergePaint; //Combines the inverted source bitmap with the image on the canvas by using the Boolean OR operator.
    4: CopyMode:= cmNotSrcCopy; //Copies the inverted source bitmap to the canvas.
    5: CopyMode:= cmNotSrcErase;//Combines the image on the canvas and the source bitmap by using the Boolean OR operator, and inverts the result.
    6: CopyMode:= cmPatCopy;    //Copies the source pattern to the canvas.
    7: CopyMode:= cmPatInvert;  //Combines the source pattern with the image on the canvas using the Boolean XOR operator
    8: CopyMode:= cmPatPaint;   //Combines the inverted source bitmap with the source pattern by using the Boolean OR operator. Combines the result of this operation with the image on the canvas by using the Boolean OR operator.
    9: CopyMode:= cmSrcAnd;     //Combines the image on the canvas and source bitmap by using the Boolean AND operator.
    10: CopyMode:= cmSrcCopy;   //Copies the source bitmap to the canvas.
    11: CopyMode:= cmSrcErase;  //Inverts the image on the canvas and combines the result with the source bitmap by using the Boolean AND operator.
    12: CopyMode:= cmSrcInvert; //Combines the image on the canvas and the source bitmap by using the Boolean XOR operator.
    13: CopyMode:= cmSrcPaint;  //Combines the image on the canvas and the source bitmap by using the Boolean OR operator.
    14: CopyMode:= cmWhiteness; //Fills the destination rectangle on the canvas with white.
    end;
    case WordAlign of
    0: for i:= 1 to Length(Code) do       //
      Canvas.Draw(WordLeft - numBmp[Pos(Code[I], numS) - 1].Width, WordTop + WordWidth * (i - 1), numBmp[Pos(Code[I], numS) - 1]);     //左对齐
    1: for i:= 1 to Length(Code) do                                            //居中
      Canvas.Draw(WordLeft - numBmp[Pos(Code[I], numS) - 1].Width, WordTop + WordWidth * (LEN - Length(Code)) div 2 + WordWidth * (i - 1), numBmp[Pos(Code[I], numS) - 1]);
    2: for i:= Length(Code) downto 1 do                                        //右对齐
      Canvas.Draw(WordLeft - numBmp[Pos(Code[I], numS) - 1].Width, WordTop + WordWidth * (LEN - WordWidth) * i, numBmp[Pos(Code[I], numS) - 1]);
    3: if Length(Code) = 1 then                                                //
      Canvas.Draw(WordLeft - numBmp[Pos(Code[I], numS) - 1].Width, WordTop + WordWidth * (LEN - Length(Code)) div 2, numBmp[Pos(Code[I], numS) - 1])
    else for i:= 1 to Length(Code) do                                       //
      Canvas.Draw(WordLeft, WordTop + WordWidth * (LEN - Length(Code)) * (i - 1), numBmp[Pos(Code[I], numS) - 1]);
    end;
  end;
end;

procedure TFFastPrinter.DrewQRCode(Canvas: TCanvas; DrewMode, CodeLeft, CodeTop: Integer; CodeBmp: TBitmap);
var
  I: Integer;
begin
  with Canvas do begin
    case DrewMode of
    0: CopyMode:= cmBlackness;  //Fills the destination rectangle on the canvas with black.
    1: CopyMode:= cmDstInvert;  //Inverts the image on the canvas and ignores the source.
    2: CopyMode:= cmMergeCopy;  //Combines the image on the canvas and the source bitmap by using the Boolean AND operator.
    3: CopyMode:= cmMergePaint; //Combines the inverted source bitmap with the image on the canvas by using the Boolean OR operator.
    4: CopyMode:= cmNotSrcCopy; //Copies the inverted source bitmap to the canvas.
    5: CopyMode:= cmNotSrcErase;//Combines the image on the canvas and the source bitmap by using the Boolean OR operator, and inverts the result.
    6: CopyMode:= cmPatCopy;    //Copies the source pattern to the canvas.
    7: CopyMode:= cmPatInvert;  //Combines the source pattern with the image on the canvas using the Boolean XOR operator
    8: CopyMode:= cmPatPaint;   //Combines the inverted source bitmap with the source pattern by using the Boolean OR operator. Combines the result of this operation with the image on the canvas by using the Boolean OR operator.
    9: CopyMode:= cmSrcAnd;     //Combines the image on the canvas and source bitmap by using the Boolean AND operator.
    10: CopyMode:= cmSrcCopy;   //Copies the source bitmap to the canvas.
    11: CopyMode:= cmSrcErase;  //Inverts the image on the canvas and combines the result with the source bitmap by using the Boolean AND operator.
    12: CopyMode:= cmSrcInvert; //Combines the image on the canvas and the source bitmap by using the Boolean XOR operator.
    13: CopyMode:= cmSrcPaint;  //Combines the image on the canvas and the source bitmap by using the Boolean OR operator.
    14: CopyMode:= cmWhiteness; //Fills the destination rectangle on the canvas with white.
    end;
    Canvas.Draw(CodeLeft - CodeBmp.Width, CodeTop, CodeBmp);
  end;
end;

procedure TFFastPrinter.EBitChange(Sender: TObject);
begin
  ELostNo.MaxLength:= StrToInt(EBit.Text);
end;

procedure TFFastPrinter.ELostNoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
    BtnAddRecordClick(nil);
end;

procedure TFFastPrinter.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  lstCode.Columns:= lstCode.Width div 60;
end;

function TFFastPrinter.Roate(Source: TBitmap): TBitmap;
var
  X, Y: Integer;
begin
  Result:= TBitmap.Create;
  Result.Width:= Source.Height;
  Result.Height:= Source.Width;
  Result.Canvas.FillRect(Rect(0, 0, Result.Width, Result.Height));
  for X := 0 to Result.Width - 1 do
    for Y := 0 to Result.Height - 1 do
      Result.Canvas.Pixels[Result.Width - 1 - X, Y]:= Source.Canvas.Pixels[Y, X];   //szbmp.Width - 1 -
end;

procedure TFFastPrinter.btnPrintClick(Sender: TObject);
var
  i, X, Y, szValue, CurrentIdx, labelIdx, CodeMode, CodeLeft, CodeTop: Integer;
  radius, dpi, zoom: Extended;
  orgImg, bkImg, codeBmp: TBitmap;
  szValues: TStrings;
  szList: TStringList;
  szCode, szNo: string;
(*[编码] Kind=0 CopyMode=9 SerialNo=1 VRate=100 HRate=100 LEN=6 BKCOLOR=-16777201
FONTCOLOR=-16777208 Top=365 Left=15 WordWidth=86 Align=0 FontName=汉鼎繁特粗黑
FontSize=134 Rotate=1 Bold=0 Italic=0 Underline=0 StrikeOut=0 ExampleText=1234567 Items=*)
  function InitNumBmp(num: Integer): TBitmap;
  var
    szbmp: TBitmap;
  begin
    szbmp:= TBitmap.Create;
    szbmp.PixelFormat := pf24Bit;
    if FP.Content.SectionExists('编号') then
      szValues:= FP.Content.GetDirectionSection('编号');
    if FP.Content.SectionExists('编码') then
      szValues:= FP.Content.GetDirectionSection('编码');
    with szValues do begin
      szbmp.Canvas.Brush.Color:= StrToInt(Values['BKCOLOR']);
      szbmp.Canvas.Font.Color:= StrToInt(Values['FONTCOLOR']);
      szbmp.Canvas.Font.Name:= Values['FontName'];
      szbmp.Canvas.Font.Size:= StrToInt(Values['FontSize']);
      szbmp.Canvas.Font.Style:= [];
      if Values['Bold'] = '1' then
        szbmp.Canvas.Font.Style:= szbmp.Canvas.Font.Style + [fsBold];
      if Values['Italic'] = '1' then
        szbmp.Canvas.Font.Style:= szbmp.Canvas.Font.Style + [fsItalic];
      if Values['Underline'] = '1' then
        szbmp.Canvas.Font.Style:= szbmp.Canvas.Font.Style + [fsUnderline];
      if Values['StrikeOut'] = '1' then
        szbmp.Canvas.Font.Style:= szbmp.Canvas.Font.Style + [fsStrikeOut];
      szbmp.Width:= szbmp.Canvas.TextWidth(IntToStr(num));
      szbmp.Height:= szbmp.Canvas.TextHeight(IntToStr(num));
      szbmp.Canvas.FillRect(Rect(0, 0, szbmp.Width, szbmp.Height));
      szbmp.Canvas.TextOut(0, 0, IntToStr(num));
      Result:= Rotate90(szbmp);
      szbmp.Free;
    end;
    zoom:= 0;
    if FP.Content.SectionExists('二维码') then
      with FP.Content.GetDirectionSection('二维码') do begin
        CodeMode:= StrToInt(Values['CopyMode']);
        CodeLeft:= StrToInt(Values['Left']);
        CodeTop:= StrToInt(Values['Top']);
        szList.DelimitedText:= Values['CodeValue'];
        radius:= StrToFloat(Values['Diameter']) / 2;
        dpi:= StrToFloat(Values['PrnDpi']);
        zoom:= StrToInt(Values['CODESIZE']);
      end;
  end;
begin
  mLog.Lines.Clear;
  orgImg:= TBitmap.Create;
  bkImg:= TBitmap.Create;
  szList:= TStringList.Create;
  szList.Delimiter:= '+';
  for I := 1 to lstValues.RowCount - 1 do
    FP.LabelValues.Add(lstValues.Cells[0, I], lstValues.Cells[1, I]);
  mLog.Lines.Add('加载参数');
  numBmp[0]:= InitNumBmp(0);
  numBmp[1]:= InitNumBmp(1);
  numBmp[2]:= InitNumBmp(2);
  numBmp[3]:= InitNumBmp(3);
  numBmp[4]:= InitNumBmp(4);
  numBmp[5]:= InitNumBmp(5);
  numBmp[6]:= InitNumBmp(6);
  numBmp[7]:= InitNumBmp(7);
  numBmp[8]:= InitNumBmp(8);
  numBmp[9]:= InitNumBmp(9);
  mLog.Lines.Add('加载数字图像');
  FP.DrewBaseBmp;
  orgImg.Assign(Roate(FP.FBaseBmp));
  orgImg.PixelFormat:= pf24bit;
  mLog.Lines.Add('加载背景');

  if LstCode.Count < 1 then
    raise Exception.Create('没有打印任务。');
  CurrentIdx:= 0;
  if Printer.Printing then Printer.EndDoc;
  Printer.Title:= Format('LB%sX%d', [LstCode.Items[0], LstCode.Count]);
  mLog.Lines.Text:= Format('------ 从%s开始，打印%s个', [LstCode.Items[0], ENum.Text]);
  mLog.Lines.Add(Format('[%s]开始打印第1页', [TimeToStr(Now)]));
  Printer.BeginDoc;
  Printer.Canvas.Brush.Style:= bsClear;
  while CurrentIdx < lstCode.Count do begin
    labelIdx:= CurrentIdx mod 20;
    if(CurrentIdx > 0)and(labelIdx = 0)then begin
      Printer.NewPage;
//  800 / 25.4 * 220 = 6929.1338582677165354330708661417
//  800 / 25.4 * 31.5 = 992.12598425196850393700787401575
      //Printer.Canvas.FillRect(Rect(0, 0, 6929, 992));
      mLog.Lines.Add(Format('[%s]正在打印第%d页.', [TimeToStr(Now), (CurrentIdx div 20) + 1]));
    end;
    X:= Round(labelIdx * 330.708 + 125.984);
    Printer.Canvas.Pen.Color:= clWhite;
    Printer.Canvas.Brush.Color:= clWhite;
    Printer.Canvas.CopyMode:= cmSrcCopy;
    Printer.Canvas.FillRect(Rect(X, 0, X + bkImg.Width, bkImg.Height));
    Printer.Canvas.Draw(X, 0, bkImg);
{$IFDEF BK_DREW}
    DrewNo(Printer.Canvas,
      StrToInt(szValues.Values['CopyMode']),
      bkImg.Width - StrToInt(szValues.Values['Top']) + X,   //纵向打印，Top->Left,left->Top
      StrToInt(szValues.Values['Left']),
      StrToInt(szValues.Values['Align']),
      StrToInt(szValues.Values['WordWidth']),
      StrToInt(szValues.Values['LEN']), lstCode.Items[CurrentIdx]);
    if Zoom > 0 then begin
      szCode:= '';
      for I := 0 to szList.Count - 1 do begin
        szNo:= lstValues.Strings.Values[szList[I]];
        if szNo = '' then szNo:= FP.Content.ReadString(szList[I], 'ExampleText', '0');
        szCode:= szCode + szNo;
      end;
      DrewQRCode(Printer.Canvas, CodeMode, bkImg.Width - CodeTop + X, CodeLeft,
        FP.QRRoateCode(StrToInt64(szCode + lstCode.Items[CurrentIdx]), radius, dpi, zoom));
    end;
{$ELSE}
    bkImg.Assign(orgImg);
    DrewNo(bkImg.Canvas,
      StrToInt(szValues.Values['CopyMode']),
      bkImg.Width - StrToInt(szValues.Values['Top']),   //纵向打印，Top->Left,left->Top
      StrToInt(szValues.Values['Left']),
      StrToInt(szValues.Values['Align']),
      StrToInt(szValues.Values['WordWidth']),
      StrToInt(szValues.Values['LEN']), lstCode.Items[CurrentIdx]);
    if Zoom > 0 then begin
      szCode:= '';
      for I := 0 to szList.Count - 1 do begin
        szNo:= lstValues.Strings.Values[szList[I]];
        if szNo = '' then szNo:= FP.Content.ReadString(szList[I], 'ExampleText', '0');
        szCode:= szCode + szNo;
      end;
      codeBmp:= FP.QRRoateCode(StrToInt64(szCode + lstCode.Items[CurrentIdx]), radius, dpi, zoom);
      DrewQRCode(bkImg.Canvas, CodeMode, bkImg.Width - CodeTop, CodeLeft, codeBmp);
      codeBmp.Free;
    end;
    Printer.Canvas.Draw(X, 0, bkImg);
    bkImg.Canvas.FillRect(Rect(0,0,bkImg.Width,bkImg.Height));
    Application.ProcessMessages;
{$ENDIF}
    Inc(CurrentIdx);
  end;
  Printer.EndDoc;
end;

end.
