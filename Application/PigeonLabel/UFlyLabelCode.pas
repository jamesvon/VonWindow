unit UFlyLabelCode;

interface

uses SysUtils, Classes, strUtils, Graphics, DCPsha1, UVonQRCode;

procedure CodeToBmp(Code: PChar; Bmp: TBitmap);

implementation

function CheckCode(Code: PChar): PChar;
begin

end;

function CodeToStr(Code: PChar): string;
var
  szI: Integer;
  szCode: TBytes;
  digest: array[0..19] of byte;
  S: string;
  Hash: TDCP_sha1;
begin
  Result:= '';
  SetLength(szCode, 17);
  S:= string(Code);
  if Length(S) < 12 then Exit;
  if not TryStrToInt(Copy(S, 1, 4), szI) then Exit;       //Year -> 7F
  szCode[0]:= ((szI + 1) and $7F) + $80;                  //1 1111111        FF
  if not TryStrToInt(Copy(S, 5, 2), szI) then Exit;       //Area -> 7F
  szCode[1]:= (szI and $7F) + $80;                        //1 111111
  if not TryStrToInt(Copy(S, 7, MaxInt), szI) then Exit;  //Code -> FFFFFF
  szCode[2]:= $F0 + (szI shr 20);
  szCode[3]:= szI shr 12;
  szCode[4]:= $F0 + (szI shr 8) and $F;
  szCode[5]:= szI;
  Hash := TDCP_sha1.Create(nil); // create the hash
  Hash.Init;                      // initialize it
  Hash.Update(szCode[0], 5);      // hash the stream contents
  Hash.Final(Digest);             // produce the digest
  for szI := 6 to 16 do
    if Digest[szI] = 0 then szCode[szI] := $EA
    else szCode[szI] := Digest[szI];
  result:= TEncoding.Unicode.GetString(szCode);// Char(szCode[0] * 256;
  Hash.Free;
end;

procedure CodeToBmp(Code: PChar; Bmp: TBitmap);
var
  szCode: TQRCode;
begin
  szCode:= TQRCode.Create(nil);
  szCode.ClearOption:= True;
  szCode.Match:= True;
  szCode.Pxmag:= 7;
  szCode.Code:= CodeToStr(Code);
  Bmp.Assign(szCode.Picture.Bitmap);
  szCode.Free;
end;

end.
