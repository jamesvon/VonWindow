inherited FFlyLabelDB: TFFlyLabelDB
  OldCreateOrder = True
  Width = 423
  inherited ADOConn: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;Initial Catalog=FlyLabel;'
    Mode = cmShareDenyNone
  end
  object DQCategory: TADOQuery
    Connection = ADOConn
    Parameters = <
      item
        Name = 'ParentID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM FL_Category'
      'WHERE IDX=:ParentID'
      'ORDER BY ListOrder')
    Left = 120
    Top = 48
  end
  object DQLables: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'CID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'SELECT [ID], [LabelName]'
      'FROM [FL_Label]'
      'WHERE [CategoryIDX]=:CID')
    Left = 120
    Top = 96
  end
  object DQOrgLabel: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'OID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM [FL_Label]'
      'WHERE [ID]=:OID')
    Left = 120
    Top = 192
  end
  object DQNewID: TADOQuery
    Connection = ADOConn
    Parameters = <>
    SQL.Strings = (
      'SELECT @@IDENTITY')
    Left = 120
    Top = 240
  end
  object DCCategory: TADOCommand
    Connection = ADOConn
    Parameters = <>
    Left = 32
    Top = 256
  end
  object DSLables: TDataSource
    DataSet = DQLables
    Left = 180
    Top = 96
  end
  object DSLabel: TDataSource
    DataSet = DQLabel
    Left = 180
    Top = 144
  end
  object DQLabel: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    DataSource = DSLables
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'SELECT * FROM FL_Label WHERE [ID]=:ID')
    Left = 120
    Top = 144
  end
  object DQSchemeKinds: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT DISTINCT ParamKind FROM FL_Params')
    Left = 240
    Top = 56
  end
  object DQParam: TADOQuery
    Connection = ADOConn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'Kind'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'Name'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'SELECT ParamKind, ParamName, ParamValue'
      'FROM FL_Params'
      'WHERE ParamKind=:Kind'
      'AND ParamName=:Name')
    Left = 240
    Top = 104
  end
  object DCDelScheme: TADOCommand
    CommandText = 'DELETE'#13#10'FROM FL_Params'#13#10'WHERE ParamKind=:Kind'
    Connection = ADOConn
    Parameters = <
      item
        Name = 'Kind'
        Size = -1
        Value = Null
      end>
    Left = 240
    Top = 152
  end
end
