unit UFlyLabelDB;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UPlatformDB, ImgList, DB, ADODB, System.ImageList, UVonLog, UVonCrypt;

type
  /// <summary>证书内容信息</summary>
  TCertData = class(TCryptItem)
  private
    FSubjectValue: string;
    FSubjectData: TBytes;
  public
    procedure ReadFromStream(Stream: TStream); override;
    procedure WriteToStream(Stream: TStream); override;
    procedure ResizeData(size: Integer);
  published
    property SubjectData: TBytes read FSubjectData write FSubjectData;
    property SubjectValue: string read FSubjectValue write FSubjectValue;
  end;

  TFFlyLabelDB = class(TFPlatformDB)
    DQCategory: TADOQuery;
    DQLables: TADOQuery;
    DQOrgLabel: TADOQuery;
    DQNewID: TADOQuery;
    DCCategory: TADOCommand;
    DSLables: TDataSource;
    DSLabel: TDataSource;
    DQLabel: TADOQuery;
    DQSchemeKinds: TADOQuery;
    DQParam: TADOQuery;
    DCDelScheme: TADOCommand;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FCert: TCryptCollection<TCertData>;
    procedure EventOnUpgrade(Sender: TObject);
    function GetCertData(name: string): TBytes;
    function GetCertValue(name: string): string;
    procedure SetCertData(name: string; const Value: TBytes);
    procedure SetCertValue(name: string; const Value: string);
  public
    { Public declarations }
    CanRun: Boolean;
    procedure InitData;
    property CertValue[name: string]: string read GetCertValue write SetCertValue;
    property CertData[name: string]: TBytes read GetCertData write SetCertData;
  end;

var
  FFlyLabelDB: TFFlyLabelDB;

implementation

uses UVonSystemFuns, UPlatformUpgrade, UPlatformLogin;

{$R *.dfm}

procedure TFFlyLabelDB.DataModuleCreate(Sender: TObject);
var
  S: string;
  b: Boolean;
begin
  inherited;
  FCert.LoadFromFile(AppPath + CERT_FILE, CERT_KEY, CERT_CIPHER, CERT_HASH);
  {$region '连接数据库'}
  ADOConn.Close;
  S := CertValue['DBConnectString'];
  if S <> '' then ADOConn.ConnectionString:= S;
  ADOConn.Open();
  if not ADOConn.Connected then raise Exception.Create('Failed to connect database.');
  {$endregion}
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'Will update ......'); {$endif}
  {$region '自动升级'}
  if CertValue['AutoUpgrade'] = 'True' then
    with TFPlatformUpgrade.Create(nil) do
      try
        OnUpgrade:= EventOnUpgrade;
        CanRun := ShowModal = mrYes;
        if not CanRun then Application.Terminate;
      finally
        Free;
      end;
  {$endregion}
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'Will LoadRuntime ......'); {$endif}
  {$region '证书检查'}
  if StrToDatetime(CertValue['Publishedate']) > Now then raise Exception.Create('证书异常！');
  S:= CertValue['LastAccessTime'];
  if(S <> '')and(StrToDatetime(S) > Now)then raise Exception.Create('证书异常！');
  if StrToDatetime(CertValue['Validate']) < Now then raise Exception.Create('证书异常！');
  {$endregion}
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'Will LoadRuntime ......'); {$endif}
  LoadRuntime;
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'AutoLogin=' + CertValue['AutoLogin']); {$endif}
  {$region 'Login'}
  S:= CertValue['AutoLogin'];
  if S = '' then begin
    CanRun := true;
    LoginInfo.LoginName:= 'Guest';
    LoginInfo.DisplayName:= '匿名用户';
  end else if TryStrToBool(S, b) then begin
    if (not b) then CanRun := Login
    else begin
      CanRun := true;
      LoginInfo.LoginName:= 'Guest';
      LoginInfo.DisplayName:= '匿名用户';
    end
  end else CanRun := LoginInfo.Login(S, S, ADOConn);
  {$endregion}
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'Will LoadUsings ......'); {$endif}
  LoadUsings;
  FOnSay := nil;
end;

procedure TFFlyLabelDB.DataModuleDestroy(Sender: TObject);
begin
  CertValue['LastAccessDate']:= DateTimeToStr(Now);
  FCert.SaveToFile(AppPath + CERT_FILE, CERT_KEY, CERT_CIPHER, CERT_HASH);
  FCert.Free;
  inherited;
end;

procedure TFFlyLabelDB.EventOnUpgrade(Sender: TObject);
var
  szUpgrade: TArray<string>;
begin
  szUpgrade:= CertValue['UpgradeUrl'].Split([',']);
  (Sender as TFPlatFormUpgrade).UpgradeProcess(szUpgrade[0], szUpgrade[1], StrToInt(szUpgrade[2]));
end;

{$region 'property of certification'}

function TFFlyLabelDB.GetCertData(name: string): TBytes;
var
  item: TCertData;
begin
  item:= FCert.Items[name];
  if Assigned(item) then Result:= item.SubjectData else Result:= nil;
end;

function TFFlyLabelDB.GetCertValue(name: string): string;
var
  item: TCertData;
begin
  item:= FCert.Items[name];
  if Assigned(item) then Result:= item.SubjectValue else Result:= '';
end;

procedure TFFlyLabelDB.SetCertData(name: string; const Value: TBytes);
var
  item: TCertData;
begin
  item:= FCert.Items[name];
  if Assigned(item) then
    item.SubjectData:= Value
  else begin
    item:= TCertData.Create;
    item.Name:= name;
    item.SubjectData:= Value;
    FCert.Add(item);
  end;
end;

procedure TFFlyLabelDB.SetCertValue(name: string; const Value: string);
var
  item: TCertData;
begin
  item:= FCert.Items[name];
  if Assigned(item) then item.SubjectValue:= Value
  else begin
    item:= TCertData.Create;
    item.Name:= name;
    item.SubjectValue:= Value;
    FCert.Add(item);
  end;
end;

{$endregion}

procedure TFFlyLabelDB.InitData();
begin

end;

{ TCertData }

procedure TCertData.ReadFromStream(Stream: TStream);
var
  FSubjectValueSize: Int64;
begin
  inherited;
  Name:= ReadStringFromStream(Stream);
  FSubjectValue:= ReadStringFromStream(Stream);
  FSubjectValueSize:= ReadInt64FromStream(Stream);
  SetLength(FSubjectData, FSubjectValueSize);
  Stream.Read(FSubjectData, FSubjectValueSize);
end;

procedure TCertData.ResizeData(size: Integer);
begin
  SetLength(FSubjectData, size);
end;

procedure TCertData.WriteToStream(Stream: TStream);
begin
  inherited;
  WriteStringToStream(Name, Stream);
  WriteStringToStream(FSubjectValue, Stream);
  WriteInt64ToStream(Length(FSubjectData), Stream);
  Stream.Write(FSubjectData, Length(FSubjectData));
end;

end.
