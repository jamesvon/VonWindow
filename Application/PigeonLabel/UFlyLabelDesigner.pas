unit UFlyLabelDesigner;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ComCtrls, ExtCtrls, UFlyLabelDB, StdCtrls, UFrameGridBar;

type
  TFFlyLabelDesigner = class(TForm)
    Image1: TImage;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DQLabel: TADOQuery;
    FrameGridBar1: TFrameGridBar;
    Label1: TLabel;
    ELabelName: TEdit;
    Label2: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFlyLabelDesigner: TFFlyLabelDesigner;

implementation

{$R *.dfm}

end.
