unit UFlyLabelEdit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, Spin, JPEG, ComCtrls, IniFiles, ADODB,
  OleCtnrs, Menus, ShellAPI, ULabelPrint, UVonClass, ImgList, ToolWin, DB,
  UFlyLabelInfo, UVonGraphicAPI, System.ImageList;

type
  TFFlyLabelEdit = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel4: TPanel;
    GpCopyMode: TRadioGroup;
    RGSizeOption: TGroupBox;
    Label4: TLabel;
    EImgWidth: TSpinEdit;
    Label7: TLabel;
    EImgHeight: TSpinEdit;
    Label8: TLabel;
    ELabelWidth: TSpinEdit;
    Label9: TLabel;
    ELabelHeight: TSpinEdit;
    Label10: TLabel;
    ELabelName: TEdit;
    Label11: TLabel;
    Label12: TLabel;
    LMouseXY: TLabel;
    LMouseRate: TLabel;
    OpenDialog1: TOpenDialog;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    ScrollBox1: TScrollBox;
    ImageEdit: TImage;
    ScrollBox2: TScrollBox;
    ImagePreview: TImage;
    ColorDialog1: TColorDialog;
    SaveDialog1: TSaveDialog;
    Panel3: TPanel;
    BtnLoadBMP: TBitBtn;
    BtnEditBMP: TBitBtn;
    BtnSaveBMP: TBitBtn;
    BitBtn1: TBitBtn;
    Label18: TLabel;
    EPixel: TComboBox;
    BtnLoadDefault: TBitBtn;
    BtnSaveDefault: TBitBtn;
    Panel6: TPanel;
    pcTask: TPageControl;
    TabSheet3: TTabSheet;
    Label3: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    Label14: TLabel;
    ETextTop: TSpinEdit;
    ETextLeft: TSpinEdit;
    ETextFontSize: TSpinEdit;
    chkTextBold: TCheckBox;
    chkTextUnderline: TCheckBox;
    ETextWordWidth: TSpinEdit;
    ETextAlign: TComboBox;
    chkTextStrikeOut: TCheckBox;
    ChkTextItalic: TCheckBox;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    Panel7: TPanel;
    Label17: TLabel;
    btnContentSave: TBitBtn;
    btnContentDel: TBitBtn;
    btnSynLabel: TButton;
    btnSynPicture: TButton;
    imgButton: TImageList;
    Label19: TLabel;
    ECodeSize: TSpinEdit;
    Label20: TLabel;
    ECodeType: TComboBox;
    Panel8: TPanel;
    ToolBar1: TToolBar;
    btnParamMoveToTop: TToolButton;
    btnParamMoveToUp: TToolButton;
    btnParamMoveToDown: TToolButton;
    btnParamMoveToBottom: TToolButton;
    lstTask: TTreeView;
    EFileName: TLabeledEdit;
    SpeedButton3: TSpeedButton;
    Label21: TLabel;
    EBmpTop: TSpinEdit;
    Label22: TLabel;
    EBmpLeft: TSpinEdit;
    Label24: TLabel;
    Label25: TLabel;
    ECodeTop: TSpinEdit;
    Label26: TLabel;
    ECodeLeft: TSpinEdit;
    Label27: TLabel;
    EBmpWidth: TSpinEdit;
    Label28: TLabel;
    EBmpHeight: TSpinEdit;
    EContentName: TEdit;
    btnCheck: TButton;
    ECheckCode: TEdit;
    btnClear: TBitBtn;
    Label29: TLabel;
    ETextDefault: TEdit;
    ETextRotate: TComboBox;
    Label16: TLabel;
    ETextLength: TSpinEdit;
    Label1: TLabel;
    EExampleText: TEdit;
    btnRefrensh: TBitBtn;
    EBmpRotate: TComboBox;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    TabSheet9: TTabSheet;
    TabSheet10: TTabSheet;
    chkSerialNo: TCheckBox;
    mPointers: TMemo;
    Panel9: TPanel;
    EPointerX: TSpinEdit;
    Label31: TLabel;
    EPointerY: TSpinEdit;
    Label32: TLabel;
    btnPtAdd: TBitBtn;
    Panel10: TPanel;
    Label34: TLabel;
    Label35: TLabel;
    ELineX0: TSpinEdit;
    ELineY0: TSpinEdit;
    btnLineAdd: TBitBtn;
    mLines: TMemo;
    Label37: TLabel;
    EPointerSize: TSpinEdit;
    EPointerKind: TComboBox;
    Label38: TLabel;
    ELineSize: TComboBoxEx;
    Label39: TLabel;
    ECodePrnDpi: TEdit;
    ECodeVersion: TComboBox;
    Label23: TLabel;
    Label40: TLabel;
    lstItems: TMemo;
    imgLine: TImageList;
    Panel11: TPanel;
    Label41: TLabel;
    Label42: TLabel;
    ECircleX0: TSpinEdit;
    ECircleY0: TSpinEdit;
    btnCircle: TBitBtn;
    mCircle: TMemo;
    Label43: TLabel;
    Label44: TLabel;
    ECircleX1: TSpinEdit;
    ECircleY1: TSpinEdit;
    Panel12: TPanel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    ERectangleX0: TSpinEdit;
    ERectangleY0: TSpinEdit;
    btnRectangleAdd: TBitBtn;
    ERectangleX1: TSpinEdit;
    ERectangleY1: TSpinEdit;
    mRectangle: TMemo;
    Label49: TLabel;
    ELineX1: TSpinEdit;
    Label50: TLabel;
    ELineY1: TSpinEdit;
    plLineColor: TPanel;
    plTextColor: TPanel;
    plCodeColor: TPanel;
    plPointerColor: TPanel;
    plCircleColor: TPanel;
    plRectangleColor: TPanel;
    Label33: TLabel;
    ECodeLevel: TComboBox;
    Panel13: TPanel;
    Label36: TLabel;
    EVRate: TSpinEdit;
    Label51: TLabel;
    EHRate: TSpinEdit;
    Label52: TLabel;
    ECodeValue: TEdit;
    ETextFontName: TComboBox;
    Label5: TLabel;
    ECodeRotate: TComboBox;
    Label53: TLabel;
    ECodeDiameter: TEdit;
    mLog: TMemo;
    BtnSave: TBitBtn;
    BtnSaveAs: TBitBtn;
    BtnCancel: TBitBtn;
    BitBtn5: TBitBtn;
    procedure ImageEditMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImageEditMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnLoadBMPClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnLoadDefaultClick(Sender: TObject);
    procedure BtnSaveDefaultClick(Sender: TObject);
    procedure ETextTopChange(Sender: TObject);
    procedure EImgWidthChange(Sender: TObject);
    procedure ELabelWidthChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtnBackColorClick(Sender: TObject);
    procedure BtnSaveBMPClick(Sender: TObject);
    procedure BtnEditBMPClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure BtnSaveAsClick(Sender: TObject);
    procedure btnRefrenshClick(Sender: TObject);
    procedure ETextAlignChange(Sender: TObject);
    procedure btnContentSaveClick(Sender: TObject);
    procedure btnContentDelClick(Sender: TObject);
    procedure btnParamMoveToTopClick(Sender: TObject);
    procedure btnSynLabelClick(Sender: TObject);
    procedure btnSynPictureClick(Sender: TObject);
    procedure btnParamMoveToUpClick(Sender: TObject);
    procedure btnParamMoveToDownClick(Sender: TObject);
    procedure btnParamMoveToBottomClick(Sender: TObject);
    procedure lstTaskDblClick(Sender: TObject);
    procedure GpCopyModeClick(Sender: TObject);
    procedure btnCheckClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure lstTaskGetSelectedIndex(Sender: TObject; Node: TTreeNode);
    procedure BtnCancelClick(Sender: TObject);
    procedure btnPtAddClick(Sender: TObject);
    procedure plColorMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnLineAddClick(Sender: TObject);
    procedure mPointersDblClick(Sender: TObject);
    procedure mLinesDblClick(Sender: TObject);
    procedure btnCircleClick(Sender: TObject);
    procedure mCircleDblClick(Sender: TObject);
    procedure btnRectangleAddClick(Sender: TObject);
    procedure mRectangleDblClick(Sender: TObject);
    procedure ETextFontNameDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
  private
    { Private declarations }
    //CanDrew: boolean;
    LabelImage: TLabelImage;
    FCurrentLabel: TLabelInfo;
    procedure LoadBackground(Filename: string);
    procedure Redrew;
  public
    { Public declarations }
    IsEdit: Boolean;
    DQLabels: TADOQuery;
    procedure LoadData(DataSet: TCustomADODataSet);
    procedure SaveData;
  end;

var
  FFlyLabelEdit: TFFlyLabelEdit;

implementation

uses UFlyLabelDB;

{$R *.dfm}

{ TFEdit }

procedure TFFlyLabelEdit.FormCreate(Sender: TObject);
begin     //初始化
  FCurrentLabel:= TLabelInfo.Create;
  ETextFontName.Items:= Screen.Fonts;
  LabelImage:= TLabelImage.Create;
  BtnLoadDefaultClick(nil);
  if FFlyLabelDB.ExecParams <> '' then begin
    if not FFlyLabelDB.DQLabel.EOF then
      LoadData(FFlyLabelDB.DQLabel);
  end else FCurrentLabel.ID:= 0;
  if FFlyLabelDB.TaskParams <> '' then
    FCurrentLabel.CategoryIDX:= StrToInt(FFlyLabelDB.TaskParams)
  else FCurrentLabel.CategoryIDX:= 1;
end;

procedure TFFlyLabelEdit.FormDestroy(Sender: TObject);
begin     //卸载
  FCurrentLabel.Free;
  LabelImage.Free;
end;

(* 背景图操作按钮及鼠标事件 *)

procedure TFFlyLabelEdit.LoadBackground(Filename: string);
var
  bmp: TBitmap;
begin     //提取背景图片
  //CanDrew:= false;
  bmp:= TBitmap.Create;
  LoadGraphicFile(Filename, 0, 0, bmp);
  LabelImage.OrgBmp.Assign(bmp);
  ImageEdit.Picture.Assign(bmp);
  EImgWidth.Value:= bmp.Width;
  EImgHeight.Value:= bmp.Height;
  bmp.Free;
end;

procedure TFFlyLabelEdit.BtnLoadBMPClick(Sender: TObject);
begin     //提取指定的背景图
  with OpenDialog1 do
    if Execute then LoadBackground(FileName);
end;

procedure TFFlyLabelEdit.BitBtn1Click(Sender: TObject);
begin     //提取缺省的背景图
  LoadBackground(ExtractFilePath(Application.ExeName) + 'Deflaut.BMP');
end;

procedure TFFlyLabelEdit.BtnEditBMPClick(Sender: TObject);
begin     //编辑背景图
  LabelImage.OrgBmp.SaveToFile('Deflaut.bmp');            //保存现图形
  ShellExecute(0,nil,'MSPAINT.EXE','Deflaut.bmp',nil,sw_shownormal);//画板
end;

procedure TFFlyLabelEdit.BtnSaveBMPClick(Sender: TObject);
begin     //存储背景图
  with SaveDialog1 do
    if Execute then
      ImageEdit.Picture.SaveToFile(Filename);
end;

procedure TFFlyLabelEdit.ImageEditMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin     //鼠标跟踪
  LMouseXY.Caption:= format('%d:%d', [X, Y]);
  if(ImageEdit.Picture.Width = 0)or(ImageEdit.Picture.Height = 0)then
    LMouseRate.Caption:= '-, -'
  else LMouseRate.Caption:= format('%.2f%%, %.2f%%',
    [X/ImageEdit.Picture.Width*100, Y/ImageEdit.Picture.Height*100]);
end;

procedure TFFlyLabelEdit.ImageEditMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin     //鼠标在图片上定点
  case pcTask.ActivePageIndex of
  0: begin ETextTop.Value:= Y; ETextLeft.Value:= X; end;        //Text
  1: begin EBmpTop.Value:= Y; EBmpLeft.Value:= X; end;          //BMP
  2: begin ECodeTop.Value:= Y; ECodeLeft.Value:= X; end;        //QRCode
  3: begin EPointerX.Value:= Y; EPointerY.Value:= X; end;       //Pointer
  4: begin ELineX0.Value:= Y; ELineY0.Value:= X; end;           //Line
  5: begin ECircleX0.Value:= Y; ECircleY0.Value:= X; end;       //Circle
  6: begin ERectangleX0.Value:= Y; ERectangleY0.Value:= X; end; //Rectangle
  end;
  Redrew;
end;

(* 标签基本信息基操作事件 *)

procedure TFFlyLabelEdit.BtnLoadDefaultClick(Sender: TObject);
begin     //提取缺省图片参数
  with TIniFile.Create(ExtractFilePath(Application.ExeName) + 'FlyLabel.INI')do try
    EExampleText.Text:= ReadString('DEFAULT', 'Example', '000000');
    EExampleText.Color:= ReadInteger('DEFAULT', 'BKCOLOR', 0);
    EExampleText.Font.Color:= ReadInteger('DEFAULT', 'FONTCOLOR', 0);
    ETextLength.Value:= ReadInteger('DEFAULT', 'LEN', 6);
    ETextTop.Value:= ReadInteger('DEFAULT', 'TextTop', 0);
    ETextLeft.Value:= ReadInteger('DEFAULT', 'TextLeft', 0);
    ETextWordWidth.Value:= ReadInteger('DEFAULT', 'WordWidth', 0);
    ETextFontSize.Value:= ReadInteger('DEFAULT', 'FontSize', 9);
    ETextAlign.ItemIndex:= ReadInteger('DEFAULT', 'Align', 0);
    ETextFontName.ItemIndex:= ETextFontName.Items.IndexOf(ReadString('DEFAULT', 'FontName', 'Arial'));
    ETextRotate.ItemIndex:= ReadInteger('DEFAULT', 'Rotate', 0);
    ChkTextBold.Checked:= ReadBool('DEFAULT', 'FontBold', false);
    ChkTextUnderline.Checked:= ReadBool('DEFAULT', 'FontUnderline', false);
    ChkTextItalic.Checked:= ReadBool('DEFAULT', 'FontItalic', false);
    ChkTextStrikeOut.Checked:= ReadBool('DEFAULT', 'FontStrikeOut', false);
    ELabelHeight.Value:= ReadInteger('DEFAULT', 'LabelHeight', 100);
    ELabelWidth.Value:= ReadInteger('DEFAULT', 'LabelWidth', 100);
    GpCopyMode.ItemIndex:= ReadInteger('DEFAULT', 'CopyMode', 0);
  finally
    Free;
  end;
end;

procedure TFFlyLabelEdit.BtnSaveDefaultClick(Sender: TObject);
begin     //将当前参数保存为缺省图片参数
  with TIniFile.Create(ExtractFilePath(Application.ExeName) + 'FlyLabel.INI')do try
    WriteString('DEFAULT', 'Example', EExampleText.Text);
    WriteInteger('DEFAULT', 'BKCOLOR', EExampleText.Color);
    WriteInteger('DEFAULT', 'FONTCOLOR', EExampleText.Font.Color);
    WriteInteger('DEFAULT', 'LEN', ETextLength.Value);
    WriteInteger('DEFAULT', 'TextTop', ETextTop.Value);
    WriteInteger('DEFAULT', 'TextLeft', ETextLeft.Value);
    WriteInteger('DEFAULT', 'WordWidth', ETextWordWidth.Value);
    WriteInteger('DEFAULT', 'FontSize', ETextFontSize.Value);
    WriteInteger('DEFAULT', 'Align', ETextAlign.ItemIndex);
    WriteString('DEFAULT', 'FontName', ETextFontName.Text);
    WriteInteger('DEFAULT', 'Rotate', ETextRotate.ItemIndex);
    WriteBool('DEFAULT', 'FontBold', ChkTextBold.Checked);
    WriteBool('DEFAULT', 'FontUnderline', ChkTextUnderline.Checked);
    WriteBool('DEFAULT', 'FontItalic', ChkTextItalic.Checked);
    WriteBool('DEFAULT', 'FontStrikeOut', ChkTextStrikeOut.Checked);
    WriteInteger('DEFAULT', 'LabelHeight', ELabelHeight.Value);
    WriteInteger('DEFAULT', 'LabelWidth', ELabelWidth.Value);
    WriteInteger('DEFAULT', 'CopyMode', GpCopyMode.ItemIndex);
  finally
    Free;
  end;
end;   

procedure TFFlyLabelEdit.btnSynLabelClick(Sender: TObject);
begin     //标签同步图片
  ELabelWidth.Value:= EImgWidth.Value;
  ELabelHeight.Value:= EImgHeight.Value;
  ELabelWidthChange(nil);
end;

procedure TFFlyLabelEdit.btnSynPictureClick(Sender: TObject);
begin     //图片同步标签
  EImgWidth.Value:= ELabelWidth.Value;
  EImgHeight.Value:= ELabelHeight.Value;
  EImgWidthChange(nil);
end;

procedure TFFlyLabelEdit.btnCheckClick(Sender: TObject);
var
  Y, A, N: Integer;
begin
  if CheckCode(PChar(ECheckCode.Text), Y, A, N) then
    ShowMessage(Format('环号 CHN%d.%d %d 校验成功！', [Y, A, N]))
  else ShowMessage('校验失败');
end;

procedure TFFlyLabelEdit.btnClearClick(Sender: TObject);
begin
  LabelImage.OrgBmp.FreeImage;
  LabelImage.OrgBmp.Canvas.FillRect(Rect(0,0,LabelImage.OrgBmp.Width,LabelImage.OrgBmp.Height));
end;

(* 标签参数执行顺序控制 *)

procedure TFFlyLabelEdit.btnParamMoveToBottomClick(Sender: TObject);
begin
  if not Assigned(lstTask.Selected) then Exit;
  if not Assigned(lstTask.Selected.getNextSibling()) then Exit;
  FCurrentLabel.Content.Move(lstTask.Selected.Index, FCurrentLabel.Content.SectionCount - 1);
  lstTask.Selected.MoveTo(lstTask.Items[lstTask.Items.Count - 1], naAdd);
end;

procedure TFFlyLabelEdit.btnParamMoveToDownClick(Sender: TObject);
begin
  if not Assigned(lstTask.Selected) then Exit;
  if not Assigned(lstTask.Selected.getNextSibling()) then Exit;
  FCurrentLabel.Content.Move(lstTask.Selected.Index, lstTask.Selected.Index + 1);
  if Assigned(lstTask.Selected.getNextSibling().getNextSibling()) then
    lstTask.Selected.MoveTo(lstTask.Selected.getNextSibling().getNextSibling(), naInsert)
  else
    lstTask.Selected.MoveTo(lstTask.Selected.getNextSibling, naAdd);
end;

procedure TFFlyLabelEdit.btnParamMoveToTopClick(Sender: TObject);
begin
  if not Assigned(lstTask.Selected) then Exit;
  if not Assigned(lstTask.Selected.getPrevSibling()) then Exit;
  FCurrentLabel.Content.Move(lstTask.Selected.Index, 0);
  lstTask.Selected.MoveTo(lstTask.Items[0], naInsert);
end;

procedure TFFlyLabelEdit.btnParamMoveToUpClick(Sender: TObject);
begin
  if not Assigned(lstTask.Selected) then Exit;
  if not Assigned(lstTask.Selected.getPrevSibling()) then Exit;
  FCurrentLabel.Content.Move(lstTask.Selected.Index, lstTask.Selected.Index - 1);
  lstTask.Selected.MoveTo(lstTask.Selected.getPrevSibling(), naInsert);
end;

(* Pointer *)

procedure TFFlyLabelEdit.btnPtAddClick(Sender: TObject);
begin
  mPointers.Lines.Add(IntToStr(EPointerX.Value) + ',' +
    IntToStr(EPointerY.Value) + ',' + IntToStr(plPointerColor.Font.Color) + ',' +
    IntToStr(EPointerKind.ItemIndex) + ',' + IntToStr(EPointerSize.Value));
end;

procedure TFFlyLabelEdit.mPointersDblClick(Sender: TObject);
var
  szList: TStringList;
  Idx: Integer;
begin
  szList:= TStringList.Create;
  try
    szList.Delimiter:= ',';
    Idx:= SendMessage(mPointers.Handle, EM_LineFromChar, mPointers.SelStart, 0); //获取当前行号
    szList.DelimitedText:= mPointers.Lines[Idx];
    if szList.Count < 1 then Exit;
    EPointerX.Value:= StrToInt(szList[0]);
    EPointerY.Value:= StrToInt(szList[1]);
    plPointerColor.Font.Color:= StrToInt(szList[2]);
    EPointerKind.ItemIndex:= StrToInt(szList[3]);
    EPointerSize.Value:= StrToInt(szList[4]);
  finally
    szList.Free;
  end;
end;

(* Line *)

procedure TFFlyLabelEdit.btnLineAddClick(Sender: TObject);
begin
  mLines.Lines.Add(IntToStr(ELineX0.Value) + ',' +
    IntToStr(ELineY0.Value) + ',' + IntToStr(ELineX1.Value) + ',' +
    IntToStr(ELineY1.Value) + ',' + IntToStr(plPointerColor.Font.Color) + ',' +
    IntToStr(ELineSize.ItemIndex));
end;

procedure TFFlyLabelEdit.mLinesDblClick(Sender: TObject);
var
  szList: TStringList;
  Idx: Integer;
begin
  szList:= TStringList.Create;
  try
    szList.Delimiter:= ',';
    Idx:= SendMessage(mLines.Handle, EM_LineFromChar, mLines.SelStart, 0); //获取当前行号
    szList.DelimitedText:= mLines.Lines[Idx];
    if szList.Count < 1 then Exit;
    ELineX0.Value:= StrToInt(szList[0]);
    ELineY0.Value:= StrToInt(szList[1]);
    ELineX1.Value:= StrToInt(szList[2]);
    ELineY1.Value:= StrToInt(szList[3]);
    plPointerColor.Font.Color:= StrToInt(szList[4]);
    ELineSize.ItemIndex:= StrToInt(szList[5]);
  finally
    szList.Free;
  end;
end;

(* Circle *)

procedure TFFlyLabelEdit.btnCircleClick(Sender: TObject);
begin
  mCircle.Lines.Add(IntToStr(ECircleX0.Value) + ',' +
    IntToStr(ECircleY0.Value) + ',' + IntToStr(ECircleX1.Value) + ',' +
    IntToStr(ECircleY1.Value) + ',' + IntToStr(plCircleColor.Font.Color));
end;

procedure TFFlyLabelEdit.mCircleDblClick(Sender: TObject);
var
  szList: TStringList;
  Idx: Integer;
begin
  szList:= TStringList.Create;
  try
    szList.Delimiter:= ',';
    Idx:= SendMessage(mCircle.Handle, EM_LineFromChar, mCircle.SelStart, 0); //获取当前行号
    szList.DelimitedText:= mCircle.Lines[Idx];
    if szList.Count < 1 then Exit;
    ECircleX0.Value:= StrToInt(szList[0]);
    ECircleY0.Value:= StrToInt(szList[1]);
    ECircleX1.Value:= StrToInt(szList[2]);
    ECircleY1.Value:= StrToInt(szList[3]);
    plCircleColor.Font.Color:= StrToInt(szList[4]);
  finally
    szList.Free;
  end;
end;

(* Rectangle *)

procedure TFFlyLabelEdit.btnRectangleAddClick(Sender: TObject);
begin
  mRectangle.Lines.Add(IntToStr(ERectangleX0.Value) + ',' +
    IntToStr(ERectangleY0.Value) + ',' + IntToStr(ERectangleX1.Value) + ',' +
    IntToStr(ERectangleY1.Value) + ',' + IntToStr(plRectangleColor.Font.Color));
end;

procedure TFFlyLabelEdit.mRectangleDblClick(Sender: TObject);
var
  szList: TStringList;
  Idx: Integer;
begin
  szList:= TStringList.Create;
  try
    szList.Delimiter:= ',';
    Idx:= SendMessage(mRectangle.Handle, EM_LineFromChar, mRectangle.SelStart, 0); //获取当前行号
    szList.DelimitedText:= mRectangle.Lines[Idx];
    if szList.Count < 1 then Exit;
    ERectangleX0.Value:= StrToInt(szList[0]);
    ERectangleY0.Value:= StrToInt(szList[1]);
    ERectangleX1.Value:= StrToInt(szList[2]);
    ERectangleY1.Value:= StrToInt(szList[3]);
    plRectangleColor.Font.Color:= StrToInt(szList[4]);
  finally
    szList.Free;
  end;
end;

(* 标签位置变动参数调整事件 *)

procedure TFFlyLabelEdit.ETextTopChange(Sender: TObject);
begin     //位置及相关参数调整
  if not (Sender as TWinControl).Focused then Exit;
  Redrew;
end;

procedure TFFlyLabelEdit.GpCopyModeClick(Sender: TObject);
begin
  Redrew;
end;

procedure TFFlyLabelEdit.Redrew;
begin     //刷新图片
  btnContentSaveClick(nil);
  LabelImage.Content.Text:= FCurrentLabel.Content.Text;
  LabelImage.Pixel:= EPixel.ItemIndex;
//  LabelImage.OrgBmp:= FCurrentLabel.Picture;
  LabelImage.DrewBaseBmp;
  LabelImage.Drew(ImageEdit.Picture.Bitmap, EExampleText.Text);
  mLog.Lines.Add(LabelImage.Info);
//  LabelImage.Drew(ImagePreview.Picture.Bitmap, EExampleText.Text);
end;

procedure TFFlyLabelEdit.EImgWidthChange(Sender: TObject);
begin     //更改图片尺寸
  if(Sender <> nil)and(not (Sender as TWinControl).Focused)then Exit;
  ImageEdit.Picture.Bitmap.Width:= EImgWidth.Value;
  ImageEdit.Picture.Bitmap.Height:= EImgHeight.Value;
  LabelImage.OrgBmp.Width:= EImgWidth.Value;
  LabelImage.OrgBmp.Height:= EImgHeight.Value;
  Redrew;
end;

procedure TFFlyLabelEdit.ELabelWidthChange(Sender: TObject);
begin     //更改标签尺寸
  if(Sender <> nil)and(not (Sender as TWinControl).Focused)then Exit;
  ImagePreview.Picture.Bitmap.Width:= ELabelWidth.Value;
  ImagePreview.Picture.Bitmap.Height:= ELabelHeight.Value;
  Redrew;
end;

procedure TFFlyLabelEdit.BtnBackColorClick(Sender: TObject);
begin     //选择背景色
  with ColorDialog1 do begin
    Color:= LabelImage.OrgBmp.Canvas.Brush.Color;
    if Execute then begin
      LabelImage.OrgBmp.Canvas.Brush.Color:= ColorDialog1.Color;
      EExampleText.Color:= ColorDialog1.Color;
    end;
  end;
end;

procedure TFFlyLabelEdit.BtnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TFFlyLabelEdit.LoadData(DataSet: TCustomADODataSet);
var
  I: Integer;
begin     //数据库读取函数
  FCurrentLabel.LoadFromDB(DataSet);
  with FCurrentLabel do begin
    ELabelName.Text:= LabelName;
    //ECategoryIDX.Value:= FieldByName('CategoryIDX').AsInteger;
    LabelImage.Pixel:= Pixel;
    LabelImage.OrgBmp:= Picture;
    ImageEdit.Picture.Bitmap.Width:= LabelImage.OrgBmp.Width;
    ImageEdit.Picture.Bitmap.Height:= LabelImage.OrgBmp.Height;
    EImgWidth.Value:= ImageWidth;
    EImgHeight.Value:= ImageLength;
    ELabelWidth.Value:= LabelWidth;
    ELabelHeight.Value:= LabelHeight;
    ImagePreview.Picture.Bitmap.Width:= ELabelWidth.Value;
    ImagePreview.Picture.Bitmap.Height:= ELabelHeight.Value;
    EPixel.ItemIndex:= FCurrentLabel.Pixel;
    LabelImage.Content.Text:= Content.Text;
    for I := 0 to FCurrentLabel.Content.SectionCount - 1 do
      with lstTask.Items.Add(nil, FCurrentLabel.Content.Sections[I]) do
        ImageIndex:= FCurrentLabel.Content.ReadInteger(FCurrentLabel.Content.Sections[I], 'Kind', 0) + 11;
    if lstTask.Items.Count > 0 then lstTaskDblClick(nil);
  end;
  //Redrew;
end;

procedure TFFlyLabelEdit.SaveData;
begin     //数据库存储函数
  FCurrentLabel.LabelName:= ELabelName.Text;
  FCurrentLabel.Picture.Assign(LabelImage.OrgBmp);
  FCurrentLabel.ImageWidth:= EImgWidth.Value;
  FCurrentLabel.ImageLength:= EImgHeight.Value;
  FCurrentLabel.LabelWidth:= ELabelWidth.Value;
  FCurrentLabel.LabelHeight:= ELabelHeight.Value;
  FCurrentLabel.Pixel:= EPixel.ItemIndex;
  FFlyLabelDB.DQLabel.DataSource:= nil;
  FCurrentLabel.SaveToDB(FFlyLabelDB.DQLabel);
  FFlyLabelDB.DQLabel.DataSource:= FFlyLabelDB.DSLables;
end;

procedure TFFlyLabelEdit.BtnSaveClick(Sender: TObject);
begin     //保存
  btnContentSaveClick(nil);
  SaveData;
  Close;
end;

procedure TFFlyLabelEdit.BtnSaveAsClick(Sender: TObject);
begin     //另存为
  btnContentSaveClick(nil);
  FCurrentLabel.ID:= 0;
  SaveData;
  Close;
end;

procedure TFFlyLabelEdit.btnRefrenshClick(Sender: TObject);
begin     //刷新
  Redrew;
end;

procedure TFFlyLabelEdit.btnContentDelClick(Sender: TObject);
begin
  if not Assigned(lstTask.Selected) then Exit;
  if EContentName.Text <> lstTask.Selected.Text then
    raise Exception.Create('提取后方可删除。');
  if MessageDlg('是否确定要删除这个项目吗?', mtWarning, mbOKCancel, 0) = mrOK then begin
    FCurrentLabel.Content.EraseSection(EContentName.Text);
    lstTask.Items.Delete(lstTask.Selected);
    EContentName.Text:= '';
  end;
end;

(* 标签单项任务信息处理事件 *)

procedure TFFlyLabelEdit.btnContentSaveClick(Sender: TObject);
const
  QRCodeVertable : array[0..39]of word = (21, 25, 29, 33, 37, 41, 45, 49, 53,
    57, 61, 65, 69, 73, 77, 81, 85, 89, 93, 97, 101, 105, 109, 113, 117, 121,
    125, 129, 133, 137, 141, 145, 149, 153, 157, 161, 165, 169, 173, 177);
var
  I: Integer;
begin     //保存任务项目
  if EContentName.Text = '' then Exit;
  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Kind', pcTask.ActivePageIndex);
  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'CopyMode', GpCopyMode.ItemIndex);
  FCurrentLabel.Content.WriteBool(EContentName.Text, 'SerialNo', chkSerialNo.Checked);
  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'VRate', EVRate.Value);
  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'HRate', EHRate.Value);
  case pcTask.ActivePageIndex of
  0: begin
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'LEN', ETextLength.Value);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'BKCOLOR', plTextColor.Color);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'FONTCOLOR', plTextColor.Font.Color);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Top', ETextTop.Value);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Left', ETextLeft.Value);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'WordWidth', ETextWordWidth.Value);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Align', ETextAlign.ItemIndex);
      FCurrentLabel.Content.WriteString(EContentName.Text, 'FontName', ETextFontName.Text);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'FontSize', ETextFontSize.Value);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Rotate', ETextRotate.ItemIndex);
      FCurrentLabel.Content.WriteBool(EContentName.Text, 'Bold', ChkTextBold.Checked);
      FCurrentLabel.Content.WriteBool(EContentName.Text, 'Italic', ChkTextItalic.Checked);
      FCurrentLabel.Content.WriteBool(EContentName.Text, 'Underline', ChkTextUnderline.Checked);
      FCurrentLabel.Content.WriteBool(EContentName.Text, 'StrikeOut', ChkTextStrikeOut.Checked);
      FCurrentLabel.Content.WriteString(EContentName.Text, 'ExampleText', ETextDefault.Text);
      lstItems.Lines.Delimiter:= ',';
      FCurrentLabel.Content.WriteString(EContentName.Text, 'Items', lstItems.Lines.DelimitedText);
    end;
  1: begin
      FCurrentLabel.Content.WriteString(EContentName.Text, 'BMPFile', EFileName.Text);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Top', EBmpTop.Value);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Left', EBmpLeft.Value);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Width', EBmpWidth.Value);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Height', EBmpHeight.Value);
    end;
  2: begin
      if QRCodeVertable[ECodeVersion.ItemIndex] * ECodeSize.Value / StrToFloat(ECodeDiameter.Text) / StrToFloat(ECodePrnDpi.Text) > 1 then
        raise Exception.Create('放大系数过大，请相应调整Dpi信息以满足二维码在可视范围以内');
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'BKCOLOR', plCodeColor.Color);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'FONTCOLOR', plCodeColor.Font.Color);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Top', ECodeTop.Value);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Left', ECodeLeft.Value);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'CODETYPE', ECodeType.ItemIndex);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'CODESIZE', ECodeSize.Value);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'CODELEVEL', ECodeLevel.ItemIndex);
      FCurrentLabel.Content.WriteString(EContentName.Text, 'Diameter', ECodeDiameter.Text);
      FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Rotate', ECodeRotate.ItemIndex);
      FCurrentLabel.Content.WriteString(EContentName.Text, 'Version', ECodeVersion.Text);
      FCurrentLabel.Content.WriteString(EContentName.Text, 'PrnDpi', ECodePrnDpi.Text);
      FCurrentLabel.Content.WriteString(EContentName.Text, 'CodeValue', ECodeValue.Text);
    end;
  3: begin
      mPointers.Lines.Delimiter:= ',';
      FCurrentLabel.Content.WriteString(EContentName.Text, 'Pointers', mPointers.Lines.DelimitedText);
    end;
  4: begin
      mPointers.Lines.Delimiter:= ',';
      FCurrentLabel.Content.WriteString(EContentName.Text, 'Lines', mLines.Lines.DelimitedText);
    end;
  5: begin
      mPointers.Lines.Delimiter:= ',';
      FCurrentLabel.Content.WriteString(EContentName.Text, 'Circles', mCircle.Lines.DelimitedText);
    end;
  6: begin
      mPointers.Lines.Delimiter:= ',';
      FCurrentLabel.Content.WriteString(EContentName.Text, 'Rectangle', mRectangle.Lines.DelimitedText);
    end;
  end;
  for I := 0 to lstTask.Items.Count - 1 do
    if SameText(lstTask.Items[I].Text, EContentName.Text) then begin
      lstTask.Items[I].ImageIndex:= pcTask.ActivePageIndex + 11;
      Exit;
    end;
  with lstTask.Items.Add(nil, EContentName.Text) do
    ImageIndex:= pcTask.ActivePageIndex + 11;
end;

procedure TFFlyLabelEdit.lstTaskDblClick(Sender: TObject);
begin
  if not Assigned(lstTask.Selected) then Exit;
  pcTask.ActivePageIndex:= lstTask.Selected.ImageIndex - 11;
  EContentName.Text:= lstTask.Selected.Text;
  EVRate.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'VRate', 100);
  EHRate.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'HRate', 100);
  case pcTask.ActivePageIndex of
  0: begin
      ETextLength.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'LEN', 0);
      plTextColor.Color:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'BKCOLOR', 0);
      plTextColor.Font.Color:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'FONTCOLOR', 0);
      ETextTop.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'Top', 0);
      ETextLeft.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'Left', 0);
      ETextWordWidth.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'WordWidth', 0);
      ETextAlign.ItemIndex:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'Align', 0);
      ETextFontName.ItemIndex:= ETextFontName.Items.IndexOf(FCurrentLabel.Content.ReadString(EContentName.Text, 'FontName', '宋体'));
      ETextFontSize.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'FontSize', 0);
      ETextRotate.ItemIndex:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'Rotate', 0);
      ChkTextBold.Checked:= FCurrentLabel.Content.ReadBool(EContentName.Text, 'Bold', False);
      ChkTextItalic.Checked:= FCurrentLabel.Content.ReadBool(EContentName.Text, 'Italic', False);
      ChkTextUnderline.Checked:= FCurrentLabel.Content.ReadBool(EContentName.Text, 'Underline', False);
      ChkTextStrikeOut.Checked:= FCurrentLabel.Content.ReadBool(EContentName.Text, 'StrikeOut', False);
      ETextDefault.Text:= FCurrentLabel.Content.ReadString(EContentName.Text, 'ExampleText', '');
      lstItems.Lines.Delimiter:= ',';
      lstItems.Lines.DelimitedText:= FCurrentLabel.Content.ReadString(EContentName.Text, 'Items', '');
    end;
  1: begin
      EFileName.Text:= FCurrentLabel.Content.ReadString(EContentName.Text, 'BMPFile', '');
      EBmpTop.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'Top', 0);
      EBmpLeft.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'Left', 0);
      EBmpWidth.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'Width', 0);
      EBmpHeight.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'Height', 0);
    end;
  2: begin
      plCodeColor.Color:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'BKCOLOR', 0);
      plCodeColor.Font.Color:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'FONTCOLOR', 0);
      ECodeTop.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'Top', 0);
      ECodeLeft.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'Left', 0);
      ECodeType.ItemIndex:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'CODETYPE', 0);
      ECodeSize.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'CODESIZE', 7);
      ECodeLevel.ItemIndex:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'CODELEVEL', 0);
      ECodeRotate.ItemIndex:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'Rotate', 0);
      ECodeDiameter.Text:= FCurrentLabel.Content.ReadString(EContentName.Text, 'Diameter', '8.5');
      ECodeVersion.ItemIndex:= ECodeVersion.Items.IndexOf(
        FCurrentLabel.Content.ReadString(EContentName.Text, 'Version', '1'));
      ECodePrnDpi.Text:= FCurrentLabel.Content.ReadString(EContentName.Text, 'PrnDpi', '28.3465');
      ECodeValue.Text:= FCurrentLabel.Content.ReadString(EContentName.Text, 'CodeValue', '编码+编号');
    end;
  3: begin
      mPointers.Lines.Delimiter:= ',';
      mPointers.Lines.DelimitedText:= FCurrentLabel.Content.ReadString(EContentName.Text, 'Pointers', '');
    end;
  4: begin
      mPointers.Lines.Delimiter:= ',';
      mLines.Lines.DelimitedText:= FCurrentLabel.Content.ReadString(EContentName.Text, 'Lines', '');
    end;
  5: begin
      mPointers.Lines.Delimiter:= ',';
      mCircle.Lines.DelimitedText:= FCurrentLabel.Content.ReadString(EContentName.Text, 'Circles', '');
    end;
  6: begin
      mPointers.Lines.Delimiter:= ',';
      mRectangle.Lines.DelimitedText:= FCurrentLabel.Content.ReadString(EContentName.Text, 'Rectangle', '');
    end;
  end;
  chkSerialNo.Checked:= FCurrentLabel.Content.ReadBool(EContentName.Text, 'SerialNo', False);
  GpCopyMode.ItemIndex:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'CopyMode', 0);
end;

procedure TFFlyLabelEdit.lstTaskGetSelectedIndex(Sender: TObject;
  Node: TTreeNode);
begin
  Node.SelectedIndex:= Node.ImageIndex;
end;

procedure TFFlyLabelEdit.plColorMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  with ColorDialog1 do begin
    case Button of
    mbLeft:   Color:= TPanel(Sender).Font.Color;
    mbRight:  Color:= TPanel(Sender).Color;
    mbMiddle: Color:= TPanel(Sender).Font.Color;
    end;
    Color:= LabelImage.OrgBmp.Canvas.Pen.Color;
    if not Execute then Exit;
    case Button of
    mbLeft:   TPanel(Sender).Font.Color:= Color;
    mbRight:  TPanel(Sender).Color:= Color;
    mbMiddle: TPanel(Sender).Font.Color:= Color;
    end;
    Redrew;
  end;
end;

procedure TFFlyLabelEdit.ETextAlignChange(Sender: TObject);
begin
  Redrew;
end;

procedure TFFlyLabelEdit.ETextFontNameDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  with TComboBox(Control) do
  begin
    Canvas.FillRect(Rect);
    Canvas.Brush.Style:= bsClear;
    Canvas.Font.Name := 'Arial';
    Canvas.Font.Size := 9;
    Canvas.TextOut(Rect.Left + 2, Rect.Top + 1, Copy(Items[Index], 1, 8));
    Canvas.TextOut(Rect.Left + 2, Rect.Top + 16, Copy(Items[Index], 9, 8));
    Canvas.Font.Name := Items[Index];
    Canvas.Font.Size := 24;
    if chkSerialNo.Checked then
      Canvas.TextOut(Rect.Left + 60, Rect.Top + 2, EExampleText.Text)
    else Canvas.TextOut(Rect.Left + 60, Rect.Top + 2, ETextDefault.Text);
    Canvas.MoveTo(58, 0);
    Canvas.LineTo(58, Rect.Bottom - Rect.Top);
  end;
end;

end.
