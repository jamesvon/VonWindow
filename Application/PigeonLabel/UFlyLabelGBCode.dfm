object FFlyLabelGBCode: TFFlyLabelGBCode
  Left = 0
  Top = 0
  Caption = 'FFlyLabelGBCode'
  ClientHeight = 502
  ClientWidth = 759
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCanResize = FormCanResize
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object mLog: TMemo
    Left = 436
    Top = 0
    Width = 323
    Height = 502
    Align = alRight
    Lines.Strings = (
      #25171#21360#35760#24405)
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 347
    Top = 0
    Width = 89
    Height = 502
    Align = alRight
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 1
    object Label1: TLabel
      Left = 6
      Top = 400
      Width = 84
      Height = 13
      Caption = #22686#34917#21518#20445#30041#20301#25968
    end
    object lbCount: TLabel
      Left = 6
      Top = 447
      Width = 6
      Height = 13
      Caption = '0'
    end
    object EYear: TLabeledEdit
      Left = 6
      Top = 51
      Width = 41
      Height = 21
      EditLabel.Width = 24
      EditLabel.Height = 13
      EditLabel.Caption = #24180#20221
      TabOrder = 1
      Text = '2015'
      OnKeyPress = EYearKeyPress
    end
    object EArea: TLabeledEdit
      Left = 50
      Top = 51
      Width = 33
      Height = 21
      EditLabel.Width = 24
      EditLabel.Height = 13
      EditLabel.Caption = #21306#21495
      TabOrder = 2
      Text = '00'
      OnKeyPress = EAreaKeyPress
    end
    object EStartNo: TLabeledEdit
      Left = 6
      Top = 91
      Width = 65
      Height = 21
      EditLabel.Width = 36
      EditLabel.Height = 13
      EditLabel.Caption = #36215#22987#21495
      TabOrder = 3
      Text = '1'
      OnKeyPress = EStartNoKeyPress
    end
    object ENum: TLabeledEdit
      Left = 6
      Top = 134
      Width = 65
      Height = 21
      EditLabel.Width = 24
      EditLabel.Height = 13
      EditLabel.Caption = #25968#37327
      TabOrder = 4
      Text = '60'
      OnKeyPress = ENumKeyPress
    end
    object btnPrint: TBitBtn
      Left = 6
      Top = 192
      Width = 75
      Height = 25
      Caption = #25171#21360
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 6
      OnClick = btnPrintClick
    end
    object btnPrinter: TBitBtn
      Left = 6
      Top = 8
      Width = 75
      Height = 25
      Caption = #25171#21360#26426
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 0
      OnClick = btnPrinterClick
    end
    object ELostNo: TLabeledEdit
      Left = 6
      Top = 311
      Width = 65
      Height = 21
      EditLabel.Width = 24
      EditLabel.Height = 13
      EditLabel.Caption = #34917#21495
      TabOrder = 8
      Text = '1'
      OnKeyPress = ELostNoKeyPress
    end
    object btnMake: TBitBtn
      Left = 6
      Top = 161
      Width = 75
      Height = 25
      Caption = #29983#25104
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 5
      OnClick = btnMakeClick
    end
    object BtnDeleteRecord: TBitBtn
      Left = 6
      Top = 369
      Width = 75
      Height = 25
      Caption = #21024#38500#35760#24405
      DoubleBuffered = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333303
        333333333333337FF3333333333333903333333333333377FF33333333333399
        03333FFFFFFFFF777FF3000000999999903377777777777777FF0FFFF0999999
        99037F3337777777777F0FFFF099999999907F3FF777777777770F00F0999999
        99037F773777777777730FFFF099999990337F3FF777777777330F00FFFFF099
        03337F773333377773330FFFFFFFF09033337F3FF3FFF77733330F00F0000003
        33337F773777777333330FFFF0FF033333337F3FF7F3733333330F08F0F03333
        33337F7737F7333333330FFFF003333333337FFFF77333333333000000333333
        3333777777333333333333333333333333333333333333333333}
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 10
      OnClick = BtnDeleteRecordClick
    end
    object BtnClearRecords: TBitBtn
      Left = 6
      Top = 223
      Width = 75
      Height = 25
      Caption = #28165#38500#35760#24405
      DoubleBuffered = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
        333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
        0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
        07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
        07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
        0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
        33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
        B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
        3BB33773333773333773B333333B3333333B7333333733333337}
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 7
      OnClick = BtnClearRecordsClick
    end
    object BtnAddRecord: TBitBtn
      Left = 6
      Top = 338
      Width = 75
      Height = 25
      Caption = #22686#34917
      DoubleBuffered = False
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333333FFFFFFFFF333333000000000033333377777777773333330FFFFF
        FFF03333337F333333373333330FFFFFFFF03333337F3FF3FFF73333330F00F0
        00F03333F37F773777373330330FFFFFFFF03337FF7F3F3FF3F73339030F0800
        F0F033377F7F737737373339900FFFFFFFF03FF7777F3FF3FFF70999990F00F0
        00007777777F7737777709999990FFF0FF0377777777FF37F3730999999908F0
        F033777777777337F73309999990FFF0033377777777FFF77333099999000000
        3333777777777777333333399033333333333337773333333333333903333333
        3333333773333333333333303333333333333337333333333333}
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 9
      OnClick = BtnAddRecordClick
    end
    object EHeaderBit: TSpinEdit
      Left = 6
      Top = 419
      Width = 67
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 11
      Value = 4
    end
  end
  object lstCode: TListBox
    Left = 0
    Top = 0
    Width = 347
    Height = 502
    Align = alClient
    Columns = 1
    ItemHeight = 13
    TabOrder = 2
  end
  object PrintDialog1: TPrintDialog
    Left = 576
    Top = 32
  end
  object PrinterSetupDialog1: TPrinterSetupDialog
    Left = 632
    Top = 40
  end
end
