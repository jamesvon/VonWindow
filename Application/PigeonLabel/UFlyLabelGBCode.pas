(*******************************************************************************
DPI		DPI/mm	  Width	Pointer	  Width	Pointer	  height	Pointer	  签距	Pointer
800	  31.49606	220	  6929.134	8	    251.9685	31.5	  992.126	  2.5	  78.74016	330.7087
800	  31.49606	220	  6929.134	8	    251.9685	31.5	  992.126	  4	    125.9843
--------------------------------------------------------------------------------
  签含边距宽    8 + 2.5 ->  251.9685 + 78.74016 = 330.70866
  左边距        4	    125.9843
--------------------------------------------------------------------------------
[CHN]
Kind=0
CopyMode=9
SerialNo=0
VRate=100
HRate=100
LEN=6
BKCOLOR=-16777201
FONTCOLOR=-16777208
Top=3
Left=26
WordWidth=71
Align=0
FontName=方正小标宋简体
FontSize=63
Rotate=0
Bold=0
Italic=0
Underline=0
StrikeOut=0
ExampleText=CHN
Items=

[年份]
Kind=0
CopyMode=9
SerialNo=0
VRate=100
HRate=100
LEN=6
BKCOLOR=-16777201
FONTCOLOR=-16777208
Top=83
Left=30
WordWidth=26
Align=0
FontName=文鼎CS中等线
FontSize=43
Rotate=0
Bold=0
Italic=0
Underline=0
StrikeOut=0
ExampleText=2015
Items=

[区号]
Kind=0
CopyMode=9
SerialNo=0
VRate=100
HRate=100
LEN=6
BKCOLOR=-16777201
FONTCOLOR=-16777208
Top=83
Left=172
WordWidth=26
Align=0
FontName=文鼎CS中等线
FontSize=43
Rotate=0
Bold=0
Italic=0
Underline=0
StrikeOut=0
ExampleText=00
Items=

[编码]
Kind=0
CopyMode=9
SerialNo=1
VRate=100
HRate=100
LEN=6
BKCOLOR=-16777201
FONTCOLOR=-16777208
Top=365
Left=15
WordWidth=86
Align=0
FontName=汉鼎繁特粗黑
FontSize=134
Rotate=1
Bold=0
Italic=0
Underline=0
StrikeOut=0
ExampleText=1234567
Items=

[二维码]
Kind=2
CopyMode=9
SerialNo=1
VRate=100
HRate=100
BKCOLOR=-16777201
FONTCOLOR=-16777208
Top=146
Left=31
CODETYPE=0
CODESIZE=9
CODELEVEL=0
Diameter=8.5
Rotate=1
Version=1
PrnDpi=31.49606299
CodeValue=编码


*******************************************************************************)
unit UFlyLabelGBCode;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, UVonQRCode, UVonGraphicAPI, Printers,
  Spin, IniFiles;

type
  TFFlyLabelGBCode = class(TForm)
    PrintDialog1: TPrintDialog;
    PrinterSetupDialog1: TPrinterSetupDialog;
    mLog: TMemo;
    Panel1: TPanel;
    EYear: TLabeledEdit;
    EArea: TLabeledEdit;
    EStartNo: TLabeledEdit;
    ENum: TLabeledEdit;
    btnPrint: TBitBtn;
    btnPrinter: TBitBtn;
    ELostNo: TLabeledEdit;
    btnMake: TBitBtn;
    BtnDeleteRecord: TBitBtn;
    BtnClearRecords: TBitBtn;
    BtnAddRecord: TBitBtn;
    lstCode: TListBox;
    Label1: TLabel;
    EHeaderBit: TSpinEdit;
    lbCount: TLabel;
    procedure btnPrintClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnPrinterClick(Sender: TObject);
    procedure btnMakeClick(Sender: TObject);
    procedure BtnClearRecordsClick(Sender: TObject);
    procedure ENumKeyPress(Sender: TObject; var Key: Char);
    procedure EYearKeyPress(Sender: TObject; var Key: Char);
    procedure EAreaKeyPress(Sender: TObject; var Key: Char);
    procedure EStartNoKeyPress(Sender: TObject; var Key: Char);
    procedure ELostNoKeyPress(Sender: TObject; var Key: Char);
    procedure BtnAddRecordClick(Sender: TObject);
    procedure BtnDeleteRecordClick(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
  private
    { Private declarations }
    numBmp: array[0..9]of TBitmap;
    FQR: TVonQRCode;
    procedure DrewCHN(X, Y: Integer);
    procedure DrewYear(X, Y: Integer);
    procedure DrewArea(X, Y: Integer);
    procedure DrewCode(X, Y: Integer; Code: string);
    procedure DrewNo(X, Y: Integer; Code: string);
    function QRCode(Code: PChar; Ver, CODESIZE: Integer; PrnDpi, Diameter: Double): TBitmap;
    function GetCopyMode(ModeID: Integer): TCopyMode;
    procedure Rotate(RotateType: Integer; srcBmp, destBmp: TBitmap);
  public
    { Public declarations }
  end;

var
  FFlyLabelGBCode: TFFlyLabelGBCode;

implementation

uses UVonSystemFuns, DCPsha1;

{$R *.dfm}

{$region 'Private functions'}

procedure ChangeBytes(var data: array of byte);
var
  tmp: byte;       //0 1 2 3 4 5  6 7 8  9  0  1 2 3 4  5  6
begin     //存储顺序 3,8,0,4,1,10,2,5,12,15,11,7,6,9,16,13,14,
  tmp:= data[0];
  data[0]:= data[3];
  data[3]:= data[4];
  data[4]:= data[1];
  data[1]:= data[8];
  data[8]:= data[12];
  data[12]:= data[6];
  data[6]:= data[2];
  data[2]:= tmp;
  tmp:= data[5];
  data[5]:= data[10];
  data[10]:= data[11];
  data[11]:= data[7];
  data[7]:= tmp;
  tmp:= data[9];
  data[9]:= data[15];
  data[15]:= data[13];
  data[13]:= tmp;
end;

function TFFlyLabelGBCode.QRCode(Code: PChar; Ver, CODESIZE: Integer; PrnDpi, Diameter: Double): TBitmap;
var
  szI: Integer;
  szCode: array[0..16] of byte;
  digest: array[0..19] of byte;
  S: string;
  Hash: TDCP_sha1;
begin
  S:= string(Code);
  if Length(S) < 12 then S:= Copy(S + '000000000000', 1, 12);                                           //123456000000
  if not TryStrToInt(Copy(S, 1, 4), szI) then Exit;       //Year -> 7F                                  //1234
  szCode[0]:= (szI + 1) and $7F;                          //1 1111111        FF                         //4D2->1001 1010010 0x52    1101101
  if not TryStrToInt(Copy(S, 5, 2), szI) then Exit;       //Area -> 7F                                  //56
  szCode[1]:= szI and $7F;                                //1 111111                                    //38 ->0000 0111000 0x38
  if not TryStrToInt(Copy(S, 7, MaxInt), szI) then Exit;  //Code -> 7FFFFF 1111111 11111111 11111111    //                  0x00
  szCode[2]:= (szI shr 16) and $7F;                       //        FFFF           1111111 1 11111111   //                  0x00
  szCode[3]:= (szI shr 9) and $7F;                        //        7F                     1111111 11   //                  0x00
  szCode[4]:= (szI shr 2) and $7F;                        //        7F                             11   //                  0x00
  szCode[5]:= (szI and $3) shl 5;                                                                       //                  0x00
  szCode[6]:=  Ord('@');                                                                                //                  0x00
  szCode[7]:=  Ord('j');                                                                                //                  0x00
  szCode[8]:=  Ord('A');                                                                                //                  0x00
  szCode[9]:=  Ord('m');                                                                                //                  0x00
  szCode[10]:= Ord('E');                                                                                //                  0x00
  szCode[11]:= Ord('5');                                                                                //                  0x00
  szCode[12]:= Ord('_');                                                                                //                  0x00
  szCode[13]:= Ord('v');                                                                                //                  0x00
  szCode[14]:= Ord('0');                                                                                //                  0x00
  szCode[15]:= Ord('N');                                                                                //                  0x00
  szCode[16]:= Ord('%');
  Hash := TDCP_sha1.Create(nil);  // create the hash
  Hash.Init;                      // initialize it
  Hash.Update(szCode[0], 17);     // hash the stream contents
  Hash.Final(Digest);             // produce the digest
  Hash.Free;
  for szI := 0 to 9 do
    Digest[szI]:= Digest[szI] xor Digest[19 - szI];
  szCode[5]:= szCode[5] + Digest[0] shr 3;                    //11 11111      0
  szCode[6]:= (Digest[0] and $7) shl 4 + Digest[1] shr 4;     //111 1111      0 1
  szCode[7]:= (Digest[1] and $F) shl 3 + Digest[2] shr 5;     //1111 111      1 2
  szCode[8]:= (Digest[2] and $1F) shl 2 + Digest[3] shr 6;    //11111 11      2 3
  szCode[9]:= (Digest[3] and $3F) shl 1 + Digest[4] shr 7;    //111111 1      3 4
  szCode[10]:= (Digest[4] and $7F);                           //1111111       4
  szCode[11]:= (Digest[5] and $FF) shr 1;                     //1111111       5
  szCode[12]:= (Digest[5] and $1) shl 6 + Digest[6] shr 2;    //1 111111      5 6
  szCode[13]:= (Digest[6] and $3) shl 5 + Digest[7] shr 3;    //11 11111      6 7
  szCode[14]:= (Digest[7] and $7) shl 4 + Digest[8] shr 4;    //111 1111      7 8
  szCode[15]:= (Digest[8] and $F) shl 3 + Digest[9] shr 5;    //1111 111      8 9
  szCode[16]:= (Digest[9] and $1F) shl 2 + Digest[10] shr 6;  //11111 11      9 A
  ChangeBytes(szCode);
  FQR.SymbolEnabled:= False;
  FQR.ClearOption:= True;
  FQR.SymbolColor:= clBlack;
  FQR.BackColor:= clWhite;
  FQR.Stretch:= qrVertical;
  FQR.Version:= Ver;
  FQR.Pxmag:= CODESIZE;
  FQR.PrnDpi:= PrnDpi;  //720 dpi / 英寸 = 28.3465 pixels/mm;
  FQR.Radius:= Diameter / 2;
  FQR.Mode:= qrSingle;
  FQR.Numbering:= nbrIfVoid;   //nbrNone, nbrHead, nbrTail, nbrIfVoid
  FQR.SymbolEnabled:= True;
  FQR.WriteData(szCode, 11);
  Result:= FQR.Bmp;
end;

function TFFlyLabelGBCode.GetCopyMode(ModeID: Integer): TCopyMode;
begin
  case ModeID of
  0: Result:= cmBlackness;  //Fills the destination rectangle on the canvas with black.
  1: Result:= cmDstInvert;  //Inverts the image on the canvas and ignores the source.
  2: Result:= cmMergeCopy;  //Combines the image on the canvas and the source bitmap by using the Boolean AND operator.
  3: Result:= cmMergePaint; //Combines the inverted source bitmap with the image on the canvas by using the Boolean OR operator.
  4: Result:= cmNotSrcCopy; //Copies the inverted source bitmap to the canvas.
  5: Result:= cmNotSrcErase;//Combines the image on the canvas and the source bitmap by using the Boolean OR operator, and inverts the result.
  6: Result:= cmPatCopy;    //Copies the source pattern to the canvas.
  7: Result:= cmPatInvert;  //Combines the source pattern with the image on the canvas using the Boolean XOR operator
  8: Result:= cmPatPaint;   //Combines the inverted source bitmap with the source pattern by using the Boolean OR operator. Combines the result of this operation with the image on the canvas by using the Boolean OR operator.
  9: Result:= cmSrcAnd;     //Combines the image on the canvas and source bitmap by using the Boolean AND operator.
  10: Result:= cmSrcCopy;   //Copies the source bitmap to the canvas.
  11: Result:= cmSrcErase;  //Inverts the image on the canvas and combines the result with the source bitmap by using the Boolean AND operator.
  12: Result:= cmSrcInvert; //Combines the image on the canvas and the source bitmap by using the Boolean XOR operator.
  13: Result:= cmSrcPaint;  //Combines the image on the canvas and the source bitmap by using the Boolean OR operator.
  14: Result:= cmWhiteness; //Fills the destination rectangle on the canvas with white.
  end;
end;

//旋转90度
function Rotate90(const Bitmap: TBitmap): TBitmap;
var
  i, j: Integer;
  rowIn, rowOut: pRGBTriple;
  Width, Height: Integer;
begin
  Result:= TBitmap.Create;
  Result.Width := Bitmap.Height;
  Result.Height := Bitmap.Width;
  Result.PixelFormat := pf24bit;
  Width:= Bitmap.Width - 1;
  Height:= Bitmap.Height - 1;
  for  j := 0 to Height do
  begin
    rowIn  := Bitmap.ScanLine[j];
    for i := 0 to Width do
    begin
      rowOut := Result.ScanLine[i];
      Inc(rowOut,Height - j);
      rowOut^ := rowIn^;
      Inc(rowIn);
    end;
  end;
end;

procedure TFFlyLabelGBCode.Rotate(RotateType: Integer; srcBmp, destBmp: TBitmap);
var
  X, Y: Integer;
  procedure InitDest(W, H: Integer);
  begin
    destBmp.Width:= W;
    destBmp.Height:= H;
    destBmp.Canvas.FillRect(Rect(0, 0, W, H));
  end;
begin
  case RotateType of
  0: begin                                //不旋转
      InitDest(srcBmp.Width, srcBmp.Height);
      destBmp.Assign(srcBmp);
    end;
  1: begin                                //90度旋转
      InitDest(srcBmp.Height, srcBmp.Width);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[srcBmp.Height - 1 - Y, X]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  2: begin                                //180度旋转
      InitDest(srcBmp.Width, srcBmp.Height);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[srcBmp.Width - 1 - X, srcBmp.Height - Y - 1]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  3: begin                                //270度旋转
      InitDest(srcBmp.Height, srcBmp.Width);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[Y, srcBmp.Width - 1 - X]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  4: begin                                //水平翻转
      InitDest(srcBmp.Width, srcBmp.Height);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[srcBmp.Width - 1 - X, Y]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  5: begin                                //90度翻转
      InitDest(srcBmp.Height, srcBmp.Width);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[Y,X]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  6: begin                                //垂直翻转
      InitDest(srcBmp.Width, srcBmp.Height);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[X, srcBmp.Height - Y - 1]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  7: begin                                //270度翻转
      InitDest(srcBmp.Height, srcBmp.Width);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[srcBmp.Height - Y - 1, srcBmp.Width - 1 - X]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  end;
end;

{$endregion}
{$region 'Component methods'}

procedure TFFlyLabelGBCode.btnPrinterClick(Sender: TObject);
begin
  PrinterSetupDialog1.Execute();
end;

procedure TFFlyLabelGBCode.EAreaKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then EStartNo.SetFocus;
end;

procedure TFFlyLabelGBCode.ELostNoKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then BtnAddRecordClick(nil);
end;

procedure TFFlyLabelGBCode.ENumKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    btnMakeClick(nil);
end;

procedure TFFlyLabelGBCode.EStartNoKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then ENum.SetFocus;
end;

procedure TFFlyLabelGBCode.EYearKeyPress(Sender: TObject; var Key: Char);
begin
  lstCode.Items.Add(IntToStr(Ord(Key)));
  if Key = #13 then EArea.SetFocus;
end;

procedure TFFlyLabelGBCode.BtnAddRecordClick(Sender: TObject);
begin
  lstCode.Items.Add(Format('%.7d', [StrToInt(ELostNo.Text)]));
  ELostNo.Text:= Copy(ELostNo.Text, 1, EHeaderBit.Value);
  ELostNo.SelStart:= 20;
  lbCount.Caption:= Format('共有%d个标签', [lstCode.Count]);
end;

procedure TFFlyLabelGBCode.BtnClearRecordsClick(Sender: TObject);
begin
  lstCode.Items.Clear;
  btnMake.SetFocus;
end;

procedure TFFlyLabelGBCode.BtnDeleteRecordClick(Sender: TObject);
begin
  if lstCode.ItemIndex < 0 then Exit;
  lstCode.Items.Delete(lstCode.ItemIndex);
  lbCount.Caption:= Format('共有%d个标签', [lstCode.Count]);
end;

procedure TFFlyLabelGBCode.btnMakeClick(Sender: TObject);
var
  I, sNo, sNum: Integer;
begin
  sNo:= StrToInt(EStartNo.Text);
  sNum:= StrToInt(ENum.Text);
  for I := 0 to sNum - 1 do
    lstCode.Items.Add(Format('%.7d', [sNo + I]));
  EStartNo.Text:= IntToStr(sNo + sNum);
  btnPrint.SetFocus;
  lbCount.Caption:= Format('共有%d个标签', [lstCode.Count]);
end;

procedure TFFlyLabelGBCode.btnPrintClick(Sender: TObject);
var
  CurrentIdx, labelIdx: Integer;
begin
  if LstCode.Count < 1 then
    raise Exception.Create('没有打印任务。');
  CurrentIdx:= 0;
  if Printer.Printing then Printer.EndDoc;
  Printer.Title:= Format('LB%sX%d', [LstCode.Items[0], LstCode.Count]);
  mLog.Lines.Text:= Format('------ 从%s开始，打印%s个', [LstCode.Items[0], ENum.Text]);
  mLog.Lines.Add(Format('[%s]开始打印第1页', [TimeToStr(Now)]));
  Printer.BeginDoc;
  Printer.Canvas.Brush.Style:= bsClear;
  while CurrentIdx < lstCode.Count do begin
    labelIdx:= CurrentIdx mod 20;
    if(CurrentIdx > 0)and(labelIdx = 0)then begin
      Printer.NewPage;
//  800 / 25.4 * 220 = 6929.1338582677165354330708661417
//  800 / 25.4 * 31.5 = 992.12598425196850393700787401575
      Printer.Canvas.FillRect(Rect(0, 0, 6929, 992));
      mLog.Lines.Add(Format('[%s]正在打印第%d页.', [TimeToStr(Now), (CurrentIdx div 20) + 1]));
    end;
    DrewCHN(Round(labelIdx * 330.708 + 125.984), 0);
    DrewYear(Round(labelIdx * 330.708 + 125.984), 0);
    DrewArea(Round(labelIdx * 330.708 + 125.984), 0);
    Printer.Canvas.Pen.Color:= clBlack;
    Printer.Canvas.Brush.Color:= clBlack;
    Printer.Canvas.Ellipse(Round(labelIdx * 330.708 + 270.984), 107, Round(labelIdx * 330.708 + 280.984), 117);
    Printer.Canvas.Pen.Color:= clWhite;
    Printer.Canvas.Brush.Color:= clWhite;
    DrewCode(Round(labelIdx * 330.708 + 125.984), 0, EYear.text + EArea.text + lstCode.Items[CurrentIdx]);
    DrewNo(Round(labelIdx * 330.708 + 145.984), 0, lstCode.Items[CurrentIdx]);
    Inc(CurrentIdx);
  end;
  Printer.EndDoc;
end;

{$endregion}
{$region 'Labels graphi'}

(*[CHN] Kind=0 CopyMode=9 SerialNo=0 VRate=100 HRate=100 LEN=6 BKCOLOR=-16777201
FONTCOLOR=-16777208 Top=3 Left=26 WordWidth=71 Align=0 FontName=方正小标宋简体
FontSize=63 Rotate=0 Bold=0 Italic=0 Underline=0 StrikeOut=0 ExampleText=CHN Items=*)
procedure TFFlyLabelGBCode.DrewCHN(X, Y: Integer);
begin
  with Printer.Canvas do begin
    Font.Name:= '方正小标宋简体';             //方正小标宋简体
//    Font.Charset:=
    Font.Size:= 7;
    Font.Style:= [];
    TextOut(X + 26, Y + 3, 'C');              //26               26
    TextOut(X + 97, Y + 3, 'H');              //26+71            91   65
    TextOut(X + 168, Y + 3, 'N');             //26+71+71         156  65
  end;
end;
(*[年份] Kind=0 CopyMode=9 SerialNo=0 VRate=100 HRate=100 LEN=6 BKCOLOR=-16777201
FONTCOLOR=-16777208 Top=83 Left=30 WordWidth=26 Align=0 FontName=文鼎CS中等线
FontSize=43 Rotate=0 Bold=0 Italic=0 Underline=0 StrikeOut=0 ExampleText=2015 Items=*)
procedure TFFlyLabelGBCode.DrewYear(X, Y: Integer);
var
  I: Integer;
begin
  with Printer.Canvas do begin
    Font.Name:= '文鼎CS中等线';
    Font.Size:= 6;
    Font.Style:= [];
    for I := 1 to Length(EYear.Text) do
      TextOut(X + 30 + (I - 1) * 25, Y + 78, EYear.Text[I]);
  end;
end;
(*[区号] Kind=0 CopyMode=9 SerialNo=0 VRate=100 HRate=100 LEN=6 BKCOLOR=-16777201
FONTCOLOR=-16777208 Top=83 Left=172 WordWidth=26 Align=0 FontName=文鼎CS中等线
FontSize=43 Rotate=0 Bold=0 Italic=0 Underline=0 StrikeOut=0 ExampleText=00 Items=*)
procedure TFFlyLabelGBCode.DrewArea(X, Y: Integer);
var
  I: Integer;
begin
  with Printer.Canvas do begin
    Font.Name:= '文鼎CS中等线';
    Font.Size:= 6;
    Font.Style:= [];
    for I := 1 to Length(EArea.Text) do
      TextOut(X + 167 + (I- 1) * 25, Y + 78, EArea.Text[I]);
  end;
end;
(*[二维码] Kind=2 CopyMode=9 SerialNo=1 VRate=100 HRate=100 BKCOLOR=-16777201
FONTCOLOR=-16777208 Top=146 Left=31 CODETYPE=0 CODESIZE=9 CODELEVEL=0 Diameter=8.5
Rotate=1 Version=1 PrnDpi=31.49606299 CodeValue=编码*)
procedure TFFlyLabelGBCode.DrewCode(X, Y: Integer; Code: string);
begin
  //QRCode(Code: PChar; Ver, CODESIZE: Integer; PrnDpi, Diameter: Double): TBitmap;
  Printer.Canvas.Draw(X + 31, Y + 146, QRCode(PChar(Code), 1, 9, 31.49606299, 8.5));
end;
(*[编码] Kind=0 CopyMode=9 SerialNo=1 VRate=100 HRate=100 LEN=6 BKCOLOR=-16777201
FONTCOLOR=-16777208 Top=365 Left=15 WordWidth=86 Align=0 FontName=汉鼎繁特粗黑
FontSize=134 Rotate=1 Bold=0 Italic=0 Underline=0 StrikeOut=0 ExampleText=1234567 Items=*)
procedure TFFlyLabelGBCode.DrewNo(X, Y: Integer; Code: string);
const
  numS = '0123456789';
var
  I: Integer;
begin
  with Printer.Canvas do
    for I := 1 to Length(Code) do
      Draw(X + 15, Y + (I - 1) * 70 + 365, numBmp[Pos(Code[I], numS) - 1]);
end;

{$endregion}
{$region 'Form methods'}

procedure TFFlyLabelGBCode.FormCreate(Sender: TObject);

(*[编码] Kind=0 CopyMode=9 SerialNo=1 VRate=100 HRate=100 LEN=6 BKCOLOR=-16777201
FONTCOLOR=-16777208 Top=365 Left=15 WordWidth=86 Align=0 FontName=汉鼎繁特粗黑
FontSize=134 Rotate=1 Bold=0 Italic=0 Underline=0 StrikeOut=0 ExampleText=1234567 Items=*)
  function InitNumBmp(num: Integer): TBitmap;
  var
    X, Y: Integer;
    szbmp: TBitmap;
  begin
    szbmp:= TBitmap.Create;
    szbmp.Canvas.FillRect(Rect(0, 0, szbmp.Width, szbmp.Height));
    szbmp.Canvas.Font.Name:= '汉鼎繁特粗黑';
    szbmp.Canvas.Font.Size:= 110;
    szbmp.Width:= szbmp.Canvas.TextWidth(IntToStr(num));
    szbmp.Height:= szbmp.Canvas.TextHeight(IntToStr(num));
    szbmp.Canvas.TextOut(0, 0, IntToStr(num));

    Result:= TBitmap.Create;
    Result.Width:= szbmp.Height;
    Result.Height:= szbmp.Width;
    Result.Canvas.FillRect(Rect(0, 0, Result.Width, Result.Height));
    for X := 0 to Result.Width - 1 do
      for Y := 0 to Result.Height - 1 do
        Result.Canvas.Pixels[Result.Width - 1 - X, Y]:= szbmp.Canvas.Pixels[Y, X];   //szbmp.Width - 1 -
    szbmp.Free;
  end;
begin
  FQR:= TVonQRCode.Create;
  numBmp[0]:= InitNumBmp(0);
  numBmp[1]:= InitNumBmp(1);
  numBmp[2]:= InitNumBmp(2);
  numBmp[3]:= InitNumBmp(3);
  numBmp[4]:= InitNumBmp(4);
  numBmp[5]:= InitNumBmp(5);
  numBmp[6]:= InitNumBmp(6);
  numBmp[7]:= InitNumBmp(7);
  numBmp[8]:= InitNumBmp(8);
  numBmp[9]:= InitNumBmp(9);
  with TIniFile.Create(ExtractFilePath(Application.ExeName) + 'FlyLabel.INI') do try
    EYear.Text:= ReadString('GBCode', 'YEAR', '2015');
    EArea.Text:= ReadString('GBCode', 'AREA', '00');
    EStartNo.Text:= ReadString('GBCode', 'START', '1');
    ENum.Text:= ReadString('GBCode', 'NUM', '1600');
    EHeaderBit.Value:= ReadInteger('GBCode', 'HeaderBit', 4);
  finally
    Free;
  end;
end;

procedure TFFlyLabelGBCode.FormDestroy(Sender: TObject);
begin
  FQR.Free;
  with TIniFile.Create(ExtractFilePath(Application.ExeName) + 'FlyLabel.INI') do try
    WriteString('GBCode', 'YEAR', EYear.Text);
    WriteString('GBCode', 'AREA', EArea.Text);
    WriteString('GBCode', 'START', EStartNo.Text);
    WriteString('GBCode', 'NUM', ENum.Text);
  finally
    Free;
  end;
end;

procedure TFFlyLabelGBCode.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  lstCode.Columns:= lstCode.Width div 60;
end;

{$endregion}

end.
