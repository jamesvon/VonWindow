unit UFlyLabelInfo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, ExtCtrls,
  ComCtrls, StdCtrls, DB, ADODB, UVonSystemFuns, UVonClass;

type
  TCategoryInfo = class
  private
	{ Private Members }
    FID: integer;
    FCaption: string;
    FIDX: integer;
    FListOrder: integer;
    FConn: TADOConnection;
    function CheckInfo: boolean;
    procedure SetCaption(const Value: string);
    procedure SetID(const Value: integer);
    procedure SetIDX(const Value: integer);
    procedure SetListOrder(const Value: integer);
  public
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean;
    function SaveToDB(DataSet : TCustomADODataSet): Boolean;
  published
    property ID: integer read FID write SetID;
    property Caption: string read FCaption write SetCaption;
    property IDX: integer read FIDX write SetIDX;
    property ListOrder: integer read FListOrder write SetListOrder;
  end;

  TLabelInfo = class
  private
	{ Private Members }
    FID: integer;
    FLabelName: string;
    FCategoryIDX: integer;
    FPicture: TBitmap;
    FImageWidth: integer;
    FImageLength: integer;
    FLastNO: integer;
    FLabelWidth: integer;
    FLabelHeight: integer;
    FContent: TVonConfig;
    FCRCCode: string;
    FConn: TADOConnection;
    FPixel: integer;
    function GetCRCStr: String;
    function CheckInfo: boolean;
    function GetContent: TVonConfig;
    function GetPicture: TBitmap;
    procedure SetCategoryIDX(const Value: integer);
    procedure SetCRCCode(const Value: string);
    procedure SetID(const Value: integer);
    procedure SetImageLength(const Value: integer);
    procedure SetImageWidth(const Value: integer);
    procedure SetLabelHeight(const Value: integer);
    procedure SetLabelName(const Value: string);
    procedure SetLabelWidth(const Value: integer);
    procedure SetLastNO(const Value: integer);
    function GetParamValue(TaskName, ParamName: string): string;
    procedure SetParamValue(TaskName, ParamName: string; const Value: string);
    procedure SetPixel(const Value: integer);
  public
    destructor Destroy; override;
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean;
    function SaveToDB(DataSet : TCustomADODataSet): Boolean;
    function CheckData: Boolean;
    property ParamValue[TaskName, ParamName: string]: string read GetParamValue write SetParamValue;
  published
    property ID: integer read FID write SetID;
    property LabelName: string read FLabelName write SetLabelName;
    property CategoryIDX: integer read FCategoryIDX write SetCategoryIDX;
    property Picture: TBitmap read GetPicture;
    property ImageWidth: integer read FImageWidth write SetImageWidth;
    property ImageLength: integer read FImageLength write SetImageLength;
    property LastNO: integer read FLastNO write SetLastNO;
    property LabelWidth: integer read FLabelWidth write SetLabelWidth;
    property LabelHeight: integer read FLabelHeight write SetLabelHeight;
    property Pixel: integer read FPixel write SetPixel;
    property Content: TVonConfig read GetContent;
    property CRCCode: string read FCRCCode write SetCRCCode;
  end;

implementation

uses UPlatformInfo;

{ TCategoryInfo }

function TCategoryInfo.CheckInfo: boolean;
begin
  Result:= False;
  if Caption = '' then begin WriteInfo('名称必须录入，数据无效。'); Exit; end;
  Result:= True;
end;

function TCategoryInfo.LoadFromDB(DataSet: TCustomADODataSet): Boolean;
begin
  Result:= False;
  FConn:= DataSet.Connection;
  with DataSet do begin
    if IsEmpty then begin
      WriteInfo('无法对空列表进行提取数据的操作。');
      Exit;
    end;
    FID:= FieldByName('ID').AsInteger;
    FCaption:= FieldByName('Caption').AsString;
    FIDX:= FieldByName('IDX').AsInteger;
    FListOrder:= FieldByName('ListOrder').AsInteger;
  end;
  Result:= True;
end;

function TCategoryInfo.SaveToDB(DataSet: TCustomADODataSet): Boolean;
begin
  Result:= False;
  FConn:= DataSet.Connection;
  if not CheckInfo then Exit;
  with DataSet do begin
    if ID = 0 then Append else Edit;
    FieldByName('ID').AsInteger:= FID;
    FieldByName('Caption').AsString:= FCaption;
    FieldByName('IDX').AsInteger:= FIDX;
    FieldByName('ListOrder').AsInteger:= FListOrder;
    Post;
    ID:= FieldByName('ID').AsInteger;
  end;
  Result:= True;
end;

procedure TCategoryInfo.SetCaption(const Value: string);
begin
  FCaption := Value;
end;

procedure TCategoryInfo.SetID(const Value: integer);
begin
  FID := Value;
end;

procedure TCategoryInfo.SetIDX(const Value: integer);
begin
  FIDX := Value;
end;

procedure TCategoryInfo.SetListOrder(const Value: integer);
begin
  FListOrder := Value;
end;

{ TLabelInfo }

function TLabelInfo.CheckData: Boolean;
begin
  Result:= true;//Get_ValidCode(GetCRCStr) = FCRCCode;
  if not Result then WriteInfo(RES_INFO_CRCERR);
end;

function TLabelInfo.CheckInfo: boolean;
begin
  Result:= False;
  if LabelName = '' then begin WriteInfo('名称必须录入，数据无效。'); Exit; end;
  Result:= True;
end;

destructor TLabelInfo.Destroy;
begin
  if Assigned(FPicture) then;
    FPicture.Free;
  if Assigned(FContent) then;
    FContent.Free;
  inherited;
end;

function TLabelInfo.GetContent: TVonConfig;
begin
  if not Assigned(FContent) then begin
    FContent:= TVonConfig.Create;
    if Assigned(FConn) then
      GetMemo(ID, 'FL_Label', 'Content', FContent, FConn);
  end;
  Result:= FContent;
end;

function TLabelInfo.GetCRCStr: String;
begin
  Result:=IntToStr(ID) + LabelName + IntToStr(CategoryIDX) +
    IntToStr(ImageWidth) + IntToStr(ImageLength) + IntToStr(LastNO) +
    IntToStr(LabelWidth) + IntToStr(LabelHeight) + Content.Text;
end;

function TLabelInfo.GetParamValue(TaskName, ParamName: string): string;
begin
  Result:= Content.ReadString(TaskName, ParamName, '');
end;

function TLabelInfo.GetPicture: TBitmap;
begin
  if not Assigned(FPicture) then begin
    FPicture:= TBitmap.Create;
    if Assigned(FConn) then
      GetMemo(ID, 'FL_Label', 'Picture', FPicture, FConn);
  end;
  Result:= FPicture;
end;

function TLabelInfo.LoadFromDB(DataSet: TCustomADODataSet): Boolean;
begin
  Result:= False;
  FConn:= DataSet.Connection;
  with DataSet do begin
    if IsEmpty then begin
      WriteInfo('无法对空列表进行提取数据的操作。');
      Exit;
    end;
    FID:= FieldByName('ID').AsInteger;
    FLabelName:= FieldByName('LabelName').AsString;
    FCategoryIDX:= FieldByName('CategoryIDX').AsInteger;
    if Assigned(FPicture)then FreeAndNil(FPicture);
    FImageWidth:= FieldByName('ImageWidth').AsInteger;
    FImageLength:= FieldByName('ImageLength').AsInteger;
    FLastNO:= FieldByName('LastNO').AsInteger;
    FPixel:= FieldByName('Pixel').AsInteger;
    FLabelWidth:= FieldByName('LabelWidth').AsInteger;
    FLabelHeight:= FieldByName('LabelHeight').AsInteger;
    if Assigned(FContent)then FreeAndNil(FContent);
    FCRCCode:= FieldByName('CRCCode').AsString;
  end;
  Result:= CheckData;
end;

function TLabelInfo.SaveToDB(DataSet: TCustomADODataSet): Boolean;
begin
  Result:= False;
  FConn:= DataSet.Connection;
  if not CheckInfo then Exit;
  with DataSet do begin
    if FID = 0 then Append else Edit;
    FieldByName('LabelName').AsString:= FLabelName;
    FieldByName('CategoryIDX').AsInteger:= FCategoryIDX;
    FieldByName('ImageWidth').AsInteger:= FImageWidth;
    FieldByName('ImageLength').AsInteger:= FImageLength;
    FieldByName('LastNO').AsInteger:= FLastNO;
    FieldByName('Pixel').AsInteger:= FPixel;
    FieldByName('LabelWidth').AsInteger:= FLabelWidth;
    FieldByName('LabelHeight').AsInteger:= FLabelHeight;
    FieldByName('CRCCode').Asstring:= '';//Get_ValidCode(GetCRCStr);	//校验码
    Post;
    ID:= FieldByName('ID').AsInteger;
    if Assigned(FContent)then
      SetMemo(ID, 'FL_Label', 'Content', FContent, FConn);
    if Assigned(FPicture)then
      SetMemo(ID, 'FL_Label', 'Picture', FPicture, FConn);
  end;
  Result:= True;
end;

procedure TLabelInfo.SetCategoryIDX(const Value: integer);
begin
  FCategoryIDX := Value;
end;

procedure TLabelInfo.SetCRCCode(const Value: string);
begin
  FCRCCode := Value;
end;

procedure TLabelInfo.SetID(const Value: integer);
begin
  FID := Value;
end;

procedure TLabelInfo.SetImageLength(const Value: integer);
begin
  FImageLength := Value;
end;

procedure TLabelInfo.SetImageWidth(const Value: integer);
begin
  FImageWidth := Value;
end;

procedure TLabelInfo.SetLabelHeight(const Value: integer);
begin
  FLabelHeight := Value;
end;

procedure TLabelInfo.SetLabelName(const Value: string);
begin
  FLabelName := Value;
end;

procedure TLabelInfo.SetLabelWidth(const Value: integer);
begin
  FLabelWidth := Value;
end;

procedure TLabelInfo.SetLastNO(const Value: integer);
begin
  FLastNO := Value;
end;

procedure TLabelInfo.SetParamValue(TaskName, ParamName: string;
  const Value: string);
begin
  Content.WriteString(TaskName, ParamName, Value);
end;

procedure TLabelInfo.SetPixel(const Value: integer);
begin
  FPixel := Value;
end;

end.
