unit UFlyLabelList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls, Buttons, DBCtrls, Grids, DBGrids,
  Menus, UFlyLabelDB, DB, ADODB, ImgList, ToolWin, UVonSystemFuns,
  System.ImageList;

type
  TFFlyLabelList = class(TForm)
    Splitter1: TSplitter;
    Panel1: TPanel;
    Splitter2: TSplitter;
    ImageList1: TImageList;
    ListMenu: TPopupMenu;
    MCutLabel: TMenuItem;
    MCopyLabel: TMenuItem;
    MPasteLabel: TMenuItem;
    N5: TMenuItem;
    MAddLabel: TMenuItem;
    MEditLabel: TMenuItem;
    MCloneLabel: TMenuItem;
    MDelLabel: TMenuItem;
    N2: TMenuItem;
    Panel5: TPanel;
    ViewCategory: TTreeView;
    ToolBar1: TToolBar;
    Folder: TImageList;
    btnMoveToTop: TToolButton;
    btnMoveUp: TToolButton;
    btnMoveDown: TToolButton;
    btnMoveBottom: TToolButton;
    ToolButton5: TToolButton;
    btnMoveToLeft: TToolButton;
    btnMoveToRight: TToolButton;
    ToolButton8: TToolButton;
    btnAdd: TToolButton;
    btnAddChild: TToolButton;
    btnSave: TToolButton;
    BtnDel: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    EFolderName: TEdit;
    ToolButton15: TToolButton;
    ToolButton9: TToolButton;
    Panel6: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    ELabelName: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    plEditor: TPanel;
    BtnAddLabel: TBitBtn;
    BtnEditLabel: TBitBtn;
    BtnCloneLabel: TBitBtn;
    BtnDelLabel: TBitBtn;
    BtnPrnLabel: TBitBtn;
    Panel3: TPanel;
    BitBtn6: TBitBtn;
    btnSearchlog: TBitBtn;
    BitBtn3: TBitBtn;
    RadioGroup1: TRadioGroup;
    DBGrid1: TDBGrid;
    btnFastPrint: TBitBtn;
    DBImage2: TDBImage;
    procedure FormCreate(Sender: TObject);
    procedure ViewCategoryGetSelectedIndex(Sender: TObject;
      Node: TTreeNode);
    procedure ViewCategoryExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure BtnAddLabelClick(Sender: TObject);
    procedure ViewCategoryChange(Sender: TObject; Node: TTreeNode); 
    procedure ViewCategoryMoveTo(Sender: TObject; Node: TTreeNode);
    procedure BtnEditLabelClick(Sender: TObject);
    procedure BtnCloneLabelClick(Sender: TObject);
    procedure BtnDelLabelClick(Sender: TObject);
    procedure BtnPrnLabelClick(Sender: TObject);
    procedure MCopyLabelClick(Sender: TObject);
    procedure MCutLabelClick(Sender: TObject);
    procedure MPasteLabelClick(Sender: TObject);
    procedure ListMenuPopup(Sender: TObject);
    procedure btnSearchlogClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure DQLablesAfterScroll(DataSet: TDataSet);
    procedure btnAddClick(Sender: TObject);
    procedure btnAddChildClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure BtnDelClick(Sender: TObject);
    procedure btnMoveToTopClick(Sender: TObject);
    procedure btnMoveUpClick(Sender: TObject);
    procedure btnMoveDownClick(Sender: TObject);
    procedure btnMoveBottomClick(Sender: TObject);
    procedure btnMoveToLeftClick(Sender: TObject);
    procedure btnMoveToRightClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure BitBtn3Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure btnFastPrintClick(Sender: TObject);
  private
    { Private declarations }
    FSQLText: string;
    FCopiedType: char;    //1: Copy 2:Cut
    procedure ExpandCategory(AParentIDX: integer; Node: TTreeNode);
    function GetNewID: Integer;
    procedure DisplayLabel;
    procedure UpdateListOrder(Node: TTreeNode);
  public
    { Public declarations }
  end;

var
  FFlyLabelList: TFFlyLabelList;

implementation

uses UFlyLabelEdit, UFlyLabelPrint, UFlyLabelMain, UFastPrinter;

{$R *.dfm}

procedure TFFlyLabelList.FormCreate(Sender: TObject);
begin     //系统初始化
  Caption:= Caption + ' ' + '';//GetApplicationVersion;
  ExpandCategory(0, nil);
  ViewCategoryChange(nil, nil);
  FSQLText:= FFlyLabelDB.DQLables.SQL.Text;
  FFlyLabelDB.DQLabel.Open;
end;

(* 目录分类管理 *)

procedure TFFlyLabelList.ViewCategoryGetSelectedIndex(Sender: TObject;
  Node: TTreeNode);
begin     //显示选择的图标
  Node.SelectedIndex:= 1;
end;

procedure TFFlyLabelList.ExpandCategory(AParentIDX: integer; Node: TTreeNode);
begin     //展开子函数
  with FFlyLabelDB.DQCategory do begin
    Close;
    Parameters[0].Value:= AParentIDX;
    Open;
    while not EOF do begin
      with ViewCategory.Items.AddChild(Node, FieldByName('Caption').AsString) do begin
        StateIndex:= FieldByName('ID').AsInteger;
        HasChildren:= true;
      end;
      Next;
    end;
    if Node = nil then Exit;
    if Node.Count = 0 then Node.HasChildren:= false;
  end;
end;

procedure TFFlyLabelList.ViewCategoryExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin     //View展开
  if node = nil then Exit;
  if Node.HasChildren then
    if Node.Count = 0 then ExpandCategory(Node.StateIndex, Node);
end;

procedure TFFlyLabelList.ViewCategoryChange(Sender: TObject; Node: TTreeNode);
begin     //目录变化提取当前目录中的样式
  if ViewCategory.Selected = nil then Exit;
  EFolderName.Text:= ViewCategory.Selected.Text;
  with FFlyLabelDB.DQLables do begin
    Close;
    FFlyLabelDB.DQLabel.Close;
    SQL.Text:= FSQLText;
    Parameters[0].Value:= ViewCategory.Selected.StateIndex;
    Open;
    RadioGroup1Click(nil);
    FFlyLabelDB.DQLabel.Open;
  end;
end;

procedure TFFlyLabelList.ViewCategoryMoveTo(Sender: TObject; Node: TTreeNode);
begin
  FFlyLabelDB.DQLabel.Edit;
  FFlyLabelDB.DQLabel.FieldByName('CategoryIDX').AsInteger:= ViewCategory.Selected.StateIndex;
  FFlyLabelDB.DQLabel.Post;
  ViewCategory.OnChange:= ViewCategoryChange;
  ViewCategoryChange(nil, ViewCategory.Selected);
end;

(* 样式管理 *)

procedure TFFlyLabelList.BtnAddLabelClick(Sender: TObject);
begin     //添加样式

  FFlyLabelMain.OpenModule('添加样式', TFFlyLabelEdit, dtMulti, 0,
    IntToStr(ViewCategory.Selected.StateIndex), '');
//  AppendLabel(Self, FFlyLabelDB.DQLabel, ViewCategory.Selected.StateIndex);
end;

procedure TFFlyLabelList.BtnEditLabelClick(Sender: TObject);
begin     //修改当前样式
  FFlyLabelMain.OpenModule('编辑样式', TFFlyLabelEdit, dtMulti, 0,
    FFlyLabelDB.DQLabel.FieldByName('CategoryIDX').AsString,
    FFlyLabelDB.DQLabel.FieldByName('ID').AsString);
//  EditLabel(self, FFlyLabelDB.DQLabel);
end;

procedure TFFlyLabelList.BtnCloneLabelClick(Sender: TObject);
begin     //克隆当前样式
//  CloneLabel(self, FFlyLabelDB.DQLabel, ViewCategory.Selected.StateIndex);
end;

procedure TFFlyLabelList.BtnDelLabelClick(Sender: TObject);
begin     //删除当前样式
  if FFlyLabelDB.DQLabel.RecordCount < 1 then Exit;
  if MessageDlg('是否确定要删除这个样式吗?', mtWarning, mbOKCancel, 0) = mrOK then begin
//    FFlyLabelDB.AddTask(FFlyLabelDB.DQLabel.FieldByName('ID').AsInteger, 2, FFlyLabelDB.LogonName, FFlyLabelDB.DQLabel.FieldByName('LabelName').AsString);
    FFlyLabelDB.DQLabel.Delete;
  end;
end;

procedure TFFlyLabelList.BtnPrnLabelClick(Sender: TObject);
begin     //打印
  DBGrid1DblClick(nil);
end;

procedure TFFlyLabelList.MCopyLabelClick(Sender: TObject);
begin     //复制当前样式
  FFlyLabelDB.DQOrgLabel.Parameters[0].Value:= FFlyLabelDB.DQLabel.Fields[0].AsInteger;
  FCopiedType:= 'P'; //Copy
end;

procedure TFFlyLabelList.MCutLabelClick(Sender: TObject);
begin     //剪切当前样式
  FFlyLabelDB.DQOrgLabel.Parameters[0].Value:= FFlyLabelDB.DQLabel.Fields[0].AsInteger;
  FCopiedType:= 'T'; //Cut
end;

procedure TFFlyLabelList.MPasteLabelClick(Sender: TObject);
var
  i: Integer;
begin     //粘贴当前样式
  case FCopiedType of
  'T': with FFlyLabelDB.DQOrgLabel do try  //CUT
      Open;
      Edit;
      FieldByName('CategoryIDX').AsInteger:= FFlyLabelDB.DQLabel.Parameters[0].Value;
      Post;
      FFlyLabelDB.DQLabel.Close; FFlyLabelDB.DQLabel.Open;
    finally
      Close;
    end;
  'P': try
      FFlyLabelDB.DQOrgLabel.Open;
      FFlyLabelDB.DQLabel.Append;
      for i:= 1 to FFlyLabelDB.DQLabel.FieldCount - 1 do
        FFlyLabelDB.DQLabel.Fields[i].Assign(FFlyLabelDB.DQOrgLabel.Fields[i]);
      FFlyLabelDB.DQLabel.FieldByName('CategoryIDX').AsInteger:= FFlyLabelDB.DQLabel.Parameters[0].Value;
      FFlyLabelDB.DQLabel.Post;
    finally
      FFlyLabelDB.DQOrgLabel.Close;
    end;
  end;
  FCopiedType:= char(0);
end;

procedure TFFlyLabelList.ListMenuPopup(Sender: TObject);
begin
  MPasteLabel.Enabled:= not(FCopiedType = char(0));
  MCopyLabel.Enabled:= FFlyLabelDB.DQLabel.RecordCount > 0;
  MCutLabel.Enabled:= FFlyLabelDB.DQLabel.RecordCount > 0;
  MDelLabel.Enabled:= FFlyLabelDB.DQLabel.RecordCount > 0;
end;

function TFFlyLabelList.GetNewID: Integer;
begin
  with FFlyLabelDB.DQNewID do begin
    Open;
    Result:= Fields[0].AsInteger;
    Close;
  end;
end;

procedure TFFlyLabelList.btnFastPrintClick(Sender: TObject);
begin
  FFlyLabelMain.OpenModule('快速打印', TFFastPrinter, dtMulti, 0,
    FFlyLabelDB.DQLabel.FieldByName('CategoryIDX').AsString,
    FFlyLabelDB.DQLabel.FieldByName('ID').AsString);
end;

procedure TFFlyLabelList.btnSearchlogClick(Sender: TObject);
begin
//  with TFLog.Create(Application) do try
//    Self.Hide;
//    ShowModal;
//  finally
//    Free;
//    Self.Show;
//  end;
end;

procedure TFFlyLabelList.BitBtn1Click(Sender: TObject);
begin     //定位
  FFlyLabelDB.DQLables.Locate('LabelName', ELabelName.Text, [loPartialKey]);
end;

procedure TFFlyLabelList.BitBtn2Click(Sender: TObject);
begin     //待条件提取当前样式
  with FFlyLabelDB.DQLables do begin
    Close;
    SQL.Text:= FSQLText;
    SQL.Add('AND [LabelName] LIKE ''%' + ELabelName.Text + '%''');
    Parameters[0].Value:= ViewCategory.Selected.StateIndex;
    Open;
  end;
end;

procedure TFFlyLabelList.DQLablesAfterScroll(DataSet: TDataSet);
begin
  DisplayLabel;
end;

procedure TFFlyLabelList.DisplayLabel;
begin
  FFlyLabelDB.DQLabel.Close;
  FFlyLabelDB.DQLabel.Parameters[0].Value:= FFlyLabelDB.DQLables.Fields[0].AsInteger;
  FFlyLabelDB.DQLabel.Open;
end;

(* Folder *)

procedure TFFlyLabelList.btnAddClick(Sender: TObject);
var
  parentIDX: Integer;
begin     //添加同级目录
  if ViewCategory.Selected = nil then Exit;
  if ViewCategory.Selected.Parent = nil then parentIDX:= 0 else parentIDX:= ViewCategory.Selected.Parent.StateIndex;
  with FFlyLabelDB.DCCategory do begin
    CommandText:= 'INSERT INTO FL_Category(Caption, IDX)VALUES(:DisplayCAption, :ParentIDX)';
    Parameters[0].Value:= EFolderName.Text;
    Parameters[1].Value:= parentIDX;
    Execute;
  end;
  ViewCategory.Items.AddChild(ViewCategory.Selected.Parent, EFolderName.Text).StateIndex:= GetNewID;
end;

procedure TFFlyLabelList.btnAddChildClick(Sender: TObject);
var
  parentIDX: Integer;
begin     //添加下级目录
  if ViewCategory.Selected = nil then parentIDX:= 0 else parentIDX:= ViewCategory.Selected.StateIndex;
  with FFlyLabelDB.DCCategory do begin
    CommandText:= 'INSERT INTO FL_Category(Caption, IDX)VALUES(:DisplayCAption, :ParentIDX)';
    Parameters[0].Value:= EFolderName.Text;
    Parameters[1].Value:= parentIDX;
    Execute;
  end;
  ViewCategory.Items.AddChild(ViewCategory.Selected, EFolderName.Text).StateIndex:= GetNewID;
end;

procedure TFFlyLabelList.btnSaveClick(Sender: TObject);
begin     //修改目录名称
  if ViewCategory.Selected = nil then Exit;
  with FFlyLabelDB.DCCategory do begin
    CommandText:= 'UPDATE FL_Category SET Caption=:DisplayCAption WHERE ID=:SelfID';
    Parameters[0].Value:= EFolderName.Text;
    Parameters[1].Value:= ViewCategory.Selected.StateIndex;
    Execute;
  end;
  ViewCategory.Selected.Text:= EFolderName.Text;
end;

procedure TFFlyLabelList.BtnDelClick(Sender: TObject);
begin     //删除目录
  if ViewCategory.Selected = nil then Exit;
  if MessageDlg('真的要删除这个类型及其子类型吗？', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
    with FFlyLabelDB.DCCategory do begin
      CommandText:= 'DELETE FROM FL_Category WHERE ID=:SelfID';
      Parameters[0].Value:= ViewCategory.Selected.StateIndex;
      Execute;
    end;
    ViewCategory.Selected.Delete;
  end;
end;

procedure TFFlyLabelList.UpdateListOrder(Node: TTreeNode);
var
  szNode: TTreeNode;

  procedure UpdateNodeListOrder(ID, ListOrder: Integer);
  begin
    with FFlyLabelDB.DCCategory do begin
      CommandText:= 'UPDATE FL_Category SET ListOrder=' + IntToStr(ListOrder) + ' WHERE ID=' + IntToStr(ID);
      Execute;
    end;
  end;
begin
  szNode:= Node;
  while Assigned(szNode) do begin
    UpdateNodeListOrder(szNode.StateIndex, szNode.Index);
    szNode:= szNode.getNextSibling;
  end;
end;

procedure TFFlyLabelList.btnMoveToTopClick(Sender: TObject);
begin
  if ViewCategory.Selected = nil then Exit;
  if ViewCategory.Selected.Index = 0 then Exit;
  ViewCategory.Selected.MoveTo(ViewCategory.Selected.Parent, naAddChildFirst);
  UpdateListOrder(ViewCategory.Selected);
  //TNodeAttachMode = (naAdd, naAddFirst, naAddChild, naAddChildFirst, naInsert);
end;

procedure TFFlyLabelList.btnMoveUpClick(Sender: TObject);
begin
  if ViewCategory.Selected = nil then Exit;
  if ViewCategory.Selected.Index = 0 then Exit;
  ViewCategory.Selected.MoveTo(ViewCategory.Selected.getPrevSibling, naInsert);
  UpdateListOrder(ViewCategory.Selected);
end;

procedure TFFlyLabelList.btnMoveDownClick(Sender: TObject);
begin
  if ViewCategory.Selected = nil then Exit;
  if not Assigned(ViewCategory.Selected.getNextSibling()) then Exit;
  if Assigned(ViewCategory.Selected.getNextSibling.getNextSibling()) then
    ViewCategory.Selected.MoveTo(ViewCategory.Selected.getNextSibling.getNextSibling, naInsert)
  else ViewCategory.Selected.MoveTo(ViewCategory.Selected.getNextSibling, naAdd);
  UpdateListOrder(ViewCategory.Selected.getPrevSibling);
end;

procedure TFFlyLabelList.btnMoveBottomClick(Sender: TObject);
var
  szNode: TTreeNode;
begin
  if ViewCategory.Selected = nil then Exit;
  if not Assigned(ViewCategory.Selected.getNextSibling()) then Exit;
  szNode:= ViewCategory.Selected.getNextSibling;
  ViewCategory.Selected.MoveTo(ViewCategory.Selected.getNextSibling, naAdd);
  UpdateListOrder(szNode);
end;

procedure TFFlyLabelList.btnMoveToLeftClick(Sender: TObject);
var
  szNode: TTreeNode;
  PID: Integer;
begin
  if ViewCategory.Selected = nil then Exit;
  if Assigned(ViewCategory.Selected.Parent) then begin
    if Assigned(ViewCategory.Selected.Parent.Parent) then
      PID:= ViewCategory.Selected.Parent.Parent.StateIndex
    else PID:= 0;
  end else Exit;
  szNode:= ViewCategory.Selected.getNextSibling;
  ViewCategory.Selected.MoveTo(ViewCategory.Selected.Parent, naAdd);
  with FFlyLabelDB.DCCategory do begin
    CommandText:= 'UPDATE FL_Category SET ListOrder=' + IntToStr(ViewCategory.Selected.Index) +
      ',Idx=' + IntToStr(PID) + ' WHERE ID=' + IntToStr(ViewCategory.Selected.StateIndex);
    Execute;
  end;
  UpdateListOrder(szNode);
end;

procedure TFFlyLabelList.btnMoveToRightClick(Sender: TObject);
var
  szNode: TTreeNode;  
  PID: Integer;
begin
  if ViewCategory.Selected = nil then Exit;
  if not Assigned(ViewCategory.Selected.getPrevSibling()) then Exit;
  PID:= ViewCategory.Selected.getPrevSibling().StateIndex;
  szNode:= ViewCategory.Selected.getPrevSibling;
  ViewCategory.Selected.MoveTo(ViewCategory.Selected.getPrevSibling, naAddChild);
  with FFlyLabelDB.DCCategory do begin
    CommandText:= 'UPDATE FL_Category SET ListOrder=' + IntToStr(ViewCategory.Selected.Index) +
      ',Idx=' + IntToStr(PID) + ' WHERE ID=' + IntToStr(ViewCategory.Selected.StateIndex);
    Execute;
  end;
  UpdateListOrder(szNode);
end;

procedure TFFlyLabelList.DBGrid1DblClick(Sender: TObject);
begin
  FFlyLabelMain.OpenModule('足环打印', TFFlyLabelPrint, dtMulti, 0, '', FFlyLabelDB.DQLabel.FieldByName('ID').AsString);
end;

procedure TFFlyLabelList.DBGrid1TitleClick(Column: TColumn);
begin
  FFlyLabelDB.DQLables.Sort:= Column.FieldName;
end;

procedure TFFlyLabelList.BitBtn3Click(Sender: TObject);
begin
  ViewCategory.OnChange:= ViewCategoryMoveTo;
end;

procedure TFFlyLabelList.RadioGroup1Click(Sender: TObject);
begin
  case RadioGroup1.ItemIndex of
  0: FFlyLabelDB.DQLables.Sort:= 'ID';
  1: FFlyLabelDB.DQLables.Sort:= 'LabelName';
  end;
end;

end.
