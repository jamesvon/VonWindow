unit UFlyLabelMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UPlatformMain, Menus, ImgList, ComCtrls, ToolWin, ExtCtrls, Tabs,
  DockTabSet, StdCtrls, System.ImageList;

type
  TFFlyLabelMain = class(TFPlatformMain)
    procedure FormCreate(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure StatusBar1DrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel;
      const Rect: TRect);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFlyLabelMain: TFFlyLabelMain;

implementation

uses UFlyLabelList, UFlyLabelDB;

{$R *.dfm}

procedure TFFlyLabelMain.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  inherited;
  if NewHeight >= Screen.Height then
    NewHeight:= Screen.WorkAreaHeight;
end;

procedure TFFlyLabelMain.FormCreate(Sender: TObject);
begin
  inherited;
  StatusBar1.Panels[0].Text:= DateTimeToStr(now);
  StatusBar1.Panels[1].Text:= FFlyLabelDB.LoginInfo.LoginName;
  StatusBar1.Panels[2].Text:= FFlyLabelDB.CertValue['ApplicationTitle'];
  Caption:= FFlyLabelDB.CertValue['ApplicationTitle'] + '[' +
    FFlyLabelDB.CurrentVersion + ']';
end;

procedure TFFlyLabelMain.StatusBar1DrawPanel(StatusBar: TStatusBar;
  Panel: TStatusPanel; const Rect: TRect);
  procedure WriteNormalText(S: string);
  var
    R: TRect;
  begin
    R:= Rect;
    StatusBar1.Canvas.Brush.Color:= StatusBar1.Color;
    StatusBar1.Canvas.Font.Color:= clBlack;
    StatusBar1.Canvas.Pen.Color:= clBlack;
    StatusBar1.Canvas.TextRect(R, S, [tfLeft]);
  end;
  procedure WriteInformationText(S: string);
  var
    R: TRect;
  begin
    R:= Rect;
    StatusBar1.Canvas.Brush.Color:= clGreen;
    StatusBar1.Canvas.Font.Color:= clWhite;
    StatusBar1.Canvas.Pen.Color:= clWhite;
    StatusBar1.Canvas.TextRect(R, S, [tfLeft]);
  end;
  procedure WriteWarningText(S: string);
  var
    R: TRect;
  begin
    R:= Rect;
    StatusBar1.Canvas.Brush.Color:= clRed;
    StatusBar1.Canvas.Font.Color:= clWhite;
    StatusBar1.Canvas.Pen.Color:= clWhite;
    StatusBar1.Canvas.TextRect(R, S, [tfLeft]);
  end;
begin
  inherited;
  case Panel.Index of
  0: WriteNormalText(FFlyLabelDB.LoginInfo.DisplayName);               //��¼�û���ʾ��
  end;
end;

end.
