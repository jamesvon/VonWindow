object FFlyLabelOrder: TFFlyLabelOrder
  Left = 0
  Top = 0
  Caption = 'FFlyLabelOrder'
  ClientHeight = 369
  ClientWidth = 637
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel7: TPanel
    Left = 460
    Top = 45
    Width = 177
    Height = 324
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 328
    ExplicitTop = -74
    ExplicitHeight = 395
    DesignSize = (
      177
      324)
    object BitBtn7: TBitBtn
      Left = 6
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #27454#24050#21040#36134
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 0
    end
    object BitBtn8: TBitBtn
      Left = 87
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #25171#21360
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 1
    end
    object BitBtn9: TBitBtn
      Left = 6
      Top = 37
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #24320#22987#21046#20316
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 2
    end
    object BitBtn10: TBitBtn
      Left = 87
      Top = 37
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #24050#21457#36135
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 3
    end
    object mOrderInfo: TMemo
      Left = 0
      Top = 68
      Width = 177
      Height = 256
      Align = alBottom
      TabOrder = 4
    end
  end
  object lvOrder: TListView
    Left = 0
    Top = 45
    Width = 460
    Height = 324
    Align = alClient
    Columns = <
      item
        Caption = #35746#21333#21495
      end
      item
        Caption = #26679#24335#21495
      end
      item
        Caption = #35746#36135#20154
      end
      item
        Caption = #35746#36135#25968#37327
      end
      item
        Caption = #22320#22336
      end
      item
        Caption = #32852#31995#30005#35805
      end
      item
        Caption = #35746#36135#26102#38388
      end>
    TabOrder = 1
    ViewStyle = vsReport
    OnClick = lvOrderClick
    ExplicitTop = -74
    ExplicitWidth = 458
    ExplicitHeight = 395
  end
  object Panel6: TPanel
    Left = 0
    Top = 0
    Width = 637
    Height = 45
    Align = alTop
    TabOrder = 2
    ExplicitLeft = -130
    ExplicitWidth = 635
    object Label2: TLabel
      Left = 8
      Top = 12
      Width = 48
      Height = 13
      Caption = #35746#21333#29366#24577
    end
    object BitBtn5: TBitBtn
      Left = 175
      Top = 8
      Width = 75
      Height = 25
      Caption = #26597#35810
      DoubleBuffered = True
      ParentDoubleBuffered = False
      TabOrder = 0
      OnClick = BitBtn5Click
    end
    object EStatus: TComboBox
      Left = 62
      Top = 8
      Width = 107
      Height = 21
      ItemIndex = 0
      TabOrder = 1
      Text = #24050#35746#36135
      Items.Strings = (
        #24050#35746#36135
        #24050#20184#27454
        #24050#21046#20316
        #24050#21457#36135
        #24050#32467#26463)
    end
  end
end
