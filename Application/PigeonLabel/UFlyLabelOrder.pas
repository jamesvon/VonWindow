unit UFlyLabelOrder;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UWebService, StdCtrls, ComCtrls, Buttons, ExtCtrls;

type
  TFFlyLabelOrder = class(TForm)
    Panel7: TPanel;
    BitBtn7: TBitBtn;
    BitBtn8: TBitBtn;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    mOrderInfo: TMemo;
    lvOrder: TListView;
    Panel6: TPanel;
    Label2: TLabel;
    BitBtn5: TBitBtn;
    EStatus: TComboBox;
    procedure BitBtn5Click(Sender: TObject);
    procedure lvOrderClick(Sender: TObject);
  private
    { Private declarations }
    lstOrder: ArrayOfOrdersInfo;
  public
    { Public declarations }
  end;

var
  FFlyLabelOrder: TFFlyLabelOrder;

implementation

{$R *.dfm}

procedure TFFlyLabelOrder.BitBtn5Click(Sender: TObject);
var
  I: Integer;
begin
  lstOrder:= GetZuHuanServiceSoap(true).ListOrder(EStatus.ItemIndex);
  lvOrder.Clear;
  for I := 0 to Length(lstOrder) - 1 do
    with lvOrder.Items.Add do begin
      Caption:= lstOrder[I].OrderNo;
      SubItems.Add(IntToStr(lstOrder[I].StyleIdx));
      SubItems.Add(lstOrder[I].Receiver);
      SubItems.Add(IntToStr(lstOrder[I].Num));
      SubItems.Add(lstOrder[I].Address);
      SubItems.Add(lstOrder[I].Linker);
      SubItems.Add(DateTimeToStr(lstOrder[I].OrderDate.AsUTCDateTime));
    end;
end;

procedure TFFlyLabelOrder.lvOrderClick(Sender: TObject);
var
  mPos: Integer;
  S: string;
begin
  if lvOrder.ItemIndex < 0 then Exit;
  with lstOrder[lvOrder.ItemIndex] do begin
    mOrderInfo.Lines.Clear;
    mOrderInfo.Lines.Add('============== 订单号：' + OrderNo + ' ==============');
    mOrderInfo.Lines.Add('订货人：' + Receiver);
    mOrderInfo.Lines.Add('样式号：' + IntToStr(StyleIdx));
    mOrderInfo.Lines.Add('邮寄地址：' + Address);
    mOrderInfo.Lines.Add('邮编：' + Zip);
    mOrderInfo.Lines.Add('联系方式：' + Linker);
    mOrderInfo.Lines.Add('样式内容：');
    mOrderInfo.Lines.Append(ParamValue);
    mOrderInfo.Lines.Add('起始号：' + IntToStr(StartNo));
    mOrderInfo.Lines.Add('订货数量：' + IntToStr(Num));
    mPos := LastDelimiter('/' + PathDelim + DriveDelim, ColorKind);
    S:= Copy(ColorKind, mPos + 1, MaxInt);
    mPos := LastDelimiter('.', S);
    S:= Copy(S, 1, mPos);
    mOrderInfo.Lines.Add('颜色：' + S);
    mOrderInfo.Lines.Add('订货时间：' + DateTimeToStr(OrderDate.AsDateTime));
    mOrderInfo.Lines.Add('附言：' + Note);
  end;
end;

end.
