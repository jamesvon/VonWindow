unit UFlyLabelPreview;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Printers;

type
  TFFlyLabelPreview = class(TForm)
    ScrollBox1: TScrollBox;
    Image1: TImage;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFlyLabelPreview: TFFlyLabelPreview;

implementation

{$R *.dfm}

procedure TFFlyLabelPreview.FormCreate(Sender: TObject);
begin
  Image1.Width:= Round(Printer.PageWidth / 300 * 72);
  Image1.Height:= Round(Printer.PageHeight / 300 * 72);
end;

end.
