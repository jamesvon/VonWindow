unit UFlyLabelPrint;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, Gauges, StdCtrls, Spin, Buttons, ADODB, ExtDlgs,
  Menus, ToolWin, ImgList, UFlyLabelDB, DB, Printers, ULabelPrint, IniFiles,
  CheckLst, UFlyLabelInfo, Grids, ValEdit, UVonSystemFuns, ShellAPI,
  UThreadLabel, UVonLog, System.ImageList;

type
  TFFlyLabelPrint = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ScrollBox1: TScrollBox;
    ImageEdit: TImage;
    TabSheet2: TTabSheet;
    ScrollBox2: TScrollBox;
    ImagePreview: TImage;
    Panel1: TPanel;
    ListRecords: TListBox;
    Panel4: TPanel;
    StatusBar1: TStatusBar;
    PrinterSetupDialog1: TPrinterSetupDialog;
    PageCtrl: TPageControl;
    TabSheet3: TTabSheet;
    Panel7: TPanel;
    BtnClose: TBitBtn;
    SheetManage: TTabSheet;
    BtnImport: TBitBtn;
    BtnExport: TBitBtn;
    BtnDeleteRecord: TBitBtn;
    BtnClearRecords: TBitBtn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    EPreviewText: TEdit;
    BtnPreview: TBitBtn;
    SheetSettings: TTabSheet;
    Label7: TLabel;
    EColCount: TSpinEdit;
    ERowCount: TSpinEdit;
    Label8: TLabel;
    Label9: TLabel;
    ESectionCount: TSpinEdit;
    Label10: TLabel;
    ELeftMargin: TSpinEdit;
    ETopMargin: TSpinEdit;
    Label11: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ENOBitNumber: TSpinEdit;
    ENONumber: TSpinEdit;
    EStartNO: TSpinEdit;
    BtnCreateRecords: TBitBtn;
    BtnGroupPrint: TBitBtn;
    BtnPrinterSetup: TBitBtn;
    BtnPrinterPreview: TBitBtn;
    BtnSaveBMP: TBitBtn;
    Label13: TLabel;
    Label14: TLabel;
    EColSpace: TSpinEdit;
    ERowSpace: TSpinEdit;
    TabSheet4: TTabSheet;
    BtnSaveScheme: TBitBtn;
    EScheme: TEdit;
    OpenPictureDialog1: TOpenPictureDialog;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    EFirstStr: TEdit;
    ELastStr: TEdit;
    PanelPages: TPanel;
    EPageCount: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    ESchemeList: TListBox;
    ImageList1: TImageList;
    Memo1: TMemo;
    TabSheet5: TTabSheet;
    lstPrinter: TCheckListBox;
    ToolBar1: TToolBar;
    btnSelectedAll: TToolButton;
    btnSelectedNone: TToolButton;
    btnPrint2: TToolButton;
    Panel2: TPanel;
    BtnLoadScheme: TBitBtn;
    btnSchemeDelete: TBitBtn;
    TabSheet6: TTabSheet;
    GroupBox2: TGroupBox;
    ERecord: TEdit;
    BtnAddRecord: TBitBtn;
    lstCard: TListBox;
    Label5: TLabel;
    lbCardCount: TLabel;
    Label15: TLabel;
    lbCardNo: TLabel;
    btnImportCard: TBitBtn;
    BtnDeleteCard: TBitBtn;
    Label4: TLabel;
    ECardCount: TSpinEdit;
    Splitter1: TSplitter;
    Label12: TLabel;
    EFillChar: TEdit;
    rbFront: TRadioButton;
    rbLast: TRadioButton;
    rbNone: TRadioButton;
    Label6: TLabel;
    EFlagWidth: TSpinEdit;
    Label16: TLabel;
    ESequencHeight: TSpinEdit;
    Label17: TLabel;
    EPointOffset: TSpinEdit;
    lstValues: TValueListEditor;
    Gauge1: TGauge;
    EPageOfDoc: TSpinEdit;
    btnRestartSvc: TBitBtn;
    ENoRemove: TSpinEdit;
    chkSayKey: TCheckBox;
    chkSayCode: TCheckBox;
    btnLoop: TToolButton;
    BtnTheatPrint: TBitBtn;
    BtnPrint: TBitBtn;
    Label18: TLabel;
    EFlagKind: TComboBox;
    procedure BtnCreateRecordsClick(Sender: TObject);
    procedure Panel1CanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure ENOBitNumberChange(Sender: TObject);
    procedure BtnAddRecordClick(Sender: TObject);
    procedure BtnDeleteRecordClick(Sender: TObject);
    procedure BtnClearRecordsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtnPreviewClick(Sender: TObject);
    procedure BtnPrinterSetupClick(Sender: TObject);
    procedure BtnGroupPrintClick(Sender: TObject);
    procedure BtnPrinterPreviewClick(Sender: TObject);
    procedure BtnSaveBMPClick(Sender: TObject);
    procedure BtnLoadSchemeClick(Sender: TObject);
    procedure BtnSaveSchemeClick(Sender: TObject);
    procedure BtnImportClick(Sender: TObject);
    procedure BtnExportClick(Sender: TObject);
    procedure btnSelectedAllClick(Sender: TObject);
    procedure btnSelectedNoneClick(Sender: TObject);
    procedure btnSchemeDeleteClick(Sender: TObject);
    procedure lstPrinterClick(Sender: TObject);
    procedure btnImportCardClick(Sender: TObject);
    procedure BtnDeleteCardClick(Sender: TObject);
    procedure lstValuesGetPickList(Sender: TObject; const KeyName: string;
      Values: TStrings);
    procedure btnRestartSvcClick(Sender: TObject);
    procedure ERecordKeyPress(Sender: TObject; var Key: Char);
    procedure BtnTheatPrintClick(Sender: TObject);
    procedure BtnPrintClick(Sender: TObject);
  private
    { Private declarations }
    QueryLabel: TADOQuery;
    TempBitmap: TBitmap;
    FCurrentLabel, pageCount: Integer;
    LabelImage: TLabelImage;
    FPrinters: TStringList;
    FCardLabels: TStringList;
    FLabelWidth, FLabelHeight, FCurrentPrintIDX, FPixel: Integer;
    FComMID: Integer;
    procedure SetPrintSettings(LabelPrint: TLabelPrint);
    function GetString(Value: integer; Number: Word): String;
    procedure ResetColumn(NewWidth: Integer);
    procedure LoadDataFromQuery(DQLabel: TADOQuery);
    procedure PrintFinally(Sender: TObject);
    procedure EndOfLabel(Sender: TObject; LabelID: Integer);
    procedure EndOfPage(Sender: TObject; PageID: Integer);
    procedure OpenParam(ParamKind, ParamName: string);
    procedure GetNextPrint;
    procedure SetCurrentLabel(const Value: Integer);
    procedure SetComMID(const Value: Integer);
    procedure ClosePrinters;
    function GetPrinter: TPrinter;
  public
    { Public declarations }
    property CurrentLabel: Integer read FCurrentLabel write SetCurrentLabel;
    property ComMID: Integer read FComMID write SetComMID;
  end;

var
  FFlyLabelPrint: TFFlyLabelPrint;

  //计算TCheckListBox选择结果以二进制数值表现（bit：0表示没选，1表示选择）
  function GetMutiChkByChkList(AList: TCheckListBox): Int64;
  //根据记录整数（二进制记忆方式）回选TCheckListBox的内容
  procedure SetMutiChkToChkList(AValue: Int64; AList: TCheckListBox);

implementation

uses UVonSinoVoiceAPI;

{$R *.dfm}

function Min(A, B: Integer): Integer;
begin
  if A > B then Result:= B
  else Result:= A;
end;

function GetMutiChkByChkList(AList: TCheckListBox): Int64;
var
  i:Integer;
  AResult: Int64;
begin
  AResult:= 1;
  Result:= 0;
  for i:= 0 to AList.Count - 1 do begin
    if AList.Checked[i] then Result:= Result + AResult;
    AResult:= AResult * 2;
  end;
end;

procedure SetMutiChkToChkList(AValue: Int64; AList: TCheckListBox);
var
  i: Integer;
begin
  for i:= 0 to AList.Count - 1 do
    AList.Checked[i]:= (AValue shr i) and 1 > 0;
end;

{ TFPrint }

procedure TFFlyLabelPrint.FormCreate(Sender: TObject);
var
  szID, I: Integer;
begin
  LabelImage:= TLabelImage.Create;
  TempBitmap:= TBitmap.Create;
  FPrinters:= TStringList.Create;
  FCardLabels:= TStringList.Create;

  lstPrinter.Items.Assign(Printer.Printers);
  PageCtrl.ActivePageIndex:= 0;
  if not TryStrToInt(FFlyLabelDB.ExecParams, szID) then
    LoadDataFromQuery(FFlyLabelDB.DQLabel)
  else begin
    FFlyLabelDB.DQLabel.DataSource:= nil;
    FFlyLabelDB.DQLabel.Close;
    FFlyLabelDB.DQLabel.Parameters[0].Value:= szID;
    FFlyLabelDB.DQLabel.Open;
    LoadDataFromQuery(FFlyLabelDB.DQLabel);
    FFlyLabelDB.DQLabel.Close;
    FFlyLabelDB.DQLabel.DataSource:= FFlyLabelDB.DSLables;
    FFlyLabelDB.DQLabel.Open;
  end;
  with TIniFile.Create(ExtractFilePath(Application.ExeName) + 'FlyLabel.INI')do try
    SetMutiChkToChkList(ReadInteger('PRINTERS', 'Choosed', 0), lstPrinter);
    ESchemeList.ItemIndex:= ReadInteger('SCHEME', 'Choosed', 0);
    FComMID:= ReadInteger('SYSTEM', 'COM', 3);
    ENONumber.Value:= ReadInteger('SYSTEM', 'DefaultCount', 1600);
    BtnLoadSchemeClick(self);
    for I := 1 to lstValues.RowCount - 1 do
      lstValues.Cells[1, I]:= ReadString('LABEL', lstValues.Cells[0, I], lstValues.Cells[1, I]);
  finally
    Free;
  end;
end;

procedure TFFlyLabelPrint.FormDestroy(Sender: TObject);
var
  I: Integer;
begin
  with TIniFile.Create(ExtractFilePath(Application.ExeName) + 'FlyLabel.INI')do try
    WriteInteger('PRINTERS', 'Choosed', GetMutiChkByChkList(lstPrinter));
    WriteInteger('SCHEME', 'Choosed', ESchemeList.ItemIndex);
    for I := 1 to lstValues.RowCount - 1 do
      WriteString('LABEL', lstValues.Cells[0, I], lstValues.Cells[1, I]);
    WriteInteger('SYSTEM', 'DefaultCount', ENONumber.Value);
  finally
    Free;
  end;
  FCardLabels.Free;
  FPrinters.Free;
  TempBitmap.Free;
  LabelImage.Free;
end;

procedure TFFlyLabelPrint.BtnCreateRecordsClick(Sender: TObject);
var
  i: Integer;
begin     //生成记录
  for i:=0 to ENONumber.Value - 1 do
    ListRecords.Items.Add(EFirstStr.Text
    + GetString(EStartNO.Value + i, ENOBitNumber.Value)
    + ELastStr.Text);
  StatusBar1.Panels[1].Text:= IntToStr(ListRecords.Items.Count);
  PageCtrl.ActivePageIndex:= 1;
  EStartNO.Value:= EStartNO.Value + ENONumber.Value;
end;

function TFFlyLabelPrint.GetString(Value: integer; Number: Word): String;
var
  S: string;
begin    //格式化数值
  if EFillChar.Text = '' then EFillChar.Text:= '0';
  if rbFront.Checked then begin        //前补位
    S:= StringOfChar(EFillChar.Text[1], Number) + IntToStr(Value);
    Delete(S, 1, (Length(S) - Number) div 2);
    Result:= Copy(S, Length(S) - Number + 1, Number);
  end else if rbLast.Checked then begin
    S:= IntToStr(Value) + StringOfChar(EFillChar.Text[1], Number);
    Result:= Copy(S, 1, Number);
  end else Result:= IntToStr(Value);
end;

procedure TFFlyLabelPrint.Panel1CanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  ResetColumn(NewWidth);
end;

procedure TFFlyLabelPrint.ResetColumn(NewWidth: Integer);
begin
  ListRecords.Columns:= (NewWidth - 280) div (ENOBitNumber.Value * 10);
end;

procedure TFFlyLabelPrint.ENOBitNumberChange(Sender: TObject);
begin
  ResetColumn(Panel1.Width);
end;

procedure TFFlyLabelPrint.ERecordKeyPress(Sender: TObject; var Key: Char);
begin
  if(Pos(Key, '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') > 0)and(chkSayKey.Checked)then
    Say(Key);
end;

procedure TFFlyLabelPrint.BtnAddRecordClick(Sender: TObject);
begin     //添加一条标签
  ListRecords.Items.Add(ERecord.Text);
  StatusBar1.Panels[1].Text:= IntToStr(ListRecords.Items.Count);
  WriteLog(LOG_INFO, 'Add', ERecord.Text);
  if chkSayCode.Checked then Say(ERecord.Text);
  ERecord.Text:= Copy(ERecord.Text, 1, ENoRemove.Value);
  ERecord.SetFocus;
  SendMessage(ERecord.Handle, EM_SETSEL, ENoRemove.Value, ENoRemove.Value);
end;

procedure TFFlyLabelPrint.BtnDeleteRecordClick(Sender: TObject);
begin     //删除记录
  ListRecords.DeleteSelected;
end;

procedure TFFlyLabelPrint.BtnClearRecordsClick(Sender: TObject);
begin     //清除记录
  ListRecords.Items.Clear;
end;

procedure TFFlyLabelPrint.LoadDataFromQuery(DQLabel: TADOQuery);
var
  I: Integer;
begin
  with DQLabel, LabelImage do begin
    CurrentLabel:= FieldByName('ID').AsInteger;
    StatusBar1.Panels[5].Text:= FieldByName('LabelName').AsString;
    (* 读取图片信息 *)
    Pixel:= FieldByName('Pixel').AsInteger;
    FPixel:= Pixel;
    OrgBmp.Assign(FieldByName('Picture'));
    (* 其他信息 *)
    Content.Text:= FieldByName('Content').AsString;
    lstValues.Strings.Clear;
    LabelValues.Clear;
    for I := 0 to Content.SectionCount - 1 do
      if not Content.ReadBool(Content.Sections[I], 'SerialNo', False) then begin
        lstValues.InsertRow(Content.Sections[I], Content.ReadString(Content.Sections[I], 'ExampleText', ''), True);
        LabelValues.Add(Content.Sections[I], Content.ReadString(Content.Sections[I], 'ExampleText', ''));
      end else if Content.ReadInteger(Content.Sections[I], 'KIND', 1) = 0 then
        ENOBitNumber.Value:= Content.ReadInteger(Content.Sections[I], 'LEN', 6);
    DrewBaseBmp;
    Drew(ImageEdit.Picture.Bitmap, EPreviewText.Text);
    (* 初始化标签尺寸 *)
    FLabelWidth:= FieldByName('LabelWidth').AsInteger;
    FLabelHeight:= FieldByName('LabelHeight').AsInteger;
    ImagePreview.Picture.Bitmap.Width:= FLabelWidth;
    ImagePreview.Picture.Bitmap.Height:= FLabelHeight;
  end;
end;

procedure TFFlyLabelPrint.BtnPreviewClick(Sender: TObject);
var
  i, szValue: Integer;
begin
  LabelImage.LabelValues.Clear;
  for I := 1 to lstValues.RowCount - 1 do
    LabelImage.LabelValues.Add(lstValues.Cells[0, I], lstValues.Cells[1, I]);
  LabelImage.DrewBaseBmp;
  LabelImage.Drew(ImageEdit.Picture.Bitmap, EPreviewText.Text);
  LabelImage.Drew(ImagePreview.Picture.Bitmap, EPreviewText.Text);
  if tryStrToInt(EPreviewText.Text, szValue) then
    EPreviewText.Text:= Format('%.' + IntToStr(ENOBitNumber.Value) + 'd', [szValue + 1]);
end;

procedure TFFlyLabelPrint.BtnPrinterSetupClick(Sender: TObject);
begin
  PrinterSetupDialog1.Execute;
end;

(* 打印期间回调函数 *)

procedure TFFlyLabelPrint.PrintFinally(Sender: TObject);
begin     //打印进程结束回调函数
  Gauge1.Visible:= False;
  ListRecords.Visible:= true;
end;

procedure TFFlyLabelPrint.EndOfLabel(Sender: TObject; LabelID: Integer);
var
  ProcessID: integer;
begin     //打印进程单标签打印结束回调函数
  Gauge1.Progress:= LabelID;
//  FFlyLabelDB.AddTask(FCurrentLabel, 1, 'PRINT', ListRecords.Items[LabelID - 1]);
end;

procedure TFFlyLabelPrint.EndOfPage(Sender: TObject; PageID: Integer);
begin     //单页打印结束回调函数
  EPageCount.Caption:= IntToStr(PageID);
  EPageCount.Repaint;
  if Assigned(Sender) then begin
    if TPrinter(Sender).PageNumber > EPageOfDoc.Value then begin
      TPrinter(Sender).EndDoc;
      TPrinter(Sender).BeginDoc;
    end else TPrinter(Sender).NewPage;
  end;
//  Memo1.Lines.Add('NewPage');
//  GetNextPrint;
//  Printer.BeginDoc;
//  Memo1.Lines.Add('BeginDoc');
end;

(* 按钮事件 *)

procedure TFFlyLabelPrint.BtnPrintClick(Sender: TObject);
var
  i, pageIdx: Integer;
  TempPrint: TLabelPrint;
  hasLabel: Boolean;
begin     //打印
  LabelImage.LabelValues.Clear;
  for I := 1 to lstValues.RowCount - 1 do              //填写用户设定值内容
    LabelImage.LabelValues.Add(lstValues.Cells[0, I], lstValues.Cells[1, I]);
  LabelImage.DrewBaseBmp;                              //画图案背景
  TempPrint:= TLabelPrint.Create();                    //创建打印控件
  SetPrintSettings(TempPrint);                         //初始化打印控件信息
  //重新分配打印序列
  Memo1.Lines.Add('即将打印标签...' + IntToStr(ListRecords.Items.Count));
  Memo1.Lines.Add(format('从%s-%s，%d个', [ListRecords.Items[0],
    ListRecords.Items[ListRecords.Count - 1], ListRecords.Count]));
  {$region '开始打印'}
  I:= 0; pageIdx:= 1;
  if Printer.Printing then
    Printer.EndDoc;
  while I < ListRecords.Items.Count do begin
    Printer.Title:= '标签' + ListRecords.Items[0];
    if I = 0 then Printer.BeginDoc
    else Printer.NewPage;
    TempPrint.Items.Clear;
    while(I < ListRecords.Items.Count)and(I < pageCount * pageIdx) do begin
      TempPrint.Items.Add(ListRecords.Items[I]);
      Inc(I);
    end;
    with Printer do begin
      Memo1.Lines.Add(Format('打印第%d页(共%d个): %d x %d',
        [pageIdx, TempPrint.Items.Count, PageWidth, PageHeight]));
      TempPrint.Print(Printer);                                                 //调用打印标签函数，开始打印标签
    end;
    Inc(pageIdx);
  end;
  if Printer.Printing then
    Printer.EndDoc;
  {$endregion}
  TempPrint.Free;
  PageCtrl.ActivePageIndex:= 1;
  if btnLoop.Down then
    for I := 0 to FPrinters.Count - 1 do                   //循环打印机
      if lstPrinter.Checked[I] then begin
         lstPrinter.Checked[I]:= False;
         if i = FPrinters.Count - 1 then
           lstPrinter.Checked[0]:= True
         else lstPrinter.Checked[i + 1]:= True;
         Exit;
      end;
end;

procedure TFFlyLabelPrint.BtnTheatPrintClick(Sender: TObject);
var
  i, pageIdx: Integer;
  TempPrint: TThreadPrint;
  hasLabel: Boolean;
begin     //打印
  FCurrentPrintIDX:= 0;
  //打印前准备，LabelImage，绘制背景图，初始化信息
  LabelImage.LabelValues.Clear;
  for I := 1 to lstValues.RowCount - 1 do              //填写用户设定值内容
    LabelImage.LabelValues.Add(lstValues.Cells[0, I], lstValues.Cells[1, I]);
  LabelImage.DrewBaseBmp;                              //画图案背景
  TempPrint:= TThreadPrint.Create;                     //创建打印控件
  {$region '初始化打印标签类'}
  with TempPrint do begin
    BaseBmp:= LabelImage.FBaseBmp;     //调用打印进程
    Params:= LabelImage.FParams;
    LabelValues:= LabelImage.LabelValues;
    Pixel:= FPixel;
    pageCount:= EColCount.Value * ESectionCount.Value * ERowCount.Value;        //每页打印的总数，150
    RowCount:= ERowCount.Value;
    ColCount:= EColCount.Value;
    LabelWidth:= FLabelWidth;
    LabelHeight:= FLabelHeight;
    SectionCount:= ESectionCount.Value;
    LeftMargin:= ELeftMargin.Value;
    TopMargin:= ETopMargin.Value;
    RowSpace:= ERowSpace.Value;
    ColSpace:= EColSpace.Value;
  end;
  {$endregion}
  //重新分配打印序列
  Memo1.Lines.Add(' 即将打印 ... ...' + IntToStr(ListRecords.Items.Count));
  {$region '开始打印'}
  if Printer.Printing then Printer.EndDoc;
  TempPrint.Items:= ListRecords.Items;
  TempPrint.Start;
//  while I < ListRecords.Items.Count do begin
//    TempPrint.Items.Clear;
//    while(I < ListRecords.Items.Count)and(I < pageCount * pageIdx) do begin
//      TempPrint.Items.Add(ListRecords.Items[I]);
//      Inc(I);
//    end;
//    with Printer do begin
//      Memo1.Lines.Add(Format('打印第%d页(共%d个): %d x %d',
//        [pageIdx, TempPrint.Items.Count, PageWidth, PageHeight]));
//      TempPrint.Print(Printer);                                                 //调用打印标签函数，开始打印标签
//    end;
//    if I < ListRecords.Items.Count then Printer.NewPage;
//    Inc(pageIdx);
//  end;
//  if Printer.Printing then Printer.EndDoc;
  {$endregion}
//  TempPrint.Free;
  PageCtrl.ActivePageIndex:= 1;
  if btnLoop.Down then
    for I := 0 to FPrinters.Count - 1 do                   //循环打印机
      if lstPrinter.Checked[I] then begin
         lstPrinter.Checked[I]:= False;
         if i = FPrinters.Count - 1 then
           lstPrinter.Checked[0]:= True
         else lstPrinter.Checked[i + 1]:= True;
         Exit;
      end;
end;

procedure TFFlyLabelPrint.BtnGroupPrintClick(Sender: TObject);
var
  i, j, startIdx, endIdx, newStart: Integer;
  TempPrint: TLabelPrint;
  hasLabel: Boolean;
begin     //打印
  FCurrentPrintIDX:= 0;
  LabelImage.LabelValues.Clear;
  for I := 1 to lstValues.RowCount - 1 do              //填写用户设定值内容
    LabelImage.LabelValues.Add(lstValues.Cells[0, I], lstValues.Cells[1, I]);
  LabelImage.DrewBaseBmp;                              //画图案背景
  TempPrint:= TLabelPrint.Create();                    //创建打印控件
  SetPrintSettings(TempPrint);                         //初始化打印控件信息
  PanelPages.Visible:= true;                           //
  ListRecords.Visible:= False;
  hasLabel:= True;
  while hasLabel do begin
    hasLabel:= False;
    for I := 0 to FPrinters.Count - 1 do begin           //循环打印机
      startIdx:= StrToInt(FPrinters.Names[I]);
      endIdx:= StrToInt(FPrinters.ValueFromIndex[I]);
      newStart:= Min(startIdx + PageCount, endIdx);
      if startIdx < endIdx then begin                    //如果尚未打印结束，继续打印
        for J := startIdx to newStart - 1 do
          TempPrint.Items.Add(ListRecords.Items[J]);
        FPrinters[I]:= IntToStr(newStart) + '=' + IntToStr(endIdx);
        if newStart < endIdx then hasLabel:= True;
        with TPrinter(FPrinters.Objects[I]) do begin
          if Printing then EndDoc;
          Title:= Format('BeginFrom%s', [TempPrint.Items[0]]);
          Memo1.Lines.Add(Format('Page:%d x %d', [PageWidth, PageHeight]));
          BeginDoc;
//          NewPage;
//          Title:= 'AAA';
//          NewPage;
//          Title:= 'NewPage';
          TempPrint.Print(TPrinter(FPrinters.Objects[I]));
          if Printing then EndDoc;
        end;
      end;
    end;
  end;
  ClosePrinters;
  TempPrint.Free;
  PageCtrl.ActivePageIndex:= 1;
  if btnLoop.Down then
    for I := 0 to FPrinters.Count - 1 do                   //循环打印机
      if lstPrinter.Checked[I] then begin
         lstPrinter.Checked[I]:= False;
         if i = FPrinters.Count - 1 then
           lstPrinter.Checked[0]:= True
         else lstPrinter.Checked[i + 1]:= True;
         Exit;
      end;
end;

procedure TFFlyLabelPrint.BtnSaveBMPClick(Sender: TObject);
var
  TempPrint: TLabelPrint;
  I: Integer;
begin     //图打
  if not OpenPictureDialog1.Execute then Exit;
  TempPrint:= TLabelPrint.Create();
  SetPrintSettings(TempPrint);
  for I := 0 to Min(ListRecords.Count, 10) - 1 do
    TempPrint.Items.Add(ListRecords.Items[I]);
  TempPrint.SaveFile(OpenPictureDialog1.FileName);
  BtnClearRecordsClick(self);
  PageCtrl.ActivePageIndex:= 0;
end;

procedure TFFlyLabelPrint.BtnPrinterPreviewClick(Sender: TObject);
var
  TempPrint: TLabelPrint;
begin     //打印预览
  TempPrint:= TLabelPrint.Create();
  SetPrintSettings(TempPrint);
  with TempPrint do
//    with TFFlyLabelPreview.Create(self) do try
//      Preview(Image1.Canvas);
//      ShowModal();
//    finally
//      TempPrint.Free;
//      Free;
//    end;
end;

procedure TFFlyLabelPrint.OpenParam(ParamKind, ParamName: string);
begin
  with FFlyLabelDB.DQParam do begin
    Close;
    Parameters[0].Value:= ParamKind;
    Parameters[1].Value:= ParamName;
    Open;
  end;
end;

procedure TFFlyLabelPrint.BtnLoadSchemeClick(Sender: TObject);
  function GetParamValue(ParamName: string; DefaultValue: Integer = 0): Integer;
  begin
    Result:= DefaultValue;
    OpenParam(EScheme.Text, ParamName);
    if FFlyLabelDB.DQParam.RecordCount > 0 then
      Result:= FFlyLabelDB.DQParam.FieldByName('ParamValue').AsInteger;
  end;
begin     //提取打印参数
  if ESchemeList.ItemIndex < 0 then Exit;
  EScheme.Text:= ESchemeList.Items[ESchemeList.ItemIndex];
  EColCount.Value:= GetParamValue('ColCount');
  ESectionCount.Value:= GetParamValue('SectionCount');
  ERowCount.Value:= GetParamValue('RowCount');
  ELeftMargin.Value:= GetParamValue('LeftMargin');
  ETopMargin.Value:= GetParamValue('TopMargin');
  ERowSpace.Value:= GetParamValue('RowSpace');
  EColSpace.Value:= GetParamValue('ColSpace');    
  ESequencHeight.Value:= GetParamValue('SequencHeight', 100);
  EFlagWidth.Value:= GetParamValue('FlagWidth', 20);
  EFlagKind.ItemIndex:= GetParamValue('FlagKind', 0);
  EPointOffset.Value:= GetParamValue('PointOffset', 50);
  FFlyLabelDB.DQParam.Close;
  PageCtrl.ActivePage:= SheetSettings;
end;

procedure TFFlyLabelPrint.BtnSaveSchemeClick(Sender: TObject);
var
  ParamKind: string;
  procedure SaveParam(ParamName: string; ParamValue: Integer);
  begin
    OpenParam(ParamKind, ParamName);
    with FFlyLabelDB.DQParam do begin
      if RecordCount < 1 then Append else Edit;
      FieldByName('ParamKind').AsString:= EScheme.Text;
      FieldByName('ParamName').AsString:= ParamName;
      FieldByName('ParamValue').AsInteger:= ParamValue;
      Post;
    end;
  end;
begin     //存储打印参数
  ParamKind:= Trim(EScheme.Text);
  if ParamKind = '' then raise Exception.Create('方案名称不能为空。');
  SaveParam('ColCount', EColCount.Value);
  SaveParam('SectionCount', ESectionCount.Value);
  SaveParam('RowCount', ERowCount.Value);
  SaveParam('LeftMargin', ELeftMargin.Value);
  SaveParam('TopMargin', ETopMargin.Value);
  SaveParam('RowSpace', ERowSpace.Value);
  SaveParam('ColSpace', EColSpace.Value);
  SaveParam('SequencHeight', ESequencHeight.Value);
  SaveParam('FlagWidth', EFlagWidth.Value);
  SaveParam('FlagKind', EFlagKind.ItemIndex);
  SaveParam('PointOffset', EPointOffset.Value);
  if ESchemeList.Items.IndexOf(ParamKind) < 0 then
    ESchemeList.Items.Add(ParamKind);
  FFlyLabelDB.DQParam.Close;
end;

procedure TFFlyLabelPrint.SetPrintSettings(LabelPrint: TLabelPrint);
var
  i, j, k, prnItemCount, maxID: Integer;
  prn: TPrinter;
begin     //设定图标输出类基础参数
  with LabelPrint do begin
    LabelImage:= Self.LabelImage;     //调用打印进程
    Pixel:= FPixel;
    OnFinally:= PrintFinally;
    OnEndOfLabel:= EndOfLabel;
    OnEndOfPage:= EndOfPage;
    pageCount:= EColCount.Value * ESectionCount.Value * ERowCount.Value;  //每页打印的总数，150
    //重新分配打印序列
    ClosePrinters;
    if ListRecords.Items.Count = 0 then Exit;
    repeat                                                  //Prepaired list of printers
      prn:= GetPrinter;                                     //Get a printer
      if Assigned(prn) then begin
        FPrinters.AddObject(lstPrinter.Items[FCurrentPrintIDX - 1], prn);
        //prn.BeginDoc;
      end;
    until not Assigned(prn) or (FPrinters.Count >= ListRecords.Items.Count / PageCount);
    if FPrinters.Count = 0 then raise Exception.Create('尚未指定打印机，系统无法打印。');
    prnItemCount:= ListRecords.Items.Count div (pageCount * FPrinters.Count);
    for i:= 0 to FPrinters.Count - 2 do begin
      Memo1.Lines.Add(FPrinters[i] + ' 即将打印 ' + IntToStr(i * prnItemCount * pageCount) + ' - ' +
        IntToStr((i + 1) * prnItemCount * pageCount));
      FPrinters[i]:= IntToStr(i * prnItemCount * pageCount) + '=' +
        IntToStr((i + 1) * prnItemCount * pageCount);
    end;
    Memo1.Lines.Add(FPrinters[FPrinters.Count - 1] + ' 即将打印 ' + IntToStr((FPrinters.Count - 1) * prnItemCount * pageCount) + ' - ' +
        IntToStr(ListRecords.Items.Count));
    FPrinters[FPrinters.Count - 1]:= IntToStr((FPrinters.Count - 1) * prnItemCount * pageCount) + '=' +
        IntToStr(ListRecords.Items.Count);
    RowCount:= ERowCount.Value;
    ColCount:= EColCount.Value;
    LabelWidth:= FLabelWidth;
    LabelHeight:= FLabelHeight;
    SectionCount:= ESectionCount.Value;
    LeftMargin:= ELeftMargin.Value;
    TopMargin:= ETopMargin.Value;
    RowSpace:= ERowSpace.Value;
    ColSpace:= EColSpace.Value;
    Pixel:= LabelPrint.Pixel;
    //LabelImage.Pixel:= 7;
    FlagDistance:= ESequencHeight.Value;
    FlagKind:= EFlagKind.ItemIndex;
    FlagLong:= EFlagWidth.Value;
    PointOffset:= EPointOffset.Value;
    //Gauge1.MaxValue:= Items.Count;
    Gauge1.MaxValue:= RowCount * ColCount * SectionCount;
    Gauge1.Visible:= true;
    EPageCount.Caption:= '0';
  end;
end;

procedure TFFlyLabelPrint.BtnImportClick(Sender: TObject);
begin     //导入
  with OpenDialog1 do
    if Execute then
      ListRecords.Items.LoadFromFile(Filename);
end;

procedure TFFlyLabelPrint.BtnExportClick(Sender: TObject);
begin     //导出
  with SaveDialog1 do
    if Execute then
      ListRecords.Items.SaveToFile(Filename);
end;

procedure TFFlyLabelPrint.btnSelectedAllClick(Sender: TObject);
var
  i: Integer;
begin
  for i:= 0 to lstPrinter.Items.Count - 1 do
    lstPrinter.Checked[i]:= True;
end;

procedure TFFlyLabelPrint.btnSelectedNoneClick(Sender: TObject);
var
  i: Integer;
begin
  for i:= 0 to lstPrinter.Items.Count - 1 do
    lstPrinter.Checked[i]:= False;
end;

procedure TFFlyLabelPrint.ClosePrinters;
var
  i: Integer;
begin     //关闭所有打印机
  for I := 0 to FPrinters.Count - 1 do
    if Assigned(FPrinters.Objects[I]) then begin
      if TPrinter(FPrinters.Objects[I]).Printing then
        TPrinter(FPrinters.Objects[I]).EndDoc;
      TPrinter(FPrinters.Objects[I]).Free;
    end;
  FPrinters.Clear;
  FCurrentPrintIdx:= 0;
end;

procedure TFFlyLabelPrint.GetNextPrint;
var
  i: Integer;
  mdevice : array[0..255] of char;
  mdriver : array[0..255] of char;
  mport : array[0..255] of char;
  mhdmode : thandle;
  mpdmode : pdevmode;
begin
  i:= FCurrentPrintIDX;
  repeat
    if lstPrinter.Checked[i] then begin
      printer.getprinter(mdevice, mdriver, mport, mhdmode);
      printer.setprinter(PChar(lstPrinter.Items[i]), mdriver, mport, mhdmode); //设置打印机
      Memo1.Lines.Add(lstPrinter.Items[i] + ' ' + IntToStr(mhdMode));
      FCurrentPrintIDX:= i + 1;
      if FCurrentPrintIDX >= lstPrinter.Count then FCurrentPrintIDX:= 0;
      Exit;
    end;
    Inc(i);
    if i >= lstPrinter.Count then i:= 0;
  until i = FCurrentPrintIDX;
  raise Exception.Create('机组内未指定打印机。');

  //  Memo1.Lines.Add(lstPrinter.Items[FCurrentPrintIDX]);
  for i:= FCurrentPrintIDX to lstPrinter.Count - 1 do
    if lstPrinter.Checked[i] then begin
      printer.getprinter(mdevice, mdriver, mport, mhdmode);
      printer.setprinter(PChar(lstPrinter.Items[i]), mdriver, mport, mhdmode); //设置打印机
      FCurrentPrintIDX:= i;
      Break;
    end;
  Inc(FCurrentPrintIDX);
  if FCurrentPrintIDX >= lstPrinter.Count then FCurrentPrintIDX:= 0;
end;

function TFFlyLabelPrint.GetPrinter: TPrinter;
var
  mdevice : array[0..255] of char;
  mdriver : array[0..255] of char;
  mport : array[0..255] of char;
  mhdmode : thandle;
  mpdmode : pdevmode;
begin    //得到一个选中的打印机对象
  Result:= nil;
  while FCurrentPrintIdx < lstPrinter.Count do begin
    if lstPrinter.Checked[FCurrentPrintIDX] then begin
      Result := TPrinter.Create;
      Result.getprinter(mdevice, mdriver, mport, mhdmode);
      Result.setprinter(PChar(lstPrinter.Items[FCurrentPrintIDX]), mdriver, mport, mhdmode); //设置打印机
      Memo1.Lines.Add(lstPrinter.Items[FCurrentPrintIDX] + ' 即将参与打印 ' + IntToStr(mhdMode));
      Inc(FCurrentPrintIDX);
      Exit;
    end;
    Inc(FCurrentPrintIDX);
  end;
end;

procedure TFFlyLabelPrint.btnSchemeDeleteClick(Sender: TObject);
begin     //提取打印参数
  if ESchemeList.ItemIndex < 0 then Exit;
  if MessageDlg('真的要删除这个打印方案吗?', mtInformation,
    [mbOK, mbCancel], 0) <> mrOK then Exit;
  with FFlyLabelDB.DCDelScheme do begin
    Parameters[0].Value:= ESchemeList.Items[ESchemeList.ItemIndex];
    Execute;
  end;
end;

procedure TFFlyLabelPrint.lstPrinterClick(Sender: TObject);
begin
  lstPrinter.Hint:= lstPrinter.Items[lstPrinter.ItemIndex];
end;

procedure TFFlyLabelPrint.lstValuesGetPickList(Sender: TObject;
  const KeyName: string; Values: TStrings);
begin
  Values.Delimiter:= ',';
  Values.DelimitedText:= LabelImage.Content.ReadString(KeyName, 'Items', '');
end;

procedure TFFlyLabelPrint.SetCurrentLabel(const Value: Integer);
begin
  FCurrentLabel := Value;
end;

procedure TFFlyLabelPrint.btnRestartSvcClick(Sender: TObject);
var
  H: HINST;
begin
  if SysUtils.FileExists(ExtractFilePath(Application.ExeName) + 'RestartSpooler.bat') then
    ShellExecute(0, 'open', 'RestartSpooler.bat', '', '', SW_SHOW)
  else begin
    ExcuteWait('NET', 'STOP spooler');
    ExcuteWait('NET', 'START spooler');
  end;
end;

procedure TFFlyLabelPrint.btnImportCardClick(Sender: TObject);
var
  i, cnt: Integer;
begin
  cnt:= ECardCount.Value;
  if(cnt = 0)or(cnt > lstCard.Count)then
    cnt:= lstCard.Count;
  for i:= 0 to cnt - 1 do
    ListRecords.Items.Add(lstCard.Items[i]);
end;

procedure TFFlyLabelPrint.SetComMID(const Value: Integer);
begin
  FComMID := Value;
end;

procedure TFFlyLabelPrint.BtnDeleteCardClick(Sender: TObject);
begin
  if lstCard.ItemIndex < 0 then Exit;
  if MessageDlg('是否确定要删除' + lstCard.Items[lstCard.ItemIndex] + '这个补录标签吗?',
    mtWarning, mbOKCancel, 0) <> mrOK then Exit;
//  FFlyLabelDB.AddTask(FCurrentLabel, 4, lbCardNo.Caption, lstCard.Items[lstCard.ItemIndex]);
  lstCard.Items.Delete(lstCard.ItemIndex);
end;

end.
