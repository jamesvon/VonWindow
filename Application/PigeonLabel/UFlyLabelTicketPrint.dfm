object FFlyLabelTicketPrint: TFFlyLabelTicketPrint
  Left = 0
  Top = 0
  Caption = 'FFlyLabelTicketPrint'
  ClientHeight = 525
  ClientWidth = 951
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 312
    Width = 951
    Height = 194
    ActivePage = TabSheet1
    Align = alBottom
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #25171#21360
      object Panel2: TPanel
        Left = 744
        Top = 0
        Width = 199
        Height = 166
        Align = alRight
        Caption = 'Panel2'
        ShowCaption = False
        TabOrder = 0
        object Label1: TLabel
          Left = 6
          Top = 5
          Width = 24
          Height = 13
          Caption = #24180#20221
        end
        object Label5: TLabel
          Left = 6
          Top = 53
          Width = 24
          Height = 13
          Caption = #21306#22495
        end
        object BtnPrinterSetup: TBitBtn
          Left = 110
          Top = 8
          Width = 75
          Height = 25
          Caption = #25171#21360#26426
          DoubleBuffered = True
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000C40E0000C40E00000000000000000000FF00FF000000
            0000000000000000000000000000000000000000000000000000000000000000
            00000000000000FF00FFFF00FF80808080808080808080808080808080808080
            8080808080808080808080808080808080808080808080FF00FF000000BFBFBF
            BFBFBFBFBFBF003F7F003F7FBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
            BFBFBFBFBFBFBF000000808080BFBFBFBFBFBFBFBFBF408080408080BFBFBFBF
            BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF808080000000BFBFBF
            BFBFBF007F7F007F7F003F7FBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
            BFBFBFBFBFBFBF000000808080BFBFBFBFBFBFFFFF80FFFF80408080BFBFBFBF
            BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF808080000000007F7F
            003F7F003F7F007F7F007F7F003F7F003F7F0000000000000000000000000000
            00000000000000000000808080FFFF80408080408080FFFF80FFFF8040808040
            8080808080808080808080808080808080808080808080808080000000007F7F
            007F7F007F7F007F7F007F7F007F7F003F7FBFBFBFFFFFFFBFBFBFFFFFFFBFBF
            BFFFFFFFBFBFBF000000808080FFFF80FFFF80FFFF80FFFF80FFFF80FFFF8040
            8080BFBFBFFFFFFFBFBFBFFFFFFFBFBFBFFFFFFFBFBFBF808080000000BFBFBF
            007F7F007F7F007F7F007F7F007F7F003F7FFFFFFFBFBFBFFFFFFFBFBFBFFFFF
            FF0000FFFFFFFF000000808080BFBFBFFFFF80FFFF80FFFF80FFFF80FFFF8040
            8080FFFFFFBFBFBFFFFFFFBFBFBFFFFFFF0000FFFFFFFF808080000000FFFFFF
            BFBFBFFFFFFFBFBFBF007F7F007F7F007F7F003F7FFFFFFFBFBFBFFFFFFFBFBF
            BFFFFFFFBFBFBF000000808080FFFFFFBFBFBFFFFFFFBFBFBFFFFF80FFFF80FF
            FF80408080FFFFFFBFBFBFFFFFFFBFBFBFFFFFFFBFBFBF808080000000000000
            000000000000000000000000000000007F7F007F7F003F7F0000000000000000
            00000000000000000000808080808080808080808080808080808080808080FF
            FF80FFFF80408080808080808080808080808080808080808080FF00FFFF00FF
            FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFF007F7F007F7F003F7F003F7F003F
            7FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF808080FFFFFFFFFFFFFFFFFFFF
            FFFFFFFF80FFFF80408080408080408080FF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FF000000FFFFFF000000000000000000000000007F7F007F7F007F7F003F
            7F003F7F003F7FFF00FFFF00FFFF00FFFF00FF808080FFFFFF80808080808080
            8080808080FFFF80FFFF80FFFF80408080408080408080FF00FFFF00FFFF00FF
            FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF007F7F007F7F007F
            7F007F7F003F7F003F7FFF00FFFF00FFFF00FF808080FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF80FFFF80FFFF80FFFF80408080408080FF00FFFF00FF
            FF00FF000000FFFFFF000000000000FFFFFF000000000000007F7F007F7F007F
            7F007F7F007F7F003F7FFF00FFFF00FFFF00FF808080FFFFFF808080808080FF
            FFFF808080808080FFFF80FFFF80FFFF80FFFF80FFFF80408080FF00FFFF00FF
            FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF007F7F007F
            7F003F7F007F7FFF00FFFF00FFFF00FFFF00FF808080FFFFFFFFFFFFFFFFFFFF
            FFFF808080FFFFFFFFFFFFFFFF80FFFF80408080FFFF80FF00FFFF00FFFF00FF
            FF00FF000000FFFFFF000000BFBFBFFFFFFF000000FFFFFF000000007F7F007F
            7F003F7F003F7FFF00FFFF00FFFF00FFFF00FF808080FFFFFF808080BFBFBFFF
            FFFF808080FFFFFF808080FFFF80FFFF80408080408080FF00FFFF00FFFF00FF
            FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000FF00FFFF00FF007F
            7F007F7FFF00FFFF00FFFF00FFFF00FFFF00FF808080FFFFFFFFFFFFFFFFFFFF
            FFFF808080808080FF00FFFF00FFFFFF80FFFF80FF00FFFF00FFFF00FFFF00FF
            FF00FF000000000000000000000000000000000000FF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF80808080808080808080808080
            8080808080FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
          NumGlyphs = 2
          ParentDoubleBuffered = False
          TabOrder = 0
          OnClick = BtnPrinterSetupClick
        end
        object BtnPrint: TBitBtn
          Left = 110
          Top = 38
          Width = 75
          Height = 25
          Caption = #25171#21360
          DoubleBuffered = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
            0003377777777777777308888888888888807F33333333333337088888888888
            88807FFFFFFFFFFFFFF7000000000000000077777777777777770F8F8F8F8F8F
            8F807F333333333333F708F8F8F8F8F8F9F07F333333333337370F8F8F8F8F8F
            8F807FFFFFFFFFFFFFF7000000000000000077777777777777773330FFFFFFFF
            03333337F3FFFF3F7F333330F0000F0F03333337F77773737F333330FFFFFFFF
            03333337F3FF3FFF7F333330F00F000003333337F773777773333330FFFF0FF0
            33333337F3F37F3733333330F08F0F0333333337F7337F7333333330FFFF0033
            33333337FFFF7733333333300000033333333337777773333333}
          NumGlyphs = 2
          ParentDoubleBuffered = False
          TabOrder = 1
          OnClick = BtnPrintClick
        end
        object EYear: TSpinEdit
          Left = 6
          Top = 24
          Width = 75
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 2
          Value = 2015
        end
        object EArea: TSpinEdit
          Left = 6
          Top = 72
          Width = 75
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 3
          Value = 0
        end
        object Button5: TButton
          Left = 110
          Top = 85
          Width = 75
          Height = 25
          Caption = #28165#38500
          TabOrder = 4
          OnClick = Button5Click
        end
        object Button6: TButton
          Left = 110
          Top = 116
          Width = 75
          Height = 25
          Caption = #21024#38500
          TabOrder = 5
          OnClick = Button6Click
        end
        object ECode: TLabeledEdit
          Left = 6
          Top = 112
          Width = 75
          Height = 21
          EditLabel.Width = 24
          EditLabel.Height = 13
          EditLabel.Caption = #34917#21495
          TabOrder = 6
          OnKeyPress = ECodeKeyPress
        end
      end
      object ListRecords: TListBox
        Left = 0
        Top = 0
        Width = 744
        Height = 166
        Align = alClient
        Columns = 1
        ItemHeight = 13
        TabOrder = 1
      end
    end
    object TabSheet2: TTabSheet
      Caption = #35774#35745
      ImageIndex = 1
      object ListBox1: TListBox
        Left = 121
        Top = 0
        Width = 192
        Height = 166
        Align = alClient
        ItemHeight = 13
        TabOrder = 0
        OnClick = ListBox1Click
      end
      object Panel1: TPanel
        Left = 313
        Top = 0
        Width = 481
        Height = 166
        Align = alRight
        BevelOuter = bvNone
        Caption = 'Panel1'
        ShowCaption = False
        TabOrder = 1
        DesignSize = (
          481
          166)
        object Label3: TLabel
          Left = 245
          Top = 35
          Width = 48
          Height = 13
          Caption = #19978#36793#20301#32622
        end
        object Label2: TLabel
          Left = 365
          Top = 35
          Width = 48
          Height = 13
          Caption = #24038#36793#20301#32622
        end
        object Label6: TLabel
          Left = 257
          Top = 63
          Width = 36
          Height = 13
          Caption = #23383#22823#23567
        end
        object Label13: TLabel
          Left = 377
          Top = 63
          Width = 36
          Height = 13
          Caption = #23383#38388#36317
        end
        object Label15: TLabel
          Left = 363
          Top = 115
          Width = 48
          Height = 13
          Caption = #26174#31034#23545#40784
        end
        object Label14: TLabel
          Left = 244
          Top = 3
          Width = 24
          Height = 13
          Caption = #26059#36716
        end
        object Label29: TLabel
          Left = 244
          Top = 115
          Width = 36
          Height = 13
          Caption = #40664#35748#20540
        end
        object Label16: TLabel
          Left = 361
          Top = 3
          Width = 24
          Height = 13
          Caption = #38271#24230
        end
        object Label51: TLabel
          Left = 364
          Top = 140
          Width = 48
          Height = 13
          Caption = #27178#21521#27604#20363
        end
        object Label36: TLabel
          Left = 244
          Top = 140
          Width = 48
          Height = 13
          Caption = #32437#21521#27604#20363
        end
        object EContentName: TLabeledEdit
          Left = 5
          Top = 18
          Width = 189
          Height = 21
          EditLabel.Width = 24
          EditLabel.Height = 13
          EditLabel.Caption = #39033#30446
          TabOrder = 0
        end
        object RadioGroup1: TRadioGroup
          Left = 5
          Top = 45
          Width = 233
          Height = 41
          Caption = #32534#30721
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            #38750#32534#30721
            #32534#30721#19968
            #32534#30721#20108)
          TabOrder = 1
        end
        object ETextTop: TSpinEdit
          Left = 299
          Top = 30
          Width = 50
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 2
          Value = 0
          OnChange = ETextTopChange
        end
        object ETextLeft: TSpinEdit
          Left = 421
          Top = 30
          Width = 48
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 3
          Value = 216
          OnChange = ETextTopChange
        end
        object ETextFontSize: TSpinEdit
          Left = 299
          Top = 60
          Width = 50
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 4
          Value = 68
          OnChange = ETextTopChange
        end
        object chkTextBold: TCheckBox
          Left = 250
          Top = 88
          Width = 43
          Height = 17
          Caption = #31895#20307
          TabOrder = 5
          OnClick = ETextTopChange
        end
        object chkTextUnderline: TCheckBox
          Left = 354
          Top = 88
          Width = 61
          Height = 17
          Caption = #19979#21010#32447
          TabOrder = 6
          OnClick = ETextTopChange
        end
        object ETextWordWidth: TSpinEdit
          Left = 421
          Top = 60
          Width = 48
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 7
          Value = 68
          OnChange = ETextTopChange
        end
        object ETextAlign: TComboBox
          Left = 417
          Top = 111
          Width = 62
          Height = 22
          Style = csOwnerDrawFixed
          ItemIndex = 0
          TabOrder = 8
          Text = #24038#23545#40784
          Items.Strings = (
            #24038#23545#40784
            #23621#20013
            #21491#23545#40784)
        end
        object chkTextStrikeOut: TCheckBox
          Left = 422
          Top = 88
          Width = 61
          Height = 17
          Caption = #21024#38500#32447
          TabOrder = 9
          OnClick = ETextTopChange
        end
        object ChkTextItalic: TCheckBox
          Left = 299
          Top = 88
          Width = 43
          Height = 17
          Caption = #26012#20307
          TabOrder = 10
          OnClick = ETextTopChange
        end
        object ETextDefault: TEdit
          Left = 286
          Top = 111
          Width = 70
          Height = 21
          TabOrder = 11
          Text = '000000'
          OnChange = ETextTopChange
        end
        object ETextRotate: TComboBox
          Left = 274
          Top = 0
          Width = 81
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 12
          Text = #19981#26059#36716
          OnChange = ETextTopChange
          Items.Strings = (
            #19981#26059#36716
            '90'#24230#26059#36716
            '180'#24230#26059#36716
            '270'#24230#26059#36716
            #27700#24179#32763#36716
            '90'#24230#32763#36716
            #22402#30452#32763#36716
            '270'#24230#32763#36716)
        end
        object ETextLength: TSpinEdit
          Left = 391
          Top = 0
          Width = 32
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 13
          Value = 6
        end
        object plTextColor: TPanel
          Left = 200
          Top = 7
          Width = 38
          Height = 32
          Caption = #39068#33394
          TabOrder = 14
          OnMouseUp = plTextColorMouseUp
        end
        object ETextFontName: TComboBox
          Left = 6
          Top = 92
          Width = 232
          Height = 42
          Style = csOwnerDrawFixed
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -32
          Font.Name = 'Arial'
          Font.Style = []
          ItemHeight = 36
          ParentFont = False
          TabOrder = 15
          OnChange = ETextTopChange
          OnDrawItem = ETextFontNameDrawItem
        end
        object EVRate: TSpinEdit
          Left = 299
          Top = 137
          Width = 57
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 16
          Value = 100
          OnChange = ETextTopChange
        end
        object EHRate: TSpinEdit
          Left = 419
          Top = 137
          Width = 58
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 17
          Value = 100
          OnChange = ETextTopChange
        end
        object Button2: TButton
          Left = 6
          Top = 139
          Width = 75
          Height = 25
          Caption = #28155#21152#39033#30446
          TabOrder = 18
          OnClick = Button2Click
        end
        object Button3: TButton
          Left = 87
          Top = 139
          Width = 75
          Height = 25
          Caption = #20462#25913#39033#30446
          TabOrder = 19
          OnClick = Button3Click
        end
        object Button4: TButton
          Left = 163
          Top = 139
          Width = 75
          Height = 25
          Caption = #21024#38500#39033#30446
          TabOrder = 20
          OnClick = Button4Click
        end
        object chkSerialNo: TCheckBox
          Left = 430
          Top = 2
          Width = 57
          Height = 17
          Anchors = [akTop, akRight]
          Caption = #21464#21160#21495
          TabOrder = 21
          OnClick = ETextTopChange
        end
      end
      object GpCopyMode: TRadioGroup
        Left = 794
        Top = 0
        Width = 149
        Height = 166
        Align = alRight
        Caption = #21472#21152#27169#24335
        Columns = 2
        ItemIndex = 9
        Items.Strings = (
          #20840#40657
          #21453#30333
          #21512#24182#22797#21046
          #21512#24182#30011#22270
          #21453#33394#22797#21046
          #21453#33394#21024#38500
          #22270#26696#22797#21046
          #22270#26696#21453#33394
          #22270#26696#21076#38500
          #22270#29255#21472#21152
          #22270#29255#22797#21046
          #22270#29255#25830#38500
          #22270#29255#21453#33394
          #22270#29255#32472#21046
          #20840#30333)
        TabOrder = 2
        OnClick = GpCopyModeClick
      end
      object RGSizeOption: TGroupBox
        Left = 0
        Top = 0
        Width = 121
        Height = 166
        Align = alLeft
        Caption = #26631#31614#25511#21046
        TabOrder = 3
        object Label4: TLabel
          Left = 6
          Top = 16
          Width = 36
          Height = 13
          Caption = #22270#29255#38271
        end
        object Label7: TLabel
          Left = 64
          Top = 16
          Width = 36
          Height = 13
          Caption = #22270#29255#39640
        end
        object Label8: TLabel
          Left = 7
          Top = 56
          Width = 36
          Height = 13
          Caption = #26631#31614#38271
        end
        object Label9: TLabel
          Left = 63
          Top = 56
          Width = 36
          Height = 13
          Caption = #26631#31614#39640
        end
        object Label18: TLabel
          Left = 10
          Top = 103
          Width = 48
          Height = 13
          Caption = #22270#29255#26684#24335
        end
        object EImgWidth: TSpinEdit
          Left = 6
          Top = 31
          Width = 51
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 0
          Value = 0
          OnChange = EImgWidthChange
        end
        object EImgHeight: TSpinEdit
          Left = 64
          Top = 31
          Width = 51
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 1
          Value = 0
          OnChange = EImgWidthChange
        end
        object ELabelWidth: TSpinEdit
          Left = 7
          Top = 71
          Width = 51
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 2
          Value = 0
          OnChange = ELabelWidthChange
        end
        object ELabelHeight: TSpinEdit
          Left = 64
          Top = 71
          Width = 51
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 3
          Value = 0
          OnChange = ELabelWidthChange
        end
        object EPixel: TComboBox
          Left = 64
          Top = 99
          Width = 51
          Height = 21
          ItemIndex = 0
          TabOrder = 4
          Text = '2'#20301
          OnChange = EPixelChange
          Items.Strings = (
            '2'#20301
            '8'#20301
            '16'#20301
            '24'#20301)
        end
        object Button1: TButton
          Left = 40
          Top = 126
          Width = 75
          Height = 25
          Caption = #25552#21462#32972#26223
          TabOrder = 5
          OnClick = Button1Click
        end
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 506
    Width = 951
    Height = 19
    Panels = <
      item
        Text = #40736#26631#20301#32622#65306
        Width = 65
      end
      item
        Width = 100
      end
      item
        Text = #20301#32622#27604#20363#65306
        Width = 65
      end
      item
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object ScrollBox1: TScrollBox
    Left = 0
    Top = 0
    Width = 951
    Height = 312
    Align = alClient
    TabOrder = 2
    object ImageEdit: TImage
      Left = 0
      Top = 0
      Width = 951
      Height = 312
      AutoSize = True
      OnMouseDown = ImageEditMouseDown
      OnMouseMove = ImageEditMouseMove
    end
  end
  object PrinterSetupDialog1: TPrinterSetupDialog
    Left = 32
    Top = 224
  end
  object DQCaptcha: TADOQuery
    Connection = FFlyLabelDB.ADOConn
    Parameters = <
      item
        Name = 'CD'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Captcha FROM Ring_Label2015_11'
      'WHERE CodeNo=:CD')
    Left = 608
    Top = 176
  end
end
