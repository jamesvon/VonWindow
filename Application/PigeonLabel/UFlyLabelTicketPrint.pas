unit UFlyLabelTicketPrint;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Spin, ComCtrls, Inifiles, Printers, ULabelPrint,
  UFlyLabelInfo, UVonGraphicAPI, Buttons, CheckLst, UFlyLabelDB, DB, ADODB;

type
  TFFlyLabelTicketPrint = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    ListBox1: TListBox;
    Panel1: TPanel;
    EContentName: TLabeledEdit;
    RadioGroup1: TRadioGroup;
    Label3: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    Label14: TLabel;
    Label29: TLabel;
    Label16: TLabel;
    ETextTop: TSpinEdit;
    ETextLeft: TSpinEdit;
    ETextFontSize: TSpinEdit;
    chkTextBold: TCheckBox;
    chkTextUnderline: TCheckBox;
    ETextWordWidth: TSpinEdit;
    ETextAlign: TComboBox;
    chkTextStrikeOut: TCheckBox;
    ChkTextItalic: TCheckBox;
    ETextDefault: TEdit;
    ETextRotate: TComboBox;
    ETextLength: TSpinEdit;
    plTextColor: TPanel;
    ETextFontName: TComboBox;
    GpCopyMode: TRadioGroup;
    EVRate: TSpinEdit;
    EHRate: TSpinEdit;
    Label51: TLabel;
    Label36: TLabel;
    StatusBar1: TStatusBar;
    RGSizeOption: TGroupBox;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label18: TLabel;
    EImgWidth: TSpinEdit;
    EImgHeight: TSpinEdit;
    ELabelWidth: TSpinEdit;
    ELabelHeight: TSpinEdit;
    EPixel: TComboBox;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Panel2: TPanel;
    BtnPrinterSetup: TBitBtn;
    BtnPrint: TBitBtn;
    PrinterSetupDialog1: TPrinterSetupDialog;
    ScrollBox1: TScrollBox;
    ImageEdit: TImage;
    chkSerialNo: TCheckBox;
    DQCaptcha: TADOQuery;
    Label1: TLabel;
    EYear: TSpinEdit;
    EArea: TSpinEdit;
    Label5: TLabel;
    Button5: TButton;
    Button6: TButton;
    ECode: TLabeledEdit;
    ListRecords: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure EImgWidthChange(Sender: TObject);
    procedure ELabelWidthChange(Sender: TObject);
    procedure EPixelChange(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ETextFontNameDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure plTextColorMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ETextFontSizeChange(Sender: TObject);
    procedure ETextTopChange(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure GpCopyModeClick(Sender: TObject);
    procedure ImageEditMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImageEditMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BtnPrinterSetupClick(Sender: TObject);
    procedure BtnPrintClick(Sender: TObject);
    procedure ECodeKeyPress(Sender: TObject; var Key: Char);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
  private
    { Private declarations }
    LabelImage: TLabelImage;
    FCurrentLabel: TLabelInfo;
    FImageFilename: string;
    FImageWidth, FImageLength, FLabelWidth, FLabelHeight: Integer;
    procedure SaveSubject;
  public
    { Public declarations }
    procedure Redrew;
  end;

var
  FFlyLabelTicketPrint: TFFlyLabelTicketPrint;

implementation

{$R *.dfm}

procedure TFFlyLabelTicketPrint.FormCreate(Sender: TObject);
var
  Idx: Integer;
begin
  LabelImage:= TLabelImage.Create;
  FCurrentLabel:= TLabelInfo.Create;
  FCurrentLabel.Content.LoadFromFile(ExtractFilePath(Application.ExeName) + 'Ticket.ini');
  if FCurrentLabel.Content.SectionExists('Background') then begin
    FImageFilename:= FCurrentLabel.Content.ReadString('Background', 'BMP', '');
    LabelImage.OrgBmp.LoadFromFile(FImageFilename);
    FImageWidth:= LabelImage.OrgBmp.Width;
    FImageLength:= LabelImage.OrgBmp.Height;
    FLabelWidth:= FCurrentLabel.Content.ReadInteger('Background', 'LabelWidth', FImageWidth);
    FLabelHeight:= FCurrentLabel.Content.ReadInteger('Background', 'LabelHeight', FImageLength);
  end;
  EImgWidth.Value:= FImageWidth;
  EImgHeight.Value:= FImageLength;
  ELabelWidth.Value:= FLabelWidth;
  ELabelHeight.Value:= FLabelHeight;

  FCurrentLabel.Content.EraseSection('Background');
  ETextFontName.Items:= Screen.Fonts;
  FCurrentLabel.Content.ReadSections(ListBox1.Items);
  StatusBar1.Panels[4].Text:= Printer.Printers[Printer.PrinterIndex];

  TabSheet2.TabVisible:= FFlyLabelDB.TaskParams = 'DESIGN';
  ImageEdit.Enabled:= FFlyLabelDB.TaskParams = 'DESIGN';
  Redrew;
end;

procedure TFFlyLabelTicketPrint.FormDestroy(Sender: TObject);
begin
  FCurrentLabel.Content.WriteString('Background', 'BMP', FImageFilename);
  FCurrentLabel.Content.WriteInteger('Background', 'LabelWidth', FLabelWidth);
  FCurrentLabel.Content.WriteInteger('Background', 'LabelHeight', FLabelHeight);
  FCurrentLabel.Content.SaveToFile(ExtractFilePath(Application.ExeName) + 'Ticket.ini');
  FCurrentLabel.Free;
  LabelImage.Free;
end;

procedure TFFlyLabelTicketPrint.ETextFontNameDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin
  with TComboBox(Control) do
  begin
    Canvas.FillRect(Rect);
    Canvas.Font.Name := 'Arial';
    Canvas.Font.Size := 9;
    Canvas.TextOut(Rect.Left + 2, Rect.Top + 1, Copy(Items[Index], 1, 8));
    Canvas.TextOut(Rect.Left + 2, Rect.Top + 16, Copy(Items[Index], 9, 8));
    Canvas.Font.Name := Items[Index];
    Canvas.Font.Size := 24;
    Canvas.TextOut(Rect.Left + 60, Rect.Top + 2, ETextDefault.Text);
    Canvas.MoveTo(58, 0);
    Canvas.LineTo(58, Rect.Bottom - Rect.Top);
  end;
end;

(* Background *)

procedure TFFlyLabelTicketPrint.Button1Click(Sender: TObject);
var
  bmp: TBitmap;
begin     //提取背景图片
  //CanDrew:= false;
  with TOpenDialog.Create(nil) do try
    if not Execute then Exit;
    bmp:= TBitmap.Create;
    LoadGraphicFile(Filename, 0, 0, bmp);
    LabelImage.OrgBmp.Assign(bmp);
    ImageEdit.Picture.Assign(bmp);
    EImgWidth.Value:= bmp.Width;
    EImgHeight.Value:= bmp.Height;
    bmp.Free;
    FImageFilename:= Filename;
    Redrew;
  finally
    Free;
  end;
end;

procedure TFFlyLabelTicketPrint.EImgWidthChange(Sender: TObject);
begin     //更改图片尺寸
  if(Sender <> nil)and(not (Sender as TWinControl).Focused)then Exit;
  ImageEdit.Picture.Bitmap.Width:= EImgWidth.Value;
  ImageEdit.Picture.Bitmap.Height:= EImgHeight.Value;
  LabelImage.OrgBmp.Width:= EImgWidth.Value;
  LabelImage.OrgBmp.Height:= EImgHeight.Value;
  Redrew;
end;

procedure TFFlyLabelTicketPrint.ELabelWidthChange(Sender: TObject);
begin     //更改标签尺寸
  if(Sender <> nil)and(not (Sender as TWinControl).Focused)then Exit;
  FLabelWidth:= ELabelWidth.Value;
  FLabelHeight:= ELabelHeight.Value;
  Redrew;
end;

procedure TFFlyLabelTicketPrint.EPixelChange(Sender: TObject);
begin
// FCurrentLabel.Content.WriteInteger('Background', 'Pixel', EPixel.ItemIndex);
end;

(* Subject *)

procedure TFFlyLabelTicketPrint.SaveSubject;
begin
  if EContentName.Text = '' then Exit;
  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Kind', 0);
  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'CopyMode', GpCopyMode.ItemIndex);
  FCurrentLabel.Content.WriteBool(EContentName.Text, 'SerialNo', chkSerialNo.Checked);
  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'VRate', EVRate.Value);
  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'HRate', EHRate.Value);

  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'LEN', ETextLength.Value);
  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'BKCOLOR', plTextColor.Color);
  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'FONTCOLOR', plTextColor.Font.Color);
  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Top', ETextTop.Value);
  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Left', ETextLeft.Value);
  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'WordWidth', ETextWordWidth.Value);
  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Align', ETextAlign.ItemIndex);
  FCurrentLabel.Content.WriteString(EContentName.Text, 'FontName', ETextFontName.Text);
  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'FontSize', ETextFontSize.Value);
  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'Rotate', ETextRotate.ItemIndex);
  FCurrentLabel.Content.WriteBool(EContentName.Text, 'Bold', ChkTextBold.Checked);
  FCurrentLabel.Content.WriteBool(EContentName.Text, 'Italic', ChkTextItalic.Checked);
  FCurrentLabel.Content.WriteBool(EContentName.Text, 'Underline', ChkTextUnderline.Checked);
  FCurrentLabel.Content.WriteBool(EContentName.Text, 'StrikeOut', ChkTextStrikeOut.Checked);
  FCurrentLabel.Content.WriteString(EContentName.Text, 'ExampleText', ETextDefault.Text);
end;

procedure TFFlyLabelTicketPrint.ListBox1Click(Sender: TObject);
begin
  if ListBox1.ItemIndex < 0 then Exit;
  EContentName.Text:= ListBox1.Items[ListBox1.ItemIndex];
  EVRate.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'VRate', 100);
  EHRate.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'HRate', 100);
  chkSerialNo.Checked:= FCurrentLabel.Content.ReadBool(EContentName.Text, 'SerialNo', False);
  ETextLength.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'LEN', 0);
  plTextColor.Color:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'BKCOLOR', 0);
  plTextColor.Font.Color:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'FONTCOLOR', 0);
  ETextTop.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'Top', 0);
  ETextLeft.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'Left', 0);
  ETextWordWidth.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'WordWidth', 0);
  ETextAlign.ItemIndex:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'Align', 0);
  ETextFontName.ItemIndex:= ETextFontName.Items.IndexOf(FCurrentLabel.Content.ReadString(EContentName.Text, 'FontName', '宋体'));
  ETextFontSize.Value:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'FontSize', 0);
  ETextRotate.ItemIndex:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'Rotate', 0);
  ChkTextBold.Checked:= FCurrentLabel.Content.ReadBool(EContentName.Text, 'Bold', False);
  ChkTextItalic.Checked:= FCurrentLabel.Content.ReadBool(EContentName.Text, 'Italic', False);
  ChkTextUnderline.Checked:= FCurrentLabel.Content.ReadBool(EContentName.Text, 'Underline', False);
  ChkTextStrikeOut.Checked:= FCurrentLabel.Content.ReadBool(EContentName.Text, 'StrikeOut', False);
  ETextDefault.Text:= FCurrentLabel.Content.ReadString(EContentName.Text, 'ExampleText', '');
  GpCopyMode.ItemIndex:= FCurrentLabel.Content.ReadInteger(EContentName.Text, 'CopyMode', 0);
end;

procedure TFFlyLabelTicketPrint.Button2Click(Sender: TObject);
begin
  ListBox1.Items.Add(EContentName.Text);
  SaveSubject;
end;

procedure TFFlyLabelTicketPrint.Button3Click(Sender: TObject);
begin
  if ListBox1.ItemIndex < 0 then
    raise Exception.Create('尚未选择项目无法进行修改。');
  EContentName.Text:= ListBox1.Items[ListBox1.ItemIndex];
  SaveSubject;
end;

procedure TFFlyLabelTicketPrint.Button4Click(Sender: TObject);
begin
  FCurrentLabel.Content.EraseSection(EContentName.Text);
end;

(* ticket *)

procedure TFFlyLabelTicketPrint.plTextColorMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  with TColorDialog.Create(nil) do try
    case Button of
    mbLeft:   Color:= TPanel(Sender).Font.Color;
    mbRight:  Color:= TPanel(Sender).Color;
    mbMiddle: Color:= TPanel(Sender).Font.Color;
    end;
    Color:= LabelImage.OrgBmp.Canvas.Pen.Color;
    if not Execute then Exit;
    case Button of
    mbLeft:   TPanel(Sender).Font.Color:= Color;
    mbRight:  TPanel(Sender).Color:= Color;
    mbMiddle: TPanel(Sender).Font.Color:= Color;
    end;
    SaveSubject;
    Redrew;
  finally
    Free;
  end;
end;

procedure TFFlyLabelTicketPrint.ETextFontSizeChange(Sender: TObject);
begin
  FCurrentLabel.Content.WriteInteger(EContentName.Text, 'FontSize', ETextFontSize.Value);
  Redrew;
end;

procedure TFFlyLabelTicketPrint.ETextTopChange(Sender: TObject);
begin
  if not (Sender as TWinControl).Focused then Exit;
  Redrew;
end;

procedure TFFlyLabelTicketPrint.GpCopyModeClick(Sender: TObject);
begin
  Redrew;
end;

procedure TFFlyLabelTicketPrint.ImageEditMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin     //鼠标在图片上定点
  ETextTop.Value:= Y + ScrollBox1.VertScrollBar.ThumbSize;
  ETextLeft.Value:= X - ScrollBox1.HorzScrollBar.ThumbSize;
  Redrew;
end;

procedure TFFlyLabelTicketPrint.ImageEditMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin     //鼠标跟踪
  StatusBar1.Panels[1].Text:= format('%d:%d', [X, Y]);
  if(ImageEdit.Picture.Width = 0)or(ImageEdit.Picture.Height = 0)then
    StatusBar1.Panels[3].Text:= '-, -'
  else StatusBar1.Panels[1].Text:= format('%.2f%%, %.2f%%',
    [X/ImageEdit.Picture.Width*100, Y/ImageEdit.Picture.Height*100]);
end;

procedure TFFlyLabelTicketPrint.Redrew;
var
  I: Integer;
begin
  SaveSubject;
  LabelImage.Content.Text:= FCurrentLabel.Content.Text;
  LabelImage.DrewBaseBmp;
  LabelImage.Drew(ImageEdit.Picture.Bitmap, ETextDefault.Text);
end;

(* Printer *)

procedure TFFlyLabelTicketPrint.BtnPrintClick(Sender: TObject);
var
  I, X, Y: Integer;
  bmp: TBitmap;
begin
  bmp:= TBitmap.Create;
  bmp.Width:= FLabelWidth;
  bmp.Height:= FLabelHeight;
  I:= 0; X:= 0; Y:= 0;
  Printer.BeginDoc;
  Printer.Title:= '环标证';
  while I < ListRecords.Items.Count do begin
    //LabelImage.Content.
    LabelImage.Content.WriteString('年份', 'ExampleText', IntToStr(EYear.Value));
    LabelImage.Content.WriteString('区域', 'ExampleText', Format('%0.2d', [EArea.Value]));
    LabelImage.Content.WriteString('环号', 'ExampleText', ListRecords.Items.Names[I]);
    LabelImage.Content.WriteString('验证码', 'ExampleText', ListRecords.Items.ValueFromIndex[I]);
    LabelImage.DrewBaseBmp;
    LabelImage.Drew(bmp, '');
    Printer.Canvas.FillRect(Rect(X, Y, X + FLabelWidth, Y + FLabelHeight));

    //Printer.Canvas.CopyRect(Rect(X, Y, bmp.Width + X, bmp.Height + Y),
    //  bmp.Canvas, Rect(0, 0, bmp.Width, bmp.Height));
    Printer.Canvas.Draw(X, Y, bmp);
    Inc(I);
    Inc(Y, FLabelHeight);
    if I mod 4 = 0 then begin Y:= 0; Inc(X, FLabelWidth); end;
    if I mod 16 = 0 then begin
      Y:= 0; X:= 0;
      Printer.EndDoc;
      Printer.BeginDoc;
      Printer.Title:= '环标证';
    end;
  end;
  Printer.EndDoc;
  bmp.Free;
end;

procedure TFFlyLabelTicketPrint.BtnPrinterSetupClick(Sender: TObject);
begin
  PrinterSetupDialog1.Execute;
  StatusBar1.Panels[4].Text:= Printer.Printers[Printer.PrinterIndex];
end;

procedure TFFlyLabelTicketPrint.ECodeKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = 13 then begin
    DQCaptcha.Close;
    DQCaptcha.SQL.Text:= 'SELECT Captcha FROM Ring_Label' + IntToStr(EYear.Value)
      + '_' + Format('%0.2d', [EArea.Value]) + ' WHERE CodeNo=' + ECode.Text;
    DQCaptcha.Open;
    if not DQCaptcha.EOF then
      ListRecords.Items.Add(Copy('000000' + ECode.Text, Length(ECode.Text), 7) + '=' + DQCaptcha.Fields[0].AsString)
    else raise Exception.Create('该环号未在数据库中发现，不允许补号');
  end;
end;

procedure TFFlyLabelTicketPrint.Button5Click(Sender: TObject);
begin
  ListRecords.Clear;
end;

procedure TFFlyLabelTicketPrint.Button6Click(Sender: TObject);
begin
  if ListRecords.ItemIndex < 0 then Exit;
  ListRecords.DeleteSelected;
end;

end.
