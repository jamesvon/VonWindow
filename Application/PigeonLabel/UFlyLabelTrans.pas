unit UFlyLabelTrans;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, UFlyLabelDB, DB, ADODB, UVonConfig;

type
  TForm3 = class(TForm)
    ProgressBar1: TProgressBar;
    BitBtn1: TBitBtn;
    ADOTable1: TADOTable;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm3.BitBtn1Click(Sender: TObject);
begin
  with ADOTable1 do begin
    Open;
    ProgressBar1.Position:= 0;
    ProgressBar1.Max:= RecordCount;
    while not EOF do begin
      FVonSetting.Clear;
      FVonSetting.Add('LEN', FieldByName('WordLength').AsString);
      FVonSetting.Add('BKCOLOR', IntToStr(FieldByName('BaseColor').AsInteger));
      FVonSetting.Add('FONTCOLOR', IntToStr(FieldByName('FontColor').AsInteger));
      FVonSetting.Add('TEXTTOP', IntToStr(FieldByName('WordTop').AsInteger));
      FVonSetting.Add('TEXTLEFT', IntToStr(FieldByName('WordLeft').AsInteger));
      FVonSetting.Add('WordWidth', IntToStr(FieldByName('WordWidth').AsInteger));
      FVonSetting.Add('Align', IntToStr(FieldByName('Align').AsInteger));
      FVonSetting.Add('FontName', FieldByName('FontName').AsString);
      FVonSetting.Add('FontSize', FieldByName('FontSize').AsString);
      FVonSetting.Add('Rotate', IntToStr(0));
      FVonSetting.Add('Bold', BoolToStr(FieldByName('IsBold').AsBoolean));
      FVonSetting.Add('Italic', BoolToStr(FieldByName('IsItalic').AsBoolean));
      FVonSetting.Add('Underline', BoolToStr(FieldByName('IsStrikeout').AsBoolean));
      FVonSetting.Add('StrikeOut', BoolToStr(FieldByName('IsUnderline').AsBoolean));
      FVonSetting.Add('CopyMode', IntToStr(FieldByName('CopyStyle').AsInteger));
      Edit;
      FieldByName('Content').AsString:= '����=' + FVonSetting.Text[VSK_COMMA];
      Post;
      Next;
      ProgressBar1.Position:= ProgressBar1.Position + 1;
    end;
    Close;
  end;
end;

end.
