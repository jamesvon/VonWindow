unit ULabelPrint;

interface

uses
  Types, Classes, Printers, SysUtils, Forms, Math, ShellAPI, ExtCtrls, Dialogs,
  Windows, Graphics, UFlyLabelDB, UVonClass, UVonQRCode, UVonLog, UZXIngQRCode,
  UVonXorCrypt;

const
  TIMERVALUE = 1;
  KEYS: array[0..6] of byte= ($46, $43, $49, $2D, $43, $48, $4E);
  KEY_VALUES: array[0..255] of byte= (
  $09, $d0, $c4, $79, $28, $c8, $ff, $e0, $84, $aa, $6c, $39, $9d, $ad, $72, $87,
  $7d, $ff, $9b, $e3, $d4, $26, $83, $61, $c9, $6d, $a1, $d4, $79, $74, $cc, $93,
  $85, $d0, $58, $2e, $2a, $4b, $57, $05, $1c, $a1, $6a, $62, $c3, $bd, $27, $9d,
  $0f, $1f, $25, $e5, $51, $60, $37, $2f, $c6, $95, $c1, $fb, $4d, $7f, $f1, $e4,
  $ae, $5f, $6b, $f4, $0d, $72, $ee, $46, $ff, $23, $de, $8a, $b1, $cf, $8e, $83,
  $f1, $49, $02, $e2, $3e, $98, $1e, $42, $8b, $f5, $3e, $b6, $7f, $4b, $f8, $ac,
  $83, $63, $1f, $83, $25, $97, $02, $05, $76, $af, $e7, $84, $3a, $79, $31, $d4,
  $4f, $84, $64, $50, $5c, $64, $c3, $f6, $21, $0a, $5f, $18, $c6, $98, $6a, $26,
  $28, $f4, $e8, $26, $3a, $60, $a8, $1c, $d3, $40, $a6, $64, $7e, $a8, $20, $c4,
  $52, $66, $87, $c5, $7e, $dd, $d1, $2b, $32, $a1, $1d, $1d, $9c, $9e, $f0, $86,
  $80, $f6, $e8, $31, $ab, $6f, $04, $ad, $56, $fb, $9b, $53, $8b, $2e, $09, $5c,
  $b6, $85, $56, $ae, $d2, $25, $0b, $0d, $29, $4a, $77, $21, $e2, $1f, $b2, $53,
  $ae, $13, $67, $49, $e8, $2a, $ae, $86, $93, $36, $51, $04, $99, $40, $4a, $66,
  $78, $a7, $84, $dc, $b6, $9b, $a8, $4b, $04, $04, $67, $93, $23, $db, $5c, $1e,
  $46, $ca, $e1, $d6, $2f, $e2, $81, $34, $5a, $22, $39, $42, $18, $63, $cd, $5b,
  $c1, $90, $c6, $e3, $07, $df, $b8, $46, $6e, $b8, $88, $16, $2d, $0d, $cc, $4a);

type
  TLabelImage = class
  private
    { Private declarations }
    FQR: TQRCode;
    FLabelValues: TVonSetting;
    FContent: TVonConfig;
    FOrgBmp: TBitmap;
    function GetContent: TVonConfig;
    procedure SetOrgBmp(const Value: TBitmap);
  private
    FPixel: Integer;
    FInfo: string;
    procedure Rotate(RotateType: Integer; srcBmp, destBmp: TBitmap);
    procedure DrewTextBmp(bmp: TBitmap; SectionName, Value: string);
    procedure DrewGrpBmp(bmp: TBitmap; SectionName, Value: string);
    procedure DrewCodeBmp(bmp: TBitmap; SectionName, Value: string);
    function GetCopyMode(ModeID: Integer): TCopyMode;
    function SetCodeValue(Value: string): PChar;
    procedure Zoom(VRate, HRate: Integer; srcBmp, destBmp: TBitmap);
    procedure SetPixel(const Value: Integer);
    function QRCode(Code: PChar; Params: TStrings): TBitmap; overload;
    function QRCode(Code: Int64; Params: TStrings): TBitmap; overload;
  public
    { Public declarations }
    FParams: TVonConfig;                    //FParams 动态标签内容
    FBaseBmp: TBitmap;
    FCrypt: TVonXorCrypt;
    FXinCode: TDelphiZXingQRCode;
    constructor Create;
    destructor Destroy;
    /// <summary>画基本图</summary>
    procedure DrewBaseBmp;
    /// <summary>在基本图的基础上画标签</summary>
    procedure Drew(bmp: TBitmap; Text: string);
    function QRRoateCode(Code: Int64; radius, dpi, zoom: Extended): TBitmap; overload;
  published
    { Published declarations }
    /// <summary>背景图</summary>
    property OrgBmp: TBitmap read FOrgBmp write SetOrgBmp;
    /// <summary>标签设置</summary>
    property Content: TVonConfig read GetContent;
    property LabelValues: TVonSetting read FLabelValues;
    /// <summary>像素</summary>
    property Pixel: Integer read FPixel write SetPixel;
    property Info: string read FInfo;
  end;

  TLabelThread = class(TThread)
  private
    lbImage: TLabelImage;
  public
    constructor Create(CreateSuspended: Boolean); overload;
    destructor Destroy; override;
    procedure Execute; override;
  end;

  TProcessEvent = procedure(Sender: TObject; ProcessID: Integer) of object;

  ELabelFlagPossion = (EP_TOP, EP_Section, EP_Bottom);

  TLabelPrint = class
  private
    { Private declarations }
    FCurrentLabel: Integer;
    FCurrentID: Integer;
    FCurrentPage: Integer;
    FLabelImage: TLabelImage;
    FTopMargin: Integer;
    FLabelHeight: Integer;
    FLabelWidth: Integer;
    FSectionCount: Integer;
    FColCount: Integer;
    FLeftMargin: Integer;
    FRowCount: Integer;
    FOnFinally: TNotifyEvent;
    FOnEndOfLabel: TProcessEvent;
    FOnEndOfPage: TProcessEvent;
    FItems: TStrings;
    FRowSpace: Integer;
    FColSpace: Integer;
    FFlagLong: Integer;
    FFlagDistance: Integer;
    FPointOffset: Integer;
    TempBitmap: TBitmap;
    FPixel: Integer;
    FFlagKind: Integer;
    procedure SetLabelImage(const Value: TLabelImage);
    procedure DrewCuttingFlag(Canvas: TCanvas; SectionID, RowNumber: Integer; Rate: real; Possion: ELabelFlagPossion);
    procedure DrewHLine(Canvas: TCanvas; SectionID, RowNumber: Integer; Rate: real);
    procedure DrewVLine(Canvas: TCanvas; SectionID, RowNumber: Integer; Rate: real);
    procedure DrewCircle(Canvas: TCanvas; SectionID, RowNumber: Integer; Rate: real);
    procedure SetColCount(const Value: Integer);
    procedure SetCurrentID(const Value: Integer);
    procedure SetItems(const Value: TStrings);
    procedure SetLabelHeight(const Value: Integer);
    procedure SetLabelWidth(const Value: Integer);
    procedure SetLeftMargin(const Value: Integer);
    procedure SetOnEndOfLabel(const Value: TProcessEvent);
    procedure SetOnEndOfPage(const Value: TProcessEvent);
    procedure SetOnFinally(const Value: TNotifyEvent);
    procedure SetRowCount(const Value: Integer);
    procedure SetSectionCount(const Value: Integer);
    procedure SetTopMargin(const Value: Integer);
    procedure PrintLabelFinally;
    procedure PrintPageFinally(prn: TPrinter);
    procedure PrintFinally;
    function DrewCanvas(Canvas: TCanvas; MaxLabels: Integer; Rate: Real): Boolean;
    function ThreadCanvas(Canvas: TCanvas; MaxLabels: Integer; Rate: Real): Boolean;
    procedure CopyCanvas(OrgCanvas: TCanvas; DestCanvas: TCanvas; Width, Height: Integer);
    procedure SetColSpace(const Value: Integer);
    procedure SetRowSpace(const Value: Integer);
    procedure SetFlagDistance(const Value: Integer);
    procedure SetFlagLong(const Value: Integer);
    procedure SetPointOffset(const Value: Integer);
    procedure Drew_L_CuttingFlag(Canvas: TCanvas; SectionCount: Integer; Rate: real);
    procedure Drew_Lifeng_CuttingFlag(Canvas: TCanvas; SectionCount: Integer; Rate: real);
    procedure SetPixel(const Value: Integer);
    procedure SetFlagKind(const Value: Integer);
  public
    { Public declarations }
    constructor Create;
    destructor Destroy;
    /// <summary>打印</summary>
    procedure Print(Prn: TPrinter);
    /// <summary>缓存为文件</summary>
    procedure SaveFile(Filename: string);
    /// <summary>预览</summary>
    procedure Preview(ViewCanvas: TCanvas);
  published
    { Published declarations }
    /// <summary>即将打印的标签（标签基本信息，含背景图）</summary>
    property LabelImage: TLabelImage read FLabelImage write SetLabelImage;
    /// <summary>标签宽度</summary>
    property LabelWidth: Integer read FLabelWidth write SetLabelWidth;
    /// <summary>标签高度</summary>
    property LabelHeight: Integer read FLabelHeight write SetLabelHeight;
    /// <summary>段数量</summary>
    property SectionCount: Integer read FSectionCount write SetSectionCount;
    /// <summary>列数量</summary>
    property ColCount: Integer read FColCount write SetColCount;
    /// <summary>行数量</summary>
    property RowCount: Integer read FRowCount write SetRowCount;
    /// <summary>上边距</summary>
    property TopMargin: Integer read FTopMargin write SetTopMargin;
    /// <summary>左边距</summary>
    property LeftMargin: Integer read FLeftMargin write SetLeftMargin;
    /// <summary>行间隔</summary>
    property RowSpace: Integer read FRowSpace write SetRowSpace;
    /// <summary>列间隔</summary>
    property ColSpace: Integer read FColSpace write SetColSpace;
    /// <summary>打印号码集</summary>
    property Items: TStrings read FItems write SetItems;
    /// <summary>当前打印序号</summary>
    property CurrentID: Integer read FCurrentID write SetCurrentID;
    /// <summary>像素</summary>
    property Pixel: Integer read FPixel write SetPixel;
    /// <summary>段间隔</summary>
    property FlagDistance: Integer read FFlagDistance write SetFlagDistance;
    /// <summary>标记类型</summary>
    property FlagKind: Integer read FFlagKind write SetFlagKind;
    /// <summary>标记大小</summary>
    property FlagLong: Integer read FFlagLong write SetFlagLong;
    /// <summary>标记偏移量</summary>
    property PointOffset: Integer read FPointOffset write SetPointOffset;
    /// <summary>打印结束事件</summary>
    property OnFinally: TNotifyEvent read FOnFinally write SetOnFinally;
    /// <summary>标签打印结束事件</summary>
    property OnEndOfLabel: TProcessEvent read FOnEndOfLabel write SetOnEndOfLabel;
    /// <summary>单页打印结束事件</summary>
    property OnEndOfPage: TProcessEvent read FOnEndOfPage write SetOnEndOfPage;
  end;

  TFastPrint = class(TThread)
  private
    FList: TList;
    FRowCount: Integer;
    FColSpace: Integer;
    FLabelHeight: Integer;
    FRowSpace: Integer;
    FItems: TStrings;
    FOnEndOfPage: TProcessEvent;
    FTopMargin: Integer;
    FLabelWidth: Integer;
    FLeftMargin: Integer;
    FLabelImage: TLabelImage;
    FColCount: Integer;
    FOnEndOfLabel: TProcessEvent;
    FOnFinally: TNotifyEvent;
    FSectionCount: Integer;
    FPixel: Integer;
    procedure SetColCount(const Value: Integer);
    procedure SetColSpace(const Value: Integer);
    procedure SetItems(const Value: TStrings);
    procedure SetLabelHeight(const Value: Integer);
    procedure SetLabelImage(const Value: TLabelImage);
    procedure SetLabelWidth(const Value: Integer);
    procedure SetLeftMargin(const Value: Integer);
    procedure SetOnEndOfLabel(const Value: TProcessEvent);
    procedure SetOnEndOfPage(const Value: TProcessEvent);
    procedure SetOnFinally(const Value: TNotifyEvent);
    procedure SetPixel(const Value: Integer);
    procedure SetRowCount(const Value: Integer);
    procedure SetRowSpace(const Value: Integer);
    procedure SetSectionCount(const Value: Integer);
    procedure SetTopMargin(const Value: Integer);
  public
    constructor Create(Labels: TStrings; ThreadCount: Integer);
    destructor Destroy; override;
    procedure Execute; override;
  published
    { Published declarations }
    property LabelImage: TLabelImage read FLabelImage write SetLabelImage;
    property LabelWidth: Integer read FLabelWidth write SetLabelWidth;
    property LabelHeight: Integer read FLabelHeight write SetLabelHeight;
    property SectionCount: Integer read FSectionCount write SetSectionCount;
    property ColCount: Integer read FColCount write SetColCount;
    property RowCount: Integer read FRowCount write SetRowCount;
    property TopMargin: Integer read FTopMargin write SetTopMargin;
    property LeftMargin: Integer read FLeftMargin write SetLeftMargin;
    property RowSpace: Integer read FRowSpace write SetRowSpace;
    property ColSpace: Integer read FColSpace write SetColSpace;
    property Items: TStrings read FItems write SetItems;
    property Pixel: Integer read FPixel write SetPixel;
    property OnFinally: TNotifyEvent read FOnFinally write SetOnFinally;
    property OnEndOfLabel: TProcessEvent read FOnEndOfLabel write SetOnEndOfLabel;
    property OnEndOfPage: TProcessEvent read FOnEndOfPage write SetOnEndOfPage;
  end;

  Int64Rec = packed record
    case Integer of
      0: (data: Int64);
      1: (Bytes: array [0..7] of Byte);
  end;

function CheckCode(Code: PChar; var Year, area, NoID: Integer): Boolean;

implementation

uses UVonSystemFuns, DCPsha1;

var
  szQR: TQRCode;
  CriticalSection : TRTLCriticalSection;  //定义临界区
  FLabelItems: TStrings;
  GlobalIdx, ThreadRuning: Integer;

procedure ChangeBytes(var data: array of byte);
var
  tmp: byte;       //0 1 2 3 4 5  6 7 8  9  0  1 2 3 4  5  6
begin     //存储顺序 3,8,0,4,1,10,2,5,12,15,11,7,6,9,16,13,14,
  tmp:= data[0];
  data[0]:= data[3];
  data[3]:= data[4];
  data[4]:= data[1];
  data[1]:= data[8];
  data[8]:= data[12];
  data[12]:= data[6];
  data[6]:= data[2];
  data[2]:= tmp;
  tmp:= data[5];
  data[5]:= data[10];
  data[10]:= data[11];
  data[11]:= data[7];
  data[7]:= tmp;
  tmp:= data[9];
  data[9]:= data[15];
  data[15]:= data[13];
  data[13]:= tmp;
end;

procedure RestoreBytes(var data: array of byte);
var
  tmp: byte;       //0 1 2 3 4 5  6 7 8  9  0  1 2 3 4  5  6
begin     //存储顺序 3,8,0,4,1,10,2,5,12,15,11,7,6,9,16,13,14,
  tmp:= data[3];                    //tmp:= data[0];                            0 115 83 0 56 6 0 18 75 27 51 -- 65 62 112 32 32 23
  data[3]:= data[0];                //data[0]:= data[3];
  data[0]:= data[2];                //data[3]:= data[4];                        115
  data[2]:= data[6];                //data[4]:= data[1];
  data[6]:= data[12];               //data[1]:= data[8];
  data[12]:= data[8];               //data[8]:= data[12];
  data[8]:= data[1];                //data[12]:= data[6];
  data[1]:= data[4];                //data[6]:= data[2];
  data[4]:= tmp;                    //data[2]:= tmp;
  tmp:= data[7];                    //tmp:= data[5];
  data[7]:= data[11];               //data[5]:= data[10];
  data[11]:= data[10];              //data[10]:= data[11];
  data[10]:= data[5];               //data[11]:= data[7];
  data[5]:= tmp;                    //data[7]:= tmp;
  tmp:= data[13];                   //tmp:= data[9];
  data[13]:= data[15];              //data[9]:= data[15];
  data[15]:= data[9];               //data[15]:= data[13];
  data[9]:= tmp;                    //data[13]:= tmp;
end;

function CheckCode(Code: PChar; var Year, area, NoID: Integer): Boolean;
var
  szI: Integer;
  B5: Byte;
  szCode: array[0..16] of byte;
  digest: array[0..19] of byte;
  S: string;
  Hash: TDCP_sha1;
begin
  S:= string(Code);
  for szI := 0 to 10 do
    szCode[szI]:= HexToInt(Copy(S, szI * 2 + 1, 2));
  RestoreBytes(szCode);
  Year:= szCode[0] + 1919;
  area:= szCode[1];
  NoID:= szCode[2] shl 16 + szCode[3] shl 9 + szCode[4] shl 2 + szCode[5] shr 5;
  B5:= szCode[5];
  szCode[5]:= szCode[5] and $60;
  szCode[6]:=  Ord('@');
  szCode[7]:=  Ord('j');
  szCode[8]:=  Ord('A');
  szCode[9]:=  Ord('m');
  szCode[10]:= Ord('E');
  szCode[11]:= Ord('5');
  szCode[12]:= Ord('_');
  szCode[13]:= Ord('v');
  szCode[14]:= Ord('0');
  szCode[15]:= Ord('N');
  szCode[16]:= Ord('%');
  Hash := TDCP_sha1.Create(nil); // create the hash
  Hash.Init;                      // initialize it
  Hash.Update(szCode[0], 17);      // hash the stream contents
  Hash.Final(Digest);             // produce the digest
  Hash.Free;
  for szI := 0 to 10 do
    szCode[szI]:= HexToInt(Copy(S, szI * 2 + 1, 2));
  RestoreBytes(szCode);
  for szI := 0 to 9 do
    Digest[szI]:= Digest[szI] xor Digest[19 - szI];
  // [8]    [a]  [B]  [C]  [F]                                       //-109 -24 60 -8 6 103 45 2 3 101 -36 99 -30 -42 -51 108 -24 -10 -14 18
  Result:= (B5 and $1F = Digest[0] shr 3)                             //11 11111      0
//           and(szCode[6] = (Digest[0] and $7) shl 4 + Digest[1] shr 4)          //111 1111      0 1
//           and(szCode[7] = (Digest[1] and $F) shl 3 + Digest[2] shr 5)          //1111 111      1 2
           and(szCode[8] = (Digest[2] and $1F) shl 2 + Digest[3] shr 6)         //11111 11      2 3
//           and(szCode[9] = (Digest[3] and $3F) shl 1 + Digest[4] shr 7)         //111111 1      3 4
           and(szCode[10] = (Digest[4] and $7F))                                //1111111       4
           and(szCode[11] = (Digest[5] and $FF) shr 1)                          //1111111       5
           and(szCode[12] = (Digest[5] and $1) shl 6 + Digest[6] shr 2)         //1 111111      5 6
//           and(szCode[13] = (Digest[6] and $3) shl 5 + Digest[7] shr 3)         //11 11111      6 7
//           and(szCode[14] = (Digest[7] and $7) shl 4 + Digest[8] shr 4)         //111 1111      7 8
           and(szCode[15] = (Digest[8] and $F) shl 3 + Digest[9] shr 5)         //1111 111      8 9
//           and(szCode[16] = (Digest[9] and $1F) shl 2 + Digest[10] shr 6)       //11111 11      9 A
           ;
end;

function CodeToBmp(Code: PChar; Params: TStrings): TBitmap;
var
  szI: Integer;
  szCode: array[0..16] of byte;
  digest: array[0..19] of byte;
  S: string;
  Hash: TDCP_sha1;
begin
  S:= string(Code);
  if Length(S) < 12 then S:= Copy(S + '000000000000', 1, 12);                                           //123456000000
  if not TryStrToInt(Copy(S, 1, 4), szI) then Exit;       //Year -> 7F                                  //1234
  szCode[0]:= (szI + 1) and $7F;                          //1 1111111        FF                         //4D2->1001 1010010 0x52    1101101
  if not TryStrToInt(Copy(S, 5, 2), szI) then Exit;       //Area -> 7F                                  //56
  szCode[1]:= szI and $7F;                                //1 111111                                    //38 ->0000 0111000 0x38
  if not TryStrToInt(Copy(S, 7, MaxInt), szI) then Exit;  //Code -> 7FFFFF 1111111 11111111 11111111    //                  0x00
  szCode[2]:= (szI shr 16) and $7F;                       //        FFFF           1111111 1 11111111   //                  0x00
  szCode[3]:= (szI shr 9) and $7F;                        //        7F                     1111111 11   //                  0x00
  szCode[4]:= (szI shr 2) and $7F;                        //        7F                             11   //                  0x00
  szCode[5]:= (szI and $3) shl 5;                                                                       //                  0x00
  szCode[6]:=  Ord('@');                                                                                //                  0x00
  szCode[7]:=  Ord('j');                                                                                //                  0x00
  szCode[8]:=  Ord('A');                                                                                //                  0x00
  szCode[9]:=  Ord('m');                                                                                //                  0x00
  szCode[10]:= Ord('E');                                                                                //                  0x00
  szCode[11]:= Ord('5');                                                                                //                  0x00
  szCode[12]:= Ord('_');                                                                                //                  0x00
  szCode[13]:= Ord('v');                                                                                //                  0x00
  szCode[14]:= Ord('0');                                                                                //                  0x00
  szCode[15]:= Ord('N');                                                                                //                  0x00
  szCode[16]:= Ord('%');                                                                                //                  0x00
  WriteLog(LOG_Debug, 'orgCode',
      Format('%s, %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d',
      [S, szCode[0], szCode[1], szCode[2], szCode[3], szCode[4], szCode[5],
      szCode[6], szCode[7], szCode[8], szCode[9], szCode[10], szCode[11], szCode[12],
      szCode[13], szCode[14], szCode[15], szCode[16]]));
  Hash := TDCP_sha1.Create(nil);  // create the hash
  Hash.Init;                      // initialize it
  Hash.Update(szCode[0], 17);     // hash the stream contents
  Hash.Final(Digest);             // produce the digest
  Hash.Free;
  for szI := 0 to 9 do
    Digest[szI]:= Digest[szI] xor Digest[19 - szI];
  szCode[5]:= szCode[5] + Digest[0] shr 3;                    //11 11111      0
  szCode[6]:= (Digest[0] and $7) shl 4 + Digest[1] shr 4;     //111 1111      0 1
  szCode[7]:= (Digest[1] and $F) shl 3 + Digest[2] shr 5;     //1111 111      1 2
  szCode[8]:= (Digest[2] and $1F) shl 2 + Digest[3] shr 6;    //11111 11      2 3
  szCode[9]:= (Digest[3] and $3F) shl 1 + Digest[4] shr 7;    //111111 1      3 4
  szCode[10]:= (Digest[4] and $7F);                           //1111111       4
  szCode[11]:= (Digest[5] and $FF) shr 1;                     //1111111       5
  szCode[12]:= (Digest[5] and $1) shl 6 + Digest[6] shr 2;    //1 111111      5 6
  szCode[13]:= (Digest[6] and $3) shl 5 + Digest[7] shr 3;    //11 11111      6 7
  szCode[14]:= (Digest[7] and $7) shl 4 + Digest[8] shr 4;    //111 1111      7 8
  szCode[15]:= (Digest[8] and $F) shl 3 + Digest[9] shr 5;    //1111 111      8 9
  szCode[16]:= (Digest[9] and $1F) shl 2 + Digest[10] shr 6;  //11111 11      9 A
  ChangeBytes(szCode);
  if not Assigned(szQR) then
    szQR:= TQRCode.Create(nil);
    szQR.SymbolEnabled:= False;
    szQR.ClearOption:= True;
    szQR.SymbolColor:= clBlack;
    szQR.BackColor:= clWhite;
    szQR.Match:= True;
    if Params.Values['Version'] = '' then szQR.Version:= 1
    else szQR.Version:= StrToInt(Params.Values['Version']);
    szQR.Pxmag:= StrToInt(Params.Values['CODESIZE']);
    szQR.PrnDpi:= StrToFloat(Params.Values['PrnDpi']);  //720 dpi / 英寸 = 28.3465 pixels/mm;
    if Params.Values['Diameter'] = '' then szQR.Radius:= 4.25
    else szQR.Radius:= StrToFloat(Params.Values['Diameter']) / 2;
    szQR.Mode:= qrSingle;
    szQR.Numbering:= nbrIfVoid;   //nbrNone, nbrHead, nbrTail, nbrIfVoid
    szQR.BinaryOperation:= True;
    if Params.Values['Rotate'] <> '' then
      szQR.Angle:= StrToInt(Params.Values['Rotate']);
//    szQR.Binary:= True;
    szQR.SymbolEnabled:= True;
//  end;
//  szQR.Code:= '~59~~0D~~5F~~01~~01~~41~~01~~67~~0D~~3B~~27~';
//  szQR.Code:= 'A'#13#10'B'#13'C'#10'D'#9'EF';//~0D~~0D~~0D~~0D~~0D~~0D~~0D~~0D~~0D~~0D~';
  szQR.WriteData(szCode, 11);
  WriteLog(LOG_Debug, 'CodeToBmp',
      Format('%s, %s, %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d',
      [BoolToStr(szQR.SymbolDisped, true), S, szCode[0], szCode[1], szCode[2], szCode[3], szCode[4], szCode[5],
      szCode[6], szCode[7], szCode[8], szCode[9], szCode[10], szCode[11], szCode[12],
      szCode[13], szCode[14], szCode[15], szCode[16]]));

  Result:= szQR.Picture.Bitmap;
//  szQR.Free;
end;

{ TLabelImage }

constructor TLabelImage.Create;
begin
  inherited;
  FBaseBmp:= TBitmap.Create;
  FOrgBmp:= TBitmap.Create;
  FParams:= TVonConfig.Create;
  FLabelValues:= TVonSetting.Create;
  FContent:= TVonConfig.Create;
  FCrypt:= TVonXorCrypt.Create(@KEYS[0], 7);
  FXinCode:= TDelphiZXingQRCode.Create;
  FQR:= TQRCode.Create(nil);
end;

destructor TLabelImage.Destroy;
begin
  FQR.Free;
  FXinCode.Free;
  FCrypt.Free;
  FContent.Free;
  FLabelValues.Free;
  FParams.Free;
  FOrgBmp.Free;
  FBaseBmp.Free;
  inherited;
end;

function Encode(Key: Byte; org: Int64Rec): Int64Rec;
var
  I, Idx: Integer;
begin
  Idx:= Key;
  Result.Data:= 0;
  for I := 0 to 5 do begin
    if Idx > 255 then Idx:= Idx - 256;
    Result.Bytes[I]:= org.Bytes[I] xor KEY_VALUES[Idx];
    Inc(Idx);
  end;
  Result.Bytes[6]:= Key;
end;

function TLabelImage.QRCode(Code: Int64; Params: TStrings): TBitmap;
var
  codeValue, dest: Int64Rec;
  Key: Byte;
  radius, dpi, zoom: Extended;
  R, C, W1, W2: Integer;
begin
  codeValue.Data:= Code;
  Key:= Random(255);
  dest:= Encode(Key, codeValue);
  FInfo:= Format('0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x',
    [dest.Bytes[0],dest.Bytes[1],dest.Bytes[2],dest.Bytes[3],dest.Bytes[4],dest.Bytes[5],dest.Bytes[6]]);
  dest:= Encode(dest.Bytes[6] xor KEY_VALUES[dest.Bytes[0]], dest);
  FInfo:= Format('0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x',
    [dest.Bytes[0],dest.Bytes[1],dest.Bytes[2],dest.Bytes[3],dest.Bytes[4],dest.Bytes[5],dest.Bytes[6]]);
  dest:= Encode(dest.Bytes[6] xor KEY_VALUES[dest.Bytes[0]], dest);
  FInfo:= FInfo + #13#10 + Format('0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x',
    [dest.Bytes[0],dest.Bytes[1],dest.Bytes[2],dest.Bytes[3],dest.Bytes[4],dest.Bytes[5],dest.Bytes[6]]);
  dest:= Encode(dest.Bytes[6] xor KEY_VALUES[dest.Bytes[0]], dest);
  FInfo:= FInfo + #13#10 + Format('0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x',
    [dest.Bytes[0],dest.Bytes[1],dest.Bytes[2],dest.Bytes[3],dest.Bytes[4],dest.Bytes[5],dest.Bytes[6]]);
  FXinCode.Data:=
    Char((dest.Bytes[0] and $F0) + (KEYS[0] and $0F)) +
    Char((dest.Bytes[0] and $0F) + (KEYS[0] and $F0)) +
    Char((dest.Bytes[1] and $F0) + (KEYS[1] and $0F)) +
    Char((dest.Bytes[1] and $0F) + (KEYS[1] and $F0)) +
    Char((dest.Bytes[2] and $F0) + (KEYS[2] and $0F)) +
    Char((dest.Bytes[2] and $0F) + (KEYS[2] and $F0)) +
    Char((dest.Bytes[3] and $F0) + (KEYS[3] and $0F)) +
    Char((dest.Bytes[3] and $0F) + (KEYS[3] and $F0)) +
    Char((dest.Bytes[4] and $F0) + (KEYS[4] and $0F)) +
    Char((dest.Bytes[4] and $0F) + (KEYS[4] and $F0)) +
    Char((dest.Bytes[5] and $F0) + (KEYS[5] and $0F)) +
    Char((dest.Bytes[5] and $0F) + (KEYS[5] and $F0)) +
    Char((dest.Bytes[6] and $F0) + (KEYS[6] and $0F)) +
    Char((dest.Bytes[6] and $0F) + (KEYS[6] and $F0));
  FInfo:= FInfo + #13#10 + Format('0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x',
    [Ord(FXinCode.Data[1]),Ord(FXinCode.Data[2]),Ord(FXinCode.Data[3]),
    Ord(FXinCode.Data[4]),Ord(FXinCode.Data[5]),Ord(FXinCode.Data[6]),
    Ord(FXinCode.Data[7]),Ord(FXinCode.Data[8]),Ord(FXinCode.Data[9]),
    Ord(FXinCode.Data[10]),Ord(FXinCode.Data[11]),Ord(FXinCode.Data[12]),
    Ord(FXinCode.Data[13]),Ord(FXinCode.Data[14])]);
  FXinCode.Encoding := qrISO88591;
  FXinCode.QuietZone := 0;

  radius:= StrToFloat(Params.Values['Diameter']) / 2;
  dpi:= StrToFloat(Params.Values['PrnDpi']);
  zoom:= StrToInt(Params.Values['CODESIZE']);
  Result:= TBitmap.Create;
  Result.Height:= Round(FXinCode.Rows * zoom);
  Result.Width:= Round(ArcSin(FXinCode.Columns * zoom / 2 / radius / dpi) * radius * dpi * 2);
  Result.Canvas.FillRect(Rect(0, 0, Result.Width, Result.Height));
  for R:= 0 to FXinCode.Rows - 1 do
    for C:= 0 to FXinCode.Columns - 1 do begin
      if FXinCode.IsBlack[R, C] then
        Result.Canvas.Brush.Color:= clBlack
      else Result.Canvas.Brush.Color:= clWhite;
      W1:= Round(Result.Width / 2 + ArcSin((C - FXinCode.Columns / 2) * zoom / radius / dpi) * radius * dpi);
      W2:= Round(Result.Width / 2 + ArcSin((C - FXinCode.Columns / 2 + 1) * zoom / radius / dpi) * radius * dpi);
      WriteLog(LOG_DEBUG, 'QRCode', Format('%d', [W1- W2]));
      Result.Canvas.FillRect(Rect(W1, Round(R * zoom), W2, Round((R + 1)* zoom)));
    end;
end;

function TLabelImage.QRRoateCode(Code: Int64; radius, dpi, zoom: Extended): TBitmap;
var
  codeValue, dest: Int64Rec;
  Key: Byte;
  R, C, W1, W2: Integer;
begin
  codeValue.Data:= Code;
  Key:= Random(255);
  dest:= Encode(Key, codeValue);
  FInfo:= Format('0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x',
    [dest.Bytes[0],dest.Bytes[1],dest.Bytes[2],dest.Bytes[3],dest.Bytes[4],dest.Bytes[5],dest.Bytes[6]]);
  dest:= Encode(dest.Bytes[6] xor KEY_VALUES[dest.Bytes[0]], dest);
  FInfo:= Format('0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x',
    [dest.Bytes[0],dest.Bytes[1],dest.Bytes[2],dest.Bytes[3],dest.Bytes[4],dest.Bytes[5],dest.Bytes[6]]);
  dest:= Encode(dest.Bytes[6] xor KEY_VALUES[dest.Bytes[0]], dest);
  FInfo:= FInfo + #13#10 + Format('0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x',
    [dest.Bytes[0],dest.Bytes[1],dest.Bytes[2],dest.Bytes[3],dest.Bytes[4],dest.Bytes[5],dest.Bytes[6]]);
  dest:= Encode(dest.Bytes[6] xor KEY_VALUES[dest.Bytes[0]], dest);
  FInfo:= FInfo + #13#10 + Format('0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x',
    [dest.Bytes[0],dest.Bytes[1],dest.Bytes[2],dest.Bytes[3],dest.Bytes[4],dest.Bytes[5],dest.Bytes[6]]);
  FXinCode.Data:=
    Char((dest.Bytes[0] and $F0) + (KEYS[0] and $0F)) +
    Char((dest.Bytes[0] and $0F) + (KEYS[0] and $F0)) +
    Char((dest.Bytes[1] and $F0) + (KEYS[1] and $0F)) +
    Char((dest.Bytes[1] and $0F) + (KEYS[1] and $F0)) +
    Char((dest.Bytes[2] and $F0) + (KEYS[2] and $0F)) +
    Char((dest.Bytes[2] and $0F) + (KEYS[2] and $F0)) +
    Char((dest.Bytes[3] and $F0) + (KEYS[3] and $0F)) +
    Char((dest.Bytes[3] and $0F) + (KEYS[3] and $F0)) +
    Char((dest.Bytes[4] and $F0) + (KEYS[4] and $0F)) +
    Char((dest.Bytes[4] and $0F) + (KEYS[4] and $F0)) +
    Char((dest.Bytes[5] and $F0) + (KEYS[5] and $0F)) +
    Char((dest.Bytes[5] and $0F) + (KEYS[5] and $F0)) +
    Char((dest.Bytes[6] and $F0) + (KEYS[6] and $0F)) +
    Char((dest.Bytes[6] and $0F) + (KEYS[6] and $F0));
  FInfo:= FInfo + #13#10 + Format('0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x,0x%2x',
    [Ord(FXinCode.Data[1]),Ord(FXinCode.Data[2]),Ord(FXinCode.Data[3]),
    Ord(FXinCode.Data[4]),Ord(FXinCode.Data[5]),Ord(FXinCode.Data[6]),
    Ord(FXinCode.Data[7]),Ord(FXinCode.Data[8]),Ord(FXinCode.Data[9]),
    Ord(FXinCode.Data[10]),Ord(FXinCode.Data[11]),Ord(FXinCode.Data[12]),
    Ord(FXinCode.Data[13]),Ord(FXinCode.Data[14])]);
  FXinCode.Encoding := qrISO88591;
  FXinCode.QuietZone := 0;

  Result:= TBitmap.Create;
  Result.Width:= Round(FXinCode.Rows * zoom);
  Result.Height:= Round(ArcSin(FXinCode.Columns * zoom / 2 / radius / dpi) * radius * dpi * 2);
  Result.Canvas.FillRect(Rect(0, 0, Result.Width, Result.Height));
  for R:= 0 to FXinCode.Rows - 1 do
    for C:= 0 to FXinCode.Columns - 1 do begin
      if FXinCode.IsBlack[R, C] then
        Result.Canvas.Brush.Color:= clBlack
      else Result.Canvas.Brush.Color:= clWhite;
      W1:= Round(Result.Height / 2 - ArcSin((C - FXinCode.Columns / 2) * zoom / radius / dpi) * radius * dpi);
      W2:= Round(Result.Height / 2 - ArcSin((C - FXinCode.Columns / 2 + 1) * zoom / radius / dpi) * radius * dpi);
      WriteLog(LOG_DEBUG, 'QRCode', Format('%d', [W1- W2]));
      Result.Canvas.FillRect(Rect(Round(R * zoom), W1, Round((R + 1)* zoom), W2));
    end;
end;

function TLabelImage.QRCode(Code: PChar; Params: TStrings): TBitmap;
var
  szI: Integer;
  szCode: array[0..16] of byte;
  digest: array[0..19] of byte;
  S: string;
  Hash: TDCP_sha1;
begin
  S:= string(Code);
  if Length(S) < 12 then S:= Copy(S + '000000000000', 1, 12);                                           //123456000000
  if not TryStrToInt(Copy(S, 1, 4), szI) then Exit;       //Year -> 7F                                  //1234
  szCode[0]:= (szI + 1) and $7F;                          //1 1111111        FF                         //4D2->1001 1010010 0x52    1101101
  if not TryStrToInt(Copy(S, 5, 2), szI) then Exit;       //Area -> 7F                                  //56
  szCode[1]:= szI and $7F;                                //1 111111                                    //38 ->0000 0111000 0x38
  if not TryStrToInt(Copy(S, 7, MaxInt), szI) then Exit;  //Code -> 7FFFFF 1111111 11111111 11111111    //                  0x00
  szCode[2]:= (szI shr 16) and $7F;                       //        FFFF           1111111 1 11111111   //                  0x00
  szCode[3]:= (szI shr 9) and $7F;                        //        7F                     1111111 11   //                  0x00
  szCode[4]:= (szI shr 2) and $7F;                        //        7F                             11   //                  0x00
  szCode[5]:= (szI and $3) shl 5;                                                                       //                  0x00
  szCode[6]:=  Ord('@');                                                                                //                  0x00
  szCode[7]:=  Ord('j');                                                                                //                  0x00
  szCode[8]:=  Ord('A');                                                                                //                  0x00
  szCode[9]:=  Ord('m');                                                                                //                  0x00
  szCode[10]:= Ord('E');                                                                                //                  0x00
  szCode[11]:= Ord('5');                                                                                //                  0x00
  szCode[12]:= Ord('_');                                                                                //                  0x00
  szCode[13]:= Ord('v');                                                                                //                  0x00
  szCode[14]:= Ord('0');                                                                                //                  0x00
  szCode[15]:= Ord('N');                                                                                //                  0x00
  szCode[16]:= Ord('%');                                                                                //                  0x00
//  WriteLog(LOG_Debug, 'orgCode',
//      Format('%s, %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d',
//      [S, szCode[0], szCode[1], szCode[2], szCode[3], szCode[4], szCode[5],
//      szCode[6], szCode[7], szCode[8], szCode[9], szCode[10], szCode[11], szCode[12],
//      szCode[13], szCode[14], szCode[15], szCode[16]]));
  Hash := TDCP_sha1.Create(nil);  // create the hash
  Hash.Init;                      // initialize it
  Hash.Update(szCode[0], 17);     // hash the stream contents
  Hash.Final(Digest);             // produce the digest
  Hash.Free;
  for szI := 0 to 9 do
    Digest[szI]:= Digest[szI] xor Digest[19 - szI];
  szCode[5]:= szCode[5] + Digest[0] shr 3;                    //11 11111      0
  szCode[6]:= (Digest[0] and $7) shl 4 + Digest[1] shr 4;     //111 1111      0 1
  szCode[7]:= (Digest[1] and $F) shl 3 + Digest[2] shr 5;     //1111 111      1 2
  szCode[8]:= (Digest[2] and $1F) shl 2 + Digest[3] shr 6;    //11111 11      2 3
  szCode[9]:= (Digest[3] and $3F) shl 1 + Digest[4] shr 7;    //111111 1      3 4
  szCode[10]:= (Digest[4] and $7F);                           //1111111       4
  szCode[11]:= (Digest[5] and $FF) shr 1;                     //1111111       5
  szCode[12]:= (Digest[5] and $1) shl 6 + Digest[6] shr 2;    //1 111111      5 6
  szCode[13]:= (Digest[6] and $3) shl 5 + Digest[7] shr 3;    //11 11111      6 7
  szCode[14]:= (Digest[7] and $7) shl 4 + Digest[8] shr 4;    //111 1111      7 8
  szCode[15]:= (Digest[8] and $F) shl 3 + Digest[9] shr 5;    //1111 111      8 9
  szCode[16]:= (Digest[9] and $1F) shl 2 + Digest[10] shr 6;  //11111 11      9 A
  ChangeBytes(szCode);
  FQR.SymbolEnabled:= False;
  FQR.ClearOption:= True;
  FQR.SymbolColor:= clBlack;
  FQR.BackColor:= clWhite;
  FQR.Match:= True;
  if Params.Values['Version'] = '' then FQR.Version:= 1
  else FQR.Version:= StrToInt(Params.Values['Version']);
  FQR.Pxmag:= StrToInt(Params.Values['CODESIZE']);
  FQR.PrnDpi:= StrToFloat(Params.Values['PrnDpi']);  //720 dpi / 英寸 = 28.3465 pixels/mm;
  if Params.Values['Diameter'] = '' then FQR.Radius:= 4.25
  else FQR.Radius:= StrToFloat(Params.Values['Diameter']) / 2;
  FQR.Mode:= qrSingle;
  FQR.Numbering:= nbrIfVoid;   //nbrNone, nbrHead, nbrTail, nbrIfVoid
  FQR.BinaryOperation:= True;
  if Params.Values['Rotate'] <> '' then
    FQR.Angle:= StrToInt(Params.Values['Rotate']);
//    FQR.Binary:= True;
  FQR.SymbolEnabled:= True;
  FQR.WriteData(szCode, 11);
//  WriteLog(LOG_Debug, 'CodeToBmp',
//      Format('%s, %s, %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d',
//      [BoolToStr(szQR.SymbolDisped, true), S, szCode[0], szCode[1], szCode[2], szCode[3], szCode[4], szCode[5],
//      szCode[6], szCode[7], szCode[8], szCode[9], szCode[10], szCode[11], szCode[12],
//      szCode[13], szCode[14], szCode[15], szCode[16]]));
  Result:= FQR.Picture.Bitmap;
end;

function TLabelImage.GetContent: TVonConfig;
begin
  Result:= FContent;
end;

function TLabelImage.GetCopyMode(ModeID: Integer): TCopyMode;
begin
  case ModeID of
  0: Result:= cmBlackness;  //Fills the destination rectangle on the canvas with black.
  1: Result:= cmDstInvert;  //Inverts the image on the canvas and ignores the source.
  2: Result:= cmMergeCopy;  //Combines the image on the canvas and the source bitmap by using the Boolean AND operator.
  3: Result:= cmMergePaint; //Combines the inverted source bitmap with the image on the canvas by using the Boolean OR operator.
  4: Result:= cmNotSrcCopy; //Copies the inverted source bitmap to the canvas.
  5: Result:= cmNotSrcErase;//Combines the image on the canvas and the source bitmap by using the Boolean OR operator, and inverts the result.
  6: Result:= cmPatCopy;    //Copies the source pattern to the canvas.
  7: Result:= cmPatInvert;  //Combines the source pattern with the image on the canvas using the Boolean XOR operator
  8: Result:= cmPatPaint;   //Combines the inverted source bitmap with the source pattern by using the Boolean OR operator. Combines the result of this operation with the image on the canvas by using the Boolean OR operator.
  9: Result:= cmSrcAnd;     //Combines the image on the canvas and source bitmap by using the Boolean AND operator.
  10: Result:= cmSrcCopy;   //Copies the source bitmap to the canvas.
  11: Result:= cmSrcErase;  //Inverts the image on the canvas and combines the result with the source bitmap by using the Boolean AND operator.
  12: Result:= cmSrcInvert; //Combines the image on the canvas and the source bitmap by using the Boolean XOR operator.
  13: Result:= cmSrcPaint;  //Combines the image on the canvas and the source bitmap by using the Boolean OR operator.
  14: Result:= cmWhiteness; //Fills the destination rectangle on the canvas with white.
  end;
end;

procedure TLabelImage.SetOrgBmp(const Value: TBitmap);
begin
  FOrgBmp.Assign(Value);
  SetPixel(FPixel);
end;

procedure TLabelImage.SetPixel(const Value: Integer);
begin
  FPixel := Value;
  case FPixel of
  0: FOrgBmp.PixelFormat:= pf1bit;
  1: FOrgBmp.PixelFormat:= pf4bit;
  2: FOrgBmp.PixelFormat:= pf8bit;
  3: FOrgBmp.PixelFormat:= pf24bit;
  4: FOrgBmp.PixelFormat:= pf32bit;
  end;
end;

function TLabelImage.SetCodeValue(Value: string): PChar;
var
  Data, hiData: Cardinal;
  i: Integer;
begin
  Value:= Space(Length(Value) mod 4, '0') + Value;
  hiData:= 0;
  Result:= StrAlloc((Length(Value) div 4) + 1);
  for I := 0 to Length(Value) div 4 - 1 do begin
    Data:= (HexToInt(Copy(Value, I * 4 + 1, 4)) + hiData + 2) and $1FFFF - 1;
    Result[I]:= Char(Data and $FFFF);
    hiData:= Data shr 16;
  end;
  Result[Length(Value) div 4]:= Char(hiData);
end;

procedure TLabelImage.Zoom(VRate, HRate: Integer; srcBmp, destBmp: TBitmap);
begin
  if (VRate = 100)and(HRate = 100) then destBmp.Assign(srcBmp)
  else begin
    destBmp.Height:= Round(srcBmp.Height * VRate / 100);
    destBmp.Width:= Round(srcBmp.Width * HRate / 100);
    destBmp.Canvas.StretchDraw(Rect(0, 0, destBmp.Width, destBmp.Height), srcBmp);
  end;
end;

procedure TLabelImage.Rotate(RotateType: Integer; srcBmp, destBmp: TBitmap);
var
  X, Y: Integer;
  procedure InitDest(W, H: Integer);
  begin
    destBmp.Width:= W;
    destBmp.Height:= H;
    destBmp.Canvas.FillRect(Rect(0, 0, W, H));
  end;
begin
  case RotateType of
  0: begin                                //不旋转
      InitDest(srcBmp.Width, srcBmp.Height);
      destBmp.Assign(srcBmp);
    end;
  1: begin                                //90度旋转
      InitDest(srcBmp.Height, srcBmp.Width);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[srcBmp.Height - 1 - Y, X]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  2: begin                                //180度旋转
      InitDest(srcBmp.Width, srcBmp.Height);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[srcBmp.Width - 1 - X, srcBmp.Height - Y - 1]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  3: begin                                //270度旋转
      InitDest(srcBmp.Height, srcBmp.Width);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[Y, srcBmp.Width - 1 - X]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  4: begin                                //水平翻转
      InitDest(srcBmp.Width, srcBmp.Height);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[srcBmp.Width - 1 - X, Y]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  5: begin                                //90度翻转
      InitDest(srcBmp.Height, srcBmp.Width);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[Y,X]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  6: begin                                //垂直翻转
      InitDest(srcBmp.Width, srcBmp.Height);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[X, srcBmp.Height - Y - 1]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  7: begin                                //270度翻转
      InitDest(srcBmp.Height, srcBmp.Width);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[srcBmp.Height - Y - 1, srcBmp.Width - 1 - X]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  end;
end;

procedure TLabelImage.Drew(bmp: TBitmap; Text: string);
var
  I: Integer;
  S: string;
begin
  if bmp = nil then Exit;
//  bmp.Width:= FBaseBmp.Width;
//  bmp.Height:= FBaseBmp.Height;
//  Bmp.Canvas.FillRect(Rect(0, 0, Bmp.Width, Bmp.Height));
  bmp.Assign(FBaseBmp);
  for I := 0 to FParams.SectionCount - 1 do begin
    case FParams.ReadInteger(FParams.Sections[I], 'Kind', 0) of
    0: DrewTextBmp(bmp, FParams.Sections[I], Text);
    1: DrewGrpBmp(bmp, FParams.Sections[I], Text);
    2: DrewCodeBmp(bmp, FParams.Sections[I], Text);
    end;
  end;
end;

procedure TLabelImage.DrewBaseBmp;
var
  I: Integer;
  S: string;
begin
  FBaseBmp.Canvas.FillRect(Rect(0,0,FOrgBmp.Width,FOrgBmp.Height));
  FBaseBmp.Assign(FOrgBmp);
  case FPixel of
  0: FBaseBmp.PixelFormat:= pf1bit;
  1: FBaseBmp.PixelFormat:= pf4bit;
  2: FBaseBmp.PixelFormat:= pf8bit;
  3: FBaseBmp.PixelFormat:= pf24bit;
  4: FBaseBmp.PixelFormat:= pf32bit;
  end;
  FParams.Clear;
  FBaseBmp.Canvas.Lock;
  for I := 0 to FContent.SectionCount - 1 do begin
    S:= FLabelValues.NameValue[FContent.Sections[I]];
    if S = '' then S:= FContent.ReadString(FContent.Sections[I], 'ExampleText', '');
    if not FContent.ReadBool(FContent.Sections[I], 'SerialNo', False) then begin
      case FContent.ReadInteger(FContent.Sections[I], 'Kind', 0) of
      0: DrewTextBmp(FBaseBmp, FContent.Sections[I], S);
      1: DrewGrpBmp(FBaseBmp, FContent.Sections[I], S);
      2: DrewCodeBmp(FBaseBmp, FContent.Sections[I], S);
      end;
    end else FParams.WriteSection(FContent.Sections[I], FContent.GetDirectionSection(I));
  end;
  FBaseBmp.Canvas.unLock;
  FOrgBmp.SaveToFile(ExtractFilePath(Application.ExeName) + 'ORG.BMP');
  FBaseBmp.SaveToFile(ExtractFilePath(Application.ExeName) + 'BAS.BMP');
end;

procedure TLabelImage.DrewCodeBmp(bmp: TBitmap; SectionName, Value: string);
var
  rotateBmp, szBmp: TBitmap;
  i: Integer;
  szList: TStringList;
  S, szValue: string;
begin
  WriteLog(LOG_Debug, 'DrewCodeBmp', SectionName);
  if(SectionName = '')or(Value = '')then Exit;
  szList:= TStringList.Create;
  szList.Delimiter:= '+';
  rotateBmp:= TBitmap.Create;
  with FContent.GetDirectionSection(SectionName) do try
    szList.DelimitedText:= Values['CodeValue'];
    szValue:= '';
    for I := 0 to szList.Count - 1 do begin
      S:= FLabelValues.NameValue[szList[I]];
      if S = '' then S:= FContent.ReadString(szList[I], 'ExampleText', '0');
      szValue:= szValue + S;
    end;
    szValue:= szValue + Value;
    szBmp:= QRCode(strToInt64(szValue), FContent.GetDirectionSection(SectionName));
    if(Values['Rotate'] <> '')and(StrToInt(Values['Rotate']) <> 0)then
      Rotate(StrToInt(Values['Rotate']), szBmp, rotateBmp)
    else rotateBmp.Assign(szBmp);
    Bmp.Canvas.CopyMode:= GetCopyMode(StrToInt(Values['CopyMode']));
    Bmp.Canvas.Draw(StrToInt(Values['Left']), StrToInt(Values['Top']), szBmp);  //rotateBmp
  finally
    rotateBmp.Free;
    szList.Free;
  end;
end;

procedure TLabelImage.DrewGrpBmp(bmp: TBitmap; SectionName, Value: string);
var
  szBmp, zoonBmp: TBitmap;
begin
  szBmp:= TBitmap.Create;
  zoonBmp:= TBitmap.Create;
  with FContent.GetDirectionSection(SectionName) do try
    szBmp.LoadFromFile(Value);
    Zoom(FContent.ReadInteger(SectionName, 'VRate', 100),
      FContent.ReadInteger(SectionName, 'HRate', 100), szBmp, zoonBmp);
    Bmp.Canvas.CopyMode:= GetCopyMode(StrToInt(Values['CopyMode']));
    Bmp.Canvas.Draw(StrToInt(Values['Left']), StrToInt(Values['Top']), zoonBmp);
    zoonBmp.Canvas.Unlock;
  finally
    szBmp.Free;
    zoonBmp.Free;
  end;
end;

procedure TLabelImage.DrewTextBmp(bmp: TBitmap; SectionName, Value: string);
var
  szBmp, rotateBmp: TBitmap;
  i: Integer;
begin
  WriteLog(LOG_Debug, 'DrewTextBmp', SectionName);
  if(SectionName = '')or(Value = '')then Exit;
  szBmp:= TBitmap.Create;
  rotateBmp:= TBitmap.Create;
  with szBmp.Canvas, FContent.GetDirectionSection(SectionName) do try
    Brush.Color:= StrToInt(Values['BKCOLOR']);
    with Font do begin
      Name:= Values['FontName'];
      Size:= StrToInt(Values['FontSize']);
      Color:= StrToInt(Values['FONTCOLOR']);
      Style:= [];
      if Values['Bold'] = '1' then Style:= Style + [fsBold];
      if Values['Italic'] = '1' then Style:= Style + [fsItalic];
      if Values['Underline'] = '1' then Style:= Style + [fsUnderline];
      if Values['StrikeOut'] = '1' then Style:= Style + [fsStrikeOut];
    end;
    szBmp.Width:= Max(StrToInt(Values['LEN']) * StrToInt(Values['WordWidth']),
      Length(Value) * StrToInt(Values['WordWidth']));
    szBmp.Height:= TextHeight('Yj');
    FillRect(Rect(0, 0, szBmp.Width, szBmp.Height));
    case StrToInt(Values['Align']) of
    0: for i:= 1 to Length(Value) do                                            //左对齐
      TextOut(StrToInt(Values['WordWidth']) * (i - 1), 0, Value[i]);
    1: for i:= 1 to Length(Value) do                                            //居中
      TextOut(StrToInt(Values['WordWidth']) * (StrToInt(Values['LEN']) -
        Length(Value)) div 2 + StrToInt(Values['WordWidth']) * (i - 1), 0, Value[i]);
    2: for i:= Length(Value) downto 1 do                                        //右对齐
      TextOut(StrToInt(Values['WordWidth']) *
        StrToInt(Values['LEN']) - StrToInt(Values['WordWidth']) * i, 0, Value[i]);
    3: if Length(Value) = 1 then                                                //
      TextOut(StrToInt(Values['WordWidth']) * (StrToInt(Values['LEN']) - Length(Value)) div 2, 0, Value[i])
    else for i:= 1 to Length(Value) do                                          //
      TextOut(StrToInt(Values['WordWidth']) * (StrToInt(Values['LEN']) - Length(Value)) * (i - 1), 0, Value[i]);
    end;
    Zoom(FContent.ReadInteger(SectionName, 'VRate', 100),
      FContent.ReadInteger(SectionName, 'HRate', 100), szBmp, rotateBmp);       //Zoom   szBmp -> rotateBmp
    if StrToInt(Values['Rotate']) <> 0 then
      Rotate(StrToInt(Values['Rotate']), rotateBmp, szBmp);                     //Rotate rotateBmp -> szBmp
    Bmp.Canvas.CopyMode:= GetCopyMode(StrToInt(Values['CopyMode']));
    Bmp.Canvas.Draw(StrToInt(Values['Left']), StrToInt(Values['Top']), szBmp);
    //Img3.Picture.Assign(rotateBmp);
  finally
    szBmp.Free;
    rotateBmp.Free;
  end;
end;

{ TLabelPrint }

constructor TLabelPrint.Create;
begin
  FItems:= TStringList.Create;
  FCurrentLabel:= 0;
  TempBitmap:= TBitmap.Create;
end;

destructor TLabelPrint.Destroy;
begin
  TempBitmap.Free;
  FItems.Free;
end;

function TLabelPrint.ThreadCanvas(Canvas: TCanvas; MaxLabels: Integer;
  Rate: Real): Boolean;
var
  CurrentSection, CurrentRow, LabelRowNumber,
  CurrentCol, CurrentX, CurrentY: Cardinal;
  szi, i, j: Cardinal;
begin
  Canvas.Lock;
  FCurrentID:= 0;
  for CurrentSection:= 0 to Min((Items.Count - 1) div (ColCount * RowCount), SectionCount - 1) do begin
    LabelRowNumber:= Items.Count;
    for CurrentCol:= 1 to ColCount do begin
      if (LabelRowNumber mod ColCount) div CurrentCol > 0 then
        szi:= Min(LabelRowNumber div ColCount + 1, RowCount)
      else szi:= Min(LabelRowNumber div ColCount, RowCount);
      for CurrentRow:= 1 to szi do
        with Canvas do begin
          //页边距+当前标签X位置
          CurrentX:= Round((LeftMargin + (CurrentCol - 1) * (LabelWidth + ColSpace)) * Rate);
          //顶高+段高+段间距+当前标签Y位置
          CurrentY:= Round((TopMargin + CurrentSection * RowCount * (LabelHeight + RowSpace) +
              CurrentSection * FLAGDISTANCE + (CurrentRow - 1) * (LabelHeight + RowSpace) + RowSpace / 2) * Rate);
          LabelImage.Drew(TempBitmap, Items[0]);
          Items.Delete(0);
          FillRect(Rect(CurrentX, CurrentY, CurrentX + TempBitmap.Width, CurrentY + TempBitmap.Height));
          Draw(CurrentX, CurrentY, TempBitmap);
          Inc(FCurrentID);
        end;
    end;
  end;
  Canvas.unLock;
  Inc(FCurrentPage);
  Result:= Items.Count > 0;
end;

function TLabelPrint.DrewCanvas(Canvas: TCanvas; MaxLabels: Integer;
  Rate: Real): Boolean;
var
  CurrentSection, CurrentRow, LabelRowNumber,
  CurrentCol, CurrentX, CurrentY: Cardinal;
  szi, i, j: Cardinal;
  dRect: TRect;
begin
  FCurrentID:= 0;
  Canvas.FillRect(Rect(0, 0, 7 * TempBitmap.Width, 7 * TempBitmap.Height));
  DrewCuttingFlag(Canvas, 0, 0, Rate, EP_TOP);
  for CurrentSection:= 0 to Min((Items.Count - 1) div (ColCount * RowCount), SectionCount - 1) do begin
    LabelRowNumber:= Min(Items.Count div ColCount + (LabelRowNumber mod ColCount), RowCount);
    DrewCuttingFlag(Canvas, CurrentSection , LabelRowNumber, Rate, EP_Section);
    //DrewFlag(Canvas, CurrentSection , LabelRowNumber, Rate);   //画横纵线
    LabelRowNumber:= Items.Count;
    for CurrentCol:= 1 to ColCount do begin
      if (LabelRowNumber mod ColCount) div CurrentCol > 0 then
        szi:= Min(LabelRowNumber div ColCount + 1, RowCount)
      else szi:= Min(LabelRowNumber div ColCount, RowCount);
      for CurrentRow:= 1 to szi do
        with Canvas do begin
          //页边距+当前标签X位置
          CurrentX:= Round((LeftMargin + (CurrentCol - 1) * (LabelWidth + ColSpace)) * Rate);
          //顶高+段高+段间距+当前标签Y位置
          CurrentY:= Round((TopMargin + CurrentSection * RowCount * (LabelHeight + RowSpace) +
              CurrentSection * FLAGDISTANCE + (CurrentRow - 1) * (LabelHeight + RowSpace) + RowSpace / 2) * Rate);
          LabelImage.Drew(TempBitmap, Items[0]);
          dRect:= Rect(CurrentX, CurrentY, CurrentX + Round(TempBitmap.Width * Rate),
            CurrentY + Round(TempBitmap.Height * Rate));
          FillRect(dRect);
          StretchDraw(dRect, TempBitmap);
          //TempBitmap.SaveToFile(FFlyLabelDB.AppPath + 'Temp\' + Items[0] + '.bmp');
          Items.Delete(0);
          //Draw(CurrentX, CurrentY, TempBitmap);
          Inc(FCurrentID);
          //TempBitmap.FreeImage;
          //TempBitmap.Free;
          PrintLabelFinally;
        end;
    end;
//    Sleep(1000);
  end;
  DrewCuttingFlag(Canvas, CurrentSection , LabelRowNumber, Rate, EP_Section);
  Inc(FCurrentPage);
  Result:= Items.Count > 0;
end;

procedure TLabelPrint.DrewHLine(Canvas: TCanvas; SectionID, RowNumber: Integer; Rate: real);
var
  i, X, X1, X2, Y: Cardinal;
begin     //划水平线
  with Canvas do begin
    //顶高+半个标线高+段高*段行数*标签高+
    Y:= Round((TopMargin + FLAGDISTANCE div 2 + SectionID * RowCount * (LabelHeight + RowSpace) +
      SectionID * FLAGDISTANCE + RowNumber * (LabelHeight + RowSpace)) * Rate);
    X1:= Round((LeftMargin - FLAGDISTANCE div 2) * Rate);
    X2:= Round((LeftMargin + FLAGDISTANCE div 2 + ColCount * (LabelWidth + ColSpace)) * Rate);
    MoveTo(X1, Y); LineTo(X2, Y);
    X:= Round((LeftMargin + FLAGDISTANCE div 2 + 2 * (LabelWidth + ColSpace)) * Rate);
    //Brush.Color:= clBlack;
    //FillRect(Rect(X - 114, Y - 14, X, Y + 14));
    //Brush.Color:= clWhite;
    for i:= 0 to ColCount do begin
      X:= Round((LeftMargin + i * (LabelWidth + ColSpace)) * Rate);
      MoveTo(X, Round((Y - FLAGLONG div 2) * Rate));
      LineTo(X, Round((Y + FLAGLONG div 2) * Rate));
    end;
  end;
end;

procedure TLabelPrint.DrewVLine(Canvas: TCanvas; SectionID, RowNumber: Integer; Rate: real);
var
  i, X1, X2, Y: Cardinal;
begin     //划垂直线
  with Canvas do begin
    X1:= LeftMargin - FLAGDISTANCE div 2;
    X2:= LeftMargin + FLAGDISTANCE div 2 + ColCount * (LabelWidth + ColSpace);
    for i:= 0 to RowNumber do begin
      Y:= Round((TopMargin + SectionID * RowCount * (LabelHeight + RowSpace) +
        SectionID * FLAGDISTANCE + i * (LabelHeight + RowSpace)) * Rate);
      MoveTo(Round((X1 - FLAGLONG div 2) * Rate), Y);
      LineTo(Round((X1 + FLAGLONG div 2) * Rate), Y);
      MoveTo(Round((X2 - FLAGLONG div 2) * Rate), Y);
      LineTo(Round((X2 + FLAGLONG div 2) * Rate), Y);
    end;
  end;
end;

procedure TLabelPrint.Drew_Lifeng_CuttingFlag(Canvas: TCanvas;
  SectionCount: Integer; Rate: real);
const PenWidth = 24;              //1mm=600dpi/25.4=23.622point
var
  i, X, X1, X2, Y1, Y2: Cardinal;
begin     //划标记线
  with Canvas do begin
    Brush.Color:= clBlack;
    // 4.5mm,   18mm
    X1:= Round((LeftMargin - PenWidth / 2) * Rate);
    Y1:= Round((TopMargin - LabelHeight * 3 - RowSpace * 3) * Rate);
    Ellipse(X1, Y1, X1 + PenWidth, Y1 + PenWidth);                    //左上点
    Pen.Width:= PenWidth;
    MoveTo(X1, Y1 + 85); LineTo(X1 + 165, Y1 + 85);                   //左上十字   -
    MoveTo(X1 + 83, Y1); LineTo(X1 + 83, Y1 + 165);                   //左上十字   |
    X2:= Round((LeftMargin + ColCount * (LabelWidth + ColSpace) - ColSpace + PenWidth / 2) * Rate);
    MoveTo(X2, Y1 + 85); LineTo(X2 - 165, Y1 + 85);                   //左上十字   -
    MoveTo(X2, Y1 + 416); LineTo(X2 - 165, Y1 + 416);                 //左上十字   -
    MoveTo(X2 - 83, Y1); LineTo(X2 - 83, Y1 + 496);                   //左上十字   |
    Y2:= Round((TopMargin + (LabelHeight + RowSpace) * 20 + FLAGDISTANCE * SectionCount - FLAGDISTANCE) * Rate);
    MoveTo(X1, Y2 + 85); LineTo(X1 + 165, Y2 + 85);                   //左下十字   -
    MoveTo(X1 + 83, Y2); LineTo(X1 + 83, Y2 + 165);                   //左下十字   |
    MoveTo(X2, Y2 + 85); LineTo(X2 - 165, Y2 + 85);                   //右下十字   -
    MoveTo(X2 - 83, Y2); LineTo(X2 - 83, Y2 + 165);                   //右下十字   |

//    X1:= Round((LeftMargin - PenWidth / 2) * Rate);
//    Y1:= Round((TopMargin - LabelHeight * 3 - RowSpace * 3) * Rate);
//    Ellipse(X1, Y1, X1 + PenWidth, Y1 + PenWidth);                    //左上点
//    Pen.Width:= PenWidth;
//    MoveTo(X1, Y1 + 85); LineTo(X1 + 165, Y1 + 85);                   //左上十字   -
//    MoveTo(X1 + 83, Y1); LineTo(X1 + 83, Y1 + 165);                   //左上十字   |
//    X2:= Round((LeftMargin + ColCount * (LabelWidth + ColSpace) - ColSpace + PenWidth / 2) * Rate);
//    MoveTo(X2, Y1 + 85); LineTo(X2 - 165, Y1 + 85);                   //左上十字   -
//    MoveTo(X2, Y1 + 416); LineTo(X2 - 165, Y1 + 416);                 //左上十字   -
//    MoveTo(X2 - 83, Y1); LineTo(X2 - 83, Y1 + 496);                   //左上十字   |
//    Y2:= Round((TopMargin + (LabelHeight + RowSpace) * 20 + FLAGDISTANCE * SectionCount - FLAGDISTANCE) * Rate);
//    MoveTo(X1, Y2 + 85); LineTo(X1 + 165, Y2 + 85);                   //左下十字   -
//    MoveTo(X1 + 83, Y2); LineTo(X1 + 83, Y2 + 165);                   //左下十字   |
//    MoveTo(X2, Y2 + 85); LineTo(X2 - 165, Y2 + 85);                   //右下十字   -
//    MoveTo(X2 - 83, Y2); LineTo(X2 - 83, Y2 + 165);                   //右下十字   |
    Pen.Width:= 1;
    Brush.Color:= clWhite;
  end;
end;

procedure TLabelPrint.Drew_L_CuttingFlag(Canvas: TCanvas; SectionCount: Integer; Rate: real);
const FlagWidth = 570;
var
  i, X, X1, X2, Y1, Y2: Cardinal;
begin     //划标记线
  with Canvas do begin
    //顶高+半个标线高+段高*段行数*标签高+
    Y1:= Round((TopMargin - FlagDistance div 2) * Rate);
    Y2:= Round((TopMargin - FlagDistance div 2 + SectionCount * FlagDistance + SectionCount * RowCount * (LabelHeight + RowSpace)) * Rate);
    X1:= Round((LeftMargin - FlagDistance div 2 - FPointOffset) * Rate);
    X2:= Round((LeftMargin + FlagDistance div 2 + ColCount * (LabelWidth + ColSpace) + FPointOffset) * Rate);
    Pen.Width:= 28;
    MoveTo(X1, Y1 + FlagWidth); LineTo(X1, Y1); LineTo(X1 + FlagWidth, Y1);
    MoveTo(X2, Y1 + FlagWidth); LineTo(X2, Y1); LineTo(X2 - FlagWidth, Y1);
    MoveTo(X1, Y2 - FlagWidth); LineTo(X1, Y2); LineTo(X1 + FlagWidth, Y2);
    MoveTo(X2, Y2 - FlagWidth); LineTo(X2, Y2); LineTo(X2 - FlagWidth, Y2);
    Pen.Width:= 1;
  end;
end;

procedure TLabelPrint.DrewCircle(Canvas: TCanvas; SectionID,
  RowNumber: Integer; Rate: real);
var
  X1, X2, Y: Cardinal;

  procedure DrewPoint(X, Y: Cardinal; Offset: Integer);
  begin
    with Canvas do begin
      Ellipse(Round((X - 50 + Offset) * Rate), Round((Y - 50) * Rate), Round((X + 50 + Offset) * Rate), Round((Y + 50) * Rate));
      MoveTo(Round((X - 50 + Offset) * Rate), Round(Y * Rate));
      LineTo(Round((X + 50 + Offset) * Rate), Round(Y * Rate));
      MoveTo(Round((X + Offset) * Rate), Round((Y - 50) * Rate));
      LineTo(Round((X + Offset) * Rate), Round((Y + 50) * Rate));
    end;
  end;
begin
  X1:= LeftMargin - FLAGDISTANCE div 2;
  X2:= LeftMargin + FLAGDISTANCE div 2 + ColCount * (LabelWidth + ColSpace);
  Y:= TopMargin + FLAGDISTANCE div 2 + SectionID * RowCount * (LabelHeight + RowSpace) +
    SectionID * FLAGDISTANCE + RowNumber * (LabelHeight + RowSpace);
  DrewPoint(X1, Y, -FPointOffset);
  DrewPoint(X2, Y, +FPointOffset);
end;

procedure TLabelPrint.DrewCuttingFlag(Canvas: TCanvas; SectionID, RowNumber: Integer;
  Rate: real; Possion: ELabelFlagPossion);

  procedure DrewFlag(Canvas: TCanvas; SectionID, RowNumber: Integer; Rate: real);
  begin     //画横纵线
    DrewHLine(Canvas, SectionID, RowNumber, Rate);
    DrewVLine(Canvas, SectionID, RowNumber, Rate);
    DrewCircle(Canvas, SectionID, RowNumber, Rate);
  end;
begin
  case FFlagKind of
  1:
    case Possion of
      ELabelFlagPossion.EP_TOP: DrewFlag(Canvas, -1, RowCount, Rate);             //画横纵线
      ELabelFlagPossion.EP_Section: DrewFlag(Canvas, SectionID , RowNumber, Rate);   //画横纵线
    end;
  2:
    case Possion of
      ELabelFlagPossion.EP_TOP: Drew_L_CuttingFlag(Canvas, SectionCount, Rate);
    end;
  3:
    case Possion of
      ELabelFlagPossion.EP_TOP: Drew_Lifeng_CuttingFlag(Canvas, SectionCount, Rate);
    end;
  end;
end;

procedure TLabelPrint.Preview(ViewCanvas: TCanvas);
begin
  if Items.Count < 1 then Exit;
  DrewCanvas(ViewCanvas, FRowCount * FColCount * FSectionCount, 1);//100 / 600);
end;

procedure TLabelPrint.Print(Prn: TPrinter);
var
  PageBitmap: TBitmap;
  XM, YM: Integer;
begin
  if Items.Count < 1 then Exit;
  with prn do begin
//    Title:= Items[0] + '-' + Items[Min(Items.Count - 1, FRowCount * FColCount * FSectionCount) - 1];
//    BeginDoc;
//    NewPage
//    XM:= Round((LeftMargin + FLAGDISTANCE div 2 + ColCount * (LabelWidth + ColSpace)));
//    YM:= Round((TopMargin + SectionCount * RowCount * (LabelHeight + RowSpace) +
//        SectionCount * FLAGDISTANCE + RowCount * (LabelHeight + RowSpace)));
//    Canvas.FillRect(Rect(0, 0, XM, YM));
//    PageBitmap:= TBitmap.Create;
//    case FPixel of
//      0: PageBitmap.PixelFormat:= pf1bit;
//      1: PageBitmap.PixelFormat:= pf4bit;
//      2: PageBitmap.PixelFormat:= pf8bit;
//      3: PageBitmap.PixelFormat:= pf24bit;
//      4: PageBitmap.PixelFormat:= pf32bit;
//    end;

    //顶高+段高+段间距+当前标签Y位置
//    PageBitmap.Width:= LeftMargin * 2 + ColCount * (LabelWidth + ColSpace);
//    PageBitmap.Height:= TopMargin * 2 + SectionCount * RowCount * (LabelHeight + RowSpace) + SectionCount * FLAGDISTANCE;
//    DrewCanvas(PageBitmap.Canvas, FRowCount * FColCount * FSectionCount, 1);
//    Canvas.CopyRect(Rect(0,0,PageBitmap.Width, PageBitmap.Height), PageBitmap.Canvas, Rect(0,0,PageBitmap.Width, PageBitmap.Height));
//    PageBitmap.Free;
//    PrintPageFinally(prn);
//    ThreadCanvas(Canvas, FRowCount * FColCount * FSectionCount, 1);
    DrewCanvas(Canvas, FRowCount * FColCount * FSectionCount, 0.85);
//    EndDoc;
  end;
  PrintFinally;
  FCurrentLabel:= 0;
end;

procedure TLabelPrint.SaveFile(Filename: string);
var
  PageBitmap: TBitmap;
  MyRect: TRect;
  ID: Integer;
  OrgFileName, FileExt: string;
begin
  if Items.Count < 1 then Exit;
  ID:= 0;
  FileExt:= ExtractFileExt(Filename);
  OrgFileName:= Copy(Filename, 1, Length(Filename) - Length(FileExt));
  PageBitmap:= TBitmap.Create;
  try
    PageBitmap.PixelFormat:= LabelImage.OrgBmp.PixelFormat;
    PageBitmap.Width:= LabelImage.OrgBmp.Width * 7;
    PageBitmap.Height:= LabelImage.OrgBmp.Height * 10;
//    PageBitmap.Width:= Printer.PageWidth;
//    PageBitmap.Height:= Printer.PageHeight;
    MyRect:= Rect(0, 0, Printer.PageWidth, Printer.PageHeight);
    while DrewCanvas(PageBitmap.Canvas, FRowCount * FColCount * FSectionCount, 1) do begin
      PageBitmap.SaveToFile(OrgFileName + IntToStr(ID) + FileExt);
      Inc(ID);
      PrintPageFinally(nil);
    end;
    PageBitmap.SaveToFile(OrgFileName + IntToStr(ID) + FileExt);
    //PrintPageFinally(nil);
  finally
    PageBitmap.Free;
  end;
end;

procedure TLabelPrint.PrintFinally;
begin
  if Assigned(FOnFinally) then
    FOnFinally(Self);
end;

procedure TLabelPrint.PrintLabelFinally;
begin
  if Assigned(FOnEndOfLabel) then
    FOnEndOfLabel(Self, FCurrentID);
end;

procedure TLabelPrint.PrintPageFinally(prn: TPrinter);
begin
  if Assigned(FOnEndOfPage) then
    FOnEndOfPage(prn, FCurrentPage);
end;

procedure TLabelPrint.SetColCount(const Value: Integer);
begin
  FColCount := Value;
end;

procedure TLabelPrint.SetCurrentID(const Value: Integer);
begin
  FCurrentID := Value;
end;

procedure TLabelPrint.SetItems(const Value: TStrings);
begin
  FItems.Assign(Value);
end;

procedure TLabelPrint.SetLabelHeight(const Value: Integer);
begin
  FLabelHeight := Value;
end;

procedure TLabelPrint.SetLabelImage(const Value: TLabelImage);
begin
  FLabelImage := Value;
  if Assigned(FLabelImage) then begin
    TempBitmap.PixelFormat:= FLabelImage.FBaseBmp.PixelFormat;
    TempBitmap.Width:= FLabelImage.FBaseBmp.Width;
    TempBitmap.Height:= FLabelImage.FBaseBmp.Height;
    TempBitmap.Canvas.FillRect(Rect(0,0,TempBitmap.Width,TempBitmap.Height));
  end;
end;

procedure TLabelPrint.SetLabelWidth(const Value: Integer);
begin
  FLabelWidth := Value;
end;

procedure TLabelPrint.SetLeftMargin(const Value: Integer);
begin
  FLeftMargin := Value;
end;

procedure TLabelPrint.SetOnEndOfLabel(const Value: TProcessEvent);
begin
  FOnEndOfLabel := Value;
end;

procedure TLabelPrint.SetOnEndOfPage(const Value: TProcessEvent);
begin
  FOnEndOfPage := Value;
end;

procedure TLabelPrint.SetOnFinally(const Value: TNotifyEvent);
begin
  FOnFinally := Value;
end;

procedure TLabelPrint.SetRowCount(const Value: Integer);
begin
  FRowCount := Value;
end;

procedure TLabelPrint.SetSectionCount(const Value: Integer);
begin
  FSectionCount := Value;
end;

procedure TLabelPrint.SetTopMargin(const Value: Integer);
begin
  FTopMargin := Value;
end;

procedure TLabelPrint.CopyCanvas(OrgCanvas, DestCanvas: TCanvas; Width, Height: Integer);
var
  i, j: Integer;
begin
  for i:= 0 to Height - 1 do
    for j:= 0 to Width - 1 do
      DestCanvas.Pixels[j, i] := OrgCanvas.Pixels[j, i];
end;

procedure TLabelPrint.SetColSpace(const Value: Integer);
begin
  FColSpace := Value;
end;

procedure TLabelPrint.SetRowSpace(const Value: Integer);
begin
  FRowSpace := Value;
end;

procedure TLabelPrint.SetFlagDistance(const Value: Integer);
begin
  FFlagDistance := Value;
end;

procedure TLabelPrint.SetFlagKind(const Value: Integer);
begin
  FFlagKind := Value;
end;

procedure TLabelPrint.SetFlagLong(const Value: Integer);
begin
  FFlagLong := Value;
end;

procedure TLabelPrint.SetPixel(const Value: Integer);
begin
  FPixel := Value;
end;

procedure TLabelPrint.SetPointOffset(const Value: Integer);
begin
  FPointOffset := Value;
end;

{ TLabelThread }

constructor TLabelThread.Create(CreateSuspended: Boolean);
begin
  inherited Create(CreateSuspended);
  lbImage:= TLabelImage.Create;
//  FreeOnTerminate := True;
end;

destructor TLabelThread.Destroy;
begin
  lbImage.Free;
  inherited;
end;

procedure TLabelThread.Execute;
var
  Idx: Integer;
  Code: string;
  bmp: TBitmap;
begin
  inherited;
  WriteLog(LOG_DEBUG, 'TLabelThread', 'begin---------------');
  while GlobalIdx < FLabelItems.Count do begin
    EnterCriticalSection(CriticalSection);    //进入临界区
    Idx:= GlobalIdx;
    Inc(GlobalIdx);
    LeaveCriticalSection(CriticalSection);    //离开临界区
    if Idx >= FLabelItems.Count then break;
    Code:= FLabelItems[Idx];
    bmp:= TBitmap(FLabelItems.Objects[Idx]);
//    bmp:= TBitmap.Create;
    lbImage.Drew(bmp, Code);
//    FLabelItems.Objects[Idx]:= bmp;
    WriteLog(LOG_DEBUG, 'TLabelThread', Format('Print %d(%s)', [Idx, Code]));
  end;
  WriteLog(LOG_DEBUG, 'TLabelThread', '-------------Finally');
  Dec(ThreadRuning);
end;

{ TFastPrint }

constructor TFastPrint.Create(Labels: TStrings; ThreadCount: Integer);
var
  I: Integer;
begin
  WriteLog(LOG_Debug, 'TFastPrint', Format('The FastPrint will print %d labels with %d threads.', [Labels.Count, ThreadCount]));
  inherited Create(True);
  FList:= TList.Create;
  FItems:= TStringList.Create;
  Self.Items:= Labels;
  for I := 1 to ThreadCount do
    FList.Add(TLabelThread.Create(True));
  FreeOnTerminate:= True;
  InitializeCriticalSection(CriticalSection);
end;

destructor TFastPrint.Destroy;
var
  I: Integer;
begin
  WriteLog(LOG_Debug, 'TFastPrint', 'Destroy');
  for I := 0 to FList.Count - 1 do               //释放标签制作进程
    with TLabelThread(FList[I]) do begin
      Terminate;
      Free;
    end;
  DeleteCriticalSection(CriticalSection);
  for I := 0 to FItems.Count - 1 do
    if Assigned(FItems.Objects[I]) then
      FItems.Objects[I].Free;
  FItems.Free;
  FList.Free;
  inherited;
end;

procedure TFastPrint.Execute;
var
  I, CurrentX, CurrentCol, CurrentY, CurrentSection, CurrentRow: Integer;
  bmp: TBitmap;
  function lbBmp: TBitmap;
  begin
    Result:= TBitmap.Create;
    Result.Assign(LabelImage.FBaseBmp);
  end;
begin
  inherited;
  WriteLog(LOG_DEBUG, 'TFastPrint.Execute', 'begin');
  ThreadRuning:= FList.Count;
  for I := 0 to ThreadRuning - 1 do
    FLabelItems.AddObject(FItems[I], lbBmp);
  GlobalIdx:= 0;
  for I := 0 to FList.Count - 1 do
    TLabelThread(FList[I]).Start;
  while ThreadRuning > 0 do begin
//    WriteLog(LOG_DEBUG, 'TFastPrint.Execute', Format('Current idx is %d / %d',
//      [GlobalIdx, FItems.Count]));
  end;
  CurrentX:= 0; CurrentY:= 0;
  Printer.BeginDoc;
  for I := 0 to FItems.Count - 1 do
    with Printer.Canvas do begin
      bmp:= TBitmap(FItems.Objects[I]);
      FillRect(Rect(CurrentX, CurrentY, CurrentX + bmp.Width, CurrentY + bmp.Height));
      Draw(CurrentX, CurrentY, bmp);
      Inc(CurrentX, LabelWidth + ColSpace);
      Inc(CurrentCol);
      if CurrentCol = ColCount then begin
        CurrentX:= 0;
        CurrentCol:= 0;
        Inc(CurrentY, LabelHeight + RowSpace)
      end;
      FItems.Objects[I].Free;
    end;
  Printer.EndDoc;
  FItems.Clear;
  WriteLog(LOG_DEBUG, 'TFastPrint.Execute', 'end');
end;

procedure TFastPrint.SetColCount(const Value: Integer);
begin
  FColCount := Value;
end;

procedure TFastPrint.SetColSpace(const Value: Integer);
begin
  FColSpace := Value;
end;

procedure TFastPrint.SetItems(const Value: TStrings);
begin
  FItems.Assign(Value);
end;

procedure TFastPrint.SetLabelHeight(const Value: Integer);
begin
  FLabelHeight := Value;
end;

procedure TFastPrint.SetLabelImage(const Value: TLabelImage);
var
  I: Integer;
begin
  FLabelImage := Value;
  for I := 0 to FList.Count - 1 do
    with TLabelThread(FList[I]).lbImage do begin
      FBaseBmp.Assign(FLabelImage.FBaseBmp);
      FParams.Assign(FLabelImage.FParams);
      FContent.Assign(FLabelImage.FContent);
      FOrgBmp.Assign(FLabelImage.FOrgBmp);
    end;
end;

procedure TFastPrint.SetLabelWidth(const Value: Integer);
begin
  FLabelWidth := Value;
end;

procedure TFastPrint.SetLeftMargin(const Value: Integer);
begin
  FLeftMargin := Value;
end;

procedure TFastPrint.SetOnEndOfLabel(const Value: TProcessEvent);
begin
  FOnEndOfLabel := Value;
end;

procedure TFastPrint.SetOnEndOfPage(const Value: TProcessEvent);
begin
  FOnEndOfPage := Value;
end;

procedure TFastPrint.SetOnFinally(const Value: TNotifyEvent);
begin
  FOnFinally := Value;
end;

procedure TFastPrint.SetPixel(const Value: Integer);
begin
  FPixel := Value;
end;

procedure TFastPrint.SetRowCount(const Value: Integer);
begin
  FRowCount := Value;
end;

procedure TFastPrint.SetRowSpace(const Value: Integer);
begin
  FRowSpace := Value;
end;

procedure TFastPrint.SetSectionCount(const Value: Integer);
begin
  FSectionCount := Value;
end;

procedure TFastPrint.SetTopMargin(const Value: Integer);
begin
  FTopMargin := Value;
end;

initialization
  FLabelItems:= TStringList.Create;

finalization
  FLabelItems.Free;

end.
