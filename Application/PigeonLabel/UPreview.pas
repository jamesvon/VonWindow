unit UPreview;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Printers;

type
  TFPreview = class(TForm)
    ScrollBox1: TScrollBox;
    Image1: TImage;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FPreview: TFPreview;

implementation

{$R *.dfm}

procedure TFPreview.FormCreate(Sender: TObject);
begin
  Image1.Width:= Round(Printer.PageWidth / 300 * 72);
  Image1.Height:= Round(Printer.PageHeight / 300 * 72);
end;

end.
