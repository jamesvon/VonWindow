object FPrint: TFPrint
  Left = 243
  Top = 67
  Caption = #26631#31614#25171#21360
  ClientHeight = 601
  ClientWidth = 851
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 15
  object Splitter1: TSplitter
    Left = 0
    Top = 331
    Width = 851
    Height = 3
    Cursor = crVSplit
    Align = alBottom
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 851
    Height = 331
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #22270#29255
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 0
        Width = 843
        Height = 301
        Align = alClient
        TabOrder = 0
        object ImageEdit: TImage
          Left = 0
          Top = 0
          Width = 684
          Height = 137
          AutoSize = True
        end
        object Memo1: TMemo
          Left = 684
          Top = 0
          Width = 185
          Height = 280
          Align = alRight
          TabOrder = 0
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #26631#31614
      ImageIndex = 1
      object ScrollBox2: TScrollBox
        Left = 0
        Top = 0
        Width = 843
        Height = 301
        Align = alClient
        TabOrder = 0
        object ImagePreview: TImage
          Left = 0
          Top = 0
          Width = 680
          Height = 137
          AutoSize = True
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 334
    Width = 851
    Height = 205
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    OnCanResize = Panel1CanResize
    object Gauge1: TGauge
      Left = 0
      Top = 0
      Width = 675
      Height = 205
      Align = alClient
      BackColor = clSilver
      BorderStyle = bsNone
      ForeColor = clMaroon
      Kind = gkPie
      Progress = 23
      Visible = False
    end
    object ListRecords: TListBox
      Left = 0
      Top = 0
      Width = 675
      Height = 205
      Align = alClient
      Columns = 1
      ItemHeight = 15
      PopupMenu = PopupMenu1
      TabOrder = 0
    end
    object PageCtrl: TPageControl
      Left = 675
      Top = 0
      Width = 176
      Height = 205
      ActivePage = TabSheet6
      Align = alRight
      MultiLine = True
      TabOrder = 1
      object TabSheet3: TTabSheet
        Caption = #26631#31614
        object Label1: TLabel
          Left = 8
          Top = 0
          Width = 48
          Height = 15
          Caption = #36215#22987#32534#30721
        end
        object Label2: TLabel
          Left = 8
          Top = 40
          Width = 24
          Height = 15
          Caption = #25968#37327
        end
        object Label3: TLabel
          Left = 76
          Top = 40
          Width = 24
          Height = 15
          Caption = #20301#25968
        end
        object Label12: TLabel
          Left = 8
          Top = 88
          Width = 36
          Height = 15
          Caption = #34917#20301#23383
        end
        object ENOBitNumber: TSpinEdit
          Left = 76
          Top = 56
          Width = 41
          Height = 24
          MaxValue = 0
          MinValue = 0
          TabOrder = 0
          Value = 6
          OnChange = ENOBitNumberChange
        end
        object ENONumber: TSpinEdit
          Left = 8
          Top = 56
          Width = 65
          Height = 24
          MaxValue = 0
          MinValue = 0
          TabOrder = 1
          Value = 150
        end
        object EStartNO: TSpinEdit
          Left = 48
          Top = 16
          Width = 73
          Height = 24
          MaxValue = 0
          MinValue = 0
          TabOrder = 2
          Value = 123456
        end
        object EFirstStr: TEdit
          Left = 8
          Top = 16
          Width = 33
          Height = 23
          TabOrder = 4
        end
        object ELastStr: TEdit
          Left = 128
          Top = 16
          Width = 33
          Height = 23
          TabOrder = 5
        end
        object GroupBox2: TGroupBox
          Left = 0
          Top = 108
          Width = 168
          Height = 47
          Align = alBottom
          Caption = #22686#34917
          TabOrder = 6
          object ERecord: TEdit
            Left = 8
            Top = 16
            Width = 81
            Height = 23
            TabOrder = 0
          end
          object BtnAddRecord: TBitBtn
            Left = 88
            Top = 16
            Width = 75
            Height = 25
            Caption = #22686#34917
            Default = True
            DoubleBuffered = True
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000130B0000130B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              33333333333FFFFFFFFF333333000000000033333377777777773333330FFFFF
              FFF03333337F333333373333330FFFFFFFF03333337F3FF3FFF73333330F00F0
              00F03333F37F773777373330330FFFFFFFF03337FF7F3F3FF3F73339030F0800
              F0F033377F7F737737373339900FFFFFFFF03FF7777F3FF3FFF70999990F00F0
              00007777777F7737777709999990FFF0FF0377777777FF37F3730999999908F0
              F033777777777337F73309999990FFF0033377777777FFF77333099999000000
              3333777777777777333333399033333333333337773333333333333903333333
              3333333773333333333333303333333333333337333333333333}
            NumGlyphs = 2
            ParentDoubleBuffered = False
            TabOrder = 1
            OnClick = BtnAddRecordClick
          end
        end
        object EFillChar: TEdit
          Left = 48
          Top = 84
          Width = 33
          Height = 23
          TabOrder = 7
          Text = '0'
        end
        object rbFront: TRadioButton
          Left = 120
          Top = 40
          Width = 41
          Height = 17
          Caption = #21069#34917
          Checked = True
          TabOrder = 8
          TabStop = True
        end
        object rbLast: TRadioButton
          Left = 120
          Top = 53
          Width = 41
          Height = 17
          Caption = #21518#34917
          TabOrder = 9
        end
        object rbNone: TRadioButton
          Left = 120
          Top = 66
          Width = 41
          Height = 17
          Caption = #19981#34917
          TabOrder = 10
        end
        object BtnCreateRecords: TBitBtn
          Left = 88
          Top = 83
          Width = 75
          Height = 25
          Caption = #25209#37327#29983#25104
          DoubleBuffered = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
            0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
            07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
            0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
            33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
            B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
            3BB33773333773333773B333333B3333333B7333333733333337}
          NumGlyphs = 2
          ParentDoubleBuffered = False
          TabOrder = 3
          OnClick = BtnCreateRecordsClick
        end
      end
      object SheetManage: TTabSheet
        Caption = #31649#29702
        ImageIndex = 2
        object BitBtn2: TBitBtn
          Left = 8
          Top = 88
          Width = 75
          Height = 25
          Caption = #23548#20837
          DoubleBuffered = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF003333330FFFFF
            FFF03333337F3FFFF3F73333330F0000F0F03333337F777737373333330FFFFF
            FFF033FFFF7FFF33FFF77000000007F00000377777777FF777770BBBBBBBB0F0
            FF037777777777F7F3730B77777BB0F0F0337777777777F7F7330B7FFFFFB0F0
            0333777F333377F77F330B7FFFFFB0009333777F333377777FF30B7FFFFFB039
            9933777F333377F777FF0B7FFFFFB0999993777F33337777777F0B7FFFFFB999
            9999777F3333777777770B7FFFFFB0399933777FFFFF77F777F3070077007039
            99337777777777F777F30B770077B039993377FFFFFF77F777330BB7007BB999
            93337777FF777777733370000000073333333777777773333333}
          NumGlyphs = 2
          ParentDoubleBuffered = False
          TabOrder = 0
          OnClick = BitBtn2Click
        end
        object BitBtn1: TBitBtn
          Left = 8
          Top = 128
          Width = 75
          Height = 25
          Caption = #23548#20986
          DoubleBuffered = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF003333330B7FFF
            FFB0333333777F3333773333330B7FFFFFB0333333777F3333773333330B7FFF
            FFB0333333777F3333773333330B7FFFFFB03FFFFF777FFFFF77000000000077
            007077777777777777770FFFFFFFF00077B07F33333337FFFF770FFFFFFFF000
            7BB07F3FF3FFF77FF7770F00F000F00090077F77377737777F770FFFFFFFF039
            99337F3FFFF3F7F777FF0F0000F0F09999937F7777373777777F0FFFFFFFF999
            99997F3FF3FFF77777770F00F000003999337F773777773777F30FFFF0FF0339
            99337F3FF7F3733777F30F08F0F0337999337F7737F73F7777330FFFF0039999
            93337FFFF7737777733300000033333333337777773333333333}
          NumGlyphs = 2
          ParentDoubleBuffered = False
          TabOrder = 1
          OnClick = BitBtn1Click
        end
        object BtnDeleteRecord: TBitBtn
          Left = 8
          Top = 8
          Width = 75
          Height = 25
          Caption = #21024#38500#35760#24405
          DoubleBuffered = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333303
            333333333333337FF3333333333333903333333333333377FF33333333333399
            03333FFFFFFFFF777FF3000000999999903377777777777777FF0FFFF0999999
            99037F3337777777777F0FFFF099999999907F3FF777777777770F00F0999999
            99037F773777777777730FFFF099999990337F3FF777777777330F00FFFFF099
            03337F773333377773330FFFFFFFF09033337F3FF3FFF77733330F00F0000003
            33337F773777777333330FFFF0FF033333337F3FF7F3733333330F08F0F03333
            33337F7737F7333333330FFFF003333333337FFFF77333333333000000333333
            3333777777333333333333333333333333333333333333333333}
          NumGlyphs = 2
          ParentDoubleBuffered = False
          TabOrder = 2
          OnClick = BtnDeleteRecordClick
        end
        object BtnClearRecords: TBitBtn
          Left = 8
          Top = 48
          Width = 75
          Height = 25
          Caption = #28165#38500#35760#24405
          DoubleBuffered = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
            0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
            0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
            33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
            B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
            3BB33773333773333773B333333B3333333B7333333733333337}
          NumGlyphs = 2
          ParentDoubleBuffered = False
          TabOrder = 3
          OnClick = BtnClearRecordsClick
        end
        object BtnPrint: TBitBtn
          Left = 88
          Top = 48
          Width = 75
          Height = 25
          Caption = #25171#21360
          DoubleBuffered = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
            0003377777777777777308888888888888807F33333333333337088888888888
            88807FFFFFFFFFFFFFF7000000000000000077777777777777770F8F8F8F8F8F
            8F807F333333333333F708F8F8F8F8F8F9F07F333333333337370F8F8F8F8F8F
            8F807FFFFFFFFFFFFFF7000000000000000077777777777777773330FFFFFFFF
            03333337F3FFFF3F7F333330F0000F0F03333337F77773737F333330FFFFFFFF
            03333337F3FF3FFF7F333330F00F000003333337F773777773333330FFFF0FF0
            33333337F3F37F3733333330F08F0F0333333337F7337F7333333330FFFF0033
            33333337FFFF7733333333300000033333333337777773333333}
          NumGlyphs = 2
          ParentDoubleBuffered = False
          TabOrder = 4
          OnClick = BtnPrintClick
        end
        object BtnPrinterSetup: TBitBtn
          Left = 88
          Top = 8
          Width = 75
          Height = 25
          Caption = #25171#21360#26426
          DoubleBuffered = True
          Glyph.Data = {
            36060000424D3606000000000000360000002800000020000000100000000100
            18000000000000060000C40E0000C40E00000000000000000000FF00FF000000
            0000000000000000000000000000000000000000000000000000000000000000
            00000000000000FF00FFFF00FF80808080808080808080808080808080808080
            8080808080808080808080808080808080808080808080FF00FF000000BFBFBF
            BFBFBFBFBFBF003F7F003F7FBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
            BFBFBFBFBFBFBF000000808080BFBFBFBFBFBFBFBFBF408080408080BFBFBFBF
            BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF808080000000BFBFBF
            BFBFBF007F7F007F7F003F7FBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
            BFBFBFBFBFBFBF000000808080BFBFBFBFBFBFFFFF80FFFF80408080BFBFBFBF
            BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF808080000000007F7F
            003F7F003F7F007F7F007F7F003F7F003F7F0000000000000000000000000000
            00000000000000000000808080FFFF80408080408080FFFF80FFFF8040808040
            8080808080808080808080808080808080808080808080808080000000007F7F
            007F7F007F7F007F7F007F7F007F7F003F7FBFBFBFFFFFFFBFBFBFFFFFFFBFBF
            BFFFFFFFBFBFBF000000808080FFFF80FFFF80FFFF80FFFF80FFFF80FFFF8040
            8080BFBFBFFFFFFFBFBFBFFFFFFFBFBFBFFFFFFFBFBFBF808080000000BFBFBF
            007F7F007F7F007F7F007F7F007F7F003F7FFFFFFFBFBFBFFFFFFFBFBFBFFFFF
            FF0000FFFFFFFF000000808080BFBFBFFFFF80FFFF80FFFF80FFFF80FFFF8040
            8080FFFFFFBFBFBFFFFFFFBFBFBFFFFFFF0000FFFFFFFF808080000000FFFFFF
            BFBFBFFFFFFFBFBFBF007F7F007F7F007F7F003F7FFFFFFFBFBFBFFFFFFFBFBF
            BFFFFFFFBFBFBF000000808080FFFFFFBFBFBFFFFFFFBFBFBFFFFF80FFFF80FF
            FF80408080FFFFFFBFBFBFFFFFFFBFBFBFFFFFFFBFBFBF808080000000000000
            000000000000000000000000000000007F7F007F7F003F7F0000000000000000
            00000000000000000000808080808080808080808080808080808080808080FF
            FF80FFFF80408080808080808080808080808080808080808080FF00FFFF00FF
            FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFF007F7F007F7F003F7F003F7F003F
            7FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF808080FFFFFFFFFFFFFFFFFFFF
            FFFFFFFF80FFFF80408080408080408080FF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FF000000FFFFFF000000000000000000000000007F7F007F7F007F7F003F
            7F003F7F003F7FFF00FFFF00FFFF00FFFF00FF808080FFFFFF80808080808080
            8080808080FFFF80FFFF80FFFF80408080408080408080FF00FFFF00FFFF00FF
            FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF007F7F007F7F007F
            7F007F7F003F7F003F7FFF00FFFF00FFFF00FF808080FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF80FFFF80FFFF80FFFF80408080408080FF00FFFF00FF
            FF00FF000000FFFFFF000000000000FFFFFF000000000000007F7F007F7F007F
            7F007F7F007F7F003F7FFF00FFFF00FFFF00FF808080FFFFFF808080808080FF
            FFFF808080808080FFFF80FFFF80FFFF80FFFF80FFFF80408080FF00FFFF00FF
            FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF007F7F007F
            7F003F7F007F7FFF00FFFF00FFFF00FFFF00FF808080FFFFFFFFFFFFFFFFFFFF
            FFFF808080FFFFFFFFFFFFFFFF80FFFF80408080FFFF80FF00FFFF00FFFF00FF
            FF00FF000000FFFFFF000000BFBFBFFFFFFF000000FFFFFF000000007F7F007F
            7F003F7F003F7FFF00FFFF00FFFF00FFFF00FF808080FFFFFF808080BFBFBFFF
            FFFF808080FFFFFF808080FFFF80FFFF80408080408080FF00FFFF00FFFF00FF
            FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000FF00FFFF00FF007F
            7F007F7FFF00FFFF00FFFF00FFFF00FFFF00FF808080FFFFFFFFFFFFFFFFFFFF
            FFFF808080808080FF00FFFF00FFFFFF80FFFF80FF00FFFF00FFFF00FFFF00FF
            FF00FF000000000000000000000000000000000000FF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF80808080808080808080808080
            8080808080FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
          NumGlyphs = 2
          ParentDoubleBuffered = False
          TabOrder = 5
          OnClick = BtnPrinterSetupClick
        end
        object BtnPrinterPreview: TBitBtn
          Left = 88
          Top = 88
          Width = 75
          Height = 25
          Caption = #39044#35272
          DoubleBuffered = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000C40E0000C40E00001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
            0000777777777777777730777777777777703788888888888887307777777777
            7770378888888888888730FFFFFFFFFFFF8037FFFFFFFFFFFFF7308000000000
            888037888888888888873080FFFFFFF088803788FFFFFFF8FFF73000FFFFFFF0
            00003778FFFFFFF877773330F00000F033333338F88888F833333330FFFFFFF0
            33333338FFFFFFF833333330F00000F033333338F88888F833333330FFFF8870
            33333338FFFFFFF833333330F000000033333338F888888833333330FFFF0F03
            33333338FFFF8F8333333330FFFF003333333338FFFF88333333333000000333
            3333333888888333333333333333333333333333333333333333}
          NumGlyphs = 2
          ParentDoubleBuffered = False
          TabOrder = 6
          OnClick = BtnPrinterPreviewClick
        end
        object BtnSaveBMP: TBitBtn
          Left = 88
          Top = 128
          Width = 75
          Height = 25
          Caption = #23384#22270
          DoubleBuffered = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333FFFFFFFFFFFFF33000077777770033377777777777773F000007888888
            00037F3337F3FF37F37F00000780088800037F3337F77F37F37F000007800888
            00037F3337F77FF7F37F00000788888800037F3337777777337F000000000000
            00037F3FFFFFFFFFFF7F00000000000000037F77777777777F7F000FFFFFFFFF
            00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
            00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
            00037F7F333333337F7F000FFFFFFFFF07037F7F33333333777F000FFFFFFFFF
            0003737FFFFFFFFF7F7330099999999900333777777777777733}
          NumGlyphs = 2
          ParentDoubleBuffered = False
          TabOrder = 7
          OnClick = BtnSaveBMPClick
        end
      end
      object SheetSettings: TTabSheet
        Caption = #35774#32622
        ImageIndex = 3
        object Label7: TLabel
          Left = 8
          Top = 0
          Width = 24
          Height = 15
          Caption = #21015#25968
        end
        object Label8: TLabel
          Left = 72
          Top = 0
          Width = 36
          Height = 15
          Caption = #27573#34892#25968
        end
        object Label9: TLabel
          Left = 40
          Top = 0
          Width = 24
          Height = 15
          Caption = #27573#33853
        end
        object Label10: TLabel
          Left = 8
          Top = 40
          Width = 36
          Height = 15
          Caption = #24038#36793#36317
        end
        object Label11: TLabel
          Left = 56
          Top = 40
          Width = 36
          Height = 15
          Caption = #19978#36793#36317
        end
        object Label13: TLabel
          Left = 8
          Top = 80
          Width = 36
          Height = 15
          Caption = #21015#38388#38548
        end
        object Label14: TLabel
          Left = 56
          Top = 80
          Width = 36
          Height = 15
          Caption = #34892#38388#26684
        end
        object Label6: TLabel
          Left = 112
          Top = 40
          Width = 36
          Height = 15
          Caption = #26631#35760#23485
        end
        object Label16: TLabel
          Left = 112
          Top = 80
          Width = 36
          Height = 15
          Caption = #27573#33853#39640
        end
        object Label17: TLabel
          Left = 112
          Top = 0
          Width = 48
          Height = 15
          Caption = #23450#20301#20559#31227
        end
        object EColCount: TSpinEdit
          Left = 8
          Top = 16
          Width = 33
          Height = 24
          MaxValue = 0
          MinValue = 0
          TabOrder = 0
          Value = 5
        end
        object ERowCount: TSpinEdit
          Left = 72
          Top = 16
          Width = 41
          Height = 24
          MaxValue = 0
          MinValue = 0
          TabOrder = 1
          Value = 10
        end
        object ESectionCount: TSpinEdit
          Left = 40
          Top = 16
          Width = 33
          Height = 24
          MaxValue = 0
          MinValue = 0
          TabOrder = 2
          Value = 3
        end
        object ELeftMargin: TSpinEdit
          Left = 8
          Top = 56
          Width = 49
          Height = 24
          MaxValue = 0
          MinValue = 0
          TabOrder = 3
          Value = 50
        end
        object ETopMargin: TSpinEdit
          Left = 56
          Top = 56
          Width = 49
          Height = 24
          MaxValue = 0
          MinValue = 0
          TabOrder = 4
          Value = 50
        end
        object EColSpace: TSpinEdit
          Left = 8
          Top = 96
          Width = 49
          Height = 24
          MaxValue = 0
          MinValue = 0
          TabOrder = 5
          Value = 5
        end
        object ERowSpace: TSpinEdit
          Left = 55
          Top = 96
          Width = 50
          Height = 24
          MaxValue = 0
          MinValue = 0
          TabOrder = 6
          Value = 5
        end
        object BtnSaveScheme: TBitBtn
          Left = 88
          Top = 128
          Width = 75
          Height = 25
          Caption = #23384#20648#26041#26696
          DoubleBuffered = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00500055555500
            00005777FFFFFF77777750B077000003333057F77777777F555750B070888803
            B3B057F7F75FFF7F555700B07700000B3B3077F7F577777F555750B07777770B
            B3B057F7F555557F555750B07777770BBB3057F7FFFFFF7F555750B00000000B
            BBB057F77777777F555750B05F5F5F0BBBB057F7F7F7F77F555750B05F5F5F0B
            BBB057F7F7F7F77F555750B05F5F5F0BBBB057F7F7F7F77F555750B07777770B
            BBB057F77777777F555750B05F5F5F0BBBB057F7F757577F5557500055555503
            BBB057775555557F555755555555550B3BB055555555557F5557555555555503
            B3B055555555557FFFF755555555550000005555555555777777}
          NumGlyphs = 2
          ParentDoubleBuffered = False
          TabOrder = 7
          OnClick = BtnSaveSchemeClick
        end
        object EScheme: TEdit
          Left = 8
          Top = 128
          Width = 73
          Height = 23
          TabOrder = 8
        end
        object EFlagWidth: TSpinEdit
          Left = 112
          Top = 56
          Width = 49
          Height = 24
          MaxValue = 0
          MinValue = 0
          TabOrder = 9
          Value = 20
        end
        object ESequencHeight: TSpinEdit
          Left = 111
          Top = 96
          Width = 50
          Height = 24
          MaxValue = 0
          MinValue = 0
          TabOrder = 10
          Value = 100
        end
        object EPointOffset: TSpinEdit
          Left = 111
          Top = 16
          Width = 49
          Height = 24
          MaxValue = 0
          MinValue = 0
          TabOrder = 11
          Value = 50
        end
      end
      object TabSheet4: TTabSheet
        Caption = #26041#26696
        ImageIndex = 3
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 168
          Height = 155
          Align = alClient
          Caption = #25171#21360#26041#26696' '
          TabOrder = 0
          object ESchemeList: TListBox
            Left = 2
            Top = 17
            Width = 164
            Height = 112
            Align = alClient
            Columns = 3
            ItemHeight = 15
            TabOrder = 0
            OnDblClick = BtnLoadSchemeClick
          end
          object Panel2: TPanel
            Left = 2
            Top = 129
            Width = 164
            Height = 24
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object BtnLoadScheme: TBitBtn
              Left = 89
              Top = 0
              Width = 75
              Height = 25
              Caption = #25552#21462
              DoubleBuffered = True
              Glyph.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000120B0000120B00001000000000000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00500000000000
                000557777777777777750BBBBBBBBBBBBBB07F5555FFFFFFF5570BBBB0000000
                BBB07F5557777777FF570BBB077BBB770BB07F557755555775570BBBBBBBBBBB
                BBB07F5555FFFFFFF5570BBBB0000000BBB07F5557777777F5570BBBB0FFFFF0
                BBB07F5557FFFFF7F5570BBBB0000000BBB07F555777777755570BBBBBBBBBBB
                BBB07FFFFFFFFFFFFFF700000000000000007777777777777777500FFFFFFFFF
                F005577FF555FFFFF7755500FFF00000005555775FF7777777F5550F777FFFFF
                F055557F777FFF5557F5550000000FFF00555577777775FF77F5550777777000
                7055557FFFFFF777F7F555000000000000555577777777777755}
              NumGlyphs = 2
              ParentDoubleBuffered = False
              TabOrder = 0
              OnClick = BtnLoadSchemeClick
            end
            object btnSchemeDelete: TBitBtn
              Left = 8
              Top = 0
              Width = 75
              Height = 25
              Caption = #21024#38500
              DoubleBuffered = True
              Glyph.Data = {
                DE010000424DDE01000000000000760000002800000024000000120000000100
                0400000000006801000000000000000000001000000000000000000000000000
                80000080000000808000800000008000800080800000C0C0C000808080000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                333333333333333333333333000033338833333333333333333F333333333333
                0000333911833333983333333388F333333F3333000033391118333911833333
                38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
                911118111118333338F3338F833338F3000033333911111111833333338F3338
                3333F8330000333333911111183333333338F333333F83330000333333311111
                8333333333338F3333383333000033333339111183333333333338F333833333
                00003333339111118333333333333833338F3333000033333911181118333333
                33338333338F333300003333911183911183333333383338F338F33300003333
                9118333911183333338F33838F338F33000033333913333391113333338FF833
                38F338F300003333333333333919333333388333338FFF830000333333333333
                3333333333333333333888330000333333333333333333333333333333333333
                0000}
              NumGlyphs = 2
              ParentDoubleBuffered = False
              TabOrder = 1
              OnClick = btnSchemeDeleteClick
            end
          end
        end
      end
      object TabSheet5: TTabSheet
        Caption = #26426#32452
        ImageIndex = 4
        object lstPrinter: TCheckListBox
          Left = 0
          Top = 22
          Width = 168
          Height = 133
          Align = alClient
          ItemHeight = 15
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = lstPrinterClick
        end
        object ToolBar1: TToolBar
          Left = 0
          Top = 0
          Width = 168
          Height = 22
          AutoSize = True
          Caption = 'ToolBar1'
          EdgeInner = esNone
          EdgeOuter = esNone
          Images = ImageList1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          object btnSelectedAll: TToolButton
            Left = 0
            Top = 0
            Hint = #25171#21360#26426#20840#37096#36873#20013
            Caption = #25171#21360#26426#20840#37096#36873#20013
            ImageIndex = 0
            OnClick = btnSelectedAllClick
          end
          object btnSelectedNone: TToolButton
            Left = 23
            Top = 0
            Hint = #25171#21360#26426#20840#37096#19981#36873
            Caption = #25171#21360#26426#20840#37096#19981#36873
            ImageIndex = 1
            OnClick = btnSelectedNoneClick
          end
          object btnPrint2: TToolButton
            Left = 46
            Top = 0
            Hint = #25191#34892#25171#21360
            Caption = #25191#34892#25171#21360
            ImageIndex = 2
            OnClick = BtnPrintClick
          end
        end
      end
      object TabSheet6: TTabSheet
        Caption = #22686#34917
        ImageIndex = 5
        DesignSize = (
          168
          155)
        object Label5: TLabel
          Left = 88
          Top = 77
          Width = 24
          Height = 15
          Caption = #25968#37327
        end
        object lbCardCount: TLabel
          Left = 112
          Top = 77
          Width = 7
          Height = 15
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label15: TLabel
          Left = 88
          Top = 61
          Width = 24
          Height = 15
          Caption = #21345#21495
        end
        object lbCardNo: TLabel
          Left = 112
          Top = 61
          Width = 3
          Height = 15
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 88
          Top = 8
          Width = 24
          Height = 15
          Caption = #25968#37327
        end
        object lstCard: TListBox
          Left = 0
          Top = 0
          Width = 79
          Height = 121
          Anchors = [akLeft, akTop, akBottom]
          ItemHeight = 15
          TabOrder = 0
        end
        object btnImportCard: TBitBtn
          Left = 88
          Top = 32
          Width = 75
          Height = 25
          Caption = #34917#21495
          DoubleBuffered = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
            0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
            07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
            0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
            33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
            B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
            3BB33773333773333773B333333B3333333B7333333733333337}
          NumGlyphs = 2
          ParentDoubleBuffered = False
          TabOrder = 1
          OnClick = btnImportCardClick
        end
        object btnReadCard: TBitBtn
          Left = 88
          Top = 96
          Width = 75
          Height = 25
          Caption = #35835#21345
          DoubleBuffered = True
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00337000000000
            73333337777777773F333308888888880333337F3F3F3FFF7F33330808089998
            0333337F737377737F333308888888880333337F3F3F3F3F7F33330808080808
            0333337F737373737F333308888888880333337F3F3F3F3F7F33330808080808
            0333337F737373737F333308888888880333337F3F3F3F3F7F33330808080808
            0333337F737373737F333308888888880333337F3FFFFFFF7F33330800000008
            0333337F7777777F7F333308000E0E080333337F7FFFFF7F7F33330800000008
            0333337F777777737F333308888888880333337F333333337F33330888888888
            03333373FFFFFFFF733333700000000073333337777777773333}
          NumGlyphs = 2
          ParentDoubleBuffered = False
          TabOrder = 2
          OnClick = btnReadCardClick
        end
        object BitBtn3: TBitBtn
          Left = 88
          Top = 128
          Width = 75
          Height = 25
          Caption = #21024#38500
          DoubleBuffered = True
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333333333000033338833333333333333333F333333333333
            0000333911833333983333333388F333333F3333000033391118333911833333
            38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
            911118111118333338F3338F833338F3000033333911111111833333338F3338
            3333F8330000333333911111183333333338F333333F83330000333333311111
            8333333333338F3333383333000033333339111183333333333338F333833333
            00003333339111118333333333333833338F3333000033333911181118333333
            33338333338F333300003333911183911183333333383338F338F33300003333
            9118333911183333338F33838F338F33000033333913333391113333338FF833
            38F338F300003333333333333919333333388333338FFF830000333333333333
            3333333333333333333888330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
          ParentDoubleBuffered = False
          TabOrder = 3
          OnClick = BitBtn3Click
        end
        object ECardCount: TSpinEdit
          Left = 112
          Top = 4
          Width = 49
          Height = 24
          MaxValue = 0
          MinValue = 0
          TabOrder = 4
          Value = 150
        end
        object btnClearCard: TBitBtn
          Left = 8
          Top = 128
          Width = 75
          Height = 25
          Anchors = [akLeft, akBottom]
          Caption = #28165#31354#21345
          DoubleBuffered = True
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333333333000033338833333333333333333F333333333333
            0000333911833333983333333388F333333F3333000033391118333911833333
            38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
            911118111118333338F3338F833338F3000033333911111111833333338F3338
            3333F8330000333333911111183333333338F333333F83330000333333311111
            8333333333338F3333383333000033333339111183333333333338F333833333
            00003333339111118333333333333833338F3333000033333911181118333333
            33338333338F333300003333911183911183333333383338F338F33300003333
            9118333911183333338F33838F338F33000033333913333391113333338FF833
            38F338F300003333333333333919333333388333338FFF830000333333333333
            3333333333333333333888330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
          ParentDoubleBuffered = False
          TabOrder = 5
          OnClick = btnClearCardClick
        end
      end
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 539
    Width = 851
    Height = 43
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel7: TPanel
      Left = 602
      Top = 0
      Width = 249
      Height = 43
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtnClose: TBitBtn
        Left = 168
        Top = 16
        Width = 75
        Height = 25
        Caption = #36864#20986
        DoubleBuffered = True
        Kind = bkClose
        NumGlyphs = 2
        ParentDoubleBuffered = False
        TabOrder = 0
      end
      object EPreviewText: TEdit
        Left = 8
        Top = 16
        Width = 73
        Height = 23
        TabOrder = 1
        Text = '0000000'
      end
      object BtnPreview: TBitBtn
        Left = 88
        Top = 16
        Width = 75
        Height = 25
        Caption = #39044#35272
        DoubleBuffered = True
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00722770000000
          00007667788888888888772270FFFFFFFFF0776678FFFFFFFFF8772220F00000
          00F0776668F8888888F8777222FFFFFFFFF0777666FFFFFFFFF87772228888F0
          F0F07776667777F8F8F877772AAAA88FFFF077776EEEE77FFFF87777A7F77A88
          00F07777E8F88E77F838777A87FFFFA8FFF0777E78FFFFE7FFF8777A87F77FA8
          00F0777E78F888E73838777A87FFFFA8FFF0777E78FFFFE7FFF8777A88F777AF
          FFF0777E77F888EFFFF87777A8888AFF00007777E7777EFF888877777AAAAFFF
          0FF077777EEEEFFF8F38777770F000FF0F07777778F8888F8F87777770FFFFFF
          0077777778FFFFFF887777777000000007777777788888888777}
        NumGlyphs = 2
        ParentDoubleBuffered = False
        TabOrder = 2
        OnClick = BtnPreviewClick
      end
    end
    object PanelPages: TPanel
      Left = 0
      Top = 0
      Width = 49
      Height = 43
      Align = alLeft
      BevelOuter = bvNone
      BevelWidth = 2
      BorderWidth = 2
      TabOrder = 1
      object Panel3: TPanel
        Left = 16
        Top = 8
        Width = 23
        Height = 33
        BevelOuter = bvNone
        Color = clAppWorkSpace
        Ctl3D = False
        Locked = True
        ParentCtl3D = False
        TabOrder = 1
      end
      object EPageCount: TPanel
        Left = 10
        Top = 4
        Width = 25
        Height = 33
        BorderStyle = bsSingle
        Caption = '0'
        Color = clWhite
        TabOrder = 0
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 582
    Width = 851
    Height = 19
    Panels = <
      item
        Text = #35760#24405#25968#37327':'
        Width = 60
      end
      item
        Width = 50
      end
      item
        Alignment = taRightJustify
        Text = #31867#22411':'
        Width = 40
      end
      item
        Width = 120
      end
      item
        Alignment = taRightJustify
        Text = #26679#24335':'
        Width = 40
      end
      item
        Width = 200
      end
      item
        Alignment = taRightJustify
        Text = #23610#23544':'
        Width = 40
      end
      item
        Width = 50
      end
      item
        Width = 50
      end>
  end
  object PrinterSetupDialog1: TPrinterSetupDialog
    Left = 176
    Top = 200
  end
  object PopupMenu1: TPopupMenu
    Left = 144
    Top = 200
    object N1: TMenuItem
      Caption = #21024#38500
    end
  end
  object DQSchemeKinds: TADOQuery
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT DISTINCT ParamKind FROM FL_Params')
    Left = 240
    Top = 200
  end
  object DQParam: TADOQuery
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'Kind'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'Name'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      'SELECT ParamKind, ParamName, ParamValue'
      'FROM FL_Params'
      'WHERE ParamKind=:Kind'
      'AND ParamName=:Name')
    Left = 272
    Top = 200
  end
  object OpenPictureDialog1: TOpenPictureDialog
    DefaultExt = '.bmp'
    Filter = 'Bitmaps (*.bmp)|*.bmp'
    Left = 304
    Top = 200
  end
  object OpenDialog1: TOpenDialog
    Left = 336
    Top = 200
  end
  object SaveDialog1: TSaveDialog
    Left = 368
    Top = 200
  end
  object ImageList1: TImageList
    Left = 208
    Top = 200
    Bitmap = {
      494C010104000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000002000000001002000000000000020
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000008400000084000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD0000000000000000000000000000FFFF00BDBD
      BD0000FFFF00BDBDBD0000FFFF00BDBDBD0000FFFF00BDBDBD0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000084000000FF000000FF000000
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF000000000000FF
      FF00BDBDBD0000FFFF00BDBDBD0000FFFF00BDBDBD0000FFFF00BDBDBD0000FF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00BDBDBD00FFFF
      FF00BDBDBD00FFFFFF00BDBDBD00FFFFFF00BDBDBD00FFFFFF00BDBDBD00FFFF
      FF00BDBDBD00FFFFFF00BDBDBD000000000000000000FFFFFF0000FFFF000000
      000000FFFF00BDBDBD0000FFFF00BDBDBD0000FFFF00BDBDBD0000FFFF00BDBD
      BD0000FFFF0000000000FFFFFF00000000000000000000000000000000000000
      000000000000000000000000FF000000000000000000000000000000FF000000
      FF00000084000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BDBDBD00FFFFFF00BDBD
      BD00FFFFFF00BDBDBD00FFFFFF00BDBDBD00FFFFFF00BDBDBD00FFFFFF00BDBD
      BD00FFFFFF000000FF00FFFFFF00000000000000000000FFFF00FFFFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF00000084000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00BDBDBD00FFFF
      FF00BDBDBD00FFFFFF00BDBDBD00FFFFFF00BDBDBD00FFFFFF00BDBDBD00FFFF
      FF00BDBDBD00FFFFFF00BDBDBD000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF0000000000FFFFFF000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000000000000000000000000000FFFFFF0000000000FFFF
      FF00000000000000000000000000000000000000000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF0000000000FFFFFF000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000000000007B7B7B00000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000BDBDBD00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000200000000100010000000000000100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFF8001FFFFF801F8010000801F
      FB9DFBFD00000000E30DE3FD00000000EA0DEBFD0000000089C58BFD00000000
      ABC5ABFD00000000ABF1ABFD00000000ABF9ABFDE0070000ABFDABFDE0078000
      A801A801E0078000AFF7AFF7E007FC00A007A007E00FFC01BFDFBFDFE01FFC03
      801F801FE03FFC07FFFFFFFFE07FFFFF00000000000000000000000000000000
      000000000000}
  end
  object DCDelScheme: TADOCommand
    CommandText = 'DELETE'#13#10'FROM FL_Params'#13#10'WHERE ParamKind=:Kind'
    Parameters = <
      item
        Name = 'Kind'
        Size = -1
        Value = Null
      end>
    Left = 272
    Top = 232
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 150
    OnTimer = Timer1Timer
    Left = 304
    Top = 232
  end
end
