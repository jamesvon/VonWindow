unit UPrintLabel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, Gauges, StdCtrls, Spin, Buttons, ADODB, ExtDlgs,
  Menus, ToolWin, ImgList, UFlyLabelDB, DB, Printers, ULabelPrint, IniFiles,
  CheckLst, UComm;

type
  TFPrint = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    ScrollBox1: TScrollBox;
    ImageEdit: TImage;
    TabSheet2: TTabSheet;
    ScrollBox2: TScrollBox;
    ImagePreview: TImage;
    Panel1: TPanel;
    ListRecords: TListBox;
    Panel4: TPanel;
    StatusBar1: TStatusBar;
    PrinterSetupDialog1: TPrinterSetupDialog;
    PageCtrl: TPageControl;
    TabSheet3: TTabSheet;
    Panel7: TPanel;
    BtnClose: TBitBtn;
    SheetManage: TTabSheet;
    BitBtn2: TBitBtn;
    BitBtn1: TBitBtn;
    BtnDeleteRecord: TBitBtn;
    BtnClearRecords: TBitBtn;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    EPreviewText: TEdit;
    BtnPreview: TBitBtn;
    SheetSettings: TTabSheet;
    Label7: TLabel;
    EColCount: TSpinEdit;
    ERowCount: TSpinEdit;
    Label8: TLabel;
    Label9: TLabel;
    ESectionCount: TSpinEdit;
    Label10: TLabel;
    ELeftMargin: TSpinEdit;
    ETopMargin: TSpinEdit;
    Label11: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ENOBitNumber: TSpinEdit;
    ENONumber: TSpinEdit;
    EStartNO: TSpinEdit;
    BtnCreateRecords: TBitBtn;
    BtnPrint: TBitBtn;
    DQSchemeKinds: TADOQuery;
    BtnPrinterSetup: TBitBtn;
    DQParam: TADOQuery;
    BtnPrinterPreview: TBitBtn;
    BtnSaveBMP: TBitBtn;
    Label13: TLabel;
    Label14: TLabel;
    EColSpace: TSpinEdit;
    ERowSpace: TSpinEdit;
    TabSheet4: TTabSheet;
    BtnSaveScheme: TBitBtn;
    EScheme: TEdit;
    OpenPictureDialog1: TOpenPictureDialog;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    EFirstStr: TEdit;
    ELastStr: TEdit;
    Gauge1: TGauge;
    PanelPages: TPanel;
    EPageCount: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    ESchemeList: TListBox;
    ImageList1: TImageList;
    Memo1: TMemo;
    TabSheet5: TTabSheet;
    lstPrinter: TCheckListBox;
    ToolBar1: TToolBar;
    btnSelectedAll: TToolButton;
    btnSelectedNone: TToolButton;
    btnPrint2: TToolButton;
    Panel2: TPanel;
    BtnLoadScheme: TBitBtn;
    btnSchemeDelete: TBitBtn;
    DCDelScheme: TADOCommand;
    TabSheet6: TTabSheet;
    Timer1: TTimer;
    GroupBox2: TGroupBox;
    ERecord: TEdit;
    BtnAddRecord: TBitBtn;
    lstCard: TListBox;
    Label5: TLabel;
    lbCardCount: TLabel;
    Label15: TLabel;
    lbCardNo: TLabel;
    btnImportCard: TBitBtn;
    btnReadCard: TBitBtn;
    BitBtn3: TBitBtn;
    Label4: TLabel;
    ECardCount: TSpinEdit;
    btnClearCard: TBitBtn;
    Splitter1: TSplitter;
    Label12: TLabel;
    EFillChar: TEdit;
    rbFront: TRadioButton;
    rbLast: TRadioButton;
    rbNone: TRadioButton;
    Label6: TLabel;
    EFlagWidth: TSpinEdit;
    Label16: TLabel;
    ESequencHeight: TSpinEdit;
    Label17: TLabel;
    EPointOffset: TSpinEdit;
    procedure BtnCreateRecordsClick(Sender: TObject);
    procedure Panel1CanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure ENOBitNumberChange(Sender: TObject);
    procedure BtnAddRecordClick(Sender: TObject);
    procedure BtnDeleteRecordClick(Sender: TObject);
    procedure BtnClearRecordsClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtnPreviewClick(Sender: TObject);
    procedure BtnPrinterSetupClick(Sender: TObject);
    procedure BtnPrintClick(Sender: TObject);
    procedure BtnPrinterPreviewClick(Sender: TObject);
    procedure BtnSaveBMPClick(Sender: TObject);
    procedure BtnLoadSchemeClick(Sender: TObject);
    procedure BtnSaveSchemeClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btnSelectedAllClick(Sender: TObject);
    procedure btnSelectedNoneClick(Sender: TObject);
    procedure btnSchemeDeleteClick(Sender: TObject);
    procedure lstPrinterClick(Sender: TObject);
    procedure btnReadCardClick(Sender: TObject);
    procedure btnImportCardClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure btnClearCardClick(Sender: TObject);
  private
    { Private declarations }
    QueryLabel: TADOQuery;
    TempBitmap: TBitmap;
    LabelImage: TLabelImage;
    FPrinters: TStringList;
    FCardLabels: TStringList;
    FLabelWidth, FLabelHeight, FCurrentPrintIDX: Integer;
    FCurrentLabel: Integer;
    FComMID: Integer;
    FComm: TComm;
    procedure SetPrintSettings(LabelPrint: TLabelPrint);
    function GetString(Value: integer; Number: Word): String;
    procedure ResetColumn(NewWidth: Integer);
    procedure LoadDataFromQuery(DQLabel: TADOQuery);
    procedure PrintFinally(Sender: TObject);
    procedure EndOfLabel(Sender: TObject; LabelID: Integer);
    procedure EndOfPage(Sender: TObject; PageID: Integer);
    procedure OpenParam(ParamKind, ParamName: string);
    procedure GetNextPrint;
    procedure SetCurrentLabel(const Value: Integer);
    procedure SetComMID(const Value: Integer);
  public
    { Public declarations }
    property CurrentLabel: Integer read FCurrentLabel write SetCurrentLabel;
    property ComMID: Integer read FComMID write SetComMID;
  end;

var
  FPrint: TFPrint;

  procedure ShowPrint(Sender: TForm; DQLabel: TADOQuery);
  //计算TCheckListBox选择结果以二进制数值表现（bit：0表示没选，1表示选择）
  function GetMutiChkByChkList(AList: TCheckListBox): Int64;
  //根据记录整数（二进制记忆方式）回选TCheckListBox的内容
  procedure SetMutiChkToChkList(AValue: Int64; AList: TCheckListBox);

implementation

//uses UFlyLabelPreview;

{$R *.dfm}

procedure ShowPrint(Sender: TForm; DQLabel: TADOQuery);
begin
  with TFPrint.Create(Application) do try
    Sender.Hide;     
    QueryLabel:= DQLabel;
    LoadDataFromQuery(DQLabel);
    with TIniFile.Create(ExtractFilePath(Application.ExeName) + 'FlyLabel.INI')do try
      ComMID:= ReadInteger('SYSTEM', 'COMM', 3);
    finally
      Free;
    end;
    ShowModal;
  finally
    Free;
    Sender.Show;
  end;
end;

function GetMutiChkByChkList(AList: TCheckListBox): Int64;
var
  i:Integer;
  AResult: Int64;
begin
  AResult:= 1;
  Result:= 0;
  for i:= 0 to AList.Count - 1 do begin
    if AList.Checked[i] then Result:= Result + AResult;
    AResult:= AResult * 2;
  end;
end;

procedure SetMutiChkToChkList(AValue: Int64; AList: TCheckListBox);
var
  i: Integer;
begin
  for i:= 0 to AList.Count - 1 do
    AList.Checked[i]:= (AValue shr i) and 1 > 0;
end;

{ TFPrint }

procedure TFPrint.FormCreate(Sender: TObject);
begin
  LabelImage:= TLabelImage.Create;
  TempBitmap:= TBitmap.Create;
  FPrinters:= TStringList.Create;
  FCardLabels:= TStringList.Create;

  FComm:= TComm.Create;
  FComm.Labels:= FCardLabels;
  with DQSchemeKinds do begin
    Open;
    while not EOF do begin
      ESchemeList.Items.Add(Fields[0].AsString);
      Next;
    end;
    Close;
  end;
  lstPrinter.Items.Assign(Printer.Printers);
  with TIniFile.Create(ExtractFilePath(Application.ExeName) + 'FlyLabel.INI')do try
    SetMutiChkToChkList(ReadInteger('PRINTERS', 'Choosed', 0), lstPrinter);
    ESchemeList.ItemIndex:= ReadInteger('SCHEME', 'Choosed', 0);
    FComMID:= ReadInteger('SYSTEM', 'COM', 3);
    BtnLoadSchemeClick(self);
  finally
    Free;
  end;    
  PageCtrl.ActivePageIndex:= 0;
end;

procedure TFPrint.FormDestroy(Sender: TObject);
begin
  with TIniFile.Create(ExtractFilePath(Application.ExeName) + 'FlyLabel.INI')do try
    WriteInteger('PRINTERS', 'Choosed', GetMutiChkByChkList(lstPrinter));
    WriteInteger('SCHEME', 'Choosed', ESchemeList.ItemIndex);
  finally
    Free;
  end;     
  FComm.Free;
  FCardLabels.Free;
  FPrinters.Free;
  TempBitmap.Free;
  LabelImage.Free;
end;

procedure TFPrint.BtnCreateRecordsClick(Sender: TObject);
var
  i: Integer;
begin     //生成记录
  for i:=0 to ENONumber.Value - 1 do
    ListRecords.Items.Add(EFirstStr.Text
    + GetString(EStartNO.Value + i, ENOBitNumber.Value)
    + ELastStr.Text);
  StatusBar1.Panels[1].Text:= IntToStr(ListRecords.Items.Count);
  PageCtrl.ActivePageIndex:= 1;
  EStartNO.Value:= EStartNO.Value + ENONumber.Value;
end;

function TFPrint.GetString(Value: integer; Number: Word): String;
var
  S: string;
begin    //格式化数值
  if EFillChar.Text = '' then EFillChar.Text:= '0';
  if rbFront.Checked then begin        //前补位
    S:= StringOfChar(EFillChar.Text[1], Number) + IntToStr(Value);
    Delete(S, 1, (Length(S) - Number) div 2);
    Result:= Copy(S, Length(S) - Number + 1, Number);
  end else if rbLast.Checked then begin
    S:= IntToStr(Value) + StringOfChar(EFillChar.Text[1], Number);
    Result:= Copy(S, 1, Number);
  end else Result:= IntToStr(Value);
end;

procedure TFPrint.Panel1CanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  ResetColumn(NewWidth);
end;

procedure TFPrint.ResetColumn(NewWidth: Integer);
begin
  ListRecords.Columns:= (NewWidth - 280) div (ENOBitNumber.Value * 10);
end;

procedure TFPrint.ENOBitNumberChange(Sender: TObject);
begin
  ResetColumn(Panel1.Width);
end;

procedure TFPrint.BtnAddRecordClick(Sender: TObject);
var
  i: integer;
begin     //添加一条标签
  ListRecords.Items.Add(ERecord.Text);
  StatusBar1.Panels[1].Text:= IntToStr(ListRecords.Items.Count);
  ERecord.Text:= '';
end;

procedure TFPrint.BtnDeleteRecordClick(Sender: TObject);
begin     //删除记录
  ListRecords.DeleteSelected;
end;

procedure TFPrint.BtnClearRecordsClick(Sender: TObject);
begin     //清除记录
  ListRecords.Items.Clear;
end;

procedure TFPrint.LoadDataFromQuery(DQLabel: TADOQuery);
begin
  with DQLabel, LabelImage do begin
    CurrentLabel:= FieldByName('ID').AsInteger;
    StatusBar1.Panels[5].Text:= FieldByName('LabelName').AsString;
    (* 读取图片信息 *)
    TempBitmap.Assign(FieldByName('Picture'));
    //BackBmp.PixelFormat:= pf1bit;
    OrgBmp.Width:= TempBitmap.Width;
    OrgBmp.Height:= TempBitmap.Height;
    OrgBmp.Canvas.Draw(0, 0, TempBitmap);
    //BackBmp.Canvas.Brush.Color:= FieldByName('BaseColor').AsInteger;
    ImageEdit.Picture.Bitmap.Assign(OrgBmp);
    (* 初始化标签尺寸 *)
    FLabelWidth:= FieldByName('LabelWidth').AsInteger;
    FLabelHeight:= FieldByName('LabelHeight').AsInteger;
    ImagePreview.Picture.Bitmap.Width:= FLabelWidth;
    ImagePreview.Picture.Bitmap.Height:= FLabelHeight;
    (* 其他信息 *)
    Content:= FieldByName('Content').AsString;
  end;
end;

procedure TFPrint.BtnPreviewClick(Sender: TObject);
begin
  LabelImage.Drew(ImageEdit.Picture.Bitmap, EPreviewText.Text);
  LabelImage.Drew(ImagePreview.Picture.Bitmap, EPreviewText.Text);
end;

procedure TFPrint.BtnPrinterSetupClick(Sender: TObject);
begin
  PrinterSetupDialog1.Execute;
end;
(* 打印期间回调函数 *)
procedure TFPrint.PrintFinally(Sender: TObject);
begin     //打印进程结束回调函数
  Gauge1.Visible:= False;
  ListRecords.Visible:= true;
end;

procedure TFPrint.EndOfLabel(Sender: TObject; LabelID: Integer);
var
  ProcessID: integer;
begin     //打印进程单标签打印结束回调函数
  Gauge1.Progress:= LabelID;
//  FFlyLabelDB.AddTask(FCurrentLabel, 1, 'PRINT', ListRecords.Items[LabelID - 1]);
end;

procedure TFPrint.EndOfPage(Sender: TObject; PageID: Integer);
begin     //单页打印结束回调函数
  EPageCount.Caption:= IntToStr(PageID);
  EPageCount.Repaint;
  Printer.EndDoc;
  Memo1.Lines.Add('EndDoc');
  GetNextPrint;
  Printer.BeginDoc;
  Memo1.Lines.Add('BeginDoc');
end;

(* 按钮事件 *)

procedure TFPrint.BtnPrintClick(Sender: TObject);
var
  TempPrint: TLabelPrint;
begin     //打印
  FCurrentPrintIDX:= 0;
  TempPrint:= TLabelPrint.Create();
  SetPrintSettings(TempPrint);
  PanelPages.Visible:= true;
  ListRecords.Visible:= False;
  GetNextPrint;
  TempPrint.Print(FCurrentLabel);
  //BtnClearRecordsClick(self);
  PageCtrl.ActivePageIndex:= 1;
end;

procedure TFPrint.BtnSaveBMPClick(Sender: TObject);
var
  TempPrint: TLabelPrint;
begin     //图打
  if not OpenPictureDialog1.Execute then Exit;
  TempPrint:= TLabelPrint.Create();
  SetPrintSettings(TempPrint);
  TempPrint.SaveFile(OpenPictureDialog1.FileName);
  BtnClearRecordsClick(self);
  PageCtrl.ActivePageIndex:= 0;
end;

procedure TFPrint.BtnPrinterPreviewClick(Sender: TObject);
var
  TempPrint: TLabelPrint;
begin     //打印预览
  TempPrint:= TLabelPrint.Create();
  SetPrintSettings(TempPrint);
  with TempPrint do
//    with TFFlyLabelPreview.Create(self) do try
//      Preview(Image1.Canvas);
//      ShowModal();
//    finally
//      TempPrint.Free;
//      Free;
//    end;
end;

procedure TFPrint.OpenParam(ParamKind, ParamName: string);
begin
  with DQParam do begin
    Close;
    Parameters[0].Value:= ParamKind;
    Parameters[1].Value:= ParamName;
    Open;
  end;
end;

procedure TFPrint.BtnLoadSchemeClick(Sender: TObject);
  function GetParamValue(ParamName: string; DefaultValue: Integer = 0): Integer;
  begin
    Result:= DefaultValue;
    OpenParam(EScheme.Text, ParamName);
    if DQParam.RecordCount > 0 then
      Result:= DQParam.FieldByName('ParamValue').AsInteger;
  end;
begin     //提取打印参数
  if ESchemeList.ItemIndex < 0 then Exit;
  EScheme.Text:= ESchemeList.Items[ESchemeList.ItemIndex];
  EColCount.Value:= GetParamValue('ColCount');
  ESectionCount.Value:= GetParamValue('SectionCount');
  ERowCount.Value:= GetParamValue('RowCount');
  ELeftMargin.Value:= GetParamValue('LeftMargin');
  ETopMargin.Value:= GetParamValue('TopMargin');
  ERowSpace.Value:= GetParamValue('RowSpace');
  EColSpace.Value:= GetParamValue('ColSpace');    
  ESequencHeight.Value:= GetParamValue('SequencHeight', 100);
  EFlagWidth.Value:= GetParamValue('FlagWidth', 20);
  EPointOffset.Value:= GetParamValue('PointOffset', 50);
  DQParam.Close;
  PageCtrl.ActivePage:= SheetSettings;
end;

procedure TFPrint.BtnSaveSchemeClick(Sender: TObject);
var
  ParamKind: string;
  procedure SaveParam(ParamName: string; ParamValue: Integer);
  begin
    OpenParam(ParamKind, ParamName);
    with DQParam do begin
      if RecordCount < 1 then Append else Edit;
      FieldByName('ParamKind').AsString:= EScheme.Text;
      FieldByName('ParamName').AsString:= ParamName;
      FieldByName('ParamValue').AsInteger:= ParamValue;
      Post;
    end;
  end;
begin     //存储打印参数
  ParamKind:= Trim(EScheme.Text);
  if ParamKind = '' then raise Exception.Create('方案名称不能为空。');
  SaveParam('ColCount', EColCount.Value);
  SaveParam('SectionCount', ESectionCount.Value);
  SaveParam('RowCount', ERowCount.Value);
  SaveParam('LeftMargin', ELeftMargin.Value);
  SaveParam('TopMargin', ETopMargin.Value);
  SaveParam('RowSpace', ERowSpace.Value);
  SaveParam('ColSpace', EColSpace.Value);
  SaveParam('SequencHeight', ESequencHeight.Value);
  SaveParam('FlagWidth', EFlagWidth.Value);
  SaveParam('PointOffset', EPointOffset.Value);
  if ESchemeList.Items.IndexOf(ParamKind) < 0 then
    ESchemeList.Items.Add(ParamKind);
  DQParam.Close;
end;

procedure TFPrint.SetPrintSettings(LabelPrint: TLabelPrint);
var
  i, j, k, pageCount, sectionCount, maxID: Integer;

begin     //设定图标输出类基础参数
  with LabelPrint do begin
    LabelImage:= Self.LabelImage;     //调用打印进程
    OnFinally:= PrintFinally;
    OnEndOfLabel:= EndOfLabel;
    OnEndOfPage:= EndOfPage;
    //重新分配打印序列
    FPrinters.Clear;
    for i:= 0 to lstPrinter.Count - 1 do      //Prepaired list of printers
      if lstPrinter.Checked[i] then
        FPrinters.Add(lstPrinter.Items[i]);
    pageCount:= EColCount.Value * ESectionCount.Value * ERowCount.Value; //150
    if pageCount * FPrinters.Count = 0 then sectionCount:= ListRecords.Items.Count
    else sectionCount:= ListRecords.Items.Count div (pageCount * FPrinters.Count);
    for i:= 0 to sectionCount - 1 do          //loop the section
      for j:= 0 to FPrinters.Count - 1 do begin
        Memo1.Lines.Add(IntToStr(j*sectionCount*pageCount+i*pageCount));
        for k:= 0 to pageCount - 1 do
          Items.Add(ListRecords.Items[j*sectionCount*pageCount+i*pageCount+k]);
      end;
    for i:= sectionCount * pageCount * FPrinters.Count to ListRecords.Items.Count - 1 do
      Items.Add(ListRecords.Items[i]);   //*)
    RowCount:= ERowCount.Value;
    ColCount:= EColCount.Value;
    LabelWidth:= FLabelWidth;
    LabelHeight:= FLabelHeight;
    SectionCount:= ESectionCount.Value;
    LeftMargin:= ELeftMargin.Value;
    TopMargin:= ETopMargin.Value;
    RowSpace:= ERowSpace.Value;
    ColSpace:= EColSpace.Value;
    //LabelImage.Pixel:= 7;
    FlagDistance:= ESequencHeight.Value;
    FlagLong:= EFlagWidth.Value;
    PointOffset:= EPointOffset.Value;
    //Gauge1.MaxValue:= Items.Count;
    Gauge1.MaxValue:= RowCount * ColCount * SectionCount;
    Gauge1.Visible:= true;
    EPageCount.Caption:= '0';
  end;
end;

procedure TFPrint.BitBtn2Click(Sender: TObject);
begin     //导入
  with OpenDialog1 do
    if Execute then
      ListRecords.Items.LoadFromFile(Filename);
end;

procedure TFPrint.BitBtn1Click(Sender: TObject);
begin     //导出
  with SaveDialog1 do
    if Execute then
      ListRecords.Items.SaveToFile(Filename);
end;

procedure TFPrint.btnSelectedAllClick(Sender: TObject);
var
  i: Integer;
begin
  for i:= 0 to lstPrinter.Items.Count - 1 do
    lstPrinter.Checked[i]:= True;
end;

procedure TFPrint.btnSelectedNoneClick(Sender: TObject);
var
  i: Integer;
begin
  for i:= 0 to lstPrinter.Items.Count - 1 do
    lstPrinter.Checked[i]:= False;
end;

procedure TFPrint.GetNextPrint;
var
  i: Integer;
  mdevice : array[0..255] of char;
  mdriver : array[0..255] of char;
  mport : array[0..255] of char;
  mhdmode : thandle;
  mpdmode : pdevmode;
begin
  i:= FCurrentPrintIDX;
  repeat
    if lstPrinter.Checked[i] then begin
      printer.getprinter(mdevice, mdriver, mport, mhdmode);
      printer.setprinter(PChar(lstPrinter.Items[i]), mdriver, mport, mhdmode); //设置打印机
      Memo1.Lines.Add(lstPrinter.Items[i] + ' ' + IntToStr(mhdMode));
      FCurrentPrintIDX:= i + 1;      
      if FCurrentPrintIDX >= lstPrinter.Count then FCurrentPrintIDX:= 0;
      Exit;
    end;
    Inc(i); 
    if i >= lstPrinter.Count then i:= 0;
  until i = FCurrentPrintIDX;
  raise Exception.Create('机组内未指定打印机。');

  //  Memo1.Lines.Add(lstPrinter.Items[FCurrentPrintIDX]);
  for i:= FCurrentPrintIDX to lstPrinter.Count - 1 do
    if lstPrinter.Checked[i] then begin
      printer.getprinter(mdevice, mdriver, mport, mhdmode);
      printer.setprinter(PChar(lstPrinter.Items[i]), mdriver, mport, mhdmode); //设置打印机
      FCurrentPrintIDX:= i;
      Break;
    end;
  Inc(FCurrentPrintIDX);
  if FCurrentPrintIDX >= lstPrinter.Count then FCurrentPrintIDX:= 0;
end;

procedure TFPrint.btnSchemeDeleteClick(Sender: TObject);
begin     //提取打印参数
  if ESchemeList.ItemIndex < 0 then Exit;
  if MessageDlg('真的要删除这个打印方案吗?', mtInformation,
    [mbOK, mbCancel], 0) <> mrOK then Exit;
  with DCDelScheme do begin
    Parameters[0].Value:= ESchemeList.Items[ESchemeList.ItemIndex];
    Execute;
  end;
end;

procedure TFPrint.lstPrinterClick(Sender: TObject);
begin
  lstPrinter.Hint:= lstPrinter.Items[lstPrinter.ItemIndex];
end;

procedure TFPrint.SetCurrentLabel(const Value: Integer);
begin
  FCurrentLabel := Value;
end;

procedure TFPrint.btnReadCardClick(Sender: TObject);
var
  Idx: Integer;

  function ReadCard: Boolean;
  begin
    Result:= FComm.Open('COM' + IntToStr(ComMID), 9600);
    if Result then FComm.Start
    else begin
      Inc(FComMID);
      if FComMID < 10 then Result:= ReadCard;
    end;
  end;
begin     //读取卡内环号      
  Screen.Cursor:= crHourGlass;
  if FComMID = 0 then FComMID:= 1;
  if not ReadCard then begin
    FComMID:= StrToInt(InputBox('通讯管理', '录入COM口序号', '1'));
  end;
  Timer1.Enabled:= True;
end;

procedure TFPrint.btnImportCardClick(Sender: TObject);
var
  i, cnt: Integer;
begin
  cnt:= ECardCount.Value;
  if(cnt = 0)or(cnt > lstCard.Count)then
    cnt:= lstCard.Count;
  for i:= 0 to cnt - 1 do
    ListRecords.Items.Add(lstCard.Items[i]);
end;

procedure TFPrint.SetComMID(const Value: Integer);
begin
  FComMID := Value;
end;

procedure TFPrint.Timer1Timer(Sender: TObject);
var
  i, cnt: Integer;
begin
  if FComm.Opened then Exit;
  lbCardNo.Caption:= FComm.MachineID;
  lstCard.Items.Assign(FComm.Labels);
//  for i:= 0 to FComm.Labels.Count - 1 do
//    FFlyLabelDB.AddTask(FCurrentLabel, 3, lbCardNo.Caption, FComm.Labels[i]);
  lbCardCount.Caption:= IntToStr(FComm.LabelCount);
  FComm.Close;
  Timer1.Enabled:= False;   
  Screen.Cursor:= crDefault;
end;

procedure TFPrint.BitBtn3Click(Sender: TObject);
begin
  if lstCard.ItemIndex < 0 then Exit;
  if MessageDlg('是否确定要删除' + lstCard.Items[lstCard.ItemIndex] + '这个补录标签吗?',
    mtWarning, mbOKCancel, 0) <> mrOK then Exit;
//  FFlyLabelDB.AddTask(FCurrentLabel, 4, lbCardNo.Caption, lstCard.Items[lstCard.ItemIndex]);
  lstCard.Items.Delete(lstCard.ItemIndex);
end;

procedure TFPrint.btnClearCardClick(Sender: TObject);
var
  Idx: Integer;

  function ClearCard: Boolean;
  begin
    Result:= FComm.Open('COM' + IntToStr(ComMID), 9600);
    if Result then FComm.Clear
    else begin
      Inc(FComMID);
      if FComMID < 10 then Result:= ClearCard;
    end;
  end;
begin     //读取卡内环号
  Screen.Cursor:= crHourGlass;
  if FComMID = 0 then FComMID:= 1;
  if not ClearCard then begin
    FComMID:= StrToInt(InputBox('通讯管理', '录入COM口序号', '1'));
  end;
  FComm.Close;
  Screen.Cursor:= crDefault;
end;

end.
