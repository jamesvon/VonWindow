unit UThreadLabel;

interface

uses
  Types, Classes, Printers, SysUtils, Forms, Math, ShellAPI, ExtCtrls, Dialogs,
  Windows, Graphics, UFlyLabelDB, UVonClass, UVonQRCode, UVonLog, UVonGraphicAPI;

type
  TLabelBmp = class (TThread)
  private
    { Private declarations }
    FQR: TQRCode;
    FParams: TVonConfig;                     //FParams 动态标签内容
    FLabelValues: TVonSetting;
  private
    FBaseBmp: TBitmap;
    FTmpBmp: TBitmap;
    procedure DrewTextBmp(bmp: TBitmap; SectionName, Value: string);
    procedure DrewGrpBmp(bmp: TBitmap; SectionName, Value: string);
    procedure DrewCodeBmp(bmp: TBitmap; SectionName, Value: string);
    function QRCode(Code: PChar; Params: TStrings): TBitmap;
    function GetCopyMode(ModeID: Integer): TCopyMode;
    /// <summary>在基本图的基础上画标签</summary>
    procedure Drew(bmp: TBitmap; Text: string);
    procedure SetLabelValues(const Value: TVonSetting);
    procedure SetParams(const Value: TVonConfig);
  public
    { Public declarations }
    constructor Create; overload;
    destructor Destroy; override;
    procedure Execute; override;
  published
    { Published declarations }
    /// <summary>动态标签设置</summary>
    property Params: TVonConfig read FParams write SetParams;
    /// <summary>用户执行参数值</summary>
    property LabelValues: TVonSetting read FLabelValues write SetLabelValues;
  end;

  TThreadPrint = class(TThread)
  private
    FList: TList;
    FItems: TStrings;
  private
    FRowCount: Integer;
    FColSpace: Integer;
    FLabelHeight: Integer;
    FRowSpace: Integer;
    FTopMargin: Integer;
    FLabelWidth: Integer;
    FLeftMargin: Integer;
    FColCount: Integer;
    FSectionCount: Integer;
    FPixel: Integer;
    FParams: TVonConfig;
    FLabelValues: TVonSetting;
    FBaseBmp: TBitmap;
    procedure SetBaseBmp(const Value: TBitmap);
    procedure SetColCount(const Value: Integer);
    procedure SetColSpace(const Value: Integer);
    procedure SetItems(const Value: TStrings);
    procedure SetLabelHeight(const Value: Integer);
    procedure SetLabelValues(const Value: TVonSetting);
    procedure SetLabelWidth(const Value: Integer);
    procedure SetLeftMargin(const Value: Integer);
    procedure SetParams(const Value: TVonConfig);
    procedure SetPixel(const Value: Integer);
    procedure SetRowCount(const Value: Integer);
    procedure SetRowSpace(const Value: Integer);
    procedure SetSectionCount(const Value: Integer);
    procedure SetTopMargin(const Value: Integer);
    function GetThreadCount: Integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Execute; override;
  published
    { Published declarations }
    /// <summary>基本图形</summary>
    property BaseBmp: TBitmap read FBaseBmp write SetBaseBmp;
    /// <summary>动态标签设置</summary>
    property Params: TVonConfig read FParams write SetParams;
    /// <summary>用户执行参数值</summary>
    property LabelValues: TVonSetting read FLabelValues write SetLabelValues;
    property LabelWidth: Integer read FLabelWidth write SetLabelWidth;
    property LabelHeight: Integer read FLabelHeight write SetLabelHeight;
    property SectionCount: Integer read FSectionCount write SetSectionCount;
    property ColCount: Integer read FColCount write SetColCount;
    property RowCount: Integer read FRowCount write SetRowCount;
    property TopMargin: Integer read FTopMargin write SetTopMargin;
    property LeftMargin: Integer read FLeftMargin write SetLeftMargin;
    property RowSpace: Integer read FRowSpace write SetRowSpace;
    property ColSpace: Integer read FColSpace write SetColSpace;
    property Items: TStrings read FItems write SetItems;
    property Pixel: Integer read FPixel write SetPixel;
    property ThreadCount: Integer read GetThreadCount;
  end;

implementation

uses UVonSystemFuns, DCPsha1;

var
  CriticalSection : TRTLCriticalSection;  //定义临界区，防止标签重复生产
  FLabelItems: TStrings;                  //待生成标签列表，Object 为生成的 Bmp
  //生成过程中使用的临时变量
  GlobalIdx,                              //GlobalIdx 当前标签提取位置,
  ThreadRuning: Integer;                  //ThreadRuning 生成执行数量

procedure ChangeBytes(var data: array of byte);
var
  tmp: byte;       //0 1 2 3 4 5  6 7 8  9  0  1 2 3 4  5  6
begin     //存储顺序 3,8,0,4,1,10,2,5,12,15,11,7,6,9,16,13,14,
  tmp:= data[0];
  data[0]:= data[3];
  data[3]:= data[4];
  data[4]:= data[1];
  data[1]:= data[8];
  data[8]:= data[12];
  data[12]:= data[6];
  data[6]:= data[2];
  data[2]:= tmp;
  tmp:= data[5];
  data[5]:= data[10];
  data[10]:= data[11];
  data[11]:= data[7];
  data[7]:= tmp;
  tmp:= data[9];
  data[9]:= data[15];
  data[15]:= data[13];
  data[13]:= tmp;
end;

{ TLabelBmp }

constructor TLabelBmp.Create;
begin
  Inherited Create(True);
  FQR:= TQRCode.Create(nil);
  FTmpBmp:= TBitmap.Create;
end;

destructor TLabelBmp.Destroy;
begin
  FTmpBmp.Free;
  FQR.Free;
  Inherited;
end;

procedure TLabelBmp.Execute;
var
  Idx: Integer;
  Code: string;
  bmp: TBitmap;
begin
  inherited;
  WriteLog(LOG_DEBUG, 'TLabelBmp', 'begin---------------');
  while True do begin
    while GlobalIdx < FLabelItems.Count do begin
      EnterCriticalSection(CriticalSection);    //进入临界区
      Idx:= GlobalIdx;
      Inc(GlobalIdx);
      LeaveCriticalSection(CriticalSection);    //离开临界区
      if Idx >= FLabelItems.Count then break;
      Code:= FLabelItems[Idx];
      bmp:= TBitmap(FLabelItems.Objects[Idx]);
      Drew(bmp, Code);
      WriteLog(LOG_DEBUG, 'TLabelBmp', Format('Print %d(%s)', [Idx, Code]));
    end;
    Dec(ThreadRuning);
    Suspend;
  end;
  WriteLog(LOG_DEBUG, 'TLabelBmp', '-------------Finally');
end;

function TLabelBmp.GetCopyMode(ModeID: Integer): TCopyMode;
begin
  case ModeID of
  0: Result:= cmBlackness;  //Fills the destination rectangle on the canvas with black.
  1: Result:= cmDstInvert;  //Inverts the image on the canvas and ignores the source.
  2: Result:= cmMergeCopy;  //Combines the image on the canvas and the source bitmap by using the Boolean AND operator.
  3: Result:= cmMergePaint; //Combines the inverted source bitmap with the image on the canvas by using the Boolean OR operator.
  4: Result:= cmNotSrcCopy; //Copies the inverted source bitmap to the canvas.
  5: Result:= cmNotSrcErase;//Combines the image on the canvas and the source bitmap by using the Boolean OR operator, and inverts the result.
  6: Result:= cmPatCopy;    //Copies the source pattern to the canvas.
  7: Result:= cmPatInvert;  //Combines the source pattern with the image on the canvas using the Boolean XOR operator
  8: Result:= cmPatPaint;   //Combines the inverted source bitmap with the source pattern by using the Boolean OR operator. Combines the result of this operation with the image on the canvas by using the Boolean OR operator.
  9: Result:= cmSrcAnd;     //Combines the image on the canvas and source bitmap by using the Boolean AND operator.
  10: Result:= cmSrcCopy;   //Copies the source bitmap to the canvas.
  11: Result:= cmSrcErase;  //Inverts the image on the canvas and combines the result with the source bitmap by using the Boolean AND operator.
  12: Result:= cmSrcInvert; //Combines the image on the canvas and the source bitmap by using the Boolean XOR operator.
  13: Result:= cmSrcPaint;  //Combines the image on the canvas and the source bitmap by using the Boolean OR operator.
  14: Result:= cmWhiteness; //Fills the destination rectangle on the canvas with white.
  end;
end;

procedure TLabelBmp.Drew(bmp: TBitmap; Text: string);
var
  I: Integer;
begin
  for I := 0 to FParams.SectionCount - 1 do try
    case FParams.ReadInteger(FParams.Sections[I], 'Kind', 0) of
    0: DrewTextBmp(bmp, FParams.Sections[I], Text);
    1: DrewGrpBmp(bmp, FParams.Sections[I], Text);
    2: DrewCodeBmp(bmp, FParams.Sections[I], Text);
    end;
  except
    on E: Exception do
      WriteLog(LOG_FAIL, 'TLabelBmp.Drew', Format('KIND=%d Text=%s',
        [FParams.ReadInteger(FParams.Sections[I], 'Kind', 0), Text]));
  end;
end;

procedure TLabelBmp.DrewCodeBmp(bmp: TBitmap; SectionName, Value: string);
var
  i: Integer;
  szList: TStringList;
  S, szValue: string;
  szBmp: TBitmap;
begin
  WriteLog(LOG_Debug, 'DrewCodeBmp', SectionName);
  if(SectionName = '')or(Value = '')then Exit;
  szList:= TStringList.Create;
  szList.Delimiter:= ',';
  with FParams.GetDirectionSection(SectionName) do try
    szList.DelimitedText:= Values['CodeValue'];
    szValue:= '';
    for I := 0 to szList.Count - 1 do begin
      S:= FLabelValues.NameValue[szList[I]];
      if S = '' then S:= FParams.ReadString(szList[I], 'ExampleText', '0');
      szValue:= szValue + S;
    end;
    szValue:= szValue + Value;
    szBmp:= QRCode(PChar(szValue), FParams.GetDirectionSection(SectionName));
    Bmp.Canvas.CopyMode:= GetCopyMode(StrToInt(Values['CopyMode']));
    Bmp.Canvas.Draw(StrToInt(Values['Left']), StrToInt(Values['Top']), szBmp);  //rotateBmp
  finally
    szList.Free;
  end;
end;

procedure TLabelBmp.DrewGrpBmp(bmp: TBitmap; SectionName, Value: string);
begin

end;

procedure TLabelBmp.DrewTextBmp(bmp: TBitmap; SectionName, Value: string);
var
  i: Integer;
begin
  WriteLog(LOG_Debug, 'DrewTextBmp', SectionName);
  if(SectionName = '')or(Value = '')then Exit;
  FTmpBmp.Canvas.FillRect(Rect(0, 0, FTmpBmp.Width, FTmpBmp.Height));
  with FTmpBmp.Canvas, FParams.GetDirectionSection(SectionName) do begin
    with Font do begin
      Name:= Values['FontName'];
      Size:= StrToInt(Values['FontSize']);
      Color:= StrToInt(Values['FONTCOLOR']);
      Style:= [];
      if Values['Bold'] = '1' then Style:= Style + [fsBold];
      if Values['Italic'] = '1' then Style:= Style + [fsItalic];
      if Values['Underline'] = '1' then Style:= Style + [fsUnderline];
      if Values['StrikeOut'] = '1' then Style:= Style + [fsStrikeOut];
    end;
//    szBmp.Width:= Max(StrToInt(Values['LEN']) * StrToInt(Values['WordWidth']),
//      Length(Value) * StrToInt(Values['WordWidth']));
//    szBmp.Height:= TextHeight('Yj');
//    szBmp.Width:= bmp.Width; szBmp.Height:= bmp.Height;
//    FillRect(Rect(0, 0, szBmp.Width, szBmp.Height));
    case StrToInt(Values['Align']) of
    0: for i:= 1 to Length(Value) do                                            //左对齐
      TextOut(StrToInt(Values['WordWidth']) * (i - 1), 0, Value[i]);
    1: for i:= 1 to Length(Value) do                                            //居中
      TextOut(StrToInt(Values['WordWidth']) * (StrToInt(Values['LEN']) -
        Length(Value)) div 2 + StrToInt(Values['WordWidth']) * (i - 1), 0, Value[i]);
    2: for i:= Length(Value) downto 1 do                                        //右对齐
      TextOut(StrToInt(Values['WordWidth']) *
        StrToInt(Values['LEN']) - StrToInt(Values['WordWidth']) * i, 0, Value[i]);
    3: if Length(Value) = 1 then                                                //
      TextOut(StrToInt(Values['WordWidth']) * (StrToInt(Values['LEN']) - Length(Value)) div 2, 0, Value[i])
    else for i:= 1 to Length(Value) do                                          //
      TextOut(StrToInt(Values['WordWidth']) * (StrToInt(Values['LEN']) - Length(Value)) * (i - 1), 0, Value[i]);
    end;
    Bmp.Canvas.CopyMode:= GetCopyMode(StrToInt(Values['CopyMode']));
    Bmp.Canvas.Draw(StrToInt(Values['Left']), StrToInt(Values['Top']), FTmpBmp);
  end;
end;

function TLabelBmp.QRCode(Code: PChar; Params: TStrings): TBitmap;
var
  szI: Integer;
  szCode: array[0..16] of byte;
  digest: array[0..19] of byte;
  S: string;
  Hash: TDCP_sha1;
begin
  S:= string(Code);
  if Length(S) < 12 then S:= Copy(S + '000000000000', 1, 12);                                           //123456000000
  if not TryStrToInt(Copy(S, 1, 4), szI) then Exit;       //Year -> 7F                                  //1234
  szCode[0]:= (szI + 1) and $7F;                          //1 1111111        FF                         //4D2->1001 1010010 0x52    1101101
  if not TryStrToInt(Copy(S, 5, 2), szI) then Exit;       //Area -> 7F                                  //56
  szCode[1]:= szI and $7F;                                //1 111111                                    //38 ->0000 0111000 0x38
  if not TryStrToInt(Copy(S, 7, MaxInt), szI) then Exit;  //Code -> 7FFFFF 1111111 11111111 11111111    //                  0x00
  szCode[2]:= (szI shr 16) and $7F;                       //        FFFF           1111111 1 11111111   //                  0x00
  szCode[3]:= (szI shr 9) and $7F;                        //        7F                     1111111 11   //                  0x00
  szCode[4]:= (szI shr 2) and $7F;                        //        7F                             11   //                  0x00
  szCode[5]:= (szI and $3) shl 5;                                                                       //                  0x00
  szCode[6]:=  Ord('@');                                                                                //                  0x00
  szCode[7]:=  Ord('j');                                                                                //                  0x00
  szCode[8]:=  Ord('A');                                                                                //                  0x00
  szCode[9]:=  Ord('m');                                                                                //                  0x00
  szCode[10]:= Ord('E');                                                                                //                  0x00
  szCode[11]:= Ord('5');                                                                                //                  0x00
  szCode[12]:= Ord('_');                                                                                //                  0x00
  szCode[13]:= Ord('v');                                                                                //                  0x00
  szCode[14]:= Ord('0');                                                                                //                  0x00
  szCode[15]:= Ord('N');                                                                                //                  0x00
  szCode[16]:= Ord('%');                                                                                //                  0x00
//  WriteLog(LOG_Debug, 'orgCode',
//      Format('%s, %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d',
//      [S, szCode[0], szCode[1], szCode[2], szCode[3], szCode[4], szCode[5],
//      szCode[6], szCode[7], szCode[8], szCode[9], szCode[10], szCode[11], szCode[12],
//      szCode[13], szCode[14], szCode[15], szCode[16]]));
  Hash := TDCP_sha1.Create(nil);  // create the hash
  Hash.Init;                      // initialize it
  Hash.Update(szCode[0], 17);     // hash the stream contents
  Hash.Final(Digest);             // produce the digest
  Hash.Free;
  for szI := 0 to 9 do
    Digest[szI]:= Digest[szI] xor Digest[19 - szI];
  szCode[5]:= szCode[5] + Digest[0] shr 3;                    //11 11111      0
  szCode[6]:= (Digest[0] and $7) shl 4 + Digest[1] shr 4;     //111 1111      0 1
  szCode[7]:= (Digest[1] and $F) shl 3 + Digest[2] shr 5;     //1111 111      1 2
  szCode[8]:= (Digest[2] and $1F) shl 2 + Digest[3] shr 6;    //11111 11      2 3
  szCode[9]:= (Digest[3] and $3F) shl 1 + Digest[4] shr 7;    //111111 1      3 4
  szCode[10]:= (Digest[4] and $7F);                           //1111111       4
  szCode[11]:= (Digest[5] and $FF) shr 1;                     //1111111       5
  szCode[12]:= (Digest[5] and $1) shl 6 + Digest[6] shr 2;    //1 111111      5 6
  szCode[13]:= (Digest[6] and $3) shl 5 + Digest[7] shr 3;    //11 11111      6 7
  szCode[14]:= (Digest[7] and $7) shl 4 + Digest[8] shr 4;    //111 1111      7 8
  szCode[15]:= (Digest[8] and $F) shl 3 + Digest[9] shr 5;    //1111 111      8 9
  szCode[16]:= (Digest[9] and $1F) shl 2 + Digest[10] shr 6;  //11111 11      9 A
  ChangeBytes(szCode);
  FQR.SymbolEnabled:= False;
  FQR.ClearOption:= True;
  FQR.SymbolColor:= clBlack;
  FQR.BackColor:= clWhite;
  FQR.Match:= True;
  if Params.Values['Version'] = '' then FQR.Version:= 1
  else FQR.Version:= StrToInt(Params.Values['Version']);
  FQR.Pxmag:= StrToInt(Params.Values['CODESIZE']);
  FQR.PrnDpi:= StrToFloat(Params.Values['PrnDpi']);  //720 dpi / 英寸 = 28.3465 pixels/mm;
  if Params.Values['Diameter'] = '' then FQR.Radius:= 4.25
  else FQR.Radius:= StrToFloat(Params.Values['Diameter']) / 2;
  FQR.Mode:= qrSingle;
  FQR.Numbering:= nbrIfVoid;   //nbrNone, nbrHead, nbrTail, nbrIfVoid
  FQR.BinaryOperation:= True;
  if Params.Values['Rotate'] <> '' then
    FQR.Angle:= StrToInt(Params.Values['Rotate']);
//    FQR.Binary:= True;
  FQR.SymbolEnabled:= True;
  FQR.WriteData(szCode, 11);
//  WriteLog(LOG_Debug, 'CodeToBmp',
//      Format('%s, %s, %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d',
//      [BoolToStr(szQR.SymbolDisped, true), S, szCode[0], szCode[1], szCode[2], szCode[3], szCode[4], szCode[5],
//      szCode[6], szCode[7], szCode[8], szCode[9], szCode[10], szCode[11], szCode[12],
//      szCode[13], szCode[14], szCode[15], szCode[16]]));
  Result:= FQR.Picture.Bitmap;
end;

procedure TLabelBmp.SetLabelValues(const Value: TVonSetting);
begin
  FLabelValues := Value;
end;

procedure TLabelBmp.SetParams(const Value: TVonConfig);
begin
  FParams := Value;
end;

{ TThreadPrint }

constructor TThreadPrint.Create;
begin
  WriteLog(LOG_Debug, 'TThreadPrint', 'ThreadPrint created.');
  inherited Create(True);
  FList:= TList.Create;                //缓存进程
  FItems:= TStringList.Create;         //缓存标签
//  FParams:= TVonConfig.Create;         //缓存动态标签内容设置
//  FLabelValues:= TVonSetting.Create;   //缓存执行参数
//  FBaseBmp:= TBitmap.Create;           //缓存背景图
  FreeOnTerminate:= True;
  InitializeCriticalSection(CriticalSection);
end;

destructor TThreadPrint.Destroy;
var
  I: Integer;
begin
  for I := 0 to FLabelItems.Count - 1 do
    FLabelItems.Objects[I].Free;          //释放标签空间
  FLabelItems.Clear;
  FItems.Free;
  FList.Free;
//  FParams.Free;
//  FLabelValues.Free;
//  FBaseBmp.Free;
  inherited;
end;

procedure TThreadPrint.Execute;
var
  I, CurrentX, CurrentCol, CurrentY, CurrentID, CurrentRow: Integer;
  bmp: TBitmap;
  function newBaseBmp: TBitmap;
  begin
    Result:= TBitmap.Create;
    Result.Assign(FBaseBmp);
  end;
  function newLabelBmp: TLabelBmp;
  begin
    Result:= TLabelBmp.Create;
    Result.FTmpBmp.Assign(FBaseBmp);
    Result.FTmpBmp.Canvas.FillRect(Rect(0, 0, FBaseBmp.Width, FBaseBmp.Height));
    Result.Params:= FParams;
    Result.LabelValues:= FLabelValues;
  end;
begin
  inherited;
  WriteLog(LOG_Debug, 'TThreadPrint', 'Begin of printing.');
  CurrentID:= 0;
  for I := 0 to 2 - 1 do   //FColCount * FRowCount;
    FList.Add(newLabelBmp);                             //添加进程
  for I := 0 to FColCount * FRowCount - 1 do
    FLabelItems.AddObject('缓存', newBaseBmp);          //初始化待处理标签
  Printer.Title:= Format('LB%s', [FItems[0]]);
  Printer.BeginDoc;
  while CurrentID < FItems.Count - 1 do begin
    {$region '准备打印,初始化标签和打印位置'}
    if CurrentID <> 0 then Printer.NewPage;
    GlobalIdx:= 0;
    ThreadRuning:= FList.Count;
    for I := 0 to FColCount * FRowCount - 1 do begin    //初始化待处理标签
      FLabelItems[I]:= FItems[CurrentID + I];
      TBitmap(FLabelItems.Objects[I]).Assign(FBaseBmp);
    end;
    Inc(CurrentID, FColCount * FRowCount);
    {$endregion}
    for I := 0 to FList.Count - 1 do                    //启动标签生成
      if TLabelBmp(FList[I]).Suspended then
        TLabelBmp(FList[I]).Resume
      else TLabelBmp(FList[I]).Start;
    while ThreadRuning > 0 do begin end;                //等待
    {$region '开始打印'}
    CurrentX:= 0; CurrentY:= 0;
    for I := 0 to FLabelItems.Count - 1 do
      with Printer.Canvas do begin
        bmp:= TBitmap(FLabelItems.Objects[I]);
        FillRect(Rect(CurrentX, CurrentY, CurrentX + bmp.Width, CurrentY + bmp.Height));
        Draw(CurrentX, CurrentY, bmp);
        Inc(CurrentX, LabelWidth + ColSpace);
        Inc(CurrentCol);
        if CurrentCol = ColCount then begin
          CurrentX:= 0;
          CurrentCol:= 0;
          Inc(CurrentY, LabelHeight + RowSpace)
        end;
        FItems.Objects[I].Free;
      end;
    {$endregion}
  end;
  Printer.EndDoc;
  for I := 0 to FList.Count - 1 do begin
    TLabelBmp(FList[I]).Terminate;
    TLabelBmp(FList[I]).Free;
  end;
  FLabelItems.Clear;
  FList.Clear;
  WriteLog(LOG_DEBUG, 'TFastPrint.Execute', 'end');
end;

function TThreadPrint.GetThreadCount: Integer;
begin
  Result:= FList.Count;
end;

procedure TThreadPrint.SetBaseBmp(const Value: TBitmap);
begin
  FBaseBmp:= Value;
//  FBaseBmp.Assign(Value);
end;

procedure TThreadPrint.SetColCount(const Value: Integer);
begin
  FColCount := Value;
end;

procedure TThreadPrint.SetColSpace(const Value: Integer);
begin
  FColSpace := Value;
end;

procedure TThreadPrint.SetItems(const Value: TStrings);
begin
  FItems.Assign(Value);
end;

procedure TThreadPrint.SetLabelHeight(const Value: Integer);
begin
  FLabelHeight := Value;
end;

procedure TThreadPrint.SetLabelValues(const Value: TVonSetting);
begin
  FLabelValues := Value;
end;

procedure TThreadPrint.SetLabelWidth(const Value: Integer);
begin
  FLabelWidth := Value;
end;

procedure TThreadPrint.SetLeftMargin(const Value: Integer);
begin
  FLeftMargin := Value;
end;

procedure TThreadPrint.SetParams(const Value: TVonConfig);
begin
  FParams:= Value;
//  FParams.Assign(Value);
end;

procedure TThreadPrint.SetPixel(const Value: Integer);
begin
  FPixel := Value;
end;

procedure TThreadPrint.SetRowCount(const Value: Integer);
begin
  FRowCount := Value;
end;

procedure TThreadPrint.SetRowSpace(const Value: Integer);
begin
  FRowSpace := Value;
end;

procedure TThreadPrint.SetSectionCount(const Value: Integer);
begin
  FSectionCount := Value;
end;

procedure TThreadPrint.SetTopMargin(const Value: Integer);
begin
  FTopMargin := Value;
end;


initialization
  FLabelItems:= TStringList.Create;

finalization
  FLabelItems.Free;

end.
