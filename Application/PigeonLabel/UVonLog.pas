unit UVonLog;

interface

uses
  SysUtils, Classes, Windows, forms, SyncObjs, Registry, SvcMgr;

const
  CONST_PIPEFORMAT = '\\%s\pipe\%s';
  VON_LOGSERVICE_NAME = 'VONLOGSERVICE';

resourcestring
  RES_LOG_NO_OPEN = 'The log is not opened.';
  RES_LOG_CONNECT_FAILED = '连接日志管理器失败';
  RES_LOG_WRITE_ERROR = '写日志失败';

type
  { LOG 日志管理部分 }
  /// <summary>日志级别，从大到小排列</summary>
  /// <param name="LOG_MAJOR">重要信息</param>
  /// <param name="LOG_FAIL">错误或失败信息</param>
  /// <param name="LOG_WARNING">警告信息</param>
  /// <param name="LOG_INFO">一般信息</param>
  /// <param name="LOG_DEBUG">调试信息</param>
  ELOG_Class = (LOG_MAJOR, LOG_FAIL, LOG_WARNING, LOG_INFO, LOG_DEBUG);

  TVonLogFile = class
  private
    FLog_File: TextFile;
    FCurrentDate: TDatetime;
    FLogClass: ELOG_Class;
    FAppName: string;
    FLogPath: string;
    procedure SetCurrentDate(const Value: TDatetime);
    procedure SetLogClass(const Value: ELOG_Class);
  public
    constructor Create(ApplicationName, LogPath: string);
    destructor Destory;
    procedure WriteLog(LogClass: ELOG_Class; Process, Info: string);
  published
    property AppName: string read FAppName;
    property LogPath: string read FLogPath;
    property CurrentDate: TDatetime read FCurrentDate write SetCurrentDate;
    property LogClass: ELOG_Class read FLogClass write SetLogClass;
  end;

  /// <summary>打开日志文件</summary>
procedure OpenLog(AppName, LogPath: string; AvailClass: ELOG_Class);
/// <summary>写日志</summary>
/// <param name="LogClass">日志等级</param>
/// <param name="Process">进程名称</param>
/// <param name="Info">日志内容</param>
procedure WriteLog(LogClass: ELOG_Class; Process, Info: string); overload;
procedure WriteLog(AppName: string; LogClass: ELOG_Class; Process, Info: string); overload;
/// <summary>关闭日志</summary>
procedure CloseLog(AppName: string = '');
/// <summary>写本地日志</summary>
/// <param name="LogClass">日志等级</param>
/// <param name="Process">进程名称</param>
/// <param name="Info">日志内容</param>
procedure WriteLocalLog(LogClass: ELOG_Class; Process, Info: string);

var
  LOG_Files: TStringList;
  FEventLogger: TEventLogger;

implementation

uses DateUtils;

{ LOG }

procedure OpenLog(AppName, LogPath: string; AvailClass: ELOG_Class);
var
  Idx: Integer;
  AppLog: TVonLogFile;
begin
  Idx:= LOG_Files.IndexOf(AppName);
  if Idx < 0 then begin
    AppLog:= TVonLogFile.Create(AppName, LogPath);
    LOG_Files.AddObject(AppName, AppLog);
  end else AppLog:= TVonLogFile(LOG_Files.Objects[Idx]);
  AppLog.LogClass:= AvailClass;
//  FEventLogger.LogMessage(AppName + ' Log service is opened on ' + LogPath, 4);
end;

procedure WriteLog(LogClass: ELOG_Class; Process, Info: string);
begin
  WriteLog(LOG_Files[0], LogClass, Process, Info);
end;

procedure WriteLog(AppName: string; LogClass: ELOG_Class; Process, Info: string);
var
  Idx: Integer;
  AppLog: TVonLogFile;
begin
  Idx:= LOG_Files.IndexOf(AppName);
  if Idx < 0 then Exit;
  TVonLogFile(LOG_Files.Objects[Idx]).WriteLog(LogClass, Process, Info);
end;

procedure CloseLog(AppName: string);
var
  Idx: Integer;
  AppLog: TVonLogFile;
begin
  if AppName = '' then Idx:= 0
  else Idx:= LOG_Files.IndexOf(AppName);
  TVonLogFile(LOG_Files.Objects[Idx]).Free;
  LOG_Files.Delete(Idx);
end;

procedure WriteLocalLog(LogClass: ELOG_Class; Process, Info: string);
begin
  WriteLog(LogClass, Process, Info);
end;

{ TVonLogFile }

constructor TVonLogFile.Create(ApplicationName, LogPath: string);
begin
  FAppName:= ApplicationName;
  FLogPath:= LogPath;
  FCurrentDate:= 0;
  CurrentDate:= Now;
end;

destructor TVonLogFile.Destory;
begin
  CloseFile(FLog_File);
end;

procedure TVonLogFile.SetCurrentDate(const Value: TDatetime);
var
  Filename: string;
begin
  if FCurrentDate = DateOf(Value) then Exit;
  if FCurrentDate > 0 then CloseFile(FLog_File);
  Filename:= FLogPath + '\' + FormatDateTime('YYYYMMDD', Value) + '.LOG';
  AssignFile(FLog_File, Filename);
  if FileExists(Filename) then Append(FLog_File)
  else Rewrite(FLog_File);
  FCurrentDate := DateOf(Value);
end;

procedure TVonLogFile.SetLogClass(const Value: ELOG_Class);
begin
  FLogClass := Value;
end;

procedure TVonLogFile.WriteLog(LogClass: ELOG_Class; Process, Info: string);
var
  szS: string;
begin
  if LogClass > FLogClass then Exit;
  CurrentDate:= Now;
  case LogClass of
    LOG_MAJOR:    szS := 'M';
    LOG_FAIL:     szS := 'E';
    LOG_WARNING:  szS := '!';
    LOG_INFO:     szS := ' ';
    LOG_DEBUG:    szS := '?';
  end;
  szS := szS + FormatDatetime('[yyyy-mm-dd hh:nn:ss:zzz]', Now) + Process + #9 + Info;
  WriteLn(FLog_File, szS);
  Flush(FLog_File);
end;

initialization
  LOG_Files:= TStringList.Create;

finalization
  LOG_Files.Free;

end.
