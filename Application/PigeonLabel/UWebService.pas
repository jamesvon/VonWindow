// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://www.zuhuan.cn/webservice/zuhuan/webservice.asmx?WSDL
//  >Import : http://www.zuhuan.cn/webservice/zuhuan/webservice.asmx?WSDL>0
// Encoding : utf-8
// Version  : 1.0
// (2013-1-9 12:41:41 - - $Rev: 34800 $)
// ************************************************************************ //

unit Uwebservice;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

const
  IS_OPTN = $0001;
  IS_UNBD = $0002;
  IS_NLBL = $0004;
  IS_REF  = $0080;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Embarcadero types; however, they could also
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:boolean         - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:int             - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:base64Binary    - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:dateTime        - "http://www.w3.org/2001/XMLSchema"[Gbl]

  OrdersInfo           = class;                 { "http://www.zuhuan.cn/"[GblCplx] }
  StyleInfo            = class;                 { "http://www.zuhuan.cn/"[GblCplx] }

  ArrayOfOrdersInfo = array of OrdersInfo;      { "http://www.zuhuan.cn/"[GblCplx] }


  // ************************************************************************ //
  // XML       : OrdersInfo, global, <complexType>
  // Namespace : http://www.zuhuan.cn/
  // ************************************************************************ //
  OrdersInfo = class(TRemotable)
  private
    FID: Integer;
    FMemberID: Integer;
    FOrderNo: string;
    FOrderNo_Specified: boolean;
    FStyleIdx: Integer;
    FParamValue: string;
    FParamValue_Specified: boolean;
    FStartNo: Integer;
    FNum: Integer;
    FReceiver: string;
    FReceiver_Specified: boolean;
    FAddress: string;
    FAddress_Specified: boolean;
    FZip: string;
    FZip_Specified: boolean;
    FLinker: string;
    FLinker_Specified: boolean;
    FOrderDate: TXSDateTime;
    FStatus: Integer;
    FColorKind: string;
    FColorKind_Specified: boolean;
    FNote: string;
    FNote_Specified: boolean;
    procedure SetOrderNo(Index: Integer; const Astring: string);
    function  OrderNo_Specified(Index: Integer): boolean;
    procedure SetParamValue(Index: Integer; const Astring: string);
    function  ParamValue_Specified(Index: Integer): boolean;
    procedure SetReceiver(Index: Integer; const Astring: string);
    function  Receiver_Specified(Index: Integer): boolean;
    procedure SetAddress(Index: Integer; const Astring: string);
    function  Address_Specified(Index: Integer): boolean;
    procedure SetZip(Index: Integer; const Astring: string);
    function  Zip_Specified(Index: Integer): boolean;
    procedure SetLinker(Index: Integer; const Astring: string);
    function  Linker_Specified(Index: Integer): boolean;
    procedure SetColorKind(Index: Integer; const Astring: string);
    function  ColorKind_Specified(Index: Integer): boolean;
    procedure SetNote(Index: Integer; const Astring: string);
    function  Note_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ID:         Integer      read FID write FID;
    property MemberID:   Integer      read FMemberID write FMemberID;
    property OrderNo:    string       Index (IS_OPTN) read FOrderNo write SetOrderNo stored OrderNo_Specified;
    property StyleIdx:   Integer      read FStyleIdx write FStyleIdx;
    property ParamValue: string       Index (IS_OPTN) read FParamValue write SetParamValue stored ParamValue_Specified;
    property StartNo:    Integer      read FStartNo write FStartNo;
    property Num:        Integer      read FNum write FNum;
    property Receiver:   string       Index (IS_OPTN) read FReceiver write SetReceiver stored Receiver_Specified;
    property Address:    string       Index (IS_OPTN) read FAddress write SetAddress stored Address_Specified;
    property Zip:        string       Index (IS_OPTN) read FZip write SetZip stored Zip_Specified;
    property Linker:     string       Index (IS_OPTN) read FLinker write SetLinker stored Linker_Specified;
    property OrderDate:  TXSDateTime  read FOrderDate write FOrderDate;
    property Status:     Integer      read FStatus write FStatus;
    property ColorKind:  string       Index (IS_OPTN) read FColorKind write SetColorKind stored ColorKind_Specified;
    property Note:       string       Index (IS_OPTN) read FNote write SetNote stored Note_Specified;
  end;



  // ************************************************************************ //
  // XML       : StyleInfo, global, <complexType>
  // Namespace : http://www.zuhuan.cn/
  // ************************************************************************ //
  StyleInfo = class(TRemotable)
  private
    FID: Integer;
    FStyleNo: string;
    FStyleNo_Specified: boolean;
    FStyleName: string;
    FStyleName_Specified: boolean;
    FStyleParams: string;
    FStyleParams_Specified: boolean;
    FStyleBImg: TByteDynArray;
    FStyleBImg_Specified: boolean;
    FStyleSImg: TByteDynArray;
    FStyleSImg_Specified: boolean;
    FBImgType: string;
    FBImgType_Specified: boolean;
    FSImgType: string;
    FSImgType_Specified: boolean;
    FBImgSize: Integer;
    FSImgSize: Integer;
    FImgLinkUrl: string;
    FImgLinkUrl_Specified: boolean;
    FUpdateDate: TXSDateTime;
    FKind1: string;
    FKind1_Specified: boolean;
    FKind2: string;
    FKind2_Specified: boolean;
    FKind3: string;
    FKind3_Specified: boolean;
    FKind4: string;
    FKind4_Specified: boolean;
    FKind5: string;
    FKind5_Specified: boolean;
    procedure SetStyleNo(Index: Integer; const Astring: string);
    function  StyleNo_Specified(Index: Integer): boolean;
    procedure SetStyleName(Index: Integer; const Astring: string);
    function  StyleName_Specified(Index: Integer): boolean;
    procedure SetStyleParams(Index: Integer; const Astring: string);
    function  StyleParams_Specified(Index: Integer): boolean;
    procedure SetStyleBImg(Index: Integer; const ATByteDynArray: TByteDynArray);
    function  StyleBImg_Specified(Index: Integer): boolean;
    procedure SetStyleSImg(Index: Integer; const ATByteDynArray: TByteDynArray);
    function  StyleSImg_Specified(Index: Integer): boolean;
    procedure SetBImgType(Index: Integer; const Astring: string);
    function  BImgType_Specified(Index: Integer): boolean;
    procedure SetSImgType(Index: Integer; const Astring: string);
    function  SImgType_Specified(Index: Integer): boolean;
    procedure SetImgLinkUrl(Index: Integer; const Astring: string);
    function  ImgLinkUrl_Specified(Index: Integer): boolean;
    procedure SetKind1(Index: Integer; const Astring: string);
    function  Kind1_Specified(Index: Integer): boolean;
    procedure SetKind2(Index: Integer; const Astring: string);
    function  Kind2_Specified(Index: Integer): boolean;
    procedure SetKind3(Index: Integer; const Astring: string);
    function  Kind3_Specified(Index: Integer): boolean;
    procedure SetKind4(Index: Integer; const Astring: string);
    function  Kind4_Specified(Index: Integer): boolean;
    procedure SetKind5(Index: Integer; const Astring: string);
    function  Kind5_Specified(Index: Integer): boolean;
  public
    destructor Destroy; override;
  published
    property ID:          Integer        read FID write FID;
    property StyleNo:     string         Index (IS_OPTN) read FStyleNo write SetStyleNo stored StyleNo_Specified;
    property StyleName:   string         Index (IS_OPTN) read FStyleName write SetStyleName stored StyleName_Specified;
    property StyleParams: string         Index (IS_OPTN) read FStyleParams write SetStyleParams stored StyleParams_Specified;
    property StyleBImg:   TByteDynArray  Index (IS_OPTN) read FStyleBImg write SetStyleBImg stored StyleBImg_Specified;
    property StyleSImg:   TByteDynArray  Index (IS_OPTN) read FStyleSImg write SetStyleSImg stored StyleSImg_Specified;
    property BImgType:    string         Index (IS_OPTN) read FBImgType write SetBImgType stored BImgType_Specified;
    property SImgType:    string         Index (IS_OPTN) read FSImgType write SetSImgType stored SImgType_Specified;
    property BImgSize:    Integer        read FBImgSize write FBImgSize;
    property SImgSize:    Integer        read FSImgSize write FSImgSize;
    property ImgLinkUrl:  string         Index (IS_OPTN) read FImgLinkUrl write SetImgLinkUrl stored ImgLinkUrl_Specified;
    property UpdateDate:  TXSDateTime    read FUpdateDate write FUpdateDate;
    property Kind1:       string         Index (IS_OPTN) read FKind1 write SetKind1 stored Kind1_Specified;
    property Kind2:       string         Index (IS_OPTN) read FKind2 write SetKind2 stored Kind2_Specified;
    property Kind3:       string         Index (IS_OPTN) read FKind3 write SetKind3 stored Kind3_Specified;
    property Kind4:       string         Index (IS_OPTN) read FKind4 write SetKind4 stored Kind4_Specified;
    property Kind5:       string         Index (IS_OPTN) read FKind5 write SetKind5 stored Kind5_Specified;
  end;


  // ************************************************************************ //
  // Namespace : http://www.zuhuan.cn/
  // soapAction: http://www.zuhuan.cn/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // use       : literal
  // binding   : ZuHuanServiceSoap12
  // service   : ZuHuanService
  // port      : ZuHuanServiceSoap12
  // URL       : http://www.zuhuan.cn/webservice/zuhuan/webservice.asmx
  // ************************************************************************ //
  ZuHuanServiceSoap = interface(IInvokable)
  ['{15517439-99BC-E4E5-AE4F-E8058F6019B7}']
    function  AddLabelStyle(const lableNo: string; const lableName: string; const lableParam: string; const lblBImg: TByteDynArray; const lblSImg: TByteDynArray; const bImgType: string;
                            const sImgType: string; const bImgSize: Integer; const sImgSize: Integer; const imgLinkUrl: string; const updateDate: TXSDateTime;
                            const kind1: string; const kind2: string; const kind3: string; const kind4: string; const kind5: string
                            ): StyleInfo; stdcall;
    function  ListOrder(const status: Integer): ArrayOfOrdersInfo; stdcall;
    function  UpdateOrderStatus(const id: Integer; const status: Integer): Boolean; stdcall;
  end;

function GetZuHuanServiceSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): ZuHuanServiceSoap;


implementation
  uses SysUtils;

function GetZuHuanServiceSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): ZuHuanServiceSoap;
const
  defWSDL = 'http://www.zuhuan.cn/webservice/zuhuan/webservice.asmx?WSDL';
  defURL  = 'http://www.zuhuan.cn/webservice/zuhuan/webservice.asmx';
  defSvc  = 'ZuHuanService';
  defPrt  = 'ZuHuanServiceSoap12';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as ZuHuanServiceSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


destructor OrdersInfo.Destroy;
begin
  SysUtils.FreeAndNil(FOrderDate);
  inherited Destroy;
end;

procedure OrdersInfo.SetOrderNo(Index: Integer; const Astring: string);
begin
  FOrderNo := Astring;
  FOrderNo_Specified := True;
end;

function OrdersInfo.OrderNo_Specified(Index: Integer): boolean;
begin
  Result := FOrderNo_Specified;
end;

procedure OrdersInfo.SetParamValue(Index: Integer; const Astring: string);
begin
  FParamValue := Astring;
  FParamValue_Specified := True;
end;

function OrdersInfo.ParamValue_Specified(Index: Integer): boolean;
begin
  Result := FParamValue_Specified;
end;

procedure OrdersInfo.SetReceiver(Index: Integer; const Astring: string);
begin
  FReceiver := Astring;
  FReceiver_Specified := True;
end;

function OrdersInfo.Receiver_Specified(Index: Integer): boolean;
begin
  Result := FReceiver_Specified;
end;

procedure OrdersInfo.SetAddress(Index: Integer; const Astring: string);
begin
  FAddress := Astring;
  FAddress_Specified := True;
end;

function OrdersInfo.Address_Specified(Index: Integer): boolean;
begin
  Result := FAddress_Specified;
end;

procedure OrdersInfo.SetZip(Index: Integer; const Astring: string);
begin
  FZip := Astring;
  FZip_Specified := True;
end;

function OrdersInfo.Zip_Specified(Index: Integer): boolean;
begin
  Result := FZip_Specified;
end;

procedure OrdersInfo.SetLinker(Index: Integer; const Astring: string);
begin
  FLinker := Astring;
  FLinker_Specified := True;
end;

function OrdersInfo.Linker_Specified(Index: Integer): boolean;
begin
  Result := FLinker_Specified;
end;

procedure OrdersInfo.SetColorKind(Index: Integer; const Astring: string);
begin
  FColorKind := Astring;
  FColorKind_Specified := True;
end;

function OrdersInfo.ColorKind_Specified(Index: Integer): boolean;
begin
  Result := FColorKind_Specified;
end;

procedure OrdersInfo.SetNote(Index: Integer; const Astring: string);
begin
  FNote := Astring;
  FNote_Specified := True;
end;

function OrdersInfo.Note_Specified(Index: Integer): boolean;
begin
  Result := FNote_Specified;
end;

destructor StyleInfo.Destroy;
begin
  SysUtils.FreeAndNil(FUpdateDate);
  inherited Destroy;
end;

procedure StyleInfo.SetStyleNo(Index: Integer; const Astring: string);
begin
  FStyleNo := Astring;
  FStyleNo_Specified := True;
end;

function StyleInfo.StyleNo_Specified(Index: Integer): boolean;
begin
  Result := FStyleNo_Specified;
end;

procedure StyleInfo.SetStyleName(Index: Integer; const Astring: string);
begin
  FStyleName := Astring;
  FStyleName_Specified := True;
end;

function StyleInfo.StyleName_Specified(Index: Integer): boolean;
begin
  Result := FStyleName_Specified;
end;

procedure StyleInfo.SetStyleParams(Index: Integer; const Astring: string);
begin
  FStyleParams := Astring;
  FStyleParams_Specified := True;
end;

function StyleInfo.StyleParams_Specified(Index: Integer): boolean;
begin
  Result := FStyleParams_Specified;
end;

procedure StyleInfo.SetStyleBImg(Index: Integer; const ATByteDynArray: TByteDynArray);
begin
  FStyleBImg := ATByteDynArray;
  FStyleBImg_Specified := True;
end;

function StyleInfo.StyleBImg_Specified(Index: Integer): boolean;
begin
  Result := FStyleBImg_Specified;
end;

procedure StyleInfo.SetStyleSImg(Index: Integer; const ATByteDynArray: TByteDynArray);
begin
  FStyleSImg := ATByteDynArray;
  FStyleSImg_Specified := True;
end;

function StyleInfo.StyleSImg_Specified(Index: Integer): boolean;
begin
  Result := FStyleSImg_Specified;
end;

procedure StyleInfo.SetBImgType(Index: Integer; const Astring: string);
begin
  FBImgType := Astring;
  FBImgType_Specified := True;
end;

function StyleInfo.BImgType_Specified(Index: Integer): boolean;
begin
  Result := FBImgType_Specified;
end;

procedure StyleInfo.SetSImgType(Index: Integer; const Astring: string);
begin
  FSImgType := Astring;
  FSImgType_Specified := True;
end;

function StyleInfo.SImgType_Specified(Index: Integer): boolean;
begin
  Result := FSImgType_Specified;
end;

procedure StyleInfo.SetImgLinkUrl(Index: Integer; const Astring: string);
begin
  FImgLinkUrl := Astring;
  FImgLinkUrl_Specified := True;
end;

function StyleInfo.ImgLinkUrl_Specified(Index: Integer): boolean;
begin
  Result := FImgLinkUrl_Specified;
end;

procedure StyleInfo.SetKind1(Index: Integer; const Astring: string);
begin
  FKind1 := Astring;
  FKind1_Specified := True;
end;

function StyleInfo.Kind1_Specified(Index: Integer): boolean;
begin
  Result := FKind1_Specified;
end;

procedure StyleInfo.SetKind2(Index: Integer; const Astring: string);
begin
  FKind2 := Astring;
  FKind2_Specified := True;
end;

function StyleInfo.Kind2_Specified(Index: Integer): boolean;
begin
  Result := FKind2_Specified;
end;

procedure StyleInfo.SetKind3(Index: Integer; const Astring: string);
begin
  FKind3 := Astring;
  FKind3_Specified := True;
end;

function StyleInfo.Kind3_Specified(Index: Integer): boolean;
begin
  Result := FKind3_Specified;
end;

procedure StyleInfo.SetKind4(Index: Integer; const Astring: string);
begin
  FKind4 := Astring;
  FKind4_Specified := True;
end;

function StyleInfo.Kind4_Specified(Index: Integer): boolean;
begin
  Result := FKind4_Specified;
end;

procedure StyleInfo.SetKind5(Index: Integer; const Astring: string);
begin
  FKind5 := Astring;
  FKind5_Specified := True;
end;

function StyleInfo.Kind5_Specified(Index: Integer): boolean;
begin
  Result := FKind5_Specified;
end;

initialization
  { ZuHuanServiceSoap }
  InvRegistry.RegisterInterface(TypeInfo(ZuHuanServiceSoap), 'http://www.zuhuan.cn/', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(ZuHuanServiceSoap), 'http://www.zuhuan.cn/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(ZuHuanServiceSoap), ioDocument);
  InvRegistry.RegisterInvokeOptions(TypeInfo(ZuHuanServiceSoap), ioSOAP12);
  { ZuHuanServiceSoap.AddLabelStyle }
  InvRegistry.RegisterMethodInfo(TypeInfo(ZuHuanServiceSoap), 'AddLabelStyle', '',
                                 '[ReturnName="AddLabelStyleResult"]', IS_OPTN);
  { ZuHuanServiceSoap.ListOrder }
  InvRegistry.RegisterMethodInfo(TypeInfo(ZuHuanServiceSoap), 'ListOrder', '',
                                 '[ReturnName="ListOrderResult"]', IS_OPTN);
  InvRegistry.RegisterParamInfo(TypeInfo(ZuHuanServiceSoap), 'ListOrder', 'ListOrderResult', '',
                                '[ArrayItemName="OrdersInfo"]');
  { ZuHuanServiceSoap.UpdateOrderStatus }
  InvRegistry.RegisterMethodInfo(TypeInfo(ZuHuanServiceSoap), 'UpdateOrderStatus', '',
                                 '[ReturnName="UpdateOrderStatusResult"]');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ArrayOfOrdersInfo), 'http://www.zuhuan.cn/', 'ArrayOfOrdersInfo');
  RemClassRegistry.RegisterXSClass(OrdersInfo, 'http://www.zuhuan.cn/', 'OrdersInfo');
  RemClassRegistry.RegisterXSClass(StyleInfo, 'http://www.zuhuan.cn/', 'StyleInfo');

end.
