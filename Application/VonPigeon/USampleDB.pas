unit USampleDB;
{$define debug}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, IniFiles, ZLib, ImgList, IdIOHandler, IdComponent,
  IdIOHandlerSocket, IdIOHandlerStack, IdBaseComponent, IdTCPConnection,
  IdTCPClient, IdHTTP, IdIOHandlerStream, HTTPApp,
  UPlatformDB, UPlatformInfo, UVonLog, System.ImageList;

type
  TFSampleDB = class(TFPlatformDB)
    DQCheckRight: TADOQuery;
    IdHTTP1: TIdHTTP;
    procedure DataModuleCreate(Sender: TObject);
  private
    procedure EventOnUpgrade(Sender: TObject);
    { Private declarations }
  public
    { Public declarations }
    CanRun: Boolean;
    procedure InitData();
    function CheckNetwork: boolean;
  published
  end;

var
  FSampleDB: TFSampleDB;

implementation

uses StrUtils, Math, XMLIntf, XMLDoc, UVonSystemFuns, UPlatformLogin, UPlatformUpgrade;

{$R *.dfm}

procedure TFSampleDB.DataModuleCreate(Sender: TObject);
var
  S: string;
  b: Boolean;
begin
  inherited;
  {$region '连接数据库'}
  ADOConn.Close;
  S := CertValue['DBConnectString'];
  if S <> '' then ADOConn.ConnectionString:= S;
  ADOConn.Open();
  if not ADOConn.Connected then raise Exception.Create('Failed to connect database.');
  {$endregion}
  {$region '自动升级'}
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'Will update ......'); {$endif}
  if CertValue['AutoUpgrade'] = 'True' then
    with TFPlatformUpgrade.Create(nil) do
      try
        OnUpgrade:= EventOnUpgrade;
        CanRun := ShowModal = mrYes;
        if not CanRun then Application.Terminate;
      finally
        Free;
      end;
  {$endregion}
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'Will LoadRuntime ......'); {$endif}
  LoadRuntime;
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'AutoLogin=' + CertValue['AutoLogin']); {$endif}
  {$region 'Login'}
  S:= CertValue['AutoLogin'];
  if S = '' then begin
    CanRun := true;
    LoginInfo.LoginName:= 'Guest';
    LoginInfo.DisplayName:= '匿名用户';
  end else if TryStrToBool(S, b) then begin
    if (not b) then CanRun := Login
    else begin
      CanRun := true;
      LoginInfo.LoginName:= 'Guest';
      LoginInfo.DisplayName:= '匿名用户';
    end
  end else CanRun := LoginInfo.Login(S, S, ADOConn);
  {$endregion}
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'Will LoadUsings ......'); {$endif}
  LoadUsings;
  FOnSay := nil;
end;

procedure TFSampleDB.EventOnUpgrade(Sender: TObject);
var
  szUpgrade: TArray<string>;
begin
  szUpgrade:= CertValue['UpgradeUrl'].Split([',']);
  (Sender as TFPlatFormUpgrade).UpgradeProcess(szUpgrade[0], szUpgrade[1], StrToInt(szUpgrade[2]));
end;

procedure TFSampleDB.InitData();
begin

end;

function TFSampleDB.CheckNetwork: boolean;
begin
  Result:= Ping('8.131.244.41');
end;

end.
