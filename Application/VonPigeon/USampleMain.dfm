inherited FSampleMain: TFSampleMain
  BorderStyle = bsNone
  Caption = 'VonWindows'
  ClientHeight = 519
  ClientWidth = 911
  ExplicitWidth = 911
  ExplicitHeight = 519
  PixelsPerInch = 96
  TextHeight = 14
  inherited plDockSite: TPanel
    Height = 500
    ExplicitHeight = 500
  end
  inherited StatusBar1: TStatusBar
    Top = 500
    Width = 911
    Panels = <
      item
        Style = psOwnerDraw
        Width = 100
      end
      item
        Style = psOwnerDraw
        Width = 200
      end
      item
        Style = psOwnerDraw
        Width = 80
      end
      item
        Style = psOwnerDraw
        Width = 80
      end
      item
        Style = psOwnerDraw
        Width = 150
      end
      item
        Style = psOwnerDraw
        Width = 200
      end
      item
        Style = psOwnerDraw
        Width = 200
      end>
    OnDrawPanel = StatusBar1DrawPanel
    ExplicitTop = 500
    ExplicitWidth = 911
  end
end
