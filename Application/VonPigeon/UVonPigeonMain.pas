unit UVonPigeonMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UPlatformMain, Menus, ImgList, ComCtrls, ToolWin, ExtCtrls, Tabs,
  DockTabSet, StdCtrls, Data.DB, Data.Win.ADODB, IniFiles, System.ImageList,
  UVonSystemFuns;

type
  TFPigeonMain = class(TFPlatformMain)
    Panel1: TPanel;
    imgLogo: TImage;
    lbTitle: TLabel;
    winMenu: TToolBar;
    MainMenu1: TMainMenu;
    menuSystem: TMenuItem;
    menuOption: TMenuItem;
    menuConfig: TMenuItem;
    N11: TMenuItem;
    menuDBTech: TMenuItem;
    menuExit: TMenuItem;
    menuPigeon: TMenuItem;
    menuMatch: TMenuItem;
    menuGrouping: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    menu: TMenuItem;
    procedure StatusBar1DrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel;
      const Rect: TRect);
    procedure menuOptionClick(Sender: TObject);
    procedure menuConfigClick(Sender: TObject);
    procedure menuDBTechClick(Sender: TObject);
    procedure menuExitClick(Sender: TObject);
    procedure menuPigeonClick(Sender: TObject);
    procedure menuMatchClick(Sender: TObject);
    procedure menuGroupingClick(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure menuClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure imgLogoMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure StatusBar1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FPigeonMain: TFPigeonMain;

implementation

uses
  UCoteDB, UDlgCote, UCoteRecharge, ShellApi, UPlatformDB, UPlatformConfig,
  UPlatformOptions, UCotePigeon, UCoteMatch, UCoteGrouping, UPlatformTachDB,
  UCoteBackSearch, UCoteGroupingSearch, UCoteMultiSearch, UCotePigeonSearch,
  UCoteSoueceSearch, UCoteSoueceStatistic, UCoteStageSearch, UCoteTeamSearch,
  UCoteCollectionSearch;

var JoinServiceStatus : Boolean;
    TaskInfo: string;
  statusMonitor: Boolean;
  FAccessWebCount, FSupportChoose, FSMSCount: Integer;

procedure JoinServiceMonitor();
var
  Conn: TADOConnection;
  function SearchTaskInfo(SQLText: string): string;
  begin
    with TADOQuery.Create(nil) do try
      Connection:= Conn;
      SQL.Text:= SQLText;
      Open;
      if not EOF then result:= '0' else result:= Fields[0].AsString;
    finally
      Free;
    end;
  end;
begin
  FAccessWebCount:= 0;
  Conn:= TADOConnection.Create(nil);
  Conn.ConnectionString:= FCoteDB.ADOConn.ConnectionString;
  Conn.LoginPrompt:= False;
  Conn.Open;
  while statusMonitor do begin
    case FAccessWebCount of
    0: FSupportChoose:= FCoteDB.GetSupportChoose;
    1: FSMSCount:= FCoteDB.GetSMSCount;
    2: JoinServiceStatus:= ServiceIsRunning('JoinKingClientService');
    3: TaskInfo:= '待上传入棚信息' + SearchTaskInfo('SELECT Count(*) FROM JOIN_Pigeon WHERE SynFlag<>''O''') + '羽';
    4: TaskInfo:= '待上传会员信息' + SearchTaskInfo('SELECT Count(*) FROM JOIN_Member WHERE SynFlag<>''O''') + '人';
    5: TaskInfo:= '待上传赛事信息' + SearchTaskInfo('SELECT Count(*) FROM JOIN_Match WHERE SynFlag<>''O''') + '场';
    6: TaskInfo:= '待上传集鸽信息' + SearchTaskInfo('SELECT Count(*) FROM JOIN_GameSource WHERE SourceOrder=0 AND SynFlag<>''O''') + '羽';
    7: TaskInfo:= '待上传归巢信息' + SearchTaskInfo('SELECT Count(*) FROM JOIN_GameSource WHERE SourceOrder>0 AND SynFlag<>''O''') + '羽';
    end;
    Application.ProcessMessages;
    FPigeonMain.StatusBar1.Repaint;
    Inc(FAccessWebCount);
    if FAccessWebCount > 7 then FAccessWebCount:= 0;
    Sleep(2000);
  end;
end;

{$R *.dfm}

procedure TFPigeonMain.FormCreate(Sender: TObject);
begin
  inherited;
  if FileExists(FCoteDB.AppPath + 'Styles\Logo.jpg') then
    imgLogo.Picture.LoadFromFile(FCoteDB.AppPath + 'Styles\Logo.jpg');
  lbTitle.Caption:= FPlatformDB.CertSetting['ApplicationTitle'];
  winMenu.ShowCaptions:= False;
  winMenu.ShowHint:= true;
end;

procedure TFPigeonMain.imgLogoMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if not (ssLeft in Shift) then Exit;
  ReleaseCapture;
  Self.Perform(WM_SYSCOMMAND, $F012, 0);
end;

{$region 'Methods of status bar'}

procedure TFPigeonMain.StatusBar1DblClick(Sender: TObject);
var
  x_left, x_right, i: integer;
  mousePointer: TPoint;
  S: string;
begin
  inherited;
  x_left := 0;
  x_right := 0;
  GetCursorPos(mousePointer);
  for i := 0 to Self.StatusBar1.Panels.Count -1 do
  begin
    x_left := x_right;
    if i<>(Self.StatusBar1.Panels.Count -1) then
      x_right := x_right + StatusBar1.Panels.Items[i].Width
    else
      x_right := StatusBar1.Width;
    if (mousePointer.X >= x_left)and(mousePointer.X <= x_right) then break;
  end;
  case i of
  1,2,3: with TFDlgCote.Create(nil) do try
    InfoToForm(FCoteDB.FCoteInfo);
    while ShowModal = mrOK do try
      FormToInfo(FCoteDB.FCoteInfo);
      FCoteDB.FCoteInfo.UpdateData(FCoteDB.ADOConn);
      Exit;
    except
      on E: Exception do begin
        DlgInfo('错误', E.Message);
      end;
    end;
  finally
    Free;
  end;
  5: if not JoinServiceStatus then begin
      RunWait(FCoteDB.AppPath + 'JoinKingClientService.exe -install', SW_NORMAL);    //RunWait('NET.EXE START ClientService', 0);
      if not StartService('JoinKingClientService')then begin
      //function ShellExecute(hWnd: HWND; Operation, FileName, Parameters, Directory: LPWSTR; ShowCmd: Integer): HINST; stdcall;
        ShellExecute(0, 'open', 'SERVICES.MSC', '', '', SW_NORMAL);
        ShowMessage('请在服务管理中启动“公棚客户端上传服务”。');
      end;
    end;
  6: with TFCoteRecharge.Create(nil) do try
    while ShowModal = mrOK do try
      Exit;
    except
      on E: Exception do begin
        DlgInfo('错误', E.Message);
      end;
    end;
  finally
    Free;
  end;
  end;
end;

procedure TFPigeonMain.StatusBar1DrawPanel(StatusBar: TStatusBar;
  Panel: TStatusPanel; const Rect: TRect);
  procedure WriteNormalText(S: string);
  var
    R: TRect;
  begin
    R:= Rect;
    StatusBar1.Canvas.Brush.Color:= StatusBar1.Color;
    StatusBar1.Canvas.Font.Color:= clBlack;
    StatusBar1.Canvas.Pen.Color:= clBlack;
    StatusBar1.Canvas.TextRect(R, S, [tfLeft]);
  end;
  procedure WriteInformationText(S: string);
  var
    R: TRect;
  begin
    R:= Rect;
    StatusBar1.Canvas.Brush.Color:= clGreen;
    StatusBar1.Canvas.Font.Color:= clWhite;
    StatusBar1.Canvas.Pen.Color:= clWhite;
    StatusBar1.Canvas.TextRect(R, S, [tfLeft]);
  end;
  procedure WriteWarningText(S: string);
  var
    R: TRect;
  begin
    R:= Rect;
    StatusBar1.Canvas.Brush.Color:= clRed;
    StatusBar1.Canvas.Font.Color:= clWhite;
    StatusBar1.Canvas.Pen.Color:= clWhite;
    StatusBar1.Canvas.TextRect(R, S, [tfLeft]);
  end;
begin
  inherited;
  case Panel.Index of
  0: WriteNormalText(FCoteDB.LoginInfo.DisplayName);                   //登录用户显示名
  1: WriteNormalText(FCoteDB.FCoteInfo.OrgName);                                //公棚名称
  2: WriteNormalText(format('%f', [FCoteDB.FCoteInfo.Longitude]));              //公棚经度
  3: WriteNormalText(format('%f', [FCoteDB.FCoteInfo.Latitude]));               //公棚纬度
  4: if JoinServiceStatus then WriteInformationText(TaskInfo)                   //上传任务信息
    else WriteWarningText(TaskInfo);
  5: if JoinServiceStatus then  WriteInformationText('上传服务运行中...')       //服务运行状态
    else WriteWarningText('上传服务已停止！！！');
  6: WriteInformationText(Format('系统允许网上指定的场次还有%d，允许发送的短信还有%d条。',
    [FSupportChoose, FSMSCount]));
  end;
end;

{$endregion}
{$region 'Methods of menu'}

(* 系统功能 *)

procedure TFPigeonMain.menuOptionClick(Sender: TObject);
begin
  OpenModule('系统选项', TFPlatformOptions, TRunType.dtMulti, 0, '', '');
end;

procedure TFPigeonMain.menuClick(Sender: TObject);
begin
  inherited;
  OpenModule('赛绩统计', TFCoteSoueceStatistic, TRunType.dtMulti, 0, '', '');
end;

procedure TFPigeonMain.menuConfigClick(Sender: TObject);
begin
  OpenModule('系统配置', TFPlatformConfig, TRunType.dtMulti, 0, '', '');
end;

procedure TFPigeonMain.menuDBTechClick(Sender: TObject);
begin
  OpenModule('数据维护', TFPlatformTachDB, TRunType.dtMulti, 0, '', '');
end;

procedure TFPigeonMain.menuExitClick(Sender: TObject);
begin
  Close();
end;

(* 公棚功能 *)

procedure TFPigeonMain.menuMatchClick(Sender: TObject);
begin
  OpenModule('赛事管理', TFCoteMatch, TRunType.dtMulti, 0, '', '');
end;

procedure TFPigeonMain.menuPigeonClick(Sender: TObject);
begin
  OpenModule('入棚管理', TFCotePigeon, TRunType.dtMulti, 0, '', '');
end;

procedure TFPigeonMain.N10Click(Sender: TObject);
begin
  OpenModule('集鸽查询', TFCoteCollectionSearch, TRunType.dtMulti, 0, '', '');
end;

procedure TFPigeonMain.menuGroupingClick(Sender: TObject);
begin
  OpenModule('辅助工具', TFCoteGrouping, TRunType.dtMulti, 0, '', '');
end;

procedure TFPigeonMain.N4Click(Sender: TObject);
begin
  OpenModule('入棚查询', TFCotePigeonSearch, TRunType.dtMulti, 0, '', '');
end;

procedure TFPigeonMain.N5Click(Sender: TObject);
begin
  OpenModule('团体查询', TFCoteGroupingSearch, TRunType.dtMulti, 0, '', '');
end;

//procedure TFPigeonMain.N6Click(Sender: TObject);
//begin
//  OpenModule('关赛查询', TFCoteStageSearch, TRunType.dtMulti, 0, '', '');
//end;

procedure TFPigeonMain.N7Click(Sender: TObject);
begin
  OpenModule('成绩查询', TFCoteSoueceSearch, TRunType.dtMulti, 0, '', '');
end;

procedure TFPigeonMain.N8Click(Sender: TObject);
begin
  OpenModule('赛绩查询', TFCoteBackSearch, TRunType.dtMulti, 0, '', '');
end;

procedure TFPigeonMain.N9Click(Sender: TObject);
begin
  OpenModule('联赛成绩', TFCoteMultiSearch, TRunType.dtMulti, 0, '', '');
end;

{$endregion}

end.
