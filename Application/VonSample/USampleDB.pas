unit USampleDB;
{$define debug}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, IniFiles, ZLib, ImgList, IdIOHandler, IdComponent,
  IdIOHandlerSocket, IdIOHandlerStack, IdBaseComponent, IdTCPConnection,
  IdTCPClient, IdHTTP, IdIOHandlerStream, HTTPApp,
  UPlatformDB, UPlatformInfo, UVonLog, System.ImageList, UVonCrypt;

type
  /// <summary>证书内容信息</summary>
  TCertData = class(TCryptItem)
  private
    FSubjectValue: string;
    FSubjectData: TBytes;
  public
    procedure ReadFromStream(Stream: TStream); override;
    procedure WriteToStream(Stream: TStream); override;
    procedure ResizeData(size: Integer);
  published
    property SubjectData: TBytes read FSubjectData write FSubjectData;
    property SubjectValue: string read FSubjectValue write FSubjectValue;
  end;

  TFSampleDB = class(TFPlatformDB)
    DQCheckRight: TADOQuery;
    IdHTTP1: TIdHTTP;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FCert: TCryptCollection<TCertData>;
    procedure EventOnUpgrade(Sender: TObject);
    function GetCertValue(name: string): string;
    procedure SetCertValue(name: string; const Value: string);
    function GetCertData(name: string): TBytes;
    procedure SetCertData(name: string; const Value: TBytes);
  public
    { Public declarations }
    CanRun: Boolean;
    procedure InitData();
    function CheckNetwork: boolean;
    property CertValue[name: string]: string read GetCertValue write SetCertValue;
    property CertData[name: string]: TBytes read GetCertData write SetCertData;
  published
  end;

var
  FSampleDB: TFSampleDB;

implementation

uses StrUtils, Math, XMLIntf, XMLDoc, UVonSystemFuns, UPlatformLogin, UPlatformUpgrade;

{$R *.dfm}

procedure TFSampleDB.DataModuleCreate(Sender: TObject);
var
  S: string;
  b: Boolean;
begin
  inherited;
  FCert.LoadFromFile(AppPath + CERT_FILE, CERT_KEY, CERT_CIPHER, CERT_HASH);
  {$region '连接数据库'}
  ADOConn.Close;
  S := CertValue['DBConnectString'];
  if S <> '' then ADOConn.ConnectionString:= S;
  ADOConn.Open();
  if not ADOConn.Connected then raise Exception.Create('Failed to connect database.');
  {$endregion}
  {$region '自动升级'}
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'Will update ......'); {$endif}
  if CertValue['AutoUpgrade'] = 'True' then
    with TFPlatformUpgrade.Create(nil) do
      try
        OnUpgrade:= EventOnUpgrade;
        CanRun := ShowModal = mrYes;
        if not CanRun then Application.Terminate;
      finally
        Free;
      end;
  {$endregion}
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'Will LoadRuntime ......'); {$endif}
  LoadRuntime;
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'AutoLogin=' + CertValue['AutoLogin']); {$endif}
  {$region 'Login'}
  S:= CertValue['AutoLogin'];
  if S = '' then begin
    CanRun := true;
    LoginInfo.LoginName:= 'Guest';
    LoginInfo.DisplayName:= '匿名用户';
  end else if TryStrToBool(S, b) then begin
    if (not b) then CanRun := Login
    else begin
      CanRun := true;
      LoginInfo.LoginName:= 'Guest';
      LoginInfo.DisplayName:= '匿名用户';
    end
  end else CanRun := LoginInfo.Login(S, S, ADOConn);
  {$endregion}
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'Will LoadUsings ......'); {$endif}
  LoadUsings;
  FOnSay := nil;
end;

procedure TFSampleDB.DataModuleDestroy(Sender: TObject);
begin
  CertValue['LastAccessDate']:= DateTimeToStr(Now);
  FCert.SaveToFile(AppPath + CERT_FILE, CERT_KEY, CERT_CIPHER, CERT_HASH);
  FCert.Free;
  inherited;
end;

procedure TFSampleDB.EventOnUpgrade(Sender: TObject);
var
  szUpgrade: TArray<string>;
begin
  szUpgrade:= CertValue['UpgradeUrl'].Split([',']);
  (Sender as TFPlatFormUpgrade).UpgradeProcess(szUpgrade[0], szUpgrade[1], StrToInt(szUpgrade[2]));
end;

{$region 'property of certification'}

function TFSampleDB.GetCertData(name: string): TBytes;
var
  item: TCertData;
begin
  item:= FCert.Items[name];
  if Assigned(item) then Result:= item.SubjectData else Result:= nil;
end;

function TFSampleDB.GetCertValue(name: string): string;
var
  item: TCertData;
begin
  item:= FCert.Items[name];
  if Assigned(item) then Result:= item.SubjectValue else Result:= '';
end;

procedure TFSampleDB.SetCertData(name: string; const Value: TBytes);
var
  item: TCertData;
begin
  item:= FCert.Items[name];
  if Assigned(item) then
    item.SubjectData:= Value
  else begin
    item:= TCertData.Create;
    item.Name:= name;
    item.SubjectData:= Value;
    FCert.Add(item);
  end;
end;

procedure TFSampleDB.SetCertValue(name: string; const Value: string);
var
  item: TCertData;
begin
  item:= FCert.Items[name];
  if Assigned(item) then item.SubjectValue:= Value
  else begin
    item:= TCertData.Create;
    item.Name:= name;
    item.SubjectValue:= Value;
    FCert.Add(item);
  end;
end;

{$endregion}

procedure TFSampleDB.InitData();
begin

end;

function TFSampleDB.CheckNetwork: boolean;
begin
  Result:= Ping('8.131.244.41');
end;

{ TCertData }

procedure TCertData.ReadFromStream(Stream: TStream);
var
  FSubjectValueSize: Int64;
begin
  inherited;
  Name:= ReadStringFromStream(Stream);
  FSubjectValue:= ReadStringFromStream(Stream);
  FSubjectValueSize:= ReadInt64FromStream(Stream);
  SetLength(FSubjectData, FSubjectValueSize);
  Stream.Read(FSubjectData, FSubjectValueSize);
end;

procedure TCertData.ResizeData(size: Integer);
begin
  SetLength(FSubjectData, size);
end;

procedure TCertData.WriteToStream(Stream: TStream);
begin
  inherited;
  WriteStringToStream(Name, Stream);
  WriteStringToStream(FSubjectValue, Stream);
  WriteInt64ToStream(Length(FSubjectData), Stream);
  Stream.Write(FSubjectData, Length(FSubjectData));
end;

end.
