unit USampleMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UPlatformMain, Menus, ImgList, ComCtrls, ToolWin, ExtCtrls, Tabs,
  DockTabSet, StdCtrls, Data.DB, Data.Win.ADODB, IniFiles, System.ImageList;

type
  TFSampleMain = class(TFPlatformMain)
    procedure StatusBar1DrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel;
      const Rect: TRect);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSampleMain: TFSampleMain;

implementation

uses USampleDB, UVonSystemFuns, ShellApi;

{$R *.dfm}

procedure TFSampleMain.StatusBar1DrawPanel(StatusBar: TStatusBar;
  Panel: TStatusPanel; const Rect: TRect);
  procedure WriteNormalText(S: string);
  var
    R: TRect;
  begin
    R:= Rect;
    StatusBar1.Canvas.Brush.Color:= StatusBar1.Color;
    StatusBar1.Canvas.Font.Color:= clBlack;
    StatusBar1.Canvas.Pen.Color:= clBlack;
    StatusBar1.Canvas.TextRect(R, S, [tfLeft]);
  end;
  procedure WriteInformationText(S: string);
  var
    R: TRect;
  begin
    R:= Rect;
    StatusBar1.Canvas.Brush.Color:= clGreen;
    StatusBar1.Canvas.Font.Color:= clWhite;
    StatusBar1.Canvas.Pen.Color:= clWhite;
    StatusBar1.Canvas.TextRect(R, S, [tfLeft]);
  end;
  procedure WriteWarningText(S: string);
  var
    R: TRect;
  begin
    R:= Rect;
    StatusBar1.Canvas.Brush.Color:= clRed;
    StatusBar1.Canvas.Font.Color:= clWhite;
    StatusBar1.Canvas.Pen.Color:= clWhite;
    StatusBar1.Canvas.TextRect(R, S, [tfLeft]);
  end;
begin
  inherited;
  case Panel.Index of
  0: WriteNormalText(FSampleDB.LoginInfo.DisplayName);               //��¼�û���ʾ��
  end;
end;

end.
