program AppMain;

uses
  Forms,
  Windows,
  SysUtils,
  Dialogs,
  UVonConfig in '..\SourceCode\Platform\UVonConfig.pas',
  UVonSystemFuns in '..\SourceCode\Platform\UVonSystemFuns.pas',
  UVonCertReg in '..\SourceCode\Certification\UVonCertReg.pas' {FPlatformReg},
  DCPblockciphers in '..\SourceCode\dcpcrypt2-2009\DCPblockciphers.pas',
  DCPreg in '..\SourceCode\dcpcrypt2-2009\DCPreg.pas',
  DCPtiger in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPtiger.pas',
  DCPhaval in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPhaval.pas',
  DCPmd4 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPmd4.pas',
  DCPmd5 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPmd5.pas',
  DCPripemd128 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPripemd128.pas',
  DCPripemd160 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPripemd160.pas',
  DCPsha256 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPsha256.pas',
  DCPsha512 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPsha512.pas',
  DCPtwofish in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPtwofish.pas',
  DCPblowfish in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPblowfish.pas',
  DCPcast128 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPcast128.pas',
  DCPcast256 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPcast256.pas',
  DCPdes in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPdes.pas',
  DCPgost in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPgost.pas',
  DCPice in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPice.pas',
  DCPidea in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPidea.pas',
  DCPmars in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPmars.pas',
  DCPmisty1 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPmisty1.pas',
  DCPrc2 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrc2.pas',
  DCPrc4 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrc4.pas',
  DCPrc5 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrc5.pas',
  DCPrc6 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrc6.pas',
  DCPrijndael in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrijndael.pas',
  DCPserpent in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPserpent.pas',
  DCPtea in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPtea.pas',
  UPlatformDB in '..\SourceCode\Test\UPlatformDB.pas' {FPlatformDB: TDataModule},
  UPlatformMain in '..\SourceCode\Platform\UPlatformMain.pas' {FPlatformMain},
  UPlatformDockForm in '..\SourceCode\Platform\UPlatformDockForm.pas' {FPlatformDockForm},
  UPlatformMenu1 in '..\SourceCode\Platform\UPlatformMenu1.pas' {FPlatformMenu1},
  UVonMenuInfo in '..\SourceCode\Certification\UVonMenuInfo.pas',
  UPlatformMenuBase in '..\SourceCode\Platform\UPlatformMenuBase.pas' {FPlatformMenuBase},
  UPlatformHelp in '..\SourceCode\Platform\UPlatformHelp.pas' {FPlatformHelp},
  UPlatformRole in '..\SourceCode\Platform\UPlatformRole.pas' {FPlatformRole},
  UFrameTree in '..\SourceCode\Components\UFrameTree.pas' {FrameTree: TFrame},
  UPlatformInfo in '..\SourceCode\Platform\UPlatformInfo.pas',
  UPlatformUser in '..\SourceCode\Platform\UPlatformUser.pas' {FPlatformUser},
  UFrameGridBar in '..\SourceCode\Components\UFrameGridBar.pas' {FrameGridBar: TFrame},
  UDlgGridColumn in '..\SourceCode\Components\UDlgGridColumn.pas' {FDlgGridColumn},
  UVonCertReader in '..\SourceCode\Certification\UVonCertReader.pas',
  UPlatformLogin in '..\SourceCode\Platform\UPlatformLogin.pas' {FPlatformLogin},
  UPlatformUpgrade in '..\SourceCode\Platform\UPlatformUpgrade.pas' {FPlatFormUpgrade},
  UCommunicationHttp in '..\SourceCode\Communication\UCommunicationHttp.pas',
  UCommunicationModule in '..\SourceCode\Communication\UCommunicationModule.pas',
  UCommunicationRes in '..\SourceCode\Communication\UCommunicationRes.pas',
  UCommunicationTCP in '..\SourceCode\Communication\UCommunicationTCP.pas',
  UCommunicationUDP in '..\SourceCode\Communication\UCommunicationUDP.pas',
  UFrameSettingInputor in '..\SourceCode\Components\UFrameSettingInputor.pas' {FrameSettingInputor: TFrame},
  UFrameDataText in '..\SourceCode\Components\UFrameDataText.pas' {FrameDataText: TFrame},
  UFrameDataBase in '..\SourceCode\Components\UFrameDataBase.pas' {FrameDataBase: TFrame},
  UFrameDataDBConn in '..\SourceCode\Components\UFrameDataDBConn.pas' {FrameDataDBConn: TFrame},
  UDlgCamera in '..\SourceCode\Components\UDlgCamera.pas' {FDlgCamera},
  UFrameImageInput in '..\SourceCode\Components\UFrameImageInput.pas' {FrameImageInput: TFrame},
  UFrameRichEditor in '..\SourceCode\Components\UFrameRichEditor.pas' {FrameRichEditor: TFrame},
  UVonMenuDesigner in '..\SourceCode\Certification\UVonMenuDesigner.pas' {FVonMenuDesigner},
  UFrameLonLat in '..\SourceCode\Components\UFrameLonLat.pas' {FrameFrameLonLat: TFrame},
  UPlatformOptions in '..\SourceCode\Platform\UPlatformOptions.pas' {FPlatformOptions},
  UCommunicationSerialPort in 'E:\MyPrograms\Communication\UCommunicationSerialPort.pas',
  UPlatformConfig in '..\SourceCode\Platform\UPlatformConfig.pas' {FPlatformConfig},
  UPlatformTachDB in '..\SourceCode\Platform\UPlatformTachDB.pas' {FPlatformTachDB},
  UVonCrypt in '..\SourceCode\dcpcrypt2-2009\UVonCrypt.pas',
  UVonSinoVoiceAPI in '..\SourceCode\Imports\UVonSinoVoiceAPI.pas',
  JTTS_ACTIVEXLib_TLB in '..\SourceCode\Imports\JTTS_ACTIVEXLib_TLB.pas',
  UVonQRCode in '..\SourceCode\Imports\UVonQRCode.pas',
  DCPbase64 in '..\SourceCode\dcpcrypt2-2009\DCPbase64.pas',
  DCPconst in '..\SourceCode\dcpcrypt2-2009\DCPconst.pas',
  DCPcrypt2 in '..\SourceCode\dcpcrypt2-2009\DCPcrypt2.pas',
  DCPsha1 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPsha1.pas',
  UVonGraphicAPI in '..\SourceCode\Imports\UVonGraphicAPI.pas',
  UVonGraphicColor in '..\SourceCode\Imports\UVonGraphicColor.pas',
  UVonGraphicCompression in '..\SourceCode\Imports\UVonGraphicCompression.pas',
  UVonGraphicEx in '..\SourceCode\Imports\UVonGraphicEx.pas',
  UVonGraphicJPG in '..\SourceCode\Imports\UVonGraphicJPG.pas',
  UVonGraphicMZLib in '..\SourceCode\Imports\UVonGraphicMZLib.pas',
  UVonGraphicStrings in '..\SourceCode\Imports\UVonGraphicStrings.pas',
  UVonFileLog in '..\SourceCode\Platform\UVonFileLog.pas',
  UVonLog in '..\SourceCode\Platform\UVonLog.pas',
  UVonTransmissionApp in '..\SourceCode\Communication\UVonTransmissionApp.pas',
  UVonTransmissionBase in '..\SourceCode\Communication\UVonTransmissionBase.pas',
  UDlgBrowser in '..\SourceCode\Components\UDlgBrowser.pas' {FDlgBrower},
  UVFW in '..\SourceCode\Components\UVFW.pas',
  OLE_Excel_TLB in '..\SourceCode\Imports\OLE_Excel_TLB.pas',
  OLE_Office_TLB in '..\SourceCode\Imports\OLE_Office_TLB.pas',
  OLE_VBIDE_TLB in '..\SourceCode\Imports\OLE_VBIDE_TLB.pas',
  UVonXorCrypt in '..\SourceCode\dcpcrypt2-2009\UVonXorCrypt.pas',
  UZXIngQRCode in '..\SourceCode\Imports\UZXIngQRCode.pas',
  UDlgConnection in '..\SourceCode\Components\UDlgConnection.pas' {FDlgConnection},
  UMMSDB in '..\SourceCode\MMS\UMMSDB.pas' {FMMSDB: TDataModule},
  UMMSMain in '..\SourceCode\MMS\UMMSMain.pas' {FMSSMain},
  UOfficeExport in '..\SourceCode\Imports\UOfficeExport.pas',
  OLE_Word_TLB in '..\SourceCode\Imports\OLE_Word_TLB.pas',
  UMMSInfo in '..\SourceCode\MMS\UMMSInfo.pas',
  UMMSServer in '..\SourceCode\MMS\UMMSServer.pas';

var
  hMutex: THandle;
  Ret: Integer;

{$R *.res}

begin
  CERT_FLAG:= #3#5'JAMESVON Maintenance Management System CERTIFICATION '#5#3;
  CERT_FILE:= 'MMS.CERT';
  SYS_NAME:= '维修服务管理系统';
  hMutex := CreateMutex(nil, true, 'APIDOCGT');
  Ret := GetLastError;
  OpenLog('FILELOG', SYS_NAME, ExtractFilePath(Application.ExeName) + 'LOG\', LOG_DEBUG); // 打开本地日志
  if Ret <> ERROR_ALREADY_EXISTS then
  begin
    Application.Initialize;
  end
  else
  begin
    Application.MessageBox('程序已经运行!', '系统提示', MB_OK);
    ReleaseMutex(hMutex);
    Exit;
  end;
  CheckPrevInstance('TVonUpgrade');               //检查本身程序
  CheckPrevInstance('TFMSSMain');                 //检查应用程序
  RegAppCommand('帮助命令', '显示系统帮助信息', TFPlatformHelp, 0, '');
  RegAppCommand('角色管理', '管理系统角色信息', TFPlatformRole, 0, '');
  RegAppCommand('人员管理', '管理系统用户', TFPlatformUser, 0, '');
  RegAppCommand('界面设计', '管理系统菜单、按钮、风格样式', TFVonMenuDesigner, 0, '');
  RegAppCommand('系统选项', '管理系统各种参数、选项以及支持信息', TFPlatformOptions, 0, '');
  RegAppCommand('系统配置', '管理系统各种配置项', TFPlatformConfig, 0, '');
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFMMSDB, FMMSDB);
  if FPlatformDB.CanRun then begin
    Application.MainFormOnTaskbar := True;
    Application.Title := '家电维修管理系统';
    Application.CreateForm(TFMSSMain, FMSSMain);
    Say('欢迎您使用 科迈兄弟的' + FPlatformDB.Cert.Values.NameValue['ApplicationName']);
  end else if GetInfoList.Count <> 0 then
    ShowMessage(GetInfoList.Text);
  Application.Run;
end.
