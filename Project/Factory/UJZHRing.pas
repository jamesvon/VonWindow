(* ******************************************************************************
  * UJZHRing v1.0 written by James Von (jamesvon@163.com) ******************
  ********************************************************************************
  * 扫环程序
  *------------------------------------------------------------------------------*
  *     扫环程序是为荣冠鸽星公司定制的一个电子环初始化和发放记录的一个小软件，在
  * UCoteManager 的基础上，将制定的环包发放给用户使用。
  *------------------------------------------------------------------------------*
  * 扫环逻辑
  *------------------------------------------------------------------------------*
  *     读环-写环（jzCode=序号，GameCode=0）-读环，正确->结束
  *============================================================================= *)
unit UJZHRing;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, ComCtrls, ExtCtrls, Buttons, UCoteComm, UCoteInfo,
  UVonLog, UCoteDB, DB, ADODB, UVonSystemFuns, CheckLst, UFrameGridBar;

type
  TFJZ_Ring = class(TForm)
    DQCode: TADOQuery;
    DQSearchPackage: TADOQuery;
    DBOrganization: TADOTable;
    SaveDialog1: TSaveDialog;
    DQRingByPackage: TADOQuery;
    DQFind: TADOQuery;
    DQMax: TADOQuery;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    plInputer: TPanel;
    Panel1: TPanel;
    ProgressBar1: TProgressBar;
    lbSearchResult: TLabel;
    Panel2: TPanel;
    plPublish: TPanel;
    btnPublish: TBitBtn;
    btnPublish2: TButton;
    lbSelectCount: TLabel;
    EOrg: TComboBox;
    mScanList: TMemo;
    Panel3: TPanel;
    Label5: TLabel;
    Label3: TLabel;
    ESearchPackageNo: TSpinEdit;
    EOrg1: TComboBox;
    ESearchPublished: TComboBox;
    EPublishedate1: TDateTimePicker;
    EPublishedate2: TDateTimePicker;
    btnSearch: TBitBtn;
    btnStatistic: TBitBtn;
    Label12: TLabel;
    EComm: TComboBox;
    Label13: TLabel;
    ECommRate: TComboBox;
    Panel4: TPanel;
    Label6: TLabel;
    lbCode5: TLabel;
    lbCode4: TLabel;
    lbCode3: TLabel;
    lbCode2: TLabel;
    lbCode1: TLabel;
    EWriteTime: TSpinEdit;
    btnStart: TBitBtn;
    btnFind: TBitBtn;
    btnStop: TBitBtn;
    Label4: TLabel;
    ECode: TEdit;
    btnSpecial: TBitBtn;
    btnManualFind: TBitBtn;
    chkUseCatch: TCheckBox;
    ECurrentCode: TSpinEdit;
    Panel11: TPanel;
    btnpackageAdd: TButton;
    btnContinueInput: TBitBtn;
    rgCommKind: TRadioGroup;
    btnPackageEdit: TButton;
    lstPackage: TListView;
    mList: TMemo;
    Splitter1: TSplitter;
    ERingColor: TColorListBox;
    lbRingColor: TLabel;
    ChkRingColor: TCheckBox;
    Label1: TLabel;
    ERingCount: TSpinEdit;
    Panel5: TPanel;
    lbPackage: TLabel;
    lbCount: TLabel;
    Image1: TImage;
    DQFactoryCode: TADOQuery;
    lbFactory: TLabel;
    EFactory: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnStartClick(Sender: TObject);
    procedure btnContinueInputClick(Sender: TObject);
    procedure btnPublishClick(Sender: TObject);
    procedure btnFindClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure btnSearchClick(Sender: TObject);
    procedure btnSpecialClick(Sender: TObject);
    procedure btnManualFindClick(Sender: TObject);
    procedure btnStatisticClick(Sender: TObject);
    procedure ESearchPublishedChange(Sender: TObject);
    procedure btnPublish2Click(Sender: TObject);
    procedure rgCommKindClick(Sender: TObject);
    procedure btnpackageAddClick(Sender: TObject);
    procedure btnPackageEditClick(Sender: TObject);
    procedure lstPackageSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure PageControl1Change(Sender: TObject);
    procedure ERingColorClick(Sender: TObject);
    procedure ERingCountChange(Sender: TObject);
  private
    { Private declarations }
    FComm: TJZCommBase;
    FRingInfo: TRingInfo;
    FPackageInfo: TPackageInfo;
    //FCount, FSelectCount, FSumCount,
    FCurrentItem: TListItem;
    FCurrentFactoryCode, FCurrentJZCode: Int64;
    FIsSaved: Boolean;
    EventOfCollection: TEventAfterCollectionReceived;
    procedure SaveRing(ID: Integer; JZCode, FactoryCode: Int64);
    procedure EventOfReceived_R(JZCode: Int64; GameCode: Cardinal;
      FactoryCode: Int64);
    procedure EventOfReceived_W(JZCode: Int64; GameCode: Cardinal;
      FactoryCode: Int64);
    procedure EventOfWrite(Successed: Boolean);
    procedure EventToFind(JZCode: Int64; GameCode: Cardinal; FactoryCode: Int64);
    function GetMaxId: Int64;
    function DisplayPackage(Q: TCustomADODataSet): TListItem;
    procedure DisplayNewRing(Ring: string);
    procedure DisplayProcess(Value: Integer);
  public
    { Public declarations }
  end;

var
  FJZ_Ring: TFJZ_Ring;

implementation

uses DateUtils, UDlgPackage;

{$R *.dfm}

procedure TFJZ_Ring.FormCreate(Sender: TObject);
begin
  FCurrentJZCode := MaxInt;
  ReadAppItems('COMMUNICATION', 'PortList', 'COM1,COM2,COM3,COM4', EComm.Items);
  EComm.ItemIndex := EComm.Items.IndexOf(ReadAppConfig('COMMUNICATION', 'Port'));
  ECommRate.ItemIndex := ECommRate.Items.IndexOf(ReadAppConfig('COMMUNICATION',
    'Speed'));
  FRingInfo := TRingInfo.Create;
//  FSumCount := 0;
//  FSelectCount := 0;
  FPackageInfo := TPackageInfo.Create;
  FPackageInfo.ID := 0;
  lbCode1.Caption := '';
  lbCode2.Caption := '';
  lbCode3.Caption := '';
  lbCode4.Caption := '';
  ECode.Text := '';  ERingColor.Items.Clear;
  lbRingColor.Caption := FCoteDB.GetListOption('环颜色', ERingColor.Items, 4);
  lbFactory.Caption := FCoteDB.GetDropList('产地', EFactory, '');
  EFactory.ItemIndex:= 0;
  ERingColor.Enabled:= True;
  ERingColor.ItemIndex:= 0;
  plPublish.Visible := FCoteDB.TaskParams = '发放';
  btnPublish.Visible := EOrg.Visible;
  EOrg1.Items.AddObject('所有', nil);
  EOrg1.Items.AddObject('未发放', nil);
  EOrg1.Items.AddObject('已发放', nil);
  with DBOrganization do
  begin
    Open;
    while not EOF do
    begin
      EOrg.Items.AddObject(FieldByName('OrgName').AsString,
        TObject(FieldByName('ID').AsInteger));
      EOrg1.Items.AddObject(FieldByName('OrgName').AsString,
        TObject(FieldByName('ID').AsInteger));
      Next;
    end;
  end;
  EOrg.ItemIndex := 0;
  EOrg1.ItemIndex := 0;
  DQMax.Open;
  if not DQMax.EOF then
    ECurrentCode.Value := DQMax.Fields[0].AsLargeInt
  else
    ECurrentCode.Value := 1;
  if ECurrentCode.Value and $FF = 0 then
    ECurrentCode.Value:= ECurrentCode.Value + 1;
  DQMax.Close;
  EPublishedate1.Date:= StartOfTheYear(Now);
  EPublishedate2.Date:= DateOf(Now) + 1;
  rgCommKind.ItemIndex:= rgCommKind.Items.IndexOf(FCoteDB.RingKind);
  rgCommKindClick(nil);
end;

procedure TFJZ_Ring.FormDestroy(Sender: TObject);
begin
  if Assigned(FComm) then
    FComm.Terminate;
  FPackageInfo.Free;
  FRingInfo.Free;
end;

(* 查询统计 *)

procedure TFJZ_Ring.btnStatisticClick(Sender: TObject);
begin
  mList.Visible := True;
  plInputer.Visible := False;
  mList.Clear;
  with DQSearchPackage do try
    Close;
    SQL.Text := 'SELECT OrgName,RingColor,Count(*),SUM(RingCount)FROM ' +
      'JOIN_Package P JOIN SYS_Organization O ON O.ID=P.OrgIdx WHERE OrgIdx>0 ' +
      'GROUP BY OrgName,RingColor';
    Open;
    mList.Lines.Add('---- 发放情况 ----------------------');
    while not EOF do
    begin
      mList.Lines.Add(Format('%s 已经发放了 %d 袋 %s 颜色 共计 %d 支环',
        [Fields[0].AsString, Fields[2].AsInteger, Fields[1].AsString,
        Fields[3].AsInteger]));
      Next;
    end;
    Close;
    SQL.Text := 'SELECT RingColor,Count(*), SUM(RingCount)FROM JOIN_Package P WHERE OrgIdx=0 GROUP BY RingColor';
    Open;
    mList.Lines.Add('---- 库存情况 ----------------------');
    while not EOF do
    begin
      mList.Lines.Add(Format('当前尚库存 %d 袋 %s 颜色 共计 %d 支环',
        [Fields[1].AsInteger, Fields[0].AsString, Fields[2].AsInteger]));
      Next;
    end;
    mList.Lines.Add('---- 统计结束 ----------------------');
  finally
    //Close;
  end;
  PageControl1.ActivePageIndex := 2;
end;

procedure TFJZ_Ring.btnSearchClick(Sender: TObject);
var
  pkgCount, pkgPublished, ringCount, ringPublished: Integer;
begin
  with DQSearchPackage do
  begin
    Close;
    SQL.Text := 'SELECT P.[ID],P.[RingCount],P.[RingKind],P.[RingColor],' +
      'P.[CreateDate],P.[OrgIDX],P.[Modifiedate],O.OrgName ' +
      'FROM JOIN_Package P LEFT JOIN SYS_Organization O ON O.ID=P.OrgIDX';
    SQL.Add('WHERE [RingKind]=''' + rgCommKind.Items[rgCommKind.ItemIndex] + '''');
    case EOrg1.ItemIndex of
    0: begin end;                       //所有
    1: SQL.Add('AND OrgIdx=0');         //未发放
    2: SQL.Add('AND OrgIdx>0');         //已发放
    else SQL.Add('AND OrgIdx=' + IntToStr(Integer(EOrg1.Items.Objects[EOrg1.ItemIndex])));
    end;
    if EPublishedate1.Checked then
      SQL.Add('AND Modifiedate>=''' + DateToStr(EPublishedate1.Date) + '''');
    if EPublishedate2.Checked then
      SQL.Add('AND Modifiedate<=''' + DateToStr(EPublishedate2.Date) + '''');
    if ChkRingColor.Checked then
      SQL.Add('AND RingColor=''' + ERingColor.Items[ERingColor.ItemIndex] + '''');
    if ESearchPackageNo.Value > 0 then
      SQL.Add('AND ID=' + IntToStr(ESearchPackageNo.Value));
    SQL.Add('ORDER BY P.[ID]');
    Open;
    lstPackage.Clear;
    pkgCount:= 0; pkgPublished:= 0; ringCount:= 0; ringPublished:= 0;
    while not EOF do
    begin
      DisplayPackage(DQSearchPackage);
      if DQSearchPackage.FieldByName('OrgIdx').AsInteger = 0 then begin
        ringCount:= ringCount + DQSearchPackage.FieldByName('RingCount').AsInteger;
        Inc(pkgCount);
      end else begin
        ringPublished:= ringPublished + DQSearchPackage.FieldByName('RingCount').AsInteger;
        Inc(pkgPublished);
      end;
      Next;
    end;
  end;
  mList.Clear;
  mList.Lines.Add('查询结果：');
  mList.Lines.Add(Format('未发行：%d袋，%d环', [pkgCount, ringCount]));
  mList.Lines.Add(Format('已发放：%d袋，%d环', [pkgPublished, ringPublished]));
  mList.Lines.Add(Format('合计：%d袋，%d环', [pkgPublished + pkgCount, ringPublished + ringCount]));
  lbSearchResult.Caption := Format('系统可发放%d袋，%d个环', [pkgPublished, ringPublished]);
  PageControl1.ActivePageIndex := 0;
end;

procedure TFJZ_Ring.rgCommKindClick(Sender: TObject);
begin
  if Assigned(FComm) then begin
    FComm.Terminate;
    FComm:= nil;
  end;
  case rgCommKind.ItemIndex of
  0: FComm:= TJZCommR.Create;
  1: FComm:= TJZCommRW.Create;
  2: FComm:= TJZCommR.Create;
  3: FComm:= TJZCommRW.Create;
  4: FComm:= TJZCommKaiXin.Create;
  end;
  FComm.Interval:= StrToInt(ReadAppConfig('COMMUNICATION', 'IntValue'));
  FComm.TimeOut:= StrToInt(ReadAppConfig('COMMUNICATION', 'TimeOut'));
  FComm.CheckRing:= False;
  btnSearchClick(nil);
end;

procedure TFJZ_Ring.ERingColorClick(Sender: TObject);
begin
  if ChkRingColor.Checked then btnSearchClick(nil);
end;

procedure TFJZ_Ring.ERingCountChange(Sender: TObject);
begin
  ProgressBar1.Max := ERingCount.Value;
end;

function TFJZ_Ring.DisplayPackage(Q: TCustomADODataSet): TListItem;
begin
  Result:= lstPackage.Items.Add;
  with Result do begin
    Caption:= Q.FieldByName('ID').AsString;
    Data:= Pointer(Q.FieldByName('OrgIdx').AsInteger);
    SubItems.Add(Q.FieldByName('RingCount').AsString);
    SubItems.Add(Q.FieldByName('RingKind').AsString);
    SubItems.Add(Q.FieldByName('RingColor').AsString);
    SubItems.Add(Q.FieldByName('OrgName').AsString);
    SubItems.Add(Q.FieldByName('Modifiedate').AsString);
  end;
end;

(* Packages *)

procedure TFJZ_Ring.btnpackageAddClick(Sender: TObject);
begin
  FPackageInfo.ID:= 0;
  FPackageInfo.RingKind:= rgCommKind.Items[rgCommKind.ItemIndex];
  FPackageInfo.RingCount:= 0;
  FPackageInfo.RingColor:= ERingColor.Items[ERingColor.ItemIndex];
  FPackageInfo.OrgIDX:= 0;
  FPackageInfo.SaveToDB(DQSearchPackage);
  DisplayPackage(DQSearchPackage);
end;

procedure TFJZ_Ring.btnPackageEditClick(Sender: TObject);
begin
  if not Assigned(lstPackage.Selected) then
    raise Exception.Create('尚未选择包装项目。');
  if not DQSearchPackage.Locate('ID', StrToInt(lstPackage.Selected.Caption), []) then
    raise Exception.Create('无法定位包装项目。');
  FPackageInfo.LoadFromDB(DQSearchPackage);
  if FPackageInfo.OrgIDX > 0 then
    raise Exception.Create('该包装已经发布不予许修改。');
  FPackageInfo.RingColor:= ERingColor.Items[ERingColor.ItemIndex];
  DQSearchPackage.Edit;
  DQSearchPackage.FieldByName('RingColor').AsString:= FPackageInfo.RingColor;
  DQSearchPackage.Post;
  lstPackage.Selected.SubItems[2]:= FPackageInfo.RingColor;
end;

procedure TFJZ_Ring.btnContinueInputClick(Sender: TObject);
begin
  if not Assigned(lstPackage.Selected) then
    raise Exception.Create('尚未选择包装袋无法进行扫环');
  if not DQSearchPackage.Locate('ID', StrToInt(lstPackage.Selected.Caption), []) then
    raise Exception.Create('无法定位包装项目。');
  FCurrentItem:= lstPackage.Selected;
  FPackageInfo.LoadFromDB(DQSearchPackage);
  lbCount.Caption := IntToStr(FPackageInfo.RingCount);
  lbPackage.Caption := IntToStr(FPackageInfo.ID);
  lbCount.Font.Color:= TColor(ERingColor.Items.Objects[ERingColor.Items.IndexOf(FPackageInfo.RingColor)]);
  PageControl1.ActivePageIndex:= 1;
  ProgressBar1.Max := ERingCount.Value;
  btnStartClick(nil);
end;

(* Events of button *)

procedure TFJZ_Ring.PageControl1Change(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
  1: btnContinueInputClick(nil);
  end;
end;

procedure TFJZ_Ring.btnStartClick(Sender: TObject);
begin
  case rgCommKind.ItemIndex of
  0, 2: begin
      FComm.OnCollected:= EventOfReceived_R;
      FComm.OnWrited:= nil;
    end;
  1, 3: begin
      FComm.OnCollected:= EventOfReceived_W;
      FComm.OnWrited:= EventOfWrite;
    end;
  end;
  FRingInfo.RingKind := rgCommKind.Items[rgCommKind.ItemIndex]; 
  DQMax.Parameters[0].Value:= FRingInfo.RingKind;
  FComm.Open(EComm.ItemIndex + 1, StrToInt(ECommRate.Text), 'n', 8, 1);
  WriteAppConfig('COMMUNICATION', 'Port', EComm.Text);
  WriteAppConfig('COMMUNICATION', 'Speed', ECommRate.Text);
  btnStart.Enabled := False;
  btnFind.Enabled := False;
  FCurrentJZCode := MaxInt;
  btnStop.Enabled := True;
  PageControl1.ActivePageIndex := 1;
  with DQCode do
  begin // 存储数据库
    Close;
    Parameters[0].Value := 0;
    Open;
  end;
end;

procedure TFJZ_Ring.btnFindClick(Sender: TObject);
begin
  FComm.OnCollected:= EventToFind;
  FComm.OnWrited:= nil;
  FComm.Open(EComm.ItemIndex + 1, StrToInt(ECommRate.Text), 'n', 8, 1);
  WriteAppConfig('COMMUNICATION', 'Port', EComm.Text);
  WriteAppConfig('COMMUNICATION', 'Speed', ECommRate.Text);
  btnStart.Enabled := False;
  btnFind.Enabled := False;
  FCurrentJZCode := MaxInt;
  btnStop.Enabled := True;
  WriteAppConfig('COMMUNICATION', 'Port', EComm.Text);
  WriteAppConfig('COMMUNICATION', 'Speed', ECommRate.Text);
  PageControl1.ActivePageIndex := 2;
end;

procedure TFJZ_Ring.btnStopClick(Sender: TObject);
begin             
  DQCode.Close;
  FComm.OnCollected:= nil;
  FComm.OnWrited:= nil;
  btnStart.Enabled := True;
  btnFind.Enabled := True;
  btnStop.Enabled := False;
end;

procedure TFJZ_Ring.btnManualFindClick(Sender: TObject);
var
  FCode: string;
begin
  with DQFind do try
    Parameters[0].Value:= FCurrentFactoryCode;
    Open;
    if EOF then mList.Text:= '未发现该电子环'
    else mList.Text:= Format('电子环 %s 在包装 %d 中，该包装颜色为 %s 数量为 %d 已经发给用户 %s',
    [Fields[0].AsString, Fields[1].AsInteger, Fields[2].AsString,
    Fields[3].AsInteger, Fields[4].AsString]);
    //SELECT R.FactoryCode,P.ID,P.RingColor,P.RingCount,O.CoteName
  finally
    Close;
  end;
  btnStop.Enabled:= True;
  PageControl1.ActivePageIndex:= 2;
end;

procedure TFJZ_Ring.btnSpecialClick(Sender: TObject);
begin
  if FCurrentJZCode = 0 then
    Exit;
  with DQCode do
  begin
    Close;
    Parameters[0].Value := FCurrentJZCode;
    Open;
    if EOF then
    begin
      FComm.WriteGameCode(GetMaxId, 0);
      ECurrentCode.Value := GetMaxId + 1;
    end
    else if DlgInfo('警告', Format('系统发现重号环 %s (%x)，是否强制进入？',
      [FCurrentJZCode, ECurrentCode.Value]), mbYesNo, 60) = mrYes then
      SaveRing(FieldByName('ID').AsInteger, ECurrentCode.Value, FCurrentJZCode);
  end;
end;

(* Publish *)

procedure TFJZ_Ring.btnPublish2Click(Sender: TObject);
var
  I, PackageIdx, OrgIdx, Cnt: Integer;
  F: TextFile;

  procedure PublishPackage(PkgID: Integer);
  begin
    DQRingByPackage.Parameters[0].Value := PkgID;
  end;

begin
  with SaveDialog1 do
  begin
    Filename := IntToStr(Integer(EOrg.Items.Objects[EOrg.ItemIndex])) + '_' +
      FormatDatetime('YYYY_m_d', now) + '.txt';
    if not Execute then
      Exit;
  end;
  OrgIdx := Integer(EOrg.Items.Objects[EOrg.ItemIndex]);
  for I := 0 to lstPackage.Items.Count - 1 do
    if lstPackage.Items[I].Selected and DQSearchPackage.Locate('ID', StrToInt(lstPackage.Items[I].Caption), []) then
      with DQSearchPackage do begin
        Edit;
        FieldByName('OrgIdx').AsInteger:= OrgIdx;
        Post;
      end;
  Cnt := 0;
  AssignFile(F, SaveDialog1.Filename);
  Rewrite(F);
  with DQRingByPackage do
    try // 将环库数据写入环库文件
      Parameters[0].Value := OrgIdx;
      Open;
      while not EOF do
      begin
        WriteLn(F, IntToHex(Fields[0].AsLargeInt, 6) + '-' +
          IntToHex(Fields[1].AsLargeInt, 8));
        Inc(Cnt);
        Next;
      end;
    finally
      Close;
    end;
  CloseFile(F);
  mScanList.Lines.Add('共发放' + IntToStr(Cnt) + '支环子给该用户。');
end;

procedure TFJZ_Ring.btnPublishClick(Sender: TObject);
var
  I, PackageIdx, OrgIdx: Integer;
  szRings: TJZRings;

  procedure PublishPackage(PkgID: Integer);
  begin
    DQRingByPackage.Parameters[0].Value := PkgID;
  end;

begin
  with SaveDialog1 do
  begin
    Filename := IntToStr(Integer(EOrg.Items.Objects[EOrg.ItemIndex])) + '_' +
      FormatDatetime('YYYYMMDD', now) + EOrg.Items[EOrg.ItemIndex] + '[' +
      rgCommKind.Items[rgCommKind.ItemIndex] + ']' + ERingColor.Items[ERingColor.ItemIndex] +
      '(' + Copy(lbSelectCount.Caption, 6, MaxInt) + ').Dat';
    if not Execute then
      Exit;
  end;
  szRings := TJZRings.Create;
  OrgIdx := Integer(EOrg.Items.Objects[EOrg.ItemIndex]);
  for I := 0 to lstPackage.Items.Count - 1 do
    if lstPackage.Items[I].Selected and DQSearchPackage.Locate('ID', StrToInt(lstPackage.Items[I].Caption), []) then
    begin // 更改包装信息，将选取的包装发放给用户
      PackageIdx := StrToInt(lstPackage.Items[I].Caption);
      DQSearchPackage.Edit;
      DQSearchPackage.FieldByName('OrgIdx').AsInteger:= OrgIdx;
      DQSearchPackage.FieldByName('Modifiedate').AsDateTime:= Now;
      DQSearchPackage.Post;
      lstPackage.Items[I].SubItems[3]:= EOrg.Text;
      lstPackage.Items[I].SubItems[4]:= DatetimeToStr(Now);
    end;
  with DQRingByPackage do
    try // 将环库数据写入环库文件
      Parameters[0].Value := OrgIdx;
      Open;
      while not EOF do
      begin
        szRings.Add(Fields[0].AsLargeInt, Fields[1].AsLargeInt);
        Next;
      end;
    finally
      Close;
    end;
  //szRings.Title:= IntToStr(OrgIdx) + '@' + FormatDatetime('YYYY_m_d', now) + ':' + IntToStr(szRings.Count);
  szRings.SaveToFile(SaveDialog1.Filename, False);
  mScanList.Lines.Add('共发放' + IntToStr(szRings.Count) + '支环子给该用户。');
  szRings.Free;
end;

procedure TFJZ_Ring.ESearchPublishedChange(Sender: TObject);
begin
  EOrg1.Visible := ESearchPublished.ItemIndex = 1;
end;

procedure TFJZ_Ring.lstPackageSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
var
  szS: string;
  I, pkgCount, ringCount: Integer;
begin
  ringCount:= 0; pkgCount:= 0;
  if Assigned(lstPackage.Selected) then
    ERingColor.ItemIndex:= ERingColor.Items.IndexOf(lstPackage.Selected.SubItems[2]);
  for I := 0 to lstPackage.Items.Count - 1 do
    if lstPackage.Items[I].Selected then begin
      if Integer(lstPackage.Items[I].Data) = 0 then begin
        ringCount := ringCount + StrToInt(lstPackage.Items[I].SubItems[0]);
        Inc(pkgCount);
      end else lstPackage.Items[I].Selected:= False;
    end;
  lbSelectCount.Caption := Format('当前已选择%d袋%d个环', [pkgCount, ringCount]);
end;

(* Event of received ring *)

procedure TFJZ_Ring.DisplayNewRing(Ring: string);
begin
  lbCode1.Caption := lbCode2.Caption;
  lbCode2.Caption := lbCode3.Caption;
  lbCode3.Caption := lbCode4.Caption;
  lbCode4.Caption := lbCode5.Caption;
  lbCode5.Caption := Ring;
end;
      
procedure TFJZ_Ring.DisplayProcess(Value: Integer);
begin
  ProgressBar1.Position := Value;
  ProgressBar1.Repaint;
end;

procedure TFJZ_Ring.EventOfReceived_R(JZCode: Int64; GameCode: Cardinal;
  FactoryCode: Int64);
var
  szW, szR: Integer;
begin
  DisplayProcess(30);
  mScanList.Lines.Add(IntToHex(JZCode, 6) + '-' + IntToHex(FactoryCode, 16));
  while mScanList.Lines.Count > mScanList.Height div 23 do
    mScanList.Lines.Delete(0);
  WriteLog(LOG_DEBUG, 'JZ_RING', Format('扫描A=%x, B=%x, C=%x, D=%x',
    [FCurrentJZCode, JZCode, FactoryCode, ECurrentCode.Value]));
  if JZCode <> FCurrentJZCode then
  begin
    if JZCode < $100000000 then begin
      DisplayNewRing(Format('错号(%x)', [JZCode]));
      Exit;
    end;
    DisplayProcess(50);
    if JZCode <> 0 then
      with DQCode do
      begin // 存储数据库
        Close;
        Parameters[0].Value := JZCode;
        Open;
        if not EOF then
        begin // 重复环提示，只能通过强制写入
          DisplayNewRing(Format('重号(%x)', [JZCode]));
          ECode.Text := IntToHex(FactoryCode, 16);
          Exit;
        end;
        ProgressBar1.Position := ProgressBar1.Position + 1;
        ProgressBar1.Repaint;
        FCurrentJZCode := JZCode;
        SaveRing(0, JZCode, FactoryCode);
        DisplayProcess(100);
        DisplayNewRing(Format('%x-%x', [JZCode, FactoryCode]));
        Exit;
      end;
  end;
end;

procedure TFJZ_Ring.EventOfReceived_W(JZCode: Int64; GameCode: Cardinal;
  FactoryCode: Int64);
var
  szW, szR: Integer;
begin
  if FactoryCode = 0 then begin
    DisplayNewRing('未初始化');
    Exit;
  end;
  DisplayProcess(30);
  mScanList.Lines.Add(IntToHex(JZCode, 6) + '-' + IntToHex(FactoryCode, 16));
  while mScanList.Lines.Count > mScanList.Height div 23 do
    mScanList.Lines.Delete(0);
  WriteLog(LOG_DEBUG, 'JZ_RING', Format('扫描Last=%x, JZ=%x, F=%x',
    [FCurrentJZCode, JZCode, FactoryCode]));
  if JZCode <> FCurrentJZCode then
  begin
    if JZCode <> 0 then
      with DQCode do
      begin // 检查重复
        Close;
        Parameters[0].Value := JZCode;
        Open;
        if not EOF then
        begin // 重复环提示，只能通过强制写入
          DisplayNewRing(Format('重号(%x)', [JZCode]));
          ECode.Text := IntToHex(FactoryCode, 16);
          WriteLog(LOG_DEBUG, 'JZ_RING', Format('重号 JZ=%x, F=%x', [JZCode, FactoryCode]));
//          with DQFactoryCode do begin
//            Close;
//            Parameters[0].Value := FactoryCode;
//            Open;
//            if EOF then begin
//              FComm.WriteGameCode(0, 0);
//              WriteLog(LOG_DEBUG, 'JZ_RING', Format('清号 JZ=%x, F=%x', [JZCode, FactoryCode]));
//            end;
//          end;
          Exit;
        end;
      end;
    DisplayProcess(50);
    FIsSaved := False;
    FCurrentJZCode := GetMaxId;
    FComm.WriteGameCode(FCurrentJZCode, 0);
    WriteLog(LOG_DEBUG, 'JZ_RING', Format('写入A=%x, B=%x, C=%x, D=%x',
      [FCurrentJZCode, JZCode, FactoryCode, ECurrentCode.Value]));
  end
  else if FIsSaved then
    Exit
  else
  begin
    WriteLog(LOG_DEBUG, 'JZ_RING', Format('存储Last=%x, JZ=%x, F=%x',
      [FCurrentJZCode, JZCode, FactoryCode]));
    SaveRing(0, FCurrentJZCode, FactoryCode);
    DisplayNewRing(IntToHex(FCurrentJZCode, 6) + '-' + IntToHex(FactoryCode, 16));
    FIsSaved := True;
    DisplayProcess(100);
  end;
end;

// procedure TFJZ_Ring.EventOfReceived(JZCode: TJZCode; GameCode: TGameCode; FactoryCode: TFactoryCode);
// procedure DisplayNewRing(Ring: string);
// begin
// lbCode1.Caption:= lbCode2.Caption;
// lbCode2.Caption:= lbCode3.Caption;
// lbCode3.Caption:= lbCode4.Caption;
// lbCode4.Caption:= lbCode5.Caption;
// lbCode5.Caption:= Ring;
// end;
// begin
// mScanList.Lines.Add(IntToHex(JZCode.Int, 6) + '-' + IntToHex(FactoryCode.Int, 16));
// while mScanList.Lines.Count > mScanList.Height div 23 do mScanList.Lines.Delete(0);
// if FactoryCode.Int = 0 then Exit;                         //没有扫到环
/////// /  if(FactoryCode.Int and $FFFFFFFF <> $31101220)then begin  //无效的环子
/////// /    DisplayNewRing(NOJUNZHUORING);
/////// /    Exit;
/////// /  end;
// WriteLog(LOG_DEBUG, 'JZ_RING', format('扫描A=%x, B=%x, C=%x, D=%x', [FCurrentJZCode, JZCode.Int, FactoryCode.Int, FMaxID]));
// if FCurrentFactoryCode = FactoryCode.Int then begin
// WriteLog(LOG_DEBUG, 'JZ_RING', '第二次扫描');
// if JZCode.Int = FCurrentJZCode then Exit;               //刚刚完成扫环的环
// if JZCode.Int = GetMaxId then begin                     //写环验证正确，写入数据库
// WriteLog(LOG_DEBUG, 'JZ_RING', '写环验证正确，写入数据库');
// FCurrentJZCode:= JZCode.Int;                          //记录验证标识
// with DQCode do begin                                  //存储数据库
// Close;
// Parameters[0].Value:= FCurrentFactoryCode;
// Open;
// if EOF then begin
// DisplayNewRing(IntToHex(FCurrentJZCode, 6) + '-' + IntToHex(FCurrentFactoryCode, 16));
// SaveRing(0, GetMaxId, FCurrentFactoryCode);
// FMaxID:= GetMaxId;                                //新环子增加序号
// end else begin                                      //重复环提示，只能通过强制写入
// DisplayNewRing(Format('重号(%x)', [FCurrentFactoryCode]));
// ECode.Text:= IntToHex(FCurrentFactoryCode, 16);
// end;
// end;
// end;
// end else begin                                          //新环子，准备写入JZCode
// FCurrentFactoryCode:= FactoryCode.Int;                //记录刚刚读到的环
// with DQCode do begin                                  //检查该环是否已经扫过
// Close;
// Parameters[0].Value:= FCurrentFactoryCode;
// Open;
// if EOF then begin
// JZCode.Int:= GetMaxId;                            //写环
// GameCode.Int:= 0;
// FComm.Comm_Write(JZCode, GameCode, JZC_ONECE);
// WriteLog(LOG_DEBUG, 'JZ_RING', Format('写入A=%x, B=%x, C=%x, D=%x', [FCurrentJZCode, JZCode.Int, FactoryCode.Int, FMaxID]));
// end else begin                                      //重复环提示，只能通过强制写入
// DisplayNewRing(Format('重号(%x)', [FCurrentFactoryCode]));
// ECode.Text:= IntToHex(FCurrentFactoryCode, 16);
// end;
// end;
// end;
// end;

procedure TFJZ_Ring.EventOfWrite(Successed: Boolean);
begin
  if (not Successed) then
  begin         
    DisplayNewRing('写入失败');
    mScanList.Lines.Add('写入失败。');
    WriteLog(LOG_DEBUG, 'JZ_RING', '写入失败');
    DisplayProcess(0);
    FCurrentJZCode:= 0;
  end else DisplayProcess(90);
end;

procedure TFJZ_Ring.EventToFind(JZCode: Int64; GameCode: Cardinal;
  FactoryCode: Int64);
begin
  if FactoryCode = 0 then
    Exit; // 没有扫到环
  // if(FactoryCode.Int and $FFFFFFFF <> $31101220)then Exit;  //无效的环子     if JZCode <> FCurrentJZCode then begin
  with DQFind do
    try
      Parameters[0].Value := JZCode;
      Open;
      if EOF then
        mList.Lines.Insert(0, '未发现该电子环')
      else
        mList.Lines.Insert(0,
          Format('电子环 %s 在包装 %d 中，该包装颜色为 %s 数量为 %d 已经发给用户 %s',
          [Fields[0].AsString, Fields[1].AsInteger, Fields[2].AsString,
          Fields[3].AsInteger, Fields[4].AsString]));
      // SELECT R.FactoryCode,P.ID,P.RingColor,P.RingCount,O.CoteName
    finally
      Close;
    end;
end;

function TFJZ_Ring.GetMaxId: Int64;
begin
  if (chkUseCatch.Checked) or (ECurrentCode.Value = 0) then begin
    Result:= FCoteDB.ADOConn.Execute('SELECT ISNULL(MAX(ChipCode),0) AS MID ' +
      'FROM JOIN_Ring WHERE RingKind=''' + rgCommKind.Items[rgCommKind.ItemIndex] +
      '''').Fields[0].Value;
    Inc(Result);
    if Result < $010001 then Result:= $010001;
    if Result and $FF = 0 then Inc(Result);
  end;
//  begin
//    DQMax.Open;
//    if not DQMax.EOF then
//      Result:= DQMax.Fields[0].AsLargeInt + 1
//    else Result:= $10000001;
//    DQMax.Close;
//  end;
  ECurrentCode.Value := Result;
//  if Result and $FF = 0 then
//    Result:= Result + 1;
//  if Result div 10 = Result / 10 then
//    Inc(Result);
end;

procedure TFJZ_Ring.SaveRing(ID: Integer; JZCode, FactoryCode: Int64);
begin
  if FPackageInfo.ID = 0 then begin
    if MessageDlg('上一袋已结束，是否确认开始新的一袋录入？', mtInformation, mbYesNo, 0) <> mrOK then Exit;
    FPackageInfo.SaveToDB(DQSearchPackage);
    FCurrentItem := DisplayPackage(DQSearchPackage);
  end;
  FRingInfo.ID := ID;
  FRingInfo.FactoryCode := FactoryCode;
  FRingInfo.Factory:= EFactory.Text;
  FRingInfo.PackageIdx := FPackageInfo.ID;
  FRingInfo.ChipCode := JZCode;
  FRingInfo.InputDate := now;
  if not FRingInfo.SaveToDB(DQCode) then
    raise Exception.Create(LastInfo);
  FPackageInfo.RingCount:= FPackageInfo.RingCount + 1;
  FPackageInfo.SaveToDB(DQSearchPackage);
  FCurrentItem.SubItems[0]:= IntToStr(FPackageInfo.RingCount);
  lbCount.Caption := IntToStr(FPackageInfo.RingCount);
  WavBeep();
  if FPackageInfo.RingCount = ERingCount.Value then
  begin
    ShowMessage('本袋录入已经结束，请标注袋号，并完成密封！');
    FPackageInfo.ID := 0;
    FPackageInfo.RingCount := 0;
    lbCount.Caption := '0';
  end;
end;

end.
