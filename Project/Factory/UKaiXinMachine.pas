unit UKaiXinMachine;

interface

uses
  Windows, Forms, SysUtils, Classes, ExtCtrls, UVonLog, UCommunication, StdCtrls,
  UVonSystemFuns, ZLib, Dialogs, UVonCrypt, UCoteComm;

type
  /// <summary>凯信只读设备通讯协议</summary>
  TJZCommKaiXin = class(TElectronicMachineBase)
  private
    function CRC(B: PByte; Len: Integer): Byte;
  protected
    procedure SynReceived; override;
    procedure EventOfReceived(buff: array of byte; Len: Integer); override;
    function GetWriteTask(BoardIdx: Integer; JZCode: Int64; GameCode: Cardinal): TVonCommTask; override;
    function GetReadTask(BoardIdx: Integer): TVonCommTask; override;
    function GetCollectTask(): TVonCommTask; override;
  end;


implementation

uses DateUtils;

{ TJZCommKaiXin }

function TJZCommKaiXin.CRC(B: PByte; Len: Integer): Byte;
begin

end;

procedure TJZCommKaiXin.EventOfReceived(buff: array of byte; Len: Integer);
var
  I, J : Integer;
  szBf: array [0..4]of byte;
  CrcCode: byte;
  JZCode, TimeVal: Int64; GameCode: Cardinal;
begin
//  //------  接收集鸽数据 -------
//  //主机返回：00表示无扫描环号，02H表示有集鸽环号  0
//  //          随机数(5)                            1-5
//  //          毫秒值(3)                            6-8
//  //          加密电子环号(5)                      9-13
//  //通讯协议与集鸽踏板相同
//  //主机返回：00表示无扫描环号，02H表示有集鸽环号
//  //          随机数（5）
//  //          毫秒值（3）
//  //          加密电子环号（5）
//  //读取回鸽、集鸽记录
//  BoardStatus[FCurrentBand]:= true;
//  if(Len = 1)and(buff[0] = 0)then Exit; //No pigeon
//  if(Len < 14)or(buff[0] <> 2)then Exit; //Error message
//  //解密电子环号
//  for j:= 0 to 2 do begin
//    szBf[0]:= (buff[13] and $F0) or (buff[ 9] and $0F);
//    szBf[1]:= (buff[10] and $F0) or (buff[12] and $0F);
//    szBf[2]:= (buff[12] and $F0) or (buff[10] and $0F);
//    szBf[3]:= (buff[11] and $F0) or (buff[13] and $0F);
//    szBf[4]:= (buff[ 9] and $F0) or (buff[11] and $0F);
//    for i:= 0 to 4 do
//      buff[9 + i]:= buff[i + 1] xor szBf[i];
//  end;
//  for i:= 0 to 4 do
//    szBf[i]:= buff[9 + i];
//  JZCode:= 0; Move(szBf[0], JZCode, 5);
//  TimeVal:= buff[6] * 65535 + buff[7] * 256 + buff[8];
//  DoReceived(JZCode, 0, 202001010000, IncMilliSecond(Now, - TimeVal), false);
//  WriteLog(LOG_Debug, 'EventOfReceived', format('time = %d or %d = %s',
//    [buff[6] * 65535 + buff[7] * 256 + buff[8], buff[8] * 65535 + buff[7] * 256 + buff[6],
//    DateTimeToStr(IncMilliSecond(Now, - TimeVal))]));
//  if Assigned(FOnCollected) then
//    FOnCollected(JZCode, 0, 202001010000);
//  if Assigned(FOnReadPigeon) then
//    FOnReadPigeon(JZCode, 0, IncMilliSecond(Now, - TimeVal));

(* ------------------------------------------------------------------------------
* 读取赛绩信息
*-------------------------------------------------------------------------------
*    计算机发送发送7EH
*        00H - 1FH (踏板序号)     公棚的踏板序列号是00H-1FH
*        80H - 9FH (踏板序号)     个人的踏板序列号是80H-9FH
*    返回：
*        00表示无扫描环号                        0
*        02H表示有环号                           0
*          5字节 随机数                          1-5
*          3字节 毫秒值                          6-8
*          5字节 加密电子环号                    9-13
*          1字节 校验                            14
*          1字节 03H结束标记                     15
*----------------------------------------------------------------------------- *)
  //WriteLog(LOG_Debug, '凯信只读', buff, len);
  if len = 1 then Exit;
  if len = 9 then begin
    JZCode:= 0; Move(buff[1], JZCode, 5);
    WriteLog(LOG_Debug, '凯信只读', Format('读取 %x(%d)', [JZCode, JZCode]));
    DoReceived(JZCode, 0, 202001010000, Now, false);
  end else begin
    //解密电子环号
    for j:= 0 to 2 do begin
      szBf[0]:= (buff[13] and $F0) or (buff[ 9] and $0F);
      szBf[1]:= (buff[10] and $F0) or (buff[12] and $0F);
      szBf[2]:= (buff[12] and $F0) or (buff[10] and $0F);
      szBf[3]:= (buff[11] and $F0) or (buff[13] and $0F);
      szBf[4]:= (buff[ 9] and $F0) or (buff[11] and $0F);
      for i:= 0 to 4 do
        buff[9 + i]:= buff[i + 1] xor szBf[i];
    end;
    for i:= 0 to 4 do
      szBf[i]:= buff[9 + i];
    JZCode:= 0; Move(szBf[0], JZCode, 5);
    TimeVal:= buff[6] * 65535 + buff[7] * 256 + buff[8];
    DoReceived(JZCode, 0, 202001010000, IncMilliSecond(Now, - TimeVal), false);
  end;
end;

function TJZCommKaiXin.GetCollectTask: TVonCommTask;
begin
  Result:= TVonCommTask.Create;
  Result.SendCount:= 0;
  Result.Cmd:= CMD_Collect;
  Result.Len:= 2;
  Result.Data[0]:= $7E;
  Result.Data[1]:= 0;
  Result.Next:= nil;
end;

function TJZCommKaiXin.GetReadTask(BoardIdx: Integer): TVonCommTask;
begin
  Result:= TVonCommTask.Create;
  Result.SendCount:= 0;
  Result.Cmd:= CMD_Collect;
  Result.Len:= 2;
  Result.Data[0]:= $7E;
  Result.Data[1]:= FBoardIdx;
  Result.Next:= nil;
  Inc(FBoardIdx);
  if FBoardIdx >= FBoardCount then FBoardIdx:= 0;
end;

function TJZCommKaiXin.GetWriteTask(BoardIdx: Integer; JZCode: Int64;
  GameCode: Cardinal): TVonCommTask;
begin

end;

procedure TJZCommKaiXin.SynReceived;
begin
  case FLastCmd of
  CMD_Collect, CMD_Read: begin
      if CheckRing then
        if not CheckElectronicCode(FCurrentJZCode) then begin         //, FCurrentFactoryCode
          WriteLog(LOG_FAIL, '发现非荣冠脚环', format('发现非荣冠脚环 JZ->%x(%d), FC->%x(%d)',
            [FCurrentJZCode, FCurrentJZCode, FCurrentFactoryCode, FCurrentFactoryCode]));
          FCurrentJZCode:= 0;
        end;
      try
        if Assigned(FOnCollected) then FOnCollected(FCurrentJZCode, FCurrentGameCode, FCurrentFactoryCode);
        if Assigned(FOnReadPigeon) then FOnReadPigeon(FCurrentJZCode, FCurrentGameCode, FCurrentFlyTime);
      except
        On E: Exception do
          WriteLog(LOG_FAIL, 'TJZCommBase.Execute', E.Message);
      end;
    end;
  CMD_Write: if Assigned(FOnWrited) then FOnWrited(FCurrentWriteSuccessed);
  end;

end;

initialization
  RegElectronicMachine('', TJZCommKaiXin);

end.
