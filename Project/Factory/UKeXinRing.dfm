object FKeXinRings: TFKeXinRings
  Left = 0
  Top = 0
  Caption = #20975#20449#29615#24211#21457#25918
  ClientHeight = 590
  ClientWidth = 818
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 688
    Top = 0
    Width = 130
    Height = 590
    Align = alRight
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    object Label1: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 42
      Width = 24
      Height = 13
      Align = alTop
      Caption = #29615#21495
    end
    object Label2: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 209
      Width = 36
      Height = 13
      Align = alTop
      Caption = #20844#26842#21495
    end
    object btnPublish: TButton
      AlignWithMargins = True
      Left = 4
      Top = 255
      Width = 122
      Height = 32
      Align = alTop
      Caption = #21457#29615
      TabOrder = 0
      OnClick = btnPublishClick
      ExplicitTop = 209
    end
    object btnReadFile: TButton
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 122
      Height = 32
      Align = alTop
      Caption = #35835#25991#20214
      TabOrder = 1
      OnClick = btnReadFileClick
      ExplicitLeft = 6
    end
    object ERing: TEdit
      AlignWithMargins = True
      Left = 4
      Top = 61
      Width = 122
      Height = 21
      Align = alTop
      TabOrder = 2
    end
    object btnNew: TButton
      AlignWithMargins = True
      Left = 4
      Top = 111
      Width = 122
      Height = 32
      Align = alTop
      Caption = #28155#21152#29615#21495
      TabOrder = 3
      OnClick = btnNewClick
    end
    object chkHex: TCheckBox
      AlignWithMargins = True
      Left = 4
      Top = 88
      Width = 122
      Height = 17
      Align = alTop
      Caption = #21313#20845#36827#21046
      TabOrder = 4
    end
    object chkAsc: TCheckBox
      AlignWithMargins = True
      Left = 4
      Top = 187
      Width = 122
      Height = 16
      Align = alTop
      Caption = #23383#33410#20498#24207
      Checked = True
      State = cbChecked
      TabOrder = 5
    end
    object btnDelete: TButton
      AlignWithMargins = True
      Left = 4
      Top = 149
      Width = 122
      Height = 32
      Align = alTop
      Caption = #21024#38500#24403#21069#25442#29615#21495
      TabOrder = 6
      OnClick = btnDeleteClick
    end
    object Memo1: TMemo
      Left = 1
      Top = 312
      Width = 128
      Height = 277
      Align = alClient
      TabOrder = 7
      ExplicitLeft = 6
      ExplicitTop = 315
    end
    object chkTest: TCheckBox
      AlignWithMargins = True
      Left = 4
      Top = 293
      Width = 122
      Height = 16
      Align = alTop
      Caption = #27979#35797
      TabOrder = 8
      ExplicitTop = 247
    end
    object ETitle: TEdit
      AlignWithMargins = True
      Left = 4
      Top = 228
      Width = 122
      Height = 21
      Align = alTop
      TabOrder = 9
      ExplicitLeft = 6
      ExplicitTop = 255
    end
  end
  object lstRing: TListBox
    Left = 0
    Top = 0
    Width = 688
    Height = 590
    Align = alClient
    Columns = 6
    ItemHeight = 13
    TabOrder = 1
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.txt'
    Filter = #25991#26412#25991#20214'|*.txt|'#20219#24847#25991#20214'|*.*'
    Left = 176
    Top = 80
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '*.dat'
    FileName = 'rings.dat'
    Left = 176
    Top = 136
  end
end
