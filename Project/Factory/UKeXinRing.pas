unit UKeXinRing;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, ZLib,
  UVonCrypt, UCoteComm, UPlatformDB;

type
  TFKeXinRings = class(TForm)
    Panel1: TPanel;
    btnPublish: TButton;
    btnReadFile: TButton;
    lstRing: TListBox;
    Label1: TLabel;
    ERing: TEdit;
    btnNew: TButton;
    chkHex: TCheckBox;
    chkAsc: TCheckBox;
    btnDelete: TButton;
    OpenDialog1: TOpenDialog;
    Memo1: TMemo;
    SaveDialog1: TSaveDialog;
    chkTest: TCheckBox;
    Label2: TLabel;
    ETitle: TEdit;
    procedure btnReadFileClick(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure btnPublishClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TVonInt64 = packed record
    case Integer of
      0: (Value: Int64);
      1: (Lo, Hi: Cardinal);
      2: (Cardinals: array [0..1] of Cardinal);
      3: (Words: array [0..3] of Word);
      4: (Bytes: array [0..7] of Byte);
  end;

var
  FKeXinRings: TFKeXinRings;

implementation

{$R *.dfm}

function HexToInt(S: string): Int64;
const HEX = '0123456789ABCDEF';
var i: Integer; h: Int64;
begin
  h:= 1; Result:= 0;
  for I := Length(S) - 1 downto 0 do begin
    Result:= Hex.IndexOf(S[I]) * h;
    h:= h * 16;
  end;
end;

procedure TFKeXinRings.btnNewClick(Sender: TObject);
var
  l: Int64;
begin
  if chkAsc.Checked then lstRing.Items.Add(IntToStr(HexToInt(ERing.Text)))
  else lstRing.Items.Add(ERing.Text);
end;

procedure TFKeXinRings.btnPublishClick(Sender: TObject);
var
  I: Integer;
  V: TVonInt64;
  R: TCryptCollection<TElectronicRing>;
  b: Byte;
  ring : TElectronicRing;
begin
  if (chkTest.Checked)and(not SaveDialog1.Execute()) then exit;
  R:= TCryptCollection<TElectronicRing>.Create;
  for I := 0 to lstRing.Items.Count - 1 do
    if chkAsc.Checked then begin
      //00 00 00 D5AF570002 -> 000000020057AFD5
      V.Value:= StrToInt64(lstRing.Items[I]);
      b:= V.Bytes[0]; V.Bytes[0]:= V.Bytes[4]; V.Bytes[4]:= b;
      b:= V.Bytes[1]; V.Bytes[1]:= V.Bytes[3]; V.Bytes[3]:= b;
      ring := TElectronicRing.Create;
      ring.ElectronicCode:= v.Value;
      ring.FactoryCode:= 202001010000;
      R.Add(ring);
    end else begin
      ring := TElectronicRing.Create;
      ring.ElectronicCode:= StrToInt64(lstRing.Items[I]);
      ring.FactoryCode:= 202001010000;
      R.Add(ring);
    end;
  if not chkTest.Checked then
    R.SaveToFile(SaveDialog1.FileName, CERT_KEY, CERT_CIPHER, CERT_HASH)
  else for I := 0 to R.Count - 1 do
    Memo1.Lines.Add(format('%x %d', [R.Items[I].ElectronicCode, R.Items[I].ElectronicCode]));
  R.Free;
end;

procedure TFKeXinRings.btnReadFileClick(Sender: TObject);
begin
  with OpenDialog1 do
    if execute then
      lstRing.Items.LoadFromFile(filename);
end;

procedure TFKeXinRings.btnDeleteClick(Sender: TObject);
begin
  lstRing.DeleteSelected;
end;

end.
