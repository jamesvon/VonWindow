unit URongguanMachine;

interface

uses
  Windows, Forms, SysUtils, Classes, ExtCtrls, UVonLog, UCommunication, StdCtrls,
  UVonSystemFuns, ZLib, Dialogs, UVonCrypt, UCoteComm;

type
  TJZRingByte = class
  private
    function FourToFive(B: Byte): Byte;
    function FiveToFour(B: Byte): Byte;
  public
    Bytes: array [0 .. 4] of Byte;
    JZCode: Cardinal;
    GameCode: Word;
    procedure TranToCode;
    procedure TranToByte;
  end;

  /// <summary>高频只读环设备通讯协议</summary>
  TJZComm2019 = class(TElectronicMachineBase)
  private
    function CRC(B: PByte; Len: Integer): Byte;
  protected
    procedure SynReceived; override;
    procedure EventOfReceived(buff: array of byte; Len: Integer); override;
    function GetWriteTask(BoardIdx: Integer; JZCode: Int64; GameCode: Cardinal): TVonCommTask; override;
    function GetReadTask(BoardIdx: Integer): TVonCommTask; override;
    function GetCollectTask(): TVonCommTask; override;
  end;

  /// <summary>高频读写设备通讯协议</summary>
  TJZCommRW = class(TElectronicMachineBase)
  private
    FJZRingByte : TJZRingByte;
    function CRC(B: PByte; Len: Integer): Byte;
  protected
    procedure SynReceived; override;
    procedure EventOfReceived(buff: array of byte; Len: Integer); override;
    function GetWriteTask(BoardIdx: Integer; JZCode: Int64; GameCode: Cardinal): TVonCommTask; override;
    function GetReadTask(BoardIdx: Integer): TVonCommTask; override;
    function GetCollectTask(): TVonCommTask; override;
  public
    constructor Create; override;
    destructor Destroy; override;
  end;

  /// <summary>高频只读设备通讯协议</summary>
  TJZCommR = class(TElectronicMachineBase)
  private
    function CRC(B: PByte; Len: Integer): Byte;
  protected
    procedure SynReceived; override;
    procedure EventOfReceived(buff: array of byte; Len: Integer); override;
    function GetWriteTask(BoardIdx: Integer; JZCode: Int64; GameCode: Cardinal): TVonCommTask; override;
    function GetReadTask(BoardIdx: Integer): TVonCommTask; override;
    function GetCollectTask(): TVonCommTask; override;
  end;


implementation

uses DateUtils;

{ TJZComm2019 }

(*通讯协议：38400BPS
数据规则：AAH，55H,踏板编号，命令，数据，校验码，0DH,0AH
踏板编号：0为集鸽器，1-64为踏板编号

7-1.822ms
命令：80H读踏板数据
23-7.03ms
返回：1字节应答（0表示没有数据，01H表示有脚环），8个字节UID，4个字节数据，4字节延时
举例：
AA 55 01 80 81 0D 0A
AA 55 01 00 01 0D 0A
AA 55 01 01 XX XX XX XX XX XX XX XX DD DD DD DD TT TT TT TT PP 0D 0A

23-5.99ms
命令：81H写入信鸽数据，8个字节UID，4个字节数据
7-1.822ms
返回：01H*)

function TJZComm2019.CRC(B: PByte; Len: Integer): Byte;
var
  I: Integer;
begin
  Result:= B[0];
  for I := 1 to Len - 1 do
    Result:= Result xor B[I];
end;

procedure TJZComm2019.EventOfReceived(buff: array of byte; Len: Integer);
var
  jzCode: Int64;
  GsmeCode: Cardinal;
begin
  case FLastCmd of
    //返回：1字节应答（0表示没有数据，01H表示有脚环），8个字节UID，4个字节数据，4字节延时
  CMD_Collect, CMD_Read: if Len > 1 then begin
      Move(buff[1], jzCode, 8);
      Move(buff[9], GsmeCode, 8);
      DoReceived(JZCode, 0, JZCode, Now, False);
    end;
  CMD_Write: if Len = 1 then
    DoReceived(JZCode, 0, JZCode, Now, buff[0] = 1);
  end;
end;

function TJZComm2019.GetCollectTask: TVonCommTask;
begin
  Result:= TVonCommTask.Create;
  Result.SendCount:= 0;
  Result.Cmd:= CMD_Collect;
  Result.Len:= 7;
  Result.Data[0]:= $AA;
  Result.Data[1]:= $55;
  Result.Data[2]:= 0;
  Result.Data[3]:= $80;
  Result.Data[4]:= CRC(@Result.Data[2], 2); //Result.Data[2] xor Result.Data[3];
  Result.Data[5]:= $0D;
  Result.Data[6]:= $0A;
end;

function TJZComm2019.GetReadTask(BoardIdx: Integer): TVonCommTask;
begin
  Result:= TVonCommTask.Create;
  Result.SendCount:= 0;
  Result.Cmd:= CMD_Read;
  Result.Len:= 7;
  Result.Data[0]:= $AA;
  Result.Data[1]:= $55;
  Result.Data[2]:= BoardIdx;
  Result.Data[3]:= $80;
  Result.Data[4]:= CRC(@Result.Data[2], 2); //Result.Data[2] xor Result.Data[3];
  Result.Data[5]:= $0D;
  Result.Data[6]:= $0A;
end;

function TJZComm2019.GetWriteTask(BoardIdx: Integer; JZCode: Int64; GameCode: Cardinal): TVonCommTask;
begin
  Result:= TVonCommTask.Create;
  Result.SendCount:= 0;
  Result.Cmd:= CMD_Write;
  Result.Len:= 14;
  Result.Data[0]:= $AA;
  Result.Data[1]:= $55;
  Result.Data[2]:= BoardIdx;
  Result.Data[3]:= $81;
  Move(JZCode, Result.Data[4], 5);
  Move(GameCode, Result.Data[9], 3);
  Result.Data[11]:= CRC(@Result.Data[2], 10);
  Result.Data[12]:= $0D;
  Result.Data[13]:= $0A;
end;

procedure TJZComm2019.SynReceived;
begin
  WriteLog(LOG_DEBUG, 'TJZComm.SynReceived', 'started ' + IntToStr(Ord(FLastCmd)));
  try
    case FLastCmd of
    CMD_Collect: begin
      WriteLog(LOG_DEBUG, 'TJZComm.SynReceived', 'CMD_Collect');
        if CheckRing then begin
          FCurrentJZCode:= CheckFactoryCode(FCurrentFactoryCode);
          if not FCurrentJZCode = 0 then begin
            WriteLog(LOG_FAIL, '发现非荣冠脚环', IntToStr(FCurrentJZCode));
            Exit;
          end;
        end else if (FCurrentFactoryCode and $00000000FFFFFFFF = 0) then begin
          WriteLog(LOG_FAIL, '发现未初始化脚环', IntToStr(FCurrentJZCode));
          FCurrentFactoryCode:= 0;
        end;
        WriteLog(LOG_DEBUG, 'OnCollected', Format('JZ=%x, GC=%x, FC=%x', [FCurrentJZCode, FCurrentGameCode, FCurrentFactoryCode]));
        if Assigned(FOnCollected) then FOnCollected(FCurrentJZCode, FCurrentGameCode, FCurrentFactoryCode);
      end;
    CMD_Read: begin
      WriteLog(LOG_DEBUG, 'TJZComm.SynReceived', 'CMD_Read');
        if CheckRing then
          if not CheckElectronicCode(FCurrentJZCode) then begin  //, FCurrentFactoryCode
            WriteLog(LOG_FAIL, '发现非荣冠脚环', IntToStr(FCurrentJZCode));
            FCurrentJZCode:= 0;
          end;
          WriteLog(LOG_DEBUG, 'OnReadPigeon', Format('JZ=%x, GC=%x, FT=%f', [FCurrentJZCode, FCurrentGameCode, FCurrentFlyTime]));
          if Assigned(FOnReadPigeon) then FOnReadPigeon(FCurrentJZCode, FCurrentGameCode, FCurrentFlyTime);
      end;
    CMD_Write: if Assigned(FOnWrited) then FOnWrited(FCurrentWriteSuccessed);
    end;
  except
    On E: Exception do
      WriteLog(LOG_FAIL, 'TJZComm.SynReceived ' + IntToStr(Ord(FLastCmd)), E.Message);
  end;
end;

{ TJZComm38400BPS }
(*通讯协议：38400BPS
数据规则：AAH，55H,踏板编号，命令，数据，校验码，0DH,0AH
踏板编号：0为集鸽器，1-64为踏板编号

7-1.822ms
命令：80H读踏板数据
23-7.03ms
返回：1字节应答（0表示没有数据，01H表示有脚环），8个字节UID，4个字节数据，4字节延时
举例：
AA 55 01 80 81 0D 0A
AA 55 01 00 01 0D 0A
AA 55 01 01 XX XX XX XX XX XX XX XX DD DD DD DD TT TT TT TT PP 0D 0A


23-5.99ms
命令：81H写入信鸽数据，8个字节UID，4个字节数据
7-1.822ms
返回：01H

AA 55 01 01 E0 04 01 50 76 71 A0 55 00 00 00 DE 00 00 26 4F F0 0D 0A
AA 55 01 01 E0 04 01 50 76 71 A0 55 00 00 00 DE 00 00 08 5D CC 0D 0A
AA 55 01 01 E0 04 01 50 76 71 A0 55 00 00 00 DE 00 00 03 B1 2B 0D 0A
AA 55 01 01 E0 04 01 50 76 71 A0 55 00 00 00 DE 00 00 02 2B B0 0D 0A
*)
function TJZCommRW.CRC(B: PByte; Len: Integer): Byte;
begin

end;

constructor TJZCommRW.Create;
begin
  inherited;
  FJZRingByte := TJZRingByte.Create;
end;

destructor TJZCommRW.Destroy;
begin
  FJZRingByte.Free;
  inherited;
end;

procedure TJZCommRW.EventOfReceived(buff: array of byte; Len: Integer);
var
  I, J : Integer;
  szBf: array [0..4]of byte;
  FactoryCode: Int64;
  JZCode: Int64;
  GameCode: Cardinal;
  CrcCode: Byte;
begin
//  WriteLog(LOG_DEBUG, 'TJZCommRW.EventOfReceived ', IntToStr(Len));
  case FLastCmd of
  (* ---------------------------------------------------------------------------
  * 读取集鸽环号
  *-----------------------------------------------------------------------------
  *    计算机发送发送7FH，00H
  *    返回:
  *        00H表示无环返回|03H表示有环             0
  *          5字节 环号                            1-5
  *          8字节 芯片UID                         6-13
  *-------------------------------------------------------------------------- *)
  CMD_Collect: if(Len = 14)and(buff[0] = 3) then begin
//      WriteLog(LOG_DEBUG, 'TJZCommRW.EventOfReceived ', 'CMD_Collect');
      Move(buff[1], FJZRingByte.Bytes[0], 5);
      FJZRingByte.TranToCode;
      Move(buff[6], FactoryCode, 8);
//      WriteLog(LOG_DEBUG, 'TJZCommRW.EventOfReceived ', 'DoReceived');
      DoReceived(FJZRingByte.JZCode, FJZRingByte.GameCode, FactoryCode and $FFFFFFFF00000000, Now, False);
//      FCurrentJZCode:= FJZRingByte.JZCode;
//      FCurrentGameCode:= FJZRingByte.GameCode;
//      FCurrentFactoryCode:= FactoryCode and $FFFFFFFF00000000;
//      FCurrentFlyTime:= Now;
//      FCurrentWriteSuccessed:= False;
//      Synchronize(SynReceived);
    end;
  (* ---------------------------------------------------------------------------
  * 读取赛绩信息
  *-----------------------------------------------------------------------------
  *    计算机发送发送7EH
  *        00H - 1FH (踏板序号)     公棚的踏板序列号是00H-1FH
  *        80H - 9FH (踏板序号)     个人的踏板序列号是80H-9FH
  *    返回：
  *        00表示无扫描环号                        0
  *        02H表示有环号                           0
  *          5字节 随机数                          1-5
  *          3字节 毫秒值                          6-8
  *          5字节 加密电子环号                    9-13
  *          1字节 校验                            14
  *          1字节 03H结束标记                     15
  *-------------------------------------------------------------------------- *)
  CMD_Read: if(Len = 16)and(buff[0] = 2) then begin
//      WriteLog(LOG_DEBUG, 'TJZCommRW.EventOfReceived ', 'CMD_Read');
      CrcCode := 0;
      for j := 1 to 13 do
        CrcCode := CrcCode xor buff[j];
      if (buff[15] <> 3) or (CrcCode <> buff[14]) then
      begin
        WriteLog(LOG_FAIL, 'Received a message, and its CRC is error ', buff, Len);
        Exit;
      end;
      for j := 0 to 2 do
      begin
        szBf[0] := (buff[13] and $F0) or (buff[9] and $0F);
        szBf[1] := (buff[10] and $F0) or (buff[12] and $0F);
        szBf[2] := (buff[12] and $F0) or (buff[10] and $0F);
        szBf[3] := (buff[11] and $F0) or (buff[13] and $0F);
        szBf[4] := (buff[9] and $F0) or (buff[11] and $0F);
        for i := 0 to 4 do
          buff[9 + i] := buff[i + 1] xor szBf[i];
      end;
      for i := 0 to 4 do
        szBf[i] := buff[9 + i];
      Move(szBf[0], FJZRingByte.Bytes[0], 5);
      FJZRingByte.TranToCode;
//      WriteLog(LOG_DEBUG, 'TJZCommRW.EventOfReceived ', 'DoReceived');
      DoReceived(FJZRingByte.JZCode, FJZRingByte.GameCode, 0, Now - (buff[6] * 65536 + buff[7] * 256 + buff[8]) / 86400000, False);

//      FCurrentJZCode:= FJZRingByte.JZCode;
//      FCurrentGameCode:= FJZRingByte.GameCode;
//      FCurrentFactoryCode:= 0;
//      FCurrentFlyTime:= Now - (buff[6] * 65536 + buff[7] * 256 + buff[8]) / 86400000;
//      WriteLog(LOG_INFO, 'W_COMM_RECEIVED', Format('Received a ring JZCode=%x GameCode=%x FactoryCode=%x, back time=%f ',
//        [FCurrentJZCode, FCurrentGameCode, FCurrentFactoryCode, FCurrentFlyTime]));
//      //SynReceived;
//      Synchronize(SynReceived);
    end;
  (* --------------------------------------------------------------------------
  * 写环
  *----------------------------------------------------------------------------
  *    计算机发送发送7DH，5字节 环号
  *    返回:
  *        00H表示正确                             0
  *        01表示错误                              0
  *-------------------------------------------------------------------------- *)
  CMD_Write: if Len = 1 then begin
//      WriteLog(LOG_DEBUG, 'TJZCommRW.EventOfReceived ', 'CMD_Write');
      FCurrentWriteSuccessed:= buff[0] = 0;
//      WriteLog(LOG_DEBUG, 'TJZCommRW.EventOfReceived ', 'DoReceived');
      Synchronize(SynReceived);
    end;
  end;
end;

function TJZCommRW.GetCollectTask: TVonCommTask;
begin
(* -----------------------------------------------------------------------------
* 读取集鸽环号
*-------------------------------------------------------------------------------
*    计算机发送发送7FH，00H
*    返回:
*        00H表示无环返回                         0
*        03H表示有环                             0
*          5字节 环号                            1-5
*          8字节 芯片UID                         6-13
*----------------------------------------------------------------------------- *)
  Result:= TVonCommTask.Create;
  Result.SendCount:= 0;
  Result.Cmd:= CMD_Collect;
  Result.Len:= 2;
  Result.Data[0]:= $7F;
  Result.Data[1]:= $00;//BoardIdx;
  Result.Next:= nil;
end;

function TJZCommRW.GetReadTask(BoardIdx: Integer): TVonCommTask;
begin
(* -----------------------------------------------------------------------------
* 读取赛绩信息
*-------------------------------------------------------------------------------
*    计算机发送发送7EH
*        00H - 1FH (踏板序号)     公棚的踏板序列号是00H-1FH
*        80H - 9FH (踏板序号)     个人的踏板序列号是80H-9FH
*    返回：
*        00表示无扫描环号                        0
*        02H表示有环号                           0
*          5字节 随机数                          1-5
*          3字节 毫秒值                          6-8
*          5字节 加密电子环号                    9-13
*          1字节 校验                            14
*          1字节 03H结束标记                     15
*----------------------------------------------------------------------------- *)
  Result:= TVonCommTask.Create;
  Result.SendCount:= 0;
  Result.Cmd:= CMD_Read;
  Result.Len:= 2;
  Result.Data[0]:= $7E;
  Result.Data[1]:= BoardIdx;
  Result.Next:= nil;
end;

function TJZCommRW.GetWriteTask(BoardIdx: Integer; JZCode: Int64;
  GameCode: Cardinal): TVonCommTask;
begin
(* -----------------------------------------------------------------------------
* 写环
*-------------------------------------------------------------------------------
*    计算机发送发送7DH，5字节 环号
*    返回:
*        00H表示正确                             0
*        01表示错误                              0
*----------------------------------------------------------------------------- *)
  Result:= TVonCommTask.Create;
  Result.SendCount:= 1;
  Result.Cmd:= CMD_Write;
  Result.Len:= 6;
  Result.Data[0]:= $7D;
  FJZRingByte.JZCode:= JZCode;
  FJZRingByte.GameCode:= GameCode;
  FJZRingByte.TranToByte;
  Move(FJZRingByte.Bytes, Result.Data[1], 5);
end;

procedure TJZCommRW.SynReceived;
begin
  //WriteLog(LOG_DEBUG, 'TJZComm.SynReceived', 'started ' + IntToStr(Ord(FLastCmd)));
  try
    case FLastCmd of
    CMD_Collect: begin
      //WriteLog(LOG_DEBUG, 'TJZComm.SynReceived', 'CMD_Collect');
        if CheckRing then begin
          FCurrentJZCode:= CheckFactoryCode(FCurrentFactoryCode);
        end else if (FCurrentFactoryCode and $00000000FFFFFFFF = 0) then begin
          WriteLog(LOG_FAIL, '发现未初始化脚环', IntToStr(FCurrentJZCode));
          FCurrentFactoryCode:= 0;
        end;
        WriteLog(LOG_DEBUG, 'OnCollected', Format('JZ=%x, GC=%x, FC=%x', [FCurrentJZCode, FCurrentGameCode, FCurrentFactoryCode]));
        if Assigned(FOnCollected) then FOnCollected(FCurrentJZCode, FCurrentGameCode, FCurrentFactoryCode);
      end;
    CMD_Read: begin
      //WriteLog(LOG_DEBUG, 'TJZComm.SynReceived', 'CMD_Read');
        if CheckRing then
          if not CheckElectronicCode(FCurrentJZCode) then begin  //, FCurrentFactoryCode
            WriteLog(LOG_FAIL, '发现非荣冠脚环', IntToStr(FCurrentJZCode));
            FCurrentJZCode:= 0;
          end;
          WriteLog(LOG_DEBUG, 'OnReadPigeon', Format('JZ=%x, GC=%x, FT=%f', [FCurrentJZCode, FCurrentGameCode, FCurrentFlyTime]));
          if Assigned(FOnReadPigeon) then FOnReadPigeon(FCurrentJZCode, FCurrentGameCode, FCurrentFlyTime);
      end;
    CMD_Write: if Assigned(FOnWrited) then FOnWrited(FCurrentWriteSuccessed);
    end;
  except
    On E: Exception do
      WriteLog(LOG_FAIL, 'TJZComm.SynReceived ' + IntToStr(Ord(FLastCmd)), E.Message);
  end;
end;

{ TJZCommR }

(*通讯协议：38400BPS
数据规则：AAH，55H,踏板编号，命令，数据，校验码，0DH,0AH
踏板编号：0为集鸽器，1-64为踏板编号

7-1.822ms
命令：80H读踏板数据
23-7.03ms
返回：1字节应答（0表示没有数据，01H表示有脚环），8个字节UID，4个字节数据，4字节延时
举例：
AA 55 01 80 81 0D 0A
AA 55 01 00 01 0D 0A
AA 55 01 01 XX XX XX XX XX XX XX XX DD DD DD DD TT TT TT TT PP 0D 0A


23-5.99ms
命令：81H写入信鸽数据，8个字节UID，4个字节数据
7-1.822ms
返回：01H

AA 55 01 01 E0 04 01 50 76 71 A0 55 00 00 00 DE 00 00 26 4F F0 0D 0A
AA 55 01 01 E0 04 01 50 76 71 A0 55 00 00 00 DE 00 00 08 5D CC 0D 0A
AA 55 01 01 E0 04 01 50 76 71 A0 55 00 00 00 DE 00 00 03 B1 2B 0D 0A
AA 55 01 01 E0 04 01 50 76 71 A0 55 00 00 00 DE 00 00 02 2B B0 0D 0A
*)

function TJZCommR.CRC(B: PByte; Len: Integer): Byte;
begin

end;

procedure TJZCommR.EventOfReceived(buff: array of byte; Len: Integer);
var
  I, J : Integer;
  szBf: array [0..4]of byte;
  JZCode, TimeVal: Int64; GameCode: Cardinal;
begin
  //------  接收集鸽数据 -------
  //主机返回：00表示无扫描环号，02H表示有集鸽环号  0
  //          随机数(5)                            1-5
  //          毫秒值(3)                            6-8
  //          加密电子环号(5)                      9-13
  //通讯协议与集鸽踏板相同
  //主机返回：00表示无扫描环号，02H表示有集鸽环号
  //          随机数（5）
  //          毫秒值（3）
  //          加密电子环号（5）
  //读取回鸽、集鸽记录
  BoardStatus[FCurrentBand]:= true;
  if(Len = 1)and(buff[0] = 0)then Exit; //No pigeon
  if(Len < 14)or(buff[0] <> 2)then Exit; //Error message
  //解密电子环号
  for j:= 0 to 2 do begin
    szBf[0]:= (buff[13] and $F0) or (buff[ 9] and $0F);
    szBf[1]:= (buff[10] and $F0) or (buff[12] and $0F);
    szBf[2]:= (buff[12] and $F0) or (buff[10] and $0F);
    szBf[3]:= (buff[11] and $F0) or (buff[13] and $0F);
    szBf[4]:= (buff[ 9] and $F0) or (buff[11] and $0F);
    for i:= 0 to 4 do
      buff[9 + i]:= buff[i + 1] xor szBf[i];
  end;
  for i:= 0 to 4 do
    szBf[i]:= buff[9 + i];
  JZCode:= 0; Move(szBf[0], JZCode, 5);
  TimeVal:= buff[6] * 65535 + buff[7] * 256 + buff[8];
  DoReceived(JZCode, 0, 202001010000, IncMilliSecond(Now, - TimeVal), false);
//  WriteLog(LOG_Debug, 'EventOfReceived', format('time = %d or %d = %s',
//    [buff[6] * 65535 + buff[7] * 256 + buff[8], buff[8] * 65535 + buff[7] * 256 + buff[6],
//    DateTimeToStr(IncMilliSecond(Now, - TimeVal))]));
//  if Assigned(FOnCollected) then
//    FOnCollected(JZCode, 0, 202001010000);
//  if Assigned(FOnReadPigeon) then
//    FOnReadPigeon(JZCode, 0, IncMilliSecond(Now, - TimeVal));
end;

function TJZCommR.GetCollectTask: TVonCommTask;
begin
  Result:= TVonCommTask.Create;
  Result.SendCount:= 0;
  Result.Cmd:= CMD_Collect;
  Result.Len:= 2;
  Result.Data[0]:= $7E;
  Result.Data[1]:= 0;
  Result.Next:= nil;
end;

function TJZCommR.GetReadTask(BoardIdx: Integer): TVonCommTask;
begin
  Result:= TVonCommTask.Create;
  Result.SendCount:= 0;
  Result.Cmd:= CMD_Read;
  Result.Len:= 2;
  Result.Data[0]:= $7E;
  Result.Data[1]:= 0;//BoardIdx;
  Result.Next:= nil;
end;

function TJZCommR.GetWriteTask(BoardIdx: Integer; JZCode: Int64;
  GameCode: Cardinal): TVonCommTask;
begin
  Result:= nil;
end;

procedure TJZCommR.SynReceived;
begin
  case FLastCmd of
  CMD_Collect, CMD_Read: begin
      if CheckRing then
        if not CheckElectronicCode(FCurrentJZCode) then begin         //, FCurrentFactoryCode
          WriteLog(LOG_FAIL, '发现非荣冠脚环', format('发现非荣冠脚环 JZ->%x(%d), FC->%x(%d)',
            [FCurrentJZCode, FCurrentJZCode, FCurrentFactoryCode, FCurrentFactoryCode]));
          FCurrentJZCode:= 0;
        end;
      try
        if Assigned(FOnCollected) then FOnCollected(FCurrentJZCode, FCurrentGameCode, FCurrentFactoryCode);
        if Assigned(FOnReadPigeon) then FOnReadPigeon(FCurrentJZCode, FCurrentGameCode, FCurrentFlyTime);
      except
        On E: Exception do
          WriteLog(LOG_FAIL, 'TJZCommBase.Execute', E.Message);
      end;
    end;
  CMD_Write: if Assigned(FOnWrited) then FOnWrited(FCurrentWriteSuccessed);
  end;

end;

{ TJZRingByte }

function TJZRingByte.FourToFive(B: Byte): Byte;
begin
  case B and $F of
    $0:
      Result := $05; // 5	$5	0	0	1	0	1    3B-4B
    $1:
      Result := $06; // 6	$6	0	0	1	1	0
    $2:
      Result := $07; // 7	$7	0	0	1	1	1
    $3:
      Result := $09; // 9	$9	0	1	0	0	1
    $4:
      Result := $0A; // 10	$A	0	1	0	1	0
    $5:
      Result := $0B; // 11	$B	0	1	0	1	1
    $6:
      Result := $0D; // 13	$D	0	1	1	0	1
    $7:
      Result := $0E; // 14	$E	0	1	1	1	0
    $8:
      Result := $0F; // 15	$F	0	1	1	1	1
    $9:
      Result := $11; // 17	$11	1	0	0	0	1    4B-5B
    $A:
      Result := $12; // 18	$12	1	0	0	1	0                26	$1A	1	1	0	1	0
    $B:
      Result := $13; // 19	$13	1	0	0	1	1                27	$1B	1	1	0	1	1
    $C:
      Result := $15; // 21	$15	1	0	1	0	1                29	$1D	1	1	1	0	1
    $D:
      Result := $16; // 22	$16	1	0	1	1	0                30	$1E	1	1	1	1	0
    $E:
      Result := $17; // 23	$17	1	0	1	1	1                31	$1F	1	1	1	1	1
    $F:
      Result := $19; // 25	$19	1	1	0	0	1
  end;
end;

function TJZRingByte.FiveToFour(B: Byte): Byte;
begin
  case B and $1F of
    $05:
      Result := $0; // 5	$5	0	0	1	0	1    3B-4B
    $06:
      Result := $1; // 6	$6	0	0	1	1	0
    $07:
      Result := $2; // 7	$7	0	0	1	1	1
    $09:
      Result := $3; // 9	$9	0	1	0	0	1
    $0A:
      Result := $4; // 10	$A	0	1	0	1	0
    $0B:
      Result := $5; // 11	$B	0	1	0	1	1
    $0D:
      Result := $6; // 13	$D	0	1	1	0	1
    $0E:
      Result := $7; // 14	$E	0	1	1	1	0
    $0F:
      Result := $8; // 15	$F	0	1	1	1	1
    $11:
      Result := $9; // 17	$11	1	0	0	0	1    4B-5B
    $12:
      Result := $A; // 18	$12	1	0	0	1	0                26	$1A	1	1	0	1	0
    $13:
      Result := $B; // 19	$13	1	0	0	1	1                27	$1B	1	1	0	1	1
    $15:
      Result := $C; // 21	$15	1	0	1	0	1                29	$1D	1	1	1	0	1
    $16:
      Result := $D; // 22	$16	1	0	1	1	0                30	$1E	1	1	1	1	0
    $17:
      Result := $E; // 23	$17	1	0	1	1	1                31	$1F	1	1	1	1	1
    $19:
      Result := $F; // 25	$19	1	1	0	0	1
  end;
end;

procedure TJZRingByte.TranToCode;
begin
{$IFDEF 4B5B}
  // GameCode 1 11111 11111 11111 ($FFFF) -> 1 1111 1111 1111  -> $1FFF 8191
  GameCode := FiveToFour(Bytes[0]) or
    (FiveToFour((Bytes[0] shr 5) or (Bytes[1] shl 3)) shl 4) or
    (FiveToFour(Bytes[1] shr 2) shl 8) or ((Bytes[1] and $80) shl 5);
  // jzCode 01111 1111 1 11111 11 111 11111 ($FF FF FF) -> 0111 1111_1111 1111_1111  -> $7FFFFF 8388607
  JZCode := FiveToFour(Bytes[2]) or
    (FiveToFour((Bytes[2] shr 5) or (Bytes[3] shl 3)) shl 4) or
    (FiveToFour(Bytes[3] shr 2) shl 8) or
    (FiveToFour((Bytes[3] shr 7) or (Bytes[4] shl 1)) shl 12) or
    (FiveToFour(Bytes[4] shr 4) shl 16);
{$ELSE}
  JZCode := 0;
  GameCode := 0;
  Move(Bytes[0], JZCode, 3);
  Move(Bytes[3], GameCode, 2);
{$ENDIF}
end;

procedure TJZRingByte.TranToByte;
begin
{$IFDEF 4B5B}
  // GameCode $1FFF 8191 -> 1 1111 1111 1111  -> 1 11111 11111 11111 ($FFFF)
  // jzCode $7FFFF 524287 -> 0111 1111_1111 1111_1111  -> 01111 11111 11111 11111 11111 ($FF FF FF)
  Bytes[0] := FourToFive(GameCode) or (FourToFive(GameCode shr 4) shl 5);
  // 111 11111
  Bytes[1] := (FourToFive(GameCode shr 4) shr 3) or
    (FourToFive(GameCode shr 8) shl 2) // 1 11111 11
    or ((GameCode and $1000) shr 5);
  Bytes[2] := FourToFive(JZCode) or (FourToFive(JZCode shr 4) shl 5);
  // 111 11111
  Bytes[3] := (FourToFive(JZCode shr 4) shr 3) or
    (FourToFive(JZCode shr 8) shl 2) or (FourToFive(JZCode shr 12) shl 7);
  // 1 11111 11
  Bytes[4] := (FourToFive(JZCode shr 12) shr 1) or
    (FourToFive(JZCode shr 16) shl 4); // 01111 111
{$ELSE}
  Move(JZCode, Bytes[0], 3);
  Move(GameCode, Bytes[3], 2);
{$ENDIF}
end;

end.
