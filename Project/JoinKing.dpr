program JoinKing;

uses
  Vcl.Forms,
  WinApi.Windows,
  System.SysUtils,
  Vcl.Dialogs,
  Vcl.Themes,
  Vcl.Styles,
  System.Classes,
  UDlgOrganization in '..\SourceCode\Platform\UDlgOrganization.pas' {FDlgOrganization},
  UDlgUser in '..\SourceCode\Platform\UDlgUser.pas' {FDlgUser},
  UOrganization in '..\SourceCode\Platform\UOrganization.pas' {FOrganization},
  UPlatformConfig in '..\SourceCode\Platform\UPlatformConfig.pas' {FPlatformConfig},
  UPlatformDB in '..\SourceCode\Platform\UPlatformDB.pas' {FPlatformDB: TDataModule},
  UPlatformDBDictionary in '..\SourceCode\Platform\UPlatformDBDictionary.pas' {FPlatformDBDictionary},
  UPlatformDebug in '..\SourceCode\Platform\UPlatformDebug.pas' {FPlatformDebug},
  UPlatformDockForm in '..\SourceCode\Platform\UPlatformDockForm.pas' {FPlatformDockForm},
  UPlatformEnterFormSetting in '..\SourceCode\Platform\UPlatformEnterFormSetting.pas' {FPlatformEnterformSetting},
  UPlatformFlow in '..\SourceCode\Platform\UPlatformFlow.pas' {FPlatformFlow},
  UPlatformLogin in '..\SourceCode\Platform\UPlatformLogin.pas' {FPlatformLogin},
  UPlatformMain in '..\SourceCode\Platform\UPlatformMain.pas' {FPlatformMain1},
  UPlatformMenu1 in '..\SourceCode\Platform\UPlatformMenu1.pas' {FPlatformMenu1},
  UPlatformMenu2 in '..\SourceCode\Platform\UPlatformMenu2.pas' {FPlatformMenu2},
  UPlatformMenu3 in '..\SourceCode\Platform\UPlatformMenu3.pas' {FPlatformMenu3},
  UPlatformMenuBase in '..\SourceCode\Platform\UPlatformMenuBase.pas' {FPlatformMenuBase},
  UPlatformOptions in '..\SourceCode\Platform\UPlatformOptions.pas' {FPlatformOptions},
  UPlatformParams in '..\SourceCode\Platform\UPlatformParams.pas' {FPlatformParams},
  UPlatformRole in '..\SourceCode\Platform\UPlatformRole.pas' {FPlatformRole},
  UPlatformStatusFollow in '..\SourceCode\Platform\UPlatformStatusFollow.pas' {FPlatformStatusFollow},
  UPlatformTachDB in '..\SourceCode\Platform\UPlatformTachDB.pas' {FPlatformTachDB},
  UPlatformUser in '..\SourceCode\Platform\UPlatformUser.pas' {FPlatformUser},
  UPlatformZone in '..\SourceCode\Platform\UPlatformZone.pas' {FPlatformZone},
  USuperObject in '..\SourceCode\Platform\USuperObject.pas',
  UVonCalculator in '..\SourceCode\Platform\UVonCalculator.pas',
  UVonConfig in '..\SourceCode\Platform\UVonConfig.pas',
  UVonConsoleLog in '..\SourceCode\Platform\UVonConsoleLog.pas',
  UVonDBLog in '..\SourceCode\Platform\UVonDBLog.pas',
  UVonEventLog in '..\SourceCode\Platform\UVonEventLog.pas',
  UVonFileLog in '..\SourceCode\Platform\UVonFileLog.pas',
  UVonLog in '..\SourceCode\Platform\UVonLog.pas',
  UVonSystemFuns in '..\SourceCode\Platform\UVonSystemFuns.pas',
  UVonOffice in '..\SourceCode\Imports\UVonOffice.pas',
  UOfficeExport in '..\SourceCode\Imports\UOfficeExport.pas',
  Office_TLB in '..\SourceCode\Imports\Office_TLB.pas',
  OLE_Excel_TLB in '..\SourceCode\Imports\OLE_Excel_TLB.pas',
  OLE_Html_TLB in '..\SourceCode\Imports\OLE_Html_TLB.pas',
  OLE_Office_TLB in '..\SourceCode\Imports\OLE_Office_TLB.pas',
  OLE_VBIDE_TLB in '..\SourceCode\Imports\OLE_VBIDE_TLB.pas',
  OLE_Word_TLB in '..\SourceCode\Imports\OLE_Word_TLB.pas',
  UPlatformInfo in '..\SourceCode\Platform\UPlatformInfo.pas',
  UVonMenuDesigner in '..\SourceCode\Certification\UVonMenuDesigner.pas' {FVonMenuDesigner},
  DCPbase64 in '..\SourceCode\dcpcrypt2-2009\DCPbase64.pas',
  DCPblockciphers in '..\SourceCode\dcpcrypt2-2009\DCPblockciphers.pas',
  DCPconst in '..\SourceCode\dcpcrypt2-2009\DCPconst.pas',
  DCPcrypt2 in '..\SourceCode\dcpcrypt2-2009\DCPcrypt2.pas',
  DCPreg in '..\SourceCode\dcpcrypt2-2009\DCPreg.pas',
  UVonCrypt in '..\SourceCode\dcpcrypt2-2009\UVonCrypt.pas',
  DCPblowfish in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPblowfish.pas',
  DCPcast128 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPcast128.pas',
  DCPcast256 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPcast256.pas',
  DCPdes in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPdes.pas',
  DCPgost in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPgost.pas',
  DCPice in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPice.pas',
  DCPidea in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPidea.pas',
  DCPmars in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPmars.pas',
  DCPmisty1 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPmisty1.pas',
  DCPrc2 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrc2.pas',
  DCPrc4 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrc4.pas',
  DCPrc5 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrc5.pas',
  DCPrc6 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrc6.pas',
  DCPrijndael in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrijndael.pas',
  DCPserpent in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPserpent.pas',
  DCPtea in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPtea.pas',
  DCPtwofish in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPtwofish.pas',
  DCPhaval in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPhaval.pas',
  DCPmd4 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPmd4.pas',
  DCPmd5 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPmd5.pas',
  DCPripemd128 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPripemd128.pas',
  DCPripemd160 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPripemd160.pas',
  DCPsha1 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPsha1.pas',
  DCPsha256 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPsha256.pas',
  DCPsha512 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPsha512.pas',
  DCPtiger in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPtiger.pas',
  UVonMenuInfo in '..\SourceCode\Certification\UVonMenuInfo.pas',
  UFrameLonLat in '..\SourceCode\Components\UFrameLonLat.pas' {FrameLonLat: TFrame},
  UVonTransmissionApp in '..\SourceCode\Communication\UVonTransmissionApp.pas',
  UVonTransmissionBase in '..\SourceCode\Communication\UVonTransmissionBase.pas',
  UVonTransmissionTCP in '..\SourceCode\Communication\UVonTransmissionTCP.pas',
  UFrameGridBar in '..\SourceCode\Components\UFrameGridBar.pas' {FrameGridBar: TFrame},
  UFrameTree in '..\SourceCode\Components\UFrameTree.pas' {FrameTree: TFrame},
  UDlgGridColumn in '..\SourceCode\Components\UDlgGridColumn.pas' {FDlgGridColumn},
  UFrameSettingInputor in '..\SourceCode\Components\UFrameSettingInputor.pas' {FrameSettingInputor: TFrame},
  UFrameImageInput in '..\SourceCode\Components\UFrameImageInput.pas' {FrameImageInput: TFrame},
  UVonGraphicAPI in '..\SourceCode\Imports\UVonGraphicAPI.pas',
  UVonGraphicColor in '..\SourceCode\Imports\UVonGraphicColor.pas',
  UVonGraphicCompression in '..\SourceCode\Imports\UVonGraphicCompression.pas',
  UVonGraphicEx in '..\SourceCode\Imports\UVonGraphicEx.pas',
  UVonGraphicJPG in '..\SourceCode\Imports\UVonGraphicJPG.pas',
  UVonGraphicMZLib in '..\SourceCode\Imports\UVonGraphicMZLib.pas',
  UVonGraphicStrings in '..\SourceCode\Imports\UVonGraphicStrings.pas',
  UDlgCamera in '..\SourceCode\Components\UDlgCamera.pas' {FDlgCamera},
  UVFW in '..\SourceCode\Components\UVFW.pas',
  UFrameRichEditor in '..\SourceCode\Components\UFrameRichEditor.pas' {FrameRichEditor: TFrame},
  UFrameSQL in '..\SourceCode\Components\UFrameSQL.pas' {FrameSql: TFrame},
  UFrameEnterForm in '..\SourceCode\Components\UFrameEnterForm.pas' {FrameEnterForm: TFrame},
  UFrameLayout in '..\SourceCode\Components\UFrameLayout.pas' {FrameLayout: TFrame},
  UVonSinoVoiceAPI in '..\SourceCode\Imports\UVonSinoVoiceAPI.pas',
  JTTS_ACTIVEXLib_TLB in '..\SourceCode\Imports\JTTS_ACTIVEXLib_TLB.pas',
  UDlgConnection in '..\SourceCode\Components\UDlgConnection.pas' {FDlgConnection},
  UFrameViewBar in '..\SourceCode\Components\UFrameViewBar.pas' {FrameViewBar: TFrame},
  UFrameArea in '..\SourceCode\Components\UFrameArea.pas' {FrameArea: TFrame},
  UFrameBannerBase in '..\SourceCode\Components\UFrameBannerBase.pas' {FrameBannerBase: TFrame},
  UFrameBannerDeepBlue in '..\SourceCode\Components\UFrameBannerDeepBlue.pas' {FrameBannerDeepBlue: TFrame},
  UFrameBannerSimple in '..\SourceCode\Components\UFrameBannerSimple.pas' {FrameBannerSimple: TFrame},
  UDlgRichEditor in '..\SourceCode\Components\UDlgRichEditor.pas' {FDlgRichEditor},
  UDlgBrowser in '..\SourceCode\Components\UDlgBrowser.pas' {FDlgBrower},
  UDlgSerialPort in '..\SourceCode\Communication\UDlgSerialPort.pas' {FDlgSerialPort},
  UCommunication in '..\SourceCode\Communication\UCommunication.pas',
  UCoteInfo in 'Pigeon\UCoteInfo.pas',
  UCoteDB in 'Pigeon\UCoteDB.pas' {FCoteDB: TDataModule},
  UCoteComm in 'Pigeon\UCoteComm.pas',
  UCoteMember in 'Pigeon\UCoteMember.pas' {FCoteMember},
  UDlgMember in 'Pigeon\UDlgMember.pas' {FDlgMember},
  UCotePigeon in 'Pigeon\UCotePigeon.pas' {FCotePigeon},
  UCoteMatch in 'Pigeon\UCoteMatch.pas' {FCoteMatch},
  UDlgMatch in 'Pigeon\UDlgMatch.pas' {FDlgMatch},
  UDlgInputGBCode in 'Pigeon\UDlgInputGBCode.pas' {FDlgInputGBCode},
  UDlgGameTime in 'Pigeon\UDlgGameTime.pas' {FDlgGameTime},
  UDlgSunTime in 'Pigeon\UDlgSunTime.pas' {FDlgSunTime},
  UCoteCheckIn in 'Pigeon\UCoteCheckIn.pas' {FCoteCheckIn},
  UJZHRing in 'Factory\UJZHRing.pas' {FJZ_Ring},
  UDlgPackage in 'Pigeon\UDlgPackage.pas' {FDlgPackage},
  UDlgPigeon in 'Pigeon\UDlgPigeon.pas' {FDlgPigeon},
  UCoteTeamSearch in 'Pigeon\UCoteTeamSearch.pas' {FCoteTeamSearch},
  UCoteStageSearch in 'Pigeon\UCoteStageSearch.pas' {FCoteStageSearch},
  UCoteSoueceSearch in 'Pigeon\UCoteSoueceSearch.pas' {FCoteSoueceSearch},
  UCotePigeonSearch in 'Pigeon\UCotePigeonSearch.pas' {FCotePigeonSearch},
  UCoteBackSearch in 'Pigeon\UCoteBackSearch.pas' {FCoteBackSearch},
  UCoteMultiSearch in 'Pigeon\UCoteMultiSearch.pas' {FCoteMultiSearch},
  UCoteCollectionSearch in 'Pigeon\UCoteCollectionSearch.pas' {FCoteCollectionSearch},
  UCoteSoueceStatistic in 'Pigeon\UCoteSoueceStatistic.pas' {FCoteSoueceStatistic},
  UDlgCommunication in '..\SourceCode\Communication\UDlgCommunication.pas' {FDlgCommunication},
  UCommunicationJoinScreen in '..\SourceCode\Communication\UCommunicationJoinScreen.pas',
  UDlgReadRing in 'Pigeon\UDlgReadRing.pas' {FDlgReadRing},
  UCoteGrouping in 'Pigeon\UCoteGrouping.pas' {FCoteGrouping},
  UCoteGroupingSearch in 'Pigeon\UCoteGroupingSearch.pas' {FCoteGroupingSearch},
  UCote in 'Pigeon\UCote.pas' {FCote},
  UDlgCote in 'Pigeon\UDlgCote.pas' {FDlgCote},
  UCoteCollection in 'Pigeon\UCoteCollection.pas' {FCollection},
  UEnvironment in 'Pigeon\UEnvironment.pas',
  UCoteChoose in 'Pigeon\UCoteChoose.pas' {FCoteChoose},
  UCoteRecharge in 'Pigeon\UCoteRecharge.pas' {FCoteRecharge},
  UCoteMain in '荣冠科信\UCoteMain.pas' {FCoteMain},
  UVonCertReader in '..\SourceCode\Certification\UVonCertReader.pas',
  UVonCertReg in '..\SourceCode\Certification\UVonCertReg.pas' {FPlatformReg},
  UPlatformUpgrade in '..\SourceCode\Platform\UPlatformUpgrade.pas' {FPlatFormUpgrade};

//  UCoteHouse in '..\SourceCode\JoinCote\UCoteHouse.pas' {FCoteHouse},
//  UCoteCheckIn in '..\SourceCode\JoinCote\UCoteCheckIn.pas' {FCoteCheckIn},
//  UDlgGameTime in '..\SourceCode\JoinCote\UDlgGameTime.pas' {FDlgGameTime},
//  UDlgSunTime in '..\SourceCode\JoinCote\UDlgSunTime.pas' {FDlgSunTime},
//  UDlgRichEditor in '..\SourceCode\Components\UDlgRichEditor.pas' {FDlgRichEditor},
//  UPlatformEnterForm in '..\SourceCode\Platform\UPlatformEnterForm.pas' {FPlatformEnterForm},
//  UCoteChangeRing in '..\SourceCode\JoinCote\UCoteChangeRing.pas' {FCoteChangeRing},
//  UDlgInputGBCode in '..\SourceCode\JoinCote\UDlgInputGBCode.pas' {FDlgInputGBCode},
//  UCotePigeonSearch in '..\SourceCode\JoinCote\UCotePigeonSearch.pas' {FCotePigeonSearch},
//  UCoteTeamSearch in '..\SourceCode\JoinCote\UCoteTeamSearch.pas' {FCoteTeamSearch},
//  UCoteSerialSearch in '..\SourceCode\JoinCote\UCoteSerialSearch.pas' {FCoteSerialSearch},
//  UCoteSoueceSearch in '..\SourceCode\JoinCote\UCoteSoueceSearch.pas' {FCoteSoueceSearch},
//  UCoteBackSearch in '..\SourceCode\JoinCote\UCoteBackSearch.pas' {FCoteBackSearch},
//  UCoteMultiSearch in '..\SourceCode\JoinCote\UCoteMultiSearch.pas' {FCoteMultiSearch},
//  UCoteCollectionSearch in '..\SourceCode\JoinCote\UCoteCollectionSearch.pas' {FCoteCollectionSearch},
//  UCoteChooseSearch in '..\SourceCode\JoinCote\UCoteChooseSearch.pas' {FCoteChooseSearch},
//  UCoteSoueceStatistic in '..\SourceCode\JoinCote\UCoteSoueceStatistic.pas' {FCoteSoueceStatistic},
//  UDlgReadRing in '..\SourceCode\JoinCote\UDlgReadRing.pas' {FDlgReadRing};

var
  hMutex: THandle;
  Ret: Integer;

{$R *.res}

begin
  CERT_FLAG := #3#5'JAMESVON JOINKING MANAGER CERTIFICATION'#3#5;
  SYS_NAME := 'JOINKING';
  CERT_FILE := 'JOINKING.CERT';
  hMutex := CreateMutex(nil, true, 'JOINKING');
  Ret := GetLastError;
  OpenLog('FILELOG', SYS_NAME, ExtractFilePath(Application.ExeName) + 'LOG\', LOG_DEBUG); // 打开本地日志
  if Ret <> ERROR_ALREADY_EXISTS then
  begin
    Application.Initialize;
  end
  else
  begin
    Application.MessageBox('程序已经运行!', '系统提示', MB_OK);
    ReleaseMutex(hMutex);
    Exit;
  end;
  {$region 'Platform'}
  RegAppCommand('帮助命令', '显示系统帮助信息', TFPlatformHelp, 0, 'HELP');
  RegAppCommand('链接网站', '显示主网站信息', TFPlatformHelp, 0, 'WEB');
  RegAppCommand('关于命令', '显示关于信息', TFPlatformHelp, 0, '');
  RegAppCommand('角色管理', '管理系统角色信息', TFPlatformRole, 0, '');
  RegAppCommand('人员管理', '管理系统用户', TFPlatFormUser, 0, '');
  RegAppCommand('界面设计', '管理系统菜单、按钮、风格样式', TFVonMenuDesigner, 0, '');
  RegAppCommand('系统选项', '管理系统各种参数、选项以及支持信息', TFPlatformOptions, 0, '');
  RegAppCommand('系统配置', '管理系统各种配置项', TFPlatformConfig, 0, '');
  RegAppCommand('系统参数', '系统参数的管理与维护', TFPlatformParams, 0, '');
  RegAppCommand('备份恢复', '备份与恢复系统数据库', TFPlatformTachDB, 0, '');
  RegAppCommand('流程管理', '状态流设计', TFPlatformStatusFollow, 0, '');
  RegAppCommand('查询管理', '通用查询模板管理', TFPlatformQueryManage, 0, '');
  RegAppCommand('查询分析', '通用查询与分析管理模块', TFPlatformQuery, 0, '');
  RegAppCommand('调试工具', '数据库调试工具', TFPlatformDebug, 0, '');
  RegAppCommand('字典管理', '数据库字段字典管理', TFPlatformDBDictionary, 0, '');
  RegAppCommand('自身信息', '管理自身的信息', TFDlgCote, 0, '自身');
  RegAppCommand('组织结构', '管理组织架构信息', TFOrganization, 0, '');
  {$endregion}
  {$region 'Join technolagy'}
  //RegAppCommand('初始化环', '对新生产的环子进行识别处理', TFJZ_InitRing, 0, '');
  RegAppCommand('环库管理', '管理自身环库的录入及发放', TFJZ_Ring, 0, '');
  {$endregion}
  {$region 'Cote manage'}
  RegAppCommand('入棚管理', '管理会员及会员赛鸽的入棚与信息录入', TFCotePigeon, 0, '');
  RegAppCommand('会员管理', '管理会员及会员信息录入', TFCoteMember, 0, '');
//  RegAppCommand('棚舍管理', '全面管理工棚的信鸽饲养棚舍', TFCoteHouse, 0, '');
  RegAppCommand('赛事管理', '对训放和比赛进行管理', TFCoteMatch, 0, '');
  RegAppCommand('分组管理', '对赛鸽分组进行专项管理', TFCoteGrouping, 0, '');
  {$endregion}
  {$region '查询功能'}
  RegAppCommand('入棚查询', '入棚查询', TFCotePigeonSearch, 0, '');
  RegAppCommand('团体查询', '团体查询', TFCoteGroupingSearch, 0, '');
  RegAppCommand('关赛查询', '关赛查询', TFCoteStageSearch, 0, '');
  RegAppCommand('成绩查询', '成绩查询', TFCoteSoueceSearch, 0, '');
  RegAppCommand('赛绩查询', '赛绩查询', TFCoteBackSearch, 0, '');
  RegAppCommand('联赛成绩', '联赛成绩', TFCoteMultiSearch, 0, '');
  RegAppCommand('集鸽查询', '集鸽查询', TFCoteCollectionSearch, 0, '');
//  RegAppCommand('指定鸽查询', '指定鸽查询', TFCoteChooseSearch, 0, '');
  {$endregion}
  {$region '统计功能'}
  RegAppCommand('赛绩统计', '赛绩统计', TFCoteSoueceStatistic, 0, '');
  {$endregion}

  Application.CreateForm(TFCoteDB, FCoteDB);
  Application.CreateForm(TFCoteMain, FCoteMain);
  Application.CreateForm(TFPlatformReg, FPlatformReg);
  Application.CreateForm(TFPlatFormUpgrade, FPlatFormUpgrade);
  //Application.CreateForm(TFCoteChoose, FCoteChoose);
  //Application.CreateForm(TFCoteGroupingSearch, FCoteGroupingSearch);
  //Application.CreateForm(TFCoteStageSearch, FCoteStageSearch);
  FCoteDB.RingKind:= FCoteDB.Cert.CertParams.NameValue['RingKind'];
  //Application.CreateForm(TFCoteGrouping, FCoteGrouping);
  WriteLog(LOG_INFO, 'Logined', FCoteDB.Cert.Values.NameValue['CurrentVersion'] +
    '[' +Get_ApplicationVersion(Application.ExeName) + ']');
  FCoteDB.RegisteChmHelp(FCoteDB.AppPath + 'Cote.CHM');  //加载帮助文件
  if FPlatformDB.CanRun then
  begin
    WriteLog(LOG_DEBUG, 'Application', 'End of inited communication ...... ');
    FCoteDB.InitData();
//    if FCoteDB.RingKind = '只读环' then FCoteDB.InitData(TJZCommR.Create)
//    else if FCoteDB.RingKind = '读写环' then FCoteDB.InitData(TJZCommRW.Create)
//    else if FCoteDB.RingKind = '高频只读' then FCoteDB.InitData(TJZCommR.Create)
//    else if FCoteDB.RingKind = '高频读写' then FCoteDB.InitData(TJZCommRW.Create);
    Application.MainFormOnTaskbar := true;
    Application.Title := FPlatformDB.Cert.Values.NameValue['ApplicationName'];
    Application.CreateForm(TFCoteMain, FCoteMain);
    FEventOnNewChildWin:= FCoteMain.OpenModule;
    TThread.CreateAnonymousThread(JoinServiceMonitor).Start;
    Say('欢迎您使用' + FPlatformDB.Cert.Values.NameValue['ApplicationName']);
  end
  else if GetInfoList.Count <> 0 then
    ShowMessage(GetInfoList.Text);
  WriteLog(LOG_DEBUG, 'Application', 'Application will run ... ...');
  Application.Run;
end.
