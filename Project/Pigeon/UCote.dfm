object FCote: TFCote
  Left = 0
  Top = 0
  Caption = #20844#26842#20449#24687
  ClientHeight = 559
  ClientWidth = 1249
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object plCondition: TPanel
    Left = 0
    Top = 0
    Width = 1249
    Height = 33
    Align = alTop
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    object lblOrgName: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 48
      Height = 25
      Align = alLeft
      Caption = #20844#26842#21517#31216
      Layout = tlCenter
      ExplicitHeight = 13
    end
    object lblRegion: TLabel
      AlignWithMargins = True
      Left = 166
      Top = 4
      Width = 48
      Height = 25
      Align = alLeft
      Caption = #25152#23646#21306#22495
      Layout = tlCenter
      ExplicitHeight = 13
    end
    object lblState: TLabel
      AlignWithMargins = True
      Left = 313
      Top = 4
      Width = 48
      Height = 25
      Align = alLeft
      Caption = #25152#22312#30465#20221
      Layout = tlCenter
      ExplicitHeight = 13
    end
    object lblCity: TLabel
      AlignWithMargins = True
      Left = 448
      Top = 4
      Width = 48
      Height = 25
      Align = alLeft
      Caption = #25152#22312#22320#24066
      Layout = tlCenter
      ExplicitHeight = 13
    end
    object lblAddress: TLabel
      AlignWithMargins = True
      Left = 601
      Top = 4
      Width = 24
      Height = 25
      Align = alLeft
      Caption = #22320#22336
      Layout = tlCenter
      ExplicitHeight = 13
    end
    object lblZip: TLabel
      AlignWithMargins = True
      Left = 693
      Top = 4
      Width = 24
      Height = 25
      Align = alLeft
      Caption = #37038#32534
      Layout = tlCenter
      ExplicitHeight = 13
    end
    object lblContact: TLabel
      AlignWithMargins = True
      Left = 785
      Top = 4
      Width = 48
      Height = 25
      Align = alLeft
      Caption = #32852#31995#26041#24335
      Layout = tlCenter
      ExplicitHeight = 13
    end
    object lblLinker: TLabel
      AlignWithMargins = True
      Left = 901
      Top = 4
      Width = 36
      Height = 25
      Align = alLeft
      Caption = #32852#31995#20154
      Layout = tlCenter
      ExplicitHeight = 13
    end
    object ESOrgName1: TEdit
      AlignWithMargins = True
      Left = 58
      Top = 4
      Width = 102
      Height = 25
      Align = alLeft
      TabOrder = 0
      ExplicitHeight = 21
    end
    object ESAddress1: TEdit
      AlignWithMargins = True
      Left = 631
      Top = 4
      Width = 56
      Height = 25
      Align = alLeft
      TabOrder = 1
      ExplicitHeight = 21
    end
    object ESZip1: TEdit
      AlignWithMargins = True
      Left = 723
      Top = 4
      Width = 56
      Height = 25
      Align = alLeft
      TabOrder = 2
      ExplicitHeight = 21
    end
    object ESContact1: TEdit
      AlignWithMargins = True
      Left = 839
      Top = 4
      Width = 56
      Height = 25
      Align = alLeft
      TabOrder = 3
      ExplicitHeight = 21
    end
    object ESLinker1: TEdit
      AlignWithMargins = True
      Left = 943
      Top = 4
      Width = 56
      Height = 25
      Align = alLeft
      TabOrder = 4
      ExplicitHeight = 21
    end
    object btnSearch: TButton
      AlignWithMargins = True
      Left = 1170
      Top = 4
      Width = 75
      Height = 25
      Align = alRight
      Anchors = [akTop, akRight]
      Caption = #26597#35810
      ImageIndex = 33
      ImageMargins.Left = 5
      Images = FCoteDB.ImgButton
      TabOrder = 5
      OnClick = btnSearchClick
    end
    object btnReset: TButton
      AlignWithMargins = True
      Left = 1089
      Top = 4
      Width = 75
      Height = 25
      Align = alRight
      Anchors = [akTop, akRight]
      Caption = #37325#32622
      ImageIndex = 32
      ImageMargins.Left = 5
      Images = FCoteDB.ImgButton
      TabOrder = 6
      OnClick = btnResetClick
    end
    object ESRegion1: TComboBox
      AlignWithMargins = True
      Left = 220
      Top = 4
      Width = 87
      Height = 21
      Align = alLeft
      Style = csDropDownList
      TabOrder = 7
    end
    object ESState1: TComboBox
      AlignWithMargins = True
      Left = 367
      Top = 4
      Width = 75
      Height = 21
      Align = alLeft
      Style = csDropDownList
      TabOrder = 8
      OnChange = ESState1Change
    end
    object ESCity1: TComboBox
      AlignWithMargins = True
      Left = 502
      Top = 4
      Width = 93
      Height = 21
      Align = alLeft
      TabOrder = 9
    end
  end
  object plBar: TPanel
    Left = 0
    Top = 526
    Width = 1249
    Height = 33
    Align = alBottom
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 1
    object btnOK: TButton
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 75
      Height = 25
      Align = alLeft
      Caption = #36873#25321
      ImageIndex = 19
      ImageMargins.Left = 5
      Images = FCoteDB.ImgButton
      ModalResult = 1
      TabOrder = 3
      Visible = False
    end
    object btnCancel: TButton
      AlignWithMargins = True
      Left = 85
      Top = 4
      Width = 75
      Height = 25
      Align = alLeft
      Caption = #25918#24323
      ImageIndex = 20
      ImageMargins.Left = 5
      Images = FCoteDB.ImgButton
      ModalResult = 2
      TabOrder = 4
      Visible = False
    end
    object btnNew: TButton
      AlignWithMargins = True
      Left = 1008
      Top = 4
      Width = 75
      Height = 25
      Align = alRight
      Anchors = [akTop, akRight]
      Caption = #28155#21152
      ImageIndex = 29
      ImageMargins.Left = 5
      Images = FCoteDB.ImgButton
      TabOrder = 0
      OnClick = btnNewClick
    end
    object btnEdit: TButton
      AlignWithMargins = True
      Left = 1089
      Top = 4
      Width = 75
      Height = 25
      Align = alRight
      Anchors = [akTop, akRight]
      Caption = #20462#25913
      ImageIndex = 26
      ImageMargins.Left = 5
      Images = FCoteDB.ImgButton
      TabOrder = 1
      OnClick = btnEditClick
    end
    object btnDelete: TButton
      AlignWithMargins = True
      Left = 1170
      Top = 4
      Width = 75
      Height = 25
      Align = alRight
      Anchors = [akTop, akRight]
      Caption = #21024#38500
      ImageIndex = 20
      ImageMargins.Left = 5
      Images = FCoteDB.ImgButton
      TabOrder = 2
      OnClick = btnDeleteClick
    end
  end
  inline GridCote: TFrameGridBar
    Left = 0
    Top = 33
    Width = 1249
    Height = 493
    HelpType = htKeyword
    Align = alClient
    TabOrder = 2
    ExplicitTop = 33
    ExplicitWidth = 1249
    ExplicitHeight = 493
    inherited split1: TSplitter
      Left = 1061
      Height = 472
      ExplicitLeft = 1061
      ExplicitHeight = 472
    end
    inherited Grid: TDBGrid
      Width = 1061
      Height = 472
    end
    inherited StatusBar1: TStatusBar
      Width = 1249
      ExplicitWidth = 1249
    end
    inherited plItem: TPanel
      Left = 1064
      Height = 472
      ExplicitLeft = 1064
      ExplicitHeight = 472
      inherited lstLegend: TColorListBox
        ExplicitLeft = 1
        ExplicitTop = 1
      end
      inherited lstField: TValueListEditor
        Height = 373
        ExplicitHeight = 373
      end
    end
    inherited GridSource: TDataSource
      DataSet = DQCote
    end
  end
  object DQCote: TADOQuery
    Connection = FCoteDB.ADOConn
    Parameters = <>
    SQL.Strings = (
      
        'SELECT [ID],[OrgName],[Region],[State],[City],[Address],[Zip],[C' +
        'ontact],[Linker],[Longitude],[Latitude],[Status],[PWD] FROM JOIN' +
        '_Cote')
    Left = 264
    Top = 176
  end
end
