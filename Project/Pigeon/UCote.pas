﻿(**************************************************************
* Copyright:      Jamesvon  mailto:Jamesvon@163.COM
* Author:         James von
* Remarks:        JOIN project files, Copyright by jamesvon
* known Problems: none
* Version:        1.0
* Description:    公棚信息 主程序
* Include:        
*=============================================================*
* TFCote description                           
*-------------------------------------------------------------*
*
**************************************************************)

unit UCote;

interface

uses
  WinAPI.Windows, WinAPI.Messages, SysUtils, Variants, Classes, Graphics, 
  Controls, Forms, Dialogs, StdCtrls, Spin, ExtCtrls, DB, Data.Win.ADODB, 
  UVonLog, UFrameGridBar, UVonConfig, UVonSystemFuns, UCoteInfo,
  UCoteDB, UDlgCote;

type
  TFCote = class(TForm)
    plCondition: TPanel;
    lblOrgName: TLabel;
    ESOrgName1: TEdit;
    lblRegion: TLabel;
    lblState: TLabel;
    lblCity: TLabel;
    lblAddress: TLabel;
    ESAddress1: TEdit;
    lblZip: TLabel;
    ESZip1: TEdit;
    lblContact: TLabel;
    ESContact1: TEdit;
    lblLinker: TLabel;
    ESLinker1: TEdit;
    DQCote: TADOQuery;
    plBar: TPanel;
    GridCote: TFrameGridBar;
    btnSearch: TButton;
    btnReset: TButton;
    ESRegion1: TComboBox;
    ESState1: TComboBox;
    ESCity1: TComboBox;
    procedure btnResetClick(Sender: TObject);
    procedure btnSearchClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure ESState1Change(Sender: TObject);
  private
    { Private declarations }
    FInfo: TCoteInfo;
  public
    { Public declarations }
  end;

var
  FCote: TFCote;

implementation

{$R *.dfm}

procedure TFCote.FormCreate(Sender: TObject);
begin
  FInfo:= TCoteInfo.Create;
  lblRegion.Caption:= FCoteDB.GetDropList('区域', ESRegion1);
  if ESRegion1.Items.Count = 0 then
    ESRegion1.Items.Text:= '华北'#13#10'西北'#13#10'华中'#13#10'东北';
  ESRegion1.ItemIndex:= 0;
  FCoteDB.GetSqlDropList('SELECT ID,ItemName FROM SYS_Zone WHERE PID=0', ESState1);
end;

procedure TFCote.FormDestroy(Sender: TObject);
begin
  FInfo.Free;
end;

procedure TFCote.btnResetClick(Sender: TObject);
begin
  ESOrgName1.Text:= '';
  ESRegion1.ItemIndex:= 0;
  ESState1.ItemIndex:= 0;
  ESCity1.ItemIndex:= 0;
  ESAddress1.Text:= '';
  ESZip1.Text:= '';
  ESContact1.Text:= '';
  ESLinker1.Text:= '';
end;

procedure TFCote.btnSearchClick(Sender: TObject);
begin
  with DQCote do begin
    Close;
    SQL.Text:= 'SELECT [ID],[OrgName],[Region],[State],[City],[Address],[Zip],' +
      '[Contact],[Linker],[Longitude],[Latitude],[Status],[PWD] FROM ' + Prefix
      + 'Cote';
    if ESOrgName1.Text <> ''then SQL.Add('AND OrgName=''' + ESOrgName1.Text + '''');
    if ESRegion1.Text <> ''then SQL.Add('AND Region=''' + ESRegion1.Text + '''');
    if ESState1.Text <> ''then SQL.Add('AND State=''' + ESState1.Text + '''');
    if ESCity1.Text <> ''then SQL.Add('AND City=''' + ESCity1.Text + '''');
    if ESAddress1.Text <> ''then SQL.Add('AND Address=''' + ESAddress1.Text + '''');
    if ESZip1.Text <> ''then SQL.Add('AND Zip=''' + ESZip1.Text + '''');
    if ESContact1.Text <> ''then SQL.Add('AND Contact=''' + ESContact1.Text + '''');
    if ESLinker1.Text <> ''then SQL.Add('AND Linker=''' + ESLinker1.Text + '''');
    if SQL.Count > 1 then SQL[1]:= 'WHERE ' + Copy(SQL[1], 5, MaxInt);
    Open;
  end;
end;

procedure TFCote.ESState1Change(Sender: TObject);
begin
  if ESState1.ItemIndex >= 0 then
    FCoteDB.GetSqlDropList('SELECT ID,ItemName FROM SYS_Zone WHERE PID=' +
      IntToStr(Integer(ESState1.Items.Objects[ESState1.ItemIndex])), ESCity1, '');
end;

procedure TFCote.btnNewClick(Sender: TObject);
begin
  with TFDlgCote.Create(nil) do try
    while ShowModal = mrOK do try
      FormToInfo(FInfo);
      FInfo.ID:= GetNewGuid();
      FInfo.AppendToDB(DQCote);
      Exit;
    except
      on E: Exception do begin
        DlgInfo('错误', E.Message);
        DQCote.Cancel;
      end;
    end;
  finally
    Free;
  end;
end;

procedure TFCote.btnEditClick(Sender: TObject);
begin
  with TFDlgCote.Create(nil) do try
    FInfo.LoadFromDB(DQCote);
    InfoToForm(FInfo);
    while ShowModal = mrOK do try
      FormToInfo(FInfo);
      FInfo.UpdateToDB(DQCote);
      Exit;
    except
      on E: Exception do begin
        DlgInfo('错误', E.Message);
        DQCote.Cancel;
      end;
    end;
  finally
    Free;
  end;
end;

procedure TFCote.btnDeleteClick(Sender: TObject);
begin
  if DQCote.RecordCount = 0 then
    raise Exception.Create(RES_NODE_NONESELECTED);
  if MessageDlg(RES_DLG_DEL_PROMPT, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  if MessageDlg(RES_DLG_DEL_WARNING, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  DQCote.Delete;
end;

end.

