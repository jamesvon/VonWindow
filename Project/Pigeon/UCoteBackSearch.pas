unit UCoteBackSearch;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, DB, ADODB, StdCtrls, Spin, ComCtrls, ToolWin, Grids,
  DBGrids, Buttons, ExtCtrls, UFrameGridBar, System.ImageList;

type
  TFCoteBackSearch = class(TForm)
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    DBGrid2: TDBGrid;
    DBMatch: TADOTable;
    DQNoBack: TADOQuery;
    DSGame: TDataSource;
    DQBack: TADOQuery;
    ImageList1: TImageList;
    FrameGridBar1: TFrameGridBar;
    FrameGridBar2: TFrameGridBar;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    FGameID: TGuid;
  public
    { Public declarations }
  end;

var
  FCoteBackSearch: TFCoteBackSearch;

implementation

uses UCoteDB;

{$R *.dfm}

procedure TFCoteBackSearch.BitBtn1Click(Sender: TObject);
var
  SQLText: string;
begin
  SQLText := '';
  FGameID := DBMatch.FieldByName('ID').AsGuid;
  with DQNoBack do
  begin
    Close;
    Parameters[0].Value := GuidToString(FGameID);
    Open;
    FrameGridBar1.StatusBar1.Panels[1].Text := '未归巢数量为：' + IntToStr(RecordCount);
  end;
  with DQBack do
  begin
    Close;
    Parameters[0].Value := GuidToString(FGameID);
    Open;
    FrameGridBar2.StatusBar1.Panels[1].Text := '已归巢数量为：' + IntToStr(RecordCount);
  end;
end;

procedure TFCoteBackSearch.FormCreate(Sender: TObject);
begin
  DBMatch.Open;
  FrameGridBar1.LoadTitles('NoBackPigeons');
  FrameGridBar2.LoadTitles('BackedPigeons');
end;

end.
