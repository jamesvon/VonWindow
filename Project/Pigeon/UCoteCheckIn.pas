unit UCoteCheckIn;
(*----------------------------------------------------------------
// Copyright (C) 2013 by jamesvon All rights reserved.
// 冯驰军 版权所有，联系邮箱jamesvon@163.com。
//
// 文件功能描述：公棚归巢
// 创建时间：2017/1/10 14:56:32
//
// 时间：2017年9月27日
// 修改说明：修改了自动归巢打印，增加了 BAT 打印处理
//----------------------------------------------------------------*)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, InvokeRegistry, Menus, Rio, SOAPHTTPClient, ADODB, DB, ImgList,
  ExtCtrls, ComCtrls, StdCtrls, Buttons, ToolWin, UVonSystemFuns, UCoteInfo,
  UCoteDB, UCoteComm, ZLib, Math, UVonLog, IniFiles, UVonConfig,
  ShellApi, UDlgCamera, System.ImageList, Vcl.Samples.Spin, Vcl.Grids,
  System.Net.URLClient;

resourcestring
  RES_SUM_CHECKIN = '共归巢 %d 羽。';

type
  /// <summary>自动集鸽文件内容信息</summary>
  TJZBackInfo = record
    ID: Int64;
    BackTime: Extended;
  end;

  TSourceLinker = class
    sourceData: TSourceView;
    tryCount: Integer;
    next: TSourceLinker;
  end;



  TFCoteCheckIn = class(TForm)
    Splitter1: TSplitter;
    StatusBar1: TStatusBar;
    DisplayTimer: TTimer;
    ImageList1: TImageList;
    PrinterSetupDialog1: TPrinterSetupDialog;
    TimeScreenBreak: TTimer;
    HTTPRIO1: THTTPRIO;
    PopupMenu1: TPopupMenu;
    menuReupload: TMenuItem;
    DQMemberInfo: TADOQuery;
    plSourceList: TPanel;
    Panel1: TPanel;
    btnScreenSW: TSpeedButton;
    btnPrintSW: TSpeedButton;
    lbCount: TLabel;
    btnSmsSW: TSpeedButton;
    btnVideo: TSpeedButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    LName: TLabel;
    LGameAddress: TLabel;
    LManager: TLabel;
    LGameLong: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    btnReconnect: TSpeedButton;
    Label11: TLabel;
    LFlyDate: TLabel;
    Label8: TLabel;
    Bevel1: TBevel;
    Bevel2: TBevel;
    EComm: TComboBox;
    ECommRate: TComboBox;
    ETopCount: TSpinEdit;
    Label3: TLabel;
    ScreenSourceView: TStringGrid;
    DQTest: TADOQuery;
    DataSource1: TDataSource;
    SourceView: TStringGrid;
    rbNomal: TRadioButton;
    rbScreen: TRadioButton;
    rbGreen: TRadioButton;
    btnUpload: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnReconnectClick(Sender: TObject);
    procedure DisplayTimerTimer(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure menuReuploadClick(Sender: TObject);
    procedure btnVideoClick(Sender: TObject);
    procedure ETopCountChange(Sender: TObject);
    procedure SourceViewMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure rbNomalClick(Sender: TObject);
    procedure rbScreenClick(Sender: TObject);
    procedure rbGreenClick(Sender: TObject);
    procedure btnSmsSWClick(Sender: TObject);
    procedure btnUploadClick(Sender: TObject);
  private
    { Private declarations }
    FComm: TJZCommBase;
    FMatchInfo: TMatchInfo;
    FSynFlag : char;
    FSmsFlag : char;
    FViewList: TFIFO;
    FDisplayTitles, FPrintTitles, FSMSTitles, FScreenTitles: TStringList;
    FPrinter: string;
    FCanOrder: Boolean;
    FCurrentGame: string;
    FCurrentOrder, FPrintCount, FPageLineCount: Integer;
    FCurrentFile: Integer;
    FLasterTimer: double;
    FPageCurrentLine: Integer;
    FCntCheckInFeather: Integer;
    FBgnCheckInFeather: Integer;
    FCrrCheckInFeather: Integer;
    FSmsMsgFormat: string;
    FSources: TFIFO;
    function BeginToRead(CommPort: Integer; CommRate: Integer): Boolean;
    procedure DisplayACheckIn(AView: TStringGrid; AData: TSourceView);
    procedure PrintACheckIn(FData: TSourceView);
//    function SmsACheckIn(FData: TSourceView): Boolean;
    procedure EMicroMsgACheckIn(FData: TSourceView);
    function CalcFlyTime(ReportTime, SunRaise, SunDown,
      FlyDate: TDatetime): Extended;
  public
    { Public declarations }
    procedure Start(MatchID: TGuid; CommPort: Integer; CommRate: Integer);
    procedure EventOfRead(JZCode: Int64; GameCode: Cardinal; FlyTime: double);
  end;

var
  FCoteCheckIn: TFCoteCheckIn;

implementation

uses DateUtils, strUtils;

var
  SMSInterval : Cardinal;

{$R *.dfm}

{ TFCoteCheckIn }

procedure TFCoteCheckIn.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := DlgInfo('警告', '是否真的中断比赛', mbYesNo) = mrYes;
end;

procedure TFCoteCheckIn.FormCreate(Sender: TObject);
var
  i, mpos, w: Integer;
  S: string;
  function initConfig(cfgName: string): TStringList;
  begin
    Result:= TStringList.Create;
    Result.Delimiter := ',';           // FieldName=Title:width
    Result.DelimitedText := ReadAppConfig('FIELDS', cfgName);
  end;
  procedure initView(AView: TStringGrid; Col: Integer; ATitle: string; AWidth: Integer);
  begin
    AView.ColWidths[Col]:= AWidth;
    AView.Cells[Col, 0]:= ATitle;
  end;
begin
  FMatchInfo := TMatchInfo.Create;
  FViewList:= TFIFO.Create;

  FDisplayTitles := initConfig('CheckInDisplayFields');
  FPrintTitles := initConfig('CheckInPrintFields');
  FSMSTitles := initConfig('CheckInSMSFields');
  FScreenTitles := initConfig('CheckInScreenFields');

  FSmsMsgFormat:= ReadAppConfig('SYSTEM', 'SMSCheckInMsg');
  FPrinter:= ReadAppConfig('SYSTEM', 'Printer');
  FPageLineCount := StrToInt(ReadAppConfig('SYSTEM', 'PrintCountPerPage'));
  ETopCount.Value:= StrToInt(ReadAppConfig('SYSTEM', 'CheckInTopCount'));
  //Display,SMS,Print,Screen
  DisplayTimer.Interval:= StrToInt(ReadAppConfig('SYSTEM', 'CheckInDisplyTimer'));

  FSmsMsgFormat := ReadAppConfig('SYSTEM', 'SMSCheckInMsg');
  ScreenSourceView.ColCount:= FDisplayTitles.Count;
  SourceView.ColCount:= FDisplayTitles.Count;
  ScreenSourceView.RowCount:= 2;
  SourceView.RowCount:= 2;
  for i := 0 to FDisplayTitles.Count - 1 do
  begin
    S:= FDisplayTitles.ValueFromIndex[I]; mpos:= Pos(':', S);
    W:= StrToInt(Copy(S, mPos + 1, MaxInt)); S:= Copy(S, 1, mPos - 1);
    initView(ScreenSourceView, i, S, W);
    initView(SourceView, i, S, W);
  end;
  WriteLog(LOG_DEBUG, 'CoteCheckIn', 'Created ... ');
  FPageCurrentLine := 0;
end;

procedure TFCoteCheckIn.FormDestroy(Sender: TObject);
begin
  if Assigned(FComm) then FCoteDB.CloseComm(FComm);

  FPrintTitles.Free;
  FDisplayTitles.Free;
  FScreenTitles.Free;
  FSmsTitles.Free;

  FViewList.Free;
  FMatchInfo.Free;
  //FCoteDB.SetComm(nil, nil, nil, true);
end;

procedure TFCoteCheckIn.Start(MatchID: TGuid; CommPort: Integer; CommRate: Integer);
var
  C: Integer;
  Q: TADOQuery;
  FSourceView: TSourceView;
begin     // 开启集鸽
  WriteLog(LOG_DEBUG, 'CoteCheckIn', 'Start to check in match of ' + GuidToString(MatchID));
  if not FMatchInfo.LoadData(FCoteDB.ADOConn, MatchID) then
    raise Exception.Create(Format(RES_Invalid_INFO, [LastInfo]));
  EComm.ItemIndex := CommPort - 1;
  ECommRate.ItemIndex := ECommRate.Items.IndexOf(IntToStr(CommRate));
  LName.Caption := FMatchInfo.MatchName;
  LGameAddress.Caption := FMatchInfo.GameAddress;
  LManager.Caption := FMatchInfo.Manager;
  LGameLong.Caption := FloatToStr(FMatchInfo.GameLong);
  btnUpload.Down := FMatchInfo.UploadSetting and 7 > 0;
  btnSmsSWClick(nil);
  LFlyDate.Caption := DateTimeToStr(FMatchInfo.FlyDate);
  FCurrentOrder := 0; FPrintCount:= 0;
  Q:= TSourceView.OpenQuery(nil, 'GameIdx=''' + GuidToString(MatchID) + '''AND SourceOrder>0 ORDER BY SourceOrder');
  FLasterTimer:= 0;
  FSourceView:= TSourceView.Create;
  with Q do   // 显示已归巢的信鸽信息
    try
      Open;
      if RecordCount > 0 then SourceView.RowCount:= RecordCount + 1;
      while not EOF do
      begin
        Inc(FCurrentOrder);
        FSourceView.LoadFromDB(Q);
        for C := 0 to FDisplayTitles.Count - 1 do
          SourceView.Cells[C, SourceView.RowCount - FCurrentOrder]:= FSourceView.Values[FDisplayTitles.Names[C]];
        SourceView.Objects[0, SourceView.RowCount - FCurrentOrder]:= TObject(FSourceView.ID);
        if RecordCount - FCurrentOrder < 10 then DisplayACheckIn(ScreenSourceView, FSourceView);
        FLasterTimer:= FSourceView.ReportTime;
        Next;
      end;
    finally
      Close;
      Q.Free;
      FSourceView.Free;
      lbCount.Caption := Format(RES_SUM_CHECKIN, [FCurrentOrder]);
    end;
  DisplayTimer.Enabled := True;
  // Begin checkin
  if not BeginToRead(CommPort, CommRate) then Exit;
  WriteLog(LOG_DEBUG, 'CoteCheckIn', 'Ready to check in match of ' + GuidToString(MatchID));
end;

function TFCoteCheckIn.BeginToRead(CommPort: Integer; CommRate: Integer): Boolean;
begin
  FComm:= FCoteDB.CreateComm(nil, EventOfRead, nil, true);
  FComm.BoardCount := StrToInt(ReadAppConfig('COMMUNICATION', 'BoardCount'));
  FComm.Open(CommPort, CommRate, 'n', 8, 1);
  Result:= FComm.Connected;
  if not Result then begin
    FCoteDB.CloseComm(FComm);
    btnReconnect.Down:= False;
    raise Exception.Create('通讯端口未能打开，可能由于其他窗口或界面占用该端口，请检查，目前不能进行归巢，可以通过手动进行重试。');
  end;
  btnReconnect.Down:= Result;
end;

procedure TFCoteCheckIn.btnReconnectClick(Sender: TObject);
begin
  if btnReconnect.Down then begin
    if not BeginToRead(EComm.ItemIndex + 1, StrToInt(ECommRate.Text))then begin
      Exit;
    end;
    WriteAppConfig('COMMUNICATION', 'Port', EComm.Text);
    WriteAppConfig('COMMUNICATION', 'Speed', ECommRate.Text);
  end else FCoteDB.CloseComm(FComm);
end;

procedure TFCoteCheckIn.btnSmsSWClick(Sender: TObject);
begin
  if btnSmsSW.Down then begin
    FSmsFlag:= 'U';
    FSynFlag:= 'U';
  end
  else begin
    FSmsFlag:= ' ';
    btnUploadClick(nil);
  end;
end;

procedure TFCoteCheckIn.btnUploadClick(Sender: TObject);
begin
  if btnUpload.Down then FSynFlag:= 'U' else FSynFlag:= 'O';
end;

(* SMS,Display,Print,Screen *)

function TFCoteCheckIn.CalcFlyTime(ReportTime, SunRaise, SunDown,
  FlyDate: TDatetime): Extended;
var
  CalcTime: TDatetime;
begin
  //计算出有效归巢时间 CalcTime
  if(TimeOf(ReportTime) < SunRaise) then      //如果早于日出时间，归巢时间是前一天+日落时间
    CalcTime:= DateOf(ReportTime) - 1 + SunDown
  else if(TimeOf(ReportTime) > SunDown) then  //如果晚于日落时间，归巢时间是当天+日落时间
    CalcTime:= DateOf(ReportTime) + SunDown
  else CalcTime:= ReportTime;        //正常归巢的飞行时间
  //有效飞行时间 = (有效归巢时间 - 开笼时间)*1400 - (当日无效时间，即日出-日落)*60*(有效归巢时间 和 开笼时间 的日期差)
  Result:= (CalcTime - FlyDate) * 1440 - (SunRaise + 1 - SunDown) * 1440 * (DateOf(CalcTime) - DateOf(FlyDate));
end;

procedure TFCoteCheckIn.EventOfRead(JZCode: int64; GameCode: Cardinal; FlyTime: double);
var
  I: Integer;
  FData: TSourceView;
  CalcTime, Speed: Extended;
  Q: TADOQuery;
begin
  // 修改逻辑关系，先修改归巢时间，再存储排名  FBackList 的结构要进行修改
  if JZCode = 0 then begin
    WriteLog(LOG_INFO, '归巢', Format('非荣冠，放弃%x-%x', [GameCode, JZCode]));
    Exit;
  end;
  WriteLog(LOG_INFO, 'EventOfRead', Format('GC=%x, JZ=%x，%f', [GameCode, JZCode, FlyTime]));
  FData:= TSourceView.Create;
  try
    Q:= TSourceView.OpenQuery(nil, 'GameIdx=''' + GuidToString(FMatchInfo.ID) + ''' AND JZCode=' + IntToStr(JZCode));
    WriteLog(LOG_INFO, 'EventOfRead', Format('Find %d pigeons.', [Q.RecordCount]));
  except
    on E: Exception do begin
      WriteLog(LOG_DEBUG, '归巢', '生成检查集鸽记录查询失败：' + E.Message);
      Exit;
    end;
  end;
  if not FData.LoadFromDB(Q)then
    WriteLog(LOG_MAJOR, '归巢', Format('未集鸽，放弃%x-%x', [GameCode, JZCode]))
  else if FData.SourceOrder > 0 then
    WriteLog(LOG_MAJOR, '归巢', Format('已经归巢，放弃%x-%x', [GameCode, JZCode]))
  else if(FData.GameCode <> GameCode)and((FCoteDB.RingKind = '读写环')or(FCoteDB.RingKind = '高频读写'))and(FMatchInfo.Kind = '比赛') then
    WriteLog(LOG_MAJOR, '归巢', Format('赛事码验证失败，放弃%x-%x', [GameCode, JZCode]))
  else begin
    WriteLog(LOG_DEBUG, '归巢', Format('准备归巢，%x-%x，%f', [GameCode, JZCode, FlyTime]));
    if FCurrentOrder = 0 then
    begin
      FMatchInfo.BeginMatch(FCoteDB.ADOConn);
    end;
    FData.SourceOrder:= FCurrentOrder + 1;
    if FLasterTimer > FlyTime then
      FlyTime:= IncMilliSecond(FLasterTimer, RandomRange(10, 50));
    FLasterTimer:= FlyTime;
    FData.ReportTime:= FlyTime;
    FData.FlyTime:= CalcFlyTime(FlyTime, FMatchInfo.SunRaise, FMatchInfo.SunDown, FMatchInfo.FlyDate);
    FData.Speed:= FMatchInfo.GameLong * 1000 / FData.FlyTime;
    if not FData.CheckIn(FCoteDB.ADOConn, FSmsFlag, FSynFlag)then Exit;
    WriteLog(LOG_DEBUG, '归巢', '归巢缓存赛绩信息....');   ;
    FViewList.Push(TFIFOItem.Create(FData, 0));
    WriteLog(LOG_MAJOR, 'CheckIn', Format('CheckIn %s(%x-%x) and fly %f',
      [FData.GBCode, JZCode, GameCode, FData.FlyTime]));
    lbCount.Caption := Format(RES_SUM_CHECKIN, [FCurrentOrder + 1]);
    Inc(FCurrentOrder);
  end;
  Q.Free;
end;

//function TFCoteCheckIn.SmsACheckIn(FData: TSourceView): Boolean;
//var
//  I: Integer;
//  smsMsg: Array of TVarRec;
//  S: string;
//begin
//  SetLength(smsMsg, FSmsTitles.Count);             S:= '';
//  for I := 0 to FSmsTitles.Count - 1 do begin
//    smsMsg[I].VType:= vtString;
//    New(smsMsg[I].vString);
//    smsMsg[I].VString^:= FData.Values[FSmsTitles.Names[I]];
//    S:= S + ',' + IntToStr(I) + ':' + FSmsTitles.Names[I] + '=' + smsMsg[I].VString^;
//  end;
//  WriteLog(LOG_DEBUG, 'SmsACheckIn -> ' + FSmsMsgFormat, S);
//  Result:= FCoteDB.SendSMS(FData.Mobile, Format(FSmsMsgFormat, smsMsg));
//  SetLength(smsMsg, 0);
//end;

procedure TFCoteCheckIn.SourceViewMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  i: Integer;
begin
  for I := 0 to SourceView.ColCount - 1 do
    ScreenSourceView.ColWidths[I]:= SourceView.ColWidths[I];
end;

procedure TFCoteCheckIn.ETopCountChange(Sender: TObject);
begin
  ScreenSourceView.Height:= ETopCount.Value * 22;
end;

procedure TFCoteCheckIn.DisplayACheckIn(AView: TStringGrid; AData: TSourceView);
var
  R, C: Integer;
begin
  AView.BeginUpdate;
  if Integer(AView.Objects[0, 1]) > 0 then begin
    AView.RowCount:= AView.RowCount + 1;
    for R := AView.RowCount - 1 downto 2 do begin
      AView.Objects[0, R]:= AView.Objects[0, R - 1];
      for C := 0 to AView.ColCount - 1 do
        AView.Cells[C, R]:= AView.Cells[C, R - 1];
    end;
  end;
  for C := 0 to FDisplayTitles.Count - 1 do
    AView.Cells[C, 1]:= AData.Values[FDisplayTitles.Names[C]];
  AView.Objects[0, 1]:= TObject(AData.ID);
  AView.EndUpdate;
end;

procedure TFCoteCheckIn.EMicroMsgACheckIn(FData: TSourceView);
begin

end;

(* 短信发送与开启 *)

//procedure SMSSender(client : TFCoteCheckIn); stdcall;
//var
//  crt: TFIFOItem;
//begin                               //FSMSFirst, FSMSLast
//  while client.btnSmsSW.Down do begin
//    crt := client.FSMSList.Pull;
//    if Assigned(crt)then begin
//      if not client.SmsACheckIn(TSourceView(crt.Obj)) then begin
//        crt.Data:= crt.Data + 1;
//        if crt.Data < 1 then begin
//          client.FSMSList.Push(crt);
//          Continue;
//        end;
//      end;
//      crt.Obj.Free;
//      crt.free;
//    end;
//    Sleep(SMSInterval);
//  end;
//end;
//
//procedure TFCoteCheckIn.btnSmsSWClick(Sender: TObject);
//var
//  FThreadID: DWORD;
//begin
//  if btnSmsSW.Down then begin
//    CreateThread(nil, 0, @SMSSender, self, 0, FThreadID);
//  end else begin
//    Sleep(SMSInterval + 500);
//  end;
//end;

(* 打印成绩单 *)

procedure TFCoteCheckIn.PrintACheckIn(FData: TSourceView);
var
  szLine, szStr: string;
  I, idx, szWidth: Integer;

  function FixWidthStr(Str: AnsiString; FixWidth: Integer): string;
  begin
    if Length(Str) > FixWidth then
      Result := Copy(Str, 1, FixWidth)
    else
      Result := Space((FixWidth - Length(Str)) div 2) + Str +
        Space((FixWidth - Length(Str) + 1) div 2);
  end;
begin
  if (FCurrentOrder - 1)mod FPageLineCount = 0 then
  begin
    printline(FPrinter, '赛事(' + FMatchInfo.MatchName + ')成绩   放飞地：' +
      FMatchInfo.GameAddress + '      司放人：' + FMatchInfo.Manager);
    printline(FPrinter, '赛事距离：' + FloatToStr(FMatchInfo.GameLong) +
      '      开笼时间：' + DateToStr(FMatchInfo.FlyDate));
    szLine:= '';
    for I := 0 to FPrintTitles.Count - 1 do
    begin
      idx:= pos(':', FPrintTitles.ValueFromIndex[I]);
      szWidth:= StrToInt(Copy(FPrintTitles.ValueFromIndex[I], idx + 1, MaxInt));
      szLine := szLine + '+' + Space(szWidth, '-');
      szStr := szStr + '|' + FixWidthStr(Copy(FPrintTitles.ValueFromIndex[I], 1, idx - 1), szWidth);
    end;
    printline(FPrinter, szLine);
    printline(FPrinter, szStr);
    printline(FPrinter, szLine);
  end;
  szStr:= '';
  for I := 0 to FPrintTitles.Count - 1 do
  begin
    idx:= pos(':', FPrintTitles.ValueFromIndex[I]);
    szWidth:= StrToInt(Copy(FPrintTitles.ValueFromIndex[I], idx + 1, MaxInt));
    szStr := szStr + '|' + FixWidthStr(FData.Values[FPrintTitles.Names[I]], szWidth);
  end;
  printline(FPrinter, szStr);
end;

procedure TFCoteCheckIn.rbGreenClick(Sender: TObject);
begin
  ScreenSourceView.Color := clBlack;
  ScreenSourceView.Font.Style := [];
  ScreenSourceView.Font.Color := clRed;
  ScreenSourceView.Font.Size := 10;
end;

procedure TFCoteCheckIn.rbNomalClick(Sender: TObject);
begin
  if rbNomal.Checked then begin
    ScreenSourceView.ParentColor := True;
    ScreenSourceView.Font.Color := SourceView.Font.Color;
    ScreenSourceView.Font.Style := SourceView.Font.Style;
    ScreenSourceView.Font.Name := SourceView.Font.Name;
    ScreenSourceView.Font.Size := SourceView.Font.Size;
    ScreenSourceView.DefaultRowHeight := SourceView.DefaultRowHeight;
  end;
end;

procedure TFCoteCheckIn.rbScreenClick(Sender: TObject);
begin
  if rbScreen.Checked then
    with TIniFile.Create(FCoteDB.ConfigFilename) do
      try
        ScreenSourceView.Font.Style := [];
        if ReadBool('SCREEN', 'Bold', false) then
          ScreenSourceView.Font.Style := ScreenSourceView.Font.Style + [fsBold];
        if ReadBool('SCREEN', 'Italic', false) then
          ScreenSourceView.Font.Style := ScreenSourceView.Font.Style + [fsItalic];
        if ReadBool('SCREEN', 'Underline', false) then
          ScreenSourceView.Font.Style := ScreenSourceView.Font.Style + [fsUnderline];
        if ReadBool('SCREEN', 'StrikeOut', false) then
          ScreenSourceView.Font.Style := ScreenSourceView.Font.Style + [fsStrikeOut];
        ScreenSourceView.Font.Name := ReadString('SCREEN', 'FontName', '宋体');
        ScreenSourceView.Font.Size := ReadInteger('SCREEN', 'FontSize', 9);
        ScreenSourceView.Font.Color := ReadInteger('SCREEN', 'FontColor', clRed);
        ScreenSourceView.Color := ReadInteger('SCREEN', 'Color', clRed);
        ScreenSourceView.DefaultRowHeight := ABS(ScreenSourceView.Font.Height) + 5;
      finally
        Free;
      end;
end;

{ Receiver and memory }

procedure TFCoteCheckIn.menuReuploadClick(Sender: TObject);
var
  ID: Integer;
begin
  if SourceView.Row < 1 then Exit;
  ID:= Integer(SourceView.Objects[0, SourceView.Row]);
  if ID = 0 then Exit;
  FCoteDB.ADOConn.Execute('UPDATE JOIN_GameSource SET SynFlag=''U'' WHERE ID=' +
    IntToStr(ID));
end;

procedure TFCoteCheckIn.btnVideoClick(Sender: TObject);
begin
  DlgCameraOpen();
  FDlgCamera.IniFilename := FCoteDB.ConfigFilename;
  FDlgCamera.Caption:= '公示屏';
end;

procedure TFCoteCheckIn.DisplayTimerTimer(Sender: TObject);
var
  item: TFIFOItem;
begin
  item:= FViewList.Pull;
  if(not Assigned(item))or(not Assigned(item.Obj)) then Exit;
  try
    //即时显示到归巢信息中
    DisplayACheckIn(ScreenSourceView, TSourceView(item.Obj));
    DisplayACheckIn(SourceView, TSourceView(item.Obj));
    //if btnScreenSW.Down then FScreenList.Push(TFIFOItem.Create(FData.Clone, 0));
    if btnPrintSW.Down then PrintACheckIn(TSourceView(item.Obj));
    //if btnSmsSW.Down then SmsACheckIn(TSourceView(item.Obj));
    item.Obj.Free;
    item.Free;
    while ScreenSourceView.RowCount > ETopCount.Value do
      ScreenSourceView.RowCount:= ETopCount.Value;
  except
    on E: Exception do
    WriteLog(LOG_DEBUG, 'DisplayTimerTimer', E.Message);
  end;

//  if FScreenList.Count = 0 then Exit;
//  if btnPrintSW.Down then PrintACheckIn(TSourceView(FScreenList[0]));
//  //if btnWebSW.Down then DisplayACheckIn(TSourceView(FScreenList[0]));
//  //if btnEmailSW.Down then EMailACheckIn(TSourceView(FScreenList[0]));
//  if btnMicroMsgSW.Down then EMicroMsgACheckIn(TSourceView(FScreenList[0]));
//  if btnScreenSW.Down then ScreenACheckIn(TSourceView(FScreenList[0]));
//  FScreenList.Delete(0);
end;

(* Screen *)

//procedure TFCoteCheckIn.SendToFileScreen(Idx: Integer);
//var
//  lst: TStringList;
//  flname, S: string;
//  i: Integer;
//begin
//  with DQScreen do
//    try
//      Parameters[0].Value := Idx;
//      Open; // 证书文件文件名称
//      with TIniFile.Create(FCoteDB.ConfigFilename) do
//        try
//          flname := ReadString('SCREEN', 'FileName', 'WENGSCREEN.txt');
//          if ExtractFileDrive(flname) = '' then
//            flname := ExtractFilePath(Application.ExeName) + flname;
//          lst := TStringList.Create;
//          lst.LoadFromFile(flname);
//          while not EOF do
//          begin
//            S := Fields[0].AsString;
//            for i := 1 to FieldCount - 1 do
//              S := S + ' ' + Fields[i].AsString;
//            lst.Add(S);
//            Next;
//          end;
//          while lst.Count > 4 do
//            lst.Delete(0);
//        finally
//          lst.SaveToFile(flname);
//          lst.Free;
//          Free;
//        end;
//    finally
//      Close;
//    end;
//end;

end.
