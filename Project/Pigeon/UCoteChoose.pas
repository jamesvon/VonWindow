unit UCoteChoose;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids, Vcl.ComCtrls,
  Vcl.ExtCtrls, Vcl.Buttons, UFrameGridBar, UCoteDB, UCoteInfo, Data.DB,
  Data.Win.ADODB, UPlatformInfo, System.ImageList, Vcl.ImgList, UVonSystemFuns;

type
  TFCoteChoose = class(TForm)
    PanelMember: TPanel;
    FrameMemberGrid: TFrameGridBar;
    plCondition: TPanel;
    Panel4: TPanel;
    btnMemberSearch: TBitBtn;
    btnMemberReset: TBitBtn;
    Panel5: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    Label22: TLabel;
    EShortName1: TEdit;
    EMemberNo1: TEdit;
    EMemberName1: TEdit;
    Panel7: TPanel;
    Label1: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    EState1: TComboBox;
    ECity1: TComboBox;
    ECounty1: TComboBox;
    spMember: TSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    gridChoose: TDrawGrid;
    Panel1: TPanel;
    统计: TTabSheet;
    TabSheet2: TTabSheet;
    Button1: TButton;
    DQMember: TADOQuery;
    Label2: TLabel;
    ESMobile: TEdit;
    DQPigeon: TADOQuery;
    imgBetting: TImageList;
    lbAmount: TLabel;
    DQChoose: TADOQuery;
    procedure btnMemberResetClick(Sender: TObject);
    procedure btnMemberSearchClick(Sender: TObject);
    procedure FrameMemberGridGridDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure gridChooseDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure gridChooseSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    FPigeonSQL: string;
    FBetSetting: TStringList;
    FBetData: TStringList;
    FMatchID: TGuid;
    FMemberID: TGuid;
    FAmount: Currency;
    procedure SetMatchID(const Value: TGuid);
  public
    { Public declarations }
  published
    property MatchID : TGuid read FMatchID write SetMatchID;
  end;

  TBetData = class
  private
    FData: array of Currency;
    FFeather: string;
    FEye: string;
    FGBCode: string;
    FPigeonIdx: TGuid;
    function GetData(Index: Integer): Currency;
    procedure SetData(Index: Integer; const Value: Currency);
  public
    { Public declarations }
    constructor Create(SubjectCount: Integer);
    destructor Destroy;
    property Data[Index: Integer]: Currency read GetData write SetData;
  published
    property PigeonIdx: TGuid read FPigeonIdx write FPigeonIdx;
    property GBCode: string read FGBCode write FGBCode;
    property Feather: string read FFeather write FFeather;
    property Eye: string read FEye write FEye;
  end;

var
  FCoteChoose: TFCoteChoose;

implementation

{$R *.dfm}

procedure TFCoteChoose.FormCreate(Sender: TObject);
begin
  FPigeonSQL := DQPigeon.SQL.Text;
  FBetSetting:= TStringList.Create;
  FBetData:= TStringList.Create;
  FBetData.OwnsObjects:= True;
  gridChoose.ColWidths[0]:= 120;
  gridChoose.ColWidths[1]:= 48;
  gridChoose.ColWidths[2]:= 48;
end;

procedure TFCoteChoose.FormDestroy(Sender: TObject);
begin
  FBetData.Clear;
  FBetData.Free;
  FBetSetting.Free;
end;

procedure TFCoteChoose.btnMemberResetClick(Sender: TObject);
begin
  EMemberName1.Text := '';
  EMemberNo1.Text := '';
  EShortName1.Text := '';
  EState1.ItemIndex := 0;
  ECity1.ItemIndex := 0;
  ECounty1.ItemIndex := 0;
  ESMobile.Text := '';
end;

procedure TFCoteChoose.btnMemberSearchClick(Sender: TObject);
begin
  with DQMember do begin
    Close;
    SQL.Text := 'SELECT * FROM ' + Prefix + 'Member WHERE Status=''启用''';
    if EMemberName1.Text <> '' then
      SQL.Add('AND MemberName LIKE ''%' + EMemberName1.Text + '%''');
    if EMemberNo1.Text <> '' then
      SQL.Add('AND MemberNo LIKE ''%' + EMemberNo1.Text + '%''');
    if EShortName1.Text <> '' then
      SQL.Add('AND ShortName LIKE ''%' + EShortName1.Text + '%''');
    if ESMobile.Text <> '' then
      SQL.Add('AND Mobile LIKE ''%' + ESMobile.Text + '%''');
    if EState1.ItemIndex > 0 then
      SQL.Add('AND State LIKE ''%' + EState1.Text + '%''');
    if ECity1.ItemIndex > 0 then
      SQL.Add('AND City LIKE ''%' + ECity1.Text + '%''');
    if ECounty1.ItemIndex > 0 then
      SQL.Add('AND County LIKE ''%' + ECounty1.Text + '%''');
    SQL.Add('ORDER BY DisplayOrder');
    Open;
  end;
  FrameMemberGridGridDblClick(nil);
end;

procedure TFCoteChoose.Button1Click(Sender: TObject);
var
  R, C: Integer;
  FInfo: TChooseInfo;
begin
  FInfo:= TChooseInfo.Create;
  FInfo.Payment:= '工棚指定';
  FInfo.TicketNo:= FCoteDB.GetTicketNo('指定鸽');
  FInfo.GamblerNo:= GetGUID();
  FInfo.Status:= '投注';
  FInfo.SynFlag:= 'A';
  for R := 0 to FBetData.Count - 1 do
    for C := 0 to FBetSetting.Count - 1 do
      if TBetData(FBetData.Objects[R]).FData[C] > 0 then begin
        FInfo.ID:= 0;
        FInfo.GameIdx:= FMatchID;
        FInfo.PigeonIdx:= TBetData(FBetData.Objects[R]).PigeonIdx;
        FInfo.Gambler:= FMemberID;
        FInfo.Subject:= FBetSetting.Names[C];
        FInfo.Amount:= TBetData(FBetData.Objects[R]).Data[C];
        FInfo.ChoicedTime:= Now;
        FInfo.Modifiedate:= Now;
        FInfo.SaveData(FCoteDB.ADOConn);
        TBetData(FBetData.Objects[R]).FData[C]:= - FInfo.Amount;
      end;
  FInfo.Free;
  gridChoose.Repaint;
end;

procedure TFCoteChoose.FrameMemberGridGridDblClick(Sender: TObject);
var
  obj: TBetData;
  R, C: Integer;
begin
  gridChoose.OnDrawCell:= nil;
  FMemberID:= DQMember.FieldByName('ID').AsGuid;
  with DQPigeon do begin
    Close;
    Sql.Text:= FPigeonSQL + ' AND MemberIdx=''' +
      DQMember.FieldByName('ID').AsString + ''' ORDER BY DisplayOrder';
    Open;
    FBetData.Clear;
    gridChoose.RowCount:= RecordCount + 1;
    while not EOF do begin
      obj:= TBetData.Create(FBetSetting.Count);
      obj.PigeonIdx:= FieldByName('ID').AsGuid;
      obj.GBCode:= FieldByName('GBCode').AsString;
      obj.Feather:= FieldByName('Feather').AsString;
      obj.Eye:= FieldByName('Eye').AsString;
      FBetData.AddObject(FieldByName('ID').AsString, obj);
      Next;
    end;
  end;
  FAmount:= 0;
  with DQChoose do begin
    Close;
    Parameters[0].Value:= GuidToString(FMatchID);
    Parameters[1].Value:= GuidToString(FMemberID);
    Open;
    while not EOF do begin
      R:= FBetData.IndexOf(FieldByName('PigeonIdx').AsString);
      C:= FBetSetting.IndexOfName(FieldByName('Subject').AsString);
      TBetData(FBetData.Objects[R]).Data[C]:= 0 - FieldByName('Amount').AsCurrency;
      Next;
    end;
  end;
  gridChoose.OnDrawCell:= gridChooseDrawCell;
  gridChoose.Repaint;
end;

procedure TFCoteChoose.gridChooseDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  I: Integer;
  bmp: TBitmap;

  procedure text(Txt: string);
  var
    S: string;
  begin
    S:= StringReplace(Txt, '_', #13#10, [rfReplaceAll]);
    gridChoose.Canvas.TextRect(rect, S, [tfCenter, tfVerticalCenter]);
  end;
begin
  if FBetSetting.Count = 0 then Exit;
  if ARow = 0 then begin
    case ACol of
    0: text('统一环号');
    1: text('羽色');
    2: text('眼砂');
    else text(FBetSetting.Names[ACol - 3]);
    end;
  end else begin
    case ACol of
    0: text(TBetData(FBetData.Objects[ARow - 1]).GBCode);
    1: text(TBetData(FBetData.Objects[ARow - 1]).Feather);
    2: text(TBetData(FBetData.Objects[ARow - 1]).Eye);
    else begin
      if ARow > FBetData.Count then Exit;
        bmp:= TBitmap.Create;
        if TBetData(FBetData.Objects[ARow - 1]).Data[ACol - 3] > 0 then  //Checked
          imgBetting.GetBitmap(1, bmp)
        else if TBetData(FBetData.Objects[ARow - 1]).Data[ACol - 3] = 0 then  //Checked
          imgBetting.GetBitmap(0, bmp)
        else imgBetting.GetBitmap(2, bmp);
        gridChoose.Canvas.Draw(Rect.Left + (Rect.Width - bmp.Width) div 2,
          Rect.Top + (Rect.Height - bmp.Height) div 2, bmp);
        bmp.Free;
      end;
    end;
  end;
end;

procedure TFCoteChoose.gridChooseSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if ACol < 3 then Exit;
  if ARow < 1 then Exit;
  if TBetData(FBetData.Objects[ARow - 1]).Data[ACol - 3] > 0 then begin
    FAmount:= FAmount - TBetData(FBetData.Objects[ARow - 1]).Data[ACol - 3];
    TBetData(FBetData.Objects[ARow - 1]).Data[ACol - 3]:= 0;
  end else if TBetData(FBetData.Objects[ARow - 1]).Data[ACol - 3] = 0 then begin
    FAmount:= FAmount + StrToCurr(FBetSetting.ValueFromIndex[ACol - 3]);
    TBetData(FBetData.Objects[ARow - 1]).Data[ACol - 3]:= StrToCurr(FBetSetting.ValueFromIndex[ACol - 3]);
  end;
  lbAmount.Caption:= '指定总金额：' + CurrToStr(FAmount);
end;

procedure TFCoteChoose.SetMatchID(const Value: TGuid);
begin
  FMatchID := Value;
  FBetSetting.Text:= GetLargeStr('ID', GuidToString(FMatchID), 'JOIN_Match', 'Settings', FCoteDB.ADOConn);
  gridChoose.ColCount:= FBetSetting.Count + 3;
end;

{ TBetData }

constructor TBetData.Create(SubjectCount: Integer);
begin
  SetLength(FData, SubjectCount);// * SizeOf(Currency));
  FillChar(FData[0], SubjectCount, 0);// * SizeOf(Currency), 0);
end;

destructor TBetData.Destroy;
begin
  SetLength(FData, 0);
end;

function TBetData.GetData(Index: Integer): Currency;
begin
  Result:= FData[Index];
end;

procedure TBetData.SetData(Index: Integer; const Value: Currency);
begin
  FData[Index]:= Value;
end;

end.
