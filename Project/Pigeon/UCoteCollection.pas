unit UCoteCollection;
(*----------------------------------------------------------------
// Copyright (C) 2013 by jamesvon All rights reserved.
// 冯驰军 版权所有，联系邮箱jamesvon@163.com。
//
// 文件功能描述：公棚集鸽
// 创建时间：2017/1/10 14:56:32
//
// 时间：2017年9月27日 修改说明：自动加鸽子遇到非入棚鸽子报错停止
// 时间：2021年2月27日 增加多机集鸽功能
//----------------------------------------------------------------
-> SendMessage(FrameViewBar1.lstView.Handle, WM_VSCROLL, SB_BOTTOM, 0); 这句话需
   要放在显示函数外面，主要是为了避免再Start时，回显历史数据缓慢问题
-> 2021年10月24日，建立显示队列，目的是防止多机集鸽时造成显示时异常崩溃。
//----------------------------------------------------------------*)
(* 多机集鸽通讯机制，05作为起始码+客户端编码+客户端服务器注册码+5位功能名+内容    ┴ ┘┐┤┼│─├┌└ ┬
 =============================================================================
   ┌───────────┐                                              ┌──────────┐
   │  Client   │    <------------------------------------>    │  Server  │
   └─────┬─────┘                                              └─────┬────┘
         ├──── 登录 ──── 05<CLIENT_NO><1byte>LOGIN<MAC_ADDRESS> ───>│
         │<───── S ───── 05SUCCES<认证码> ──────────────────────────┤
         │                                                          │
         ├── 获取环库 ── 05<CLIENT_NO><认证码>RINGS ───────────────>│
         │<───── S ───── 05SUCCES<RingsList> ───────────────────────┤
         │                                                          │
         ├── 获取参数 ── 05<CLIENT_NO><认证码>PARAM ───────────────>│
         │<───── S ───── 05SUCCES<ParamList> ───────────────────────┤
         │                                                          │
         ├── 获取赛事 ── 05<CLIENT_NO><认证码>MATCH ───────────────>│
         │<───── S ───── 05SUCCES<MatchInfo> ───────────────────────┤
         │                                                          │
         │<───── F ───── 05FAILED|WARNIN|PROMPT<error info> ────────┤
 ─────────────────┬────────────────────────────────────────────────────────────
 <CLIENT_NO>      │ 客户端序号
 <认证码>         │ 服务端认证客户码（1byte）
 <RingsList>      │ JZCode(HEX[16]) + GameCode(HEX[8]) + FactoryCode(HEX[16])
 <MatchInfo>      │ 按照设置提取显示内容，发送给客户端
 <error info>     │ 错误信息
 ─────────────────┴───────────────────────────────────────────────────────────*)
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ImgList, ComCtrls, StdCtrls, Buttons, UCoteDB, DB, ADODB,
  UCoteComm, DateUtils, ShellAPI, ToolWin, Spin, IniFiles, UVonSystemFuns,
  UFrameViewBar, UCoteInfo, UVonLog, UDlgCamera, Menus, OleCtrls, SHDocVw,
  System.ImageList, Printers, UCommunication, UCoteMatch, UVonConfig;

const
  COMMTIMOUT = 15;

type
  EWarningKind = (
    COL_NONE,         //无                  白
    COL_SUCCES,       //成功集鸽            蓝
    COL_PROMPT,       //有标记赛鸽警告      绿
    COL_WARNIN,       //非正常状态          黄
    COL_FAILED        //未入棚              红


//    COL_DBL,        //重复集鸽            湖蓝
//    COL_SUCCESS,    //成功集鸽            蓝
//    COL_FLAG,       //有标记赛鸽警告      绿
//    COL_STATUS,     //非正常状态          黄
//    COL_NO_JZ,      //非荣冠              黄
//    COL_FAIL        //未入棚              红
  );   //成功 绿色，未入棚 红色，其他灰色

  TCollectionClientInfo = class
  private
    FID: byte;
    FReg: Byte;
    FMAC: Array[0..9]of byte;
    FClientCollectId: Integer;
    FLasterRing: Int64;
    FLasterTimer: TDatetime;
    FImg: TImage;
  public
    constructor Create(ClientID: Byte; const MAC: array of byte; AOwner: TWinControl; MatchId: TGuid);
    destructor Destroy; override;
    procedure IncCount;
  published
    property ID: byte read FID;
    property Reg: Byte read FReg;
    property ClientCollectId: Integer read FClientCollectID;
    property LasterRing: Int64 read FLasterRing;
    property LasterTimer: TDatetime read FLasterTimer;
  end;

  TFCollection = class(TForm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    LName: TLabel;
    LGameAddress: TLabel;
    LManager: TLabel;
    LGameLong: TLabel;
    DQCollection: TADOQuery;
    DQPigeonInfo: TADOQuery;
    DQCheckDbl_W: TADOQuery;
    imgLed: TImageList;
    Label11: TLabel;
    ECountToBeep: TSpinEdit;
    LCountOfBatch: TLabel;
    btnStop: TBitBtn;
    btnChangeEleCode: TBitBtn;
    plHint: TPanel;
    DQSource: TADOQuery;
    BtnPrint: TSpeedButton;
    Label3: TLabel;
    EComm: TComboBox;
    Label6: TLabel;
    ECommRate: TComboBox;
    btnReconnect: TSpeedButton;
    DQMatch: TADOQuery;
    btnSms: TSpeedButton;
    DQDisplay: TADOQuery;
    FrameViewBar1: TFrameViewBar;
    btnVideo: TSpeedButton;
    btnPause: TBitBtn;
    btnScreen: TSpeedButton;
    btnConnect: TSpeedButton;
    imgClient: TImageList;
    DQCheckDbl_R: TADOQuery;
    ImgInfo3: TImage;
    ImgInfo2: TImage;
    ImgInfo1: TImage;
    plClients: TPanel;
    Timer1: TTimer;
    btnUpload: TSpeedButton;
    procedure btnStopClick(Sender: TObject);
    procedure btnPauseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnReconnectClick(Sender: TObject);
    procedure btnVideoClick(Sender: TObject);
    procedure btnScreenClick(Sender: TObject);
    procedure BtnPrintClick(Sender: TObject);
    procedure btnConnectClick(Sender: TObject);
    procedure plHintResize(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    FComm: TJZCommBase;
    FCollections: array[0..5]of string;
    FMatchInfo: TMatchInfo;
    //FPigeonView1: TPigeonView;
    FCount, FPrintCount, FCage, FCageNum: Integer;
    FCheckIndex, FCurrentCollectID: Integer;
    FPageLineCount: Integer;
    FLastFCode: Int64;
    FLastJCode: Int64;
    FGameCodes: PArrayInt;
    FCountOfNodataAfterWrite: Integer;
    FInfo: TGameSourceInfo;
    FScreenComm: TUart;
    FIsCollected: Boolean;
    FTryToWriteCount: Integer;
    FPrinter: string;
    FAutoPrint: Boolean;
    FDisplayTitles, FPrintTitles, FSMSTitles: TStringList;    //FieldName=Title:width
    FPrintLine: string;
    FPrnFileIdx: Integer;
    FSmsMsgFormat: string;
    FSvr: TVonTCPServer;
    FClients: TList;
    FMultiMode: string;
    FSayCount: Integer;
    FSourceMemory, FInfoMemory: TFIFO;
    procedure AddCollectionCount;
    procedure AddInfo(Code, Info: string; InfoKind: EWarningKind);
    function CompareDT(ADate, BDate: TDateTime): Boolean;
    procedure EventOfReceived(JZCode: Int64; GameCode: Cardinal; FactoryCode: Int64);
    procedure EventOfReceived_W(JZCode: Int64; GameCode: Cardinal; FactoryCode: Int64);
    procedure EventOfWrite(Successed: Boolean);
    procedure EventOfWrite_W(Successed: Boolean);
    procedure UpdateMatchStatus(Status: TJZMatchStatus);
    procedure DisplayACollection(Q: TCustomADODataSet);
    procedure PrintACollection(Q: TCustomADODataSet);
//    procedure SmsACollection(Q: TCustomADODataSet);
    procedure BeginToCollection(CommPort, CommRate: Integer);
    procedure JoinScreenStr(s: string);
    procedure EventOfReceived_R(JZCode: Int64; GameCode: Cardinal;
      FactoryCode: Int64);
    procedure ClearClient;
    procedure EventOfServer(var Data: array of byte; var Len: Integer;
      size: Integer);
    function EventLogin(var Data: array of byte; size: Integer): Integer;
    procedure IncCount(P: TCollectionClientInfo);
    function FindCollection(ClientID, RegCode: byte): TCollectionClientInfo;
    function EventGetParams(var Data: array of byte; size: Integer): Integer;
    function EventGetRings(var Data: array of byte; size: Integer): Integer;
    function EventClientCollected(P: TCollectionClientInfo; var Data: array of byte;
      size: Integer): Integer;
    procedure Collected(P: TCollectionClientInfo; ID: Integer);
    function EventClientHeartbeat(P: TCollectionClientInfo;
      var Data: array of byte; size: Integer): Integer;
    function EventClientGetCollectID(P: TCollectionClientInfo;
      var Data: array of byte; size: Integer): Integer;
    procedure MemoryCollected(P: TCollectionClientInfo; ID: Integer);
  public
    { Public declarations }
    function GetNewCollectID: Integer;
    procedure Start(MatchID: TGuid; CommPort: Word; CommRate: Integer);
  published
    property MultiMode: string read FMultiMode;
  end;

  TLedInfo = class
    public
    Code, Info: string; InfoKind: EWarningKind;
    constructor create(ACode, AInfo: string; AInfoKind: EWarningKind);
  end;

var
  FCollection: TFCollection;

implementation

uses ADOInt, Math, UVonSinoVoiceAPI, StrUtils, UDlgCommunication;

type
  TMyHint = class(TPanel)
  published
    property Canvas;
  end;

{$R *.dfm}

procedure TFCollection.FormCreate(Sender: TObject);
  function initConfig(cfgName: string): TStringList;
  begin
    Result:= TStringList.Create;
    Result.Delimiter := ',';           // FieldName=Title:width
    Result.DelimitedText := ReadAppConfig('FIELDS', cfgName);
  end;
begin
  //FOnSay:= nil;
  FSourceMemory:= TFIFO.Create;
  FInfoMemory:= TFIFO.Create;
  FInfo := TGameSourceInfo.Create;
  FInfo.ClientID:= 'A';
  FInfo.Conn:= FCoteDB.ADOConn;
  FClients:= TList.Create;
  if FileExists(FCoteDB.AppPath + 'Styles\LEDS.BMP') then
    LoadImages(FCoteDB.AppPath + 'Styles\LEDS.BMP', imgLed);
//  FPigeonView:= TPigeonView.Create;
  FMatchInfo := TMatchInfo.Create;
  FDisplayTitles := initConfig('CollectDisplayFields');
  FPrintTitles := initConfig('CollectPrintFields');
  FSMSTitles := initConfig('CollectSMSFields');
  BtnPrint.Down:= ReadAppConfig('SYSTEM', 'CollectionAutoPrint') = '1';
  btnSms.Down:= ReadAppConfig('SYSTEM', 'CollectionAutoSms') = '1';
  FSmsMsgFormat:= ReadAppConfig('SYSTEM', 'SMSCollectionMsg');
  FMultiMode:= ReadAppConfig('MultiCollection', 'Mode');
  FSayCount:= StrToInt(ReadAppConfig('SYSTEM', 'CollectSayCount'));
  FAutoPrint:= False;
  FPrinter:= ReadAppConfig('SYSTEM', 'Printer');
  FPageLineCount := StrToInt(ReadAppConfig('SYSTEM', 'PrintCountPerPage'));
  New(FGameCodes);
  with TIniFile.Create(FCoteDB.ConfigFilename) do
    try
      FrameViewBar1.LCount.Visible :=
        ReadBool('SYSTEM', 'DisplayCollenctCount', true);
    finally
      Free;
    end;
  FSmsMsgFormat := ReadAppConfig('SYSTEM', 'SMSCollectionMsg');
  ReadAppItems('COMMUNICATION', 'PortList', 'COM1', EComm.Items);
end;

procedure TFCollection.FormDestroy(Sender: TObject);
begin
  if Assigned(FComm) then FCoteDB.CloseComm(FComm);
  FSMSTitles.Free;
  FPrintTitles.Free;
  FDisplayTitles.Free;
  FInfo.Free;
  if Assigned(FScreenComm) then begin
    FScreenComm.Close;
    FreeAndNil(FScreenComm);
  end;
  ClearClient;
  Dispose(FGameCodes);
  FClients.Free;
  FInfoMemory.Free;
  FSourceMemory.Free;
end;

procedure TFCollection.Start(MatchID: TGuid;
  CommPort: Word; CommRate: Integer);
var
  I, j, mpos: Integer;
  S: string;
  FSourceView: TSourceView;
begin // 开启集鸽
  with DQMatch do
  begin
    Close;
    Parameters[0].Value := GuidToString(MatchID);
    Open;
    if not FMatchInfo.LoadFromDB(DQMatch) then
      raise Exception.Create(Format(RES_Invalid_INFO, [LastInfo]));
  end;
  {$region '初始化赛绩信息'}
  FInfo.CollecteNo := '集鸽单-主机';
  FInfo.GameIdx := MatchID;
  FInfo.SetCode := 'A';
  FInfo.ReportTime := 0;
  FInfo.FlyTime := 0;
  FInfo.Speed := 0;
  FInfo.IsBack := 0;
  FInfo.Kind := 1;
  FInfo.SourceOrder := 0;
  FInfo.Note := '';
  {$endregion}
  WriteLog(LOG_MAJOR, '集鸽', '赛事' + DQMatch.FieldByName('MatchName').AsString + '（' + DQMatch.FieldByName('ID').AsString + '）开始集鸽。');
  FPrnFileIdx := 1;
  FrameViewBar1.lstView.Clear;
  EComm.ItemIndex := CommPort - 1;
  ECommRate.ItemIndex := ECommRate.Items.IndexOf(IntToStr(CommRate));
  LName.Caption := FMatchInfo.MatchName;
  LGameAddress.Caption := FMatchInfo.GameAddress;
  LManager.Caption := FMatchInfo.Manager;
  LGameLong.Caption := FloatToStr(FMatchInfo.GameLong);
  btnUpload.Down := FMatchInfo.UploadSetting and 1 > 0;
  RandomNums(1, 6, FMatchInfo.ID.D1, FGameCodes); //产生8192个赛事环号->电子环号
  WriteLog(LOG_MAJOR, '集鸽', '生成赛事码 ' + IntToStr(Length(TArrayInt(FGameCodes^))));
  FCageNum:= 0;
  FrameViewBar1.lstView.Columns.Clear;
  for I := 0 to FDisplayTitles.Count - 1 do
    with FrameViewBar1.lstView.Columns.Add do
    begin
      S:= FDisplayTitles.ValueFromIndex[I]; mpos:= Pos(':', S);
      Caption := Copy(S, 1, mPos - 1);
      Width := StrToInt(Copy(S, mPos + 1, MaxInt));
    end;
  ClearClient;
  FCage:= 1; FCageNum:= 0;
  FSourceView:= TSourceView.Create;
  TSourceView.OpenQuery(DQDisplay, 'GameIdx=''' + GuidToString(FMatchInfo.ID) + ''' ORDER BY CollectID');
  while not DQDisplay.EOF do begin  // 显示赛事信息
    DisplayACollection(DQDisplay);
    DQDisplay.Next;
  end;
  FSourceView.Free;
  SendMessage(FrameViewBar1.lstView.Handle, WM_VSCROLL, SB_BOTTOM, 0);
  AddInfo('', '尚未集鸽', COL_NONE);
  AddInfo('', '尚未集鸽', COL_NONE);
  AddInfo('', '尚未集鸽', COL_NONE);
  FPrintCount:= 0;
  FCount := DQDisplay.RecordCount;
  FCurrentCollectID:= FCount;
  FrameViewBar1.LCount.Caption := Format('目前本赛事已集鸽%d羽', [FCount]);
  DQCollection.Open;
  btnReconnect.down:= true;
  btnReconnectClick(nil);
  TSourceView.OpenQuery(DQDisplay, ' S.ID=:ID');
  Timer1.Enabled:= True;
end;

(* 缓存及屏幕显示 *)

procedure TFCollection.Timer1Timer(Sender: TObject);
var
  FCurrent: TFIFOItem;
  FLedInfo: TLedInfo;
begin
  FCurrent:= FSourceMemory.Pull;
  if Assigned(FCurrent) then begin
    Collected(TCollectionClientInfo(FCurrent.Obj), FCurrent.Data);
    FCurrent.Free;
  end;
  FCurrent:= FInfoMemory.Pull;
  if Assigned(FCurrent) then begin
    FLedInfo:= TLedInfo(FCurrent.Obj);
    FCurrent.Free;
    ImgInfo3.Picture.Assign(ImgInfo2.Picture);
    ImgInfo2.Picture.Assign(ImgInfo1.Picture);
    with ImgInfo1.Picture.Bitmap.Canvas do begin
      FillRect(Rect(0, 0, ImgInfo1.Width, ImgInfo1.Height));
      imgLed.Draw(ImgInfo1.Picture.Bitmap.Canvas, 5, (ImgInfo1.Height - imgLed.Height) div 2, Ord(FLedInfo.InfoKind), true);
      case FLedInfo.InfoKind of
      COL_NONE: Font.Color:= clWhite;         //无             白        0
      COL_SUCCES: Font.Color:= $0080FF80;     //成功集鸽       浅绿色    1
      COL_PROMPT: Font.Color:= $0080FFFF;     //有标记赛鸽警告 浅黄色    2
      COL_WARNIN: Font.Color:= $004080FF;     //非正常状态     橘黄色    3
      COL_FAILED: Font.Color:= clRed;         //未入棚         红色      4
      end;
      Font.Size:= 24;
      TextOut(imgLed.Width + 10, 0, FLedInfo.Code);
      TextOut(imgLed.Width + 10, ImgInfo1.Height div 2, FLedInfo.Info);
    end;
    FLedInfo.Free;
  end;
end;

function TFCollection.GetNewCollectID: Integer;
begin
  Inc(FCurrentCollectID);
  Result:= FCurrentCollectID;
end;

(* 荣冠小屏 *)

procedure TFCollection.btnScreenClick(Sender: TObject);
begin
  if btnScreen.Down then
    with TFDlgCommunication.Create(nil)do try
      if ShowDlg(ReadAppConfig('SmallScreen', 'ScreenComm'),
        StrToInt(ReadAppConfig('SmallScreen', 'ScreenRate')), 'n', 8, 1) = mrOK then
      begin
        WriteAppConfig('SmallScreen', 'ScreenComm', Port);
        WriteAppConfig('SmallScreen', 'ScreenRate', IntToStr(BaudRate));
        FScreenComm:= TUart.Create;
        if not FScreenComm.Open(Port, BaudRate, Parity, ByteSize, StopBits)then
          btnScreen.Down:= False;
      end;
    finally
      Free;
    end else if Assigned(FScreenComm) then begin
      FScreenComm.Close;
      FreeAndNil(FScreenComm);
    end;
end;

function UnicodeToBuff(S: string; var buff: array of byte; Idx: Integer): Word;
type
  WC = packed record
    case integer of
    0: (c: char);
    1: (w: word);
    2: (b: array[0..1]of byte);
    end;
var
  strData: TBytes;
  ch: WC;
  i, nCount: Integer;
begin
  nCount:= Length(S);
  Result:= 0;
  for I := 1 to nCount do begin
    ch.c:= S[I];
		if ch.w >= $80 then begin
			if ch.w <= $FF then
				ch.w := ch.w - $80
			else if(ch.w >= $2000)and(ch.w <= $266F) then
				ch.w := ch.w - $2000 + 128
			else if(ch.w >= $3000)and(ch.w <= $33FF) then
				ch.w := ch.w - $3000 + 1648 + 128
			else if(ch.w >= $4E00)and(ch.w <= $9FA5) then
				ch.w := ch.w - $4E00 + 1648 + 1024 + 128
			else if(ch.w >= $F900)and(ch.w <= $FFFF) then
				ch.w := ch.w - $F900 + 1648 + 1024 + 20902 + 128;
			ch.w := ch.w + 128;
    end;
    buff[Idx]:= ch.b[0];
    buff[Idx + 1]:= ch.b[1];
    Inc(Idx, 2);
    Inc(Result, 2);
  end;
end;

procedure TFCollection.JoinScreenStr(s: string);
var
  Data: array[0..655]of byte;
  FScreenComm: TUart;
  l, crc: Word;
  I: Integer;
begin
  l:= UnicodeToBuff(s, Data, 48);
  with TUart.Create do try
    Open('COM3', 57600, 'n', 8, 1);
    Data[00]:= $A0; //信息头，固定 0xA0
    crc:= l + 16 + 32 + 4;
    Move(crc, Data[01], 2); //字节1-2：整个信息包长度，低字节在前，高字节在后。
    Data[03]:= 1; Data[04]:= $00; //字节3-4： 16bit屏ID，低字节在前，高字节在后，0x0000代表广播信息
    Data[05]:= $05; Data[06]:= $00; Data[07]:= $00; Data[08]:= $00; //字节5-8： 流水号低32位，作为指令的临时唯一标识，服务器端可以以此ID作为指令的唯一标识。
    Data[19]:= $00; Data[10]:= $00; Data[11]:= $00; Data[12]:= $00; //字节9-12：流水号高32位，低字节在前，高字节在后。（该字段暂时保留）
    Data[13]:= $03; //字节13：具体指令代码。详见第三部分具体描述。(在终端回馈包中，该字段用于表示错误代码)
    Data[14]:= $00; Data[15]:= $00; //字节14-15：保留，填充为0
    // 指令参数：变长，至少32字节，少于32字节的用零填充到32字节。具体含义详见第三部分具体指令描述
    Data[16]:= $01; Data[17]:= $00; //字节0-1：信息序号：低字节在前，高字节在后，从1开始，上限取决于控制板类型，信息的序号就是信息存放的位置。播放时将按照序号从小到大轮流播放。
    Data[18]:= $0D; //字节2：动画方式    0x0D-连续左移（走马灯）
    //字节3：停留方式 0000 0000
    //                  │    └──────低4位代表信息停留时是否闪烁0x0-不闪烁，0x1-亮灭闪，0x2-反白闪。
    //                  └───────────高4位代表信息播放时是否伴随“环绕边框”，bit4：是否有环绕边框(1=有，0=没有)；bit5：4点环绕还是单点环绕（1=4点，0=单点）；bit6：环绕速度（1=慢速，0=快速）。
    Data[29]:= $10;
    Data[20]:= $7F; //字节4：移动速度和级别，低4bit代表移动速度0x0-0xF，数字越大越快。高4bit代表速度级别，有效范围0x1-0xF数字越大越慢。
    Data[21]:= $00; //字节5：页面停留时间
    Data[22]:= $02; //字节6：闪烁间隔，有效范围0x02-0xFF，秒数
    Data[23]:= $00; //字节7：连播次数，范围0x01-0xFF
    //字节8-15：起播时间段
    Data[24]:= $00; Data[25]:= $00; Data[26]:= $00; Data[27]:= $00;
    Data[28]:= $00; Data[29]:= $00; Data[30]:= $00; Data[31]:= $00;
    //字节16-23：停播时间段
    Data[32]:= $00; Data[33]:= $00; Data[34]:= $00; Data[35]:= $00;
    Data[36]:= $00; Data[37]:= $00; Data[38]:= $00; Data[39]:= $00;
    Data[40]:= $03; //字节24：文字颜色，bit0=红色开关；bit1=绿色开关；bit2=蓝色开关；
    Data[41]:= $00; //字节25：低4位代表内容类型，0x0代表文本，高4位代表播放方式，0x0代表普通信息
    Data[42]:= $00; //字节26：本信息处于分组中的序号
    Data[43]:= $00; //字节27：本组信息总最大序号。（普通信息这2个字节直接填0）
    Move(l, Data[44], 2);
    //Data[44]:= $08; Data[45]:= $00; //字节28-29：“信息内容”的长度，低字节在前，高字节在后。
    Data[46]:= $00; //字节30：选项，bit7，代表本条信息播放前，是否先清屏（仅TF-3/TF-4支持），bit0-3，代表本条信息所在的分区。（注，该选项仅分区卡支持）
    Data[47]:= $00; //字节31：保留，填充为0
    //Move(strData[0], Data[48], l);
    crc:= 0;
    for I:= 0 to 47 + l do crc:= crc + Data[I];
    Move(crc, Data[48 + l], 2);
    Data[48 + l + 2]:= $00;
    Data[48 + l + 3]:= $50;
    Send(Data, 48 + l + 4);
  finally
    Free;
  end;
end;

procedure TFCollection.BeginToCollection(CommPort: Integer; CommRate: Integer);
begin
  WriteLog(LOG_Debug, 'Collection', 'Create a communicator ...');
  FComm:= FCoteDB.CreateComm(EventOfReceived, nil, nil, true);
  FComm.BoardCount := StrToInt(ReadAppConfig('COMMUNICATION', 'BoardCount'));
  FComm.Open(CommPort, CommRate, 'n', 8, 1);
  if not FComm.Connected then begin
    FCoteDB.CloseComm(FComm);
    raise Exception.Create('通讯端口未能打开，可能由于其他窗口或界面占用该端口，请检查，目前不能进行归巢，可以通过手动进行重试。');
  end else WriteLog(LOG_Debug, 'Collection', 'Begin to read ring...');
end;

procedure TFCollection.DisplayACollection(Q: TCustomADODataSet);
var
  I: Integer;
  szS: string;
  WaitReturn: DWORD;
begin
  WriteLog(LOG_DEBUG, 'DisplayACollection', '...' + Q.FieldByName('GBCode').AsString);
  with FrameViewBar1.lstView.Items.Add do
  begin
    if Q.FindField(FDisplayTitles.Names[0]) = nil then ShowMessage('Can not find field ' + FDisplayTitles.Names[0]);
    Caption := Q.FieldByName(FDisplayTitles.Names[0]).AsString;
    szS:= Caption;
    for I := 1 to FDisplayTitles.Count - 1 do begin
      SubItems.Add(Q.FieldByName(FDisplayTitles.Names[I]).AsString);
      szS:= szS + ' ' + Q.FieldByName(FDisplayTitles.Names[I]).AsString;
    end;
  end;
  if Assigned(FDlgCamera) then begin
    FDlgCamera.VideoText(szS);
  end;
end;

//procedure TFCollection.SmsACollection(Q: TCustomADODataSet);
//var
//  I: Integer;
//  smsMsg: Array of TVarRec;
//begin
//  SetLength(smsMsg, FSmsTitles.Count);
//  for I := 1 to FSmsTitles.Count - 1 do begin
//    smsMsg[I].VType:= vtString;
//    New(smsMsg[I].vString);
//    smsMsg[I].VString^:= Q.FieldByName(FSmsTitles.Names[I]).AsString;
//  end;
//  FCoteDB.SendSMS(Q.FieldByName('Mobile').AsString, Format(FSmsMsgFormat, smsMsg));
//  SetLength(smsMsg, 0);
//end;

procedure TFCollection.UpdateMatchStatus(Status: TJZMatchStatus);
begin
  with DQMatch do
  begin
    FMatchInfo.Status := TJZMatchStatus(Status);
    FMatchInfo.UpdateToDB(DQMatch);
  end;
end;

procedure TFCollection.EventOfReceived(JZCode: Int64; GameCode: Cardinal;
  FactoryCode: Int64);
begin
    if FCoteDB.RingKind = '只读环' then EventOfReceived_R(JZCode, GameCode, FactoryCode)
    else if FCoteDB.RingKind = '读写环' then EventOfReceived_W(JZCode, GameCode, FactoryCode)
    else if FCoteDB.RingKind = '高频只读' then EventOfReceived_R(JZCode, GameCode, FactoryCode)
    else if FCoteDB.RingKind = '高频读写' then EventOfReceived_W(JZCode, GameCode, FactoryCode)
    else if FCoteDB.RingKind = '凯信只读' then EventOfReceived_R(JZCode, GameCode, FactoryCode);
end;

(*
只读处理逻辑
  1、检查足环有效性（非荣冠、重复扫环、已集鸽）
  2、状态标记检查（非正常、有标记->提示）
  3、存储
  4、打印、显示、短信
  5、显示笼号等信息
*)
procedure TFCollection.EventOfReceived_R(JZCode: Int64; GameCode: Cardinal;
  FactoryCode: Int64);
var
  Idx: Integer;
  smsMsg: string;
  FPigeonView: TPigeonView;
begin
{$REGION '检测足环的有效性'}
  if FactoryCode = 0 then Exit; //No data
  if JZCode = 0 then begin
    writeLog(LOG_DEBUG, 'EventOfReceived',
      Format('非 JZCode=%x, GameCode=%x, FactoryCode=%x',
      [JZCode, GameCode, FactoryCode]));
    AddInfo(Format('%x(%x)', [JZCode, FactoryCode]), NOJUNZHUORING, COL_FAILED);
    Exit; // 没有扫到环
  end;
  if JZCode = FLastFCode then begin
    AddInfo('...', '重复扫环！', COL_PROMPT);
    Exit;
  end;
  with DQCheckDbl_R do try
    Parameters[0].Value:= JZCode;
    Parameters[1].Value:= GuidToString(FMatchInfo.ID);
    Open;
    if not EOF then begin
      AddInfo(Fields[1].AsString, '已集鸽', COL_PROMPT);
      writeLog(LOG_DEBUG, 'Collection', Format('已集鸽数据(%x-%x)放弃。',
        [JZCode, FactoryCode]));; // 存储集鸽数据
      Exit;
    end; // 提取原始信息（未转换的信息）入棚记录，未入棚不能集鸽
  finally
    Close;
  end;
{$ENDREGION}
  writeLog(LOG_DEBUG, 'EventOfReceived',
    Format('save to db JZCode=%x, GameCode=%x, FactoryCode=%x',
    [JZCode, GameCode, FactoryCode]));
  FPigeonView:= TPigeonView.Create;
  try
{$region '检查赛鸽是否入棚，检查赛鸽状态，非正常赛鸽提示是否集鸽'}
    if not FPigeonView.LoadData(FCoteDB.AdoConn, 'JZCode=' + InttoStr(JZCode)) then begin
      AddInfo(Format('%x(%x)', [JZCode, FactoryCode]), '尚未入棚', COL_FAILED);
      writeLog(LOG_DEBUG, 'Collection', Format('尚未入棚(%x-%x)放弃。',
        [JZCode, FactoryCode])); // 存储集鸽数据
      Exit;
    end; // 提取入棚记录，鸽主未交费不能集鸽
    if FPigeonView.Status <> '正常' then begin
      DlgInfo('警告', '该鸽主处于' + FPigeonView.Status +
        '状态，系统不允许集鸽.', 3);
      Exit;
    end;  // 提取入棚记录，赛鸽未交费不能集鸽
    if FPigeonView.Cashkind <> '正常' then begin
      DlgInfo('警告', FPigeonView.GBCode + '处于' + FPigeonView.Cashkind +
        '状态，系统不允许集鸽.', 3);
      Exit;
    end;
{$ENDREGION}
{$REGION '存储赛事信息'}
    FInfo.ID := 0;
    FInfo.CollecteNo := '集鸽单';
    FInfo.CollecteDate := Now;
    FInfo.CollectID := GetNewCollectID();
    FInfo.PigeonIdx:= FPigeonView.ID;
    FInfo.GameCode := $B52B;
    FInfo.SetCode := Format('A%0.6d', [TCollectionClientInfo(FClients[0]).ClientCollectId]);
    FInfo.CollecteNo := '';
    FInfo.CollecteDate := FCoteDB.CurrTime;
    if btnUpload.Down then FInfo.SynFlag:= 'A' else FInfo.SynFlag:= 'O';
    if btnSms.Down then FInfo.SmsFlag:= 'A' else FInfo.SmsFlag:= 'O';
    try
      FInfo.SaveToDB(DQCollection);
    except
      DQCollection.Cancel;
      Dec(FCurrentCollectID);
      AddInfo(Format('%x(%x)', [JZCode, FactoryCode]), '重复集鸽', COL_PROMPT);
      writeLog(LOG_DEBUG, 'Collection', Format('重复集鸽数据(%x-%x)放弃。',
        [JZCode, FactoryCode])); // 存储集鸽数据
      Exit;
    end;
{$ENDREGION}
{$REGION '显示集鸽记录,发送短信，显示数量及笼号信息}
    // 提取原始信息入棚状态，提示警告
    if FPigeonView.Flag <> '' then
      DlgInfo('警告', FPigeonView.GBCode + '标记为' + FPigeonView.Flag, 3);
    WavBeep;
    MemoryCollected(FClients[0], FInfo.ID);
    FLastFCode := 0;
    AddInfo(FPigeonView.GBCode, '完成集鸽', COL_SUCCES);
{$endregion}
  finally
    FPigeonView.Free;
  end;
  FLastFCode := FactoryCode;
  (* Load tran code *)
  writeLog(LOG_DEBUG, 'EventOfReceived',
    Format('Write JZCode=%x, GameCode=%x, FactoryCode=%x', [JZCode, 0, FactoryCode]));
  FLastJCode := JZCode;
end;

(*
可读写处理逻辑
  1、检查足环有效性（非荣冠、重复扫环、已集鸽）
  ------------------------------------------------------------------------------
        1--->发现新环      |      2--->写入环号      |      3--->读取复验
  ------------------------------------------------------------------------------
  1.1、提取原始信息入棚状态| 2.1、写环后验证         |3.1、存储赛事信息
     提示警告，拒绝集鸽    |                         |3.2、显示集鸽记录,发送短信
  1.2、检查重复集鸽        |                         |3.3、打印集鸽单
  1.3、发送写环命令        |                         |
*)
procedure TFCollection.EventOfReceived_W(JZCode: Int64; GameCode: Cardinal;
  FactoryCode: Int64);
var
  Idx: Integer;
  smsMsg: string;
  FPigeonView: TPigeonView;
begin
{$REGION '检测足环的有效性'}
  if FactoryCode = 0 then Exit; //No data
  if JZCode = 0 then begin
    writeLog(LOG_DEBUG, 'EventOfReceived',
      Format('非荣冠 JZCode=%x, GameCode=%x, FactoryCode=%x',
      [JZCode, GameCode, FactoryCode]));
    AddInfo(Format('%x(%x)', [JZCode, FactoryCode]), NOJUNZHUORING, COL_FAILED);
    Exit; // 没有扫到环
  end;
{$ENDREGION}
  writeLog(LOG_DEBUG, 'EventOfReceived',
    Format('Read Status=%s JZCode=%x, GameCode=%x, FactoryCode=%x, LJZ=%x, LGC=%x, LFC=%x',
    [BoolToStr(FIsCollected, true), JZCode, GameCode, FactoryCode, FLastJCode, FInfo.GameCode, FLastFCode]));
  if not FIsCollected and(FLastFCode = FactoryCode)and(FLastJCode = JZCode)and(FInfo.GameCode = GameCode)then
  begin  //第二次读取，确认后存储
    writeLog(LOG_DEBUG, 'Second Received',
      Format('JZCode=%x, GameCode=%x, FactoryCode=%x', [JZCode, GameCode, FactoryCode]));
{$REGION '3--->3.1、存储赛事信息'}
    FInfo.ID := 0;
    FInfo.SetCode := Format('A%0.6d', [FCount]);
    FInfo.CollectID := GetNewCollectID();
    FInfo.CollecteDate := FCoteDB.CurrTime;
    if btnUpload.Down then FInfo.SynFlag:= 'A' else FInfo.SynFlag:= 'O';
    if btnSms.Down then FInfo.SmsFlag:= 'A' else FInfo.SmsFlag:= 'O';
    try
      FInfo.SaveToDB(DQCollection);
    except
      DQCollection.Cancel;
      Dec(FCurrentCollectID);
      AddInfo(Format('%x(%x)', [JZCode, FactoryCode]), '重复集鸽', COL_PROMPT);
      writeLog(LOG_DEBUG, 'Collection', Format('重复集鸽数据(%x-%x)放弃。',
        [JZCode, FactoryCode])); // 存储集鸽数据
      Exit;
    end;
    //writeLog(LOG_DEBUG, 'EventOfReceived', 'save over');
{$ENDREGION}
{$REGION '3---> 3.2、显示集鸽记录,发送短信'}
    MemoryCollected(FClients[0], FInfo.ID);         //显示集鸽记录,发送短信
    WavBeep;
    FLastFCode := 0;
    FIsCollected := true;
    FTryToWriteCount:= 0;
{$ENDREGION}
  end else if(FLastFCode = FactoryCode)then begin
{$region '2--->写环后验证'}
    if(FTryToWriteCount = 0)then //第一次写入失败，进行第二次写入，在失败则不再写入
      FComm.WriteGameCode(JZCode, FInfo.GameCode)
    else DlgInfo('提示', '写环失败，请先集另一只鸽子，再集这只鸽子！', [], 3);      //连续写入失败，换环子再尝试
    Inc(FTryToWriteCount);
{$ENDREGION}
  end else begin
    FLastJCode := JZCode;
    FPigeonView:= TPigeonView.Create;
    try
{$REGION '1---> 1.1、检查入棚信息，检查入棚状态及标记'}
    if not FPigeonView.LoadData(FCoteDB.AdoConn, 'FactoryCode=' + IntToStr(FactoryCode)) then begin
      AddInfo(Format('%x(%x)', [JZCode, FactoryCode]), '尚未入棚', COL_FAILED);
      writeLog(LOG_DEBUG, 'Collection', Format('尚未入棚(%x-%x)放弃。',
        [JZCode, FactoryCode]));
      Exit;
    end;
    // 提取原始信息入棚状态，提示警告，拒绝集鸽
    if FPigeonView.Status <> '正常' then begin
      DlgInfo('警告', FPigeonView.GBCode + '处于' + FPigeonView.Status +
        '状态，系统不允许集鸽.', 3);
      Exit;
    end;
    // 提取原始信息缴费状态，提示警告，拒绝集鸽
    if FPigeonView.Cashkind <> '正常' then begin
      DlgInfo('警告', '该鸽主处于' + FPigeonView.Cashkind +
        '状态，系统不允许集鸽.', 3);
      Exit;
    end;
    FInfo.PigeonIdx:= FPigeonView.ID;
    FInfo.GameCode := FGameCodes^[TCollectionClientInfo(FClients[0]).ClientCollectId];
{$ENDREGION}
{$REGION '1---> 1.2、检查是否重复集鸽，得到GBCode'}
    with DQCheckDbl_W do try
      Parameters[0].Value:= FactoryCode;
      Parameters[1].Value:= GuidToString(FMatchInfo.ID);
      Open;
      if not EOF then begin
        AddInfo(Fields[1].AsString, '已集鸽', COL_PROMPT);
        writeLog(LOG_DEBUG, 'Collection', Format('已集鸽数据(%x-%x)放弃。',
          [JZCode, FactoryCode])); // 存储集鸽数据
        Exit;
      end; // 提取原始信息（未转换的信息）入棚记录，未入棚不能集鸽
    finally
      Close;
    end;
{$ENDREGION}
      FLastFCode := FactoryCode;
      // 提取原始信息入棚状态，提示警告
      if FPigeonView.Flag <> '' then
        DlgInfo('警告', FPigeonView.GBCode + '标记为' + FPigeonView.Flag, [], 5);
    finally
      FPigeonView.Free;
    end;
    // 1.3、写入赛事编号，在写成功时添加到集鸽记录中
    writeLog(LOG_DEBUG, 'EventOfReceived',
      Format('Write Status=%s JZCode=%x, GameCode=%x, FactoryCode=%x',
      [BoolToStr(FIsCollected, true), JZCode, FInfo.GameCode, FactoryCode]));
    FLastJCode := FComm.Rings.CheckRing(FactoryCode);       //用厂商码还原出厂JZCode
    FComm.WriteGameCode(FLastJCode, FInfo.GameCode);         //发送写环命令
    FIsCollected := false;
    FTryToWriteCount:= 0;
  end;
end;

procedure TFCollection.EventOfWrite(Successed: Boolean);
begin
  if FCoteDB.RingKind = '只读环' then EventOfWrite_W(Successed)
  else if FCoteDB.RingKind = '读写环' then EventOfWrite_W(Successed)
  else if FCoteDB.RingKind = '高频只读' then EventOfWrite_W(Successed)
  else if FCoteDB.RingKind = '高频读写' then EventOfWrite_W(Successed);
end;

procedure TFCollection.EventOfWrite_W(Successed: Boolean);
var
  Idx: Integer;
begin
  if not Successed then
    writeLog(LOG_DEBUG, 'Collection', Format('写环错误 %s %x %x, %s',
      [BoolToStr(FIsCollected, true), FInfo.GameCode, FLastFCode, LName.Caption]))
  else
    writeLog(LOG_DEBUG, 'Collection', Format('写环成功 %s %x %x, %s',
      [BoolToStr(FIsCollected, true), FInfo.GameCode, FLastFCode, LName.Caption]));
end;

procedure TFCollection.btnStopClick(Sender: TObject);
var
  I: Integer;
begin
  if MessageDlg('真的要结束集鸽吗，一旦结束该赛事将不能再集鸽？', mtInformation, [mbOK, mbCancel], 0)
    = mrOK then
  begin
    UpdateMatchStatus(JZS_TRAN);
    if FileExists(FCoteDB.AppPath + GuidToString(FMatchInfo.ID) + '_C.dat') then
      DeleteFile(FCoteDB.AppPath + GuidToString(FMatchInfo.ID) + '_C.dat');
    Close;
  end;
  // with FCoteDB.FTranCodes do
  // for i:= 0 to Count - 1 do begin
  // DCDelPigeon.Parameters[0].Value:= Names[i];
  // DCDelPigeon.Execute;
  // end;
  // DeleteFile(PAnsiChar(ExtractFilePath(Application.ExeName) + '.TRAN'));
end;

procedure TFCollection.btnVideoClick(Sender: TObject);
begin
  DlgTextOpen();
  FDlgCamera.IniFilename := FCoteDB.ConfigFilename;
  FDlgCamera.Caption:= '公示屏';
end;

procedure TFCollection.btnPauseClick(Sender: TObject);
begin
  Close;
end;

procedure TFCollection.AddCollectionCount;
begin
  Inc(FCount);
  FrameViewBar1.LCount.Caption := IntToStr(FCount);
end;

procedure TFCollection.PrintACollection(Q: TCustomADODataSet);
var
  szLine, szStr: string;
  I, idx, szWidth: Integer;
  function FixWidthStr(Str: AnsiString; FixWidth: Integer): string;
  begin
    if Length(Str) > FixWidth then
      Result := Copy(Str, 1, FixWidth)
    else
      Result := Space((FixWidth - Length(Str)) div 2) + Str +
        Space((FixWidth - Length(Str) + 1) div 2);
  end;
begin
  if FPrintCount mod FPageLineCount = 0 then
  begin
    printer.Title:= '即时打印';
    printline(FPrinter, '（' + LName.Caption + '）集鸽记录      ' + DateToStr(Now));
    printline(FPrinter, '赛事名称：' + LName.Caption + '      放飞地：' +
      LGameAddress.Caption + '      司放人：' + LManager.Caption);
    printline(FPrinter, '赛事距离：' +
      LGameLong.Caption + '      集鸽时间：' + DateToStr(Now));
    szLine:= '';
    for I := 0 to FPrintTitles.Count - 1 do
    begin
      idx:= pos(':', FPrintTitles.ValueFromIndex[I]);
      szWidth:= StrToInt(Copy(FPrintTitles.ValueFromIndex[I], idx + 1, MaxInt));
      szLine := szLine + '+' + Space(szWidth, '-');
      szStr := szStr + '|' + FixWidthStr(Copy(FPrintTitles.ValueFromIndex[I], 1, idx - 1), szWidth);
    end;
    printline(FPrinter, szLine + #13#10 + szStr + #13#10 + szLine);
  end;
  szStr:= '';
  for I := 0 to FPrintTitles.Count - 1 do
  begin
    idx:= pos(':', FPrintTitles.ValueFromIndex[I]);
    szWidth:= StrToInt(Copy(FPrintTitles.ValueFromIndex[I], idx + 1, MaxInt));
    szStr := szStr + '|' + FixWidthStr(Q.FieldByName(FPrintTitles.Names[I]).AsString, szWidth);
  end;
  printline(FPrinter, szStr);
  Inc(FPrintCount);
end;

procedure TFCollection.BtnPrintClick(Sender: TObject);
begin
  FAutoPrint:= BtnPrint.Down;
end;

procedure TFCollection.btnReconnectClick(Sender: TObject);
begin
  if btnReconnect.Down then begin
    BeginToCollection(EComm.ItemIndex + 1, StrToInt(ECommRate.Text));
    WriteAppConfig('COMMUNICATION', 'Port', EComm.Text);
    WriteAppConfig('COMMUNICATION', 'Speed', ECommRate.Text);
  end else FCoteDB.CloseComm(FComm);
end;
            
procedure TFCollection.MemoryCollected(P: TCollectionClientInfo; ID: Integer);
begin
  if FCount = 0 then FCoteDB.ADOConn.Execute('UPDATE ' + Prefix +
    'Match SET Status=3 WHERE ID=''' + GuidToString(FMatchInfo.ID) + '''');
  P.IncCount;
  Inc(FCount);
  writeLog(LOG_DEBUG, 'Display and sms', Format('第%d/%d集鸽赛鸽，在%s.', [P.FClientCollectId, FCount, Char(P.FID)]));
  FSourceMemory.Push(TFIFOItem.Create(P, ID));
end;

procedure TFCollection.Collected(P: TCollectionClientInfo; ID: Integer);
var
  FCollectCommBuff: array[0..25]of byte;
  Q: TADOQuery;
  GBCode: string;
begin
{$region '显示数量及笼号信息'}
  FrameViewBar1.LCount.Caption := Format('合计集鸽%d羽', [FCount]);
//  if FMultiMode = '主机显示打印' then
//    FrameViewBar1.LCount.Caption := Format('合计集鸽%d羽，其中本机集鸽%d羽',
//      [FCount, TCollectionClientInfo(FClients[0]).ClientCollectId])
//  else FrameViewBar1.LCount.Caption := Format('已集鸽%d羽', [FCount]);
  if(P.FID = 65)then begin     //本机入笼支数控制
    if ECountToBeep.Value > 0 then begin
      Inc(FCageNum);
      LCountOfBatch.Caption := '当前正在集第' + IntToStr(FCage) +
          '笼赛鸽，当前笼内已入' + IntToStr(FCageNum) + '羽。';
      if FCageNum >= ECountToBeep.Value then
      begin
        DlgInfo('提示', '该笼已满！', 3);
        Inc(FCage); FCageNum:= 0;
        WavBeep(FCoteDB.AppPath + 'Styles\提示音音效\满笼.wav');
      end else WavBeep(FCoteDB.AppPath + 'Styles\提示音音效\集鸽.wav');
    end else WavBeep(FCoteDB.AppPath + 'Styles\提示音音效\集鸽.wav');
  end;
{$endregion}
{$REGION '显示集鸽记录,发送短信'}
  Q:= TSourceView.OpenQuery(nil, 'S.ID=' + IntToStr(ID));
  GBCode:= Q.FieldByName('GBCode').AsString;
  if(P.FID = 65)or(FMultiMode = '主机显示打印') then begin
      writeLog(LOG_FAIL, 'Collection', 'DisplayACollection ' + GBCode);
    DisplayACollection(Q);    
    SendMessage(FrameViewBar1.lstView.Handle, WM_VSCROLL, SB_BOTTOM, 0);
    writeLog(LOG_FAIL, 'Collection', 'SmsACollection ' + GBCode);
 //   if btnSms.Down then SmsACollection(Q);
    Say(Copy(GBCode, Length(GBCode) - FSayCount + 1, FSayCount));
      writeLog(LOG_FAIL, 'Collection', 'PrintACollection ' + GBCode);
    if FAutoPrint then PrintACollection(Q); //打印集鸽单
      writeLog(LOG_FAIL, 'Collection', 'AddInfo ' + GBCode);
    AddInfo(GBCode, '完成集鸽', COL_SUCCES);
  end;
  Q.Free;
{$ENDREGION}
  writeLog(LOG_DEBUG, 'Collection', Format('1.赛鸽 %s 已经在%s集鸽%d到 %s 赛事中.',
    [GBCode, Char(P.ID), P.ClientCollectId, LName.Caption]));
end;

function TFCollection.CompareDT(ADate, BDate: TDateTime): Boolean;
var
  AYear, AMonth, ADay, AHour, AMinute, ASecond, AMilliSecond: Word;
  BYear, BMonth, BDay, BHour, BMinute, BSecond, BMilliSecond: Word;
begin
  DecodeDateTime(ADate, AYear, AMonth, ADay, AHour, AMinute, ASecond,
    AMilliSecond);
  DecodeDateTime(BDate, BYear, BMonth, BDay, BHour, BMinute, BSecond,
    BMilliSecond);
  Result := (AYear = BYear) and (AMonth = BMonth) and (ADay = BDay) and
    (AHour = BHour) and (AMinute = BMinute) and (ASecond = BSecond);
end;

(* 扫环信息显示区域相关函数 *)

procedure TFCollection.plHintResize(Sender: TObject);
begin
  ImgInfo1.Width:= plHint.Width div 3;
  ImgInfo2.Width:= ImgInfo1.Width;
  ImgInfo3.Width:= ImgInfo1.Width;
  ImgInfo1.Picture.Bitmap.Width:= ImgInfo1.Width;
  ImgInfo1.Picture.Bitmap.Height:= ImgInfo1.Height;
  ImgInfo1.Picture.Bitmap.Canvas.Brush.Color:= clBlack;
  ImgInfo1.Picture.Bitmap.Canvas.Brush.Style:= bsSolid;
  ImgInfo1.Picture.Bitmap.Canvas.Pen.Color:= clWhite;
  ImgInfo1.Picture.Bitmap.Canvas.Font.Color:= clWhite;
end;

procedure TFCollection.AddInfo(Code, Info: string; InfoKind: EWarningKind);
begin
  FInfoMemory.Push(TFIFOItem.Create(TLedInfo.Create(Code, Info, InfoKind), 0));
end;

function TFCollection.FindCollection(ClientID, RegCode: byte): TCollectionClientInfo;
var
  I: Integer;
begin
  for I := 0 to FClients.Count - 1 do
    if(ClientID = TCollectionClientInfo(FClients[I]).ID)and(RegCode = TCollectionClientInfo(FClients[I]).Reg)then begin
      Result:= TCollectionClientInfo(FClients[I]);
      Exit;
    end;
  Result:= nil;
end;
(* 多机集鸽通讯机制，05作为起始码+客户端编码+客户端服务器注册码+5位功能名+内容    ┴ ┘┐┤┼│─├┌└ ┬
 =============================================================================
   ┌───────────┐                                              ┌──────────┐
   │  Client   │    <------------------------------------>    │  Server  │
   └─────┬─────┘                                              └─────┬────┘
         ├──── 登录 ──── 05<CLIENT_NO><1byte>LOGIN<MAC_ADDRESS> ───>│
         │<───── S ───── 05SUCCES<认证码> ──────────────────────────┤
         │                                                          │
         ├── 获取环库 ── 05<CLIENT_NO><认证码>RINGS ───────────────>│
         │<───── S ───── 05SUCCES<RingsList> ───────────────────────┤
         │                                                          │
         ├── 获取参数 ── 05<CLIENT_NO><认证码>PARAM ───────────────>│
         │<───── S ───── 05SUCCES<ParamList> ───────────────────────┤
         │                                                          │
         ├── 分机集鸽 ── 05<CLIENT_NO><认证码>COLIN<CollectID> ────>│
         │<───── S ───── 05SUCCES<SumCount> ────────────────────────┤
         │                                                          │
         ├── 分机心跳 ── 05<CLIENT_NO><认证码>HEART ───────────────>│
         │<───── S ───── 05SUCCES<SumCount> ────────────────────────┤
         │                                                          │
         │<───── F ───── 05FAILED|WARNIN|PROMPT<error info> ────────┤
 ─────────────────┬────────────────────────────────────────────────────────────
 <CLIENT_NO>      │ 客户端序号
 <认证码>         │ 服务端认证客户码（1byte）
 <RingsList>      │ JZCode(HEX[16]) + GameCode(HEX[8]) + FactoryCode(HEX[16])
 <error info>     │ 错误信息
 ─────────────────┴───────────────────────────────────────────────────────────*)
procedure TFCollection.EventOfServer(var Data: array of byte; var Len: Integer; size: Integer);
var
  Cmd: string;
  P: TCollectionClientInfo;
begin
  if Data[0] <> 5 then begin Len:= 0; Exit; end;
  try
    Cmd:= BytesToStr(Data, 3, 5);
    if Cmd = 'LOGIN' then begin Len:= EventLogin(Data, size); Exit; end;
    P:= FindCollection(Data[1], Data[2]);
    if not Assigned(P) then begin
      AddInfo('远程客户端' + char(Data[1]), '认证失败', EWarningKind.COL_FAILED);
      Exit;
    end;// else AddInfo('远程客户端' + char(Data[1]), '认证成功', EWarningKind.COL_SUCCES);
    P.FLasterTimer:= Now;
    if Cmd = 'RINGS' then Len:= EventGetRings(Data, Size)
    else if Cmd = 'PARAM' then Len:= EventGetParams(Data, Size)
    else if Cmd = 'COLIN' then Len:= EventClientCollected(P, Data, Size)
    else if Cmd = 'MAXID' then Len:= EventClientGetCollectID(P, Data, Size)
    else if Cmd = 'HEART' then Len:= EventClientHeartbeat(P, Data, Size);
  except
    on E: Exception do begin
      Len:= StrToBytes(E.Message, Data[1], Size) + 1;
    end;
  end;
end;

procedure TFCollection.IncCount(P: TCollectionClientInfo);
begin
  Inc(FCount);     //合计集鸽数
  P.IncCount;      //本机几个数
  FrameViewBar1.LCount.Caption := Format('共集鸽%d羽，其中本机集鸽%d羽。',
    [FCount, TCollectionClientInfo(FClients[0]).FClientCollectId]);
  if FCount = 1 then UpdateMatchStatus(JZS_COLL);// 当第一羽信鸽集鸽时，修改赛事状态
  if(P = FClients[0])and(ECountToBeep.Value > 0)then begin             // 本机
    Inc(FCageNum); //本机入笼数
    LCountOfBatch.Caption := Format('已经集鸽到第%d笼中第%d羽', [FCage, FCageNum]);
    if FCageNum = ECountToBeep.Value then begin
      DlgInfo('提示', '该笼已满！开始下一笼的集鸽。', 3);
      FCageNum:= 0; Inc(FCage);
    end;
  end;
  if P = FClients[0] then WavBeep;
end;

procedure TFCollection.ClearClient;
var
  I: Integer;
begin
  if Assigned(FSvr) then begin
    FSvr.Close;
    FreeAndNil(FSvr);
  end;
  for I := 0 to FClients.Count - 1 do
    TCollectionClientInfo(FClients[I]).Free;
  FClients.Clear;
  FClients.Add(TCollectionClientInfo.Create(65, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    plClients, FMatchInfo.ID));
  WriteLog(Log_Debug, 'ClearClient', 'Clear clients of multi collection.');
end;

(* ┌───────────┐                                              ┌──────────┐
   │  Client   │    <------------------------------------>    │  Server  │
   └─────┬─────┘                                              └─────┬────┘
         ├──── 登录 ──── 05<CLIENT_NO><1byte>LOGIN<MAC_ADDRESS> ───>│
         │<───── S ───── 05SUCCES<认证码> ──────────────────────────┤
         │<───── F ───── 05FAILED<error info> ──────────────────────┤         *)
function TFCollection.EventLogin(var Data: array of byte; size: Integer): Integer;
var
  I, J: Integer;
  B: Boolean;
  P: TCollectionClientInfo;
  function SetData(C: TCollectionClientInfo): Integer;
  var S: AnsiString;
  begin
    StrToBytes('SUCCES', Data[1], size);
    Data[7]:= C.Reg;
    Result:= 8;
  end;
begin    //登录 05<CLIENT_NO><1byte>LOGIN<MAC_ADDRESS> 1,1,1,5,10
  for I := 0 to FClients.Count - 1 do begin
    if TCollectionClientInfo(FClients[I]).ID = Data[1] then begin //检查分机号是否存在
      B:= true;                                                   //如果存在检查MAC是否一致
      for J := 0 to 9 do
        B:= B and (TCollectionClientInfo(FClients[I]).FMAC[J] = Data[8 + J]);
      if B then begin                                             //重复登录，返回以前登录记录
        Result:= SetData(TCollectionClientInfo(FClients[I]));
        Exit;
      end else raise Exception.Create('FAILED该客户端编号已经被占用');   //序号重复报错
    end;
  end;   //没有发现分机号被使用，登录成功，记录并缓存
  P:= TCollectionClientInfo.Create(Data[1], Data[8], plClients, FMatchInfo.ID);
  FClients.Add(P);
  Result:= SetData(P);
end;
(* ┌───────────┐                                              ┌──────────┐
   │  Client   │    <----------- 获取系统换库 ----------->    │  Server  │
   └─────┬─────┘                                              └─────┬────┘
         ├── 获取环库 ── 05<CLIENT_NO><认证码>RINGS ───────────────>│
         │<───── S ───── 05SUCCES<RingsList> ───────────────────────┤
         │<───── F ───── 05FAILED|WARNIN|PROMPT<error info> ────────┤         *)
function TFCollection.EventGetRings(var Data: array of byte; size: Integer): Integer;
var
  I, J: Cardinal;
begin
  Move(Data[8], I, 4);
  WriteLog(LOG_INFO, 'EventGetRings', 'Get rings from ' + IntToStr(I) + '/' + IntToStr(FComm.Rings.Count));
  StrToBytes(#05'SUCCES', Data, size);
  Result:= 7;
  while I < FComm.Rings.Count do begin
    with FComm.Rings.Rings[I] do begin
      Move(JZCode, Data[Result], 8);
      Move(FactoryCode, Data[Result + 8], 8);
    end;
    Result:= Result + 16;
    Inc(I);
    //(2048 - 7) / 16 ~ 127, 127 * 16 + 7 = 2039，所以只要大于2030就表示是最后一组了，在多就会超出缓存界限，需要等待下一次传输
    if Result > 2030 then Exit;
  end;
end;
(* ┌───────────┐                                              ┌──────────┐
   │  Client   │    <----------- 获取系统参数 ----------->    │  Server  │
   └─────┬─────┘                                              └─────┬────┘
         ├── 获取参数 ── 05<CLIENT_NO><认证码>PARAM ───────────────>│
         │<───── S ───── 05SUCCES<ParamList> ───────────────────────┤
         │<───── F ───── 05FAILED|WARNIN|PROMPT<error info> ────────┤         *)
function TFCollection.EventGetParams(var Data: array of byte; size: Integer): Integer;
var
  S: string;
begin
  Result:= StrToBytes(#5'SUCCES', Data, size);
  S:= 'Provider=SQLOLEDB.1;Password=nd001;Persist Security Info=True;User ID=sa;Initial Catalog=Pigeon2020;Data Source=' + GetLocalIP + ',1433\SqlExpress';
  Result:= Result + StrToBytes('ConnectStr=' + S, Data[Result], size);
  Result:= Result + StrToBytes(#13#10'RingKind=' + FCoteDB.RingKind, Data[Result], size);
  Result:= Result + StrToBytes(#13#10'MatchID=' + GuidToString(FMatchInfo.ID), Data[Result], size);
  Result:= Result + StrToBytes(#13#10'CollectDisplayFields=' + ReadAppConfig('FIELDS', 'CollectDisplayFields'), Data[Result], size);
  Result:= Result + StrToBytes(#13#10'CollectPrintFields=' + ReadAppConfig('FIELDS', 'CollectPrintFields'), Data[Result], size);
  Result:= Result + StrToBytes(#13#10'SMSTitles=' + ReadAppConfig('FIELDS', 'SMSTitles'), Data[Result], size);
  Result:= Result + StrToBytes(#13#10'SmsMsgFormat=' + FSmsMsgFormat, Data[Result], size);
  Result:= Result + StrToBytes(#13#10'MultiMode=' + FMultiMode, Data[Result], size);
  Result:= Result + StrToBytes(#13#10'PageLineCount=' + IntToStr(FPageLineCount), Data[Result], size);
  Result:= Result + StrToBytes(#13#10'DisplayCollenctCount=' + BoolToStr(FrameViewBar1.LCount.Visible, true), Data[Result], size);
  WriteLog(LOG_Debug, 'EventGetParams', 'Will send ' + IntToStr(Result) + ' bytes.');
end;
(* ┌───────────┐                                              ┌──────────┐
   │  Client   │    <------------------------------------>    │  Server  │
   └─────┬─────┘                                              └─────┬────┘
         ├── 分机集鸽 ── 05<CLIENT_NO><认证码>COLIN<CollectID> ────>│
         │<───── S ───── 05SUCCES<SumCount> ────────────────────────┤
         │<───── F ───── 05FAILED<error info> ──────────────────────┤         *)
function TFCollection.EventClientCollected(P: TCollectionClientInfo; var Data: array of byte; size: Integer): Integer;
var
  szSourceID: Integer;
begin    //分机集鸽  05<CLIENT_NO><认证码>COLIN<CollectID>
  Move(Data[8], szSourceID, 4);
  WriteLog(LOG_INFO, 'EventClientCollected', 'Collected ' + IntToStr(szSourceID) + ' from ' + Char(P.ID));   
  MemoryCollected(P, szSourceID);         //显示集鸽记录,发送短信
  Result:= StrToBytes(#5'SUCCES', Data[7], size);
  Move(FCount, Data[Result], 4);
  Inc(Result, 4);
end;
(* ┌───────────┐                                              ┌──────────┐
   │  Client   │    <------------------------------------>    │  Server  │
   └─────┬─────┘                                              └─────┬────┘
         ├── 分机集鸽 ── 05<CLIENT_NO><认证码>HEART ───────────────>│
         │<───── S ───── 05SUCCES<SumCount> ────────────────────────┤         *)
function TFCollection.EventClientHeartbeat(P: TCollectionClientInfo; var Data: array of byte; size: Integer): Integer;
var
  szSourceID: Integer;
begin    //分机集鸽  05<CLIENT_NO><认证码>HEART
  //WriteLog(LOG_INFO, 'EventClientCollected', 'Heartbeat from ' + Char(P.ID));
  Result:= StrToBytes(#5'SUCCES', Data[0], size);
  Move(FCount, Data[Result], 4);
  Inc(Result, 4);
end;
//(* ┌───────────┐                                              ┌──────────┐
//   │  Client   │    <------------------------------------>    │  Server  │
//   └─────┬─────┘                                              └─────┬────┘
//         ├── 集鸽序号 ── 05<CLIENT_NO><认证码>MAXID ───────────────>│
//         │<───── S ───── 05SUCCES<CollectID> ────────────────────────┤         *)
function TFCollection.EventClientGetCollectID(P: TCollectionClientInfo; var Data: array of byte; size: Integer): Integer;
var
  szID: Integer;
begin    //集鸽序号  05<CLIENT_NO><认证码>MAXID
  szID:= GetNewCollectID();
  WriteLog(LOG_INFO, 'EventClientGetCollectID', 'From ' + Char(P.ID) + ' CurrentCollectID=' + IntToStr(szID));
  Result:= StrToBytes(#5'SUCCES', Data, size);
  Move(szID, Data[Result], 4);
  Inc(Result, 4);
end;

procedure TFCollection.btnConnectClick(Sender: TObject);
begin
  if btnConnect.Down then begin
    WriteLog(Log_Debug, 'MultiCollection', 'Multi client to collect.');
    FSvr:= TVonTCPServer.Create;
    FSvr.TimeOut:= 200;
    FSvr.Params['Port']:= ReadAppConfig('MultiCollection', 'Port');
    FSvr.OnReceiver:= EventOfServer;
    FSvr.Open;
  end else ClearClient;
end;

{ TCollectionClientInfo }

constructor TCollectionClientInfo.Create(ClientID: Byte; const MAC: array of byte; AOwner: TWinControl; MatchId: TGuid);
begin
  FID:= ClientID;
  FMAC[0]:= MAC[0];
  FMAC[1]:= MAC[1];
  FMAC[2]:= MAC[2];
  FMAC[3]:= MAC[3];
  FMAC[4]:= MAC[4];
  FMAC[5]:= MAC[5];
  FMAC[6]:= MAC[6];
  FMAC[7]:= MAC[7];
  FMAC[8]:= MAC[8];
  FMAC[9]:= MAC[9];
  FReg:= RandomRange(Ord('a'), Ord('z'));
  FLasterTimer:= Now;
  FImg:= TImage.Create(nil);
  FImg.Parent:= AOwner;
  FImg.Picture.LoadFromFile(FCoteDB.AppPath + 'Styles\Client.bmp');
  FImg.Align:= alTop;
  FLasterRing:= 0;
  FClientCollectId:= FCoteDB.ADOConn.Execute('SELECT COUNT(*) FROM ' + Prefix +
    'GameSource WHERE Left(SetCode, 1)=''' + Char(ID) + ''' AND [GameIdx]=''' +
    GuidToString(MatchID) + '''').Fields[0].Value;
  FImg.Picture.Bitmap.Canvas.Brush.Style:= bsClear;
  FImg.Picture.Bitmap.Canvas.TextOut(31, 48, Char(ID));
end;

destructor TCollectionClientInfo.Destroy;
begin
  FImg.Free;
  inherited;
end;

procedure TCollectionClientInfo.IncCount;
begin
  Inc(FClientCollectId);
  FLasterTimer:= Now;
end;

{ TLedInfo }

constructor TLedInfo.create(ACode, AInfo: string; AInfoKind: EWarningKind);
begin
  Code:= ACode;
  Info:= AInfo;
  InfoKind:= AInfoKind;
end;

end.
