unit UCoteCollectionSearch;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, ImgList, DB, ExtCtrls, ComCtrls, ToolWin, Grids, DBGrids,
  StdCtrls, Buttons, UFrameGridBar, Menus, System.ImageList;

type
  TFCoteCollectionSearch = class(TForm)
    Splitter1: TSplitter;
    Panel1: TPanel;
    DBGrid2: TDBGrid;
    GroupBox2: TGroupBox;
    BitBtn2: TBitBtn;
    DQStatistic: TADOQuery;
    ImageList1: TImageList;
    DBGame: TADOTable;
    DSGame: TDataSource;
    DQMemberID: TADOQuery;
    FrameGridBar1: TFrameGridBar;
    Panel2: TPanel;
    plChangeMember: TPanel;
    lbChangeMember: TLabel;
    EMember: TComboBox;
    BitBtn3: TBitBtn;
    Label2: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    BtnReset: TBitBtn;
    BitBtn1: TBitBtn;
    EKeyWord: TEdit;
    EGBCode: TEdit;
    EMemberID: TEdit;
    EMemberName: TEdit;
    EFromDate: TDateTimePicker;
    EToDate: TDateTimePicker;
    procedure FormCreate(Sender: TObject);
    procedure BtnResetClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FrameGridBar1menuConditionClick(Sender: TObject);
    procedure lbChangeMemberDblClick(Sender: TObject);
  private
    { Private declarations }
    FSQLText: string;
  public
    { Public declarations }
  end;

var
  FCoteCollectionSearch: TFCoteCollectionSearch;

implementation

uses UCoteDB;

{$R *.dfm}

procedure TFCoteCollectionSearch.BitBtn1Click(Sender: TObject);
var
  SQLText: string;
  i: Integer;
begin
  if DBGame.RecordCount = 0 then
    Exit;
  SQLText := '';
  if EMemberName.Text <> '' then
    SQLText := SQLText + ' AND M.[MemberName] LIKE ''%' +
      EMemberName.Text + '%''';
  if EMemberID.Text <> '' then
    SQLText := SQLText + ' AND MemberID =' + EMemberID.Text;
  if EGBCode.Text <> '' then
    SQLText := SQLText + ' AND P.GBCode LIKE ''%' + EGBCode.Text + '%''';
  if EFromDate.Checked then
    SQLText := SQLText + ' AND JoinDate>''' + DateToStr(EFromDate.Date) + '''';
  if EToDate.Checked then
    SQLText := SQLText + ' AND JoinDate<''' + DateToStr(EToDate.Date) + '''';
  if EKeyWord.Text <> '' then
    SQLText := SQLText + ' AND P.Note LIKE ''%' + EKeyWord.Text + '%''';
  with DQStatistic do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT S.ID,S.[CollecteDate],S.[CollecteNo],S.[CollectID],P.[ID] ' +
      'AS PID, P.[PigeonNo],P.JZCode,S.GameCode,M.[MemberNo], M.[MemberName], M.[Mobile],' +
      'M.[State], M.[City], M.[County], P.[GBCode],P.[Feather],P.[Eye],P.[Gender], ' +
      'P.[Group1], P.[Group2], P.[Group3], P.[Group4], P.[Group5],P.[JoinDate]');
    SQL.Add('FROM JOIN_GameSource S');
    SQL.Add('JOIN JOIN_Pigeon P ON P.[ID]=S.[PigeonIDX]');
    SQL.Add('JOIN JOIN_Match G ON G.[ID]=S.[GameIDX]');
    SQL.Add('JOIN JOIN_Member M ON P.[MemberIDX] = M.[ID]');
    SQL.Add('WHERE [GameIDX]=:GID');
    SQL.Add(SQLText);
    Parameters[0].Value := DBGame.FieldByName('ID').AsString;
    Open;
    FrameGridBar1.StatusBar1.Panels[1].Text := '查询鸽子数量为：' + IntToStr(RecordCount);
  end;
end;

procedure TFCoteCollectionSearch.BitBtn2Click(Sender: TObject);
var
  SQLText: string;
  i: Integer;
begin
  if DBGame.RecordCount = 0 then
    Exit;
  SQLText := '';
  with DQStatistic do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT ''--'' AS [CollectionDate], ''--'' AS [CollectionNo], ' +
      'P.[PigeonNo],M.[MemberName],M.[State],M.[City], M.[County], [GBCode], ' +
      'P.[JZCode],P.[Feather],P.[Eye],P.[Gender],P.[Group1], P.[Group2], ' +
      'P.[Group3], P.[Group4], P.[Group5], P.[JoinDate]');
    SQL.Add('FROM JOIN_Pigeon P');
    SQL.Add('JOIN JOIN_Member M ON P.[MemberIDX]=M.[ID]');
    SQL.Add('WHERE P.[ID] NOT IN');
    SQL.Add('(SELECT [PigeonIDX] FROM JOIN_GameSource WHERE [GameIDX]=''' +
      DBGame.FieldByName('ID').AsString + ''')');
    Open;
    FrameGridBar1.StatusBar1.Panels[1].Text := '查询鸽子数量为：' + IntToStr(RecordCount);
  end;
  BtnResetClick(nil);
end;

procedure TFCoteCollectionSearch.BtnResetClick(Sender: TObject);
begin
  EMemberName.Text := '';
  EMemberID.Text := '';
  EGBCode.Text := '';
  EKeyWord.Text := '';
  EFromDate.DateTime := Now;
  EToDate.DateTime := Now;
  EFromDate.Checked := false;
  EToDate.Checked := false;
end;

procedure TFCoteCollectionSearch.FormCreate(Sender: TObject);
begin
  FrameGridBar1.LoadTitles('CollectionSearch');
  DBGame.Open;
  FSQLText := DQStatistic.SQL.Text;
  EFromDate.DateTime := Now;
  EToDate.DateTime := Now;
  EFromDate.Checked := false;
  EToDate.Checked := false;
end;

procedure TFCoteCollectionSearch.FrameGridBar1menuConditionClick(
  Sender: TObject);
begin
  FrameGridBar1.menuConditionClick(Sender);
  plChangeMember.Visible := not FrameGridBar1.menuCondition.Checked;
end;

procedure TFCoteCollectionSearch.lbChangeMemberDblClick(Sender: TObject);
var
  mid: PGuid;
begin
   plChangeMember.Width:= 232 + 48 - plChangeMember.Width;
  if EMember.Items.Count = 0 then
    with TADOQuery.Create(nil) do try
      Connection:= FCoteDB.ADOConn;
      SQL.Text:= 'SELECT ID,MemberName,MemberNo FROM JOIN_Member';
      Open;
      while not EOF do begin
        New(mid); mid^:= FieldByName('ID').AsGuid;
        EMember.Items.AddObject(FieldByName('MemberName').AsString + '(' +
          FieldByName('MemberNo').AsString + ')', TObject(mid));
        Next;
      end;
    finally
      Free;
    end;
end;

procedure TFCoteCollectionSearch.BitBtn3Click(Sender: TObject);
var
  mid: string;
begin     //
  mid:= GuidToString(PGuid(EMember.Items.Objects[EMember.ItemIndex])^);
  FCoteDB.ADOConn.Execute('UPDATE JOIN_GameSource SET MemberIdX=''' + mid +
    ''',SynFlag=''U'' WHERE ID=' + DQStatistic.FieldByName('ID').AsString);
  FCoteDB.ADOConn.Execute('UPDATE JOIN_Pigeon SET MemberIdx=''' + mid +
    ''',SynFlag=''U'' WHERE ID=' + DQStatistic.FieldByName('PID').AsString);
  DQStatistic.Close;
  DQStatistic.Open;
end;

end.
