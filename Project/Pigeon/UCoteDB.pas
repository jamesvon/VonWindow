unit UCoteDB;
{$define debug}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, IniFiles, ZLib, ImgList, IdIOHandler, IdComponent,
  IdIOHandlerSocket, IdIOHandlerStack, IdBaseComponent, IdTCPConnection,
  IdTCPClient, IdHTTP, IdIOHandlerStream, HTTPApp, UFrameSettingInputor,
  UPlatformDB, UPlatformInfo, UVonLog, UCoteInfo, System.ImageList,
  UVonCertReader, UCoteComm;

const
  RES_DLG_DEL_SOURCE_PROMPT = '真的要删除该场赛事的成绩吗?';

type
  /// <summary>自动集鸽文件内容信息</summary>
  TJZElectricCodeInfo = record
    JZCode: Int64;
    GameCode: Word;
    FactoryCode: Int64;
  end;

  /// <summary>转换环库文件</summary>
  TJZElectricRings = class
  private
    FRings: array of TJZElectricCodeInfo;
    FCount: Integer;
    FCapacity: Integer;
    FTitle: string;
    procedure Grow;
    function GetRings(Index: Integer): TJZElectricCodeInfo;
  public
    constructor Create;
    destructor Destroy; override;
    function Add(JZCode: Int64; GameCode: Word; FactoryCode: Int64): Integer;
    procedure SaveToFile(Filename: string);
    procedure LoadFromFile(Filename: string);
    procedure SaveToStream(AStream: TStream);
    procedure LoadFromStream(AStream: TStream);
    procedure Delete(Index: Integer);
    procedure Clear;
    function IndexOfJZCode(JZCode: Int64): Integer;
    function IndexOfGameCode(GameCode: Word): Integer;
    function IndexOfFactoryCode(FactoryCode: Int64): Integer;
    property Rings[Index: Integer]: TJZElectricCodeInfo read GetRings;
  published
    property Title: string read FTitle write FTitle;
    property Count: Integer read FCount;
  end;

  TFCoteDB = class(TFPlatformDB)
    DQCheckRight: TADOQuery;
    ImgsEdit: TImageList;
    IdHTTP1: TIdHTTP;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    /// <summary>系统证书</summary>
    Cert: TCert;
    FRingKind: string;
    FSendSMS: Boolean;
    FSMSUrl: string;
    //FComm: TJZCommBase;
    function CalcUliage1(Lon1, Lat1, Lon2, Lat2, EarthRadius: Extended): Extended;
    function CalcUliage2(Lon1, Lat1, Lon2, Lat2: Extended): Extended;
    function CalcUliage3(Lon1, Lat1, Lon2, Lat2, EarthRadius: Extended): Extended;
  public
    { Public declarations }
    FCoteInfo: TCoteInfo;
    FCheckInCode: TJZElectricRings; // 自动归巢
    FTranCodes: TJZElectricRings;   // 转换环号
    FTicketCode: TStringList;
    FGroupNames: array[0..4]of string;  //分组名称集合
    FGroupCount: array[0..4]of Integer; //各分组允许的最大赛鸽数量
    FGroupKeys: array[0..4, 0..52]of string;  //各分组序列值列表
    procedure InitData();
    function EventOfGeneratorSql(OrgTable, LinkTable: string): string;
    //function SendSMS(Mobile, Msg: string): Boolean;
    /// <summary>根据经纬度计算直线距离函数</summary>
    /// <param name="Lon1">本地经度值</param>
    /// <param name="Lat1">本地纬度值</param>
    /// <param name="Lon2">目标地经度值</param>
    /// <param name="Lat2">目标地纬度值</param>
    function CalcUliage(Lon1, Lat1, Lon2, Lat2: Extended): Extended;
    function CreateComm(EventOfCollection: TEventAfterCollectionReceived;
      EventOfRead: TEventAfterReadReceived;
      EventOfWrite: TEventAfterWriteReceived;
      CheckRing: Boolean): TJZCommBase;
    procedure CloseComm(var Comm: TJZCommBase);
//    function SetComm(EventOfCollection: TEventAfterCollectionReceived;
//      EventOfRead: TEventAfterReadReceived;
//      EventOfWrite: TEventAfterWriteReceived;
//      CheckRing: Boolean): TJZCommBase;
    procedure ReinitGroup;
    /// <summary>获取当前分组中指定的分组名称已分配的组名数量，如果超出规定数量，系统会自动移动到下一个名称节点</summary>
    /// <param name="GroupIdx">组序号</param>
    /// <param name="MemberIdx">会员序号</param>
    /// <param name="KeyIdx">当前组值序号</param>
    /// <returns>当前组名已分组数量</returns>
    function GetMemberGroupValue(GroupIdx: Integer; MemberIdx: TGuid; var KeyIdx: Integer): Integer;
    /// <summary>获取当前分组中鸽主最大可以分配的分组号，不允许不足组数量约定的分组出现</summary>
    /// <param name="GroupIdx">组序号</param>
    /// <param name="MemberIdx">会员序号</param>
    /// <returns>当前组名已分组数量</returns>
    function GetMemberMaxGroupKeyId(GroupIdx: Integer; MemberIdx: TGuid): Integer;
    function GetSupportChoose: Integer;
    function GetWebData(URL: string): string;
    function GetSMSCount: Integer;
    function CheckNetwork: boolean;
  published
    //property Comm1: TJZCommBase read FComm;
    property RingKind: string read FRingKind write FRingKind;
  end;

  //转换标题设置内容格式信息，FieldName=Title:width,[...]，转换为列表格式 FieldName=Title,object=width
  function GetDisplayTitles(SectionName, IdentName: string): TStringList;

var
  FCoteDB: TFCoteDB;

implementation

uses UVonSystemFuns, UDlgCote, StrUtils, Math, XMLIntf, XMLDoc, UPlatformUpgrade;

function GetDisplayTitles(SectionName, IdentName: string): TStringList;
var
  I, mPos: Integer;
  S: string;
begin
  Result:= TStringList.Create;
  Result.Delimiter := ',';           // FieldName=Title:width
  Result.DelimitedText := ReadAppConfig(SectionName, IdentName);
  for I := 0 to Result.Count - 1 do begin
    S:= Result.ValueFromIndex[I];
    mPos:= Pos(':', S);
    if mPos > 0 then begin
      Result.ValueFromIndex[I]:= Copy(S, 1, mPos - 1);
      Result.Objects[I]:= TObject(StrToInt(Copy(S, mPos + 1, MaxInt)));
    end;
  end;
end;

{$R *.dfm}

function TFCoteDB.CalcUliage(Lon1, Lat1, Lon2, Lat2: Extended): Extended;
var
  FUliageKind: Integer;
  FEarthRadius: Extended;
begin
  FUliageKind:= StrToInt(ReadAppConfig('SYSTEM', 'ULIAGEKIND'));
  FEarthRadius:= StrToFloat(ReadAppConfig('SYSTEM', 'EARTHRADIUS'));
  case FUliageKind of
  0: Result:= CalcUliage1(Lon1, Lat1, Lon2, Lat2, FEarthRadius);
  1: Result:= CalcUliage2(Lon1, Lat1, Lon2, Lat2);
  2: Result:= CalcUliage3(Lon1, Lat1, Lon2, Lat2, FEarthRadius);
  end;
end;

function TFCoteDB.CalcUliage1(Lon1, Lat1, Lon2, Lat2, EarthRadius: Extended): Extended;
var
  dAlpha, dBeta, dTheta, dCosResult, dResult: Extended;
begin
  dTheta := ABS(Lon1 - Lon2);
  if (dTheta - 180.00 > 0) then
    dTheta := 360.0 - dTheta;
  dAlpha := Lat1 / 180.0 * PI;
  dBeta := Lat2 / 180.0 * PI;
  dTheta := dTheta / 180.0 * PI;
  dCosResult := cos(dAlpha) * cos(dBeta) * cos(dTheta) + sin(dAlpha) *
    sin(dBeta);
  dResult := arccos(dCosResult); // dResult ranges [0,Pi],needn't change
  result := EarthRadius * dResult;
end;

(*//放飞地经纬度：x,y
x=108.281131
y=34.247354
//公棚经纬度：x0,y0
x0=112.562417
y0=37.759991
pi=3.1415926
//取差值
jdz=x-x0 =
wdz=y-y0
//
w0=111.3200144*cos(y*pi/180)
w1=111.3200144*cos(y0*pi/180)
w2=(w0+w1)*jdz/2
//平方根
w3=w2^2
h0=110.9481458*wdz
h1=h0^2
dd=(w3+h1)
//小数点保留四位，不取四舍五入
ddj=sqr(dd)
//测试数据位上面
单位：公里
ddj=548.0823*)
function TFCoteDB.CalcUliage2(Lon1, Lat1, Lon2, Lat2: Extended): Extended;
const
  pi: extended = 3.1415926;
var
  jdz, wdz, w0, w1, w2, h1: Extended;
begin
  jdz:= Lon1 - Lon2;
  wdz:= Lat1 - Lat2;
  w0:= 111.3200144 * cos(Lat1 * pi / 180);
  w1:= 111.3200144 * cos(Lat2 * pi / 180);
  w2:= (w0 + w1) * jdz / 2;
  h1:= 110.9481458 * wdz;
  Result:= Sqrt(w2 * w2 + h1 * h1) * 1000;
end;

function TFCoteDB.CalcUliage3(Lon1, Lat1, Lon2, Lat2, EarthRadius: Extended): Extended;
begin

end;

function TFCoteDB.CheckNetwork: boolean;
begin
  Result:= Ping('8.131.244.41');
end;

function TFCoteDB.CreateComm(EventOfCollection: TEventAfterCollectionReceived;
  EventOfRead: TEventAfterReadReceived; EventOfWrite: TEventAfterWriteReceived;
  CheckRing: Boolean): TJZCommBase;
begin
  if FCoteDB.RingKind = '只读环' then Result:= TJZCommR.Create
  else if FCoteDB.RingKind = '读写环' then Result:= TJZCommRW.Create
  else if FCoteDB.RingKind = '高频只读' then Result:= TJZCommR.Create
  else if FCoteDB.RingKind = '高频读写' then Result:= TJZCommRW.Create
  else if FCoteDB.RingKind = '凯信只读' then Result:= TJZCommKaixin.Create;
  Result.Interval:= StrToInt(ReadAppConfig('COMMUNICATION', 'IntValue'));
  Result.TimeOut:= StrToInt(ReadAppConfig('COMMUNICATION', 'TimeOut'));
  Result.BoardCount:= StrToInt(ReadAppConfig('COMMUNICATION', 'BoardCount'));
  Result.Rings.LoadFromFile(AppPath + 'Rings.dat', true);

  Result.OnCollected:= EventOfCollection;
  Result.OnReadPigeon:= EventOfRead;
  Result.OnWrited:= EventOfWrite;
  Result.CheckRing:= CheckRing;
end;

procedure TFCoteDB.CloseComm(var Comm: TJZCommBase);
begin
  Comm.Stop;
  Comm.Terminate;
  Comm:= nil;
end;

procedure TFCoteDB.DataModuleCreate(Sender: TObject);
var
  S, CertificationFile: string;
begin
  inherited;
  CertificationFile := AppPath + CERT_FILE;                     //证书文件文件名称
  Cert := TCert.Create; // 加载证书
  if not Cert.LoadCert(CertificationFile, SYS_NAME) then
    raise Exception.Create(RES_CERT_LOAD_FAILED);
  ADOConn.Close; // 连接数据库
  if S = '' then
    S := Cert.CertParams.NameValue['DBConnStr'];
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'DBConnStr=' + S); {$endif}
  if S <> '' then
  begin
    S := StringReplace(S, '<APPPATH>', AppPath, [rfReplaceAll, rfIgnoreCase]);
    ADOConn.ConnectionString := S;
  end;
  if not SameText(Cert.Values.NameValue['MainFile'],
    ExtractFileName(Application.ExeName)) then
    Exit;// 读取应用文件文件名
  if not Cert.CheckCert(SYS_NAME, ADOConn) then
    Exit; // 检查证书有效性
  Cert.AccessDate := Now; // 回写证书
  Cert.SaveCert(AppPath + CERT_FILE, SYS_NAME, CERT_FLAG);
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'Will update ......'); {$endif}
  if Cert.Values.NameValue['DoUpgrade'] = 'True' then // 自动升级
    with TFPlatformUpgrade.Create(nil) do
      try
        OnUpgrade:= EventOnUpgrade;
        CanRun := ShowModal = mrYes;
        if not CanRun then Application.Terminate;
      finally
        Free;
      end;
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'Will LoadRuntime ......'); {$endif}
  LoadRuntime;
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'Will Login ......'); {$endif}
  WriteLog(LOG_DEBUG, 'Login', Format('Cert file is %s, sys is %s', [
    Cert.Values.NameValue['DoLogin'], BoolToStr(NeedLogin, True)]));
  if(Cert.Values.NameValue['DoLogin'] = 'True')or NeedLogin then
    CanRun := Login
  else if S = '' then
    CanRun := true
  else if LoginInfo.Login('guest', 'guest', ADOConn) then
    CanRun := true
  else
    CanRun := Login;
  {$ifdef debug} WriteLog(LOG_DEBUG, 'DataModuleCreate', 'Will LoadUsings ......'); {$endif}
  LoadUsings;
  {$ifdef debug} WriteLog(LOG_DEBUG, 'Cotr_DataModuleCreate', 'In cote db ......'); {$endif}
  FTranCodes := TJZElectricRings.Create;
  FCheckInCode := TJZElectricRings.Create;
  FCoteInfo:= TCoteInfo.Create;
  {$ifdef debug} WriteLog(LOG_DEBUG, 'Cotr_DataModuleCreate', 'Is can run ' + BoolToStr(Canrun, true)); {$endif}
  if not CanRun then Exit;
  {$ifdef debug} WriteLog(LOG_DEBUG, 'Cotr_DataModuleCreate', 'Is Cote id is ' + Cert.CertParams.NameValue['OrgID']); {$endif}
  FCoteInfo.ID:= StringToGuid(Cert.CertParams.NameValue['OrgID']);
  FCoteInfo.OrgName:= Cert.CertParams.NameValue['CoteName'];
  {$ifdef debug} WriteLog(LOG_DEBUG, 'Cotr_DataModuleCreate', 'while load cote info with ' + Cert.CertParams.NameValue['OrgID']); {$endif}
  if not FCoteInfo.LoadData(FCoteDB.ADOConn, FCoteInfo.ID)then begin
    with TFDlgCote.Create(nil) do try
      InfoToForm(FCoteInfo);
      while ShowModal = mrOK do try
        FormToInfo(FCoteInfo);
        FCoteInfo.AppendData(ADOConn);
        Exit;
      except
        on E: Exception do begin
          DlgInfo('错误', E.Message);
        end;
      end;
    finally
      Free;
    end;
  end;
  {$ifdef debug} WriteLog(LOG_DEBUG, 'Cotr_DataModuleCreate', 'Will load rings ......'); {$endif}
  if not FileExists(AppPath + 'Rings.dat') then
  begin
    CanRun := False;
    WriteInfo('重要文件缺失，系统不能启动');
  end;
  {$ifdef debug} WriteLog(LOG_DEBUG, 'Cotr_DataModuleCreate', 'finally ......'); {$endif}
  FOnSay := nil;
end;

procedure TFCoteDB.DataModuleDestroy(Sender: TObject);
begin
  inherited;
//  if Assigned(FComm) then begin
//    SetComm(nil, nil, nil, false);
//    FComm.Stop;
//    FComm.Terminate;
//  end;
  Cert.Free;
  FCoteInfo.Free;
  FCheckInCode.Free;
  FTranCodes.Free;
end;

function TFCoteDB.EventOfGeneratorSql(OrgTable, LinkTable: string): string;
begin
  if SameText(OrgTable, 'GameSource') then
  begin
    if SameText(LinkTable, 'Member') then
      Result := 'Member.ID=Pigeon.MemberIDX'
    else if SameText(LinkTable, 'Pigeon') then
      Result := 'Pigeon.ID=GameSource.PigeonIDX'
    else if SameText(LinkTable, 'Match') then
      Result := 'Match.ID=GameSource.GameIDX';
  end
  else if SameText(OrgTable, 'Pigeon') then
  begin
    if SameText(LinkTable, 'GameSource') then
      Result := 'Pigeon.ID=GameSource.PigeonIDX'
    else if SameText(LinkTable, 'Member') then
      Result := 'Member.ID=Pigeon.MemberIdx';
  end
  else if SameText(OrgTable, 'Match') then
  begin
    if SameText(LinkTable, 'GameSource') then
      Result := 'GameSource.GameIDX=Match.ID';
  end;
end;

function TFCoteDB.GetMemberGroupValue(GroupIdx: Integer; MemberIdx: TGuid;
  var KeyIdx: Integer): Integer;
var
  GroupName: string;
begin
  GroupName:= 'Group' + IntToStr(GroupIdx + 1);
  with TADOQuery.Create(nil) do try
    Connection:= ADOConn;
    SQL.Text:= 'SELECT COUNT(*) FROM JOIN_Pigeon WHERE ' +
      GroupName + '=''' + FGroupKeys[GroupIdx, KeyIdx] +
      ''' AND MemberIdx=''' + GuidToString(MemberIdx) + ''' GROUP BY ' +
      GroupName + ' ORDER BY ' + GroupName;
    Open;
    if EOF then Result:= 0
    else begin
      Result:= Fields[0].AsInteger;
      if FCoteDB.FGroupCount[GroupIdx] = 0 then Result:= 1
      else if Result >= FCoteDB.FGroupCount[GroupIdx] then begin
        Inc(KeyIdx);
        Result:= GetMemberGroupValue(GroupIdx, MemberIdx, KeyIdx);
      end;
    end;
  finally
    Free;
  end;
end;

function TFCoteDB.GetMemberMaxGroupKeyId(GroupIdx: Integer;
  MemberIdx: TGuid): Integer;
begin
  with TADOQuery.Create(nil) do try
    Connection:= ADOConn;
    SQL.Text:= 'SELECT COUNT(*) FROM JOIN_Pigeon WHERE MemberIdx=''' +
      GuidToString(MemberIdx) + ''' AND Status<>''删除''';
    Open;
    if EOF then Result:= 0
    else if FGroupCount[GroupIdx] = 0 then Result:= 1
    else Result:= Fields[0].AsInteger div FGroupCount[GroupIdx];
  finally
    Free;
  end;
end;

function TFCoteDB.GetSMSCount: Integer;
var
  S: string;
begin
  S:= GetWebData('/Join/Admin/GetSMSCount/' + Copy(FCoteInfo.ID.ToString(), 2, 36));
  if S = '' then Result:= 0 else Result:= StrToInt(S);
end;

function TFCoteDB.GetSupportChoose: Integer;
var
  S: string;
begin
  S:= GetWebData('/Join/Admin/GetSupportChoose/' + Copy(FCoteInfo.ID.ToString(), 2, 36));
  if S = '' then Result:= 0 else Result:= StrToInt(S);
end;

function TFCoteDB.GetWebData(URL: string): string;
begin
  Result:= '';
  WriteLog(LOG_Info, 'GetWebData', 'Get data from web ：' + URL);
  if not CheckNetwork then Exit;
  with IdHTTP1.Request do
  begin
    IdHTTP1.ReadTimeout := 1000;
    IdHTTP1.ConnectTimeout:= 3000;
    AcceptLanguage := 'UTF-8';
    Referer := FPlatformDB.Cert.Values.NameValue['WebUrl'];
    AcceptEncoding := 'gzip, deflate';
    UserAgent := 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022; InfoPath.2; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)';
    Host := FPlatformDB.Cert.Values.NameValue['WebUrl'];
    Connection := 'Keep-Alive';
  end;
  try
    Result:= IdHTTP1.Get(FPlatformDB.Cert.Values.NameValue['WebUrl'] + URL);
  except
    on E: Exception do begin
      WriteLog(LOG_FAIL, 'GetWebData', 'Error on get data from web ：' + E.Message + #13#10 + URL);
    end;
  end;
  IdHTTP1.Disconnect;
end;

//function TFCoteDB.SetComm(EventOfCollection: TEventAfterCollectionReceived;
//  EventOfRead: TEventAfterReadReceived;
//  EventOfWrite: TEventAfterWriteReceived;
//      CheckRing: Boolean): TJZCommBase;
//begin
//  FComm.OnCollected:= EventOfCollection;
//  FComm.OnReadPigeon:= EventOfRead;
//  FComm.OnWrited:= EventOfWrite;
//  FComm.CheckRing:= CheckRing;
//  Result:= FComm;
//end;

procedure TFCoteDB.InitData();
begin
  {$ifdef debug} WriteLog(LOG_DEBUG, 'Cotr_DataModuleCreate', 'Will init communication ...... '); {$endif}
//  FComm:= AComm;
//  FComm.Interval:= StrToInt(ReadAppConfig('COMMUNICATION', 'IntValue'));
//  FComm.TimeOut:= StrToInt(ReadAppConfig('COMMUNICATION', 'TimeOut'));
//  FComm.BoardCount:= StrToInt(ReadAppConfig('COMMUNICATION', 'BoardCount'));
//  FComm.Rings.LoadFromFile(AppPath + 'Rings.dat', true);
  //FComm.Rings.SaveToFile(AppPath + 'Rings.txt', False);
  if FileExists(AppPath + 'Trans.dat') then
    FTranCodes.LoadFromFile(AppPath + 'Trans.dat');
  if FileExists(AppPath + 'CheckIn.dat') then
    FCheckInCode.LoadFromFile(AppPath + 'CheckIn.dat');
  if FileExists(AppPath + 'Ticket.dat') then
    ReadStringsFromZipFile(AppPath + 'Ticket.dat', FTicketCode);
  FSMSUrl:= ReadAppConfig('SYSTEM', 'SMSURL');
  {$ifdef debug} WriteLog(LOG_DEBUG, 'Cotr_DataModuleCreate', 'End of inited communication ...... '); {$endif}
end;


procedure TFCoteDB.ReinitGroup;
var
  lst: TStringList;
  procedure SetGrouping(Idx: Integer);
  var
    I, j: Integer;
    K, v: string;
  begin
    FGroupNames[Idx]:= lst.Names[Idx];
    K:= ''; V:= lst.ValueFromIndex[Idx];
    for I := Length(V) downto 2 do
      if Pos(V[I], '0123456789') > 0 then K:= V[I] + K;
    if K = '' then FGroupCount[Idx]:= 0 else FGroupCount[Idx]:= StrToInt(K);
    if V[1] = 'A' then begin
      for I := 0 to 25 do FGroupKeys[Idx, I]:= Char(I + Ord('A'));
      for I := 1 to 26 do FGroupKeys[Idx, I + 25]:= 'A' + IntToStr(I);
    end
    else if V[1] = 'a' then begin
      for I := 0 to 25 do FGroupKeys[Idx, I]:= Char(I + Ord('a'));
      for I := 1 to 26 do FGroupKeys[Idx, I + 25]:= 'a' + IntToStr(I);
    end
    else if V[1] = '数' then begin
      for I := 0 to 52 do FGroupKeys[Idx, I]:= IntToStr(I);
    end
    else for I := 0 to 52 do FGroupKeys[Idx, I]:= Copy(V, 1, Length(V) - Length(K));
  end;
  procedure ClsGrouping(Idx: Integer);
  var i: Integer;
  begin
    FGroupNames[Idx]:= '';        //分组名称集合
    FGroupCount[Idx]:= 0;         //各分组允许的最大赛鸽数量
    for I := 0 to 52 do
      FGroupKeys[Idx, I]:= '';    //各分组序列值列表
  end;
begin
  lst:= TStringList.Create;
  try
    FCoteDB.GetListOption('分组', lst, 0);
    if lst.Count > 0 then SetGrouping(0) else ClsGrouping(0);
    if lst.Count > 1 then SetGrouping(1) else ClsGrouping(1);
    if lst.Count > 2 then SetGrouping(2) else ClsGrouping(2);
    if lst.Count > 3 then SetGrouping(3) else ClsGrouping(3);
    if lst.Count > 4 then SetGrouping(4) else ClsGrouping(4);
  finally
    lst.Free;
  end;
end;

//function TFCoteDB.SendSMS(Mobile, Msg: string): boolean;
//var
//  I: Integer;
//  S: string;
//  msgToSend: RawByteString;
//  FXMLDoc: IXMLDocument;
//  returnsms: IXMLNode;
//begin
//  WriteLog(LOG_DEBUG, 'SendSMS', 'Will send a SMS(' + Msg + ') to ' + Mobile);
//  with IdHTTP1.Request do
//  begin
//    IdHTTP1.ReadTimeout := 300;
//    IdHTTP1.ConnectTimeout:= 300;
//    AcceptLanguage := 'UTF-8';
//    Referer := 'http://www.gexingwang.com/';
//    AcceptEncoding := 'gzip, deflate';
//    UserAgent := 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022; InfoPath.2; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)';
//    Host := 'www.gexingwang.com';
//    Connection := 'Keep-Alive';
//    CustomHeaders.Text := 'Set-Cookie: OXPYJ911=1207DD4D; cookies_login_state=user_id=2560F2C27F0FC02E4356D51B827A7204; ASP.NET_SessionId=zu2fmjaobt3zh055foo0xwus; AJSTAT_ok_times=28; AJSTAT_ok_pages=3';
//  end;
//  try
//    msgToSend:= HTTPEncode(UTF8Encode(Msg));
//    S:= Format(FSMSUrl, [Mobile, msgToSend]);
//    WriteLog(LOG_DEBUG, 'SendSMS', 'Encode ......' + S);
//    //IdHTTP1.Connect;
//    S:= IdHTTP1.Get(S);
//    FXMLDoc := TXMLDocument.Create(nil);
//    FXMLDoc.XML.Text := S;
//    FXMLDoc.Active := True;
//    (*<?xml version="1.0" encoding="utf-8" ?>
//      <returnsms>
//        <returnstatus>Success</returnstatus><message>OK</message><remainpoint>2889000</remainpoint><taskID>3B33A8C382735E2F</taskID><resplist><resp>3B33A8C382735E2F#@#18020013672#@#0#@#</resp></resplist><successCounts>1</successCounts></returnsms>*)
//    returnsms:= FXMLDoc.ChildNodes[1];
//    Result:= returnsms.ChildValues['returnstatus'] = 'Success';
//    if Result then WriteLog(LOG_DEBUG, 'SendSMS', 'Return a Success from SMS ：' + S)
//    else WriteLog(LOG_DEBUG, 'SendSMS', 'Return a Failed from SMS ：' + S);
//    FXMLDoc.Active := False;
//  except
//    on E: Exception do begin
//      WriteLog(LOG_FAIL, 'SendSMS', 'Error on send SMS ：' + E.Message);
//      //DlgInfo('短信发送失败', E.Message, 10);
//    end;
//  end;
//  IdHTTP1.Disconnect;
//end;

{ TJZElectricRings }

function TJZElectricRings.Add(JZCode: Int64; GameCode: Word;
  FactoryCode: Int64): Integer;
begin
  Result := FCount;
  Inc(FCount);
  if FCount > FCapacity then
    Grow;
  FRings[Result].JZCode := JZCode;
  FRings[Result].GameCode := GameCode;
  FRings[Result].FactoryCode := FactoryCode;
end;

procedure TJZElectricRings.Clear;
begin
  FCount := 0;
end;

constructor TJZElectricRings.Create;
begin

end;

procedure TJZElectricRings.Delete(Index: Integer);
var
  i: Integer;
begin
  if (Index < 0) or (Index >= FCount) then
    Exit;
  for i := Index to FCount - 2 do
    FRings[i] := FRings[i + 1];
  Dec(FCount);
end;

destructor TJZElectricRings.Destroy;
begin
  SetLength(FRings, 0);
  inherited;
end;

function TJZElectricRings.GetRings(Index: Integer): TJZElectricCodeInfo;
begin
  if (Index < 0) or (Index >= FCount) then
    Exit;
  Result := FRings[Index];
end;

procedure TJZElectricRings.Grow;
var
  Delta: Integer;
begin // 成长一下系统实际开创的记录空间
  if FCapacity > 64 then
    Delta := FCapacity div 4
  else if FCapacity > 8 then
    Delta := 16
  else
    Delta := 4;
  FCapacity := FCapacity + Delta;
  SetLength(FRings, FCapacity);
end;

function TJZElectricRings.IndexOfFactoryCode(FactoryCode: Int64): Integer;
begin
  for Result := 0 to FCount - 1 do
    if FRings[Result].FactoryCode = FactoryCode then
      Exit;
  Result := -1;
end;

function TJZElectricRings.IndexOfGameCode(GameCode: Word): Integer;
begin
  for Result := 0 to FCount - 1 do
    if FRings[Result].GameCode = GameCode then
      Exit;
  Result := -1;
end;

function TJZElectricRings.IndexOfJZCode(JZCode: Int64): Integer;
begin
  for Result := 0 to FCount - 1 do
    if FRings[Result].JZCode = JZCode then
      Exit;
  Result := -1;
end;

procedure TJZElectricRings.LoadFromFile(Filename: string);
var
  fs: TFileStream;
begin
  fs := TFileStream.Create(Filename, fmOpenRead);
  LoadFromStream(fs);
  fs.Free;
end;

procedure TJZElectricRings.LoadFromStream(AStream: TStream);
var
  JZCode: Int64;
  GameCode: Word;
  FactoryCode: Int64;
  szStream: TMemoryStream;
  fs: TFileStream;
begin
  szStream := TMemoryStream.Create;
  ZDecompressStream(AStream, szStream);
  szStream.Position := 0;
  FTitle:= ReadStringFromStream(szStream);
//  fs := TFileStream.Create('C:\T2.txt', fmCreate);
//  fs.CopyFrom(szStream, szStream.Size);
//  fs.Free;
//  szStream.Position := 0;
  while szStream.Position < szStream.Size do
  begin
    szStream.Read(JZCode, 8);
    szStream.Read(GameCode, 2);
    szStream.Read(FactoryCode, 8);
    Add(JZCode, GameCode, FactoryCode);
  end;
  szStream.Free;
end;

procedure TJZElectricRings.SaveToFile(Filename: string);
var
  fs: TFileStream;
begin
  fs := TFileStream.Create(Filename, fmCreate);
  SaveToStream(fs);
  fs.Free;
end;

procedure TJZElectricRings.SaveToStream(AStream: TStream);
var
  i: Integer;
  szStream: TMemoryStream;
  fs: TFileStream;
begin
  szStream := TMemoryStream.Create;
  WriteStringToStream(FTitle, szStream);
  for i := 0 to FCount - 1 do
  begin
    szStream.Write(FRings[i].JZCode, 8);
    szStream.Write(FRings[i].GameCode, 2);
    szStream.Write(FRings[i].FactoryCode, 8);
  end;
  szStream.Position := 0;
//  fs := TFileStream.Create('C:\T1.txt', fmCreate);
//  fs.CopyFrom(szStream, szStream.Size);
//  fs.Free;
//  szStream.Position := 0;
  ZCompressStream(szStream, AStream);
  szStream.Free;
end;

initialization

RegAppConfig('SYSTEM', 'ULIAGEKIND', '空距算法', LIDT_Choice, '常规算法,勾股算法', '0');
RegAppConfig('SYSTEM', 'EARTHRADIUS', '地球半径', LIDT_Float, '6378137', '6378137');

end.
