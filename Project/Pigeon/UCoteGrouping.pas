unit UCoteGrouping;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, UCoteComm, ImgList, ExtCtrls, ComCtrls, ToolWin,
  DB, UCoteInfo, ADODB, Spin, System.ImageList, UVonLog, Vcl.CheckLst,
  UFrameGridBar, clipbrd, Vcl.Grids;

resourcestring
  RES_FOUND_DBL = '该环已经存在于记录中，不能进行，请核实。';

type
  TFCoteGrouping = class(TForm)
    mLog: TMemo;
    GroupBox3: TGroupBox;
    DBMatch: TADOTable;
    DQMaxGroup: TADOQuery;
    DQCHECK: TADOQuery;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    btnAutoGroup: TBitBtn;
    Panel3: TPanel;
    Panel5: TPanel;
    Label3: TLabel;
    EGroup: TComboBox;
    Label5: TLabel;
    EGroupOrder: TComboBox;
    lstMatch: TCheckListBox;
    GroupBox1: TGroupBox;
    Panel7: TPanel;
    Label6: TLabel;
    ESourceKind: TComboBox;
    Panel8: TPanel;
    lbChangeStatus: TLabel;
    EChangeStatus: TComboBox;
    btnChangeStatus: TBitBtn;
    Panel4: TPanel;
    lbGroupStatus: TLabel;
    EGroupStatus: TComboBox;
    ProgressBar1: TProgressBar;
    ProgressBar2: TProgressBar;
    DQMember: TADOQuery;
    DQPigeonBySource: TADOQuery;
    btnDelGroup: TBitBtn;
    Memo1: TMemo;
    pc1: TPageControl;
    TabSheet1: TTabSheet;
    plCondition: TPanel;
    Panel10: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    Label22: TLabel;
    Label9: TLabel;
    EShortName1: TEdit;
    EMemberNo1: TEdit;
    EMemberName1: TEdit;
    ESCashKind: TComboBox;
    Panel12: TPanel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    EState1: TComboBox;
    ECity1: TComboBox;
    ECounty1: TComboBox;
    btnMemberSearch: TBitBtn;
    btnMemberReset: TBitBtn;
    FrameMemberGrid: TFrameGridBar;
    TabSheet2: TTabSheet;
    GridManual: TStringGrid;
    chkMember: TCheckBox;
    btnSynData: TButton;
    FlowPanel1: TFlowPanel;
    Label1: TLabel;
    ESubjectName: TEdit;
    ESubjectValue: TEdit;
    btnGridAdd: TButton;
    btnGridEdit: TButton;
    btnGridDel: TButton;
    btnGridPaste: TButton;
    btnGridUpdate: TButton;
    btnRefrenshRing: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnDelGroupClick(Sender: TObject);
    procedure btnAutoGroupClick(Sender: TObject);
    procedure btnChangeStatusClick(Sender: TObject);
    procedure btnMemberResetClick(Sender: TObject);
    procedure btnMemberSearchClick(Sender: TObject);
    procedure GridManualFixedCellClick(Sender: TObject; ACol, ARow: Integer);
    procedure btnGridPasteClick(Sender: TObject);
    procedure btnGridAddClick(Sender: TObject);
    procedure btnGridEditClick(Sender: TObject);
    procedure btnGridDelClick(Sender: TObject);
    procedure btnGridUpdateClick(Sender: TObject);
    procedure btnSynDataClick(Sender: TObject);
    procedure GridManualDblClick(Sender: TObject);
    procedure btnRefrenshRingClick(Sender: TObject);
  private
    { Private declarations }
    FComm: TJZCommBase;
    function GetMatchSql: string;   //各分组系列名称集合
    function GetMemberSql: string; //获取鸽主选择结果对应的SQL语句
  public
    { Public declarations }
  end;

var
  FCoteGrouping: TFCoteGrouping;

implementation

uses UCoteDB, UDlgInputGBCode, UVonSystemFuns;

{$R *.dfm}

procedure TFCoteGrouping.FormCreate(Sender: TObject);
var
  MID: PGUID;
  I: Integer;

  procedure InitPigeonExtendField(ExName: string; Lb1, Lb2: TLabel;
    Co1, Co2: TComboBox);
  begin
    Lb1.Caption := FCoteDB.GetDropList(ExName, Co1, '');
    Lb2.Caption := Lb1.Caption;
    Lb1.Visible := Lb1.Caption <> '';
    Lb2.Visible := Lb1.Visible;
    Co1.Visible := Lb1.Visible;
    Co2.Visible := Lb1.Visible;
    Co2.Items.Assign(Co1.Items);
  end;
begin
  FComm:= FCoteDB.CreateComm(nil, nil, nil, true);
  GridManual.Cells[0, 0]:= '统一环号';
  GridManual.Cells[1, 0]:= '标记';
  GridManual.Cells[2, 0]:= '执行结果';
  FCoteDB.ReinitGroup;
  for I := 0 to 4 do
    if FCoteDB.FGroupNames[I] <> '' then
      EGroup.Items.Add(FCoteDB.FGroupNames[I]);
  EGroup.ItemIndex := 0;
  InitPigeonExtendField('信鸽状态', lbGroupStatus, lbChangeStatus, EChangeStatus, EGroupStatus);
  EGroupStatus.Items.Insert(0, '所有');
  EGroupStatus.ItemIndex := 0;
  EChangeStatus.ItemIndex := 0;
  with DBMatch do
    try
      Open;
      while not EOF do
      begin
        New(MID);
        MID^:= FieldByName('ID').AsGuid;
        lstMatch.AddItem(FieldByName('MatchName').AsString, TObject(MID));
        Next;
      end;
      lstMatch.ItemIndex := lstMatch.Count - 1;
    finally
      Close;
    end;
  FCoteDB.GetDropList('缴费方式', ESCashKind);
  FrameMemberGrid.LoadTitles('CoteMember');
  btnRefrenshRing.Visible:= (FCoteDB.RingKind = '读写环')or(FCoteDB.RingKind = '高频读写');
  btnMemberSearchClick(nil);
end;

procedure TFCoteGrouping.FormDestroy(Sender: TObject);
begin
  FCoteDB.ADOConn.Execute('UPDATE JOIN_Pigeon SET SynFlag=''U'' WHERE SynFlag=''E''');
  FCoteDB.CloseComm(FComm);
end;

(* Speicail *)

procedure TFCoteGrouping.btnRefrenshRingClick(Sender: TObject);
var
  szCode: Int64;
begin
  ProgressBar1.Position:= 0;
  with TADOQuery.Create(nil)do try
    Connection:= FCoteDB.ADOConn;
    SQL.Text:= 'SELECT [ID],[PigeonNO],[GBCode],[FactoryCode],[JZCode],[Status],[SynFlag] FROM [JOIN_Pigeon]';
    Open;
    ProgressBar1.Max:= RecordCount;
    while not EOF do begin
      szCode:= FComm.Rings.CheckRing(FieldByName('FactoryCode').AsLargeInt);
      if szCode = 0 then begin
        WriteLog(LOG_MAJOR, '强制摘环', '摘环JZCode->' + FieldByName('JZCode').AsString +
          ' with ID=' + FieldByName('ID').AsString);
        FCoteDB.ADOConn.Execute('UPDATE [JOIN_Pigeon] SET JZCode=0,FactoryCode=0 WHERE ID=''' + FieldByName('ID').AsString + '''');
      end else if szCode <> FieldByName('JZCode').AsLargeInt then begin
        WriteLog(LOG_MAJOR, '强制检查', '替换JZCode->' + FieldByName('JZCode').AsString +
          ',' + IntToStr(szCode) + ' with ID=' + FieldByName('ID').AsString);
        FCoteDB.ADOConn.Execute('UPDATE [JOIN_Pigeon] SET JZCode=' +
          IntToStr(szCode) + ' WHERE ID=''' + FieldByName('ID').AsString + '''');
      end;
      ProgressBar1.Position:= ProgressBar1.Position + 1;
      ProgressBar1.Repaint;
      Next;
    end;
    Close;
    SQL.Text:= 'SELECT [FactoryCode],Count(*) AS CNT FROM [JOIN_Pigeon] GROUP BY [FactoryCode] HAVING Count(*)>1';
    Open;
    while not EOF do begin
      WriteLog(LOG_MAJOR, '强制摘重', '摘环FactoryCode->' + FieldByName('FactoryCode').AsString);
      FCoteDB.ADOConn.Execute('UPDATE [JOIN_Pigeon] SET JZCode=0,FactoryCode=0 WHERE FactoryCode=''' + FieldByName('FactoryCode').AsString + '''');
      Next;
    end;
  finally
    Free;
  end;
  ProgressBar1.Position:= 0;
end;

procedure TFCoteGrouping.btnSynDataClick(Sender: TObject);
begin
  FCoteDB.ADOConn.Execute('UPDATE JOIN_Member SET SynFlag=''U''');
  FCoteDB.ADOConn.Execute('UPDATE JOIN_Pigeon SET SynFlag=''U''');
  DlgInfo('提示', '注意服务程序要处于开启状态！', 2);
end;

(* Member *)

procedure TFCoteGrouping.btnMemberResetClick(Sender: TObject);
begin
  EMemberName1.Text := '';
  EMemberNo1.Text := '';
  EShortName1.Text := '';
  EState1.ItemIndex := 0;
  ECity1.ItemIndex := 0;
  ECounty1.ItemIndex := 0;
  ESCashKind.ItemIndex := 0;
end;

procedure TFCoteGrouping.btnMemberSearchClick(Sender: TObject);
begin
  with DQMember do begin
    SQL.Text := 'SELECT * FROM ' + Prefix + 'Member WHERE Status=''启用''';
    if EMemberName1.Text <> '' then
      SQL.Add('AND MemberName LIKE ''%' + EMemberName1.Text + '%''');
    if EMemberNo1.Text <> '' then
      SQL.Add('AND MemberNo LIKE ''%' + EMemberNo1.Text + '%''');
    if EShortName1.Text <> '' then
      SQL.Add('AND ShortName LIKE ''%' + EShortName1.Text + '%''');
    if ESCashKind.ItemIndex > 0 then
      SQL.Add('AND CashKind=''' + ESCashKind.Text + '''');
    if EState1.ItemIndex > 0 then
      SQL.Add('AND State LIKE ''%' + EState1.Text + '%''');
    if ECity1.ItemIndex > 0 then
      SQL.Add('AND City LIKE ''%' + ECity1.Text + '%''');
    if ECounty1.ItemIndex > 0 then
      SQL.Add('AND County LIKE ''%' + ECounty1.Text + '%''');
    SQL.Add('ORDER BY DisplayOrder');
    Open;
  end;
end;

(* Group *)

function TFCoteGrouping.GetMatchSql: string;
var
  i: Integer;
begin
  Result:= '';
  for I := 0 to lstMatch.Count - 1 do
    if lstMatch.Checked[I] then begin
      Result:= Result + ',''' + GuidToString(PGuid(lstMatch.Items.Objects[I])^) + '''';
      mLog.Lines.Add('    针对指定赛事：' + lstMatch.Items[I]);
    end;
  if Result = '' then begin
    if lstMatch.ItemIndex < 0 then raise Exception.Create('尚未选择赛事！')
    else begin
      Result:= 'GameIdx=''' + GuidToString(PGuid(lstMatch.Items.Objects[lstMatch.ItemIndex])^) + '''';
      mLog.Lines.Add('    针对指定赛事：' + lstMatch.Items[lstMatch.ItemIndex]);
    end;
  end else Result:= 'GameIdx IN (' + Copy(Result, 2, MaxInt) + ')';
end;

function TFCoteGrouping.GetMemberSql: string;
var
  i: Integer;
  b: TBookmark;
begin
  Result:= '';
  if not chkMember.Checked then Exit;
  for I := 0 to FrameMemberGrid.Grid.SelectedRows.Count - 1 do begin
    FrameMemberGrid.GridSource.DataSet.GotoBookmark(FrameMemberGrid.Grid.SelectedRows.Items[i]);
    Result:= Result + ',''' + FrameMemberGrid.GridSource.DataSet.FieldByName('ID').AsString + '''';
    mLog.Lines.Add('    针对特定鸽主：' + FrameMemberGrid.GridSource.DataSet.FieldByName('MemberName').AsString);
  end;
  if Result = '' then begin
    Result:= ' AND MemberIdx=''' + FrameMemberGrid.GridSource.DataSet.FieldByName('ID').AsString + '''';
    mLog.Lines.Add('    针对特定鸽主：' + FrameMemberGrid.GridSource.DataSet.FieldByName('MemberName').AsString);
  end else Result:= ' AND MemberIdx IN (' + Copy(Result, 2, MaxInt) + ')';
end;

procedure TFCoteGrouping.btnAutoGroupClick(Sender: TObject);
var
  AQuery: TADOQuery;
  StatusSql, GroupName: String;
  KID, GCnt, MaxKey: Integer;
  MID: TGuid;
begin
  if DlgInfo('提示', '是否确认自动完成所有入棚赛鸽的分组工作？', mbYesNo) = mrNo then Exit;
  GroupName:= 'Group' + IntToStr(EGroup.ItemIndex + 1);
  AQuery := TADOQuery.Create(nil);
  AQuery.Connection := FCoteDB.ADOConn;
  mLog.Lines.Add('系统将进行自动分组');
  StatusSql:= GetMemberSql;
  if EGroupStatus.ItemIndex > 0 then begin
    StatusSql:= StatusSql + ' AND Status=''' + EGroupStatus.Text + '''';
    mLog.Lines.Add('    针对特殊赛鸽：' + EGroupStatus.Text);
  end;
  with AQuery do try
    case EGroupOrder.ItemIndex of
      //按单场分速
      //按照所选当前赛事中，各只赛鸽的归巢分速作为排名依据，按照系统设定的该分组
      //最大赛鸽数量为分组限额，对每一名鸽主的赛鸽进行分组，系统会从最大号分组还
      //是继续分组，以前未分满分组不再继续分组。
      0: SQL.Text := 'SELECT P.MemberIdx,P.ID,Speed,S.SourceOrder FROM ' +
          'Join_Pigeon P JOIN Join_GameSource S ON S.GameIdx=''' +
          GuidToString(PGuid(lstMatch.Items.Objects[lstMatch.ItemIndex])^) +
          ''' AND S.PigeonIdx=P.ID WHERE P.' + GroupName + '=''''' + StatusSql +
          ' ORDER BY S.Speed DESC';
      //按信鸽编号
      //按照赛鸽在入棚时所录入的信鸽编号作为排名依据，按照系统设定的该分组最大赛
      //鸽数量为分组限额，对每一名鸽主的赛鸽进行分组，系统会从最大号分组还是继续
      //分组，以前未分满分组不再继续分组。
      1: SQL.Text := 'SELECT P.MemberIdx,P.ID FROM Join_Pigeon P ' +
          'WHERE P.' + GroupName + '=''''' + StatusSql + ' ORDER BY PigeonNO';
      //按平均分速
      //按照所选当前赛事中，各只赛鸽的归巢平均分速作为排名依据，按照系统设定的该
      //分组最大赛鸽数量为分组限额，对每一名鸽主的赛鸽进行分组，系统会从最大号分
      //组还是继续分组，以前未分满分组不再继续分组。
      2: SQL.Text := 'SELECT P.MemberIdx,P.ID,AVG(Speed) AS AP FROM ' +
          'Join_Pigeon P JOIN Join_GameSource S ON S.' + GetMatchSql +
          ' AND S.PigeonIdx=P.ID WHERE P.' + GroupName + '=''''' + StatusSql +
          ' GROUP BY P.MemberIdx,P.ID ORDER BY AP DESC';
      //按集鸽记录
      //按照所选当前赛事中，各只赛鸽的集鸽序号作为排名依据，按照系统设定的该
      //分组最大赛鸽数量为分组限额，对每一名鸽主的赛鸽进行自动分组。
      3: SQL.Text := 'SELECT P.MemberIdx,P.ID FROM ' +
          'Join_Pigeon P JOIN Join_GameSource S ON S.' + GetMatchSql +
          ' AND S.PigeonIdx=P.ID WHERE P.' + GroupName + '=''''' + StatusSql +
          ' GROUP BY P.MemberIdx,P.ID ORDER BY S.[CollectID]';
      //按最佳名次
      //按照所选当前赛事中，各只赛鸽的归巢最佳名次作为分组排序依据，按照系统设定
      //的该分组最大赛鸽数量为分组限额，对每一名鸽主的赛鸽进行分组，系统会从最大
      //号分组还是继续分组，以前未分满分组不再继续分组。
      4: SQL.Text := 'SELECT P.MemberIdx,P.ID,MIN(S.SourceOrder) AS AP FROM ' +
          'Join_Pigeon P JOIN Join_GameSource S ON S.' + GetMatchSql +
          ' AND S.PigeonIdx=P.ID WHERE S.SourceOrder>0 AND P.' + GroupName + '=''''' + StatusSql +
          ' GROUP BY P.MemberIdx,P.ID ORDER BY AP ASC';
      //按稳定性
      //按照所选当前赛事中，系统会按照归巢的次数和分速作为分组排序依据，即首先保
      //证能按时归巢再考虑分速快慢，按照系统设定的该分组最大赛鸽数量为分组限额，
      //对每一名鸽主的赛鸽进行分组，系统会从最大号分组还是继续分组，以前未分满分
      //组不再继续分组。
      5: SQL.Text := 'SELECT P.MemberIdx,P.ID,Count(*) AS A1,AVG(Speed) ' +
          'AS A2 FROM Join_Pigeon P JOIN Join_GameSource S ON S.' +
          GetMatchSql + ' AND S.PigeonIdx=P.ID WHERE P.' + GroupName + '=''''' +
          StatusSql + ' GROUP BY P.MemberIdx,P.ID ORDER BY A1,A2 DESC';
    end;
    mLog.Lines.Add('    采用' + EGroupOrder.Text + '排序策略');
    MID:= TGuid.Empty;
    Open;
    ProgressBar1.Max:= RecordCount;
    ProgressBar1.Position:= 0;
    ProgressBar2.Max:= FCoteDB.FGroupCount[EGroup.ItemIndex];
    ProgressBar2.Position:= 0;
    mLog.Lines.Add('    开始自动分组... ...');
    while not EOF do begin
      if MID <> Fields[0].AsGuid then begin
        MaxKey:= FCoteDB.GetMemberMaxGroupKeyId(EGroup.ItemIndex, Fields[0].AsGuid);
        KID:= 0;
        GCnt:= FCoteDB.GetMemberGroupValue(EGroup.ItemIndex, Fields[0].AsGuid, KID);
      end;
      if KID < MaxKey then begin
        if GCnt >= FCoteDB.FGroupCount[EGroup.ItemIndex] then begin
          Inc(KID);
          GCnt:= FCoteDB.GetMemberGroupValue(EGroup.ItemIndex, Fields[0].AsGuid, KID);
        end;
        FCoteDB.ADOConn.Execute('UPDATE JOIN_Pigeon SET ' + GroupName + '=''' +
          FCoteDB.FGroupKeys[EGroup.ItemIndex, KID] + ''',SynFlag=''E'' WHERE ID=''' +
          Fields[1].AsString + '''');
        ProgressBar1.Position:= ProgressBar1.Position + 1;
        ProgressBar1.Repaint;
        ProgressBar2.Position:= GCnt;
        ProgressBar2.Repaint;
        Inc(GCnt);
      end;
      Next;
    end;
    mLog.Lines.Add('    系统已完成对' + IntToStr(RecordCount) + '只赛鸽的自动分组');
  finally
    AQuery.Free;
  end;
  ShowMessage('完成' + EGroup.Text + '自动分组设置');
end;

procedure TFCoteGrouping.btnDelGroupClick(Sender: TObject);
var
  AQuery: TADOQuery;
  GroupName, Condition: string;
begin
  if DlgInfo('提示', '是否确认取消所有入棚赛鸽的分组信息？', mbYesNo) = mrNo then Exit;
  GroupName:= 'Group' + IntToStr(EGroup.ItemIndex + 1);
  mLog.Lines.Add('系统将进行分组自动撤销');
  Condition:= GetMemberSql;
  if EGroupStatus.ItemIndex > 0 then begin
    Condition:= Condition + ' AND Status=''' + EGroupStatus.Text + '''';
    mLog.Lines.Add('    针对特殊赛鸽：' + EGroupStatus.Text);
  end;
  mLog.Lines.Add('    开始分组撤销... ...');
  if Condition = '' then
    FCoteDB.ADOConn.Execute('UPDATE JOIN_Pigeon SET ' + GroupName + '='''',SynFlag=''E''')
  else FCoteDB.ADOConn.Execute('UPDATE JOIN_Pigeon SET ' + GroupName + '='''',SynFlag=''E'' ' +
    'WHERE ' + Copy(Condition, 5, MaxInt));
  mLog.Lines.Add('    系统已完成自动分组撤销工作。');
  ShowMessage('完成取消' + EGroup.Text + '分组信息');
end;

(* Change status *)

procedure TFCoteGrouping.btnChangeStatusClick(Sender: TObject);
var
  StatusSql: String;
begin
  if DlgInfo('提示', '是否确认批量修改赛鸽状态！', mbYesNo) = mrNo then Exit;
  mLog.Lines.Add('系统将进行赛鸽状态修改工作');
  StatusSql:= GetMemberSql;
  with DQPigeonBySource do
  begin
    case ESourceKind.ItemIndex of
    //归巢记录
    0: SQL.Text := 'SELECT DISTINCT P.ID FROM JOIN_Pigeon P JOIN JOIN_GameSource S ON ' +
        'S.PigeonIdx=P.ID AND S.IsBack = 1 AND S.' + GetMatchSql + StatusSql;
    //集鸽记录
    1: SQL.Text := 'SELECT DISTINCT P.ID FROM JOIN_Pigeon P JOIN JOIN_GameSource S ON ' +
        'S.PigeonIdx=P.ID AND S.' + GetMatchSql + StatusSql;
    //未归巢记录
    2: SQL.Text := 'SELECT DISTINCT P.ID FROM JOIN_Pigeon P JOIN JOIN_GameSource S ON ' +
        'S.PigeonIdx=P.ID AND S.IsBack = 0 AND S.' + GetMatchSql + StatusSql;
    //未集鸽记录
    3: SQL.Text := 'SELECT DISTINCT P.ID FROM JOIN_Pigeon P WHERE P.ID not IN(' +
      'SELECT PigeonIdx FROM JOIN_GameSource WHERE ' + GetMatchSql + ')' + StatusSql;
    end;
    Open;
    while not EOF do begin
      FCoteDB.ADOConn.Execute('UPDATE JOIN_Pigeon SET Status=''' + EChangeStatus.Text +
        ''',SynFlag=''E'' WHERE ID=''' + Fields[0].AsString + '''');
      Next;
    end;
    mLog.Lines.Add('    系统已完成赛鸽状态修改工作。');
  end;
  ShowMessage('修改完成');
end;

(* 人工表格处理 *)

procedure TFCoteGrouping.GridManualDblClick(Sender: TObject);
begin
  ESubjectName.Text:= GridManual.Cells[0, GridManual.Row];
  ESubjectValue.Text:= GridManual.Cells[1, GridManual.Row];
end;

procedure TFCoteGrouping.GridManualFixedCellClick(Sender: TObject; ACol,
  ARow: Integer);
begin
  if ACol = 0 then Exit;
  if GridManual.Cells[1, 0] = '标记' then GridManual.Cells[1, 0]:= '大团体'
  else if GridManual.Cells[1, 0] = '大团体' then GridManual.Cells[1, 0]:= '小团体'
  else if GridManual.Cells[1, 0] = '小团体' then GridManual.Cells[1, 0]:= '标记';
end;

procedure TFCoteGrouping.btnGridAddClick(Sender: TObject);
begin
  if (GridManual.RowCount > 2) or (GridManual.Cells[0, 1] <> '') then
    GridManual.RowCount:= GridManual.RowCount + 1;
  GridManual.Cells[0, GridManual.RowCount - 1]:= ESubjectName.Text;
  GridManual.Cells[1, GridManual.RowCount - 1]:= ESubjectValue.Text;
end;

procedure TFCoteGrouping.btnGridDelClick(Sender: TObject);
var
  I: Integer;
begin
  for I := GridManual.Row to GridManual.RowCount - 2 do begin
    GridManual.Cells[0, I]:= GridManual.Cells[0, I + 1];
    GridManual.Cells[1, I]:= GridManual.Cells[1, I + 1];
  end;
  GridManual.Cells[0, GridManual.RowCount - 1]:= '';
  GridManual.Cells[1, GridManual.RowCount - 1]:= '';
  if GridManual.RowCount > 2 then GridManual.RowCount:= GridManual.RowCount - 1;
end;

procedure TFCoteGrouping.btnGridEditClick(Sender: TObject);
begin
  GridManual.Cells[0, GridManual.Row]:= ESubjectName.Text;
  GridManual.Cells[1, GridManual.Row]:= ESubjectValue.Text;
end;

procedure TFCoteGrouping.btnGridPasteClick(Sender: TObject);
var
  lst: TStringList;
  I, mPos: Integer;
  S: string;
begin
  lst:= TStringList.Create;
  lst.Text:= Clipboard.AsText;
  GridManual.RowCount:= lst.Count + 1;
  for I := 0 to lst.Count - 1 do begin
    S:= lst[I];
    mPos:= Pos(#9, S);
    if mPos > 0 then begin
      GridManual.Cells[0, I + 1]:= Copy(S, 1, mPos - 1);
      GridManual.Cells[1, I + 1]:= Copy(S, mPos + 1, MaxInt);
    end;
  end;
  lst.Free;
end;

procedure TFCoteGrouping.btnGridUpdateClick(Sender: TObject);
var
  S: string;
  I, mPos, FailedCount, ExecResult: Integer;
begin
  if GridManual.Cells[1, 0] = '状态' then S:= 'Status=''%s'''
  else if GridManual.Cells[1, 0] = '大团体' then S:= 'Group1=''%s'''
  else if GridManual.Cells[1, 0] = '小团体' then S:= 'Group2=''%s'''
  else if GridManual.Cells[1, 0] = '标记' then S:= 'Flag=''%s''';
  FailedCount:= 0;
  for I := 1 to GridManual.RowCount - 1 do begin
    if GridManual.Cells[0, I] = '' then Continue;
    FCoteDB.ADOConn.Execute(Format('UPDATE JOIN_Pigeon SET ' + S + ',SynFlag=''E'' WHERE GBCode=''%s''',
      [GridManual.Cells[1, I], GridManual.Cells[0, I]]), ExecResult);
    if ExecResult > 0 then GridManual.Cells[2, I]:= '成功'
    else begin
      Inc(FailedCount);
      GridManual.Cells[2, I]:= '执行失败';
      mLog.Lines.Add(GridManual.Cells[0, I] + ' 执行失败!');
    end;
  end;
  if FailedCount > 0 then DlgInfo('执行结果', '有' + IntToStr(FailedCount) + '未执行成功，请检查！', 3);
end;

end.
