object FCoteGroupingSearch: TFCoteGroupingSearch
  Left = 0
  Top = 0
  Caption = 'FCoteGroupingSearch'
  ClientHeight = 565
  ClientWidth = 853
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 153
    Top = 0
    Height = 565
    ExplicitLeft = 225
    ExplicitTop = -53
    ExplicitHeight = 551
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 153
    Height = 565
    Align = alLeft
    Caption = #36187#20107#21015#34920
    TabOrder = 0
    object Panel1: TPanel
      Left = 2
      Top = 397
      Width = 149
      Height = 166
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object btnStatistic: TBitBtn
        AlignWithMargins = True
        Left = 3
        Top = 138
        Width = 143
        Height = 25
        Align = alBottom
        Caption = #32479#35745
        Default = True
        Glyph.Data = {
          4E010000424D4E01000000000000760000002800000013000000120000000100
          040000000000D800000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FF8F8F8F8F8F
          8F8F8F800000F0000000000000000000000080FFFFFFFFFFFFFFFFF00000F0FF
          FFFFFFFF9FFFFFF0000080FFFFFFFFFF99FFFFF00000F0FFFFFFFFF9F9FFFFF0
          000080FFFFFFFFF9FF9FFFC00000F0CFF9FFFF9FFF9FFCF0000080FC9F9FFF9F
          FFF9FCF00000F0F9CFF9FF9FFFFFCFF0000080F9FCFF99FFFFFFCFF00000F09F
          FFCFF9FFCCCCF9F0000080FFFFFCFFFCFFFFF9F00000F0FFFFFFCFFCFFFFFF90
          000080FFFFFFFCCFFFFFFFF00000F0FFFFFFFFFFFFFFFFF0000080FFFFFFFFFF
          FFFFFFF00000F0FFFFFFFFFFFFFFFFF00000}
        TabOrder = 0
        OnClick = btnStatisticClick
      end
      object RGOrderType: TRadioGroup
        AlignWithMargins = True
        Left = 3
        Top = 66
        Width = 143
        Height = 66
        Align = alBottom
        Caption = #25490#21517#26041#24335
        ItemIndex = 0
        Items.Strings = (
          #25968#37327#21644#21512#35745#20998#36895
          #25968#37327#21644#26368#22909#25104#32489)
        TabOrder = 1
      end
      object Panel2: TPanel
        AlignWithMargins = True
        Left = 3
        Top = 37
        Width = 143
        Height = 27
        Align = alTop
        AutoSize = True
        BevelOuter = bvNone
        Caption = 'Panel2'
        ShowCaption = False
        TabOrder = 2
        object Label2: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 48
          Height = 21
          Align = alLeft
          Caption = #37319#29992#20998#32452
          Layout = tlCenter
          ExplicitHeight = 13
        end
        object EGroup: TComboBox
          AlignWithMargins = True
          Left = 57
          Top = 3
          Width = 83
          Height = 21
          Align = alClient
          Style = csDropDownList
          TabOrder = 0
        end
      end
      object Panel3: TPanel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 143
        Height = 28
        Align = alTop
        AutoSize = True
        BevelOuter = bvNone
        Caption = 'Panel2'
        ShowCaption = False
        TabOrder = 3
        object Label1: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 48
          Height = 22
          Align = alLeft
          Caption = #26377#25928#21517#27425
          Layout = tlCenter
          ExplicitHeight = 13
        end
        object EMaxOrder: TSpinEdit
          AlignWithMargins = True
          Left = 57
          Top = 3
          Width = 83
          Height = 22
          Align = alClient
          MaxValue = 5000
          MinValue = 10
          TabOrder = 0
          Value = 200
        end
      end
    end
    object DBGrid2: TDBGrid
      Left = 2
      Top = 15
      Width = 149
      Height = 382
      Align = alClient
      DataSource = DSMatch
      ReadOnly = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'MatchName'
          Title.Caption = #36187#20107#21517#31216
          Width = 96
          Visible = True
        end>
    end
  end
  object Panel4: TPanel
    Left = 156
    Top = 0
    Width = 697
    Height = 565
    Align = alClient
    BevelOuter = bvSpace
    Caption = 'Panel2'
    ShowCaption = False
    TabOrder = 1
    object Panel5: TPanel
      Left = 1
      Top = 529
      Width = 695
      Height = 35
      Align = alBottom
      BevelOuter = bvNone
      Caption = 'Panel3'
      ShowCaption = False
      TabOrder = 0
      object btnExportExcel: TButton
        AlignWithMargins = True
        Left = 575
        Top = 3
        Width = 117
        Height = 29
        Align = alRight
        Caption = #23548#20986'Excel'
        ImageIndex = 34
        ImageMargins.Left = 5
        Images = FCoteDB.ImgButton
        TabOrder = 0
        OnClick = btnExportExcelClick
      end
    end
    object grid: TStringGrid
      Left = 1
      Top = 1
      Width = 695
      Height = 528
      Align = alClient
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goRowMoving]
      TabOrder = 1
    end
  end
  object DBMatch: TADOTable
    Connection = FCoteDB.ADOConn
    Filter = 'Status<>0'
    Filtered = True
    TableName = 'JOIN_Match'
    Left = 48
    Top = 152
  end
  object DSMatch: TDataSource
    DataSet = DBMatch
    Left = 48
    Top = 200
  end
  object DQStatistic: TADOQuery
    Connection = FCoteDB.ADOConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT MG.[PID], MG.[State], MG.[CITY], MG.[County], MG.[MID], M' +
        'G.[MNAME], MG.GBCode, MG.CT, CAST(MG.SumValue AS FLOAT) AS SumVa' +
        'lue FROM(SELECT TT.[PID], TT.[State], TT.[CITY], TT.[County], TT' +
        '.[MID], TT.[MNAME], TT.[GBCode], COUNT(*) AS CT, SUM([Order1])AS' +
        ' SumValue FROM('
      
        'SELECT S.[GameID], P.[ID] AS PID, M.[State], M.[CITY], M.[County' +
        '], P.[MemberIDX] AS MID, M.[MemberName] AS MNAME, P.[GBCode] AS ' +
        'GBCode, S.[Order1]'
      'FROM PGN_GameSource S, PGN_Pigeon P, PGN_Member M'
      
        'WHERE S.[IsBack]=1 AND P.[ID] = S.[PigeonID] AND P.[MemberIDX] =' +
        ' M.[ID]'
      'AND S.[GameID]=1'
      'AND S.[IsBack]=1'
      'AND S.[Order1] <= 123'
      'UNION'
      
        'SELECT S.[GameID], P.[ID] AS PID, M.[State], M.[CITY], M.[County' +
        '], P.[MemberIDX] AS MID, M.[MemberName] AS MNAME, P.[GBCode] AS ' +
        'GBCode, S.[Order1]'
      'FROM PGN_GameSource S, PGN_Pigeon P, PGN_Member M'
      
        'WHERE S.[IsBack]=1 AND P.[ID] = S.[PigeonID] AND P.[MemberIDX] =' +
        ' M.[ID]'
      'AND S.[GameID]=2'
      'AND S.[IsBack]=1'
      'AND S.[Order1] <= 123'
      
        ')AS TT GROUP BY TT.[PID], TT.[State], TT.[CITY], TT.[County], TT' +
        '.[MID], TT.[MNAME], TT.[GBCode])AS MG WHERE MG.CT = 2'
      'ORDER BY MG.SumValue ASC'
      '')
    Left = 48
    Top = 256
  end
  object SaveDialog1: TSaveDialog
    Left = 777
    Top = 472
  end
end
