unit UCoteGroupingSearch;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.ExtCtrls, Vcl.Grids,
  Vcl.ComCtrls, Vcl.StdCtrls, Data.Win.ADODB, Vcl.DBGrids, Vcl.Samples.Spin,
  Vcl.Buttons, UCoteDB, UCoteInfo, UVonOffice;

type
  TFCoteGroupingSearch = class(TForm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    btnStatistic: TBitBtn;
    RGOrderType: TRadioGroup;
    Panel2: TPanel;
    Label2: TLabel;
    EGroup: TComboBox;
    Panel3: TPanel;
    Label1: TLabel;
    EMaxOrder: TSpinEdit;
    DBGrid2: TDBGrid;
    DBMatch: TADOTable;
    DSMatch: TDataSource;
    Panel4: TPanel;
    Panel5: TPanel;
    btnExportExcel: TButton;
    grid: TStringGrid;
    Splitter1: TSplitter;
    DQStatistic: TADOQuery;
    SaveDialog1: TSaveDialog;
    procedure FormCreate(Sender: TObject);
    procedure btnStatisticClick(Sender: TObject);
    procedure btnExportExcelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  PSourceData = ^RSourceData;
  RSourceData = record
    MemberIdx: TGuid;
    GroupValue: string;
    Pigeons: TList;
  end;

var
  FCoteGroupingSearch: TFCoteGroupingSearch;

implementation

uses Math;

{$R *.dfm}

function OrderPigeon(Item1, Item2: Pointer): Integer;
begin
  result:= Sign(TSourceView(Item1).SourceOrder - TSourceView(Item2).SourceOrder);
end;

function OrderSourcceByOrder(Item1, Item2: Pointer): Integer;
  function MinOrder(Data: PSourceData): Integer;
  var
    I: Integer;
  begin
    Result:= MaxInt;
    for I := 0 to Data.Pigeons.Count - 1 do
      if Result > TSourceView(Data.Pigeons[I]).SourceOrder then
        Result := TSourceView(Data.Pigeons[I]).SourceOrder;
  end;
begin
  result:= Sign(PSourceData(Item2).Pigeons.Count - PSourceData(Item1).Pigeons.Count);
  if result = 0 then
    result:= Sign(MinOrder(PSourceData(Item1)) - MinOrder(PSourceData(Item2)));
end;

function OrderSourcceBySpeed(Item1, Item2: Pointer): Integer;
  function SumSpeed(Data: PSourceData): Extended;
  var
    I: Integer;
  begin
    Result:= 0;
    for I := 0 to Data.Pigeons.Count - 1 do
      Result:= Result + TSourceView(Data.Pigeons[I]).Speed;
  end;
begin
  result:= Sign(PSourceData(Item2).Pigeons.Count - PSourceData(Item1).Pigeons.Count);
  if result = 0 then
    result:= Sign(SumSpeed(PSourceData(Item2)) - SumSpeed(PSourceData(Item1)));
end;

procedure TFCoteGroupingSearch.btnExportExcelClick(Sender: TObject);
var
  Row, Col: Integer;
  xls: TVonExcelWriter;
begin
  SaveDialog1.DefaultExt := '';
  SaveDialog1.Filter := 'Excel文件|*.xls';
  if not SaveDialog1.Execute then Exit;
  xls:= TVonExcelWriter.Create(ChangeFileExt(SaveDialog1.filename, '.xls'));
  Try
    for Row := 0 to Grid.RowCount - 1 do
      for Col := 0 to Grid.ColCount - 1 do
        xls.WriteCell(Col, Row, Grid.Cells[Col, Row]);
  finally
    xls.Free;
  end;
end;

procedure TFCoteGroupingSearch.btnStatisticClick(Sender: TObject);
var
  lstSource: TList;
  Titles: TStringList;
  Info: TSourceView;
  R, C, MID, PID: Integer;
  S: String;
  procedure SumMemberGrouping(Src: TSourceView; GroupValue: string);
  var
    I: Integer;
    Data: PSourceData;
  begin
    for I := 0 to lstSource.Count - 1 do
      if(PSourceData(lstSource[I]).MemberIdx = Src.MemberIdx)and(PSourceData(lstSource[I]).GroupValue = GroupValue)then begin
        PSourceData(lstSource[I]).Pigeons.Add(Src);
        Exit;
      end;
    New(Data);
    Data.MemberIdx:= Src.MemberIdx;
    Data.GroupValue:= GroupValue;
    Data.Pigeons:= TList.Create;
    Data.Pigeons.Add(Src);
    lstSource.Add(Data);
  end;
begin
  lstSource:= TList.Create;
  //提取该场赛事所有有效成绩的赛鸽信息
  case EGroup.ItemIndex of
  0: S:= 'Group1 <> ''''';
  1: S:= 'Group2 <> ''''';
  2: S:= 'Group3 <> ''''';
  3: S:= 'Group4 <> ''''';
  4: S:= 'Group5 <> ''''';
  end;
  S:= S + ' AND SourceOrder > 0 AND SourceOrder <= ' + IntToStr(EMaxOrder.Value) +
    ' AND GameIdx=''' + DBMatch.FieldByName('ID').AsString + '''';
  TSourceView.OpenQuery(DQStatistic, S);
  with DQStatistic do
    while not EOF do begin
      Info:= TSourceView.Create;
      Info.LoadFromDB(DQStatistic);
      case EGroup.ItemIndex of
      0: SumMemberGrouping(Info, Info.Group1);
      1: SumMemberGrouping(Info, Info.Group2);
      2: SumMemberGrouping(Info, Info.Group3);
      3: SumMemberGrouping(Info, Info.Group4);
      4: SumMemberGrouping(Info, Info.Group5);
      end;
      Next;
    end;
  //排序
  for MID := 0 to lstSource.Count - 1 do                   //按人员+分组排名循环
    PSourceData(lstSource[MID]).Pigeons.Sort(OrderPigeon);
  case RGOrderType.ItemIndex of
  0: lstSource.Sort(OrderSourcceBySpeed);
  1: lstSource.Sort(OrderSourcceByOrder);
  end;
  //提取显示字段标题
  Titles:= GetDisplayTitles('FIELDS', 'StageStatisticDisplayFields');
  //显示标题
  grid.RowCount:= DQStatistic.RecordCount + 1;
  grid.ColCount:= Titles.Count + 1;
  grid.Cells[0, 0]:= '排名';
  for C := 0 to Titles.Count - 1 do begin
    grid.Cells[C + 1, 0]:= Titles.ValueFromIndex[C];
    grid.ColWidths[C + 1]:= Integer(Titles.Objects[C]);
  end;
  grid.FixedRows:= 1;
  R:= 1;
  for MID := 0 to lstSource.Count - 1 do begin         //按人员+分组排名循环
    grid.Cells[0, R]:= IntToStr(MID + 1);              //显示排名
    for PID := 0 to PSourceData(lstSource[MID]).Pigeons.Count - 1 do begin//显示下属赛鸽
      for C := 0 to Titles.Count - 1 do                //显示内容
        grid.Cells[C + 1, R]:= TSourceView(PSourceData(lstSource[MID]).Pigeons[PID]).Values[Titles.Names[C]];
      TSourceView(PSourceData(lstSource[MID]).Pigeons[PID]).Free;
      Inc(R);
      grid.Cells[0, R]:= '';                           //清空排名
    end;
    PSourceData(lstSource[MID]).Pigeons.Clear;
    FreeMemory(lstSource[MID]);
  end;
  lstSource.Clear;
  lstSource.Free;
end;

procedure TFCoteGroupingSearch.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  FCoteDB.ReinitGroup;
  for I := 0 to 4 do
    if FCoteDB.FGroupNames[I] <> '' then
      EGroup.Items.Add(FCoteDB.FGroupNames[I]);
  EGroup.ItemIndex := 0;
  DBMatch.Open;
end;

end.
