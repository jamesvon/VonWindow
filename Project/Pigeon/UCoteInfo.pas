unit UCoteInfo;

interface

uses SysUtils, Classes, DB, ADODB, UVonSystemFuns, UPlatformInfo, UVonCrypt,
  UVonConfig, Graphics, ComCtrls, UVonLog;

resourcestring
  RES_Invalid_GBCode = '%s 不是一个有效的统一环号';
  RES_Invalid_INFO = '提取数据失败，%s';

const Prefix = 'JOIN_';

type
  /// <summary>赛事状态</summary>
  /// <param name="JZS_CANCEL">取消状态</param>
  /// <param name="JZS_EDIT">编辑状态</param>
  /// <param name="JZS_ENRL">开始报名</param>
  /// <param name="JZS_COLL">集鸽状态</param>
  /// <param name="JZS_TRAN">在途状态</param>
  /// <param name="JZS_START">开笼状态</param>
  /// <param name="JZS_BACK">归巢状态</param>
  /// <param name="JZS_OVER">结束状态</param>
  TJZMatchStatus = (JZS_CANCEL, JZS_EDIT, JZS_ENRL, JZS_COLL, JZS_TRAN,
    JZS_START, JZS_BACK, JZS_OVER);

const
  JZMatchStatusNames: array[0..7]of string = ('取消', '编辑', '报名', '集鸽', '在途', '开笼', '归巢', '结束');

type
  TGBCode = class
  private
    FNation: string;
    FAssociation: string;
    FYear: integer;
    FCode: string;
    procedure SetAssociation(const Value: string);
    procedure SetCode(const Value: string);
    procedure SetNation(const Value: string);
    procedure SetValue(const Value: string);
    procedure SetYear(const Value: integer);
    function GetValue: string;
  published
    property Nation: string read FNation write SetNation;
    property Association: string read FAssociation write SetAssociation;
    property Year: integer read FYear write SetYear;
    property Code: string read FCode write SetCode;
    property Value: string read GetValue write SetValue;
  end;
  /// <summary>基础信息</summary>
  TBaseInfo = class
  private
    FConn: TADOConnection;
    FSynFlag: Char;
    FModifiedate: TDatetime;
  published
    /// <summary>修改日期</summary>
    property Modifiedate: TDatetime read FModifiedate write FModifiedate;
    /// <summary>同步标记，A:Appended,U:Updated,D:Deleted;O:Origened</summary>
    property SynFlag: Char read FSynFlag write FSynFlag;
    property Conn: TADOConnection read FConn write FConn;
  end;

  /// <summary>公棚信息</summary>
  TCoteInfo = class(TInfoBase)
  private
    FPWD: string;
    FLatitude: Extended;
    FOrgName: string;
    FShortName: string;
    FZip: string;
    FState: string;
    FID: TGUID;
    FLongitude: Extended;
    FStatus: string;
    FAddress: string;
    FCity: string;
    FRegion: string;
    FContact: string;
    FLinker: string;
    FMobile: string;
    FLoaded: Boolean;
  public
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean; override;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: TGuid): Boolean;
    /// <summary>存储信息</summary>
    function UpdateToDB(DataSet : TCustomADODataSet): Boolean;
    function AppendToDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接存储</summary>
    procedure UpdateData(AConn: TADOConnection);
    procedure AppendData(AConn: TADOConnection);
    /// <summary>检查数据的有效性</summary>
    function CheckInfo: boolean; override;
  published
    /// <summary>序号</summary>
    property ID: TGUID read FID write FID;
    /// <summary>公棚名称</summary>
    property OrgName: string read FOrgName write FOrgName;
    /// <summary>公棚简称</summary>
    property ShortName: string read FShortName write FShortName;
    /// <summary>所属区域</summary>
    property Region: string read FRegion write FRegion;
    /// <summary>所在省份</summary>
    property State: string read FState write FState;
    /// <summary>所在地市</summary>
    property City: string read FCity write FCity;
    /// <summary>地址</summary>
    property Address: string read FAddress write FAddress;
    /// <summary>邮编</summary>
    property Zip: string read FZip write FZip;
    /// <summary>联系方式</summary>
    property Contact: string read FContact write FContact;
    /// <summary>联系人</summary>
    property Linker: string read FLinker write FLinker;
    /// <summary>联系电话</summary>
    property Mobile: string read FMobile write FMobile;
    /// <summary>经度</summary>
    property Longitude: Extended read FLongitude write FLongitude;
    /// <summary>纬度</summary>
    property Latitude: Extended read FLatitude write FLatitude;
    /// <summary>状态</summary>
    property Status: string read FStatus write FStatus;
    /// <summary>PWD</summary>
    property PWD: string read FPWD write FPWD;
    property Loaded: Boolean read FLoaded write FLoaded;
  end;

  /// <summary>鸽主</summary>
  TMemberInfo = class(TBaseInfo)
  private
    FNote: String;
    FOrgIDX: TGUID;
    FMemberName: string;
    FCash: Extended;
    FLat: Extended;
    FLon: Extended;
    FCashDate: TDatetime;
    FMemberNo: string;
    FDisplayOrder: integer;
    FManager: string;
    FEMail: string;
    FID: TGUID;
    FKind: string;
    FShortName: string;
    FAddress: string;
    FCity: string;
    FCounty: string;
    FMobile: string;
    FCashKind: string;
    FAssociationNo: string;
    FState: string;
    FStatus: string;
  public
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: TGuid): Boolean;
    /// <summary>追加信息</summary>
    function AppendToDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>修改信息</summary>
    function UpdateToDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接追加</summary>
    function AppendData(AConn: TADOConnection): Boolean;
    /// <summary>SQL直接修改</summary>
    function UpdateData(AConn: TADOConnection): Boolean;
    /// <summary>检查数据的有效性</summary>
    function CheckInfo: boolean;
    /// <summary>获取最大序号</summary>
    function MaxOrder(AConn: TADOConnection): Integer;
  published
    /// <summary>序号</summary>
    property ID: TGUID read FID write FID;
    /// <summary>姓名</summary>
    property MemberName: string read FMemberName write FMemberName;
    /// <summary>鸽主编号</summary>
    property MemberNo: string read FMemberNo write FMemberNo;
    /// <summary>会员编号</summary>
    property AssociationNo: string read FAssociationNo write FAssociationNo;
    /// <summary>所属公棚</summary>
    property OrgIDX: TGUID read FOrgIDX write FOrgIDX;
    /// <summary>短名称</summary>
    property ShortName: string read FShortName write FShortName;
    /// <summary>类型</summary>
    property Kind: string read FKind write FKind;
    /// <summary>Status</summary>
    property Status: string read FStatus write FStatus;
    /// <summary>联系方式</summary>
    property EMail: string read FEMail write FEMail;
    /// <summary>缴费金额</summary>
    property Cash: Extended read FCash write FCash;
    /// <summary>缴费日期</summary>
    property CashDate: TDatetime read FCashDate write FCashDate;
    /// <summary>缴费方式</summary>
    property CashKind: string read FCashKind write FCashKind;
    /// <summary>所在省份</summary>
    property State: string read FState write FState;
    /// <summary>所在城市</summary>
    property City: string read FCity write FCity;
    /// <summary>所在区县</summary>
    property County: string read FCounty write FCounty;
    /// <summary>地址</summary>
    property Address: string read FAddress write FAddress;
    /// <summary>管理人</summary>
    property Manager: string read FManager write FManager;
    /// <summary>联系电话</summary>
    property Mobile: string read FMobile write FMobile;
    /// <summary>显示序号</summary>
    property DisplayOrder: integer read FDisplayOrder write FDisplayOrder;
    /// <summary>经度</summary>
    property Lon: Extended read FLon write FLon;
    /// <summary>维度</summary>
    property Lat: Extended read FLat write FLat;
    /// <summary>备注</summary>
    property Note: String read FNote write FNote;
  end;

  /// <summary>赛鸽</summary>
  TPigeonInfo = class(TBaseInfo)
  private
    FPigeonNO: string;
    FFeather: string;
    FFlag: string;
    FMemberIdx: TGUID;
    FOrgIdx: TGUID;
    FJoinDate: TDatetime;
    FEye: string;
    FFactoryCode: int64;
    FGender: string;
    FID: TGUID;
    FHouseNo: string;
    FStatus: string;
    FJZCode: int64;
    FGroup: array[0..4]of string;
    FAssist: array[0..4]of string;
    FGBCode: string;
    FDisplayOrder: Integer;
    function GetGroup(index: integer): string;
    procedure SetGroup(index: integer; const Value: string);
    function GetAssist(index: integer): string;
    procedure SetAssist(index: integer; const Value: string);
  public
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: TGuid): Boolean;
    /// <summary>追加信息</summary>
    function AppendToDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>修改信息</summary>
    function UpdateToDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接追加</summary>
    function AppendData(AConn: TADOConnection): Boolean;
    /// <summary>SQL直接修改</summary>
    function UpdateData(AConn: TADOConnection): Boolean;
    /// <summary>检查数据的有效性</summary>
    function CheckInfo: boolean;
    function MaxOrder(AConn: TADOConnection): Integer;
    property Group[index: integer]: string read GetGroup write SetGroup;
    property Assist[index: integer]: string read GetAssist write SetAssist;
  published
    /// <summary>序号</summary>
    property ID: TGUID read FID write FID;
    /// <summary>赛鸽编号</summary>
    property PigeonNO: string read FPigeonNO write FPigeonNO;
    /// <summary>公棚索引</summary>
    property OrgIdx: TGUID read FOrgIdx write FOrgIdx;
    /// <summary>鸽主索引</summary>
    property MemberIdx: TGUID read FMemberIdx write FMemberIdx;
    /// <summary>窝棚号</summary>
    property HouseNo: string read FHouseNo write FHouseNo;
    /// <summary>统一环号</summary>
    property GBCode: string read FGBCode write FGBCode;
    /// <summary>系统标记</summary>
    property Flag: string read FFlag write FFlag;
    /// <summary>厂商码</summary>
    property FactoryCode: int64 read FFactoryCode write FFactoryCode;
    /// <summary>内部码</summary>
    property JZCode: int64 read FJZCode write FJZCode;
    /// <summary>公母</summary>
    property Gender: string read FGender write FGender;
    /// <summary>羽色</summary>
    property Feather: string read FFeather write FFeather;
    /// <summary>眼砂</summary>
    property Eye: string read FEye write FEye;
    /// <summary>显示序号</summary>
    property DisplayOrder: Integer read FDisplayOrder write FDisplayOrder;
    /// <summary>状态</summary>
    property Status: string read FStatus write FStatus;
    /// <summary>入棚日期</summary>
    property JoinDate: TDatetime read FJoinDate write FJoinDate;
  end;

  /// <summary>赛事</summary>
  TMatchInfo = class(TBaseInfo)
  private
    FInfo: String;
    FRule: String;
    FNote: String;
    FSettings: string;
    FSunRaise: TDatetime;
    FGameLong: Extended;
    FGameAddress: string;
    FCreateDate: TDatetime;
    FFlyDate: TDatetime;
    FHumidity: string;
    FSunDown: TDatetime;
    FCipher: string;
    FWindPower: string;
    FLat: Extended;
    FLon: Extended;
    FWind: string;
    FUploadSetting: Integer;
    FManager: string;
    FID: TGUID;
    FStatus: TJZMatchStatus;
    FKind: string;
    FCollectionDate: TDatetime;
    FCoteIdx: TGUID;
    FMatchName: string;
    FTemperature: Extended;
    FCloseDate: TDatetime;
    FTelphone: string;
    FSMSCondition: string;
    function GetFlag(Delault: Char): Char;
  public
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: TGuid): Boolean;
    /// <summary>追加信息</summary>
    function AppendToDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>修改信息</summary>
    function UpdateToDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接追加</summary>
    function AppendData(AConn: TADOConnection): Boolean;
    /// <summary>SQL直接修改</summary>
    function UpdateData(AConn: TADOConnection): Boolean;
    /// <summary>检查数据的有效性</summary>
    function CheckInfo: boolean;
    function BeginMatch(AConn: TADOConnection): Boolean;
  published
    /// <summary>序号</summary>
    property ID: TGUID read FID write FID;
    /// <summary>赛事名称</summary>
    property MatchName: string read FMatchName write FMatchName;
    /// <summary>所属公棚</summary>
    property CoteIdx: TGUID read FCoteIdx write FCoteIdx;
    /// <summary>放飞地址</summary>
    property GameAddress: string read FGameAddress write FGameAddress;
    /// <summary>司放人</summary>
    property Manager: string read FManager write FManager;
    /// <summary>赛事距离</summary>
    property GameLong: Extended read FGameLong write FGameLong;
    /// <summary>上传设置，#000：表示不上传，#001：集鸽上传，#010：归巢上传，#100：指定上传</summary>
    property UploadSetting: Integer read FUploadSetting write FUploadSetting;
    /// <summary>赛事类型</summary>
    property SMSCondition: string read FSMSCondition write FSMSCondition;
    /// <summary>赛事类型</summary>
    property Kind: string read FKind write FKind;
    /// <summary>赛事状态</summary>
    property Status: TJZMatchStatus read FStatus write FStatus;
    /// <summary>联系电话</summary>
    property Telphone: string read FTelphone write FTelphone;
    /// <summary>放飞日期</summary>
    property FlyDate: TDatetime read FFlyDate write FFlyDate;
    /// <summary>经度</summary>
    property Lon: Extended read FLon write FLon;
    /// <summary>维度</summary>
    property Lat: Extended read FLat write FLat;
    /// <summary>结束时间</summary>
    property CloseDate: TDatetime read FCloseDate write FCloseDate;
    /// <summary>秘钥</summary>
    property Cipher: string read FCipher write FCipher;
    /// <summary>集鸽日期</summary>
    property CollectionDate: TDatetime read FCollectionDate write FCollectionDate;
    /// <summary>日出时间</summary>
    property SunRaise: TDatetime read FSunRaise write FSunRaise;
    /// <summary>风向</summary>
    property Wind: string read FWind write FWind;
    /// <summary>风力</summary>
    property WindPower: string read FWindPower write FWindPower;
    /// <summary>温度</summary>
    property Temperature: Extended read FTemperature write FTemperature;
    /// <summary>湿度</summary>
    property Humidity: string read FHumidity write FHumidity;
    /// <summary>日落时间</summary>
    property SunDown: TDatetime read FSunDown write FSunDown;
    /// <summary>创建日期</summary>
    property CreateDate: TDatetime read FCreateDate write FCreateDate;
    /// <summary>赛事说明</summary>
    property Info: String read FInfo write FInfo;
    /// <summary>赛事规则</summary>
    property Rule: String read FRule write FRule;
    /// <summary>备注</summary>
    property Note: String read FNote write FNote;
    /// <summary>赛事设置</summary>
    property Settings: string read FSettings write FSettings;
  end;

  /// <summary>赛绩</summary>
  TGameSourceInfo = class(TBaseInfo)
  private
    FSourceOrder: integer;
    FReportTime: TDatetime;
    FSetCode: string;
    FGameIdx: TGUID;
    FMatchLong: Extended;
    FID: integer;
    FGameCode: integer;
    FKind: integer;
    FIsBack: integer;
    FFlyTime: Extended;
    FCollectID: integer;
    FCollecteDate: TDatetime;
    FSpeed: Extended;
    FPigeonIdx: TGUID;
    FNote: string;
    FCollecteNo: string;
    FClientID: Char;
    FSmsFlag: Char;
    procedure AutoCollectID;
  public
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: Integer): Boolean;
    /// <summary>存储信息</summary>
    function SaveToDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接存储</summary>
    function SaveData(AConn: TADOConnection): Boolean;
    /// <summary>检查数据的有效性</summary>
    function CheckInfo: boolean;
  published
    /// <summary>集鸽设备序号</summary>
    property ClientID: Char read FClientID write FClientID;
    /// <summary>序号</summary>
    property ID: integer read FID write FID;
    /// <summary>赛事索引</summary>
    property GameIdx: TGUID read FGameIdx write FGameIdx;
    /// <summary>赛鸽索引</summary>
    property PigeonIdx: TGUID read FPigeonIdx write FPigeonIdx;
    /// <summary>赛事码</summary>
    property GameCode: integer read FGameCode write FGameCode;
    /// <summary>绑印号</summary>
    property CollecteNo: string read FCollecteNo write FCollecteNo;
    /// <summary>集鸽日期</summary>
    property CollecteDate: TDatetime read FCollecteDate write FCollecteDate;
    /// <summary>集鸽序号</summary>
    property CollectID: integer read FCollectID write FCollectID;
    /// <summary>设备编码</summary>
    property SetCode: string read FSetCode write FSetCode;
    /// <summary>报道时间</summary>
    property ReportTime: TDatetime read FReportTime write FReportTime;
    /// <summary>归巢时间</summary>
    property FlyTime: Extended read FFlyTime write FFlyTime;
    /// <summary>分速</summary>
    property Speed: Extended read FSpeed write FSpeed;
    /// <summary>归巢状态</summary>
    property IsBack: integer read FIsBack write FIsBack;
    /// <summary>参赛类型</summary>
    property Kind: integer read FKind write FKind;
    /// <summary>排名1</summary>
    property SourceOrder: integer read FSourceOrder write FSourceOrder;
    /// <summary>备注</summary>
    property Note: string read FNote write FNote;
    /// <summary>赛事距离</summary>
    property MatchLong: Extended read FMatchLong write FMatchLong;
    /// <summary>短信标志</summary>
    property SmsFlag: Char read FSmsFlag write FSmsFlag;
  end;

  /// <summary>赛绩查询综合信息</summary>
  TSourceView = class
  private
    FPigeonNO: string;
    FFeather: string;
    FFlag: string;
    FMemberIdx: TGUID;
    FAssist2: string;
    FReportTime: TDatetime;
    FAssist3: string;
    FSetCode: string;
    FGameIdx: TGUID;
    FState: string;
    FAssist1: string;
    FMemberNo: string;
    FMatchLong: Extended;
    FAssist4: string;
    FEye: string;
    FFactoryCode: integer;
    FAssist5: string;
    FGender: string;
    FID: Integer;
    FGameCode: integer;
    FNote: string;
    FKind: integer;
    FShortName: string;
    FStatus: string;
    FIsBack: integer;
    FFlyTime: Extended;
    FGroup2: string;
    FJZCode: integer;
    FGroup3: string;
    FCollectID: integer;
    FGroup1: string;
    FCollecteDate: TDatetime;
    FCity: string;
    FSpeed: Extended;
    FGBCode: string;
    FGroup4: string;
    FPigeonIdx: TGUID;
    FCounty: string;
    FGroup5: string;
    FCollecteNo: string;
    FAssociationNo: string;
    FMemberName: string;
    FEmail: string;
    FMobile: string;
    FMatchName: string;
    FSourceOrder: integer;
    function GetValues(FieldName: string): string;
  public
    /// <summary>初始化Query SQL</summary>
    class function OpenQuery(Query : TADOQuery; Condition: string): TADOQuery;
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: Integer): Boolean;
    /// <summary>SQL直接读取</summary>
    function LoadJZCode(AConn: TADOConnection; jzCode: Int64): Boolean;
    /// <summary>存储归巢数据</summary>
    function CheckIn(AConn: TADOConnection; Sms, Flag: Char): Boolean;
    function Clone: TSourceView;
    property Values[FieldName: string]: string read GetValues;
  published
    /// <summary>序号</summary>
    property ID: Integer read FID write FID;
    /// <summary>赛鸽编号</summary>
    property PigeonNO: string read FPigeonNO write FPigeonNO;
    /// <summary>鸽主索引</summary>
    property MemberIdx: TGUID read FMemberIdx write FMemberIdx;
    /// <summary>统一环号</summary>
    property GBCode: string read FGBCode write FGBCode;
    /// <summary>系统标记</summary>
    property Flag: string read FFlag write FFlag;
    /// <summary>厂商码</summary>
    property FactoryCode: integer read FFactoryCode write FFactoryCode;
    /// <summary>内部码</summary>
    property JZCode: integer read FJZCode write FJZCode;
    /// <summary>分组1</summary>
    property Group1: string read FGroup1 write FGroup1;
    /// <summary>分组2</summary>
    property Group2: string read FGroup2 write FGroup2;
    /// <summary>分组3</summary>
    property Group3: string read FGroup3 write FGroup3;
    /// <summary>分组4</summary>
    property Group4: string read FGroup4 write FGroup4;
    /// <summary>分组5</summary>
    property Group5: string read FGroup5 write FGroup5;
    /// <summary>公母</summary>
    property Gender: string read FGender write FGender;
    /// <summary>羽色</summary>
    property Feather: string read FFeather write FFeather;
    /// <summary>眼砂</summary>
    property Eye: string read FEye write FEye;
    /// <summary>辅助1</summary>
    property Assist1: string read FAssist1 write FAssist1;
    /// <summary>辅助2</summary>
    property Assist2: string read FAssist2 write FAssist2;
    /// <summary>辅助3</summary>
    property Assist3: string read FAssist3 write FAssist3;
    /// <summary>辅助4</summary>
    property Assist4: string read FAssist4 write FAssist4;
    /// <summary>辅助5</summary>
    property Assist5: string read FAssist5 write FAssist5;
    /// <summary>状态</summary>
    property Status: string read FStatus write FStatus;
    /// <summary>鸽主编号</summary>
    property MemberNo: string read FMemberNo write FMemberNo;
    /// <summary>鸽主姓名</summary>
    property MemberName: string read FMemberName write FMemberName;
    /// <summary>会员编号</summary>
    property AssociationNo: string read FAssociationNo write FAssociationNo;
    /// <summary>短名称</summary>
    property ShortName: string read FShortName write FShortName;
    /// <summary>所在省份</summary>
    property State: string read FState write FState;
    /// <summary>所在城市</summary>
    property City: string read FCity write FCity;
    /// <summary>所在区县</summary>
    property County: string read FCounty write FCounty;
    /// <summary>Email</summary>
    property Email: string read FEmail write FEmail;
    /// <summary>联系电话</summary>
    property Mobile: string read FMobile write FMobile;
    /// <summary>赛事索引</summary>
    property GameIdx: TGUID read FGameIdx write FGameIdx;
    /// <summary>赛鸽索引</summary>
    property PigeonIdx: TGUID read FPigeonIdx write FPigeonIdx;
    /// <summary>赛事码</summary>
    property GameCode: integer read FGameCode write FGameCode;
    /// <summary>绑印号</summary>
    property CollecteNo: string read FCollecteNo write FCollecteNo;
    /// <summary>集鸽日期</summary>
    property CollecteDate: TDatetime read FCollecteDate write FCollecteDate;
    /// <summary>集鸽序号</summary>
    property CollectID: integer read FCollectID write FCollectID;
    /// <summary>设备编码</summary>
    property SetCode: string read FSetCode write FSetCode;
    /// <summary>报道时间</summary>
    property ReportTime: TDatetime read FReportTime write FReportTime;
    /// <summary>归巢时间</summary>
    property FlyTime: Extended read FFlyTime write FFlyTime;
    /// <summary>分速</summary>
    property Speed: Extended read FSpeed write FSpeed;
    /// <summary>归巢状态</summary>
    property IsBack: integer read FIsBack write FIsBack;
    /// <summary>参赛类型</summary>
    property Kind: integer read FKind write FKind;
    /// <summary>排名1</summary>
    property SourceOrder: integer read FSourceOrder write FSourceOrder;
    /// <summary>备注</summary>
    property Note: string read FNote write FNote;
    /// <summary>赛事名称</summary>
    property MatchName: string read FMatchName write FMatchName;
    /// <summary>赛事距离</summary>
    property MatchLong: Extended read FMatchLong write FMatchLong;
  end;

  /// <summary>赛鸽综合查询信息</summary>
  TPigeonView = class
  private
    FPigeonNO: string;
    FFeather: string;
    FFlag: string;
    FMemberIdx: TGUID;
    FAssist2: string;
    FAssist3: string;
    FState: string;
    FAssist1: string;
    FMemberNo: string;
    FAssist4: string;
    FEye: string;
    FFactoryCode: Int64;
    FAssist5: string;
    FGender: string;
    FID: TGUID;
    FShortName: string;
    FStatus: string;
    FGroup2: string;
    FJZCode: Int64;
    FGroup3: string;
    FGroup1: string;
    FCity: string;
    FGBCode: string;
    FGroup4: string;
    FCounty: string;
    FGroup5: string;
    FAssociationNo: string;
    FMemberName: string;
    FEMail: string;
    FMobile: string;
    FCashkind: string;
  public
    /// <summary>初始化Query SQL</summary>
    function OpenQuery(Query : TADOQuery; Condition: string): Boolean;
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: TGuid): Boolean; overload;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; Condition: string): Boolean; overload;
  published
    /// <summary>序号</summary>
    property ID: TGUID read FID write FID;
    /// <summary>赛鸽编号</summary>
    property PigeonNO: string read FPigeonNO write FPigeonNO;
    /// <summary>鸽主索引</summary>
    property MemberIdx: TGUID read FMemberIdx write FMemberIdx;
    /// <summary>统一环号</summary>
    property GBCode: string read FGBCode write FGBCode;
    /// <summary>系统标记</summary>
    property Flag: string read FFlag write FFlag;
    /// <summary>厂商码</summary>
    property FactoryCode: Int64 read FFactoryCode write FFactoryCode;
    /// <summary>内部码</summary>
    property JZCode: Int64 read FJZCode write FJZCode;
    /// <summary>分组1</summary>
    property Group1: string read FGroup1 write FGroup1;
    /// <summary>分组2</summary>
    property Group2: string read FGroup2 write FGroup2;
    /// <summary>分组3</summary>
    property Group3: string read FGroup3 write FGroup3;
    /// <summary>分组4</summary>
    property Group4: string read FGroup4 write FGroup4;
    /// <summary>分组5</summary>
    property Group5: string read FGroup5 write FGroup5;
    /// <summary>公母</summary>
    property Gender: string read FGender write FGender;
    /// <summary>羽色</summary>
    property Feather: string read FFeather write FFeather;
    /// <summary>眼砂</summary>
    property Eye: string read FEye write FEye;
    /// <summary>辅助1</summary>
    property Assist1: string read FAssist1 write FAssist1;
    /// <summary>辅助2</summary>
    property Assist2: string read FAssist2 write FAssist2;
    /// <summary>辅助3</summary>
    property Assist3: string read FAssist3 write FAssist3;
    /// <summary>辅助4</summary>
    property Assist4: string read FAssist4 write FAssist4;
    /// <summary>辅助5</summary>
    property Assist5: string read FAssist5 write FAssist5;
    /// <summary>状态</summary>
    property Status: string read FStatus write FStatus;
    /// <summary>鸽主编号</summary>
    property MemberNo: string read FMemberNo write FMemberNo;
    /// <summary>鸽主姓名</summary>
    property MemberName: string read FMemberName write FMemberName;
    /// <summary>会员编号</summary>
    property AssociationNo: string read FAssociationNo write FAssociationNo;
    /// <summary>短名称</summary>
    property ShortName: string read FShortName write FShortName;
    /// <summary>所在省份</summary>
    property State: string read FState write FState;
    /// <summary>所在城市</summary>
    property City: string read FCity write FCity;
    /// <summary>所在区县</summary>
    property County: string read FCounty write FCounty;
    /// <summary>联系电话</summary>
    property EMail: string read FEMail write FEMail;
    /// <summary>联系电话</summary>
    property Mobile: string read FMobile write FMobile;
    /// <summary>付款类型</summary>
    property Cashkind: string read FCashkind write FCashkind;
  end;

  /// <summary>指定鸽</summary>
  TChooseInfo = class(IIdentityInterface)
  private
    FConn: TADOConnection;
    FChoicedTime: TDatetime;
    FPayment: string;
    FGameIdx: TGUID;
    FGambler: TGUID;
    FSubject: string;
    FID: integer;
    FAmount: Extended;
    FStatus: string;
    FSynFlag: string;
    FGamblerNo: TGUID;
    FModifiedate: TDatetime;
    FTicketNo: string;
    FPigeonIdx: TGUID;
  public
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: Integer): Boolean;
    /// <summary>存储信息</summary>
    function SaveToDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接存储</summary>
    function SaveData(AConn: TADOConnection): Boolean;
    /// <summary>检查数据的有效性</summary>
    function CheckInfo: boolean; override;
  published
    /// <summary>序号</summary>
    property ID: integer read FID write FID;
    /// <summary>赛事索引</summary>
    property GameIdx: TGUID read FGameIdx write FGameIdx;
    /// <summary>赛鸽索引</summary>
    property PigeonIdx: TGUID read FPigeonIdx write FPigeonIdx;
    /// <summary>投注人</summary>
    property Gambler: TGUID read FGambler write FGambler;
    /// <summary>投注状态</summary>
    property Status: string read FStatus write FStatus;
    /// <summary>投注项目</summary>
    property Subject: string read FSubject write FSubject;
    /// <summary>投注金额</summary>
    property Amount: Extended read FAmount write FAmount;
    /// <summary>付款方式</summary>
    property Payment: string read FPayment write FPayment;
    /// <summary>单据号</summary>
    property TicketNo: string read FTicketNo write FTicketNo;
    /// <summary>投注号</summary>
    property GamblerNo: TGUID read FGamblerNo write FGamblerNo;
    /// <summary>投注时间</summary>
    property ChoicedTime: TDatetime read FChoicedTime write FChoicedTime;
    /// <summary>修改日期</summary>
    property Modifiedate: TDatetime read FModifiedate write FModifiedate;
    /// <summary>同步标记</summary>
    property SynFlag: string read FSynFlag write FSynFlag;
  end;

  /// <summary>环袋</summary>
  TPackageInfo = class
  private
    FConn: TADOConnection;
    FCreateDate: TDatetime;
    FOrgIDX: integer;
    FID: integer;
    FRingCount: integer;
    FRingColor: string;
    FModifiedate: TDatetime;
    FRingKind: string;
  public
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: Integer): Boolean;
    /// <summary>存储信息</summary>
    function SaveToDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接存储</summary>
    function SaveData(AConn: TADOConnection): Boolean;
    /// <summary>检查数据的有效性</summary>
    function CheckInfo: boolean;
  published
    /// <summary>序号</summary>
    property ID: integer read FID write FID;
    /// <summary>环类型</summary>
    property RingKind: string read FRingKind write FRingKind;
    /// <summary>袋内环数量</summary>
    property RingCount: integer read FRingCount write FRingCount;
    /// <summary>环颜色</summary>
    property RingColor: string read FRingColor write FRingColor;
    /// <summary>创建日期</summary>
    property CreateDate: TDatetime read FCreateDate write FCreateDate;
    /// <summary>所属公棚或协会</summary>
    property OrgIDX: integer read FOrgIDX write FOrgIDX;
    /// <summary>发放日期</summary>
    property Modifiedate: TDatetime read FModifiedate write FModifiedate;
  end;

  /// <summary>环库</summary>
  TRingInfo = class
  private
    FConn: TADOConnection;
    FChipCode: int64;
    FFactoryCode: int64;
    FPackageIdx: integer;
    FID: integer;
    FInputDate: TDatetime;
    FRingKind: string;
    FFactory: string;
  public
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: Integer): Boolean;
    /// <summary>存储信息</summary>
    function SaveToDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接存储</summary>
    function SaveData(AConn: TADOConnection): Boolean;
    /// <summary>检查数据的有效性</summary>
    function CheckInfo: boolean;
  published
    /// <summary>序号</summary>
    property ID: integer read FID write FID;
    /// <summary>环子类型</summary>
    property RingKind: string read FRingKind write FRingKind;
    /// <summary>产地</summary>
    property Factory: string read FFactory write FFactory;
    /// <summary>袋号</summary>
    property PackageIdx: integer read FPackageIdx write FPackageIdx;
    /// <summary>芯片号</summary>
    property ChipCode: int64 read FChipCode write FChipCode;
    /// <summary>厂商号</summary>
    property FactoryCode: int64 read FFactoryCode write FFactoryCode;
    /// <summary>录入时间</summary>
    property InputDate: TDatetime read FInputDate write FInputDate;
  end;

function GetMatchStatusName(Status: TJZMatchStatus): string;

implementation

uses UCoteDB;

function GetMatchStatusName(Status: TJZMatchStatus): string;
begin
  case Status of
    JZS_EDIT:
      Result := '编辑状态';
    JZS_CANCEL:
      Result := '取消状态';
    JZS_COLL:
      Result := '集鸽状态';
    JZS_TRAN:
      Result := '在途状态';
    JZS_START:
      Result := '开笼状态';
    JZS_BACK:
      Result := '归巢状态';
    JZS_OVER:
      Result := '结束状态';
  end;
end;

{ TCoteInfo }

function TCoteInfo.CheckInfo: boolean;
begin
  Result:= False;
  ClearInfo;
  if Length(FOrgName) < 4 then WriteInfo('公棚名称不能为空，数据无效。');
  if FLongitude <= 0 then WriteInfo('纬度必读录入。');
  if FLatitude <= 0 then WriteInfo('经度必读录入。');
  Result:= LastInfo = '';
end;

function TCoteInfo.LoadFromDB(DataSet : TCustomADODataSet): Boolean;
begin
  inherited;
  with DataSet do begin
    FID:= FieldByName('ID').AsGuid;
    FOrgName:= FieldByName('OrgName').AsString;
    FShortName:= FieldByName('ShortName').AsString;
    FRegion:= FieldByName('Region').AsString;
    FState:= FieldByName('State').AsString;
    FCity:= FieldByName('City').AsString;
    FAddress:= FieldByName('Address').AsString;
    FZip:= FieldByName('Zip').AsString;
    FContact:= FieldByName('Contact').AsString;
    FLinker:= FieldByName('Linker').AsString;
    FMobile:= FieldByName('Mobile').AsString;
    FLongitude:= FieldByName('Longitude').AsExtended;
    FLatitude:= FieldByName('Latitude').AsExtended;
    FStatus:= FieldByName('Status').AsString;
    FPWD:= FieldByName('PWD').AsString;
    Result:= true;
  end;
end;

function TCoteInfo.AppendToDB(DataSet: TCustomADODataSet): Boolean;
begin
  FConn:= DataSet.Connection;
  with DataSet do begin
    Append;
    FieldByName('ID').AsGuid:= FID;
    FieldByName('OrgName').AsString:= FOrgName;
    FieldByName('ShortName').AsString:= FShortName;
    FieldByName('Region').AsString:= FRegion;
    FieldByName('State').AsString:= FState;
    FieldByName('City').AsString:= FCity;
    FieldByName('Address').AsString:= FAddress;
    FieldByName('Zip').AsString:= FZip;
    FieldByName('Contact').AsString:= FContact;
    FieldByName('Linker').AsString:= FLinker;
    FieldByName('Mobile').AsString:= FMobile;
    FieldByName('Longitude').AsExtended:= FLongitude;
    FieldByName('Latitude').AsExtended:= FLatitude;
    FieldByName('Status').AsString:= FStatus;
    FieldByName('PWD').AsString:= FPWD;
    Post;
    Result:= true;
  end;
end;

function TCoteInfo.UpdateToDB(DataSet : TCustomADODataSet): Boolean;
begin
  FConn:= DataSet.Connection;
  with DataSet do begin
    Edit;
    FieldByName('OrgName').AsString:= FOrgName;
    FieldByName('ShortName').AsString:= FShortName;
    FieldByName('Region').AsString:= FRegion;
    FieldByName('State').AsString:= FState;
    FieldByName('City').AsString:= FCity;
    FieldByName('Address').AsString:= FAddress;
    FieldByName('Zip').AsString:= FZip;
    FieldByName('Contact').AsString:= FContact;
    FieldByName('Linker').AsString:= FLinker;
    FieldByName('Mobile').AsString:= FMobile;
    FieldByName('Longitude').AsExtended:= FLongitude;
    FieldByName('Latitude').AsExtended:= FLatitude;
    FieldByName('Status').AsString:= FStatus;
    FieldByName('PWD').AsString:= FPWD;
    Post;
    Result:= true;
  end;
end;

procedure TCoteInfo.AppendData(AConn: TADOConnection);
begin
  FConn:= AConn;
  AConn.Execute('INSERT INTO ' + Prefix + 'Cote([ID],[OrgName],[ShortName],[Region],' +
    '[State],[City],[Address],[Zip],[Contact],[Linker],[Mobile],[Longitude],[Latitude],' +
    '[Status],[PWD])VALUES(''' + GUIDToString(FID) + ''',''' + FOrgName + ''',''' +
    FShortName + ''',''' + FRegion + ''',''' + FState + ''',''' + FCity + ''',''' +
    FAddress + ''',''' + FZip + ''',''' + FContact + ''',''' + FLinker + ''',''' +
    FMobile + ''',' + FloatToStr(FLongitude) +
    ',' + FloatToStr(FLatitude) + ',''' + FStatus + ''',''' + FPWD + ''')');
end;

procedure TCoteInfo.UpdateData(AConn: TADOConnection);
begin
  FConn:= AConn;
  AConn.Execute('UPDATE ' + Prefix + 'Cote SET [OrgName]=''' +
    FOrgName + ''',[ShortName]=''' + FShortName + ''',[Region]=''' + FRegion + ''',[State]=''' + FState +
    ''',[City]=''' + FCity + ''',[Address]=''' + FAddress + ''',[Zip]=''' +
    FZip + ''',[Contact]=''' + FContact + ''',[Linker]=''' + FLinker +
    ''',[Mobile]=''' + FMobile + ''',[Longitude]=' + FloatToStr(FLongitude) + ',[Latitude]=' +
    FloatToStr(FLatitude) + ',[Status]=''' + FStatus + ''',[PWD]=''' + FPWD +
    ''' WHERE ID=''' + GuidToString(FID) + '''');
end;

function TCoteInfo.LoadData(AConn: TADOConnection; AID: TGuid): Boolean;
var
  DQData: TADOQuery;
begin
  FConn:= AConn;
  DQData:= TADOQuery.Create(nil);
  try
    DQData.Connection:= AConn;
    DQData.SQL.Text:= 'SELECT [ID],[OrgName],[ShortName],[Region],[State],[City],[Address],' +
      '[Zip],[Contact],[Linker],[Mobile],[Longitude],[Latitude],[Status],[PWD] FROM ' +
      Prefix + 'Cote WHERE ID=''' + GuidToString(AID) + '''';
    DQData.Open;
    if DQData.EOF then Loaded:= False
    else Loaded:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
  Result:= Loaded;
end;

{ TMemberInfo }

function TMemberInfo.CheckInfo: boolean;
begin
  Result:= False;
  if FMemberName = '' then begin WriteInfo('姓名不能为空，数据无效。'); Exit; end;
  if Length(FMemberName) >= 20 then begin WriteInfo('姓名长度不允许超过20个文字。'); Exit; end;
  if FMemberNo = '' then begin WriteInfo('鸽主编号不能为空，数据无效。'); Exit; end;
  Result:= True;
end;

function TMemberInfo.LoadFromDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    FID:= FieldByName('ID').AsGuid;
    FMemberName:= FieldByName('MemberName').AsString;
    FMemberNo:= FieldByName('MemberNo').AsString;
    FAssociationNo:= FieldByName('AssociationNo').AsString;
    FOrgIDX:= FieldByName('OrgIDX').AsGuid;
    FShortName:= FieldByName('ShortName').AsString;
    FKind:= FieldByName('Kind').AsString;
    FStatus:= FieldByName('Status').AsString;
    FEMail:= FieldByName('EMail').AsString;
    FCash:= FieldByName('Cash').AsExtended;
    FCashDate:= FieldByName('CashDate').AsDatetime;
    FCashKind:= FieldByName('CashKind').AsString;
    FState:= FieldByName('State').AsString;
    FCity:= FieldByName('City').AsString;
    FCounty:= FieldByName('County').AsString;
    FAddress:= FieldByName('Address').AsString;
    FManager:= FieldByName('Manager').AsString;
    FMobile:= FieldByName('Mobile').AsString;
    FDisplayOrder:= FieldByName('DisplayOrder').AsInteger;
    FLon:= FieldByName('Lon').AsExtended;
    FLat:= FieldByName('Lat').AsExtended;
    FModifiedate:= FieldByName('Modifiedate').AsDatetime;
    Result:= true;
  end;
  FNote:= GetLargeStr('ID', GuidToString(FID), Prefix + 'Member', 'Note', FConn);
end;

function TMemberInfo.MaxOrder(AConn: TADOConnection): Integer;
begin
  Result:= AConn.Execute('SELECT ISNULL(Max(DisplayOrder),0)+1 FROM ' + Prefix + 'Member').Fields[0].Value;
end;

function TMemberInfo.AppendToDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    CreateGuid(FID);
    Append;
    FieldByName('ID').AsGuid:= FID;
    FieldByName('MemberName').AsString:= FMemberName;
    FieldByName('MemberNo').AsString:= FMemberNo;
    FieldByName('AssociationNo').AsString:= FAssociationNo;
    FieldByName('OrgIDX').AsGuid:= FCoteDB.FCoteInfo.ID;
    FieldByName('ShortName').AsString:= FShortName;
    FieldByName('Kind').AsString:= FKind;
    FieldByName('Status').AsString:= '启用';
    FieldByName('EMail').AsString:= FEMail;
    FieldByName('Cash').AsExtended:= FCash;
    FieldByName('CashDate').AsDatetime:= FCashDate;
    FieldByName('CashKind').AsString:= FCashKind;
    FieldByName('State').AsString:= FState;
    FieldByName('City').AsString:= FCity;
    FieldByName('County').AsString:= FCounty;
    FieldByName('Address').AsString:= FAddress;
    FieldByName('Manager').AsString:= FManager;
    FieldByName('Mobile').AsString:= FMobile;
    //FieldByName('DisplayOrder').AsInteger:= FDisplayOrder;  现在是自增长字段，不需要维护
    FieldByName('Lon').AsExtended:= FLon;
    FieldByName('Lat').AsExtended:= FLat;
    FieldByName('Modifiedate').AsDatetime:= FCoteDB.CurrTime;
    FieldByName('SynFlag').AsAnsiString:= 'A';
    Post;
    SetLargeStr('ID', GuidToString(FID), 'JOIN_Member', 'Note', FNote, FConn);
  end;
end;

function TMemberInfo.UpdateToDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    Edit;
    FieldByName('MemberName').AsString:= FMemberName;
    FieldByName('MemberNo').AsString:= FMemberNo;
    FieldByName('AssociationNo').AsString:= FAssociationNo;
    FieldByName('OrgIDX').AsGuid:= FCoteDB.FCoteInfo.ID;
    FieldByName('ShortName').AsString:= FShortName;
    FieldByName('Kind').AsString:= FKind;
    FieldByName('Status').AsString:= '启用';
    FieldByName('EMail').AsString:= FEMail;
    FieldByName('Cash').AsExtended:= FCash;
    FieldByName('CashDate').AsDatetime:= FCashDate;
    FieldByName('CashKind').AsString:= FCashKind;
    FieldByName('State').AsString:= FState;
    FieldByName('City').AsString:= FCity;
    FieldByName('County').AsString:= FCounty;
    FieldByName('Address').AsString:= FAddress;
    FieldByName('Manager').AsString:= FManager;
    FieldByName('Mobile').AsString:= FMobile;
    //FieldByName('DisplayOrder').AsInteger:= FDisplayOrder;      现在是自增长字段，不需要维护
    FieldByName('Lon').AsExtended:= FLon;
    FieldByName('Lat').AsExtended:= FLat;
    FieldByName('Modifiedate').AsDatetime:= FCoteDB.CurrTime;
    FieldByName('SynFlag').AsAnsiString:= 'U';
    Post;
    SetLargeStr('ID', GuidToString(FID), 'JOIN_Member', 'Note', FNote, FConn);
  end;
end;

function TMemberInfo.AppendData(AConn: TADOConnection): Boolean;
var
  cnt: Integer;
begin
  FConn:= AConn;
  AConn.Execute('INSERT INTO ' + Prefix + 'Member([ID],[MemberName],[MemberNo],' +
    '[AssociationNo],[OrgIDX],[ShortName],[Kind],[Status],[EMail],[Cash],[CashDate],' +
    '[CashKind],[State],[City],[County],[Address],[Manager],[Mobile],[DisplayOrder],' +
    '[Lon],[Lat],[Modifiedate],[SynFlag])VALUES(''' + GuidToString(FID) + ''',''' +
    FMemberName + ''',''' + FMemberNo + ''',''' + FAssociationNo + ''',''' +
    GuidToString(FCoteDB.FCoteInfo.ID) + ''',''' + FShortName + ''',''' + FKind +
    ''',''启用'',' + FEMail + ''',' + FloatToStr(FCash) + ',''' +
    DatetimeToStr(FCashDate) + ''',''' + FCashKind + ''',''' + FState + ''',''' +
    FCity + ''',''' + FCounty + ''',''' + FAddress + ''',''' + FManager +
    ''',''' + FMobile + ''',' + IntToStr(DisplayOrder) + ',' + FloatToStr(FLon) +
    ',' + FloatToStr(FLat) + ',GetDate(),''A'')', cnt);
  Result:= cnt > 0;
  SetLargeStr('ID', GuidToString(FID), 'JOIN_Member', 'Note', FNote, FConn);
end;

function TMemberInfo.UpdateData(AConn: TADOConnection): Boolean;
var
  cnt: Integer;
begin
  FConn:= AConn;
  AConn.Execute('UPDATE ' + Prefix + 'Member SET [MemberName]=''' + FMemberName +
    ''',[MemberNo]=''' + FMemberNo + ''',[AssociationNo]=''' + FAssociationNo +
    ''',[OrgIDX]=''' + GuidToString(FCoteDB.FCoteInfo.ID) + ''',[ShortName]=''' +
    FShortName + ''',[Kind]=''' + FKind + ''',[EMail]=''' + FEMail + ''',[Cash]=' +
    FloatToStr(FCash) + ',[CashDate]=''' + DatetimeToStr(FCashDate) +
    ''',[CashKind]=''' + FCashKind + ''',[State]=''' + FState + ''',[City]=''' +
    FCity + ''',[County]=''' + FCounty + ''',[Address]=''' + FAddress +
    ''',[Manager]=''' + FManager + ''',[Mobile]=''' + FMobile + ''',[DisplayOrder]=' +
    IntToStr(DisplayOrder) + ',[Lon]=' + FloatToStr(FLon) + ',[Lat]=' +
    FloatToStr(FLat) + ',[Modifiedate]=GetDate(),SynFlag=''U'' WHERE ID=''' + GuidToString(FID) + '''', cnt);
  Result:= cnt > 0;
  SetLargeStr('ID', GuidToString(FID), 'JOIN_Member', 'Note', FNote, FConn);
end;

function TMemberInfo.LoadData(AConn: TADOConnection; AID: TGuid): Boolean;
var
  DQData: TADOQuery;
begin
  DQData:= TADOQuery.Create(nil);
  try
    FConn:= AConn;
    DQData.Connection:= AConn;
    DQData.SQL.Text:= 'SELECT [ID],[MemberName],[MemberNo],[AssociationNo],' +
      '[OrgIDX],[ShortName],[Kind],[Status],[EMail],[Cash],[CashDate],[CashKind],' +
      '[State],[City],[County],[Address],[Manager],[Mobile],[DisplayOrder],[Lon],' +
      '[Lat],[Modifiedate] FROM ' + Prefix + 'Member WHERE ID=''' + GuidToString(AID) + '''';
    DQData.Open;
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

{ TPigeonInfo }

function TPigeonInfo.CheckInfo: boolean;
begin
  Result:= False;
  if FGBCode = '' then begin WriteInfo('统一环号不能为空，数据无效。'); Exit; end;
  if FJZCode <= 0 then begin WriteInfo('内部码不能为空，数据无效。'); Exit; end;
  Result:= True;
end;

function TPigeonInfo.GetAssist(index: integer): string;
begin
  Result:= FAssist[Index];
end;

function TPigeonInfo.GetGroup(index: integer): string;
begin
  Result:= FGroup[Index];
end;

function TPigeonInfo.LoadFromDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    FID:= FieldByName('ID').AsGuid;
    FPigeonNO:= FieldByName('PigeonNO').AsString;
    FOrgIdx:= FieldByName('OrgIdx').AsGuid;
    FMemberIdx:= FieldByName('MemberIdx').AsGuid;
    FHouseNo:= FieldByName('HouseNo').AsString;
    FGBCode:= FieldByName('GBCode').AsString;
    FFlag:= FieldByName('Flag').AsString;
    FFactoryCode:= FieldByName('FactoryCode').AsLargeInt;
    FJZCode:= FieldByName('JZCode').AsLargeInt;
    FGroup[0]:= FieldByName('Group1').AsString;
    FGroup[1]:= FieldByName('Group2').AsString;
    FGroup[2]:= FieldByName('Group3').AsString;
    FGroup[3]:= FieldByName('Group4').AsString;
    FGroup[4]:= FieldByName('Group5').AsString;
    FGender:= FieldByName('Gender').AsString;
    FFeather:= FieldByName('Feather').AsString;
    FEye:= FieldByName('Eye').AsString;
    FDisplayOrder:= FieldByName('DisplayOrder').AsInteger;
    FAssist[0]:= FieldByName('Assist1').AsString;
    FAssist[1]:= FieldByName('Assist2').AsString;
    FAssist[2]:= FieldByName('Assist3').AsString;
    FAssist[3]:= FieldByName('Assist4').AsString;
    FAssist[4]:= FieldByName('Assist5').AsString;
    FStatus:= FieldByName('Status').AsString;
    FJoinDate:= FieldByName('JoinDate').AsDatetime;
    FModifiedate:= FieldByName('Modifiedate').AsDatetime;
    Result:= true;
  end;
end;

function TPigeonInfo.MaxOrder(AConn: TADOConnection): Integer;
begin
  Result:= AConn.Execute('SELECT ISNULL(Max(DisplayOrder),0)+1 FROM ' + Prefix + 'Pigeon').Fields[0].Value;
end;

procedure TPigeonInfo.SetAssist(index: integer; const Value: string);
begin
  FAssist[Index]:= Value;
end;

procedure TPigeonInfo.SetGroup(index: integer; const Value: string);
begin
  FGroup[Index]:= Value;
end;

function TPigeonInfo.AppendToDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    CreateGuid(FID);
    Append;
    FieldByName('ID').AsGuid:= FID;
    FieldByName('PigeonNO').AsString:= FPigeonNO;
    FieldByName('OrgIdx').AsGuid:= FCoteDB.FCoteInfo.ID;
    FieldByName('MemberIdx').AsGuid:= FMemberIdx;
    FieldByName('HouseNo').AsString:= FHouseNo;
    FieldByName('GBCode').AsString:= FGBCode;
    FieldByName('Flag').AsString:= FFlag;
    FieldByName('FactoryCode').AsLargeInt:= FFactoryCode;
    FieldByName('JZCode').AsLargeInt:= FJZCode;
    FieldByName('Group1').AsString:= FGroup[0];
    FieldByName('Group2').AsString:= FGroup[1];
    FieldByName('Group3').AsString:= FGroup[2];
    FieldByName('Group4').AsString:= FGroup[3];
    FieldByName('Group5').AsString:= FGroup[4];
    FieldByName('Gender').AsString:= FGender;
    FieldByName('Feather').AsString:= FFeather;
    FieldByName('Eye').AsString:= FEye;
    FieldByName('Assist1').AsString:= FAssist[0];
    FieldByName('Assist2').AsString:= FAssist[1];
    FieldByName('Assist3').AsString:= FAssist[2];
    FieldByName('Assist4').AsString:= FAssist[3];
    FieldByName('Assist5').AsString:= FAssist[4];
    FieldByName('Status').AsString:= FStatus;
    FieldByName('JoinDate').AsDatetime:= FJoinDate;
    FieldByName('Modifiedate').AsDatetime:= FCoteDB.CurrTime;
    FieldByName('SynFlag').AsAnsiString:= 'A';
    Post;
  end;
end;

function TPigeonInfo.UpdateToDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    Edit;
    FieldByName('PigeonNO').AsString:= FPigeonNO;
    FieldByName('OrgIdx').AsGuid:= FCoteDB.FCoteInfo.ID;
    FieldByName('MemberIdx').AsGuid:= FMemberIdx;
    FieldByName('HouseNo').AsString:= FHouseNo;
    FieldByName('GBCode').AsString:= FGBCode;
    FieldByName('Flag').AsString:= FFlag;
    FieldByName('FactoryCode').AsLargeInt:= FFactoryCode;
    FieldByName('JZCode').AsLargeInt:= FJZCode;
    FieldByName('Group1').AsString:= FGroup[0];
    FieldByName('Group2').AsString:= FGroup[1];
    FieldByName('Group3').AsString:= FGroup[2];
    FieldByName('Group4').AsString:= FGroup[3];
    FieldByName('Group5').AsString:= FGroup[4];
    FieldByName('Gender').AsString:= FGender;
    FieldByName('Feather').AsString:= FFeather;
    FieldByName('Eye').AsString:= FEye;
    FieldByName('Assist1').AsString:= FAssist[0];
    FieldByName('Assist2').AsString:= FAssist[1];
    FieldByName('Assist3').AsString:= FAssist[2];
    FieldByName('Assist4').AsString:= FAssist[3];
    FieldByName('Assist5').AsString:= FAssist[4];
    FieldByName('Status').AsString:= FStatus;
    FieldByName('JoinDate').AsDatetime:= FJoinDate;
    FieldByName('Modifiedate').AsDatetime:= FCoteDB.CurrTime;
    FieldByName('SynFlag').AsAnsiString:= 'U';
    Post;
  end;
end;

function TPigeonInfo.AppendData(AConn: TADOConnection): Boolean;
var
  cnt: Integer;
begin
  FConn:= AConn;
  CreateGuid(FID);
  AConn.Execute('INSERT INTO ' + Prefix + 'Pigeon([ID],[PigeonNO],[OrgIdx],' +
    '[MemberIdx],[HouseNo],[GBCode],[Flag],[FactoryCode],[JZCode],[Group1],' +
    '[Group2],[Group3],[Group4],[Group5],[Gender],[Feather],[Eye],[Assist1],' +
    '[Assist2],[Assist3],[Assist4],[Assist5],[Status],[JoinDate],[Modifiedate],[SynFlag])' +
    'VALUES(''' + GuidToString(FID) + ''',''' + FPigeonNO + ''',''' +
    GuidToString(FCoteDB.FCoteInfo.ID) + ''',''' + GuidToString(FMemberIdx) + ''',''' +
    FHouseNo + ''',''' + FGBCode + ''',''' + FFlag + ''',' + IntToStr(FFactoryCode) +
    ',' + IntToStr(FJZCode) + ',''' + FGroup[0] + ''',''' + FGroup[1] + ''',''' +
    FGroup[2] + ''',''' + FGroup[3] + ''',''' + FGroup[4] + ''',''' + FGender + ''',''' +
    FFeather + ''',''' + FEye + ''',''' + FAssist[0] + ''',''' + FAssist[1] + ''',''' +
    FAssist[2] + ''',''' + FAssist[3] + ''',''' + FAssist[4] + ''',''' + FStatus + ''',''' +
    DatetimeToStr(FJoinDate) + ''',GetDate(),''A'')', cnt);
  Result:= cnt > 0;
end;

function TPigeonInfo.UpdateData(AConn: TADOConnection): Boolean;
var
  cnt: Integer;
begin
  FConn:= AConn;
  AConn.Execute('UPDATE ' + Prefix + 'Pigeon SET [PigeonNO]=''' + FPigeonNO +
    ''',[OrgIdx]=''' + GuidToString(FCoteDB.FCoteInfo.ID) + ''',[MemberIdx]=''' + GuidToString(FMemberIdx) +
    ''',[HouseNo]=''' + FHouseNo + ''',[GBCode]=''' + FGBCode + ''',[Flag]=''' +
    FFlag + ''',[FactoryCode]=' + IntToStr(FFactoryCode) + ',[JZCode]=' +
    IntToStr(FJZCode) + ',[Group1]=''' + FGroup[0] + ''',[Group2]=''' + FGroup[1] +
    ''',[Group3]=''' + FGroup[2] + ''',[Group4]=''' + FGroup[3] + ''',[Group5]=''' +
    FGroup[4] + ''',[Gender]=''' + FGender + ''',[Feather]=''' + FFeather +
    ''',[Eye]=''' + FEye + ''',[Assist1]=''' + FAssist[0] + ''',[Assist2]=''' +
    FAssist[1] + ''',[Assist3]=''' + FAssist[2] + ''',[Assist4]=''' + FAssist[3] +
    ''',[Assist5]=''' + FAssist[4] + ''',[Status]=''' + FStatus + ''',[JoinDate]=''' +
    DatetimeToStr(FJoinDate) + ''',[Modifiedate]=GetDate(),SynFlag=''U'' WHERE ID=''' + GuidToString(FID) + '''', cnt);
  Result:= cnt > 0;
end;

function TPigeonInfo.LoadData(AConn: TADOConnection; AID: TGuid): Boolean;
var
  DQData: TADOQuery;
begin
  DQData:= TADOQuery.Create(nil);
  try
    FConn:= AConn;
    DQData.Connection:= AConn;
    DQData.SQL.Text:= 'SELECT [ID],[PigeonNO],[OrgIdx],[MemberIdx],[HouseNo],' +
      '[GBCode],[Flag],[FactoryCode],[JZCode],[Group1],[Group2],[Group3],' +
      '[Group4],[Group5],[Gender],[Feather],[Eye],[Assist1],[Assist2],[Assist3],' +
      '[Assist4],[Assist5],[Status],[JoinDate],[Modifiedate],SynFlag FROM ' +
      Prefix + 'Pigeon WHERE ID=''' + GuidToString(AID) + '''';
    DQData.Open;
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

{ TMatchInfo }

function TMatchInfo.CheckInfo: boolean;
begin
  Result:= False;
  if Length(FMatchName) < 2 then begin WriteInfo('赛事名称录入不符合规范。'); Exit; end;
  if FGameLong < 1 then begin WriteInfo('赛事距离录入不符合规范。'); Exit; end;
  Result:= True;
end;

function TMatchInfo.LoadFromDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    FID:= FieldByName('ID').AsGuid;
    FMatchName:= FieldByName('MatchName').AsString;
    FCoteIdx:= FieldByName('CoteIdx').AsGuid;
    FGameAddress:= FieldByName('GameAddress').AsString;
    FManager:= FieldByName('Manager').AsString;
    FGameLong:= FieldByName('GameLong').AsExtended;
    FUploadSetting:= FieldByName('UploadSetting').AsInteger;
    FSMSCondition:= FieldByName('SMSCondition').AsString;
    FKind:= FieldByName('Kind').AsString;
    FStatus:= TJZMatchStatus(FieldByName('Status').AsInteger);
    FTelphone:= FieldByName('Telphone').AsString;
    FFlyDate:= FieldByName('FlyDate').AsDatetime;
    FLon:= FieldByName('Lon').AsExtended;
    FLat:= FieldByName('Lat').AsExtended;
    FCloseDate:= FieldByName('CloseDate').AsDatetime;
    FCipher:= FieldByName('Cipher').AsString;
    FCollectionDate:= FieldByName('CollectionDate').AsDatetime;
    FSunRaise:= FieldByName('SunRaise').AsDatetime;
    FWind:= FieldByName('Wind').AsString;
    FWindPower:= FieldByName('WindPower').AsString;
    FTemperature:= FieldByName('Temperature').AsExtended;
    FHumidity:= FieldByName('Humidity').AsString;
    FSunDown:= FieldByName('SunDown').AsDatetime;
    FCreateDate:= FieldByName('CreateDate').AsDatetime;
    FModifiedate:= FieldByName('Modifiedate').AsDatetime;
    Result:= true;
  end;
  FInfo:= GetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Info', FConn);
  FRule:= GetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Rule', FConn);
  FNote:= GetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Note', FConn);
  FSettings:= GetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Settings', FConn);
end;

function TMatchInfo.AppendToDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    CreateGuid(FID);
    Append;
    FieldByName('ID').AsGuid:= FID;
    FieldByName('MatchName').AsString:= FMatchName;
    FieldByName('CoteIdx').AsGuid:= FCoteDB.FCoteInfo.ID;
    FieldByName('GameAddress').AsString:= FGameAddress;
    FieldByName('Manager').AsString:= FManager;
    FieldByName('GameLong').AsExtended:= FGameLong;
    FieldByName('UploadSetting').AsInteger:= FUploadSetting;
    FieldByName('SMSCondition').AsString:= FSMSCondition;
    FieldByName('Kind').AsString:= FKind;
    FieldByName('Status').AsInteger:= Integer(FStatus);
    FieldByName('Telphone').AsString:= FTelphone;
    FieldByName('FlyDate').AsDatetime:= FFlyDate;
    FieldByName('Lon').AsExtended:= FLon;
    FieldByName('Lat').AsExtended:= FLat;
    FieldByName('CloseDate').AsDatetime:= FCloseDate;
    FieldByName('Cipher').AsString:= FCipher;
    FieldByName('CollectionDate').AsDatetime:= FCollectionDate;
    FieldByName('SunRaise').AsDatetime:= FSunRaise;
    FieldByName('Wind').AsString:= FWind;
    FieldByName('WindPower').AsString:= FWindPower;
    FieldByName('Temperature').AsExtended:= FTemperature;
    FieldByName('Humidity').AsString:= FHumidity;
    FieldByName('SunDown').AsDatetime:= FSunDown;
    FieldByName('CreateDate').AsDatetime:= Now;
    FieldByName('Modifiedate').AsDatetime:= Now;
    FieldByName('SynFlag').AsAnsiString:= GetFlag('A');
    Post;
    SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Info', FInfo, FConn);
    SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Rule', FRule, FConn);
    SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Note', FNote, FConn);
    SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Settings', FSettings, FConn);
  end;
end;

function TMatchInfo.BeginMatch(AConn: TADOConnection): Boolean;
var
  cnt: Integer;
begin
  FConn:= AConn;
  FStatus := JZS_BACK;
  AConn.Execute('UPDATE ' + Prefix + 'Match SET [Status]=' + IntToStr(Ord(FStatus)) +
    ',Modifiedate=Getdate(),SynFlag=''' + GetFlag('U') + ''' WHERE ID=''' + GuidToString(FID) + '''', cnt);
  Result:= cnt > 0;
  SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Info', FInfo, FConn);
  SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Rule', FRule, FConn);
  SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Note', FNote, FConn);
  SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Settings', FSettings, FConn);
end;

function TMatchInfo.UpdateToDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    Edit;
    FieldByName('MatchName').AsString:= FMatchName;
    FieldByName('CoteIdx').AsGuid:= FCoteDB.FCoteInfo.ID;
    FieldByName('GameAddress').AsString:= FGameAddress;
    FieldByName('Manager').AsString:= FManager;
    FieldByName('GameLong').AsExtended:= FGameLong;
    FieldByName('UploadSetting').AsInteger:= FUploadSetting;
    FieldByName('SMSCondition').AsString:= FSMSCondition;
    FieldByName('Kind').AsString:= FKind;
    FieldByName('Status').AsInteger:= Integer(FStatus);
    FieldByName('Telphone').AsString:= FTelphone;
    FieldByName('FlyDate').AsDatetime:= FFlyDate;
    FieldByName('Lon').AsExtended:= FLon;
    FieldByName('Lat').AsExtended:= FLat;
    FieldByName('CloseDate').AsDatetime:= FCloseDate;
    FieldByName('Cipher').AsString:= FCipher;
    FieldByName('CollectionDate').AsDatetime:= FCollectionDate;
    FieldByName('SunRaise').AsDatetime:= FSunRaise;
    FieldByName('Wind').AsString:= FWind;
    FieldByName('WindPower').AsString:= FWindPower;
    FieldByName('Temperature').AsExtended:= FTemperature;
    FieldByName('Humidity').AsString:= FHumidity;
    FieldByName('SunDown').AsDatetime:= FSunDown;
    FieldByName('Modifiedate').AsDatetime:= Now;
    FieldByName('SynFlag').AsAnsiString:= GetFlag('U');
    Post;
    SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Info', FInfo, FConn);
    SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Rule', FRule, FConn);
    SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Note', FNote, FConn);
    SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Settings', FSettings, FConn);
  end;
end;

function TMatchInfo.AppendData(AConn: TADOConnection): Boolean;
var
  cnt: Integer;
begin
  FConn:= AConn;
  CreateGuid(FID);
  AConn.Execute('INSERT INTO ' + Prefix + 'Match([ID],[MatchName],[CoteIdx],' +
    '[GameAddress],[Manager],[GameLong],[UploadSetting],[SMSCondition],[Kind],' +
    '[Status],[Telphone],[FlyDate],[Lon],[Lat],[CloseDate],[Cipher],' +
    '[Wind],[WindPower],[Temperature],[Humidity],[SunDown],[CreateDate],' +
    '[CollectionDate],[SunRaise],[Modifiedate],[SynFlag])VALUES(''' +
    GuidToString(FID) + ''',''' + FMatchName + ''',''' +
    GuidToString(FCoteDB.FCoteInfo.ID) + ''',''' + FGameAddress + ''',''' +
    FManager + ''',' + FloatToStr(FGameLong) + ',''' + IntToStr(FUploadSetting) +
    ',''' + FSMSCondition + ''',''' + FKind + ''',' +
    IntToStr(Ord(FStatus)) + ',''' + FTelphone + ''',''' + DatetimeToStr(FFlyDate) +
    ''',' + FloatToStr(FLon) + ',' + FloatToStr(FLat) + ',''' + DatetimeToStr(FCloseDate) +
    ''',''' + FCipher + ''',''' + DatetimeToStr(FCollectionDate) + ''',''' +
    DatetimeToStr(FSunRaise) + ''',''' + FWind + ''',''' + FWindPower + ''',' +
    FloatToStr(FTemperature) + ',''' + FHumidity + ''',''' + DatetimeToStr(FSunDown) +
    ''',GetDate(),GetDate(),''' + GetFlag('A') + ''')', cnt);
  Result:= cnt > 0;
  SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Info', FInfo, FConn);
  SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Rule', FRule, FConn);
  SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Note', FNote, FConn);
  SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Settings', FSettings, FConn);
end;

function TMatchInfo.UpdateData(AConn: TADOConnection): Boolean;
var
  cnt: Integer;
begin
  FConn:= AConn;

  AConn.Execute('UPDATE ' + Prefix + 'Match SET [MatchName]=''' + FMatchName +
    ''',[CoteIdx]=''' + GuidToString(FCoteDB.FCoteInfo.ID) + ''',[GameAddress]=''' + FGameAddress +
    ''',[Manager]=''' + FManager + ''',[GameLong]=' + FloatToStr(FGameLong) +
    ',[UploadSetting]=''' + IntToStr(FUploadSetting) + ''',[SMSCondition]=''' +
    FSMSCondition + ''',[Kind]=''' + FKind + ''',[Status]=' +
    IntToStr(Ord(FStatus)) + ',[Telphone]=''' + FTelphone + ''',[FlyDate]=''' +
    DatetimeToStr(FFlyDate) + ''',[Lon]=' + FloatToStr(FLon) + ',[Lat]=' +
    FloatToStr(FLat) + ',[CloseDate]=''' + DatetimeToStr(FCloseDate) +
    ''',[Cipher]=''' + FCipher + ''',[CollectionDate]=''' + DatetimeToStr(FCollectionDate) +
    ''',[SunRaise]=''' + DatetimeToStr(FSunRaise) + ''',[Wind]=''' + FWind +
    ''',[WindPower]=''' + FWindPower + ''',[Temperature]=' + FloatToStr(FTemperature) +
    ',[Humidity]=''' + FHumidity + ''',[SunDown]=''' + DatetimeToStr(FSunDown) +
    ''',[Modifiedate]=GetDate(),SynFlag=''' + GetFlag('U') + ''' WHERE ID=''' + GuidToString(FID) + '''', cnt);
  Result:= cnt > 0;
  SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Info', FInfo, FConn);
  SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Rule', FRule, FConn);
  SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Note', FNote, FConn);
  SetLargeStr('ID', GuidToString(FID), 'JOIN_Match', 'Settings', FSettings, FConn);
end;

function TMatchInfo.LoadData(AConn: TADOConnection; AID: TGuid): Boolean;
var
  DQData: TADOQuery;
begin
  DQData:= TADOQuery.Create(nil);
  try
    FConn:= AConn;
    DQData.Connection:= AConn;
    DQData.SQL.Text:= 'SELECT [ID],[MatchName],[CoteIdx],[GameAddress],' +
      '[Manager],[GameLong],[UploadSetting],[SMSCondition],[Kind],[Status],[Telphone],[FlyDate],' +
      '[Lon],[Lat],[CloseDate],[Cipher],[CollectionDate],[SunRaise],[Wind],' +
      '[WindPower],[Temperature],[Humidity],[SunDown],[CreateDate],' +
      '[Modifiedate] FROM ' + Prefix + 'Match WHERE ID=''' + GuidToString(AID) + '''';
    DQData.Open;
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

function TMatchInfo.GetFlag(Delault: Char): Char;
begin
  if FUploadSetting = 0 then Result:= 'O' else Result:= Delault;
end;

{ TGameSourceInfo }


function TGameSourceInfo.CheckInfo: boolean;
begin
  Result:= False;
  if FGameIdx = TGUID.Empty then begin WriteInfo('赛事索引不能为空，数据无效。'); Exit; end;
  if FPigeonIdx = TGUID.Empty then begin WriteInfo('赛鸽索引不能为空，数据无效。'); Exit; end;
  Result:= True;
end;

function TGameSourceInfo.LoadFromDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    FID:= FieldByName('ID').AsInteger;
    FGameIdx:= FieldByName('GameIdx').AsGuid;
    FPigeonIdx:= FieldByName('PigeonIdx').AsGuid;
    FGameCode:= FieldByName('GameCode').AsInteger;
    FCollecteNo:= FieldByName('CollecteNo').AsString;
    FCollecteDate:= FieldByName('CollecteDate').AsDatetime;
    FCollectID:= FieldByName('CollectID').AsInteger;
    FSetCode:= FieldByName('SetCode').AsString;
    FReportTime:= FieldByName('ReportTime').AsDatetime;
    FFlyTime:= FieldByName('FlyTime').AsExtended;
    FSpeed:= FieldByName('Speed').AsExtended;
    FIsBack:= FieldByName('IsBack').AsInteger;
    FKind:= FieldByName('Kind').AsInteger;
    FSourceOrder:= FieldByName('SourceOrder').AsInteger;
    FNote:= FieldByName('Note').AsString;
    FMatchLong:= FieldByName('MatchLong').AsExtended;
    FModifiedate:= FieldByName('Modifiedate').AsDatetime;
    Result:= true;
  end;
end;

function TGameSourceInfo.SaveToDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    if FID = 0 then begin Append; AutoCollectID; end else Edit;
    FieldByName('GameIdx').AsGuid:= FGameIdx;
    FieldByName('PigeonIdx').AsGuid:= FPigeonIdx;
    FieldByName('GameCode').AsInteger:= FGameCode;
    FieldByName('CollecteNo').AsString:= FCollecteNo;
    FieldByName('CollecteDate').AsDatetime:= FCollecteDate;
    FieldByName('CollectID').AsInteger:= FCollectID;
    FieldByName('SetCode').AsString:= FSetCode;
    FieldByName('ReportTime').AsDatetime:= FReportTime;
    FieldByName('FlyTime').AsExtended:= FFlyTime;
    FieldByName('Speed').AsExtended:= FSpeed;
    FieldByName('IsBack').AsInteger:= FIsBack;
    FieldByName('Kind').AsInteger:= FKind;
    FieldByName('SourceOrder').AsInteger:= FSourceOrder;
    FieldByName('Note').AsString:= FNote;
    FieldByName('SynFlag').AsAnsiString:= FSynFlag;
    FieldByName('SmsFlag').AsAnsiString:= FSmsFlag;
    FieldByName('MatchLong').AsExtended:= FMatchLong;
    FieldByName('Modifiedate').AsDatetime:= FCoteDB.CurrTime;
    Post;
    FID:= FieldByName('ID').AsInteger;
    Result:= FID > 0;
  end;
end;

procedure TGameSourceInfo.AutoCollectID;
var
  CNT: Integer;
begin
//  FCollectID:= FConn.Execute('SELECT ISNULL(MAX(CollectID),0) + 1 FROM ' +
//    Prefix + 'GameSource WHERE GameIdx=''' + GuidToString(FGameIdx) +
//    '''').Fields[0].Value;
  CNT:= FConn.Execute('SELECT COUNT(*) + 1 FROM ' +
    Prefix + 'GameSource WHERE GameIdx=''' + GuidToString(FGameIdx) +
    '''AND LEFT(SetCode,1)=''' + ClientID + '''').Fields[0].Value;
  FSetCode:= format('%s%0.5d', [FClientID, CNT]);
end;

function TGameSourceInfo.SaveData(AConn: TADOConnection): Boolean;
begin
  FConn:= AConn;
  if FID = 0 then begin
    AutoCollectID;
    FID:= AConn.Execute('INSERT INTO ' + Prefix + 'GameSource([GameIdx],' +
      '[PigeonIdx],[GameCode],[CollecteNo],[CollecteDate],[CollectID],' +
      '[SetCode],[ReportTime],[FlyTime],[Speed],[IsBack],[Kind],[SourceOrder],' +
      '[Note],[MatchLong],[Modifiedate],[SynFlag],[SmsFlag])' +
      'OUTPUT INSERTED.ID VALUES(''' + GuidToString(FGameIdx) + ''',''' +
      GuidToString(FPigeonIdx) + ''',' + IntToStr(FGameCode) + ',''' + FCollecteNo +
      ''',''' + DatetimeToStr(FCollecteDate) + ''',' + IntToStr(FCollectID) +
      ',''' + FSetCode + ''',''' + DatetimeToStr(FReportTime) + ''',' +
      FloatToStr(FFlyTime) + ',' + FloatToStr(FSpeed) + ',' + IntToStr(FIsBack) +
      ',' + IntToStr(FKind) + ',' + IntToStr(FSourceOrder) + ',''' + FNote + ''',' +
      FloatToStr(FMatchLong) + ',GetDate(),''' + FSynFlag + ',''' + FSmsFlag + ''')').Fields[0].Value
  end else AConn.Execute('UPDATE ' + Prefix + 'GameSource SET [GameIdx]=''' +
    GuidToString(FGameIdx) + ''',[PigeonIdx]=''' + GuidToString(FPigeonIdx) +
    ''',[GameCode]=' + IntToStr(FGameCode) + ',[CollecteNo]=''' + FCollecteNo +
    ''',[CollecteDate]=''' + DatetimeToStr(FCollecteDate) + ''',[CollectID]=' +
    IntToStr(FCollectID) + ',[SetCode]=''' + FSetCode + ''',[ReportTime]=''' +
    DatetimeToStr(FReportTime) + ''',[FlyTime]=' + FloatToStr(FFlyTime) +
    ',[Speed]=' + FloatToStr(FSpeed) + ',[IsBack]=' + IntToStr(FIsBack) +
    ',[Kind]=' + IntToStr(FKind) + ',[SourceOrder]=' + IntToStr(FSourceOrder) +
    ',[Note]=''' + FNote + ''',[MatchLong]=' + FloatToStr(FMatchLong) +
    ',[Modifiedate]=GetDate(),SynFlag=''' + FSynFlag + ',SmsFlag=''' + FSmsFlag + ''' WHERE ID=' + IntToStr(FID));
  Result:= FID > 0;
end;

function TGameSourceInfo.LoadData(AConn: TADOConnection; AID: Integer): Boolean;
var
  DQData: TADOQuery;
begin
  DQData:= TADOQuery.Create(nil);
  try
    FConn:= AConn;
    DQData.Connection:= AConn;
    DQData.SQL.Text:= 'SELECT [ID],[GameIdx],[PigeonIdx],[GameCode],' +
      '[CollecteNo],[CollecteDate],[CollectID],[SetCode],[ReportTime],' +
      '[FlyTime],[Speed],[IsBack],[Kind],[SourceOrder],[Note],[MatchLong],' +
      '[Modifiedate] FROM ' + Prefix + 'GameSource WHERE ID=' + IntToStr(AID);
    DQData.Open;
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

{ TChooseInfo }

function TChooseInfo.CheckInfo: boolean;
begin
  Result:= False;
  if FGameIdx = TGUID.Empty then begin WriteInfo('赛事索引不能为空，数据无效。'); Exit; end;
  if FPigeonIdx = TGUID.Empty then begin WriteInfo('赛鸽索引不能为空，数据无效。'); Exit; end;
  if FSubject = '' then begin WriteInfo('投注项目不能为空，数据无效。'); Exit; end;
  if FAmount <= 0 then begin WriteInfo('投注金额不能为空，数据无效。'); Exit; end;
  if FStatus = '' then begin WriteInfo('投注状态不能为空，数据无效。'); Exit; end;
  Result:= True;
end;

function TChooseInfo.LoadFromDB(DataSet : TCustomADODataSet): Boolean;
begin
  inherited;
  with DataSet do begin
    FConn:= Connection;
    FID:= FieldByName('ID').AsInteger;
    FGameIdx:= FieldByName('GameIdx').AsGuid;
    FPigeonIdx:= FieldByName('PigeonIdx').AsGuid;
    FGambler:= FieldByName('Gambler').AsGuid;
    FStatus:= FieldByName('Status').AsString;
    FSubject:= FieldByName('Subject').AsString;
    FAmount:= FieldByName('Amount').AsExtended;
    FPayment:= FieldByName('Payment').AsString;
    FTicketNo:= FieldByName('TicketNo').AsString;
    FGamblerNo:= FieldByName('GamblerNo').AsGuid;
    FChoicedTime:= FieldByName('ChoicedTime').AsDatetime;
    FModifiedate:= FieldByName('Modifiedate').AsDatetime;
    FSynFlag:= FieldByName('SynFlag').AsString;
    Result:= true;
  end;
end;

function TChooseInfo.SaveToDB(DataSet : TCustomADODataSet): Boolean;
begin
  inherited;
  with DataSet do begin
    FConn:= Connection;
    if FID = 0 then begin
      Append;
    end else begin
      Edit;
    end;
    FieldByName('GameIdx').AsGuid:= FGameIdx;
    FieldByName('PigeonIdx').AsGuid:= FPigeonIdx;
    FieldByName('Gambler').AsGuid:= FGambler;
    FieldByName('Status').AsString:= FStatus;
    FieldByName('Subject').AsString:= FSubject;
    FieldByName('Amount').AsExtended:= FAmount;
    FieldByName('Payment').AsString:= FPayment;
    FieldByName('TicketNo').AsString:= FTicketNo;
    FieldByName('GamblerNo').AsGuid:= FGamblerNo;
    FieldByName('ChoicedTime').AsDatetime:= FChoicedTime;
    FieldByName('Modifiedate').AsDatetime:= FModifiedate;
    FieldByName('SynFlag').AsString:= FSynFlag;
    Post;
    FID:= FieldByName('ID').AsInteger;
  end;
end;

function TChooseInfo.SaveData(AConn: TADOConnection): Boolean;
var
  cnt: Integer;
begin
  inherited;
  if FID = 0 then begin
    FConn:= AConn;
    FID:= AConn.Execute('INSERT INTO ' + Prefix + 'Choose([GameIdx],[PigeonIdx],' +
      '[Gambler],[Status],[Subject],[Amount],[Payment],[TicketNo],[GamblerNo],' +
      '[ChoicedTime],[Modifiedate],[SynFlag])OUTPUT INSERTED.ID VALUES(''' +
      GuidToString(FGameIdx) + ''',''' + GuidToString(FPigeonIdx) + ''',''' +
      GuidToString(FGambler) + ''',''' + FStatus + ''',''' + FSubject + ''',' +
      FloatToStr(FAmount) + ',''' + FPayment + ''',''' + FTicketNo + ''',''' +
      GuidToString(FGamblerNo) + ''',''' + DatetimeToStr(FChoicedTime) + ''',''' +
      DatetimeToStr(FModifiedate) + ''',''' + FSynFlag + ''')').Fields[0].Value;
    Result:= FID > 0;
  end else begin
    AConn.Execute('UPDATE ' + Prefix + 'Choose SET [GameIdx]=''' + GuidToString(FGameIdx) +
      ''',[PigeonIdx]=''' + GuidToString(FPigeonIdx) + ''',[Gambler]=''' +
      GuidToString(FGambler) + ''',[Status]=''' + FStatus + ''',[Subject]=''' +
      FSubject + ''',[Amount]=' + FloatToStr(FAmount) + ',[Payment]=''' +
      FPayment + ''',[TicketNo]=''' + FTicketNo + ''',[GamblerNo]=''' +
      GuidToString(FGamblerNo) + ''',[ChoicedTime]=''' + DatetimeToStr(FChoicedTime) +
      ''',[Modifiedate]=''' + DatetimeToStr(FModifiedate) + ''',[SynFlag]=''' +
      FSynFlag + ''' WHERE ID=' + IntToStr(FID), cnt);
    Result:= cnt > 0;
  end;
end;

function TChooseInfo.LoadData(AConn: TADOConnection; AID: Integer): Boolean;
var
  DQData: TADOQuery;
begin
  inherited;
  DQData:= TADOQuery.Create(nil);
  try
    DQData.Connection:= AConn;
    DQData.SQL.Text:= 'SELECT [ID],[GameIdx],[PigeonIdx],[Gambler],[Status],' +
      '[Subject],[Amount],[Payment],[TicketNo],[GamblerNo],[ChoicedTime],' +
      '[Modifiedate],[SynFlag] FROM ' + Prefix + 'Choose WHERE ID=' + IntToStr(AID);
    DQData.Open;
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

{ TPackageInfo }

function TPackageInfo.CheckInfo: boolean;
begin
  Result:= False;
  Result:= True;
end;

function TPackageInfo.LoadFromDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    FID:= FieldByName('ID').AsInteger;
    FRingKind:= FieldByName('RingKind').AsString;
    FRingCount:= FieldByName('RingCount').AsInteger;
    FRingColor:= FieldByName('RingColor').AsString;
    FCreateDate:= FieldByName('CreateDate').AsDatetime;
    FOrgIDX:= FieldByName('OrgIDX').AsInteger;
    FModifiedate:= FieldByName('Modifiedate').AsDatetime;
    Result:= true;
  end;
end;

function TPackageInfo.SaveToDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    if FID = 0 then begin
      Append;
      FieldByName('CreateDate').AsDatetime:= Now;
    end else begin
      Edit;
    end;
    FieldByName('RingKind').AsString:= FRingKind;
    FieldByName('RingCount').AsInteger:= FRingCount;
    FieldByName('RingColor').AsString:= FRingColor;
    FieldByName('CreateDate').AsDatetime:= Now;
    FieldByName('OrgIDX').AsInteger:= FOrgIDX;
    FieldByName('Modifiedate').AsDatetime:= Now;
    Post;
    FID:= FieldByName('ID').AsInteger;
  end;
end;

function TPackageInfo.SaveData(AConn: TADOConnection): Boolean;
begin
  FConn:= AConn;
  if FID = 0 then
    FID:= AConn.Execute('INSERT INTO ' + Prefix + 'Package([RingKind],[RingCount],' +
    '[RingColor],[CreateDate],[OrgIDX],[Modifiedate])OUTPUT INSERTED.ID VALUES(''' +
    FRingKind + ''',' + IntToStr(FRingCount) + ',''' + FRingColor + ''',''' +
    DatetimeToStr(FCreateDate) + ''',' + IntToStr(FOrgIDX) + ',GetDate())').Fields[0].Value
  else AConn.Execute('UPDATE ' + Prefix + 'Package SET [RingKind]=''' + FRingKind +
    ''',[RingCount]=' + IntToStr(FRingCount) + ',[RingColor]=''' + FRingColor +
    ''',[CreateDate]=''' + DatetimeToStr(FCreateDate) + ''',[OrgIDX]=' +
    IntToStr(FOrgIDX) + ',[Modifiedate]=GetDate() WHERE ID=' + IntToStr(FID));
  Result:= FID > 0;
end;

function TPackageInfo.LoadData(AConn: TADOConnection; AID: Integer): Boolean;
var
  DQData: TADOQuery;
begin
  DQData:= TADOQuery.Create(nil);
  try
    FConn:= AConn;
    DQData.Connection:= AConn;
    DQData.SQL.Text:= 'SELECT [ID],[RingKind],[RingCount],[RingColor],[CreateDate],' +
      '[OrgIDX],[Modifiedate] FROM ' + Prefix + 'Package WHERE ID=' + IntToStr(AID);
    DQData.Open;
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

{ TRingInfo }

function TRingInfo.CheckInfo: boolean;
begin
  Result:= False;
  if FChipCode <= 0 then begin WriteInfo('芯片号不能为空，数据无效。'); Exit; end;
  Result:= True;
end;

function TRingInfo.LoadFromDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    FID:= FieldByName('ID').AsInteger;
    FRingKind:= FieldByName('RingKind').AsString;
    FFactory:= FieldByName('Factory').AsString;
    FPackageIdx:= FieldByName('PackageIdx').AsInteger;
    FChipCode:= FieldByName('ChipCode').AsInteger;
    FFactoryCode:= FieldByName('FactoryCode').AsInteger;
    FInputDate:= FieldByName('InputDate').AsDatetime;
    Result:= true;
  end;
end;

function TRingInfo.SaveToDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    if FID = 0 then begin
      Append;
    end else begin
      Edit;
    end;
    FieldByName('PackageIdx').AsInteger:= FPackageIdx;
    FieldByName('RingKind').AsString:= FRingKind;
    FieldByName('Factory').AsString:= FFactory;
    FieldByName('ChipCode').AsLargeInt:= FChipCode;
    FieldByName('FactoryCode').AsLargeInt:= FFactoryCode;
    FieldByName('InputDate').AsDatetime:= FInputDate;
    Post;
    FID:= FieldByName('ID').AsInteger;
  end;
  Result:= FID > 0;
end;

function TRingInfo.SaveData(AConn: TADOConnection): Boolean;
begin
  FConn:= AConn;
  if FID = 0 then
    FID:= AConn.Execute('INSERT INTO ' + Prefix + 'Ring([PackageIdx],' +
      '[RingKind],[Factory],[ChipCode],[FactoryCode],[InputDate])OUTPUT INSERTED.ID VALUES(' +
      IntToStr(FPackageIdx) + ',''' + FRingKind +  ',''' + FFactory +''',' + IntToStr(FChipCode) + ',' +
      IntToStr(FFactoryCode) + ',''' + DatetimeToStr(FInputDate) + ''')').Fields[0].Value
  else AConn.Execute('UPDATE ' + Prefix + 'Ring SET [PackageIdx]=' +
    IntToStr(FPackageIdx) + ',[RingKind]=''' + FRingKind + ',[Factory]=''' + FFactory + ''',[ChipCode]=' + IntToStr(FChipCode) +
    ',[FactoryCode]=' + IntToStr(FFactoryCode) + ',[InputDate]=''' +
    DatetimeToStr(FInputDate) + ''' WHERE ID=' + IntToStr(FID));
  Result:= FID > 0;
end;

function TRingInfo.LoadData(AConn: TADOConnection; AID: Integer): Boolean;
var
  DQData: TADOQuery;
begin
  DQData:= TADOQuery.Create(nil);
  try
    FConn:= AConn;
    DQData.Connection:= AConn;
    DQData.SQL.Text:= 'SELECT [ID],[PackageIdx],[RingKind],[Factory],[ChipCode],[FactoryCode],' +
      '[InputDate] FROM ' + Prefix + 'Ring WHERE ID=' + IntToStr(AID);
    DQData.Open;
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

{ TGBCode }

function TGBCode.GetValue: string;
begin
  if FNation <> 'CHN' then
    Result := Trim(FCode)
  else
    Result := Format(ReadAppConfig('SYSTEM', 'GBCodeFmt'),
      [FNation, IntToStr(Year), FAssociation, FCode]);
end;

procedure TGBCode.SetAssociation(const Value: string);
begin
  FAssociation := Value;
end;

procedure TGBCode.SetCode(const Value: string);
begin
  FCode := Value;
end;

procedure TGBCode.SetNation(const Value: string);
begin
  FNation := Value;
end;

procedure TGBCode.SetValue(const Value: string);
const
  NATIONFLG: array [0 .. 66] of string = ('CHN', 'CRPA', 'THPA', 'ROC',
    'LUXEMBURG', 'LUXEM', 'LUX', 'SDU', 'SURP', 'SHU', 'BURP', 'NEHU', 'NNG',
    'NPA', 'NTU', 'NUHP', 'GB', 'SDU', 'H', 'HLRU', 'HOLLAND', 'N', 'NL',
    'SUERGE', 'SV', 'ARPU', 'AU', 'IF', 'OKY', 'NUU', 'NUPP', 'NRP', 'NURP',
    'NUHU', 'NIPPON', 'JAPAN', 'JUGOSLAWIEN', 'YU', 'ITALIA', 'LEARPU', '+',
    'SUI', 'BELGE', 'KBDB', 'PL', 'P', 'KOP', 'IU', 'LUVF', 'UI', 'IUVF', 'DV',
    'DAN', 'ESP', 'FCC', 'HPF', 'CHU', 'HKPA', 'NORGE', 'FRANCE', 'MALTR',
    'SUR', 'RSR', 'INDO', 'PORT', 'WHU', 'AIXIN');
var
  P: PChar;
  szLst: TStringList;
  szS, szValue: string;
  isNum: boolean;

  function IsNation(S: string): boolean;
  var
    i: integer;
  begin
    Result := True;
    for i := 0 to 64 do
      if SameText(S, NATIONFLG[i]) then
      begin
        FNation := NATIONFLG[i];
        Exit;
      end;
    Result := False;
    FNation := 'CHN';
  end;

  function IsYear(S: string): boolean;
  var
    szInt: integer;
  begin
    Result := TryStrToInt(S, szInt);
    if Result then
      Year := szInt
    else
      FYear := CurrentYear;
  end;

begin
  // [CHN 2012 01 123456][CHN201201123456][2012 01 123456][201201123456][1201123456][2012011234567]
  szValue := UpperCase(Trim(Value));
  if szValue = '' then
    Exit;
  szLst := TStringList.Create;
{$REGION 'Put value to list'}
  P := PChar(szValue);
  szS := '';
  while P[0] <> #0 do
  begin
    if ((P[0] <= 'Z') and (P[0] >= 'A')) then
    begin
      if isNum then
      begin
        szLst.Add(szS);
        szS := P[0];
      end
      else
        szS := szS + P[0];
    end
    else if ((P[0] <= 'z') and (P[0] >= 'z')) then
    begin
      if isNum then
      begin
        szLst.Add(szS);
        szS := P[0];
      end
      else
        szS := szS + P[0];
    end
    else if ((P[0] <= '9') and (P[0] >= '0')) then
    begin
      if (not isNum) and (szS <> '') then
      begin
        szLst.Add(szS);
        szS := P[0];
      end
      else
        szS := szS + P[0];
      isNum := True;
    end
    else if (P[0] <> '.') then
    begin
      szLst.Add(szS);
      szS := '';
    end;
    Inc(P);
  end;
  if szS <> '' then
    szLst.Add(szS);
  if IsNation(szLst[0]) and (FNation <> 'CHN') then
  begin // Nation
    FCode := Value;
    Exit;
  end;
{$ENDREGION}
  if szLst.Count = 4 then
  begin
{$REGION '<Nation><Year><Association><Code> or <Nation><Association><Year><Code>'}
    if not IsNation(szLst[0]) then // Nation
      raise Exception.Create(Format(RES_Invalid_GBCode, [Value]));
    if IsYear(szLst[1]) then
      FAssociation := szLst[2] // Year+Association
    else if IsYear(szLst[2]) then
      FAssociation := szLst[1] // Association+Year
    else
      raise Exception.Create(Format(RES_Invalid_GBCode, [Value]));
    FCode := szLst[3];
{$ENDREGION}
  end
  else if szLst.Count = 3 then
  begin
{$REGION '<Nation><Year+Association><Code> or <Nation><Association+Year><Code> or <Year><Association><Code>'}
    if IsNation(szLst[0]) then
    begin // Nation
      case Length(szLst[1]) of
        4:
          if IsYear(Copy(szLst[1], 1, 2)) then
            FAssociation := Copy(szLst[1], 3, 2)
          else
            raise Exception.Create(Format(RES_Invalid_GBCode, [Value]));
        6:
          if IsYear(Copy(szLst[1], 1, 4)) then
            FAssociation := Copy(szLst[1], 5, 2)
          else
            raise Exception.Create(Format(RES_Invalid_GBCode, [Value]));
      end;
    end
    else if IsYear(szLst[0]) then
      FAssociation := szLst[1] // Year+Association
    else if IsYear(szLst[1]) then
      FAssociation := szLst[0] // Association+Year
    else
      raise Exception.Create(Format(RES_Invalid_GBCode, [Value]));
    FCode := szLst[2];
{$ENDREGION}
  end
  else if szLst.Count = 2 then
  begin
{$REGION '<Nation><Year+Association+Code> or <Nation><Association+Code>'}
    if IsNation(szLst[0]) then
    begin // Nation
      case Length(szLst[1]) of
        8, 9:
          begin // Association+Code
            FYear := CurrentYear;
            FAssociation := Copy(szLst[1], 1, 2);
            FCode := Copy(szLst[1], 3, Length(szLst[1]) - 2);
          end;
        10, 11:
          if IsYear(Copy(szLst[1], 1, 2)) then
          begin // YY+Association+Code
            FAssociation := Copy(szLst[1], 3, 2);
            FCode := Copy(szLst[1], 5, Length(szLst[1]) - 4);
          end
          else
            raise Exception.Create(Format(RES_Invalid_GBCode, [Value]));
        12, 13:
          if IsYear(Copy(szLst[1], 1, 4)) then
          begin // YYYY+Association+Code
            FAssociation := Copy(szLst[1], 5, 2);
            FCode := Copy(szLst[1], 7, Length(szLst[1]) - 6);
          end
          else
            raise Exception.Create(Format(RES_Invalid_GBCode, [Value]));
      else
        raise Exception.Create(Format(RES_Invalid_GBCode, [Value]));
      end;
    end
    else
      Exception.Create(Format(RES_Invalid_GBCode, [Value]));
{$ENDREGION}
  end
  else
  begin
{$REGION '<Year+Association+Code> or <Association+Code>'}
    FNation := 'CHN';
    case Length(szLst[0]) of
      8, 9:
        begin // Association+Code
          FYear := CurrentYear;
          FAssociation := Copy(szLst[0], 1, 2);
          FCode := Copy(szLst[0], 3, Length(szLst[0]) - 2);
        end;
      10, 11:
        if IsYear(Copy(szLst[0], 1, 2)) then
        begin // YY+Association+Code
          FAssociation := Copy(szLst[0], 3, 2);
          FCode := Copy(szLst[0], 5, Length(szLst[0]) - 4);
        end
        else
          raise Exception.Create(Format(RES_Invalid_GBCode, [Value]));
      12, 13:
        if IsYear(Copy(szLst[0], 1, 4)) then
        begin // YYYY+Association+Code
          FAssociation := Copy(szLst[0], 5, 2);
          FCode := Copy(szLst[0], 7, Length(szLst[0]) - 6);
        end
        else
          raise Exception.Create(Format(RES_Invalid_GBCode, [Value]));
    else
      raise Exception.Create(Format(RES_Invalid_GBCode, [Value]));
    end;
{$ENDREGION}
  end;
  szLst.Free;
end;

procedure TGBCode.SetYear(const Value: integer);
var
  CenturyBase: integer;
begin
  FYear := Value;
  if FYear <= 100 then
  begin
    CenturyBase := CurrentYear - FormatSettings.TwoDigitYearCenturyWindow;
    Inc(FYear, CenturyBase div 100 * 100);
    if (FormatSettings.TwoDigitYearCenturyWindow > 0) and (FYear < CenturyBase) then
      Inc(FYear, 100);
  end;
end;

{ TSourceView }

function TSourceView.CheckIn(AConn: TADOConnection; Sms, Flag: Char): Boolean;
begin
  try
    AConn.Execute('UPDATE [' + Prefix + 'GameSource] SET FlyTime=' +
      FloatToStr(FlyTime) + ',ReportTime=''' + DateTimeToStr(ReportTime) +
      ''',Speed=' + FloatToStr(Speed) + ',IsBack=1,SourceOrder=' + IntToStr(FSourceOrder) +
      ',[Modifiedate]=GetDate(),[SmsFlag]=''' + Sms + ''',[SynFlag]=''' + Flag + ''' WHERE ID=' + IntToStr(FID));
    Result:= True;
  except
    on E: Exception do begin
      DlgInfo('错误', E.Message, 10);
      Result:= False;
    end;
  end;
end;

function TSourceView.Clone: TSourceView;
begin
  Result:= TSourceView.Create;
  Result.ID:= ID;
  Result.PigeonNO:= PigeonNO;
  Result.MemberIdx:= MemberIdx;
  Result.GBCode:= GBCode;
  Result.Flag:= Flag;
  Result.FactoryCode:= FactoryCode;
  Result.JZCode:= JZCode;
  Result.Group1:= Group1;
  Result.Group2:= Group2;
  Result.Group3:= Group3;
  Result.Group4:= Group4;
  Result.Group5:= Group5;
  Result.Gender:= Gender;
  Result.Feather:= Feather;
  Result.Eye:= Eye;
  Result.Assist1:= Assist1;
  Result.Assist2:= Assist2;
  Result.Assist3:= Assist3;
  Result.Assist4:= Assist4;
  Result.Assist5:= Assist5;
  Result.Status:= Status;
  Result.MemberNo:= MemberNo;
  Result.MemberName:= MemberName;
  Result.AssociationNo:= AssociationNo;
  Result.ShortName:= ShortName;
  Result.State:= State;
  Result.City:= City;
  Result.County:= County;
  Result.EMail:= EMail;
  Result.Mobile:= Mobile;
  Result.GameIdx:= GameIdx;
  Result.PigeonIdx:= PigeonIdx;
  Result.GameCode:= GameCode;
  Result.CollecteNo:= CollecteNo;
  Result.CollecteDate:= CollecteDate;
  Result.CollectID:= CollectID;
  Result.SetCode:= SetCode;
  Result.ReportTime:= ReportTime;
  Result.FlyTime:= FlyTime;
  Result.Speed:= Speed;
  Result.IsBack:= IsBack;
  Result.Kind:= Kind;
  Result.SourceOrder:= SourceOrder;
  Result.Note:= Note;
  Result.MatchName:= MatchName;
  Result.MatchLong:= MatchLong;
end;

function TSourceView.GetValues(FieldName: string): string;
begin
    if SameText(FieldName,'ID') then Result := IntToStr(FID)
    else if SameText(FieldName,'PigeonNO') then Result := FPigeonNO
    else if SameText(FieldName,'MemberIdx') then Result := GuidToString(FMemberIdx)
    else if SameText(FieldName,'GBCode') then Result := FGBCode
    else if SameText(FieldName,'Flag') then Result := FFlag
    else if SameText(FieldName,'FactoryCode') then Result := IntToStr(FFactoryCode)
    else if SameText(FieldName,'JZCode') then Result := IntToStr(FJZCode)
    else if SameText(FieldName,'Group1') then Result := FGroup1
    else if SameText(FieldName,'Group2') then Result := FGroup2
    else if SameText(FieldName,'Group3') then Result := FGroup3
    else if SameText(FieldName,'Group4') then Result := FGroup4
    else if SameText(FieldName,'Group5') then Result := FGroup5
    else if SameText(FieldName,'Gender') then Result := FGender
    else if SameText(FieldName,'Feather') then Result := FFeather
    else if SameText(FieldName,'Eye') then Result := FEye
    else if SameText(FieldName,'Assist1') then Result := FAssist1
    else if SameText(FieldName,'Assist2') then Result := FAssist2
    else if SameText(FieldName,'Assist3') then Result := FAssist3
    else if SameText(FieldName,'Assist4') then Result := FAssist4
    else if SameText(FieldName,'Assist5') then Result := FAssist5
    else if SameText(FieldName,'Status') then Result := FStatus
    else if SameText(FieldName,'MemberNo') then Result := FMemberNo
    else if SameText(FieldName,'MemberName') then Result := FMemberName
    else if SameText(FieldName,'AssociationNo') then Result := FAssociationNo
    else if SameText(FieldName,'ShortName') then Result := FShortName
    else if SameText(FieldName,'State') then Result := FState
    else if SameText(FieldName,'City') then Result := FCity
    else if SameText(FieldName,'County') then Result := FCounty
    else if SameText(FieldName,'Email') then Result := FEmail
    else if SameText(FieldName,'Mobile') then Result := FMobile
    else if SameText(FieldName,'GameIdx') then Result := GuidToString(FGameIdx)
    else if SameText(FieldName,'PigeonIdx') then Result := GuidToString(FPigeonIdx)
    else if SameText(FieldName,'GameCode') then Result := IntToStr(FGameCode)
    else if SameText(FieldName,'CollecteNo') then Result := FCollecteNo
    else if SameText(FieldName,'CollecteDate') then Result := DateTimeToStr(FCollecteDate)
    else if SameText(FieldName,'CollectID') then Result := IntToStr(FCollectID)
    else if SameText(FieldName,'SetCode') then Result := FSetCode
    else if SameText(FieldName,'ReportTime') then Result := DateTimeToStr(FReportTime)
    else if SameText(FieldName,'FlyTime') then Result := FloatToStr(FFlyTime)
    else if SameText(FieldName,'Speed') then Result := FloatToStr(FSpeed)
    else if SameText(FieldName,'IsBack') then Result := IntToStr(FIsBack)
    else if SameText(FieldName,'Kind') then Result := IntToStr(FKind)
    else if SameText(FieldName,'SourceOrder') then Result := IntToStr(SourceOrder)
    else if SameText(FieldName,'Note') then Result := FNote
    else if SameText(FieldName,'MatchName') then Result := FMatchName
    else if SameText(FieldName,'MatchLong') then Result := FloatToStr(FMatchLong);
end;

function TSourceView.LoadData(AConn: TADOConnection; AID: Integer): Boolean;
var
  DQData: TADOQuery;
begin
  DQData:= TADOQuery.Create(nil);
  try
    DQData.Connection:= AConn;
    OpenQuery(DQData, 'S.ID=' + IntToStr(AID));
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

function TSourceView.LoadFromDB(DataSet: TCustomADODataSet): Boolean;
begin
  if DataSet.EOF then Result:= False
  else with DataSet do begin
    FID:= FieldByName('ID').AsInteger;
    FPigeonNO:= FieldByName('PigeonNO').AsString;
    FMemberIdx:= StrToGuid(FieldByName('MemberIdx').AsString);
    FGBCode:= FieldByName('GBCode').AsString;
    FFlag:= FieldByName('Flag').AsString;
    FFactoryCode:= FieldByName('FactoryCode').AsInteger;
    FJZCode:= FieldByName('JZCode').AsInteger;
    FGroup1:= FieldByName('Group1').AsString;
    FGroup2:= FieldByName('Group2').AsString;
    FGroup3:= FieldByName('Group3').AsString;
    FGroup4:= FieldByName('Group4').AsString;
    FGroup5:= FieldByName('Group5').AsString;
    FGender:= FieldByName('Gender').AsString;
    FFeather:= FieldByName('Feather').AsString;
    FEye:= FieldByName('Eye').AsString;
    FAssist1:= FieldByName('Assist1').AsString;
    FAssist2:= FieldByName('Assist2').AsString;
    FAssist3:= FieldByName('Assist3').AsString;
    FAssist4:= FieldByName('Assist4').AsString;
    FAssist5:= FieldByName('Assist5').AsString;
    FStatus:= FieldByName('Status').AsString;
    FMemberNo:= FieldByName('MemberNo').AsString;
    FMemberName:= FieldByName('MemberName').AsString;
    FAssociationNo:= FieldByName('AssociationNo').AsString;
    FShortName:= FieldByName('ShortName').AsString;
    FState:= FieldByName('State').AsString;
    FCity:= FieldByName('City').AsString;
    FCounty:= FieldByName('County').AsString;
    FEMail:= FieldByName('EMail').AsString;
    FMobile:= FieldByName('Mobile').AsString;
    FGameIdx:= StrToGuid(FieldByName('GameIdx').AsString);
    FPigeonIdx:= StrToGuid(FieldByName('PigeonIdx').AsString);
    FGameCode:= FieldByName('GameCode').AsInteger;
    FCollecteNo:= FieldByName('CollecteNo').AsString;
    FCollecteDate:= FieldByName('CollecteDate').AsDatetime;
    FCollectID:= FieldByName('CollectID').AsInteger;
    FSetCode:= FieldByName('SetCode').AsString;
    FReportTime:= FieldByName('ReportTime').AsDatetime;
    FFlyTime:= FieldByName('FlyTime').AsExtended;
    FSpeed:= FieldByName('Speed').AsExtended;
    FIsBack:= FieldByName('IsBack').AsInteger;
    FKind:= FieldByName('Kind').AsInteger;
    FSourceOrder:= FieldByName('SourceOrder').AsInteger;
    FNote:= FieldByName('Note').AsString;
    FMatchName:= FieldByName('MatchName').AsString;
    FMatchLong:= FieldByName('MatchLong').AsExtended;
    Result:= true;
  end;
end;

function TSourceView.LoadJZCode(AConn: TADOConnection; jzCode: Int64): Boolean;
var
  DQData: TADOQuery;
begin
  DQData:= TADOQuery.Create(nil);
  try
    DQData.Connection:= AConn;
    OpenQuery(DQData, 'JZCode=' + IntToStr(jzCode));
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

class function TSourceView.OpenQuery(Query: TADOQuery; Condition: string): TADOQuery;
begin
  if not Assigned(Query) then begin
    Result:= TADOQuery.Create(nil);
    Result.Connection:= FCoteDB.ADOConn;
  end else Result:= Query;
  Result.Close;
  Result.SQL.Text:= 'SELECT S.[ID],P.[PigeonNO],P.[MemberIdx],P.[GBCode],' +
    'P.[Flag],P.[FactoryCode],P.[JZCode],P.[Group1],P.[Group2],P.[Group3],' +
    'P.[Group4],P.[Group5],P.[Gender],P.[Feather],P.[Eye],P.[Assist1],' +
    'P.[Assist2],P.[Assist3],P.[Assist4],P.[Assist5],P.[Status],M.[MemberNo],' +
    'M.MemberName,M.[AssociationNo],M.[ShortName],M.[State],M.[City],M.[County],' +
    'M.Email,M.Mobile,S.[GameIdx],S.[PigeonIdx],S.[GameCode],S.[CollecteNo],' +
    'S.[CollecteDate],S.[CollectID],S.[SetCode],S.[ReportTime],S.[FlyTime],' +
    'S.[Speed],S.[IsBack],S.[Kind],S.[SourceOrder],S.[Note],H.[MatchName],' +
    'S.[MatchLong]FROM [' + Prefix + 'GameSource] S JOIN ' + Prefix +
    'Pigeon P ON P.ID=S.PigeonIdx JOIN ' + Prefix +
    'Member M ON M.ID=P.MemberIdx JOIN ' + Prefix +
    'Match H ON S.[GameIdx]=H.ID WHERE ' + Condition;
  WriteLog(LOG_DEBUG, 'TSourceView.OpenQuery', 'Will open a query (' + Condition + ').');
  if Result.Parameters.Count = 0 then Result.Open;
end;

{ TPigeonView }

function TPigeonView.LoadData(AConn: TADOConnection; AID: TGuid): Boolean;
var
  DQData: TADOQuery;
begin
  DQData:= TADOQuery.Create(nil);
  try
    DQData.Connection:= AConn;
    OpenQuery(DQData, 'P.ID=''' + GuidToString(AID) + '''');
    DQData.Open;
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

function TPigeonView.LoadData(AConn: TADOConnection;
  Condition: string): Boolean;
var
  DQData: TADOQuery;
begin
  DQData:= TADOQuery.Create(nil);
  try
    DQData.Connection:= AConn;
    OpenQuery(DQData, Condition);
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

function TPigeonView.LoadFromDB(DataSet: TCustomADODataSet): Boolean;
begin
  if DataSet.EOF then Result:= False
  else with DataSet do begin
    FID:= FieldByName('ID').AsGuid;
    FPigeonNO:= FieldByName('PigeonNO').AsString;
    FMemberIdx:= StrToGuid(FieldByName('MemberIdx').AsString);
    FGBCode:= FieldByName('GBCode').AsString;
    FFlag:= FieldByName('Flag').AsString;
    FFactoryCode:= FieldByName('FactoryCode').AsLargeInt;
    FJZCode:= FieldByName('JZCode').AsLargeInt;
    FGroup1:= FieldByName('Group1').AsString;
    FGroup2:= FieldByName('Group2').AsString;
    FGroup3:= FieldByName('Group3').AsString;
    FGroup4:= FieldByName('Group4').AsString;
    FGroup5:= FieldByName('Group5').AsString;
    FGender:= FieldByName('Gender').AsString;
    FFeather:= FieldByName('Feather').AsString;
    FEye:= FieldByName('Eye').AsString;
    FAssist1:= FieldByName('Assist1').AsString;
    FAssist2:= FieldByName('Assist2').AsString;
    FAssist3:= FieldByName('Assist3').AsString;
    FAssist4:= FieldByName('Assist4').AsString;
    FAssist5:= FieldByName('Assist5').AsString;
    FStatus:= FieldByName('Status').AsString;
    FMemberNo:= FieldByName('MemberNo').AsString;
    FMemberName:= FieldByName('MemberName').AsString;
    FAssociationNo:= FieldByName('AssociationNo').AsString;
    FShortName:= FieldByName('ShortName').AsString;
    FState:= FieldByName('State').AsString;
    FCity:= FieldByName('City').AsString;
    FCounty:= FieldByName('County').AsString;
    FEMail:= FieldByName('EMail').AsString;
    FMobile:= FieldByName('Mobile').AsString;
    FCashkind:= FieldByName('Cashkind').AsString;
    Result:= true;
  end;
end;

function TPigeonView.OpenQuery(Query: TADOQuery; Condition: string): Boolean;
begin
  Query.SQL.Text:= 'SELECT P.[ID],P.[PigeonNO],P.[MemberIdx],P.[GBCode],' +
    'P.[Flag],P.[FactoryCode],P.[JZCode],P.[Group1],P.[Group2],P.[Group3],' +
    'P.[Group4],P.[Group5],P.[Gender],P.[Feather],P.[Eye],P.[Assist1],' +
    'P.[Assist2],P.[Assist3],P.[Assist4],P.[Assist5],P.[Status],M.[MemberNo],' +
    'M.MemberName,M.[AssociationNo],M.[ShortName],M.[State],M.[City],' +
    'M.[County],M.EMail,M.Mobile,M.Cashkind FROM [' + Prefix +
    'Pigeon] P JOIN ' + Prefix + 'Member M ON M.ID=P.MemberIdx WHERE ' + Condition;
  Query.Open;
end;

end.
