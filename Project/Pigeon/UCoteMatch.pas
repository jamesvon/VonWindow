unit UCoteMatch;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrameLonLat, StdCtrls, UFrameGridBar, DB, ADODB, Buttons, ComCtrls,
  ExtCtrls, UCoteDB, UCoteInfo, ToolWin, UVonSystemFuns, //UCommunicationModule,
  UFrameRichEditor, IdBaseComponent, IdComponent,
  UVonLog, IdTCPConnection, IdTCPClient, IdHTTP, Menus, Vcl.Samples.Spin;

resourcestring
  CHK_WARNING_BACK = '请先提取再进行归巢.';
  CHK_WARNING_COLL = '请先提取再进行集鸽.';
  CHK_WARNING_START = '请先提取再进行开笼.';
  CHK_WARNING_CHOICE = '请先提取再进行指定.';
  CHK_WARNING_CALC = '请先提取再进行计算.';

  RES_WARNING_DO = '当前处于%s状态不允许进行%s操作。';

type
  TFCoteMatch = class(TForm)
    DQMatch: TADOQuery;
    DBPigeon: TADOTable;
    DQSource: TADOQuery;
    DQSourceCount: TADOQuery;
    FrameMatchGrid: TFrameGridBar;
    Panel1: TPanel;
    Label1: TLabel;
    EComm: TComboBox;
    Label2: TLabel;
    ECommRate: TComboBox;
    btnStart: TBitBtn;
    btnCollect: TBitBtn;
    btnCheckIn: TBitBtn;
    btnChoice: TBitBtn;
    btnUpload: TBitBtn;
    DCUpdate: TADOCommand;
    btnUploadCollection: TBitBtn;
    DQWeather: TADOQuery;
    btnAdd: TBitBtn;
    btnEdit: TBitBtn;
    btnDel: TBitBtn;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Panel5: TPanel;
    LMatchName: TLabel;
    LManager: TLabel;
    LGameAddress: TLabel;
    Label3: TLabel;
    LKind: TLabel;
    LTelphone: TLabel;
    EMatchName1: TEdit;
    EManager1: TEdit;
    EGameAddress1: TEdit;
    ESerial1: TEdit;
    EGameLong1: TEdit;
    EGameLong2: TEdit;
    chkGameLong: TCheckBox;
    EKind1: TComboBox;
    ETelphone1: TEdit;
    btnReset: TBitBtn;
    plCondition: TPanel;
    chkLon: TCheckBox;
    ELat1: TEdit;
    ELat2: TEdit;
    chkLat: TCheckBox;
    dtCollectionDate1: TDateTimePicker;
    dtCollectionDate2: TDateTimePicker;
    chkCollectionDate: TCheckBox;
    dtFlyDate1: TDateTimePicker;
    dtFlyDate2: TDateTimePicker;
    chkFlyDate: TCheckBox;
    ELon2: TEdit;
    ELon1: TEdit;
    btnSearch: TBitBtn;
    Button1: TButton;
    ProgressBar1: TProgressBar;
    btnDeleteSource: TButton;
    btnStopUpload: TBitBtn;
    btnChooseScheme: TBitBtn;
    Bevel3: TBevel;
    Bevel4: TBevel;
    btnRule: TBitBtn;
    Panel2: TPanel;
    Label4: TLabel;
    ESynDataOrder1: TSpinEdit;
    ESynDataOrder2: TSpinEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnSearchClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnCollectClick(Sender: TObject);
    procedure btnStartClick(Sender: TObject);
    procedure btnCheckInClick(Sender: TObject);
    procedure btnChoiceClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btnUploadClick(Sender: TObject);
    procedure btnUploadCollectionClick(Sender: TObject);
    procedure btnDeleteSourceClick(Sender: TObject);
    procedure btnStopUploadClick(Sender: TObject);
    procedure btnRuleClick(Sender: TObject);
    procedure btnChooseSchemeClick(Sender: TObject);
  private
    { Private declarations }
    FInfo: TMatchInfo;
    function GetMatchInfo: TMatchInfo;
  public
    { Public declarations }
  published
    property Info: TMatchInfo read GetMatchInfo;
  end;

var
  FCoteMatch: TFCoteMatch;
  FEventOnNewChildWin: TEventOnNewChildWin;

implementation

uses UCoteCollection, UDlgGameTime,
  UDlgRichEditor, UCoteCheckIn, //UCoteCheckInW,
  ///UCoteChoice,  UDlgSunTime,  UDlgRichEditor,
  UVonConfig, DateUtils, USuperObject, UDlgMatch, UCoteChoose;

{$R *.dfm}

procedure TFCoteMatch.FormCreate(Sender: TObject);
begin
  FInfo := TMatchInfo.Create;
  FInfo.Conn:= FCoteDB.ADOConn;
  FrameMatchGrid.LoadTitles('CoteMatch');
  LKind.Caption := FCoteDB.GetListOption('赛事类型', EKind1.Items);
  EKind1.Items.Insert(0, '训放');
  EKind1.Items.Insert(0, '所有');
  EKind1.ItemIndex := 0;
  btnResetClick(nil);
  btnSearchClick(nil);
  ReadAppItems('COMMUNICATION', 'PortList', 'COM1,COM2,COM3,COM4', EComm.Items);
  EComm.ItemIndex := EComm.Items.IndexOf(ReadAppConfig('COMMUNICATION', 'Port'));
  ECommRate.ItemIndex := ECommRate.Items.IndexOf(ReadAppConfig('COMMUNICATION', 'Speed'));
end;

procedure TFCoteMatch.FormDestroy(Sender: TObject);
begin
  FInfo.Free;
end;

function TFCoteMatch.GetMatchInfo: TMatchInfo;
begin
  if DQMatch.FieldByName('ID').AsGuid <> FInfo.ID then
    FInfo.LoadFromDB(DQMatch);
  Result:= FInfo;
end;

(* 赛事信息管理 *)

procedure TFCoteMatch.btnAddClick(Sender: TObject);
var
  FSource: TGameSourceInfo;
  Idx: Integer;
  S: string;
begin
  with TFDlgMatch.Create(nil) do try
    while ShowModal = mrOK do try
      FormToInfo(FInfo, 'A');
      FInfo.ID:= GetGUID();
      if FInfo.Kind = '训放' then FInfo.Status:= JZS_TRAN
      else FInfo.Status:= JZS_EDIT;
      FInfo.FlyDate:= DB_Min_Time;
      FInfo.AppendToDB(DQMatch);
      WriteLog(LOG_INFO, '赛事管理', '新建赛事' + FInfo.MatchName + '（' + GuidToString(FInfo.ID) + '）。');
      //------------------------------------------//
      if FInfo.Kind = '训放' then try
        FSource:= TGameSourceInfo.Create;
        FSource.Conn:= FCoteDB.ADOConn;
        ProgressBar1.Visible:= true;
        ProgressBar1.Top:= 0;ProgressBar1.Position:= 0;
        Idx:= 1;
        S:= 'INSERT INTO [JOIN_GameSource]([GameIdx],[PigeonIdx],[GameCode],' +
          '[CollecteNo],[CollecteDate],[CollectID],[SetCode],[ReportTime],' +
          '[FlyTime],[Speed],[IsBack],[Kind],[SourceOrder],[Note],[MatchLong],[Modifiedate],[SynFlag])' +
          'SELECT ''' + GuidToString(FInfo.id) + ''',ID,0,' +
          'row_number()over(order by id),GETDATE(),row_number()over(order by id),' +
          '''JOIN_COLLECTION'',0,0,0,0,0,0,''自动集鸽'',' +
          FloatToStr(FInfo.GameLong) + ',GetDate(),''';
        if (FInfo.UploadSetting mod 2) > 0 then S:= S + 'A' else S:= S + 'O';
        S:= S + ''' FROM [JOIN_Pigeon] WHERE JZCode>0 AND Status<>''删除''';
        FCoteDB.ADOConn.Execute(S);

//        with DBPigeon do begin
//          Open;
//          ProgressBar1.Max:= RecordCount;
//          while not EOF do begin
//            WriteLog(LOG_DEBUG, '训放集鸽', Format('ID=%s,JZCode=%d,Status=%s', [FieldByName('ID').AsString, FieldByName('JZCode').AsLargeInt, FieldByName('Status').AsString ]));
//            if(FieldByName('Status').AsString <> '删除')and(FieldByName('JZCode').AsLargeInt > 0)then
//              with FSource do begin
//                ID:= 0;
//                GameIdx:= FInfo.id;
//                PigeonIdx:= FieldByName('ID').AsGuid;
//                GameCode:= 0;
//                CollecteNo:= DateToStr(Now) + IntToStr(Idx);
//                CollecteDate:= Now;
//                CollectID:= Idx;
//                SetCode:= 'JOIN_COLLECTION';
//                ReportTime:= 1;
//                FlyTime:= 0;
//                Speed:= 0;
//                IsBack:= 0;
//                Kind:= 0;
//                SourceOrder:= 0;
//                if FInfo.Serial = '集鸽单和成绩' then SynFlag:= 'U'
//                else SynFlag:= 'O';
//                Note:= '自动集鸽';
//                MatchLong:= FInfo.GameLong;
//                Modifiedate:= Now;
//                SaveData(FCoteDB.ADOConn);
//                ProgressBar1.Position:= Idx; Inc(Idx);
//                ProgressBar1.Repaint;
//              end;
//            Next;
//          end;
//        end;
      finally
        DBPigeon.Close;
        ProgressBar1.Visible:= false;
        FSource.Free;
      end;
//        FCoteDB.AdoConn.Execute('INSERT INTO [' + Prefix + 'GameSource]([GameIdx],' +
//          '[PigeonIdx],[GameCode],[CollecteNo],[CollecteDate],[CollectID],[SetCode],' +
//          '[ReportTime],[FlyTime],[Speed],[IsBack],[Kind],[SourceOrder],[Note],[MatchLong],[Modifiedate],[SynFlag])SELECT ''' +
//          GuidToString(FInfo.ID) + ''',ID,0,'''',GetDate(),row_number()over(order by ' +
//          'DisplayOrder)as RowNum,'''',GetDate(),0,0,0,1,0,0,0,0,0,'''',111,GetDate(),''A'' FROM ' + Prefix + 'Pigeon');
      Exit;
    except
      on E: Exception do begin
        WriteLog(LOG_FAIL, '训放集鸽', '错误:' + E.Message);
        DlgInfo('错误', E.Message);
        DQMatch.Cancel;
      end;
    end;
  finally
    Free;
  end;
end;

procedure TFCoteMatch.btnEditClick(Sender: TObject);
begin
  FInfo.LoadFromDB(DQMatch);
  case FInfo.Status of
    JZS_CANCEL, JZS_START, JZS_BACK, JZS_OVER:
      raise Exception.Create(Format(RES_WARNING_DO,
        [GetMatchStatusName(FInfo.Status), '编辑']));
  end;
  with TFDlgMatch.Create(nil) do try
    InfoToForm(FInfo);
    EKind.Enabled:= False;
    while ShowModal = mrOK do try
      FormToInfo(FInfo, 'U');
      FInfo.UpdateToDB(DQMatch);
      WriteLog(LOG_INFO, '赛事管理', '修改赛事' + FInfo.MatchName + '（' + GuidToString(FInfo.ID) + '）。');
      Exit;
    except
      on E: Exception do begin
        DlgInfo('错误', E.Message);
        DQMatch.Cancel;
      end;
    end;
  finally
    Free;
  end;
end;

procedure TFCoteMatch.btnDelClick(Sender: TObject);
begin
  if MessageDlg(RES_DLG_DEL_PROMPT, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  if MessageDlg(RES_DLG_DEL_WARNING, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  DQMatch.Edit;
  DQMatch.FieldByName('Status').AsInteger := Ord(JZS_CANCEL);
  DQMatch.FieldByName('SynFlag').AsString := 'D';
  DQMatch.Post;
  WriteLog(LOG_MAJOR, '赛事管理', '删除赛事' + DQMatch.FieldByName('MatchName').AsString + '（' + DQMatch.FieldByName('ID').AsString + '）。');
  btnSearchClick(nil);
end;

(* Button events *)

procedure TFCoteMatch.btnCheckInClick(Sender: TObject);
var
  szForm: TFCoteCheckIn;
begin
  DlgInfo('警告', '已开赛，禁止更改系统时间');
  /// <summary>赛事状态</summary>
  /// <param name="JZS_EDIT">编辑状态</param>
  /// <param name="JZS_CANCEL">取消状态</param>
  /// <param name="JZS_COLL">集鸽状态</param>
  /// <param name="JZS_TRAN">在途状态</param>
  /// <param name="JZS_START">开笼状态</param>
  /// <param name="JZS_BACK">归巢状态</param>
  /// <param name="JZS_OVER">结束状态</param>
  case Info.Status of
    JZS_EDIT, JZS_CANCEL, JZS_COLL, JZS_TRAN, JZS_OVER:
      raise Exception.Create(Format(RES_WARNING_DO,
        [GetMatchStatusName(Info.Status), '归巢']));
  end;
//  if DlgInfo('警告', '进入归巢前是否先清除缓存数据？', mbYesNo) <> mrNo then begin
//    DlgInfo('清理中... ...', '系统正在清除缓存数据，请稍等20秒后进入归巢界面！', 20);
//    FCoteDB.Comm.BoardCount := StrToInt(ReadAppConfig('COMMUNICATION', 'BoardCount'));
//    FCoteDB.SetComm(nil, nil, nil, true);
//    FCoteDB.Comm.Open(EComm.ItemIndex + 1, StrToInt(ECommRate.Text), 'n', 8, 1);
//    if FCoteDB.Comm.Connected then
//      DlgInfo('清理中... ...', '系统正在清除缓存数据，请稍等20秒后进入归巢界面！', 20);
//    Sleep(20000);
//  end;

  // function TFPlatformMain.OpenModule(Caption: string; FormClass: TFormClass;
  // RunType: TRunType; ControlID: Integer; TaskParam, ExecuteParam: string): TForm;
  if Assigned(FEventOnNewChildWin) then begin
    szForm := FEventOnNewChildWin('归巢', TFCoteCheckIn, dtMulti, 0, '', '') as TFCoteCheckIn;
    szForm.Start(FInfo.ID, EComm.ItemIndex + 1, StrToInt(ECommRate.Text));
  end;
  WriteAppConfig('COMMUNICATION', 'Port', EComm.Text);
  WriteAppConfig('COMMUNICATION', 'Speed', ECommRate.Text);
  Self.Close;
end;

procedure TFCoteMatch.btnChoiceClick(Sender: TObject);
//var
//  szForm: TFCoteChoice;
begin
//  if FInfo.ID <> DQMatch.FieldByName('ID').AsInteger then
//    raise Exception.Create(CHK_WARNING_CHOICE);
  /// <summary>赛事状态</summary>
  /// <param name="JZS_EDIT">编辑状态</param>
  /// <param name="JZS_CANCEL">取消状态</param>
  /// <param name="JZS_COLL">集鸽状态</param>
  /// <param name="JZS_TRAN">在途状态</param>
  /// <param name="JZS_START">开笼状态</param>
  /// <param name="JZS_BACK">归巢状态</param>
  /// <param name="JZS_OVER">结束状态</param>
  // case FInfo.Status of
  // JZS_CANCEL, JZS_TRAN, JZS_BACK, JZS_OVER:
  // raise Exception.Create(Format(RES_WARNING_DO,
  // [GetMatchStatusName(FInfo.Status), '指定']));
  // end;

  // function OpenModule(UsingName: string; UsingInfo: TAppCmdInfo; RunType: TRunType;
  // ControlID: Integer; TaskParam, ExecuteParam: string): TForm; overload;
  // function OpenModule(Caption: string; FormClass: TFormClass; RunType: TRunType;
  // ControlID: Integer; TaskParam, ExecuteParam: string): TForm; overload;
  //2020
//  szForm := FPlatformMain.OpenModule('指定鸽', TFCoteChoice, dtMulti, 0, '', '')
//    as TFCoteChoice;
//  szForm.Start(FInfo.ID);
//  Self.Close;
end;

procedure TFCoteMatch.btnChooseSchemeClick(Sender: TObject);
var
  szForm: TFCoteChoose;
begin
  if DQMatch.RecordCount = 0 then Exit;
  if Assigned(FEventOnNewChildWin) then
    szForm := FEventOnNewChildWin('指定鸽', TFCoteChoose, dtMulti, 0, GuidToString(FInfo.ID), '') as TFCoteChoose;
  szForm.MatchID:= DQMatch.FieldByName('ID').AsGuid;
end;

procedure TFCoteMatch.btnCollectClick(Sender: TObject);
var
  szForm: TFCollection;
begin
  /// <summary>赛事状态</summary>
  /// <param name="JZS_EDIT">编辑状态</param>
  /// <param name="JZS_CANCEL">取消状态</param>
  /// <param name="JZS_COLL">集鸽状态</param>
  /// <param name="JZS_TRAN">在途状态</param>
  /// <param name="JZS_START">开笼状态</param>
  /// <param name="JZS_BACK">归巢状态</param>
  /// <param name="JZS_OVER">结束状态</param>
  if FInfo.LoadFromDB(DQMatch)then
    case FInfo.Status of
      JZS_CANCEL, JZS_TRAN, JZS_START, JZS_BACK, JZS_OVER:
        begin
          DlgInfo('错误', Format(RES_WARNING_DO,
            [GetMatchStatusName(FInfo.Status), '集鸽']));
          Exit;
        end;
    end;
  if Assigned(FEventOnNewChildWin) then begin
    szForm := FEventOnNewChildWin('集鸽', TFCollection, dtMulti, 0, GuidToString(FInfo.ID), '') as TFCollection;
    szForm.Start(FInfo.ID, Self.EComm.ItemIndex + 1,
      StrToInt(Self.ECommRate.Text));
  end;
  WriteAppConfig('COMMUNICATION', 'Port', EComm.Text);
  WriteAppConfig('COMMUNICATION', 'Speed', ECommRate.Text);
  Self.Close;
end;

procedure TFCoteMatch.btnStartClick(Sender: TObject);
var
  SourceCount: Integer;
begin     // 开赛
  DQMatch.Refresh;
  {$REGION '开赛前保护,状态检查、有归巢后不允许再开笼'}
  if DQMatch.RecordCount < 1 then
    Exit;
  with DQSourceCount do // 提取集鸽数量
    try
      Parameters[0].Value := DQMatch.FieldByName('ID').AsString;
      Open;
      SourceCount := Fields[0].AsInteger;
    finally
      Close;
    end;
  if SourceCount = 0 then
  begin
    ShowMessage('尚未进行集鸽，无法进行比赛，拒绝开笼。');
    Exit;
  end;
  case Info.Status of
    // JZS_COLL, JZS_TRAN, JZS_START
    JZS_EDIT, JZS_CANCEL, JZS_BACK, JZS_OVER:
      raise Exception.Create(Format(RES_WARNING_DO,
        [GetMatchStatusName(FInfo.Status), '开笼']));
  end;
  {$ENDREGION}
  (* 录入开始时间 *)
  with TFDlgGameTime.Create(Self) do
    try
      LGameName.Caption := Info.MatchName;
      LGameKind.Caption := Info.Kind;
      LGameLong.Caption := FloatToStr(Info.GameLong);
      LCalcLong.Caption := LGameLong.Caption;
      if Info.SunRaise = 0 then dtSunRaise.Time := EncodeTime(5,0,0,0)
      else dtSunRaise.Time := Info.SunRaise;
      if Info.SunDown = 0 then dtSunDown.Time := EncodeTime(19,0,0,0)
      else dtSunDown.Time := Info.SunDown;
      FrameFrameLon.Value := FInfo.Lon;
      FrameFrameLat.Value := FInfo.Lat;
      EWind.Text := FInfo.Wind;
      EWindPower.Text := FInfo.WindPower;
      ETemperature.Text := FloatToStr(FInfo.Temperature);
      EHumidity.Text := FInfo.Humidity;
      EDate.Date := Date;
      ETime.Time := Time;
      if ShowModal <> mrOK then
        Exit;
      FInfo.SunRaise := dtSunRaise.Time;
      FInfo.SunDown := dtSunDown.Time;
      FInfo.Wind := EWind.Text;
      FInfo.WindPower := EWindPower.Text;
      FInfo.Temperature := StrToFloat(ETemperature.Text);
      FInfo.Humidity := EHumidity.Text;
      FInfo.Lon := FrameFrameLon.Value;
      FInfo.Lat := FrameFrameLat.Value;
      FInfo.FlyDate := FlyTime;
      FInfo.Status := JZS_START;
    finally
      Free;
    end;
  (* 开赛数据准备 *)
  if DlgInfo('警告',
    '该赛事一旦开赛，系统将对所有参赛信鸽开始启动飞行计时，并以此点开始计算信鸽的飞行时间和速度，并且一旦开赛后系统将无法重新开赛和集鸽，您是否确定要开赛?',
    [mbOK, mbCancel], 30) = mrOK then
  begin
    (* change status to match and init start time *)
    FInfo.UpdateToDB(DQMatch);
    FCoteDB.ADOConn.Execute('UPDATE JOIN_GameSource SET SynFlag=''O'' WHERE GameIdx=''' + GuidToString(FInfo.ID) + '''');
    WriteLog(LOG_MAJOR, 'Match', Format('The match %s is start at %s.',
      [FInfo.MatchName, DatetimeToStr(FInfo.FlyDate)]));
  end;
end;

procedure TFCoteMatch.Button1Click(Sender: TObject);
var       //模拟归巢
  ReportTime: TDatetime;
  FCurrentOrder: Integer;
begin
//  ReportTime:= Now;
//  with TADOQuery.Create(nil)do try
//    Connection:= FCoteDB.ADOConn;
//    SQL.Text:= 'SELECT * FROM JOIN_GameSource WHERE GameIdx=''' + DQMatch.FieldByName('ID').AsString + '''';
//    Open;
//    FCurrentOrder:= 1;
//    while not EOF do begin
//      Edit;
//      FieldByName('SourceOrder').AsInteger:= FCurrentOrder;
//      FieldByName('ReportTime').AsDateTime:= ReportTime;
//      FieldByName('FlyTime').AsExtended:= (ReportTime - DQMatch.FieldByName('FlyDate').AsDateTime) * 1440;
//      FieldByName('Speed').AsExtended:= DQMatch.FieldByName('GameLong').AsExtended * 1000 / FieldByName('FlyTime').AsExtended;
//      FieldByName('SynFlag').AsString:= 'U';
//      FieldByName('Modifiedate').AsDatetime:= Now;
//      Post;
//      ReportTime:= IncMilliSecond(ReportTime, Random(5000));
//      Inc(FCurrentOrder);
//      Next;
//    end;
//  finally
//    Free;
//  end;
end;

procedure TFCoteMatch.btnDeleteSourceClick(Sender: TObject);
begin
  if MessageDlg(RES_DLG_DEL_SOURCE_PROMPT, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  if MessageDlg(RES_DLG_DEL_WARNING, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  FCoteDB.ADOConn.Execute('UPDATE [JOIN_GameSource] SET [ReportTime]=''' +
    '1899-12-31 00:00:00.000'',[FlyTime]=0,[Speed]=0,[IsBack]=0,[SourceOrder]=0,' +
    '[Modifiedate] = GetDate(),[SynFlag] = ''U''WHERE [GameIdx]=''' +
    DQMatch.FieldByName('ID').AsString + '''');
  WriteLog(LOG_MAJOR, '赛事管理', '赛事' + DQMatch.FieldByName('MatchName').AsString + '（' + DQMatch.FieldByName('ID').AsString + '）成绩被强制清除。');
  DlgInfo('清理中完成', '该赛事成绩已经归零，可以进入归巢界面进行归巢了！');
end;

(* 查询 *)

procedure TFCoteMatch.btnResetClick(Sender: TObject);
begin // 设置查询条件为空
  EMatchName1.Text := '';
  EGameAddress1.Text := '';
  EManager1.Text := '';
  EGameLong1.Text := '0.00';
  EGameLong2.Text := '0.00';
  chkGameLong.Checked := False;
  ESerial1.Text := '';
  EKind1.Text := '';
  ETelphone1.Text := '';
  ELon1.Text := '0.00';
  ELon2.Text := '0.00';
  chkLon.Checked := False;
  ELat1.Text := '0.00';
  ELat2.Text := '0.00';
  chkLat.Checked := False;
  dtCollectionDate1.Date := Now;
  dtCollectionDate2.Date := Now;
  chkCollectionDate.Checked := False;
  dtFlyDate1.Date := Now;
  dtFlyDate2.Date := Now;
  chkFlyDate.Checked := False;
end;

procedure TFCoteMatch.btnRuleClick(Sender: TObject);
begin
  with TFDlgRichEditor.Create(nil)do try
    //Editor.Lines.Assign();
    //if True then

  finally
    Free;
  end;
end;

procedure TFCoteMatch.btnSearchClick(Sender: TObject);
var
  Idx: Integer;
begin // 查询
  with DQMatch.SQL do
  begin
    Text := 'SELECT * FROM [JOIN_Match] WHERE Status>0';
    Idx := Count;
    if EMatchName1.Text <> '' then
      Add('AND MatchName LIKE ''%' + EMatchName1.Text + '%''');
    if EGameAddress1.Text <> '' then
      Add('AND GameAddress LIKE ''%' + EGameAddress1.Text + '%''');
    if EManager1.Text <> '' then
      Add('AND Manager LIKE ''%' + EManager1.Text + '%''');
    if chkGameLong.Checked then
    begin
      Add('AND GameLong >= ' + EGameLong1.Text);
      Add('AND GameLong >= ' + EGameLong2.Text);
    end;
    if ESerial1.Text <> '' then
      Add('AND Serial LIKE ''%' + ESerial1.Text + '%''');
    if EKind1.Text <> '所有' then
      Add('AND Kind LIKE ''%' + EKind1.Text + '%''');
    if ETelphone1.Text <> '' then
      Add('AND Telphone LIKE ''%' + ETelphone1.Text + '%''');
    if chkLon.Checked then
    begin
      Add('AND Lon >= ' + ELon1.Text);
      Add('AND Lon >= ' + ELon2.Text);
    end;
    if chkLat.Checked then
    begin
      Add('AND Lat >= ' + ELat1.Text);
      Add('AND Lat >= ' + ELat2.Text);
    end;
    if chkCollectionDate.Checked then
    begin
      Add('AND CollectionDate >= ' + FormatDateTime('yyyy-mm-dd',
        dtCollectionDate1.Date));
      Add('AND CollectionDate >= ' + FormatDateTime('yyyy-mm-dd',
        dtCollectionDate2.Date));
    end;
    if chkFlyDate.Checked then
    begin
      Add('AND FlyDate >= ' + FormatDateTime('yyyy-mm-dd', dtFlyDate1.Date));
      Add('AND FlyDate >= ' + FormatDateTime('yyyy-mm-dd', dtFlyDate2.Date));
    end;
  end;
  DQMatch.SQL.Add('ORDER BY [CreateDate]DESC');
  DQMatch.Open;
end;

(* 强制处理 *)

procedure TFCoteMatch.btnUploadCollectionClick(Sender: TObject);
begin     //强制上传赛事信息
  if DQMatch.RecordCount = 0 then Exit;
  if DlgInfo('警告', '该赛事是否强制上传赛事信息?', [mbOK, mbCancel], 30) <> mrOK then Exit;
  with DQMatch do begin
    Edit;
    FieldByName('UploadSetting').AsInteger:= 3;
    FieldByName('SynFlag').AsString:= 'U';
    Post;
  end;

  WriteLog(LOG_MAJOR, '赛事管理', '强制上传赛事' + DQMatch.FieldByName('MatchName').AsString + '（' + DQMatch.FieldByName('ID').AsString + '）信息。');
  DlgInfo('信息', '该赛事已经准备上传赛事信息，请确认系统服务工作正常！');
end;

procedure TFCoteMatch.btnUploadClick(Sender: TObject);
var
  gameIdx, S : string;
begin     //强制上传赛绩信息
  if DQMatch.RecordCount = 0 then Exit;
  if DlgInfo('警告', '该赛事是否强制上传赛事成绩?', [mbOK, mbCancel], 30) <> mrOK then Exit;
  gameIdx:= DQMatch.FieldByName('ID').AsString;
  DQMatch.Close;
  DQMatch.Open;
  if not DQMatch.Locate('ID', gameIdx, [])then
    raise Exception.Create(RES_NO_LOCATE);
  if DQMatch.FieldByName('SynFlag').AsString <> 'O' then
    raise Exception.Create('赛事信息尚未同步，不允许上传赛绩信息');
  S:= '';
  if ESynDataOrder1.Value > 0 then S:= S + ' AND SourceOrder >= ' + IntToStr(ESynDataOrder1.Value);
  if ESynDataOrder2.Value > 0 then S:= S + ' AND SourceOrder <= ' + IntToStr(ESynDataOrder2.Value);

  FCoteDB.ADOConn.Execute('UPDATE [dbo].[JOIN_GameSource] SET [SynFlag]=''U''' +
    ' WHERE [GameIdx]=''' + gameIdx + ''' AND SourceOrder>0' + S);
  WriteLog(LOG_MAJOR, '赛事管理', '强制上传赛事' + DQMatch.FieldByName('MatchName').AsString + '（' + DQMatch.FieldByName('ID').AsString + '）成绩。');
  DlgInfo('信息', '该赛事成绩已经准备上传，请确认系统服务工作正常！');
end;

procedure TFCoteMatch.btnStopUploadClick(Sender: TObject);
begin     //强制终止上传赛绩信息
  if DQMatch.RecordCount = 0 then Exit;
  if DlgInfo('警告', '该赛事是否强制终止上传赛事成绩?', [mbOK, mbCancel], 30) <> mrOK then Exit;
  FCoteDB.ADOConn.Execute('UPDATE [dbo].[JOIN_GameSource] SET [SynFlag]=''O''' +
    ' WHERE [GameIdx]=''' + DQMatch.FieldByName('ID').AsString + ''' AND [SynFlag]=''U''');
  WriteLog(LOG_MAJOR, '赛事管理', '强制终止上传赛事' + DQMatch.FieldByName('MatchName').AsString + '（' + DQMatch.FieldByName('ID').AsString + '）信息。');
  DlgInfo('信息', '该赛事成绩已经准备上传，请确认系统服务工作正常！');
end;

end.
