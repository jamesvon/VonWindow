unit UCoteMember;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrameGridBar, ExtCtrls, UCoteDB, DB, ADODB, StdCtrls, Buttons,
  UFrameLonLat, ComCtrls, UFrameImageInput, UFrameRichEditor, UCoteInfo,
  UVonSystemFuns, IniFiles, Menus, Vcl.ButtonGroup, Data.Bind.EngExt,
  Vcl.Bind.DBEngExt, System.Rtti, System.Bindings.Outputs, Vcl.Bind.Editors,
  Data.Bind.Components;

type
  TFCoteMember = class(TForm)
    DQMember: TADOQuery;
    plEditor: TPanel;
    DQPigeonCount: TADOQuery;
    FrameMemberGrid: TFrameGridBar;
    btnAdd: TBitBtn;
    btnEdit: TBitBtn;
    btnDel: TBitBtn;
    DQZone: TADOQuery;
    btnSearch: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnSearchClick(Sender: TObject);
    procedure FrameMemberGridmenuConditionClick(Sender: TObject);
    procedure FrameMemberGridmenuEditClick(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
  private
    { Private declarations }
    FSQLText: string;
    FInfo: TMemberInfo;
    FMemberNoFmt: string;
  public
    { Public declarations }
  end;

var
  FCoteMember: TFCoteMember;

implementation

uses DateUtils, UDlgMember;

{$R *.dfm}

procedure TFCoteMember.FormCreate(Sender: TObject);
begin
  FInfo := TMemberInfo.Create;
  FSQLText := DQMember.SQL.Text;
  FrameMemberGrid.LoadTitles('CoteMember');
  FMemberNoFmt := ReadAppConfig('SYSTEM', 'AutoMemberCode');
  btnSearchClick(nil);
end;

procedure TFCoteMember.FormDestroy(Sender: TObject);
begin
  FInfo.Free;
end;

procedure TFCoteMember.btnSearchClick(Sender: TObject);
begin
  TFDlgMember.SearchDlg(DQMember);
end;

procedure TFCoteMember.btnAddClick(Sender: TObject);
begin
  TFDlgMember.InputDlg(DQMember, true);
end;

procedure TFCoteMember.btnDelClick(Sender: TObject);
begin
  with DQPigeonCount do
    try
      Parameters[0].Value := GuidToString(FInfo.ID);
      Open;
      if RecordCount > 0 then
        raise Exception.Create(RES_NODE_HASCHILDREN);
    finally
      Close;
    end;
  if MessageDlg(RES_DLG_DEL_PROMPT, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  if MessageDlg(RES_DLG_DEL_WARNING, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  DQMember.Edit;
  DQMember.FieldByName('MemberName').AsString:= '__' + RandomStr(18, true, true, true, false);
  DQMember.FieldByName('MemberNo').AsString:= '__' + RandomStr(36, true, true, true, false);
  DQMember.FieldByName('Status').AsString:= 'ɾ��';
  DQMember.FieldByName('SynFlag').AsString:= 'U';
  DQMember.Post;
end;

procedure TFCoteMember.btnEditClick(Sender: TObject);
begin
  TFDlgMember.InputDlg(DQMember, false);
end;

procedure TFCoteMember.FrameMemberGridmenuConditionClick(Sender: TObject);
begin
  FrameMemberGrid.menuConditionClick(Sender);
  TFDlgMember.SearchDlg(DQMember);
end;

procedure TFCoteMember.FrameMemberGridmenuEditClick(Sender: TObject);
begin
  FrameMemberGrid.menuEditClick(Sender);
  plEditor.Visible := not FrameMemberGrid.menuEdit.Checked;
end;

end.
