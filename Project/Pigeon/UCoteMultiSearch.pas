unit UCoteMultiSearch;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, InvokeRegistry, UFrameGridBar, Rio, SOAPHTTPClient, ImgList, ADODB,
  DB, Grids, DBGrids, StdCtrls, Spin, Buttons, ExtCtrls, ComCtrls, ToolWin,
  UFrameViewBar, System.ImageList, System.Net.URLClient;

type
  TFCoteMultiSearch = class(TForm)
    Splitter1: TSplitter;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Label1: TLabel;
    btnSub: TBitBtn;
    btnMain: TBitBtn;
    SpinEdit1: TSpinEdit;
    DBGrid2: TDBGrid;
    DBMatch: TADOTable;
    DSMatch: TDataSource;
    DQBack: TADOQuery;
    ImageList1: TImageList;
    SaveDialog1: TSaveDialog;
    HTTPRIO1: THTTPRIO;
    DQSubOrder: TADOQuery;
    FrameViewBar1: TFrameViewBar;
    procedure FormCreate(Sender: TObject);
    procedure btnMainClick(Sender: TObject);
    procedure btnSubClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCoteMultiSearch: TFCoteMultiSearch;

implementation

uses UCoteDB;

{$R *.dfm}

procedure TFCoteMultiSearch.btnMainClick(Sender: TObject);
var
  I: Integer;
  G: PGuid;
begin
  FrameViewBar1.lstView.Clear;
  FrameViewBar1.lstView.Columns.Clear;
  FrameViewBar1.lstView.Columns.Add.Caption := '决赛排名';
  FrameViewBar1.lstView.Columns.Add.Caption := '统一环号';
  FrameViewBar1.lstView.Columns.Add.Caption := '省份';
  FrameViewBar1.lstView.Columns.Add.Caption := '城市';
  FrameViewBar1.lstView.Columns.Add.Caption := '地区';
  FrameViewBar1.lstView.Columns.Add.Caption := '鸽主';
  FrameViewBar1.lstView.Columns.Add.Caption := '决赛名称';
  FrameViewBar1.lstView.Columns.Add.Caption := '分速';
  FrameViewBar1.lstView.Columns.Add.Caption := '归巢时间';
  FrameViewBar1.lstView.Columns.Add.Caption := '集鸽时间';
  FrameViewBar1.lstView.Columns.Add.Caption := '羽色';
  // for I := FrameViewBar1.lstView.Columns.Count - 1 downto 10 do
  // FrameViewBar1.lstView.Columns.Delete(I);
  with DQBack do
  begin
    Close;
    Parameters[0].Value := DBMatch.FieldByName('ID').AsString;
    Parameters[1].Value := SpinEdit1.Value;
    Open;
    while not EOF do
    begin
      with FrameViewBar1.lstView.Items.Add do
      begin
        // P.ID,S.[SourceOrder],P.[GBCode],M.[ID] MemberID,M.[State],M.[City],M.[County],M.[MemberName] MemberName,
        // G.[MatchName],S.Speed,S.BackTime,S.CollecteDate,S.Feather
        New(G); G^:= Fields[0].AsGuid;
        Data := G; // P.ID
        Caption := Fields[1].AsString; // S.[SourceOrder]
        SubItems.Add(Fields[2].AsString); // P.[GBCode]
        // M.[ID] MemberID
        SubItems.Add(Fields[4].AsString); // M.[State]
        SubItems.Add(Fields[5].AsString); // M.[City]
        SubItems.Add(Fields[6].AsString); // M.[County]
        SubItems.Add(Fields[7].AsString); // MemberName
        SubItems.Add(Fields[8].AsString); // G.[MatchName]
        SubItems.Add(Fields[9].AsString); // S.Speed
        SubItems.Add(Fields[10].AsString); // S.BackTime
        SubItems.Add(Fields[11].AsString); // S.CollecteDate
        SubItems.Add(Fields[12].AsString); // S.Feather
      end;
      Next;
    end;
    Close;
  end;
  btnSub.Enabled := True;
end;

procedure TFCoteMultiSearch.btnSubClick(Sender: TObject);
var
  I: Integer;
begin // 查询系列赛事
  FrameViewBar1.lstView.Columns.Add.Caption := '排名';
  FrameViewBar1.lstView.Columns.Add.Caption := '赛事名称';
  FrameViewBar1.lstView.Columns.Add.Caption := '分速';
  FrameViewBar1.lstView.Columns.Add.Caption := '归巢时间';
  for I := 0 to FrameViewBar1.lstView.Items.Count - 1 do
    with DQSubOrder do
    begin
      Parameters[0].Value := DBMatch.Fields[0].AsString;
      Parameters[1].Value := GuidToString(PGuid(FrameViewBar1.lstView.Items[I].Data)^);
      Open;
      if not EOF then
      begin
        FrameViewBar1.lstView.Items[I].SubItems.Add(Fields[0].AsString);
        FrameViewBar1.lstView.Items[I].SubItems.Add(Fields[1].AsString);
        FrameViewBar1.lstView.Items[I].SubItems.Add(Fields[2].AsString);
        FrameViewBar1.lstView.Items[I].SubItems.Add(Fields[3].AsString);
      end else
      begin
        FrameViewBar1.lstView.Items[I].SubItems.Add('');
        FrameViewBar1.lstView.Items[I].SubItems.Add('');
        FrameViewBar1.lstView.Items[I].SubItems.Add('');
        FrameViewBar1.lstView.Items[I].SubItems.Add('');
      end;
      Close;
    end;
end;

procedure TFCoteMultiSearch.FormCreate(Sender: TObject);
begin
  // FrameGridBar1.LoadTitles('CoteMulti');
  DBMatch.Open;
end;

end.
