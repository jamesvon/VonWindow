unit UCotePigeon;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, Buttons, UFrameImageInput, DB,
  IniFiles, ADODB, Grids, DBGrids, DBCtrls, UCoteDB,
  DateUtils, ToolWin, Spin, ImgList, UFrameGridBar, Tabs, UCoteInfo, UCoteComm,
  UVonSystemFuns, UVonLog, UFrameRichEditor, UDlgCamera, Menus;

resourcestring
  RES_DISPLAY_COUNT = '当前公棚共有%d只信鸽，当前会员拥有%d只信息，新增%d只信鸽';
  RES_NO_GET_MEMBER = '未提取会员信息，不允许本操作';
  RES_NO_GET_PIGEON = '未提取信鸽信息，不允许本操作';
  RES_MEMBER_HAS_PIGEON = '名下有鸽子不允许删除';

type
  TFCotePigeon = class(TForm)
    DSHouse: TDataSource;
    PanelMember: TPanel;
    DCChangeMID: TADOCommand;
    DCChangeUserName: TADOCommand;
    DQMember: TADOQuery;
    TabSet1: TTabSet;
    DQPigeon: TADOQuery;
    DQPigeonByID: TADOQuery;
    FrameMemberGrid: TFrameGridBar;
    FramePigeonGrid: TFrameGridBar;
    Panel18: TPanel;
    btnPigeonSearch: TBitBtn;
    btnPigeonAdd: TBitBtn;
    btnPigeonEdit: TBitBtn;
    btnPigeonDel: TBitBtn;
    btnCheckIn: TBitBtn;
    selGroup: TComboBox;
    btnAutoGroup: TBitBtn;
    btnChoiceGroup: TBitBtn;
    btnVideo: TBitBtn;
    spMember: TSplitter;
    DQCHECK_W: TADOQuery;
    Bevel1: TBevel;
    btnTakeOn: TBitBtn;
    btnTakeOff: TBitBtn;
    Bevel2: TBevel;
    plMemberHint: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBText6: TDBText;
    plCondition: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    EShortName1: TEdit;
    Label22: TLabel;
    EMemberNo1: TEdit;
    EMemberName1: TEdit;
    Panel7: TPanel;
    Label1: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    EState1: TComboBox;
    ECity1: TComboBox;
    ECounty1: TComboBox;
    btnMemberReset: TBitBtn;
    btnMemberSearch: TBitBtn;
    EGroupValue: TComboBox;
    btnRemoveGroup: TBitBtn;
    Panel8: TPanel;
    btnMemberAdd: TBitBtn;
    btnMemberEdit: TBitBtn;
    btnMemberDel: TBitBtn;
    Bevel3: TBevel;
    btnTranfer: TBitBtn;
    Bevel4: TBevel;
    ESGBCode: TEdit;
    Label9: TLabel;
    ESCashKind: TComboBox;
    ChkAutoGetECode: TCheckBox;
    lbElectricCode: TLabel;
    EElectricCode: TEdit;
    Panel2: TPanel;
    Label13: TLabel;
    ECommRate: TComboBox;
    Panel1: TPanel;
    Label12: TLabel;
    EComm: TComboBox;
    Bevel5: TBevel;
    Bevel6: TBevel;
    Bevel7: TBevel;
    rbAutoJoin: TSpeedButton;
    rbAutoRing: TSpeedButton;
    DBText7: TDBText;
    DBText8: TDBText;
    DBText9: TDBText;
    DBText10: TDBText;
    rbAutoFind: TSpeedButton;
    rbAutoSearch: TSpeedButton;
    Label10: TLabel;
    EMobile1: TEdit;
    DQCHECK_R: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ECommChange(Sender: TObject);
    procedure btnMemberAddClick(Sender: TObject);
    procedure btnMemberEditClick(Sender: TObject);
    procedure btnMemberDelClick(Sender: TObject);
    procedure btnPigeonSearchClick(Sender: TObject);
    procedure btnPigeonAddClick(Sender: TObject);
    procedure btnPigeonEditClick(Sender: TObject);
    procedure btnPigeonDelClick(Sender: TObject);
    procedure ChkAutoGetECodeClick(Sender: TObject);
    procedure TabSet1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnCheckInClick(Sender: TObject);
    procedure btnTranferClick(Sender: TObject);
    procedure btnAutoGroupClick(Sender: TObject);
    procedure btnChoiceGroupClick(Sender: TObject);
    procedure btnVideoClick(Sender: TObject);
    procedure DQMemberAfterScroll(DataSet: TDataSet);
    procedure FrameMemberGridmenuConditionClick(Sender: TObject);
    procedure PanelMemberResize(Sender: TObject);
    procedure btnTakeOffClick(Sender: TObject);
    procedure btnTakeOnClick(Sender: TObject);
    procedure EState1Change(Sender: TObject);
    procedure ECity1Change(Sender: TObject);
    procedure btnMemberResetClick(Sender: TObject);
    procedure btnMemberSearchClick(Sender: TObject);
    procedure selGroupChange(Sender: TObject);
    procedure btnRemoveGroupClick(Sender: TObject);
    procedure rbAutoSearchClick(Sender: TObject);
    procedure rbAutoJoinClick(Sender: TObject);
    procedure rbAutoRingClick(Sender: TObject);
    procedure ESGBCodeChange(Sender: TObject);
    procedure rbAutoFindClick(Sender: TObject);
    procedure EShortName1Change(Sender: TObject);
    procedure EMemberName1Change(Sender: TObject);
  private
    { Private declarations }
    FComm: TJZCommBase;
    FNewCount, FAllCount: Integer;
    FGameCodes: PArrayInt;
    FMemberSQL: string;
    FPigeonInfo: TPigeonInfo;
    FPigeonSQL: string;
    FCurrentFactoryCode: Int64;
    FCurrentJZCode: Int64;
    FCheckPigeonNO: Boolean;
    FTransferList: TStringList;         //准备过户的赛鸽缓存
    FGroupCount: array[0..4]of Integer; //各分组允许的最大赛鸽数量
    FGroupKeys: array[0..32]of string;  //分组的序列值表
    procedure RefrenshCount;
    procedure EventOfReceived(JZCode: Int64; GameCode: Cardinal; FactoryCode: Int64);
    function CheckDoublication(PID: TGuid): Boolean;
    procedure DeleteECode();
    procedure DisplayMember(IsShow: Boolean);
    procedure EventOnKeyMessage(var Msg: TMsg; var Handled: Boolean);
  public
    { Public declarations }
  end;

var
  FCotePigeon: TFCotePigeon;

implementation

uses UDlgMember, UDlgPigeon;

{$R *.dfm}

procedure TFCotePigeon.FormCreate(Sender: TObject);
var
  lst: TStringList;
  I: Integer;
begin
  ReadAppItems('COMMUNICATION', 'PortList', 'COM1,COM2,COM3,COM4', EComm.Items);
  EComm.ItemIndex := EComm.Items.IndexOf(ReadAppConfig('COMMUNICATION', 'Port'));
  ECommRate.ItemIndex := ECommRate.Items.IndexOf(ReadAppConfig('COMMUNICATION',
    'Speed'));
  //FMemberInfo := TMemberInfo.Create;
  FTransferList := TStringList.Create;
  FMemberSQL := DQMember.SQL.Text;
  FPigeonInfo := TPigeonInfo.Create;
  New(FGameCodes);
  RandomNums(1, 6, 0, FGameCodes); // 产生8192个赛事环号->电子环号
  FPigeonSQL := DQPigeon.SQL.Text;
  FrameMemberGrid.LoadTitles('CoteMember');
  FramePigeonGrid.LoadTitles('CotePigeon');
  FCheckPigeonNO := ReadAppConfig('SYSTEM', 'CheckPigeonNo') = '1';
{$REGION 'Init member environment'}
  //FMemberInfo := TMemberInfo.Create;
  FMemberSQL := DQMember.SQL.Text;
  FCoteDB.GetSqlDropList('SELECT ID, ItemName FROM SYS_Zone WHERE PID=0', EState1);
  EState1.ItemIndex := 0;
  EState1Change(nil);
  ECity1Change(nil);
  FCoteDB.GetDropList('缴费方式', ESCashKind);
{$ENDREGION}
  (* Init variables *)
  //TFDlgMember.SearchDlg(DQMember);
  DQMember.Open;
  DQMember.AfterScroll:= DQMemberAfterScroll;
{$REGION 'Init group environment'}
  FCoteDB.ReinitGroup;
  for I := 0 to 4 do
    if FCoteDB.FGroupNames[I] <> '' then
      selGroup.Items.Add(FCoteDB.FGroupNames[I]);
  selGroup.ItemIndex := 0;
{$ENDREGION}
  DQMemberAfterScroll(nil);
  RefrenshCount;
  Application.OnMessage:= EventOnKeyMessage;

end;

procedure TFCotePigeon.FormDestroy(Sender: TObject);
begin
  if Assigned(FComm) then FCoteDB.CloseComm(FComm);
  Dispose(FGameCodes);
  FTransferList.Free;
  FPigeonInfo.Free;
  //FMemberInfo.Free;
  DQMember.Close;
  Application.OnMessage:= nil;
end;

procedure TFCotePigeon.EventOnKeyMessage(var Msg: TMsg; var Handled:Boolean);
begin
  if Msg.message = WM_KEYDOWN then
    if Msg.Wparam = VK_F5 then
    begin
      DQPigeon.Close;
      DQPigeon.Open;
    end;
end;

{ TFPigeon.Member }

procedure TFCotePigeon.ESGBCodeChange(Sender: TObject);
begin
  if ESGBCode.Text <> '' then begin
    DisplayMember(False);
    DQPigeon.Close;
    DQPigeon.SQL.Text := FPigeonSQL;
    DQPigeon.SQL.Add(' AND GBCode LIKE ''%' + ESGBCode.Text + '%''');
    DQPigeon.Open;
    if(DQPigeon.RecordCount = 1)and(rbAutoFind.Down)then begin
      if DQPigeon.FieldByName('JZCode').AsLongWord = 0 then begin
        if DlgInfo('警告', '是否真的将该电子环佩戴给' + DQPigeon.FieldByName('GBCode').AsString +
          '？', mbYesNo, 20) = mrYes then btnTakeOnClick(nil);
      end;
    end;
  end else begin
    DisplayMember(true);
  end;
end;

procedure TFCotePigeon.EShortName1Change(Sender: TObject);
begin
  btnMemberSearchClick(nil);
end;

procedure TFCotePigeon.EState1Change(Sender: TObject);
begin
  if EState1.ItemIndex < 1 then Exit;
  FCoteDB.GetSqlDropList('SELECT ID, ItemName FROM SYS_Zone WHERE PID=' +
    IntToStr(Integer(EState1.Items.Objects[EState1.ItemIndex])), ECity1);
end;

procedure TFCotePigeon.ECity1Change(Sender: TObject);
begin
  if ECity1.ItemIndex < 1 then Exit;
  FCoteDB.GetSqlDropList('SELECT ID, ItemName FROM SYS_Zone WHERE PID=' +
    IntToStr(Integer(ECity1.Items.Objects[ECity1.ItemIndex])), ECounty1);
end;

procedure TFCotePigeon.btnMemberResetClick(Sender: TObject);
begin
  EMemberName1.Text := '';
  EMemberNo1.Text := '';
  EShortName1.Text := '';
  EState1.ItemIndex := 0;
  ECity1.ItemIndex := 0;
  ECounty1.ItemIndex := 0;
  ESCashKind.ItemIndex := 0;
end;

procedure TFCotePigeon.btnMemberSearchClick(Sender: TObject);
begin
  with DQMember do begin
    Close;
    SQL.Text := 'SELECT * FROM ' + Prefix + 'Member WHERE Status=''启用''';
    if EMemberName1.Text <> '' then
      SQL.Add('AND MemberName LIKE ''%' + EMemberName1.Text + '%''');
    if EMemberNo1.Text <> '' then
      SQL.Add('AND MemberNo LIKE ''%' + EMemberNo1.Text + '%''');
    if EShortName1.Text <> '' then
      SQL.Add('AND ShortName LIKE ''%' + EShortName1.Text + '%''');
    if EMobile1.Text <> '' then
      SQL.Add('AND [Mobile] LIKE ''%' + EMobile1.Text + '%''');
    if ESCashKind.ItemIndex > 0 then
      SQL.Add('AND CashKind=''' + ESCashKind.Text + '''');
    if EState1.ItemIndex > 0 then
      SQL.Add('AND State LIKE ''%' + EState1.Text + '%''');
    if ECity1.ItemIndex > 0 then
      SQL.Add('AND City LIKE ''%' + ECity1.Text + '%''');
    if ECounty1.ItemIndex > 0 then
      SQL.Add('AND County LIKE ''%' + ECounty1.Text + '%''');
    SQL.Add('ORDER BY DisplayOrder');
    Open;
  end;
  DBText6.Font.Color:= clMaroon;
  DBText7.Font.Color:= clMaroon;
  DBText8.Font.Color:= clMaroon;
  DBText9.Font.Color:= clMaroon;
  DBText10.Font.Color:= clMaroon;
end;

procedure TFCotePigeon.FrameMemberGridmenuConditionClick(Sender: TObject);
begin
  TFDlgMember.SearchDlg(DQMember);
end;

procedure TFCotePigeon.PanelMemberResize(Sender: TObject);
begin
  if PanelMember.Width < 120 then begin
    PanelMember.Visible := False;
    spMember.Visible := False;
  end;
  plMemberHint.Visible:= PanelMember.Visible;
end;

procedure TFCotePigeon.DisplayMember(IsShow: Boolean);
begin
  PanelMember.Visible:= IsShow;
  if PanelMember.Visible then begin
    spMember.Visible := True;
    spMember.Left:= MaxInt;
    btnRemoveGroup.Visible:= True;
    btnAutoGroup.Visible:= True;
    btnChoiceGroup.Visible:= True;
    EGroupValue.Visible:= True;
    selGroup.Visible:= True;
    PanelMember.Left:= TabSet1.Left + TabSet1.Width + 1;
    PanelMember.Width:= 416;
    rbAutoRing.Down:= False;
    rbAutoSearch.Down:= False;
    DQMemberAfterScroll(DQMember);
  end else begin
    spMember.Visible := False;
    selGroup.Visible:= False;
    EGroupValue.Visible:= False;
    btnChoiceGroup.Visible:= False;
    btnAutoGroup.Visible:= False;
    btnRemoveGroup.Visible:= False;
  end;
  plMemberHint.Visible:= PanelMember.Visible;
end;

procedure TFCotePigeon.DQMemberAfterScroll(DataSet: TDataSet);
begin
  if DQMember.EOF then Exit;
  with DQPigeon do begin
    Close;
    Sql.Text:= FPigeonSQL + ' AND MemberIdx=''' +
      DQMember.FieldByName('ID').AsString + ''' ORDER BY DisplayOrder';
    Open;
  end;
  selGroupChange(nil);
end;

procedure TFCotePigeon.TabSet1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  PanelMember.Visible := not PanelMember.Visible;
  DisplayMember(PanelMember.Visible);
end;

procedure TFCotePigeon.btnMemberAddClick(Sender: TObject);
begin
  DQMember.AfterScroll:= nil;
  TFDlgMember.InputDlg(DQMember, true);
  DQMember.AfterScroll:= DQMemberAfterScroll;
  DQMemberAfterScroll(DQMember);
end;

procedure TFCotePigeon.btnMemberDelClick(Sender: TObject);
begin
  if DQPigeon.RecordCount <> 0 then
    raise Exception.Create(RES_MEMBER_HAS_PIGEON);
  if MessageDlg(RES_DLG_DEL_PROMPT, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  if MessageDlg(RES_DLG_DEL_WARNING, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  DQMember.Edit;
  DQMember.FieldByName('MemberName').AsString:= '__' + RandomStr(18, true, true, true, false);
  DQMember.FieldByName('MemberNo').AsString:= '__' + RandomStr(36, true, true, true, false);
  DQMember.FieldByName('Status').AsString:= '删除';
  DQMember.FieldByName('SynFlag').AsString:= 'U';
  DQMember.Post;
end;

procedure TFCotePigeon.btnMemberEditClick(Sender: TObject);
begin
  TFDlgMember.InputDlg(DQMember, False);
end;

{ 摘环、带环、过户 }
//准备过户
procedure TFCotePigeon.btnCheckInClick(Sender: TObject);
var
  i: Integer;
  S: string;
begin
  if FramePigeonGrid.Grid.SelectedRows.Count > 0 then
  begin
    FTransferList.Clear;
    with DQPigeon do
      for i := 0 to FramePigeonGrid.Grid.SelectedRows.Count - 1 do
      begin
        GotoBookmark(FramePigeonGrid.Grid.SelectedRows.Items[i]);
        FTransferList.Add(FieldByName('GBCode').AsString + '=' +
          FieldByName('ID').AsString);
        S:= S + #13#10 + FieldByName('GBCode').AsString;
      end;
    btnTranfer.Enabled := MessageDlg('以下信鸽准备过户' + S,
      mtInformation, mbYesNo, 0) = mrYes;
    if btnTranfer.Enabled then
      DlgInfo('提示', '现在需要选择要过户到的鸽主信息，然后再点击下面的“过到此户”按钮，完成过户操作。');
  end;
end;
//摘环
procedure TFCotePigeon.btnTakeOffClick(Sender: TObject);
begin
  if DQPigeon.FieldByName('JZCode').AsLargeInt = 0 then
    raise Exception.Create('尚未带环无法摘环！');
  if DlgInfo('警告', '是否真的将' + DQPigeon.FieldByName('GBCode').AsString +
    '电子环摘除？', mbYesNo, 20) <> mrYes then Exit;
  with DQPigeon do begin
    Edit;
    DQPigeon.FieldByName('JZCode').AsLargeInt:= 0;
    DQPigeon.FieldByName('FactoryCode').AsLargeInt:= 0;
    DQPigeon.FieldByName('Modifiedate').AsDateTime:= Now;
    Post;
  end;
  ESGBCode.SetFocus;
end;
//带环
procedure TFCotePigeon.btnTakeOnClick(Sender: TObject);
begin
  if not EElectricCode.Visible then
    raise Exception.Create('尚未扫环无法带环！');
  if EElectricCode.Text = '' then
    raise Exception.Create('尚未扫环无法带环！');
  if FCurrentJZCode = 0 then
    raise Exception.Create('尚未扫环无法带环！');
  if DQPigeon.FieldByName('JZCode').AsLargeInt > 0 then
    raise Exception.Create('该赛鸽已经带环不允许再带环！');
  if CheckDoublication(DQPigeon.FieldByName('ID').AsGuid)then
    raise Exception.Create('该电子环已经入棚，无法再入棚。');
//  if DlgInfo('警告', '是否真的将该电子环佩戴给' + DQPigeon.FieldByName('GBCode').AsString +
//    '？', mbYesNo, 20) <> mrYes then Exit;
  with DQPigeon do begin
    Edit;
    DQPigeon.FieldByName('JZCode').AsLargeInt:= FCurrentJZCode;
    DQPigeon.FieldByName('FactoryCode').AsLargeInt:= FCurrentFactoryCode;
    DQPigeon.FieldByName('Modifiedate').AsDateTime:= Now;
    Post;
  end;
  DeleteECode;
  DlgInfo('提示', '带环完成！', 10);
  ESGBCode.SelectAll;
  ESGBCode.SetFocus;
end;

{ Group }

procedure TFCotePigeon.btnAutoGroupClick(Sender: TObject);
var
  S: string;
  KID, CNT, SUM: Integer;
begin     //自动分组
  if DlgInfo('提示', '是否确认系统自动分组？', mbYesNo) <> mrYes then Exit;
  KID:= 0;
  CNT:= FCoteDB.GetMemberGroupValue(selGroup.ItemIndex, DQMember.FieldByName('ID').AsGuid, KID);
  if KID >= EGroupValue.Items.Count then raise Exception.Create('可用分组租已经满，无法进行自动分组');
  SUM:= 0;
  with DQPigeon do begin
    First;
    while not EOF do begin
      if FieldByName('Group' + IntToStr(selGroup.ItemIndex + 1)).AsString = '' then begin
        Edit;
        FieldByName('Group' + IntToStr(selGroup.ItemIndex + 1)).AsString:= FCoteDB.FGroupKeys[selGroup.ItemIndex, KID];
        FieldByName('SynFlag').AsString:= 'U';
        Post;
        Inc(CNT); Inc(SUM);
        if(FCoteDB.FGroupCount[selGroup.ItemIndex] <> 0)and(CNT >= FCoteDB.FGroupCount[selGroup.ItemIndex])then begin
          Inc(KID);
          CNT:= FCoteDB.GetMemberGroupValue(selGroup.ItemIndex, DQMember.FieldByName('ID').AsGuid, KID);
          if KID >= EGroupValue.Items.Count then begin
            if SUM > 0 then begin
              DlgInfo('提示', '已完成' + IntToStr(sum) + '只赛鸽自动分组！');
              Exit;
            end else raise Exception.Create('可用分组租已经满，无法进行自动分组');
          end;
        end;
      end;
      Next;
    end;
  end;
end;

procedure TFCotePigeon.btnChoiceGroupClick(Sender: TObject);
var
  S: string;
  I, KID, CNT: Integer;
begin     //选定分组
  if FramePigeonGrid.Grid.SelectedRows.Count = 0 then
    raise Exception.Create(RES_NO_GET_MEMBER);
  KID:= EGroupValue.ItemIndex;
  with DQPigeon do
    for i := 0 to FramePigeonGrid.Grid.SelectedRows.Count - 1 do
    begin
      GotoBookmark(FramePigeonGrid.Grid.SelectedRows.Items[i]);
      CNT:= FCoteDB.GetMemberGroupValue(selGroup.ItemIndex, DQMember.FieldByName('ID').AsGuid, KID);
      if KID <> EGroupValue.ItemIndex then raise Exception.Create('该分组已经满，不能再分入新的赛鸽了！');
      with DQPigeon do begin
        Edit;
        FieldByName('Group' + IntToStr(selGroup.ItemIndex + 1)).AsString:= EGroupValue.Text;
        FieldByName('SynFlag').AsString:= 'U';
        Post;
      end;
    end;
end;

procedure TFCotePigeon.btnRemoveGroupClick(Sender: TObject);
var
  I: Integer;
begin     //取消分组
  if FramePigeonGrid.Grid.SelectedRows.Count = 0 then
    raise Exception.Create(RES_NO_GET_MEMBER);
  with DQPigeon do
    for i := 0 to FramePigeonGrid.Grid.SelectedRows.Count - 1 do
    begin
      GotoBookmark(FramePigeonGrid.Grid.SelectedRows.Items[i]);
      with DQPigeon do begin
        Edit;
        FieldByName('Group' + IntToStr(selGroup.ItemIndex + 1)).AsString:= '';
        FieldByName('SynFlag').AsString:= 'U';
        Post;
      end;
    end;
end;

{ TFPigeon.Pigeon }

function TFCotePigeon.CheckDoublication(PID: TGuid): Boolean;
  function CheckRing(Q: TADOQuery; checkID: Int64): Boolean;
  begin
    with Q do begin
      Parameters[0].Value:= checkID;
      Open;
      if RecordCount > 0 then begin
        if RecordCount > 1 then Result:= True
        else Result:= not GuidComp(PID, FieldByName('ID').AsGuid);
      end else Result:= False;
      Close;
    end;
  end;
begin
  if FCoteDB.RingKind = '只读环' then Result:= CheckRing(DQCHECK_R, FCurrentJZCode)
  else if FCoteDB.RingKind = '读写环' then Result:= CheckRing(DQCHECK_W, FCurrentFactoryCode)
  else if FCoteDB.RingKind = '高频只读' then Result:= CheckRing(DQCHECK_R, FCurrentJZCode)
  else if FCoteDB.RingKind = '高频读写' then Result:= CheckRing(DQCHECK_W, FCurrentFactoryCode);
end;

procedure TFCotePigeon.btnPigeonAddClick(Sender: TObject);
begin
  if DQMember.RecordCount < 1 then raise Exception.Create('没有办法添加没有鸽主的赛鸽');
  if(FCurrentJZCode > 0) and CheckDoublication(TGuid.Empty)then
    raise Exception.Create('该电子环已经入棚，无法再入棚。');
  TFDlgPigeon.AppendDlg(DQPigeon, DQMember.FieldByName('MemberNo').AsString,
    DQMember.FieldByName('ID').AsGuid, FCurrentJZCode, FCurrentFactoryCode);
  RefrenshCount;
  WriteLog(LOG_MAJOR, 'Pigeon', Format('Add a Pigeon %s.', [DQPigeon.FieldByName('GBCode').AsString]));
  DeleteECode;
end;

procedure TFCotePigeon.btnPigeonEditClick(Sender: TObject);
begin
  if DQPigeon.RecordCount < 1 then raise Exception.Create('没有选择要修改的赛鸽');
  if(FCurrentJZCode > 0) and CheckDoublication(DQPigeon.FieldByName('ID').AsGuid)then
    raise Exception.Create('该电子环已经入棚，无法再入棚。');
  if Assigned(FComm) then
    FCoteDB.CloseComm(FComm);
  TFDlgPigeon.UpdateDlg(DQPigeon);
  ChkAutoGetECodeClick(nil);
end;

procedure TFCotePigeon.btnPigeonDelClick(Sender: TObject);
var
  S: string;
begin
  if DQPigeon.FieldByName('JZCode').AsLargeInt > 0 then
    raise Exception.Create('请先摘环后，再删除！');
  if MessageDlg(RES_DLG_DEL_PROMPT, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  if MessageDlg(RES_DLG_DEL_WARNING, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  FCurrentFactoryCode := 0;
  FCurrentJZCode := 0;
  S:= DQPigeon.FieldByName('GBCode').AsString;
  DQPigeon.Edit;
  DQPigeon.FieldByName('GBCode').AsString:= '';
  DQPigeon.FieldByName('JZCode').AsInteger:= 0;
  DQPigeon.FieldByName('Status').AsString:= '删除';
  DQPigeon.FieldByName('SynFlag').AsString:= 'U';
  DQPigeon.Post;
  RefrenshCount;
  WriteLog(LOG_MAJOR, 'Pigeon', Format('Delete a Pigeon %s.', [S]))
end;

procedure TFCotePigeon.btnVideoClick(Sender: TObject);
begin
  DlgCameraOpen();
  FDlgCamera.IniFilename := FCoteDB.ConfigFilename;
end;

procedure TFCotePigeon.RefrenshCount;
begin
//  if DQPigeon.Active then
//    FramePigeonGrid.StatusBar1.Panels[1].Text:= Format(RES_DISPLAY_COUNT,
//      [FAllCount, DQPigeon.RecordCount, FNewCount]);
end;

procedure TFCotePigeon.selGroupChange(Sender: TObject);
var
  I: Integer;
begin
  EGroupValue.Items.Clear;
  if selGroup.ItemIndex < 0 then Exit;
  for I := 0 to FCoteDB.GetMemberMaxGroupKeyId(selGroup.ItemIndex, DQMember.FieldByName('ID').AsGuid) - 1 do
    EGroupValue.Items.Add(FCoteDB.FGroupKeys[selGroup.ItemIndex, I]);
  EGroupValue.ItemIndex:= 0;
end;

procedure TFCotePigeon.btnPigeonSearchClick(Sender: TObject);
begin // 查询
  DisplayMember(False);
  if EElectricCode.Text <> '' then
    with DQPigeon do begin
      Close;
      if FCoteDB.RingKind = '只读环' then Sql.Text:= FPigeonSql + ' AND JZCode=' + EElectricCode.Text
      else if FCoteDB.RingKind = '读写环' then Sql.Text:= FPigeonSql + ' AND FactoryCode=' + IntToStr(FCurrentFactoryCode)
      else if FCoteDB.RingKind = '高频只读' then Sql.Text:= FPigeonSql + ' AND JZCode=' + EElectricCode.Text
      else if FCoteDB.RingKind = '高频读写' then Sql.Text:= FPigeonSql + ' AND FactoryCode=' + IntToStr(FCurrentFactoryCode)
      else if FCoteDB.RingKind = '凯信只读' then Sql.Text:= FPigeonSql + ' AND JZCode=' + EElectricCode.Text;
      Open;
    end
  else begin
    DQPigeon.SQL.Text := FPigeonSQL;
    TFDlgpigeon.SearchDlg(DQPigeon, TGuid.Empty, 0);
  end;
  DeleteECode;
end;

procedure TFCotePigeon.btnTranferClick(Sender: TObject);
var
  i: Integer;
begin // btnTranfer
  if FTransferList.Count <= 0 then
    Exit;
  for i := 0 to FTransferList.Count - 1 do
    with DQPigeonByID do
      try
        Parameters[0].Value := FTransferList.ValueFromIndex[i];
        Open;
        if not FPigeonInfo.LoadFromDB(DQPigeonByID) then
        begin
          DlgInfo('错误', LastInfo);
          Continue;
        end;
        FPigeonInfo.MemberIdx := DQMember.FieldByName('ID').AsGuid;
        FPigeonInfo.UpdateToDB(DQPigeonByID);
      finally
        Close;
      end;
  btnTranfer.Enabled := False;
  DQPigeon.Close;
  DQPigeon.Open;
  DlgInfo('提示', '过户成功！');
end;

{ Pigeon.Grid }

procedure TFCotePigeon.ECommChange(Sender: TObject);
begin
  ChkAutoGetECode.Checked := False;
  ChkAutoGetECodeClick(nil);
end;

procedure TFCotePigeon.EMemberName1Change(Sender: TObject);
begin
  btnMemberSearchClick(nil);
end;

procedure TFCotePigeon.ChkAutoGetECodeClick(Sender: TObject);
begin
  try
    if ChkAutoGetECode.Checked then
    begin
      FComm:= FCoteDB.CreateComm(EventOfReceived, nil, nil, True);
      //FCoteDB.Comm.CheckRing := true;
      FComm.Open(EComm.ItemIndex + 1, StrToInt(ECommRate.Text), 'n', 8, 1);
      WriteAppConfig('COMMUNICATION', 'Port', EComm.Text);
      WriteAppConfig('COMMUNICATION', 'Speed', ECommRate.Text);
    end else begin
      FCoteDB.CloseComm(FComm);
      rbAutoSearch.Down:= False;
      rbAutoJoin.Down:= False;
      rbAutoRing.Down:= False;
      DeleteECode;
    end;
  except
    ChkAutoGetECode.Checked := not ChkAutoGetECode.Checked;
  end;
  EComm.Enabled := not ChkAutoGetECode.Checked;
  ECommRate.Enabled := not ChkAutoGetECode.Checked;
  //lbElectricCode.Enabled := not ChkAutoGetECode.Checked;
  EElectricCode.Enabled := not ChkAutoGetECode.Checked;
  EElectricCode.Text := '';
end;

procedure TFCotePigeon.rbAutoFindClick(Sender: TObject);
begin
  rbAutoJoin.Down:= False;
  rbAutoSearch.Down:= False;
  rbAutoRing.Down:= False;
  ChkAutoGetECode.Checked:= true;
  DisplayMember(False);         //关闭鸽主部分
end;

procedure TFCotePigeon.rbAutoJoinClick(Sender: TObject);
begin
  rbAutoFind.Down:= false;
  rbAutoRing.Down:= False;
  rbAutoSearch.Down:= False;
  ChkAutoGetECode.Checked:= true;
  DisplayMember(True);          //打开鸽主部分
end;

procedure TFCotePigeon.rbAutoRingClick(Sender: TObject);
begin
  rbAutoJoin.Down:= False;
  rbAutoFind.Down:= false;
  rbAutoSearch.Down:= False;
  rbAutoRing.Down:= not rbAutoRing.Down;
  ChkAutoGetECode.Checked:= true;
  DisplayMember(False);         //关闭鸽主部分
end;

procedure TFCotePigeon.rbAutoSearchClick(Sender: TObject);
begin
  rbAutoJoin.Down:= False;
  rbAutoRing.Down:= False;
  rbAutoFind.Down:= false;
  ChkAutoGetECode.Checked:= true;
  DisplayMember(False);         //关闭鸽主部分
  if not rbAutoSearch.Down then DeleteECode;
end;

procedure TFCotePigeon.DeleteECode;
begin
  EElectricCode.Text:= '';
  FCurrentFactoryCode:= 0;
  FCurrentJZCode:= 0;
end;

{ Communication }

procedure TFCotePigeon.EventOfReceived(JZCode: Int64; GameCode: Cardinal;
  FactoryCode: Int64);
begin
  if FactoryCode = 0 then Exit;
  if JZCode = 0 then
    EElectricCode.Text := NOJUNZHUORING
  else if JZCode <> FCurrentJZCode then begin
    WriteLog(LOG_DEBUG, 'CotePigeon', Format('CurrentJZCode=%x JZCode=%x FCode=#x', [FCurrentJZCode, JZCode, FactoryCode]));
    FCurrentJZCode := JZCode;
    EElectricCode.Text := IntToStr(JZCode);
    FCurrentFactoryCode := FactoryCode;
    if rbAutoJoin.Down then begin
      if CheckDoublication(TGUID.Empty) then
        DlgInfo('不允许自动入棚', '该电子环已经入棚。', 6)
      else btnPigeonAddClick(nil);
    end;
    if rbAutoRing.Down then begin
      DQPigeon.SQL.Text := FPigeonSQL;
      TFDlgpigeon.SearchDlg(DQPigeon, TGuid.Empty, 0);
    end;
    if rbAutoSearch.Down then
      btnPigeonSearchClick(nil);
    WavBeep;
  end;
end;

end.
