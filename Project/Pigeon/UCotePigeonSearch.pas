unit UCotePigeonSearch;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, ComCtrls, UFrameGridBar, DB, ADODB,
  UCoteComm, UDlgReadRing, Menus;

type
  TFCotePigeonSearch = class(TForm)
    DQPigeon: TADOQuery;
    DQZone: TADOQuery;
    FramePigeonGrid: TFrameGridBar;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel2: TPanel;
    btnPigeonSearch: TBitBtn;
    btnPigeonReset: TBitBtn;
    Panel3: TPanel;
    Panel1: TPanel;
    Label4: TLabel;
    ECity1: TComboBox;
    Panel5: TPanel;
    Label3: TLabel;
    EState1: TComboBox;
    Panel6: TPanel;
    Label6: TLabel;
    ECounty1: TComboBox;
    Panel4: TPanel;
    Label5: TLabel;
    EMemberShortName: TEdit;
    rgRing: TRadioGroup;
    EGender1: TRadioGroup;
    Panel9: TPanel;
    LPigeonNO: TLabel;
    EPigeonNO1: TEdit;
    LGBCode: TLabel;
    EGBCode1: TEdit;
    LFeather: TLabel;
    EFeather1: TComboBox;
    LEye: TLabel;
    EEye1: TComboBox;
    LStatus: TLabel;
    EStatus1: TComboBox;
    Label1: TLabel;
    EMemberName: TEdit;
    Label2: TLabel;
    EMemberNo: TEdit;
    Panel10: TPanel;
    Panel11: TPanel;
    chkModifiedate: TCheckBox;
    dtModifiedate1: TDateTimePicker;
    dtModifiedate2: TDateTimePicker;
    chkJoinDate: TCheckBox;
    dtJoinDate2: TDateTimePicker;
    dtJoinDate1: TDateTimePicker;
    Panel12: TPanel;
    chkGroup1: TCheckBox;
    ESGroup1: TEdit;
    chkGroup3: TCheckBox;
    ESGroup2: TEdit;
    chkGroup4: TCheckBox;
    ESGroup3: TEdit;
    chkGroup2: TCheckBox;
    ESGroup4: TEdit;
    chkGroup5: TCheckBox;
    ESGroup5: TEdit;
    LExtend1: TLabel;
    ESExtend1: TComboBox;
    LExtend2: TLabel;
    ESExtend2: TComboBox;
    LExtend3: TLabel;
    ESExtend3: TComboBox;
    LExtend4: TLabel;
    ESExtend4: TComboBox;
    LExtend5: TLabel;
    ESExtend5: TComboBox;
    chkFlag: TCheckBox;
    ChkAutoGetECode: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure btnPigeonSearchClick(Sender: TObject);
    procedure btnPigeonResetClick(Sender: TObject);
    procedure EState1Change(Sender: TObject);
    procedure ECity1Change(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ChkAutoGetECodeClick(Sender: TObject);
  private
    { Private declarations }
    FComm: TJZCommBase;
    FPigeonSQL: string;
    FFactoryCode: Int64;
    FJZCode: Int64;
    procedure EventOfEventOfReceived(JZCode: Int64; GameCode: Cardinal;
      FactoryCode: Int64);
  public
    { Public declarations }
  end;

var
  FCotePigeonSearch: TFCotePigeonSearch;

implementation

uses UCoteDB;

{$R *.dfm}

procedure TFCotePigeonSearch.btnPigeonResetClick(Sender: TObject);
begin // 设置查询条件为空
  EPigeonNO1.Text := '';
  EGBCode1.Text := '';
  ESGroup1.Text := '';
  ESGroup2.Text := '';
  ESGroup3.Text := '';
  ESGroup4.Text := '';
  ESGroup5.Text := '';
  EGender1.ItemIndex := 0;
  EFeather1.Text := '';
  EEye1.Text := '';
  EMemberName.Text := '';
  EMemberNo.Text := '';
  EMemberShortName.Text := '';
  ECounty1.ItemIndex := -1;
  ECity1.ItemIndex := -1;
  EState1.ItemIndex := -1;
  ESExtend1.ItemIndex := 0;
  ESExtend2.ItemIndex := 0;
  ESExtend3.ItemIndex := 0;
  ESExtend4.ItemIndex := 0;
  ESExtend5.ItemIndex := 0;
  dtModifiedate1.Date := Now - 200;
  dtModifiedate2.Date := Now;
  chkModifiedate.Checked := False;
  EStatus1.ItemIndex := 0;
  dtJoinDate1.Date := Now - 200;
  dtJoinDate2.Date := Now;
  chkJoinDate.Checked := False;
  ChkAutoGetECode.Checked:= False;
  ChkAutoGetECodeClick(nil);
  FFactoryCode:= 0;
  FJZCode:= 0;
end;

procedure TFCotePigeonSearch.btnPigeonSearchClick(Sender: TObject);
var
  Idx: Integer;
begin // 查询
  with DQPigeon.SQL do
  begin
    DQPigeon.Close;
    Text := FPigeonSQL;
    Idx := Count;
    if FJZCode > 0 then
      Add('AND JZCode = ' + IntToStr(FJZCode))
    else begin
      {$region "其他查询条件加载"}
      if EPigeonNO1.Text <> '' then
        Add('AND PigeonNO LIKE ''%' + EPigeonNO1.Text + '%''');
      if EState1.ItemIndex > 0 then
        Add('AND State = ''' + EState1.Text + '''');
      if ECity1.ItemIndex > 0 then
        Add('AND City LIKE ''' + ECity1.Text + '''');
      if ECounty1.ItemIndex > 0 then
        Add('AND County LIKE ''' + ECounty1.Text + '''');
      if EGBCode1.Text <> '' then
        Add('AND GBCode LIKE ''%' + EGBCode1.Text + '%''');
      if EMemberName.Text <> '' then
        Add('AND MemberName LIKE ''%' + EMemberName.Text + '%''');
      if EMemberNo.Text <> '' then
        Add('AND MemberNo LIKE ''%' + EMemberNo.Text + '%''');
      if EGBCode1.Text <> '' then
        Add('AND MemberNo LIKE ''%' + EMemberNo.Text + '%''');
      if EGBCode1.Text <> '' then
        Add('AND ShortName LIKE ''%' + EMemberShortName.Text + '%''');
      if chkGroup1.Checked then
        Add('AND Group1 = ''' + ESGroup1.Text + '''');
      if chkGroup2.Checked then
        Add('AND Group2 = ''' + ESGroup2.Text + '''');
      if chkGroup3.Checked then
        Add('AND Group3 = ''' + ESGroup3.Text + '''');
      if chkGroup4.Checked then
        Add('AND Group4 = ''' + ESGroup4.Text + '''');
      if chkGroup5.Checked then
        Add('AND Group5 = ''' + ESGroup5.Text + '''');
      if chkFlag.Checked then
        Add('AND Flag<>''''');
      case EGender1.ItemIndex of
        1:
          Add('AND Gender = ''公''');
        2:
          Add('AND Gender = ''母''');
      end;
      case rgRing.ItemIndex of
        1:
          Add('AND JZCode = 0');
        2:
          Add('AND JZCode <> 0');
      end;
      if EFeather1.ItemIndex > 0 then
        Add('AND Feather = ''' + EFeather1.Text + '''');
      if EEye1.ItemIndex > 0 then
        Add('AND Eye = ''' + EEye1.Text + '''');
      if ESExtend1.ItemIndex > 0 then
        Add('AND Assist1 = ''' + ESExtend1.Text + '''');
      if ESExtend2.ItemIndex > 0 then
        Add('AND Assist2 = ''' + ESExtend2.Text + '''');
      if ESExtend3.ItemIndex > 0 then
        Add('AND Assist3 = ''' + ESExtend3.Text + '''');
      if ESExtend4.ItemIndex > 0 then
        Add('AND Assist4 = ''' + ESExtend4.Text + '''');
      if ESExtend5.ItemIndex > 0 then
        Add('AND Assist5 = ''' + ESExtend5.Text + '''');
      if chkModifiedate.Checked then
      begin
        Add('AND P.Modifiedate >= ''' + FormatDateTime('yyyy-mm-dd',
          dtModifiedate1.Date) + '''');
        Add('AND P.Modifiedate <= ''' + FormatDateTime('yyyy-mm-dd',
          dtModifiedate2.Date) + '''');
      end;
      if EStatus1.ItemIndex > 0 then
      begin
        Add('AND P.Status LIKE ''%' + EStatus1.Text + '%''');
      end;
      if chkJoinDate.Checked then
      begin
        Add('AND JoinDate >= ''' + FormatDateTime('yyyy-mm-dd',
          dtJoinDate1.Date) + '''');
        Add('AND JoinDate <= ''' + FormatDateTime('yyyy-mm-dd',
          dtJoinDate2.Date) + '''');
      end;
      {$endregion}
    end;
    DQPigeon.Open;
  end;
end;

procedure TFCotePigeonSearch.ChkAutoGetECodeClick(Sender: TObject);
begin
  if ChkAutoGetECode.Checked then
    with TFDlgReadRing.Create(nil) do try
      FComm:= OpenComm(EventOfEventOfReceived, nil, nil);
      ChkAutoGetECode.Checked:= Assigned(FComm);
    finally
      Free;
    end
  else begin
    FCoteDB.CloseComm(FComm);
    FFactoryCode:= 0;
    FJZCode:= 0;
  end;
end;

procedure TFCotePigeonSearch.ECity1Change(Sender: TObject);
begin
  if ECity1.ItemIndex = 0 then
    Exit;
  with DQZone do
  begin
    Parameters[0].Value := Integer(ECity1.Items.Objects[ECity1.ItemIndex]);
    Open;
    ECounty1.Items.Clear;
    ECounty1.Items.AddObject('无', nil);
    while not EOF do
    begin
      ECounty1.Items.AddObject(FieldByName('ItemName').AsString,
        TObject(FieldByName('ID').AsInteger));
      Next;
    end;
    Close;
  end;
  ECounty1.ItemIndex := 0;
end;

procedure TFCotePigeonSearch.EState1Change(Sender: TObject);
begin
  if EState1.ItemIndex = 0 then
    Exit;
  with DQZone do
  begin
    Parameters[0].Value := Integer(EState1.Items.Objects[EState1.ItemIndex]);
    Open;
    ECity1.Items.Clear;
    ECity1.Items.AddObject('无', nil);
    while not EOF do
    begin
      ECity1.Items.AddObject(FieldByName('ItemName').AsString,
        TObject(FieldByName('ID').AsInteger));
      Next;
    end;
    Close;
  end;
  ECity1.ItemIndex := 0;
end;

procedure TFCotePigeonSearch.EventOfEventOfReceived(JZCode: Int64;
  GameCode: Cardinal; FactoryCode: Int64);
begin
  FJZCode:= JZCode;
  FFactoryCode:= FactoryCode;
  btnPigeonSearchClick(nil);
end;

procedure TFCotePigeonSearch.FormCreate(Sender: TObject);
var
  szList: TStringList;

  procedure InitPigeonGroupField(grpName: string; Lb1: TCheckBox; Co1: TEdit);
  begin
    Lb1.Caption := grpName;
    Lb1.Visible := Lb1.Caption <> '';
    Co1.Visible := Lb1.Visible;
  end;

  procedure InitPigeonExtendField(ExName: string; Lb1: TLabel; Co1: TComboBox);
  begin
    Lb1.Caption := FCoteDB.GetListOption(ExName, Co1.Items);
    Lb1.Visible := Lb1.Caption <> '';
    Co1.Visible := Lb1.Visible;
    if Lb1.Visible then
    begin
      Co1.Items.Insert(0, '无');
      Co1.ItemIndex := 0;
    end;
  end;

begin
  FPigeonSQL := DQPigeon.SQL.Text;
  FramePigeonGrid.LoadTitles('CotePigeonSearch');
{$REGION 'Init pigeon environment'}
  InitPigeonExtendField('羽色', LFeather, EFeather1);
  InitPigeonExtendField('眼砂', LEye, EEye1);
  InitPigeonExtendField('Extend1', LExtend1, ESExtend1);
  InitPigeonExtendField('Extend2', LExtend2, ESExtend2);
  InitPigeonExtendField('Extend3', LExtend3, ESExtend3);
  InitPigeonExtendField('Extend4', LExtend4, ESExtend4);
  InitPigeonExtendField('Extend5', LExtend5, ESExtend5);
  InitPigeonExtendField('信鸽状态', LStatus, EStatus1);
  szList := TStringList.Create;
  FCoteDB.GetListOption('分组', szList);
  if szList.Count > 0 then
    InitPigeonGroupField(szList[0], chkGroup1, ESGroup1);
  if szList.Count > 1 then
    InitPigeonGroupField(szList[1], chkGroup2, ESGroup2);
  if szList.Count > 2 then
    InitPigeonGroupField(szList[2], chkGroup3, ESGroup3);
  if szList.Count > 3 then
    InitPigeonGroupField(szList[3], chkGroup4, ESGroup4);
  if szList.Count > 4 then
    InitPigeonGroupField(szList[4], chkGroup5, ESGroup5);
  dtModifiedate1.DateTime := Now - 6 * 30;
  dtJoinDate1.DateTime := Now - 6 * 30;
  dtModifiedate2.DateTime := Now - 3 * 30;
  dtJoinDate2.DateTime := Now;
  with DQZone do
  begin
    Parameters[0].Value := 0;
    Open;
    EState1.Items.Clear;
    EState1.Items.AddObject('无', nil);
    while not EOF do
    begin
      EState1.Items.AddObject(FieldByName('ItemName').AsString,
        TObject(FieldByName('ID').AsInteger));
      Next;
    end;
    Close;
  end;
  EState1.ItemIndex := 0;
{$ENDREGION}
end;

procedure TFCotePigeonSearch.FormDestroy(Sender: TObject);
begin
  if Assigned(FComm) then
    FCoteDB.CloseComm(FComm);
end;

end.
