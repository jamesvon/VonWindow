unit UCoteRecharge;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.OleCtrls, SHDocVw, Vcl.ExtCtrls,
  Vcl.StdCtrls, Vcl.Samples.Spin, WinApi.ActiveX, ShellAPI, Vcl.OleCtnrs;

type
  TFCoteRecharge = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    ECount: TSpinEdit;
    EKind: TRadioGroup;
    btnAliPay: TImage;
    btnWxPay: TImage;
    WebBrowser1: TWebBrowser;
    procedure btnWxPayClick(Sender: TObject);
    procedure btnAliPayClick(Sender: TObject);
  private
    { Private declarations }
    procedure DisplayUrl(Url: string);
  public
    { Public declarations }
  end;

var
  FCoteRecharge: TFCoteRecharge;

implementation

uses UCoteDB, HttpApp;

{$R *.dfm}

procedure TFCoteRecharge.btnAliPayClick(Sender: TObject);
var
  AliPay: string;
begin
  //public async Task<string> AliRechargeChooseCount(string CoteIdx, int Count)
  case EKind.ItemIndex of
  0: AliPay:= FCoteDB.GetWebData('/Join/Admin/AliRechargeChooseCount?CoteIdx=' +
    Copy(FCoteDB.FCoteInfo.ID.ToString(), 2, 36) + '&Count=' + IntToStr(ECount.Value));
  1: AliPay:= FCoteDB.GetWebData('/Join/Admin/AliRechargeSMSCount?CoteIdx=' +
    Copy(FCoteDB.FCoteInfo.ID.ToString(), 2, 36) + '&Count=' + IntToStr(ECount.Value));
  end;
  DisplayUrl(AliPay);
end;

procedure TFCoteRecharge.btnWxPayClick(Sender: TObject);
var
  WxPay: string;
  html: TStringStream;
begin
  //public async Task<string> WxRechargeChooseCount(string CoteIdx, int Count)
  case EKind.ItemIndex of
  0: WxPay:= FCoteDB.GetWebData('/Join/Admin/WxRechargeChooseCount?CoteIdx=' +
    Copy(FCoteDB.FCoteInfo.ID.ToString(), 2, 36) + '&Count=' + IntToStr(ECount.Value));
  1: WxPay:= FCoteDB.GetWebData('/Join/Admin/WxRechargeSMSCount?CoteIdx=' +
    Copy(FCoteDB.FCoteInfo.ID.ToString(), 2, 36) + '&Count=' + IntToStr(ECount.Value));
  end;
  DisplayUrl(WxPay);
end;

function urlencode(Aurl: string): string;
var
  i: integer;
  stmp,Tstmp: string;
begin
  result:=Aurl;
  if length(Aurl) > 0 then
  begin
    for i := 1 to length(Aurl) do
    begin
      if Integer(Ord(Aurl[i])) >255 then
        begin
        stmp := copy(Aurl, i, 1);
        Tstmp:=HttpEncode(stmp);
        result:=stringreplace(result,stmp,Tstmp,[]);
        end;
    end;
  end;
end;

procedure TFCoteRecharge.DisplayUrl(Url: string);
var
  html: TStringStream;
  S: String;
begin
//  OleContainer1.CreateObject('InternetExplorer.Application', false);
//  Olecontainer1.DoVerb(ovPrimary);
//  OleContainer1.OleObject.navigate(FCoteDB.Cert.Values.NameValue['WebUrl'] + Url);
//
//  ShellExecute(Application.Handle, nil, PChar(FCoteDB.Cert.Values.NameValue['WebUrl'] + Url), nil, nil, SW_SHOWNORMAL);
//  S:= FCoteDB.Cert.Values.NameValue['WebUrl'] + Url;
//  WinExec(PAnsichar(S), SW_SHOW);
  S:= FCoteDB.Cert.Values.NameValue['WebUrl'] + Url;
  WebBrowser1.Navigate2(urlencode(S));
//  sleep(2000);
//  html:= TStringStream.Create;
//  html.WriteString('<!DOCTYPE html><html><head><meta charset="utf-8"></head><body><img src="' +
//    FCoteDB.Cert.Values.NameValue['WebUrl'] + Url + '" /></body></html>');
//  html.Position:= 0;
//  WebBrowser1.Navigate('about:blank');
//  (WebBrowser1.Document as IPersistStreamInit).Load(TStreamadapter.Create(html)); // �ؼ����
//  html.Free;
end;

end.
