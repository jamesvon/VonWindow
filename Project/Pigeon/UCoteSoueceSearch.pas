unit UCoteSoueceSearch;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrameGridBar, ExtCtrls, ImgList, DB, ADODB, StdCtrls, Buttons,
  ComCtrls, UCoteComm, Menus, System.ImageList, UVonLog, Printers;

type
  TFCoteSoueceSearch = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    EFromDate: TDateTimePicker;
    EToDate: TDateTimePicker;
    BtnReset: TBitBtn;
    BtnSearch: TBitBtn;
    EGameName: TComboBox;
    DQStatistic: TADOQuery;
    ImageList1: TImageList;
    Timer1: TTimer;
    DSGames: TDataSource;
    DQGames: TADOQuery;
    FrameGridBar1: TFrameGridBar;
    EMemberName: TEdit;
    Label3: TLabel;
    EMemberID: TEdit;
    Label4: TLabel;
    EGBCode: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    rbClose: TRadioButton;
    rbCollect: TRadioButton;
    rbChickIn: TRadioButton;
    Panel1: TPanel;
    chkAutoPrint: TCheckBox;
    btnPrinterSetting: TBitBtn;
    btnSinglePrint: TBitBtn;
    btnMulitPrint: TBitBtn;
    PrinterSetupDialog1: TPrinterSetupDialog;
    btnSingleAuction: TBitBtn;
    btnMultiAuction: TBitBtn;
    chkAutoPrintAuction: TCheckBox;
    procedure BtnResetClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnSearchClick(Sender: TObject);
    procedure FrameGridBar1menuConditionClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure rbCloseClick(Sender: TObject);
    procedure rbCollectClick(Sender: TObject);
    procedure rbChickInClick(Sender: TObject);
    procedure btnPrinterSettingClick(Sender: TObject);
    procedure btnSinglePrintClick(Sender: TObject);
    procedure btnSingleAuctionClick(Sender: TObject);
    procedure btnMultiAuctionClick(Sender: TObject);
    procedure btnMulitPrintClick(Sender: TObject);
  private
    { Private declarations }
    FComm: TJZCommBase;
    FFactoryCode: Int64;
    FJZCode: Int64;
    procedure EventOfEventOfCollect(JZCode: Int64; GameCode: Cardinal;
      FactoryCode: Int64);
    procedure EventOfEventOfRead(JZCode: Int64; GameCode: Cardinal;
      FlyTime: Double);
    procedure PrintLabel(IsAuction: Boolean);
  public
    { Public declarations }
  end;

var
  FCoteSoueceSearch: TFCoteSoueceSearch;

implementation

uses UCoteDB, UDlgReadRing;

{$R *.dfm}

procedure TFCoteSoueceSearch.btnPrinterSettingClick(Sender: TObject);
begin
  PrinterSetupDialog1.Execute();
end;

procedure TFCoteSoueceSearch.BtnResetClick(Sender: TObject);
begin
  EMemberName.Text := '';
  EMemberID.Text := '';
  EGBCode.Text := '';
  EGameName.Text := '';
  EFromDate.Checked := False;
  EToDate.Checked := False;
  FFactoryCode:= 0;
  FJZCode:= 0;
end;

procedure TFCoteSoueceSearch.BtnSearchClick(Sender: TObject);
var
  SQLText: string;
  i: Integer;
begin
  SQLText := '';
  if EMemberName.Text <> '' then
    SQLText := SQLText + ' AND M.[MemberName] LIKE ''' +
      EMemberName.Text + '%''';
  if EMemberID.Text <> '' then
    SQLText := SQLText + ' AND M.[MemberNo] =' + EMemberID.Text;
  if EGBCode.Text <> '' then
    SQLText := SQLText + ' AND P.GBCode LIKE ''%' + EGBCode.Text + '%''';
  if EFromDate.Checked then
    SQLText := SQLText + ' AND S.[FlyTime]>''' + DateToStr(EFromDate.Date) + '''';
  if EToDate.Checked then
    SQLText := SQLText + ' AND S.[FlyTime]<''' + DateToStr(EToDate.Date) + '''';
  // if EKeyWord.Text <> '' then SQLText:= SQLText + ' AND P.Note LIKE ''%' + EKeyWord.Text + '%''';
  if EGameName.Text <> '' then
    SQLText := SQLText + ' AND G.[MatchName]=''' + EGameName.Text + '''';
  if FFactoryCode <> 0 then
    SQLText := SQLText + ' AND P.FactoryCode = ' + IntToStr(FFactoryCode)
  else if FJZCode > 0 then
    SQLText := SQLText + ' AND P.JZCode = ' + IntToStr(FJZCode);
  with DQStatistic do
  begin // 赛事监控、成绩查询、赛绩查询  DATEADD(ms, S.BackTime * 60000, S.StartTime)
    Close;
    SQL.Clear;
    SQL.Add('SELECT P.[ID],[PigeonNo],P.[GBCode],M.[MemberNo],M.[MemberName],' +
      'M.[State], M.[City], M.[County],M.[Mobile],P.[Group1],P.[Group2],P.[Group3],' +
      'P.[Group4],P.[Group5],P.[Feather],S.[SourceOrder],G.[MatchName],S.FlyTime,S.Speed,S.ReportTime');
    SQL.Add('FROM JOIN_GameSource S');
    SQL.Add('JOIN JOIN_Pigeon P ON S.[PigeonIDX] = P.[ID]');
    SQL.Add('JOIN JOIN_Member M ON P.[MemberIDX] = M.[ID]');
    SQL.Add('JOIN JOIN_Match G ON S.[GameIDX] = G.[ID]');
    SQL.Add('WHERE G.[Status]<>0');
    SQL.Add(SQLText);
    Open;
    if chkAutoPrintAuction.Checked then btnMultiAuctionClick(nil);
    if chkAutoPrint.Checked then btnMulitPrintClick(nil);
    WriteLog(LOG_DEBUG, 'TFCoteSoueceSearch.BtnSearch', DQStatistic.SQL.Text);
  end;
  // BtnResetClick(nil);
end;

procedure TFCoteSoueceSearch.PrintLabel(IsAuction: Boolean);
begin
  with Printer.Canvas, DQStatistic do begin
    Font.Size:= 12;
    TextOut(10, 10, FieldByName('City').AsString);
    TextOut(220, 10, '编号：' + FieldByName('MemberNo').AsString);
    TextOut(50, 90, FieldByName('MemberName').AsString);
    TextOut(10, 170, FieldByName('GBCode').AsString);
    TextOut(300, 170, FieldByName('Feather').AsString);
    if IsAuction then begin
      TextOut(10, 250, EGameName.Text);
      TextOut(300, 250, '第' + FieldByName('SourceOrder').AsString + '名');
      WriteLog(LOG_DEBUG, 'PrintLabel', '第' + FieldByName('SourceOrder').AsString + '名');
    end else begin
      TextOut(10, 250, FormatDatetime('yyyy年mm月dd日 hh点', Now));
      WriteLog(LOG_DEBUG, 'PrintLabel', FormatDatetime('yyyy年mm月dd日 hh点', Now));
    end;
  end;
end;

procedure TFCoteSoueceSearch.btnMulitPrintClick(Sender: TObject);
begin
  with DQStatistic do begin
    Printer.BeginDoc;
    while Not EOF do begin
      PrintLabel(false);
      Next;
      if not EOF then Printer.NewPage;
    end;
    Printer.EndDoc;
  end;
end;

procedure TFCoteSoueceSearch.btnMultiAuctionClick(Sender: TObject);
begin
  with DQStatistic do begin
    Printer.BeginDoc;
    while Not EOF do begin
      PrintLabel(true);
      Next;
      if not EOF then Printer.NewPage;
    end;
    Printer.EndDoc;
  end;
end;

procedure TFCoteSoueceSearch.btnSingleAuctionClick(Sender: TObject);
begin
  Printer.BeginDoc;
  PrintLabel(true);
  Printer.EndDoc;
end;

procedure TFCoteSoueceSearch.btnSinglePrintClick(Sender: TObject);
begin
  Printer.BeginDoc;
  PrintLabel(False);
  Printer.EndDoc;
end;

procedure TFCoteSoueceSearch.EventOfEventOfCollect(JZCode: Int64;
  GameCode: Cardinal; FactoryCode: Int64);
begin
  if FFactoryCode = FactoryCode then Exit;
  FFactoryCode:= 0;
  FJZCode:= 0;
  if FCoteDB.RingKind = '只读环' then FJZCode:= JZCode
  else if FCoteDB.RingKind = '读写环' then FFactoryCode:= FactoryCode
  else if FCoteDB.RingKind = '高频只读' then FJZCode:= JZCode
  else if FCoteDB.RingKind = '高频读写' then FFactoryCode:= FactoryCode
  else if FCoteDB.RingKind = '凯信只读' then FJZCode:= JZCode;
  BtnSearchClick(nil);
end;

procedure TFCoteSoueceSearch.EventOfEventOfRead(JZCode: Int64; GameCode: Cardinal;
    FlyTime: Double);
begin
  if FJZCode = JZCode then Exit;
  FFactoryCode:= 0;
  FJZCode:= JZCode;
  BtnSearchClick(nil);
end;

procedure TFCoteSoueceSearch.FormCreate(Sender: TObject);
begin
  FrameGridBar1.LoadTitles('CoteSouece');
  with DQGames do
    try
      Open;
      while not EOF do
      begin
        EGameName.Items.Add(Fields[0].AsString);
        Next;
      end;
    finally
      Close;
    end;
  FrameGridBar1.RegStatistic('ID', '查询鸽子数量为', ES_COUNT, '');
end;

procedure TFCoteSoueceSearch.FormDestroy(Sender: TObject);
begin
  if Assigned(FComm) then FCoteDB.CloseComm(FComm);
end;

procedure TFCoteSoueceSearch.FrameGridBar1menuConditionClick(Sender: TObject);
begin
  FrameGridBar1.menuConditionClick(Sender);
  GroupBox1.Visible := not FrameGridBar1.menuCondition.Checked;
end;

procedure TFCoteSoueceSearch.rbChickInClick(Sender: TObject);
begin
  with TFDlgReadRing.Create(nil) do try
    FComm:= OpenComm(nil, EventOfEventOfRead, nil);
  finally
    Free;
  end
end;

procedure TFCoteSoueceSearch.rbCloseClick(Sender: TObject);
begin
  FCoteDB.CloseComm(FComm);
  FFactoryCode:= 0;
  FJZCode:= 0;
end;

procedure TFCoteSoueceSearch.rbCollectClick(Sender: TObject);
begin
  with TFDlgReadRing.Create(nil) do try
    FComm:= OpenComm(EventOfEventOfCollect, nil, nil);
  finally
    Free;
  end
end;

end.
