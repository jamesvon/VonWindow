unit UCoteSoueceStatistic;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, Buttons, Grids, DBGrids, ExtCtrls, ComCtrls,
  ToolWin, ImgList, VclTee.TeeGDIPlus, System.ImageList, VCLTee.TeEngine,
  VCLTee.Series, VCLTee.TeeProcs, VCLTee.Chart, VCLTee.DBChart;

type
  TFCoteSoueceStatistic = class(TForm)
    DQPigeon: TADOQuery;
    DSPigeon: TDataSource;
    Q1: TADOQuery;
    DBGrid2: TDBGrid;
    ImageList1: TImageList;
    DataSource1: TDataSource;
    DBMatch: TADOTable;
    DSMatch: TDataSource;
    Q2: TADOQuery;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    DBGrid1: TDBGrid;
    Panel6: TPanel;
    Label1: TLabel;
    EGBCode: TEdit;
    btnSearch: TSpeedButton;
    Splitter1: TSplitter;
    DBGrid3: TDBGrid;
    Panel7: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    DBChart1: TDBChart;
    Series1: TBarSeries;
    Series2: TBarSeries;
    procedure btnSearchClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCoteSoueceStatistic: TFCoteSoueceStatistic;

implementation

uses UCoteDB;

{$R *.dfm}

procedure TFCoteSoueceStatistic.btnSearchClick(Sender: TObject);
begin
  with DQPigeon do
  begin
    Close;
    Parameters[0].Value := EGBCode.Text;
    Open;
  end;
end;

procedure TFCoteSoueceStatistic.Button1Click(Sender: TObject);
var
  cs: TLineSeries;
begin
  while DBChart1.SeriesList.Count > 0 do
    DBChart1.SeriesList.Delete(0);
  Q1.Close;
  Q1.SQL.Clear;
  Q1.SQL.Add('SELECT G.MatchName,S.FlyTime FROM dbo.JOIN_GameSource S');
  Q1.SQL.Add
    ('JOIN JOIN_Match G ON G.ID=S.GameIdx WHERE S.IsBack = 1 AND S.GBCode=:GBC');
  Q1.Parameters[0].Value := DQPigeon.FieldByName('GBCode').AsString;
  DataSource1.DataSet := Q1;
  Q1.Open;
  cs := TLineSeries.Create(self);
  DBChart1.AddSeries(cs);
  cs.DataSource := Q1;
  cs.XLabelsSource := 'MatchName';
  cs.LinePen.Color := $000000;
  cs.Marks.Visible := True;
  // cs.XValues.ValueSource:='FlyTime';
  cs.YValues.ValueSource := 'FlyTime';
  cs.YValues.Name := '����';
  cs.CheckDataSource;
  cs.Repaint;
  DBChart1.Repaint;
  DBChart1.Axes.Left.Inverted := False;
  DBChart1.Axes.Left.Visible := True;
  DBChart1.Legend.Visible := False;
end;

procedure TFCoteSoueceStatistic.Button2Click(Sender: TObject);
var
  cs: TLineSeries;
begin
  while DBChart1.SeriesList.Count > 0 do
    DBChart1.SeriesList.Delete(0);
  Q1.Close;
  Q1.SQL.Clear;
  Q1.SQL.Add('SELECT G.MatchName,S.Speed FROM dbo.JOIN_GameSource S');
  Q1.SQL.Add
    ('JOIN JOIN_Match G ON G.ID=S.GameIdx WHERE S.IsBack = 1 AND S.GBCode=:GBC');
  Q1.Parameters[0].Value := DQPigeon.FieldByName('GBCode').AsString;
  DataSource1.DataSet := Q1;
  Q1.Open;
  cs := TLineSeries.Create(self);
  DBChart1.AddSeries(cs);
  cs.DataSource := Q1;
  cs.XLabelsSource := 'MatchName';
  cs.LinePen.Color := $000000;
  // cs.XValues.ValueSource:='FlyTime';
  cs.YValues.ValueSource := 'Speed';
  cs.CheckDataSource;
  cs.Repaint;
  DBChart1.Repaint;
  DBChart1.Axes.Left.Inverted := False;
  DBChart1.Axes.Left.Visible := True;
  DBChart1.Legend.Visible := False;
end;

procedure TFCoteSoueceStatistic.Button3Click(Sender: TObject);
var
  cs: TLineSeries;
begin
  while DBChart1.SeriesList.Count > 0 do
    DBChart1.SeriesList.Delete(0);
  Q1.Close;
  Q1.SQL.Clear;
  Q1.SQL.Add('SELECT G.MatchName,S.SourceOrder FROM dbo.JOIN_GameSource S');
  Q1.SQL.Add
    ('JOIN JOIN_Match G ON G.ID=S.GameIdx WHERE S.IsBack = 1 AND S.GBCode=:GBC');
  Q1.Parameters[0].Value := DQPigeon.FieldByName('GBCode').AsString;
  DataSource1.DataSet := Q1;
  Q1.Open;
  cs := TLineSeries.Create(self);
  DBChart1.AddSeries(cs);
  cs.DataSource := Q1;
  cs.XLabelsSource := 'MatchName';
  cs.LinePen.Color := $000000;
  // cs.XValues.ValueSource:='FlyTime';
  cs.YValues.ValueSource := 'SourceOrder';
  cs.CheckDataSource;
  cs.Repaint;
  DBChart1.Axes.Left.Inverted := True;
  DBChart1.Repaint;
  DBChart1.Axes.Left.Visible := True;
  DBChart1.Legend.Visible := False;
end;

procedure TFCoteSoueceStatistic.Button4Click(Sender: TObject);
var
  cs: TBarSeries;
begin
  while DBChart1.SeriesList.Count > 0 do
    DBChart1.SeriesList.Delete(0);
  Q1.Close;
  Q1.SQL.Clear;
  Q1.SQL.Add
    ('SELECT RIGHT(''000000'' + CAST(SourceOrder AS nvarchar(20)), 6) AS Code,FlyTime FROM JOIN_GameSource');
  Q1.SQL.Add('WHERE IsBack = 1 AND SourceOrder < 20 AND GameIdx=''' +
    DBMatch.FieldByName('ID').AsString + '''');
  Q1.SQL.Add('UNION SELECT GBCode AS Code,FlyTime FROM dbo.JOIN_GameSource');
  Q1.SQL.Add('WHERE IsBack = 1 AND GBCode=:GBC AND GameIdx=''' +
    DBMatch.FieldByName('ID').AsString + '''');
  Q1.SQL.Add('ORDER BY 1');
  Q1.Parameters[0].Value := DQPigeon.FieldByName('GBCode').AsString;
  DataSource1.DataSet := Q1;
  Q1.Open;
  cs := TBarSeries.Create(self);
  DBChart1.AddSeries(cs);
  cs.DataSource := Q1;
  cs.XLabelsSource := 'Code';
  cs.BarBrush.Color := $000000;
  // cs.XValues.ValueSource:='FlyTime';
  cs.YValues.ValueSource := 'FlyTime';
  cs.CheckDataSource;
  cs.Repaint;
  DBChart1.Repaint;
  DBChart1.Axes.Left.Inverted := False;
  DBChart1.Axes.Left.Visible := True;
  DBChart1.Legend.Visible := False;
end;

procedure TFCoteSoueceStatistic.Button5Click(Sender: TObject);
var
  cs: TBarSeries;
begin
  while DBChart1.SeriesList.Count > 0 do
    DBChart1.SeriesList.Delete(0);
  Q1.Close;
  Q1.SQL.Clear;
  Q1.SQL.Add
    ('SELECT RIGHT(''000000'' + CAST(SourceOrder AS nvarchar(20)), 6) AS Code,Speed FROM JOIN_GameSource');
  Q1.SQL.Add('WHERE IsBack = 1 AND SourceOrder < 20 AND GameIdx=' +
    DBMatch.FieldByName('ID').AsString);
  Q1.SQL.Add('UNION SELECT GBCode AS Code,Speed FROM dbo.JOIN_GameSource');
  Q1.SQL.Add('WHERE IsBack = 1 AND GBCode=:GBC AND GameIdx=''' +
    DBMatch.FieldByName('ID').AsString + '''');
  Q1.SQL.Add('ORDER BY 1');
  Q1.Parameters[0].Value := DQPigeon.FieldByName('GBCode').AsString;
  DataSource1.DataSet := Q1;
  Q1.Open;
  cs := TBarSeries.Create(self);
  DBChart1.AddSeries(cs);
  cs.DataSource := Q1;
  cs.XLabelsSource := 'Code';
  cs.BarBrush.Color := $000000;
  cs.YValues.ValueSource := 'Speed';
  cs.CheckDataSource;
  cs.Repaint;
  DBChart1.Repaint;
  DBChart1.Axes.Left.Inverted := False;
  DBChart1.Axes.Left.Visible := True;
  DBChart1.Legend.Visible := False;
end;

procedure TFCoteSoueceStatistic.Button6Click(Sender: TObject);
var
  cs: TBarSeries;
begin
  while DBChart1.SeriesList.Count > 0 do
    DBChart1.SeriesList.Delete(0);
  Q1.Close;
  Q1.SQL.Clear;
  Q1.SQL.Add('SELECT G.ID,G.MatchName,S.FlyTime FROM dbo.JOIN_GameSource S');
  Q1.SQL.Add
    ('JOIN JOIN_Match G ON G.ID=S.GameIdx WHERE S.IsBack = 1 AND(S.GBCode=:GBC OR SourceOrder=1)');
  Q1.SQL.Add('ORDER BY G.ID, S.SourceOrder');
  Q1.Parameters[0].Value := DQPigeon.FieldByName('GBCode').AsString;
  DataSource1.DataSet := Q1;
  Q1.Open;
  cs := TBarSeries.Create(self);
  DBChart1.AddSeries(cs);
  cs.DataSource := Q1;
  cs.XLabelsSource := 'MatchName';
  cs.BarBrush.Color := $000000;
  cs.YValues.ValueSource := 'FlyTime';
  cs.CheckDataSource;
  cs.Repaint;
  DBChart1.Repaint;
  DBChart1.Axes.Left.Inverted := False;
  DBChart1.Axes.Left.Visible := True;
  DBChart1.Legend.Visible := False;
end;

procedure TFCoteSoueceStatistic.Button7Click(Sender: TObject);
var
  cs1, cs2: TBarSeries;
begin
  while DBChart1.SeriesList.Count > 0 do
    DBChart1.SeriesList.Delete(0);
  Q1.Close;
  Q1.SQL.Clear;
  Q1.SQL.Add('SELECT S.GameIdx,S.FlyTime FROM JOIN_GameSource S');
  Q1.SQL.Add('WHERE S.IsBack = 1 AND S.GBCode=:GBC');
  Q1.SQL.Add('ORDER BY S.GameIdx, S.SourceOrder');
  Q1.Parameters[0].Value := DQPigeon.FieldByName('GBCode').AsString;
  DataSource1.DataSet := Q1;
  Q1.Open;
  cs1 := TBarSeries.Create(self);
  DBChart1.AddSeries(cs1);
  cs1.DataSource := Q1;
  cs1.XLabelsSource := 'GameIdx';
  cs1.BarBrush.Color := $000000;
  cs1.YValues.ValueSource := 'FlyTime';

  Q2.Close;
  Q2.SQL.Clear;
  Q2.SQL.Add
    ('SELECT S.GameIdx,AVG(S.FlyTime) AS FlyTime FROM JOIN_GameSource S');
  Q2.SQL.Add
    ('JOIN JOIN_GameSource A ON A.GameIdx=S.GameIdx AND A.GBCode=:GBC AND A.IsBack=1');
  Q2.SQL.Add('WHERE S.SourceOrder<501');
  Q2.SQL.Add('GROUP BY S.GameIdx');
  Q2.SQL.Add('ORDER BY S.GameIdx');
  Q2.Parameters[0].Value := DQPigeon.FieldByName('GBCode').AsString;
  DataSource1.DataSet := Q2;
  Q2.Open;
  cs2 := TBarSeries.Create(self);
  DBChart1.AddSeries(cs2);
  cs2.DataSource := Q2;
  cs2.XLabelsSource := 'GameIdx';
  cs2.BarBrush.Color := $FFFFF1;
  cs2.YValues.ValueSource := 'FlyTime';

  cs1.CheckDataSource;
  cs1.Repaint;
  cs2.CheckDataSource;
  cs2.Repaint;
  DBChart1.Repaint;
  DBChart1.Axes.Left.Inverted := False;
  DBChart1.Axes.Left.Visible := True;
  DBChart1.Legend.Visible := False;
end;

procedure TFCoteSoueceStatistic.Button8Click(Sender: TObject);
var
  cs1, cs2: TBarSeries;
begin
  while DBChart1.SeriesList.Count > 0 do
    DBChart1.SeriesList.Delete(0);
  Q1.Close;
  Q1.SQL.Clear;
  Q1.SQL.Add('SELECT S.GameIdx,S.FlyTime FROM JOIN_GameSource S');
  Q1.SQL.Add('WHERE S.IsBack = 1 AND S.GBCode=:GBC');
  Q1.SQL.Add('ORDER BY S.GameIdx, S.SourceOrder');
  Q1.Parameters[0].Value := DQPigeon.FieldByName('GBCode').AsString;
  DataSource1.DataSet := Q1;
  Q1.Open;
  cs1 := TBarSeries.Create(self);
  DBChart1.AddSeries(cs1);
  cs1.DataSource := Q1;
  cs1.XLabelsSource := 'GameIdx';
  cs1.BarBrush.Color := $000000;
  cs1.YValues.ValueSource := 'FlyTime';

  Q2.Close;
  Q2.SQL.Clear;
  Q2.SQL.Add('SELECT S.GameIdx,S.FlyTime FROM JOIN_GameSource S');
  Q2.SQL.Add
    ('JOIN JOIN_GameSource A ON A.GameIdx=S.GameIdx AND A.GBCode=:GBC AND A.IsBack=1');
  Q2.SQL.Add('WHERE S.SourceOrder<5');
  Q2.SQL.Add('ORDER BY S.GameIdx,S.SourceOrder');
  Q2.Parameters[0].Value := DQPigeon.FieldByName('GBCode').AsString;
  DataSource1.DataSet := Q2;
  Q2.Open;
  cs2 := TBarSeries.Create(self);
  DBChart1.AddSeries(cs2);
  cs2.DataSource := Q2;
  cs2.XLabelsSource := 'GameIdx';
  cs2.BarBrush.Color := $FFFFF1;
  cs2.YValues.ValueSource := 'FlyTime';

  cs1.CheckDataSource;
  cs1.Repaint;
  cs2.CheckDataSource;
  cs2.Repaint;
  DBChart1.Repaint;
  DBChart1.Axes.Left.Inverted := False;
  DBChart1.Axes.Left.Visible := True;
  DBChart1.Legend.Visible := False;
end;

procedure TFCoteSoueceStatistic.FormCreate(Sender: TObject);
begin
  DBMatch.Open;
end;

end.
