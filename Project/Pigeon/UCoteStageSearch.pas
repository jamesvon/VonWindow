unit UCoteStageSearch;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, DB, ADODB, ComCtrls, StdCtrls, Buttons, ToolWin, DBGrids,
  Grids, ValEdit, ExtCtrls, UFrameGridBar, System.ImageList, Vcl.CheckLst,
  UVonSystemFuns, UCoteInfo, UVonOffice;

const MAX_MATCH_STAGE = 5;

type
  TFCoteStageSearch = class(TForm)
    Splitter1: TSplitter;
    GroupBox1: TGroupBox;
    DBMatch: TADOTable;
    DQStatistic: TADOQuery;
    ImageList1: TImageList;
    DQItems: TADOQuery;
    Panel1: TPanel;
    btnStatisticBySpeed: TBitBtn;
    btnStatisticByOrder: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    grid: TStringGrid;
    btnExportExcel: TButton;
    lstMatch: TValueListEditor;
    ProgressBar1: TProgressBar;
    SaveDialog1: TSaveDialog;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btnStatisticBySpeedClick(Sender: TObject);
    procedure btnStatisticByOrderClick(Sender: TObject);
    procedure DQStatisticListOrderGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure btnExportExcelClick(Sender: TObject);
  private
    { Private declarations }
    FDisplayTitles: TStringList;
    procedure Statistic(BySpeed: Boolean);
    procedure DisplayProcess(PorcessValue: integer);
  public
    { Public declarations }
  end;

  TSourceStatistic = class(TSourceView)
  public
    Data: array[0..MAX_MATCH_STAGE]of Extended; //排名或分速
  end;

var
  FCoteStageSearch: TFCoteStageSearch;

implementation

uses Math, UCoteDB;

{$R *.dfm}

procedure TFCoteStageSearch.FormCreate(Sender: TObject);
var
  GID: PGuid;
begin
  with DBMatch do
  begin
    Open;
    lstMatch.Strings.Clear;
    while not Eof do
    begin
      New(GID);
      Gid^:= FieldByName('ID').AsGuid;
      lstMatch.Strings.AddObject(FieldByName('MatchName').AsString + '=', TObject(Gid));
      Next;
    end;
    Close;
  end;
  FDisplayTitles := GetDisplayTitles('FIELDS', 'StageStatisticDisplayFields');
end;

procedure TFCoteStageSearch.btnStatisticBySpeedClick(Sender: TObject);
begin
  Statistic(true);
end;

procedure TFCoteStageSearch.btnExportExcelClick(Sender: TObject);
var
  Row, Col: Integer;
  xls: TVonExcelWriter;
begin
  SaveDialog1.DefaultExt := '';
  SaveDialog1.Filter := 'Excel文件|*.xls';
  if not SaveDialog1.Execute then Exit;
  xls:= TVonExcelWriter.Create(ChangeFileExt(SaveDialog1.filename, '.xls'));
  Try
    for Row := 0 to Grid.RowCount - 1 do
      for Col := 0 to Grid.ColCount - 1 do
        xls.WriteCell(Col, Row, Grid.Cells[Col, Row]);
  finally
    xls.Free;
  end;
end;

procedure TFCoteStageSearch.btnStatisticByOrderClick(Sender: TObject);
begin
  Statistic(false);
end;

procedure TFCoteStageSearch.DQStatisticListOrderGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := IntToStr(Sender.DataSet.RecNo);
end;

procedure TFCoteStageSearch.DisplayProcess(PorcessValue: integer);
begin
  ProgressBar1.Visible:= PorcessValue > 0;
  ProgressBar1.Position:= PorcessValue;
  ProgressBar1.Repaint;
end;

function Cmp(List: TStringList; Index1, Index2: Integer): Integer;
begin
  result:= Sign(StrToint(List.ValueFromIndex[index1]) - StrToint(List.ValueFromIndex[Index2]));
end;

function OrderSourcceByOrder(Item1, Item2: Pointer): Integer;
var
  I: Integer;
  sum1, sum2: Extended;
begin
  sum1:= 0; sum2:= 0;
  for I := 0 to MAX_MATCH_STAGE do begin
    sum1:= sum1 + TSourceStatistic(Item1).Data[I];
    sum2:= sum2 + TSourceStatistic(Item2).Data[I];
  end;
  result:= Sign(sum1 - sum2);
  if result = 0 then
    result:= Sign(TSourceStatistic(Item2).Data[0] - TSourceStatistic(Item1).Data[0]);
end;

function OrderSourcceBySpeed(Item1, Item2: Pointer): Integer;
var
  I: Integer;
  sum1, sum2: Extended;
begin
  sum1:= 0; sum2:= 0;
  for I := 0 to MAX_MATCH_STAGE do begin
    sum1:= sum1 + TSourceStatistic(Item1).Data[I];
    sum2:= sum2 + TSourceStatistic(Item2).Data[I];
  end;
  result:= Sign(sum2 - sum1);
  if result = 0 then
    result:= Sign(TSourceStatistic(Item1).Data[0] - TSourceStatistic(Item2).Data[0]);
end;

procedure TFCoteStageSearch.Statistic(BySpeed: Boolean);
var
  listSource: TList;
  I, j, cnt: Integer;
  Data: TSourceStatistic;
  Matchs, Titles: TStringList;
  CalcField: string;
begin
  //排序关赛信息，参与排名数量较少者将作为主要赛事信息优先查询，并以此作为比较基数进行分析
  Matchs:= TStringList.Create;
  for I := 0 to lstMatch.Strings.Count - 1 do
    if tryStrToInt(lstMatch.Strings.ValueFromIndex[I], cnt) then
      Matchs.AddObject(lstMatch.Strings[I], lstMatch.Strings.Objects[I]);
  if Matchs.Count >= MAX_MATCH_STAGE then raise Exception.Create('关赛选择过多，系统不支持统计');
  if Matchs.Count = 0 then raise Exception.Create('请填写参与统计的关赛有效名次');
  Matchs.Sorted:= False;
  Matchs.CustomSort(Cmp);
  DisplayProcess(1);
  //开始统计，先提取第一场赛事的有效名次
  if BySpeed then CalcField := 'Speed' else CalcField:= 'SourceOrder';
  listSource:= TList.Create;
  TSourceStatistic.OpenQuery(DQStatistic, 'GameIdx=''' + GuidToString(
    PGuid(Matchs.Objects[0])^) + ''' AND SourceOrder>0 AND SourceOrder<=' +
    Matchs.ValueFromIndex[0] + ' ORDER BY SourceOrder');
  with DQStatistic do begin
    while not EOF do begin
      Data:= TSourceStatistic.Create;
      Data.LoadFromDB(DQStatistic);
      Data.Data[0]:=  FieldByName(CalcField).AsExtended; for J := 1 to MAX_MATCH_STAGE do Data.Data[J]:= 0;
      listSource.Add(Data);
      Next;
    end;
  DisplayProcess(30);
  end;
  //获取其余赛事成绩，与基数成绩进行比较，有的留下，没有的从基数中删除
  for I := 1 to Matchs.Count - 1 do
    with DQStatistic do begin
      Close;
      SQL.Text:= 'SELECT PigeonIdx,' + CalcField +
        ' FROM JOIN_GameSource WHERE GameIdx=''' + GuidToString(PGuid(Matchs.Objects[I])^) +
        ''' AND SourceOrder>0 AND SourceOrder<=' + Matchs.ValueFromIndex[I];
      Open;
      for J := listSource.Count - 1 downto 0 do
        if Locate('PigeonIdx', GuidToString(TSourceStatistic(listSource[J]).PigeonIdx), []) then
          TSourceStatistic(listSource[J]).Data[I]:= Fields[1].AsExtended
        else begin
          FreeMemory(listSource[J]);
          listSource.Delete(J);
        end;
    end;
  if BySpeed then listSource.Sort(OrderSourcceBySpeed) else listSource.Sort(OrderSourcceByOrder);
  DisplayProcess(100);
  //提取显示字段标题
  Titles:= GetDisplayTitles('FIELDS', 'StageStatisticDisplayFields');
  //显示标题
  grid.RowCount:= listSource.Count + 1;
  grid.ColCount:= Titles.Count + Matchs.Count + 1;
  grid.Cells[0, 0]:= '排名';
  for J := 0 to Titles.Count - 1 do begin
    grid.Cells[J + 1, 0]:= Titles.ValueFromIndex[J];
    grid.ColWidths[J + 1]:= Integer(Titles.Objects[J]);
  end;
  for J := 0 to Matchs.Count - 1 do
    grid.Cells[Titles.Count + J + 1, 0]:= Matchs.Names[J];
  grid.FixedRows:= 1;
  for I := 0 to listSource.Count - 1 do begin
    grid.Cells[0, I + 1]:= IntToStr(I + 1);
    for J := 0 to Titles.Count - 1 do
      grid.Cells[J + 1, I + 1]:= TSourceStatistic(listSource[I]).Values[Titles.Names[J]];
    for J := 0 to Matchs.Count - 1 do
      grid.Cells[Titles.Count + J + 1, I + 1]:= FloatToStr(TSourceStatistic(listSource[I]).Data[J]);
    TSourceStatistic(listSource[I]).Free;
  end;
  listSource.Clear;
  listSource.Free;
  Matchs.Free;
end;

initialization
// 集鸽配置
//SELECT M.State,M.City,M.County,M.MemberNo,M.MemberName,M.Mobile,P.PigeonNO,
//P.GBCode,P.Feather,P.Status,P.Flag,P.Group1,P.Group2,P.Group3,P.Group4,P.Group5,
//  C.MatchName,S.Speed,S.SourceOrder
RegAppConfig('FIELDS', 'StageStatisticDisplayFields', '多关赛查询显示字段名称', LIDT_Memo, '',
  'State=省份:64,City=地市:64,County=区县:64,MemberNo=鸽主编号:64,MemberName=' +
  '鸽主:64,Mobile=手机号:64,GBCode=统一环号:128,Feather=羽色:64,Status=状态:64,' +
  'Flag=标记:68,Group1=分组1:64,Group2=分组2:64,Group3=分组3:64,Group4=分组4:64,Group5=分组5:64');
end.
