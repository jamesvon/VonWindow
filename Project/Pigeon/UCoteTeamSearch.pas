unit UCoteTeamSearch;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ImgList, DB, ADODB, ComCtrls, ToolWin, Grids, DBGrids,
  ExtCtrls, Spin, Buttons, UFrameGridBar, System.ImageList, Vcl.Menus;

type
  TFCoteTeamSearch = class(TForm)
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    RGOrderType: TRadioGroup;
    DBGrid2: TDBGrid;
    DBMatch: TADOTable;
    DSMatch: TDataSource;
    DQItems: TADOQuery;
    ImageList1: TImageList;
    DQStatistic: TADOQuery;
    FrameTeamGrid: TFrameGridBar;
    FrameTeamItemGrid: TFrameGridBar;
    Panel2: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    EMaxOrder: TSpinEdit;
    Label2: TLabel;
    EGroup: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure DQStatisticListOrderGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCoteTeamSearch: TFCoteTeamSearch;

implementation

uses UCoteDB, UVonLog, UVonSystemFuns;

{$R *.dfm}

procedure TFCoteTeamSearch.BitBtn1Click(Sender: TObject);
var
  GroupField: string;
  i: Integer;
begin
  DQItems.Close;
  GroupField:= 'P.Group' + IntToStr(EGroup.ItemIndex - 2);
  with DQStatistic do
  begin
    Close;
    SQL.Clear;                                                      //2014-10-23系统不再支持多人同一分组的需求
    SQL.Add('SELECT identity(int,1,1) AS IDX, * INTO #CTS FROM(');
    (*SELECT S.[GameIdx],M.[ID] AS MID,M.[State] AS GroupName,COUNT( * ) AS CT, Min(S.[SourceOrder]) AS MinOrder, SUM(S.Speed) AS SumSpeed
FROM JOIN_GameSource S
JOIN JOIN_Match H ON H.[ID]=S.[GameIdx]
JOIN JOIN_Pigeon P ON P.[ID]=S.[PigeonIdx]
JOIN JOIN_Member M ON M.[ID]=P.[MemberIdx]
WHERE S.IsBack = 1 AND S.GameIdx =:GID AND S.[SourceOrder] <=:MaxOrd
GROUP BY GameIdx,M.[State]
*)
    SQL.Add('SELECT TOP 20000 M.[MemberName], S.[GameIdx], M.[ID] AS MID,');
    SQL.Add(GroupField + ' AS GroupName, ');
    SQL.Add('COUNT(*) AS CT, Min(S.[SourceOrder]) AS MinOrder, SUM(S.Speed) AS SumSpeed');
    SQL.Add('FROM JOIN_GameSource S');
    SQL.Add('JOIN JOIN_Match H ON H.[ID]=S.[GameIdx]');
    SQL.Add('JOIN JOIN_Pigeon P ON P.[ID]=S.[PigeonIdx]');
    SQL.Add('JOIN JOIN_Member M ON M.[ID]=P.[MemberIdx]');
    SQL.Add('WHERE S.IsBack = 1 AND S.GameIdx=''' + DBMatch.FieldByName('ID')
      .AsString + ''' AND S.[SourceOrder] <=' + IntToStr(EMaxOrder.Value));
    SQL.Add('AND ' + GroupField + '<>''''');
    SQL.Add('GROUP BY MemberName, S.[GameIdx],M.[ID],M.[State],' + GroupField);
    case RGOrderType.ItemIndex of
      0:
        SQL.Add('ORDER BY CT DESC');
      1:
        SQL.Add('ORDER BY CT DESC, MinOrder');
    end;
    SQL.Add(') AA');
    SQL.Add('SELECT * FROM #CTS');
    SQL.Add('DROP TABLE #CTS');
    WriteLog(LOG_DEBUG, 'TFCoteTeamSearch', SQL.Text);
    Open;
  end;
  with DQItems do begin
    SQL.Text:= 'SELECT P.[GBCode], S.[SourceOrder], S.Speed, M.[ID] AS MemberID, ' +
      'M.[MemberName] AS MemberName FROM JOIN_Pigeon P JOIN JOIN_GameSource S ' +
      'ON S.PigeonIdx=P.[ID] JOIN JOIN_Member M ON P.[MemberIDX]=M.[ID] ' +
      'JOIN JOIN_Match H ON S.[GameIdx]=H.[ID] WHERE S.[GameIdx]=:GameIdx AND ' +
      'M.[ID]=:MID AND ' + GroupField + '=:GroupName ORDER BY S.[SourceOrder]';
    Open;
  end;
  // SELECT P.[GBCode], S.[SourceOrder], H.GameLong/S.BackTime AS Speed, M.[ID] AS MemberID, M.[MemberName] AS MemberName
  // FROM PGN_Pigeon P
  // JOIN PGN_GameSource S ON S.PigeonID=P.[ID]
  // JOIN PGN_Member M ON P.[MemberIDX]=M.[ID]
  // JOIN PGN_Match H ON S.[GameID]=H.[ID]
  // WHERE S.[GameID]=:GameID
  // AND XXXX=:GroupName
  // ORDER BY S.[SourceOrder]
end;

procedure TFCoteTeamSearch.DQStatisticListOrderGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := IntToStr(Sender.DataSet.RecNo);
end;

procedure TFCoteTeamSearch.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  FrameTeamGrid.LoadTitles('TeamSearch');
  FrameTeamItemGrid.LoadTitles('TeamItemSearch');
  FCoteDB.ReinitGroup;
  for I := 0 to 4 do
    if FCoteDB.FGroupNames[I] <> '' then
      EGroup.Items.Add(FCoteDB.FGroupNames[I]);
  EGroup.ItemIndex := 0;
  DBMatch.Open;
end;

end.
