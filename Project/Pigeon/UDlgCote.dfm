object FDlgCote: TFDlgCote
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #20844#26842#20449#24687
  ClientHeight = 411
  ClientWidth = 266
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel6: TPanel
    Left = 0
    Top = 0
    Width = 266
    Height = 34
    Align = alTop
    AutoSize = True
    BevelOuter = bvNone
    Caption = 'Panel6'
    ShowCaption = False
    TabOrder = 0
    object lblOrgName: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 60
      Height = 28
      Align = alLeft
      Alignment = taRightJustify
      AutoSize = False
      Caption = #20844#26842#21517#31216
      Layout = tlCenter
      ExplicitLeft = 26
      ExplicitTop = 5
      ExplicitHeight = 26
    end
    object EOrgName: TEdit
      AlignWithMargins = True
      Left = 69
      Top = 3
      Width = 194
      Height = 28
      Align = alClient
      TabOrder = 0
      ExplicitHeight = 21
    end
  end
  object Panel7: TPanel
    Left = 0
    Top = 68
    Width = 266
    Height = 30
    Align = alTop
    AutoSize = True
    BevelOuter = bvNone
    Caption = 'Panel6'
    ShowCaption = False
    TabOrder = 1
    object lbRegion: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 60
      Height = 24
      Align = alLeft
      Alignment = taRightJustify
      AutoSize = False
      Caption = #25152#23646#21306#22495
      Layout = tlCenter
      ExplicitTop = 6
      ExplicitHeight = 21
    end
    object ERegion: TComboBox
      AlignWithMargins = True
      Left = 69
      Top = 3
      Width = 194
      Height = 21
      Align = alClient
      Style = csDropDownList
      TabOrder = 0
    end
  end
  object Panel8: TPanel
    Left = 0
    Top = 218
    Width = 266
    Height = 30
    Align = alTop
    AutoSize = True
    BevelOuter = bvNone
    Caption = 'Panel6'
    ShowCaption = False
    TabOrder = 2
    object Label6: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 60
      Height = 24
      Align = alLeft
      Alignment = taRightJustify
      AutoSize = False
      Caption = #32852#31995#26041#24335
      Layout = tlCenter
    end
    object EContact: TEdit
      AlignWithMargins = True
      Left = 69
      Top = 3
      Width = 194
      Height = 24
      Align = alClient
      TabOrder = 0
      ExplicitHeight = 21
    end
  end
  object Panel9: TPanel
    Left = 0
    Top = 188
    Width = 266
    Height = 30
    Align = alTop
    AutoSize = True
    BevelOuter = bvNone
    Caption = 'Panel6'
    ShowCaption = False
    TabOrder = 3
    object Label5: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 60
      Height = 24
      Align = alLeft
      Alignment = taRightJustify
      AutoSize = False
      Caption = #37038#32534
      Layout = tlCenter
    end
    object EZip: TEdit
      AlignWithMargins = True
      Left = 69
      Top = 3
      Width = 194
      Height = 24
      Align = alClient
      TabOrder = 0
      ExplicitHeight = 21
    end
  end
  object Panel10: TPanel
    Left = 0
    Top = 158
    Width = 266
    Height = 30
    Align = alTop
    AutoSize = True
    BevelOuter = bvNone
    Caption = 'Panel6'
    ShowCaption = False
    TabOrder = 4
    object Label4: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 60
      Height = 24
      Align = alLeft
      Alignment = taRightJustify
      AutoSize = False
      Caption = #22320#22336
      Layout = tlCenter
    end
    object EAddress: TEdit
      AlignWithMargins = True
      Left = 69
      Top = 3
      Width = 194
      Height = 24
      Align = alClient
      TabOrder = 0
      ExplicitHeight = 21
    end
  end
  object Panel11: TPanel
    Left = 0
    Top = 128
    Width = 266
    Height = 30
    Align = alTop
    AutoSize = True
    BevelOuter = bvNone
    Caption = 'Panel6'
    ShowCaption = False
    TabOrder = 5
    object lbCity: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 60
      Height = 24
      Align = alLeft
      Alignment = taRightJustify
      AutoSize = False
      Caption = #25152#22312#22320#24066
      Layout = tlCenter
    end
    object ECity: TComboBox
      AlignWithMargins = True
      Left = 69
      Top = 3
      Width = 194
      Height = 21
      Align = alClient
      TabOrder = 0
    end
  end
  object Panel12: TPanel
    Left = 0
    Top = 98
    Width = 266
    Height = 30
    Align = alTop
    AutoSize = True
    BevelOuter = bvNone
    Caption = 'Panel6'
    ShowCaption = False
    TabOrder = 6
    object lbState: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 60
      Height = 24
      Align = alLeft
      Alignment = taRightJustify
      AutoSize = False
      Caption = #25152#22312#30465#20221
      Layout = tlCenter
    end
    object EState: TComboBox
      AlignWithMargins = True
      Left = 69
      Top = 3
      Width = 194
      Height = 21
      Align = alClient
      Style = csDropDownList
      TabOrder = 0
      OnChange = EStateChange
    end
  end
  object Panel13: TPanel
    Left = 0
    Top = 248
    Width = 266
    Height = 30
    Align = alTop
    AutoSize = True
    BevelOuter = bvNone
    Caption = 'Panel6'
    ShowCaption = False
    TabOrder = 7
    object Label7: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 60
      Height = 24
      Align = alLeft
      Alignment = taRightJustify
      AutoSize = False
      Caption = #32852#31995#20154
      Layout = tlCenter
    end
    object ELinker: TEdit
      AlignWithMargins = True
      Left = 69
      Top = 3
      Width = 194
      Height = 24
      Align = alClient
      TabOrder = 0
      ExplicitHeight = 21
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 308
    Width = 266
    Height = 53
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Panel5'
    ShowCaption = False
    TabOrder = 8
    OnResize = Panel5Resize
    inline FrameLonLon: TFrameLonLat
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 127
      Height = 47
      Align = alLeft
      TabOrder = 0
      ExplicitLeft = 3
      ExplicitTop = 3
      ExplicitWidth = 127
      inherited EC: TEdit
        Width = 45
        ExplicitWidth = 45
      end
      inherited EF: TEdit
        Left = 45
        ExplicitLeft = 45
      end
      inherited ES: TEdit
        Left = 86
        ExplicitLeft = 86
        ExplicitTop = 21
        ExplicitHeight = 21
      end
      inherited Panel1: TPanel
        Width = 127
        ExplicitWidth = 127
        inherited Label1: TLabel
          Width = 28
          Height = 21
          Caption = #32463#24230
          ExplicitWidth = 24
        end
        inherited rbS: TRadioButton
          Left = 94
          ExplicitLeft = 94
        end
        inherited rbF: TRadioButton
          Left = 61
          ExplicitLeft = 61
        end
        inherited rbC: TRadioButton
          Left = 28
          ExplicitLeft = 28
        end
      end
    end
    inline FrameLonLat: TFrameLonLat
      AlignWithMargins = True
      Left = 136
      Top = 3
      Width = 127
      Height = 47
      Align = alClient
      AutoSize = True
      TabOrder = 1
      ExplicitLeft = 136
      ExplicitTop = 3
      ExplicitWidth = 127
      inherited EC: TEdit
        Width = 45
        ExplicitWidth = 45
      end
      inherited EF: TEdit
        Left = 45
        ExplicitLeft = 45
      end
      inherited ES: TEdit
        Left = 86
        ExplicitLeft = 86
        ExplicitTop = 21
        ExplicitHeight = 21
      end
      inherited Panel1: TPanel
        Width = 127
        ExplicitWidth = 127
        inherited Label1: TLabel
          Width = 28
          Height = 21
          Caption = #32428#24230
          ExplicitWidth = 24
        end
        inherited rbS: TRadioButton
          Left = 94
          ExplicitLeft = 94
        end
        inherited rbF: TRadioButton
          Left = 61
          ExplicitLeft = 61
        end
        inherited rbC: TRadioButton
          Left = 28
          ExplicitLeft = 28
        end
      end
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 361
    Width = 266
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Panel4'
    ShowCaption = False
    TabOrder = 9
    object btnSave: TButton
      AlignWithMargins = True
      Left = 28
      Top = 3
      Width = 119
      Height = 35
      Align = alRight
      Caption = #20445#23384
      ImageIndex = 29
      ImageMargins.Left = 5
      Images = FCoteDB.ImgButton
      ModalResult = 1
      TabOrder = 0
      OnClick = btnSaveClick
    end
    object btnCancel: TButton
      AlignWithMargins = True
      Left = 153
      Top = 3
      Width = 110
      Height = 35
      Align = alRight
      Cancel = True
      Caption = #25918#24323
      ImageIndex = 20
      ImageMargins.Left = 5
      Images = FCoteDB.ImgButton
      ModalResult = 2
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 34
    Width = 266
    Height = 34
    Align = alTop
    AutoSize = True
    BevelOuter = bvNone
    Caption = 'Panel6'
    ShowCaption = False
    TabOrder = 10
    object Label1: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 60
      Height = 28
      Align = alLeft
      Alignment = taRightJustify
      AutoSize = False
      Caption = #31616#31216
      Layout = tlCenter
      ExplicitLeft = 26
      ExplicitTop = 5
      ExplicitHeight = 26
    end
    object EShortName: TEdit
      AlignWithMargins = True
      Left = 69
      Top = 3
      Width = 194
      Height = 28
      Align = alClient
      TabOrder = 0
      ExplicitHeight = 21
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 278
    Width = 266
    Height = 30
    Align = alTop
    AutoSize = True
    BevelOuter = bvNone
    Caption = 'Panel6'
    ShowCaption = False
    TabOrder = 11
    object Label2: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 60
      Height = 24
      Align = alLeft
      Alignment = taRightJustify
      AutoSize = False
      Caption = #32852#31995#30005#35805
      Layout = tlCenter
    end
    object EMobile: TEdit
      AlignWithMargins = True
      Left = 69
      Top = 3
      Width = 194
      Height = 24
      Align = alClient
      TabOrder = 0
      ExplicitHeight = 21
    end
  end
end
