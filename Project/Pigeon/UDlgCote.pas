unit UDlgCote;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ExtCtrls, DB, ADODB, UCoteDB, UCoteInfo,
  UFrameLonLat, UVonSystemFuns;

type
  TFDlgCote = class(TForm)
    Panel6: TPanel;
    lblOrgName: TLabel;
    EOrgName: TEdit;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    lbRegion: TLabel;
    ERegion: TComboBox;
    lbState: TLabel;
    EState: TComboBox;
    lbCity: TLabel;
    ECity: TComboBox;
    Label4: TLabel;
    EAddress: TEdit;
    Label5: TLabel;
    EZip: TEdit;
    Label6: TLabel;
    EContact: TEdit;
    Panel13: TPanel;
    Label7: TLabel;
    ELinker: TEdit;
    Panel5: TPanel;
    FrameLonLon: TFrameLonLat;
    FrameLonLat: TFrameLonLat;
    Panel4: TPanel;
    btnSave: TButton;
    btnCancel: TButton;
    Panel1: TPanel;
    Label1: TLabel;
    EShortName: TEdit;
    Panel2: TPanel;
    Label2: TLabel;
    EMobile: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure EStateChange(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure Panel5Resize(Sender: TObject);
  private
    { Private declarations }
    SelfEdited: Boolean;
  public
    { Public declarations }
    procedure FormToInfo(FInfo: TCoteInfo);
    procedure InfoToForm(FInfo: TCoteInfo);
  end;

var
  FDlgCote: TFDlgCote;

implementation

{$R *.dfm}

procedure TFDlgCote.FormCreate(Sender: TObject);
begin
  SelfEdited:= (FCoteDB.ExecParams = '自身')or(FCoteDB.TaskParams = '自身');
  lbRegion.Caption:= FCoteDB.GetDropList('区域', ERegion, '');
  if ERegion.Items.Count = 0 then
    ERegion.Items.Text:= '华北'#13#10'西北'#13#10'华中'#13#10'东北';
  ERegion.ItemIndex:= 0;
  FCoteDB.GetSqlDropList('SELECT ID,ItemName FROM SYS_Zone WHERE PID=0', EState, '');
  if SelfEdited then InfoToForm(FCoteDB.FCoteInfo);
end;

procedure TFDlgCote.FormToInfo(FInfo: TCoteInfo);
begin
  FInfo.OrgName:= EOrgName.Text;
  FInfo.Region:= ERegion.Text;
  FInfo.State:= EState.Text;
  FInfo.City:= ECity.Text;
  FInfo.Address:= EAddress.Text;
  FInfo.Zip:= EZip.Text;
  FInfo.Contact:= EContact.Text;
  FInfo.Linker:= ELinker.Text;
  FInfo.Longitude:= FrameLonLon.Value;
  FInfo.Latitude:= FrameLonLat.Value;
end;

procedure TFDlgCote.InfoToForm(FInfo: TCoteInfo);
begin
  EOrgName.Text:= FInfo.OrgName;
  ERegion.Text:= FInfo.Region;
  EState.ItemIndex:= EState.Items.IndexOf(FInfo.State);
  if EState.ItemIndex >= 0 then begin
    EStateChange(nil);
    ECity.ItemIndex:= ECity.Items.IndexOf(FInfo.City);
  end else ECity.Text:= FInfo.City;
  EAddress.Text:= FInfo.Address;
  EZip.Text:= FInfo.Zip;
  EContact.Text:= FInfo.Contact;
  ELinker.Text:= FInfo.Linker;
  FrameLonLon.Value:= FInfo.Longitude;
  FrameLonLat.Value:= FInfo.Latitude;
end;

procedure TFDlgCote.Panel5Resize(Sender: TObject);
begin
  FrameLonLat.Width:= (Sender as TPanel).width div 2;
  FrameLonLon.Width:= (Sender as TPanel).width div 2;
  Self.Height:= Panel4.Height + Panel4.Top + 28;
end;

procedure TFDlgCote.btnSaveClick(Sender: TObject);
begin
  if Length(EOrgName.Text) < 4 then raise Exception.Create('公棚名称不能为空，数据无效。');
  if FrameLonLon.Value <= 0 then raise Exception.Create('纬度必读录入。');
  if FrameLonLon.Value <= 0 then raise Exception.Create('经度必读录入。');
  if SelfEdited then begin
    FormToInfo(FCoteDB.FCoteInfo);
    FCoteDB.FCoteInfo.UpdateData(FCoteDB.ADOConn)
  end;
//  FCoteDB.FCoteInfo.Status:= '编辑';
//  if FCoteDB.FCoteInfo.Loaded then FCoteDB.FCoteInfo.UpdateData(FCoteDB.ADOConn)
//  else FCoteDB.FCoteInfo.AppendData(FCoteDB.ADOConn);
//  FCoteDB.FCoteInfo.Loaded:= True;
//  if FCoteDB.FCoteInfo.Loaded then ModalResult:= mrOK;
end;

procedure TFDlgCote.EStateChange(Sender: TObject);
begin
  if EState.ItemIndex >= 0 then
    FCoteDB.GetSqlDropList('SELECT ID,ItemName FROM SYS_Zone WHERE PID=' +
      IntToStr(Integer(EState.Items.Objects[EState.ItemIndex])), ECity, '');
end;

end.
