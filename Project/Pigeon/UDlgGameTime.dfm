object FDlgGameTime: TFDlgGameTime
  AlignWithMargins = True
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  BorderWidth = 4
  Caption = 'FDlgGameTime'
  ClientHeight = 424
  ClientWidth = 424
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object lbPrompty: TLabel
    Left = 0
    Top = 0
    Width = 424
    Height = 54
    Align = alTop
    Caption = 
      '        '#27492#26102#38388#20026#27604#36187#30340#24320#31548#26102#38388#65292#24182#23558#20316#20026#20998#36895#21644#25104#32489#30340#35745#26102#22522#30784#12290#19968#26086#30830#35748#65292#31995#32479#20170#21518#23558#19981#20801#35768#36827#34892#20462#25913#12290#22240#27492#35831#24910#37325#26680#23454#65292#28982#21518#20877 +
      #30830#35748#65292#21542#21017#21518#26524#33258#36127#12290
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    Layout = tlCenter
    WordWrap = True
    ExplicitWidth = 416
  end
  object plCheck: TPanel
    Left = 0
    Top = 304
    Width = 424
    Height = 120
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    Visible = False
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 424
      Height = 49
      Align = alTop
      Shape = bsFrame
      Style = bsRaised
      ExplicitLeft = 8
      ExplicitTop = 40
      ExplicitWidth = 362
    end
    object Label7: TLabel
      Left = 142
      Top = 6
      Width = 60
      Height = 13
      Caption = #31995#32479#26102#38388#65306
    end
    object LNow: TLabel
      Left = 206
      Top = 6
      Width = 32
      Height = 15
      Caption = 'LNow'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbDayValue: TLabel
      Left = 54
      Top = 30
      Width = 32
      Height = 15
      Alignment = taRightJustify
      Caption = 'LNow'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbHourValue: TLabel
      Left = 126
      Top = 30
      Width = 32
      Height = 15
      Alignment = taRightJustify
      Caption = 'LNow'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbMinuteValue: TLabel
      Left = 210
      Top = 30
      Width = 32
      Height = 15
      Alignment = taRightJustify
      Caption = 'LNow'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 94
      Top = 30
      Width = 12
      Height = 13
      Caption = #22825
    end
    object Label12: TLabel
      Left = 166
      Top = 30
      Width = 24
      Height = 13
      Caption = #23567#26102
    end
    object Label13: TLabel
      Left = 248
      Top = 31
      Width = 24
      Height = 13
      Caption = #20998#38047
    end
    object lbSecondValue: TLabel
      Left = 290
      Top = 30
      Width = 32
      Height = 15
      Alignment = taRightJustify
      Caption = 'LNow'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object Label15: TLabel
      Left = 330
      Top = 30
      Width = 12
      Height = 13
      Caption = #31186
    end
    object Label3: TLabel
      Left = 0
      Top = 49
      Width = 424
      Height = 18
      Align = alTop
      Alignment = taCenter
      Caption = #36825#26159#26368#21518#30340#30830#35748#65292#35831#26680#23454#21518#30830#35748#12290
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Layout = tlCenter
      WordWrap = True
      ExplicitWidth = 240
    end
    object Label10: TLabel
      Left = 32
      Top = 6
      Width = 36
      Height = 13
      Caption = #20498#35745#26102
    end
    object btnOk: TBitBtn
      Left = 265
      Top = 91
      Width = 75
      Height = 25
      Caption = #23436#25104
      Default = True
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      TabOrder = 0
      OnClick = btnOkClick
    end
    object btnCancel2: TBitBtn
      Left = 346
      Top = 91
      Width = 75
      Height = 25
      Caption = #25918#24323
      Kind = bkCancel
      NumGlyphs = 2
      TabOrder = 1
    end
  end
  object plInfo: TPanel
    Left = 0
    Top = 54
    Width = 424
    Height = 41
    Align = alTop
    BevelInner = bvLowered
    TabOrder = 1
    object Label4: TLabel
      AlignWithMargins = True
      Left = 5
      Top = 5
      Width = 60
      Height = 31
      Align = alLeft
      Caption = #36187#20107#21517#31216#65306
      Layout = tlCenter
      ExplicitHeight = 13
    end
    object LGameName: TLabel
      AlignWithMargins = True
      Left = 71
      Top = 5
      Width = 75
      Height = 31
      Align = alLeft
      Caption = 'LGameName'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Layout = tlCenter
      ExplicitHeight = 15
    end
    object Label5: TLabel
      AlignWithMargins = True
      Left = 152
      Top = 5
      Width = 60
      Height = 31
      Align = alLeft
      Caption = #27604#36187#36317#31163#65306
      Layout = tlCenter
      ExplicitHeight = 13
    end
    object LGameLong: TLabel
      AlignWithMargins = True
      Left = 218
      Top = 5
      Width = 69
      Height = 31
      Align = alLeft
      Caption = 'LGameLong'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Layout = tlCenter
      ExplicitHeight = 15
    end
    object Label6: TLabel
      AlignWithMargins = True
      Left = 293
      Top = 5
      Width = 60
      Height = 31
      Align = alLeft
      Caption = #36187#20107#31867#22411#65306
      Layout = tlCenter
      ExplicitHeight = 13
    end
    object LGameKind: TLabel
      AlignWithMargins = True
      Left = 359
      Top = 5
      Width = 66
      Height = 31
      Align = alLeft
      Caption = 'LGameKind'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Layout = tlCenter
      ExplicitHeight = 15
    end
  end
  object plTimer: TPanel
    Left = 0
    Top = 95
    Width = 424
    Height = 114
    Align = alTop
    BevelOuter = bvNone
    Caption = 'plTimer'
    ShowCaption = False
    TabOrder = 2
    object Label1: TLabel
      Left = 133
      Top = 2
      Width = 48
      Height = 13
      Caption = #25918#39134#26085#26399
    end
    object Label2: TLabel
      Left = 134
      Top = 47
      Width = 48
      Height = 13
      Caption = #25918#39134#26102#38388
    end
    object btnSumTime: TSpeedButton
      Left = 331
      Top = 17
      Width = 23
      Height = 22
      Hint = #35745#31639#26085#20986#26085#33853#26102#38388
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00337000000000
        73333337777777773F333308888888880333337F3F3F3FFF7F33330808089998
        0333337F737377737F333308888888880333337F3F3F3F3F7F33330808080808
        0333337F737373737F333308888888880333337F3F3F3F3F7F33330808080808
        0333337F737373737F333308888888880333337F3F3F3F3F7F33330808080808
        0333337F737373737F333308888888880333337F3FFFFFFF7F33330800000008
        0333337F7777777F7F333308000E0E080333337F7FFFFF7F7F33330800000008
        0333337F777777737F333308888888880333337F333333337F33330888888888
        03333373FFFFFFFF733333700000000073333337777777773333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = btnSumTimeClick
    end
    object Label16: TLabel
      Left = 235
      Top = 48
      Width = 60
      Height = 13
      Caption = #26085#33853#26102#38388#65306
    end
    object Label8: TLabel
      Left = 235
      Top = 1
      Width = 60
      Height = 13
      Caption = #26085#20986#26102#38388#65306
    end
    object Label17: TLabel
      Left = 10
      Top = 91
      Width = 60
      Height = 13
      Caption = #27979#31639#31354#36317#65306
    end
    object LCalcLong: TLabel
      Left = 76
      Top = 91
      Width = 7
      Height = 15
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    inline FrameFrameLon: TFrameLonLat
      Left = 5
      Top = 0
      Width = 121
      Height = 41
      AutoSize = True
      TabOrder = 0
      OnExit = FrameFrameLonExit
      ExplicitLeft = 5
      ExplicitWidth = 121
      ExplicitHeight = 41
      inherited EC: TEdit
        Width = 39
        Height = 20
        ExplicitWidth = 39
      end
      inherited EF: TEdit
        Left = 39
        Height = 20
        ExplicitLeft = 39
      end
      inherited ES: TEdit
        Left = 80
        Height = 20
        ExplicitLeft = 80
        ExplicitTop = 21
        ExplicitHeight = 21
      end
      inherited Panel1: TPanel
        Width = 121
        ExplicitWidth = 121
        inherited Label1: TLabel
          Width = 22
          Height = 21
          Caption = #32463#24230
          ExplicitWidth = 24
        end
        inherited rbS: TRadioButton
          Left = 88
          ExplicitLeft = 88
        end
        inherited rbF: TRadioButton
          Left = 55
          ExplicitLeft = 55
        end
        inherited rbC: TRadioButton
          Left = 22
          ExplicitLeft = 22
        end
      end
    end
    inline FrameFrameLat: TFrameLonLat
      Left = 6
      Top = 44
      Width = 121
      Height = 41
      AutoSize = True
      TabOrder = 1
      ExplicitLeft = 6
      ExplicitTop = 44
      ExplicitWidth = 121
      ExplicitHeight = 41
      inherited EC: TEdit
        Width = 39
        Height = 20
        ExplicitWidth = 39
      end
      inherited EF: TEdit
        Left = 39
        Height = 20
        ExplicitLeft = 39
      end
      inherited ES: TEdit
        Left = 80
        Height = 20
        ExplicitLeft = 80
        ExplicitTop = 21
        ExplicitHeight = 21
      end
      inherited Panel1: TPanel
        Width = 121
        ExplicitWidth = 121
        inherited Label1: TLabel
          Width = 22
          Height = 21
          Caption = #32428#24230
          ExplicitWidth = 24
        end
        inherited rbS: TRadioButton
          Left = 88
          ExplicitLeft = 88
        end
        inherited rbF: TRadioButton
          Left = 55
          ExplicitLeft = 55
        end
        inherited rbC: TRadioButton
          Left = 22
          ExplicitLeft = 22
        end
      end
    end
    object EDate: TDateTimePicker
      Left = 132
      Top = 18
      Width = 97
      Height = 23
      Date = 38913.000000000000000000
      Time = 0.440023148148611700
      TabOrder = 2
    end
    object ETime: TDateTimePicker
      Left = 132
      Top = 64
      Width = 97
      Height = 23
      Date = 38913.000000000000000000
      Time = 0.440162395832885500
      Kind = dtkTime
      TabOrder = 3
    end
    object dtSunRaise: TDateTimePicker
      Left = 235
      Top = 17
      Width = 97
      Height = 23
      Date = 38913.000000000000000000
      Time = 0.440162395832885500
      Kind = dtkTime
      TabOrder = 4
    end
    object dtSunDown: TDateTimePicker
      Left = 235
      Top = 64
      Width = 97
      Height = 23
      Date = 38913.000000000000000000
      Time = 0.440162395832885500
      Kind = dtkTime
      TabOrder = 5
    end
  end
  object plWeather: TPanel
    Left = 0
    Top = 209
    Width = 424
    Height = 58
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 3
    object Label9: TLabel
      Left = 129
      Top = 9
      Width = 24
      Height = 13
      Caption = #39118#21521
    end
    object Label18: TLabel
      Left = 129
      Top = 36
      Width = 24
      Height = 13
      Caption = #28287#24230
    end
    object Label19: TLabel
      Left = 254
      Top = 9
      Width = 24
      Height = 13
      Caption = #39118#21147
    end
    object Label20: TLabel
      Left = 341
      Top = 36
      Width = 15
      Height = 13
      Caption = ' '#8451
    end
    object Label21: TLabel
      Left = 254
      Top = 36
      Width = 24
      Height = 13
      Caption = #28201#24230
    end
    object EWind: TComboBox
      Left = 159
      Top = 6
      Width = 72
      Height = 21
      ItemIndex = 0
      TabOrder = 0
      Text = #19996#39118
      Items.Strings = (
        #19996#39118
        #19996#21335#39118
        #21335#39118
        #35199#21335#39118
        #35199#39118
        #35199#21271#39118
        #21271#39118
        #19996#21271#39118)
    end
    object EWindPower: TComboBox
      Left = 284
      Top = 6
      Width = 73
      Height = 21
      ItemIndex = 0
      TabOrder = 1
      Text = '0'
      Items.Strings = (
        '0'
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12'
        '13')
    end
    object ETemperature: TEdit
      Left = 284
      Top = 33
      Width = 60
      Height = 21
      TabOrder = 2
      Text = '20'
    end
    object btnWeather: TButton
      Left = 8
      Top = 6
      Width = 115
      Height = 26
      Caption = #27668#35937#20449#24687
      TabOrder = 3
      OnClick = btnWeatherClick
    end
    object EHumidity: TEdit
      Left = 159
      Top = 33
      Width = 72
      Height = 21
      TabOrder = 4
      Text = '20'
    end
  end
  object plBtn1: TPanel
    Left = 0
    Top = 267
    Width = 424
    Height = 32
    Align = alTop
    BevelOuter = bvNone
    Caption = 'plBtn1'
    ShowCaption = False
    TabOrder = 4
    object lbArea: TLabel
      Left = 0
      Top = 0
      Width = 115
      Height = 32
      Align = alLeft
      AutoSize = False
      WordWrap = True
      ExplicitLeft = 8
      ExplicitTop = -7
      ExplicitHeight = 39
    end
    object btnReady: TBitBtn
      AlignWithMargins = True
      Left = 265
      Top = 3
      Width = 75
      Height = 26
      Align = alRight
      Caption = #30830#23450
      Default = True
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      TabOrder = 0
      OnClick = btnReadyClick
    end
    object btnCancel1: TBitBtn
      AlignWithMargins = True
      Left = 346
      Top = 3
      Width = 75
      Height = 26
      Align = alRight
      Caption = #25918#24323
      Kind = bkCancel
      NumGlyphs = 2
      TabOrder = 1
    end
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 8
    Top = 356
  end
  object IdHTTP1: TIdHTTP
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 64
    Top = 360
  end
  object DQWeather: TADOQuery
    Connection = FCoteDB.ADOConn
    Parameters = <
      item
        Name = 'LN'
        Size = -1
        Value = Null
      end
      item
        Name = 'LM'
        Size = -1
        Value = Null
      end
      item
        Name = 'LS'
        Size = -1
        Value = Null
      end
      item
        Name = 'TN'
        Size = -1
        Value = Null
      end
      item
        Name = 'TM'
        Size = -1
        Value = Null
      end
      item
        Name = 'TS'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT TOP 1 ABS(Lon - :LN)*3600 + ABS(LonM - :LM)*60 + ABS(LonS' +
        ' - :LS) +'
      
        '  ABS(Lat - :TN)*3600 + ABS(LatM - :TM)*60 + ABS(LatS - :TS), ID' +
        ', WeatherCode, Code, ItemName,'
      '  Lon+LonM/60+LonS/3600 AS LonV,Lat+LatM/60+LatS/3600 AS LatV'
      'FROM PGN_Zone'
      'WHERE WeatherCode <> '#39#39' AND Lon>0'
      'ORDER BY 1')
    Left = 152
    Top = 280
  end
end
