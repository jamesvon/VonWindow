unit UDlgGameTime;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, DateUtils, UFrameLonLat, DB,
  ADODB, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP;

type
  TFDlgGameTime = class(TForm)
    lbPrompty: TLabel;
    plCheck: TPanel;
    Label7: TLabel;
    LNow: TLabel;
    lbDayValue: TLabel;
    lbHourValue: TLabel;
    lbMinuteValue: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    lbSecondValue: TLabel;
    Label15: TLabel;
    Label3: TLabel;
    Bevel1: TBevel;
    Label10: TLabel;
    btnOk: TBitBtn;
    btnCancel2: TBitBtn;
    plInfo: TPanel;
    Label4: TLabel;
    LGameName: TLabel;
    Label5: TLabel;
    LGameLong: TLabel;
    Label6: TLabel;
    LGameKind: TLabel;
    Timer1: TTimer;
    IdHTTP1: TIdHTTP;
    DQWeather: TADOQuery;
    plTimer: TPanel;
    plWeather: TPanel;
    FrameFrameLon: TFrameLonLat;
    FrameFrameLat: TFrameLonLat;
    Label1: TLabel;
    Label2: TLabel;
    btnSumTime: TSpeedButton;
    Label16: TLabel;
    Label8: TLabel;
    EDate: TDateTimePicker;
    ETime: TDateTimePicker;
    dtSunRaise: TDateTimePicker;
    dtSunDown: TDateTimePicker;
    Label9: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    EWind: TComboBox;
    EWindPower: TComboBox;
    ETemperature: TEdit;
    btnWeather: TButton;
    EHumidity: TEdit;
    plBtn1: TPanel;
    btnReady: TBitBtn;
    btnCancel1: TBitBtn;
    Label17: TLabel;
    LCalcLong: TLabel;
    lbArea: TLabel;
    procedure btnReadyClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FrameFrameLonExit(Sender: TObject);
    procedure btnSumTimeClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnWeatherClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
    function GetFlyTime: TDatetime;
  public
    { Public declarations }
    property FlyTime: TDatetime read GetFlyTime;
  end;

var
  FDlgGameTime: TFDlgGameTime;

implementation

uses UVonSystemFuns, UCoteDB, UDlgSunTime, USuperObject;

{$R *.dfm}

procedure TFDlgGameTime.FormCreate(Sender: TObject);
begin
  LNow.Caption := DateTimeToStr(Now);
  plCheck.Visible := False;
  Self.Height := lbPrompty.Height + plTimer.Height + plWeather.Height +
    plInfo.Height + plBtn1.Height + Height - Self.ClientHeight;
  EDate.DateTime := Now;
  ETime.DateTime := Now;
end;

procedure TFDlgGameTime.FormDestroy(Sender: TObject);
begin
end;

(* auxiliary functions *)

procedure TFDlgGameTime.btnSumTimeClick(Sender: TObject);
begin // 计算日出日落时间
  with TFDlgSunTime.Create(nil) do
    try
      SunRaise := Self.dtSunRaise.Time;
      SunDown := Self.dtSunDown.Time;
      DateTimePicker1.Date := EDate.Date;
      ShowModal;
      Self.dtSunRaise.DateTime := SunRaise;
      Self.dtSunDown.DateTime := SunDown;
    finally
      Free;
    end;
end;

procedure TFDlgGameTime.FrameFrameLonExit(Sender: TObject);
begin
  LCalcLong.Caption := FloatToStr(FCoteDB.CalcUliage(FCoteDB.OrgInfo.Lon +
    FCoteDB.OrgInfo.LonM / 60 + FCoteDB.OrgInfo.LonS / 3600,
    FCoteDB.OrgInfo.Lat + FCoteDB.OrgInfo.LatM / 60 + FCoteDB.OrgInfo.LatS / 3600,
    FrameFrameLon.Value, FrameFrameLat.Value) / 1000);
end;

function TFDlgGameTime.GetFlyTime: TDatetime;
begin
  Result := DateOf(EDate.Date) + TimeOf(ETime.Time);
end;

(* button events *)

procedure TFDlgGameTime.btnWeatherClick(Sender: TObject);
var
  URL: Ansistring;
  vJson: ISuperObject;
  szNode: TTreeNode;
begin
  with DQWeather do
    try
      Parameters[0].Value := FrameFrameLon.L;
      Parameters[1].Value := FrameFrameLon.M;
      Parameters[2].Value := FrameFrameLon.S;
      Parameters[3].Value := FrameFrameLat.L;
      Parameters[4].Value := FrameFrameLat.M;
      Parameters[5].Value := FrameFrameLat.S;
      Open;
      URL := 'http://www.weather.com.cn/data/sk/' + FieldByName('WeatherCode')
        .AsString + '.html';
      // {"weatherinfo":{"city":"望江","cityid":"101220607","temp":"11","WD":"东风","WS":"2级","SD":"76%","WSE":"2","time":"20:10","isRadar":"0","Radar":""}}
      vJson := SO(UrlGetStr(URL));
      if vJson.N['weatherinfo'].DataType <> stNull then
      begin
        EWind.Text := vJson.N['weatherinfo.WD'].AsString;
        EWindPower.Text := vJson.N['weatherinfo.WS'].AsString;
        ETemperature.Text := vJson.N['weatherinfo.temp'].AsString;
        EHumidity.Text := vJson.N['weatherinfo.SD'].AsString;
      end;
      lbArea.Caption := Format('放飞地在%s[%s](%s,%s)附近。',
        [FieldByName('ItemName').AsString, FieldByName('Code').AsString,
        FieldByName('LonV').AsString, FieldByName('LatV').AsString]);
    finally
      Close;
    end;
end;

procedure TFDlgGameTime.btnOkClick(Sender: TObject);
var
  StartDatetime: TDatetime;
begin
  (* 开笼时间保护 *)
  StartDatetime := FlyTime;
  if StartDatetime < Now - 2 then
  begin
    ShowMessage('开笼时间太早，系统不允许。');
    Exit;
  end;
  if StartDatetime > Now + 3 then
  begin
    ShowMessage('开笼时间太晚，系统不允许。');
    Exit;
  end;
  if StartDatetime < IncMinute(Now, -30) then
    if MessageDlg('您设置的这个时间（' + DateTimeToStr(StartDatetime) +
      '）太早了，一旦确定以后将不能进行修改，是否确认?', mtInformation, [mbOK, mbCancel], 0)
      = mrCancel then
      Exit;
  if TimeOf(ETime.Time) < IncMinute(TimeOf(dtSunRaise.Time), -30) then
    raise Exception.Create('您设置的开赛时间太早，早于该赛事的日出时间太多，因此拒绝开笼。');
  if TimeOf(ETime.Time) > IncMinute(TimeOf(dtSunDown.Time), 30) then
    raise Exception.Create('您设置的开赛时间太晚，晚于该赛事的日落时间太多，因此拒绝开笼。');
  ModalResult := mrOK;
end;

procedure TFDlgGameTime.btnReadyClick(Sender: TObject);
begin
  plCheck.Visible := true;
  Height := Height + plCheck.Height;
end;

procedure TFDlgGameTime.Timer1Timer(Sender: TObject);
begin
  LNow.Caption := DateTimeToStr(Now);
  lbDayValue.Caption := IntToStr(Trunc(Now - FlyTime));
  lbHourValue.Caption := IntToStr(HoursBetween(Now, FlyTime) mod 24);
  lbMinuteValue.Caption := IntToStr(MinutesBetween(Now, FlyTime) mod 60);
  lbSecondValue.Caption := IntToStr(SecondsBetween(Now, FlyTime) mod 60);
end;

end.
