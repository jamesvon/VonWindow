object FDlgInputGBCode: TFDlgInputGBCode
  Left = 0
  Top = 0
  Caption = 'FDlgInputGBCode'
  ClientHeight = 267
  ClientWidth = 567
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    567
    267)
  PixelsPerInch = 96
  TextHeight = 13
  object lbGBCode: TLabel
    AlignWithMargins = True
    Left = 8
    Top = 8
    Width = 48
    Height = 13
    Caption = #32479#19968#29615#21495
    Layout = tlCenter
  end
  object Label1: TLabel
    Left = 8
    Top = 56
    Width = 48
    Height = 13
    Caption = #40509#20027#22995#21517
  end
  object Label2: TLabel
    Left = 8
    Top = 78
    Width = 48
    Height = 13
    Caption = #40509#20027#32534#21495
  end
  object Label3: TLabel
    Left = 8
    Top = 101
    Width = 48
    Height = 13
    Caption = #25152#22312#30465#20221
  end
  object Label4: TLabel
    Left = 8
    Top = 124
    Width = 48
    Height = 13
    Caption = #25152#22312#22478#24066
  end
  object Label5: TLabel
    Left = 8
    Top = 147
    Width = 48
    Height = 13
    Caption = #25152#22312#21306#21439
  end
  object Label6: TLabel
    Left = 8
    Top = 170
    Width = 48
    Height = 13
    Caption = #32479#19968#29615#21495
  end
  object Label7: TLabel
    Left = 8
    Top = 193
    Width = 48
    Height = 13
    Caption = #32701'        '#33394
  end
  object Label8: TLabel
    Left = 8
    Top = 216
    Width = 48
    Height = 13
    Caption = #30524'        '#30722
  end
  object Label9: TLabel
    Left = 10
    Top = 239
    Width = 48
    Height = 13
    Caption = #20844'        '#27597
  end
  object DBText1: TDBText
    Left = 64
    Top = 56
    Width = 65
    Height = 17
    DataField = 'MemberName'
    DataSource = DSPigeon
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object DBText2: TDBText
    Left = 64
    Top = 78
    Width = 65
    Height = 17
    DataField = 'MemberNo'
    DataSource = DSPigeon
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object DBText3: TDBText
    Left = 64
    Top = 101
    Width = 65
    Height = 17
    DataField = 'State'
    DataSource = DSPigeon
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object DBText4: TDBText
    Left = 64
    Top = 124
    Width = 65
    Height = 17
    DataField = 'City'
    DataSource = DSPigeon
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object DBText5: TDBText
    Left = 64
    Top = 147
    Width = 65
    Height = 17
    DataField = 'County'
    DataSource = DSPigeon
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object DBText6: TDBText
    Left = 64
    Top = 170
    Width = 65
    Height = 17
    DataField = 'GBCode'
    DataSource = DSPigeon
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object DBText7: TDBText
    Left = 64
    Top = 193
    Width = 65
    Height = 17
    DataField = 'Feather'
    DataSource = DSPigeon
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object DBText8: TDBText
    Left = 64
    Top = 216
    Width = 65
    Height = 17
    DataField = 'Eye'
    DataSource = DSPigeon
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object DBText9: TDBText
    Left = 64
    Top = 239
    Width = 65
    Height = 17
    DataField = 'Gender'
    DataSource = DSPigeon
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object EGBCode: TEdit
    Left = 8
    Top = 24
    Width = 137
    Height = 21
    TabOrder = 0
    OnKeyDown = EGBCodeKeyDown
    OnKeyUp = EGBCodeKeyUp
  end
  object btnOK: TBitBtn
    Left = 403
    Top = 234
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Enabled = False
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 1
  end
  object BitBtn2: TBitBtn
    Left = 484
    Top = 234
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 2
  end
  object DBGrid1: TDBGrid
    Left = 151
    Top = 8
    Width = 408
    Height = 220
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = DSList
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'MemberName'
        Title.Caption = #40509#20027#22995#21517
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'GBCode'
        Title.Caption = #32479#19968#29615#21495
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'JZCode'
        Title.Caption = #30005#23376#29615#21495
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Feather'
        Title.Caption = #32701#33394
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Eye'
        Title.Caption = #30524#30722
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Gender'
        Title.Caption = #20844#27597
        Visible = True
      end>
  end
  object DQPigeon: TADOQuery
    Connection = FCoteDB.ADOConn
    CursorType = ctStatic
    DataSource = DSList
    Parameters = <
      item
        Name = 'PID'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT M.MemberName,M.MemberNo,M.State,M.City,M.County,P.GBCode,' +
        'P.JZCode,P.Feather,P.Eye,P.Gender'
      'FROM Join_Pigeon P'
      'JOIN Join_Member M ON M.ID=P.MemberIdx'
      'WHERE P.ID=:PID')
    Left = 176
    Top = 80
  end
  object DQList: TADOQuery
    Connection = FCoteDB.ADOConn
    Parameters = <
      item
        Name = 'GBC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT P.ID AS PID,M.MemberName,P.GBCode,P.JZCode,P.Feather,P.Ey' +
        'e,P.Gender'
      'FROM Join_Pigeon P'
      'JOIN Join_Member M ON M.ID=P.MemberIdx'
      'WHERE GBCode LIKE '#39'%'#39' + :GBC + '#39'%'#39)
    Left = 232
    Top = 80
  end
  object DSList: TDataSource
    DataSet = DQList
    Left = 232
    Top = 136
  end
  object DSPigeon: TDataSource
    DataSet = DQPigeon
    Left = 176
    Top = 136
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 500
    OnTimer = Timer1Timer
    Left = 416
    Top = 80
  end
end
