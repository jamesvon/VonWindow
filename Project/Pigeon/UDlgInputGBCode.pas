unit UDlgInputGBCode;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls, Buttons, UCoteDB, Grids, DBGrids, DBCtrls,
  UCoteInfo, Vcl.ExtCtrls;

type
  TFDlgInputGBCode = class(TForm)
    EGBCode: TEdit;
    lbGBCode: TLabel;
    btnOK: TBitBtn;
    BitBtn2: TBitBtn;
    DQPigeon: TADOQuery;
    DQList: TADOQuery;
    DSList: TDataSource;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    DBText5: TDBText;
    DBText6: TDBText;
    DBText7: TDBText;
    DBText8: TDBText;
    DBText9: TDBText;
    DSPigeon: TDataSource;
    Timer1: TTimer;
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure EGBCodeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EGBCodeKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    MyGB: TGBCode;
    FAutoCheckGBCode: Boolean;
  public
    { Public declarations }
  end;

var
  FDlgInputGBCode: TFDlgInputGBCode;

implementation

uses UVonSystemFuns;

{$R *.dfm}

procedure TFDlgInputGBCode.DBGrid1DblClick(Sender: TObject);
begin
  EGBCode.Text:= DQList.FieldByName('GBCode').AsString;
  btnOk.Enabled:= True;
end;

procedure TFDlgInputGBCode.EGBCodeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  Timer1.Enabled:= False;
end;

procedure TFDlgInputGBCode.EGBCodeKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  Timer1.Enabled:= True;
end;

procedure TFDlgInputGBCode.FormCreate(Sender: TObject);
begin
  MyGB:= TGBCode.Create;
  FAutoCheckGBCode := ReadAppConfig('SYSTEM', 'AutoCheckGBCode') = '1';
end;

procedure TFDlgInputGBCode.FormDestroy(Sender: TObject);
begin
  MyGB.Free;
end;

procedure TFDlgInputGBCode.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:= False;
  btnOK.Enabled := False;
  DQPigeon.Close;
  with DQList do
  begin
    Close;
    Parameters[0].Value := EGBCode.Text;
    Open;
    DQPigeon.Open;
    btnOK.Enabled := RecordCount > 0;
    if RecordCount = 1 then DBGrid1DblClick(nil);
  end;
end;

end.
