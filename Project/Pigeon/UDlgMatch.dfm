object FDlgMatch: TFDlgMatch
  Left = 0
  Top = 0
  Caption = 'FDlgMatch'
  ClientHeight = 578
  ClientWidth = 912
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 145
    Height = 543
    Align = alLeft
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    object Label4: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 137
      Height = 13
      Align = alTop
      Caption = #36187#20107#21517#31216
      ExplicitWidth = 48
    end
    object Label5: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 212
      Width = 137
      Height = 13
      Align = alTop
      Caption = #25918#39134#22320
      ExplicitWidth = 36
    end
    object Label6: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 258
      Width = 137
      Height = 13
      Align = alTop
      Caption = #21496#25918#20154
      ExplicitWidth = 36
    end
    object lbKind: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 304
      Width = 137
      Height = 13
      Align = alTop
      Caption = #36187#20107#31867#22411
      ExplicitWidth = 48
    end
    object Label14: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 350
      Width = 137
      Height = 13
      Align = alTop
      Caption = #36187#20107#30005#35805
      ExplicitWidth = 48
    end
    object Label7: TLabel
      AlignWithMargins = True
      Left = 4
      Top = 396
      Width = 137
      Height = 13
      Align = alTop
      Caption = #36187#20107#36317#31163#65288#20844#37324#65289
      ExplicitWidth = 96
    end
    object EMatchName: TEdit
      AlignWithMargins = True
      Left = 4
      Top = 23
      Width = 137
      Height = 21
      Align = alTop
      TabOrder = 0
    end
    object EGameAddress: TEdit
      AlignWithMargins = True
      Left = 4
      Top = 231
      Width = 137
      Height = 21
      Align = alTop
      TabOrder = 1
    end
    object EManager: TEdit
      AlignWithMargins = True
      Left = 4
      Top = 277
      Width = 137
      Height = 21
      Align = alTop
      TabOrder = 2
    end
    object EKind: TComboBox
      AlignWithMargins = True
      Left = 4
      Top = 323
      Width = 137
      Height = 21
      Align = alTop
      ItemIndex = 0
      TabOrder = 3
      Text = #27604#36187
      Items.Strings = (
        #27604#36187)
    end
    object ETelphone: TEdit
      AlignWithMargins = True
      Left = 4
      Top = 369
      Width = 137
      Height = 21
      Align = alTop
      TabOrder = 4
    end
    object EGameLong: TEdit
      AlignWithMargins = True
      Left = 4
      Top = 415
      Width = 137
      Height = 21
      Align = alTop
      TabOrder = 5
      Text = '0'
    end
    inline FrameLon: TFrameLonLat
      AlignWithMargins = True
      Left = 4
      Top = 442
      Width = 137
      Height = 47
      Align = alTop
      TabOrder = 6
      OnExit = FrameLonExit
      ExplicitLeft = 4
      ExplicitTop = 442
      ExplicitWidth = 137
      inherited EC: TEdit
        Width = 55
        ExplicitWidth = 55
      end
      inherited EF: TEdit
        Left = 55
        ExplicitLeft = 55
      end
      inherited ES: TEdit
        Left = 96
        ExplicitLeft = 96
        ExplicitTop = 21
        ExplicitHeight = 21
      end
      inherited Panel1: TPanel
        Width = 137
        ExplicitWidth = 137
        inherited Label1: TLabel
          Width = 38
          Height = 21
          Caption = #32463#24230
          ExplicitWidth = 24
        end
        inherited rbS: TRadioButton
          Left = 104
          ExplicitLeft = 104
        end
        inherited rbF: TRadioButton
          Left = 71
          ExplicitLeft = 71
        end
        inherited rbC: TRadioButton
          Left = 38
          ExplicitLeft = 38
        end
      end
    end
    inline FrameLat: TFrameLonLat
      AlignWithMargins = True
      Left = 4
      Top = 495
      Width = 137
      Height = 47
      Align = alTop
      TabOrder = 7
      OnExit = FrameLonExit
      ExplicitLeft = 4
      ExplicitTop = 495
      ExplicitWidth = 137
      inherited EC: TEdit
        Width = 55
        ExplicitWidth = 55
      end
      inherited EF: TEdit
        Left = 55
        ExplicitLeft = 55
      end
      inherited ES: TEdit
        Left = 96
        ExplicitLeft = 96
        ExplicitTop = 21
        ExplicitHeight = 21
      end
      inherited Panel1: TPanel
        Width = 137
        ExplicitWidth = 137
        inherited Label1: TLabel
          Width = 38
          Height = 21
          Caption = #32428#24230
          ExplicitWidth = 24
        end
        inherited rbS: TRadioButton
          Left = 104
          ExplicitLeft = 104
        end
        inherited rbF: TRadioButton
          Left = 71
          ExplicitLeft = 71
        end
        inherited rbC: TRadioButton
          Left = 38
          ExplicitLeft = 38
        end
      end
    end
    object ChkUploadCollection: TCheckBox
      AlignWithMargins = True
      Left = 4
      Top = 50
      Width = 137
      Height = 17
      Align = alTop
      Caption = #38598#40509#25104#32489#19978#20256
      TabOrder = 8
    end
    object ChkUploadCheckIn: TCheckBox
      AlignWithMargins = True
      Left = 4
      Top = 73
      Width = 137
      Height = 17
      Align = alTop
      Caption = #24402#24034#25104#32489#19978#20256
      TabOrder = 9
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 116
      Width = 143
      Height = 93
      Align = alTop
      Caption = #30701#20449#21457#36865#26465#20214
      TabOrder = 10
      Visible = False
      object chkSmsOrder: TCheckBox
        AlignWithMargins = True
        Left = 3
        Top = 22
        Width = 68
        Height = 17
        Caption = #21517#27425#26465#20214
        TabOrder = 0
      end
      object chkSmsKind: TCheckBox
        AlignWithMargins = True
        Left = 3
        Top = 45
        Width = 68
        Height = 17
        Caption = #29366#24577#26465#20214
        TabOrder = 1
      end
      object ESmsOrder: TSpinEdit
        Left = 70
        Top = 22
        Width = 68
        Height = 22
        Hint = #21069#20960#21517#21457#30701#20449#65292'0'#34920#31034#25152#26377
        MaxValue = 0
        MinValue = 0
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        Value = 0
      end
      object ESmsKind: TComboBox
        Left = 70
        Top = 45
        Width = 68
        Height = 21
        Style = csDropDownList
        TabOrder = 3
      end
      object chkSmsCash: TCheckBox
        AlignWithMargins = True
        Left = 3
        Top = 68
        Width = 68
        Height = 17
        Caption = #32564#36153#26465#20214
        TabOrder = 4
      end
      object ESmsCash: TComboBox
        Left = 70
        Top = 68
        Width = 68
        Height = 21
        Style = csDropDownList
        TabOrder = 5
      end
    end
    object chkWebChoose: TCheckBox
      AlignWithMargins = True
      Left = 4
      Top = 96
      Width = 137
      Height = 17
      Align = alTop
      Caption = #20801#35768#32593#19978#25351#23450
      TabOrder = 11
    end
  end
  object pcEditor: TPageControl
    Left = 145
    Top = 0
    Width = 767
    Height = 543
    ActivePage = TabSheet2
    Align = alClient
    DoubleBuffered = True
    ParentDoubleBuffered = False
    TabOrder = 1
    object TabSheet2: TTabSheet
      Caption = #31456#31243
      ImageIndex = 1
      inline FrameRichEditor1: TFrameRichEditor
        Left = 0
        Top = 0
        Width = 759
        Height = 515
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Default'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        ExplicitWidth = 759
        ExplicitHeight = 515
        inherited Ruler: TPanel
          Top = 52
          Width = 759
          ExplicitTop = 52
          ExplicitWidth = 759
          inherited Bevel1: TBevel
            Width = 759
            ExplicitWidth = 759
          end
        end
        inherited Editor: TRichEdit
          Top = 78
          Width = 759
          Height = 418
          ExplicitTop = 78
          ExplicitWidth = 759
          ExplicitHeight = 418
        end
        inherited StandardToolBar: TToolBar
          Width = 759
          Height = 52
          ExplicitWidth = 759
          ExplicitHeight = 52
          inherited FontSize: TEdit
            Width = 299
            ExplicitWidth = 299
          end
          inherited ToolButton2: TToolButton [12]
            Left = 4
            Wrap = True
            ExplicitLeft = 4
            ExplicitHeight = 30
          end
          inherited UpDown1: TUpDown [13]
            Left = 4
            Top = 30
            ExplicitLeft = 4
            ExplicitTop = 30
          end
          inherited BoldButton: TToolButton
            Left = 19
            Top = 30
            ExplicitLeft = 19
            ExplicitTop = 30
          end
          inherited ItalicButton: TToolButton
            Left = 42
            Top = 30
            ExplicitLeft = 42
            ExplicitTop = 30
          end
          inherited UnderlineButton: TToolButton
            Left = 65
            Top = 30
            ExplicitLeft = 65
            ExplicitTop = 30
          end
          inherited ToolButton16: TToolButton
            Left = 88
            Top = 30
            ExplicitLeft = 88
            ExplicitTop = 30
          end
          inherited LeftAlign: TToolButton
            Left = 96
            Top = 30
            ExplicitLeft = 96
            ExplicitTop = 30
          end
          inherited CenterAlign: TToolButton
            Left = 119
            Top = 30
            ExplicitLeft = 119
            ExplicitTop = 30
          end
          inherited RightAlign: TToolButton
            Left = 142
            Top = 30
            ExplicitLeft = 142
            ExplicitTop = 30
          end
          inherited ToolButton20: TToolButton
            Left = 165
            Top = 30
            ExplicitLeft = 165
            ExplicitTop = 30
          end
          inherited BulletsButton: TToolButton
            Left = 173
            Top = 30
            ExplicitLeft = 173
            ExplicitTop = 30
          end
          inherited ToolButton3: TToolButton
            Left = 196
            Top = 30
            ExplicitLeft = 196
            ExplicitTop = 30
          end
          inherited ToolButton4: TToolButton
            Left = 204
            Top = 30
            ExplicitLeft = 204
            ExplicitTop = 30
          end
          inherited ToolButton7: TToolButton
            Left = 227
            Top = 30
            ExplicitLeft = 227
            ExplicitTop = 30
          end
          inherited ColorBox2: TColorBox
            Left = 235
            Top = 30
            ExplicitLeft = 235
            ExplicitTop = 30
          end
          inherited ColorBox1: TColorBox
            Left = 327
            Top = 30
            ExplicitLeft = 327
            ExplicitTop = 30
          end
          inherited ToolButton6: TToolButton
            Left = 419
            Top = 30
            ExplicitLeft = 419
            ExplicitTop = 30
          end
          inherited EDateTime: TComboBox
            Left = 427
            Top = 30
            ExplicitLeft = 427
            ExplicitTop = 30
          end
        end
        inherited StatusBar: TStatusBar
          Top = 496
          Width = 759
          ExplicitTop = 496
          ExplicitWidth = 759
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = #35760#24405
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object lstNote: TMemo
        Left = 0
        Top = 0
        Width = 759
        Height = 515
        Align = alClient
        TabOrder = 0
      end
    end
    object TabSheet4: TTabSheet
      Caption = #25351#23450#40509#35774#32622
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object lstChoise: TValueListEditor
        Left = 0
        Top = 0
        Width = 759
        Height = 515
        Align = alClient
        KeyOptions = [keyEdit, keyAdd, keyDelete, keyUnique]
        TabOrder = 0
        TitleCaptions.Strings = (
          #25351#23450#40509#39033#30446
          #25351#23450#37329#39069)
        ColWidths = (
          150
          603)
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 543
    Width = 912
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'Panel5'
    ShowCaption = False
    TabOrder = 2
    object Button1: TButton
      AlignWithMargins = True
      Left = 663
      Top = 3
      Width = 120
      Height = 29
      Align = alRight
      Caption = #30830#23450
      Default = True
      ImageIndex = 19
      ImageMargins.Left = 5
      Images = FCoteDB.ImgButton
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      AlignWithMargins = True
      Left = 789
      Top = 3
      Width = 120
      Height = 29
      Align = alRight
      Cancel = True
      Caption = #25918#24323
      ImageIndex = 21
      ImageMargins.Left = 5
      Images = FCoteDB.ImgButton
      ModalResult = 2
      TabOrder = 1
    end
  end
end
