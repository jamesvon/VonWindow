unit UDlgMatch;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UFrameLonLat, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.Buttons, Vcl.ComCtrls, UFrameRichEditor, DB, Data.Win.ADODB, UCoteInfo,
  UCoteDB, UVonSystemFuns, UVonConfig, Vcl.Grids, Vcl.ValEdit, Vcl.Samples.Spin;

type
  TFDlgMatch = class(TForm)
    Panel1: TPanel;
    Label4: TLabel;
    EMatchName: TEdit;
    Label5: TLabel;
    EGameAddress: TEdit;
    Label6: TLabel;
    EManager: TEdit;
    lbKind: TLabel;
    EKind: TComboBox;
    Label14: TLabel;
    ETelphone: TEdit;
    Label7: TLabel;
    EGameLong: TEdit;
    pcEditor: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    lstNote: TMemo;
    TabSheet4: TTabSheet;
    Panel5: TPanel;
    FrameRichEditor1: TFrameRichEditor;
    FrameLon: TFrameLonLat;
    FrameLat: TFrameLonLat;
    Button1: TButton;
    Button2: TButton;
    lstChoise: TValueListEditor;
    ChkUploadCollection: TCheckBox;
    ChkUploadCheckIn: TCheckBox;
    GroupBox1: TGroupBox;
    chkSmsOrder: TCheckBox;
    chkSmsKind: TCheckBox;
    ESmsOrder: TSpinEdit;
    ESmsKind: TComboBox;
    chkSmsCash: TCheckBox;
    ESmsCash: TComboBox;
    chkWebChoose: TCheckBox;
    procedure FrameLonExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure FormToInfo(FInfo: TMatchInfo; ASynFlag: char);
    procedure InfoToForm(FInfo: TMatchInfo);
  end;

var
  FDlgMatch: TFDlgMatch;

implementation

uses DateUtils;

{$R *.dfm}

{ TForm1 }

procedure TFDlgMatch.FormCreate(Sender: TObject);
begin
  lbKind.Caption := FCoteDB.GetListOption('赛事类型', EKind.Items);
  lstChoise.Strings.Delimiter:= ',';
  FCoteDB.GetDropList('会员类型', ESmsKind, '');
  FCoteDB.GetDropList('缴费方式', ESmsCash, '');
  lstChoise.Strings.DelimitedText:= ReadAppConfig('FIELDS', 'DefaultChoice');
  EKind.Items.Insert(0, '训放');
end;

procedure TFDlgMatch.FormToInfo(FInfo: TMatchInfo; ASynFlag: char);
begin
  FInfo.MatchName:= EMatchName.Text;
  FInfo.GameAddress:= EGameAddress.Text;
  FInfo.Manager:= EManager.Text;
  FInfo.GameLong:= StrToFloat(EGameLong.Text);
  FInfo.Kind:= EKind.Text;
  FInfo.Telphone:= ETelphone.Text;
  FInfo.Info:= FrameRichEditor1.Editor.Lines.Text;
  FInfo.Note:= lstNote.Lines.Text;
  FInfo.Settings:= lstChoise.Strings.Text;
  FInfo.Lon:= FrameLon.Value;
  FInfo.Lat:= FrameLat.Value;
  FInfo.UploadSetting:= 0;
  if ChkUploadCollection.Checked then FInfo.UploadSetting:= 1;
  if ChkUploadCheckIn.Checked then FInfo.UploadSetting:= FInfo.UploadSetting + 2;
  if ChkWebChoose.Checked then FInfo.UploadSetting:= FInfo.UploadSetting + 4;
  if chkSmsOrder.Checked then FInfo.SMSCondition:= 'ORD(' + ESmsOrder.Text + ')' else FInfo.SMSCondition:= '';
  if chkSmsKind.Checked then FInfo.SMSCondition:= FInfo.SMSCondition + 'KIND(' + ESmsKind.Text + ')';
  if chkSmsCash.Checked then FInfo.SMSCondition:= FInfo.SMSCondition + 'CASH(' + ESmsCash.Text + ')';

  if not FInfo.CheckInfo then
    raise Exception.Create(LastInfo);
end;

procedure TFDlgMatch.FrameLonExit(Sender: TObject);
begin
  EGameLong.Text := FloatToStr(FCoteDB.CalcUliage(FCoteDB.FCoteInfo.Longitude,
    FCoteDB.FCoteInfo.Latitude, FrameLon.Value, FrameLat.Value) / 1000);
end;

procedure TFDlgMatch.InfoToForm(FInfo: TMatchInfo);
var
  S: string;
begin
  EMatchName.Text:= FInfo.MatchName;
  EGameAddress.Text:= FInfo.GameAddress;
  EManager.Text:= FInfo.Manager;
  EGameLong.Text:= FloatToStr(FInfo.GameLong);
  FVonSetting.Text[VSK_SEMICOLON]:= FInfo.SMSCondition;
  ChkUploadCollection.Checked:= FInfo.UploadSetting and 1 > 0;
  ChkUploadCheckIn.Checked:= FInfo.UploadSetting and 2 > 0;
  ChkWebChoose.Checked:= FInfo.UploadSetting and 4 > 0;
  S:= FVonSetting.NameValue['ORD'];
  if S <> '' then begin chkSmsOrder.Checked:= true; ESmsOrder.Text:= S; end;
  S:= FVonSetting.NameValue['KIND'];
  if S <> '' then begin chkSmsKind.Checked:= true; ESmsKind.ItemIndex:= ESmsKind.Items.IndexOf(S); end;
  S:= FVonSetting.NameValue['CASH'];
  if S <> '' then begin chkSmsCash.Checked:= true; ESmsCash.ItemIndex:= ESmsCash.Items.IndexOf(S); end;
  EKind.ItemIndex:= EKind.Items.IndexOf(FInfo.Kind);
  ETelphone.Text:= FInfo.Telphone;
  FrameRichEditor1.Editor.Lines.Text:= FInfo.Info;
  lstNote.Lines.Text:= FInfo.Note;
  FrameLon.Value:= FInfo.Lon;
  FrameLat.Value:= FInfo.Lat;
  lstChoise.Strings.Text:= FInfo.Settings;
end;

end.
