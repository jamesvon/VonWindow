object FDlgMember: TFDlgMember
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #40509#20027
  ClientHeight = 416
  ClientWidth = 405
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 0
    Width = 405
    Height = 377
    Align = alTop
    Shape = bsBottomLine
    ExplicitLeft = -3
    ExplicitTop = -3
  end
  object lblMemberName: TLabel
    Left = 48
    Top = 20
    Width = 24
    Height = 13
    Caption = #22995#21517
  end
  object lblMemberNo: TLabel
    Left = 24
    Top = 47
    Width = 48
    Height = 13
    Caption = #40509#20027#32534#21495
  end
  object lblAssociationNo: TLabel
    Left = 24
    Top = 74
    Width = 48
    Height = 13
    Caption = #21327#20250#32534#21495
  end
  object lblShortName: TLabel
    Left = 36
    Top = 101
    Width = 36
    Height = 13
    Caption = #30701#21517#31216
  end
  object lblKind: TLabel
    Left = 239
    Top = 20
    Width = 24
    Height = 13
    Caption = #31867#22411
  end
  object lblLinker: TLabel
    Left = 239
    Top = 157
    Width = 24
    Height = 13
    Caption = 'EMail'
  end
  object lbCash: TLabel
    Left = 215
    Top = 75
    Width = 48
    Height = 13
    Caption = #32564#36153#37329#39069
  end
  object lbCashDate: TLabel
    Left = 215
    Top = 102
    Width = 48
    Height = 13
    Caption = #32564#36153#26085#26399
  end
  object lblCashKind: TLabel
    Left = 215
    Top = 47
    Width = 48
    Height = 13
    Caption = #32564#36153#26041#24335
  end
  object lblState: TLabel
    Left = 24
    Top = 155
    Width = 48
    Height = 13
    Caption = #25152#22312#30465#20221
  end
  object lblCity: TLabel
    Left = 24
    Top = 182
    Width = 48
    Height = 13
    Caption = #25152#22312#22478#24066
  end
  object lblCounty: TLabel
    Left = 24
    Top = 209
    Width = 48
    Height = 13
    Caption = #25152#22312#21306#21439
  end
  object lblAddress: TLabel
    Left = 48
    Top = 237
    Width = 24
    Height = 13
    Caption = #22320#22336
  end
  object lblManager: TLabel
    Left = 227
    Top = 183
    Width = 36
    Height = 13
    Caption = #31649#29702#20154
  end
  object lblTelphong: TLabel
    Left = 215
    Top = 210
    Width = 48
    Height = 13
    Caption = #32852#31995#30005#35805
  end
  object lbDisplayOrder: TLabel
    Left = 24
    Top = 129
    Width = 48
    Height = 13
    Caption = #26174#31034#24207#21495
  end
  object lblNote: TLabel
    Left = 48
    Top = 263
    Width = 24
    Height = 13
    Caption = #22791#27880
  end
  object EMemberName: TEdit
    Left = 78
    Top = 18
    Width = 116
    Height = 21
    TabOrder = 0
    OnChange = EMemberNameChange
  end
  object EMemberNo: TEdit
    Left = 78
    Top = 45
    Width = 116
    Height = 21
    TabOrder = 1
  end
  object EAssociationNo: TEdit
    Left = 78
    Top = 72
    Width = 116
    Height = 21
    TabOrder = 2
  end
  object EShortName: TEdit
    Left = 78
    Top = 99
    Width = 116
    Height = 21
    TabOrder = 3
  end
  object EEMail: TEdit
    Left = 269
    Top = 154
    Width = 116
    Height = 21
    TabOrder = 14
  end
  object ECash: TEdit
    Left = 269
    Top = 72
    Width = 60
    Height = 21
    TabOrder = 8
    Text = '0.00'
  end
  object ECashDate: TDateTimePicker
    Left = 269
    Top = 99
    Width = 116
    Height = 22
    Date = 1.000000000000000000
    Time = 0.970618090279458600
    DateFormat = dfLong
    TabOrder = 9
  end
  object EAddress: TEdit
    Left = 78
    Top = 235
    Width = 307
    Height = 21
    TabOrder = 17
  end
  object EManager: TEdit
    Left = 269
    Top = 181
    Width = 116
    Height = 21
    TabOrder = 15
  end
  object EMobile: TEdit
    Left = 269
    Top = 208
    Width = 116
    Height = 21
    TabOrder = 16
  end
  object EDisplayOrder: TSpinEdit
    Left = 78
    Top = 125
    Width = 67
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 4
    Value = 0
  end
  object ENote: TMemo
    Left = 78
    Top = 262
    Width = 307
    Height = 99
    TabOrder = 18
  end
  object btnSave: TButton
    AlignWithMargins = True
    Left = 156
    Top = 380
    Width = 120
    Height = 33
    Align = alRight
    Caption = #30830#23450
    ImageIndex = 19
    ImageMargins.Left = 5
    Images = FCoteDB.ImgButton
    ModalResult = 1
    TabOrder = 19
  end
  object btnCancel: TButton
    AlignWithMargins = True
    Left = 282
    Top = 380
    Width = 120
    Height = 33
    Align = alRight
    Cancel = True
    Caption = #25918#24323
    ImageIndex = 20
    ImageMargins.Left = 5
    Images = FCoteDB.ImgButton
    ModalResult = 2
    TabOrder = 20
  end
  object EKind: TComboBox
    AlignWithMargins = True
    Left = 269
    Top = 18
    Width = 116
    Height = 21
    TabOrder = 6
  end
  object EState: TComboBox
    AlignWithMargins = True
    Left = 78
    Top = 153
    Width = 116
    Height = 21
    TabOrder = 11
    OnChange = EStateChange
  end
  object ECity: TComboBox
    AlignWithMargins = True
    Left = 78
    Top = 180
    Width = 116
    Height = 21
    TabOrder = 12
    OnChange = ECityChange
  end
  object ECounty: TComboBox
    AlignWithMargins = True
    Left = 78
    Top = 207
    Width = 116
    Height = 21
    TabOrder = 13
  end
  object ECashKind: TComboBox
    AlignWithMargins = True
    Left = 269
    Top = 45
    Width = 116
    Height = 21
    TabOrder = 7
  end
  object ECashDate2: TDateTimePicker
    Left = 269
    Top = 126
    Width = 116
    Height = 22
    Date = 1.000000000000000000
    Time = 0.970618090279458600
    DateFormat = dfLong
    TabOrder = 10
    Visible = False
  end
  object EDisplayOrder2: TSpinEdit
    Left = 151
    Top = 125
    Width = 67
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 5
    Value = 0
    Visible = False
  end
  object ECash2: TEdit
    Left = 327
    Top = 72
    Width = 58
    Height = 21
    TabOrder = 21
    Text = '0.00'
    Visible = False
  end
  object DQZone: TADOQuery
    Connection = FCoteDB.ADOConn
    Parameters = <
      item
        Name = 'PID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'SELECT * FROM SYS_Zone'
      'WHERE PID=:PID')
    Left = 40
    Top = 296
  end
  object DQMaxMemberIdx: TADOQuery
    Connection = FCoteDB.ADOConn
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'SELECT ISNULL(MAX(DisplayOrder),0)+1 FROM JOIN_Member')
    Left = 96
    Top = 296
  end
end
