(**************************************************************
* Copyright:      Jamesvon  mailto:Jamesvon@163.COM
* Author:         James von
* Remarks:        JOIN project files, Copyright by jamesvon
* known Problems: none
* Version:        1.0
* Description:    鸽主 录入对话程序
* Include:
*=============================================================*
* TFDlgMember description
*-------------------------------------------------------------*
*
**************************************************************)

unit UDlgMember;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, UVonLog, UVonConfig, UVonSystemFuns, UCoteInfo, UCoteDB,
  Vcl.ComCtrls, Data.DB, Data.Win.ADODB, Vcl.ExtCtrls;

type
  TFDlgMember = class(TForm)
    lblMemberName: TLabel;
    EMemberName: TEdit;
    lblMemberNo: TLabel;
    EMemberNo: TEdit;
    lblAssociationNo: TLabel;
    EAssociationNo: TEdit;
    lblShortName: TLabel;
    EShortName: TEdit;
    lblKind: TLabel;
    lblLinker: TLabel;
    EEMail: TEdit;
    lbCash: TLabel;
    ECash: TEdit;
    lbCashDate: TLabel;
    ECashDate: TDateTimePicker;
    lblCashKind: TLabel;
    lblState: TLabel;
    lblCity: TLabel;
    lblCounty: TLabel;
    lblAddress: TLabel;
    EAddress: TEdit;
    lblManager: TLabel;
    EManager: TEdit;
    lblTelphong: TLabel;
    EMobile: TEdit;
    lbDisplayOrder: TLabel;
    EDisplayOrder: TSpinEdit;
    lblNote: TLabel;
    ENote: TMemo;
    btnSave: TButton;
    btnCancel: TButton;
    EKind: TComboBox;
    EState: TComboBox;
    ECity: TComboBox;
    ECounty: TComboBox;
    ECashKind: TComboBox;
    DQZone: TADOQuery;
    ECashDate2: TDateTimePicker;
    EDisplayOrder2: TSpinEdit;
    ECash2: TEdit;
    DQMaxMemberIdx: TADOQuery;
    Bevel1: TBevel;
    procedure EStateChange(Sender: TObject);
    procedure ECityChange(Sender: TObject);
    procedure EMemberNameChange(Sender: TObject);
  private
    { Private declarations }
    FIsSearch: Boolean;
    procedure Init(IsSearch: Boolean);
  public
    { Public declarations }
    class procedure InputDlg(Q: TCustomADODataSet; IsNew: Boolean);
    class procedure SearchDlg(Q: TADOQuery);
    procedure FormToInfo(FInfo: TMemberInfo);
    procedure InfoToForm(FInfo: TMemberInfo);
  end;

var
  FDlgMember: TFDlgMember;

implementation

uses DateUtils;

{$R *.dfm}

procedure TFDlgMember.ECityChange(Sender: TObject);
begin
  if ECity.ItemIndex < 0 then
    Exit;
  with DQZone do
  begin
    Parameters[0].Value := Integer(ECity.Items.Objects[ECity.ItemIndex]);
    Open;
    if FIsSearch then ECounty.Items.Text:= '所有' else ECounty.Items.Clear;
    while not EOF do
    begin
      ECounty.Items.AddObject(FieldByName('ItemName').AsString,
        TObject(FieldByName('ID').AsInteger));
      Next;
    end;
    Close;
  end;
  ECounty.ItemIndex := 0;
end;

procedure TFDlgMember.EMemberNameChange(Sender: TObject);
begin
  if not FIsSearch then EShortName.Text := ShortSpellString(EMemberName.Text);
end;

procedure TFDlgMember.EStateChange(Sender: TObject);
begin
  if EState.ItemIndex < 0 then
    Exit;
  with DQZone do
  begin
    Parameters[0].Value := Integer(EState.Items.Objects[EState.ItemIndex]);
    Open;
    if FIsSearch then ECity.Items.Text:= '所有' else ECity.Items.Clear;
    while not EOF do
    begin
      ECity.Items.AddObject(FieldByName('ItemName').AsString,
        TObject(FieldByName('ID').AsInteger));
      Next;
    end;
    Close;
  end;
  ECity.ItemIndex := 0;
  ECityChange(nil);
end;

procedure TFDlgMember.FormToInfo(FInfo: TMemberInfo);
begin
  FInfo.MemberName:= EMemberName.Text;
  FInfo.MemberNo:= EMemberNo.Text;
  FInfo.AssociationNo:= EAssociationNo.Text;
  FInfo.ShortName:= EShortName.Text;
  FInfo.Kind:= EKind.Text;
  FInfo.EMail:= EEMail.Text;
  FInfo.Cash:= StrToFloat(ECash.Text);
  FInfo.CashDate:= ECashDate.Date;
  FInfo.CashKind:= ECashKind.Text;
  FInfo.State:= EState.Text;
  FInfo.City:= ECity.Text;
  FInfo.County:= ECounty.Text;
  FInfo.Address:= EAddress.Text;
  FInfo.Manager:= EManager.Text;
  FInfo.Mobile:= EMobile.Text;
  FInfo.DisplayOrder:= EDisplayOrder.Value;
  FInfo.Modifiedate:= Now;
  FInfo.Note:= ENote.Lines.Text;
end;

procedure TFDlgMember.InfoToForm(FInfo: TMemberInfo);
begin
  EMemberName.Text:= FInfo.MemberName;
  EMemberNo.Text:= FInfo.MemberNo;
  EAssociationNo.Text:= FInfo.AssociationNo;
  EShortName.Text:= FInfo.ShortName;
  EKind.Text:= FInfo.Kind;
  EEMail.Text:= FInfo.EMail;
  ECash.Text:= FloatToStr(FInfo.Cash);
  ECashDate.Date:= FInfo.CashDate;
  ECashKind.Text:= FInfo.CashKind;
  if FInfo.State <> '' then
  begin
    EState.ItemIndex := EState.Items.IndexOf(FInfo.State);
    EState.Text:= FInfo.State;
    EStateChange(nil);
  end;
  if FInfo.City <> '' then
  begin
    ECity.Text := FInfo.City;
    ECityChange(nil);
  end;
  if FInfo.County <> '' then
    ECounty.Text := FInfo.County;
  EAddress.Text:= FInfo.Address;
  EManager.Text:= FInfo.Manager;
  EMobile.Text:= FInfo.Mobile;
  EDisplayOrder.Value:= FInfo.DisplayOrder;
  ENote.Lines.Text:= FInfo.Note;
end;

procedure TFDlgMember.Init(IsSearch: Boolean);
var
  S: string;
  newID: Integer;
begin
  FIsSearch:= IsSearch;
  ECashDate.Date := DateOf(Now);
  ECashDate.ShowCheckbox:= IsSearch;
  ECashDate.Checked:= False;
  ECashDate2.DateTime:= Now;
  ECashDate2.ShowCheckbox:= IsSearch;
  ECashDate2.Checked:= False;
  ECashDate2.Visible:= IsSearch;
  ECash2.Visible:= IsSearch;
  with DQZone do
  begin
    Parameters[0].Value := 0;
    Open;
    if IsSearch then EState.Items.Text:= '所有' else EState.Items.Clear;
    while not EOF do
    begin
      EState.Items.AddObject(FieldByName('ItemName').AsString,
        TObject(FieldByName('ID').AsInteger));
      Next;
    end;
    Close;
  end;
  EState.ItemIndex := 0;
  EStateChange(nil);
  if IsSearch then begin
    S:= '所有';
  end else begin
    S:= '';
    newID:= FCoteDB.ADOConn.Execute('SELECT ISNULL(MAX(DisplayOrder),0)+1 FROM '
      + Prefix + 'Member').Fields[0].Value;
    EMemberNo.Text:= Format(ReadAppConfig('SYSTEM', 'AutoMemberCode'), [newID]);
    EDisplayOrder.Value:= newID;
  end;
  FCoteDB.GetDropList('会员类型', EKind, S);
  FCoteDB.GetDropList('缴费方式', ECashKind, S);
end;


class procedure TFDlgMember.InputDlg(Q: TCustomADODataSet; IsNew: Boolean);
var
  FInfo: TMemberInfo;
begin
  FInfo:= TMemberInfo.Create;
  with TFDlgMember.Create(nil) do try
    Init(False);
    if not IsNew then begin FInfo.LoadFromDB(Q); InfoToForm(FInfo); end;
    while ShowModal = mrOK do try
      FormToInfo(FInfo);
      if not FInfo.CheckInfo then
        raise Exception.Create(LastInfo);
      if IsNew then FInfo.AppendToDB(Q) else FInfo.UpdateToDB(Q);
      Exit;
    except
      on E: Exception do begin
        DlgInfo('错误', E.Message);
        Q.Cancel;
      end;
    end;
  finally
    Free;
    FInfo.Free;
  end;
end;

class procedure TFDlgMember.SearchDlg(Q: TADOQuery);
var
  Idx: Integer;
begin
  with TFDlgMember.Create(nil) do try
    Init(True);
    if ShowModal = mrOK then begin
      Q.SQL.Text := 'SELECT * FROM ' + Prefix + 'Member WHERE Status=''启用''';
      Idx := Q.SQL.Count;
      if EMemberName.Text <> '' then
        Q.SQL.Add('AND MemberName LIKE ''%' + EMemberName.Text + '%''');
      if EMemberNo.Text <> '' then
        Q.SQL.Add('AND MemberNo LIKE ''%' + EMemberNo.Text + '%''');
      if EAssociationNo.Text <> '' then
        Q.SQL.Add('AND AssociationNo LIKE ''%' + EAssociationNo.Text + '%''');
      if EShortName.Text <> '' then
        Q.SQL.Add('AND ShortName LIKE ''%' + EShortName.Text + '%''');
      if EKind.ItemIndex > 0 then
        Q.SQL.Add('AND Kind LIKE ''%' + EKind.Text + '%''');
      if EEMail.Text <> '' then
        Q.SQL.Add('AND EMail LIKE ''%' + EEMail.Text + '%''');
      AddRangeCondition(Q, StrToFloat(ECash.Text), StrToFloat(ECash2.Text), 'Cash');
      AddRangeCondition(Q, ECashDate, ECashDate2, 'CashDate');
      if EState.ItemIndex > 0 then
        Q.SQL.Add('AND State LIKE ''%' + EState.Text + '%''');
      if ECity.ItemIndex > 0 then
        Q.SQL.Add('AND City LIKE ''%' + ECity.Text + '%''');
      if ECounty.ItemIndex > 0 then
        Q.SQL.Add('AND County LIKE ''%' + ECounty.Text + '%''');
      if EAddress.Text <> '' then
        Q.SQL.Add('AND Address LIKE ''%' + EAddress.Text + '%''');
      if EMobile.Text <> '' then
        Q.SQL.Add('AND Mobile LIKE ''%' + EMobile.Text + '%''');
      Q.SQL.Add('ORDER BY DisplayOrder');
      Q.Open;
    end;
  finally
    Free;
  end;
end;

end.
