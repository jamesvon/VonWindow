unit UDlgPackage;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Samples.Spin,
  Vcl.ExtCtrls, UCoteDB, UCoteInfo;

type
  TFDlgPackage = class(TForm)
    Panel1: TPanel;
    lbRingColor: TLabel;
    Label1: TLabel;
    ERingCount: TSpinEdit;
    btnOK: TButton;
    btnCancel: TButton;
    ERingColor: TColorListBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDlgPackage: TFDlgPackage;

implementation

{$R *.dfm}

procedure TFDlgPackage.FormCreate(Sender: TObject);
begin
  ERingColor.Items.Clear;
  lbRingColor.Caption := FCoteDB.GetListOption('����ɫ', ERingColor.Items, 4);
  ERingColor.Enabled:= True;
end;

end.
