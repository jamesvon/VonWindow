object FDlgPigeon: TFDlgPigeon
  Left = 0
  Top = 0
  Caption = #36187#40509#20449#24687#24405#20837
  ClientHeight = 310
  ClientWidth = 483
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 0
    Width = 483
    Height = 265
    Align = alTop
    Shape = bsBottomLine
    ExplicitTop = -2
  end
  object lbPigeonNO: TLabel
    Left = 27
    Top = 57
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = ' '#20449#40509#32534#21495
    Layout = tlCenter
  end
  object lbGBCode: TLabel
    Left = 27
    Top = 28
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = ' '#32479#19968#29615#21495
    Layout = tlCenter
  end
  object Label1: TLabel
    Left = 54
    Top = 111
    Width = 24
    Height = 13
    Caption = #26631#35760
    Layout = tlCenter
  end
  object lbEye: TLabel
    Left = 54
    Top = 84
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = #30524#30722
  end
  object lbStatus: TLabel
    Left = 254
    Top = 56
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = #20449#40509#29366#24577
  end
  object lbFeather: TLabel
    Left = 278
    Top = 28
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = #32701#33394
  end
  object lbJoinDate: TLabel
    Left = 254
    Top = 111
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = #20837#26842#26085#26399
  end
  object lbAssist1: TLabel
    Left = 262
    Top = 169
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Extend1'
    Visible = False
  end
  object lbAssist2: TLabel
    Left = 38
    Top = 196
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Extend2'
    Visible = False
  end
  object lbAssist3: TLabel
    Left = 262
    Top = 198
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Extend1'
    Visible = False
  end
  object lbAssist4: TLabel
    Left = 38
    Top = 225
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Extend2'
    Visible = False
  end
  object lbAssist5: TLabel
    Left = 262
    Top = 225
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Extend1'
    Visible = False
  end
  object lbDisplayOrder: TLabel
    Left = 254
    Top = 85
    Width = 48
    Height = 13
    Caption = #26174#31034#24207#21495
  end
  object EGender: TRadioGroup
    AlignWithMargins = True
    Left = 84
    Top = 135
    Width = 149
    Height = 45
    Caption = #20844#27597
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      #20844
      #27597)
    TabOrder = 10
  end
  object EPigeonNO: TEdit
    Left = 84
    Top = 54
    Width = 149
    Height = 21
    Hint = #36825#26159#19968#20010#20869#37096#32534#21495#65292#27599#19968#21482#40509#23376#24517#39035#25317#26377#33258#24049#21807#19968#30340#32534#21495
    TabOrder = 2
  end
  object EGBCode: TEdit
    Left = 84
    Top = 25
    Width = 149
    Height = 21
    TabOrder = 0
    OnExit = EGBCodeExit
  end
  object EFlag: TEdit
    Left = 84
    Top = 108
    Width = 49
    Height = 21
    TabOrder = 7
  end
  object EEye: TComboBox
    Left = 84
    Top = 81
    Width = 149
    Height = 21
    TabOrder = 4
  end
  object EStatus: TComboBox
    Left = 308
    Top = 53
    Width = 149
    Height = 21
    Style = csDropDownList
    TabOrder = 3
  end
  object EFeather: TComboBox
    Left = 308
    Top = 25
    Width = 149
    Height = 21
    TabOrder = 1
  end
  object dtJoinDate: TDateTimePicker
    Left = 308
    Top = 108
    Width = 149
    Height = 23
    Date = 1.000000000000000000
    Time = 0.482400393520947500
    DateFormat = dfLong
    TabOrder = 8
  end
  object EDisplayOrder: TSpinEdit
    Left = 308
    Top = 80
    Width = 67
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 5
    Value = 0
  end
  object EDisplayOrder2: TSpinEdit
    Left = 390
    Top = 80
    Width = 67
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 6
    Value = 0
    Visible = False
  end
  object dtJoinDate2: TDateTimePicker
    Left = 308
    Top = 137
    Width = 149
    Height = 23
    Date = 1.000000000000000000
    Time = 0.482400393520947500
    DateFormat = dfLong
    TabOrder = 9
    Visible = False
  end
  object EAssist1: TComboBox
    Left = 308
    Top = 166
    Width = 149
    Height = 21
    Style = csDropDownList
    TabOrder = 11
    Visible = False
  end
  object EAssist3: TComboBox
    Left = 308
    Top = 193
    Width = 149
    Height = 21
    Style = csDropDownList
    TabOrder = 13
    Visible = False
  end
  object EAssist5: TComboBox
    Left = 308
    Top = 220
    Width = 149
    Height = 21
    Style = csDropDownList
    TabOrder = 15
    Visible = False
  end
  object EAssist2: TComboBox
    Left = 84
    Top = 193
    Width = 149
    Height = 21
    Style = csDropDownList
    TabOrder = 12
    Visible = False
  end
  object EAssist4: TComboBox
    Left = 84
    Top = 220
    Width = 149
    Height = 21
    Style = csDropDownList
    TabOrder = 14
    Visible = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 269
    Width = 483
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 16
    object btnSave: TButton
      AlignWithMargins = True
      Left = 234
      Top = 3
      Width = 120
      Height = 35
      Align = alRight
      Caption = #30830#23450
      Default = True
      ImageIndex = 19
      ImageMargins.Left = 5
      Images = FCoteDB.ImgButton
      ModalResult = 1
      TabOrder = 0
    end
    object btnCancel: TButton
      AlignWithMargins = True
      Left = 360
      Top = 3
      Width = 120
      Height = 35
      Align = alRight
      Cancel = True
      Caption = #25918#24323
      ImageIndex = 20
      ImageMargins.Left = 5
      Images = FCoteDB.ImgButton
      ModalResult = 2
      TabOrder = 1
    end
  end
  object ERing: TEdit
    Left = 139
    Top = 108
    Width = 94
    Height = 21
    ReadOnly = True
    TabOrder = 17
  end
  object DQCHECK: TADOQuery
    Connection = FCoteDB.ADOConn
    Parameters = <
      item
        Name = 'GCODE'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'PCode'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 20
        Value = Null
      end
      item
        Name = 'JCode'
        Attributes = [paSigned, paNullable]
        DataType = ftLargeint
        Precision = 19
        Size = 8
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'SELECT ID,GBCode,PigeonNO,JZCode,FactoryCode'
      'FROM JOIN_Pigeon'
      'WHERE (GBCode=:GCODE'
      'OR (PigeonNO=:PCode)'
      'OR (JZCode=:JCode))')
    Left = 19
    Top = 88
  end
  object DQMaxIdx: TADOQuery
    Connection = FCoteDB.ADOConn
    Parameters = <
      item
        Name = 'MID'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'SELECT ISNULL(MAX(DisplayOrder),0)+1 FROM JOIN_Pigeon'
      'WHERE MemberIdx=:MID')
    Left = 16
    Top = 136
  end
  object ADOQuery1: TADOQuery
    Connection = FCoteDB.ADOConn
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      '')
    Left = 320
    Top = 48
  end
end
