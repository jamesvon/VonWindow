unit UDlgPigeon;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls,UCoteInfo, UCoteDB,
  Data.DB, Data.Win.ADODB, Vcl.Samples.Spin, UVonSystemFuns;

type
  TFDlgPigeon = class(TForm)
    EGender: TRadioGroup;
    lbPigeonNO: TLabel;
    EPigeonNO: TEdit;
    lbGBCode: TLabel;
    EGBCode: TEdit;
    Label1: TLabel;
    EFlag: TEdit;
    lbEye: TLabel;
    EEye: TComboBox;
    lbStatus: TLabel;
    EStatus: TComboBox;
    lbFeather: TLabel;
    EFeather: TComboBox;
    lbJoinDate: TLabel;
    dtJoinDate: TDateTimePicker;
    lbAssist1: TLabel;
    lbAssist2: TLabel;
    lbAssist3: TLabel;
    lbAssist4: TLabel;
    lbAssist5: TLabel;
    Bevel1: TBevel;
    lbDisplayOrder: TLabel;
    EDisplayOrder: TSpinEdit;
    EDisplayOrder2: TSpinEdit;
    dtJoinDate2: TDateTimePicker;
    DQCHECK: TADOQuery;
    EAssist1: TComboBox;
    EAssist3: TComboBox;
    EAssist5: TComboBox;
    EAssist2: TComboBox;
    EAssist4: TComboBox;
    Panel1: TPanel;
    btnSave: TButton;
    btnCancel: TButton;
    DQMaxIdx: TADOQuery;
    ADOQuery1: TADOQuery;
    ERing: TEdit;
    procedure EGBCodeExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FAutoCheckGBCode, FAutoCalcPigeonNO, FCheckPigeonNo: Boolean;
    MyGB : TGBCode;
    procedure Init(IsSearch: Boolean; MemberIdx: TGuid);
    function CheckDoublication(GBCode, PigeonNo: string; ID: TGuid; JZCode: int64): Boolean;
  public
    { Public declarations }
    class procedure AppendDlg(Q: TCustomADODataSet; MemberNo: string;
      MemberIdx: TGuid; JZCode, FactoryCode: Int64);
    class procedure UpdateDlg(Q: TCustomADODataSet);
    class procedure SearchDlg(Q: TADOQuery; MemberIdx: TGuid; JZCode: Int64);
    procedure FormToInfo(FInfo: TPigeonInfo);
    procedure InfoToForm(FInfo: TPigeonInfo);
    procedure CheckPigeonEditor;
  end;

var
  FDlgPigeon: TFDlgPigeon;

implementation

uses DateUtils, UVonLog;

{$R *.dfm}

procedure TFDlgPigeon.FormCreate(Sender: TObject);
begin
  MyGB := TGBCode.Create;
end;

procedure TFDlgPigeon.FormDestroy(Sender: TObject);
begin
  MyGB.Free;
end;

procedure TFDlgPigeon.Init(IsSearch: Boolean; MemberIdx: TGuid);
var
  S: string;

  procedure SetAssist(lb: TLabel; opt: string; edt: TComboBox);
  begin
    lb.Caption:= FCoteDB.GetDropList(opt, edt, S);
    lb.Visible:= lb.Caption <> '';
    edt.Visible:= lb.Visible;
  end;
begin
  FAutoCheckGBCode := ReadAppConfig('SYSTEM', 'AutoCheckGBCode') = '1';
  FAutoCalcPigeonNO := ReadAppConfig('SYSTEM', 'AutoPigeonNo') = '1';
  FCheckPigeonNo := ReadAppConfig('SYSTEM', 'CheckPigeonNo') = '1';
  if IsSearch then begin
    S:= '所有';
    dtJoinDate.Date := Now - 200;
    dtJoinDate2.Date := Now;
    EGender.Items.Insert(0, S);
    EGBCode.OnExit:= nil;
  end else begin
    S:= '';
    dtJoinDate.DateTime := DateOf(Now);
  end;
  dtJoinDate.ShowCheckbox:= IsSearch;
  dtJoinDate.Checked:= False;
  dtJoinDate2.ShowCheckbox:= IsSearch;
  dtJoinDate2.Checked:= False;
  EDisplayOrder2.Visible:= IsSearch;
  lbFeather.Caption:= FCoteDB.GetDropList('羽色', EFeather, S);
  lbEye.Caption:= FCoteDB.GetDropList('眼砂', EEye, S);
  lbStatus.Caption:= FCoteDB.GetDropList('信鸽状态', EStatus, S);
  SetAssist(lbAssist1, '辅助信息1', EAssist1);
  SetAssist(lbAssist2, '辅助信息2', EAssist2);
  SetAssist(lbAssist3, '辅助信息3', EAssist3);
  SetAssist(lbAssist4, '辅助信息4', EAssist4);
  SetAssist(lbAssist5, '辅助信息4', EAssist5);
end;

procedure TFDlgPigeon.EGBCodeExit(Sender: TObject);
begin
  if FAutoCheckGBCode then
  begin
    MyGB.Value := EGBCode.Text;
    EGBCode.Text := MyGB.Value;
  end else EGBCode.Text := Trim(EGBCode.Text);
end;

function TFDlgPigeon.CheckDoublication(GBCode, PigeonNo: string; ID: TGuid; JZCode: int64): Boolean;
begin
  ClearInfo;
  with DQCHECK do
    try
      Parameters[0].Value := GBCode;
      Parameters[1].Value := PigeonNO;
      Parameters[2].Value := JZCode;
      Open;
      while not EOF do
      begin
        if (FieldByName('GBCode').AsString = GBCode) and
          not GuidComp(FieldByName('ID').AsGuid, ID) then
          WriteInfo('统一环号重复，系统不允许保存。');
        if FCheckPigeonNo and (FieldByName('PigeonNO').AsString = PigeonNO) and
          not GuidComp(FieldByName('ID').AsGuid, ID) then
          WriteInfo('信鸽编号重复，系统不允许保存，请手工修改信鸽编号。');
        if (JZCode <> 0) and (FieldByName('JZCode').AsLargeInt = JZCode) and
          not GuidComp(FieldByName('ID').AsGuid, ID) then
          WriteInfo('电子环号重复，系统不允许保存。');
        Next;
      end;
      Result := GetInfoList.Count < 1;
    finally
      Close;
    end;
end;

procedure TFDlgPigeon.CheckPigeonEditor;
begin
  if Length(EGBCode.Text) < 6 then raise Exception.Create('统一环号必须录入正确');
  if Length(EPigeonNO.Text) < 1 then raise Exception.Create('信鸽编号必须录入正确');
end;

procedure TFDlgPigeon.FormToInfo(FInfo: TPigeonInfo);
begin
  CheckPigeonEditor;
  FInfo.PigeonNO:= EPigeonNO.Text;
  FInfo.HouseNo:= '';
  FInfo.GBCode:= EGBCode.Text;
  FInfo.Flag:= EFlag.Text;
  case EGender.ItemIndex of
  0: FInfo.Gender:= '公';
  1: FInfo.Gender:= '母';
  end;
  FInfo.Feather:= EFeather.Text;
  FInfo.Eye:= EEye.Text;
  FInfo.DisplayOrder:= EDisplayOrder.Value;
  FInfo.Assist[0]:= EAssist1.Text;
  FInfo.Assist[1]:= EAssist2.Text;
  FInfo.Assist[2]:= EAssist3.Text;
  FInfo.Assist[3]:= EAssist4.Text;
  FInfo.Assist[4]:= EAssist5.Text;
  FInfo.Status:= EStatus.Text;
  FInfo.JoinDate:= dtJoinDate.Date;
  FInfo.Modifiedate:= Now;
end;

procedure TFDlgPigeon.InfoToForm(FInfo: TPigeonInfo);
begin
  EPigeonNO.Text:= FInfo.PigeonNO;
  EGBCode.Text:= FInfo.GBCode;
  EFlag.Text:= FInfo.Flag;
  if FInfo.Gender = '公' then EGender.ItemIndex:= 0 else EGender.ItemIndex:= 1;
  EFeather.Text:= FInfo.Feather;
  EEye.Text:= FInfo.Eye;
  EDisplayOrder.Value:= FInfo.DisplayOrder;
  EAssist1.ItemIndex:= EAssist1.Items.IndexOf(FInfo.Assist[0]);
  EAssist2.ItemIndex:= EAssist2.Items.IndexOf(FInfo.Assist[1]);
  EAssist3.ItemIndex:= EAssist3.Items.IndexOf(FInfo.Assist[2]);
  EAssist4.ItemIndex:= EAssist4.Items.IndexOf(FInfo.Assist[3]);
  EAssist5.ItemIndex:= EAssist5.Items.IndexOf(FInfo.Assist[4]);
  EStatus.ItemIndex:= EStatus.Items.IndexOf(FInfo.Status);
  dtJoinDate.Date:= FInfo.JoinDate;
end;

class procedure TFDlgPigeon.AppendDlg(Q: TCustomADODataSet;
  MemberNo: string; MemberIdx: TGuid; JZCode, FactoryCode: Int64);
var
  FInfo: TPigeonInfo;
  szMaxIdx: Integer;
begin
  FInfo:= TPigeonInfo.Create;
  with TFDlgPigeon.Create(nil) do try
    Init(False, MemberIdx);
    szMaxIdx:= FCoteDB.ADOConn.Execute('SELECT ISNULL(MAX(DisplayOrder),0)+1 FROM ' +
      'JOIN_Pigeon WHERE MemberIdx=''' + GuidToString(MemberIdx) + '''').Fields[0].Value;
//    with DQMaxIdx do begin
//      Parameters[0].Value:= GuidToString(MemberIdx);
//      Open;
//      szMaxIdx:= Fields[0].Value;
//    end;
    if jzCode > 0 then begin
      ERing.Text:= IntToStr(jzCode);
      ERing.Color:= clMoneyGreen;
    end;
    EPigeonNo.Text:= Format(ReadAppConfig('SYSTEM', 'PigeonNoFmt'), [MemberNo, szMaxIdx]);
    EDisplayOrder.Value:= szMaxIdx;
    while ShowModal = mrOK do try
      FormToInfo(FInfo);
      FInfo.Group[0]:= '';
      FInfo.Group[1]:= '';
      FInfo.Group[2]:= '';
      FInfo.Group[3]:= '';
      FInfo.Group[4]:= '';
      FInfo.JZCode:= JZCode;
      FInfo.FactoryCode:= FactoryCode;
      FInfo.OrgIdx:= FCoteDB.OrgInfo.OrgID;
      FInfo.MemberIdx:= MemberIdx;
      if not CheckDoublication(FInfo.GBCode, FInfo.PigeonNO, FInfo.ID, FInfo.JZCode) then
        raise Exception.Create(GetInfoList.Text);
      FInfo.AppendToDB(Q);
      Exit;
    except
      on E: Exception do begin
        DlgInfo('错误', E.Message);
        Q.Cancel;
      end;
    end;
  finally
    Free;
    FInfo.Free;
  end;
end;

class procedure TFDlgPigeon.UpdateDlg(Q: TCustomADODataSet);
var
  FInfo: TPigeonInfo;
begin
  FInfo:= TPigeonInfo.Create;
  with TFDlgPigeon.Create(nil) do try
    FInfo.LoadFromDB(Q);
    Init(False, FInfo.MemberIdx);
    InfoToForm(FInfo);
    while ShowModal = mrOK do try
      FormToInfo(FInfo);
      if not CheckDoublication(FInfo.GBCode, FInfo.PigeonNO, FInfo.ID, FInfo.JZCode) then
        raise Exception.Create(GetInfoList.Text);
      FInfo.UpdateToDB(Q);
      Exit;
    except
      on E: Exception do begin
        DlgInfo('错误', E.Message);
        Q.Cancel;
      end;
    end;
  finally
    Free;
    FInfo.Free;
  end;
end;

class procedure TFDlgPigeon.SearchDlg(Q: TADOQuery; MemberIdx: TGuid; JZCode: Int64);
begin
  with TFDlgPigeon.Create(nil) do try
    Init(True, TGuid.Empty);
    if ShowModal = mrOK then begin
      if EPigeonNO.Text <> '' then
        Q.SQL.Add('AND PigeonNO LIKE ''%' + EPigeonNO.Text + '%''');
      if EGBCode.Text <> '' then
        Q.SQL.Add('AND GBCode LIKE ''%' + EGBCode.Text + '%''');
      if EFlag.Text <> '' then
        Q.SQL.Add('AND Flag=''' + EFlag.Text + '''');
      case EGender.ItemIndex of
      1: Q.SQL.Add('AND Gender=''公''');
      2: Q.SQL.Add('AND Gender=''母''');
      end;
      if EAssist1.Visible and(EAssist1.ItemIndex > 0) then
        Q.SQL.Add('AND Assist1 LIKE ''%' + EAssist1.Text + '%''');
      if EAssist2.Visible and(EAssist2.ItemIndex > 0) then
        Q.SQL.Add('AND Assist2 LIKE ''%' + EAssist2.Text + '%''');
      if EAssist3.Visible and(EAssist3.ItemIndex > 0) then
        Q.SQL.Add('AND Assist3 LIKE ''%' + EAssist3.Text + '%''');
      if EAssist4.Visible and(EAssist4.ItemIndex > 0) then
        Q.SQL.Add('AND Assist4 LIKE ''%' + EAssist4.Text + '%''');
      if EAssist5.Visible and(EAssist5.ItemIndex > 0) then
        Q.SQL.Add('AND Assist5 LIKE ''%' + EAssist5.Text + '%''');
      if EFeather.ItemIndex > 0 then
        Q.SQL.Add('AND Feather=''' + EFeather.Text + '''');
      if EEye.ItemIndex > 0 then
        Q.SQL.Add('AND Eye=''' + EEye.Text + '''');
      if EStatus.ItemIndex > 0 then
        Q.SQL.Add('AND P.Status=''' + EStatus.Text + '''');
      if JZCode > 0 then
        Q.SQL.Add('AND JZCode=''' + IntToStr(JZCode) + '''');
      if MemberIdx <> TGUID.Empty then
        Q.SQL.Add('AND MemberIdx=''' + GuidToString(MemberIdx) + '''');
      AddRangeCondition(Q, dtJoinDate, dtJoinDate2, 'JoinDate');
      AddRangeCondition(Q, EDisplayOrder.Value, EDisplayOrder2.Value, 'DisplayOrder');
      Q.SQL.Add('ORDER BY DisplayOrder');
      Q.Open;
    end;
  finally
    Free;
  end;
end;

end.
