object FDlgReadRing: TFDlgReadRing
  Left = 0
  Top = 0
  Caption = 'FDlgReadRing'
  ClientHeight = 174
  ClientWidth = 175
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object gbComm: TGroupBox
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 169
    Height = 100
    Align = alTop
    Caption = #36890#35759#35774#32622
    TabOrder = 0
    object Panel1: TPanel
      AlignWithMargins = True
      Left = 5
      Top = 18
      Width = 159
      Height = 32
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Panel1'
      ShowCaption = False
      TabOrder = 0
      object Label12: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 48
        Height = 26
        Align = alLeft
        Caption = #37319#38598#31471#21475
        Layout = tlCenter
        ExplicitHeight = 13
      end
      object EComm: TComboBox
        AlignWithMargins = True
        Left = 57
        Top = 3
        Width = 99
        Height = 27
        Align = alClient
        Style = csOwnerDrawFixed
        ItemHeight = 21
        ItemIndex = 0
        TabOrder = 0
        Text = 'COM1'
        Items.Strings = (
          'COM1'
          'COM2'
          'COM3'
          'COM4'
          'COM5'
          'COM6'
          'COM7'
          'COM8')
      end
    end
    object Panel2: TPanel
      AlignWithMargins = True
      Left = 5
      Top = 56
      Width = 159
      Height = 32
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Panel2'
      ShowCaption = False
      TabOrder = 1
      object Label13: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 48
        Height = 26
        Align = alLeft
        Caption = #31471#21475#36895#29575
        Layout = tlCenter
        ExplicitHeight = 13
      end
      object ECommRate: TComboBox
        AlignWithMargins = True
        Left = 57
        Top = 3
        Width = 99
        Height = 27
        Align = alClient
        Style = csOwnerDrawFixed
        ItemHeight = 21
        ItemIndex = 4
        TabOrder = 0
        Text = '9600'
        Items.Strings = (
          '300'
          '1200'
          '2400'
          '4800'
          '9600'
          '19200'
          '38400'
          '576000'
          '1152000')
      end
    end
  end
  object BitBtn1: TBitBtn
    Left = 92
    Top = 109
    Width = 75
    Height = 25
    Caption = #25171#24320
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 1
  end
  object BitBtn2: TBitBtn
    Left = 92
    Top = 140
    Width = 75
    Height = 25
    Caption = #25918#24323
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 2
  end
end
