unit UDlgReadRing;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, UCoteComm;

type
  TFDlgReadRing = class(TForm)
    gbComm: TGroupBox;
    Panel1: TPanel;
    Label12: TLabel;
    EComm: TComboBox;
    Panel2: TPanel;
    Label13: TLabel;
    ECommRate: TComboBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
  private
    { Private declarations }
  public
    { Public declarations }
    function OpenComm(
      EventOfCollection: TEventAfterCollectionReceived;
      EventOfRead: TEventAfterReadReceived;
      EventOfWrite: TEventAfterWriteReceived): TJZCommBase;
  end;

var
  FDlgReadRing: TFDlgReadRing;

implementation

uses UCoteDB, UVonSystemFuns;

{$R *.dfm}


{ TFDlgReadRing }

function TFDlgReadRing.OpenComm(
  EventOfCollection: TEventAfterCollectionReceived;
  EventOfRead: TEventAfterReadReceived;
  EventOfWrite: TEventAfterWriteReceived): TJZCommBase;
begin
  ReadAppItems('COMMUNICATION', 'PortList', 'COM1,COM2,COM3,COM4', EComm.Items);
  EComm.ItemIndex := EComm.Items.IndexOf(ReadAppConfig('COMMUNICATION', 'Port'));
  ECommRate.ItemIndex := ECommRate.Items.IndexOf(ReadAppConfig('COMMUNICATION',
    'Speed'));
  if ShowModal = mrOK then begin
    Result:= FCoteDB.CreateComm(EventOfCollection, EventOfRead, EventOfWrite, true);
    Result.Open(EComm.ItemIndex + 1, StrToInt(ECommRate.Text), 'n', 8, 1);
    WriteAppConfig('COMMUNICATION', 'Port', EComm.Text);
    WriteAppConfig('COMMUNICATION', 'Speed', ECommRate.Text);
  end else begin
    FCoteDB.CloseComm(Result);
  end;
end;

end.
