unit UDlgSunTime;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, DB, ADODB;

type
  TFDlgSunTime = class(TForm)
    DQZone: TADOQuery;
    Label1: TLabel;
    ECity: TComboBox;
    EState: TComboBox;
    ECounty: TComboBox;
    Label2: TLabel;
    DateTimePicker1: TDateTimePicker;
    BitBtn1: TBitBtn;
    lbSunRaise: TLabel;
    dtSunRaise: TDateTimePicker;
    lbSunDown: TLabel;
    dtSunDown: TDateTimePicker;
    BitBtn2: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure ECityChange(Sender: TObject);
    procedure EStateChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    function GetSunDown: TDateTime;
    function GetSunRaise: TDateTime;
    procedure SetSunDown(const Value: TDateTime);
    procedure SetSunRaise(const Value: TDateTime);
    { Private declarations }
  public
    { Public declarations }
    property SunRaise: TDateTime read GetSunRaise Write SetSunRaise;
    property SunDown: TDateTime read GetSunDown Write SetSunDown;
  end;

var
  FDlgSunTime: TFDlgSunTime;

implementation

uses UCoteDB;

{$R *.dfm}

procedure TFDlgSunTime.BitBtn1Click(Sender: TObject);
var
  Lon: Double;
  Y, M, D: Word;
begin
  with TADOQuery.Create(nil) do
    try
      Connection := FCoteDB.ADOConn;
      SQL.Text := 'SELECT Lon FROM SYS_Zone WHERE ID=:ID';
      Parameters[0].Value := Integer(ECity.Items.Objects[ECity.ItemIndex]);
      Open;
      Lon := Fields[0].AsFloat;
      Close;
      SQL.Text :=
        'SELECT [RaiseHour], [RaiseMinute], [DownHour], [DownMinute] FROM [PGN_SunTime] WHERE [Month]=:M AND [Day]=:D';
      DecodeDate(DateTimePicker1.Date, Y, M, D);
      Parameters[0].Value := M;
      Parameters[1].Value := D;
      Open;
      M := Fields[0].AsInteger;
      D := Fields[1].AsInteger;
      if Lon > 0 then
        dtSunRaise.DateTime := EncodeTime(M, D, 0, 0) -
          (Lon - 116.4011742) / 360
      else
        dtSunRaise.DateTime := EncodeTime(M, D, 0, 0);
      M := Fields[2].AsInteger;
      D := Fields[3].AsInteger;
      if Lon > 0 then
        dtSunDown.DateTime := EncodeTime(M, D, 0, 0) - (Lon - 116.4011742) / 360
      else
        dtSunDown.DateTime := EncodeTime(M, D, 0, 0);
    finally
      Free;
    end;
end;

procedure TFDlgSunTime.ECityChange(Sender: TObject);
begin
  with DQZone do
    try
      Parameters[0].Value := Integer(ECity.Items.Objects[ECity.ItemIndex]);
      Open;
      ECounty.Visible := True;
      ECounty.Items.Clear;
      while not EOF do
      begin
        ECounty.Items.AddObject(FieldByName('ItemName').AsString,
          TObject(FieldByName('ID').AsInteger));
        Next;
      end;
      ECounty.ItemIndex := 0;
    finally
      Close;
    end;
end;

procedure TFDlgSunTime.EStateChange(Sender: TObject);
begin
  with DQZone do
    try
      Parameters[0].Value := Integer(EState.Items.Objects[EState.ItemIndex]);
      Open;
      ECity.Visible := True;
      ECity.Items.Clear;
      while not EOF do
      begin
        ECity.Items.AddObject(FieldByName('ItemName').AsString,
          TObject(FieldByName('ID').AsInteger));
        Next;
      end;
      ECity.ItemIndex := 0;
    finally
      Close;
    end;
end;

procedure TFDlgSunTime.FormCreate(Sender: TObject);
begin
  with DQZone do
  begin
    Parameters[0].Value := 0;
    Open;
    while not EOF do
    begin
      EState.Items.AddObject(FieldByName('ItemName').AsString,
        TObject(FieldByName('ID').AsInteger));
      Next;
    end;
    Close;
  end;
  DateTimePicker1.Date := Now + 2;
end;

function TFDlgSunTime.GetSunDown: TDateTime;
begin
  Result := dtSunDown.DateTime;
end;

function TFDlgSunTime.GetSunRaise: TDateTime;
begin
  Result := dtSunRaise.DateTime;
end;

procedure TFDlgSunTime.SetSunDown(const Value: TDateTime);
begin
  dtSunDown.DateTime := Value;
end;

procedure TFDlgSunTime.SetSunRaise(const Value: TDateTime);
begin
  dtSunRaise.DateTime := Value;
end;

end.
