unit UEnvironment;

interface

uses UVonSystemFuns, UVonLog;

implementation

initialization
  WriteLog(LOG_DEBUG, 'UJZEnvironment', 'initialization ... ...');
// 通讯配置
RegAppConfig('COMMUNICATION', 'PortList', '可选设备通讯端口', LIDT_TEXT, '',
  'COM1,COM2,COM3,COM4,COM5,COM6,COM7,COM8,COM9,COM10');
RegAppConfig('COMMUNICATION', 'Port', '扫描设备通讯端口', LIDT_TEXT, '', 'COM1');
RegAppConfig('COMMUNICATION', 'Speed', '扫描设备通讯波特率', LIDT_Selection,
  '300,1200,2400,4800,9600,19200,38400,57600,115200', '9600');
RegAppConfig('COMMUNICATION', 'IntValue', '扫描设备间隔', LIDT_Selection,
  '20,40,80,100,200,250,300,500', '200');
RegAppConfig('COMMUNICATION', 'TimeOut', '接收超时时间', LIDT_Selection,
  '30,40,50,60,70,80,90,100,200,300,400', '300');
RegAppConfig('COMMUNICATION', 'BoardCount', '踏板数量', LIDT_Int, '', '5');
// 大屏配置
RegAppConfig('LargeScreen', 'Kind', '大屏幕类型', LIDT_Selection,
  '新科16,新科24,中信,奥百特,捷威大屏,CL2005,鸣威大屏,文本文件,配置文件', '文本文件');
RegAppConfig('LargeScreen', 'Port', '大屏幕通讯端口', LIDT_Selection,
  'COM1,COM2,COM3,COM4,COM5,COM6,COM7,COM8,COM9,COM10', 'COM1');
RegAppConfig('LargeScreen', 'Speed', '大屏幕通讯波特率', LIDT_Selection,
  '300,1200,2400,4800,9600,19200,38400,57600,115200', '9600');
RegAppConfig('LargeScreen', 'IntValue', '大屏幕通讯间隔', LIDT_Selection,
  '300,400,500,600,700,800,900,1000,11000', '500');
// 小屏配置
RegAppConfig('SmallScreen', 'ScreenComm', '荣冠小屏通讯端口', LIDT_Selection,
  'COM1,COM2,COM3,COM4,COM5,COM6,COM7,COM8,COM9,COM10', 'COM1');
RegAppConfig('SmallScreen', 'ScreenRate', '荣冠小屏通讯波特率', LIDT_Selection,
  '300,1200,2400,4800,9600,19200,38400,57600,115200', '576000');
// 打印机及短信配置
RegAppConfig('SYSTEM', 'Printer', '即时打印机', LIDT_Text, '', 'LPT1');
RegAppConfig('SYSTEM', 'SMSURL', '短信服务网址', LIDT_Text, '',
  'http://127.0.0.1/hy?uid=1&auth=a&mobile=%s&msg=%s&expid=0&encode=utf-8');
RegAppConfig('SYSTEM', 'SMSCollectionMsg', '集鸽短信内容', LIDT_Text, '',
  '您的赛鸽%s已完成集鸽[公棚名称]',
  '您的赛鸽%s已完成集鸽[公棚名称]');
RegAppConfig('SYSTEM', 'SMSCheckInMsg', '归巢短信内容', LIDT_Text, '',
  '您的赛鸽<code>已于<backtime>归巢，当前排第<order>名[<cote>]',
  '系统提供的参数有<会员编号><会员名称><统一环号><归巢时间><飞行时间><分速><名次><工棚名称><眼砂><羽色><公母><信鸽编号><赛事名称><赛事距离><赛事类型>');
RegAppConfig('SYSTEM', 'CollectionAutoPrint', '集鸽后是否默认自动打印', LIDT_Bool, '', '0');
RegAppConfig('SYSTEM', 'CollectionAutoSms', '集鸽后是否默认自动发送短信', LIDT_Bool, '', '0');
RegAppConfig('SYSTEM', 'PrintCountPerPage', '即时打印每页打印行数', LIDT_Int, '', '50');
// 集鸽配置
RegAppConfig('FIELDS', 'CollectDisplayFields', '集鸽显示字段名称', LIDT_Memo, '',
  'GameIdx=赛事序号:64,State=省份:64,City=地市:64,County=区县:64,MemberName=' +
  '鸽主:64,GBCode=统一环号:128,Feather=羽色:64,Eye=眼砂:64,Gender=公母:64,' +
  'CollecteDate=集鸽时间:128,CollectID=集鸽序号:128');
RegAppConfig('FIELDS', 'CollectPrintFields', '集鸽打印字段列表', LIDT_Memo, '',
  'GameIdx=赛事序号:64,State=省份:64,City=地市:64,County=区县:64,MemberName=' +
  '鸽主:64,GBCode=统一环号:128,Feather=羽色:64,Eye=眼砂:64,Gender=公母:64,' +
  'CollecteDate=集鸽时间:128,CollectID=集鸽序号:128');
RegAppConfig('FIELDS', 'CollectSMSFields', '集鸽短信字段名称', LIDT_Memo, '',
  'GBCode=统一环号:128,CollecteDate=集鸽时间:128,CollectID=集鸽序号:128');
RegAppConfig('SYSTEM', 'CollectSayCount', '播报环号尾数', LIDT_Int, '', '3');
// 归巢配置
RegAppConfig('FIELDS', 'CheckInDisplayFields', '成绩显示字段列表', LIDT_Memo, '',
  'GameIdx=赛事序号:64,State=省份:64,City=地市:64,County=区县:64,MemberName=' +
  '鸽主:64,GBCode=统一环号:128,Feather=羽色:64,Eye=眼砂:64,Gender=公母:64,' +
  'CollecteDate=集鸽时间:128,CollectID=集鸽序号:128');
RegAppConfig('FIELDS', 'CheckInSMSFields', '成绩短信字段名称', LIDT_Memo, '',
  'GameIdx=赛事序号:64,State=省份:64,City=地市:64,County=区县:64,MemberName=' +
  '鸽主:64,GBCode=统一环号:128,Feather=羽色:64,Eye=眼砂:64,Gender=公母:64,' +
  'CollecteDate=集鸽时间:128,CollectID=集鸽序号:128');
RegAppConfig('FIELDS', 'CheckInScreenFields', '成绩大屏显示字段名称', LIDT_Memo, '',
  'GameIdx=赛事序号:64,State=省份:64,City=地市:64,County=区县:64,MemberName=' +
  '鸽主:64,GBCode=统一环号:128,Feather=羽色:64,Eye=眼砂:64,Gender=公母:64,' +
  'CollecteDate=集鸽时间:128,CollectID=集鸽序号:128');
RegAppConfig('FIELDS', 'CheckInPrintFields', '成绩打印字段列表', LIDT_Memo, '',
  'GameIdx=赛事序号:64,State=省份:64,City=地市:64,County=区县:64,MemberName=' +
  '鸽主:64,GBCode=统一环号:128,Feather=羽色:64,Eye=眼砂:64,Gender=公母:64,' +
  'CollecteDate=集鸽时间:128,CollectID=集鸽序号:128');
RegAppConfig('SYSTEM', 'CheckInTopCount', '归巢固定显示的信鸽数量', LIDT_Int, '', '20');
RegAppConfig('SYSTEM', 'CheckInDisplyTimer', '归巢信鸽显示时间间隔', LIDT_Int, '', '400');
// 检查与格式设置
RegAppConfig('SYSTEM', 'CheckPigeonNo', '是否检测信鸽编码的唯一性', LIDT_Bool, '', '1');
RegAppConfig('SYSTEM', 'PigeonNoFmt', '信鸽编码格式', LIDT_Text, '', '%0:s-%1:0.4d');
RegAppConfig('SYSTEM', 'AutoPigeonNo', '自动生成信鸽编码', LIDT_Bool, '', '1');
RegAppConfig('SYSTEM', 'PigeonGroupHeader', '信鸽分组前缀格式', LIDT_Text, '', '%0:s');
RegAppConfig('SYSTEM', 'AutoCheckGBCode', '自动整理统一环号', LIDT_Bool, '', '1');
RegAppConfig('SYSTEM', 'AutoMemberCode', '自动生成会员编号格式', LIDT_Text, '', 'A%0.4d');
RegAppConfig('SYSTEM', 'LonFlatKind', '经度、维度录入方式', LIDT_Selection,
  '度,度分,度分秒', '度分');
RegAppConfig('SYSTEM', 'GBCodeFmt', '统一环号格式', LIDT_Text, '', '%s %s %s %s');
// 指定鸽设置
RegAppConfig('FIELDS', 'ChooseResult', '指定鸽查询结果内容', LIDT_Memo, '',
  '%ID%) 指定人:%Gambler% 奖金：%奖金% 指定鸽:%GBCode% 鸽主:%MemberName% 鸽主编号:%MemberNo% ' +
  '鸽主城市:%City% 羽色:%Feather% 眼砂:%Eye% 实际排名:%SourceOrder% 分速:%Speed% ' +
  '归巢时间:%ReportTime% 飞行时间:%BackTime% 投注金额:%Amount% 投票号:%TicketNo% 投票日期:%ChoicedTime%');
RegAppConfig('FIELDS', 'DefaultChoice', '默认指定鸽项目内容', LIDT_Memo, '',
  '一把抓_100元组=100,一把抓_200元组=200,一把抓_300元组=300,一把抓_500元组=500,' +
  '一把抓_800元组=800,一把抓_1000元组=1000,11取1_100元组=100,11取1_200元组=200,' +
  '11取1_500元组=500,11取1_800元组=800,11取1_1000元组=1000,22取1_100元组=100,' +
  '22取1_200元组=200,22取1_500元组=500,22取1_800元组=800,22取1_1000元组=1000,' +
  '33取1_100元组=100,33取1_200元组=200,33取1_500元组=500,33取1_800元组=800,' +
  '33取1_1000元组=1000,幸运300_100元组=100,幸运300_200元组=200,幸运500_200元组=200,' +
  '幸运500_500元组=500,幸运1000_300元组=300,幸运1000_800元组=800');
// 荣冠内部设置
RegAppConfig('FACTORY', 'MAXRING', '每袋包装内最大环数', LIDT_Text, '', '1000');
RegAppConfig('FACTORY', 'RINGCOLOR', '电子环颜色', LIDT_Selection, '黑,白,兰,绿,绛,黄,灰,褐', '黑');
// 多机集鸽配置
RegAppConfig('MultiCollection', 'Port', '多机服务端口', LIDT_Int, '', '9007');
RegAppConfig('MultiCollection', 'Mode', '多机集鸽模式', LIDT_Selection,
  '主机显示打印,独立显示打印', '主机显示打印');

finalization

end.
