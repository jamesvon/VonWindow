program PigeonLabel;

uses
  Vcl.Forms,
  WinApi.Windows,
  System.SysUtils,
  Vcl.Dialogs,
  Vcl.Themes,
  Vcl.Styles,
  System.Classes,
  OLE_Excel_TLB in '..\SourceCode\Imports\OLE_Excel_TLB.pas',
  OLE_Html_TLB in '..\SourceCode\Imports\OLE_Html_TLB.pas',
  OLE_Office_TLB in '..\SourceCode\Imports\OLE_Office_TLB.pas',
  OLE_VBIDE_TLB in '..\SourceCode\Imports\OLE_VBIDE_TLB.pas',
  OLE_Word_TLB in '..\SourceCode\Imports\OLE_Word_TLB.pas',
  UVonOffice in '..\SourceCode\Imports\UVonOffice.pas',
  UOfficeExport in '..\SourceCode\Imports\UOfficeExport.pas',
  UVonGraphicAPI in '..\SourceCode\Imports\UVonGraphicAPI.pas',
  UVonGraphicColor in '..\SourceCode\Imports\UVonGraphicColor.pas',
  UVonGraphicCompression in '..\SourceCode\Imports\UVonGraphicCompression.pas',
  UVonGraphicEx in '..\SourceCode\Imports\UVonGraphicEx.pas',
  UVonGraphicJPG in '..\SourceCode\Imports\UVonGraphicJPG.pas',
  UVonGraphicMZLib in '..\SourceCode\Imports\UVonGraphicMZLib.pas',
  UVonGraphicStrings in '..\SourceCode\Imports\UVonGraphicStrings.pas',
  UVonSinoVoiceAPI in '..\SourceCode\Imports\UVonSinoVoiceAPI.pas',
  JTTS_ACTIVEXLib_TLB in '..\SourceCode\Imports\JTTS_ACTIVEXLib_TLB.pas',
  DCPbase64 in '..\SourceCode\dcpcrypt2-2009\DCPbase64.pas',
  DCPblockciphers in '..\SourceCode\dcpcrypt2-2009\DCPblockciphers.pas',
  DCPconst in '..\SourceCode\dcpcrypt2-2009\DCPconst.pas',
  DCPcrypt2 in '..\SourceCode\dcpcrypt2-2009\DCPcrypt2.pas',
  UVonCrypt in '..\SourceCode\dcpcrypt2-2009\UVonCrypt.pas',
  DCPblowfish in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPblowfish.pas',
  DCPcast128 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPcast128.pas',
  DCPcast256 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPcast256.pas',
  DCPdes in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPdes.pas',
  DCPgost in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPgost.pas',
  DCPice in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPice.pas',
  DCPidea in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPidea.pas',
  DCPmars in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPmars.pas',
  DCPmisty1 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPmisty1.pas',
  DCPrc2 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrc2.pas',
  DCPrc4 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrc4.pas',
  DCPrc5 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrc5.pas',
  DCPrc6 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrc6.pas',
  DCPrijndael in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrijndael.pas',
  DCPserpent in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPserpent.pas',
  DCPtea in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPtea.pas',
  DCPtwofish in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPtwofish.pas',
  DCPhaval in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPhaval.pas',
  DCPmd4 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPmd4.pas',
  DCPmd5 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPmd5.pas',
  DCPripemd128 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPripemd128.pas',
  DCPripemd160 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPripemd160.pas',
  DCPsha1 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPsha1.pas',
  DCPsha256 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPsha256.pas',
  DCPsha512 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPsha512.pas',
  DCPtiger in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPtiger.pas',
  UVFW in '..\SourceCode\Components\UVFW.pas',
  UFrameLonLat in '..\SourceCode\Components\UFrameLonLat.pas' {FrameLonLat: TFrame},
  UFrameGridBar in '..\SourceCode\Components\UFrameGridBar.pas' {FrameGridBar: TFrame},
  UFrameTree in '..\SourceCode\Components\UFrameTree.pas' {FrameTree: TFrame},
  UDlgGridColumn in '..\SourceCode\Components\UDlgGridColumn.pas' {FDlgGridColumn},
  UFrameSettingInputor in '..\SourceCode\Components\UFrameSettingInputor.pas' {FrameSettingInputor: TFrame},
  UFrameImageInput in '..\SourceCode\Components\UFrameImageInput.pas' {FrameImageInput: TFrame},
  UDlgCamera in '..\SourceCode\Components\UDlgCamera.pas' {FDlgCamera},
  UFrameRichEditor in '..\SourceCode\Components\UFrameRichEditor.pas' {FrameRichEditor: TFrame},
  UFrameSQL in '..\SourceCode\Components\UFrameSQL.pas' {FrameSql: TFrame},
  UFrameEnterForm in '..\SourceCode\Components\UFrameEnterForm.pas' {FrameEnterForm: TFrame},
  UFrameLayout in '..\SourceCode\Components\UFrameLayout.pas' {FrameLayout: TFrame},
  UDlgConnection in '..\SourceCode\Components\UDlgConnection.pas' {FDlgConnection},
  UFrameViewBar in '..\SourceCode\Components\UFrameViewBar.pas' {FrameViewBar: TFrame},
  UFrameArea in '..\SourceCode\Components\UFrameArea.pas' {FrameArea: TFrame},
  UFrameBarBase in '..\SourceCode\Components\UFrameBarBase.pas' {FrameBarBase: TFrame},
  UFrameBarDeepBlue in '..\SourceCode\Components\UFrameBarDeepBlue.pas' {FrameBarDeepBlue: TFrame},
  UFrameBarSimple in '..\SourceCode\Components\UFrameBarSimple.pas' {FrameBarSimple: TFrame},
  UDlgRichEditor in '..\SourceCode\Components\UDlgRichEditor.pas' {FDlgRichEditor},
  UDlgBrowser in '..\SourceCode\Components\UDlgBrowser.pas' {FDlgBrower},
  USuperObject in '..\SourceCode\Platform\USuperObject.pas',
  UDlgOrganization in '..\SourceCode\Platform\UDlgOrganization.pas' {FDlgOrganization},
  UDlgUser in '..\SourceCode\Platform\UDlgUser.pas' {FDlgUser},
  UOrganization in '..\SourceCode\Platform\UOrganization.pas' {FOrganization},
  UPlatformConfig in '..\SourceCode\Platform\UPlatformConfig.pas' {FPlatformConfig},
  UPlatformDB in '..\SourceCode\Platform\UPlatformDB.pas' {FPlatformDB: TDataModule},
  UPlatformDockForm in '..\SourceCode\Platform\UPlatformDockForm.pas' {FPlatformDockForm},
  UPlatformLogin in '..\SourceCode\Platform\UPlatformLogin.pas' {FPlatformLogin},
  UPlatformMain in '..\SourceCode\Platform\UPlatformMain.pas' {FPlatformMain},
  UPlatformMenu1 in '..\SourceCode\Platform\UPlatformMenu1.pas' {FPlatformMenu1},
  UPlatformMenu2 in '..\SourceCode\Platform\UPlatformMenu2.pas' {FPlatformMenu2},
  UPlatformMenu3 in '..\SourceCode\Platform\UPlatformMenu3.pas' {FPlatformMenu3},
  UPlatformMenuBase in '..\SourceCode\Platform\UPlatformMenuBase.pas' {FPlatformMenuBase},
  UPlatformOptions in '..\SourceCode\Platform\UPlatformOptions.pas' {FPlatformOptions},
  UPlatformParams in '..\SourceCode\Platform\UPlatformParams.pas' {FPlatformParams},
  UPlatformRole in '..\SourceCode\Platform\UPlatformRole.pas' {FPlatformRole},
  UPlatformUser in '..\SourceCode\Platform\UPlatformUser.pas' {FPlatformUser},
  UPlatformZone in '..\SourceCode\Platform\UPlatformZone.pas' {FPlatformZone},
  UVonCalculator in '..\SourceCode\Platform\UVonCalculator.pas',
  UVonClass in '..\SourceCode\Platform\UVonClass.pas',
  UVonConsoleLog in '..\SourceCode\Platform\UVonConsoleLog.pas',
  UVonDBLog in '..\SourceCode\Platform\UVonDBLog.pas',
  UVonEventLog in '..\SourceCode\Platform\UVonEventLog.pas',
  UVonFileLog in '..\SourceCode\Platform\UVonFileLog.pas',
  UVonLog in '..\SourceCode\Platform\UVonLog.pas',
  UVonSystemFuns in '..\SourceCode\Platform\UVonSystemFuns.pas',
  UPlatformInfo in '..\SourceCode\Platform\UPlatformInfo.pas',
  UVonMenuInfo in '..\SourceCode\Platform\UVonMenuInfo.pas',
  UPlatformUpgrade in '..\SourceCode\Platform\UPlatformUpgrade.pas' {FPlatFormUpgrade},
  UVonTransmissionApp in '..\SourceCode\Platform\UVonTransmissionApp.pas',
  UVonTransmissionBase in '..\SourceCode\Platform\UVonTransmissionBase.pas',
  UVonMenuDesigner in '..\SourceCode\Platform\UVonMenuDesigner.pas' {FVonMenuDesigner},
  UFastPrinter in '..\Application\PigeonLabel\UFastPrinter.pas' {FFastPrinter},
  UFlyLabelDB in '..\Application\PigeonLabel\UFlyLabelDB.pas' {FFlyLabelDB: TDataModule},
  UFlyLabelDesigner in '..\Application\PigeonLabel\UFlyLabelDesigner.pas' {FFlyLabelDesigner},
  UFlyLabelEdit in '..\Application\PigeonLabel\UFlyLabelEdit.pas' {FFlyLabelEdit},
  UFlyLabelGBCode in '..\Application\PigeonLabel\UFlyLabelGBCode.pas' {FFlyLabelGBCode},
  UFlyLabelInfo in '..\Application\PigeonLabel\UFlyLabelInfo.pas',
  UFlyLabelList in '..\Application\PigeonLabel\UFlyLabelList.pas' {FFlyLabelList},
  UFlyLabelMain in '..\Application\PigeonLabel\UFlyLabelMain.pas' {FFlyLabelMain},
  UFlyLabelPreview in '..\Application\PigeonLabel\UFlyLabelPreview.pas' {FFlyLabelPreview},
  UFlyLabelPrint in '..\Application\PigeonLabel\UFlyLabelPrint.pas' {FFlyLabelPrint},
  UFlyLabelTicketPrint in '..\Application\PigeonLabel\UFlyLabelTicketPrint.pas' {FFlyLabelTicketPrint},
  ULabelPrint in '..\Application\PigeonLabel\ULabelPrint.pas',
  UThreadLabel in '..\Application\PigeonLabel\UThreadLabel.pas',
  UVonQRCode in '..\Application\PigeonLabel\UVonQRCode.pas',
  UVonXorCrypt in '..\Application\PigeonLabel\UVonXorCrypt.pas',
  UZXIngQRCode in '..\Application\PigeonLabel\UZXIngQRCode.pas';

var
  hMutex: THandle;
  Ret: Integer;

{$R *.res}

begin
  CERT_CIPHER:= 'ice';
  CERT_HASH:= 'sha1';
  CERT_KEY := 'JAMESVON VON PigeonLabel CERTIFICATION';
  SYS_NAME := 'PIGEONLB';         //用于远程升级唯一名称
  CERT_FILE := 'PigeonLabel.CERT';
  hMutex := CreateMutex(nil, true, 'PIGEONLB');
  Ret := GetLastError;
  OpenLog('FILELOG', SYS_NAME, ExtractFilePath(Application.ExeName) + 'LOG\', LOG_DEBUG); // 打开本地日志
  if Ret <> ERROR_ALREADY_EXISTS then
  begin
    Application.Initialize;
  end
  else
  begin
    Application.MessageBox('程序已经运行!', '系统提示', MB_OK);
    ReleaseMutex(hMutex);
    Exit;
  end;
  {$region 'Platform'}
  RegAppCommand('界面设计', '系统功能及菜单设计', TFVonMenuDesigner, 0, '');
  RegAppCommand('角色管理', '管理系统角色信息', TFPlatformRole, 0, '');
  RegAppCommand('人员管理', '管理系统用户', TFPlatFormUser, 0, '');
  RegAppCommand('系统选项', '管理系统各种参数、选项以及支持信息', TFPlatformOptions, 0, '');
  RegAppCommand('系统配置', '管理系统各种配置项', TFPlatformConfig, 0, '');
  RegAppCommand('系统参数', '系统参数的管理与维护', TFPlatformParams, 0, '');
  RegAppCommand('组织结构', '管理组织架构信息', TFOrganization, 0, '');
  {$endregion}
  {$region 'Platform'}
  RegAppCommand('样式管理', '管理各种足环的样式定制和类别管理', TFFlyLabelList, 0, '');
  RegAppCommand('环标打印', '环标打印', TFFlyLabelPrint, 0, '');
  RegAppCommand('环标证', '环标证设计', TFFlyLabelDesigner, 0, '');
  RegAppCommand('环标证补打', '环标证补打', TFFlyLabelTicketPrint, 0, '');
  RegAppCommand('快速打印', '快速打印', TFFastPrinter, 0, '');
  {$endregion}

  TStyleManager.TrySetStyle('Cyan Night');
  Application.CreateForm(TFFlyLabelDB, FFlyLabelDB);
  if FFlyLabelDB.CanRun then
  begin
    WriteLog(LOG_DEBUG, 'Application', 'End of inited communication ...... ');
    FFlyLabelDB.InitData();
    Application.MainFormOnTaskbar := true;
    Application.CreateForm(TFFlyLabelMain, FFlyLabelMain);
    Say('欢迎您使用' + FFlyLabelDB.ApplicationTitle);
  end
  else if GetInfoList.Count <> 0 then
    ShowMessage(GetInfoList.Text);
  WriteLog(LOG_DEBUG, 'Application', 'Application will run ... ...');
  Application.Run;
end.
