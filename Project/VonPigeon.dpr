program VonPigeon;

uses
  System.SysUtils,
  System.Classes,
  Vcl.Forms,
  Vcl.Themes,
  Vcl.Styles,
  Vcl.Dialogs,
  WinApi.Windows,
  UPlatformDB,
  UVonSystemFuns,
  UVonLog,
  UVonFileLog,
  UVonSinoVoiceAPI,
  UCoteInfo in '..\SourceCode\Pigeon\UCoteInfo.pas',
  UPigeonDB in '..\SourceCode\Pigeon\UPigeonDB.pas' {FPigeonDB: TDataModule},
  UCoteComm in '..\SourceCode\Pigeon\UCoteComm.pas',
  UCoteMember in '..\SourceCode\Pigeon\UCoteMember.pas' {FCoteMember},
  UDlgMember in '..\SourceCode\Pigeon\UDlgMember.pas' {FDlgMember},
  UCotePigeon in '..\SourceCode\Pigeon\UCotePigeon.pas' {FCotePigeon},
  UCoteMatch in '..\SourceCode\Pigeon\UCoteMatch.pas' {FCoteMatch},
  UDlgMatch in '..\SourceCode\Pigeon\UDlgMatch.pas' {FDlgMatch},
  UDlgInputGBCode in '..\SourceCode\Pigeon\UDlgInputGBCode.pas' {FDlgInputGBCode},
  UDlgGameTime in '..\SourceCode\Pigeon\UDlgGameTime.pas' {FDlgGameTime},
  UDlgSunTime in '..\SourceCode\Pigeon\UDlgSunTime.pas' {FDlgSunTime},
  UCoteCheckIn in '..\SourceCode\Pigeon\UCoteCheckIn.pas' {FCoteCheckIn},
  UDlgPigeon in '..\SourceCode\Pigeon\UDlgPigeon.pas' {FDlgPigeon},
  UCoteTeamSearch in '..\SourceCode\Pigeon\UCoteTeamSearch.pas' {FCoteTeamSearch},
  UCoteStageSearch in '..\SourceCode\Pigeon\UCoteStageSearch.pas' {FCoteStageSearch},
  UCoteSoueceSearch in '..\SourceCode\Pigeon\UCoteSoueceSearch.pas' {FCoteSoueceSearch},
  UCotePigeonSearch in '..\SourceCode\Pigeon\UCotePigeonSearch.pas' {FCotePigeonSearch},
  UCoteBackSearch in '..\SourceCode\Pigeon\UCoteBackSearch.pas' {FCoteBackSearch},
  UCoteMultiSearch in '..\SourceCode\Pigeon\UCoteMultiSearch.pas' {FCoteMultiSearch},
  UCoteCollectionSearch in '..\SourceCode\Pigeon\UCoteCollectionSearch.pas' {FCoteCollectionSearch},
  UCoteSoueceStatistic in '..\SourceCode\Pigeon\UCoteSoueceStatistic.pas' {FCoteSoueceStatistic},
  UDlgReadRing in '..\SourceCode\Pigeon\UDlgReadRing.pas' {FDlgReadRing},
  UCoteGrouping in '..\SourceCode\Pigeon\UCoteGrouping.pas' {FCoteGrouping},
  UCoteGroupingSearch in '..\SourceCode\Pigeon\UCoteGroupingSearch.pas' {FCoteGroupingSearch},
  UCote in '..\SourceCode\Pigeon\UCote.pas' {FCote},
  UDlgCote in '..\SourceCode\Pigeon\UDlgCote.pas' {FDlgCote},
  UCoteCollection in '..\SourceCode\Pigeon\UCoteCollection.pas' {FCollection},
  UEnvironment in '..\SourceCode\Pigeon\UEnvironment.pas',
  UCoteChoose in '..\SourceCode\Pigeon\UCoteChoose.pas' {FCoteChoose},
  UCoteRecharge in '..\SourceCode\Pigeon\UCoteRecharge.pas' {FCoteRecharge},
  UCommunication in '..\SourceCode\Factory\UCommunication.pas',
  UDlgCommunication in '..\SourceCode\Factory\UDlgCommunication.pas' {FDlgCommunication},
  UKaiXinRing in '..\SourceCode\Factory\UKaiXinRing.pas' {FKaiXinRings},
  UKaiXinMachine in '..\SourceCode\Factory\UKaiXinMachine.pas',
  URongguanMachine in '..\SourceCode\Factory\URongguanMachine.pas',
  UVonPigeonMain in '..\SourceCode\Pigeon\UVonPigeonMain.pas' {FPigeonMain};

var
  hMutex: THandle;
  Ret: Integer;

{$R *.res}

begin
  CERT_CIPHER:= 'ice';
  CERT_HASH:= 'sha1';
  CERT_KEY := #3#5'JAMESVON VON PIGEON CERTIFICATION'#5#3;
  SYS_NAME := 'V_PIGEON';         //用于远程升级唯一名称
  CERT_FILE := 'VonPigeon.CERT';
  hMutex := CreateMutex(nil, true, 'V_PIGEON');
  Ret := GetLastError;
  OpenLog('FILELOG', SYS_NAME, ExtractFilePath(Application.ExeName) + 'LOG\', LOG_DEBUG); // 打开本地日志
  if Ret <> ERROR_ALREADY_EXISTS then
  begin
    Application.Initialize;
  end
  else
  begin
    Application.MessageBox('程序已经运行!', '系统提示', MB_OK);
    ReleaseMutex(hMutex);
    Exit;
  end;
  {$region 'Platform'}
//  RegAppCommand('帮助命令', '显示系统帮助信息', TFPlatformHelp, 0, 'HELP');
//  RegAppCommand('链接网站', '显示主网站信息', TFPlatformHelp, 0, 'WEB');
//  RegAppCommand('关于命令', '显示关于信息', TFPlatformHelp, 0, '');
//  RegAppCommand('角色管理', '管理系统角色信息', TFPlatformRole, 0, '');
//  RegAppCommand('人员管理', '管理系统用户', TFPlatFormUser, 0, '');
//  RegAppCommand('系统选项', '管理系统各种参数、选项以及支持信息', TFPlatformOptions, 0, '');
//  RegAppCommand('系统配置', '管理系统各种配置项', TFPlatformConfig, 0, '');
//  RegAppCommand('系统参数', '系统参数的管理与维护', TFPlatformParams, 0, '');
//  RegAppCommand('备份恢复', '备份与恢复系统数据库', TFPlatformTachDB, 0, '');
//  RegAppCommand('查询管理', '通用查询模板管理', TFPlatformQueryManage, 0, '');
//  RegAppCommand('查询分析', '通用查询与分析管理模块', TFPlatformQuery, 0, '');
//  RegAppCommand('调试工具', '数据库调试工具', TFPlatformDebug, 0, '');
//  RegAppCommand('字典管理', '数据库字段字典管理', TFPlatformDBDictionary, 0, '');
//  RegAppCommand('自身信息', '管理自身的信息', TFDlgCote, 0, '自身');
//  RegAppCommand('组织结构', '管理组织架构信息', TFOrganization, 0, '');
  {$endregion}
  {$region 'Cote manage'}
  RegAppCommand('入棚管理', '管理会员及会员赛鸽的入棚与信息录入', TFCotePigeon, 0, '');
  RegAppCommand('会员管理', '管理会员及会员信息录入', TFCoteMember, 0, '');
  RegAppCommand('赛事管理', '对训放和比赛进行管理', TFCoteMatch, 0, '');
  RegAppCommand('分组管理', '对赛鸽分组进行专项管理', TFCoteGrouping, 0, '');
  {$endregion}
  {$region '查询功能'}
  RegAppCommand('入棚查询', '入棚查询', TFCotePigeonSearch, 0, '');
  RegAppCommand('团体查询', '团体查询', TFCoteGroupingSearch, 0, '');
  RegAppCommand('关赛查询', '关赛查询', TFCoteStageSearch, 0, '');
  RegAppCommand('成绩查询', '成绩查询', TFCoteSoueceSearch, 0, '');
  RegAppCommand('赛绩查询', '赛绩查询', TFCoteBackSearch, 0, '');
  RegAppCommand('联赛成绩', '联赛成绩', TFCoteMultiSearch, 0, '');
  RegAppCommand('集鸽查询', '集鸽查询', TFCoteCollectionSearch, 0, '');
  //RegAppCommand('指定鸽查询', '指定鸽查询', TFCoteChooseSearch, 0, '');
  {$endregion}
  {$region '统计功能'}
  RegAppCommand('赛绩统计', '赛绩统计', TFCoteSoueceStatistic, 0, '');
  {$endregion}
  Application.UseMetropolisUI;
  Application.DefaultFont.Size:= 8;
  TStyleManager.TrySetStyle('Iceberg Classico');
  Application.CreateForm(TFPigeonDB, FPigeonDB);
  //Application.CreateForm(TFKeXinRings, FKeXinRings);
  WriteLog(LOG_INFO, 'Logined', FPigeonDB.CertValue['CurrentVersion'] +
    '[' + Get_ApplicationVersion(Application.ExeName) + ']');
  //FCoteDB.RegisteChmHelp(FCoteDB.AppPath + 'Cote.CHM');  //加载帮助文件
  if FPigeonDB.CanRun then
  begin
    WriteLog(LOG_DEBUG, 'Application', 'End of inited communication ...... ');
    Application.MainFormOnTaskbar := true;
    Application.Title := FPigeonDB.CertValue['ApplicationName'];
    Application.CreateForm(TFPigeonMain, FPigeonMain);
    //Application.CreateForm(TFCoteMain, FCoteMain);
    FEventOnNewChildWin:= FPigeonMain.OpenModule;
    //TThread.CreateAnonymousThread(JoinServiceMonitor).Start;
    Say('欢迎您使用' + FPigeonDB.CertValue['ApplicationName']);
  end
  else if GetInfoList.Count <> 0 then
    ShowMessage(GetInfoList.Text);
  WriteLog(LOG_DEBUG, 'Application', 'Application will run ... ...');
  Application.Run;
end.
