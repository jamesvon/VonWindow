program VonTools;

uses
  Vcl.Forms,
  UVonTools in '..\SourceCode\Tools\UVonTools.pas' {FVonTools},
  DCPbase64 in '..\SourceCode\dcpcrypt2-2009\DCPbase64.pas',
  DCPblockciphers in '..\SourceCode\dcpcrypt2-2009\DCPblockciphers.pas',
  DCPconst in '..\SourceCode\dcpcrypt2-2009\DCPconst.pas',
  DCPcrypt2 in '..\SourceCode\dcpcrypt2-2009\DCPcrypt2.pas',
  UVonCrypt in '..\SourceCode\dcpcrypt2-2009\UVonCrypt.pas',
  DCPblowfish in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPblowfish.pas',
  DCPcast128 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPcast128.pas',
  DCPcast256 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPcast256.pas',
  DCPdes in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPdes.pas',
  DCPgost in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPgost.pas',
  DCPice in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPice.pas',
  DCPidea in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPidea.pas',
  DCPmars in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPmars.pas',
  DCPmisty1 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPmisty1.pas',
  DCPrc2 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrc2.pas',
  DCPrc4 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrc4.pas',
  DCPrc5 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrc5.pas',
  DCPrc6 in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrc6.pas',
  DCPrijndael in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPrijndael.pas',
  DCPserpent in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPserpent.pas',
  DCPtea in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPtea.pas',
  DCPtwofish in '..\SourceCode\dcpcrypt2-2009\Ciphers\DCPtwofish.pas',
  DCPhaval in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPhaval.pas',
  DCPmd4 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPmd4.pas',
  DCPmd5 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPmd5.pas',
  DCPripemd128 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPripemd128.pas',
  DCPripemd160 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPripemd160.pas',
  DCPsha1 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPsha1.pas',
  DCPsha256 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPsha256.pas',
  DCPsha512 in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPsha512.pas',
  DCPtiger in '..\SourceCode\dcpcrypt2-2009\Hashes\DCPtiger.pas',
  UVonClass in '..\SourceCode\Platform\UVonClass.pas',
  UVonSystemFuns in '..\SourceCode\Platform\UVonSystemFuns.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFVonTools, FVonTools);
  //Application.CreateForm(TFTestCrypt, FTestCrypt);
  Application.Run;
end.
