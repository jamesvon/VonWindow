object FDlgGridColumn: TFDlgGridColumn
  Left = 225
  Top = 114
  BorderStyle = bsDialog
  Caption = #21015#34920#26174#31034#35843#25972
  ClientHeight = 428
  ClientWidth = 181
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 15
  object Label1: TLabel
    Left = 8
    Top = 304
    Width = 72
    Height = 15
    Caption = #23383#27573#26174#31034#21517#31216
  end
  object btnDelColumn: TSpeedButton
    Left = 152
    Top = 298
    Width = 23
    Height = 22
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333333333333333333333333FFF33FF333FFF339993370733
      999333777FF37FF377733339993000399933333777F777F77733333399970799
      93333333777F7377733333333999399933333333377737773333333333990993
      3333333333737F73333333333331013333333333333777FF3333333333910193
      333333333337773FF3333333399000993333333337377737FF33333399900099
      93333333773777377FF333399930003999333337773777F777FF339993370733
      9993337773337333777333333333333333333333333333333333333333333333
      3333333333333333333333333333333333333333333333333333}
    NumGlyphs = 2
    Visible = False
    OnClick = btnDelColumnClick
  end
  object Label2: TLabel
    Left = 8
    Top = 344
    Width = 48
    Height = 15
    Caption = #23383#27573#23485#24230
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 181
    Height = 297
    Align = alTop
    Caption = #26174#31034#20869#23481
    TabOrder = 0
    object CheckListBox1: TCheckListBox
      Left = 2
      Top = 17
      Width = 152
      Height = 278
      Align = alClient
      ItemHeight = 15
      TabOrder = 0
      OnClick = CheckListBox1Click
    end
    object Panel1: TPanel
      Left = 154
      Top = 17
      Width = 25
      Height = 278
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object SpeedButton1: TSpeedButton
        Left = 4
        Top = 8
        Width = 17
        Height = 17
        Hint = #20840#36873
        Flat = True
        Glyph.Data = {
          66030000424D6603000000000000360000002800000010000000110000000100
          1800000000003003000000000000000000000000000000000000FF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FF8000008000008000008000008000008000008000
          00800000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF800000FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000FF00FFFF00FFFF00FFFF00FF
          FF00FF800000800000800000800000800000800000800000800000FFFFFFFFFF
          FF800000FF00FFFF00FFFF00FFFF00FFFF00FF800000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF800000FFFFFFFFFFFF800000FF00FFFF00FF800000800000
          800000800000800000800000800000800000FFFFFFFFFFFF800000FFFFFFFFFF
          FF800000FF00FFFF00FF800000FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF80
          0000FFFFFFFFFFFF800000FFFFFFFFFFFF800000FF00FFFF00FF800000FFFFFF
          0000FF0000FFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFF800000FFFFFFFFFF
          FF800000FF00FFFF00FF8000000000FFFFFFFFFFFFFF0000FFFFFFFFFFFFFF80
          0000FFFFFFFFFFFF800000800000800000800000FF00FFFF00FF800000FFFFFF
          FFFFFFFFFFFFFFFFFF0000FFFFFFFF800000FFFFFFFFFFFF800000FF00FFFF00
          FFFF00FFFF00FFFF00FF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FF80
          0000800000800000800000FF00FFFF00FFFF00FFFF00FFFF00FF800000FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000FF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FF80000080000080000080000080000080000080000080
          0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FF}
        ParentShowHint = False
        ShowHint = True
        OnClick = SpeedButton1Click
      end
      object SpeedButton2: TSpeedButton
        Left = 4
        Top = 40
        Width = 17
        Height = 17
        Hint = #20840#37096#21462#28040
        Flat = True
        Glyph.Data = {
          66030000424D6603000000000000360000002800000010000000110000000100
          1800000000003003000000000000000000000000000000000000FF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FF8000008000008000008000008000008000008000
          00800000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF800000FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000FF00FFFF00FFFF00FFFF00FF
          FF00FF800000800000800000800000800000800000800000800000FFFFFFFFFF
          FF800000FF00FFFF00FFFF00FFFF00FFFF00FF800000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF800000FFFFFFFFFFFF800000FF00FFFF00FF800000800000
          800000800000800000800000800000800000FFFFFFFFFFFF800000FFFFFFFFFF
          FF800000FF00FFFF00FF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80
          0000FFFFFFFFFFFF800000FFFFFFFFFFFF800000FF00FFFF00FF800000FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFF800000FFFFFFFFFF
          FF800000FF00FFFF00FF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80
          0000FFFFFFFFFFFF800000800000800000800000FF00FFFF00FF800000FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFF800000FF00FFFF00
          FFFF00FFFF00FFFF00FF800000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80
          0000800000800000800000FF00FFFF00FFFF00FFFF00FFFF00FF800000FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800000FF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FF80000080000080000080000080000080000080000080
          0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FF}
        ParentShowHint = False
        ShowHint = True
        OnClick = SpeedButton2Click
      end
      object SpeedButton3: TSpeedButton
        Left = 4
        Top = 80
        Width = 17
        Height = 17
        Hint = #19978#31227
        Flat = True
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          04000000000080000000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333000333333333333309033333333333330903333333333333090333
          3333333333090333333333333309033333333333000900033333333309999903
          3333333330999033333333333099903333333333330903333333333333090333
          3333333000000000333333300000000033333333333333333333}
        ParentShowHint = False
        ShowHint = True
        OnClick = SpeedButton3Click
      end
      object SpeedButton4: TSpeedButton
        Left = 4
        Top = 120
        Width = 17
        Height = 17
        Hint = #19979#31227
        Flat = True
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          04000000000080000000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333000333333333333309033333333333330903333333333333090333
          3333333333090333333333333309033333333333000900033333333309999903
          3333333330999033333333333099903333333333330903333333333333090333
          3333333333303333333333333330333333333333333333333333}
        ParentShowHint = False
        ShowHint = True
        OnClick = SpeedButton4Click
      end
      object SpeedButton5: TSpeedButton
        Left = 4
        Top = 160
        Width = 17
        Height = 17
        Hint = #19978#31227
        Flat = True
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          04000000000080000000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333303333333333333330333333333333330903333333333333090333
          3333333330999033333333333099903333333333099999033333333300090003
          3333333333090333333333333309033333333333330903333333333333090333
          3333333333090333333333333300033333333333333333333333}
        ParentShowHint = False
        ShowHint = True
        OnClick = SpeedButton5Click
      end
      object SpeedButton6: TSpeedButton
        Left = 4
        Top = 200
        Width = 17
        Height = 17
        Hint = #19979#31227
        Flat = True
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          04000000000080000000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333300000003333333330000000333333333330903333333333333090333
          3333333330999033333333333099903333333333099999033333333300090003
          3333333333090333333333333309033333333333330903333333333333090333
          3333333333090333333333333300033333333333333333333333}
        ParentShowHint = False
        ShowHint = True
        OnClick = SpeedButton6Click
      end
    end
  end
  object BitBtn1: TBitBtn
    Left = 8
    Top = 392
    Width = 67
    Height = 25
    Caption = #30830#23450
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 1
  end
  object BitBtn2: TBitBtn
    Left = 112
    Top = 392
    Width = 67
    Height = 25
    Caption = #25918#24323
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 2
  end
  object EFieldCaption: TEdit
    Left = 8
    Top = 320
    Width = 169
    Height = 23
    TabOrder = 3
  end
  object EWidth: TSpinEdit
    Left = 8
    Top = 360
    Width = 97
    Height = 24
    MaxValue = 0
    MinValue = 0
    TabOrder = 4
    Value = 0
  end
  object btnUpdate: TBitBtn
    Left = 112
    Top = 360
    Width = 67
    Height = 25
    Caption = #20462#25913
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
      000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
      00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
      F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
      0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
      FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
      FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
      0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
      00333377737FFFFF773333303300000003333337337777777333}
    NumGlyphs = 2
    TabOrder = 5
    OnClick = btnUpdateClick
  end
end
