object FDlgOrganization: TFDlgOrganization
  Left = 0
  Top = 0
  Caption = #22522#26412#20449#24687
  ClientHeight = 309
  ClientWidth = 321
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 11
    Top = 72
    Width = 48
    Height = 13
    Caption = #25152#22312#30465#24066
  end
  object lbKind: TLabel
    Left = 11
    Top = 115
    Width = 48
    Height = 13
    Caption = #21333#20301#24615#36136
  end
  object lbCity: TLabel
    Left = 113
    Top = 72
    Width = 48
    Height = 13
    Caption = #25152#22312#22320#24066
  end
  object lbCounty: TLabel
    Left = 217
    Top = 72
    Width = 48
    Height = 13
    Caption = #25152#22312#21306#21439
  end
  object lbOrgID: TLabel
    Left = 11
    Top = 8
    Width = 3
    Height = 13
  end
  object lbCode: TLabel
    Left = 11
    Top = 28
    Width = 24
    Height = 13
    Caption = #32534#30721
  end
  object EOrgName: TLabeledEdit
    Left = 113
    Top = 44
    Width = 201
    Height = 21
    EditLabel.Width = 24
    EditLabel.Height = 13
    EditLabel.Caption = #21517#31216
    TabOrder = 0
  end
  object EState: TComboBox
    Left = 11
    Top = 91
    Width = 96
    Height = 21
    Style = csDropDownList
    TabOrder = 1
    OnChange = EStateChange
  end
  object ECounty: TComboBox
    Left = 217
    Top = 91
    Width = 97
    Height = 21
    Style = csDropDownList
    TabOrder = 2
    Visible = False
  end
  object EManager: TLabeledEdit
    Left = 113
    Top = 132
    Width = 98
    Height = 21
    EditLabel.Width = 36
    EditLabel.Height = 13
    EditLabel.Caption = #36127#36131#20154
    TabOrder = 3
  end
  object ETelphong: TLabeledEdit
    Left = 217
    Top = 132
    Width = 97
    Height = 21
    EditLabel.Width = 48
    EditLabel.Height = 13
    EditLabel.Caption = #32852#31995#30005#35805
    TabOrder = 4
  end
  object EAddress: TLabeledEdit
    Left = 11
    Top = 175
    Width = 303
    Height = 21
    EditLabel.Width = 24
    EditLabel.Height = 13
    EditLabel.Caption = #22320#22336
    TabOrder = 5
  end
  object btnOK: TBitBtn
    Left = 158
    Top = 263
    Width = 75
    Height = 25
    Caption = #30830#23450
    Default = True
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    ModalResult = 1
    NumGlyphs = 2
    TabOrder = 6
    OnClick = btnOKClick
  end
  object BitBtn2: TBitBtn
    Left = 239
    Top = 263
    Width = 75
    Height = 25
    Caption = #25918#24323
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 7
    OnClick = BitBtn2Click
  end
  object ECity: TComboBox
    Left = 113
    Top = 91
    Width = 98
    Height = 21
    Style = csDropDownList
    TabOrder = 10
    Visible = False
    OnChange = ECityChange
  end
  object EKind: TComboBox
    Left = 11
    Top = 132
    Width = 96
    Height = 21
    Style = csDropDownList
    TabOrder = 8
  end
  object ECode: TMaskEdit
    Left = 11
    Top = 44
    Width = 96
    Height = 21
    ReadOnly = True
    TabOrder = 9
    Text = ''
  end
  inline FrameLonLat1: TFrameLonLat
    Left = 11
    Top = 202
    Width = 151
    Height = 55
    TabOrder = 11
    ExplicitLeft = 11
    ExplicitTop = 202
    ExplicitWidth = 151
    ExplicitHeight = 55
    inherited EC: TEdit
      Width = 69
      Height = 34
      ExplicitWidth = 69
      ExplicitHeight = 34
    end
    inherited EF: TEdit
      Left = 69
      Height = 34
      ExplicitLeft = 69
      ExplicitHeight = 34
    end
    inherited ES: TEdit
      Left = 110
      Height = 34
      ExplicitLeft = 110
      ExplicitTop = 21
      ExplicitHeight = 21
    end
    inherited Panel1: TPanel
      Width = 151
      ExplicitWidth = 151
      inherited Label1: TLabel
        Width = 24
        Caption = #32463#24230
        ExplicitWidth = 24
      end
      inherited rbS: TRadioButton
        Left = 118
        ExplicitLeft = 118
      end
      inherited rbF: TRadioButton
        Left = 85
        ExplicitLeft = 85
      end
      inherited rbC: TRadioButton
        Left = 52
        ExplicitLeft = 52
      end
    end
  end
  inline FrameLonLat2: TFrameLonLat
    Left = 168
    Top = 202
    Width = 140
    Height = 55
    TabOrder = 12
    ExplicitLeft = 168
    ExplicitTop = 202
    ExplicitHeight = 55
    inherited EC: TEdit
      Height = 34
      ExplicitHeight = 34
    end
    inherited EF: TEdit
      Height = 34
      ExplicitHeight = 34
    end
    inherited ES: TEdit
      Height = 34
      ExplicitLeft = 99
      ExplicitTop = 21
      ExplicitHeight = 21
    end
    inherited Panel1: TPanel
      inherited Label1: TLabel
        Width = 24
        Caption = #32428#24230
        ExplicitWidth = 24
      end
    end
  end
  object DQZone: TADOQuery
    Connection = FPlatformDB.ADOConn
    Parameters = <
      item
        Name = 'PID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'SELECT * FROM SYS_Zone'
      'WHERE PID=:PID')
    Left = 72
    Top = 272
  end
end
