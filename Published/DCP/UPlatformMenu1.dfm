inherited FPlatformMenu1: TFPlatformMenu1
  BorderStyle = bsSizeToolWin
  Caption = 'FPlatformMenu1'
  ClientHeight = 352
  ClientWidth = 115
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  ExplicitWidth = 131
  ExplicitHeight = 391
  PixelsPerInch = 96
  TextHeight = 13
  object lvMenu: TListView
    Left = 0
    Top = 20
    Width = 115
    Height = 332
    Align = alClient
    Columns = <>
    ParentColor = True
    TabOrder = 0
    OnClick = lvMenuClick
  end
  object plParent: TPanel
    Left = 0
    Top = 0
    Width = 115
    Height = 20
    Align = alTop
    Alignment = taLeftJustify
    Caption = '<<'
    TabOrder = 1
  end
end
