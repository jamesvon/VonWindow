inherited FPlatformMenu2: TFPlatformMenu2
  BorderStyle = bsSizeToolWin
  Caption = 'FPlatformMenu2'
  ClientHeight = 419
  ClientWidth = 120
  Color = clBackground
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  ExplicitWidth = 136
  ExplicitHeight = 458
  PixelsPerInch = 96
  TextHeight = 13
  object ButtonGroup1: TButtonGroup
    Left = 0
    Top = 0
    Width = 120
    Height = 419
    Align = alClient
    BorderStyle = bsNone
    ButtonOptions = [gboFullSize, gboShowCaptions]
    Images = FPlatformDB.ImgButton
    Items = <
      item
        Caption = #31995#32479
        ImageIndex = 5
      end
      item
        Caption = #27979#35797
        ImageIndex = 5
      end>
    TabOrder = 0
  end
end
