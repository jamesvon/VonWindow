inherited FPlatformMenu3: TFPlatformMenu3
  BorderStyle = bsSizeToolWin
  Caption = 'FPlatformMenu3'
  ClientHeight = 338
  ClientWidth = 122
  ParentFont = True
  OnCreate = FormCreate
  ExplicitWidth = 138
  ExplicitHeight = 377
  PixelsPerInch = 96
  TextHeight = 13
  object CategoryButtons1: TCategoryButtons
    Left = 0
    Top = 0
    Width = 122
    Height = 338
    Align = alClient
    ButtonFlow = cbfVertical
    ButtonOptions = [boFullSize, boGradientFill, boShowCaptions, boVerticalCategoryCaptions]
    Categories = <>
    DoubleBuffered = True
    ParentDoubleBuffered = False
    RegularButtonColor = clWhite
    SelectedButtonColor = 15132390
    TabOrder = 0
    OnButtonClicked = CategoryButtons1ButtonClicked
    OnSelectedCategoryChange = CategoryButtons1SelectedCategoryChange
  end
end
