/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2012                    */
/* Created on:     2022/04/12 09:01:36                          */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('SYS_Right') and o.name = 'FK_RIGH_REF_ROLE')
alter table SYS_Right
   drop constraint FK_RIGH_REF_ROLE
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('SYS_User') and o.name = 'FK_USER_REF_GROUP')
alter table SYS_User
   drop constraint FK_USER_REF_GROUP
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('SYS_UserRole') and o.name = 'FK_ROLE_REF_USER')
alter table SYS_UserRole
   drop constraint FK_ROLE_REF_USER
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('SYS_UserRole') and o.name = 'FK_USER_REF_ROLE')
alter table SYS_UserRole
   drop constraint FK_USER_REF_ROLE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('SYS_Log')
            and   name  = 'Idx_LogClass'
            and   indid > 0
            and   indid < 255)
   drop index SYS_Log.Idx_LogClass
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('SYS_Log')
            and   name  = 'Idx_Processor'
            and   indid > 0
            and   indid < 255)
   drop index SYS_Log.Idx_Processor
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('SYS_Log')
            and   name  = 'Idx_User'
            and   indid > 0
            and   indid < 255)
   drop index SYS_Log.Idx_User
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('SYS_Log')
            and   name  = 'Idx_LogTime'
            and   indid > 0
            and   indid < 255)
   drop index SYS_Log.Idx_LogTime
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SYS_Log')
            and   type = 'U')
   drop table SYS_Log
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SYS_Option')
            and   type = 'U')
   drop table SYS_Option
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SYS_Organization')
            and   type = 'U')
   drop table SYS_Organization
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SYS_Params')
            and   type = 'U')
   drop table SYS_Params
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SYS_Right')
            and   type = 'U')
   drop table SYS_Right
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SYS_Role')
            and   type = 'U')
   drop table SYS_Role
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SYS_User')
            and   type = 'U')
   drop table SYS_User
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SYS_UserGroup')
            and   type = 'U')
   drop table SYS_UserGroup
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SYS_UserRole')
            and   type = 'U')
   drop table SYS_UserRole
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('SYS_Zone')
            and   name  = 'IDX_Zone'
            and   indid > 0
            and   indid < 255)
   drop index SYS_Zone.IDX_Zone
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SYS_Zone')
            and   type = 'U')
   drop table SYS_Zone
go

/*==============================================================*/
/* Table: SYS_Log                                               */
/*==============================================================*/
create table SYS_Log (
   LogTime              datetime             null,
   UserName             nvarchar(20)         null,
   LogClass             int                  null,
   Processor            nvarchar(28)         null,
   Content              nvarchar(1000)       null
)
go

/*==============================================================*/
/* Index: Idx_LogTime                                           */
/*==============================================================*/
create index Idx_LogTime on SYS_Log (
LogTime ASC
)
go

/*==============================================================*/
/* Index: Idx_User                                              */
/*==============================================================*/
create index Idx_User on SYS_Log (
UserName ASC
)
go

/*==============================================================*/
/* Index: Idx_Processor                                         */
/*==============================================================*/
create index Idx_Processor on SYS_Log (
Processor ASC
)
go

/*==============================================================*/
/* Index: Idx_LogClass                                          */
/*==============================================================*/
create index Idx_LogClass on SYS_Log (
LogClass ASC
)
go

/*==============================================================*/
/* Table: SYS_Option                                            */
/*==============================================================*/
create table SYS_Option (
   ID                   int                  identity,
   OptName              nvarchar(20)         null,
   OptKind              int                  null,
   OptInfo              nvarchar(50)         null,
   OptValues            text                 null,
   constraint PK_SYS_OPTION primary key (ID)
)
go

/*==============================================================*/
/* Table: SYS_Organization                                      */
/*==============================================================*/
create table SYS_Organization (
   ID                   int                  identity,
   OrgID                uniqueidentifier     not null,
   OrgCode              nvarchar(20)         null,
   OrgName              nvarchar(100)        not null,
   PID                  int                  null,
   State                nvarchar(20)         null,
   City                 nvarchar(20)         null,
   County               nvarchar(20)         null,
   Kind                 int                  null,
   Address              nvarchar(500)        null,
   Manager              nvarchar(20)         null,
   Telphong             nvarchar(100)        null,
   ListOrder            int                  null,
   Lon                  int                  null,
   Lat                  int                  null,
   LonM                 int                  null,
   LatM                 int                  null,
   LonS                 smallmoney           null,
   LatS                 smallmoney           null,
   Info                 text                 null,
   Map                  text                 null,
   CRCCode              nvarchar(23)         null,
   constraint PK_SYS_ORGANIZATION primary key nonclustered (OrgID),
   constraint AK_PK_ORG_ID_SYS_ORGA unique clustered (ID)
)
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SYS_Organization')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'Kind')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SYS_Organization', 'column', 'Kind'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '0:协会
   4:公棚
   5:俱乐部
   6:会所
   7:其他',
   'user', @CurrentUser, 'table', 'SYS_Organization', 'column', 'Kind'
go

/*==============================================================*/
/* Table: SYS_Params                                            */
/*==============================================================*/
create table SYS_Params (
   ID                   int                  identity,
   Name                 nvarchar(50)         null,
   Kind                 nvarchar(50)         null,
   Header               nvarchar(50)         null,
   StrValue             nvarchar(50)         null,
   IntValue             int                  null,
   FloatValue           float                null,
   Trailler             nvarchar(50)         null,
   "Backup"             nvarchar(500)        null,
   constraint PK_SYS_PARAMS primary key (ID)
)
go

/*==============================================================*/
/* Table: SYS_Right                                             */
/*==============================================================*/
create table SYS_Right (
   ID                   int                  identity,
   RoleIdx              int                  null,
   UsingName            nvarchar(20)         null,
   ControlID            int                  null,
   constraint PK_SYS_RIGHT primary key (ID)
)
go

/*==============================================================*/
/* Table: SYS_Role                                              */
/*==============================================================*/
create table SYS_Role (
   ID                   int                  identity,
   RoleName             nvarchar(20)         null,
   PID                  int                  null,
   RoleInfo             nvarchar(50)         null,
   LinkType             int                  null,
   ListOrder            int                  null,
   SpecialFlag          int                  null,
   constraint PK_SYS_ROLE primary key (ID)
)
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SYS_Role')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'LinkType')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SYS_Role', 'column', 'LinkType'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '层级关系：
   没关系
   继承上级
   包含下级
   ',
   'user', @CurrentUser, 'table', 'SYS_Role', 'column', 'LinkType'
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SYS_Role')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = 'SpecialFlag')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SYS_Role', 'column', 'SpecialFlag'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '0:普通级
   100:管理员
   200:应用管理员
   500:系统管理员',
   'user', @CurrentUser, 'table', 'SYS_Role', 'column', 'SpecialFlag'
go

INSERT INTO [SYS_Role]([RoleName], [PID], [RoleInfo], [LinkType], [ListOrder], [SpecialFlag])
VALUES('SYSTEM',0,'系统管理员级别',0,0,500)

/*==============================================================*/
/* Table: SYS_User                                              */
/*==============================================================*/
create table SYS_User (
   ID                   int                  identity,
   GroupIdx             int                  null,
   LoginName            nvarchar(20)         null,
   DisplayName          nvarchar(20)         null,
   LoginPWD             nvarchar(28)         null,
   Extends              ntext                null,
   SubRight             binary(10)           null,
   DepartmentIdx        int                  null,
   GroupName            nvarchar(6)          null,
   Work                 nvarchar(6)          null,
   Job                  nvarchar(6)          null,
   constraint PK_SYS_USER primary key (ID)
)
go

INSERT INTO [SYS_User]([GroupIdx], [LoginName], [DisplayName], [LoginPWD], Extends, SubRight)
VALUES(1,'security', 'System manager', 'tZoPdFmIIHZPRDdCF6lpkXK3ENQA','',0x0000000000000)

/*==============================================================*/
/* Table: SYS_UserGroup                                         */
/*==============================================================*/
create table SYS_UserGroup (
   ID                   int                  identity,
   GroupName            nvarchar(20)         null,
   constraint PK_SYS_USERGROUP primary key (ID)
)
go

INSERT INTO [SYS_UserGroup]([GroupName])
VALUES('SYSTEM')

/*==============================================================*/
/* Table: SYS_UserRole                                          */
/*==============================================================*/
create table SYS_UserRole (
   ID                   int                  identity,
   UserIdx              int                  null,
   RoleIdx              int                  null,
   constraint PK_SYS_USERROLE primary key (ID)
)
go

INSERT INTO [SYS_UserRole]([UserIdx], [RoleIdx])
VALUES(1,1)

/*==============================================================*/
/* Table: SYS_Zone                                              */
/*==============================================================*/
create table SYS_Zone (
   ID                   int                  not null,
   Code                 int                  null,
   WeatherCode          nvarchar(9)          null,
   ItemName             nvarchar(50)         null,
   PID                  int                  null,
   Lon                  int                  null,
   LonM                 int                  null,
   LonS                 smallmoney           null,
   Lat                  int                  null,
   LatM                 int                  null,
   LatS                 smallmoney           null,
   constraint PK_SYS_ZONE primary key (ID)
)
go

/*==============================================================*/
/* Index: IDX_Zone                                              */
/*==============================================================*/
create index IDX_Zone on SYS_Zone (
PID ASC
)
go

alter table SYS_Right
   add constraint FK_RIGH_REF_ROLE foreign key (RoleIdx)
      references SYS_Role (ID)
go

alter table SYS_User
   add constraint FK_USER_REF_GROUP foreign key (GroupIdx)
      references SYS_UserGroup (ID)
go

alter table SYS_UserRole
   add constraint FK_ROLE_REF_USER foreign key (UserIdx)
      references SYS_User (ID)
go

alter table SYS_UserRole
   add constraint FK_USER_REF_ROLE foreign key (RoleIdx)
      references SYS_Role (ID)
go

