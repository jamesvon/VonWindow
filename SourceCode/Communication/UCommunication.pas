unit UCommunication;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Winapi.WinSOCK;

type
  TEventVonCommunicationReceiver = procedure (var Data: array of byte; var Len: Integer; size: Integer) of object;
  TEventVonCommunicationConnected = procedure (ConnectInfo: string) of object;
  TEventOfLog = procedure (Sender: string; Info: string) of object;

  TVonCommunicationBase = class(TInterfacedObject)
  private
    FParams: TStringList;
    FMustReply: Integer;
    function GetParams(Key: string): string;
    procedure SetParams(Key: string; const Value: string);
  protected
    FReplyCount: Integer;
    FTimeOut: Cardinal;
    FOnError: TEventOfLog;
    FOnLog: TEventOfLog;
    FOnReceiver: TEventVonCommunicationReceiver;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    function GetParam(Key: string; Default: string): string;
    ///<summary>通讯参数键值</summary>
    property Params[Key: string]: string read GetParams write SetParams;
  published
    ///<summary>连接超时设置</summary>
    property TimeOut: Cardinal read FTimeOut write FTimeOut;
    ///<summary>失败后自动重发，发送失败后系统会自动尝试重发次数设置</summary>
    property MustReply: Integer read FMustReply write FMustReply;
    ///<summary>异常报警事件</summary>
    property OnError: TEventOfLog read FOnError write FOnError;
    ///<summary>日志输出事件</summary>
    property OnLog: TEventOfLog read FOnLog write FOnLog;
    ///<summary>接收到信息处理事件</summary>
    property OnReceiver: TEventVonCommunicationReceiver read FOnReceiver write FOnReceiver;
  end;

  IVonCommunicationServer = interface(IInterface) ['{91D2B6B7-59FD-4D3F-BDC5-584357F67025}']
    procedure SetActived(const Value: Boolean);
    function GetActived: Boolean;
    procedure SetOnConnected(Value: TEventVonCommunicationConnected);
    function GetOnConnected: TEventVonCommunicationConnected;
    function Open: Boolean;
    procedure Close;
    ///<summary>设置服务状态</summary>
    property Actived: Boolean read GetActived write SetActived;
    ///<summary>当有客户端连接时触发的事件</summary>
    property OnConnected: TEventVonCommunicationConnected read GetOnConnected write SetOnConnected;
  end;

  TVonCommunicationServer = class(TVonCommunicationBase, IVonCommunicationServer)
  private
    FActived: Boolean;
    FOnConnected: TEventVonCommunicationConnected;
    procedure SetActived(const Value: Boolean);
    function GetActived: Boolean;
    procedure SetOnConnected(Value: TEventVonCommunicationConnected);
    function GetOnConnected: TEventVonCommunicationConnected;
  protected
    function Open: Boolean; virtual; abstract;
    procedure Close; virtual; abstract;
  published
    ///<summary>设置服务状态</summary>
    property Actived: Boolean read GetActived write SetActived;
    ///<summary>当有客户端连接时触发的事件</summary>
    property OnConnected: TEventVonCommunicationConnected read GetOnConnected write SetOnConnected;
  end;

  IVonCommunicationClient = interface(IInterface) ['{1742A1FA-7BC6-4711-83EC-CAEADA717E4C}']
    function GetConnected: Boolean;
    function Send(var Data: array of byte; var len: Integer; size: Integer): boolean;
    function Receive(var Data: array of byte; size: Integer): Integer;
    procedure Connect;
    procedure Disconnect;
    ///<summary>与服务端的连接状态</summary>
    property Connected: Boolean read GetConnected;
  end;

  TVonCommunicationClient = class(TVonCommunicationBase, IVonCommunicationClient)
  private
  protected
    FConnected: Boolean;
    function GetConnected: Boolean;
  public
    function Send(var Data: array of byte; var len: Integer; size: Integer): boolean; virtual; abstract;
    function Receive(var Data: array of byte; size: Integer): Integer; virtual; abstract;
    procedure Connect; virtual;
    procedure Disconnect; virtual; abstract;
  published
    ///<summary>与服务端的连接状态</summary>
    property Connected: Boolean read GetConnected;
  end;

  TVonTCPServer = class(TVonCommunicationServer)
  private
    FRunning: Boolean;
    FSockt: TSocket;
    FAddress : sockaddr_in;
    FThreadID: DWORD;
    FClients: TList;
  public
    constructor Create;
    destructor Destroy; override;
    function Open: Boolean; override;
    procedure Close; override;
  end;

  TVonTCPClient = class(TVonCommunicationClient)
  private
    FSck: TSocket;
    addr : sockaddr_in;
  public
    function Send(var Data: array of byte; var len: Integer; size: Integer): Boolean; override;
    function Receive(var Data: array of byte; size: Integer): Integer; override;
    procedure Connect; override;
    procedure Disconnect; override;
  end;

  TVonUDPServer = class(TVonCommunicationServer)
  private
    FRunning: Boolean;
    FSockt: TSocket;
    FAddress : sockaddr_in;
    FThreadID: DWORD;
  public
    constructor Create;
    destructor Destroy; override;
    function Open: Boolean; override;
    procedure Close; override;
  end;

  TVonUDPClient = class(TVonCommunicationClient)
  private
    FSck: TSocket;
    addr : sockaddr_in;
  public
    function Send(var Data: array of byte; var len: Integer; size: Integer): Boolean; override;
    function Receive(var Data: array of byte; size: Integer): Integer; override;
    procedure Connect; override;
    procedure Disconnect; override;
  end;

  TEventOnReceived = procedure (const buf; len: DWord) of object;

  TUart = class(TObject)
  private
    OpenFlag: Boolean; // 打开标志
    handle: THandle; // 串口句柄
    Fport: string;
    Fbps: longint;
    Fpar: char;
    Fdbit, Fsbit: byte;
    FOnReceived: TEventOnReceived;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    // 打开串口，例：Open('COM1',CBR_9600,'n',8,1)
    function Open(): Boolean; overload;
    function Open(port: string; bps: longint; par: char; dbit, sbit: byte): Boolean; overload;
    function Open(port: integer; bps: longint; par: char; dbit, sbit: byte): Boolean; overload;
    // 关闭串口
    procedure Close;
    // 返回输入缓冲区中的字符数
    function InbufChars: DWord;
    // 返回输出缓冲区中的字符数
    function OutBufChars: longint;
    // 写串口 buf:存放要写的数据，len:要写的长度
    function Send(var buf; len: DWord): DWord;
    // 读串口 buf:存放读到的数据，len:要读的长度，tmout:超时值(ms)
    // 返回: 实际读到的字符数
    function Receive(var buf; len: DWord; tmout: integer): integer;
    // 向串口发一个字符
    procedure PutChar(ch: char);
    // 向串口发一个字符串
    procedure Puts(s: string);
    // 从串口读一个字符，未收到字符则返回#0
    function GetChar: char;
    // 从串口取一个字符串，忽略不可见字符,收到#13或255个字符结束，tmout:超时值(ms)
    // 返回:true表示收到完整字符串，false表示超时退出
    function Gets(var s: string; tmout: integer): Boolean;
    // 清接收缓冲区
    procedure ClearInBuf;
    // 等待一个字符串,tmout:超时值(ms),返回false表示超时
    function Wait(s: String; tmout: integer): Boolean;
    // 执行一个AT命令，返回true表示成功
    function ExecAtComm(s: string): Boolean;
    // 挂机，返回true表示成功
    function Hangup: Boolean;
    // 应答，返回true表示成功
    function Answer: Boolean;
  published
    /// <summary>串口开启状态</summary>
    property Opened: Boolean read OpenFlag;
    /// <summary>串口端口名称</summary>
    property port: string read Fport;
    /// <summary>波特率</summary>
    property bps: longint read Fbps;
    /// <summary>校验位</summary>
    property par: char read Fpar;
    /// <summary>数据位</summary>
    property dbit: byte read Fdbit;
    /// <summary>停止位</summary>
    property sbit: byte read Fsbit;
    /// <summary>接收事件</summary>
    property OnReceived: TEventOnReceived read FOnReceived write FOnReceived;
  end;

  TVonSerialPort = class(TVonCommunicationBase, IVonCommunicationServer, IVonCommunicationClient)
  private
    FComm: TUart;
    FThreadID: DWORD;
    FOnConnected: TEventVonCommunicationConnected;
    function GetConnected: Boolean;                                             //IVonCommunicationClient
    procedure SetActived(const Value: Boolean);                                 //IVonCommunicationServer
    function GetActived: Boolean;                                               //IVonCommunicationServer
    procedure SetOnConnected(Value: TEventVonCommunicationConnected);           //IVonCommunicationServer
    function GetOnConnected: TEventVonCommunicationConnected;                   //IVonCommunicationServer
    function OpenSerialPort: Boolean;
  public
    constructor Create;
    destructor Destroy; override;
    function Open: Boolean;                                                     //IVonCommunicationServer
    procedure Close;                                                            //IVonCommunicationServer
    function Send(var Data: array of byte; var len: Integer; size: Integer): Boolean;//IVonCommunicationClient
    function Receive(var Data: array of byte; size: Integer): Integer;          //IVonCommunicationClient
    procedure Connect;                                                          //IVonCommunicationClient
    procedure Disconnect;                                                       //IVonCommunicationClient
  published
    ///<summary>设置服务状态</summary>
    property Actived: Boolean read GetActived write SetActived;                 //IVonCommunicationServer
    ///<summary>当有客户端连接时触发的事件</summary>
    property OnConnected: TEventVonCommunicationConnected read GetOnConnected write SetOnConnected;  //IVonCommunicationServer
    ///<summary>与服务端的连接状态</summary>
    property Connected: Boolean read GetConnected;                              //IVonCommunicationClient
  end;

  procedure InitWSA;

var
  FWsa : TWSAData;
  SocketInited: Boolean;

implementation

const buflen = 2048;

(* Global functions *)

procedure InitWSA;
var
  wsstatus :Integer;
begin
  if(SocketInited) then Exit;
  wsstatus := WSAStartup($0202, Fwsa);
  if wsstatus <> 0 then
    raise Exception.Create('初始化socket出错！');
  SocketInited:= true;
end;

function HostToAddr(Host: string): Longint;
var
  S: AnsiString;
begin
  S:= Host;
  Result:= inet_addr(PAnsiChar(S));
end;

{ TVonCommunicationBase }

constructor TVonCommunicationBase.Create;
begin
  FParams:= TStringList.Create;
end;

destructor TVonCommunicationBase.Destroy;
begin
  FParams.Free;
  inherited;
end;

function TVonCommunicationBase.GetParam(Key, Default: string): string;
begin
  Result:= FParams.Values[Key];
  if Result = '' then Result:= Default;
end;

function TVonCommunicationBase.GetParams(Key: string): string;
begin
  Result:= FParams.Values[Key];
end;

procedure TVonCommunicationBase.SetParams(Key: string; const Value: string);
begin
  FParams.Values[Key]:= Value;
end;

{ TVonCommunicationServer }

function TVonCommunicationServer.GetActived: Boolean;
begin
  Result:= FActived;
end;

function TVonCommunicationServer.GetOnConnected: TEventVonCommunicationConnected;
begin
  Result:= FOnConnected;
end;

procedure TVonCommunicationServer.SetActived(const Value: Boolean);
begin
  if Value then Open else Close;
  FActived := Value;
end;

procedure TVonCommunicationServer.SetOnConnected(
  Value: TEventVonCommunicationConnected);
begin
  FOnConnected:= Value;
end;

{ TVonTCPServer }

constructor TVonTCPServer.Create;
begin
  inherited;
  FClients:= TList.Create;
end;

destructor TVonTCPServer.Destroy;
begin
  Close;
  FClients.Free;
  inherited;
end;

procedure TVonTCPServer.Close;
begin
  inherited;
  FRunning:= False;
  while FClients.Count > 0 do
  begin sleep(1000); end;
  FClients.Clear;
  closesocket(FSockt);
end;

type
  PTCPServerHandle = ^TCPServerHandle;
  TCPServerHandle = record
    Svr: TVonTCPServer;
    Sck : TSocket;
    Add :sockaddr_in;
  end;

procedure TCPWorkThread(client : PTCPServerHandle); stdcall;
var
  recvbuf : array[0..buflen -1] of byte;
  rtn, tryCount : Integer;
  error : string;
begin
  try
    tryCount:= 300; //延时超时300次则表示连接断开
    //rtn:= setsockopt(client.Sck, SOL_SOCKET, SO_SNDTIMEO, @k, sizeof(k));
    rtn:= setsockopt(client.Sck, SOL_SOCKET, SO_SNDTIMEO, @client.Svr.FTimeOut, sizeof(Integer));
    rtn:= setsockopt(client.Sck, SOL_SOCKET, SO_RCVTIMEO, @client.Svr.FTimeOut, sizeof(Integer));
    while client.Svr.FRunning and(tryCount > 0) do begin
      rtn := recv(client.Sck, recvbuf, buflen, 0);
      if rtn > 0 then begin
        if Assigned(client.Svr.OnReceiver) then begin
          client.Svr.OnReceiver(recvbuf, rtn, buflen);
          if rtn > 0 then
            send(client.Sck, recvbuf, rtn, 0);
         end;
         tryCount:= 300;
      end else Dec(tryCount);
    end;
    client.Svr.FClients.Remove(client);
    FreeMem(client);
  except
  end;
end;

procedure TCPServerAccept(Svr : TVonTCPServer); stdcall;
var
  addr_in : sockaddr_in;
  ra_len, k : integer;
  recev : TSocket;
  ReceiverID :DWORD;
  ip : string;
  newclient : PTCPServerHandle;
begin
  ra_len := SizeOf(addr_in);
  try
    while Svr.FRunning do
    begin
      recev := accept(Svr.FSockt, @addr_in, @ra_len);
      if recev = -1 then
      begin
        ExitThread(0);
      end;
      ip := IntToStr(Ord(addr_in.sin_addr.S_un_b.s_b1)) + '.' +
            IntToStr(Ord(addr_in.sin_addr.S_un_b.s_b2)) + '.' +
            IntToStr(Ord(addr_in.sin_addr.S_un_b.s_b3)) + '.' +
            IntToStr(Ord(addr_in.sin_addr.S_un_b.s_b4)) + ':' +
            IntToStr(Ord(addr_in.sin_port));
      GetMem(newclient, SizeOf(TCPServerHandle));
      newclient.Svr := Svr;
      newclient.Sck := recev;
      newclient.Add := addr_in;
      k:= Svr.FClients.Add(newclient);
      if Assigned(Svr.OnConnected) then
        Svr.OnConnected('IP：' + ip + ' connected with TCP(' + IntToStr(k) + ').');
      CreateThread(nil, 0, @TCPWorkThread, newclient, 0, ReceiverID);
    end;
  except
  end;
end;

function TVonTCPServer.Open: Boolean;
var
  wsstatus :Integer;
begin
  Result:= False;
  try
    InitWSA;
    FSockt := Socket(AF_INET, SOCK_STREAM, 0);
    if FSockt < 0 then
      raise Exception.Create('创建socket出错！');
    FAddress.sin_port := htons(StrToInt(FParams.Values['Port']));
    FAddress.sin_family := AF_INET;
    FAddress.sin_addr.S_addr := INADDR_ANY;
    if bind(FSockt, FAddress, SizeOf(FAddress)) <> 0 then begin
      if Assigned(FOnError) then FOnError('TCP Server', '绑定socket出错');
      Exit;
    end;
    if listen(FSockt, 5) <> 0 then begin
      if Assigned(FOnError) then FOnError('TCP Server', '监听出错！');
      Exit;
    end;
    FRunning:= True;
    setsockopt(FSockt, SOL_SOCKET, SO_RCVTIMEO, @FTimeOut, sizeof(FTimeOut));
    CreateThread(nil, 0, @TCPServerAccept, Self, 0, FThreadID);
    Result:= True;
    if Assigned(FOnLog) then
      FOnLog('TCP Server', 'Start with port : ' + FParams.Values['Port']);
  except
    on E: Exception do begin
      if Assigned(FOnError) then FOnError('VonServer', E.Message);
      WSACleanup;
    end;
  end;
end;

{ TVonCommunicationClient }

procedure TVonCommunicationClient.Connect;
begin
end;

function TVonCommunicationClient.GetConnected: Boolean;
begin
  Result:= FConnected;
end;

{ TVonTCPClient }

procedure TVonTCPClient.Connect;
var
  addr: sockaddr_in;
  hostaddr: u_long;
  RecTimeOut: integer;
begin
  inherited;
  InitWSA;
  if FConnected then Disconnect;
  FConnected:= False;
  FSck := socket(PF_INET, Sock_Stream, IPPROTO_IP);
  if FSck <> INVALID_SOCKET then begin
    addr.sin_family := PF_INET;
    addr.sin_port := htons(StrToInt(FParams.Values['Port']));
    hostaddr := HostToAddr(FParams.Values['Host']);
    if hostaddr = -1 then
      raise Exception.Create('连接服务失败!');
    addr.sin_addr.S_addr := hostaddr;
    if Winapi.WinSock.connect(FSck, addr, SizeOf(addr)) <> 0 then begin
      if Assigned(FOnError) then
        FOnError('TCP Client', Format('%s:%s 连接服务失败', [FParams.Values['Host'], FParams.Values['Port']]));
      Exit;
    end;
    RecTimeOut:= 3000;
    setsockopt(FSck, SOL_SOCKET, SO_RCVTIMEO, @RecTimeOut, sizeof(RecTimeOut));
    setsockopt(FSck, SOL_SOCKET, SO_SNDTIMEO, @FTimeOut, sizeof(FTimeOut));
    FConnected:= true;
    if Assigned(FOnLog) then FOnLog('TCP Client', Format('%s:%s 连接服务成功', [FParams.Values['Host'], FParams.Values['Port']]));
  end else FOnLog('TCP Client', 'TCP Socket 启动失败!');
end;

procedure TVonTCPClient.Disconnect;
begin
  inherited;
  closesocket(FSck);
  FConnected:= False;
end;

function TVonTCPClient.Receive(var Data: array of byte; size: Integer): Integer;
begin
  Result:= recv(FSck, Data, size, 0);
  if(Result > 0)and Assigned(FOnReceiver) then
    FOnReceiver(Data, Result, size);
end;

function TVonTCPClient.Send(var Data: array of byte; var len: Integer; size: Integer): Boolean;
var
  l: Integer;
begin
  inherited;
  Result:= Winapi.WinSock.Send(FSck, Data, len, 0) = len;
  if Result then len:= Receive(Data, size);
end;

{ TVonUDPServer }

type
  PUDPServerHandle = ^UDPServerHandle;
  UDPServerHandle = record
    ThreadID: DWord;
    Svr : TVonUDPServer;
    Add : sockaddr_in;
    ra_len : Integer;
    recvbuf : array[0..buflen -1] of byte;
    recev: Integer;
  end;

procedure UDPWorkThread(UDP : PUDPServerHandle); stdcall;
begin
  try
    if Assigned(UDP.Svr.FOnLog) then
      UDP.Svr.FOnLog('UDP Server', 'Received ' + IntToStr(UDP.recev) + ' bytes.');
    if Assigned(UDP.Svr.OnReceiver) then begin
        UDP.Svr.OnReceiver(UDP.recvbuf, UDP.recev, buflen);
        if UDP.recev > 0 then begin
          sendto(UDP.Svr.FSockt, UDP.recvbuf, UDP.recev, 0, UDP.add, UDP.ra_len);
          UDP.Svr.FOnLog('UDP Server', 'Send ' + IntToStr(UDP.recev) + ' bytes.');
       end;
    end;
  except
  end;
end;

procedure UDPServerThread(UDP : TVonUDPServer); stdcall;
var
  PThread : PUDPServerHandle;
begin
  try
    while UDP.FRunning do begin
      New(PThread);
      PThread.ra_len := SizeOf(PThread.Add);
      PThread.Svr:= UDP;
    //function recvfrom(s: TSocket; var Buf; len, flags: Integer; var from: TSockAddr; var fromlen: Integer): Integer; stdcall;
      PThread.recev := recvfrom(UDP.FSockt, PThread.recvbuf, buflen, 0, PThread.Add, PThread.ra_len);
      if PThread.recev > 0 then
        CreateThread(nil, 0, @UDPWorkThread, PThread, 0, PThread.ThreadID);
    end;
  except
  end;
end;

procedure TVonUDPServer.Close;
begin
  FRunning:= False;
  closesocket(FSockt);
  if Assigned(FOnLog) then FOnError('UDP Server', 'Closed...');
end;

constructor TVonUDPServer.Create;
begin
  inherited;

end;

destructor TVonUDPServer.Destroy;
begin
  Close;
  inherited;
end;

function TVonUDPServer.Open: Boolean;
var
  wsstatus :Integer;
begin
  Result:= False;
  try
    InitWSA;
    FSockt := Socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if FSockt < 0 then
      raise Exception.Create('创建socket出错！');
    FAddress.sin_port := htons(StrToInt(Params['Port']));
    FAddress.sin_family := AF_INET;
    FAddress.sin_addr.S_addr := INADDR_ANY;
    if bind(FSockt, FAddress, SizeOf(FAddress)) <> 0 then
      raise Exception.Create('绑定socket出错');
    setsockopt(FSockt, SOL_SOCKET, SO_RCVTIMEO, @FTimeOut, sizeof(FTimeOut));
    FRunning:= True;
    CreateThread(nil, 0, @UDPServerThread, Self, 0, FThreadID);
    if Assigned(FOnLog) then FOnError('UDP Server', 'Opened...');
    Result:= true;
  except
    on E: Exception do begin
      if Assigned(FOnError) then FOnError('UDP Server', 'Open : ' + E.Message);
      WSACleanup;
    end;
  end;
end;

{ TVonUDPClient }

procedure TVonUDPClient.Connect;
var
  addr: sockaddr_in;
  hostaddr: u_long;
  RecTimeOut: integer;
begin
  inherited;
  InitWSA;
  if FConnected then Disconnect;
  FConnected:= False;
  FSck := socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if FSck <> INVALID_SOCKET then begin
    RecTimeOut:= 3000;
    setsockopt(FSck, SOL_SOCKET, SO_RCVTIMEO, @RecTimeOut, sizeof(RecTimeOut));
    setsockopt(FSck, SOL_SOCKET, SO_SNDTIMEO, @FTimeOut, sizeof(FTimeOut));
    FConnected:= true;
    if Assigned(FOnLog) then FOnLog('UDP', '启动成功');
  end else if Assigned(FOnError) then FOnError('UDP', '启动失败');
end;

procedure TVonUDPClient.Disconnect;
begin
  inherited;
  closesocket(FSck);
  FConnected:= False;
end;

function TVonUDPClient.Receive(var Data: array of byte; size: Integer): Integer;
var
  recev: Integer;
  ra_len : integer;
begin
//recvfrom(s: TSocket; var Buf; len, flags: Integer; var from: TSockAddr; var fromlen: Integer): Integer; stdcall;
  ra_len := SizeOf(addr);
  recev := recvfrom(FSck, Data, size, 0, addr, ra_len);
  if (recev > 0)and Assigned(OnReceiver) then
    OnReceiver(Data, recev, size);
end;

function TVonUDPClient.Send(var Data: array of byte; var len: Integer; size: Integer): Boolean;
var
  ra_len: Integer;
begin
  addr.sin_family := PF_INET;
  addr.sin_port := htons(StrToInt(Params['Port']));
  addr.sin_addr.S_addr := HostToAddr(Params['Host']);
  ra_len := SizeOf(addr);
  Result:= sendto(FSck, Data, len, 0, addr, ra_len) = len;
  if Result then Receive(Data, size);
end;

const
  CommInQueSize = 4096; // 输入缓冲区大小
  CommOutQueSize = 4096; // 输出缓冲区大小

(* **********************      计时器        ********************** *)

type
  Timers = Class
  private
    StartTime: TTimeStamp;
  public
    constructor Create;
    Procedure Start; // 开始计时
    Function Get: longint; // 返回计时值(单位:ms)
  end;

constructor Timers.Create;
begin // 构造函数
  inherited Create;
  Start;
end;

procedure Timers.Start;
begin // 开始计时
  StartTime := DateTimeToTimeStamp(Now);
end;

function Timers.Get: longint;
var // 返回计时值(单位:ms)
  CurTime: TTimeStamp;
begin
  CurTime := DateTimeToTimeStamp(Now);
  Result := (CurTime.Date - StartTime.Date) * 24 * 3600000 +
    (CurTime.Time - StartTime.Time);
end;

(* **********************      TUart     ********************** *)

constructor TUart.Create;
begin // 构造函数
  inherited Create;
  OpenFlag := False;
end;

Destructor TUart.Destroy;
begin // 析构函数
  Close;
  inherited Destroy;
end;

(* 例：Open('COM1',CBR_9600,'n',8,1)
  波特率：
  CBR_110     CBR_19200
  CBR_300     CBR_38400
  CBR_600     CBR_56000
  CBR_1200    CBR_57600
  CBR_2400    CBR_115200
  CBR_4800    CBR_128000
  CBR_14400   CBR_256000
  校验位:
  'n'=no,'o'=odd,'e'=even,'m'=mark,'s'=space
  数据位： 4-8 (other=8)
  停止位： 1-2 (other=1.5)  // *)
function TUart.Open: Boolean;
begin
  Result:= Open(Fport, Fbps, Fpar, Fdbit, Fsbit);
end;

function TUart.Open(port: integer; bps: longint; par: char;
  dbit, sbit: byte): Boolean;
begin
  Result:= Open('COM' + IntToStr(port), bps, par, dbit, sbit);
end;

function TUart.Open(port: string; bps: Integer; par: char; dbit,
  sbit: byte): Boolean;
var // 打开串口
  dcb: TDCB;
begin
  Result := False;
  Fport:= port;
  Fbps:= bps;
  Fpar:= par;
  Fdbit:= dbit;
  Fsbit:= sbit;
  OpenFlag := False;
  // 初始化串口
  handle := CreateFile(PChar(port),
    GENERIC_READ + GENERIC_WRITE, 0, nil, OPEN_EXISTING, 0, 0);
  if handle = INVALID_HANDLE_VALUE then
    Exit;
  GetCommState(handle, dcb);
  with dcb do
  begin
    BaudRate := bps; // 波特率
    if dbit in [4, 5, 6, 7, 8] then
      ByteSize := dbit // 数据位
    else
      ByteSize := 8;
    case sbit of // 停止位
      1:
        StopBits := 0; // 1
      2:
        StopBits := 2; // 2
    else
      StopBits := 0; // 1
    end;
    case par of // 校验
      'n', 'N':
        Parity := 0; // no
      'o', 'O':
        Parity := 1; // odd
      'e', 'E':
        Parity := 2; // even
      'm', 'M':
        Parity := 3; // mark
      's', 'S':
        Parity := 4; // space
    else
      Parity := 0; // no
    end;
  end;
  SetCommState(handle, dcb);
  SetupComm(handle, CommOutQueSize, CommInQueSize);
  OpenFlag := True;
  Result := True;
end;

// 关闭串口
procedure TUart.Close;
begin
  if OpenFlag then
  begin
    CloseHandle(handle);
    handle := 0;
  end;
  OpenFlag := False;
end;

// 检测输入缓冲区中的字符数
function TUart.InbufChars: DWord;
var
  ErrCode: DWord;
  Stat: TCOMSTAT;
begin
  Result := 0;
  if not OpenFlag then
    Exit;
  ClearCommError(handle, ErrCode, @Stat);
  Result := Stat.cbInQue;
end;

// 检测输出缓冲区中的字符数
function TUart.OutBufChars: longint;
var
  ErrCode: DWord;
  Stat: TCOMSTAT;
begin
  Result := 0;
  if not OpenFlag then
    Exit;
  ClearCommError(handle, ErrCode, @Stat);
  Result := Stat.cbOutQue;
end;

// 写串口 buf:存放要写的数据，len:要写的长度
function TUart.Send(var buf; len: DWord): DWord;
begin
  WriteFile(handle, buf, len, Result, nil); // 写串口
end;

// 读串口 buf:存放读到的数据，len:要读的长度，tmout:超时值(ms)
// 返回: 实际读到的字符数
function TUart.Receive(var buf; len: DWord; tmout: integer): integer;
var
  Timer: Timers;
  i: DWord;
  BufChs: DWord;
begin
  Timer := Timers.Create;
  Timer.Start;
  repeat
  until (InbufChars >= len) or (Timer.Get > tmout); // 收到指定长度数据或超时
  BufChs := InbufChars;
  if len > BufChs then
    len := BufChs;
  ReadFile(handle, buf, len, i, nil); // 读串口
  Result := i;
  if(Result > 0)and Assigned(FOnReceived) then
    FOnReceived(buf, Result);
  Timer.free;
end;

// 向串口发一个字符
procedure TUart.PutChar(ch: char);
var
  i: DWord;
begin
  i:= 1;
  WriteFile(handle, ch, 1, i, nil); // 写串口
end;

// 向串口发一个字符串
procedure TUart.Puts(s: string);
var
  i: integer;
begin
  for i := 1 to length(s) do
    PutChar(s[i]);
end;

// 从串口读一个字符，未收到字符则返回#0
function TUart.GetChar: char;
var
  i: DWord;
begin
  Result := #0;
  if InbufChars > 0 then
    ReadFile(handle, Result, 1, i, nil); // 读串口
end;

// 从串口取一个字符串，忽略不可见字符,收到#13或255个字符结束，tmout:超时值(ms)
// 返回:true表示收到完整字符串，false表示超时退出
function TUart.Gets(var s: string; tmout: integer): Boolean;
var
  Timer: Timers;
  ch: char;
begin
  Timer := Timers.Create;
  Timer.Start;
  s := '';
  Result := False;
  repeat
    ch := GetChar;
    if ch <> #0 then
      Timer.Start; // 如收到字符则清定时器
    if ch >= #32 then
      s := s + ch;
    if (ch = #13) or (length(s) >= 255) then
    begin
      Result := True;
      break;
    end;
  until Timer.Get > tmout; // 超时
  Timer.free;
end;

// 清接收缓冲区
procedure TUart.ClearInBuf;
begin
  if not OpenFlag then
    Exit;
  PurgeComm(handle, PURGE_RXCLEAR);
end;

// 等待一个字符串,tmout:超时值(ms),返回false表示超时
function TUart.Wait(s: String; tmout: integer): Boolean;
var
  s1: string;
  Timer: Timers;
begin
  Timer := Timers.Create;
  Timer.Start;
  Result := False;
  repeat
    Gets(s1, tmout);
    if pos(s, s1) > 0 then
    begin
      Result := True;
      break
    end;
  until Timer.Get > tmout;
  Timer.free;
end;

// 执行一个AT命令，返回true表示成功
function TUart.ExecAtComm(s: string): Boolean;
begin
  ClearInBuf;
  Puts(s);
  Result := False;
  if Wait('OK', 3000) then
    Result := True;
end;

// 挂机，返回true表示成功
function TUart.Hangup: Boolean;
begin
  Result := False;
  ExecAtComm('+++');
  if not ExecAtComm('ATH'#13) then
    Exit;
  Result := True;
end;

// 应答，返回true表示成功
function TUart.Answer: Boolean;
begin
  ClearInBuf;
  Puts('ATA'#13);
  Result := False;
  if Wait('CONNECT', 30000) then
    Result := True;
end;


{ TVonSerialPort }

procedure TVonSerialPort.Close;
begin
  inherited;
  FComm.Close;
  FThreadID:= 0;
  Sleep(TimeOut + 200);
end;

procedure TVonSerialPort.Connect;
begin     //客户端连接不起侦听事务
  inherited;
  if OpenSerialPort then begin
    if Assigned(FOnLog) then FOnLog('Serial Port', '启动成功');
    if Assigned(FOnConnected) then FOnConnected('启动成功');

  end else if Assigned(FOnError) then FOnError('Serial Port', '启动失败');
end;

constructor TVonSerialPort.Create;
begin
  inherited;
  FComm:= TUart.Create;
end;

destructor TVonSerialPort.Destroy;
begin
  Close;
  FComm.Free;
  inherited;
end;

procedure TVonSerialPort.Disconnect;
begin     //断开客户端连接状态
  inherited;
  Close;
end;

function TVonSerialPort.GetActived: Boolean;
begin    //返回服务状态
  Result:= FComm.Opened;
end;

function TVonSerialPort.GetConnected: Boolean;
begin    //返回串口连接状态
  Result:= FComm.Opened;
end;

function TVonSerialPort.GetOnConnected: TEventVonCommunicationConnected;
begin
  Result:= FOnConnected;
end;

procedure SerialPortWorkThread(S_P : TVonSerialPort); stdcall;
var
  recvbuf : array[0..buflen -1] of byte;
  rtn, tryCount : Integer;
  error : string;
begin
  try
    while S_P.FThreadID <> 0 do begin
      while S_P.FComm.Opened do begin
        rtn:= S_P.FComm.Receive(recvbuf, buflen, S_P.TimeOut);
        if(rtn > 0)and Assigned(S_P.FOnReceiver) then
          S_P.FOnReceiver(recvbuf, rtn, buflen);
      end;
      Sleep(1000);
    end;
  except
  end;
end;

function TVonSerialPort.Open: Boolean;
begin    //打开串口服务
  OpenSerialPort;
  if FComm.Opened then
    CreateThread(nil, 0, @SerialPortWorkThread, Self, 0, FThreadID);
end;

function TVonSerialPort.OpenSerialPort: Boolean;
begin    //打开串口
  Result:= False;
  try
    FComm.Close;
    FComm.Open(Params['Port'], StrToInt(Params['bps']), Params['par'][1],
      StrToInt(Params['dbit']), StrToInt(Params['sbit']));
    if Assigned(FOnLog) then FOnError('Serial port', 'Opened...');
    Result:= FComm.Opened;
  except
    on E: Exception do begin
      if Assigned(FOnError) then FOnError('Serial port', 'Open : ' + E.Message);
      WSACleanup;
    end;
  end;
end;

function TVonSerialPort.Receive(var Data: array of byte;
  size: Integer): Integer;
begin    //客户端接收数据
  FComm.Receive(Data, size, TimeOut)
end;

function TVonSerialPort.Send(var Data: array of byte; var len: Integer;
  size: Integer): Boolean;
begin    //发送内容
  FComm.Send(Data, len);
end;

procedure TVonSerialPort.SetActived(const Value: Boolean);
begin     //设置服务状态
  if Value then Open
  else Close;
end;

procedure TVonSerialPort.SetOnConnected(Value: TEventVonCommunicationConnected);
begin
  FOnConnected:= Value;
end;

end.
