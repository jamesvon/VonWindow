unit UCommunicationHttp;

interface

uses UCommunicationModule;

type
  TCommunicationHttpServer = class(TCommunicationServer)
  public
    constructor Create(AppExeName, ConfigFilename: string); override;

  end;

  TCommunicationHttpClient = class(TCommunicationClient)

  public
    constructor Create(AppExeName, AppName: string); override;
  end;

implementation

{ TCommunicationHttpClient }

constructor TCommunicationHttpClient.Create(AppExeName, AppName: string);
begin
  inherited;

end;

{ TCommunicationHttpServer }

constructor TCommunicationHttpServer.Create(AppExeName, ConfigFilename: string);
begin
  inherited;

end;

end.
