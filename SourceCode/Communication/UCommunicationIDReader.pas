unit UCommunicationIDReader;

interface

uses
  WinApi.Windows, SysUtils, Classes, Forms, StdCtrls, DateUtils, IniFiles,
  Dialogs, ExtCtrls, Graphics;

type
  TCommunicationIDReader = class
  private
    FDllH: THandle;
    FTimer: TTimer;
    FID: Cardinal;
    FOnRead: TNotifyEvent;
    procedure EventReadComm(Sender: TObject);
    procedure SetOnRead(const Value: TNotifyEvent);
  private
    FCVR_InitComm: function(Port: integer): integer; stdcall; // 初始化连接;
    FCVR_Authenticate: function(): integer; stdcall; // 卡认证;
    FCVR_Read_Content: function(active: integer): integer; stdcall; // 读卡操作。
    FCVR_CloseComm: function(): integer; stdcall; // 关闭连接;
    FGetManuID: function(ID: PCardinal): integer; stdcall;
    FGetPeopleName: function(strTmp: PChar; strLen: Pinteger): integer; stdcall;
    FUserName: string;
    FIsConnected: Boolean;
    FUserID: string;
    FPort: integer;
    FNation: string;
    FIssuer: string;
    FExpiry: TDate;
    FBirthday: TDate;
    FGender: string;
    FAddress: string;
    FAvailability: TDate;
    FNationList: TStringList;
  public
    constructor Create(); overload;
    destructor Destroy; override;
    function OpenIDReader(APort: integer): Boolean;
    procedure Close;
    procedure LoadPhoto(bmp: TBitmap);
  published
    property OnRead: TNotifyEvent read FOnRead write SetOnRead;
    property IsConnected: Boolean read FIsConnected;
    /// <summary>姓名</summary>
    property UserName: string read FUserName;
    /// <summary>性别</summary>
    property Gender: string read FGender;
    /// <summary>民族</summary>
    property Nation: string read FNation;
    /// <summary>出生日期</summary>
    property Birthday: TDate read FBirthday;
    /// <summary>地址</summary>
    property Address: string read FAddress;
    /// <summary>身份证号码</summary>
    property UserID: string read FUserID;
    /// <summary>发证机构</summary>
    property Issuer: string read FIssuer;
    /// <summary>启用时间</summary>
    property Availability: TDate read FAvailability;
    /// <summary>有效期</summary>
    property Expiry: TDate read FExpiry;
  end;

implementation

{ TCommunicationIDReader }

procedure TCommunicationIDReader.Close;
begin
  if Assigned(FCVR_CloseComm) then
    FCVR_CloseComm();
  FTimer.Enabled := False;
  FIsConnected := False;
end;

constructor TCommunicationIDReader.Create;
begin
  FTimer := TTimer.Create(nil);
  //----------以下先后循序不能错-----------//
  FNationList:= TStringList.Create;
  FNationList.Add('汉族');
  FNationList.Add( '蒙古族');
  FNationList.Add('回族');
  FNationList.Add('藏族');
  FNationList.Add('维吾尔族');
  FNationList.Add('苗族');
  FNationList.Add('彝族');
  FNationList.Add('壮族');
  FNationList.Add('布依族');
  FNationList.Add('朝鲜族');
  FNationList.Add('满族');
  FNationList.Add('侗族');
  FNationList.Add('瑶族');
  FNationList.Add('白族');
  FNationList.Add('土家族');
  FNationList.Add('哈尼族');
  FNationList.Add('哈萨克族');
  FNationList.Add('傣族');
  FNationList.Add('黎族');
  FNationList.Add('傈僳族');
  FNationList.Add('佤族');
  FNationList.Add('畲族');
  FNationList.Add('高山族');
  FNationList.Add('拉祜族');
  FNationList.Add('水族');
  FNationList.Add('东乡族');
  FNationList.Add('纳西族');
  FNationList.Add('景颇族');
  FNationList.Add('柯尔克孜族');
  FNationList.Add('土族');
  FNationList.Add('达翰尔族');
  FNationList.Add('仫佬族');
  FNationList.Add('羌族');
  FNationList.Add('布朗族');
  FNationList.Add('撒拉族');
  FNationList.Add('毛南族');
  FNationList.Add('仡佬族');
  FNationList.Add('锡伯族');
  FNationList.Add('阿昌族');
  FNationList.Add('普米族');
  FNationList.Add('塔吉克族');
  FNationList.Add('怒族');
  FNationList.Add('乌孜别克族');
  FNationList.Add('俄罗斯族');
  FNationList.Add('鄂温克族');
  FNationList.Add('德昂族');
  FNationList.Add('保安族');
  FNationList.Add('裕固族');
  FNationList.Add('京族');
  FNationList.Add('塔塔尔族');
  FNationList.Add('独龙族');
  FNationList.Add('鄂伦春族');
  FNationList.Add('赫哲族');
  FNationList.Add('门巴族');
  FNationList.Add('珞巴族');
  FNationList.Add('基诺族');
  FNationList.Add('其它');
  FNationList.Add('外国人入籍');
  FTimer.Interval := 500;
  FTimer.Enabled := False;
  FTimer.OnTimer := EventReadComm;
  FDllH := LoadLibrary(PChar(ExtractFilePath(Application.ExeName) +
    'termb.dll'));
  try
    if FDllH <> 0 then
    begin
      @FCVR_InitComm := GetProcAddress(FDllH, 'CVR_InitComm');
      if not Assigned(FCVR_InitComm) then
        RaiseLastWin32Error;
      @FCVR_Authenticate := GetProcAddress(FDllH, 'CVR_Authenticate');
      if not Assigned(FCVR_Authenticate) then
        RaiseLastWin32Error;
      @FCVR_Read_Content := GetProcAddress(FDllH, 'CVR_Read_Content');
      if not Assigned(FCVR_Read_Content) then
        RaiseLastWin32Error;
      @FCVR_CloseComm := GetProcAddress(FDllH, 'CVR_CloseComm');
      if not Assigned(FCVR_CloseComm) then
        RaiseLastWin32Error;
      @FGetManuID := GetProcAddress(FDllH, 'GetManuID');
      if not Assigned(FGetManuID) then
        RaiseLastWin32Error;
      @FGetPeopleName := GetProcAddress(FDllH, 'GetPeopleName');
      if not Assigned(FGetPeopleName) then
        RaiseLastWin32Error;
    end
    else
      RaiseLastWin32Error;
  except
    if FDllH <> 0 then
      FreeLibrary(FDllH); // 调用完毕收回DLL占用的资源
    FDllH := 0;
  end;
end;

destructor TCommunicationIDReader.Destroy;
begin
  FNationList.Free;
  FTimer.Free;
  if FDllH <> 0 then
    FreeLibrary(FDllH); // 调用完毕收回DLL占用的资源
  inherited;
end;

procedure TCommunicationIDReader.EventReadComm(Sender: TObject);
var // 读第二代身份证
  iRet: integer;
  iFileHandle, iFileLength: integer;
  Buffer: PWideChar;

  sWZ: WideString;
  iniFileName: string;

  ID: Cardinal;

  pucIIN: integer;
  EdziIfOpen: integer;
  iLenGet: integer;
begin
  FGetManuID(@ID); // 20080610 Add By Kim
  iRet := FCVR_Authenticate();
  // FCVR_Read_Content(1);
  iRet := FCVR_Read_Content(1); //
  if iRet <> 1 then
    Exit;
  try
    iniFileName := ExtractFilePath(Application.ExeName) + 'ICINIT.INI';
    iFileHandle := FileOpen(ExtractFilePath(Application.ExeName) + 'wz.txt',
      fmOpenRead);
    iFileLength := FileSeek(iFileHandle, 0, 2);
    FileSeek(iFileHandle, 0, 0);
    Buffer := PWideChar(AllocMem(iFileLength + 2));
    FileRead(iFileHandle, Buffer^, iFileLength);
    FileClose(iFileHandle);
    sWZ := WideChartostring(Buffer);

    FUserName := Trim(copy(sWZ, 1, 15)); // 姓名
    if Trim(copy(sWZ, 16, 1)) = '1' then FGender:= '男';    //性别
    if Trim(copy(sWZ, 16, 1)) = '2' then FGender:= '女';    //性别
    FNation := FNationList.Strings[StrToInt(Trim(copy(sWZ, 17, 2))) - 1];       //民族
    FBirthday:= EncodeDate(StrToInt(Copy(sWZ, 19, 4)), StrToInt(Copy(sWZ, 23, 2)), StrToInt(Copy(sWZ, 25, 2)));
    FAddress:= Trim(copy(sWZ, 27, 35));
    FUserID := Trim(copy(sWZ, 62, 18));
    FIssuer := Trim(copy(sWZ, 80, 15));
    FAvailability:= EncodeDate(StrToInt(Copy(sWZ, 95, 4)), StrToInt(Copy(sWZ, 99, 2)), StrToInt(Copy(sWZ, 101, 2)));
    FExpiry:= EncodeDate(StrToInt(Copy(sWZ, 103, 4)), StrToInt(Copy(sWZ, 107, 2)), StrToInt(Copy(sWZ, 109, 2)));

    if Assigned(FOnRead) then
    begin
      FOnRead(Self);
      FID := ID;
    end;
  finally
    FreeMem(Buffer);
    FCVR_CloseComm();
  end;
end;

procedure TCommunicationIDReader.LoadPhoto(bmp: TBitmap);
begin
  bmp.LoadFromFile(ExtractFilePath(Application.ExeName) + 'zp.bmp');
end;

function TCommunicationIDReader.OpenIDReader(APort: integer): Boolean;
begin
  FIsConnected := true;
  FPort := APort;
  if Assigned(FCVR_InitComm) then
    FIsConnected := FCVR_InitComm(FPort) = 1;
  Result := FIsConnected;
  FTimer.Enabled := FIsConnected;
end;

procedure TCommunicationIDReader.SetOnRead(const Value: TNotifyEvent);
begin
  FOnRead := Value;
end;

end.
