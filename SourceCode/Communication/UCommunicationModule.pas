unit UCommunicationModule;

interface

uses
  SysUtils, StdCtrls, Classes, IniFiles, ZLib, IdBaseComponent, Dialogs,
  IdAntiFreezeBase, IdAntiFreeze, WinAPI.ShellApi, UVonLog, UVonSystemFuns,
  UVonConfig, DB, Data.Win.ADODB, WinAPI.ActiveX;

const
  MAX_BUFFER_SIZE = 10485760; // 10M
  FLAG_SIZE = 6;

  { 升级文件相关类 }

type
  /// <summary>更新文件信息</summary>
  TVonUpgradeFile = class
  private
    FVer: string;
    FFileStream: TFileStream;
    FApp: string;
    FAppPath: string;
    function GetFileStream: TFileStream;
    procedure SetVer(const Value: string);
    procedure SetApp(const Value: string);
    procedure SetAppPath(const Value: string);
  public
    { Public declarations }
    /// <summary>文件标识</summary>
    Flag: string;
    /// <summary>文件类型（替换文件，SQL更新，直接执行，任务列表，SQL查询）/summary>
    TypeID: Word;
    /// <summary>目标文件名称</summary>
    DestFilename: string;
    /// <summary>文件版本值</summary>
    VerValue: TVonVersion;
    destructor Destroy; override;
    /// <summary>设置文件版本值</summary>
    procedure SetVerValue(const Value: TVonVersion);
    /// <summary>将信息写入文件中</summary>
    procedure WriteFile(ADataStream: TStream; APosition, ASize: Int64);
    /// <summary>关闭文件，同时释放空间，准备以后进一步存储</summary>
    procedure CloseFile;
  published
    /// <summary>文件流信息</summary>
    property FileStream: TFileStream read GetFileStream;
    /// <summary>设置或读取文件版本</summary>
    property Ver: string read FVer write SetVer;
    /// <summary>设置或读取应用名称</summary>
    property App: string read FApp write SetApp;
    /// <summary>设置或读取应用工作目录</summary>
    property AppPath: string read FAppPath write SetAppPath;
  end;

  /// <summary>更新文件集合信息（不同应用不同版本及文件标识管理）</summary>
  TVonUpgradeFiles = class
  private
    FAppList: TStringList;
    FIniFile: TIniFile;
    function GetAppFile(App, Flag: string): TVonUpgradeFile;
    function GetAppFiles(App: string): TStringList;
    procedure SetIniFile(const Value: TIniFile);
    /// <summary>从配置文件中读取一个应用的所有版本升级文件信息</summary>
    /// <param name="IniFile">配置文件</param>
    /// <param name="AppName">应用名称</param>
    procedure ReadFromIniFile(IniFile: TIniFile; AppName: string);
  public
    constructor Create;
    destructor Destroy; override;
    /// <summary>保存一个应用的版本升级文件</summary>
    /// <param name="App">应用名称</param>
    /// <param name="AFile">版本文件信息</param>
    procedure Save(App: string; AFile: TVonUpgradeFile);
    /// <summary>删除一个应用的版本升级文件</summary>
    /// <param name="App">应用名称</param>
    /// <param name="Flag">版本文件标识</param>
    procedure Del(App, Flag: string);
    /// <summary>根据用户提供的版本信息进行比较，返回需要升级的文件列表</summary>
    /// <param name="App">应用名称</param>
    /// <param name="Ver">客户端版本信息</param>
    /// <param name="outStream">文件列表文件流，输出文件标示和目标位置</param>
    procedure GetUpgradeFiles(App, Ver: string; outStream: TStream);
    /// <summary>配置文件，设置时系统会自动读取每一个应用的所有版本升级文件信息</summary>
    property IniFile: TIniFile read FIniFile write SetIniFile;
    /// <summary>得到应用系统文件列表</summary>
    /// <param name="App">应用名称</param>
    property AppFiles[App: string]: TStringList read GetAppFiles;
    /// <summary>得到应用系统中某一标识文件信息</summary>
    /// <param name="App">应用名称</param>
    /// <param name="Flag">版本文件标识</param>
    property AppFile[App, Flag: string]: TVonUpgradeFile read GetAppFile;
  end;

  { 通讯相关类 }

type
  /// <summary>通讯方式类型</summary>
  /// <param name="COMM_HTTP">采用 Http 的方式进行通讯</param>
  /// <param name="COMM_TCP">采用 TCP 的方式进行通讯</param>
  /// <param name="COMM_UDP">采用 UDP 的方式进行通讯</param>
  ECOMMTYPE = (COMM_HTTP, COMM_TCP, COMM_UDP);

  /// <summary>通讯过程中所有命令</summary>
  /// <param name="CMD_GETVER">客户端获取版本</param>
  /// <param name="CMD_SETVER">客户端设置版本</param>
  /// <param name="CMD_RESEND">重新发送命令</param>
  /// <param name="CMD_SNDFILE">客户端发送文件</param>
  /// <param name="CMD_RCVFILE">客户端接收文件</param>
  /// <param name="CMD_SNDDATA">客户端发送数据</param>
  /// <param name="CMD_RCVDATA">客户端接收数据</param>
  EVonCommunicationCmd = (CMD_GETVER, CMD_SETVER, CMD_RESEND, CMD_RESERV,
    CMD_SNDFILE, CMD_RCVFILE, CMD_SNDDATA, CMD_RCVDATA);

  /// <summary>通讯信息类</summary>
  TVonCommunicationMessage = class
  private
    SendStream: TMemoryStream;
  public
    Cmd: EVonCommunicationCmd;
    BindingHost: string;
    AppName: string;
    Params: string;
    DataStream: TStream;
    constructor Create; overload;
    constructor Create(ACmd: EVonCommunicationCmd;
      AAppName, AParams: string); overload;
    destructor Destroy; override;
    function MerageStream: TStream;
    function ReadFromStream(ReceiveStream: TStream): Boolean;
    function CommandString: string;
  end;

  /// <summary>接收数据后处理数据事件</summary>
  TEventToProcessData = function(AppName, FileFlag: string; DataStream: TStream)
    : string of object;
  /// <summary>升级完成后处理版本信息事件</summary>
  /// <param name="newVersion">新版本</param>
  /// <param name="UpgradeFiles">下载文件列表[Flag]=[TypeID][DestFilename]</param>
  TEventToUpgradeVer = procedure(newVersion: string; UpgradeFiles: TStrings)
    of object;

  /// <summary>通讯控件基类</summary>
  TCommunicationBase = class
  private
    { Private properties declarations }
    FAppPath: string; // 系统工作目录
    FCommunicationPort: Integer;
    FLogComponent: TMemo;
    procedure SetCommunicationPort(const Value: Integer);
    procedure SetLogComponent(const Value: TMemo);
    procedure SetOnEndOfReceiveData(const Value: TEventToProcessData);
    procedure SetOnBeginToSendData(const Value: TEventToProcessData);
  private
    FAppExeName: string;
    procedure SetAppExeName(const Value: string);
    procedure WriteCommLog(LogClass: ELOG_Class; Process, Info: string);
  protected
    FOnEndOfReceiveData: TEventToProcessData;
    FOnBeginToSendData: TEventToProcessData;
    /// <summary>压缩数据</summary>
    /// <param name="OrgData">原始数据流</param>
    /// <param name="RtnData">压缩后数据流</param>
    procedure CompressData(OrgData, RtnData: TStream);
    /// <summary>解压缩数据</summary>
    /// <param name="OrgData">原始压缩数据流</param>
    /// <param name="RtnData">解压后数据流</param>
    procedure DecompressData(OrgData, RtnData: TStream);
    /// <summary>将流数据按照逗号间隔规则分解到 Params 变量中</summary>
    /// <param name="AStream">数据流</param>
    /// <param name="ParamLen">流中内容长度</param>
    /// <param name="Params">参数列表</param>
    procedure SplitParams(AStream: TStream; ParamLen: Integer;
      Params: TStringList);
    procedure ShowStream(Msg: TVonCommunicationMessage);
  public
    { Public declarations }
    constructor Create(ApplicationExeName: string);
    destructor Destroy; override;
  published
    /// <summary>系统工作路径以斜杠结尾</summary>
    property AppPath: string read FAppPath;
    /// <summary>系统应用执行文件名称</summary>
    property ExeName: string read FAppExeName write SetAppExeName;
    /// <summary>日志显示用控件（TMemo）</summary>
    property LogComponent: TMemo read FLogComponent write SetLogComponent;
    /// <summary>当前通讯端口</summary>
    property CommunicationPort: Integer read FCommunicationPort
      write SetCommunicationPort;
    property OnEndOfReceiveData: TEventToProcessData read FOnEndOfReceiveData
      write SetOnEndOfReceiveData;
    property OnBeginToSendData: TEventToProcessData read FOnBeginToSendData
      write SetOnBeginToSendData;
  end;

  TVonConnects = class
  private
    FNameList: array of string;
    FValueList: array of string;
    FConnList: array of TADOConnection;
    FCount: Integer;
    FCapacity: Integer;
    function GetNames(Index: Integer): string;
    function GetNameValue(Name: string): string;
    function GetValues(Index: Integer): string;
    procedure SetNames(Index: Integer; const Value: string);
    procedure SetNameValue(Name: string; const Value: string);
    procedure SetValues(Index: Integer; const Value: string);
  private
    function GetText: string;
    procedure SetText(const Value: string);
    function GetConn(Index: Integer): TADOConnection;
    procedure SetConn(Index: Integer; const Value: TADOConnection);
    procedure Grow;
    function GetNameConn(Name: string): TADOConnection;
    procedure SetNameConn(Name: string; const Value: TADOConnection);
  public
    constructor Create;
    destructor Destroy; override;
    /// <summary>清除所有</summary>
    procedure Clear;
    /// <summary>不检查 Name 的重复性，直接添加</summary>
    function Add(const Name, Value: string; Conn: TADOConnection = nil)
      : Integer;
    /// <summary>检查 Name 的重复性，非重复值添加，重复值替换</summary>
    function Save(const Name, Value: string;
      Conn: TADOConnection = nil): Integer;
    /// <summary>读取inifile中连接的信息</summary>
    procedure ReadValues(IniFile: TIniFile; Section: string);
    /// <summary>删除所有等于 Name 的值</summary>
    procedure Delete(const Name: string); overload;
    /// <summary>删除该序号的值</summary>
    procedure Delete(const Index: Integer); overload;
    /// <summary>得到第一个等于 Name 的位置</summary>
    function IndexOfName(Name: string): Integer;
    /// <summary>得到第一个等于 Value 的位置</summary>
    function IndexOfValue(Value: string): Integer;
    /// <summary>得到该位置的 Name 信息</summary>
    property Names[Index: Integer]: string read GetNames write SetNames;
    /// <summary>得到该位置的 Value 信息</summary>
    property Values[Index: Integer]: string read GetValues write SetValues;
    /// <summary>得到该位置的 Object 信息</summary>
    property Conn[Index: Integer]: TADOConnection read GetConn write SetConn;
    /// <summary>根据 Name 得到 Value 值</summary>
    property NameValue[Name: string]: string read GetNameValue
      write SetNameValue;
    /// <summary>根据 Name 得到 Object 值</summary>
    property NameConn[Name: string]: TADOConnection read GetNameConn
      write SetNameConn;
  published
    /// <summary>[ReadOnly]当前记录数量</summary>
    property Count: Integer read FCount;
    /// <summary>Name-Value的存储文本</summary>
    property Text: string read GetText write SetText;
  end;

  /// <summary>通讯服务器控件基类</summary>
  TCommunicationServer = class(TCommunicationBase)
  private
    { Private properties declarations }
    FAutoStart: Boolean;
    FActived: Boolean;
    FConfigFile: TIniFile; // 系统配置文件
    FConfigFilename: string;
    procedure SetActived(const Value: Boolean);
    procedure SetAutoStart(const Value: Boolean);
  private
    FConnections: TStringList; // 应用服务的数据库链接列表
    FUpgradeFiles: TVonUpgradeFiles; // 升级文件列表TVonUpgradeFiles
    procedure SetConfigFilename(const Value: string);
    procedure SetVerFile(Flag: string; TypeID: Word; Ver, DestName: string;
      Msg: TVonCommunicationMessage);
  protected
    procedure ReceiveIt(Msg: TVonCommunicationMessage);
  public
    /// <summary>初始化通讯控件</summary>
    /// <param name="CommType">通讯方式</param>
    /// <param name="ApplicationExeName">应用系统程序名称</param>
    /// <param name="ConfigFilename">配置文件名称</param>
    class function InitCommunication(CommType: ECOMMTYPE;
      ApplicationExeName, ConfigFilename: string): TCommunicationServer; static;
    constructor Create(ApplicationExeName, ConfigFilename: string); virtual;
    destructor Destroy; override;
    procedure Start; virtual; abstract;
    procedure Stop; virtual; abstract;
    function GetConnections(AppName: string): TADOConnection;
    /// <summary>客户端读取服务端版本，在返回内容中包含当前版本和需升级的文件列表</summary>
    /// <param name="Ver">客户端当前版本信息</param>
    function GetVer(Ver, App: string; outStream: TStream): string;
    /// <summary>客户端向服务器发送文件</summary>
    /// <param name="FileFlag">文件标识</param>
    /// <param name="Msg">通讯数据结构</param>
    procedure SendFile(FileFlag: string; Msg: TVonCommunicationMessage);
    /// <summary>客户端从服务器接收数据</summary>
    /// <param name="DataFlag">数据标识</param>
    /// <param name="Msg">通讯数据结构</param>
    procedure ReceiverData(DataFlag: string; Msg: TVonCommunicationMessage);
    procedure SaveConnection(AppName, ConnStr: string);
    property Connections[AppName: string]: TADOConnection read GetConnections;
  published
    /// <summary>服务是否自动启动</summary>
    property AutoStart: Boolean read FAutoStart write SetAutoStart;
    /// <summary>启动状态</summary>
    property Actived: Boolean read FActived write SetActived;
    /// <summary>系统配置文件名称</summary>
    property ConfigFilename: string read FConfigFilename
      write SetConfigFilename;
    /// <summary>系统配置文件对象</summary>
    property ConfigFile: TIniFile read FConfigFile;
  end;

  /// <summary>通讯客户端控件基类</summary>
  TCommunicationClient = class(TCommunicationBase)
  private
    { Private properties declarations }
    FAppName: string;
    /// <summary>待接收的文件缓存列表</summary>
    FReceiveFiles: TStringList;
    /// <summary>待发送的文件缓存列表</summary>
    FSendFiles: TStringList;
    FCurrentVer: string;
    FHostIP: string;
    FOnUpgradeVersion: TEventToUpgradeVer;
    procedure SetCurrentVer(const Value: string);
    procedure SetHostIP(const Value: string);
    procedure SetOnUpgradeVersion(const Value: TEventToUpgradeVer);
  protected
    procedure SendIt(Msg: TVonCommunicationMessage); virtual; abstract;
  public
    /// <summary>初始化通讯控件</summary>
    /// <param name="CommType">通讯方式</param>
    /// <param name="AppExeName">应用系统执行文件名称</param>
    /// <param name="AppName">应用系简称长度为6个字节</param>
    class function InitCommunication(CommType: ECOMMTYPE;
      ApplicationExeName, ApplicationName: string)
      : TCommunicationClient; static;
    /// <summary>构造函数</summary>
    /// <param name="ApplicationExeName">应用系统执行文件名称</param>
    /// <param name="ApplicationName">应用系统简称（8个字节）</param>
    constructor Create(ApplicationExeName, ApplicationName: string); virtual;
    destructor Destroy; override;
    /// <summary>客户端发布版本文件</summary>
    /// <param name="FileFlag">文件标识</param>
    /// <param name="Ver">版本信息</param>
    /// <param name="TypeID">文件类型（替换文件，SQL更新，直接执行，任务列表，SQL查询）</param>
    /// <param name="OrgFilename">要发送的文件名</param>
    /// <param name="DestFilename">目标文件名(含相对路径)</param>
    procedure SendVerFile(FileFlag, Ver: string; TypeID: Word;
      OrgFilename, DestFilename: string);
    /// <summary>客户端接收版本文件</summary>
    /// <param name="FileFlag">文件标识</param>
    /// <param name="FileStream">返回的文件内容</param>
    procedure ReceiveFile(FileFlag: string; FileStream: TStream);
    /// <summary>客户端读取服务端版本，在返回内容中包含当前版本和需升级的文件列表</summary>
    /// <param name="Ver">客户端当前版本信息</param>
    /// <param name="Files">版本信息列表，其中 Ver=[最新版本]</param>
    /// <returns>是否需要升级</returns>
    function CheckVer(Ver: string; Files: TStrings): Boolean;
    /// <summary>客户端现在文件列表中的文件，下载到Upgrade目录中</summary>
    /// <param name="Files">版本信息列表，其中 Ver=[最新版本]</param>
    procedure DownloadFiles(Files: TStrings; LogMemo: TMemo);
    /// <summary>客户端向服务器发送数据</summary>
    /// <param name="DataFlag">数据标识</param>
    /// <param name="DataStream">发过来的流</param>
    procedure SendData(DataFlag: string; DataStream: TStream);
  published
    property AppExeName: string read FAppExeName;
    property AppName: string read FAppName;
    property HostIP: string read FHostIP write SetHostIP;
    property CurrentVer: string read FCurrentVer write SetCurrentVer;
    /// <summary>版本比较，得到差异文件列表后调用该事件</summary>
    property OnUpgradeVersion: TEventToUpgradeVer read FOnUpgradeVersion
      write SetOnUpgradeVersion;
  end;

  /// <summary>将一个字符串写入流</summary>
procedure WriteStringToStream(AStream: TStream; S: string);

var
  IdFreeze: TIdAntiFreeze;

implementation

uses
  DateUtils, StrUtils, UCommunicationRes, UCommunicationHttp, UCommunicationTCP,
  UCommunicationUDP;

const
  FILE_INFO_BREAK = '|';

  { %CLASSGROUP 'System.Classes.TPersistent' }

  /// <summary>将一个字符串写入流中</summary>
  /// <param name="AStream">流</param>
  /// <param name="S">要写入的字符串</param>
procedure WriteStringToStream(AStream: TStream; S: string);
begin
  AStream.Write(S[1], Length(S) * SizeOf(Char));
end;

procedure WriteStringToStreamWithSize(AStream: TStream; S: string);
var
  szInt: Int64;
begin
  szInt := Length(S);
  AStream.Write(szInt, 8);
  AStream.Write(S[1], Length(S) * SizeOf(Char));
end;

{ TCommunicationBase }

procedure TCommunicationBase.CompressData(OrgData, RtnData: TStream);
begin // 压缩数据流
  OrgData.Position := 0;
  RtnData.Position := 0;
  ZCompressStream(OrgData, RtnData);
  OrgData.Position := 0;
  RtnData.Position := 0;
end;

constructor TCommunicationBase.Create(ApplicationExeName: string);
begin
  ExeName := ApplicationExeName;
  if not DirectoryExists(FAppPath + 'Temp') then
    CreateDir(FAppPath + 'Temp'); // 创建缓存目录
  if not DirectoryExists(FAppPath + 'Upgrade') then
    CreateDir(FAppPath + 'Upgrade'); // 创建升级目录
  if not DirectoryExists(FAppPath + 'Data') then
    CreateDir(FAppPath + 'Data'); // 创建数据存储目录
end;

procedure TCommunicationBase.DecompressData(OrgData, RtnData: TStream);
begin // 解压缩数据流信息
  OrgData.Position := 0;
  RtnData.Size := 0;
  ZDecompressStream(OrgData, RtnData);
  OrgData.Position := 0;
  RtnData.Position := 0;
end;

destructor TCommunicationBase.Destroy;
begin
  inherited;
end;

procedure TCommunicationBase.SetAppExeName(const Value: string);
begin
  FAppExeName := Value;
  FAppPath := ExtractFilePath(Value);
end;

procedure TCommunicationBase.SetCommunicationPort(const Value: Integer);
begin
  FCommunicationPort := Value;
end;

procedure TCommunicationBase.SetLogComponent(const Value: TMemo);
begin
  FLogComponent := Value;
end;

procedure TCommunicationBase.SetOnBeginToSendData
  (const Value: TEventToProcessData);
begin
  FOnBeginToSendData := Value;
end;

procedure TCommunicationBase.SetOnEndOfReceiveData
  (const Value: TEventToProcessData);
begin
  FOnEndOfReceiveData := Value;
end;

procedure TCommunicationBase.ShowStream(Msg: TVonCommunicationMessage);
var
  szPos: Int64;
  S: string;
  b: Byte;
begin // 想日志中写入信息内容，可用于测试和写日志
  WriteCommLog(LOG_INFO, 'ShowStream', Msg.AppName + '[' + Msg.CommandString +
    ']' + Msg.Params);
  szPos := Msg.DataStream.Position;
  Msg.DataStream.Position := 0;
  S := '';
  while Msg.DataStream.Position < Msg.DataStream.Size do
  begin
    Msg.DataStream.Read(b, 1);
    S := S + ' ' + IntToHex(b, 2);
  end;
  Msg.DataStream.Position := szPos;
  WriteCommLog(LOG_INFO, 'ShowStream', S);
end;

procedure TCommunicationBase.SplitParams(AStream: TStream; ParamLen: Integer;
  Params: TStringList);
var
  arrData: TBytes;
begin // 分解参数
  SetLength(arrData, ParamLen);
  AStream.Read(arrData[0], AStream.Size);
  Params.Delimiter := ',';
  Params.DelimitedText := TEncoding.Unicode.GetString(arrData);
  SetLength(arrData, 0);
end;

procedure TCommunicationBase.WriteCommLog(LogClass: ELOG_Class;
  Process, Info: string);
var
  S: string;
begin
  WriteLog(LogClass, Process, Info);
  S := Format('[%s]%s', [DateTimeToStr(Now), Info]);
  if Assigned(FLogComponent) then
    FLogComponent.Lines.Add(S);
end;

{ TCommunicationServer }

constructor TCommunicationServer.Create(ApplicationExeName,
  ConfigFilename: string);
begin
  inherited Create(ApplicationExeName);
  FConnections := TStringList.Create;
  FUpgradeFiles := TVonUpgradeFiles.Create;
  FConfigFile := TIniFile.Create(AppPath + ConfigFilename);
  FConfigFile.ReadSectionValues('CONNECTIONS', FConnections);
  FUpgradeFiles.IniFile := FConfigFile;
end;

destructor TCommunicationServer.Destroy;
var
  I: Integer;
begin
  FConfigFile.Free;
  FUpgradeFiles.Free;
  for I := 0 to FConnections.Count - 1 do
    if Assigned(FConnections.Objects[I]) then
      FConnections.Objects[I].Free;
  FConnections.Free;
  inherited;
end;

function TCommunicationServer.GetConnections(AppName: string): TADOConnection;
var
  Idx: Integer;
begin
  Idx := FConnections.IndexOfName(AppName);
  if Idx < 0 then
    Result := nil
  else
  begin
    CoInitialize(Nil);
    Result := TADOConnection.Create(nil);
    Result.ConnectionString := FConnections.ValueFromIndex[Idx];
    Result.LoginPrompt := False;
    Result.Open();
    CoUninitialize;
  end;
  Exit;

  Idx := FConnections.IndexOfName(AppName);
  if Idx < 0 then
    Result := nil
  else
  begin
    if not Assigned(FConnections.Objects[Idx]) then
      try
        CoInitialize(Nil);
        Result := TADOConnection.Create(nil);
        Result.ConnectionString := FConnections.ValueFromIndex[Idx];
        Result.LoginPrompt := False;
        Result.Open();
        FConnections.Objects[Idx] := Result;
        CoUninitialize;
      except
        on E: Exception do
        begin
          ShowMessage(E.Message);
          Result.Free;
          Result := nil;
          CoUninitialize;
        end;
      end
    else
      Result := TADOConnection(FConnections.Objects[Idx]);
  end;
end;

function TCommunicationServer.GetVer(Ver, App: string;
  outStream: TStream): string;
begin
  outStream.Size := 0;
  FUpgradeFiles.GetUpgradeFiles(App, Ver, outStream);
  outStream.Position := 0;
end;

class function TCommunicationServer.InitCommunication(CommType: ECOMMTYPE;
  ApplicationExeName, ConfigFilename: string): TCommunicationServer;
begin
  case CommType of
    COMM_HTTP:
      Result := TCommunicationHttpServer.Create(ApplicationExeName,
        ConfigFilename);
    COMM_TCP:
      Result := TCommunicationTCPServer.Create(ApplicationExeName,
        ConfigFilename);
    COMM_UDP:
      Result := TCommunicationUDPServer.Create(ApplicationExeName,
        ConfigFilename);
  end;
end;

procedure TCommunicationServer.ReceiveIt(Msg: TVonCommunicationMessage);
var
  paramStr: TStringList;
begin
  WriteCommLog(LOG_INFO, 'CommunicationServer.ReceiveIt',
    Format(RES_COMM_ReceiveInfo, [Msg.CommandString, Msg.AppName]));
  // ShowStream(Msg);
  // WriteCommLog(LOG_DEBUG, 'CommunicationServer', format('App=%s'#13#10'Cmd=%s'#13#10'Param=%s', [Msg.AppName, Msg.CommandString, Msg.Params]));
  case Msg.Cmd of
    CMD_GETVER:
      begin // GETVER(6)<Ver>
        GetVer(Msg.Params, Msg.AppName, Msg.DataStream);
      end;
    CMD_SETVER:
      begin // SETVER(6)<AppName(6)><HeaderLen(4)><FileFlag>,<TypeID>,<Version>,<DestFilename>
        paramStr := TStringList.Create;
        paramStr.Delimiter := ',';
        paramStr.DelimitedText := Msg.Params;
        SetVerFile(paramStr[0], StrToInt(paramStr[1]), paramStr[2],
          paramStr[3], Msg);
        paramStr.Free;
      end;
    CMD_RCVFILE:
      begin // RCVFIL(6)<AppName(6)><HeaderLen(4)><FileFlag>
        SendFile(Msg.Params, Msg);
      end;
    CMD_SNDDATA:
      begin // SETDAT(6)<AppName(6)><HeaderLen(4)><DataFlag>
        ReceiverData(Msg.Params, Msg);
      end;
  end;
  WriteCommLog(LOG_INFO, 'CommunicationServer.SendIt',
    Format(RES_COMM_ReceiveInfo, [Msg.CommandString, Msg.AppName]));
  // ShowStream(Msg);
end;

procedure TCommunicationServer.ReceiverData(DataFlag: string;
  Msg: TVonCommunicationMessage);
var
  // unCommStream: TMemoryStream;
  unCommStream: TFileStream;
  bakFilename, ReturnValue: string;

  procedure WriteStream(st: TStream);
  var
    S: string;
    b: Byte;
    p: Integer;
  begin
    S := '';
    p := st.Position;
    while st.Position < st.Size do
    begin
      st.Read(b, 1);
      S := S + ' ' + IntToHex(b, 2);
    end;
    st.Position := p;
    WriteCommLog(LOG_INFO, 'ShowStream', S);
  end;

begin // 接收客户端向服务器发送的数据
  ReturnValue := '';
  bakFilename := AppPath + 'Data\' + Msg.AppName + Msg.Params + '_' +
    StringReplace(Msg.BindingHost, '.', '_', [rfReplaceAll, rfIgnoreCase]) + '_'
    + FormatDatetime('YYYYMMDDHHNNSS', Now);
  if FileExists(bakFilename) then
    Exit;
  // unCommStream:= TMemoryStream.Create;
  unCommStream := TFileStream.Create(bakFilename, fmCreate);
  // save a file to backup
  try
    // WriteStream(Msg.DataStream);
    DecompressData(Msg.DataStream, unCommStream);
    unCommStream.Position := 0;
    // WriteStream(unCommStream);
    if Assigned(FOnEndOfReceiveData) then
      ReturnValue := FOnEndOfReceiveData(Msg.AppName, DataFlag, unCommStream);
  finally
    unCommStream.Free;
    Msg.DataStream.Size := 0;
    Msg.Params := ReturnValue;
  end;
end;

procedure TCommunicationServer.SaveConnection(AppName, ConnStr: string);
var
  Idx: Integer;
  Conn: TADOConnection;
begin
  FConfigFile.WriteString('CONNECTIONS', AppName, ConnStr);
  Idx := FConnections.IndexOfName(AppName);
  if (Idx < 0) or (not Assigned(FConnections.Objects[Idx])) then
    Conn := TADOConnection.Create(nil)
  else
    Conn := TADOConnection(FConnections.Objects[Idx]);
  if Idx < 0 then
    Idx := FConnections.AddObject(AppName + '=' + ConnStr, Conn)
  else
    FConnections.ValueFromIndex[Idx] := ConnStr;
  try
    Conn.Close();
    Conn.ConnectionString := ConnStr;
    Conn.LoginPrompt := False;
    Conn.Open();
  except
    Conn.Free;
    FConnections.Objects[Idx] := nil;
  end;
end;

procedure TCommunicationServer.SendFile(FileFlag: string;
  Msg: TVonCommunicationMessage);
var
  szFile: TVonUpgradeFile;
  fs: TStream;
begin // 向客户端发送一个文件
  szFile := FUpgradeFiles.GetAppFile(Msg.AppName, FileFlag);
  if not Assigned(szFile) then
    Exit;
  szFile.FileStream.Position := 0;
  Msg.DataStream.Size := 0;
  Msg.DataStream.CopyFrom(szFile.FileStream, szFile.FileStream.Size);
end;

procedure TCommunicationServer.SetActived(const Value: Boolean);
begin
  FActived := Value;
  if FActived then
    Start
  else
    Stop;
end;

procedure TCommunicationServer.SetAutoStart(const Value: Boolean);
begin
  FAutoStart := Value;
  if FAutoStart then
    Start;
end;

procedure TCommunicationServer.SetConfigFilename(const Value: string);
begin
  FConfigFilename := Value;
end;

procedure TCommunicationServer.SetVerFile(Flag: string; TypeID: Word;
  Ver, DestName: string; Msg: TVonCommunicationMessage);
var
  szFilename: string;
  fs: TFileStream;
  szStream: TStream;

  procedure SaveToUpgradeFile;
  var
    szFile: TVonUpgradeFile;
  begin
    szFile := FUpgradeFiles.GetAppFile(Msg.AppName, Flag);
    if not Assigned(szFile) then
      szFile := TVonUpgradeFile.Create;
    szFile.Ver := Ver;
    szFile.Flag := Flag;
    szFile.TypeID := TypeID;
    szFile.AppPath := FAppPath;
    szFile.FApp := Msg.AppName;
    szFile.DestFilename := DestName;
    WriteLog(LOG_DEBUG, 'SaveToUpgradeFile',
      Format('FVer=%s, FApp=%s, FAppPath=%s, Flag=%s',
      [szFile.FVer, szFile.FApp, szFile.FAppPath, szFile.Flag]));
    szFile.WriteFile(Msg.DataStream, 0, Msg.DataStream.Size);
    FUpgradeFiles.Save(Msg.AppName, szFile);
    // szFile.CloseFile;
    Msg.DataStream.Size := 0;
    Msg.Params := 'SUCCESS';
  end;

begin
  CoInitialize(nil);
  WriteLog(LOG_DEBUG, 'SetVerFile',
    Format('Flag=%s, TypeID=%d, Ver=%s, DestName=%s', [Flag, TypeID, Ver,
    DestName]));
  case TypeID of
    0, 1, 2, 3:
      SaveToUpgradeFile; // 升级替换,升级SQL,直接执行,任务列表，接收后放入升级文件缓存文件列表中，待用户端下载升级
{$REGION 'SQL查询'}
    4:
      with TADOQuery.Create(nil) do
        try
          ConnectionString := FConnections.Values[Msg.AppName];
          Msg.DataStream.Position := 0;
          szStream := TMemoryStream.Create;
          DecompressData(Msg.DataStream, szStream);
          szStream.Position := 0;
          SQL.LoadFromStream(szStream);
          szStream.Free;
          // ShowMessage(SQL.Text);
          Msg.DataStream.Size := 0;
          try
            Open;
            szFilename := FAppPath + 'Temp\' + GetNewID();
            SaveToFile(szFilename, pfXML);
            fs := TFileStream.Create(szFilename, fmOpenRead);
            CompressData(fs, Msg.DataStream);
            fs.Free;
            DeleteFile(szFilename);
            Msg.Params := 'SUCCESS';
          except
            Msg.Params := 'FAILED';
          end;
        finally
          Free;
        end;
{$ENDREGION}
  end;
  CoUninitialize;
end;

{ TCommunicationClient }

constructor TCommunicationClient.Create(ApplicationExeName,
  ApplicationName: string);
begin
  inherited Create(ApplicationExeName);
  FAppName := Copy(ApplicationName + '________', 1, 8);
  // FAppname必须是8个字节长度，不足补下划线
  FReceiveFiles := TStringList.Create;
  FSendFiles := TStringList.Create;
end;

destructor TCommunicationClient.Destroy;
begin
  FSendFiles.Free;
  FReceiveFiles.Free;
  inherited;
end;

procedure TCommunicationClient.DownloadFiles(Files: TStrings; LogMemo: TMemo);
var
  I: Integer;
  szFile: TFileStream;
begin
  for I := 0 to Files.Count - 1 do
    try
      // Download files to upgrade
      szFile := TFileStream.Create(FAppPath + 'Upgrade\' + Files.Names[I],
        fmCreate);
      ReceiveFile(Files.Names[I], szFile);
      if Assigned(LogMemo) then
        LogMemo.Lines.Add('文件 ' + Files.Names[I] + ' 完成下载。');
    finally
      szFile.Free;
    end;
end;

class function TCommunicationClient.InitCommunication(CommType: ECOMMTYPE;
  ApplicationExeName, ApplicationName: string): TCommunicationClient;
begin
  case CommType of
    COMM_HTTP:
      Result := TCommunicationHttpClient.Create(ApplicationExeName,
        ApplicationName);
    COMM_TCP:
      Result := TCommunicationTCPClient.Create(ApplicationExeName,
        ApplicationName);
    COMM_UDP:
      Result := TCommunicationUDPClient.Create(ApplicationExeName,
        ApplicationName);
  end;
end;

procedure TCommunicationClient.ReceiveFile(FileFlag: string;
  FileStream: TStream);
var
  Msg: TVonCommunicationMessage;
begin
  Msg := TVonCommunicationMessage.Create(CMD_RCVFILE, AppName, FileFlag);
  SendIt(Msg);
  DecompressData(Msg.DataStream, FileStream);
  Msg.Free;
end;

function TCommunicationClient.CheckVer(Ver: string; Files: TStrings): Boolean;
var
  Msg: TVonCommunicationMessage;
  arrData: TBytes;
  newVer: string;
begin
  Msg := TVonCommunicationMessage.Create(CMD_GETVER, AppName, Ver);
  SendIt(Msg);
  SetLength(arrData, Msg.DataStream.Size);
  Msg.DataStream.Read(arrData[0], Msg.DataStream.Size);
  Files.Delimiter := ',';
  Files.DelimitedText := TEncoding.Unicode.GetString(arrData);
  WriteCommLog(LOG_INFO, 'CheckVer', Files.Text);
  SetLength(arrData, 0);
  newVer := Files.Values['VER'];
  Files.Delete(Files.IndexOfName('VER'));
  Result := Ver <> newVer;
  if Assigned(FOnUpgradeVersion) and (Files.Count > 0) and Result then
    FOnUpgradeVersion(newVer, Files);
end;

procedure TCommunicationClient.SendData(DataFlag: string; DataStream: TStream);
var
  Msg: TVonCommunicationMessage;
  SendCount: Integer;
  FReturnValue: string;
begin // SETVER(6)<AppName(6)><HeaderLen(4)><FileFlag>,<Version>,<DestFilename>
  WriteCommLog(LOG_INFO, 'CommunicationClient',
    'Ready to send ' + IntToStr(DataStream.Size) + ' datas.');
  Msg := TVonCommunicationMessage.Create(CMD_SNDDATA, AppName, DataFlag);
  SendCount := 0;
  FReturnValue := '';
  repeat
    DataStream.Position := 0;
    Msg.DataStream.Size := 0;
    CompressData(DataStream, Msg.DataStream);
    SendIt(Msg); // 发送
    Inc(SendCount);
  until (Copy(Msg.Params, 1, 7) = 'SUCCESS') or (SendCount >= 3);
  FReturnValue := Msg.Params;
  Msg.Free;
  if Copy(FReturnValue, 1, 7) <> 'SUCCESS' then
    raise Exception.Create('The data ' + DataFlag + ' is not send to server.');
  WriteCommLog(LOG_INFO, 'CommunicationClient', 'Send data ' + DataFlag +
    ' successed!');
  Delete(FReturnValue, 1, 7);
end;

procedure TCommunicationClient.SetCurrentVer(const Value: string);
begin
  FCurrentVer := Value;
end;

procedure TCommunicationClient.SetHostIP(const Value: string);
begin
  FHostIP := Value;
end;

procedure TCommunicationClient.SetOnUpgradeVersion
  (const Value: TEventToUpgradeVer);
begin
  FOnUpgradeVersion := Value;
end;

procedure TCommunicationClient.SendVerFile(FileFlag, Ver: string; TypeID: Word;
  OrgFilename, DestFilename: string);
var
  Msg: TVonCommunicationMessage;
  szFile: TFileStream;
  szStream: TStream;
  SendCount: Integer;
  FReturnValue: string;
  RtnData: array [0 .. 1024] of Char;
begin // SETVER(6)<AppName(6)><HeaderLen(4)><FileFlag>,<TypeID>,<Version>,<DestFilename>
  Msg := TVonCommunicationMessage.Create(CMD_SETVER, AppName,
    Format('%s,%d,%s,%s', [FileFlag, TypeID, Ver, DestFilename]));
  SendCount := 0;
  FReturnValue := '';
  repeat
    szFile := TFileStream.Create(OrgFilename, fmOpenRead);
    Msg.DataStream.Size := 0;
    CompressData(szFile, Msg.DataStream); // 压缩
    szFile.Free;
    SendIt(Msg); // 发送
    Inc(SendCount);
  until (Copy(Msg.Params, 1, 7) = 'SUCCESS') or (SendCount >= 3);
  FReturnValue := Msg.Params;
  Msg.Free;
  if Copy(FReturnValue, 1, 7) <> 'SUCCESS' then
    raise Exception.Create('The file ' + FileFlag + ' is not send to server.');
  WriteCommLog(LOG_INFO, 'CommunicationClient', 'Send file ' + FileFlag +
    ' successed!');
end;

{ TVonCommunicationMessage }

constructor TVonCommunicationMessage.Create;
begin
  SendStream := TMemoryStream.Create;
  DataStream := TMemoryStream.Create;
end;

function TVonCommunicationMessage.CommandString: string;
begin
  case Cmd of // 写入命令及应用名称
    CMD_GETVER:
      Result := 'GETVER';
    CMD_SETVER:
      Result := 'SETVER';
    CMD_RESEND:
      Result := 'RESEND';
    CMD_RESERV:
      Result := 'RESERV';
    CMD_SNDFILE:
      Result := 'SNDFIL';
    CMD_RCVFILE:
      Result := 'RCVFIL';
    CMD_SNDDATA:
      Result := 'SNDDAT';
    CMD_RCVDATA:
      Result := 'RCVDAT';
  end;
end;

constructor TVonCommunicationMessage.Create(ACmd: EVonCommunicationCmd;
  AAppName, AParams: string);
begin
  Create;
  Cmd := ACmd;
  AppName := AAppName;
  Params := AParams;
end;

destructor TVonCommunicationMessage.Destroy;
begin
  DataStream.Free;
  SendStream.Free;
  inherited;
end;

function TVonCommunicationMessage.MerageStream: TStream;
var
  szSize: Integer;
begin
  SendStream.SetSize(0);
  WriteStringToStream(SendStream, CommandString + AppName); // 写入命令及应用名称
  szSize := Length(Params) * SizeOf(Char); // 写入参数信息
  SendStream.Write(szSize, 4);
  if szSize > 0 then
    SendStream.Write(Params[1], szSize);
  DataStream.Position := 0;
  if DataStream.Size > 0 then
    SendStream.CopyFrom(DataStream, DataStream.Size); // 写入数据
  SendStream.Position := 0;
  Result := SendStream;
end;

function TVonCommunicationMessage.ReadFromStream(ReceiveStream
  : TStream): Boolean;
var
  szStr: PChar;
  cmdStr: string;
  szSize: Integer;
  b: Byte;
begin
  Result := False;
  try
    ReceiveStream.Position := 0;
    if ReceiveStream.Size < 16 then
      raise Exception.Create('The message size is very shorter.');
    szStr := StrAlloc(9);
    ReceiveStream.Read(szStr[0], 6 * SizeOf(Char)); // Read command         (6)
    szStr[6] := #0;
    cmdStr := string(szStr);
    ReceiveStream.Read(szStr[0], 8 * SizeOf(Char)); // Read application name(8)
    szStr[8] := #0;
    AppName := string(szStr);
    StrDispose(szStr);
    if cmdStr = 'GETVER' then
      Cmd := CMD_GETVER
    else if cmdStr = 'SETVER' then
      Cmd := CMD_SETVER
    else if cmdStr = 'RESEND' then
      Cmd := CMD_RESEND
    else if cmdStr = 'RESERV' then
      Cmd := CMD_RESERV
    else if cmdStr = 'SNDFIL' then
      Cmd := CMD_SNDFILE
    else if cmdStr = 'RCVFIL' then
      Cmd := CMD_RCVFILE
    else if cmdStr = 'SNDDAT' then
      Cmd := CMD_SNDDATA
    else if cmdStr = 'RCVDAT' then
      Cmd := CMD_RCVDATA
    else
      raise Exception.Create('Error command of ' + cmdStr + '.');
    ReceiveStream.Read(szSize, 4); // 读取参数长度
    if szSize > 0 then
    begin
      szStr := StrAlloc((szSize div SizeOf(Char)) + 1);
      ReceiveStream.Read(szStr[0], szSize);
      szStr[(szSize div SizeOf(Char))] := #0;
      Params := string(szStr);
      StrDispose(szStr);
    end
    else
      Params := '';
    DataStream.Size := 0;
    DataStream.CopyFrom(ReceiveStream, ReceiveStream.Size -
      ReceiveStream.Position);
    DataStream.Position := 0;
  except
    on E: Exception do
    begin
      ReceiveStream.Position := 0;
      cmdStr := '';
      szSize := 0;
      while ReceiveStream.Position < ReceiveStream.Size do
      begin
        ReceiveStream.Read(b, 1);
        cmdStr := cmdStr + ' ' + IntToHex(b, 2);
        Inc(szSize);
        if szSize mod 16 = 0 then
          cmdStr := cmdStr + #13#10;
      end;
      WriteLog(LOG_FAIL, 'StreamToMsg',
        'Receiver a message from client. The error is raised. '#13#10 +
        E.Message + cmdStr);
    end;
  end;
  Result := True;
end;

{ TVonUpgradeFile }

procedure TVonUpgradeFile.CloseFile;
begin
  if Assigned(FFileStream) then
    FreeAndNil(FFileStream);
end;

destructor TVonUpgradeFile.Destroy;
begin
  if not Assigned(FFileStream) then
    FFileStream.Free;
  inherited;
end;

function TVonUpgradeFile.GetFileStream: TFileStream;
var
  szPath: string;
begin
  WriteLog(LOG_DEBUG, 'GetFileStream', Format('FAppPath=%s, FApp=%s, Flag=%s',
    [FAppPath, FApp, Flag]));
  if not Assigned(FFileStream) then
  begin
    if not DirectoryExists(FAppPath + FApp) then
      CreateDir(FAppPath + FApp);
    if FileExists(FAppPath + FApp + '\' + Flag) then
      FFileStream := TFileStream.Create(FAppPath + FApp + '\' + Flag,
        fmOpenWrite)
    else
      FFileStream := TFileStream.Create(FAppPath + FApp + '\' + Flag, fmCreate)
  end;
  Result := FFileStream;
end;

procedure TVonUpgradeFile.SetApp(const Value: string);
begin
  FApp := Value;
end;

procedure TVonUpgradeFile.SetAppPath(const Value: string);
begin
  FAppPath := Value;
end;

procedure TVonUpgradeFile.SetVer(const Value: string);
begin
  FVer := Value;
  VerValue.Text:= Value;
end;

procedure TVonUpgradeFile.SetVerValue(const Value: TVonVersion);
begin
  VerValue.Int := Value.Int;
  Ver := VerValue.Text;
end;

procedure TVonUpgradeFile.WriteFile(ADataStream: TStream;
  APosition, ASize: Int64);
var
  szPath: string;
begin
  with FileStream do
  begin
    Position := APosition;
    CopyFrom(ADataStream, ASize);
  end;
  WriteLog(LOG_DEBUG, 'WriteFile', Format('APosition=%d, ASize=%d',
    [APosition, ASize]));
end;

{ TVonUpgradeFiles }

function ValueSort(List: TStringList; Index1, Index2: Integer): Integer;
begin // 根据value来比较排序
  Result := AnsiCompareText(List.ValueFromIndex[Index1],
    List.ValueFromIndex[Index2]);
end;

procedure TVonUpgradeFiles.Save(App: string; AFile: TVonUpgradeFile);
var
  Idx: Integer;
  lst: TStringList;
begin // <Flag>=<TypeID>|<Ver>|<DestFilename>
  WriteLog(LOG_DEBUG, 'TVonUpgradeFiles',
    'Receiver a file to upgrade. The filename = ' + AFile.Flag + ' ' +
    AFile.Ver);
  lst := GetAppFiles(App);
  with lst do
  begin
    Idx := IndexOfName(AFile.Flag);
    WriteLog(LOG_DEBUG, 'Save', Format('Find it at %d', [Idx]));
    if Idx >= 0 then
    begin
      ValueFromIndex[Idx] := AFile.VerValue.Hex;
      if AFile <> Objects[Idx] then
      begin
        WriteLog(LOG_DEBUG, 'Save', Format('Find a other %d', [Idx]));
        lst.Objects[Idx].Free;
      end;
      Objects[Idx] := AFile;
    end
    else
    begin
      WriteLog(LOG_DEBUG, 'Save', Format('AddObject %s=%s in to %s',
        [AFile.Flag, AFile.VerValue.Hex, Text]));
      AddObject(AFile.Flag + '=' + AFile.VerValue.Hex, AFile);
    end;
    CustomSort(ValueSort);
  end;
  FIniFile.WriteString(App, AFile.Flag, IntToStr(AFile.TypeID) + FILE_INFO_BREAK
    + AFile.Ver + FILE_INFO_BREAK + AFile.DestFilename);
  FIniFile.UpdateFile;
end;

procedure TVonUpgradeFiles.SetIniFile(const Value: TIniFile);
var
  I: Integer;
  szList: TStringList;
begin
  szList := TStringList.Create;
  FIniFile := Value;
  FIniFile.ReadSections(szList);
  if szList.IndexOf('CONNECTIONS') >= 0 then
    szList.Delete(szList.IndexOf('CONNECTIONS'));
  for I := 0 to szList.Count - 1 do
    ReadFromIniFile(FIniFile, szList[I]);
  szList.Free;
end;

constructor TVonUpgradeFiles.Create;
begin
  FAppList := TStringList.Create;
  FAppList.Sorted := True;
  // FAppList.;
end;

procedure TVonUpgradeFiles.Del(App, Flag: string);
var
  Idx: Integer;
begin
  with GetAppFiles(App) do
  begin
    Idx := IndexOfName(Flag);
    if Idx >= 0 then
    begin
      Objects[Idx].Free;
      Delete(Idx);
      FIniFile.DeleteKey(App, Flag);
      FIniFile.UpdateFile;
    end;
  end;
end;

destructor TVonUpgradeFiles.Destroy;
var
  I, j: Integer;
begin
  for I := 0 to FAppList.Count - 1 do
    with FAppList.Objects[I] as TStringList do
    begin
      for j := 0 to Count - 1 do
        if Assigned(Objects[j]) then
          Objects[j].Free;
      Free;
    end;
  FAppList.Free;
  inherited;
end;

function TVonUpgradeFiles.GetAppFile(App, Flag: string): TVonUpgradeFile;
var
  Idx: Integer;
begin
  Result := nil;
  with GetAppFiles(App) do
  begin
    Idx := IndexOfName(Flag);
    if Idx >= 0 then
      Result := TVonUpgradeFile(Objects[Idx]);
  end;
end;

function TVonUpgradeFiles.GetAppFiles(App: string): TStringList;
var
  Idx: Integer;
begin
  Idx := FAppList.IndexOf(App);
  if Idx < 0 then
  begin
    Result := TStringList.Create;
    FAppList.AddObject(App, Result);
  end
  else
    Result := TStringList(FAppList.Objects[Idx]);
  WriteLog(LOG_DEBUG, 'GetAppFiles', Format('Find it at %d', [Idx]));
end;

procedure TVonUpgradeFiles.GetUpgradeFiles(App, Ver: string;
  outStream: TStream);
var
  I: Integer;
  AppVer, lastVer: TVonVersion;
begin
  AppVer.Text:= Ver;
  lastVer.Int:= AppVer.Int;
  with GetAppFiles(App) do
    for I := 0 to Count - 1 do
      with Objects[I] as TVonUpgradeFile do
        if AppVer.Check(VerValue) < 0 then
        begin
          WriteStringToStream(outStream, Flag + '=' + IntToStr(TypeID) +
            DestFilename + #13#10);
          lastVer.Int:= VerValue.Int;
        end;
  WriteStringToStream(outStream, 'VER=' + lastVer.Text);
end;

procedure TVonUpgradeFiles.ReadFromIniFile(IniFile: TIniFile; AppName: string);
var
  lstFlag, szList: TStringList;
  I, j: Integer;
  szValur: TVonUpgradeFile;
begin // <Flag>=<TypeID>|<Ver>|<DestFilename>
  FIniFile := IniFile;
  lstFlag := TStringList.Create;
  szList := TStringList.Create;
  szList.Delimiter := FILE_INFO_BREAK;
  with GetAppFiles(AppName) do
    try
      FIniFile.ReadSectionValues(AppName, lstFlag);
      for j := 0 to lstFlag.Count - 1 do
      begin // Loop flags
        szValur := TVonUpgradeFile.Create;
        szValur.Flag := lstFlag.Names[j];
        szValur.App := AppName;
        szList.DelimitedText := lstFlag.ValueFromIndex[j];
        szValur.TypeID := StrToInt(szList[0]);
        szValur.Ver := szList[1];
        szValur.DestFilename := szList[2];
        AddObject(szValur.Flag + '=' + szValur.VerValue.Hex, szValur);
      end;
      CustomSort(ValueSort);
    finally
      szList.Free;
      lstFlag.Free;
    end;
end;

{ TVonConnects }

function TVonConnects.Add(const Name, Value: string;
  Conn: TADOConnection): Integer;
begin // 添加一个内容
  Result := FCount;
  Inc(FCount);
  if FCount > FCapacity then
    Grow;
  FNameList[Result] := Name;
  FValueList[Result] := Value;
  FConnList[Result] := Conn;
end;

procedure TVonConnects.Clear;
var
  I: Integer;
begin // 清除所有内容
  for I := 0 to FCount - 1 do
  begin
    FNameList[I] := '';
    FValueList[I] := '';
    FConnList[I].Free;
    FConnList[I] := nil;
  end;
  FCount := 0;
end;

constructor TVonConnects.Create;
begin // 初始化
  FCapacity := 20;
  SetLength(FNameList, FCapacity);
  SetLength(FValueList, FCapacity);
  SetLength(FConnList, FCapacity);
  FCount := 0;
end;

procedure TVonConnects.Delete(const Index: Integer);
var
  I: Integer;
begin
  if (Index < 0) or (Index >= FCount) then
    Exit;
  for I := Index to FCount - 2 do
  begin
    FNameList[I] := FNameList[I + 1];
    FValueList[I] := FValueList[I + 1];
    FConnList[I] := FConnList[I + 1];
  end;
  Dec(FCount);
end;

procedure TVonConnects.Delete(const Name: string);
var
  I: Integer;
begin
  for I := 0 to FCount - 1 do
    if SameText(FNameList[I], Name) then
      Delete(I);
end;

destructor TVonConnects.Destroy;
var
  I: Integer;
begin
  for I := 0 to FCount - 1 do
    if Assigned(FConnList[I]) then
      FConnList[I].Free;
  SetLength(FNameList, 0);
  SetLength(FValueList, 0);
  SetLength(FConnList, 0);
  inherited;
end;

function TVonConnects.GetConn(Index: Integer): TADOConnection;
begin
  if (Index < 0) or (Index >= FCount) then
    Exit;
  if not Assigned(FConnList[Index]) then
  begin
    FConnList[Index] := TADOConnection.Create(nil);
    FConnList[Index].ConnectionString := Values[Index];
    FConnList[Index].LoginPrompt := False;
  end;
  Result := FConnList[Index];
end;

function TVonConnects.GetNameConn(Name: string): TADOConnection;
begin
  Result := GetConn(IndexOfName(Name));
end;

function TVonConnects.GetNames(Index: Integer): string;
begin
  if (Index < 0) or (Index >= FCount) then
    Exit;
  Result := FNameList[Index];
end;

function TVonConnects.GetNameValue(Name: string): string;
begin
  Result := GetValues(IndexOfName(Name));
end;

function TVonConnects.GetText: string;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to FCount - 1 do
    Result := Result + #13#10 + FNameList[I] + '=' + FValueList[I];
  if Length(Result) > 0 then
    System.Delete(Result, 1, 2);
end;

function TVonConnects.GetValues(Index: Integer): string;
begin
  if (Index < 0) or (Index >= FCount) then
    Exit;
  Result := FValueList[Index];
end;

procedure TVonConnects.Grow;
var
  Delta: Integer;
begin // 成长一下系统实际开创的记录空间
  if FCapacity > 64 then
    Delta := FCapacity div 4
  else if FCapacity > 8 then
    Delta := 16
  else
    Delta := 4;
  FCapacity := FCapacity + Delta;
  SetLength(FNameList, FCapacity);
  SetLength(FValueList, FCapacity);
  SetLength(FConnList, FCapacity);
end;

function TVonConnects.IndexOfName(Name: string): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to FCount - 1 do
    if SameText(FNameList[I], Name) then
    begin
      Result := I;
      Exit;
    end;
end;

function TVonConnects.IndexOfValue(Value: string): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to FCount - 1 do
    if SameText(FValueList[I], Value) then
    begin
      Result := I;
      Exit;
    end;
end;

procedure TVonConnects.ReadValues(IniFile: TIniFile; Section: string);
var
  lst: TStringList;
  I: Integer;
begin
  lst := TStringList.Create;
  IniFile.ReadSectionValues(Section, lst);
  Clear();
  while lst.Count > FCapacity do
    Grow;
  for I := 0 to lst.Count - 1 do
  begin
    FNameList[I] := lst.Names[I];
    FValueList[I] := lst.ValueFromIndex[I];
  end;
end;

function TVonConnects.Save(const Name, Value: string;
  Conn: TADOConnection): Integer;
begin // 添加一个内容
  Result := IndexOfName(Name);
  if Result < 0 then
  begin
    Result := FCount;
    Inc(FCount);
  end;
  if FCount > FCapacity then
    Grow;
  FNameList[Result] := Name;
  FValueList[Result] := Value;
  FConnList[Result].Free;
  FConnList[Result] := Conn;
end;

procedure TVonConnects.SetConn(Index: Integer; const Value: TADOConnection);
begin
  if (Index < 0) or (Index >= FCount) then
    Exit;
  FConnList[Index].Free;
  FConnList[Index] := Value;
end;

procedure TVonConnects.SetNameConn(Name: string; const Value: TADOConnection);
begin
  SetConn(IndexOfName(Name), Value);
end;

procedure TVonConnects.SetNames(Index: Integer; const Value: string);
begin
  if (Index < 0) or (Index >= FCount) then
    Exit;
  FNameList[Index] := Value;
end;

procedure TVonConnects.SetNameValue(Name: string; const Value: string);
begin
  SetNames(IndexOfName(Name), Value);
end;

procedure TVonConnects.SetText(const Value: string);
var
  p, Q: PChar;
  szName, szValue: string;
begin
  Clear;
  p := PChar(Value);
  szName := '';
  szValue := '';
  while p[0] <> #0 do
  begin
    case p[0] of
      '=':
        begin
          if szName <> '' then
            szValue := szValue + '='
          else
          begin
            szName := szValue;
            szValue := '';
          end;
        end;
      #13, #10:
        begin
          if szName <> '' then
            Save(szName, szValue, nil);
          szName := '';
          szValue := '';
        end;
    else
      szValue := szValue + p[0];
    end;
    Inc(p);
  end;
  if szValue <> '' then
    Save(szName, szValue, nil);
end;

procedure TVonConnects.SetValues(Index: Integer; const Value: string);
begin

end;

initialization

CoInitialize(nil); // 手动调用 CoInitialize()
IdFreeze := TIdAntiFreeze.Create(nil);

finalization

IdFreeze.Free;
CoUninitialize; // 释放内存

end.
