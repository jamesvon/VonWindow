unit UCommunicationRes;

interface

resourcestring
  RES_COMM_ConnectionInfo = 'The %s is connected on %d';
  // The <IP> is connected on <Port>
  RES_COMM_DisconnectionInfo = 'The %s is disconnected on %d';
  // The <IP> is disconnected on <Port>
  RES_COMM_ConnectToInfo = 'Begin to connect server %s on %d';
  // Begin to connect server <IP> on <Port>
  RES_COMM_DisconnectToInfo = 'Disconnect a server %s on %d';
  // Disconnect a server <IP> on <Port>
  RES_COMM_ReceiveInfo = 'Receive a %s command of %s';
  // Receive a <commandName> command of <ApplicationName>
  RES_COMM_MessageInfo = 'Received %d bytes from %s on %d';
  // Received <number> bytes from <IP> on <Port>
  RES_COMM_MaxErrorInfo = 'Received %d bytes, very large.';

implementation

end.
