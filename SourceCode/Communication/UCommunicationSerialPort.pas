unit UCommunicationSerialPort;

interface

uses Types, SysUtils, Classes, Windows, ExtCtrls, Contnrs, UVonLog, UVonSystemFuns;

resourcestring
  RES_InvalidRing = '无效脚环';
  NOJUNZHUORING = '非荣冠信鸽脚环';

const
  MAXBUFFERSIZE = 1024;
  TIMEINTERVALUE = 200;

type
  IComm = interface
    ['{CC998405-37A9-4FBD-AB18-B1AD2B276DDA}']
    // 打开串口 APort:端口号 ABaudRate:波特率 AParity:校验位 AByteSize:数据位 AStopBits:停止位
    // 例：Open('COM1',CBR_9600,'n',8,1)
    // 波特率：110 300 600 1200 2400 4800 14400 19200 38400 56000 57600 115200 128000 256000
    // 校验位: 'n'=no,'o'=odd,'e'=even,'m'=mark,'s'=space
    // 数据位： 4-8 (other=8)
    // 停止位： 1-2 (other=1.5)  //*)
    function Open(APort: word; ABaudRate: longint; AParity: char;
      AByteSize, AStopBits: byte): Boolean;
    // 写串口 buf:存放要写的数据，len:要写的长度
    procedure Send(var buf; len: DWord);
    // 读串口 buf:存放读到的数据，len: 存储空间大小，tmout:超时值(ms) 返回: 实际读到的字符数
    function Receive(var buf; len: DWord; tmout: DWord): integer;
    // 关闭串口
    procedure Close;
    // private
    function GetOpened: Boolean;
    // published
    // Comm是否打开
    property Opened: Boolean read GetOpened;
  end;

  TUart = class(TObject)
  private
    OpenFlag: Boolean; // 打开标志
    handle: THandle; // 串口句柄
  public
    constructor Create; virtual;
    destructor Destroy; override;
    // 打开串口，例：Open('COM1',CBR_9600,'n',8,1)
    function Open(port: string; bps: longint; par: char;
      dbit, sbit: byte): Boolean; overload;
    function Open(port: integer; bps: longint; par: char;
      dbit, sbit: byte): Boolean; overload;
    // 关闭串口
    procedure Close;
    // 返回输入缓冲区中的字符数
    function InbufChars: DWord;
    // 返回输出缓冲区中的字符数
    function OutBufChars: longint;
    // 写串口 buf:存放要写的数据，len:要写的长度
    procedure Send(var buf; len: DWord);
    // 读串口 buf:存放读到的数据，len:要读的长度，tmout:超时值(ms)
    // 返回: 实际读到的字符数
    function Receive(var buf; len: DWord; tmout: integer): integer;
    // 向串口发一个字符
    procedure PutChar(ch: char);
    // 向串口发一个字符串
    procedure Puts(s: string);
    // 从串口读一个字符，未收到字符则返回#0
    function GetChar: char;
    // 从串口取一个字符串，忽略不可见字符,收到#13或255个字符结束，tmout:超时值(ms)
    // 返回:true表示收到完整字符串，false表示超时退出
    function Gets(var s: string; tmout: integer): Boolean;
    // 清接收缓冲区
    procedure ClearInBuf;
    // 等待一个字符串,tmout:超时值(ms),返回false表示超时
    function Wait(s: String; tmout: integer): Boolean;
    // 执行一个AT命令，返回true表示成功
    function ExecAtComm(s: string): Boolean;
    // 挂机，返回true表示成功
    function Hangup: Boolean;
    // 应答，返回true表示成功
    function Answer: Boolean;

    property Opened: Boolean read OpenFlag;
  end;

  TCommFile = class(TInterfacedObject, IComm)
  public
    constructor Create; overload;
    destructor Destory; overload;
    function Open(APort: word; ABaudRate: longint; AParity: char;
      AByteSize, AStopBits: byte): Boolean;
    procedure Send(var buf; len: DWord); // 写串口 buf:存放要写的数据，len:要写的长度
    function Receive(var buf; len: DWord; tmout: DWord): integer;
    // 读串口 buf:存放读到的数据，tmout:超时值(ms) 返回: 实际读到的字符数
    procedure Close; // 关闭串口
  private
    FUart: TUart;
    function GetOpened: Boolean;
  published
    property Opened: Boolean read GetOpened; // Comm是否打开
  end;

  TJunzhuoPigeonInfo = class
  private
    FElectronicCode: string; // 电子环号
    function GetECode: string;
    procedure SetElectronicCode(const Value: string);
  public
    GBCode: string; // 统一环号
    Association: string; // 协会区号
    GameNo: string; // 赛事编号
    RingGameNo: integer; // 赛事环号(针对赛事所启用的赛事内唯一环号)
    PhoneNo: string; // 电话号码(赛事对应的信息台号码)
    FlyTime: integer; // 回鸽时间
    LagValue: integer; // 回鸽延时
    Status: integer; // 记录状态(0:集鸽 1:回鸽 2:已上传)
    FactoryCode: string;
  published
    property ElectronicCode: string read FElectronicCode
      write SetElectronicCode; // 电子环号
    property ECode: string read GetECode;
  end;

  TCommStatusEmnu = (csAuto, csAutoReceive, csAutoSend, csManual, csStop);

  TComm2012 = class
  private
    FCOMM: IComm;
    FTimer: TTimer;
    FOpened: Boolean;
    FParity: char;
    FTimerOut: integer;
    FBaudRate: longint;
    FType: string;
    FStatus: TCommStatusEmnu;
    FOnRecevered: TNotifyEvent;
    FByteSize: word;
    FStopBits: word;
    FPort: word;
    FSendCmdID, FReceiveCmdID: integer;
    FRecords: TObjectList;
    FMatchineNo: Cardinal;
    FMatchineTime: TDateTime;
    FSucessed: Boolean;
    FCollectionCount: integer;
    FReceivedCount, FSendCount: integer;
    FCurrentRecordIdx: integer;
    FTempStr: string;
    FLastCode: string;
    FCheckCode: Boolean;
    procedure EventToReceive(Sender: TObject);
    procedure DoSendEvent(EventID: integer);
    function GetLoopTime: integer;
    function GetMatchineTime: TDateTime;
    procedure SetBaudRate(const Value: longint);
    procedure SetByteSize(const Value: word);
    procedure SetCommunicationType(const Value: string);
    procedure SetOnRecevered(const Value: TNotifyEvent);
    procedure SetOpened(const Value: Boolean);
    procedure SetParity(const Value: char);
    procedure SetPort(const Value: word);
    procedure SetStatus(const Value: TCommStatusEmnu);
    procedure SetStopBits(const Value: word);
    procedure SetTimerOut(const Value: integer);
    function GetInfoCount: integer;
    function GetInfos: TObjectList;
    procedure SetCurrentRecordIdx(const Value: integer);
    function GetInfo(Index: integer): TJunzhuoPigeonInfo;
    procedure BeginRead(EventID, ReceiveID, Timer: integer;
      LoopKind: TCommStatusEmnu);
    function GetOpened: Boolean;
    procedure SetCheckCode(const Value: Boolean);
    procedure WriteBuff(const Buff: array of byte; const BuffCount: integer);
  public
    constructor Create(AType: string);
    destructor Destroy; override;
    function Open(APort, ABaudRate: longint; AParity: char;
      AByteSize, AStopBits: byte): Boolean;
    procedure Close;
    procedure DeleteInfo(Index: integer);
    procedure InitMatchine; // 1读取主机状态和信息并同步时钟
    procedure GetMatchine; // 2读取时钟和记录指针（测试用）
    procedure DeleteRecord(ElectronicCode: string); // 3清除主机回鸽、集鸽记录
    procedure ReadRecord(LoopKind: TCommStatusEmnu = csAutoSend); // 4读取回鸽、集鸽记录
    procedure WriteCollection(AreaID, YearID, GBCode: integer; // 5写入集鸽数据
      ECode: string; AssID, GameID, RndNum: integer; PhoneNo: string);
    procedure ClearMemory; // 6清除主机内存
    procedure ReadCode(LoopKind: TCommStatusEmnu); // 7读取集鸽环号
    procedure WriteGameCode(HighCode, LowerCode: Cardinal;
      LoopKind: TCommStatusEmnu); // 8写环：
    property Info[Index: integer]: TJunzhuoPigeonInfo read GetInfo;
  published
    // Communication
    property port: word read FPort write SetPort; // 设置通讯口 1=COM1
    property BaudRate: longint read FBaudRate write SetBaudRate; // 设置波特率 9600
    property Parity: char read FParity write SetParity;
    // 奇偶校验位 0-4分别表示无、奇、偶、传号、空号校验
    property ByteSize: word read FByteSize write SetByteSize; // 字符位数   8
    property StopBits: word read FStopBits write SetStopBits; // 设置停止位 1
    property Opened: Boolean read GetOpened write SetOpened; // Comm是否打开
    property TimerOut: integer read FTimerOut write SetTimerOut; // 超时值(ms)
    property CommunicationType: string read FType write SetCommunicationType;
    // 通讯选取的方式名称
    property MatchineNo: Cardinal read FMatchineNo; // 设备编码
    property MatchineTime: TDateTime read GetMatchineTime; // 读取当前设备的时间
    property CollectionCount: integer read FCollectionCount; // 集鸽数量
    property CurrentRecordIdx: integer read FCurrentRecordIdx
      write SetCurrentRecordIdx; // 当前记录位置
    property InfoCount: integer read GetInfoCount;
    property Infos: TObjectList read GetInfos;
    property Sucessed: Boolean read FSucessed; // 发送是否成功标志
    property Status: TCommStatusEmnu read FStatus write SetStatus; // 系统扫描状态
    property LoopTime: integer read GetLoopTime; // 时间扫面间隔
    property CheckCode: Boolean read FCheckCode write SetCheckCode;
    property OnRecevered: TNotifyEvent read FOnRecevered write SetOnRecevered;
    // 接收成功相应事件
  end;

implementation

uses Math, DateUtils;

function BCDToInt(BCD: byte): word; // 将BCD码转换为整数
begin
  Result := (BCD shr 4) * 10 + (BCD and $0F)
end;

{$H+}

const
  CommInQueSize = 4096; // 输入缓冲区大小
  CommOutQueSize = 4096; // 输出缓冲区大小

  (* **********************      计时器        ********************** *)

type
  Timers = Class
  private
    StartTime: TTimeStamp;
  public
    constructor Create;
    Procedure Start; // 开始计时
    Function Get: longint; // 返回计时值(单位:ms)
  end;

constructor Timers.Create;
begin // 构造函数
  inherited Create;
  Start;
end;

procedure Timers.Start;
begin // 开始计时
  StartTime := DateTimeToTimeStamp(Now);
end;

function Timers.Get: longint;
var // 返回计时值(单位:ms)
  CurTime: TTimeStamp;
begin
  CurTime := DateTimeToTimeStamp(Now);
  Result := (CurTime.Date - StartTime.Date) * 24 * 3600000 +
    (CurTime.Time - StartTime.Time);
end;

(* **********************      TUart     ********************** *)

constructor TUart.Create;
begin // 构造函数
  inherited Create;
  OpenFlag := False;
end;

Destructor TUart.Destroy;
begin // 析构函数
  Close;
  inherited Destroy;
end;

(* 例：Open('COM1',CBR_9600,'n',8,1)
  波特率：
  CBR_110     CBR_19200
  CBR_300     CBR_38400
  CBR_600     CBR_56000
  CBR_1200    CBR_57600
  CBR_2400    CBR_115200
  CBR_4800    CBR_128000
  CBR_14400   CBR_256000
  校验位:
  'n'=no,'o'=odd,'e'=even,'m'=mark,'s'=space
  数据位： 4-8 (other=8)
  停止位： 1-2 (other=1.5)  // *)
function TUart.Open(port: integer; bps: longint; par: char;
  dbit, sbit: byte): Boolean;
begin
  Result:= Open('COM' + IntToStr(port), bps, par, dbit, sbit);
end;

function TUart.Open(port: string; bps: Integer; par: char; dbit,
  sbit: byte): Boolean;
var // 打开串口
  dcb: TDCB;
begin
  Result := False;
  OpenFlag := False;
  // 初始化串口
  handle := CreateFile(PChar(port),
    GENERIC_READ + GENERIC_WRITE, 0, nil, OPEN_EXISTING, 0, 0);
  if handle = INVALID_HANDLE_VALUE then
    Exit;
  GetCommState(handle, dcb);
  with dcb do
  begin
    BaudRate := bps; // 波特率
    if dbit in [4, 5, 6, 7, 8] then
      ByteSize := dbit // 数据位
    else
      ByteSize := 8;
    case sbit of // 停止位
      1:
        StopBits := 0; // 1
      2:
        StopBits := 2; // 2
    else
      StopBits := 0; // 1
    end;
    case par of // 校验
      'n', 'N':
        Parity := 0; // no
      'o', 'O':
        Parity := 1; // odd
      'e', 'E':
        Parity := 2; // even
      'm', 'M':
        Parity := 3; // mark
      's', 'S':
        Parity := 4; // space
    else
      Parity := 0; // no
    end;
  end;
  SetCommState(handle, dcb);
  SetupComm(handle, CommOutQueSize, CommInQueSize);
  OpenFlag := True;
  Result := True;
end;

// 关闭串口
procedure TUart.Close;
begin
  if OpenFlag then
  begin
    CloseHandle(handle);
    handle := 0;
  end;
  OpenFlag := False;
end;

// 检测输入缓冲区中的字符数
function TUart.InbufChars: DWord;
var
  ErrCode: DWord;
  Stat: TCOMSTAT;
begin
  Result := 0;
  if not OpenFlag then
    Exit;
  ClearCommError(handle, ErrCode, @Stat);
  Result := Stat.cbInQue;
end;

// 检测输出缓冲区中的字符数
function TUart.OutBufChars: longint;
var
  ErrCode: DWord;
  Stat: TCOMSTAT;
begin
  Result := 0;
  if not OpenFlag then
    Exit;
  ClearCommError(handle, ErrCode, @Stat);
  Result := Stat.cbOutQue;
end;

// 写串口 buf:存放要写的数据，len:要写的长度
procedure TUart.Send(var buf; len: DWord);
var
  i: DWord;
begin
  WriteFile(handle, buf, len, i, nil); // 写串口
end;

// 读串口 buf:存放读到的数据，len:要读的长度，tmout:超时值(ms)
// 返回: 实际读到的字符数
function TUart.Receive(var buf; len: DWord; tmout: integer): integer;
var
  Timer: Timers;
  i: DWord;
  BufChs: DWord;
begin
  Timer := Timers.Create;
  Timer.Start;
  repeat
  until (InbufChars >= len) or (Timer.Get > tmout); // 收到指定长度数据或超时
  BufChs := InbufChars;
  if len > BufChs then
    len := BufChs;
  ReadFile(handle, buf, len, i, nil); // 读串口
  Result := i;
  Timer.free;
end;

// 向串口发一个字符
procedure TUart.PutChar(ch: char);
var
  i: DWord;
begin
  i:= 1;
  WriteFile(handle, ch, 1, i, nil); // 写串口
end;

// 向串口发一个字符串
procedure TUart.Puts(s: string);
var
  i: integer;
begin
  for i := 1 to length(s) do
    PutChar(s[i]);
end;

// 从串口读一个字符，未收到字符则返回#0
function TUart.GetChar: char;
var
  i: DWord;
begin
  Result := #0;
  if InbufChars > 0 then
    ReadFile(handle, Result, 1, i, nil); // 读串口
end;

// 从串口取一个字符串，忽略不可见字符,收到#13或255个字符结束，tmout:超时值(ms)
// 返回:true表示收到完整字符串，false表示超时退出
function TUart.Gets(var s: string; tmout: integer): Boolean;
var
  Timer: Timers;
  ch: char;
begin
  Timer := Timers.Create;
  Timer.Start;
  s := '';
  Result := False;
  repeat
    ch := GetChar;
    if ch <> #0 then
      Timer.Start; // 如收到字符则清定时器
    if ch >= #32 then
      s := s + ch;
    if (ch = #13) or (length(s) >= 255) then
    begin
      Result := True;
      break;
    end;
  until Timer.Get > tmout; // 超时
  Timer.free;
end;

// 清接收缓冲区
procedure TUart.ClearInBuf;
begin
  if not OpenFlag then
    Exit;
  PurgeComm(handle, PURGE_RXCLEAR);
end;

// 等待一个字符串,tmout:超时值(ms),返回false表示超时
function TUart.Wait(s: String; tmout: integer): Boolean;
var
  s1: string;
  Timer: Timers;
begin
  Timer := Timers.Create;
  Timer.Start;
  Result := False;
  repeat
    Gets(s1, tmout);
    if pos(s, s1) > 0 then
    begin
      Result := True;
      break
    end;
  until Timer.Get > tmout;
  Timer.free;
end;

// 执行一个AT命令，返回true表示成功
function TUart.ExecAtComm(s: string): Boolean;
begin
  ClearInBuf;
  Puts(s);
  Result := False;
  if Wait('OK', 3000) then
    Result := True;
end;

// 挂机，返回true表示成功
function TUart.Hangup: Boolean;
begin
  Result := False;
  ExecAtComm('+++');
  if not ExecAtComm('ATH'#13) then
    Exit;
  Result := True;
end;

// 应答，返回true表示成功
function TUart.Answer: Boolean;
begin
  ClearInBuf;
  Puts('ATA'#13);
  Result := False;
  if Wait('CONNECT', 30000) then
    Result := True;
end;

{ TCommFile }

procedure TCommFile.Close;
begin
  FUart.Close;
end;

constructor TCommFile.Create;
begin
  FUart := TUart.Create;
end;

destructor TCommFile.Destory;
begin
  FUart.free;
end;

function TCommFile.GetOpened: Boolean;
begin
  Result := FUart.OpenFlag;
end;

function TCommFile.Open(APort: word; ABaudRate: integer; AParity: char;
  AByteSize, AStopBits: byte): Boolean;
begin
  Result := FUart.Open(APort, ABaudRate, AParity, AByteSize, AStopBits);
end;

function TCommFile.Receive(var buf; len, tmout: DWord): integer;
begin
  Result := FUart.Receive(buf, len, tmout);
end;

procedure TCommFile.Send(var buf; len: DWord);
begin
  FUart.Send(buf, len);
end;

{ TJunzhuoComm }

procedure TComm2012.BeginRead(EventID, ReceiveID, Timer: integer;
  LoopKind: TCommStatusEmnu);
begin // 开始自动执行任务
  FSendCmdID := EventID;
  DoSendEvent(EventID);
  FReceivedCount := 0;
  FSendCount := 1;
  FReceiveCmdID := ReceiveID;
  FStatus := LoopKind;
  FTimer.Interval := Timer;
  FTimer.OnTimer := EventToReceive;
  FTimer.Enabled := True;
end;

procedure TComm2012.ClearMemory;
begin // 6清除主机内存
  BeginRead(6, 6, 1000, csAutoReceive);
end;

procedure TComm2012.Close;
begin
  FTimer.Enabled := False;
  FCOMM.Close;
end;

constructor TComm2012.Create(AType: string);
begin // 初始化
  FLastCode := '';
  if AType = 'FILE' then
    FCOMM := TCommFile.Create
  else
    raise Exception.Create('Not found the ' + AType +
      ' type of communication.');
  FRecords := TObjectList.Create;
  FTimer := TTimer.Create(nil);
  FTimer.Enabled := False;
end;

procedure TComm2012.DeleteInfo(Index: integer);
begin
  FRecords.Delete(Index);
end;

procedure TComm2012.DeleteRecord(ElectronicCode: string);
begin // 3清除主机回鸽、集鸽记录：计算机发送80H，02H，电子环号（5）
  FTempStr := ElectronicCode;
  BeginRead(3, 3, 1000, csAutoReceive);
end;

destructor TComm2012.Destroy;
begin // 析构函数
  FTimer.Enabled := False;
  FTimer.free;
  FRecords.free;
  inherited;
end;

procedure TComm2012.DoSendEvent(EventID: integer);
var
  Buff: array [0 .. 255] of byte;

  procedure SendBuff1();
  var
    AYear, AMonth, ADay, AHour, AMinute, ASecond, AMilliSecond: word;
  begin // 读取主机状态和信息并同步时钟:计算机发送80H，00H，年（1），月（1），日（1），时（1），分（1），秒（1）
    Buff[0] := $80;
    Buff[1] := 0;
    DecodeDateTime(Now, AYear, AMonth, ADay, AHour, AMinute, ASecond,
      AMilliSecond);
    Buff[2] := AYear mod 100;
    Buff[3] := AMonth;
    Buff[4] := ADay;
    Buff[5] := AHour;
    Buff[6] := AMinute;
    Buff[7] := ASecond;
    FCOMM.Send(Buff, 8);
  end;

  procedure SendBuff2();
  begin // 读取时钟和记录指针（测试用）：计算机发送80H，01H
    Buff[0] := $80;
    Buff[1] := 1;
    FCOMM.Send(Buff, 2);
  end;

  procedure SendBuff3();
  begin // 清除主机回鸽、集鸽记录：计算机发送80H，02H，电子环号（5）
    Buff[0] := $80;
    Buff[1] := 2;
    Buff[2] := HexToInt(FTempStr[1] + FTempStr[2]);
    Buff[3] := HexToInt(FTempStr[3] + FTempStr[4]);
    Buff[4] := HexToInt(FTempStr[5] + FTempStr[6]);
    Buff[5] := HexToInt(FTempStr[7] + FTempStr[8]);
    Buff[6] := HexToInt(FTempStr[9] + FTempStr[10]);
    FCOMM.Send(Buff, 7);
  end;

  procedure SendBuff4();
  begin // 4读取回鸽、集鸽记录：计算机发送80H，03H，记录序号（2）（0-511）
    Buff[0] := $80;
    Buff[1] := 3;
    Buff[2] := FCurrentRecordIdx div 256;
    Buff[3] := FCurrentRecordIdx mod 256;
    FCOMM.Send(Buff, 4);
  end;

  procedure SendBuff5();
  var
    D: Cardinal;
  begin // 5写入集鸽数据,计算机发送80H，04H，5个字节统一环号，5个字节电子环号，
    // 1个字节协会区号，1个字节赛事编号，3个字节脚环随机码，12个字节信息台号码（电话号码为字符串）
    // Format('%0.2d   %0.2d   %0.6d   %s    %0.3d   %0.3d   %0.8d   %12S',
    // [AreaID,  YearID, GBCode, ECode, AssID, GameID, RndNum, PhoneNo]);
    // Example: '01091234567E986912001423100054321010123456789'
    Buff[0] := $80;
    Buff[1] := 4;
    Buff[2] := StrToInt(FTempStr[1] + FTempStr[2]); // 1统一环号区号 %0.2d   2-2  1-2
    Buff[3] := StrToInt(FTempStr[3] + FTempStr[4]); // 1统一环号年号 %0.2d   3-3  3-4
    D := StrToInt(Copy(FTempStr, 5, 6)); // 3统一环号号码 %0.6d   4-6  5-10
    Buff[4] := D div 65536;
    Buff[5] := D div 256;
    Buff[6] := D;
    Buff[7] := HexToInt(FTempStr[11] + FTempStr[12]);
    // 5个字节电子环号 %10s  7-11 11-20
    Buff[8] := HexToInt(FTempStr[13] + FTempStr[14]);
    Buff[9] := HexToInt(FTempStr[15] + FTempStr[16]);
    Buff[10] := HexToInt(FTempStr[17] + FTempStr[18]);
    Buff[11] := HexToInt(FTempStr[19] + FTempStr[20]);
    Buff[12] := StrToInt(Copy(FTempStr, 21, 3));
    // 1个字节节协会区号 %0.3d   12-12 21-23
    Buff[13] := StrToInt(Copy(FTempStr, 24, 3));
    // 1个字节赛事编号%0.3d      13-13 24-26
    D := StrToInt(Copy(FTempStr, 27, 8)); // 3个字节脚环随机码%0.8d    14-16 27-34
    Buff[14] := D div 65536;
    Buff[15] := D div 256;
    Buff[16] := D;
    Buff[17] := Ord(FTempStr[35]);
    Buff[18] := Ord(FTempStr[36]); // 12个字节信息台号码（电话号码为字符串）  17-28 35-46
    Buff[19] := Ord(FTempStr[37]);
    Buff[20] := Ord(FTempStr[38]);
    Buff[21] := Ord(FTempStr[39]);
    Buff[22] := Ord(FTempStr[40]);
    Buff[23] := Ord(FTempStr[41]);
    Buff[24] := Ord(FTempStr[42]);
    Buff[25] := Ord(FTempStr[43]);
    Buff[26] := Ord(FTempStr[44]);
    Buff[27] := Ord(FTempStr[45]);
    Buff[28] := Ord(FTempStr[46]);
    FCOMM.Send(Buff, 29);
  end;

  procedure SendBuff6();
  begin // 6清除主机内存,计算机发送80H，05H
    Buff[0] := $80;
    Buff[1] := 5;
    FCOMM.Send(Buff, 2);
  end;

  procedure SendBuff7();
  begin // 7读取集鸽环号,计算机发送7EH，00H
    Buff[0] := $7E;
    Buff[1] := 0;
    FCOMM.Send(Buff, 2);
  end;

  procedure SendBuff8();
  begin // 8写入赛事环号：发送7DH，5字节环号
    Buff[0] := $7D;
    Buff[1] := 0;
    FCOMM.Send(Buff, 2);
  end;

begin
  (*
    1读取主机状态和信息并同步时钟：
    计算机发送80H，00H，年（1），月（1），日（1），时（1），分（1），秒（1）
    主机返回：主机编号（3），集鸽数量（2），年（1），月（1），日（1），时（1），分（1），秒（1）

    2读取时钟和记录指针（测试用）：
    计算机发送80H，01H
    主机返回：非比赛记录指针（2），年（1），月（1），日（1），时（1），分（1），秒（1）

    3清除主机回鸽、集鸽记录：
    计算机发送80H，02H，电子环号（5）
    主机返回：01H

    4读取回鸽、集鸽记录：
    计算机发送80H，03H，记录序号（2）（0-511）
    主机返回：1个字节标志（0表示集鸽，1表示回鸽，2为已上传，FFH为空记录），5个字节统一环号，5个字节电子环号， 1个字节协会区号，1个字节赛事编号，3个字节脚环随机码，3个字节回鸽时间单位为秒，1个字节回鸽毫秒值单位为10毫秒（0-99），12字节电话号码

    5写入集鸽数据：
    计算机发送80H，04H，5个字节统一环号，5个字节电子环号， 1个字节协会区号，1个字节赛事编号，3个字节脚环随机码，12个字节信息台号码（电话号码为字符串）
    主机返回：01H

    6清除主机内存：
    计算机发送80H，05H
    主机返回：01H

    7读取集鸽环号：
    计算机发送7EH，00H
    主机返回：00表示无扫描环号，
    02H表示有集鸽环号，随机数（5），毫秒值（3），加密电子环号（5）
    8写环：
    发送7DH，5字节环号
    返回00表示正确
    返回01表示错误

    通讯协议与集鸽踏板相同
  *)
  FillChar(Buff, 255, 0);
  case EventID of
    1:
      SendBuff1(); // 读取主机状态和信息并同步时钟：
    2:
      SendBuff2(); // 读取时钟和记录指针（测试用）：
    3:
      SendBuff3(); // 清除主机回鸽、集鸽记录
    4:
      SendBuff4(); // 读取回鸽、集鸽记录：
    5:
      SendBuff5(); // 写入集鸽数据
    6:
      SendBuff6(); // 清除主机内存
    7:
      SendBuff7(); // 读取集鸽环号
    8:
      SendBuff8(); // 写入赛事环号
  end;
end;

procedure TComm2012.EventToReceive(Sender: TObject);
var // 接收相应事件
  Buff: array [0 .. 255] of byte;
  szBf: array [0 .. 4] of byte;
  i, j, szSize: integer;
  szInfo: TJunzhuoPigeonInfo;
  szCode: string;
  CrcCode: byte;

  function ReceiveMsg(): integer;
  begin
    FillChar(Buff, 255, 0);
    Result := FCOMM.Receive(Buff, 255, 100);
    // WriteBuff(buff, Result);
  end;

  procedure DoUserEvent(DoNext, DoSend: Boolean);
  begin
    if Assigned(FOnRecevered) then
      FOnRecevered(Self);
    if DoSend then
    begin
      DoSendEvent(FSendCmdID);
      FReceivedCount := 0;
    end;
    FTimer.Enabled := DoNext;
  end;

  procedure EndOfProcesser(ASuccessed: Boolean);
  begin
    FSucessed := ASuccessed;
    if FSucessed then
    begin // 接收成功
      case FStatus of
        csAutoReceive:
          DoUserEvent(False, False); // 自动接收->回调->结束
        csAutoSend:
          DoUserEvent(True, True); // 自动发送->回调->发送->自动发送
        csManual:
          DoUserEvent(False, False); // 手动->回调->结束
        csAuto:
          DoUserEvent(True, True); // 全自动->回调->发送
      end;
      if not FTimer.Enabled then
        FStatus := csStop;
    end
    else
    begin
      if FReceivedCount > 3 then
      begin // 连续三次接收失败
        case FStatus of
          csAutoReceive:
            DoUserEvent(False, False); // 自动接收->回调->结束
          csAutoSend:
            if FSendCount < 3 then
            begin // 自动发送->连续三次发送
              DoSendEvent(FSendCmdID);
              Inc(FSendCount);
            end
            else
              DoUserEvent(False, False); // 连续三次发送失败->回调->结束
          csManual:
            DoUserEvent(False, False); // 手动->回调->结束
          csAuto: // 全自动->连续三次发送
            if FSendCount < 3 then
            begin
              DoSendEvent(FSendCmdID);
              Inc(FSendCount);
            end
            else
            begin
              DoSendEvent(FSendCmdID);
              FSendCount := 0;
              FReceivedCount := 0; // 全自动->连续三次发送失败->异常
              // raise Exception.Create('通讯出现异常');
            end;
        end;
      end;
    end;
  end;

begin // 自动接收事件
  // 接收主机状态数据
  (* 1: SendBuff1();
    2: SendBuff2();
    3: SendBuff3();   //清除主机回鸽、集鸽记录
    4: SendBuff4();   ：
    5: SendBuff5();   //写入集鸽数据
    6: SendBuff6();   //清除主机内存
    7: SendBuff7();   //读取集鸽环号
  *)
  Inc(FReceivedCount);
  case FReceiveCmdID of
    1:
      if ReceiveMsg() >= 11 then
      begin // 读取主机状态和信息并同步时钟：
        FMatchineNo := Buff[0] * 65536 + Buff[1] * 256 + Buff[2];
        FCollectionCount := Buff[3] * 256 + Buff[4];
        FMatchineTime := EncodeDateTime(2000 + Buff[5], Buff[6], Buff[7],
          Buff[8], Buff[9], Buff[10], 0);
        EndOfProcesser(True);
      end
      else
        EndOfProcesser(False);
    2:
      if ReceiveMsg() >= 9 then
      begin // 读取时钟和记录指针（测试用）：
        FMatchineNo := Buff[0] * 65536 + Buff[1] * 256 + Buff[2];
        FMatchineTime := EncodeDateTime(2000 + Buff[3], Buff[4], Buff[5],
          Buff[6], Buff[7], Buff[8], 0);
        EndOfProcesser(True);
      end
      else
        EndOfProcesser(False);
    3, // -------- 接收写入集鸽数据应答 ---------
    5, // -------- 接收删除集鸽数据应答 ---------
    6: // -------- 接收清除数据应答     ---------
      // 主机返回：01H
      if (ReceiveMsg() > 0) and (Buff[0] = 1) then
        EndOfProcesser(True)
      else
        EndOfProcesser(False);
    4: // -------- 接收读取集鸽数据 ----------
      // 主机返回：1个字节标志（0表示集鸽，1表示回鸽，2为已上传，FFH为空记录），
      // 5个字节统一环号，
      // 5个字节电子环号，
      // 1个字节协会区号，
      // 1个字节赛事编号，
      // 3个字节脚环随机码，
      // 3个字节回鸽时间单位为秒，
      // 1个字节回鸽毫秒值单位为10毫秒（0-99），12字节电话号码
      // --------------------------------------------------------------------------
      // 应答：		00H表示踏板通讯正常，没有找到脚环
      // 01H表示有脚环，5个字节环号，3个字节延时
      // 02H表示有脚环，5个字节随机码，3个字节延时,5个字节加密环号
      if (ReceiveMsg() >= 20) and (Buff[0] < 3) then
      begin
        szCode := IntToHex(Buff[6], 2) + IntToHex(Buff[7], 2) +
          IntToHex(Buff[8], 2) + IntToHex(Buff[9], 2) + IntToHex(Buff[10], 2);
        // 电子环号
        WriteLog(LOG_DEBUG, 'Receiver ECode', FLastCode + ' ++ ' + szCode + ' '
          + Copy(szCode, 7, 4) + ' ' + Copy(szCode, 8, 3) + ' ' + Copy(szCode,
          9, 2) + ' ' + Copy(szCode, 10, 1));
        if (FLastCode <> '') and (Copy(FLastCode, 1, 6) = Copy(szCode, 1, 6))
          and (Copy(szCode, 7, 4) = '0000') then
        begin
          EndOfProcesser(False);
          Exit;
        end;
        if (FLastCode <> '') and (Copy(FLastCode, 1, 7) = Copy(szCode, 1, 7))
          and (Copy(szCode, 8, 3) = '000') then
        begin
          EndOfProcesser(False);
          Exit;
        end;
        if (FLastCode <> '') and (Copy(FLastCode, 1, 8) = Copy(szCode, 1, 8))
          and (Copy(szCode, 9, 2) = '00') then
        begin
          EndOfProcesser(False);
          Exit;
        end;
        if (FLastCode <> '') and (Copy(FLastCode, 1, 9) = Copy(szCode, 1, 9))
          and (Copy(szCode, 10, 1) = '0') then
        begin
          EndOfProcesser(False);
          Exit;
        end;
        szInfo := TJunzhuoPigeonInfo.Create;
        szInfo.Status := Buff[0]; // 记录状态(0:集鸽 1:回鸽 2:已上传)
        szInfo.GBCode := IntToHex(Buff[1], 2) + IntToHex(Buff[2], 2) + // 统一环号
          IntToHex(Buff[3], 2) + IntToHex(Buff[4], 2) + IntToHex(Buff[5], 2);
        szInfo.ElectronicCode := szCode; // 电子环号
        szInfo.Association := IntToHex(Buff[11], 2); // 区号(协会号)
        szInfo.GameNo := IntToHex(Buff[12], 2); // 赛事编号
        szInfo.RingGameNo := Buff[13] * 65536 + Buff[14] * 256 + Buff[15];
        // 随机码
        szInfo.FlyTime := Buff[16] * 65536 + Buff[17] * 256 + Buff[18]; // 回鸽时间
        szInfo.LagValue := Buff[19];
        szInfo.PhoneNo := char(Buff[20]) + char(Buff[21]) + char(Buff[22]) +
        // 字节电话号码
          char(Buff[23]) + char(Buff[24]) + char(Buff[25]) + char(Buff[26]) +
          char(Buff[27]) + char(Buff[28]) + char(Buff[29]) + char(Buff[29]) +
          char(Buff[30]);
        FRecords.Add(szInfo);
        FCurrentRecordIdx := (FCurrentRecordIdx + 1) mod 512;
        EndOfProcesser(True);
        FLastCode := szCode;
      end
      else
        EndOfProcesser(False);
    7: // ------  接收集鸽数据 -------
      // 主机返回：00表示无扫描环号，02H表示有集鸽环号  0
      // 随机数(5)                            1-5
      // 毫秒值(3)                            6-8
      // 加密电子环号(5)                      9-13
      // 校验(1)                              14
      // 03H结束标记                          15
      // 发送7EH，00H：返回00H表示无环
      // 返回03H，5字节环号，8字节芯片UID
      // 通讯协议与集鸽踏板相同
      begin
        szSize := ReceiveMsg(); // 读取回鸽、集鸽记录
        case szSize of
          14:
            begin
            end; // 不做校验（老踏板）
          16:
            begin // 做校验（新踏板2012年10月最后修改）
              CrcCode := 0;
              for j := 1 to 13 do
                CrcCode := CrcCode xor Buff[j];
              if (Buff[0] <> 2) or (Buff[15] <> 3) or (CrcCode <> Buff[14]) then
              begin
                WriteLog(LOG_FAIL, 'Received a message, and its CRC is error ',
                  IntToHex(Buff[0], 2) + ' ' + IntToHex(Buff[1], 2) + ' ' +
                  IntToHex(Buff[2], 2) + ' ' + IntToHex(Buff[3], 2) + ' ' +
                  IntToHex(Buff[4], 2) + ' ' + IntToHex(Buff[5], 2) + ' ' +
                  IntToHex(Buff[6], 2) + ' ' + IntToHex(Buff[7], 2) + ' ' +
                  IntToHex(Buff[8], 2) + ' ' + IntToHex(Buff[9], 2) + ' ' +
                  IntToHex(Buff[10], 2) + ' ' + IntToHex(Buff[11], 2) + ' ' +
                  IntToHex(Buff[12], 2) + ' ' + IntToHex(Buff[13], 2) + ' ' +
                  IntToHex(Buff[14], 2) + ' ' + IntToHex(Buff[15], 2));
                EndOfProcesser(False);
                Exit;
              end;
            end;
        else
          begin
            EndOfProcesser(False);
            Exit;
          end;
        end;
        if (Buff[0] = 2) then
        begin
          // 解密电子环号
          for j := 0 to 2 do
          begin
            szBf[0] := (Buff[13] and $F0) or (Buff[9] and $0F);
            szBf[1] := (Buff[10] and $F0) or (Buff[12] and $0F);
            szBf[2] := (Buff[12] and $F0) or (Buff[10] and $0F);
            szBf[3] := (Buff[11] and $F0) or (Buff[13] and $0F);
            szBf[4] := (Buff[9] and $F0) or (Buff[11] and $0F);
            for i := 0 to 4 do
              Buff[9 + i] := Buff[i + 1] xor szBf[i];
          end;
          for i := 0 to 4 do
            szBf[i] := Buff[9 + i];
          szCode := IntToHex(szBf[0], 2) + IntToHex(szBf[1], 2) +
            IntToHex(szBf[2], 2) + IntToHex(szBf[3], 2) + IntToHex(szBf[4], 2);
          if (FLastCode <> '') and (Copy(FLastCode, 1, 6) = Copy(szCode, 1, 6))
            and (Copy(szCode, 7, 4) = '0000') then
          begin
            EndOfProcesser(False);
            Exit;
          end;
          if (FLastCode <> '') and (Copy(FLastCode, 1, 7) = Copy(szCode, 1, 7))
            and (Copy(szCode, 8, 3) = '000') then
          begin
            EndOfProcesser(False);
            Exit;
          end;
          if (FLastCode <> '') and (Copy(FLastCode, 1, 8) = Copy(szCode, 1, 8))
            and (Copy(szCode, 9, 2) = '00') then
          begin
            EndOfProcesser(False);
            Exit;
          end;
          if (FLastCode <> '') and (Copy(FLastCode, 1, 9) = Copy(szCode, 1, 9))
            and (Copy(szCode, 10, 1) = '0') then
          begin
            EndOfProcesser(False);
            Exit;
          end;
          szInfo := TJunzhuoPigeonInfo.Create;
          szInfo.FlyTime := Buff[6] * 65536 + Buff[7] * 256 + Buff[8]; // 回鸽时间
          szInfo.ElectronicCode := szCode;
          FRecords.Add(szInfo);
          FLastCode := szCode;
          EndOfProcesser(True);
        end
        else if (Buff[0] = 3) then
        begin // 返回03H，5字节环号，8字节芯片UID
          WriteLog(LOG_FAIL, 'Received a new message, ',
            IntToHex(Buff[0], 2) + ' ' + IntToHex(Buff[1], 2) + ' ' +
            IntToHex(Buff[2], 2) + ' ' + IntToHex(Buff[3], 2) + ' ' +
            IntToHex(Buff[4], 2) + ' ' + IntToHex(Buff[5], 2) + ' ' +
            IntToHex(Buff[6], 2) + ' ' + IntToHex(Buff[7], 2) + ' ' +
            IntToHex(Buff[8], 2) + ' ' + IntToHex(Buff[9], 2) + ' ' +
            IntToHex(Buff[10], 2) + ' ' + IntToHex(Buff[11], 2) + ' ' +
            IntToHex(Buff[12], 2) + ' ' + IntToHex(Buff[13], 2));
          szCode := IntToHex(Buff[1], 2) + IntToHex(Buff[2], 2) +
            IntToHex(Buff[3], 2) + IntToHex(Buff[4], 2) + IntToHex(Buff[5], 2);
          if szCode = FLastCode then
          begin
            EndOfProcesser(True);
            Exit;
          end;
          szInfo := TJunzhuoPigeonInfo.Create;
          szInfo.ElectronicCode := szCode;
          szInfo.FactoryCode := IntToHex(Buff[6], 2) + IntToHex(Buff[7], 2) +
            IntToHex(Buff[8], 2) + IntToHex(Buff[9], 2) + IntToHex(Buff[10], 2)
            + IntToHex(Buff[11], 2) + IntToHex(Buff[12], 2) +
            IntToHex(Buff[13], 2);
          FRecords.Add(szInfo);
          WriteLog(LOG_FAIL, 'Add a info after receive msg',
            szInfo.ElectronicCode + ' ' + szInfo.ElectronicCode);
          FLastCode := szCode;
          EndOfProcesser(True);
        end;
      end;
  else
    FTimer.Enabled := False;
  end;
  // if FReceivedCount > 3 then EndOfProcesser(false);
end;

function TComm2012.GetInfo(Index: integer): TJunzhuoPigeonInfo;
begin
  if FRecords.Count > Index then
    Result := TJunzhuoPigeonInfo(FRecords[Index])
  else
    Result := nil;
end;

function TComm2012.GetInfoCount: integer;
begin
  Result := FRecords.Count;
end;

function TComm2012.GetInfos: TObjectList;
begin
  Result := FRecords;
end;

function TComm2012.GetLoopTime: integer;
begin
  Result := FTimer.Interval;
end;

procedure TComm2012.GetMatchine;
begin // 2读取时钟和记录指针（测试用）：
  BeginRead(2, 2, 1000, csAutoReceive);
end;

function TComm2012.GetMatchineTime: TDateTime;
begin
  Result := FMatchineTime;
end;

function TComm2012.GetOpened: Boolean;
begin
  Result := FCOMM.Opened;
end;

procedure TComm2012.InitMatchine;
begin // 1读取主机状态和信息并同步时钟
  BeginRead(1, 1, 1000, csAutoReceive);
end;

function TComm2012.Open(APort, ABaudRate: integer; AParity: char;
  AByteSize, AStopBits: byte): Boolean;
begin // 打开通讯
  if not Assigned(FCOMM) then
    Exit;
  if FCOMM.Opened then
  begin
    if (APort = FPort) and (ABaudRate = FBaudRate) and (AParity = FParity) and
      (AByteSize = FByteSize) and (AStopBits = FStopBits) then
      Exit
    else
      FCOMM.Close;
  end;
  Result := FCOMM.Open(APort, ABaudRate, AParity, AByteSize, AStopBits);
  FPort := APort;
  FBaudRate := ABaudRate;
  FParity := AParity;
  FByteSize := AByteSize;
  FStopBits := AStopBits;
  FCurrentRecordIdx := 0;
end;

procedure TComm2012.ReadCode(LoopKind: TCommStatusEmnu);
begin // 7读取集鸽环号
  FCurrentRecordIdx := 0;
  BeginRead(7, 7, TIMEINTERVALUE, LoopKind);
end;

procedure TComm2012.ReadRecord(LoopKind: TCommStatusEmnu = csAutoSend);
begin // 4读取回鸽、集鸽记录
  FCurrentRecordIdx := 0;
  BeginRead(4, 4, TIMEINTERVALUE, LoopKind);
end;

procedure TComm2012.SetBaudRate(const Value: longint);
begin
  FBaudRate := Value;
end;

procedure TComm2012.SetByteSize(const Value: word);
begin
  FByteSize := Value;
end;

procedure TComm2012.SetCheckCode(const Value: Boolean);
begin
  FCheckCode := Value;
end;

procedure TComm2012.SetCommunicationType(const Value: string);
begin
  FType := Value;
end;

procedure TComm2012.SetCurrentRecordIdx(const Value: integer);
begin
  FCurrentRecordIdx := Value;
end;

procedure TComm2012.SetOnRecevered(const Value: TNotifyEvent);
begin
  FOnRecevered := Value;
end;

procedure TComm2012.SetOpened(const Value: Boolean);
begin
  FOpened := Value;
end;

procedure TComm2012.SetParity(const Value: char);
begin
  FParity := Value;
end;

procedure TComm2012.SetPort(const Value: word);
begin
  FPort := Value;
end;

procedure TComm2012.SetStatus(const Value: TCommStatusEmnu);
begin
  FStatus := Value;
end;

procedure TComm2012.SetStopBits(const Value: word);
begin
  FStopBits := Value;
end;

procedure TComm2012.SetTimerOut(const Value: integer);
begin
  FTimerOut := Value;
end;

procedure TComm2012.WriteBuff(const Buff: array of byte;
  const BuffCount: integer);
var
  i: integer;
  s: string;
begin
  s := '';
  for i := 0 to BuffCount - 1 do
    s := s + IntToHex(Buff[i], 2) + ' ';
  WriteLog(LOG_INFO, 'WriteBuff', IntToStr(FReceiveCmdID) + ':' + s);
end;

procedure TComm2012.WriteCollection(AreaID, YearID, GBCode: integer;
  ECode: string; AssID, GameID, RndNum: integer; PhoneNo: string);
begin // 5写入集鸽数据
  FTempStr := Format('%0.2d%0.2d%0.6d%s%0.3d%0.3d%0.8d%12S',
    [AreaID, YearID, GBCode, ECode, AssID, GameID, RndNum, PhoneNo]);
  WriteLog(LOG_DEBUG, 'WriteCollection', FTempStr);
  BeginRead(5, 5, TIMEINTERVALUE, csManual);
end;

procedure TComm2012.WriteGameCode(HighCode, LowerCode: Cardinal;
  LoopKind: TCommStatusEmnu);
begin

end;

{ TJunzhuoPigeonInfo }

function TJunzhuoPigeonInfo.GetECode: string;
  function transCode(ch: char): char;
  begin
    case ch of
      '0':
        Result := 'D';
      '1':
        Result := 'F';
      '2':
        Result := '1';
      '3':
        Result := '3';
      '4':
        Result := '6';
      '5':
        Result := '8';
      '6':
        Result := '2';
      '7':
        Result := 'E';
      '8':
        Result := 'C';
      '9':
        Result := '0';
      'A':
        Result := '5';
      'B':
        Result := '9';
      'C':
        Result := 'A';
      'D':
        Result := '4';
      'E':
        Result := '7';
      'F':
        Result := 'B';
    end;
  end;

begin
  if FElectronicCode = NOJUNZHUORING then
    Result := NOJUNZHUORING
    (* else Result:= transCode(ElectronicCode[2]) + transCode(ElectronicCode[5]) +
      transCode(ElectronicCode[9]) + transCode(ElectronicCode[7]) + '-' +
      transCode(ElectronicCode[3]) + transCode(ElectronicCode[1]) +
      transCode(ElectronicCode[10]) + transCode(ElectronicCode[8]) +
      transCode(ElectronicCode[4]) + transCode(ElectronicCode[6]);   // *)
  else
    Result := Copy(ElectronicCode, 1, 4) + '-' + Copy(ElectronicCode, 5, 6);
end;

procedure TJunzhuoPigeonInfo.SetElectronicCode(const Value: string);
begin
  FElectronicCode := Value;
  // if (DM.FElectricCodes.IndexOf(Copy(Value, 1, 4) + '-' + Copy(Value, 5, 6)) < 0) then begin
  // WriteLog(6, 'ReadComm', Format('Found error electric code (%s), cancel it.', [ECode]));
  // FElectronicCode:= NOJUNZHUORING;
  // end;
end;

end.
