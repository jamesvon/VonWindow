object FCommunicationSerialPortAutoSave: TFCommunicationSerialPortAutoSave
  Left = 0
  Top = 0
  Anchors = [akTop, akRight]
  Caption = 'FCommunicationSerialPortAutoSave'
  ClientHeight = 416
  ClientWidth = 555
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    555
    416)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 183
    Top = 53
    Width = 84
    Height = 13
    Caption = #25509#25910#20449#24687#20856#22411#30721
  end
  object Label2: TLabel
    Left = 182
    Top = 98
    Width = 72
    Height = 13
    Caption = #33258#21160#22238#22797#20449#24687
  end
  object Label3: TLabel
    Left = 183
    Top = 10
    Width = 48
    Height = 13
    Caption = #20449#24687#21517#31216
  end
  object Label4: TLabel
    Left = 8
    Top = 205
    Width = 45
    Height = 13
    Caption = 'Com'#31471#21475
  end
  object Label5: TLabel
    Left = 81
    Top = 205
    Width = 57
    Height = 13
    Caption = 'Com'#27874#29305#29575
  end
  object lstMsg: TListBox
    Left = 8
    Top = 8
    Width = 168
    Height = 191
    ItemHeight = 13
    TabOrder = 0
    OnDblClick = lstMsgDblClick
  end
  object mLog: TMemo
    Left = 8
    Top = 251
    Width = 539
    Height = 157
    Anchors = [akLeft, akTop, akRight, akBottom]
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object ESender: TMemo
    Left = 182
    Top = 117
    Width = 365
    Height = 81
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
  end
  object ERecerver: TEdit
    Left = 182
    Top = 69
    Width = 365
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 3
    TextHint = #20197#21313#20845#36827#21046#22635#20889#25509#25910#30340#20856#22411#20195#30721#65292#25903#25345#65311'*N'#31561#26631#35760#23383
  end
  object ESettingName: TEdit
    Left = 183
    Top = 26
    Width = 202
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 4
  end
  object btnSave: TButton
    Left = 391
    Top = 24
    Width = 75
    Height = 25
    Hint = #20445#25345#35774#23450#26041#26696
    Anchors = [akTop, akRight]
    Caption = 'Save'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 5
    OnClick = btnSaveClick
  end
  object btnDelete: TButton
    Left = 472
    Top = 24
    Width = 75
    Height = 25
    Hint = #21024#38500#24403#21069#35774#23450#26041#26696
    Anchors = [akTop, akRight]
    Caption = 'Delete'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 6
    OnClick = btnDeleteClick
  end
  object ECommPort: TComboBox
    Left = 8
    Top = 224
    Width = 67
    Height = 21
    Style = csDropDownList
    ItemIndex = 0
    TabOrder = 7
    Text = 'COM1'
    Items.Strings = (
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10'
      'COM11'
      'COM12')
  end
  object ECommRate: TComboBox
    Left = 81
    Top = 224
    Width = 61
    Height = 21
    Style = csDropDownList
    ItemIndex = 0
    TabOrder = 8
    Text = '9600'
    Items.Strings = (
      '9600')
  end
  object btnOpen: TButton
    Left = 148
    Top = 222
    Width = 75
    Height = 25
    Hint = #25171#24320#36890#35759#31471#21475
    Caption = 'Open'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 9
    OnClick = btnOpenClick
  end
  object btnSend: TButton
    Left = 310
    Top = 222
    Width = 75
    Height = 25
    Hint = #21457#36865#33258#21160#22238#22797#30340#20449#24687#20869#23481
    Caption = 'Send'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 10
    OnClick = btnSendClick
  end
  object btnClose: TButton
    Left = 229
    Top = 222
    Width = 75
    Height = 25
    Hint = #20851#38381#36890#35759#31471#21475
    Caption = 'Close'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 11
    OnClick = btnCloseClick
  end
  object btnClear: TButton
    Left = 391
    Top = 222
    Width = 75
    Height = 25
    Hint = #28165#38500#35760#24405#20869#23481
    Caption = 'Clear'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 12
    OnClick = btnClearClick
  end
  object btnSaveFile: TButton
    Left = 472
    Top = 222
    Width = 75
    Height = 25
    Hint = #20445#23384#35760#24405#32467#26524#21040#25991#20214
    Caption = 'SaveToFile'
    CommandLinkHint = #23558#24403#21069#26085#24535#20013#30340#20869#23481#23384#20648#21040#25991#20214#20013#12290
    ParentShowHint = False
    ShowHint = True
    TabOrder = 13
    OnClick = btnSaveFileClick
  end
  object timer: TTimer
    Enabled = False
    Interval = 100
    OnTimer = timerTimer
    Left = 192
    Top = 296
  end
end
