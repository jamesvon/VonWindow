unit UCommunicationSerialPortAutoSend;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, IniFiles, UCommunicationSerialPort, UVonSystemFuns,
  ExtCtrls;

type
  TFCommunicationSerialPortAutoSave = class(TForm)
    lstMsg: TListBox;
    mLog: TMemo;
    ESender: TMemo;
    ERecerver: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    ESettingName: TEdit;
    Label3: TLabel;
    btnSave: TButton;
    btnDelete: TButton;
    Label4: TLabel;
    ECommPort: TComboBox;
    Label5: TLabel;
    ECommRate: TComboBox;
    btnOpen: TButton;
    timer: TTimer;
    btnSend: TButton;
    btnClose: TButton;
    btnClear: TButton;
    btnSaveFile: TButton;
    procedure btnSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lstMsgDblClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure timerTimer(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure btnSaveFileClick(Sender: TObject);
  private
    { Private declarations }
    FConfig: TIniFile;
    FSvc, FSnd: TStringList;
    FComm: TUart;
    FBuff: array[0..255]of byte;
    function StrToBuff(Setting: string): Integer;
    function CheckMessage(buffSize: Integer; Setting: string): boolean;
    procedure Write(title: string; len: Integer);
  public
    { Public declarations }
  end;

var
  FCommunicationSerialPortAutoSave: TFCommunicationSerialPortAutoSave;

implementation

{$R *.dfm}

{ TFCommunicationSerialPortAutoSend }

procedure TFCommunicationSerialPortAutoSave.FormCreate(Sender: TObject);
begin
  FComm:= TUart.Create;
  FSvc:= TStringList.Create;
  FSnd:= TStringList.Create;
  FConfig:= TIniFile.Create(ExtractFilePath(Application.ExeName) + 'CSAS.INI');
  FConfig.ReadSection('RECEIVER', lstMsg.Items);
  FConfig.ReadSectionValues('RECEIVER', FSvc);
  FConfig.ReadSectionValues('SEND', FSnd);
end;

procedure TFCommunicationSerialPortAutoSave.FormDestroy(Sender: TObject);
begin
  FSnd.Free;
  FSvc.Free;
  FComm.Free;
  FConfig.Free;
end;

(* Events of button *)

procedure TFCommunicationSerialPortAutoSave.btnSaveClick(Sender: TObject);
begin
  if lstMsg.Items.IndexOf(ESettingName.Text) < 0 then
    lstMsg.Items.Add(ESettingName.Text);
  FConfig.WriteString('RECEIVER', ESettingName.Text, ERecerver.Text);
  FConfig.WriteString('SEND', ESettingName.Text, ESender.Text);
  FSvc.Values[ESettingName.Text]:= ERecerver.Text;
  FSnd.Values[ESettingName.Text]:= ESender.Text;
end;

procedure TFCommunicationSerialPortAutoSave.btnSaveFileClick(Sender: TObject);
begin
  with TFileSaveDialog.Create(nil)do try
    if Execute then mLog.Lines.SaveToFile(FileName);
  finally
    Free;
  end;
end;

procedure TFCommunicationSerialPortAutoSave.lstMsgDblClick(Sender: TObject);
begin
  ESettingName.Text:= lstMsg.Items[lstMsg.ItemIndex];
  ERecerver.Text:= FConfig.ReadString('RECEIVER', ESettingName.Text, '');
  ESender.Text:= FConfig.ReadString('SEND', ESettingName.Text, '');
end;

procedure TFCommunicationSerialPortAutoSave.btnClearClick(Sender: TObject);
begin
  mLog.Clear;
end;

procedure TFCommunicationSerialPortAutoSave.btnCloseClick(Sender: TObject);
begin
  FComm.Close;
end;

procedure TFCommunicationSerialPortAutoSave.btnDeleteClick(Sender: TObject);
var
  Idx: Integer;
begin
  Idx:= lstMsg.Items.IndexOf(ESettingName.Text);
  if Idx < 0 then raise Exception.Create('未发现该设定信息，无法进行删除。');
  lstMsg.Items.Delete(Idx);
  FConfig.DeleteKey('RECEIVER', ESettingName.Text);
  FConfig.DeleteKey('SEND', ESettingName.Text);
end;

procedure TFCommunicationSerialPortAutoSave.btnSendClick(Sender: TObject);
var
  szCnt: Integer;
begin
  szCnt:= StrToBuff(ESender.Text);
  Write('Send', szCnt);
  FComm.Send(FBuff, szCnt);
end;

(* functions *)

function TFCommunicationSerialPortAutoSave.CheckMessage(buffSize: Integer;
  Setting: string): boolean;
var
  P: PChar;
  szS: string;
  Idx: Integer;
begin
  P:= PChar(Setting); Idx:= 0; szS:= ''; Result:= True;
  while P[0] <> #0 do begin
    if P[0] = '?' then Inc(Idx)              //单个字节放弃
    else if P[0] = '*' then Break            //其余免检测
    else if P[0] = ' ' then begin            //字节间隔符
      if szS <> '' then begin
        Result:= FBuff[Idx] = HexToInt(szS);
        Inc(Idx);
      end;
      szS:= '';
      if not Result then Exit;
    end else if(P[0] <> #13)or(P[0] <> #10) then szS:= szS + P[0];
    Inc(P);
  end;
  if szS <> '' then Result:= FBuff[Idx] = HexToInt(szS)
  else Result:= True;
end;

function TFCommunicationSerialPortAutoSave.StrToBuff(Setting: string): Integer;
var
  P: PChar;
  szS: string;
  cnt: Integer;
begin
  mLog.Lines.Add('StrToBuff:' + Setting);
  FillChar(FBuff, 256, 0);
  P:= PChar(Setting); Result:= 0; szS:= '';
  while P[0] <> #0 do begin
    if P[0] = '?' then begin
      FBuff[Result]:= Random(256);
      Inc(Result);              //单个字节放弃
    end else if P[0] = 'N' then begin            //数字字符
      FBuff[Result]:= Random(10) + Ord('0');
      Inc(Result);
    end else if P[0] = '*' then begin            //其余免检测
      cnt:= Random(8);
      while cnt > 0 do begin
        FBuff[Result]:= Random(256);
        Inc(Result);
        Dec(cnt);
      end;
    end else if P[0] = ' ' then begin            //字节间隔符
      if szS <> '' then begin
        FBuff[Result]:= HexToInt(szS);
        Inc(Result);
      end;
      szS:= '';
    end else if(P[0] <> #13)or(P[0] <> #10) then szS:= szS + P[0];
    Inc(P);
  end;
  if szS <> '' then begin
    FBuff[Result]:= HexToInt(szS);
    Inc(Result);
  end;
end;

(* Communication *)

procedure TFCommunicationSerialPortAutoSave.btnOpenClick(Sender: TObject);
begin
  FComm.Close;
  FComm.Open(ECommPort.ItemIndex + 1, StrToInt(ECommRate.Text), 'n', 8, 1);
  timer.Enabled:= True;
end;

procedure TFCommunicationSerialPortAutoSave.timerTimer(Sender: TObject);
var
  I, buffSize: Integer;
  szS: string;
begin     //
  buffSize:= FComm.Receive(FBuff, 256, 300);
  if buffSize > 0 then begin
    Write('Recived', buffSize);
    for I := 0 to FSvc.Count - 1 do
      if CheckMessage(buffSize, FSvc.ValueFromIndex[I])then begin
        buffSize:= StrToBuff(FSnd.ValueFromIndex[I]);
        Sleep(300);
        Write('Send ' + FSvc.Names[I], buffSize);
        FComm.Send(FBuff, buffSize);
        Exit;
      end else mLog.Lines.Add('[Not found setting]' + szS);
  end;
end;

procedure TFCommunicationSerialPortAutoSave.Write(title: string; len: Integer);
var
  szS: string;
  I: Integer;
begin
  szS:= '';
  for I := 0 to len - 1 do begin
    if I mod 32 = 0 then szS:= szS + #13#10
    else if I mod 8 = 0 then szS:= szS + ' '
    else if I mod 16 = 0 then szS:= szS + '  '
    else if I mod 24 = 0 then szS:= szS + ' ';
    szS:= szS + ' ' + IntToHex(Fbuff[I],2);
  end;
  mLog.Lines.Add('[' + DatetimeToStr(Now) + ' ' + title + ']'#13#10 + szS);
end;

end.
