unit UCommunicationTCP;

interface

uses
  WinAPI.Windows, WinAPI.Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, UCommunicationModule, IdContext, IdAntiFreezeBase,
  IdAntiFreeze,
  IdBaseComponent, IdComponent, IdCustomTCPServer, IdTCPServer, IdTCPClient,
  UVonLog;

type
  TCommunicationTCPServer = class(TCommunicationServer)
  private
    FTCPSvr: TIdTCPServer;
    procedure FTCPSvrExecute(AContext: TIdContext);
    procedure FTCPSvrDisconnect(AContext: TIdContext);
    procedure FTCPSvrConnect(AContext: TIdContext);
  public
    constructor Create(AppExeName, ConfigFilename: string); override;
    destructor Destroy; override;
    procedure Start; override;
    procedure Stop; override;
  end;

  TCommunicationTCPClient = class(TCommunicationClient)
  private
    FTCPClient: TIdTCPClient;
  public
    constructor Create(AppExeName, AppName: string); override;
    destructor Destroy; override;
    procedure SendIt(Msg: TVonCommunicationMessage); override;
  end;

implementation

uses UCommunicationRes;

{ TCommunicationTCPServer }

constructor TCommunicationTCPServer.Create(AppExeName, ConfigFilename: string);
begin
  inherited;
  FTCPSvr := TIdTCPServer.Create(nil);
  FTCPSvr.OnConnect := FTCPSvrConnect;
  FTCPSvr.OnExecute := FTCPSvrExecute;
  FTCPSvr.OnDisconnect := FTCPSvrDisconnect;
end;

destructor TCommunicationTCPServer.Destroy;
begin
  FTCPSvr.Active := False;
  FTCPSvr.Free;
  inherited;
end;

procedure TCommunicationTCPServer.FTCPSvrConnect(AContext: TIdContext);
begin
  with AContext.Binding do
    WriteLog(LOG_INFO, 'TCPSvr', Format(RES_COMM_ConnectionInfo,
      [PeerIP, PeerPort]));
end;

procedure TCommunicationTCPServer.FTCPSvrDisconnect(AContext: TIdContext);
begin
  with AContext.Binding do
    WriteLog(LOG_INFO, 'TCPSvr', Format(RES_COMM_DisconnectionInfo,
      [PeerIP, PeerPort]));
end;

procedure TCommunicationTCPServer.FTCPSvrExecute(AContext: TIdContext);
var
  Msg: TVonCommunicationMessage;
  szStream, szOutStream, szTmpStream: TStream;
  szBuff: array [0 .. 3] of byte;
  msgSize: Cardinal;
  sendSize: Cardinal;
begin
  inherited;
  // 接收客户端发过来的信息长度（流信息的前4个字节）
  szStream := TMemoryStream.Create;
  AContext.Connection.IOHandler.ReadStream(szStream, 4);
  szStream.Position := 0;
  szStream.Read(szBuff, 4);
  szStream.Free;
  msgSize := szBuff[0] * 16777216 + szBuff[1] * 65536 + szBuff[2] * 256 +
    szBuff[3];
  if msgSize > MAX_BUFFER_SIZE then
  begin
    with AContext.Binding do
      WriteLog(LOG_FAIL, 'TCPSvr', Format(RES_COMM_MaxErrorInfo,
        [msgSize, PeerIP, PeerPort]));
    AContext.Connection.Disconnect;
    Exit;
  end;
  // 确定接收长度，接收全部信息
  with AContext.Binding do
    WriteLog(LOG_INFO, 'TCPSvr', Format(RES_COMM_MessageInfo,
      [msgSize, PeerIP, PeerPort]));
  try
    szTmpStream := TMemoryStream.Create; // 临时流，接收准备发送的流内容
    Msg := TVonCommunicationMessage.Create;
    AContext.Connection.IOHandler.ReadStream(szTmpStream, msgSize, False);
    // 接收全部流信息
    Msg.BindingHost := AContext.Binding.PeerIP;
    if not Msg.ReadFromStream(szTmpStream) then // 分解信息
      raise Exception.Create('Can not process a message');
    ReceiveIt(Msg); // 处理流内容，并将准备发送的内容回写到 Msg 中
    szStream := Msg.MerageStream;
    // ShowStream(szStream);
    sendSize := szStream.Size;
    szOutStream := TMemoryStream.Create; // 最终发送的流内容
    try
      szOutStream.Write(sendSize, 4); // 写入返回流长度
      szOutStream.CopyFrom(szStream, sendSize);
      AContext.Connection.IOHandler.Write(szOutStream); // 发送
    finally
      szOutStream.Free;
    end;
  except
    on E: Exception do
    begin
      WriteLog(LOG_INFO, 'TCPSvr', 'Faild to process a message.' + E.Message);
    end;
  end;
  szTmpStream.Free;
  Msg.Free;
  AContext.Connection.Disconnect;
end;

procedure TCommunicationTCPServer.Start;
begin
  inherited;
  if FTCPSvr.Active then
    FTCPSvr.Active := False;
  FTCPSvr.DefaultPort := CommunicationPort;
  FTCPSvr.Active := True;
end;

procedure TCommunicationTCPServer.Stop;
begin
  inherited;
  FTCPSvr.Active := False;
end;

{ TCommunicationTCPClient }

constructor TCommunicationTCPClient.Create(AppExeName, AppName: string);
begin
  inherited;
  FTCPClient := TIdTCPClient.Create(nil);
end;

destructor TCommunicationTCPClient.Destroy;
begin
  FTCPClient.Free;
  inherited;
end;

procedure TCommunicationTCPClient.SendIt(Msg: TVonCommunicationMessage);
var
  msgSize: Integer;
  szStream, sendStream: TStream;
begin
  inherited;
  Msg.AppName := AppName;
  sendStream := Msg.MerageStream; // 准备发送的流信息
  WriteLog(LOG_INFO, 'TCPClient', Format(RES_COMM_ConnectToInfo,
    [HostIP, CommunicationPort]));
  FTCPClient.Connect(HostIP, CommunicationPort); // 连接服务器
  if not FTCPClient.Connected then
    raise Exception.Create('Can not connecte server.');
  WriteLog(LOG_INFO, 'TCPClient', Format('Ready to send %s to %s with %s',
    [Msg.CommandString, Msg.AppName, Msg.Params]));
  FTCPClient.IOHandler.Write(sendStream, sendStream.Size, True); // 发送
  szStream := TMemoryStream.Create;
  FTCPClient.IOHandler.ReadStream(szStream, 4, False); // 接收数据长度信息
  szStream.Position := 0;
  szStream.Read(msgSize, 4);
  szStream.Size := 0;
  FTCPClient.IOHandler.ReadStream(szStream, msgSize, False); // 接收数据内容
  if not Msg.ReadFromStream(szStream) then
    raise Exception.Create
      ('Can not read a stream what is received from server');
  WriteLog(LOG_INFO, 'TCPClient', Format('Receive a %s from %s with %s',
    [Msg.CommandString, Msg.AppName, Msg.Params]));
  szStream.Free;
  FTCPClient.Disconnect; // 断开连接
  WriteLog(LOG_INFO, 'TCPClient', Format(RES_COMM_DisconnectToInfo,
    [HostIP, CommunicationPort]));
end;

initialization
//  TVonApplicationClientBase.RegistTransmission('TCP', TCommunicationTCPClient);

end.
