unit UCommunicationUDP;

interface

uses UCommunicationModule;

type
  TCommunicationUDPServer = class(TCommunicationServer)
  public
    constructor Create(AppExeName, ConfigFilename: string); override;

  end;

  TCommunicationUDPClient = class(TCommunicationClient)
  public
    constructor Create(AppExeName, AppName: string); override;

  end;

implementation

{ TCommunicationUDPClient }

constructor TCommunicationUDPClient.Create(AppExeName, AppName: string);
begin
  inherited;

end;

{ TCommunicationUDPServer }

constructor TCommunicationUDPServer.Create(AppExeName, ConfigFilename: string);
begin
  inherited;

end;

end.
