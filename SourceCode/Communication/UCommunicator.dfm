object FTransCommunication: TFTransCommunication
  Left = 0
  Top = 0
  Caption = 'FCommunicator'
  ClientHeight = 793
  ClientWidth = 1260
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 137
    Top = 0
    Width = 800
    Height = 793
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 428
      Width = 800
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      ExplicitLeft = -16
      ExplicitTop = 366
      ExplicitWidth = 1123
    end
    object Panel3: TPanel
      Left = 0
      Top = 764
      Width = 800
      Height = 29
      Align = alBottom
      Caption = 'Panel3'
      ShowCaption = False
      TabOrder = 0
      object btnSend: TButton
        AlignWithMargins = True
        Left = 725
        Top = 4
        Width = 71
        Height = 21
        Align = alRight
        Caption = #21457#36865
        TabOrder = 0
        OnClick = btnSendClick
      end
      object ESendText: TEdit
        Left = 129
        Top = 1
        Width = 593
        Height = 27
        Align = alClient
        TabOrder = 1
        ExplicitHeight = 21
      end
      object Panel8: TPanel
        Left = 1
        Top = 1
        Width = 128
        Height = 27
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'Panel8'
        ShowCaption = False
        TabOrder = 2
        object rbText: TRadioButton
          Left = 13
          Top = 5
          Width = 52
          Height = 17
          Caption = #25991#26412
          TabOrder = 0
        end
        object rbHex: TRadioButton
          Left = 61
          Top = 5
          Width = 68
          Height = 17
          Caption = #21313#20845#36827#21046
          Checked = True
          TabOrder = 1
          TabStop = True
        end
      end
    end
    object PageControl3: TPageControl
      Left = 0
      Top = 431
      Width = 800
      Height = 333
      ActivePage = TabSheet5
      Align = alBottom
      TabOrder = 1
      object TabSheet5: TTabSheet
        Caption = #23458#25143#31471#36890#35759#20869#23481
        object mClientLog: TMemo
          Left = 0
          Top = 0
          Width = 792
          Height = 305
          Align = alClient
          ScrollBars = ssBoth
          TabOrder = 0
        end
      end
      object TabSheet6: TTabSheet
        Caption = #22238#22797#35774#32622
        ImageIndex = 1
        object Panel15: TPanel
          Left = 0
          Top = 185
          Width = 792
          Height = 120
          Align = alBottom
          Caption = 'Panel11'
          ShowCaption = False
          TabOrder = 0
          object pcClientReplyConditionKind: TPageControl
            Left = 1
            Top = 1
            Width = 289
            Height = 118
            ActivePage = TabSheet14
            Align = alLeft
            TabOrder = 0
            object TabSheet14: TTabSheet
              Caption = #26080#26465#20214
              ImageIndex = 2
            end
            object TabSheet15: TTabSheet
              Caption = #38271#24230#26465#20214
              object Label20: TLabel
                AlignWithMargins = True
                Left = 3
                Top = 36
                Width = 275
                Height = 27
                Align = alTop
                AutoSize = False
                Caption = #38271#24230
                Layout = tlCenter
                ExplicitTop = 3
              end
              object rgClientReplyLenCondition: TRadioGroup
                Left = 0
                Top = 0
                Width = 281
                Height = 33
                Align = alTop
                Caption = #26465#20214
                Columns = 6
                ItemIndex = 2
                Items.Strings = (
                  '>'
                  '>='
                  '=='
                  '!='
                  '<='
                  '<')
                TabOrder = 0
              end
              object EClientReplyLenConditionParam: TSpinEdit
                Left = 0
                Top = 66
                Width = 281
                Height = 22
                Align = alTop
                MaxValue = 0
                MinValue = 0
                TabOrder = 1
                Value = 0
              end
            end
            object TabSheet16: TTabSheet
              Caption = #20301#32622#23383#31526
              ImageIndex = 1
              object EClientReplyCharCondition: TRadioGroup
                Left = 0
                Top = 0
                Width = 281
                Height = 41
                Align = alTop
                Caption = #26465#20214
                Columns = 4
                ItemIndex = 0
                Items.Strings = (
                  #27491#24207
                  #21069#21253#21547
                  #21518#21253#21547
                  #20498#24207)
                TabOrder = 0
              end
              object Panel16: TPanel
                Left = 0
                Top = 41
                Width = 281
                Height = 24
                Align = alTop
                BevelOuter = bvNone
                Caption = 'Panel8'
                ShowCaption = False
                TabOrder = 1
                object Label21: TLabel
                  AlignWithMargins = True
                  Left = 135
                  Top = 3
                  Width = 48
                  Height = 18
                  Align = alLeft
                  Caption = #36215#22987#20301#32622
                  Layout = tlCenter
                  ExplicitHeight = 13
                end
                object EClientReplyCharConditionKind: TRadioButton
                  AlignWithMargins = True
                  Left = 3
                  Top = 3
                  Width = 52
                  Height = 18
                  Align = alLeft
                  Caption = #25991#26412
                  TabOrder = 0
                end
                object RadioButton3: TRadioButton
                  AlignWithMargins = True
                  Left = 61
                  Top = 3
                  Width = 68
                  Height = 18
                  Align = alLeft
                  Caption = #21313#20845#36827#21046
                  Checked = True
                  TabOrder = 1
                  TabStop = True
                end
                object EClientReplyCharConditionStart: TSpinEdit
                  Left = 186
                  Top = 0
                  Width = 95
                  Height = 24
                  Align = alClient
                  MaxValue = 0
                  MinValue = 0
                  TabOrder = 2
                  Value = 0
                end
              end
              object EClientReplyCharConditionParam: TEdit
                Left = 0
                Top = 65
                Width = 281
                Height = 25
                Align = alClient
                TabOrder = 2
                ExplicitHeight = 21
              end
            end
          end
          object Panel17: TPanel
            Left = 290
            Top = 1
            Width = 501
            Height = 118
            Align = alClient
            Caption = 'Panel12'
            TabOrder = 1
            object EClientReplyText: TEdit
              Left = 1
              Top = 1
              Width = 499
              Height = 75
              Align = alClient
              TabOrder = 0
              ExplicitHeight = 21
            end
            object Panel18: TPanel
              Left = 1
              Top = 76
              Width = 499
              Height = 41
              Align = alBottom
              BevelOuter = bvNone
              Caption = 'Panel13'
              ShowCaption = False
              TabOrder = 1
              object rgClientAutoSendKind: TRadioGroup
                Left = 0
                Top = 0
                Width = 192
                Height = 41
                Align = alLeft
                Caption = #25968#25454#26684#24335
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  #25991#26412
                  'Hex'
                  #21407#25991)
                TabOrder = 0
              end
              object btnClientAddReply: TButton
                AlignWithMargins = True
                Left = 163
                Top = 3
                Width = 107
                Height = 35
                Align = alRight
                Caption = #28155#21152
                TabOrder = 1
                OnClick = btnClientAddReplyClick
              end
              object btnClientEditReply: TButton
                AlignWithMargins = True
                Left = 276
                Top = 3
                Width = 107
                Height = 35
                Align = alRight
                Caption = #20462#25913
                TabOrder = 2
                OnClick = btnClientEditReplyClick
              end
              object btnClientDelReply: TButton
                AlignWithMargins = True
                Left = 389
                Top = 3
                Width = 107
                Height = 35
                Align = alRight
                Caption = #21024#38500
                TabOrder = 3
                OnClick = btnClientDelReplyClick
              end
            end
          end
        end
        object mClientReply: TMemo
          Left = 0
          Top = 0
          Width = 792
          Height = 185
          Align = alClient
          ScrollBars = ssBoth
          TabOrder = 1
          OnDblClick = mClientReplyDblClick
        end
      end
      object TabSheet7: TTabSheet
        Caption = #20998#26512#35774#32622
        ImageIndex = 2
      end
    end
    object PageControl4: TPageControl
      Left = 0
      Top = 0
      Width = 800
      Height = 428
      ActivePage = TabSheet8
      Align = alClient
      TabOrder = 2
      object TabSheet8: TTabSheet
        Caption = #26381#21153#22120#36890#35759#20869#23481
        object mSvrLog: TMemo
          Left = 0
          Top = 0
          Width = 792
          Height = 400
          Align = alClient
          ScrollBars = ssBoth
          TabOrder = 0
        end
      end
      object TabSheet9: TTabSheet
        Caption = #22238#22797#35774#32622
        ImageIndex = 1
        object mServerReply: TMemo
          Left = 0
          Top = 0
          Width = 792
          Height = 280
          Align = alClient
          ScrollBars = ssBoth
          TabOrder = 0
          OnDblClick = mServerReplyDblClick
        end
        object Panel11: TPanel
          Left = 0
          Top = 280
          Width = 792
          Height = 120
          Align = alBottom
          Caption = 'Panel11'
          ShowCaption = False
          TabOrder = 1
          object pcServerReplyConditionKind: TPageControl
            Left = 1
            Top = 1
            Width = 289
            Height = 118
            ActivePage = TabSheet13
            Align = alLeft
            TabOrder = 0
            object TabSheet13: TTabSheet
              Caption = #26080#26465#20214
              ImageIndex = 2
            end
            object TabSheet11: TTabSheet
              Caption = #38271#24230#26465#20214
              object Label18: TLabel
                AlignWithMargins = True
                Left = 3
                Top = 36
                Width = 275
                Height = 27
                Align = alTop
                AutoSize = False
                Caption = #38271#24230
                Layout = tlCenter
                ExplicitTop = 3
              end
              object rgServerReplyLenCondition: TRadioGroup
                Left = 0
                Top = 0
                Width = 281
                Height = 33
                Align = alTop
                Caption = #26465#20214
                Columns = 6
                ItemIndex = 2
                Items.Strings = (
                  '>'
                  '>='
                  '=='
                  '!='
                  '<='
                  '<')
                TabOrder = 0
              end
              object EServerReplyLenConditionParam: TSpinEdit
                Left = 0
                Top = 66
                Width = 281
                Height = 22
                Align = alTop
                MaxValue = 0
                MinValue = 0
                TabOrder = 1
                Value = 0
              end
            end
            object TabSheet12: TTabSheet
              Caption = #20301#32622#23383#31526
              ImageIndex = 1
              object EServerReplyCharCondition: TRadioGroup
                Left = 0
                Top = 0
                Width = 281
                Height = 41
                Align = alTop
                Caption = #26465#20214
                Columns = 4
                ItemIndex = 0
                Items.Strings = (
                  #27491#24207
                  #21069#21253#21547
                  #21518#21253#21547
                  #20498#24207)
                TabOrder = 0
              end
              object Panel14: TPanel
                Left = 0
                Top = 41
                Width = 281
                Height = 24
                Align = alTop
                BevelOuter = bvNone
                Caption = 'Panel8'
                ShowCaption = False
                TabOrder = 1
                object Label19: TLabel
                  AlignWithMargins = True
                  Left = 135
                  Top = 3
                  Width = 48
                  Height = 18
                  Align = alLeft
                  Caption = #36215#22987#20301#32622
                  Layout = tlCenter
                  ExplicitHeight = 13
                end
                object EServerReplyCharConditionKind: TRadioButton
                  AlignWithMargins = True
                  Left = 3
                  Top = 3
                  Width = 52
                  Height = 18
                  Align = alLeft
                  Caption = #25991#26412
                  TabOrder = 0
                end
                object RadioButton2: TRadioButton
                  AlignWithMargins = True
                  Left = 61
                  Top = 3
                  Width = 68
                  Height = 18
                  Align = alLeft
                  Caption = #21313#20845#36827#21046
                  Checked = True
                  TabOrder = 1
                  TabStop = True
                end
                object EServerReplyCharConditionStart: TSpinEdit
                  Left = 186
                  Top = 0
                  Width = 95
                  Height = 24
                  Align = alClient
                  MaxValue = 0
                  MinValue = 0
                  TabOrder = 2
                  Value = 0
                end
              end
              object EServerReplyCharConditionParam: TEdit
                Left = 0
                Top = 65
                Width = 281
                Height = 25
                Align = alClient
                TabOrder = 2
                ExplicitHeight = 21
              end
            end
          end
          object Panel12: TPanel
            Left = 290
            Top = 1
            Width = 501
            Height = 118
            Align = alClient
            Caption = 'Panel12'
            TabOrder = 1
            object EServerReplyText: TEdit
              Left = 1
              Top = 1
              Width = 499
              Height = 75
              Align = alClient
              TabOrder = 0
              ExplicitHeight = 21
            end
            object Panel13: TPanel
              Left = 1
              Top = 76
              Width = 499
              Height = 41
              Align = alBottom
              BevelOuter = bvNone
              Caption = 'Panel13'
              ShowCaption = False
              TabOrder = 1
              object rgServerAutoSendKind: TRadioGroup
                Left = 0
                Top = 0
                Width = 192
                Height = 41
                Align = alLeft
                Caption = #25968#25454#26684#24335
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  #25991#26412
                  'Hex'
                  #21407#25991)
                TabOrder = 0
              end
              object btnServerAddReply: TButton
                AlignWithMargins = True
                Left = 163
                Top = 3
                Width = 107
                Height = 35
                Align = alRight
                Caption = #28155#21152
                TabOrder = 1
                OnClick = btnServerAddReplyClick
              end
              object btnServerEditReply: TButton
                AlignWithMargins = True
                Left = 276
                Top = 3
                Width = 107
                Height = 35
                Align = alRight
                Caption = #20462#25913
                TabOrder = 2
                OnClick = btnServerEditReplyClick
              end
              object btnServerDelReply: TButton
                AlignWithMargins = True
                Left = 389
                Top = 3
                Width = 107
                Height = 35
                Align = alRight
                Caption = #21024#38500
                TabOrder = 3
                OnClick = btnServerDelReplyClick
              end
            end
          end
        end
      end
      object TabSheet10: TTabSheet
        Caption = #20998#26512#35774#32622
        ImageIndex = 2
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 137
    Height = 793
    Align = alLeft
    Caption = 'Panel2'
    ShowCaption = False
    TabOrder = 1
    object btnClient: TButton
      AlignWithMargins = True
      Left = 4
      Top = 764
      Width = 129
      Height = 25
      Align = alBottom
      Caption = #36830#25509
      TabOrder = 0
      OnClick = btnClientClick
    end
    object btnServer: TButton
      AlignWithMargins = True
      Left = 4
      Top = 288
      Width = 129
      Height = 25
      Align = alTop
      Caption = #25171#24320
      TabOrder = 1
      OnClick = btnServerClick
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 428
      Width = 135
      Height = 317
      Align = alTop
      Caption = #23458#25143#31471#35774#32622
      TabOrder = 2
      object pcClient: TPageControl
        Left = 2
        Top = 15
        Width = 131
        Height = 162
        ActivePage = TabSheet1
        Align = alTop
        MultiLine = True
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = #20018#21475#36890#20449
          object FlowPanel1: TFlowPanel
            Left = 0
            Top = 0
            Width = 123
            Height = 134
            Align = alClient
            BevelOuter = bvNone
            Caption = 'FlowPanel1'
            ShowCaption = False
            TabOrder = 0
            object Label1: TLabel
              AlignWithMargins = True
              Left = 3
              Top = 3
              Width = 38
              Height = 20
              AutoSize = False
              Caption = #31471#21475#21495
              Layout = tlCenter
            end
            object ECommPort: TComboBox
              Left = 44
              Top = 0
              Width = 73
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 0
              Text = 'COM1'
              Items.Strings = (
                'COM1'
                'COM2'
                'COM3'
                'COM4'
                'COM5'
                'COM6'
                'COM7'
                'COM8'
                'COM9'
                'COM10'
                'COM11'
                'COM12'
                'COM13'
                'COM14'
                'COM15'
                'COM16'
                'COM17'
                'COM18'
                'COM19'
                'COM20'
                'COM21'
                'COM22'
                'COM23'
                'COM24'
                'COM25'
                'COM26'
                'COM27'
                'COM28'
                'COM29'
                'COM30')
            end
            object Label2: TLabel
              AlignWithMargins = True
              Left = 3
              Top = 29
              Width = 38
              Height = 20
              AutoSize = False
              Caption = #27874#29305#29575
              Layout = tlCenter
            end
            object ECommBaud: TComboBox
              Left = 44
              Top = 26
              Width = 73
              Height = 21
              Style = csDropDownList
              ItemIndex = 6
              TabOrder = 1
              Text = '38400'
              Items.Strings = (
                '300'
                '1200'
                '2400'
                '4800'
                '9600'
                '19200'
                '38400'
                '43000'
                '56000'
                '57600'
                '115200')
            end
            object Label3: TLabel
              AlignWithMargins = True
              Left = 3
              Top = 55
              Width = 38
              Height = 20
              AutoSize = False
              Caption = #26657#39564#20301
              Layout = tlCenter
            end
            object ECommParity: TComboBox
              Left = 44
              Top = 52
              Width = 73
              Height = 21
              Style = csDropDownList
              ItemIndex = 1
              TabOrder = 2
              Text = #26080
              Items.Strings = (
                #26377
                #26080)
            end
            object Label4: TLabel
              AlignWithMargins = True
              Left = 3
              Top = 81
              Width = 38
              Height = 20
              AutoSize = False
              Caption = #25968#25454#20301
              Layout = tlCenter
            end
            object ECommBit: TComboBox
              Left = 44
              Top = 78
              Width = 73
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 3
              Text = '8'
              Items.Strings = (
                '8'
                '7'
                '6'
                '5')
            end
            object Label5: TLabel
              AlignWithMargins = True
              Left = 3
              Top = 107
              Width = 38
              Height = 20
              AutoSize = False
              Caption = #20572#27490#20301
              Layout = tlCenter
            end
            object ECommStp: TComboBox
              Left = 44
              Top = 104
              Width = 73
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 4
              Text = '1'
              Items.Strings = (
                '1'
                '0')
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = #32593#32476#36890#20449
          ImageIndex = 1
          object plPort1: TPanel
            AlignWithMargins = True
            Left = 3
            Top = 82
            Width = 117
            Height = 22
            Align = alTop
            BevelOuter = bvNone
            Caption = 'plPort1'
            ShowCaption = False
            TabOrder = 0
            object Label12: TLabel
              AlignWithMargins = True
              Left = 3
              Top = 3
              Width = 30
              Height = 16
              Align = alLeft
              AutoSize = False
              Caption = 'Port'
              Layout = tlCenter
              ExplicitLeft = 0
              ExplicitTop = 6
            end
            object EClientPort: TSpinEdit
              Left = 36
              Top = 0
              Width = 81
              Height = 22
              Align = alClient
              MaxValue = 0
              MinValue = 0
              TabOrder = 0
              Value = 0
            end
          end
          object rgClientKind: TRadioGroup
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 117
            Height = 45
            Align = alTop
            Caption = #26041#24335
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'TCP'
              'UDP')
            TabOrder = 1
          end
          object Panel4: TPanel
            AlignWithMargins = True
            Left = 3
            Top = 54
            Width = 117
            Height = 22
            Align = alTop
            BevelOuter = bvNone
            Caption = 'plPort1'
            ShowCaption = False
            TabOrder = 2
            object Label13: TLabel
              AlignWithMargins = True
              Left = 3
              Top = 3
              Width = 30
              Height = 16
              Align = alLeft
              AutoSize = False
              Caption = 'Host'
              Layout = tlCenter
              ExplicitLeft = 0
              ExplicitTop = 6
            end
            object EClientHost: TEdit
              Left = 36
              Top = 0
              Width = 81
              Height = 22
              Align = alClient
              TabOrder = 0
              Text = '127.0.0.1'
              ExplicitHeight = 21
            end
          end
          object Panel7: TPanel
            AlignWithMargins = True
            Left = 3
            Top = 110
            Width = 117
            Height = 22
            Align = alTop
            BevelOuter = bvNone
            Caption = 'plPort1'
            ShowCaption = False
            TabOrder = 3
            object Label15: TLabel
              AlignWithMargins = True
              Left = 3
              Top = 3
              Width = 30
              Height = 16
              Align = alLeft
              AutoSize = False
              Caption = #24310#26102
              Layout = tlCenter
              ExplicitLeft = 0
              ExplicitTop = 6
            end
            object EClientTimeOut: TSpinEdit
              Left = 36
              Top = 0
              Width = 81
              Height = 22
              Align = alClient
              MaxValue = 0
              MinValue = 0
              TabOrder = 0
              Value = 0
            end
          end
        end
      end
      object rgClientReveived: TRadioGroup
        Left = 2
        Top = 177
        Width = 131
        Height = 110
        Align = alTop
        Caption = #22788#29702#26041#24335
        ItemIndex = 0
        Items.Strings = (
          #26080#21160#20316
          #23450#26102#21457#36865
          #33258#21160#24212#31572
          #20998#26512#24212#31572
          #26381#21153#25509#31649)
        TabOrder = 1
        OnClick = rgClientReveivedClick
      end
      object Panel10: TPanel
        AlignWithMargins = True
        Left = 5
        Top = 290
        Width = 125
        Height = 22
        Align = alTop
        BevelOuter = bvNone
        Caption = 'plPort1'
        ShowCaption = False
        TabOrder = 2
        object Label17: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 50
          Height = 16
          Align = alLeft
          AutoSize = False
          Caption = #21457#36865#38388#38548
          Layout = tlCenter
        end
        object ESendInterValue: TSpinEdit
          Left = 56
          Top = 0
          Width = 69
          Height = 22
          Align = alClient
          MaxValue = 0
          MinValue = 0
          TabOrder = 0
          Value = 0
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 1
      Top = 1
      Width = 135
      Height = 284
      Align = alTop
      Caption = #26381#21153#31471#35774#32622
      TabOrder = 3
      object pcServer: TPageControl
        Left = 2
        Top = 15
        Width = 131
        Height = 162
        ActivePage = TabSheet3
        Align = alTop
        MultiLine = True
        TabOrder = 0
        object TabSheet3: TTabSheet
          Caption = #20018#21475#36890#20449
          object FlowPanel2: TFlowPanel
            Left = 0
            Top = 0
            Width = 123
            Height = 134
            Align = alClient
            BevelOuter = bvNone
            Caption = 'FlowPanel1'
            ShowCaption = False
            TabOrder = 0
            object Label6: TLabel
              AlignWithMargins = True
              Left = 3
              Top = 3
              Width = 38
              Height = 20
              AutoSize = False
              Caption = #31471#21475#21495
              Layout = tlCenter
            end
            object ESerialPort: TComboBox
              Left = 44
              Top = 0
              Width = 73
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 0
              Text = 'COM1'
              Items.Strings = (
                'COM1'
                'COM2'
                'COM3'
                'COM4'
                'COM5'
                'COM6'
                'COM7'
                'COM8'
                'COM9'
                'COM10'
                'COM11'
                'COM12'
                'COM13'
                'COM14'
                'COM15'
                'COM16'
                'COM17'
                'COM18'
                'COM19'
                'COM20'
                'COM21'
                'COM22'
                'COM23'
                'COM24'
                'COM25'
                'COM26'
                'COM27'
                'COM28'
                'COM29'
                'COM30')
            end
            object Label7: TLabel
              AlignWithMargins = True
              Left = 3
              Top = 29
              Width = 38
              Height = 20
              AutoSize = False
              Caption = #27874#29305#29575
              Layout = tlCenter
            end
            object ESerialBaud: TComboBox
              Left = 44
              Top = 26
              Width = 73
              Height = 21
              Style = csDropDownList
              ItemIndex = 6
              TabOrder = 1
              Text = '38400'
              Items.Strings = (
                '300'
                '1200'
                '2400'
                '4800'
                '9600'
                '19200'
                '38400'
                '43000'
                '56000'
                '57600'
                '115200')
            end
            object Label8: TLabel
              AlignWithMargins = True
              Left = 3
              Top = 55
              Width = 38
              Height = 20
              AutoSize = False
              Caption = #26657#39564#20301
              Layout = tlCenter
            end
            object ESerialParity: TComboBox
              Left = 44
              Top = 52
              Width = 73
              Height = 21
              Style = csDropDownList
              ItemIndex = 1
              TabOrder = 2
              Text = #26080
              Items.Strings = (
                #26377
                #26080)
            end
            object Label9: TLabel
              AlignWithMargins = True
              Left = 3
              Top = 81
              Width = 38
              Height = 20
              AutoSize = False
              Caption = #25968#25454#20301
              Layout = tlCenter
            end
            object ESerialDataBit: TComboBox
              Left = 44
              Top = 78
              Width = 73
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 3
              Text = '8'
              Items.Strings = (
                '8'
                '7'
                '6'
                '5')
            end
            object Label10: TLabel
              AlignWithMargins = True
              Left = 3
              Top = 107
              Width = 38
              Height = 20
              AutoSize = False
              Caption = #20572#27490#20301
              Layout = tlCenter
            end
            object ESerialStopBit: TComboBox
              Left = 44
              Top = 104
              Width = 73
              Height = 21
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 4
              Text = '1'
              Items.Strings = (
                '1'
                '0')
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = #32593#32476#36890#20449
          ImageIndex = 1
          object Panel5: TPanel
            AlignWithMargins = True
            Left = 3
            Top = 54
            Width = 117
            Height = 22
            Align = alTop
            BevelOuter = bvNone
            Caption = 'plPort1'
            ShowCaption = False
            TabOrder = 0
            object Label11: TLabel
              AlignWithMargins = True
              Left = 3
              Top = 3
              Width = 30
              Height = 16
              Align = alLeft
              AutoSize = False
              Caption = #31471#21475
              Layout = tlCenter
              ExplicitLeft = 0
              ExplicitTop = 6
            end
            object EServerPort: TSpinEdit
              Left = 36
              Top = 0
              Width = 81
              Height = 22
              Align = alClient
              MaxValue = 0
              MinValue = 0
              TabOrder = 0
              Value = 0
            end
          end
          object rgServerKind: TRadioGroup
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 117
            Height = 45
            Align = alTop
            Caption = #26041#24335
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'TCP'
              'UDP')
            TabOrder = 1
          end
          object Panel6: TPanel
            AlignWithMargins = True
            Left = 3
            Top = 82
            Width = 117
            Height = 22
            Align = alTop
            BevelOuter = bvNone
            Caption = 'plPort1'
            ShowCaption = False
            TabOrder = 2
            object Label14: TLabel
              AlignWithMargins = True
              Left = 3
              Top = 3
              Width = 30
              Height = 16
              Align = alLeft
              AutoSize = False
              Caption = #24310#26102
              Layout = tlCenter
              ExplicitLeft = 0
              ExplicitTop = 6
            end
            object EServerTimeOut: TSpinEdit
              Left = 36
              Top = 0
              Width = 81
              Height = 22
              Align = alClient
              MaxValue = 0
              MinValue = 0
              TabOrder = 0
              Value = 0
            end
          end
        end
      end
      object rgServerReveived: TRadioGroup
        Left = 2
        Top = 177
        Width = 131
        Height = 105
        Align = alTop
        Caption = #22788#29702#26041#24335
        ItemIndex = 0
        Items.Strings = (
          #25509#25910#21518#19981#22788#29702
          #25509#25910#21518#33258#21160#36716#21457
          #25509#25910#21518#33258#21160#22238#22797
          #25509#25910#21518#20998#26512#22788#29702)
        TabOrder = 1
        OnClick = rgServerReveivedClick
      end
    end
    object rgDisplay: TRadioGroup
      Left = 1
      Top = 316
      Width = 135
      Height = 84
      Align = alTop
      Caption = #20449#24687#26174#31034#26684#24335
      ItemIndex = 0
      Items.Strings = (
        #25991#26412
        'HEX'
        'Hex+'#25991#26412)
      TabOrder = 4
    end
    object Panel9: TPanel
      AlignWithMargins = True
      Left = 4
      Top = 403
      Width = 129
      Height = 22
      Align = alTop
      BevelOuter = bvNone
      Caption = 'plPort1'
      ShowCaption = False
      TabOrder = 5
      object Label16: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 52
        Height = 16
        Align = alLeft
        AutoSize = False
        Caption = #26174#31034#34892#25968
        Layout = tlCenter
      end
      object EDisplayLineCount: TSpinEdit
        Left = 58
        Top = 0
        Width = 71
        Height = 22
        Align = alClient
        MaxValue = 0
        MinValue = 0
        TabOrder = 0
        Value = 0
      end
    end
  end
  object mLog: TMemo
    Left = 937
    Top = 0
    Width = 323
    Height = 793
    Align = alRight
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 93
    Top = 336
  end
end
