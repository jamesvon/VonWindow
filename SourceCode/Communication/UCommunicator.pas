unit UCommunicator;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.ComCtrls,
  Vcl.Samples.Spin, UCommunication, IniFiles;

type
  TReplyInfo = class
  private
    FParams: TStringList;
    FText: string;
    FTextKind: string;
    function GetParams(Key: string): string;
    procedure SetParams(Key: string; const Value: string);
    function GetSetting: string;
    procedure SetSetting(const Value: string);
  public
    constructor Create;
    destructor Destroy;
    procedure Clear;
    property Params[Key: string]: string read GetParams write SetParams;
  published
    property TextKind: string read FTextKind write FTextKind;
    property Text: string read FText write FText;
    property Setting: string read GetSetting write SetSetting;
  end;

  TFTransCommunication = class(TForm)
    Panel1: TPanel;
    Splitter1: TSplitter;
    Panel2: TPanel;
    btnClient: TButton;
    btnServer: TButton;
    Panel3: TPanel;
    btnSend: TButton;
    ESendText: TEdit;
    GroupBox1: TGroupBox;
    pcClient: TPageControl;
    TabSheet1: TTabSheet;
    FlowPanel1: TFlowPanel;
    Label1: TLabel;
    ECommPort: TComboBox;
    Label2: TLabel;
    ECommBaud: TComboBox;
    Label3: TLabel;
    ECommParity: TComboBox;
    Label4: TLabel;
    ECommBit: TComboBox;
    Label5: TLabel;
    ECommStp: TComboBox;
    TabSheet2: TTabSheet;
    plPort1: TPanel;
    Label12: TLabel;
    EClientPort: TSpinEdit;
    rgClientKind: TRadioGroup;
    rgClientReveived: TRadioGroup;
    GroupBox2: TGroupBox;
    pcServer: TPageControl;
    TabSheet3: TTabSheet;
    FlowPanel2: TFlowPanel;
    Label6: TLabel;
    ESerialPort: TComboBox;
    Label7: TLabel;
    ESerialBaud: TComboBox;
    Label8: TLabel;
    ESerialParity: TComboBox;
    Label9: TLabel;
    ESerialDataBit: TComboBox;
    Label10: TLabel;
    ESerialStopBit: TComboBox;
    TabSheet4: TTabSheet;
    Panel5: TPanel;
    Label11: TLabel;
    EServerPort: TSpinEdit;
    rgServerKind: TRadioGroup;
    rgServerReveived: TRadioGroup;
    PageControl3: TPageControl;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    mClientLog: TMemo;
    PageControl4: TPageControl;
    TabSheet8: TTabSheet;
    mSvrLog: TMemo;
    TabSheet9: TTabSheet;
    TabSheet10: TTabSheet;
    rgDisplay: TRadioGroup;
    Panel4: TPanel;
    Label13: TLabel;
    EClientHost: TEdit;
    Panel6: TPanel;
    Label14: TLabel;
    EServerTimeOut: TSpinEdit;
    Panel7: TPanel;
    Label15: TLabel;
    EClientTimeOut: TSpinEdit;
    Panel8: TPanel;
    rbText: TRadioButton;
    rbHex: TRadioButton;
    Panel9: TPanel;
    Label16: TLabel;
    EDisplayLineCount: TSpinEdit;
    mServerReply: TMemo;
    Timer1: TTimer;
    Panel10: TPanel;
    Label17: TLabel;
    ESendInterValue: TSpinEdit;
    Panel11: TPanel;
    pcServerReplyConditionKind: TPageControl;
    TabSheet11: TTabSheet;
    TabSheet12: TTabSheet;
    rgServerReplyLenCondition: TRadioGroup;
    EServerReplyCharCondition: TRadioGroup;
    Label18: TLabel;
    EServerReplyLenConditionParam: TSpinEdit;
    Panel14: TPanel;
    EServerReplyCharConditionKind: TRadioButton;
    RadioButton2: TRadioButton;
    EServerReplyCharConditionParam: TEdit;
    TabSheet13: TTabSheet;
    Panel12: TPanel;
    EServerReplyText: TEdit;
    Panel13: TPanel;
    rgServerAutoSendKind: TRadioGroup;
    btnServerAddReply: TButton;
    btnServerEditReply: TButton;
    btnServerDelReply: TButton;
    EServerReplyCharConditionStart: TSpinEdit;
    Label19: TLabel;
    Panel15: TPanel;
    pcClientReplyConditionKind: TPageControl;
    TabSheet14: TTabSheet;
    TabSheet15: TTabSheet;
    Label20: TLabel;
    rgClientReplyLenCondition: TRadioGroup;
    EClientReplyLenConditionParam: TSpinEdit;
    TabSheet16: TTabSheet;
    EClientReplyCharCondition: TRadioGroup;
    Panel16: TPanel;
    Label21: TLabel;
    EClientReplyCharConditionKind: TRadioButton;
    RadioButton3: TRadioButton;
    EClientReplyCharConditionStart: TSpinEdit;
    EClientReplyCharConditionParam: TEdit;
    Panel17: TPanel;
    EClientReplyText: TEdit;
    Panel18: TPanel;
    rgClientAutoSendKind: TRadioGroup;
    btnClientAddReply: TButton;
    btnClientEditReply: TButton;
    btnClientDelReply: TButton;
    mClientReply: TMemo;
    mLog: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure btnServerClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure btnClientClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure rgServerReveivedClick(Sender: TObject);
    procedure rgClientReveivedClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure btnServerAddReplyClick(Sender: TObject);
    procedure btnServerEditReplyClick(Sender: TObject);
    procedure btnServerDelReplyClick(Sender: TObject);
    procedure mServerReplyDblClick(Sender: TObject);
    procedure btnClientAddReplyClick(Sender: TObject);
    procedure mClientReplyDblClick(Sender: TObject);
    procedure btnClientEditReplyClick(Sender: TObject);
    procedure btnClientDelReplyClick(Sender: TObject);
  private
    { Private declarations }
    FReplyInfo: TReplyInfo;
    FServer: IVonCommunicationServer;
    FClient: IVonCommunicationClient;
    procedure EventOnServerReceive(var Data: array of byte; var Len: Integer; size: Integer);
    procedure EventOnClientReceive(var Data: array of byte; var Len: Integer; size: Integer);
    procedure EventOnServerLog(Sender: string; Info: string);
    procedure EventOnClientLog(Sender: string; Info: string);
    procedure EventOnServerError(Sender: string; Info: string);
    procedure EventOnClientError(Sender: string; Info: string);
    procedure EventOnServerConnected(ConnectInfo: string);
    procedure EventOnClientConnected(ConnectInfo: string);
    function GetServerReplySetting: string;
    function GetClientReplySetting: string;
    procedure Log(title: string; var Data: array of byte; Len: Integer; mLog: TMemo);
    function StrToData(S: string; var Data: array of byte;
      Size: Integer; IsHex: Boolean): integer;
  public
    { Public declarations }
  end;

var
  FTransCommunication: TFTransCommunication;

/// <summary>将byte转换按长度读取字符串</summary>
function BytesToStr(bytes: array of byte; Start: Integer = 0; len: Integer = 0): string; //overload;
/// <summary>将字符串转换为bytes</summary>
function StrToBytes(S: string; var bytes: array of byte; Size: Integer): Integer;

implementation

function BytesToStr(bytes: array of byte; Start, len: Integer): string;
var
  Bs: TBytes;
begin
  SetLength(Bs, Len - Start);
  Move(bytes[Start], Bs[0], Len - Start);
  Result:= TEncoding.Default.GetString(Bs);
end;

function StrToBytes(S: string; var bytes: array of byte; Size: Integer): Integer;
var
  Bs: TBytes;
begin
  Bs:= TEncoding.Default.GetBytes(S);
  if Length(Bs) < Size then Result:= Length(Bs) else Result:= Size;
  Move(Bs[0], bytes, Result);
end;

{ TReplyInfo }

procedure TReplyInfo.Clear;
begin
  FParams.Clear;
  FText:= '';
  FTextKind:= 'ORG';
end;

constructor TReplyInfo.Create;
begin
  FParams:= TStringList.Create;
end;

destructor TReplyInfo.Destroy;
begin
  FParams.Free;
end;

function TReplyInfo.GetParams(Key: string): string;
begin
  Result:= FParams.Values[Key];
end;

function TReplyInfo.GetSetting: string;
var
  I: Integer;
begin
  for I := 0 to FParams.Count - 1 do
    Result:= Result + FParams.KeyNames[I] + '(' + FParams.ValueFromIndex[I] + ')';
  Result:= Result + FTextKind + ':' + FText;
end;

procedure TReplyInfo.SetParams(Key: string; const Value: string);
begin
  FParams.Values[Key]:= Value;
end;

procedure TReplyInfo.SetSetting(const Value: string);
var
  P: PChar;
  K, V: string;
  function GetData(c: char): string;
  begin
    Result:= '';
    while(P^ <> c)and(P^ <> ':')do begin
      Result:= Result + P^;
      Inc(P);
    end;
  end;
begin
  FParams.Clear;
  P:= PChar(Value);
  while P^ <> ':' do begin
    K:= GetData('(');
    if(p^ <> ':')then Inc(P) else Break;
    V:= GetData(')'); Inc(P);
    FParams.Add(K + '=' + V);
  end;
  FTextKind:= K;
  FText:= StrPas(P + 1);
end;

{$R *.dfm}

procedure TFTransCommunication.FormCreate(Sender: TObject);
var
  I: Integer;
  S: string;
begin
  FReplyInfo:= TReplyInfo.Create;
  with TIniFile.Create(ExtractFilePath(Application.ExeName) + 'Trans.Ini')do try
    pcServer.ActivePageIndex:= ReadInteger('SERVER', 'TYPE', 0);
    rgServerKind.ItemIndex:= ReadInteger('SERVER', 'KIND', 0);
    EServerPort.Value:= ReadInteger('SERVER', 'Port', 0);
    EServerTimeOut.Value:= ReadInteger('SERVER', 'TimeOut', 0);
    ESerialPort.ItemIndex:= ReadInteger('SERVER', 'SerialPort', 0);
    ESerialBaud.ItemIndex:= ESerialBaud.Items.IndexOf(ReadString('SERVER', 'SerialBaud', '9600'));
    ESerialParity.ItemIndex:= ESerialParity.Items.IndexOf(ReadString('SERVER', 'SerialParity', '0'));
    ESerialDataBit.ItemIndex:= ESerialDataBit.Items.IndexOf(ReadString('SERVER', 'SerialDataBit', '8'));
    ESerialStopBit.ItemIndex:= ESerialStopBit.Items.IndexOf(ReadString('SERVER', 'SerialStopBit', '1'));
    rgServerAutoSendKind.ItemIndex:= ReadInteger('SERVER', 'AutoSendKind', 0);
    rgServerReveived.ItemIndex:= ReadInteger('SERVER', 'Reveived', 0);

    rgDisplay.ItemIndex:= ReadInteger('DISPLAY', 'Display', 0);
    EDisplayLineCount.Value:= ReadInteger('DISPLAY', 'LineCount', 0);

    pcClient.ActivePageIndex:= ReadInteger('CLIENT', 'TYPE', 0);
    rgClientKind.ItemIndex:= ReadInteger('CLIENT', 'KIND', 0);
    EClientPort.Value:= ReadInteger('CLIENT', 'Port', 0);
    EClientTimeOut.Value:= ReadInteger('CLIENT', 'TimeOut', 0);
    ECommPort.ItemIndex:= ReadInteger('CLIENT', 'SerialPort', 0);
    ECommBaud.ItemIndex:= ECommBaud.Items.IndexOf(ReadString('CLIENT', 'SerialBaud', '9600'));
    ECommParity.ItemIndex:= ECommParity.Items.IndexOf(ReadString('CLIENT', 'SerialParity', '0'));
    ECommBit.ItemIndex:= ECommBit.Items.IndexOf(ReadString('CLIENT', 'SerialDataBit', '8'));
    ECommStp.ItemIndex:= ECommStp.Items.IndexOf(ReadString('CLIENT', 'SerialStopBit', '1'));
    rgClientReveived.ItemIndex:= ReadInteger('CLIENT', 'Reveived', 0);
    rbText.Checked:= ReadBool('CLIENT', 'SendKind', false);
    rbHex.Checked:= not rbText.Checked;
    ESendText.Text:= ReadString('CLIENT', 'SendText', '');
    ESendInterValue.Value:= ReadInteger('CLIENT', 'InterValue', 5000);
    I := 0; S:= ReadString('ServerReply', '0', '');
    mServerReply.Lines.Clear;
    while S <> '' do begin
      mServerReply.Lines.Add(S);
      Inc(I);
      S:= ReadString('ServerReply', IntToStr(I), '');
    end;
    I := 0; S:= ReadString('ClientReply', '0', '');
    mClientReply.Lines.Clear;
    while S <> '' do begin
      mClientReply.Lines.Add(S);
      Inc(I);
      S:= ReadString('ClientReply', IntToStr(I), '');
    end;
  finally
    Free;
  end;
end;

procedure TFTransCommunication.FormDestroy(Sender: TObject);
var
  I: Integer;
  S: string;
begin
  if Assigned(FClient) and FClient.Connected then FClient.Disconnect;
  if Assigned(FServer) and FServer.Actived then FServer.Actived:= False;
  with TIniFile.Create(ExtractFilePath(Application.ExeName) + 'Trans.Ini')do try
    WriteInteger('SERVER', 'TYPE', pcServer.ActivePageIndex);
    WriteInteger('SERVER', 'KIND', rgServerKind.ItemIndex);
    WriteInteger('SERVER', 'Port', EServerPort.Value);
    WriteInteger('SERVER', 'TimeOut', EServerTimeOut.Value);
    WriteInteger('SERVER', 'SerialPort', ESerialPort.ItemIndex);
    WriteString('SERVER', 'SerialBaud', ESerialBaud.Text);
    WriteString('SERVER', 'SerialParity', ESerialParity.Text);
    WriteString('SERVER', 'SerialDataBit', ESerialDataBit.Text);
    WriteString('SERVER', 'SerialStopBit', ESerialStopBit.Text);
    WriteInteger('SERVER', 'Reveived', rgServerReveived.ItemIndex);
    WriteInteger('SERVER', 'AutoSendKind', rgServerAutoSendKind.ItemIndex);

    WriteInteger('DISPLAY', 'Display', rgDisplay.ItemIndex);
    WriteInteger('DISPLAY', 'LineCount', EDisplayLineCount.Value);

    WriteInteger('CLIENT', 'TYPE', pcClient.ActivePageIndex);
    WriteInteger('CLIENT', 'KIND', rgClientKind.ItemIndex);
    WriteInteger('CLIENT', 'Port', EClientPort.Value);
    WriteInteger('CLIENT', 'TimeOut', EClientTimeOut.Value);
    WriteInteger('CLIENT', 'SerialPort', ECommPort.ItemIndex);
    WriteString('CLIENT', 'SerialBaud', ECommBaud.Text);
    WriteString('CLIENT', 'SerialParity', ECommParity.Text);
    WriteString('CLIENT', 'SerialDataBit', ECommBit.Text);
    WriteString('CLIENT', 'SerialStopBit', ECommStp.Text);
    WriteInteger('CLIENT', 'Reveived', rgClientReveived.ItemIndex);
    WriteBool('CLIENT', 'SendKind', rbText.Checked);
    WriteString('CLIENT', 'SendText', ESendText.Text);
    WriteInteger('CLIENT', 'InterValue', ESendInterValue.Value);

    for I := 0 to mServerReply.Lines.Count - 1 do
      WriteString('ServerReply', IntToStr(I), mServerReply.Lines[I]);
    for I := 0 to mClientReply.Lines.Count - 1 do
      WriteString('ClientReply', IntToStr(I), mClientReply.Lines[I]);

  finally
    Free;
  end;
  FReplyInfo.Free;
end;

(* Global *)

function TFTransCommunication.StrToData(S: string; var Data: array of byte;
  Size: Integer; IsHex: Boolean): integer;
const HEXCHAR = '0123456789ABCDEF';
var
  I, Idx: Integer;
  b: Byte;
begin
  Result:= 0; Idx:= 0;
  if IsHex then
    while(Result < size)and(S[Idx + 1] <> #0)do begin
      b:= Pos(S[Idx + 1], HEXCHAR); Inc(Idx);
      if b = 0 then Continue;
      Data[Result]:= b - 1;
      b:= Pos(S[Idx + 1], HEXCHAR); Inc(Idx);
      if b > 0 then Data[Result]:= (Data[Result] shl 4) + b - 1;
      Inc(Result);
    end
  else
    Result:= StrToBytes(S, Data, Size);
//    while(Result < size)and(Result < Length(S))do begin
//      Data[Result]:= Ord(S[Result + 1]);
//      Inc(Result);
//    end
end;

procedure TFTransCommunication.Log(title: string; var Data: array of byte; Len: Integer; mLog: TMemo);
var
  I: Integer;
  S: string;
begin
  S:= '';
  mLog.Lines.Add(DateTimeToStr(Now) + ':' + Title);
  case rgDisplay.ItemIndex of
  0: begin
      for I := 0 to Len - 1 do
        S:= S + Char(Data[I]);
      mLog.Lines.Add(S);
  end;
  1: begin
      for I := 0 to Len - 1 do
        S:= S + ' ' + IntToHex(Data[I], 2);
      mLog.Lines.Add(S);
    end;
  2: begin
      for I := 0 to Len - 1 do
        S:= S + ' ' + IntToHex(Data[I], 2);
      mLog.Lines.Add(S);
      mLog.Lines.Add(BytesToStr(Data, 0, Len));
    end;
  end;
  if EDisplayLineCount.Value > 0 then
    while mLog.Lines.Count > EDisplayLineCount.Value do mLog.Lines.Delete(0);
end;

(* Server *)

procedure TFTransCommunication.btnServerClick(Sender: TObject);
begin
  if btnServer.Caption = '打开' then begin
    btnServer.Caption:= '关闭';
    case pcServer.ActivePageIndex of
    0: begin       //SerialPort
         FServer:= TVonSerialPort.Create;
         with FServer AS TVonSerialPort do begin
           Params['Port']:= ESerialPort.Text;
           Params['bps']:= ESerialBaud.Text;
           case ESerialParity.ItemIndex of
           0: Params['par']:= 'n';
           1: Params['par']:= 'n';
           end;
           Params['dbit']:= ESerialDataBit.Text;
           Params['sbit']:= ESerialStopBit.Text;
           Open;
         end;
      end;
    1: case rgServerKind.ItemIndex of
        0: begin   //TCP
          FServer:= TVonTCPServer.Create;
          with FServer AS TVonTCPServer do begin
            Params['Port']:= EServerPort.Text;
            TimeOut:= StrToInt(EServerTimeOut.Text);
            Open;
          end;
        end;
        1: begin   //UDP
          FServer:= TVonUDPServer.Create;
          with FServer AS TVonUDPServer do begin
            Params['Port']:= EServerPort.Text;
            TimeOut:= StrToInt(EServerTimeOut.Text);
            Open;
          end;
        end;
      end;
    end;
    FServer.OnConnected:= EventOnServerConnected;
    (FServer as TVonCommunicationBase).OnLog:= EventOnServerLog;
    (FServer as TVonCommunicationBase).OnError:= EventOnServerError;
    (FServer as TVonCommunicationBase).OnReceiver:= EventOnServerReceive;
    pcServer.Enabled:= False;
  end else begin
    btnServer.Caption:= '打开';
    FServer.Actived:= False;
    Sleep(1000);
    FreeAndNil(FServer);
    pcServer.Enabled:= True;
  end;
end;
          //TEventVonCommunicationReceiver = procedure (var Data: array of byte; var Len: Integer) of object;
procedure TFTransCommunication.EventOnServerReceive(var Data: array of byte; var Len: Integer; size: Integer);
//var
//  szData: array[0..1024] of byte;
//  szLen, szSize: Integer;
begin
  Log('Server received', Data, Len, mSvrLog);
  case rgServerReveived.ItemIndex of
  0: begin Len:= 0; end; //接收后不处理
  1: begin      //接收后自动转发
      if Assigned(FClient)and FClient.Connected then begin
        Log('转发', Data, Len, mClientLog);
        FClient.Send(Data, Len, size);
      end;
    end;
  2: begin      //接收后自动回复
      case rgServerAutoSendKind.ItemIndex of
      0, 1: Len:= StrToData(mServerReply.Text, Data, size, rgServerAutoSendKind.ItemIndex = 1);
      end;
      Log('回复', Data, Len, mSvrLog);
    end;
  3: begin end;  //接收后分析处理
  end;
end;

(* Server auto reply setting *)

function TFTransCommunication.GetServerReplySetting: string;
begin
  FReplyInfo.Clear;
  case pcServerReplyConditionKind.ActivePageIndex of
  0: FReplyInfo.Params['Kind']:= 'n';
  1: begin
      FReplyInfo.Params['Kind']:= 'l';
      FReplyInfo.Params['Condition']:= rgServerReplyLenCondition.Items[rgServerReplyLenCondition.ItemIndex];
      FReplyInfo.Params['PARAM']:= IntToStr(EServerReplyLenConditionParam.Value);
    end;
  2: begin
      FReplyInfo.Params['Kind']:= 'c';
      FReplyInfo.Params['Condition']:= EServerReplyCharCondition.Items[EServerReplyCharCondition.ItemIndex];
      FReplyInfo.Params['START']:= IntToStr(EServerReplyCharConditionStart.Value);
      FReplyInfo.Params['CHAR']:= EServerReplyCharConditionParam.Text;
    end;
  end;
  case rgServerAutoSendKind.ItemIndex of
  0: FReplyInfo.TextKind:= 'TXT';
  1: FReplyInfo.TextKind:= 'HEX';
  2: FReplyInfo.TextKind:= 'ORG';
  end;
  FReplyInfo.Text:= EServerReplyText.Text;
  Result:= FReplyInfo.Setting
end;

procedure TFTransCommunication.btnServerAddReplyClick(Sender: TObject);
begin
  if Sender = btnServerAddReply then
    mServerReply.Lines.Add(GetServerReplySetting)
  else mClientReply.Lines.Add(GetServerReplySetting);
end;

procedure TFTransCommunication.mServerReplyDblClick(Sender: TObject);
var
  idx: Integer;
begin
  FReplyInfo.Clear;
  FReplyInfo.Setting:= mServerReply.Lines[mServerReply.CaretPos.Y];
  if FReplyInfo.Params['Kind'] = 'n' then begin
    pcServerReplyConditionKind.ActivePageIndex:= 0;
  end else if FReplyInfo.Params['Kind'] = 'l' then begin
   pcServerReplyConditionKind.ActivePageIndex:= 1;
    rgServerReplyLenCondition.ItemIndex:= rgServerReplyLenCondition.Items.IndexOf(FReplyInfo.Params['Condition']);
    EServerReplyLenConditionParam.Value:= StrToInt(FReplyInfo.Params['PARAM']);
  end else if FReplyInfo.Params['Kind'] = 'c' then begin
    pcServerReplyConditionKind.ActivePageIndex:= 2;
    EServerReplyCharCondition.ItemIndex:= EServerReplyCharCondition.Items.IndexOf(FReplyInfo.Params['Condition']);
    EServerReplyCharConditionStart.Value:= StrToInt(FReplyInfo.Params['START']);
    EServerReplyCharConditionParam.Text:= FReplyInfo.Params['CHAR'];
  end;
  if FReplyInfo.TextKind = 'TXT' then rgServerAutoSendKind.ItemIndex:= 0
  else if FReplyInfo.TextKind = 'HEX' then rgServerAutoSendKind.ItemIndex:= 1
  else rgServerAutoSendKind.ItemIndex:= 2;
  EServerReplyText.Text:= FReplyInfo.Text;
end;

procedure TFTransCommunication.btnServerEditReplyClick(Sender: TObject);
begin
  mServerReply.Lines[mServerReply.CaretPos.Y]:= GetServerReplySetting;
end;

procedure TFTransCommunication.btnServerDelReplyClick(Sender: TObject);
begin
  mServerReply.Lines.Delete(mServerReply.CaretPos.Y);
end;

(* Client auto reply setting *)

function TFTransCommunication.GetClientReplySetting: string;
begin
  FReplyInfo.Clear;
  case pcClientReplyConditionKind.ActivePageIndex of
  0: FReplyInfo.Params['Kind']:= 'n';
  1: begin
      FReplyInfo.Params['Kind']:= 'l';
      FReplyInfo.Params['Condition']:= rgClientReplyLenCondition.Items[rgClientReplyLenCondition.ItemIndex];
      FReplyInfo.Params['PARAM']:= IntToStr(EClientReplyLenConditionParam.Value);
    end;
  2: begin
      FReplyInfo.Params['Kind']:= 'c';
      FReplyInfo.Params['Condition']:= EClientReplyCharCondition.Items[EClientReplyCharCondition.ItemIndex];
      FReplyInfo.Params['START']:= IntToStr(EClientReplyCharConditionStart.Value);
      FReplyInfo.Params['CHAR']:= EClientReplyCharConditionParam.Text;
    end;
  end;
  case rgClientAutoSendKind.ItemIndex of
  0: FReplyInfo.TextKind:= 'TXT';
  1: FReplyInfo.TextKind:= 'HEX';
  2: FReplyInfo.TextKind:= 'ORG';
  end;
  FReplyInfo.Text:= EClientReplyText.Text;
  Result:= FReplyInfo.Setting
end;

procedure TFTransCommunication.mClientReplyDblClick(Sender: TObject);
var
  idx: Integer;
begin
  FReplyInfo.Clear;
  FReplyInfo.Setting:= mClientReply.Lines[mClientReply.CaretPos.Y];
  if FReplyInfo.Params['Kind'] = 'n' then begin
    pcClientReplyConditionKind.ActivePageIndex:= 0;
  end else if FReplyInfo.Params['Kind'] = 'l' then begin
   pcClientReplyConditionKind.ActivePageIndex:= 1;
    rgClientReplyLenCondition.ItemIndex:= rgClientReplyLenCondition.Items.IndexOf(FReplyInfo.Params['Condition']);
    EClientReplyLenConditionParam.Value:= StrToInt(FReplyInfo.Params['PARAM']);
  end else if FReplyInfo.Params['Kind'] = 'c' then begin
    pcClientReplyConditionKind.ActivePageIndex:= 2;
    EClientReplyCharCondition.ItemIndex:= EClientReplyCharCondition.Items.IndexOf(FReplyInfo.Params['Condition']);
    EClientReplyCharConditionStart.Value:= StrToInt(FReplyInfo.Params['START']);
    EClientReplyCharConditionParam.Text:= FReplyInfo.Params['CHAR'];
  end;
  if FReplyInfo.TextKind = 'TXT' then rgClientAutoSendKind.ItemIndex:= 0
  else if FReplyInfo.TextKind = 'HEX' then rgClientAutoSendKind.ItemIndex:= 1
  else rgClientAutoSendKind.ItemIndex:= 2;
  EClientReplyText.Text:= FReplyInfo.Text;
end;

procedure TFTransCommunication.btnClientAddReplyClick(Sender: TObject);
begin
  mClientReply.Lines.Add(GetClientReplySetting);
end;

procedure TFTransCommunication.btnClientEditReplyClick(Sender: TObject);
begin
  mClientReply.Lines[mClientReply.CaretPos.Y]:= GetClientReplySetting;
end;

procedure TFTransCommunication.btnClientDelReplyClick(Sender: TObject);
begin
  mClientReply.Lines.Delete(mClientReply.CaretPos.Y);
end;

(* Client *)

procedure TFTransCommunication.btnClientClick(Sender: TObject);
begin
  if btnClient.Caption = '连接' then begin
    case pcClient.ActivePageIndex of
    0: begin       //SerialPort
         FClient:= TVonSerialPort.Create;
         with FClient AS TVonSerialPort do begin
           Params['Port']:= ECommPort.Text;
           Params['bps']:= ECommBaud.Text;
           case ECommParity.ItemIndex of
           0: Params['par']:= 'n';
           1: Params['par']:= 'n';
           end;
           Params['dbit']:= ECommBit.Text;
           Params['sbit']:= ECommStp.Text;
           Connect;
         end;
       end;
    1: case rgClientKind.ItemIndex of
        0: begin   //TCP
          FClient:= TVonTCPClient.Create;
          with FClient as TVonTCPClient do begin
            Params['Host']:= EClientHost.Text;
            Params['Port']:= EClientPort.Text;
            TimeOut:= StrToInt(EClientTimeOut.Text);
            Connect;
          end;
        end;
        1: begin   //UDP
          FClient:= TVonUDPClient.Create;
          with FClient as TVonUDPClient do begin
            Params['Host']:= EClientHost.Text;
            Params['Port']:= EClientPort.Text;
            TimeOut:= StrToInt(EClientTimeOut.Text);
            Connect;
          end;
        end;
      end;
    end;
    if not FClient.Connected then raise Exception.Create('连接失败！');
    pcClient.Enabled:= False;
    (FClient as TVonCommunicationBase).OnReceiver:= EventOnClientReceive;
    btnClient.Caption:= '断开';
  end else begin
    btnClient.Caption:= '连接';
    FClient.Disconnect;
    Sleep(1000);
    FreeAndNil(FClient);
    pcClient.Enabled:= True;
  end;
end;

procedure TFTransCommunication.rgClientReveivedClick(Sender: TObject);
begin
  Timer1.Enabled:= False;
  case rgClientReveived.ItemIndex of
  0: begin end;
  1: begin Timer1.Interval:= ESendInterValue.Value; Timer1.Enabled:= true; end;
  end;
end;

procedure TFTransCommunication.rgServerReveivedClick(Sender: TObject);
begin
  if rgServerReveived.ItemIndex = 1 then rgClientReveived.ItemIndex:= 4;
  rgClientReveived.Enabled:= rgServerReveived.ItemIndex <> 1;
  rgClientReveivedClick(nil);
end;

procedure TFTransCommunication.btnSendClick(Sender: TObject);
var
  Data: array[0..1024] of byte;
  Len, size: Integer;
begin
  Len:= StrToData(ESendText.Text, Data, 1024, rbHex.Checked);
  size:= 1024;
  if Assigned(FClient) and FClient.Connected then begin
    Log('发送', Data, Len, mClientLog);
    FClient.Send(Data, len, 1024);
  end;
end;

procedure TFTransCommunication.Timer1Timer(Sender: TObject);
begin
  btnSendClick(nil);
end;

(* Trans *)

procedure TFTransCommunication.EventOnClientReceive(var Data: array of byte;
  var Len: Integer; size: Integer);
begin
  Log('客户端接收', Data, Len, mClientLog);
  case rgServerReveived.ItemIndex of
  0: begin end;
  1: begin end;
  2: begin end;
  3: begin end;
  4: begin end;
  end;
end;

procedure TFTransCommunication.EventOnServerConnected(ConnectInfo: string);
begin
  mLog.Lines.Add('SVR ====' + DateTimeToStr(Now) + ':' + ConnectInfo + '====');
  if EDisplayLineCount.Value > 0 then
    while mLog.Lines.Count > EDisplayLineCount.Value do mLog.Lines.Delete(0);
end;

procedure TFTransCommunication.EventOnServerError(Sender, Info: string);
begin
  mLog.Lines.Add('SVR !!!!' + DateTimeToStr(Now) + ':' + Sender);
  mLog.Lines.Add('    ' + Info);
  if EDisplayLineCount.Value > 0 then
    while mLog.Lines.Count > EDisplayLineCount.Value do mLog.Lines.Delete(0);
end;

procedure TFTransCommunication.EventOnServerLog(Sender, Info: string);
begin
  mLog.Lines.Add('SVR ----' + DateTimeToStr(Now) + ':' + Sender);
  mLog.Lines.Add('    ' + Info);
  if EDisplayLineCount.Value > 0 then
    while mLog.Lines.Count > EDisplayLineCount.Value do mLog.Lines.Delete(0);
end;

procedure TFTransCommunication.EventOnClientConnected(ConnectInfo: string);
begin
  mLog.Lines.Add('CLT ====' + DateTimeToStr(Now) + ':' + ConnectInfo + '====');
  if EDisplayLineCount.Value > 0 then
    while mLog.Lines.Count > EDisplayLineCount.Value do mLog.Lines.Delete(0);
end;

procedure TFTransCommunication.EventOnClientError(Sender, Info: string);
begin
  mLog.Lines.Add('CLT !!!!' + DateTimeToStr(Now) + ':' + Sender);
  mLog.Lines.Add('    ' + Info);
  if EDisplayLineCount.Value > 0 then
    while mLog.Lines.Count > EDisplayLineCount.Value do mLog.Lines.Delete(0);
end;

procedure TFTransCommunication.EventOnClientLog(Sender, Info: string);
begin
  mLog.Lines.Add('CLT ----' + DateTimeToStr(Now) + ':' + Sender);
  mLog.Lines.Add('    ' + Info);
  if EDisplayLineCount.Value > 0 then
    while mLog.Lines.Count > EDisplayLineCount.Value do mLog.Lines.Delete(0);
end;

end.
