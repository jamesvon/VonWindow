object FDlgCommunication: TFDlgCommunication
  Left = 0
  Top = 0
  Caption = 'FDlgCommunication'
  ClientHeight = 298
  ClientWidth = 200
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 24
    Top = 24
    Width = 156
    Height = 241
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    object Panel2: TPanel
      Left = 56
      Top = 160
      Width = 100
      Height = 81
      Align = alRight
      BevelOuter = bvNone
      Caption = 'Panel2'
      ShowCaption = False
      TabOrder = 0
      object Button1: TButton
        AlignWithMargins = True
        Left = 3
        Top = 8
        Width = 94
        Height = 32
        Align = alBottom
        Caption = #30830#23450
        Default = True
        ImageIndex = 19
        ImageMargins.Left = 5
        Images = FCoteDB.ImgButton
        ModalResult = 1
        TabOrder = 0
        OnClick = Button1Click
      end
      object Button2: TButton
        AlignWithMargins = True
        Left = 3
        Top = 46
        Width = 94
        Height = 32
        Align = alBottom
        Cancel = True
        Caption = #25918#24323
        ImageIndex = 20
        ImageMargins.Left = 5
        Images = FCoteDB.ImgButton
        ModalResult = 2
        TabOrder = 1
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 128
      Width = 156
      Height = 32
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Panel3'
      ShowCaption = False
      TabOrder = 1
      object Label4: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 48
        Height = 26
        Align = alLeft
        Caption = #25968#25454#20301#25968
        Layout = tlCenter
        ExplicitHeight = 13
      end
      object EDatabits: TComboBox
        AlignWithMargins = True
        Left = 57
        Top = 3
        Width = 96
        Height = 21
        Align = alClient
        Style = csDropDownList
        ItemIndex = 1
        TabOrder = 0
        Text = '8'
        Items.Strings = (
          '7'
          '8')
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 32
      Width = 156
      Height = 32
      Align = alTop
      AutoSize = True
      BevelOuter = bvNone
      Caption = 'Panel3'
      ShowCaption = False
      TabOrder = 2
      object Label6: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 48
        Height = 26
        Align = alLeft
        Caption = #31471#21475#36895#29575
        Layout = tlCenter
        ExplicitHeight = 13
      end
      object ECommRate: TComboBox
        AlignWithMargins = True
        Left = 57
        Top = 3
        Width = 96
        Height = 21
        Align = alClient
        Style = csDropDownList
        ItemIndex = 0
        TabOrder = 0
        Text = '300'
        Items.Strings = (
          '300'
          '1200'
          '2400'
          '4800'
          '9600'
          '19200'
          '38400'
          '57600'
          '115200')
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 156
      Height = 32
      Align = alTop
      AutoSize = True
      BevelOuter = bvNone
      Caption = 'Panel3'
      ShowCaption = False
      TabOrder = 3
      object Label3: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 48
        Height = 26
        Align = alLeft
        Caption = #37319#38598#31471#21475
        Layout = tlCenter
        ExplicitHeight = 13
      end
      object EComm: TComboBox
        AlignWithMargins = True
        Left = 57
        Top = 3
        Width = 96
        Height = 21
        Align = alClient
        Style = csDropDownList
        ItemIndex = 0
        TabOrder = 0
        Text = 'COM1'
        Items.Strings = (
          'COM1'
          'COM2'
          'COM3'
          'COM4'
          'COM5'
          'COM6'
          'COM7'
          'COM8'
          'COM9'
          'COM10'
          'COM11'
          'COM12'
          'COM13'
          'COM14'
          'COM15'
          'COM16'
          'COM17'
          'COM18'
          'COM19'
          'COM20')
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 96
      Width = 156
      Height = 32
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Panel3'
      ShowCaption = False
      TabOrder = 4
      object Label2: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 48
        Height = 26
        Align = alLeft
        Caption = #20572#27490#20301#25968
        Layout = tlCenter
        ExplicitHeight = 13
      end
      object EStopbits: TComboBox
        AlignWithMargins = True
        Left = 57
        Top = 3
        Width = 96
        Height = 21
        Align = alClient
        Style = csDropDownList
        ItemIndex = 0
        TabOrder = 0
        Text = '0'
        Items.Strings = (
          '0'
          '1'
          '2')
      end
    end
    object Panel7: TPanel
      Left = 0
      Top = 64
      Width = 156
      Height = 32
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Panel3'
      ShowCaption = False
      TabOrder = 5
      object Label1: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 48
        Height = 26
        Align = alLeft
        Caption = #26816#39564#26041#24335
        Layout = tlCenter
        ExplicitHeight = 13
      end
      object EParity: TComboBox
        AlignWithMargins = True
        Left = 57
        Top = 3
        Width = 96
        Height = 21
        Align = alClient
        Style = csDropDownList
        ItemIndex = 0
        TabOrder = 0
        Text = 'n'#65306#26080
        Items.Strings = (
          'n'#65306#26080
          'o'#65306#22855#26657#39564
          'e'#65306#20598#26657#39564
          's'#65306#26631#35760)
      end
    end
  end
end
