unit UDlgCommunication;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TFDlgCommunication = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    Button2: TButton;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Label3: TLabel;
    EComm: TComboBox;
    Label6: TLabel;
    ECommRate: TComboBox;
    Panel6: TPanel;
    Panel7: TPanel;
    Label1: TLabel;
    EParity: TComboBox;
    Label2: TLabel;
    EStopbits: TComboBox;
    Label4: TLabel;
    EDatabits: TComboBox;
    procedure Button1Click(Sender: TObject);
  private
    FBaudRate: Integer;
    FParity: char;
    FStopBits: word;
    FPort: string;
    FByteSize: word;
    { Private declarations }
  public
    { Public declarations }
    function ShowDlg(port: string; bps: longint; par: char; dbit, sbit: byte): Integer;
  published
    property Port: string read FPort;           //端口号
    property BaudRate: Integer read FBaudRate;  //波特率：110 300 600 1200 2400 4800 14400 19200 38400 56000 57600 115200 128000 256000
    property Parity: char read FParity;         //校验位: 'n'=no,'o'=odd,'e'=even,'m'=mark,'s'=space
    property ByteSize: word read FByteSize;     //数据位： 4-8 (other=8)
    property StopBits: word read FStopBits;     //停止位： 1-2 (other=1.5)
  end;

var
  FDlgCommunication: TFDlgCommunication;

implementation

{$R *.dfm}

{ TFDlgCommunication }

procedure TFDlgCommunication.Button1Click(Sender: TObject);
begin
  FPort:= EComm.Text;
  FBaudRate:= StrToInt(ECommRate.Text);
  FParity:= EParity.Text[1];
  FByteSize:= StrToInt(EDatabits.Text);
  FStopBits:= StrToInt(EStopbits.Text);
end;

function TFDlgCommunication.ShowDlg(port: string; bps: longint; par: char; dbit, sbit: byte): Integer;
begin
  EComm.ItemIndex:= EComm.Items.IndexOf(port);
  ECommRate.ItemIndex:= ECommRate.Items.IndexOf(intToStr(bps));
  case par of
  'n': EParity.ItemIndex:= 0;
  'o': EParity.ItemIndex:= 1;
  'e': EParity.ItemIndex:= 2;
  's': EParity.ItemIndex:= 3;
  end;
  EStopbits.ItemIndex:= EStopbits.Items.IndexOf(IntToStr(sbit));
  EDatabits.ItemIndex:= EDatabits.Items.IndexOf(IntToStr(dbit));
  Result:= ShowModal;
end;

end.
