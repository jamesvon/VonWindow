object FDlgSerialPort: TFDlgSerialPort
  Left = 0
  Top = 0
  Caption = #35774#32622#20018#21475
  ClientHeight = 244
  ClientWidth = 205
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object gbComm: TGroupBox
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 199
    Height = 238
    Align = alTop
    Caption = #36890#35759#35774#32622
    TabOrder = 0
    object Panel1: TPanel
      AlignWithMargins = True
      Left = 5
      Top = 18
      Width = 189
      Height = 32
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Panel1'
      ShowCaption = False
      TabOrder = 0
      object Label12: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 48
        Height = 26
        Align = alLeft
        Caption = #37319#38598#31471#21475
        Layout = tlCenter
        ExplicitHeight = 13
      end
      object EComm: TComboBox
        AlignWithMargins = True
        Left = 57
        Top = 3
        Width = 129
        Height = 27
        Align = alClient
        Style = csOwnerDrawFixed
        ItemHeight = 21
        ItemIndex = 0
        TabOrder = 0
        Text = 'COM1'
        Items.Strings = (
          'COM1'
          'COM2'
          'COM3'
          'COM4'
          'COM5'
          'COM6'
          'COM7'
          'COM8'
          'COM9'
          'COM10'
          'COM11'
          'COM12'
          'COM13'
          'COM14'
          'COM15'
          'COM16'
          'COM17'
          'COM18'
          'COM19')
      end
    end
    object Panel2: TPanel
      AlignWithMargins = True
      Left = 5
      Top = 56
      Width = 189
      Height = 32
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Panel2'
      ShowCaption = False
      TabOrder = 1
      object Label13: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 48
        Height = 26
        Align = alLeft
        Caption = #31471#21475#36895#29575
        Layout = tlCenter
        ExplicitHeight = 13
      end
      object ECommRate: TComboBox
        AlignWithMargins = True
        Left = 57
        Top = 3
        Width = 129
        Height = 27
        Align = alClient
        Style = csOwnerDrawFixed
        ItemHeight = 21
        ItemIndex = 4
        TabOrder = 0
        Text = '9600'
        Items.Strings = (
          '300'
          '1200'
          '2400'
          '4800'
          '9600'
          '19200'
          '38400'
          '57600'
          '115200')
      end
    end
    object Panel3: TPanel
      AlignWithMargins = True
      Left = 5
      Top = 94
      Width = 189
      Height = 32
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Panel2'
      ShowCaption = False
      TabOrder = 2
      object Label1: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 48
        Height = 26
        Align = alLeft
        Alignment = taRightJustify
        AutoSize = False
        Caption = #25968#25454#20301
        Layout = tlCenter
      end
      object EBit: TComboBox
        AlignWithMargins = True
        Left = 57
        Top = 3
        Width = 129
        Height = 27
        Align = alClient
        Style = csOwnerDrawFixed
        ItemHeight = 21
        ItemIndex = 3
        TabOrder = 0
        Text = '8'
        Items.Strings = (
          '5'
          '6'
          '7'
          '8')
      end
    end
    object Panel4: TPanel
      AlignWithMargins = True
      Left = 5
      Top = 132
      Width = 189
      Height = 32
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Panel2'
      ShowCaption = False
      TabOrder = 3
      object Label2: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 48
        Height = 26
        Align = alLeft
        Alignment = taRightJustify
        AutoSize = False
        Caption = #20572#27490#20301
        Layout = tlCenter
        ExplicitHeight = 13
      end
      object ComboBox2: TComboBox
        AlignWithMargins = True
        Left = 57
        Top = 3
        Width = 129
        Height = 27
        Align = alClient
        Style = csOwnerDrawFixed
        ItemHeight = 21
        ItemIndex = 0
        TabOrder = 0
        Text = '0'
        Items.Strings = (
          '0'
          '1')
      end
    end
    object Panel5: TPanel
      AlignWithMargins = True
      Left = 5
      Top = 170
      Width = 189
      Height = 32
      Align = alTop
      BevelOuter = bvNone
      Caption = 'Panel2'
      ShowCaption = False
      TabOrder = 4
      object Label3: TLabel
        AlignWithMargins = True
        Left = 3
        Top = 3
        Width = 60
        Height = 26
        Align = alLeft
        Caption = #22855#20598#26657#39564#20301
        Layout = tlCenter
        ExplicitHeight = 13
      end
      object ComboBox3: TComboBox
        AlignWithMargins = True
        Left = 69
        Top = 3
        Width = 117
        Height = 27
        Align = alClient
        Style = csOwnerDrawFixed
        ItemHeight = 21
        ItemIndex = 0
        TabOrder = 0
        Text = #20598
        Items.Strings = (
          #20598
          #22855
          #39640
          #20302)
      end
    end
    object BitBtn1: TBitBtn
      AlignWithMargins = True
      Left = 38
      Top = 208
      Width = 75
      Height = 25
      Align = alRight
      Caption = #25171#24320
      Kind = bkOK
      NumGlyphs = 2
      TabOrder = 5
    end
    object BitBtn2: TBitBtn
      AlignWithMargins = True
      Left = 119
      Top = 208
      Width = 75
      Height = 25
      Align = alRight
      Caption = #25918#24323
      Kind = bkCancel
      NumGlyphs = 2
      TabOrder = 6
    end
  end
end
