unit UDlgSerialPort;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, UCoteComm;

type
  TFDlgSerialPort = class(TForm)
    gbComm: TGroupBox;
    Panel1: TPanel;
    Label12: TLabel;
    EComm: TComboBox;
    Panel2: TPanel;
    Label13: TLabel;
    ECommRate: TComboBox;
    Panel3: TPanel;
    Label1: TLabel;
    EBit: TComboBox;
    Panel4: TPanel;
    Label2: TLabel;
    ComboBox2: TComboBox;
    Panel5: TPanel;
    Label3: TLabel;
    ComboBox3: TComboBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
  private
    { Private declarations }
  public
    { Public declarations }
    function OpenComm(
      EventOfCollection: TEventAfterCollectionReceived;
      EventOfRead: TEventAfterReadReceived;
      EventOfWrite: TEventAfterWriteReceived): TJZCommBase;
  end;

var
  FDlgSerialPort: TFDlgSerialPort;

implementation

uses UCoteDB, UVonSystemFuns;

{$R *.dfm}


{ TFDlgReadRing }

function TFDlgSerialPort.OpenComm(
  EventOfCollection: TEventAfterCollectionReceived;
  EventOfRead: TEventAfterReadReceived;
  EventOfWrite: TEventAfterWriteReceived): TJZCommBase;
begin
  ReadAppItems('COMMUNICATION', 'PortList', '', EComm.Items);
  EComm.ItemIndex := EComm.Items.IndexOf(ReadAppConfig('COMMUNICATION', 'Port'));
  ECommRate.ItemIndex := ECommRate.Items.IndexOf(ReadAppConfig('COMMUNICATION',
    'Speed'));
  if ShowModal = mrOK then begin
    Result:= FCoteDB.CreateComm(EventOfCollection, EventOfRead, EventOfWrite, true);
    Result.Open(EComm.ItemIndex + 1, StrToInt(ECommRate.Text), 'n', 8, 1);
    WriteAppConfig('COMMUNICATION', 'Port', EComm.Text);
    WriteAppConfig('COMMUNICATION', 'Speed', ECommRate.Text);
  end else begin
    if Assigned(Result) then Result.Free;
  end;
end;

end.
