unit UIDREader;

interface

uses
  SysUtils, Windows, Forms;

const
  DLL_SDTAPI = 'sdtapi.dll';
  DLL_WLTRS = 'WltRS.dll';
  // 提示信息
  TIP_TITLE = '提示';
  TIP_ICREADER_NO_CARD = '未放卡或者卡未放好，请重新放卡！';
  TIP_ICREADER_READ_FAILED = '读卡失败！';
  TIP_ICREADER_CALL_DLL_FAILED = '调用sdtapi.dll错误！';
  TIP_ICREADER_PICTURE_DECODE_FAILED = '相片解码错误！';
  TIP_ICREADER_WLT_FILE_EXTEND_FAILED = 'wlt文件后缀错误！';
  TIP_ICREADER_WLT_FILE_OPEN_FAILED = 'wlt文件打开错误！';
  TIP_ICREADER_WLT_FILE_FORMAT_FAILED = 'wlt文件格式错误！';
  TIP_ICREADER_NO_LICENSE = '软件未授权！';
  TIP_ICREADER_DEVICE_FAILED = '设备连接错误！';
  TIP_PRINT_NO_CARD_FOUND = '未找到身份证相关信息，请将证件放在天线上后重试！';
  TIP_ICREADER_SAVE_SUCCESS = '保存成功，身份信息已自动录入本地数据库！';
  TIP_ICREADER_BLACK_CARD_FOUND = '发现黑名单内包括的人员！';
  ERROR_ICREADER_OPEN_PORT = '端口打开失败，请检测相应的端口或者重新连接读卡器！';

type
  TIdCardInfo = packed record
    Name: array [0 .. 14] of WideChar;
    Sex: array [0 .. 0] of WideChar;
    Nation: array [0 .. 1] of WideChar;
    BirthDay: array [0 .. 7] of WideChar;
    Addr: array [0 .. 34] of WideChar;
    IdSN: array [0 .. 17] of WideChar;
    IssueOrgan: array [0 .. 14] of WideChar;
    VaildBegin: array [0 .. 7] of WideChar;
    VaildEnd: array [0 .. 7] of WideChar;
    theNewestAddr: array [0 .. 34] of WideChar;
  end;

function SDT_OpenPort(iPortID: Integer): Integer; stdcall;
  external DLL_SDTAPI name 'SDT_OpenPort';
function SDT_ClosePort(iPortID: Integer): Integer; stdcall;
  external DLL_SDTAPI name 'SDT_ClosePort';
function SDT_StartFindIDCard(iPortID: Integer; var pucIIN: Integer;
  iIfOpen: Integer): Integer; stdcall;
  external DLL_SDTAPI name 'SDT_StartFindIDCard';
function SDT_SelectIDCard(iPortID: Integer; var pucIIN: Integer;
  iIfOpen: Integer): Integer; stdcall;
  external DLL_SDTAPI name 'SDT_SelectIDCard';
function SDT_ReadBaseMsg(iPortID: Integer; pucCHMsg: PAnsiChar;
  var puiCHMsgLen: Integer; pucPHMsg: PAnsiChar; var puiPHMsgLen: Integer;
  iIfOpen: Integer): Integer; stdcall;
  external DLL_SDTAPI name 'SDT_ReadBaseMsg';
function SDT_ReadBaseMsgToFile(iPortID: Integer; fileName1: PAnsiChar;
  var puiCHMsgLen: Integer; fileName2: PAnsiChar; var puiPHMsgLen: Integer;
  iIfOpen: Integer): Integer; stdcall;
  external DLL_SDTAPI name 'SDT_ReadBaseMsgToFile';

function ReadICCard(var ACardInfo: TIdCardInfo; var AErrMsg: string): Boolean;

implementation

uses
  Classes, Dialogs;

var
  LstEthnic: TStrings;

function EthnicNoToName(ANo: string): string;
begin
  Result := LstEthnic.Values[ANo];
end;

function FormatDateStr(AValue: string): string;
begin
  Result := Copy(AValue, 1, 4) + '-' + Copy(AValue, 5, 2) + '-' +
    Copy(AValue, 7, 2);
end;

function ReadICCard(var ACardInfo: TIdCardInfo; var AErrMsg: string): Boolean;
var
  iPort: Integer;
  intOpenPortRtn: Integer;
  bUsbPort: Boolean;
  EdziPortID: Integer;
  iRet: Integer;
  pucIIN: Integer;
  EdziIfOpen: Integer;
  pucSN: Integer;
  puiCHMsgLen: Integer;
  puiPHMsgLen: Integer;
  fs: TFileStream;
  idCardInfo: TIdCardInfo;
  // FileHandle: Integer;
begin
  AErrMsg := '';
  // Result:= False;
  bUsbPort := False;
  EdziIfOpen := 1;
  EdziPortID := 0;
  puiCHMsgLen := 0;
  puiPHMsgLen := 0;
  // 检测usb口的机具连接，必须先检测usb
  for iPort := 1001 to 1016 do
  begin
    intOpenPortRtn := SDT_OpenPort(iPort);
    if intOpenPortRtn = 144 then
    begin
      EdziPortID := iPort;
      bUsbPort := true;
      break;
    end;
  end;
  // 检测串口的机具连接
  if not bUsbPort then
  begin
    for iPort := 1 to 2 do
    begin
      intOpenPortRtn := SDT_OpenPort(iPort);
      if intOpenPortRtn = 144 then
      begin
        EdziPortID := iPort;
        bUsbPort := False;
        break;
      end;
    end;
  end;
  if intOpenPortRtn <> 144 then
  begin
    // Application.MessageBox(ERROR_ICREADER_OPEN_PORT, ERROR_TITLE);
    AErrMsg := ERROR_ICREADER_OPEN_PORT;
    Result := False;
    Exit;
  end;
  // 下面找卡
  iRet := SDT_StartFindIDCard(EdziPortID, pucIIN, EdziIfOpen);
  if iRet <> 159 then
  begin
    iRet := SDT_StartFindIDCard(EdziPortID, pucIIN, EdziIfOpen); // 再找卡
    if iRet <> 159 then
    begin
      SDT_ClosePort(EdziPortID);
      // Application.MessageBox(TIP_ICREADER_NO_CARD, TIP_TITLE);
      AErrMsg := TIP_ICREADER_NO_CARD;
      Result := False;
      Exit;
    end;
  end;
  // 选卡
  iRet := SDT_SelectIDCard(EdziPortID, pucSN, EdziIfOpen);
  if iRet <> 144 then
  begin
    iRet := SDT_SelectIDCard(EdziPortID, pucSN, EdziIfOpen);
    if iRet <> 144 then
    begin
      SDT_ClosePort(EdziPortID);
      // Application.MessageBox(TIP_ICREADER_READ_FAILED, TIP_TITLE);
      AErrMsg := TIP_ICREADER_READ_FAILED;
      Result := False;
      Exit;
    end;
  end;
  // 注意，在这里，用户必须有应用程序当前目录的读写权限
  if FileExists('wz.txt') then
    SysUtils.DeleteFile('wz.txt');
  if FileExists('zp.bmp') then
    SysUtils.DeleteFile('zp.bmp');
  if FileExists('zp.wlt') then
    SysUtils.DeleteFile('zp.wlt');
  // PAnsiChar(AnsiString(Str))
  iRet := SDT_ReadBaseMsgToFile(EdziPortID, PAnsiChar(AnsiString('wz.txt')),
    puiCHMsgLen, PAnsiChar(AnsiString('zp.wlt')), puiPHMsgLen, 1);
  if iRet <> 144 then
  begin
    SDT_ClosePort(EdziPortID);
    // Application.MessageBox(TIP_ICREADER_READ_FAILED, TIP_TITLE);
    AErrMsg := TIP_ICREADER_READ_FAILED;
    Result := False;
    Exit;
  end;
  SDT_ClosePort(EdziPortID);
  fs := TFileStream.Create('wz.txt', fmOpenRead);
  fs.Position := 0;
  fs.Read(ACardInfo, SizeOf(ACardInfo));
  fs.Free;
  //
  // 姓名 ：AnsiString(idCardInfo.Name);
  // 性别 ： if AnsiString(idCardInfo.Sex)= '1' then 性别:= '男' else 性别:= '女';
  // 民族 ： EthnicNoToName(AnsiString(idCardInfo.Nation));
  // 出生年月日：  FormatDateStr(AnsiString(idCardInfo.BirthDay));
  // 住址：  Address:= Trim(AnsiString(idCardInfo.Addr));
  // 身份证号码：  Id:= Trim(AnsiString(idCardInfo.IdSN));
  // 发证机构：  Place:= Trim(AnsiString(idCardInfo.IssueOrgan));
  // 有效日期开始  ValidDateStart:= FormatDateStr(AnsiString(idCardInfo.VaildBegin));
  // 有效日期结束  if Trim(AnsiString(idCardInfo.VaildEnd)) = '长期' then
  // ValidDateEnd:= FormatDateTime('yyyy-MM-dd', MaxDateTime)
  // else
  // ValidDateEnd:= FormatDateStr(AnsiString(idCardInfo.VaildEnd));
  //
  Result := true;
end;

initialization

LstEthnic := TStringList.Create;
with LstEthnic do
begin
  Add('01' + '=' + '汉族');
  Add('02' + '=' + '蒙古族');
  Add('03' + '=' + '回族');
  Add('04' + '=' + '藏族');
  Add('05' + '=' + '维吾尔族');
  Add('06' + '=' + '苗族');
  Add('07' + '=' + '彝族');
  Add('08' + '=' + '壮族');
  Add('09' + '=' + '布依族');
  Add('10' + '=' + '朝鲜族');
  Add('11' + '=' + '满族');
  Add('12' + '=' + '侗族');
  Add('13' + '=' + '瑶族');
  Add('14' + '=' + '白族');
  Add('15' + '=' + '土家族');
  Add('16' + '=' + '哈尼族');
  Add('17' + '=' + '哈萨克族');
  Add('18' + '=' + '傣族');
  Add('19' + '=' + '黎族');
  Add('20' + '=' + '傈僳族');
  Add('21' + '=' + '佤族');
  Add('22' + '=' + '畲族');
  Add('23' + '=' + '高山族');
  Add('24' + '=' + '拉祜族');
  Add('25' + '=' + '水族');
  Add('26' + '=' + '东乡族');
  Add('27' + '=' + '纳西族');
  Add('28' + '=' + '景颇族');
  Add('29' + '=' + '柯尔克孜族');
  Add('30' + '=' + '土族');
  Add('31' + '=' + '达翰尔族');
  Add('32' + '=' + '仫佬族');
  Add('33' + '=' + '羌族');
  Add('34' + '=' + '布朗族');
  Add('35' + '=' + '撒拉族');
  Add('36' + '=' + '毛南族');
  Add('37' + '=' + '仡佬族');
  Add('38' + '=' + '锡伯族');
  Add('39' + '=' + '阿昌族');
  Add('40' + '=' + '普米族');
  Add('41' + '=' + '塔吉克族');
  Add('42' + '=' + '怒族');
  Add('43' + '=' + '乌孜别克族');
  Add('44' + '=' + '俄罗斯族');
  Add('45' + '=' + '鄂温克族');
  Add('46' + '=' + '德昂族');
  Add('47' + '=' + '保安族');
  Add('48' + '=' + '裕固族');
  Add('49' + '=' + '京族');
  Add('50' + '=' + '塔塔尔族');
  Add('51' + '=' + '独龙族');
  Add('52' + '=' + '鄂伦春族');
  Add('53' + '=' + '赫哲族');
  Add('54' + '=' + '门巴族');
  Add('55' + '=' + '珞巴族');
  Add('56' + '=' + '基诺族');
  Add('57' + '=' + '其它');
  Add('98' + '=' + '外国人入籍');
end;

finalization

LstEthnic.Free;

end.
