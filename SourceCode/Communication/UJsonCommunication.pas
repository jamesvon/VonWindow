{ *********************************************************************** }
{ Delphi Json communication 2.0                                           }
{ Copyright(c) 2016 jamesvon                                              }
{                                                                         }
{ ======= TJsonCommunication 完成客户端和服务器端的 json 信息通讯 ======= }
{                                                                         }
{ 服务器端缓存有客户端登陆信息，一旦验证通过则无需登录，如果信息不一致，  }
{ 则需要重新登录，缓存机制是应用码+本地机器码和用户名对应，登录时系统查看 }
{ FOnLogin 事件是否重新指定，如果没有没有指定则，系统则调用系统默认登录界 }
{ 面完成登录。                                                            }
{ *********************************************************************** }

unit UJsonCommunication;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UPlatformDB, DB, ADODB, ImgList, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP, UVonConfig, ExtCtrls, UPlatformLogin,
  UVonLog, DBXJSON, DBXJSONReflect;

type
  EServiceResult = (SR_RELOGIN, SR_SUCCESS, SR_FAILED);

  TJsonCommunication = class
  private
    FOnLogin: TDlgLoginEvent;
    FAppID: string;
    FServiceUrl: string;
    FMachineCode: string;
    FConnected: Boolean;
    mmsServer: TIdHTTP;
    timerConnection: TTimer;
    FLastResult: TJSONObject;
    timerValue: Integer;
    FCurrentUser: string;
    FLastError: string;
    FUserName: string;
    procedure SetAppID(const Value: string);
    procedure SetMatchCode(const Value: string);
    procedure SetOnLogin(const Value: TDlgLoginEvent);
    procedure SetServiceUrl(const Value: string);
    procedure EventOfTimer(sender: TObject);
    procedure mmsServerConnected(Sender: TObject);
    procedure mmsServerDisconnected(Sender: TObject);
    procedure SetCurrentUser(const Value: string);
    function EventLogin(UserName, UserPwd: string): Boolean;
    function EventChangePwd(UserName, UserPwd: string): Boolean;
    procedure SetLastError(const Value: string);
    procedure SetUserName(const Value: string);
  public
    constructor Create();
    destructor Destroy;
    function Connect: Boolean;
    function Logon: Boolean;
    function ChangePwd: Boolean;

    function LoadData(Cmd, Params: string): TJSONObject;
    function LoadDataByJson(Cmd, jsonParams: string): TJSONObject; overload;
    function LoadDataByJson(Cmd: string; jsonParams: TJSONArray): TJSONObject; overload;

    function ReadString(Cmd, params: string): string;
    function ReadStringByJson(Cmd, Params: string): string; overload;
    function ReadStringByJson(Cmd: string; jsonParams: TJSONArray): string; overload;

    function ExecuteCmd(Cmd, params: string): Boolean; overload;
    function ExecuteCmdByJson(Cmd, jsonParams: string): Boolean; overload;
    function ExecuteCmdByJson(Cmd: string; jsonParams: TJSONObject): Boolean; overload;

  published
    property ServiceUrl: string read FServiceUrl write SetServiceUrl;
    property MatchCode: string read FMachineCode write SetMatchCode;
    property AppID: string read FAppID write SetAppID;
    property UserName: string read FUserName write SetUserName;
    property LastResult: TJSONObject read FLastResult;
    property LastError: string read FLastError write SetLastError;
    property OnLogin: TDlgLoginEvent read FOnLogin write SetOnLogin;
    property Connected: Boolean read FConnected;
    property CurrentUser: string read FCurrentUser write SetCurrentUser;
  end;

implementation

{ TJsonCommunication }

function TJsonCommunication.ChangePwd: Boolean;
var
  Idx: Integer;
begin
  Idx:= 0;
  while (not Result) and (Idx < 3) do begin
    if Assigned(FOnLogin) then FOnLogin(self)
    else ChangePassword(FUserName, EventChangePwd);
    Inc(Idx);
  end;
end;

function TJsonCommunication.Connect: Boolean;
begin
  Result:= False;
  mmsServer.Connect;
  if not mmsServer.Connected then Exit;
  if not ExecuteCmd('login', '') then Exit;
  Result:= True;
end;

constructor TJsonCommunication.Create;
begin
  mmsServer:= TIdHTTP.Create(nil);
  mmsServer.Request.UserAgent:='Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022; InfoPath.2; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)';
  mmsServer.Request.Accept := '*/*';
  mmsServer.Request.AcceptLanguage := 'zh-cn';
  mmsServer.Request.AcceptEncoding := 'gzip, deflate';
  mmsServer.Request.Connection := 'Keep-Alive';
  mmsServer.Request.CacheControl := 'no-cache';
  mmsServer.Request.Referer:='http://xxx.xxx.cn/';
  mmsServer.Request.CustomHeaders.Text := 'Cookie:OXPYJ911=1207DD4D; cookies_login_state=user_id=2560F2C27F0FC02E4356D51B827A7204; ASP.NET_SessionId=zu2fmjaobt3zh055foo0xwus; AJSTAT_ok_times=28; AJSTAT_ok_pages=3';
  mmsServer.HTTPOptions:= mmsServer.HTTPOptions+[hoKeepOrigProtocol];
  mmsServer.ProtocolVersion:=pv1_1;
  mmsServer.Request.ContentType := 'application/json';
  mmsServer.Request.Charset := 'utf-8';
  mmsServer.ReadTimeout := 30000;
  timerConnection:= TTimer.Create(nil);
  timerConnection.Enabled:= False;
  timerConnection.Interval:= 30000;
  timerConnection.OnTimer:= EventOfTimer;
  FLastResult:= TJSONObject.Create;
end;

destructor TJsonCommunication.Destroy;
begin
  FLastResult.Free;
  timerConnection.Enabled:= False;
  timerConnection.Free;
  mmsServer.Disconnect;
  mmsServer.Free;
end;

function TJsonCommunication.EventChangePwd(UserName, UserPwd: string): Boolean;
begin
  Result:= ExecuteCmd('changepwd', 'LoginName=' + UserName + '&PWD=' + UserPwd);
end;

function TJsonCommunication.EventLogin(UserName, UserPwd: string): Boolean;
begin
  Result:= ExecuteCmd('loginapp', 'LoginName=' + UserName + '&PWD=' + UserPwd);
end;

procedure TJsonCommunication.EventOfTimer(sender: TObject);
begin
  if timerValue = 0 then ExecuteCmd('openapp', '');
  timerValue:= 0;
end;

function TJsonCommunication.ExecuteCmd(Cmd, params: string): Boolean;
var
  URL: string;
  respstream: TStringStream;
  szResult: TJSONObject;
begin    //{result:RELOGIN|FAILED|SUCCESS,error:<error info>,data:<data>}
  respstream:= TStringStream.Create('', TEncoding.UTF8);
  szResult:= TJSONObject.Create;
  Url:= FServiceUrl + Cmd + '?AppID=' + FAppID + '&MachineCode=' + FMachineCode;
  if params <> '' then
    Url:= Url + '&' + params;
  Inc(timerValue, 1);
  Result:= False;
  try
    mmsServer.Get(Url, respstream);
    respstream.Position:= 0;
    szResult:= TJSONObject.ParseJSONValue(respstream.DataString) as TJSONObject;
    if SameText(szResult.Get('result').JsonValue.Value, 'RELOGIN') then begin
      Result:= Login;
    end else if SameText(szResult.Get('result').JsonValue.Value, 'SUCCESS') then begin
      Result:= True;
      FLastResult.Free;
      FLastResult:= szResult.Get('data').JsonValue as TJSONObject;
    end else begin
      Result:= False;
      FLastError:= szResult.Get('msg').JsonValue.Value;
    end;
  finally
    respstream.Free;
    szResult.Free;
  end;
end;

function TJsonCommunication.ExecuteCmdByJson(Cmd: string;
  jsonParams: TJSONObject): Boolean;
var
  I: Integer;
  Params: string;
begin
  Params:= '';
  for I := 0 to jsonParams.Size - 1 do
    Params:= '&' + jsonParams.Get(I).UnitName + '=' + jsonParams.Get(I).Value;
  Result:= ExecuteCmd(Cmd, Params);
end;

function TJsonCommunication.ExecuteCmdByJson(Cmd, jsonParams: string): Boolean;
var
  I: Integer;
  Params: string;
  szParams: TJSONObject;
begin
  szParams:= TJSONObject.ParseJSONValue(jsonParams) as TJSONObject;
  Params:= 'UserName=' + szParams.Get('UserName').Value + ',UserPwd=' + szParams.Get('UserPwd').Value;
  Result:= ExecuteCmd(Cmd, Params);
  szParams.Free;
end;

function TJsonCommunication.LoadData(Cmd, Params: string): TJSONObject;
begin
  if ExecuteCmd(Cmd, Params) then
    Result:= FLastResult
  else Result:= nil;
end;

function TJsonCommunication.LoadDataByJson(Cmd: string;
  jsonParams: TJSONArray): TJSONObject;
var
  I: Integer;
  Params: string;
begin
  Params:= '';
  for I := 0 to jsonParams.Size - 1 do
    Params:= '&' + jsonParams.Get(I).UnitName + '=' + jsonParams.Get(I).Value;
  if ExecuteCmd(Cmd, Params) then
    Result:= FLastResult
  else Result:= nil;
end;

function TJsonCommunication.Logon: Boolean;
var
  Idx: Integer;
begin
  Idx:= 0;
  Result:= False;
  while (not Result) and (Idx < 3) do begin
    if Assigned(FOnLogin) then Result:= FOnLogin(self)
    else Result:= Login(EventLogin);
    Inc(Idx);
  end;
end;

procedure TJsonCommunication.mmsServerConnected(Sender: TObject);
begin

end;

procedure TJsonCommunication.mmsServerDisconnected(Sender: TObject);
begin

end;

function TJsonCommunication.LoadDataByJson(Cmd, jsonParams: string): TJSONObject;
begin
  if ExecuteCmd(Cmd, jsonParams) then
    Result:= FLastResult
  else Result:= nil;
end;

function TJsonCommunication.ReadString(Cmd, params: string): string;
begin
  if ExecuteCmd(Cmd, params) then
    Result:= FLastResult.Value
  else Result:= '';
end;

function TJsonCommunication.ReadStringByJson(Cmd: string;
  jsonParams: TJSONArray): string;
var
  I: Integer;
  Params: string;
begin
  Params:= '';
  for I := 0 to jsonParams.Size - 1 do
    Params:= '&' + jsonParams.Get(I).UnitName + '=' + jsonParams.Get(I).Value;
  if ExecuteCmd(Cmd, Params) then
    Result:= FLastResult.Value
  else Result:= '';
end;

function TJsonCommunication.ReadStringByJson(Cmd, Params: string): string;
begin
  if ExecuteCmd(Cmd, Params) then
    Result:= FLastResult.Value
  else Result:= '';
end;

procedure TJsonCommunication.SetAppID(const Value: string);
begin
  FAppID:= Value;
end;

procedure TJsonCommunication.SetCurrentUser(const Value: string);
begin
  FCurrentUser := Value;
end;

procedure TJsonCommunication.SetLastError(const Value: string);
begin
  FLastError := Value;
end;

procedure TJsonCommunication.SetMatchCode(const Value: string);
begin
  FMachineCode:= Value;
end;

procedure TJsonCommunication.SetOnLogin(const Value: TDlgLoginEvent);
begin
  FOnLogin:= Value;
end;

procedure TJsonCommunication.SetServiceUrl(const Value: string);
begin
  FServiceUrl:= Value;
end;

procedure TJsonCommunication.SetUserName(const Value: string);
begin
  FUserName := Value;
end;

end.
