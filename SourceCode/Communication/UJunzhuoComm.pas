unit UJunzhuoComm;

interface

uses Types, SysUtils, Classes, UCommunicationSerialPort, UVonSystemFuns,
  ExtCtrls, Contnrs, UCoteDB, UVonLog, UCoteComm;

const
  MAXBUFFERSIZE = 1024;
  TIMEINTERVALUE = 500;
  NO_VALID_RING = '非君卓信鸽脚环';

type
  TCommPigeonInfo = class
    ElectricCode: string;       //环号（5）
    RndValue: Integer;          //随机码（4）
    RecordDateTimr: TDatetime;  //集鸽年月日时分秒（6）
    GameID: integer;            //赛事序号（1）[0..2]
    Flag: integer;              //赛事序号（1）[7]
  end;

  TJunzhuoPigeonInfo = class
    GBCode: string;             //统一环号
    Association: string;        //协会区号
    GameNo: Integer;            //赛事编号
    RingGameNo: Integer;        //赛事环号(针对赛事所启用的赛事内唯一环号)
    PhoneNo: string;            //电话号码(赛事对应的信息台号码)
    FlyTime: Integer;           //回鸽时间
    LagValue: Integer;          //回鸽延时
    Status: integer;
  private
    FElectronicCode: string;
    FGender: Integer;
    FEye: Integer;
    FFeather: Integer;
    FCollectionTime: TDatetime;
    FDataTime: TDatetime;
    procedure SetElectronicCode(const Value: string);
    procedure SetEye(const Value: Integer);
    procedure SetFeather(const Value: Integer);
    procedure SetGender(const Value: Integer);
    procedure SetCollectionTime(const Value: TDatetime);
    procedure SetDataTime(const Value: TDatetime);           //记录状态(0:集鸽 1:回鸽 2:已上传)
  published
    property ElectronicCode: string read FElectronicCode write SetElectronicCode;    //电子环号
    property Gender: Integer read FGender write SetGender;
    property Eye: Integer read FEye write SetEye;
    property Feather: Integer read FFeather write SetFeather;
    property CollectionTime: TDatetime read FCollectionTime write SetCollectionTime;
    property DataTime: TDatetime read FDataTime write SetDataTime;
  end;

  TCommStatusEmnu = (csAuto, csAutoReceive, csAutoSend, csManual, csStop);
  TJunzhuoComm = class
  private
    FCOMM: IComm;
    FTimer: TTimer;
    FOpened: boolean;
    FParity: char;
    FTimerOut: Integer;
    FBaudRate: longint;
    FType: string;
    FStatus: TCommStatusEmnu;
    FOnRecevered: TNotifyEvent;
    FByteSize: word;
    FStopBits: word;
    FPort: word;
    FSendCmdID, FReceiveCmdID: Integer;
    FRecords: TObjectList;
    FMatchineNo: Cardinal;
    FMatchineTime: TDateTime;
    FSucessed: boolean;
    FCollectionCount: Integer;
    FReceivedCount, FSendCount: Integer;
    FCurrentRecordIdx: Integer;
    FTempStr: string;
    procedure EventToReceive(Sender: TObject);
    procedure DoSendEvent(EventID: Integer);
    function GetLoopTime: Integer;
    function GetMatchineTime: TDateTime;
    procedure SetBaudRate(const Value: longint);
    procedure SetByteSize(const Value: word);
    procedure SetCommunicationType(const Value: string);
    procedure SetOnRecevered(const Value: TNotifyEvent);
    procedure SetOpened(const Value: boolean);
    procedure SetParity(const Value: char);
    procedure SetPort(const Value: word);
    procedure SetStatus(const Value: TCommStatusEmnu);
    procedure SetStopBits(const Value: word);
    procedure SetTimerOut(const Value: Integer);
    function GetInfoCount: Integer;
    function GetInfos: TObjectList;
    procedure SetCurrentRecordIdx(const Value: Integer);
    function GetInfo(Index: Integer): TJunzhuoPigeonInfo;
    procedure BeginRead(EventID, ReceiveID, Timer: Integer; LoopKind: TCommStatusEmnu);
    function GetOpened: boolean;
  public
    constructor Create(AType: string);
    destructor Destroy; override;
    function Open(APort, ABaudRate: longint; AParity: char; AByteSize, AStopBits: byte): Boolean;
    procedure Close;
    procedure DeleteInfo(Index: Integer);
    procedure InitMatchine;                             //1读取主机状态和信息并同步时钟
    procedure GetMatchine;                              //2读取时钟和记录指针（测试用）
    procedure DeleteRecord(ElectronicCode: string);     //3清除主机回鸽、集鸽记录
    procedure ReadRecord;                               //4读取回鸽、集鸽记录
    procedure WriteCollection(GBYear, GBArea, GBCode: Integer;           //5写入集鸽数据
      ECode: string; AssID, GameID, RndNum: Integer; PhoneNo: string;
      Gender, Eye, Feather: Integer);
    procedure ClearMemory;                              //6清除主机内存
    procedure ReadCode(LoopKind: TCommStatusEmnu);      //7读取集鸽环号
    property Info[Index: Integer]: TJunzhuoPigeonInfo read GetInfo;
  published
    //Communication
    property Port: word read FPort write SetPort;                               //设置通讯口 1=COM1
    property BaudRate: longint read FBaudRate write SetBaudRate;                //设置波特率 9600
    property Parity: char read FParity write SetParity;                         //奇偶校验位 0-4分别表示无、奇、偶、传号、空号校验
    property ByteSize: word read FByteSize write SetByteSize;                   //字符位数   8
    property StopBits: word read FStopBits write SetStopBits;                   //设置停止位 1
    property Opened: boolean read GetOpened write SetOpened;                    //Comm是否打开
    property TimerOut: Integer read FTimerOut write SetTimerOut;                //超时值(ms)
    property CommunicationType: string read FType write SetCommunicationType;   //通讯选取的方式名称
    property MatchineNo: Cardinal read FMatchineNo;                             //设备编码
    property MatchineTime: TDateTime read GetMatchineTime;                      //读取当前设备的时间
    property CollectionCount: Integer read FCollectionCount;                    //集鸽数量
    property CurrentRecordIdx: Integer read FCurrentRecordIdx write SetCurrentRecordIdx;  //当前记录位置
    property InfoCount: Integer read GetInfoCount;
    property Infos: TObjectList read GetInfos;
    property Sucessed: boolean read FSucessed;                                  //发送是否成功标志
    property Status: TCommStatusEmnu read FStatus write SetStatus;              //系统扫描状态
    property LoopTime: Integer read GetLoopTime;                                //时间扫面间隔
    property OnRecevered: TNotifyEvent read FOnRecevered write SetOnRecevered;  //接收成功相应事件
  end;

  TPigeonComm = class
  private
    FCOMM: IComm;
    FOpened: boolean;
    FMatchineCount: Integer;
    FTimerOut: Integer;
    FSetCountOfMatchine: integer;
    FBaudRate: longint;
    FStopBits: word;
    FParity: char;
    FByteSize: word;
    FPort: word;
    FMatchineID: Integer;
    FBackCount: Integer;
    FCollectionCount: Integer;
    FMathineTime: TDateTime;
    FMatchineNo: Integer;
    FMatchID: Integer;
    FRecordID: Integer;
    FPigeonCount: Integer;
    procedure SetBaudRate(const Value: longint);
    procedure SetByteSize(const Value: word);
    procedure SetMatchineCount(const Value: Integer);
    procedure SetOpened(const Value: boolean);
    procedure SetParity(const Value: char);
    procedure SetPort(const Value: word);
    procedure SetSetCountOfMatchine(const Value: integer);
    procedure SetStopBits(const Value: word);
    procedure SetTimerOut(const Value: Integer);
    procedure SetMatchineID(const Value: Integer);
    function GetBoardStatus(Index: Integer): Boolean;
    procedure SetBoardStatus(Index: Integer; const Value: Boolean);
    procedure SetBackCount(const Value: Integer);
    procedure SetCollectionCount(const Value: Integer);
    procedure SetMathineTime(const Value: TDateTime);
    procedure SetMatchineNo(const Value: Integer);
    procedure SetMatchID(const Value: Integer);
    procedure SetCommunicationKind(const Value: Integer);
    procedure SetRecordID(const Value: Integer);
    procedure SetTask(const Value: Integer);
    procedure SetPigeonCount(const Value: Integer);
  private
    FServerStatus: array of integer;
    FCodes: TObjectList;
    FTask: Integer;
    FTimer: TTimer;
    FReturn: Boolean;
    FKind: Integer;
    FBoardStatus: array[0..7]of Boolean;
    FLastPigeon: TCommPigeonInfo;
    FCollectionData: TCommPigeonInfo;
    FCollectPigeonRecord: TCommPigeonInfo;
    procedure ReadCommunication(Sender: TObject);
  public
    DTMF: array[0..63]of byte;                                          //DTMF数据序列
    constructor Create(AType: string);
    destructor Destroy; override;
    procedure Open(APort, ABaudRate: longint; AParity: char; AByteSize, AStopBits: byte);
    procedure Close;                                                    //关闭串口
    procedure BeginReadElectricCode;
    procedure EndReadElectricCode;
    function ReadElectricCode: TCommPigeonInfo;                         //读取电子环号和其他相关信息
    function ReadMatchineStatus(SetID: Integer): Boolean;               //读取主机状态
    procedure WriteCollectData(ElectricCode: string; RndValue: Integer;
      RecordDateTimr: TDatetime; GameID: integer; Flag: integer);
    procedure SynTimer;                                                 //同步主机时间
    property BoardStatus[Index: Integer]: Boolean read GetBoardStatus write SetBoardStatus; //踏板状态
  published
    //Communication
    property Port: word read FPort write SetPort;                       //设置通讯口 1=COM1
    property BaudRate: longint read FBaudRate write SetBaudRate;        //设置波特率 9600
    property Parity: char read FParity write SetParity;                 //奇偶校验位 0-4分别表示无、奇、偶、传号、空号校验
    property ByteSize: word read FByteSize write SetByteSize;           //字符位数   8
    property StopBits: word read FStopBits write SetStopBits;           //设置停止位 1
    property Opened: boolean read FOpened write SetOpened;              //Comm是否打开
    property TimerOut: Integer read FTimerOut write SetTimerOut;        //超时值(ms)
    property CommunicationKind: Integer read FKind write SetCommunicationKind;
    //Status
    property MatchineNo: Integer read FMatchineNo write SetMatchineNo;  //设备号
    property MatchineID: Integer read FMatchineID write SetMatchineID;  //设备序号
    property PigeonCount: Integer read FPigeonCount write SetPigeonCount;     //鸽子数量（2）
    property BackCount: Integer read FBackCount write SetBackCount;     //回鸽数量（2）
    property CollectionCount: Integer read FCollectionCount write SetCollectionCount; //集鸽数量（2）
    property MathineTime: TDateTime read FMathineTime write SetMathineTime;     //机器时间
    property MatchID: Integer read FMatchID write SetMatchID;           //赛事序号
    property RecordID: Integer read FRecordID write SetRecordID;

    property Task: Integer read FTask write SetTask;

    property MatchineCount: Integer read FMatchineCount write SetMatchineCount; //主机数量
    property SetCountOfMatchine: integer read FSetCountOfMatchine write SetSetCountOfMatchine; //每个主机的踏板数量
  end;

  TDalianYunfeiCom = class
  private
    FCOMM: IComm;
    FTimer: TTimer;
    FOpened: boolean;
    FParity: char;
    FBaudRate: longint;
    FByteSize: word;
    FPort: word;
    FStopBits: word;
    FRecords: TStringList;
    function GetPigeonBacktime(Index: Integer): Integer;
    function GetPigeonCount: Integer;
    function GetIsReader: Boolean;
  public
    constructor Create(AType: string);
    destructor Destroy; override;
    procedure Open(APort, ABaudRate: longint; AParity: char; AByteSize, AStopBits: byte);
    procedure ReadPigeonBacktime(Sender: TObject);
    procedure ClearPigeon;
    property PigeonBacktime[Index: Integer]: Integer read GetPigeonBacktime;
  published
    //Communication
    property IsReader: Boolean read GetIsReader;
    property PigeonCount: Integer read GetPigeonCount;
  end;

implementation

uses Math, DateUtils;

{ TPigeonComm }

procedure TPigeonComm.Close;
begin     //通讯关闭
  if not Assigned(FComm) then Exit;
  FComm.Close;
end;

constructor TPigeonComm.Create(AType: string);
begin       //创建通讯端口
  if AType = 'FILE' then FCOMM:= IComm(TCommFile.Create)
  else raise Exception.Create('Not found the ' + AType + ' type of communication.');
  FLastPigeon:= TCommPigeonInfo.Create;
  FCodes:= TObjectList.Create;
  FCollectionData:= TCommPigeonInfo.Create;
  FTimer:= TTimer.Create(nil);
  FRecordID:= 0;
  FTimer.Enabled:= False;
  FTimer.Interval:= TIMEINTERVALUE;
  FTimer.OnTimer:= ReadCommunication;
  FCollectPigeonRecord:= TCommPigeonInfo.Create;
end;

destructor TPigeonComm.Destroy;
begin      //通讯结束，释放空间
  Close;
  FLastPigeon.Free;
  FCollectionData.Free;
  FCodes.Free;
  FTimer.Free;
  inherited;
end;

function TPigeonComm.GetBoardStatus(Index: Integer): Boolean;
begin
  Result:= FBoardStatus[Index];
end;

procedure TPigeonComm.Open(APort, ABaudRate: Integer;
  AParity: char; AByteSize, AStopBits: byte);
begin     //打开通讯
  if not Assigned(FComm) then Exit;
  if FComm.Opened then begin
    if (APort = FPort)and(ABaudRate = FBaudRate)and (AParity = FParity)and
      (AByteSize = FByteSize)and(AStopBits = FStopBits) then Exit
    else FComm.Close;
  end;
  FComm.Open(APort, ABaudRate, AParity, AByteSize, AStopBits);
end;

procedure TPigeonComm.ReadCommunication(Sender: TObject);
var
  Buf: array[0..255] of byte;
  BufSize, Y, M, D, H, N, S, C: Word;
  PigeonRecord: TCommPigeonInfo;
begin     //读取一个通讯内容
  if FTask = 0 then begin
    FTimer.Enabled:= False;
    Exit;
  end;
  if not Assigned(FComm) then Exit;
  if FKind > 0 then begin     //接收(1)或连续接收(2)
    FillChar(Buf, SizeOf(Buf), 0);
    BufSize:= FComm.Receive(Buf, 255, 50);
    (* Receive data *)
    case FTask of
    1: //读取主机状态和信息： 计算机发送80H，00H 主机返回：主机编号（4），踏板状态（1），鸽子数量（2），回鸽数量（2），集鸽数量（2），年（1），月（1），日（1），时（1），分（1），秒（1）
        if BufSize > 16 then begin
          FMatchineNo:= (Buf[0] shl 24)or(Buf[1] shl 16)or(Buf[2] shl 8)or Buf[3];//主机编号（4）
          FBoardStatus[0]:= (Buf[4] and $80) > 0;//10000000
          FBoardStatus[1]:= (Buf[4] and $40) > 0;//01000000
          FBoardStatus[2]:= (Buf[4] and $20) > 0;//00100000
          FBoardStatus[3]:= (Buf[4] and $10) > 0;//00010000
          FBoardStatus[4]:= (Buf[4] and $8 ) > 0;//00001000
          FBoardStatus[5]:= (Buf[4] and $4 ) > 0;//00000100
          FBoardStatus[6]:= (Buf[4] and $2 ) > 0;//00000010
          FBoardStatus[7]:= (Buf[4] and $1 ) > 0;//00000001
          FPigeonCount:= (Buf[5] shl 8) or Buf[6];
          FBackCount:= (Buf[7] shl 8) or Buf[8];
          FCollectionCount:= (Buf[9] shl 8) or Buf[10];
          if not TryEncodeDateTime(2000 + BCDToInt(Buf[11]), BCDToInt(Buf[12]), BCDToInt(Buf[13]),
            BCDToInt(Buf[14]), BCDToInt(Buf[15]), BCDToInt(Buf[16]), 0, FMathineTime)then FMathineTime:= 0;
          FReturn:= True;
        end;
    2,3,4,6:
      //2、设置主机编号：	计算机发送80H，7FH，编号（4）	主机返回：01H
      //3、设置时钟：计算机发送80H，01H，年（1），月（1），日（1），时（1），分（1），秒（1）主机返回：01H
      //4、清除主机回鸽记录、集鸽记录：计算机发送80H，02H	主机返回：01H
      //6、写入集鸽数据：计算机发送80H，04H，记录指针（2），环号（5），随机码（4），集鸽年月日时分秒（6），赛事序号（1）最高位为1	主机返回：01H
        FReturn:= (BufSize > 0)and(Buf[0] = 1);
    5: //读取集鸽记录：计算机发送80H，03H，记录指针（2）主机返回：环号（5），随机码（4），集鸽年月日时分秒（6），赛事序号（1）（bit7=1为未回鸽标志）
        if BufSize > 11 then begin
          PigeonRecord:= TCommPigeonInfo.Create;
          PigeonRecord.ElectricCode:= IntToHex((Buf[0] shl 8) or Buf[1], 4) + '-' +
            IntToHex(Buf[2] shl 16 or (Buf[3] shl 8) or Buf[4], 6);             //环号（5）
          PigeonRecord.RndValue:= (Buf[5] shl 24) or (Buf[6] shl 16) or (Buf[7] shl 8) or Buf[8];          //随机码（4）
          PigeonRecord.RecordDateTimr:= EncodeDateTime(2000 + BCDToInt(Buf[9]), BCDToInt(Buf[10]), BCDToInt(Buf[11]),
            BCDToInt(Buf[12]), BCDToInt(Buf[13]), BCDToInt(Buf[14]), 0);                                   //集鸽年月日时分秒（6）
          PigeonRecord.GameID:= Buf[15] and $07;              //赛事序号（1）
          PigeonRecord.Flag:= Buf[15] and $80;                //（bit7为比赛标志）
          FCodes.Add(PigeonRecord);
        end;
    7: //读取回鸽记录:计算机发送80H，05H，记录指针（2）	主机返回：环号（5），随机码（4），日期时间（6），标志位（1）（bit7为比赛标志）
        if BufSize > 15 then begin
          PigeonRecord:= TCommPigeonInfo.Create;
          PigeonRecord.ElectricCode:= IntToHex((Buf[0] shl 8) or Buf[1], 4) + '-' +
            IntToHex(Buf[2] shl 16 or (Buf[3] shl 8) or Buf[4], 6);             //环号（5）
          PigeonRecord.RndValue:= (Buf[5] shl 24) or (Buf[6] shl 16) or (Buf[7] shl 8) or Buf[8];          //随机码（4）
          PigeonRecord.RecordDateTimr:= EncodeDateTime(2000 + BCDToInt(Buf[9]), BCDToInt(Buf[10]), BCDToInt(Buf[11]),
            BCDToInt(Buf[12]), BCDToInt(Buf[13]), BCDToInt(Buf[14]), 0);                                   //集鸽年月日时分秒（6）
          PigeonRecord.GameID:= 0;
          PigeonRecord.Flag:= Buf[15] and $80;                //赛事序号（1）
          FCodes.Add(PigeonRecord);
          Inc(FRecordID);
          if FRecordID >= FBackCount then FTask:= 0;
        end;
    8: //读取赛事配置：计算机发送80H，06H，赛事序号（1）主机返回：赛事配置数据64字节
        if BufSize > 63 then
          for BufSize:= 0 to 63 do
            DTMF[BufSize]:= Buf[BufSize];
    9: //设置赛事配置：计算机发送80H，07H，赛事序号（1），赛事配置数据（64）主机返回：赛事序号（1）
        if BufSize > 0 then
          MatchID:= Buf[0];
    end;
    if FKind = 1 then begin
      FTimer.Enabled:= False;
      FTask:= 0;
    end;
  end;
  if FKind <> 1 then begin                       //发送或连续接收
    (* Send data *)
    FillChar(Buf, SizeOf(Buf), 0);
    case FTask of
    1: begin    //读取主机状态和信息： 计算机发送80H，00H 主机返回：主机编号（4），踏板状态（1），记录指针（2），回鸽数量（2），集鸽数量（2），年（1），月（1），日（1），时（1），分（1），秒（1）
        Buf[0]:= $80;
        Buf[1]:= $00;
        FComm.Send(Buf, 2);
      end;
    2: begin    //2、设置主机编号：	计算机发送80H，7FH，编号（4）	主机返回：01H
        Buf[0]:= $80;
        Buf[1]:= $7F;
        Buf[2]:= FMatchineNo shr 24;
        Buf[3]:= FMatchineNo shr 16;
        Buf[4]:= FMatchineNo shr 8;
        Buf[5]:= FMatchineNo;
        FComm.Send(Buf, 6);
      end;
    3: begin    //3、设置时钟：计算机发送80H，01H，年（1），月（1），日（1），时（1），分（1），秒（1）主机返回：01H
        Buf[0]:= $80;
        Buf[1]:= $01;
        DecodeDateTime(Now, Y, M, D, H, N, S, C);
        Buf[2]:= Y - 2000; Buf[3]:= M; Buf[4]:= D; Buf[5]:= H; Buf[6]:= N; Buf[7]:= S;
        FComm.Send(Buf, 8); //MilliSecond被省略
      end;
    4: begin    //4、清除主机回鸽记录、集鸽记录：计算机发送80H，02H	主机返回：01H
        Buf[0]:= $80;
        Buf[1]:= $02;
        FComm.Send(Buf, 2);
      end;
    5: begin    //读取集鸽记录：计算机发送80H，03H，记录指针（2）主机返回：环号（5），随机码（4），集鸽年月日时分秒（6），赛事序号（1）（bit7=1为未回鸽标志）
        Buf[0]:= $80;
        Buf[1]:= $03;
        Buf[2]:= FRecordID shr 8;
        Buf[3]:= FRecordID;
        FComm.Send(Buf, 4);
        Inc(FRecordID);
        if FRecordID = FCollectionCount then FTask:= 0;
      end;
    6: begin    //6、写入集鸽数据：计算机发送80H，04H，记录指针（2），环号（5），随机码（4），集鸽年月日时分秒（6），赛事序号（1）最高位为1	主机返回：01H
        Buf[0]:= $80;
        Buf[1]:= $04;
        Buf[2]:= $00;
        Buf[3]:= HexToInt(Copy(FCollectPigeonRecord.ElectricCode, 1, 2));  //环号（5）1234-6789AB
        Buf[4]:= HexToInt(Copy(FCollectPigeonRecord.ElectricCode, 3, 2));
        Buf[5]:= HexToInt(Copy(FCollectPigeonRecord.ElectricCode, 6, 2));
        Buf[6]:= HexToInt(Copy(FCollectPigeonRecord.ElectricCode, 8, 2));
        Buf[7]:= HexToInt(Copy(FCollectPigeonRecord.ElectricCode, 10, 2));
        Buf[8]:= FCollectPigeonRecord.RndValue shr 24;                     //随机码（4）
        Buf[9]:= FCollectPigeonRecord.RndValue shr 16;
        Buf[10]:=FCollectPigeonRecord.RndValue shr 8;
        Buf[11]:=FCollectPigeonRecord.RndValue;
        DecodeDateTime(Now, Y, M, D, H, N, S, C);
        Buf[12]:= Y - 2000; Buf[13]:= M; Buf[14]:= D; Buf[15]:= H; Buf[16]:= N; Buf[17]:= S;  //集鸽年月日时分秒（6）
        Buf[18]:= (MatchID and $000000F)or $80;                    //赛事序号（1）最高位为1
        FComm.Send(Buf, 19);
      end;
    7: begin    //读取回鸽记录:计算机发送80H，05H，记录指针（2）	主机返回：环号（5），随机码（4），日期时间（6），标志位（1）（bit7为比赛标志）
        Buf[0]:= $80;
        Buf[1]:= $05;
        Buf[2]:= FRecordID shr 8;
        Buf[3]:= FRecordID;
        FComm.Send(Buf, 4);
      end;
    8: begin    //读取赛事配置：计算机发送80H，06H，赛事序号（1）主机返回：赛事配置数据64字节
        Buf[0]:= $80;
        Buf[1]:= $06;
        Buf[2]:= MatchID;
        FComm.Send(Buf, 3);
      end;
    9: begin    //设置赛事配置：计算机发送80H，07H，赛事序号（1），赛事配置数据（64）主机返回：赛事序号（1）
        Buf[0]:= $80;
        Buf[1]:= $07;
        Buf[2]:= MatchID;
        for BufSize:= 0 to 63 do
            Buf[BufSize + 3]:= DTMF[BufSize];
        FComm.Send(Buf, 67);
      end;
    end;
    if FKind = 0 then FKind:= 1;
  end;
end;

procedure TPigeonComm.BeginReadElectricCode;
begin     //开始连续读取电子环信息
  CommunicationKind:= 2;
  FTimer.Enabled:= True;
  FRecordID:= 0;
  FTask:= 7;
  ReadCommunication(nil);
end;

procedure TPigeonComm.EndReadElectricCode;
begin
  CommunicationKind:= 1;
  FTask:= 0;
end;

function TPigeonComm.ReadElectricCode: TCommPigeonInfo;
begin
  //7、读取回鸽记录：计算机发送80H，05H，记录指针（2）主机返回：环号（5），随机码（4），日期时间（6），标志位（1）（bit7为比赛标志）
  FTask:= 7;
  if FCodes.Count > 0 then begin
    FLastPigeon.ElectricCode:= (FCodes[0]as TCommPigeonInfo).ElectricCode;     //环号（5）
    FLastPigeon.RndValue:= (FCodes[0]as TCommPigeonInfo).RndValue;             //随机码（4）
    FLastPigeon.RecordDateTimr:= (FCodes[0]as TCommPigeonInfo).RecordDateTimr; //集鸽年月日时分秒（6）
    FLastPigeon.Flag:= (FCodes[0]as TCommPigeonInfo).Flag;                     //赛事序号（1）
    FCodes.Delete(0);
  end;
  Result:= FLastPigeon;
end;

function TPigeonComm.ReadMatchineStatus(SetID: Integer): Boolean;
begin
  //1、读取主机状态和信息：计算机发送80H，00H 主机返回：主机编号（4），踏板状态（1），记录指针（2），回鸽数量（2），集鸽数量（2），年（1），月（1），日（1），时（1），分（1），秒（1）
  FTask:= 1;
  FKind:= 0;
  ReadCommunication(nil);
  Sleep(500);
  ReadCommunication(nil);
  Result:= FReturn;
end;

procedure TPigeonComm.SetBackCount(const Value: Integer);
begin
  FBackCount := Value;
end;

procedure TPigeonComm.SetBaudRate(const Value: longint);
begin
  FBaudRate := Value;
end;

procedure TPigeonComm.SetBoardStatus(Index: Integer; const Value: Boolean);
begin
  FBoardStatus[Index]:= Value;
end;

procedure TPigeonComm.SetByteSize(const Value: word);
begin
  FByteSize := Value;
end;

procedure TPigeonComm.SetCollectionCount(const Value: Integer);
begin
  FCollectionCount := Value;
end;

procedure TPigeonComm.SetCommunicationKind(const Value: Integer);
begin
  FKind := Value;
  FTimer.Enabled:= FKind = 3;  //Auto
end;

procedure TPigeonComm.SetMatchID(const Value: Integer);
begin
  FMatchID := Value;
end;

procedure TPigeonComm.SetMatchineCount(const Value: Integer);
begin
  FMatchineCount := Value;
  SetLength(FServerStatus, Value);
end;

procedure TPigeonComm.SetMatchineID(const Value: Integer);
begin
  FMatchineID := Value;
end;

procedure TPigeonComm.SetMatchineNo(const Value: Integer);
begin
  FMatchineNo := Value;
end;

procedure TPigeonComm.SetMathineTime(const Value: TDateTime);
begin
  FMathineTime := Value;
end;

procedure TPigeonComm.SetOpened(const Value: boolean);
begin
  FOpened := Value;
end;

procedure TPigeonComm.SetParity(const Value: char);
begin
  FParity := Value;
end;

procedure TPigeonComm.SetPort(const Value: word);
begin
  FPort := Value;
end;

procedure TPigeonComm.SetSetCountOfMatchine(const Value: integer);
begin
  FSetCountOfMatchine := Value;
end;

procedure TPigeonComm.SetStopBits(const Value: word);
begin
  FStopBits := Value;
end;

procedure TPigeonComm.SetTimerOut(const Value: Integer);
begin
  FTimerOut := Value;
end;

procedure TPigeonComm.SynTimer;
begin

end;

procedure TPigeonComm.SetRecordID(const Value: Integer);
begin
  FRecordID := Value;
end;

procedure TPigeonComm.SetTask(const Value: Integer);
begin
  FTask := Value;
  if FTask > 0 then begin
    FKind:= 0;
    FTimer.Enabled:= True;
  end;
end;

procedure TPigeonComm.SetPigeonCount(const Value: Integer);
begin
  FPigeonCount := Value;
end;

procedure TPigeonComm.WriteCollectData(ElectricCode: string;
  RndValue: Integer; RecordDateTimr: TDatetime; GameID, Flag: integer);
begin
  FCollectPigeonRecord.ElectricCode:= ElectricCode;
  FCollectPigeonRecord.RndValue:= RndValue;
  FCollectPigeonRecord.RecordDateTimr:= RecordDateTimr;
  FCollectPigeonRecord.GameID:= GameID;
  FTask:= 6;
  ReadCommunication(nil);
end;

{ TJunzhuoComm }

procedure TJunzhuoComm.BeginRead(EventID, ReceiveID, Timer: Integer; LoopKind: TCommStatusEmnu);
begin     //开始自动执行任务
  FSendCmdID:= EventID;
  DoSendEvent(EventID);
  FReceivedCount:= 0;
  FSendCount:= 1;
  FReceiveCmdID:= ReceiveID;
  FStatus:= LoopKind;
  FTimer.Interval:= Timer;
  FTimer.OnTimer:= EventToReceive;
  FTImer.Enabled:= True;
end;

procedure TJunzhuoComm.ClearMemory;
begin     //6清除主机内存
  BeginRead(6, 6, 1000, csAutoReceive);
end;

procedure TJunzhuoComm.Close;
begin
  FTimer.Enabled:= False;
  FCOMM.Close;
end;

constructor TJunzhuoComm.Create(AType: string);
begin       //初始化
  if AType = 'FILE' then FCOMM:= TCommFile.Create
  else raise Exception.Create('Not found the ' + AType + ' type of communication.');
  FRecords:= TObjectList.Create;
  FTimer:= TTimer.Create(nil);
  FTimer.Enabled:= False;
end;

procedure TJunzhuoComm.DeleteInfo(Index: Integer);
begin
  FRecords.Delete(Index);
end;

procedure TJunzhuoComm.DeleteRecord(ElectronicCode: string);
begin     //3清除主机回鸽、集鸽记录：计算机发送80H，02H，电子环号（5）
  FTempStr:= ElectronicCode;
  BeginRead(3, 3, 1000, csAutoReceive);
end;

destructor TJunzhuoComm.Destroy;
begin      //析构函数
  FTimer.Enabled:= False;
  FTimer.Free;
  FRecords.Free;
  inherited;
end;

procedure TJunzhuoComm.DoSendEvent(EventID: integer);
var
  buff: array[0..255]of byte;

  //读取主机状态和信息并同步时钟：
  //计算机发送80H，00H，年（1），月（1），日（1），时（1），分（1），秒（1）
  //主机返回：主机编号（3），集鸽数量（2），年（1），月（1），日（1），时（1），分（1），秒（1）
  procedure SendBuff1();
  var AYear, AMonth, ADay, AHour, AMinute, ASecond, AMilliSecond: Word;
  begin     //读取主机状态和信息并同步时钟:计算机发送80H，00H，年（1），月（1），日（1），时（1），分（1），秒（1）
    Buff[0]:= $80; Buff[1]:= 0;
    DecodeDateTime(Now, AYear, AMonth, ADay, AHour, AMinute, ASecond, AMilliSecond);
    Buff[2]:= AYear mod 100; Buff[3]:= AMonth; Buff[4]:= ADay;
    Buff[5]:= AHour; Buff[6]:= AMinute; Buff[7]:= ASecond;
    FComm.Send(Buff, 8);
  end;
  //读取时钟和记录指针（测试用）：
	//计算机发送80H，01H
	//主机返回：主机编号（3），年（1），月（1），日（1），时（1），分（1），秒（1）
  procedure SendBuff2();
  begin     //读取时钟和记录指针（测试用）：计算机发送80H，01H
    Buff[0]:= $80; Buff[1]:= 1;
    FComm.Send(Buff, 2);
  end;
  //删除一条主机回鸽、集鸽记录：
	//计算机发送80H，02H，电子环号（5）
	//主机返回：01H
  procedure SendBuff3();
  begin     //清除主机回鸽、集鸽记录：计算机发送80H，02H，电子环号（5）
    Buff[0]:= $80; Buff[1]:= 2;
    Buff[2]:= HexToInt(FTempStr[1] + FTempStr[2]);
    Buff[3]:= HexToInt(FTempStr[3] + FTempStr[4]);
    Buff[4]:= HexToInt(FTempStr[5] + FTempStr[6]);
    Buff[5]:= HexToInt(FTempStr[7] + FTempStr[8]);
    Buff[6]:= HexToInt(FTempStr[9] + FTempStr[10]);
    FComm.Send(Buff, 7);
  end;
  //读取回鸽、集鸽记录：
	//计算机发送80H，03H，记录序号（2）
	//主机返回：1个字节标志（0表示集鸽，1表示回鸽，2为已上传，FFH为空记录），5个字节统一环号，5个字节电子环号， 1个字节协会区号，1个字节赛事编号，3个字节脚环随机码，3个字节回鸽时间单位为秒，1个字节回鸽毫秒值单位为10毫秒（0-99），6个字节集鸽时间（年月日时分秒）,1个字节性别，1个字节眼砂，1个字节羽色，共29字节。
  procedure SendBuff4();
  begin     //4读取回鸽、集鸽记录：计算机发送80H，03H，记录序号（2）（0-511）
    Buff[0]:= $80; Buff[1]:= 3;
    Buff[2]:= FCurrentRecordIdx div 256; Buff[3]:= FCurrentRecordIdx mod 256;
    FComm.Send(Buff, 4);
  end;
  //写入集鸽数据：
  //计算机发送80H，04H，5个字节统一环号，5个字节电子环号，1个字节协会区号，1个字节赛事编号，3个字节脚环随机码，12个字节信息台号码（电话号码为字符串）
	//主机返回：01H

  //写入集鸽数据：
  //计算机发送80H，04H，5个字节统一环号，5个字节电子环号，1个字节协会区号，1个字节赛事编号，3个字节脚环随机码，12个字节信息台号码（电话号码为字符串）,1个字节性别，1个字节眼砂，1个字节羽色，共32字节。
	//主机返回：01H
  procedure SendBuff5();
  var
    D: Cardinal;

    function GetPhongNo(ch: Char): Byte;
    begin
      if ch = '-' then Result:= 0 else Result:= Ord(ch);
    end;
  begin     //5写入集鸽数据,计算机发送80H，04H，5个字节统一环号，5个字节电子环号，
            //1个字节协会区号，1个字节赛事编号，3个字节脚环随机码，12个字节信息台号码（电话号码为字符串）
            //1个字节性别，1个字节眼砂，1个字节羽色
            //Format('%0.2d   %0.2d   %0.7d   %s    %0.3d   %0.3d   %0.8d   %12S     %0.2d%  0.2d%0.2d',
            //      [GBYear,  GBArea, GBCode, ECode, AssID, GameID, RndNum, PhoneNo, Gender, Eye, Feather]);
            //Example: '010901234567E986912001423100054321010123456789'
    Buff[0]:= $80; Buff[1]:= 4;
    Buff[2]:= StrToInt(Copy(FTempStr, 1, 2));             //1统一环号年号 %0.2d   2-2  1-2
    Buff[3]:= StrToInt(Copy(FTempStr, 3, 2));             //1统一环号区号 %0.2d   3-3  3-4
//    Buff[2]:= (Ord(FTempStr[1]) - 48) * 16 - 48 + Ord(FTempStr[2]);             //1统一环号年号 %0.2d   2-2  1-2
//    Buff[3]:= (Ord(FTempStr[3]) - 48) * 16 - 48 + Ord(FTempStr[4]);             //1统一环号区号 %0.2d   3-3  3-4
    D:= StrToInt(Copy(FTempStr, 5, 7));
    Buff[4]:= D div 65536;                                                      //3统一环号号码 %0.7d   4-6  5-11
    Buff[5]:= D div 256;
    Buff[6]:= D;
    Buff[7]:= HexToInt(FTempStr[12] + FTempStr[13]);                            //5个字节电子环号 %10s  7-11 12-21
    Buff[8]:= HexToInt(FTempStr[14] + FTempStr[15]);
    Buff[9]:= HexToInt(FTempStr[16] + FTempStr[17]);
    Buff[10]:= HexToInt(FTempStr[18] + FTempStr[19]);
    Buff[11]:= HexToInt(FTempStr[20] + FTempStr[21]);
    Buff[12]:= StrToInt(Copy(FTempStr, 22, 3));                                 //1个字节节协会区号 %0.3d   12-12 22-24
    Buff[13]:= StrToInt(Copy(FTempStr, 25, 3));                                 //1个字节赛事编号 %0.3d     13-13 25-27
    D:= StrToInt(Copy(FTempStr, 28, 8));                                        //3个字节脚环随机码 %0.8d   14-16 28-35
    Buff[14]:= D div 65536;
    Buff[15]:= D div 256;
    Buff[16]:= D;
    Buff[17]:= GetPhongNo(FTempStr[36]); Buff[18]:= GetPhongNo(FTempStr[37]);         //12个字节信息台号码（电话号码为字符串）  17-28 36-47
    Buff[19]:= GetPhongNo(FTempStr[38]); Buff[20]:= GetPhongNo(FTempStr[39]);
    Buff[21]:= GetPhongNo(FTempStr[40]); Buff[22]:= GetPhongNo(FTempStr[41]);
    Buff[23]:= GetPhongNo(FTempStr[42]); Buff[24]:= GetPhongNo(FTempStr[43]);
    Buff[25]:= GetPhongNo(FTempStr[44]); Buff[26]:= GetPhongNo(FTempStr[45]);
    Buff[27]:= GetPhongNo(FTempStr[46]); Buff[28]:= GetPhongNo(FTempStr[47]);
    Buff[29]:= StrToInt(FTempStr[48] + FTempStr[49]);           //1个字节性别
    Buff[30]:= StrToInt(FTempStr[50] + FTempStr[51]);           //1个字节眼砂
    Buff[31]:= StrToInt(FTempStr[52] + FTempStr[53]);           //1个字节羽色
    FComm.Send(Buff, 32);
  end;
  //清除全部集鸽记录（测试用）：
	//计算机发送80H，05H
	//主机返回：01H
  procedure SendBuff6();
  begin     //6清除主机内存,计算机发送80H，05H
    Buff[0]:= $80; Buff[1]:= 5;
    FComm.Send(Buff, 2);
  end;
  //读取集鸽环号：通讯协议与集鸽踏板相同
	//计算机发送7EH，00H
	//主机返回：00表示无扫描环号，
	//02H表示有集鸽环号，随机数（5），毫秒值（3），加密电子环号（5）
  procedure SendBuff7();
  begin     //7读取集鸽环号,计算机发送7EH，00H
    Buff[0]:= $7E; Buff[1]:= 0;
    FComm.Send(Buff, 2);
  end;
begin
(*
通讯协议：
读取主机状态和信息并同步时钟：
计算机发送80H，00H，年（1），月（1），日（1），时（1），分（1），秒（1）
主机返回：主机编号（3），集鸽数量（2），年（1），月（1），日（1），时（1），分（1），秒（1）

设置主机编号：（测试用）
	计算机发送80H，7FH，编号（3）
	主机返回：01H

读取时钟和记录指针（测试用）：
	计算机发送80H，01H
	主机返回：主机编号（3），年（1），月（1），日（1），时（1），分（1），秒（1）

删除一条主机回鸽、集鸽记录：
	计算机发送80H，02H，电子环号（5）
	主机返回：01H

读取回鸽、集鸽记录：
	计算机发送80H，03H，记录序号（2）
	主机返回：1个字节标志（0表示集鸽，1表示回鸽，2为已上传，FFH为空记录），5个字节统一环号，5个字节电子环号， 1个字节协会区号，1个字节赛事编号，3个字节脚环随机码，3个字节回鸽时间单位为秒，1个字节回鸽毫秒值单位为10毫秒（0-99），6个字节集鸽时间（年月日时分秒）,1个字节性别，1个字节眼砂，1个字节羽色，共29字节。

写入集鸽数据：
计算机发送80H，04H，5个字节统一环号，5个字节电子环号， 1个字节协会区号，1个字节赛事编号，3个字节脚环随机码，12个字节信息台号码（电话号码为字符串）,1个字节性别，1个字节眼砂，1个字节羽色，共32字节。
	主机返回：01H

清除全部集鸽记录（测试用）：
	计算机发送80H，05H
主机返回：01H

读取内存：
计算机发送80H，10H，3个字节地址，1个字节长度
主机返回数据
清空内存：
计算机发送80H，11H，3个字节地址
主机返回01H

读取集鸽环号：通讯协议与集鸽踏板相同
	计算机发送7EH，00H
	主机返回：00表示无扫描环号，
02H表示有集鸽环号，随机数（5），毫秒值（3），加密电子环号（5）
*)
  FillChar(buff, 255, 0);
  case EventID of
  1: SendBuff1();    //读取主机状态和信息并同步时钟：
  2: SendBuff2();    //读取时钟和记录指针（测试用）：
  3: SendBuff3();    //删除一条主机回鸽、集鸽记录：
  4: SendBuff4();    //读取回鸽、集鸽记录：
  5: begin SendBuff5(); SendBuff5(); end;   //写入集鸽数据：   (回写两遍)
  6: SendBuff6();    //清除全部集鸽记录（测试用）
  7: SendBuff7();    //读取集鸽环号：通讯协议与集鸽踏板相同
  end;
end;

procedure TJunzhuoComm.EventToReceive(Sender: TObject);
var       //接收相应事件
  buff: array [0..255]of byte;
  szBf: array [0..4]of byte;
  i, j: Integer;
  szInfo: TJunzhuoPigeonInfo;
  szDatetime: TDatetime;

  function ReceiveMsg(): Integer;
  begin
    FillChar(buff, 255, 0);
    Result:= FCOMM.Receive(buff, 255, 100);
  end;

  procedure DoUserEvent(DoNext, DoSend: Boolean);
  begin
    if Assigned(FOnRecevered)then FOnRecevered(Self);
    if DoSend then begin
      DoSendEvent(FSendCmdID);
      FReceivedCount:= 0;
    end;
    FTimer.Enabled:= DoNext;
  end;

  procedure EndOfProcesser(ASuccessed: Boolean);
  begin
    FSucessed:= ASuccessed;
    if FSucessed then begin                       //接收成功
      case FStatus of
      csAutoReceive: DoUserEvent(False, False);   //自动接收->回调->结束
      csAutoSend:    DoUserEvent(True, True);     //自动发送->回调->发送->自动发送
      csManual:      DoUserEvent(False, False);   //手动->回调->结束
      csAuto:        DoUserEvent(True, True);     //全自动->回调->发送
      end;
      if not FTimer.Enabled then FStatus:= csStop;
    end else begin
      if FReceivedCount > 3 then begin            //连续三次接收失败
        case FStatus of
        csAutoReceive: DoUserEvent(False, False); //自动接收->回调->结束
        csAutoSend:
          if FSendCount < 3 then begin            //自动发送->连续三次发送
            DoSendEvent(FSendCmdID);
            Inc(FSendCount);
          end else DoUserEvent(False, False);     //连续三次发送失败->回调->结束
        csManual: DoUserEvent(False, False);      //手动->回调->结束
        csAuto:                                   //全自动->连续三次发送
          if FSendCount < 3 then begin
            DoSendEvent(FSendCmdID);
            Inc(FSendCount);
          end else begin
            DoSendEvent(FSendCmdID);
            FSendCount:= 0;
            FReceivedCount:= 0;                   //全自动->连续三次发送失败->异常
//            raise Exception.Create('通讯出现异常');
          end;
        end;
      end;
    end;
  end;

begin     //自动接收事件
  //接收主机状态数据
  (*
  1: SendBuff1();    //读取主机状态和信息并同步时钟：
  2: SendBuff2();    //读取时钟和记录指针（测试用）：
  3: SendBuff3();    //删除一条主机回鸽、集鸽记录：
  4: SendBuff4();    //读取回鸽、集鸽记录：
  5: SendBuff5();    //写入集鸽数据：
  6: SendBuff6();    //清除全部集鸽记录（测试用）
  7: SendBuff7();    //读取集鸽环号：通讯协议与集鸽踏板相同
  *)
  Inc(FReceivedCount);
  case FReceiveCmdID of
  1: if ReceiveMsg() >= 11 then begin
      //读取主机状态和信息并同步时钟：
      //计算机发送80H，00H，年（1），月（1），日（1），时（1），分（1），秒（1）
      //主机返回：主机编号（3），集鸽数量（2），年（1），月（1），日（1），时（1），分（1），秒（1）
      FMatchineNo:= buff[0] * 65536 + buff[1] * 256 + buff[2];
      FCollectionCount:= buff[3] * 256 + buff[4];
      if not TryEncodeDateTime(2000 + buff[5], buff[6], buff[7], buff[8], buff[9], buff[10], 0, FMatchineTime) then
        FMatchineTime:= Now;
      EndOfProcesser(true);
    end else EndOfProcesser(false);
  2: if ReceiveMsg() >= 9 then begin
      //读取时钟和记录指针（测试用）：
	    //计算机发送80H，01H
	    //主机返回：主机编号（3），年（1），月（1），日（1），时（1），分（1），秒（1）
      FMatchineNo:= buff[0] * 65536 + buff[1] * 256 + buff[2];
      if not TryEncodeDateTime(2000 + buff[3], buff[4], buff[5], buff[6], buff[7], buff[8], 0, FMatchineTime) then
        FMatchineTime:= 0;
      EndOfProcesser(true);
    end else EndOfProcesser(false);
  3,//-------- 接收写入集鸽数据应答 ---------
  5,//-------- 接收删除集鸽数据应答 ---------
  6://-------- 接收清除数据应答     ---------
(*删除一条主机回鸽、集鸽记录：
	计算机发送80H，02H，电子环号（5）
	主机返回：01H

读取回鸽、集鸽记录：
	计算机发送80H，03H，记录序号（2）
	主机返回：1个字节标志（0表示集鸽，1表示回鸽，2为已上传，FFH为空记录），5个字节统一环号，5个字节电子环号， 1个字节协会区号，1个字节赛事编号，3个字节脚环随机码，3个字节回鸽时间单位为秒，1个字节回鸽毫秒值单位为10毫秒（0-99），6个字节集鸽时间（年月日时分秒）,1个字节性别，1个字节眼砂，1个字节羽色，共29字节。

写入集鸽数据：
计算机发送80H，04H，5个字节统一环号，5个字节电子环号， 1个字节协会区号，1个字节赛事编号，3个字节脚环随机码，12个字节信息台号码（电话号码为字符串）,1个字节性别，1个字节眼砂，1个字节羽色，共32字节。
	主机返回：01H    //*)

	  //主机返回：01H
    if(ReceiveMsg() > 0)and(buff[0] = 1)then EndOfProcesser(true)
    else EndOfProcesser(false);
  4://读取回鸽、集鸽记录：
	  //计算机发送80H，03H，记录序号（2）
	  //主机返回：1个字节标志（0表示集鸽，1表示回鸽，2为已上传，FFH为空记录），             //0
    //5个字节统一环号，5个字节电子环号， 1个字节协会区号，1个字节赛事编号，               //1..5,6..10,11,12
    //3个字节脚环随机码，3个字节回鸽时间单位为秒，1个字节回鸽毫秒值单位为10毫秒（0-99）， //13..15,16..18,19
    //6个字节集鸽时间（年月日时分秒）,1个字节性别，1个字节眼砂，1个字节羽色，共29字节。   //20..25,26,27,28
    if(ReceiveMsg() >= 23)and(buff[0] < 3) then begin
      szInfo:= TJunzhuoPigeonInfo.Create;
      szInfo.Status:= buff[0];                       //1个字节标志（0表示集鸽，1表示回鸽，2为已上传，FFH为空记录）
      szInfo.GBCode:= Format('%0.2d', [buff[1]]) + Format('%0.2d', [buff[2]]) +
        Format('%0.7d', [buff[3] * 65536 + buff[4] * 256 + buff[5]]);         //5个字节统一环号
      szInfo.ElectronicCode:= IntToHex(buff[6], 2) + IntToHex(buff[7], 2) +
        IntToHex(buff[8], 2) + IntToHex(buff[9], 2) + IntToHex(buff[10], 2);  //5个字节电子环号
      szInfo.Association:= IntToHex(buff[11], 2);                             //1个字节协会区号
      szInfo.GameNo:= buff[12];                                               //1个字节赛事编号
      szInfo.RingGameNo:= buff[13] * 65536 + buff[14] * 256 + buff[15];       //3个字节脚环随机码
      szInfo.FlyTime:= buff[16] * 65536 + buff[17] * 256 + buff[18];          //3个字节回鸽时间单位为秒
      szInfo.LagValue:= buff[19];                                             //1个字节回鸽毫秒值单位为10毫秒（0-99）
      if not TryEncodeDateTime(2000 + buff[20], buff[21], buff[22],
        buff[23], buff[24], buff[25], 0, szDatetime) then szDatetime:= now;   //6个字节集鸽时间（年月日时分秒）
      szInfo.CollectionTime:= szDatetime;
      szInfo.Gender:= Ord(buff[26]);                                          //1个字节性别
      szInfo.Eye:= Ord(buff[27]);                                             //1个字节眼砂
      szInfo.Feather:= Ord(buff[28]);                                         //1个字节羽色
      szInfo.DataTime:= Now;
      FRecords.Add(szInfo);
      FCurrentRecordIdx:= (FCurrentRecordIdx + 1)mod 512;
      EndOfProcesser(true);
    end else EndOfProcesser(false);
  7://------  接收集鸽数据 -------
    //主机返回：00表示无扫描环号，02H表示有集鸽环号  0
    //          随机数(5)                            1-5
    //          毫秒值(3)                            6-8
    //          加密电子环号(5)                      9-13
    //通讯协议与集鸽踏板相同
	  //主机返回：00表示无扫描环号，02H表示有集鸽环号
    //          随机数（5）
    //          毫秒值（3）
    //          加密电子环号（5）
    if(ReceiveMsg() >= 14)and(buff[0] = 2)then begin  //读取回鸽、集鸽记录
      //解密电子环号
      for j:= 0 to 2 do begin
        szBf[0]:= (buff[13] and $F0) or (buff[ 9] and $0F);
        szBf[1]:= (buff[10] and $F0) or (buff[12] and $0F);
        szBf[2]:= (buff[12] and $F0) or (buff[10] and $0F);
        szBf[3]:= (buff[11] and $F0) or (buff[13] and $0F);
        szBf[4]:= (buff[ 9] and $F0) or (buff[11] and $0F);
        for i:= 0 to 4 do
          buff[9 + i]:= buff[i + 1] xor szBf[i];
      end;
      for i:= 0 to 4 do
        szBf[i]:= buff[9 + i];
      szInfo:= TJunzhuoPigeonInfo.Create;
      szInfo.ElectronicCode:= IntToHex(szBf[0], 2) + IntToHex(szBf[1], 2) +
        IntToHex(szBf[2], 2) + IntToHex(szBf[3], 2) + IntToHex(szBf[4], 2);
      szInfo.DataTime:= Now;
      FRecords.Add(szInfo);
      EndOfProcesser(true);
    end else EndOfProcesser(false);
  else
    FTimer.Enabled:= False;
  end;
//  if FReceivedCount > 3 then EndOfProcesser(false);
end;

function TJunzhuoComm.GetInfo(Index: Integer): TJunzhuoPigeonInfo;
begin
  if FRecords.Count > Index then
    Result:= TJunzhuoPigeonInfo(FRecords[Index])
  else Result:= nil;
end;

function TJunzhuoComm.GetInfoCount: Integer;
begin
  Result:= FRecords.Count;
end;

function TJunzhuoComm.GetInfos: TObjectList;
begin
  Result:= FRecords;
end;

function TJunzhuoComm.GetLoopTime: Integer;
begin
  Result:= FTimer.Interval;
end;

procedure TJunzhuoComm.GetMatchine;
begin     //2读取时钟和记录指针（测试用）：
  BeginRead(2, 2, 1000, csAutoReceive);
end;

function TJunzhuoComm.GetMatchineTime: TDateTime;
begin
  Result:= FMatchineTime;
end;

function TJunzhuoComm.GetOpened: boolean;
begin
  Result:= FCOMM.Opened;
end;

procedure TJunzhuoComm.InitMatchine;
begin     //1读取主机状态和信息并同步时钟
  BeginRead(1, 1, 1000, csAutoReceive);
end;

function TJunzhuoComm.Open(APort, ABaudRate: Integer; AParity: char;
  AByteSize, AStopBits: byte): Boolean;
begin    //打开通讯
  if not Assigned(FComm) then Exit;
  if FComm.Opened then begin
    if (APort = FPort)and(ABaudRate = FBaudRate)and (AParity = FParity)and
      (AByteSize = FByteSize)and(AStopBits = FStopBits) then Exit
    else FComm.Close;
  end;
  Result:= FComm.Open(APort, ABaudRate, AParity, AByteSize, AStopBits);
  FPort:= APort;
  FBaudRate:= ABaudRate;
  FParity:= AParity;
  FByteSize:= AByteSize;
  FStopBits:= AStopBits;
  FCurrentRecordIdx:= 0;
end;

procedure TJunzhuoComm.ReadCode(LoopKind: TCommStatusEmnu);
begin     //7读取集鸽环号
  FCurrentRecordIdx:= 0;
  BeginRead(7, 7, TIMEINTERVALUE, LoopKind);
end;

procedure TJunzhuoComm.ReadRecord;
begin     //4读取回鸽、集鸽记录
  FCurrentRecordIdx:= 0;
  BeginRead(4, 4, TIMEINTERVALUE, csAutoSend);
end;

procedure TJunzhuoComm.SetBaudRate(const Value: longint);
begin
  FBaudRate := Value;
end;

procedure TJunzhuoComm.SetByteSize(const Value: word);
begin
  FByteSize := Value;
end;

procedure TJunzhuoComm.SetCommunicationType(const Value: string);
begin
  FType := Value;
end;

procedure TJunzhuoComm.SetCurrentRecordIdx(const Value: Integer);
begin
  FCurrentRecordIdx := Value;
end;

procedure TJunzhuoComm.SetOnRecevered(const Value: TNotifyEvent);
begin
  FOnRecevered := Value;
end;

procedure TJunzhuoComm.SetOpened(const Value: boolean);
begin
  FOpened := Value;
end;

procedure TJunzhuoComm.SetParity(const Value: char);
begin
  FParity := Value;
end;

procedure TJunzhuoComm.SetPort(const Value: word);
begin
  FPort := Value;
end;

procedure TJunzhuoComm.SetStatus(const Value: TCommStatusEmnu);
begin
  FStatus := Value;
end;

procedure TJunzhuoComm.SetStopBits(const Value: word);
begin
  FStopBits := Value;
end;

procedure TJunzhuoComm.SetTimerOut(const Value: Integer);
begin
  FTimerOut := Value;
end;

procedure TJunzhuoComm.WriteCollection(GBYear, GBArea, GBCode: Integer;
  ECode: string; AssID, GameID, RndNum: Integer; PhoneNo: string; Gender, Eye, Feather: Integer);
begin     //5写入集鸽数据
  FTempStr:= Format('%0.2d%0.2d%0.7d%s%0.3d%0.3d%0.8d%.12S%0.2d%0.2d%0.2d',
    [GBYear, GBArea, GBCode, ECode, AssID, GameID, RndNum, Copy(PhoneNo + '------------', 1, 12), Gender, Eye, Feather]);
  WriteLog(LOG_DEBUG, 'WriteCollection', FTempStr);
  BeginRead(5, 5, TIMEINTERVALUE, csManual);
end;

{ TJunzhuoPigeonInfo }

procedure TJunzhuoPigeonInfo.SetCollectionTime(const Value: TDatetime);
begin
  FCollectionTime := Value;
end;

procedure TJunzhuoPigeonInfo.SetDataTime(const Value: TDatetime);
begin
  FDataTime := Value;
end;

procedure TJunzhuoPigeonInfo.SetElectronicCode(const Value: string);
begin
  if(JZRings.CheckRing(HexToInt(Value)) < 0) then begin
    FElectronicCode := NO_VALID_RING;
    WriteLog(LOG_DEBUG, 'NO_VALID_RING', Value);
  end else FElectronicCode := Value;
end;

procedure TJunzhuoPigeonInfo.SetEye(const Value: Integer);
begin
  FEye := Value;
end;

procedure TJunzhuoPigeonInfo.SetFeather(const Value: Integer);
begin
  FFeather := Value;
end;

procedure TJunzhuoPigeonInfo.SetGender(const Value: Integer);
begin
  FGender := Value;
end;

{ TDalianYunfeiCom }

procedure TDalianYunfeiCom.ClearPigeon;
begin
  FRecords.Clear;
end;

constructor TDalianYunfeiCom.Create(AType: string);
begin       //创建通讯端口
  if AType = 'FILE' then FCOMM:= TCommFile.Create
  else raise Exception.Create('Not found the ' + AType + ' type of communication.');
  FTimer:= TTimer.Create(nil);
  FTimer.Enabled:= False;
  FTimer.Interval:= TIMEINTERVALUE;
  FTimer.OnTimer:= ReadPigeonBacktime;
end;

destructor TDalianYunfeiCom.Destroy;
begin
  FTimer.Enabled:= False;
  FTimer.Free;
  inherited;
end;

function TDalianYunfeiCom.GetIsReader: Boolean;
begin
  Result:= FTimer.Enabled;
end;

function TDalianYunfeiCom.GetPigeonBacktime(Index: Integer): Integer;
begin
  Result:= StrToInt(FRecords[Index]);
end;

function TDalianYunfeiCom.GetPigeonCount: Integer;
begin
  Result:= FRecords.Count;
end;

procedure TDalianYunfeiCom.Open(APort, ABaudRate: Integer; AParity: char;
  AByteSize, AStopBits: byte);
begin     //打开通讯
  if not Assigned(FComm) then Exit;
  if FComm.Opened then begin
    if (APort = FPort)and(ABaudRate = FBaudRate)and (AParity = FParity)and
      (AByteSize = FByteSize)and(AStopBits = FStopBits) then Exit
    else FComm.Close;
  end;
  FComm.Open(APort, ABaudRate, AParity, AByteSize, AStopBits);
end;

procedure TDalianYunfeiCom.ReadPigeonBacktime(Sender: TObject);
var
  i: Integer;
  buffer: array[0..72]of byte;
begin
  FComm.Receive(buffer, 72, 30);
  if Length(buffer) <> 72 then begin
    FTimer.Enabled:= False;
    Exit;
  end;
  for i:= 0 to buffer[71] - 1 do
    FRecords.Add(IntToStr(buffer[i * 3] * 3600 + buffer[i * 3 + 1] * 60 + buffer[i * 3 + 2]));
end;

end.
