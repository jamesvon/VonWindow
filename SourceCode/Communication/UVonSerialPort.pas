unit UVonSerialPort;

interface

uses Types, SysUtils, Classes, WinApi.Windows, Forms, ExtCtrls, Contnrs,
  MSCommLib_TLB, System.Variants, WinApi.ActiveX ;

const
  MAXBUFFERSIZE = 1024;
  TIMEINTERVALUE = 200;

type
  TVonComm = class(TObject)
  protected
    function GetOpened: Boolean; virtual; abstract;
  public
    // 打开串口 APort:端口号 ABaudRate:波特率 AParity:校验位 AByteSize:数据位 AStopBits:停止位
    // 例：Open('COM1',CBR_9600,'n',8,1)
    // 波特率：110 300 600 1200 2400 4800 14400 19200 38400 56000 57600 115200 128000 256000
    // 校验位: 'n'=no,'o'=odd,'e'=even,'m'=mark,'s'=space
    // 数据位： 4-8 (other=8)
    // 停止位： 1-2 (other=1.5)  //*)
    function Open(APort: word; ABaudRate: longint = CBR_9600; AParity: char = 'n';
      AByteSize : byte = 8; AStopBits: byte = 1): Boolean; overload; virtual; abstract;
    function Open(APort: string; ABaudRate: longint = CBR_9600; AParity: char = 'n';
      AByteSize : byte = 8; AStopBits: byte = 1): Boolean; overload; virtual; abstract;
    // 向串口发一个字符
    procedure PutChar(ch: char); virtual; abstract;
    // 向串口发一个字符串
    procedure Puts(s: string); virtual; abstract;
    // 写串口 buf:存放要写的数据，len:要写的长度
    procedure Send(var buf : array of byte; len: DWord); virtual; abstract;
    // 读串口 buf:存放读到的数据，len: 存储空间大小，tmout:超时值(ms) 返回: 实际读到的字符数
    function Receive(var buf; len: DWord; tmout: DWord): integer; virtual; abstract;
    // 关闭串口
    procedure Close; virtual; abstract;
  published
    // Comm是否打开
    property Opened: Boolean read GetOpened;
  end;

  TUartSerialPort = class(TVonComm)
  private
    OpenFlag: Boolean; // 打开标志
    handle: THandle;
    function GetOpened: Boolean; override;// 串口句柄
  public
    constructor Create;
    destructor Destroy; override;
    // 打开串口，例：Open('COM1',CBR_9600,'n',8,1)
    function Open(APort: word; ABaudRate: longint = CBR_9600; AParity: char = 'n';
      AByteSize : byte = 8; AStopBits: byte = 1): Boolean; overload; override;
    function Open(APort: string; ABaudRate: longint = CBR_9600; AParity: char = 'n';
      AByteSize : byte = 8; AStopBits: byte = 1): Boolean; overload; override;
    // 关闭串口
    procedure Close; override;
    // 返回输入缓冲区中的字符数
    function InbufChars: DWord;
    // 返回输出缓冲区中的字符数
    function OutBufChars: longint;
    // 写串口 buf:存放要写的数据，len:要写的长度
    procedure Send(var buf : array of byte; len: DWord); override;
    // 读串口 buf:存放读到的数据，len:要读的长度，tmout:超时值(ms)
    // 返回: 实际读到的字符数
    function Receive(var buf; len: DWord; tmout: DWord): integer; override;
    // 向串口发一个字符
    procedure PutChar(ch: char); override;
    // 向串口发一个字符串
    procedure Puts(s: string); override;
    // 从串口读一个字符，未收到字符则返回#0
    function GetChar: char;
    // 从串口取一个字符串，忽略不可见字符,收到#13或255个字符结束，tmout:超时值(ms)
    // 返回:true表示收到完整字符串，false表示超时退出
    function Gets(var s: string; tmout: integer): Boolean;
    // 清接收缓冲区
    procedure ClearInBuf;
    // 等待一个字符串,tmout:超时值(ms),返回false表示超时
    function Wait(s: String; tmout: integer): Boolean;
    // 执行一个AT命令，返回true表示成功
    function ExecAtComm(s: string): Boolean;
    // 挂机，返回true表示成功
    function Hangup: Boolean;
    // 应答，返回true表示成功
    function Answer: Boolean;

    property Opened: Boolean read GetOpened;
  end;

  TMSSerialPort = class(TVonComm)
  private
    FComm: TMSComm;
  protected
    function GetOpened: Boolean; override;
  public
    constructor Create;
    destructor Destroy; override;
    // 打开串口 APort:端口号 ABaudRate:波特率 AParity:校验位 AByteSize:数据位 AStopBits:停止位
    // 例：Open('COM1',CBR_9600,'n',8,1)
    // 波特率：110 300 600 1200 2400 4800 14400 19200 38400 56000 57600 115200 128000 256000
    // 校验位: 'n'=no,'o'=odd,'e'=even,'m'=mark,'s'=space
    // 数据位： 4-8 (other=8)
    // 停止位： 1-2 (other=1.5)  //*)
    function Open(APort: word; ABaudRate: longint = CBR_9600; AParity: char = 'n';
      AByteSize : byte = 8; AStopBits: byte = 1): Boolean; overload; override;
    function Open(APort: string; ABaudRate: longint = CBR_9600; AParity: char = 'n';
      AByteSize : byte = 8; AStopBits: byte = 1): Boolean; overload; override;
    // 向串口发一个字符
    procedure PutChar(ch: char); override;
    // 向串口发一个字符串
    procedure Puts(s: string); override;
    // 写串口 buf:存放要写的数据，len:要写的长度
    procedure Send(var buf : array of byte; len: DWord); override;
    // 读串口 buf:存放读到的数据，len: 存储空间大小，tmout:超时值(ms) 返回: 实际读到的字符数
    function Receive(var buf; len: DWord; tmout: DWord): integer; override;
    // 关闭串口
    procedure Close; override;
  published
    // Comm是否打开
    property Opened: Boolean read GetOpened;
  end;

implementation

uses Math, DateUtils;

function BCDToInt(BCD: byte): word; // 将BCD码转换为整数
begin
  Result := (BCD shr 4) * 10 + (BCD and $0F)
end;

{$H+}

const
  CommInQueSize = 4096; // 输入缓冲区大小
  CommOutQueSize = 4096; // 输出缓冲区大小

  (* **********************      计时器        ********************** *)

type
  Timers = Class
  private
    StartTime: TTimeStamp;
  public
    constructor Create;
    Procedure Start; // 开始计时
    Function Get: longint; // 返回计时值(单位:ms)
  end;

constructor Timers.Create;
begin // 构造函数
  inherited Create;
  Start;
end;

procedure Timers.Start;
begin // 开始计时
  StartTime := DateTimeToTimeStamp(Now);
end;

function Timers.Get: longint;
var // 返回计时值(单位:ms)
  CurTime: TTimeStamp;
begin
  CurTime := DateTimeToTimeStamp(Now);
  Result := (CurTime.Date - StartTime.Date) * 24 * 3600000 +
    (CurTime.Time - StartTime.Time);
end;

(* **********************      TUart     ********************** *)

constructor TUartSerialPort.Create;
begin // 构造函数
  inherited Create;
  OpenFlag := False;
end;

Destructor TUartSerialPort.Destroy;
begin // 析构函数
  Close;
  inherited Destroy;
end;

(* 例：Open('COM1',CBR_9600,'n',8,1)
  波特率：
  CBR_110     CBR_19200
  CBR_300     CBR_38400
  CBR_600     CBR_56000
  CBR_1200    CBR_57600
  CBR_2400    CBR_115200
  CBR_4800    CBR_128000
  CBR_14400   CBR_256000
  校验位:
  'n'=no,'o'=odd,'e'=even,'m'=mark,'s'=space
  数据位： 4-8 (other=8)
  停止位： 1-2 (other=1.5)  // *)
function TUartSerialPort.Open(APort: word; ABaudRate: longint = CBR_9600; AParity: char = 'n';
      AByteSize : byte = 8; AStopBits: byte = 1): Boolean;
begin
  Result:= Open('COM' + IntToStr(APort), ABaudRate, AParity, AByteSize, AStopBits);
end;

function TUartSerialPort.Open(APort: string; ABaudRate: longint = CBR_9600; AParity: char = 'n';
      AByteSize : byte = 8; AStopBits: byte = 1): Boolean;
var
  dcb: TDCB;
begin    // 打开串口
  Result := False;
  OpenFlag := False;
  // 初始化串口
  handle := CreateFile(PChar(APort),
    GENERIC_READ + GENERIC_WRITE, 0, nil, OPEN_EXISTING, 0, 0);
  if handle = INVALID_HANDLE_VALUE then
    Exit;
  GetCommState(handle, dcb);
  with dcb do
  begin
    BaudRate := ABaudRate; // 波特率
    if AByteSize in [4, 5, 6, 7, 8] then
      ByteSize := AByteSize // 数据位
    else
      ByteSize := 8;
    case AStopBits of // 停止位
      1:
        StopBits := 0; // 1
      2:
        StopBits := 2; // 2
    else
      StopBits := 0; // 1
    end;
    case AParity of // 校验
      'n', 'N':
        Parity := 0; // no
      'o', 'O':
        Parity := 1; // odd
      'e', 'E':
        Parity := 2; // even
      'm', 'M':
        Parity := 3; // mark
      's', 'S':
        Parity := 4; // space
    else
      Parity := 0; // no
    end;
  end;
  SetCommState(handle, dcb);
  SetupComm(handle, CommOutQueSize, CommInQueSize);
  OpenFlag := True;
  Result := True;
end;

// 关闭串口
procedure TUartSerialPort.Close;
begin
  if OpenFlag then
  begin
    CloseHandle(handle);
    handle := 0;
  end;
  OpenFlag := False;
end;

// 检测输入缓冲区中的字符数
function TUartSerialPort.InbufChars: DWord;
var
  ErrCode: DWord;
  Stat: TCOMSTAT;
begin
  Result := 0;
  if not OpenFlag then
    Exit;
  ClearCommError(handle, ErrCode, @Stat);
  Result := Stat.cbInQue;
end;

// 检测输出缓冲区中的字符数
function TUartSerialPort.OutBufChars: longint;
var
  ErrCode: DWord;
  Stat: TCOMSTAT;
begin
  Result := 0;
  if not OpenFlag then
    Exit;
  ClearCommError(handle, ErrCode, @Stat);
  Result := Stat.cbOutQue;
end;

// 写串口 buf:存放要写的数据，len:要写的长度
procedure TUartSerialPort.Send(var buf : array of byte; len: DWord);
var
  i: DWord;
begin
  WriteFile(handle, buf, len, i, nil); // 写串口
end;

// 读串口 buf:存放读到的数据，len:要读的长度，tmout:超时值(ms)
// 返回: 实际读到的字符数
function TUartSerialPort.Receive(var buf; len: DWord; tmout: DWord): integer;
var
  Timer: Timers;
  i: DWord;
  BufChs: DWord;
begin
  Timer := Timers.Create;
  Timer.Start;
  repeat
    Application.ProcessMessages;
  until (InbufChars >= len) or (Timer.Get > tmout); // 收到指定长度数据或超时
  BufChs := InbufChars;
  if len > BufChs then
    len := BufChs;
  ReadFile(handle, buf, len, i, nil); // 读串口
  Result := i;
  Timer.free;
end;

// 向串口发一个字符
procedure TUartSerialPort.PutChar(ch: char);
var
  i: DWord;
begin
  i:= 1;
  WriteFile(handle, ch, 1, i, nil); // 写串口
end;

// 向串口发一个字符串
procedure TUartSerialPort.Puts(s: string);
var
  i: integer;
begin
  for i := 1 to length(s) do
    PutChar(s[i]);
end;

// 从串口读一个字符，未收到字符则返回#0
function TUartSerialPort.GetChar: char;
var
  i: DWord;
begin
  Result := #0;
  if InbufChars > 0 then
    ReadFile(handle, Result, 1, i, nil); // 读串口
end;

function TUartSerialPort.GetOpened: Boolean;
begin
  Result:= OpenFlag;
end;

// 从串口取一个字符串，忽略不可见字符,收到#13或255个字符结束，tmout:超时值(ms)
// 返回:true表示收到完整字符串，false表示超时退出
function TUartSerialPort.Gets(var s: string; tmout: integer): Boolean;
var
  Timer: Timers;
  ch: char;
begin
  Timer := Timers.Create;
  Timer.Start;
  s := '';
  Result := False;
  repeat
    ch := GetChar;
    if ch <> #0 then
      Timer.Start; // 如收到字符则清定时器
    if ch >= #32 then
      s := s + ch;
    if (ch = #13) or (length(s) >= 255) then
    begin
      Result := True;
      break;
    end;
  until Timer.Get > tmout; // 超时
  Timer.free;
end;

// 清接收缓冲区
procedure TUartSerialPort.ClearInBuf;
begin
  if not OpenFlag then
    Exit;
  PurgeComm(handle, PURGE_RXCLEAR);
end;

// 等待一个字符串,tmout:超时值(ms),返回false表示超时
function TUartSerialPort.Wait(s: String; tmout: integer): Boolean;
var
  s1: string;
  Timer: Timers;
begin
  Timer := Timers.Create;
  Timer.Start;
  Result := False;
  repeat
    Gets(s1, tmout);
    if pos(s, s1) > 0 then
    begin
      Result := True;
      break
    end;
  until Timer.Get > tmout;
  Timer.free;
end;

// 执行一个AT命令，返回true表示成功
function TUartSerialPort.ExecAtComm(s: string): Boolean;
begin
  ClearInBuf;
  Puts(s);
  Result := False;
  if Wait('OK', 3000) then
    Result := True;
end;

// 挂机，返回true表示成功
function TUartSerialPort.Hangup: Boolean;
begin
  Result := False;
  ExecAtComm('+++');
  if not ExecAtComm('ATH'#13) then
    Exit;
  Result := True;
end;

// 应答，返回true表示成功
function TUartSerialPort.Answer: Boolean;
begin
  ClearInBuf;
  Puts('ATA'#13);
  Result := False;
  if Wait('CONNECT', 30000) then
    Result := True;
end;

{ TMSSerialPort }

procedure TMSSerialPort.Close;
begin
  FComm.PortOpen:= False;
end;

constructor TMSSerialPort.Create;
begin
  FComm:= TMSComm.Create(nil);
end;

destructor TMSSerialPort.Destroy;
begin
  FComm.Free;
  inherited;
end;

function TMSSerialPort.GetOpened: Boolean;
begin
  Result:= FComm.PortOpen;
end;

function TMSSerialPort.Open(APort: word; ABaudRate: Integer; AParity: char;
  AByteSize, AStopBits: byte): Boolean;
begin
  with FComm do begin
    CommPort := APort;                     //指定端口
    Settings := Format('%d,%s,%d,%d', [ABaudRate, AParity, AByteSize, AStopBits]);         //其它参数
    InBufferSize := 1;             //接收缓冲区
    OutBufferSize := 1;             //发送缓冲区
    InputMode := comInputModeBinary; //接收模式
    InputLen := 1;                     //一次读取所有数据
    SThreshold := 0;                   //一次发送所有数据
    InBufferCount := 0;               //清空读取缓冲区
    OutBufferCount := 0;               //清空发送缓冲区
    RThreshold := 1;               //设置接收多少字节开产生oncomm事件
    PortOpen:= true;                     //打开端口
  end;
end;

function TMSSerialPort.Open(APort: string; ABaudRate: Integer; AParity: char;
  AByteSize, AStopBits: byte): Boolean;
begin
  Open(StrToInt(Copy(APort, 4, MaxInt)), ABaudRate, AParity, AByteSize, AStopBits);
end;

procedure TMSSerialPort.PutChar(ch: char);
var
  val: Variant;
begin
  val := VarArrayCreate([0, 0], VT_UI1);
  val[0]:= Ord(ch);
  FComm.Output:= val;
end;

procedure TMSSerialPort.Puts(s: string);
begin
  FComm.Output:= S;
end;

function TMSSerialPort.Receive(var buf; len, tmout: DWord): integer;
var
  reData: array of byte;
  I: Integer;
begin
  if FComm.CommEvent <> comEvReceive then Exit;
  reData:= FComm.Input;       //接收数据
  if len > vararrayhighbound(redata, 1) then result:= vararrayhighbound(redata, 1) + 1
  else result:= Len;
  if result <= 0 then Exit;
  Move(reData[0], buf, result);
  FComm.InputLen:= 0;
end;

procedure TMSSerialPort.Send(var buf : array of byte; len: DWord);
var
  reData: array of byte;
begin
  SetLength(reData, len);
  Move(buf, reData[0], len);
  FComm.Output:= reData;
  SetLength(reData, 0);
end;

end.
