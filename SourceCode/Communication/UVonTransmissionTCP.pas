unit UVonTransmissionTCP;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, IdContext, IdAntiFreezeBase, IdAntiFreeze, IdBaseComponent,
  IdComponent, IdCustomTCPServer, IdTCPServer, IdTCPClient, UVonLog,
  UVonTransmissionBase;

type

  TVonTransmissionTCPServer = class(TVonTransmissionServer)
  private
    FTCPSvr: TIdTCPServer;
    procedure FTCPSvrExecute(AContext: TIdContext);
    procedure FTCPSvrDisconnect(AContext: TIdContext);
    procedure FTCPSvrConnect(AContext: TIdContext);
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Start(Port: Integer); override;
    procedure Stop; override;
  end;

  TVonTransmissionTCPClient = class(TVonTransmissionClient)
  private
    FTCPClient: TIdTCPClient;
  public
    constructor Create(AppClient: TVonApplicationClientBase); override;
    destructor Destroy; override;
    procedure SendIt(clientMsg: TVonTransmissionClientMsg;
      serverMsg: TVonTransmissionServerMsg); override;
  end;

implementation

{ TVonTransmissionTCPServer }

constructor TVonTransmissionTCPServer.Create;
begin
  inherited;
  FTCPSvr := TIdTCPServer.Create(nil);
  FTCPSvr.OnConnect := FTCPSvrConnect;
  FTCPSvr.OnExecute := FTCPSvrExecute;
  FTCPSvr.OnDisconnect := FTCPSvrDisconnect;
  WriteLog(LOG_INFO, 'TVonTransmissionTCPServer', 'Created');
end;

destructor TVonTransmissionTCPServer.Destroy;
begin
  FTCPSvr.Active := False;
  FTCPSvr.Free;
  inherited;
end;

procedure TVonTransmissionTCPServer.FTCPSvrConnect(AContext: TIdContext);
begin
  with AContext.Binding do
    WriteLog(LOG_INFO, 'TCPSvr', Format(RES_COMM_ConnectionInfo,
      [PeerIP, PeerPort]));
end;

procedure TVonTransmissionTCPServer.FTCPSvrDisconnect(AContext: TIdContext);
begin
  with AContext.Binding do
    WriteLog(LOG_INFO, 'TCPSvr', Format(RES_COMM_DisconnectionInfo,
      [PeerIP, PeerPort]));
end;

procedure TVonTransmissionTCPServer.FTCPSvrExecute(AContext: TIdContext);
var
  clientMsg: TVonTransmissionClientMsg;
  serverMsg: TVonTransmissionServerMsg;
  szStream, szOutStream, szTmpStream: TStream;
  szBuff: array [0 .. 3] of byte;
  msgSize: Cardinal;
  sendSize: Cardinal;
begin
  inherited;
  // 接收客户端发过来的信息长度（流信息的前4个字节）
  szStream := TMemoryStream.Create;
  AContext.Connection.IOHandler.ReadStream(szStream, 4);
  szStream.Position := 0;
  szStream.Read(szBuff, 4);
  szStream.Free;
  msgSize := szBuff[0] * 16777216 + szBuff[1] * 65536 + szBuff[2] * 256 +
    szBuff[3];
  if msgSize > MAX_BUFFER_SIZE then
  begin
    with AContext.Binding do
      WriteLog(LOG_FAIL, 'TCPSvr', Format(RES_COMM_MaxErrorInfo,
        [msgSize, PeerIP, PeerPort]));
    AContext.Connection.Disconnect;
    Exit;
  end;
  // 确定接收长度，接收全部信息
  with AContext.Binding do
    WriteLog(LOG_INFO, 'TCPSvr', Format(RES_COMM_MessageInfo,
      [msgSize, PeerIP, PeerPort]));
  szTmpStream := TMemoryStream.Create; // 临时流，接收准备发送的流内容
  clientMsg := TVonTransmissionClientMsg.Create;
  try
    AContext.Connection.IOHandler.ReadStream(szTmpStream, msgSize, False);
    // 接收全部流信息
    if not clientMsg.ReadFromStream(szTmpStream) then // 分解信息
      raise Exception.Create('Can not process a message');
    serverMsg:= TVonTransmissionServerMsg.Create(clientMsg.AppName, clientMsg.Flag);
    ReceivedIt(clientMsg, serverMsg);     // 处理流内容，并将准备发送的内容回写到 Msg 中
    szStream := serverMsg.SendStream;
    // ShowStream(szStream);
    sendSize := szStream.Size;
    szOutStream := TMemoryStream.Create;  // 最终发送的流内容
    try
      szOutStream.Write(sendSize, 4);     // 写入返回流长度
      szOutStream.CopyFrom(szStream, sendSize);
      AContext.Connection.IOHandler.Write(szOutStream); // 发送
    finally
      serverMsg.Free;
      szOutStream.Free;
    end;
  except
    on E: Exception do
    begin
      WriteLog(LOG_INFO, 'TCPSvr', 'Faild to process a message.' + E.Message);
    end;
  end;
  szTmpStream.Free;
  clientMsg.Free;
  AContext.Connection.Disconnect;
end;

procedure TVonTransmissionTCPServer.Start(Port: Integer);
begin
  inherited;
  WriteLog(LOG_INFO, 'TCPSvr', 'Will to start at ' + IntToStr(Port));
  if FTCPSvr.Active then
    FTCPSvr.Active := False;
  FTCPSvr.DefaultPort := Port;
  FTCPSvr.Active := True;
  WriteLog(LOG_INFO, 'TCPSvr', 'Started at ' + IntToStr(Port));
end;

procedure TVonTransmissionTCPServer.Stop;
begin
  inherited;
  FTCPSvr.Active := False;
end;

{ TVonTransmissionTCPClient }

constructor TVonTransmissionTCPClient.Create(AppClient: TVonApplicationClientBase);
begin
  inherited;
  FTCPClient := TIdTCPClient.Create(nil);
end;

destructor TVonTransmissionTCPClient.Destroy;
begin
  FTCPClient.Free;
  inherited;
end;

procedure TVonTransmissionTCPClient.SendIt(clientMsg: TVonTransmissionClientMsg;
      serverMsg: TVonTransmissionServerMsg);
var
  msgSize: Integer;
  szStream, sendStream: TStream;
begin
  inherited;
  clientMsg.AppName := FAppClient.AppName;
  sendStream := clientMsg.SendStream; // 准备发送的流信息
  WriteLog(LOG_INFO, 'TCPClient', Format(RES_COMM_ConnectToInfo,
    [FAppClient.HostIP, FAppClient.HostPort]));
  FTCPClient.Connect(FAppClient.HostIP, FAppClient.HostPort); // 连接服务器
  if not FTCPClient.Connected then
    raise Exception.Create('Can not connecte server.');
  WriteLog(LOG_INFO, 'TCPClient', Format('Ready to send %s to %s with %s',
    [clientMsg.CommandString, clientMsg.AppName, clientMsg.Params.Text]));
  FTCPClient.IOHandler.Write(sendStream, sendStream.Size, True); // 发送
  szStream := TMemoryStream.Create;
  FTCPClient.IOHandler.ReadStream(szStream, 4, False); // 接收数据长度信息
  szStream.Position := 0;
  szStream.Read(msgSize, 4);
  szStream.Size := 0;
  FTCPClient.IOHandler.ReadStream(szStream, msgSize, False); // 接收数据内容
  if not serverMsg.ReadFromStream(szStream) then
    raise Exception.Create
      ('Can not read a stream what is received from server');
  WriteLog(LOG_INFO, 'TCPClient', Format('Receive a %d from %s with %s',
    [Ord(serverMsg.ReturnValue), serverMsg.AppName, serverMsg.ErrorInfo]));
  szStream.Free;
  FTCPClient.Disconnect; // 断开连接
  WriteLog(LOG_INFO, 'TCPClient', Format(RES_COMM_DisconnectToInfo,
    [FAppClient.HostIP, FAppClient.HostPort]));
end;

initialization

  TVonTransmissionServer.RegistTransmission('TCP', TVonTransmissionTCPServer);
  TVonApplicationClientBase.RegistTransmission('TCP', TVonTransmissionTCPClient);

end.

