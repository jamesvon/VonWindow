// =============================================================================
//
// Barcode VCL Component for Quick Report
//
// For Delphi 4/5/6/7, C++ Builder 4/5/6, BDS 2005/2005, Turbo Delphi 2006
//
// Copyright (c) 2001, 2007  Han-soft Software, all rights reserved.
//
// $Rev: 44 $   $Id: pQRBarcodeReg.pas 44 2007-01-16 01:16:04Z hanjy $
//
// =============================================================================

unit pQRBarcodeReg;

interface

uses
  Classes;

procedure Register;

implementation

uses
  pQRBarcode, pQRDBBarcode;

procedure Register;
begin
  RegisterComponents('Han-soft', [TQRBarcode, TQRDBBarcode]);
end;

end.
