{ PASCAL INTERFACE for AVICAP32 DLL }
{ Converted from microsoft Header file by ArTee }
{ Free to use - I'm NOT responsible fo bugs }
unit UAviCap32;

interface

uses Windows, Messages, SysUtils, MMSystem;

// This part might be buggy.
// It 'Assumes' types because I cannot find the correct unit
type
  HVIDEO = longword; // Handle;
  UINT = longword;

  POINT = record
    x, y: integer end;
    LPPOINT = ^POINT;

    FOURCC = record c1, c2, c3, c4: char;
  end;

  mmioFOURCC = FOURCC;
  LPVOID = Pointer;
  // LPWAVEHDR, LPVIDEOHDR worden gebruikt in de call-back functions! ik heb ze maar gereplaced met integers.

  { ***************************************************************************
    *
    *   avicap.h
    *
    *   Main include file.
    *
    *   Microsoft Video for Windows Sample Capture Class
    *
    *   Copyright (c) 1992, 1993 Microsoft Corporation.  All Rights Reserved.
    *
    *    You have a royalty-free right to use, modify, reproduce and
    *    distribute the Sample Files (and/or any modified version) in
    *    any way you find useful, provided that you agree that
    *    Microsoft has no warranty obligations or liability for any
    *    Sample Application Files which are modified.
    *
    ************************************************************************** }
  // const AVICapSM =SendMessage;    { SendMessage in C }


  // ------------------------------------------------------------------
  // Window Messages  WM_CAP... which can be sent to an AVICAP window
  // ------------------------------------------------------------------

  // Defines start of the message range
const
  WM_CAP_START = WM_USER;

const
  WM_CAP_GET_CAPSTREAMPTR = (WM_CAP_START + 1);

const
  WM_CAP_SET_CALLBACK_ERROR = (WM_CAP_START + 2);

const
  WM_CAP_SET_CALLBACK_STATUS = (WM_CAP_START + 3);

const
  WM_CAP_SET_CALLBACK_YIELD = (WM_CAP_START + 4);

const
  WM_CAP_SET_CALLBACK_FRAME = (WM_CAP_START + 5);

const
  WM_CAP_SET_CALLBACK_VIDEOSTREAM = (WM_CAP_START + 6);

const
  WM_CAP_SET_CALLBACK_WAVESTREAM = (WM_CAP_START + 7);

const
  WM_CAP_GET_USER_DATA = (WM_CAP_START + 8);

const
  WM_CAP_SET_USER_DATA = (WM_CAP_START + 9);

const
  WM_CAP_DRIVER_CONNECT = (WM_CAP_START + 10);

const
  WM_CAP_DRIVER_DISCONNECT = (WM_CAP_START + 11);

const
  WM_CAP_DRIVER_GET_NAME = (WM_CAP_START + 12);

const
  WM_CAP_DRIVER_GET_VERSION = (WM_CAP_START + 13);

const
  WM_CAP_DRIVER_GET_CAPS = (WM_CAP_START + 14);

const
  WM_CAP_FILE_SET_CAPTURE_FILE = (WM_CAP_START + 20);

const
  WM_CAP_FILE_GET_CAPTURE_FILE = (WM_CAP_START + 21);

const
  WM_CAP_FILE_ALLOCATE = (WM_CAP_START + 22);

const
  WM_CAP_FILE_SAVEAS = (WM_CAP_START + 23);

const
  WM_CAP_FILE_SET_INFOCHUNK = (WM_CAP_START + 24);

const
  WM_CAP_FILE_SAVEDIB = (WM_CAP_START + 25);

const
  WM_CAP_EDIT_COPY = (WM_CAP_START + 30);

const
  WM_CAP_SET_AUDIOFORMAT = (WM_CAP_START + 35);

const
  WM_CAP_GET_AUDIOFORMAT = (WM_CAP_START + 36);

const
  WM_CAP_DLG_VIDEOFORMAT = (WM_CAP_START + 41);

const
  WM_CAP_DLG_VIDEOSOURCE = (WM_CAP_START + 42);

const
  WM_CAP_DLG_VIDEODISPLAY = (WM_CAP_START + 43);

const
  WM_CAP_GET_VIDEOFORMAT = (WM_CAP_START + 44);

const
  WM_CAP_SET_VIDEOFORMAT = (WM_CAP_START + 45);

const
  WM_CAP_DLG_VIDEOCOMPRESSION = (WM_CAP_START + 46);

const
  WM_CAP_SET_PREVIEW = (WM_CAP_START + 50);

const
  WM_CAP_SET_OVERLAY = (WM_CAP_START + 51);

const
  WM_CAP_SET_PREVIEWRATE = (WM_CAP_START + 52);

const
  WM_CAP_SET_SCALE = (WM_CAP_START + 53);

const
  WM_CAP_GET_STATUS = (WM_CAP_START + 54);

const
  WM_CAP_SET_SCROLL = (WM_CAP_START + 55);

const
  WM_CAP_GRAB_FRAME = (WM_CAP_START + 60);

const
  WM_CAP_GRAB_FRAME_NOSTOP = (WM_CAP_START + 61);

const
  WM_CAP_SEQUENCE = (WM_CAP_START + 62);

const
  WM_CAP_SEQUENCE_NOFILE = (WM_CAP_START + 63);

const
  WM_CAP_SET_SEQUENCE_SETUP = (WM_CAP_START + 64);

const
  WM_CAP_GET_SEQUENCE_SETUP = (WM_CAP_START + 65);

const
  WM_CAP_SET_MCI_DEVICE = (WM_CAP_START + 66);

const
  WM_CAP_GET_MCI_DEVICE = (WM_CAP_START + 67);

const
  WM_CAP_STOP = (WM_CAP_START + 68);

const
  WM_CAP_ABORT = (WM_CAP_START + 69);

const
  WM_CAP_SINGLE_FRAME_OPEN = (WM_CAP_START + 70);

const
  WM_CAP_SINGLE_FRAME_CLOSE = (WM_CAP_START + 71);

const
  WM_CAP_SINGLE_FRAME = (WM_CAP_START + 72);

const
  WM_CAP_PAL_OPEN = (WM_CAP_START + 80);

const
  WM_CAP_PAL_SAVE = (WM_CAP_START + 81);

const
  WM_CAP_PAL_PASTE = (WM_CAP_START + 82);

const
  WM_CAP_PAL_AUTOCREATE = (WM_CAP_START + 83);

const
  WM_CAP_PAL_MANUALCREATE = (WM_CAP_START + 84);

  // Following added post VFW 1.1
const
  WM_CAP_SET_CALLBACK_CAPCONTROL = (WM_CAP_START + 85);

  // Defines end of the message range
const
  WM_CAP_END = WM_CAP_SET_CALLBACK_CAPCONTROL;

  // ------------------------------------------------------------------
  // structures
  // ------------------------------------------------------------------

type
  tagCapDriverCaps = packed record
    wDeviceIndex: word; // Driver index in system.ini
    fHasOverlay: wordbool; // Can device overlay?
    fHasDlgVideoSource: wordbool; // Has Video source dlg?
    fHasDlgVideoFormat: wordbool; // Has Format dlg?
    fHasDlgVideoDisplay: wordbool; // Has External out dlg?
    fCaptureInitialized: wordbool; // Driver ready to capture?
    fDriverSuppliesPalettes: wordbool; // Can driver make palettes?
    hVideoIn: HVIDEO; // Driver In channel
    hVideoOut: HVIDEO; // Driver Out channel
    hVideoExtIn: HVIDEO; // Driver Ext In channel
    hVideoExtOut: HVIDEO; // Driver Ext Out channel
  end;

  CAPDRIVERCAPS = tagCapDriverCaps;
  PCAPDRIVERCAPS = ^tagCapDriverCaps;
  LPCAPDRIVERCAPS = ^tagCapDriverCaps;

type
  tagCapStatus = record
    uiImageWidth: UINT; // Width of the image
    uiImageHeight: UINT; // Height of the image
    fLiveWindow: longbool; // Now Previewing video?
    fOverlayWindow: longbool; // Now Overlaying video?
    fScale: longbool; // Scale image to client?
    ptScroll: POINT; // Scroll position
    fUsingDefaultPalette: longbool; // Using default driver palette?
    fAudioHardware: longbool; // Audio hardware present?
    fCapFileExists: longbool; // Does capture file exist?
    dwCurrentVideoFrame: DWORD; // # of video frames cap'td
    dwCurrentVideoFramesDropped: DWORD; // # of video frames dropped
    dwCurrentWaveSamples: DWORD; // # of wave samples cap'td
    dwCurrentTimeElapsedMS: DWORD; // Elapsed capture duration
    hPalCurrent: HPALETTE; // Current palette in use
    fCapturingNow: longbool; // Capture in progress?
    dwReturn: DWORD; // Error value after any operation
    wNumVideoAllocated: word; // Actual number of video buffers
    wNumAudioAllocated: word; // Actual number of audio buffers
  end;

  CAPSTATUS = tagCapStatus;
  PCAPSTATUS = ^tagCapStatus;
  LPCAPSTATUS = ^tagCapStatus;

  // Default values in parenthesis
type
  tagCaptureParms = record
    dwRequestMicroSecPerFrame: DWORD; // Requested capture rate
    fMakeUserHitOKToCapture: longbool; // Show "Hit OK to cap" dlg?
    wPercentDropForError: word; // Give error msg if > (10%)
    fYield: longbool; // Capture via background task?
    dwIndexSize: DWORD; // Max index size in frames (32K)
    wChunkGranularity: word; // Junk chunk granularity (2K)
    fUsingDOSMemory: longbool; // Use DOS buffers?
    wNumVideoRequested: word; // # video buffers, If 0, autocalc
    fCaptureAudio: longbool; // Capture audio?
    wNumAudioRequested: word; // # audio buffers, If 0, autocalc
    vKeyAbort: word; // Virtual key causing abort
    fAbortLeftMouse: longbool; // Abort on left mouse?
    fAbortRightMouse: longbool; // Abort on right mouse?
    fLimitEnabled: longbool; // Use wTimeLimit?
    wTimeLimit: word; // Seconds to capture
    fMCIControl: longbool; // Use MCI video source?
    fStepMCIDevice: longbool; // Step MCI device?
    dwMCIStartTime: DWORD; // Time to start in MS
    dwMCIStopTime: DWORD; // Time to stop in MS
    fStepCaptureAt2x: longbool; // Perform spatial averaging 2x
    wStepCaptureAverageFrames: word; // Temporal average n Frames
    dwAudioBufferSize: DWORD; // Size of audio bufs (0 : default)
    fDisableWriteCache: longbool; // Attempt to disable write cache
  end;

  CAPTUREPARMS = tagCaptureParms;
  PCAPTUREPARMS = ^tagCaptureParms;
  LPCAPTUREPARMS = ^tagCaptureParms;

type
  tagCapInfoChunk = record
    fccInfoID: FOURCC; // Chunk ID, "ICOP" for copyright
    lpData: LPVOID; // poSmallInter to data
    cbData: integer; // size of lpData
  end;

  CAPINFOCHUNK = tagCapInfoChunk;
  PCAPINFOCHUNK = ^tagCapInfoChunk;
  LPCAPINFOCHUNK = ^tagCapInfoChunk;

  // Type TVideoHDR is not present in avicap32.. we might need it so we add it
type
  TVideoHDR = record
    lpData: Pointer { LPBYTE }; // * pointer to locked data buffer */
    dwBufferLength: DWORD; // * Length of data buffer */
    dwBytesUsed: DWORD; // * Bytes actually used */
    dwTimeCaptured: DWORD; // * Milliseconds from startof stream */
    dwUser: DWORD; // * for client's use */
    dwFlags: DWORD; // assorted flags(see defines)*/
    dwReserved { [4] } : DWORD; // * reserved for driver */
  end;

  PVideoHDR = ^TVideoHDR;
  LPVideoHDR = PVideoHDR;

  // * dwFlags field of VIDEOHDR */
const
  VHDR_DONE = $00000001; // * Done bit */

const
  VHDR_PREPARED = $00000002; // * Set if this header has been prepared */

const
  VHDR_INQUEUE = $00000004; // * Reserved for driver */

const
  VHDR_KEYFRAME = $00000008; // * Key Frame */


  // ------------------------------------------------------------------
  // Callback Definitions
  // ------------------------------------------------------------------
  // Nog zo'n mooi stukkie dat ik ff laat zitten.
  // componentje van maken, zelf de callback doen.

type
  CAPYIELDCALLBACK = function(hWnd: hWnd): LRESULT; stdcall;

type
  CAPSTATUSCALLBACK = function(hWnd: hWnd; nID: smallint; lpsz: LPCSTR)
    : LRESULT; stdcall;

type
  CAPERRORCALLBACK = function(hWnd: hWnd; nID: smallint; lpsz: LPCSTR)
    : LRESULT; stdcall;

type
  CAPVIDEOCALLBACK = function(hWnd: hWnd; lpVHdr: integer { LPVIDEOHDR } )
    : LRESULT; stdcall;

type
  CAPWAVECALLBACK = function(hWnd: hWnd; lpWHdr: integer { LPWAVEHDR } )
    : LRESULT; stdcall;

type
  CAPCONTROLCALLBACK = function(hWnd: hWnd; nState: smallint): LRESULT; stdcall;

  // ------------------------------------------------------------------
  // CapControlCallback states
  // ------------------------------------------------------------------
const
  CONTROLCALLBACK_PREROLL = 1; { Waiting to start capture }

const
  CONTROLCALLBACK_CAPTURING = 2; { Now capturing }

  // ------------------------------------------------------------------
  // The only exported functions from AVICAP.DLL
  // ------------------------------------------------------------------

function { VFWAPI } capCreateCaptureWindow(lpszWindowName: LPCSTR;
  dwStyle: DWORD; x, y, nWidth, nHeight: integer; hwndParent: hWnd;
  nID: integer): hWnd; stdcall;
  external 'AVICAP32.DLL' Name 'capCreateCaptureWindowA';

function { VFWAPI } capGetDriverDescription(wDriverIndex: word; lpszName: LPSTR;
  cbName: integer; lpszVer: LPSTR; cbVer: integer): longbool; stdcall;
  external 'AVICAP32.DLL' Name 'capGetDriverDescriptionA';

// ------------------------------------------------------------------
// New Information chunk IDs
// ------------------------------------------------------------------
const
  infotypeDIGITIZATION_TIME = ('IDIT');

const
  infotypeSMPTE_TIME = ('ISMP');

  // ------------------------------------------------------------------
  // String IDs from status and error callbacks
  // ------------------------------------------------------------------

const
  IDS_CAP_BEGIN = 300; { "Capture Start" }

const
  IDS_CAP_END = 301; { "Capture End" }

const
  IDS_CAP_INFO = 401; { "%s" }

const
  IDS_CAP_OUTOFMEM = 402; { "Out of memory" }

const
  IDS_CAP_FILEEXISTS = 403; { "File '%s' exists -- overwrite it?" }

const
  IDS_CAP_ERRORPALOPEN = 404; { "Error opening palette '%s'" }

const
  IDS_CAP_ERRORPALSAVE = 405; { "Error saving palette '%s'" }

const
  IDS_CAP_ERRORDIBSAVE = 406; { "Error saving frame '%s'" }

const
  IDS_CAP_DEFAVIEXT = 407; { "avi" }

const
  IDS_CAP_DEFPALEXT = 408; { "pal" }

const
  IDS_CAP_CANTOPEN = 409; { "Cannot open '%s'" }

const
  IDS_CAP_SEQ_MSGSTART = 410;
  { "Select OK to start capture\nof video sequence\nto %s." }

const
  IDS_CAP_SEQ_MSGSTOP = 411; { "Hit ESCAPE or click to end capture" }

const
  IDS_CAP_VIDEDITERR = 412; { "An error occurred while trying to run VidEdit." }

const
  IDS_CAP_READONLYFILE = 413; { "The file '%s' is a read-only file." }

const
  IDS_CAP_WRITEERROR = 414;
  { "Unable to write to file '%s'.\nDisk may be full." }

const
  IDS_CAP_NODISKSPACE = 415;
  { "There is no space to create a capture file on the specified device." }

const
  IDS_CAP_SETFILESIZE = 416; { "Set File Size" }

const
  IDS_CAP_SAVEASPERCENT = 417; { "SaveAs: %2ld%%  Hit Escape to abort." }

const
  IDS_CAP_DRIVER_ERROR = 418; { Driver specific error message }

const
  IDS_CAP_WAVE_OPEN_ERROR = 419;
  { "Error: Cannot open the wave input device.\nCheck sample size, frequency, and channels." }

const
  IDS_CAP_WAVE_ALLOC_ERROR = 420; { "Error: Out of memory for wave buffers." }

const
  IDS_CAP_WAVE_PREPARE_ERROR = 421; { "Error: Cannot prepare wave buffers." }

const
  IDS_CAP_WAVE_ADD_ERROR = 422; { "Error: Cannot add wave buffers." }

const
  IDS_CAP_WAVE_SIZE_ERROR = 423; { "Error: Bad wave size." }

const
  IDS_CAP_VIDEO_OPEN_ERROR = 424;
  { "Error: Cannot open the video input device." }

const
  IDS_CAP_VIDEO_ALLOC_ERROR = 425; { "Error: Out of memory for video buffers." }

const
  IDS_CAP_VIDEO_PREPARE_ERROR = 426; { "Error: Cannot prepare video buffers." }

const
  IDS_CAP_VIDEO_ADD_ERROR = 427; { "Error: Cannot add video buffers." }

const
  IDS_CAP_VIDEO_SIZE_ERROR = 428; { "Error: Bad video size." }

const
  IDS_CAP_FILE_OPEN_ERROR = 429; { "Error: Cannot open capture file." }

const
  IDS_CAP_FILE_WRITE_ERROR = 430;
  { "Error: Cannot write to capture file.  Disk may be full." }

const
  IDS_CAP_RECORDING_ERROR = 431;
  { "Error: Cannot write to capture file.  Data rate too high or disk full." }

const
  IDS_CAP_RECORDING_ERROR2 = 432; { "Error while recording" }

const
  IDS_CAP_AVI_INIT_ERROR = 433; { "Error: Unable to initialize for capture." }

const
  IDS_CAP_NO_FRAME_CAP_ERROR = 434;
  { "Warning: No frames captured.\nConfirm that vertical sync SmallInterrupts\nare configured and enabled." }

const
  IDS_CAP_NO_PALETTE_WARN = 435; { "Warning: Using default palette." }

const
  IDS_CAP_MCI_CONTROL_ERROR = 436; { "Error: Unable to access MCI device." }

const
  IDS_CAP_MCI_CANT_STEP_ERROR = 437; { "Error: Unable to step MCI device." }

const
  IDS_CAP_NO_AUDIO_CAP_ERROR = 438;
  { "Error: No audio data captured.\nCheck audio card settings." }

const
  IDS_CAP_AVI_DRAWDIB_ERROR = 439; { "Error: Unable to draw this data format." }

const
  IDS_CAP_COMPRESSOR_ERROR = 440; { "Error: Unable to initialize compressor." }

const
  IDS_CAP_AUDIO_DROP_ERROR = 441;
  { "Error: Audio data was lost during capture, reduce capture rate." }

  { status string IDs }
const
  IDS_CAP_STAT_LIVE_MODE = 500; { "Live window" }

const
  IDS_CAP_STAT_OVERLAY_MODE = 501; { "Overlay window" }

const
  IDS_CAP_STAT_CAP_INIT = 502; { "Setting up for capture - Please wait" }

const
  IDS_CAP_STAT_CAP_FINI = 503; { "Finished capture, now writing frame %ld" }

const
  IDS_CAP_STAT_PALETTE_BUILD = 504; { "Building palette map" }

const
  IDS_CAP_STAT_OPTPAL_BUILD = 505; { "Computing optimal palette" }

const
  IDS_CAP_STAT_I_FRAMES = 506; { "%d frames" }

const
  IDS_CAP_STAT_L_FRAMES = 507; { "%ld frames" }

const
  IDS_CAP_STAT_CAP_L_FRAMES = 508; { "Captured %ld frames" }

const
  IDS_CAP_STAT_CAP_AUDIO = 509; { "Capturing audio" }

const
  IDS_CAP_STAT_VIDEOCURRENT = 510;
  { "Captured %ld frames (%ld dropped) %d.%03d sec." }

const
  IDS_CAP_STAT_VIDEOAUDIO = 511;
  { "Captured %d.%03d sec.  %ld frames (%ld dropped) (%d.%03d fps).  %ld audio bytes (%d,%03d sps)" }

const
  IDS_CAP_STAT_VIDEOONLY = 512;
  { "Captured %d.%03d sec.  %ld frames (%ld dropped) (%d.%03d fps)" }

const
  IDS_CAP_STAT_FRAMESDROPPED = 513;

  { "Dropped %ld of %ld frames (%d.%02d%%) during capture." }

function capSetCallbackOnStatus(Handle: hWnd; fpProc: Pointer): integer;
function capSetCallbackOnYield(Handle: hWnd; fpProc: Pointer): integer;
function capSetCallbackOnFrame(Handle: hWnd; fpProc: Pointer): integer;
function capSetCallbackOnVideoStream(Handle: hWnd; fpProc: Pointer): integer;
function capSetCallbackOnWaveStream(Handle: hWnd; fpProc: Pointer): integer;
function capSetCallbackOnCapControl(Handle: hWnd; fpProc: Pointer): integer;
function capSetUserData(Handle: hWnd; lUser: integer): integer;
function capGetUserData(Handle: hWnd): integer;
function capDriverConnect(Handle: hWnd; i: integer): integer;
function capDriverDisconnect(Handle: hWnd): integer;
function capDriverGetName(Handle: hWnd; szName: PChar; wSize: integer): integer;
function capDriverGetVersion(Handle: hWnd; szVer: integer;
  wSize: integer): integer;
function capDriverGetCaps(Handle: hWnd; s: LPCAPDRIVERCAPS;
  wSize: integer): integer;
function capFileSetCaptureFile(Handle: hWnd; szName: PChar): integer;
function capFileGetCaptureFile(Handle: hWnd; szName: PChar;
  wSize: integer): integer;
function capFileAlloc(Handle: hWnd; dwSize: integer): integer;
function capFileSaveAs(Handle: hWnd; szName: PChar): integer;
function capFileSetInfoChunk(Handle: hWnd; lpInfoChunk: LPCAPINFOCHUNK)
  : integer;
function capFileSaveDIB(Handle: hWnd; szName: PChar): integer;
function capEditCopy(Handle: hWnd): integer;
function capSetAudioFormat(Handle: hWnd; s: PWaveFormatEx;
  wSize: integer): integer;
function capGetAudioFormat(Handle: hWnd; s: PWaveFormatEx;
  wSize: integer): DWORD;
function capGetAudioFormatSize(Handle: hWnd): DWORD;
function capDlgVideoFormat(Handle: hWnd): integer;
function capDlgVideoSource(Handle: hWnd): integer;
function capDlgVideoDisplay(Handle: hWnd): integer;
function capDlgVideoCompression(Handle: hWnd): integer;
function capGetVideoFormat(Handle: hWnd; s: integer; wSize: integer): DWORD;
function capGetVideoFormatSize(Handle: hWnd): DWORD;
function capSetVideoFormat(Handle: hWnd; s: integer; wSize: integer): integer;
function capPreview(Handle: hWnd; f: longbool): integer;
function capPreviewRate(Handle: hWnd; wMS: integer): integer;
function capOverlay(Handle: hWnd; f: longbool): integer;
function capPreviewScale(Handle: hWnd; f: longbool): integer;
function capGetStatus(Handle: hWnd; s: LPCAPSTATUS; wSize: integer): integer;
function capSetScrollPos(Handle: hWnd; lpP: LPPOINT): integer;
function capGrabFrame(Handle: hWnd): integer;
function capGrabFrameNoStop(Handle: hWnd): integer;
function capCaptureSequence(Handle: hWnd): integer;
function capCaptureSequenceNoFile(Handle: hWnd): integer;
function capCaptureStop(Handle: hWnd): integer;
function capCaptureAbort(Handle: hWnd): integer;
function capCaptureSingleFrameOpen(Handle: hWnd): integer;
function capCaptureSingleFrameClose(Handle: hWnd): integer;
function capCaptureSingleFrame(Handle: hWnd): integer;
function capCaptureGetSetup(Handle: hWnd; s: LPCAPTUREPARMS;
  wSize: integer): integer;
function capCaptureSetSetup(Handle: hWnd; s: LPCAPTUREPARMS;
  wSize: integer): integer;
function capSetMCIDeviceName(Handle: hWnd; szName: PChar): integer;
function capGetMCIDeviceName(Handle: hWnd; szName: PChar; wSize: integer)
  : longbool;
function capPaletteOpen(Handle: hWnd; szName: PChar): integer;
function capPaletteSave(Handle: hWnd; szName: PChar): integer;
function capPalettePaste(Handle: hWnd): integer;
function capPaletteAuto(Handle: hWnd; iFrames: integer;
  iColors: integer): integer;
function capPaletteManual(Handle: hWnd; fGrab: integer;
  iColors: integer): integer;

implementation

// ------------------------------------------------------------------
// Message crackers for above
// ------------------------------------------------------------------


// Leuk, maar dat moet dan richting implemenation...

function capSetCallbackOnError(hWnd: integer; fpProc: Pointer): integer;
begin
  Result := sendmessage(hWnd, WM_CAP_SET_CALLBACK_ERROR, 0, integer(fpProc));
end;

function capSetCallbackOnStatus(Handle: hWnd; fpProc: Pointer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SET_CALLBACK_STATUS, 0, integer(fpProc));
end;

function capSetCallbackOnYield(Handle: hWnd; fpProc: Pointer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SET_CALLBACK_YIELD, 0, integer(fpProc));
end;

function capSetCallbackOnFrame(Handle: hWnd; fpProc: Pointer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SET_CALLBACK_FRAME, 0, integer(fpProc));
end;

function capSetCallbackOnVideoStream(Handle: hWnd; fpProc: Pointer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SET_CALLBACK_VIDEOSTREAM, 0,
    integer(fpProc));
end;

function capSetCallbackOnWaveStream(Handle: hWnd; fpProc: Pointer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SET_CALLBACK_WAVESTREAM, 0,
    integer(fpProc));
end;

function capSetCallbackOnCapControl(Handle: hWnd; fpProc: Pointer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SET_CALLBACK_CAPCONTROL, 0,
    integer(fpProc));
end;

function capSetUserData(Handle: hWnd; lUser: integer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SET_USER_DATA, 0, lUser);
end;

function capGetUserData(Handle: hWnd): integer;
begin
  Result := sendmessage(Handle, WM_CAP_GET_USER_DATA, 0, 0);
end;

function capDriverConnect(Handle: hWnd; i: integer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_DRIVER_CONNECT, i, 0);
end;

function capDriverDisconnect(Handle: hWnd): integer;
begin
  Result := sendmessage(Handle, WM_CAP_DRIVER_DISCONNECT, 0, 0);
end;

function capDriverGetName(Handle: hWnd; szName: PChar; wSize: integer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_DRIVER_GET_NAME, (wSize),
    integer(szName));
end;

function capDriverGetVersion(Handle: hWnd; szVer: integer;
  wSize: integer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_DRIVER_GET_VERSION, (wSize), (szVer));
end;

function capDriverGetCaps(Handle: hWnd; s: LPCAPDRIVERCAPS;
  wSize: integer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_DRIVER_GET_CAPS, (wSize), integer(s));
end;

function capFileSetCaptureFile(Handle: hWnd; szName: PChar): integer;
begin
  Result := sendmessage(Handle, WM_CAP_FILE_SET_CAPTURE_FILE, 0,
    integer(szName));
end;

function capFileGetCaptureFile(Handle: hWnd; szName: PChar;
  wSize: integer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_FILE_GET_CAPTURE_FILE, (wSize),
    integer(szName));
end;

function capFileAlloc(Handle: hWnd; dwSize: integer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_FILE_ALLOCATE, 0, (dwSize));
end;

function capFileSaveAs(Handle: hWnd; szName: PChar): integer;
begin
  Result := sendmessage(Handle, WM_CAP_FILE_SAVEAS, 0, integer(szName));
end;

function capFileSetInfoChunk(Handle: hWnd; lpInfoChunk: LPCAPINFOCHUNK)
  : integer;
begin
  Result := sendmessage(Handle, WM_CAP_FILE_SET_INFOCHUNK, 0,
    integer(lpInfoChunk));
end;

function capFileSaveDIB(Handle: hWnd; szName: PChar): integer;
begin
  Result := sendmessage(Handle, WM_CAP_FILE_SAVEDIB, 0, integer(szName));
end;

function capEditCopy(Handle: hWnd): integer;
begin
  Result := sendmessage(Handle, WM_CAP_EDIT_COPY, 0, 0);
end;

function capSetAudioFormat(Handle: hWnd; s: PWaveFormatEx;
  wSize: integer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SET_AUDIOFORMAT, (wSize), integer(s));
end;

function capGetAudioFormat(Handle: hWnd; s: PWaveFormatEx;
  wSize: integer): DWORD;
begin
  Result := sendmessage(Handle, WM_CAP_GET_AUDIOFORMAT, (wSize), integer(s));
end;

function capGetAudioFormatSize(Handle: hWnd): DWORD;
begin
  Result := sendmessage(Handle, WM_CAP_GET_AUDIOFORMAT, 0, 0 { NULL } );
end;

function capDlgVideoFormat(Handle: hWnd): integer;
begin
  Result := sendmessage(Handle, WM_CAP_DLG_VIDEOFORMAT, 0, 0);
end;

function capDlgVideoSource(Handle: hWnd): integer;
begin
  Result := sendmessage(Handle, WM_CAP_DLG_VIDEOSOURCE, 0, 0);
end;

function capDlgVideoDisplay(Handle: hWnd): integer;
begin
  Result := sendmessage(Handle, WM_CAP_DLG_VIDEODISPLAY, 0, 0);
end;

function capDlgVideoCompression(Handle: hWnd): integer;
begin
  Result := sendmessage(Handle, WM_CAP_DLG_VIDEOCOMPRESSION, 0, 0);
end;

function capGetVideoFormat(Handle: hWnd; s: integer; wSize: integer): DWORD;
begin
  Result := sendmessage(Handle, WM_CAP_GET_VIDEOFORMAT, (wSize), (s));
end;

function capGetVideoFormatSize(Handle: hWnd): DWORD;
begin
  Result := DWORD(sendmessage(Handle, WM_CAP_GET_VIDEOFORMAT, 0, 0));
end;

function capSetVideoFormat(Handle: hWnd; s: integer; wSize: integer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SET_VIDEOFORMAT, (wSize), (s));
end;

function capPreview(Handle: hWnd; f: longbool): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SET_PREVIEW, integer(f), 0);
end;

function capPreviewRate(Handle: hWnd; wMS: integer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SET_PREVIEWRATE, (wMS), 0);
end;

function capOverlay(Handle: hWnd; f: longbool): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SET_OVERLAY, integer(f), 0);
end;

function capPreviewScale(Handle: hWnd; f: longbool): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SET_SCALE, integer(f), 0);
end;

function capGetStatus(Handle: hWnd; s: LPCAPSTATUS; wSize: integer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_GET_STATUS, wSize, integer(s));
end;

function capSetScrollPos(Handle: hWnd; lpP: LPPOINT): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SET_SCROLL, 0, integer(lpP));
end;

function capGrabFrame(Handle: hWnd): integer;
begin
  Result := sendmessage(Handle, WM_CAP_GRAB_FRAME, 0, 0);
end;

function capGrabFrameNoStop(Handle: hWnd): integer;
begin
  Result := sendmessage(Handle, WM_CAP_GRAB_FRAME_NOSTOP, 0, 0);
end;

function capCaptureSequence(Handle: hWnd): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SEQUENCE, 0, 0);
end;

function capCaptureSequenceNoFile(Handle: hWnd): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SEQUENCE_NOFILE, 0, 0);
end;

function capCaptureStop(Handle: hWnd): integer;
begin
  Result := sendmessage(Handle, WM_CAP_STOP, 0, 0);
end;

function capCaptureAbort(Handle: hWnd): integer;
begin
  Result := sendmessage(Handle, WM_CAP_ABORT, 0, 0);
end;

function capCaptureSingleFrameOpen(Handle: hWnd): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SINGLE_FRAME_OPEN, 0, 0);
end;

function capCaptureSingleFrameClose(Handle: hWnd): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SINGLE_FRAME_CLOSE, 0, 0);
end;

function capCaptureSingleFrame(Handle: hWnd): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SINGLE_FRAME, 0, 0);
end;

function capCaptureGetSetup(Handle: hWnd; s: LPCAPTUREPARMS;
  wSize: integer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_GET_SEQUENCE_SETUP, (wSize), integer(s));
end;

function capCaptureSetSetup(Handle: hWnd; s: LPCAPTUREPARMS;
  wSize: integer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SET_SEQUENCE_SETUP, (wSize), integer(s));
end;

function capSetMCIDeviceName(Handle: hWnd; szName: PChar): integer;
begin
  Result := sendmessage(Handle, WM_CAP_SET_MCI_DEVICE, 0, integer(szName));
end;

function capGetMCIDeviceName(Handle: hWnd; szName: PChar; wSize: integer)
  : longbool;
begin
  Result := longbool(sendmessage(Handle, WM_CAP_GET_MCI_DEVICE, (wSize),
    integer(szName)));
end;

function capPaletteOpen(Handle: hWnd; szName: PChar): integer;
begin
  Result := sendmessage(Handle, WM_CAP_PAL_OPEN, 0, integer(szName));
end;

function capPaletteSave(Handle: hWnd; szName: PChar): integer;
begin
  Result := sendmessage(Handle, WM_CAP_PAL_SAVE, 0, integer(szName));
end;

function capPalettePaste(Handle: hWnd): integer;
begin
  Result := sendmessage(Handle, WM_CAP_PAL_PASTE, 0, 0);
end;

function capPaletteAuto(Handle: hWnd; iFrames: integer;
  iColors: integer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_PAL_AUTOCREATE, (iFrames), (iColors));
end;

function capPaletteManual(Handle: hWnd; fGrab: integer;
  iColors: integer): integer;
begin
  Result := sendmessage(Handle, WM_CAP_PAL_MANUALCREATE, (fGrab), (iColors));
end;

end.
