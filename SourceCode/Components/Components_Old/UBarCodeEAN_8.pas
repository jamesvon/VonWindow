unit UBarCodeEAN_8;

interface

uses Graphics, Windows, SysUtils, Dialogs;

const
  //EAN 左资料码 A 类编码
  EAN_A: array [0..9] of String =
  ( '0001101', '0011001', '0010011', '0111101', '0100011'
  , '0110001', '0101111', '0111011', '0110111', '0001011'
  );
  //EAN 左资料码 B 类编码
  EAN_B: array [0..9] of String =
  ( '0100111', '0110011', '0011011', '0100001', '0011101'
  , '0111001', '0000101', '0010001', '0001001', '0010111'
  );
  //EAN 右资料码 C 类编码
  EAN_C: array [0..9] of String =
  ( '1110010', '1100110', '1101100', '1000010', '1011100'
  , '1001110', '1010000', '1000100', '1001000', '1110100'
  );
//EAN 检查码
function EANCheck(InChar: String): String;
//EAN-8 转换二进制码
function EAN_8Convert(ConvertStr: String): String;
//输出EAN-8码
procedure CreateEAN_8(InChar: String; CanvasArea: TCanvas; bcArea: TRect; bcStep: Integer; bcColorB: TColor = clBlack; bcColorW: TColor = clWhite);

procedure openport(PrinterName:pchar);stdcall;far; external 'tsclib.dll';
procedure closeport; external 'tsclib.dll';
procedure sendcommand(Command:pchar);stdcall;far;external 'tsclib.dll';
procedure setup(LabelWidth, LabelHeight, Speed, Density, Sensor, Vertical, Offset:pchar);stdcall; far; external 'tsclib.dll';
procedure downloadpcx(Filename,ImageName:pchar);stdcall;far;external 'tsclib.dll';
procedure barcode(X, Y, CodeType, Height, Readable, Rotation, Narrow, Wide, Code :pchar); stdcall; far; external 'tsclib.dll';
procedure printerfont(X, Y, FontName, Rotation, Xmul, Ymul, Content:pchar);stdcall;far; external 'tsclib.dll';
procedure clearbuffer; external 'tsclib.dll';
procedure printlabel(NumberOfSet, NumberOfCopoy:pchar);stdcall; far;external 'tsclib.dll';
procedure formfeed;external 'tsclib.dll';
procedure nobackfeed; external 'tsclib.dll';
procedure windowsfont (X, Y, FontHeight, Rotation, FontStyle, FontUnderline : integer; FaceName, TextContect:pchar);stdcall;far;external 'tsclib.dll';

procedure PrinterCode(Code: string);

implementation

procedure PrinterCode(Code: string);
begin
  openport('TSC TDP-245');
  clearbuffer;
  sendcommand('DIRECTION 1');
  barcode('50', '335', '39', '64', '1', '0', '2', '4', pchar('3140302001-2038'));
  printlabel('1', '1');
  closeport;
end;
//******************************************************************************
//***                           EAN 检查码                                   ***
//***                   C1 = 奇数位之和                                      ***
//***                   C2 = 偶数位之和                                      ***
//***                   CC = (C1 + (C2 * 3))　取个位数                       ***
//***                   C (检查码) = 10 - CC　 (若值为10，则取0)             ***
//******************************************************************************
function EANCheck(InChar: String): String;
var
  i, c1, c2, cc: Integer;
begin
  c1 := 0;
  c2 := 0;
  cc := 0;
  for i := 1 to Length(InChar) do
  begin
    if (i MOD 2) = 1 then
      c1 := c1 + StrToInt(InChar[i])
    else
      c2 := c2 + StrToInt(InChar[i]);
  end;
  cc := (c1 + (c2 * 3)) MOD 10;
  if cc = 0 then
    result := '0'
  else
    result := IntToStr(10 - cc);
end;
//******************************************************************************
//***                          EAN-8 转换二进制码                            ***
//***        导入值   左资料码   值      A          B      右资料码C         ***
//***                            0    0001101    0100111    1110010          ***
//***          1       AAAAAA    1    0011001    0110011    1100110          ***
//***          2       AABABB    2    0010011    0011011    1101100          ***
//***          3       AABBAB    3    0111101    0100001    1000010          ***
//***          4       ABAABB    4    0100011    0011101    1011100          ***
//***          5       ABBAAB    5    0110001    0111001    1001110          ***
//***          6       ABBBAA    6    0101111    0000101    1010000          ***
//***          7       ABABAB    7    0111011    0010001    1000100          ***
//***          8       ABABBA    8    0110111    0001001    1001000          ***
//***          9       ABBABA    9    0001011    0010111    1110100          ***
//******************************************************************************
function EAN_8Convert(ConvertStr: String): String;
var
  i: Integer;
  TempStr, LeftStr, RightStr: String;
begin
  TempStr := '';
  LeftStr := Copy(ConvertStr, 1, 4);
  RightStr := Copy(ConvertStr, 5, 4);
  //############################ 左资料编码 Start  #############################
  for i := 1 to Length(LeftStr) do
  begin
    TempStr := TempStr + EAN_A[StrToInt(LeftStr[i])];
  end;
  //############################  左资料编码  End  #############################
  TempStr := TempStr + '01010';             //中线编码
  //############################ 右资料编码 Start  #############################
  for i := 1 to Length(RightStr) do
  begin
    TempStr := TempStr + EAN_C[StrToInt(RightStr[i])];
  end;
  //############################  右资料编码  End  #############################
  result := TempStr;
end;
//******************************************************************************
//**                           EAB-8 条码生成                                 **
//**                              条码格式          Length(81)                **
//**左空白、起始符、系统码、左数据符、中间线、右数据符、检查码、终止符、右空白**
//** >=7      3       0        28       5        21       7       3      >=7  **
//**         101                      01010                      101          **
//******************************************************************************
procedure CreateEAN_8(InChar: String; CanvasArea: TCanvas; bcArea: TRect; bcStep: Integer; bcColorB: TColor = clBlack; bcColorW: TColor = clWhite);
var
  CheckBar, OutBar, OutsideBar: String;
  OutX, OutY, OutHeight: Word;
  i, j: Integer;
begin
  if Length(InChar) <> 7 then
  begin
    ShowMessage('输入的不是7位数字！');
    Abort;
  end;
  CheckBar := EANCheck(InChar);
  OutBar := InChar + CheckBar;
//  OutBar := InChar;
  OutsideBar := '101' + EAN_8Convert(OutBar) + '101';
  //设置画布
  CanvasArea.Pen.Color := bcColorW;
  CanvasArea.Rectangle(bcArea);
  OutX := ((bcArea.Right - bcArea.Left) div 2) - (bcStep * 41);
  OutY := ((bcArea.Bottom - bcArea.Top) div 2) - (bcStep * 17);
  OutHeight := bcStep * 34;
  //设置字体
  CanvasArea.Font.Name  := 'OCR-B 10 BT';
  CanvasArea.Font.Style := [fsBold];
  CanvasArea.Font.Size  := (bcStep - 1) * 6;
  //输出字符
  for i := 1 to Length(OutBar) do
  begin
    if i < 5 then
      CanvasArea.TextOut(OutX + (bcStep * 10) + (bcStep * 7 * (i - 1)) + bcStep, OutY + OutHeight - Round(bcStep * 8.5), OutBar[i])
    else
      CanvasArea.TextOut(OutX + (bcStep * 15) + (bcStep * 7 * (i - 1)) + bcStep, OutY + OutHeight - Round(bcStep * 8.5), OutBar[i]);
  end;
  //输出条码
  for i := 1 to Length(OutsideBar) do
  begin
    if OutsideBar[i] = '1' then
      CanvasArea.Pen.Color := bcColorB
    else
      CanvasArea.Pen.Color := bcColorW;
    for j := 1 to bcStep do
    begin
      CanvasArea.MoveTo(OutX + (bcStep * 7) + (bcStep * (i - 1)) + (j - 1), OutY + (bcStep * 5));
      if i in [1..3, 32..36, 65..67] then
        CanvasArea.LineTo(OutX + (bcStep * 7) + (bcStep * (i - 1)) + (j - 1), OutY + OutHeight - (bcStep * 5))
      else
        CanvasArea.LineTo(OutX + (bcStep * 7) + (bcStep * (i - 1)) + (j - 1), OutY + OutHeight - (bcStep * 9));
    end;
  end;
end;

end.
