object FDlgImage: TFDlgImage
  Left = 0
  Top = 0
  Caption = 'FDlgImage'
  ClientHeight = 319
  ClientWidth = 492
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    492
    319)
  PixelsPerInch = 96
  TextHeight = 13
  object btnOK: TBitBtn
    Left = 328
    Top = 295
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #30830#23450
    Kind = bkOK
    NumGlyphs = 2
    TabOrder = 0
  end
  object btnCancel: TBitBtn
    Left = 417
    Top = 295
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = #25918#24323
    Kind = bkCancel
    NumGlyphs = 2
    TabOrder = 2
  end
  object ComboBox1: TComboBox
    Left = 0
    Top = 0
    Width = 492
    Height = 21
    Align = alTop
    TabOrder = 1
    Text = 'ComboBox1'
    OnChange = ComboBox1Change
    Items.Strings = (
      '1'
      '2'
      '3'
      '4'
      '5'
      '6')
  end
end
