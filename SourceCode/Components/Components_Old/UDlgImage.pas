unit UDlgImage;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrameImageInput, StdCtrls, Buttons;

type
  TFDlgImage = class(TForm)
    FrameImageInput1: TFrameImageInput;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    ComboBox1: TComboBox;
    procedure ComboBox1Change(Sender: TObject);
  private
    { Private declarations }
    function GetBmp: TBitmap;
    procedure SetBmp(const Value: TBitmap);
  public
    { Public declarations }
    procedure SetBmpSize(AWidth, AHeight: Integer; FullScreen: Boolean = true);
  published
    property Bitmap: TBitmap read GetBmp write SetBmp;
  end;

var
  FDlgImage: TFDlgImage;

implementation

{$R *.dfm}

{ TFDlgImage }

procedure TFDlgImage.ComboBox1Change(Sender: TObject);
begin
  case ComboBox1.ItemIndex of
  0: SetBmpSize(200, 100, True);
  1: SetBmpSize(200, 100, False);
  2: SetBmpSize(200, 100, True);
  3: SetBmpSize(200, 100, False);
  4: SetBmpSize(200, 100, True);
  5: SetBmpSize(200, 100, False);
  end;
end;

function TFDlgImage.GetBmp: TBitmap;
begin
  Result:= FrameImageInput1.Bitmap;
end;

procedure TFDlgImage.SetBmp(const Value: TBitmap);
begin
  FrameImageInput1.Bitmap:= Value;
end;

procedure TFDlgImage.SetBmpSize(AWidth, AHeight: Integer; FullScreen: Boolean);
var
  bmp: TBitmap;
  WRate, HRate: Extended;
  DestRect: TRect;
begin
  bmp:= TBitmap.Create;
  bmp.Width:= AWidth;
  bmp.Height:= AHeight;
  Bmp.Canvas.FillRect(Rect(0, 0, AWidth, AHeight));
  WRate:= FrameImageInput1.Bitmap.Width/AWidth;
  HRate:= FrameImageInput1.Bitmap.Height/AHeight;
  if FullScreen then begin
    with FrameImageInput1.Bitmap do
      if WRate > HRate then
        DestRect:= Rect(0, Round(AHeight - Height/WRate) div 2, AWidth,
          AHeight - Round(AHeight - Height/WRate) div 2)
      else
        DestRect:= Rect(Round(AWidth - Width/HRate) div 2, 0,
          AWidth - Round(AWidth - Width/HRate) div 2, AHeight)
  end else begin
    with FrameImageInput1.Bitmap do
      if WRate > HRate then
        DestRect:= Rect(Round(AWidth/HRate - AWidth) div 2, 0,
          AWidth - Round(AWidth/HRate - AWidth) div 2, AHeight)
      else
        DestRect:= Rect(0, Round(AHeight/WRate - AHeight) div 2, AWidth,
          AHeight - Round(AHeight/WRate - AHeight) div 2)
  end;
//  if FullScreen then begin
//    with FrameImageInput1.Bitmap do
//      SrcRect:= Rect(0, 0, Width, Height);
//    if WRate > HRate then
//      DestRect:= Rect(0, Round(AHeight - AHeight/WRate) div 2, AWidth,
//        AHeight - Round(AHeight - AHeight/WRate) div 2)
//    else
//      DestRect:= Rect(Round(AWidth - AWidth/HRate) div 2, 0,
//        AWidth - Round(AWidth - AWidth/HRate) div 2, AHeight)
//  end else begin
//    DestRect:= Rect(0, 0, AWidth, AHeight);
//    with FrameImageInput1.Bitmap do
//      if WRate > HRate then
//        SrcRect:= Rect(Round(Width - Width/HRate) div 2, 0,
//          Width - Round(Width - Width/HRate) div 2, Height)
//      else
//       SrcRect:= Rect(0, Round(Height - Height/WRate) div 2, Width,
//        Height - Round(Height - Height/WRate) div 2)
//  end;
  Bmp.Canvas.StretchDraw(DestRect, FrameImageInput1.Bitmap);
  FrameImageInput1.Bitmap.Assign(Bmp);
  FrameImageInput1.DrawImage;
  Bmp.Free;
end;

end.
