object FDlgImageInput: TFDlgImageInput
  Left = 0
  Top = 0
  Caption = 'FDlgImageInput'
  ClientHeight = 380
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnDestroy = FormDestroy
  OnMouseWheel = FormMouseWheel
  PixelsPerInch = 96
  TextHeight = 13
  object ScrollBox1: TScrollBox
    Left = 0
    Top = 0
    Width = 750
    Height = 380
    Align = alClient
    TabOrder = 0
    object Image1: TImage
      Left = 0
      Top = 0
      Width = 105
      Height = 105
      AutoSize = True
    end
    object plVideo: TPanel
      Left = 30
      Top = 27
      Width = 142
      Height = 150
      BevelOuter = bvNone
      Caption = 'plVideo'
      ShowCaption = False
      TabOrder = 0
      Visible = False
    end
  end
  object MainMenu1: TMainMenu
    Left = 424
    Top = 192
    object menuOk: TMenuItem
      Caption = #30830#23450'(&K)'
      Visible = False
      OnClick = menuOkClick
    end
    object menuCancel: TMenuItem
      Caption = #25918#24323'(&C)'
      Visible = False
      OnClick = menuCancelClick
    end
    object N1: TMenuItem
      Caption = #25991#20214'(&F)'
      object menuOpen: TMenuItem
        Caption = #25991#20214#25171#24320'(&O)'
        OnClick = menuOpenClick
      end
      object menuSave: TMenuItem
        Caption = #20445#23384#25991#20214'(&S)'
        OnClick = menuSaveClick
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object menuExit: TMenuItem
        Caption = #36864#20986'(&X)'
        ShortCut = 16499
        OnClick = menuExitClick
      end
    end
    object N7: TMenuItem
      Caption = #25235#21462'(&C)'
      object menuScreen: TMenuItem
        Caption = #25130#20840#23631'(&S)'
        OnClick = menuScreenClick
      end
      object meneForm: TMenuItem
        Caption = #25130#31383#21475'(&W)'
        OnClick = meneFormClick
      end
      object menuActive: TMenuItem
        Caption = #27963#21160#31383#21475'(&A)'
        OnClick = menuActiveClick
      end
      object menuRegion: TMenuItem
        Caption = #21306#22495#25130#21462'(&R)'
        OnClick = menuRegionClick
      end
      object N8: TMenuItem
        Caption = '-'
      end
      object menuCamera: TMenuItem
        Caption = #25668#20687#25235#21462'(&C)'
        OnClick = menuCameraClick
      end
      object N3: TMenuItem
        Caption = '+'
        OnClick = N3Click
      end
    end
    object N4: TMenuItem
      Caption = #25805#20316'(&P)'
      object menuCopy: TMenuItem
        Caption = #22797#21046
        ShortCut = 16451
        OnClick = menuCopyClick
      end
      object menuPaste: TMenuItem
        Caption = #31896#36148
        ShortCut = 16470
        OnClick = menuPasteClick
      end
      object N20: TMenuItem
        Caption = '-'
      end
      object menuSize: TMenuItem
        Caption = #35009#21098'(&S)'
        OnClick = menuSizeClick
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object menuCW: TMenuItem
        Caption = #39034#26102#38024#26059#36716'(&W)'
        OnClick = menuCWClick
      end
      object menuCCW: TMenuItem
        Caption = #36870#26102#38024#26059#36716'(&C)'
        OnClick = menuCCWClick
      end
      object menuRotation: TMenuItem
        Caption = #26059#36716'180(&R)'
        OnClick = menuRotationClick
      end
      object menuAnyRotation: TMenuItem
        Caption = #20219#24847#35282#24230#26059#36716'(&A)'
        OnClick = menuAnyRotationClick
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object menuHpeizontalFlip: TMenuItem
        Caption = #27700#24179#32763#36716'(&H)'
        OnClick = menuHpeizontalFlipClick
      end
      object menuVerticalFlip: TMenuItem
        Caption = #22402#30452#32763#36716'(&V)'
        OnClick = menuVerticalFlipClick
      end
      object menuHMirrow: TMenuItem
        Caption = #27700#24179#26144#20687'(&M)'
        OnClick = menuHMirrowClick
      end
      object menuVMirrow: TMenuItem
        Caption = #22402#30452#26144#20687'(&D)'
        OnClick = menuVMirrowClick
      end
    end
    object N10: TMenuItem
      Caption = #32472#22270'(&D)'
      Visible = False
      object N11: TMenuItem
        Caption = #30011#32447
      end
      object N12: TMenuItem
        Caption = #30697#24418
      end
      object N13: TMenuItem
        Caption = #22253
      end
      object N17: TMenuItem
        Caption = #25991#23383
      end
      object N18: TMenuItem
        Caption = #27833#28422#26742
      end
      object N19: TMenuItem
        Caption = #21943#22696
      end
    end
    object S3: TMenuItem
      Caption = #36873#25321'(&S)'
      Visible = False
      object N14: TMenuItem
        Caption = #30697#24418#36873#25321
      end
      object N15: TMenuItem
        Caption = #22253#36873#25321
      end
      object N16: TMenuItem
        Caption = #33258#23450#20041#36873#25321
      end
    end
    object R1: TMenuItem
      Caption = #28388#38236'(&R)'
      object menuGray: TMenuItem
        Caption = #28784#24230'(&G)'
        OnClick = menuGrayClick
      end
      object menuBit: TMenuItem
        Caption = #20108#20540#21270'(&R)'
        OnClick = menuBitClick
      end
      object menuBright: TMenuItem
        Caption = #20142#24230#35843#33410'(&B)'
        OnClick = menuBrightClick
      end
      object menuContrast: TMenuItem
        Caption = #23545#27604#24230'(&C)'
        OnClick = menuContrastClick
      end
      object menuSaturation: TMenuItem
        Caption = #33394#39281#21644#24230'(&S)'
        OnClick = menuSaturationClick
      end
      object menuInverse: TMenuItem
        Caption = #21453#33394'(&I)'
        OnClick = menuInverseClick
      end
    end
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Left = 384
    Top = 16
  end
  object SavePictureDialog1: TSavePictureDialog
    Left = 480
    Top = 16
  end
end
