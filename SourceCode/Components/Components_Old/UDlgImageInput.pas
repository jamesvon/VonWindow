unit UDlgImageInput;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtDlgs, Menus, ExtCtrls, UVonGraphicEx, jpeg, UVonGraphicAPI,
  Buttons, Clipbrd, UVFW, ComCtrls;

type
  TRegionImage = class;

  TFDlgImageInput = class(TForm)
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    menuOpen: TMenuItem;
    menuSave: TMenuItem;
    N4: TMenuItem;
    menuSize: TMenuItem;
    menuScreen: TMenuItem;
    N5: TMenuItem;
    menuCW: TMenuItem;
    menuCCW: TMenuItem;
    menuRotation: TMenuItem;
    menuHpeizontalFlip: TMenuItem;
    menuVerticalFlip: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    S3: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    R1: TMenuItem;
    OpenPictureDialog1: TOpenPictureDialog;
    N2: TMenuItem;
    menuCancel: TMenuItem;
    menuOk: TMenuItem;
    ScrollBox1: TScrollBox;
    Image1: TImage;
    SavePictureDialog1: TSavePictureDialog;
    menuExit: TMenuItem;
    meneForm: TMenuItem;
    menuActive: TMenuItem;
    menuRegion: TMenuItem;
    N20: TMenuItem;
    menuAnyRotation: TMenuItem;
    N6: TMenuItem;
    menuHMirrow: TMenuItem;
    menuVMirrow: TMenuItem;
    N7: TMenuItem;
    menuCopy: TMenuItem;
    menuPaste: TMenuItem;
    N8: TMenuItem;
    menuCamera: TMenuItem;
    menuGray: TMenuItem;
    menuBit: TMenuItem;
    menuBright: TMenuItem;
    menuContrast: TMenuItem;
    menuSaturation: TMenuItem;
    menuInverse: TMenuItem;
    N3: TMenuItem;
    plVideo: TPanel;
    procedure FormDestroy(Sender: TObject);
    procedure menuOpenClick(Sender: TObject);
    procedure menuSaveClick(Sender: TObject);
    procedure menuScreenClick(Sender: TObject);
    procedure meneFormClick(Sender: TObject);
    procedure menuActiveClick(Sender: TObject);
    procedure menuRegionClick(Sender: TObject);
    procedure menuSizeClick(Sender: TObject);
    procedure menuCWClick(Sender: TObject);
    procedure menuCCWClick(Sender: TObject);
    procedure menuAnyRotationClick(Sender: TObject);
    procedure menuRotationClick(Sender: TObject);
    procedure menuHpeizontalFlipClick(Sender: TObject);
    procedure menuHMirrowClick(Sender: TObject);
    procedure menuVerticalFlipClick(Sender: TObject);
    procedure menuVMirrowClick(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure menuCancelClick(Sender: TObject);
    procedure menuOkClick(Sender: TObject);
    procedure menuExitClick(Sender: TObject);
    procedure menuCopyClick(Sender: TObject);
    procedure menuPasteClick(Sender: TObject);
    procedure menuCameraClick(Sender: TObject);
    procedure menuGrayClick(Sender: TObject);
    procedure menuBitClick(Sender: TObject);
    procedure menuBrightClick(Sender: TObject);
    procedure menuContrastClick(Sender: TObject);
    procedure menuSaturationClick(Sender: TObject);
    procedure menuInverseClick(Sender: TObject);
    procedure N3Click(Sender: TObject);
  private
    { Private declarations }
    FRegionImage: TRegionImage;
    BmpInfo: TBitmapInfo;
    CaptureHandle: THandle;
    fpProc: TMethod;
    FIsDialog: Boolean;
    FVideoIdx: Integer;
    procedure RotateMethod(CCW: Boolean; aBitmap: TBitmap);
    procedure bmp_rotate(Srcbmp, DestBmp: Tbitmap; angle: extended);
    procedure EventOfCamera(sender: TObject);
    procedure EventOnTrancBright(Sender: TObject);
    procedure EventOnTrancContrast(Sender: TObject);
    procedure EventOnTrancSaturation(Sender: TObject);
    procedure SetIsDialog(const Value: Boolean);
    procedure SetVideoIdx(const Value: Integer);
    function GetDisplayVideo: Boolean;
    procedure SetDisplayVideo(const Value: Boolean);
    function GetPicture: TPicture;
    procedure SetPicture(const Value: TPicture);
  public
    { Public declarations }
  published
    property IsDialog: Boolean read FIsDialog write SetIsDialog;
    property VideoIdx: Integer read FVideoIdx write SetVideoIdx;
    property DisplayVideo: Boolean read GetDisplayVideo write SetDisplayVideo;
    property Picture: TPicture read GetPicture write SetPicture;
  end;

  TEVENTCAPVIDEOCALLBACK = function(hwnd: hwnd; lpVHdr: PVIDEOHDR): DWORD of object;

  TFormToCaptureWindow = class(TCustomForm)
  private
    { Private declarations }
    procedure WMEraseBkGnd(var Msg: TWMEraseBkGnd); message WM_ERASEBKGND;
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormActivate(Sender: TObject);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

  TFormToRegionWindow = class(TCustomForm)
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormPaint(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    fDragging: Boolean;
    OldPos: TPoint;
    fRect: TRect;
    fBmp: TBitmap;
    procedure WMEraseBkGnd(var Msg: TWMEraseBkGnd); message WM_ERASEBKGND;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

  TRegionImage = class(TPanel)
  private
    FDown: Boolean;
    FOldPosition: TPoint;
    ShapeList: Array [1 .. 8] of TShape;
    FRectList: array [1 .. 8] of TRect;
    FPosList: array [1 .. 8] of Integer;
    Fbmp: TBitmap;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure WmNcHitTest(var Msg: TWmNcHitTest); message wm_NcHitTest;
    procedure WmSize(var Msg: TWmSize); message wm_Size;
    procedure WmLButtonDown(var Msg: TWmLButtonDown); message wm_LButtonDown;
    procedure WmMove(var Msg: TWmMove); message Wm_Move;
    procedure Setbmp(const Value: TBitmap);
  public
    procedure Paint; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property bmp: TBitmap read Fbmp write Setbmp;
  end;

  TFormWithTrackBar = class(TCustomForm)
  private
    TrackBar1: TTrackBar;
    Image1: TImage;
    FBmp: TBitmap;
    pl: TPanel;
    btnOK, btnCancel: TBitBtn;
    FOnTrackChange: TNotifyEvent;
    procedure SetBmp(const Value: TBitmap);
    procedure SetOnTrackChange(const Value: TNotifyEvent);
    procedure EventTracked(Sender: TObject);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property Bmp: TBitmap read FBmp write SetBmp;
    property OnTrackChange: TNotifyEvent read FOnTrackChange write SetOnTrackChange;
  end;

var
  FDlgImageInput: TFDlgImageInput;

implementation

uses Math;

function CaptureScreenRect(ARect: TRect): TBitmap;
var
  ScreenDC: HDC;
begin
  Result := TBitmap.Create;
  with Result, ARect do
  begin
    Width := Right - Left;
    Height := Bottom - Top;
    ScreenDC := GetDC(0);
    try
      BitBlt(Canvas.Handle, 0, 0, Width, Height, ScreenDC,
        Left, Top, SRCCOPY);
    finally
      ReleaseDC(0, ScreenDC);
    end;
  end;
end;

{$R *.dfm}

procedure TFDlgImageInput.FormDestroy(Sender: TObject);
begin
  if Assigned(FRegionImage) then FRegionImage.Free;
  if CaptureHandle <> 0 then
  begin
    SendMessage(CaptureHandle, WM_CAP_DRIVER_DISCONNECT, 0, 0);
    CaptureHandle := 0;
  end;
end;

(* Properties *)

function TFDlgImageInput.GetDisplayVideo: Boolean;
begin
  Result:= plVideo.Visible;
end;

function TFDlgImageInput.GetPicture: TPicture;
begin
  if DisplayVideo then EventOfCamera(nil);
  Result:= Image1.Picture;
end;

procedure TFDlgImageInput.SetDisplayVideo(const Value: Boolean);
begin
  if Value then begin
    if plVideo.Visible then Exit;
    plVideo.Visible:= True;
    plVideo.Top:= 0;
    plVideo.Left:= 0;
    plVideo.Width:= 1024;
    plVideo.Height:= 768;
    CaptureHandle := capCreateCaptureWindowA('Capture Window',
      WS_VISIBLE or WS_CHILD, 0, 0, 1024, 768, plVideo.Handle, 0); // 创建一个AVICap捕获窗口
    if CaptureHandle = 0 then
    begin
      ShowMessage('创建窗口失败!');
      DisplayVideo:= False;
      Exit;
    end;
    plVideo.OnDblClick:= EventOfCamera;
    menuCamera.Caption:= '抓取(&C)';
    menuCamera.OnClick:= EventOfCamera;
    SendMessage(CaptureHandle, WM_CAP_DRIVER_CONNECT, FVideoIdx, 0);
    SendMessage(CaptureHandle, WM_CAP_SET_PREVIEW, -1, 0);
    SendMessage(CaptureHandle, WM_CAP_SET_PREVIEWRATE, 100, 0);
    SendMessage(CaptureHandle, WM_CAP_SET_SCALE, -1, 0);
//    SendMessage(CaptureHandle, WM_CAP_SET_CALLBACK_VIDEOSTREAM, 0, 0);
//    SendMessage(CaptureHandle, WM_CAP_SET_CALLBACK_ERROR, 0, 0);
//    SendMessage(CaptureHandle, WM_CAP_SET_CALLBACK_STATUSA, 0, 0);
//    SendMessage(CaptureHandle, WM_CAP_SET_SCALE, 1, 0);
//    SendMessage(CaptureHandle, WM_CAP_SET_PREVIEWRATE, 66, 0);
//    SendMessage(CaptureHandle, WM_CAP_SET_OVERLAY, 1, 0);
//    SendMessage(CaptureHandle, WM_CAP_SET_PREVIEW, 1, 0);
    Image1.Visible:= False;
  end else begin
    if not plVideo.Visible then Exit;
    plVideo.Visible:= False;
    Image1.Visible:= True;
    Image1.OnClick:= nil;
    menuCamera.Caption:= '摄像抓取(&C)';
    menuCamera.OnClick:= menuCameraClick;
    //SendMessage(CaptureHandle, WM_CAP_STOP, 0, 0);
    SendMessage(CaptureHandle, WM_CAP_SET_PREVIEW, 0, 0);
    SendMessage(CaptureHandle, WM_CAP_DRIVER_DISCONNECT, 0, 0);
    SendMessage(CaptureHandle, WM_CLOSE, 0, 0);
    CaptureHandle := 0;
//    Image1.Visible:= True;
//    Image1.Top:= 0;
//    Image1.Left:= 0;
//    Image1.Width:= 1024;
//    Image1.Height:= 768;
//    plVideo.Visible:= False;
  end;
end;

procedure TFDlgImageInput.SetIsDialog(const Value: Boolean);
begin
  FIsDialog := Value;
  menuOK.Visible:= Value;
  menuCancel.Visible:= Value;
  menuExit.Visible:= not Value;
end;

procedure TFDlgImageInput.SetPicture(const Value: TPicture);
begin
  DisplayVideo:= False;
  Image1.Picture.Assign(Value);
end;

procedure TFDlgImageInput.SetVideoIdx(const Value: Integer);
begin
  FVideoIdx := Value;
end;

(* Event of camera or other set *)

procedure TFDlgImageInput.EventOfCamera(sender: TObject);
var
  tmpFilename: String;
begin
  tmpFilename := ExtractFilePath(Application.ExeName) + 'tmp.bmp';
  //   WM_CAP_FILE_SAVEDIBW  WM_CAP_FILE_SAVEDIBA
  if CaptureHandle <> 0 then
  begin
    SendMessage(CaptureHandle, WM_CAP_FILE_SAVEDIBW, 0, LongInt(PAnsiChar(tmpFilename)));
    Image1.Picture.LoadFromFile(tmpFilename);
  end;
  DisplayVideo:= False;
end;

procedure TFDlgImageInput.EventOnTrancBright(Sender: TObject);
var
  p: PByteArray;
  x, y: Integer;
  Bmp: TBitmap;
begin
  Bmp := TBitmap.Create;
  Bmp.Assign(TFormWithTrackBar(Sender).Bmp);
  Bmp.PixelFormat := pf24Bit;                  //24位真彩色
  for y := 0 to Bmp.Height - 1 do
  begin
    p := Bmp.scanline[y];
    for x := 0 to Bmp.Width - 1 do begin       //每个象素点的R、G、B分量进行调节
      p[x * 3] := Max(0, Min(255, p[x * 3] + TFormWithTrackBar(Sender).TrackBar1.Position));    //不能越界，限制在0～255
      p[x * 3 + 1] := Max(0, Min(255, p[x * 3 + 1] + TFormWithTrackBar(Sender).TrackBar1.Position));
      p[x * 3 + 2] := Max(0, Min(255, p[x * 3 + 2] + TFormWithTrackBar(Sender).TrackBar1.Position));
    end;
  end;
  TFormWithTrackBar(Sender).Image1.Picture.Bitmap.Assign(Bmp);
  Bmp.Free;
end;

procedure TFDlgImageInput.EventOnTrancContrast(Sender: TObject);
var
  p: PByteArray;
  x, y: Integer;
  Bmp: TBitmap;
begin
  Bmp := TBitmap.Create;
  Bmp.Assign(TFormWithTrackBar(Sender).Bmp);
  Bmp.PixelFormat := pf24Bit;
  for y := 0 to Bmp.Height - 1 do
  begin
    p := Bmp.scanline[y];
    for x := 0 to Bmp.Width - 1 do
    begin //确定阀值为128
      if (p[x * 3] < 246)and(p[x * 3] > 128)and(p[x * 3 + 1] > 128)
        and(p[x * 3 + 1] < 246)and(p[x * 3 + 2] > 128)and(p[x * 3 + 2] < 246)then
      begin
        p[x * 3] := Max(0, Min(255, p[x * 3] + TFormWithTrackBar(Sender).TrackBar1.Position));
        p[x * 3 + 1] := Max(0, Min(255, p[x * 3 + 1] + TFormWithTrackBar(Sender).TrackBar1.Position));
        p[x * 3 + 2] := Max(0, Min(255, p[x * 3 + 2] + TFormWithTrackBar(Sender).TrackBar1.Position));
      end;
      if (p[x * 3] > 10)and(p[x * 3] < 128)and(p[x * 3 + 1] > 10)
        and(p[x * 3 + 1] < 128)and(p[x * 3 + 2] > 10)and(p[x * 3 + 2] < 128)then
      begin
        p[x * 3] := Max(0, Min(255, p[x * 3] - TFormWithTrackBar(Sender).TrackBar1.Position));
        p[x * 3 + 1] := Max(0, Min(255, p[x * 3 + 1] - TFormWithTrackBar(Sender).TrackBar1.Position));
        p[x * 3 + 2] := Max(0, Min(255, p[x * 3 + 2] - TFormWithTrackBar(Sender).TrackBar1.Position));
      end;
    end;
  end;
  TFormWithTrackBar(Sender).Image1.Picture.Bitmap.Assign(Bmp);
  Bmp.Free;
end;

procedure TFDlgImageInput.EventOnTrancSaturation(Sender: TObject);
const
  CMax = 255;
var
  Grays: array[0..767] of Integer;
  Alpha: array[0..255] of Word;
  Gray, x, y: Integer;
  SrcRGB: PRGBTriple;
  Bmp: TBitmap;
  i: Byte;
begin
  Bmp := TBitmap.Create;
  Bmp.Assign(TFormWithTrackBar(Sender).Bmp);
  Bmp.PixelFormat := pf24Bit;
  for i := 0 to CMax do
  begin
    Alpha[i] := (i * (TFormWithTrackBar(Sender).TrackBar1.Position + 256) * 8) shr 8;
  end;
  x := 0;
  for i := 0 to CMax do
  begin
    Gray := i - Alpha[i];
    Grays[x] := Gray;
    Inc(x);
    Grays[x] := Gray;
    inc(x);
    Grays[x] := Gray;
    Inc(x);
  end;
  for y := 0 to bmp.Height - 1 do
  begin
    SrcRGB := bmp.ScanLine[y];
    for x := 0 to bmp.Width - 1 do
    begin
      Gray := Grays[SrcRGB.rgbtRed + SrcRGB.rgbtBlue + SrcRGB.rgbtGreen];
      if Gray + Alpha[SrcRGB.rgbtRed] > 0 then
        SrcRGB.rgbtRed := Min(CMax, Gray + Alpha[SrcRGB.rgbtRed])
      else
        SrcRGB.rgbtRed := 0;

      if Gray + Alpha[SrcRGB.rgbtGreen] > 0 then
        SrcRGB.rgbtGreen := Min(CMax, Gray + Alpha[SrcRGB.rgbtGreen])
      else
        SrcRGB.rgbtGreen := 0;

      if Gray + Alpha[SrcRGB.rgbtBlue] > 0 then
        SrcRGB.rgbtBlue := Min(CMax, Gray + Alpha[SrcRGB.rgbtBlue])
      else
        SrcRGB.rgbtBlue := 0;

      Inc(SrcRGB);
    end;
  end;
  TFormWithTrackBar(Sender).Image1.Picture.Bitmap.Assign(Bmp);
  Bmp.Free;
end;

procedure TFDlgImageInput.FormMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
  if WheelDelta < 0 then
    ScrollBox1.Perform(WM_VSCROLL,SB_LINEDOWN,0)    //SendMessage(ScrollBox1.Handle,WM_VSCROLL,SB_LINEDOWN,0)
  else
    ScrollBox1.Perform(WM_VSCROLL,SB_LINEUP,0);     //SendMessage(ScrollBox1.Handle,WM_VSCROLL,SB_LINEUP,0)
end;

(* Event of menu *)

procedure TFDlgImageInput.meneFormClick(Sender: TObject);
var
//  ABitmap: TBitmap;
  P1: TPoint;
  R: TRect;
  theHandle: HWnd;
begin
  Application.Minimize;
  DisplayVideo:= False;
   //提供系统刷新时间
  Sleep(200);
   // 建立新的文件
  with TFormToCaptureWindow.Create(Application) do try
    if ShowModal = mrOK then begin
      Sleep(200);
      GetCursorPos(P1);   // 获得鼠标位置
      { 如果函数运行成功，则返回值为窗口的handle,如果没有窗口的存在则返回值为空}
      theHandle := WindowFromPoint(P1);
      GetWindowRect(theHandle, R);
      Image1.Picture.Bitmap.Assign(CaptureScreenRect(R));
      SendMessage(theHandle, WM_Paint, 0, 0);
    end;
  finally
    Free;
  end;
  Application.Restore;
end;

procedure TFDlgImageInput.menuActiveClick(Sender: TObject);
var
  Re: TRect;
  AWnd: HWND;
begin
  AWnd := GetForeGroundWindow;
  GetWindowRect(AWnd, Re);
  Application.Minimize;
  DisplayVideo:= False;
  Sleep(200);
  Image1.Picture.Bitmap.Assign(CaptureScreenRect(Re));
  Application.Restore;
end;

procedure TFDlgImageInput.menuAnyRotationClick(Sender: TObject);
var
  newbmp: TBitmap;
  Bitmap: TBitmap;
  angle: integer;
begin
  if DisplayVideo then EventOfCamera(nil);
  newbmp := TBitmap.Create;
  Bitmap := TBitmap.Create;
  screen.Cursor := crhourglass;
  newbmp.Assign(image1.Picture.Bitmap);
  //newbmp.pixelFormat:=pf8bit;
  //Bitmap.pixelFormat:=pf8bit;
  angle := strtoint(inputbox('旋转位图', '请输入旋转角度', '90'));
  Bmp_Rotate(newbmp, bitmap, angle);
  image1.picture.Bitmap.Assign(bitmap);
  image1.Left := (self.Width div 2) - (bitmap.Width div 2);
  image1.Top := (self.Height div 2) - (bitmap.Height div 2);
  screen.Cursor := crdefault;
  newbmp.Free;
  Bitmap.Free;
end;

procedure TFDlgImageInput.menuBitClick(Sender: TObject);
var
  p: PByteArray;
  Gray, x, y: Integer;
  Bmp: TBitmap;
begin
  if DisplayVideo then EventOfCamera(nil);
  Bmp := TBitmap.Create;
  Bmp.Assign(Image1.Picture.Bitmap);
  //设置为24位真彩色
  Bmp.PixelFormat := pf24Bit;
  randomize;
  for y := 0 to Bmp.Height - 1 do
  begin
    p := Bmp.scanline[y];
    for x := 0 to Bmp.Width - 1 do
    begin
      //一个象素点三个字节
      Gray := Round(p[x * 3 + 2] * 0.3 + p[x * 3 + 1] * 0.59 + p[x * 3] * 0.11);
      if gray > 128 then //全局阀值128
      begin
        p[x * 3] := 255;
        p[x * 3 + 1] := 255;
        p[x * 3 + 2] := 255;
      end
      else
      begin
        p[x * 3] := 0;
        p[x * 3 + 1] := 0;
        p[x * 3 + 2] := 0;
      end;
    end;
  end;
  Image1.Picture.Bitmap.Assign(Bmp);
  Bmp.Free;
end;

procedure TFDlgImageInput.menuBrightClick(Sender: TObject);
begin
  if DisplayVideo then EventOfCamera(nil);
  with TFormWithTrackBar.Create(Application)do try
    Bmp:= Self.Image1.Picture.Bitmap;
    OnTrackChange:= EventOnTrancBright;
    if ShowModal = mrOK then
      Self.Image1.Picture.Bitmap.Assign(Image1.Picture.Bitmap);
  finally
    Free;
  end;
end;

procedure TFDlgImageInput.N3Click(Sender: TObject);
begin
  Inc(FVideoIdx);
end;

procedure TFDlgImageInput.menuCameraClick(Sender: TObject);
begin
  if Assigned(Image1.OnClick) then EventOfCamera(nil)
  else begin
    DisplayVideo:= True;
  end;
end;

procedure TFDlgImageInput.menuCancelClick(Sender: TObject);
begin
  ModalResult:= mrCancel;
end;

procedure TFDlgImageInput.menuCCWClick(Sender: TObject);
begin
  if DisplayVideo then EventOfCamera(nil);
  image1.Picture.Bitmap.PixelFormat := pf24bit;
  RotateMethod(True, Image1.Picture.Bitmap);
end;

procedure TFDlgImageInput.menuContrastClick(Sender: TObject);
begin
  if DisplayVideo then EventOfCamera(nil);
  with TFormWithTrackBar.Create(Application)do try
    Bmp:= Self.Image1.Picture.Bitmap;
    OnTrackChange:= EventOnTrancContrast;
    if ShowModal = mrOK then
      Self.Image1.Picture.Bitmap.Assign(Image1.Picture.Bitmap);
  finally
    Free;
  end;
end;

procedure TFDlgImageInput.menuCopyClick(Sender: TObject);
begin
  if DisplayVideo then EventOfCamera(nil);
  Clipboard.Assign(Image1.Picture); //把 Image1 中的图片放入剪切板
end;

procedure TFDlgImageInput.menuCWClick(Sender: TObject);
begin
  if DisplayVideo then EventOfCamera(nil);
  image1.Picture.Bitmap.PixelFormat := pf24bit;
  RotateMethod(False, Image1.Picture.Bitmap);
end;

procedure TFDlgImageInput.menuExitClick(Sender: TObject);
begin
  Close;
end;

procedure TFDlgImageInput.menuGrayClick(Sender: TObject);
var
    p: pbyteArray;
    x, y: Integer;
    Bmp: TBitmap;
    Gray: integer;
begin
  if DisplayVideo then EventOfCamera(nil);
  Bmp := TBitmap.Create;
  Bmp.Assign(Image1.Picture.Bitmap);
  Bmp.PixelFormat := pf24bit;
  //转为真彩色处理
  for y := 0 to Bmp.Height - 1 do
  begin
    //获取每一行象素信息
    p := Bmp.scanline[y];
    for x := 0 to Bmp.Width - 1 do
    begin
      //这里采用方法三
      //即 Y＝0.299R+0587G+0.114B
      Gray := Round(p[3 * x + 2] * 0.3 + p[3 * x + 1] * 0.59 + p[3 * x] * 0.11);
      //由于是24位真彩色，故一个象素点为三个字节
      p[3 * x + 2] := byte(Gray);
      p[3 * x + 1] := byte(Gray);
      p[3 * x] := byte(Gray);
      //Gray的值必须在0～255之间
    end;
  end;
  Image1.Picture.Bitmap.Assign(Bmp);
  Bmp.Free;
end;

procedure TFDlgImageInput.menuHMirrowClick(Sender: TObject);
var
  bmp1, bmp2, bmp3: TBitmap;
  T, P: pByteArray;
  X, Y: integer;
begin
  bmp1 := TBitmap.Create;
  bmp2 := TBitmap.Create;
  bmp3 := TBitmap.Create;
  image1.picture.Bitmap.PixelFormat := pf24bit;
  bmp2.Assign(Image1.Picture.Bitmap);
  bmp1.Assign(Image1.Picture.Bitmap);
  bmp3.Width := 2 * bmp1.Width;
  bmp3.Height := Image1.Picture.Bitmap.Height;
  for Y := 0 to bmp2.Height - 1 do
  begin
    T := bmp2.ScanLine[Y];
    P := bmp1.ScanLine[Y];
    for X := 0 to bmp2.Width - 1 do
    begin
      P[3 * X + 2] := T[3 * (bmp2.Width - 1 - X) + 2];
      P[3 * X + 1] := T[3 * (bmp2.Width - 1 - X) + 1];
      P[3 * X] := T[3 * (bmp2.Width - 1 - X)];
    end;
  end;
  bmp3.Canvas.Draw(0, 0, bmp2);
  bmp3.Canvas.Draw(bmp2.Width, 0, bmp1);
  Image1.Picture.Bitmap.Assign(bmp3);
  bmp1.Free;
  bmp2.Free;
  bmp3.Free;
end;

procedure TFDlgImageInput.menuHpeizontalFlipClick(Sender: TObject);
var
  bmp1: TBitmap;
  T, P: pByteArray;
  X, Y: integer;
begin
  bmp1 := TBitmap.Create;
  image1.picture.Bitmap.PixelFormat := pf24bit;
  bmp1.Assign(Image1.Picture.Bitmap);
  for Y := 0 to Image1.Picture.Bitmap.Height - 1 do
  begin
    T := Image1.Picture.Bitmap.ScanLine[Y];
    P := bmp1.ScanLine[Y];
    for X := 0 to Image1.Picture.Bitmap.Width - 1 do
    begin
      P[3 * X + 2] := T[3 * (Image1.Picture.Bitmap.Width - 1 - X) + 2];
      P[3 * X + 1] := T[3 * (Image1.Picture.Bitmap.Width - 1 - X) + 1];
      P[3 * X] := T[3 * (Image1.Picture.Bitmap.Width - 1 - X)];
    end;
  end;
  Image1.Picture.Bitmap.Assign(bmp1);
  bmp1.Free;
end;

procedure TFDlgImageInput.menuInverseClick(Sender: TObject);
var
  BMP: TBitmap;
begin
  if DisplayVideo then EventOfCamera(nil);
  Bmp := TBitmap.Create;
  try
    Bmp.Width := Image1.Picture.Bitmap.Width;
    Bmp.Height := Image1.Picture.Bitmap.Height;
    Bitblt(Bmp.Canvas.Handle, 0, 0, Bmp.Width, Bmp.Height, Image1.Picture.Bitmap.Canvas.Handle, 0, 0, NOTSRCCOPY);
    Image1.Picture.Bitmap.Assign(Bmp);
  finally
    Bmp.Free;
  end;
end;

procedure TFDlgImageInput.menuRegionClick(Sender: TObject);
begin
  Application.Minimize;
  DisplayVideo:= False;
  Sleep(500);
  with TFormToRegionWindow.Create(Application) do
  try
    if ShowModal = mrOK then
      with fRect do begin
        if (Right > Left) and (Bottom > Top) then begin // 提供系统刷新时间
          Image1.Picture.Bitmap.Assign(CaptureScreenRect(fRect));
        end
        else
        begin
          Close;
          MessageDlg('选择错误，请正确拖动鼠标再试!', mtInformation, [mbOk], 0);
          menuRegionClick(Self);
          Exit;
        end;                            {If}
      end;                              {fRect}
  finally                               {ShowModal}
    Free;
  end;
  Application.Restore;
end;

procedure TFDlgImageInput.menuRotationClick(Sender: TObject);
var
  bmp1, bmp2: Tbitmap;
  i, j: integer;
  p, p1: pbyteArray;
begin
  if DisplayVideo then EventOfCamera(nil);
  bmp1 := Tbitmap.Create;
  bmp2 := Tbitmap.Create;
  bmp1.Assign(Image1.Picture.Bitmap);
  bmp2.Assign(image1.Picture.Bitmap);
  bmp1.PixelFormat := pf24bit;
  bmp2.PixelFormat := pf24bit;
  for j := 0 to image1.Picture.Graphic.Height - 1 do
  begin
    p := bmp1.ScanLine[j];
    p1 := bmp2.ScanLine[IMAGE1.Picture.Graphic.Height - 1 - j];
    for i := 0 to image1.Picture.Graphic.Width - 1 do
    begin
      p[3 * i] := p1[3 * (image1.Picture.Graphic.Width - 1 - i)];
      p[3 * i + 1] := p1[3 * (image1.Picture.Graphic.Width - 1 - i) + 1];
      p[3 * i + 2] := p1[2 + 3 * (image1.Picture.Graphic.Width - 1 - i)];
    end;
  end;
  Image1.Picture.Bitmap.Assign(bmp1);
  Bmp1.Free;
  bmp2.Free;
end;

procedure TFDlgImageInput.menuOkClick(Sender: TObject);
begin
  ModalResult:= mrOK;
end;

procedure TFDlgImageInput.menuOpenClick(Sender: TObject);
var
  bmp: TBitmap;
begin
  with OpenPictureDialog1 do
    if Execute then begin
      bmp:= TBitmap.Create;
      LoadGraphicFile(Filename, 0, 0, bmp);
      Image1.Picture.Bitmap.Assign(bmp);
      bmp.Free;
      DisplayVideo:= false;
    end;
end;

procedure TFDlgImageInput.menuPasteClick(Sender: TObject);
begin
  DisplayVideo:= False;
  Image1.Picture.Assign(Clipboard);
end;

procedure TFDlgImageInput.menuSaturationClick(Sender: TObject);
begin
  if DisplayVideo then EventOfCamera(nil);
  with TFormWithTrackBar.Create(Application)do try
    Bmp:= Self.Image1.Picture.Bitmap;
    OnTrackChange:= EventOnTrancSaturation;
    if ShowModal = mrOK then
      Self.Image1.Picture.Bitmap.Assign(Image1.Picture.Bitmap);
  finally
    Free;
  end;
end;

procedure TFDlgImageInput.menuScreenClick(Sender: TObject);
begin
  Application.Minimize;
  DisplayVideo:= False;
  //提供系统刷新时间
  Sleep(200);
  Image1.Picture.Bitmap.Assign(CaptureScreenRect(Rect(0, 0, Screen.Width, Screen.Height)));
  Application.Restore;
end;

procedure TFDlgImageInput.menuSizeClick(Sender: TObject);
var
  bmp: TBitmap;
begin
  if DisplayVideo then EventOfCamera(nil);
  if menuSize.Checked then begin
    Image1.Visible:= True;
    bmp:= TBitmap.Create;
    bmp.Width:= FRegionImage.Width;
    bmp.Height:= FRegionImage.Height;
    bmp.Canvas.CopyRect(Rect(0, 0, FRegionImage.Width, FRegionImage.Height),
      FRegionImage.bmp.Canvas, Rect(FRegionImage.Left, FRegionImage.Top,
      FRegionImage.Left + FRegionImage.Width, FRegionImage.Top + FRegionImage.Height));
    Image1.Picture.Bitmap.Assign(bmp);
    FreeAndNil(FRegionImage);
  end else begin
    Image1.Visible:= False;
    FRegionImage:= TRegionImage.Create(self);
    FRegionImage.Parent:= ScrollBox1;
    FRegionImage.Left:= 0;
    FRegionImage.Top:= 0;
    FRegionImage.bmp:= Image1.Picture.Bitmap;
  end;
  menuSize.Checked:= not menuSize.Checked;
end;

procedure TFDlgImageInput.menuVerticalFlipClick(Sender: TObject);
var
  bmp1: Tbitmap;
  i, j: integer;
  p, p1: pbyteArray;
begin
  image1.picture.Bitmap.PixelFormat := pf24bit;
  bmp1 := Tbitmap.Create;
  bmp1.Assign(Image1.Picture.Bitmap);
  bmp1.PixelFormat := pf24bit;
  for j := 0 to Self.image1.Picture.Graphic.Height - 1 do
  begin
    p := bmp1.ScanLine[j];
    p1 := Image1.Picture.Bitmap.ScanLine[Self.IMAGE1.Picture.Graphic.Height - 1 - j];
    for i := 0 to image1.Picture.Graphic.Width - 1 do
    begin
      p[3 * i] := p1[3 * i];
      p[3 * i + 1] := p1[3 * i + 1];
      p[3 * i + 2] := p1[2 + 3 * i];
    end;
  end;
  Image1.Picture.Bitmap.Assign(bmp1);
  bmp1.Free;
end;

procedure TFDlgImageInput.menuVMirrowClick(Sender: TObject);
var
  bmp1, bmp2: Tbitmap;
  i, j: integer;
  p, p1: pbyteArray;
begin
  bmp1 := Tbitmap.Create;
  bmp2 := Tbitmap.Create;
  bmp1.Assign(Self.Image1.Picture.Bitmap);
  bmp2.Assign(Self.image1.Picture.Bitmap);
  bmp1.PixelFormat := pf24bit;
  bmp2.PixelFormat := pf24bit;
  for j := 0 to Self.image1.Picture.Graphic.Height - 1 do
  begin
    p := bmp1.ScanLine[j];
    p1 := bmp2.ScanLine[Self.IMAGE1.Picture.Graphic.Height - 1 - j];
    for i := 0 to Self.image1.Picture.Graphic.Width - 1 do
    begin
      p[3 * i] := p1[3 * i];
      p[3 * i + 1] := p1[3 * i + 1];
      p[3 * i + 2] := p1[2 + 3 * i];
    end;
  end;
  Image1.Picture.Bitmap.Width:= bmp2.Width;
  Image1.Picture.Bitmap.Height:= bmp2.Height * 2;
  Image1.Picture.Bitmap.Canvas.Draw(0, 0, bmp2);
  Image1.Picture.Bitmap.Canvas.Draw(0, bmp1.Height, bmp1);
  bmp1.Free;
  bmp2.Free;
end;

procedure TFDlgImageInput.menuSaveClick(Sender: TObject);
begin
  with SavePictureDialog1 do
    if Execute then
      Image1.Picture.SaveToFile(Filename);
end;

(* 算法 *)

procedure TFDlgImageInput.bmp_rotate(Srcbmp, DestBmp: Tbitmap; angle: extended);
var
  c1x, c1y, c2x, c2y: integer;
  p1x, p1y, p2x, p2y: integer;
  radius, n: integer;
  alpha: extended;
  c0, c1, c2, c3: tcolor;
begin
//  if SrcBmp.Width > SrcBmp.Height then
//  begin
//    DestBmp.width := SrcBmp.Width;
//    DestBmp.height := SrcBmp.Width;
//  end
//  else
//    DestBmp.Width := SrcBmp.Height;
//  DestBmp.Height := SrcBmp.Height;
  //将角度转换为PI值
  angle := (angle / 180) * pi;
  DestBmp.Width:= Round(Cos(arctan2(SrcBmp.Height, SrcBmp.Width) - angle) *
    SQRT(SrcBmp.Height * SrcBmp.Height + SrcBmp.Width * SrcBmp.Width));
  DestBmp.Height:= Round(Sin(arctan2(SrcBmp.Height, SrcBmp.Width) + angle) *
    SQRT(SrcBmp.Height * SrcBmp.Height + SrcBmp.Width * SrcBmp.Width));
  // 计算中心点，你可以修改它
  c1x := SrcBmp.width div 2;
  c1y := SrcBmp.height div 2;
  c2x := DestBmp.width div 2;
  c2y := DestBmp.height div 2;
  // 步骤数值number
  if c2x < c2y then
    n := c2y
  else
    n := c2x;
  dec(n, 1);
  // 开始旋转
  for p2x := 0 to n do
  begin
    for p2y := 0 to n do
    begin
      if p2x = 0 then
        alpha := pi / 2
      else
        alpha := arctan2(p2y, p2x);
      radius := round(sqrt((p2x * p2x) + (p2y * p2y)));
      p1x := round(radius * cos(angle + alpha));
      p1y := round(radius * sin(angle + alpha));

      c0 := SrcBmp.canvas.pixels[c1x + p1x, c1y + p1y];
      c1 := SrcBmp.canvas.pixels[c1x - p1x, c1y - p1y];
      c2 := SrcBmp.canvas.pixels[c1x + p1y, c1y - p1x];
      c3 := SrcBmp.canvas.pixels[c1x - p1y, c1y + p1x];

      DestBmp.Canvas.pixels[c2x + p2x, c2y + p2y] := c0;
      DestBmp.canvas.pixels[c2x - p2x, c2y - p2y] := c1;
      DestBmp.canvas.pixels[c2x + p2y, c2y - p2x] := c2;
      DestBmp.canvas.pixels[c2x - p2y, c2y + p2x] := c3;
    end;
    application.processmessages
  end;
end;

procedure TFDlgImageInput.RotateMethod(CCW: Boolean; aBitmap: TBitmap);
var
  nIdx, nOfs, x, y, i, nMultiplier: integer;
  nMemWidth, nMemHeight, nMemSize, nScanLineSize: LongInt;
  aScnLnBuffer: PByte;
  aScanLine: PByteArray;
begin
  //消耗时间
  nMultiplier := 3;
  nMemWidth := aBitmap.Height;
  nMemHeight := aBitmap.Width;
  //实际需要内存大小
  nMemSize := nMemWidth * nMemHeight * nMultiplier;
  //开辟内存
  GetMem(aScnLnBuffer, nMemSize);
  try
    //Scanline的长度
    nScanLineSize := aBitmap.Width * nMultiplier;
    //为ScanLine分配内存
    GetMem(aScanLine, nScanLineSize);
    try
      for y := 0 to aBitmap.Height - 1 do begin //进行数据块的移动
        if CCW then
          Move(aBitmap.ScanLine[y]^, aScanLine^, nScanLineSize)
        else Move(aBitmap.ScanLine[aBitmap.Height - 1 - y]^, aScanLine^, nScanLineSize);
        for x := 0 to aBitmap.Width - 1 do begin
          nIdx := ((aBitmap.Width - 1) - x) * nMultiplier;
          nOfs := (x * nMemWidth * nMultiplier) + (y * nMultiplier);
          for i := 0 to nMultiplier - 1 do
            Byte(aScnLnBuffer[nOfs + i]) := aScanLine[nIdx + i];
        end;
      end;
      //宽和高交换开始，逆时针旋转
      aBitmap.Height := nMemHeight;
      aBitmap.Width := nMemWidth;
      for y := 0 to nMemHeight - 1 do begin       //数据移动
        nOfs := y * nMemWidth * nMultiplier;
        if CCW then
          Move((@(aScnLnBuffer[nOfs]))^, aBitmap.ScanLine[y]^, nMemWidth * nMultiplier)
        else Move((@(aScnLnBuffer[nOfs]))^, aBitmap.ScanLine[aBitmap.Height - 1 - y]^, nMemWidth * nMultiplier);
      end;
    finally
      //释放内存aScanLine
      FreeMem(aScanLine, nScanLineSize);
    end;
  finally
    //释放内存aScnLnBuffer
    FreeMem(aScnLnBuffer, nMemSize);
  end;
end;

{ TFormToCaptureWindow }

constructor TFormToCaptureWindow.Create(AOwner: TComponent);
var
  aDC: HDC;
begin
  inherited CreateNew(AOwner);
  OnActivate:= FormActivate;
  OnMouseUp:= FormMouseUp;
  Cursor := crHandPoint;
  BorderStyle:= bsNone;
  FormStyle:= fsStayOnTop;
  SetBounds(0, 0, Screen.Width, Screen.Height);
end;

destructor TFormToCaptureWindow.Destroy;
begin
  Screen.Cursor := crDefault;
  inherited;
end;

procedure TFormToCaptureWindow.FormActivate(Sender: TObject);
begin
  Cursor := crHandPoint;
end;

procedure TFormToCaptureWindow.FormMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbLeft then
    ModalResult := mrOK
  else
    ModalResult := mrCancel;
end;

procedure TFormToCaptureWindow.WMEraseBkGnd(var Msg: TWMEraseBkGnd);
begin
  Msg.Result := 1;
end;

{ TFormToRegionWindow }

constructor TFormToRegionWindow.Create(AOwner: TComponent);
var
  aDC: HDC;
begin
  inherited CreateNew(AOwner);
  BorderStyle:= bsNone;
  FormStyle:= fsStayOnTop;
  OnMouseDown:= FormMouseDown;
  OnMouseMove:= FormMouseMove;
  OnMouseUp:= FormMouseUp;
  OnPaint:= FormPaint;
  OnShow:= FormShow;
  fBMP := TBitmap.Create;
  fBMP.Width := Screen.Width;
  fBMP.Height := Screen.Height;
  aDC := GetDC(0);
  BitBlt(fBMP.Canvas.handle, 0, 0, Screen.Width, Screen.Height, aDC, 0, 0, srcCopy);
  ReleaseDC(0, aDC);
  SetBounds(0, 0, Screen.Width, Screen.Height);
end;

destructor TFormToRegionWindow.Destroy;
begin
  fBMP.Free;
  inherited;
end;

procedure TFormToRegionWindow.FormMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if mbLeft = Button then begin
    fDragging := True;
    SetRect(fRect, X, Y, X, Y);
    Canvas.DrawFocusrect(fRect);
  end;
end;

procedure TFormToRegionWindow.FormMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  if fDragging then
  begin
    Canvas.DrawFocusrect(fRect);
    fRect.Right := X;
    fRect.Bottom := Y;
    Canvas.DrawFocusrect(fRect);
  end;

  with Canvas do
  begin
    Pen.Mode := pmXor;
    Pen.Color := clWhite;
    MoveTo(OldPos.x, 0);
    LineTo(OldPos.x, Screen.Height);
    MoveTo(0, OldPos.y);
    LineTo(Screen.Width, OldPos.y);
    MoveTo(X, 0);
    LineTo(X, Screen.Height);
    MoveTo(0, Y);
    LineTo(Screen.Width, Y);
    OldPos := Point(X, Y);
  end;
end;

procedure TFormToRegionWindow.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if fDragging then
  begin
    Canvas.DrawFocusrect(fRect);
    fDragging := False;
  end;
  ModalResult := mrOK;
end;

procedure TFormToRegionWindow.FormPaint(Sender: TObject);
begin
  Canvas.Brush.Style:= bsSolid;
  Canvas.Draw(0, 0, fBMP);
end;

procedure TFormToRegionWindow.FormShow(Sender: TObject);
begin
  setCursorPos(0, 0);
end;

procedure TFormToRegionWindow.WMEraseBkGnd(var Msg: TWMEraseBkGnd);
begin
  Msg.Result := 1;
end;

{ TRegionImage }

procedure TRegionImage.CMExit(var Message: TCMExit);
begin
  Inherited;
  Paint;
end;

constructor TRegionImage.Create(AOwner: TComponent);
var
  I: Integer;
begin
  inherited;
  FPosList[1] := htTopLeft;
  FPosList[2] := htTop;
  FPosList[3] := htTopRight;
  FPosList[4] := htRight;
  FPosList[5] := htBottomRight;
  FPosList[6] := htBottom;
  FPosList[7] := htBottomLeft;
  FPosList[8] := htLeft;
  for I := 1 to 8 do
  begin
    ShapeList[I] := TShape.Create(Self);
    ShapeList[I].Parent := Self;
    ShapeList[I].Brush.Color := clBlack;
    ShapeList[I].Visible := False;
  end;
end;

destructor TRegionImage.Destroy;
begin

  inherited;
end;

procedure TRegionImage.MouseDown(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  inherited;
  FDown := True;
  FOldPosition := Point(X, Y);
  SetFocus;
  Paint;
  if Assigned(OnClick) then
    OnClick(self);
end;

procedure TRegionImage.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  NewPoint: TPoint;

  function findnearest(X, Y: Integer): TPoint;
  begin
    Result.X := (X div 5) * 5 + Round((X mod 5) / 5) * 5;
    Result.Y := (Y div 5) * 5 + Round((Y mod 5) / 5) * 5;
  end;
begin
  inherited;
  if FDown then
  begin
    NewPoint := findnearest(Left + X - FOldPosition.X, Top + Y - FOldPosition.Y);
    with Self do
      SetBounds(NewPoint.X, NewPoint.Y, Width, height);
    Paint;
  end;
end;

procedure TRegionImage.MouseUp(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  inherited;
  FDown := False;
end;

procedure TRegionImage.Paint;
var
  I, baseH: Integer;
begin
  inherited;
  Canvas.Lock;
  Canvas.Brush.Color := clBlack;
  Canvas.Pen.Color := clRed;
  Canvas.FillRect(Rect(0, 0, BoundsRect.right, BoundsRect.Bottom));
  Canvas.Draw(-Left, -Top, Fbmp);
  for I := 1 to 8 do
    with FRectList[I] do
      Canvas.RecTangle(Left, Top, right, Bottom);
  Canvas.Unlock;
end;

procedure TRegionImage.Setbmp(const Value: TBitmap);
begin
  Fbmp := Value;
  Width:= Value.Width;
  Height:= Value.Height;
end;

procedure TRegionImage.WmLButtonDown(var Msg: TWmLButtonDown);
begin
  inherited;
end;

procedure TRegionImage.WmMove(var Msg: TWmMove);
var
  R: TRect;
begin
  R := BoundsRect;
  InflateRect(R, -2, -2);
  Paint;
end;

procedure TRegionImage.WmNcHitTest(var Msg: TWmNcHitTest);
var
  Pt: TPoint;
  I: Integer;
begin
  Pt := Point(Msg.XPos, Msg.YPos);
  Pt := ScreenToClient(Pt);
  Msg.Result := 0;
  for I := 1 to 8 do
    if PtInRect(FRectList[I], Pt) then
      Msg.Result := FPosList[I];
  if Msg.Result = 0 then
    inherited;
end;

procedure TRegionImage.WmSize(var Msg: TWmSize);
var
  R: TRect;
begin
  FRectList[1] := Rect(0, 0, 5, 5);
  FRectList[2] := Rect(Width div 2 - 3, 0, Width div 2 + 2, 5);
  FRectList[3] := Rect(Width - 5, 0, Width, 5);
  FRectList[4] := Rect(Width - 5, height div 2 - 3, Width, height div 2 + 2);
  FRectList[5] := Rect(Width - 5, height - 5, Width, height);
  FRectList[6] := Rect(Width div 2 - 3, height - 5, Width div 2 + 2, height);
  FRectList[7] := Rect(0, height - 5, 5, height);
  FRectList[8] := Rect(0, height div 2 - 3, 5, height div 2 + 2);
  Paint;
end;

{ TFormWithTrackBar }

constructor TFormWithTrackBar.Create(AOwner: TComponent);
begin
  inherited CreateNew(AOwner);
  TrackBar1:= TTrackBar.Create(self);
  TrackBar1.Parent:= Self;
  TrackBar1.Align:= alTop;
  TrackBar1.Min:= -255;
  TrackBar1.Max:= 255;
  TrackBar1.OnChange:= EventTracked;
  Image1:= TImage.Create(Self);
  Image1.Parent:= Self;
  Image1.Align:= alClient;
  FBmp:= TBitmap.Create;
  pl:= TPanel.Create(Self);
  pl.Parent:= Self;
  pl.Height:= 32;
  pl.Align:= alBottom;
  pl.ShowCaption:= False;
  btnOK:= TBitBtn.Create(self);
  btnOK.Parent:= pl;
  btnOK.Kind:= bkOK;
  btnOK.Top:= 4;
  btnOK.Left:= 4;
  btnCancel:= TBitBtn.Create(self);
  btnCancel.Parent:= pl;
  btnCancel.Kind:= bkCancel;
  btnCancel.Top:= 4;
  btnCancel.Left:= 84;
end;

destructor TFormWithTrackBar.Destroy;
begin
  FBmp.Free;
  Image1.Free;
  TrackBar1.Free;
  inherited;
end;

procedure TFormWithTrackBar.EventTracked(Sender: TObject);
begin
  if Assigned(FOnTrackChange) then
    FOnTrackChange(Self);
end;

procedure TFormWithTrackBar.SetBmp(const Value: TBitmap);
begin
  FBmp.Assign(Value);
  ClientWidth:= Value.Width;
  ClientHeight:= Value.Height + TrackBar1.Height + pl.Height;
  Image1.Picture.Bitmap.Assign(Value);
end;

procedure TFormWithTrackBar.SetOnTrackChange(const Value: TNotifyEvent);
begin
  FOnTrackChange := Value;
end;

end.
