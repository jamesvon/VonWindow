unit UDlgInputor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ValEdit, ExtCtrls, Buttons;

type
  TFDlgInputor = class(TForm)
    Panel1: TPanel;
    ValueListEditor1: TValueListEditor;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ValueListEditor1GetPickList(Sender: TObject;
      const KeyName: string; Values: TStrings);
  private
    function GetNames(Idx: integer): string;
    function GetValues(Subject: string): string;
    procedure SetValues(Subject: string; const Value: string);
    function GetSubjectTitle: string;
    function GetValueTitle: string;
    procedure SetSubjectTitle(const Value: string);
    procedure SetValueTitle(const Value: string);
  private
    { Private declarations }
    FItems: TStringList;
  public
    { Public declarations }
    procedure AddSubject(Subject: string; items: string; default: string);
    property Values[Subject: string]: string read GetValues write SetValues;
    property Names[Idx: integer]: string read GetNames;
  published
    property SubjectTitle: string read GetSubjectTitle write SetSubjectTitle;
    property ValueTitle: string read GetValueTitle write SetValueTitle;
  end;

var
  FDlgInputor: TFDlgInputor;

implementation

{$R *.dfm}

{ TFDlgInputor }

procedure TFDlgInputor.AddSubject(Subject, items, default: string);
begin
  ValueListEditor1.InsertRow(Subject, default, true);
  FItems.Add(Subject + '=' + items);
end;

procedure TFDlgInputor.FormCreate(Sender: TObject);
begin
  FItems:= TStringList.Create;
end;

procedure TFDlgInputor.FormDestroy(Sender: TObject);
begin
  FItems.Free;
end;

function TFDlgInputor.GetNames(Idx: integer): string;
begin
  Result:= FItems[Idx];
end;

function TFDlgInputor.GetSubjectTitle: string;
begin
  Result:= ValueListEditor1.TitleCaptions[0];
end;

function TFDlgInputor.GetValues(Subject: string): string;
begin
  Result:= ValueListEditor1.Cells[1, FItems.IndexOf(Subject) + 1];
end;

function TFDlgInputor.GetValueTitle: string;
begin
  Result:= ValueListEditor1.TitleCaptions[1];
end;

procedure TFDlgInputor.SetSubjectTitle(const Value: string);
begin
  ValueListEditor1.TitleCaptions[0]:= Value;
end;

procedure TFDlgInputor.SetValues(Subject: string; const Value: string);
begin
  ValueListEditor1.Cells[1, FItems.IndexOf(Subject) + 1]:= Value;
end;

procedure TFDlgInputor.SetValueTitle(const Value: string);
begin
  ValueListEditor1.TitleCaptions[1]:= Value;
end;

procedure TFDlgInputor.ValueListEditor1GetPickList(Sender: TObject;
  const KeyName: string; Values: TStrings);
begin
  Values.Delimiter:= ',';
  Values.DelimitedText:= FItems.Values[KeyName];
end;

end.
