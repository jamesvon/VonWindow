object FDlgListSelection: TFDlgListSelection
  Left = 0
  Top = 0
  ClientHeight = 398
  ClientWidth = 282
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    282
    398)
  PixelsPerInch = 96
  TextHeight = 13
  object ListBox1: TListBox
    Left = 8
    Top = 8
    Width = 266
    Height = 345
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 13
    TabOrder = 0
    OnDblClick = ListBox1DblClick
  end
  object BitBtn1: TBitBtn
    Left = 8
    Top = 359
    Width = 75
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = #30830#23450
    DoubleBuffered = True
    Kind = bkYes
    NumGlyphs = 2
    ParentDoubleBuffered = False
    TabOrder = 1
  end
  object BitBtn2: TBitBtn
    Left = 199
    Top = 359
    Width = 75
    Height = 25
    Anchors = [akLeft, akRight, akBottom]
    Caption = #25918#24323
    DoubleBuffered = True
    Kind = bkCancel
    NumGlyphs = 2
    ParentDoubleBuffered = False
    TabOrder = 2
  end
end
