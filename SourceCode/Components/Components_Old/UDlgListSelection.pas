(*******************************************************************************
  列表选择对话框   ListBox                          jamesvon write 2014/10/30
================================================================================
  MultiSelect : Boolean   是否支持多选
*******************************************************************************)
unit UDlgListSelection;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ImgList;

type
  TFDlgListSelection = class(TForm)
    ListBox1: TListBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure ListBox1DblClick(Sender: TObject);
    procedure ListBox1DrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
  private
    FImages: TImageList;
    procedure SetMultiSelect(const Value: Boolean);
    function GetMultiSelect: Boolean;
    procedure SetImages(const Value: TImageList);
    { Private declarations }
  public
    { Public declarations }
    property MultiSelect: Boolean read GetMultiSelect write SetMultiSelect;
    property Images: TImageList read FImages write SetImages;
  end;

  function DlgListSelection(Items: TStrings; Multi: Boolean): Integer; overload;
  function DlgListSelection(Items: TStrings; Imgs: TImageList; Multi: Boolean): Integer; overload;

var
  FDlgListSelection: TFDlgListSelection;

implementation

function DlgListSelection(Items: TStrings; Multi: Boolean): Integer;
begin
  Result:= DlgListSelection(Items, nil, Multi);
end;

function DlgListSelection(Items: TStrings; Imgs: TImageList; Multi: Boolean): Integer;
var
  I: Integer;
  AResult: Int64;
begin
  AResult := 1;
  result := 0;
  with TFDlgListSelection.Create(nil) do try
    ListBox1.Items.Assign(Items);
    MultiSelect:= Multi;
    Images:= Imgs;
    if ShowModal = mrYes then begin
      if MultiSelect then begin
        for I := 0 to ListBox1.Items.Count - 1 do begin
          if ListBox1.Selected[I] then
            result := result + AResult;
          AResult := AResult * 2;
        end;
      end else Result:= ListBox1.ItemIndex;
    end;
  finally
    Free;
  end;
end;

{$R *.dfm}

{ TFDlgListSelection }

function TFDlgListSelection.GetMultiSelect: Boolean;
begin
  Result:= ListBox1.MultiSelect;
end;

procedure TFDlgListSelection.ListBox1DblClick(Sender: TObject);
begin
  ModalResult:= mrYes;
end;

procedure TFDlgListSelection.ListBox1DrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin     //当有Images时采用此段代码
  ListBox1.Canvas.FillRect(Rect);
  FImages.Draw(ListBox1.Canvas, Rect.Left, Rect.Top, Index, dsTransparent, itImage, true);
  ListBox1.Canvas.TextOut(Rect.Left + FImages.Width + 2, Rect.Top, ListBox1.Items[Index]);
end;

procedure TFDlgListSelection.SetImages(const Value: TImageList);
begin
  FImages := Value;
  if Assigned(FImages) then
    ListBox1.OnDrawItem:= ListBox1DrawItem
  else ListBox1.OnDrawItem:= nil;
end;

procedure TFDlgListSelection.SetMultiSelect(const Value: Boolean);
begin
  ListBox1.MultiSelect:= Value;
end;

end.
