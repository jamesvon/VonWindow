object FDlgMemo: TFDlgMemo
  Left = 0
  Top = 0
  Caption = 'FDlgMemo'
  ClientHeight = 324
  ClientWidth = 531
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 288
    Width = 531
    Height = 36
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    DesignSize = (
      531
      36)
    object btnOK: TBitBtn
      Left = 360
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = #30830#23450
      Kind = bkOK
      NumGlyphs = 2
      TabOrder = 0
    end
    object btnCancel: TBitBtn
      Left = 449
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = #25918#24323
      Kind = bkCancel
      NumGlyphs = 2
      TabOrder = 1
    end
  end
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 531
    Height = 288
    Align = alClient
    ScrollBars = ssBoth
    TabOrder = 1
    ExplicitHeight = 283
  end
end
