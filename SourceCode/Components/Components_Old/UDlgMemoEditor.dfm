object DlgMemoEditor: TDlgMemoEditor
  Left = 0
  Top = 0
  Caption = 'DlgMemoEditor'
  ClientHeight = 220
  ClientWidth = 421
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 179
    Width = 421
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    ExplicitTop = 185
    DesignSize = (
      421
      41)
    object BitBtn1: TBitBtn
      Left = 248
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #30830#23450
      DoubleBuffered = True
      Kind = bkOK
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 0
    end
    object BitBtn2: TBitBtn
      Left = 336
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #25918#24323
      DoubleBuffered = True
      Kind = bkCancel
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 1
    end
  end
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 421
    Height = 179
    Align = alClient
    TabOrder = 1
    ExplicitLeft = 184
    ExplicitTop = 48
    ExplicitWidth = 185
    ExplicitHeight = 89
  end
end
