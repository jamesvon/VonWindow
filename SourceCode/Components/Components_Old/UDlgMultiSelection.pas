unit UDlgMultiSelection;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls, UVonSystemFuns;

type
  TEventOfGetValue = function (Idx: Integer): string of object;
  TFDlgMultiSelection = class(TForm)
    Panel2: TPanel;
    btnOK: TBitBtn;
    BitBtn2: TBitBtn;
    chklst1: TCheckListBox;
    btnSelectAll: TSpeedButton;
    btnSelectNone: TSpeedButton;
    procedure btnSelectAllClick(Sender: TObject);
    procedure btnSelectNoneClick(Sender: TObject);
  private
    FOnGetValue: TEventOfGetValue;
    function GetSelected: Int64;
    function GetValues: string;
    procedure SetSelected(const Value: Int64);
    procedure SetValues(const Value: string);
    procedure SetOnGetValue(const Value: TEventOfGetValue);
    { Private declarations }
  public
    { Public declarations }
    procedure Clear;
    procedure AddItem(AText: string);
    procedure AddDelimitedText(AText: string);
  published
    property Values: string read GetValues write SetValues;
    property Selected: Int64 read GetSelected write SetSelected;
    property OnGetValue: TEventOfGetValue read FOnGetValue write SetOnGetValue;
  end;

var
  FDlgMultiSelection: TFDlgMultiSelection;

implementation

{$R *.dfm}

procedure TFDlgMultiSelection.AddDelimitedText(AText: string);
var
  szList: TStringList;
begin
  szList:= TStringList.Create;
  szList.Delimiter:= ',';
  szList.DelimitedText:= AText;
  chklst1.Items.AddStrings(szList);
  szList.Free;
end;

procedure TFDlgMultiSelection.AddItem(AText: string);
begin
  chklst1.Items.Add(AText);
end;

procedure TFDlgMultiSelection.btnSelectAllClick(Sender: TObject);
begin
  chklst1.CheckAll(cbChecked);
end;

procedure TFDlgMultiSelection.btnSelectNoneClick(Sender: TObject);
begin
  chklst1.CheckAll(cbUnchecked);
end;

procedure TFDlgMultiSelection.Clear;
begin
  chklst1.Items.Clear;
end;

function TFDlgMultiSelection.GetSelected: Int64;
begin
  Result:= GetMutiChkByChkList(chklst1);
end;

function TFDlgMultiSelection.GetValues: string;
var
  I: Integer;
begin
  result := '';
  for I := 0 to chklst1.Count - 1 do
    if chklst1.Checked[I] then begin
      if Assigned(FOnGetValue) then
        Result := result + ',' + FOnGetValue(I)
      else result := result + ',' + chklst1.Items[I];
    end;
  if result <> '' then
    Delete(Result, 1, 1);
end;

procedure TFDlgMultiSelection.SetOnGetValue(const Value: TEventOfGetValue);
begin
  FOnGetValue := Value;
end;

procedure TFDlgMultiSelection.SetSelected(const Value: Int64);
begin
  SetMutiChkToChkList(Value, chklst1);
end;

procedure TFDlgMultiSelection.SetValues(const Value: string);
var
  szList: TStringList;
  I, Idx: Integer;
begin
  szList:= TStringList.Create;
  szList.Delimiter:= ',';
  szList.DelimitedText:= Value;
  chklst1.CheckAll(cbUnchecked);
  for I := 0 to szList.Count - 1 do begin
    Idx:= chklst1.Items.IndexOf(szList[I]);
    if Idx >= 0 then
      chklst1.Checked[Idx]:= True;
  end;
  szList.Free;
end;

end.
