object FDlgSetting: TFDlgSetting
  Left = 0
  Top = 0
  Caption = 'FDlgSetting'
  ClientHeight = 427
  ClientWidth = 270
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 386
    Width = 270
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    ExplicitLeft = -56
    ExplicitTop = 179
    ExplicitWidth = 421
    DesignSize = (
      270
      41)
    object BitBtn1: TBitBtn
      Left = 104
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #30830#23450
      DoubleBuffered = True
      Kind = bkOK
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 0
    end
    object BitBtn2: TBitBtn
      Left = 185
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #25918#24323
      DoubleBuffered = True
      Kind = bkCancel
      NumGlyphs = 2
      ParentDoubleBuffered = False
      TabOrder = 1
      ExplicitLeft = 336
    end
  end
  object lstSetting: TValueListEditor
    Left = 0
    Top = 0
    Width = 270
    Height = 386
    Align = alClient
    KeyOptions = [keyEdit, keyAdd, keyDelete, keyUnique]
    TabOrder = 1
    TitleCaptions.Strings = (
      #39033#30446
      #20869#23481)
    ExplicitWidth = 271
    ColWidths = (
      110
      154)
  end
end
