unit UDlgSetting;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ValEdit, StdCtrls, Buttons, ExtCtrls;

type
  TFDlgSetting = class(TForm)
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    lstSetting: TValueListEditor;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FSettingParams: TStringList;
    procedure EventOfGetPickList(Sender: TObject; const KeyName: string;
      Values: TStrings);
  public
    { Public declarations }
    procedure AddParams(Key, Param: string);
  end;

var
  FDlgSetting: TFDlgSetting;

implementation

{$R *.dfm}

procedure TFDlgSetting.AddParams(Key, Param: string);
begin
  FSettingParams.Values[Key]:= Param;
  lstSetting.OnGetPickList:= EventOfGetPickList;
end;

procedure TFDlgSetting.EventOfGetPickList(Sender: TObject;
  const KeyName: string; Values: TStrings);
var
  S: string;
begin
  S:= FSettingParams.Values[KeyName];
  if S <> '' then begin
    Values.Delimiter:= ',';
    Values.DelimitedText:= S;
  end;
end;

procedure TFDlgSetting.FormCreate(Sender: TObject);
begin
  FSettingParams:= TStringList.Create;
end;

procedure TFDlgSetting.FormDestroy(Sender: TObject);
begin
  FSettingParams.Free;
end;

end.
