unit UDlgVideo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin;

type
  TFDlgVideo = class(TForm)
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDlgVideo: TFDlgVideo;

implementation

{$R *.dfm}

procedure   TForm1.OpenPlayClick(Sender:   TObject);
begin
if   OpenDialog.Execute   then
begin
OffsetX   :=   Image.Width;

FilterGraph.Mode   :=   gmCapture;
FilterGraph.Active   :=   False;
FilterGraph.Active   :=   true;

FilterGraph.RenderFile(OpenDialog.FileName);
FilterGraph.Play;

end;
end;

procedure   TForm1.VMRTextOut(AText:   string);
var
VMRBitmap                   :   TVMRBitmap;
begin
if   AText   =   ��   then
exit;

end;

procedure   TForm1.SnapshotClick(Sender:   TObject);
begin
SampleGrabber.GetBitmap(Image.Picture.Bitmap)
end;

procedure   TForm1.FormClose(Sender:   TObject;   var   Action:   TCloseAction);
begin
CallBack.Checked   :=   false;
FilterGraph.ClearGraph;
FilterGraph.Active   :=   false;
end;

procedure   TForm1.SampleGrabberBuffer(sender:   TObject;   SampleTime:   Double;
pBuffer:   Pointer;   BufferLen:   Integer);
var
textLength                 :   integer;
begin
if   CallBack.Checked   then
begin
Image.Canvas.Lock;
try
SampleGrabber.GetBitmap(Image.Picture.Bitmap,   pBuffer,   BufferLen);

textLength   :=   Image.Picture.Bitmap.Canvas.TextWidth(ShowText);
if   (OffsetX   +   textLength)   >   0   then
begin
Image.Picture.Bitmap.Canvas.Brush.Style   :=   bsClear;
Image.Picture.Bitmap.Canvas.Font.Color   :=   clRed;
Image.Picture.Bitmap.Canvas.TextOut(OffsetX,   10,   ShowText);
Dec(OffsetX,   1);
end;

finally
Image.Canvas.Unlock;
end;
end;
end;

end.
