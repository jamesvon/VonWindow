object FrameAttachment: TFrameAttachment
  Left = 0
  Top = 0
  Width = 265
  Height = 304
  Align = alRight
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 265
    Height = 304
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    object lvDrawing: TListView
      Left = 0
      Top = 22
      Width = 265
      Height = 282
      Align = alClient
      Columns = <
        item
          Caption = #22270#32440#21517#31216
        end
        item
          Caption = #22270#21495
        end
        item
          Caption = #22270#32440#31867#21035
        end
        item
          Caption = #29256#26412
        end
        item
          Caption = #22791#27880
        end
        item
          Caption = #24405#20837#26102#38388
        end
        item
          Caption = #24405#20837#20154
        end>
      LargeImages = FThDB.imgFileTypeLarge
      SmallImages = FThDB.imgFileType
      TabOrder = 0
    end
    object ToolBar1: TToolBar
      Left = 0
      Top = 0
      Width = 265
      Height = 22
      AutoSize = True
      Caption = 'ToolBar1'
      Images = FThDB.ImgButton
      TabOrder = 1
      object btnDrawingAdd: TToolButton
        Left = 0
        Top = 0
        Hint = #28155#21152#22270#32440
        Caption = #28155#21152#22270#32440
        ImageIndex = 30
      end
      object btnDrawingView: TToolButton
        Left = 23
        Top = 0
        Hint = #26597#38405#22270#32440
        Caption = 'btnDrawingView'
        ImageIndex = 86
        OnClick = btnDrawingViewClick
      end
      object ToolButton5: TToolButton
        Left = 46
        Top = 0
        Width = 8
        Caption = 'ToolButton5'
        ImageIndex = 4
        Style = tbsSeparator
      end
      object btnDrawingDel: TToolButton
        Left = 54
        Top = 0
        Hint = #21024#38500#22270#32440
        Caption = 'btnDrawingDel'
        ImageIndex = 21
      end
      object ToolButton6: TToolButton
        Left = 77
        Top = 0
        Width = 8
        Caption = 'ToolButton6'
        ImageIndex = 4
        Style = tbsSeparator
      end
      object btnDrawingIcon: TToolButton
        Left = 85
        Top = 0
        Caption = 'btnDrawingIcon'
        ImageIndex = 6
        OnClick = btnDrawingIconClick
      end
      object btnDrawingSmall: TToolButton
        Left = 108
        Top = 0
        Caption = 'btnDrawingSmall'
        ImageIndex = 7
        OnClick = btnDrawingSmallClick
      end
      object btnDrawingList: TToolButton
        Left = 131
        Top = 0
        Caption = 'btnDrawingList'
        ImageIndex = 8
        OnClick = btnDrawingListClick
      end
      object btnDrawingDetail: TToolButton
        Left = 154
        Top = 0
        Caption = 'btnDrawingDetail'
        ImageIndex = 9
        OnClick = btnDrawingDetailClick
      end
    end
  end
end
