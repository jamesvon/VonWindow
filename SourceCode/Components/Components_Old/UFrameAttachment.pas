unit UFrameAttachment;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, Vcl.ToolWin,
  Vcl.ExtCtrls, ADODB, DB, UPlatformInfo;

type
  TFrameAttachment = class(TFrame)
    Panel1: TPanel;
    lvDrawing: TListView;
    ToolBar1: TToolBar;
    btnDrawingAdd: TToolButton;
    btnDrawingView: TToolButton;
    ToolButton5: TToolButton;
    btnDrawingDel: TToolButton;
    ToolButton6: TToolButton;
    btnDrawingIcon: TToolButton;
    btnDrawingSmall: TToolButton;
    btnDrawingList: TToolButton;
    btnDrawingDetail: TToolButton;
    procedure btnDrawingViewClick(Sender: TObject);
    procedure btnDrawingIconClick(Sender: TObject);
    procedure btnDrawingSmallClick(Sender: TObject);
    procedure btnDrawingListClick(Sender: TObject);
    procedure btnDrawingDetailClick(Sender: TObject);
  private
    { Private declarations }
    FFields: string;
    AQuery: TADOQuery;
    FTableName: string;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor destroy; override;
    procedure ListAttachment(TableName: string; OrgIdx: Integer);
    procedure AddColumn(Title, FieldName: string);
  end;

implementation

uses UPlatformDB, UDlgBrowser;

{$R *.dfm}

{ TFrameAttachment }

procedure TFrameAttachment.AddColumn(Title, FieldName: string);
begin
  FFields:= FFields + ',' + FieldName;
  lvDrawing.Columns.Add.Caption:= Title;
end;

procedure TFrameAttachment.btnDrawingDetailClick(Sender: TObject);
begin
  lvDrawing.ViewStyle:= vsReport;
end;

procedure TFrameAttachment.btnDrawingIconClick(Sender: TObject);
begin
  lvDrawing.ViewStyle:= vsIcon;
end;

procedure TFrameAttachment.btnDrawingListClick(Sender: TObject);
begin
  lvDrawing.ViewStyle:= vsList;
end;

procedure TFrameAttachment.btnDrawingSmallClick(Sender: TObject);
begin
  lvDrawing.ViewStyle:= vsSmallIcon;
end;

procedure TFrameAttachment.btnDrawingViewClick(Sender: TObject);
var
  fs: TFileStream;
  Filename: string;
begin
  if not Assigned(lvDrawing.Selected) then Exit;
  if not AQuery.Locate('ID', Integer(lvDrawing.Selected.Data), [])then Exit;
  Filename:= FPlatformDB.AppPath + 'TEMP\RES' + AQuery.Fields[2].AsString;
  fs:= TFileStream.Create(Filename, fmCreate);
  with TADOQuery.Create(nil) do
    try
      Connection := FPlatformDB.ADOConn;
      SQL.Text := 'SELECT Content FROM ' + FTableName + ' WHERE ID=' + IntToStr(Integer(lvDrawing.Selected.Data));
      Open;
      if not EOF then
        (FieldByName('Content') as TBlobField).SaveToStream(fs);
    finally
      Free;
    end;
  fs.Free;
  OpenHtml(Filename);
end;

constructor TFrameAttachment.Create(AOwner: TComponent);
begin
  inherited;
  AQuery:= TADOQuery.Create(nil);
  AQuery.Connection:= FPlatformDB.ADOConn;
end;

destructor TFrameAttachment.destroy;
begin
  AQuery.Free;
  inherited;
end;

procedure TFrameAttachment.ListAttachment(TableName: string; OrgIdx: Integer);
var
  I: Integer;
begin
  FTableName:= TableName;
  with AQuery do begin
    Close;
    SQL.Text:= 'SELECT A.ID, A.Attachment, A.FileExt' + FFields + ' FROM ' + TableName + ' A JOIN(SELECT ' +
      'A.Attachment,MAX(Version) AS VER FROM ' + TableName + ' WHERE OrgIdx=' + IntToStr(OrgIdx) + ') B ON ' +
      'B.Attachment=A.Attachment AND B.VER=A.Version WHERE A.OrgIdx=' + IntToStr(OrgIdx);
    Open;
    lvDrawing.Items.Clear;
    while not EOF do begin
      with lvDrawing.Items.Add do begin
        Caption:= Fields[1].AsString;
        for I := 3 to FieldCount - 1 do
          SubItems.Add(Fields[2].AsString);
        ImageIndex:= FPlatformDB.GetFileTypeImageIndex(Fields[2].AsString);
        Data:= Pointer(Fields[0].AsInteger);
      end;
      Next;
    end;
  end;
end;

end.
