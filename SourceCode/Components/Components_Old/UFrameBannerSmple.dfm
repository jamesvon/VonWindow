inherited FrameBannerSimple: TFrameBannerSimple
  Height = 25
  ExplicitWidth = 451
  ExplicitHeight = 25
  object plWinBar: TPanel
    Left = 0
    Top = 0
    Width = 451
    Height = 25
    Align = alTop
    BevelOuter = bvLowered
    Color = clInactiveCaption
    ParentBackground = False
    TabOrder = 0
    object btnWinIcon: TSpeedButton
      Left = 1
      Top = 1
      Width = 23
      Height = 23
      Align = alLeft
      BiDiMode = bdLeftToRight
      Flat = True
      ParentBiDiMode = False
      OnClick = btnWinIconClick
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitHeight = 32
    end
    object lbWinCaption: TLabel
      AlignWithMargins = True
      Left = 27
      Top = 1
      Width = 63
      Height = 23
      Margins.Top = 0
      Margins.Bottom = 0
      Align = alLeft
      Caption = 'lbWinCaption'
      Layout = tlCenter
      OnDblClick = lbWinCaptionDblClick
      OnMouseMove = lbWinCaptionMouseMove
      ExplicitHeight = 13
    end
    object lbWinVersion: TLabel
      AlignWithMargins = True
      Left = 96
      Top = 1
      Width = 61
      Height = 23
      Margins.Top = 0
      Margins.Bottom = 0
      Align = alLeft
      Caption = 'lbWinVersion'
      Layout = tlCenter
      OnMouseMove = lbWinCaptionMouseMove
      ExplicitHeight = 13
    end
    object lbSystemHint: TLabel
      AlignWithMargins = True
      Left = 163
      Top = 1
      Width = 3
      Height = 23
      Margins.Top = 0
      Margins.Bottom = 0
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Layout = tlCenter
      ExplicitHeight = 13
    end
    object ToolBar1: TToolBar
      Left = 170
      Top = 1
      Width = 280
      Height = 23
      Align = alRight
      AutoSize = True
      ButtonWidth = 24
      Caption = 'tbWinMenu'
      Images = FPlatformDB.ImgButton
      List = True
      AllowTextButtons = True
      TabOrder = 0
      object btnWinCascade: TToolButton
        Left = 0
        Top = 0
        Caption = 'btnWinCascade'
        ImageIndex = 10
        Visible = False
        OnClick = btnWinCascadeClick
      end
      object btnWinHTitle: TToolButton
        Left = 24
        Top = 0
        Caption = 'btnWinHTitle'
        ImageIndex = 11
        Visible = False
        OnClick = btnWinHTitleClick
      end
      object btnWinVTitle: TToolButton
        Left = 48
        Top = 0
        Caption = 'btnWinVTitle'
        ImageIndex = 12
        Visible = False
        OnClick = btnWinVTitleClick
      end
      object tbSeparator1: TToolButton
        Left = 72
        Top = 0
        Width = 8
        Caption = 'tbSeparator1'
        ImageIndex = 12
        Style = tbsSeparator
        Visible = False
      end
      object btnWinMin: TToolButton
        Left = 80
        Top = 0
        Caption = 'btnWinMin'
        ImageIndex = 0
        OnClick = btnWinMinClick
      end
      object btnWinMax: TToolButton
        Left = 104
        Top = 0
        Caption = 'btnWinMax'
        ImageIndex = 1
        OnClick = btnWinMaxClick
      end
      object btnWinClose: TToolButton
        Left = 128
        Top = 0
        Caption = 'btnWinClose'
        ImageIndex = 3
        OnClick = btnWinCloseClick
      end
      object ToolButton1: TToolButton
        Left = 152
        Top = 0
        Width = 8
        Caption = 'ToolButton1'
        ImageIndex = 8
        Style = tbsSeparator
      end
      object btnWinHelp: TToolButton
        Left = 160
        Top = 0
        Caption = 'btnWinHelp'
        ImageIndex = 4
        OnClick = btnWinHelpClick
      end
      object btnWinAbout: TToolButton
        Left = 184
        Top = 0
        Caption = 'btnWinAbout'
        ImageIndex = 101
        OnClick = btnWinAboutClick
      end
      object btnWinHome: TToolButton
        Left = 208
        Top = 0
        Caption = 'btnWinHome'
        ImageIndex = 5
        OnClick = btnWinHomeClick
      end
      object ToolButton2: TToolButton
        Left = 232
        Top = 0
        Caption = 'ToolButton2'
        ImageIndex = 86
        OnClick = ToolButton2Click
      end
      object btnWinExit: TToolButton
        Left = 256
        Top = 0
        Caption = 'btnWinExit'
        ImageIndex = 132
        Visible = False
        OnClick = btnWinExitClick
      end
    end
    object tabWinChildren: TTabSet
      Left = 169
      Top = 1
      Width = 1
      Height = 23
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBackground = True
      SoftTop = True
      TabPosition = tpTop
    end
  end
end
