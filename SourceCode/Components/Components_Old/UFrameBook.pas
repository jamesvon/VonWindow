unit UFrameBook;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, Grids, ComCtrls, UVonSystemFuns, UVonConfig;

type
  TFrameBook = class(TFrame)
    TabControl1: TTabControl;
    Grid: TStringGrid;
    procedure TabControl1Change(Sender: TObject);
    procedure GridDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
  private
    FBook: TVonBook;
    procedure SetBook(const Value: TVonBook);
    { Private declarations }
  public
    { Public declarations }
    function AppendPage: Integer;
    procedure InsertPage(PageIdx: Integer);
    procedure DeletePage;
  published
    property Book: TVonBook read FBook write SetBook;
    property CurrentPage: Integer;
    property CurrentRow: Integer;
    property CurrentCol: Integer;
  end;

implementation

{$R *.dfm}

{ TFrameBook }

procedure TFrameBook.GridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
  procedure WriteText(Rect: TRect; Text: string);
  var
    w, h: Integer;
  begin
    w:= Grid.Canvas.TextWidth(Text);
    h:= Grid.Canvas.TextHeight(Text);
    Grid.Canvas.TextRect(Rect,
      Rect.Left + (Rect.Right - Rect.Left - w) div 2,
      Rect.Top + (Rect.Bottom - Rect.Top - h) div 2, Text);
  end;
begin
  if(ACol = 0)and(ARow = 0)then Exit;
  if ACol = 0 then
    WriteText(Rect, IntToStr(ARow));
  if ARow = 0 then
    WriteText(Rect, IntToCol(ACol - 1));
end;

procedure TFrameBook.SetBook(const Value: TVonBook);
var
  I: Integer;
begin
  FBook := Value;
  for I := 1 to FBook.PageCount do
    TabControl1.Tabs.Add(IntToStr(I));
  TabControl1.TabIndex:= 0;
end;

procedure TFrameBook.TabControl1Change(Sender: TObject);
begin
  Grid.RowCount:= FBook.Pages[TabControl1.TabIndex].Count + 1;
  Grid.ColCount:= FBook.Pages[TabControl1.TabIndex].ColCount[0] + 1;
end;

end.
