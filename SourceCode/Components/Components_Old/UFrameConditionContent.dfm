object FrameConditionContent: TFrameConditionContent
  Left = 0
  Top = 0
  Width = 501
  Height = 21
  TabOrder = 0
  object Label1: TLabel
    AlignWithMargins = True
    Left = 357
    Top = 3
    Width = 48
    Height = 15
    Align = alRight
    Caption = #26465#20214#20851#31995
    Layout = tlCenter
    ExplicitHeight = 13
  end
  object Label2: TLabel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 48
    Height = 15
    Align = alLeft
    Caption = #26465#20214#20869#23481
    Layout = tlCenter
    ExplicitHeight = 13
  end
  object ERelation: TComboBox
    Left = 408
    Top = 0
    Width = 93
    Height = 21
    Align = alRight
    Style = csDropDownList
    ItemIndex = 0
    TabOrder = 0
    Text = #26080
    Items.Strings = (
      #26080
      #19982
      #25110
      #38750)
  end
  object ECondition: TEdit
    Left = 54
    Top = 0
    Width = 300
    Height = 21
    Align = alClient
    TabOrder = 1
    TextHint = #22635#20889#26465#20214#20869#23481
    ExplicitLeft = 96
    ExplicitTop = 8
    ExplicitWidth = 121
  end
end
