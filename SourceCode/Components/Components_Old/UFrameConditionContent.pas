unit UFrameConditionContent;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFrameConditionContent = class(TFrame)
    Label1: TLabel;
    ERelation: TComboBox;
    Label2: TLabel;
    ECondition: TEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
