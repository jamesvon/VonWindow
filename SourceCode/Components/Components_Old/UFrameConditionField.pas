unit UFrameConditionField;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, Buttons, StdCtrls, DB, UVonConfig;

type
  TEventToLoadFields = procedure (TableIdx: Integer; Fields: TVonNameValueData) of object;

  TFrameConditionField = class(TFrame)
    ECondition: TComboBox;
    EOrgTable: TComboBox;
    EComp: TComboBox;
    EDestTable: TComboBox;
    EOrgField: TComboBox;
    EDestField: TComboBox;
    btnLevelLeft: TSpeedButton;
    btnLevelRight: TSpeedButton;
    btnLevelMid: TSpeedButton;
    btnDelete: TSpeedButton;
    btnNew: TSpeedButton;
    procedure btnLevelRightClick(Sender: TObject);
    procedure btnLevelLeftClick(Sender: TObject);
    procedure EOrgTableChange(Sender: TObject);
    procedure EDestTableChange(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
  private
    { Private declarations }
    FLevel: Integer;
    FTables, FOrgFields, FDestFields: TVonNameValueData;
    FOnSelectedCondition: TNotifyEvent;
    FOnDeletedCondition: TNotifyEvent;
    FOnLoadFields: TEventToLoadFields;
    FIndex: Integer;
    FOnNewCondition: TNotifyEvent;
    procedure SetLevel(const Value: Integer);
    procedure SetOnDeletedCondition(const Value: TNotifyEvent);
    procedure SetOnSelectedCondition(const Value: TNotifyEvent);
    procedure SetIndex(const Value: Integer);
    procedure SetOnLoadFields(const Value: TEventToLoadFields);
    procedure SetOnNewCondition(const Value: TNotifyEvent);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetTables(const ADataSet: TDataSet);
    function SetPSQL(PSQL: PChar; HasLinker: Boolean): PChar;
    function GetSQL: string;
  published
    property Level: Integer read FLevel write SetLevel;
    property Index: Integer read FIndex write SetIndex;
    property OnSelectedCondition: TNotifyEvent read FOnSelectedCondition write SetOnSelectedCondition;
    property OnDeletedCondition: TNotifyEvent read FOnDeletedCondition write SetOnDeletedCondition;
    property OnNewCondition: TNotifyEvent read FOnNewCondition write SetOnNewCondition;
    property OnLoadFields: TEventToLoadFields read FOnLoadFields write SetOnLoadFields;
  end;

implementation

{$R *.dfm}

{ TFrameConditionField }

constructor TFrameConditionField.Create(AOwner: TComponent);
begin
  inherited;
  FTables:= TVonNameValueData.Create;
  FOrgFields:= TVonNameValueData.Create;
  FDestFields:= TVonNameValueData.Create;
end;

destructor TFrameConditionField.Destroy;
begin
  inherited;
  FTables.Free;
  FOrgFields.Free;
  FDestFields.Free;
end;

(* Button methods *)

procedure TFrameConditionField.btnDeleteClick(Sender: TObject);
begin
  if Assigned(FOnDeletedCondition) then
    FOnDeletedCondition(Self);
end;

procedure TFrameConditionField.btnLevelLeftClick(Sender: TObject);
begin
  Level:= Level - 1;
end;

procedure TFrameConditionField.btnLevelRightClick(Sender: TObject);
begin
  Level:= Level + 1;
end;

procedure TFrameConditionField.btnNewClick(Sender: TObject);
begin
  if Assigned(FOnNewCondition) then
    FOnNewCondition(Self);
end;

(* Table changed *)

procedure TFrameConditionField.EDestTableChange(Sender: TObject);
var
  I: Integer;
begin
  EDestField.Items.Clear;
  if(EDestTable.ItemIndex > 0) and Assigned(FOnLoadFields) then begin
    FOnLoadFields(Integer(FTables.Objects[EDestTable.ItemIndex - 1]), FDestFields);
    for I := 0 to FDestFields.Count - 1 do
      EDestField.Items.Add(FDestFields.Values[I]);
  end;
end;

procedure TFrameConditionField.EOrgTableChange(Sender: TObject);
var
  I: Integer;
begin
  if Assigned(FOnLoadFields) then begin
    FOnLoadFields(Integer(FTables.Objects[EOrgTable.ItemIndex - 1]), FOrgFields);
    EOrgField.Items.Clear;
    for I := 0 to FOrgFields.Count - 1 do
      EOrgField.Items.Add(FOrgFields.Values[I]);
  end;
end;

(* Generator *)

function TFrameConditionField.GetSQL: string;
begin
  Result:= '';
  Result:= Result + FTables.Names[EOrgTable.ItemIndex] + '.[' +
    FOrgFields.Names[EOrgField.ItemIndex] + ']' + EComp.Text + ' ';
  if EDestTable.ItemIndex = 0 then Result:= Result + EDestField.Text
  else begin
    Result:= Result + FTables.Names[EDestTable.ItemIndex] + '.[' +
      FDestFields.Names[EDestField.ItemIndex] + ']'
  end;
end;

function TFrameConditionField.SetPSQL(PSQL: PChar; HasLinker: Boolean): PChar;
var
  S: string;
  Idx: Integer;
  IsComp: Boolean;

  procedure AddStr;
  begin
    if S[1] = '[' then Delete(S, 1, 1);
    if S[Length(S)] = ']' then Delete(S, Length(S), 1);
    case Idx of
    0: if S = 'AND' then ECondition.ItemIndex:= 0 else ECondition.ItemIndex:= 1;//AND   OR
    1: EOrgField.ItemIndex:= FOrgFields.IndexOfName(S);                         //A.ID  [name]
    2: EComp.ItemIndex:= EComp.Items.IndexOf(S);                                // =    <>
    3: begin
        EDestField.ItemIndex:= FDestFields.IndexOfName(S);                      // 123  'weerwq'
        if EDestField.ItemIndex < 0 then EDestField.Text:= S;
      end;
    end;
    S:= '';
    Inc(Idx);
    IsComp:= False;
  end;
  procedure AddTable;
  begin
    if S[1] = '[' then Delete(S, 1, 1);
    if S[Length(S)] = ']' then Delete(S, Length(S) - 1, 1);
    if Idx = 1 then begin
      EOrgTable.ItemIndex:= FTables.IndexOfName(S);
      EOrgTableChange(nil);
    end else begin
      EDestTable.ItemIndex:= FTables.IndexOfName(S) + 1;
      EDestTableChange(nil);
    end;
    S:= '';
  end;
begin
  if HasLinker then Idx:= 0 else Idx:= 1;     //A.ID<>D.Ee AND(A.dd>12 OR s<>'ddd')
  IsComp:= False;
  Result:= PSQL;
  repeat
    case Result^ of
    '.': AddTable;
    '(': Level:= Level + 1;
    '>', '<', '=': begin
        AddStr;
        IsComp:= True;
        S:= S + Result^;
      end;
    ')', ' ', #13, #10: begin
        if S <> '' then AddStr;
        if Idx > 3 then Exit;
      end;
    else begin
        if IsComp and not (Result^ in ['=', '<', '>']) then AddStr
        else S:= S + Result^;
      end;
    end;
    Inc(Result);
  until Result^ = #0;
  if S <> '' then AddStr;
end;

(* properties *)

procedure TFrameConditionField.SetIndex(const Value: Integer);
begin
  FIndex := Value;
end;

procedure TFrameConditionField.SetLevel(const Value: Integer);
begin
  FLevel := Value;
  btnLevelLeft.Visible:= FLevel > 0;
  btnLevelMid.Visible:= FLevel > 1;
  if btnLevelMid.Visible then
    btnLevelMid.Width:= (FLevel - 1) * 23;
end;

procedure TFrameConditionField.SetOnDeletedCondition(const Value: TNotifyEvent);
begin
  FOnDeletedCondition := Value;
end;

procedure TFrameConditionField.SetOnLoadFields(const Value: TEventToLoadFields);
begin
  FOnLoadFields := Value;
end;

procedure TFrameConditionField.SetOnNewCondition(const Value: TNotifyEvent);
begin
  FOnNewCondition := Value;
end;

procedure TFrameConditionField.SetOnSelectedCondition(
  const Value: TNotifyEvent);
begin
  FOnSelectedCondition := Value;
end;

procedure TFrameConditionField.SetTables(const ADataSet: TDataSet);
var
  I: Integer;
begin
  FTables.Clear;
  EOrgTable.Clear;
  EDestTable.Items.Text:= '�Ǳ�����';
  if Assigned(ADataSet) then
    with ADataSet do begin
      First;
      while not EOF do begin
        FTables.Add(ADataSet.FieldByName('tbCode').AsString,
          ADataSet.FieldByName('tbName').AsString,
          TObject(ADataSet.FieldByName('ID').AsInteger));
        EOrgTable.Items.Add(ADataSet.FieldByName('tbName').AsString);
        EDestTable.Items.Add(ADataSet.FieldByName('tbName').AsString);
        Next;
      end;
    end;
  EOrgTable.ItemIndex:= 0;
  EDestTable.ItemIndex:= 0;
end;

end.
