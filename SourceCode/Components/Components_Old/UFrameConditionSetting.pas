unit UFrameConditionSetting;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, UFrameConditionContent, ToolWin, UPlatformDB,
  ImgList, System.ImageList;

type
  TFrameConditionSetting = class(TFrame)
    Panel1: TPanel;
    ToolBar1: TToolBar;
    FrameConditionContent1: TFrameConditionContent;
    Bevel1: TBevel;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ImageList1: TImageList;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
