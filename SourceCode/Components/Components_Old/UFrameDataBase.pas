unit UFrameDataBase;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFrameDataBase = class(TFrame)
    lbKey: TLabel;
  private
    FParams: string;
    { Private declarations }
    procedure SetKeyValue(const Value: string);
    function GetKeyValue: string;
    procedure SetParams(const Value: string);
  protected
    procedure SetDataValue(const Value: string); virtual; abstract;
    function GetDataValue: string; virtual; abstract;
  public
    { Public declarations }
    constructor Create;
  published
    property KeyValue: string read GetKeyValue write SetKeyValue;
    property DataValue: string read GetDataValue write SetDataValue;
    property Params: string read FParams write SetParams;
  end;

implementation

{$R *.dfm}
{ TFrameDataBase }

constructor TFrameDataBase.Create;
begin
  inherited Create(nil);
end;

function TFrameDataBase.GetKeyValue: string;
begin
  Result := lbKey.Caption;
end;

procedure TFrameDataBase.SetKeyValue(const Value: string);
begin
  lbKey.Caption := Value;
end;

procedure TFrameDataBase.SetParams(const Value: string);
begin
  FParams := Value;
end;

end.
