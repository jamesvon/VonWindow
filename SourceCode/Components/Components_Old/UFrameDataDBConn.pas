unit UFrameDataDBConn;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrameDataBase, StdCtrls, Data.win.AdoConEd, Buttons;

type
  TFrameDataDBConn = class(TFrameDataBase)
    Edit1: TEdit;
    SpeedButton1: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure SetDataValue(const Value: string); override;
    function GetDataValue: string; override;
  public
    { Public declarations }
  end;

var
  FrameDataDBConn: TFrameDataDBConn;

implementation

{$R *.dfm}

function TFrameDataDBConn.GetDataValue: string;
begin
  Result := Edit1.Text;
end;

procedure TFrameDataDBConn.SetDataValue(const Value: string);
begin
  inherited;
  Edit1.Text := Value;
end;

procedure TFrameDataDBConn.SpeedButton1Click(Sender: TObject);
var
  InitialConnStr: WideString;
begin
  with TConnEditForm.Create(Application) do
    try
      InitialConnStr := Edit1.Text;
      Edit1.Text := Edit(InitialConnStr);
    finally
      Free;
    end;
end;

end.
