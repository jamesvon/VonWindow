unit UFrameDataList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, UVonConfig;

type
  ///  <summary>录入数据类型</summary>
  ///  <param name="LIDT_Text">单行文字录入（TEdit）</param>
  ///  <param name="LIDT_Int">整数录入(TSpinEdit)</param>
  ///  <param name="LIDT_Float">浮点数据录入（TEdit）</param>
  ///  <param name="LIDT_Conn">数据库链接录入(DlgConnection)</param>
  ///  <param name="LIDT_Date">日期数据录入（TPickDatetime）</param>
  ///  <param name="LIDT_Time">时间数据录入（TPickDatetime）</param>
  ///  <param name="LIDT_Memo">多行数据录入(TMemo)</param>
  ///  <param name="LIDT_RichText">富文本数据录入(FrameRichImputor)</
  ///  <param name="LIDT_SelectText">选择数据录入，返回文字（TComBoBox）</param>
  ///  <param name="LIDT_SelectValue">选择数据录入，返回序号（TComBoBox）</param>
  ///  <param name="LIDT_ChoiceText">单选数据录入，返回文字(TRadioGroup)</param>
  ///  <param name="LIDT_ChoiceValue">单选数据录入，返回序号(TRadioGroup)</param>
  ///  <param name="LIDT_CheckListText">复选列表录入，返回以逗号间隔的文字(TCheckList)</param>
  ///  <param name="LIDT_CheckListValue">复选列表录入，返回Int64(TCheckList)</param>
  ///  <param name="LIDT_Bitmap">图片录入(FrameImageImputor)</param>
  ///  <param name="LIDT_Dialog">...</param>
  TVonListInputDataType = (LIDT_Text, LIDT_Int, LIDT_Float, LIDT_Conn, LIDT_Date,
    LIDT_Time, LIDT_Memo, LIDT_RichText, LIDT_SelectText, LIDT_SelectValue,
    LIDT_ChoiceText, LIDT_ChoiceValue, LIDT_CheckListText, LIDT_CheckListValue,
    LIDT_Bitmap, LIDT_Dialog);

  TVonListInputDataBase = class
  published
    property DataType: TVonListInputDataType;
    property Text: string;
    property Value: string;
    property OnClick: TNotifyEvent;
  end;

  TFrameDataList = class(TFrame)
    ScrollBox1: TScrollBox;
  private
    { Private declarations }
    FValueList: TVonSetting;
    FTypeList: TVonArraySetting;
    FList: TList;
    procedure SetTypeList(const Value: TVonArraySetting);
    procedure SetValueList(const Value: TVonSetting);
    function GetValueList: TVonSetting;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Clear;
    property TypeList: TVonArraySetting read FTypeList write SetTypeList;       //应用系统设置的参数列表
    property ValueList: TVonSetting read GetValueList write SetValueList;         //证书本身部分参数列表
  end;

implementation

uses UFrameDataBase, UFrameDataText, UFrameDataDBConn;

{$R *.dfm}

{ TFrameDataList }

procedure TFrameDataList.Clear;
var
  I: Integer;
begin
  for I := 0 to FList.Count - 1 do
    TObject(FList[I]).Free;
  FList.Clear;
end;

constructor TFrameDataList.Create(AOwner: TComponent);
begin
  inherited;
  FList:= TList.Create;
end;

destructor TFrameDataList.Destroy;
begin
  Clear;
  FList.Free;
  inherited;
end;

function TFrameDataList.GetValueList: TVonSetting;
var
  I: Integer;
begin
  for I := 0 to FList.Count - 1 do
    with TFrameDataBase(FList[I]) do
      FValueList.NameValue[KeyValue]:= DataValue;
  Result:= FValueList;
end;

procedure TFrameDataList.SetTypeList(const Value: TVonArraySetting);
var
  I: Integer;

  procedure AddField(szField: TFrameDataBase);
  begin
    FList.Add(Pointer(szField));
    szField.Top:= 65536;
    szField.Align:= alTop;
    szField.Parent:= ScrollBox1;
    szField.KeyValue:= FTypeList.Values[I, 0];
    szField.Params:= FTypeList.Values[I, 2];
  end;
begin
  FTypeList := Value;
  Clear;
  for I := 0 to FTypeList.Count - 1 do
    if FTypeList.Values[I, 1] = '文字' then
      AddField(TFrameDataText.Create)
    else if FTypeList.Values[I, 1] = '数据库提取' then
      AddField(TFrameDataDBConn.Create);
//文字
//数字
//枚举

//树选择
//唯一值

end;

procedure TFrameDataList.SetValueList(const Value: TVonSetting);
var
  I: Integer;
begin
  FValueList := Value;
  for I := 0 to FList.Count - 1 do
    with TFrameDataBase(FList[I]) do
      DataValue:= FValueList.NameValue[KeyValue];
end;

end.
