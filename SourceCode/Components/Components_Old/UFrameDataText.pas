unit UFrameDataText;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrameDataBase, StdCtrls;

type
  TFrameDataText = class(TFrameDataBase)
    Edit1: TEdit;
  private
    { Private declarations }
    procedure SetDataValue(const Value: string); override;
    function GetDataValue: string; override;
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}
{ TFrameDataBase1 }

function TFrameDataText.GetDataValue: string;
begin
  Result := Edit1.Text;
end;

procedure TFrameDataText.SetDataValue(const Value: string);
begin
  inherited;
  Edit1.Text := Value;
end;

end.
