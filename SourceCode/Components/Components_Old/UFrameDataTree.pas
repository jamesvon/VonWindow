unit UFrameDataTree;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, DB, ToolWin, ComCtrls;

type
  TTreeDataLink = class(TDataLink)
  private
    FTree: TTreeView;
  protected
    procedure ActiveChanged; override;
    procedure DataSetChanged; override;
    procedure DataSetScrolled(Distance: Integer); override;
    procedure FocusControl(Field: TFieldRef); override;
    procedure EditingChanged; override;
    procedure LayoutChanged; override;
    procedure RecordChanged(Field: TField); override;
    procedure UpdateData; override;
  public
    constructor Create(ATree: TTreeView);
    destructor Destroy; override;
  published
    property Tree: TTreeView read FTree;
  end;

  TFrameDataTree = class(TFrame)
    TreeView1: TTreeView;
    ToolBar1: TToolBar;
  private
    { Private declarations }
  public
    { Public declarations }
  published
    property DataSource: TDataSource;
    property DisplayFieldname: string;
    property IDFieldname: string;
    property ParentFieldname: string;
  end;

implementation

{$R *.dfm}

end.
