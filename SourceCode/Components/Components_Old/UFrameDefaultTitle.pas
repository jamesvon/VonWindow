unit UFrameDefaultTitle;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Imaging.jpeg,
  Vcl.ExtCtrls, UPlatformInfo, Vcl.ComCtrls, Vcl.ToolWin, System.ImageList,
  Vcl.ImgList, Vcl.Tabs, Vcl.StdCtrls, Vcl.Buttons, UFrameBannerBase;

resourcestring
  WIN_HINT = 'The system will expire after %d days.';
  WIN_MULTI_MORE = '您打开的窗口太多，请关闭一些窗口后再打开。';
  RES_NO_RIGHT = '您的权限不能使用本功能';

type
  TFrameDefaultTitle = class(TFrameBannerBase)
    imgWinBar: TImageList;
    plWinBar: TPanel;
    ToolBar1: TToolBar;
    btnWinCascade: TToolButton;
    btnWinHTitle: TToolButton;
    btnWinVTitle: TToolButton;
    tbSeparator1: TToolButton;
    btnWinMin: TToolButton;
    btnWinMax: TToolButton;
    btnWinClose: TToolButton;
    ToolButton1: TToolButton;
    btnWinHelp: TToolButton;
    btnWinAbout: TToolButton;
    btnWinHome: TToolButton;
    ToolButton2: TToolButton;
    btnWinExit: TToolButton;
    tabWinChildren: TTabSet;
    btnWinIcon: TSpeedButton;
    lbWinCaption: TLabel;
    lbWinVersion: TLabel;
    lbSystemHint: TLabel;
    procedure btnWinIconClick(Sender: TObject);
    procedure btnWinAboutClick(Sender: TObject);
    procedure btnWinCascadeClick(Sender: TObject);
    procedure btnWinCloseClick(Sender: TObject);
    procedure btnWinExitClick(Sender: TObject);
    procedure btnWinHelpClick(Sender: TObject);
    procedure btnWinHomeClick(Sender: TObject);
    procedure btnWinHTitleClick(Sender: TObject);
    procedure btnWinMaxClick(Sender: TObject);
    procedure btnWinMinClick(Sender: TObject);
    procedure btnWinVTitleClick(Sender: TObject);
    procedure lbWinCaptionDblClick(Sender: TObject);
    procedure lbWinCaptionMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure tabWinChildrenChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure tabWinChildrenGetImageIndex(Sender: TObject; TabIndex: Integer;
      var ImageIndex: Integer);
    procedure tabWinChildrenMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ToolButton2Click(Sender: TObject);
  private{ Private declarations }
    { Private declarations }
    FOpenASystemWindow: TEventToOpenASystemWindow;
    function GetFormCount: Integer; override;
    procedure SetOpenASystemWindow(const Value: TEventToOpenASystemWindow);
  public
    { Public declarations }
    function OpenedAForm(FormTitle: string; FormClass: TFormClass): TForm; override;
    procedure CloseForm(AForm: TForm); override;
    procedure ChangeFormState(AForm: TForm); override;
    procedure DisplayWinChildrenBtns; override;
    procedure SetTitle(const Value: string); override;
    procedure SetVersion(const Value: string); override;
    procedure SetSysHint(const SysHint: string); override;
    procedure SetIcon(Icon: TIcon); override;
    function GetFormByName(UsingName: string): TForm; override;
    function GetFormByIndex(Index: Integer): TForm; override;
  published
    property FormCount: Integer read GetFormCount;
    property OpenASystemWindow : TEventToOpenASystemWindow read FOpenASystemWindow write SetOpenASystemWindow;
  end;

implementation

{$R *.dfm}

{ TFrameJamesTitle }

function TFrameDefaultTitle.OpenedAForm(FormTitle: string; FormClass: TFormClass): TForm;
var
  Idx: Integer;
begin
  if tabWinChildren.Width - tabWinChildren.ItemRect(tabWinChildren.TabIndex).Right < 100 then
    raise Exception.Create(WIN_MULTI_MORE)
  else
  begin
    Idx:= tabWinChildren.Tabs.IndexOf(FormTitle);
    if Idx < 0 then begin
      Application.CreateForm(FormClass, Result);
      tabWinChildren.TabIndex := tabWinChildren.Tabs.AddObject(FormTitle, Result);
      //tabWinChildren.Width := tabWinChildren.Canvas.TextWidth(tabWinChildren.Tabs.Text) + 5;
      if Assigned(Result.OnActivate) then
        Result.OnActivate(Result);
    end else Result:= tabWinChildren.Tabs.Objects[Idx] as TForm;
  end;
end;

procedure TFrameDefaultTitle.SetIcon(Icon: TIcon);
var
  bmp1: TBitmap;
begin
//  bmp1:= TBitmap.Create;
//  Icon.AssignTo(bmp1);
//  btnWinIcon.Glyph.SetSize(btnWinIcon.Width, btnWinIcon.Height);
//  btnWinIcon.Glyph.Canvas.Brush.Color := bmp1.Canvas.Pixels[1,1];
//  btnWinIcon.Glyph.Canvas.CopyMode:= cmMergeCopy;
//  btnWinIcon.Glyph.TransparentColor := bmp1.Canvas.Pixels[1,1];
//  btnWinIcon.Glyph.Canvas.StretchDraw(Rect(0, 0, btnWinIcon.Width, btnWinIcon.Height), bmp1);
//  bmp1.Free;
  btnWinIcon.Glyph.Assign(Icon);
end;

procedure TFrameDefaultTitle.SetSysHint(const SysHint: string);
begin
  lbSystemHint.Caption := SysHint;
end;

procedure TFrameDefaultTitle.SetTitle(const Value: string);
begin
  lbWinCaption.Caption := Value;
end;

procedure TFrameDefaultTitle.SetVersion(const Value: string);
begin
  lbWinVersion.Caption := Value;
end;

procedure TFrameDefaultTitle.ChangeFormState(AForm: TForm);
begin
  case AForm.WindowState of
    wsNormal:
      btnWinMax.ImageIndex := 2;
    wsMinimized:
      btnWinMax.ImageIndex := 1;
    wsMaximized:
      btnWinMax.ImageIndex := 1;
  end;
  //tabWinChildren.TabIndex := tabWinChildren.Tabs.IndexOfObject(AForm);
end;

procedure TFrameDefaultTitle.CloseForm(AForm: TForm);
var
  Idx: Integer;
begin
  if not Assigned(AForm) then Idx := tabWinChildren.TabIndex
  else Idx := tabWinChildren.Tabs.IndexOfObject(AForm);
  if Idx >= 0 then
    tabWinChildren.Tabs.Delete(Idx);
  DisplayWinChildrenBtns;
end;

{$endregion}

function TFrameDefaultTitle.GetFormByIndex(Index: Integer): TForm;
begin
  if Index >= tabWinChildren.Tabs.Count then Result:= nil
  else Result := TForm(tabWinChildren.Tabs.Objects[Index]);
end;

function TFrameDefaultTitle.GetFormByName(UsingName: string): TForm;
var
  Idx: Integer;
begin
  Idx := tabWinChildren.Tabs.IndexOf(UsingName);
  if Idx >= 0 then begin
    Result := TForm(tabWinChildren.Tabs.Objects[Idx]);
    tabWinChildren.TabIndex:= Idx;
  end else Result:= nil;
end;

function TFrameDefaultTitle.GetFormCount: Integer;
begin
  Result:= tabWinChildren.Tabs.Count;
end;

procedure TFrameDefaultTitle.lbWinCaptionDblClick(Sender: TObject);
begin
  case MainForm.WindowState of
    wsNormal:
      MainForm.WindowState := wsMaximized;
    wsMinimized:
      MainForm.WindowState := wsNormal;
    wsMaximized:
      MainForm.WindowState := wsNormal;
  end;
end;

procedure TFrameDefaultTitle.lbWinCaptionMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if MainForm.WindowState = wsMaximized then
    Exit;
  ReleaseCapture;
  MainForm.Perform(WM_SYSCOMMAND, $F012, 0);
end;

procedure TFrameDefaultTitle.SetOpenASystemWindow(
  const Value: TEventToOpenASystemWindow);
begin
  FOpenASystemWindow := Value;
end;

procedure TFrameDefaultTitle.btnWinCascadeClick(Sender: TObject);
begin
  if (MainForm.FormStyle = fsMDIForm) and (MainForm.ClientHandle <> 0) then
    SendMessage(MainForm.ClientHandle, WM_MDICASCADE, 0, 0);
end;

procedure TFrameDefaultTitle.btnWinCloseClick(Sender: TObject);
begin
  if Assigned(MainForm.ActiveMDIChild) then
    MainForm.ActiveMDIChild.Close
  else
    MainForm.Close;
end;

procedure TFrameDefaultTitle.btnWinExitClick(Sender: TObject);
begin
  MainForm.Close;
end;

procedure TFrameDefaultTitle.btnWinAboutClick(Sender: TObject);
begin
  if Assigned(FOpenASystemWindow) then
    FOpenASystemWindow('关于命令', 'ABOUT');
end;

procedure TFrameDefaultTitle.btnWinHelpClick(Sender: TObject);
begin
  if Assigned(FOpenASystemWindow) then
    FOpenASystemWindow('帮助命令', 'HELP');
end;

procedure TFrameDefaultTitle.btnWinHomeClick(Sender: TObject);
begin
  if Assigned(FOpenASystemWindow) then
    FOpenASystemWindow('链接网站', 'WEB');
end;

procedure TFrameDefaultTitle.btnWinHTitleClick(Sender: TObject);
begin
  if (MainForm.FormStyle = fsMDIForm) and (MainForm.ClientHandle <> 0) then
    SendMessage(MainForm.ClientHandle, WM_MDITILE, MDITILE_VERTICAL, 0);
end;

procedure TFrameDefaultTitle.btnWinIconClick(Sender: TObject);
begin
  with btnWinIcon do
    if Assigned(PopupMenu) then
      PopupMenu.Popup(MainForm.Left + Width, MainForm.Top + Height);
end;

procedure TFrameDefaultTitle.btnWinMaxClick(Sender: TObject);
var
  szForm: TForm;
begin
  if Assigned(MainForm.ActiveMDIChild) then
    szForm := MainForm.ActiveMDIChild
  else szForm := MainForm;
  case szForm.WindowState of
    wsNormal:
      szForm.WindowState := wsMaximized;
    wsMinimized:
      szForm.WindowState := wsNormal;
    wsMaximized:
      szForm.WindowState := wsNormal;
  end;
end;

procedure TFrameDefaultTitle.btnWinMinClick(Sender: TObject);
begin
  MainForm.WindowState := wsMinimized;
end;

procedure TFrameDefaultTitle.btnWinVTitleClick(Sender: TObject);
begin
  if (MainForm.FormStyle = fsMDIForm) and (MainForm.ClientHandle <> 0) then
    SendMessage(MainForm.ClientHandle, WM_MDITILE, MDITILE_HORIZONTAL, 0);
end;

procedure TFrameDefaultTitle.tabWinChildrenChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
  DisplayWinChildrenBtns;
  if NewTab < 0 then
    Exit;
  (tabWinChildren.Tabs.Objects[NewTab] as TForm).Show;
end;

procedure TFrameDefaultTitle.tabWinChildrenGetImageIndex(Sender: TObject;
  TabIndex: Integer; var ImageIndex: Integer);
begin
  if TabIndex = tabWinChildren.TabIndex then
    ImageIndex := 3
  else
    ImageIndex := -1;
end;

procedure TFrameDefaultTitle.tabWinChildrenMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  P: TPoint;
  R: TRect;
  Idx: Integer;
begin
  P.X := X;
  P.Y := Y;
  Idx := tabWinChildren.ItemAtPos(P);
  if Idx < 0 then
    Exit;
  if Idx <> tabWinChildren.TabIndex then
    Exit;
  R := tabWinChildren.ItemRect(Idx);
  if (R.Left < X) and (R.Left + 18 > X) then
    (tabWinChildren.Tabs.Objects[Idx] as TForm).Close;
end;

procedure TFrameDefaultTitle.ToolButton2Click(Sender: TObject);
begin
  MainForm.WindowState:= wsMinimized;
end;

procedure TFrameDefaultTitle.DisplayWinChildrenBtns;
begin
  btnWinCascade.Visible := tabWinChildren.Tabs.Count > 1;
  btnWinHTitle.Visible := btnWinCascade.Visible;
  btnWinVTitle.Visible := btnWinCascade.Visible;
  tbSeparator1.Visible := btnWinCascade.Visible;
end;

initialization
  RegBannerComponent('普通标题', TFrameDefaultTitle);

end.