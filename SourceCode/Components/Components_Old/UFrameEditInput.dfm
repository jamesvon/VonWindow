object FrameEditInput: TFrameEditInput
  Left = 0
  Top = 0
  Width = 320
  Height = 23
  TabOrder = 0
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 8
    Height = 23
    Align = alLeft
    Caption = 'lb'
    Layout = tlCenter
    ExplicitHeight = 13
  end
  object SpeedButton1: TSpeedButton
    Left = 297
    Top = 0
    Width = 23
    Height = 23
    Align = alRight
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00303333333333
      333337F3333333333333303333333333333337F33FFFFF3FF3FF303300000300
      300337FF77777F77377330000BBB0333333337777F337F33333330330BB00333
      333337F373F773333333303330033333333337F3377333333333303333333333
      333337F33FFFFF3FF3FF303300000300300337FF77777F77377330000BBB0333
      333337777F337F33333330330BB00333333337F373F773333333303330033333
      333337F3377333333333303333333333333337FFFF3FF3FFF333000003003000
      333377777F77377733330BBB0333333333337F337F33333333330BB003333333
      333373F773333333333330033333333333333773333333333333}
    NumGlyphs = 2
    ExplicitLeft = 280
    ExplicitHeight = 22
  end
  object Edit1: TEdit
    Left = 153
    Top = 0
    Width = 144
    Height = 23
    Align = alClient
    TabOrder = 0
    ExplicitLeft = 56
    ExplicitTop = 48
    ExplicitWidth = 121
    ExplicitHeight = 21
  end
  object DateTimePicker1: TDateTimePicker
    Left = 153
    Top = 0
    Width = 144
    Height = 23
    Align = alClient
    Date = 41925.416863946760000000
    Time = 41925.416863946760000000
    TabOrder = 1
    ExplicitLeft = 152
    ExplicitWidth = 145
  end
  object DateTimePicker2: TDateTimePicker
    Left = 8
    Top = 0
    Width = 145
    Height = 23
    Align = alLeft
    Date = 41925.416863946760000000
    Time = 41925.416863946760000000
    TabOrder = 2
  end
end
