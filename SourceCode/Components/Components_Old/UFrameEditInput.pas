unit UFrameEditInput;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, Buttons, StdCtrls, ComCtrls, UVonConfig;

type
  TFrameEditInput = class(TFrame)
    Label1: TLabel;
    Edit1: TEdit;
    SpeedButton1: TSpeedButton;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
  private
    { Private declarations }
    procedure EventOfIntKeyPress(Sender: TObject);
    procedure EventOfFloatKeyPress(Sender: TObject);
    procedure EventOfGuidButtonClick(Sender: TObject);
  public
    { Public declarations }
  published
    property EditType: TVonParamType;
    property OnButtonClick: TNotifyEvent;
  end;

implementation

{$R *.dfm}

end.
