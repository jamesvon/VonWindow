unit UFrameFieldSetting;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, Grids, UVonConfig, ComCtrls, ToolWin, ImgList;

type
  TFrameFieldSetting = class(TFrame)
    StringGrid1: TStringGrid;
    ComboBox1: TComboBox;
    imgButton: TImageList;
    ToolBar1: TToolBar;
    btnTreeDelete: TToolButton;
    ToolButton1: TToolButton;
    btnTreeMoveToTop: TToolButton;
    btnTreeMoveToUp: TToolButton;
    btnTreeMoveToDown: TToolButton;
    btnTreeMoveToBottom: TToolButton;
    ToolButton3: TToolButton;
    btnTreeImport: TToolButton;
    btnTreeExport: TToolButton;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    lbTitle: TLabel;
    procedure StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure StringGrid1SetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure ComboBox1Exit(Sender: TObject);
    procedure btnTreeDeleteClick(Sender: TObject);
    procedure btnTreeMoveToBottomClick(Sender: TObject);
    procedure btnTreeMoveToTopClick(Sender: TObject);
    procedure btnTreeMoveToUpClick(Sender: TObject);
    procedure btnTreeMoveToDownClick(Sender: TObject);
    procedure btnTreeImportClick(Sender: TObject);
    procedure btnTreeExportClick(Sender: TObject);
  private
    { Private declarations }
    szCol, szRow: Integer;
    FSetting: TVonArraySetting;
    procedure SetSetting(const Value: TVonArraySetting);
    procedure DeleteRow(ARow: Integer);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    property Setting: TVonArraySetting read FSetting write SetSetting;
  end;

implementation

{$R *.dfm}

procedure TFrameFieldSetting.btnTreeDeleteClick(Sender: TObject);
begin
  if StringGrid1.Row >= StringGrid1.RowCount - 1 then Exit;
  DeleteRow(StringGrid1.Row);
end;

procedure TFrameFieldSetting.btnTreeExportClick(Sender: TObject);
begin
  if not Assigned(FSetting) then Exit;
  if not SaveDialog1.Execute then Exit;
  FSetting.SaveToFile(SaveDialog1.FileName);
end;

procedure TFrameFieldSetting.btnTreeImportClick(Sender: TObject);
begin
  if not Assigned(FSetting) then Exit;
  if not OpenDialog1.Execute then Exit;
  FSetting.LoadFromFile(OpenDialog1.FileName);
  SetSetting(FSetting);
end;

procedure TFrameFieldSetting.btnTreeMoveToBottomClick(Sender: TObject);
var
  Idx: Integer;
  S1, S2, S3: string;
begin
  if StringGrid1.Row >= StringGrid1.RowCount - 2 then Exit;
  if Assigned(FSetting) then
    FSetting.Move(StringGrid1.Row - 1, FSetting.Count - 1);
  S1:= StringGrid1.Cells[1, StringGrid1.Row];
  S2:= StringGrid1.Cells[2, StringGrid1.Row];
  S3:= StringGrid1.Cells[3, StringGrid1.Row];
  for Idx := StringGrid1.Row to StringGrid1.RowCount - 3 do begin
    StringGrid1.Cells[1, Idx]:= StringGrid1.Cells[1, Idx + 1];
    StringGrid1.Cells[2, Idx]:= StringGrid1.Cells[2, Idx + 1];
    StringGrid1.Cells[3, Idx]:= StringGrid1.Cells[3, Idx + 1];
  end;
  StringGrid1.Cells[1, StringGrid1.RowCount - 2]:= S1;
  StringGrid1.Cells[2, StringGrid1.RowCount - 2]:= S2;
  StringGrid1.Cells[3, StringGrid1.RowCount - 2]:= S3;
  StringGrid1.Row:= StringGrid1.RowCount - 2;
end;

procedure TFrameFieldSetting.btnTreeMoveToDownClick(Sender: TObject);
var
  S1, S2, S3: string;
begin
  if StringGrid1.Row >= StringGrid1.RowCount - 2 then Exit;
  if Assigned(FSetting) then
    FSetting.Move(StringGrid1.Row - 1, StringGrid1.Row);
  S1:= StringGrid1.Cells[1, StringGrid1.Row];
  S2:= StringGrid1.Cells[2, StringGrid1.Row];
  S3:= StringGrid1.Cells[3, StringGrid1.Row];
  StringGrid1.Cells[1, StringGrid1.Row]:= StringGrid1.Cells[1, StringGrid1.Row + 1];
  StringGrid1.Cells[2, StringGrid1.Row]:= StringGrid1.Cells[2, StringGrid1.Row + 1];
  StringGrid1.Cells[3, StringGrid1.Row]:= StringGrid1.Cells[3, StringGrid1.Row + 1];
  StringGrid1.Cells[1, StringGrid1.Row + 1]:= S1;
  StringGrid1.Cells[2, StringGrid1.Row + 1]:= S2;
  StringGrid1.Cells[3, StringGrid1.Row + 1]:= S3;
  StringGrid1.Row:= StringGrid1.Row + 1;
end;

procedure TFrameFieldSetting.btnTreeMoveToTopClick(Sender: TObject);
var
  Idx: Integer;
  S1, S2, S3: string;
begin
  if StringGrid1.Row < 1 then Exit;
  if Assigned(FSetting) then
    FSetting.Move(StringGrid1.Row - 1, 0);
  S1:= StringGrid1.Cells[1, StringGrid1.Row];
  S2:= StringGrid1.Cells[2, StringGrid1.Row];
  S3:= StringGrid1.Cells[3, StringGrid1.Row];
  for Idx := StringGrid1.Row downto 2 do begin
    StringGrid1.Cells[1, Idx]:= StringGrid1.Cells[1, Idx - 1];
    StringGrid1.Cells[2, Idx]:= StringGrid1.Cells[2, Idx - 1];
    StringGrid1.Cells[3, Idx]:= StringGrid1.Cells[3, Idx - 1];
  end;
  StringGrid1.Cells[1, 1]:= S1;
  StringGrid1.Cells[2, 1]:= S2;
  StringGrid1.Cells[3, 1]:= S3;
  StringGrid1.Row:= 1;
end;

procedure TFrameFieldSetting.btnTreeMoveToUpClick(Sender: TObject);
var
  S1, S2, S3: string;
begin
  if StringGrid1.Row < 1 then Exit;
  if Assigned(FSetting) then
    FSetting.Move(StringGrid1.Row - 1, 0);
  S1:= StringGrid1.Cells[1, StringGrid1.Row];
  S2:= StringGrid1.Cells[2, StringGrid1.Row];
  S3:= StringGrid1.Cells[3, StringGrid1.Row];
  StringGrid1.Cells[1, StringGrid1.Row]:= StringGrid1.Cells[1, StringGrid1.Row - 1];
  StringGrid1.Cells[2, StringGrid1.Row]:= StringGrid1.Cells[2, StringGrid1.Row - 1];
  StringGrid1.Cells[3, StringGrid1.Row]:= StringGrid1.Cells[3, StringGrid1.Row - 1];
  StringGrid1.Cells[1, StringGrid1.Row - 1]:= S1;
  StringGrid1.Cells[2, StringGrid1.Row - 1]:= S2;
  StringGrid1.Cells[3, StringGrid1.Row - 1]:= S3;
  StringGrid1.Row:= StringGrid1.Row - 1;
end;

procedure TFrameFieldSetting.ComboBox1Exit(Sender: TObject);
begin
  StringGrid1.Cells[szCol, szRow]:= ComboBox1.Text;
  if Assigned(FSetting) then begin
    if FSetting.Count > szRow - 1 then
      FSetting.Values[szRow - 1, szCol - 1]:= IntToStr(ComboBox1.ItemIndex)
    else FSetting.AppendRow([StringGrid1.Cells[1, szRow],
      StringGrid1.Cells[2, szRow], StringGrid1.Cells[3, szRow]]);
  end;
end;

constructor TFrameFieldSetting.Create(AOwner: TComponent);
begin
  inherited;
  StringGrid1.Cells[1, 0]:= '参数名称';
  StringGrid1.Cells[2, 0]:= '参数类型';
  StringGrid1.Cells[3, 0]:= '参数说明';
end;

procedure TFrameFieldSetting.DeleteRow(ARow: Integer);
var
  Idx: Integer;
begin
  if Assigned(FSetting) then FSetting.Delete(ARow - 1);
  for Idx := ARow to StringGrid1.RowCount - 2 do begin
    StringGrid1.Cells[0, Idx]:= IntToStr(Idx);
    StringGrid1.Cells[1, Idx]:= StringGrid1.Cells[1, Idx + 1];
    StringGrid1.Cells[2, Idx]:= StringGrid1.Cells[2, Idx + 1];
    StringGrid1.Cells[3, Idx]:= StringGrid1.Cells[3, Idx + 1];
  end;
  if StringGrid1.RowCount > 2 then
    StringGrid1.RowCount:= StringGrid1.RowCount - 1
  else begin
    StringGrid1.Cells[1, 1]:= '';
    StringGrid1.Cells[2, 1]:= '';
    StringGrid1.Cells[3, 1]:= '';
  end;
end;

procedure TFrameFieldSetting.SetSetting(const Value: TVonArraySetting);
var
  Idx: Integer;
begin
  lbTitle.Visible:= lbTitle.Caption <> '';
  FSetting := Value;
  StringGrid1.RowCount:= Value.Count + 2;
  for Idx := 0 to Value.Count - 1 do begin
    StringGrid1.Cells[0, Idx + 1]:= IntToStr(Idx + 1);
    StringGrid1.Cells[1, Idx + 1]:= Value.Values[Idx, 0];
    StringGrid1.Cells[2, Idx + 1]:= Value.Values[Idx, 1];
    StringGrid1.Cells[3, Idx + 1]:= Value.Values[Idx, 2];
  end;
  StringGrid1.Cells[1, StringGrid1.RowCount - 1]:= '';
  StringGrid1.Cells[2, StringGrid1.RowCount - 1]:= '';
  StringGrid1.Cells[3, StringGrid1.RowCount - 1]:= '';
end;

procedure TFrameFieldSetting.StringGrid1SelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if(szRow > 0)and(StringGrid1.Cells[1, szRow] = '')and(szRow < StringGrid1.RowCount - 1)then begin
    DeleteRow(szRow);
    if szRow <= ARow then begin
      ARow:= ARow - 1;
      StringGrid1.MouseToCell(0, 0, ACol, ARow);
    end;
  end;
  if ARow >= StringGrid1.RowCount then Exit;
  if ACol = 2 then begin
    ComboBox1.Visible:= True;
    with StringGrid1.CellRect(ACol, ARow)do begin
      ComboBox1.Left:= StringGrid1.Left + Left + 1;
      ComboBox1.Top:= StringGrid1.Top + Top + 1;
      ComboBox1.Width:= Right - Left;
    end;
    ComboBox1.SetFocus;
    if StringGrid1.Cells[ACol, ARow] <> '' then
      ComboBox1.ItemIndex:= ComboBox1.Items.IndexOf(StringGrid1.Cells[ACol, ARow]);
  end else begin
    ComboBox1.Visible:= False;
    if szRow - 1 < Setting.Count then
      Setting.Values[szRow - 1, szCol - 1]:= StringGrid1.Cells[szCol, szRow];
  end;
  szRow:= ARow; szCol:= ACol;
end;

procedure TFrameFieldSetting.StringGrid1SetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
var
  Idx: Integer;
begin
  if(ACol = 1)and(Value <> '')and(ARow = StringGrid1.RowCount - 1)then
    StringGrid1.RowCount:= StringGrid1.RowCount + 1;
end;

end.
