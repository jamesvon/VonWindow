object FrameFileView: TFrameFileView
  Left = 0
  Top = 0
  Width = 451
  Height = 305
  Align = alClient
  TabOrder = 0
  ExplicitHeight = 304
  object pageContent: TPageControl
    Left = 0
    Top = 0
    Width = 451
    Height = 305
    ActivePage = tsOffice
    Align = alClient
    TabOrder = 0
    ExplicitHeight = 304
    object tsHtml: TTabSheet
      Caption = 'tsHtml'
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object wbPicture: TWebBrowser
        Left = 0
        Top = 0
        Width = 443
        Height = 294
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 418
        ExplicitHeight = 321
        ControlData = {
          4C000000C92D0000631E00000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E126208000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
    end
    object tsRich: TTabSheet
      Caption = 'tsRich'
      ImageIndex = 1
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object tsOffice: TTabSheet
      Caption = 'tsOffice'
      ImageIndex = 2
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object wbTemp: TWebBrowser
        Left = 0
        Top = 0
        Width = 443
        Height = 294
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 418
        ExplicitHeight = 321
        ControlData = {
          4C000000C92D0000631E00000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E126208000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
    end
    object tsText: TTabSheet
      Caption = 'tsText'
      ImageIndex = 3
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object mText: TMemo
        Left = 0
        Top = 0
        Width = 443
        Height = 294
        Align = alClient
        TabOrder = 0
      end
    end
  end
end
