unit UFrameFileView;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  UFrameRichEditor, Vcl.OleCtrls, SHDocVw, Vcl.ComCtrls;

type
  TFrameFileView = class(TFrame)
    pageContent: TPageControl;
    tsHtml: TTabSheet;
    wbPicture: TWebBrowser;
    tsRich: TTabSheet;
    rich: TFrameRichEditor;
    tsOffice: TTabSheet;
    wbTemp: TWebBrowser;
    tsText: TTabSheet;
    mText: TMemo;
  private
    { Private declarations }
    FWebOpended: Boolean;
    procedure CloseOleTemp;
    procedure OpenOfficeFile(Filename: string);
    procedure OpenPictureFile(Filename: string);
    procedure OpenRichFile(Filename: string);
    procedure OpenTextFile(Filename: string);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    procedure ViewFile(Filename: string);
  end;

implementation

{$R *.dfm}

procedure TFrameFileView.CloseOleTemp;
begin
  if FWebOpended then try
    OleVariant(wbTemp.Document).all.FramerControl1.Close;
  except

  end;
  FWebOpended:= False;
end;

procedure TFrameFileView.ViewFile(Filename: string);
var
  ext: string;
begin
  ext:= LowerCase(ExtractFileExt(Filename));
  if Pos(ext, '.xlt.xltx.xls.xlsx.dot.dotx.doc.docx.ppt.pptx') > 0 then
    OpenOfficeFile(Filename)
  else if Pos(ext, '.gif.jpeg.jpg.png.bmp.pdf') > 0 then
    OpenPictureFile(Filename)
  else if Pos(ext, '.rtf') > 0 then
    OpenRichFile(Filename)
  else OpenTextFile(Filename);
end;

constructor TFrameFileView.Create(AOwner: TComponent);
begin
  inherited;
  tsOffice.Show;
  wbTemp.Navigate(ExtractFilePath(Application.ExeName) + 'Data\OleApp.htm');
end;

procedure TFrameFileView.OpenOfficeFile(Filename: string);
begin
  CloseOleTemp;
  tsOffice.Show;
  OleVariant(wbTemp.Document).all.FramerControl1.Open(Filename);
  FWebOpended:= True;
end;

procedure TFrameFileView.OpenPictureFile(Filename: string);
begin
  CloseOleTemp;
  wbPicture.Navigate(Filename);
  tsHtml.Show;
end;

procedure TFrameFileView.OpenRichFile(Filename: string);
begin
  CloseOleTemp;
  rich.Editor.Lines.LoadFromFile(Filename);
  tsRich.Show;
end;

procedure TFrameFileView.OpenTextFile(Filename: string);
begin
  CloseOleTemp;
  mText.Lines.LoadFromFile(FileName);
  tsText.Show;
end;

end.
