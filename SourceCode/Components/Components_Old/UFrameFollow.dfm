object FrameFollow: TFrameFollow
  Left = 0
  Top = 0
  Width = 200
  Height = 37
  TabOrder = 0
  object lbTitle: TLabel
    Left = 0
    Top = 0
    Width = 200
    Height = 13
    Align = alTop
    Alignment = taCenter
    Color = clGradientActiveCaption
    ParentColor = False
    Transparent = False
    ExplicitWidth = 3
  end
  object bgAddition: TButtonGroup
    Left = 0
    Top = 13
    Width = 200
    Height = 24
    Align = alBottom
    BevelInner = bvNone
    BevelOuter = bvNone
    BevelKind = bkTile
    BorderStyle = bsNone
    ButtonOptions = []
    Items = <>
    TabOrder = 0
    OnButtonClicked = bgAdditionButtonClicked
  end
end
