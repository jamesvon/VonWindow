unit UFrameFollow;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, ExtCtrls, ButtonGroup, Math, Buttons, UVonSystemFuns,
  UVonConfig;

const
  DEFAULT_FOLLOW_WIDTH = 200;
  DEFAULT_FOLLOW_HEIGHT = 23;

type
  TMoveEvent = procedure (Sender: TObject; Org, Dest: Integer) of object;
  TSaveEvent = procedure (Sender: TVonFollowItem) of object;

  TFrameFollow = class;
  TFollowField = class(TPanel)
  private
    FChildren: TList;
    FBmp: TImage;
    btnTop: TSpeedButton;
    btnUp: TSpeedButton;
    btnDown: TSpeedButton;
    btnBottom: TSpeedButton;
    btnC_E: TSpeedButton;
    btnEdit: TSpeedButton;
    btnDelete: TSpeedButton;
    FonDelete: TNotifyEvent;
    FonEdit: TNotifyEvent;
    FModuleImages: TImageList;
    FField: TVonFollowItem;
    FOnE_C: TNotifyEvent;
    FOnMove: TMoveEvent;
    FImageIndex: Integer;
    FonResize: TNotifyEvent;
    procedure btnTopClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure btnBottomClick(Sender: TObject);
    procedure btnC_EClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure SetField(const Value: TVonFollowItem);
    procedure SetImageIndex(const Value: Integer);
    procedure SetModuleImages(const Value: TImageList);
    procedure SetonDelete(const Value: TNotifyEvent);
    procedure SetonEdit(const Value: TNotifyEvent);
    procedure SetOnMove(const Value: TMoveEvent);
    function GetChildName: string;
    procedure EventOfResize(Sender: TObject);
    procedure SetonResize(const Value: TNotifyEvent);
    function GetChildCount: Integer;
    function GetChildren(Index: Integer): TFrameFollow;
    function NewFollow(caseValue: string; Index: Integer = -1): TFrameFollow;
    procedure EventOfMouseEnter(Sender: TObject);
    procedure EventOfMouseLeave(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Clear;
    function AddChild(CaseValue: string): TFrameFollow;
    function InsertChild(Index: Integer; CaseValue: string): TFrameFollow;
    procedure RemoveChild(Index: Integer);
    function IndexOfCaseValue(CaseValue: string): Integer;
    property Children[Index: Integer]: TFrameFollow read GetChildren;
  published
    property ChildCount: Integer read GetChildCount;
    property Field: TVonFollowItem read FField write SetField;
    property ImageIndex: Integer read FImageIndex write SetImageIndex;
    property ModuleImages: TImageList read FModuleImages write SetModuleImages;
    property OnEdit: TNotifyEvent read FonEdit write SetonEdit;
    property OnDelete: TNotifyEvent read FonDelete write SetonDelete;
    property OnMove: TMoveEvent read FOnMove write SetOnMove;
    property onResize: TNotifyEvent read FonResize write SetonResize;
  end;

  TFrameFollow = class(TFrame)
    bgAddition: TButtonGroup;
    lbTitle: TLabel;
    procedure bgAdditionButtonClicked(Sender: TObject; Index: Integer);
  private
    FonDelete: TNotifyEvent;
    FonEdit: TNotifyEvent;
    FonAdd: TNotifyEvent;
    FModuleImages: TImageList;
    FFollow: TVonFollow;
    FonResize: TNotifyEvent;
    function GetImages: TImageList;
    procedure SetFollow(const Value: TVonFollow);
    procedure SetImages(const Value: TImageList);
    procedure SetModuleImages(const Value: TImageList);
    procedure SetonAdd(const Value: TNotifyEvent);
    procedure SetonDelete(const Value: TNotifyEvent);
    procedure SetonEdit(const Value: TNotifyEvent);
    procedure SetonResize(const Value: TNotifyEvent);
    procedure SetonSave(const Value: TSaveEvent);
  private
    FonSave: TSaveEvent;
    { Private declarations }
    function GetChildName: string;
    procedure EventOfDelete(Sender: TObject);
    procedure EventOfResize(Sender: TObject);
    function NewField(Index, Kind: Integer): TFollowField;
    procedure EventOfMove(Sender: TObject; Org, Dest: Integer);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function AddField(Kind: Integer): TFollowField;
    function InsertField(Index, Kind: Integer): TFollowField;
    procedure DeleteField(Index: Integer);
    procedure Move(Org, Dest: Integer);
  published
    property Follow: TVonFollow read FFollow write SetFollow;
    property onEdit: TNotifyEvent read FonEdit write SetonEdit;
    property onDelete: TNotifyEvent read FonDelete write SetonDelete;
    property onAdd: TNotifyEvent read FonAdd write SetonAdd;
    property onSave: TSaveEvent read FonSave write SetonSave;
    property onResize: TNotifyEvent read FonResize write SetonResize;
    property BarImages: TImageList read GetImages write SetImages;
    property ModuleImages: TImageList read FModuleImages write SetModuleImages;
  end;

implementation

var R, G, B, GrandRate: byte;
  RR, BR, GR: integer;

function GetGrandColor: TColor;
  function GetColor(CR: Byte; var VR: integer): Byte;
  var valueRatio: Double;
  begin
      if(CR + VR > 255)or(CR + VR < 0)then VR:= - VR;
      Result:= Round(CR + VR);
  end;
begin
  if R = G then G:= G + GrandRate div 3;
  if B = G then B:= B + GrandRate div 3;
  if B = R then R:= R + GrandRate div 3;
  R:= GetColor(R, RR);
  B:= GetColor(B, BR);
  G:= GetColor(G, GR);
  Result:= RGB(R, G, B);
end;

{$R *.dfm}

{ TFrameFollow }

constructor TFrameFollow.Create(AOwner: TComponent);
begin
  inherited;
  FFollow:= TVonFollow.Create;
end;

destructor TFrameFollow.Destroy;
begin
  FFollow.Free;
  inherited;
end;

procedure TFrameFollow.bgAdditionButtonClicked(Sender: TObject;
  Index: Integer);
begin     //
  AddField(Index);
end;

{ TFrameFollow Methods }

function TFrameFollow.AddField(Kind: Integer): TFollowField;
begin
  NewField(Follow.Count, Kind);
end;

procedure TFrameFollow.DeleteField(Index: Integer);
begin
  if Assigned(FonDelete) then FonDelete(FFollow.Items[Index]);   //存储Info信息
  Height:= Height - TFollowField(FFollow.Items[Index].Obj).Height;
  Follow.Delete(Index);
  EventOfResize(Self);
end;

function TFrameFollow.InsertField(Index, Kind: Integer): TFollowField;
begin
  NewField(Index, Kind);
end;

procedure TFrameFollow.Move(Org, Dest: Integer);
begin
  EventOfMove(Self, Org, Dest);
end;

function TFrameFollow.NewField(Index, Kind: Integer): TFollowField;
var
  newField: TVonFollowItem;
  newChild: TFollowField;
begin
  //完成Info的添加
  newField:= TVonFollowItem.Create;
  newField.Data:= Pointer(Kind);
  newField.ID:= 0;
  newField.Key:= '';
  newChild:= TFollowField.Create(Self);
  newField.Obj:= newChild;
  if Index = Follow.Count then Follow.Add(newField)
  else Follow.Insert(Index, newField);
  if Assigned(FonAdd) then FonAdd(newField.Obj);   //存储Info信息
  //开始设置显示组件信息
  newChild.Parent:= Self;
  newChild.Name:= GetChildName;
  newChild.Top:= bgAddition.Top;
//  newChild.Align:= alTop;
  newChild.ImageIndex:= Kind;
  newChild.ModuleImages:= FModuleImages;
  newChild.OnEdit:= FonEdit;
  newChild.OnMove:= EventOfMove;
  newChild.OnDelete:= EventOfDelete;
  newChild.onResize:= EventOfResize;
  newChild.Field:= newField;
  case Kind of
    6: begin newChild.AddChild('True'); newChild.AddChild('False'); end;
    7: begin newChild.AddChild('1'); newChild.AddChild('2'); newChild.AddChild('3'); end;
  end;
  if Assigned(FonSave) then FonSave(newField);
  newChild.EventOfResize(nil);
end;

{ TFrameFollow Events }

procedure TFrameFollow.EventOfDelete(Sender: TObject);
begin
  DeleteField(FFollow.IndexOfObj(Sender));
end;

procedure TFrameFollow.EventOfMove(Sender: TObject; Org, Dest: Integer);
  procedure MoveField(F, E: Integer);
  var
    I, Z: Integer;
  begin
    if lbTitle.Visible then Z:= lbTitle.Height else Z:= 0;
    if F <> 0 then with TFollowField(FFollow.Items[F - 1].Obj)do
      Z:= Top + Height;
    for I := F to E do
      with TFollowField(FFollow.Items[I].Obj)do begin
        Top:= Z;
        Inc(Z, Height);
        if Assigned(FonSave) then FonSave(FFollow.Items[I]);
    end;
  end;
begin
  if(Org < 0)or(Org >= FFollow.Count)or(Dest < 0)or(Dest >= FFollow.Count) then Exit;
  FFollow.Move(Org, Dest);
  if Org > Dest then MoveField(Dest, Org)
  else MoveField(Org, Dest);
end;

procedure TFrameFollow.EventOfResize(Sender: TObject);
var
  I, H, W: Integer;
begin
  if lbTitle.Visible then H:= lbTitle.Height
  else H:= 0;
  W:= DEFAULT_FOLLOW_WIDTH;
  for I := 0 to FFollow.Count - 1 do begin
    TFollowField(FFollow.Items[I].Obj).Top:= H;
    Inc(H, TFollowField(FFollow.Items[I].Obj).Height);
    W:= Max(W, TFollowField(FFollow.Items[I].Obj).Width);
  end;
  Inc(H, bgAddition.Height);
  Height:= H;
  Width:= W;
  if Assigned(FonResize) then FonResize(self);
end;

function TFrameFollow.GetChildName: string;
var
  Idx: Integer;
begin
  Idx:= 1;
  while Assigned(FindComponent('EField' + IntToStr(Idx))) do Inc(Idx);
  Result:= 'EField' + IntToStr(Idx);
end;

{ TFrameFollow Properties }

function TFrameFollow.GetImages: TImageList;
begin
  Result:= TImageList(bgAddition.Images);
end;

procedure TFrameFollow.SetFollow(const Value: TVonFollow);
begin
  FFollow := Value;
end;

procedure TFrameFollow.SetImages(const Value: TImageList);
var
  Idx: Integer;
begin
  bgAddition.Images:= Value;
  for Idx := 0 to Value.Count - 1 do
    bgAddition.Items.Add.ImageIndex:= Idx;
end;

procedure TFrameFollow.SetModuleImages(const Value: TImageList);
begin
  FModuleImages := Value;
end;

procedure TFrameFollow.SetonAdd(const Value: TNotifyEvent);
begin
  FonAdd := Value;
end;

procedure TFrameFollow.SetonDelete(const Value: TNotifyEvent);
begin
  FonDelete := Value;
end;

procedure TFrameFollow.SetonEdit(const Value: TNotifyEvent);
var
  I: Integer;
begin
  FonEdit := Value;
  for I := 0 to FFollow.Count - 1 do
    TFollowField(FFollow.Items[I].Obj).OnEdit:= Value;
end;

procedure TFrameFollow.SetonResize(const Value: TNotifyEvent);
begin
  FonResize := Value;
end;

procedure TFrameFollow.SetonSave(const Value: TSaveEvent);
begin
  FonSave := Value;
end;

{ TFollowField }

constructor TFollowField.Create(AOwner: TComponent);
  function AddBtn(btn: TSpeedButton; ALeft: Integer; ImageName: string; Method: TNotifyEvent): TSpeedButton;
  begin
    btn.Parent:= Self;
    btn.Top:= 0;
    btn.Left:= ALeft;
    btn.Name:= GetChildName;
    btn.Glyph.Handle:= GetResBitmap(ImageName);
    btn.NumGlyphs:= 2;
    btn.OnClick:= Method;
    Result:= btn;
    Result.OnMouseEnter:= EventOfMouseEnter;
    Result.OnMouseLeave:= EventOfMouseLeave;
  end;
begin
  inherited;
  FChildren:= TList.Create;
  Width:= DEFAULT_FOLLOW_WIDTH;
  Height:= DEFAULT_FOLLOW_HEIGHT;
  OnMouseEnter:= EventOfMouseEnter;
  OnMouseLeave:= EventOfMouseLeave;
  ParentBackground:= False;
  Color:= GetGrandColor;
  ShowCaption:= False;
  // Create buttons of field bar
  btnTop:= AddBtn(TSpeedButton.Create(self), 23, 'TOP', btnTopClick);
  btnUp:= AddBtn(TSpeedButton.Create(self), 46, 'UP', btnUpClick);
  btnDown:= AddBtn(TSpeedButton.Create(self), 69, 'DOWN', btnDownClick);
  btnBottom:= AddBtn(TSpeedButton.Create(self), 92, 'BOTTOM', btnBottomClick);
  btnC_E:= AddBtn(TSpeedButton.Create(self), 0, 'COLLAPSE', btnC_EClick);
  btnEdit:= AddBtn(TSpeedButton.Create(self), 150, 'EDIT', btnEditClick);
  btnDelete:= AddBtn(TSpeedButton.Create(self), 177, 'DELETE', btnDeleteClick);
  // Create image to display follow icon
  FBmp:= TImage.Create(Self);
  FBmp.Parent:= Self;
  FBmp.Top:= 0;
  FBmp.Left:= 116;
  FBmp.Height:= DEFAULT_FOLLOW_HEIGHT;
  FBMp.Center:= True;
  FBMp.Transparent:= True;
  FBmp.OnClick:= btnEditClick;
  FBmp.OnMouseEnter:= EventOfMouseEnter;
  FBmp.OnMouseLeave:= EventOfMouseLeave;
  btnC_E.Visible:= False;
end;

destructor TFollowField.Destroy;
begin
  Clear;
  FChildren.Free;
  btnTop.Free;
  btnUp.Free;
  btnDown.Free;
  btnBottom.Free;
  btnC_E.Free;
  btnEdit.Free;
  btnDelete.Free;
  FBmp.Free;
  inherited;
end;

procedure TFollowField.Clear;
begin
  while FChildren.Count > 0 do begin
    TFrameFollow(FChildren[0]).Free;
    FChildren.Delete(0);
  end;
end;

procedure TFollowField.btnC_EClick(Sender: TObject);
var
  I, H: Integer;
  Display: Boolean;
begin
  H:= 0;
  Display:= Height = DEFAULT_FOLLOW_HEIGHT;
  if Display then begin
    btnC_E.Glyph.Handle:= GetResBitmap('COLLAPSE');
    EventOfResize(Self);
  end else begin
    btnC_E.Glyph.Handle:= GetResBitmap('EXPAND');
    Width:= DEFAULT_FOLLOW_WIDTH;
    Height:= DEFAULT_FOLLOW_HEIGHT;
    btnEdit.Left:= Width - 49;
    btnDelete.Left:= Width - 23;
    FBmp.Width:= Width - 164;
    if Assigned(FonResize) then FonResize(Self);
  end;
end;

procedure TFollowField.btnDeleteClick(Sender: TObject);
begin
  if Assigned(FonDelete) then FonDelete(Self);
end;

procedure TFollowField.btnEditClick(Sender: TObject);
begin
  if Assigned(FonEdit) then FonEdit(Self);
end;

{ TFollowField Methods of moving }

procedure TFollowField.btnBottomClick(Sender: TObject);
var
  Idx: Integer;
begin
  if Assigned(FOnMove) then begin
    Idx:= FField.Parent.IndexOfObj(Self);
    FOnMove(self, Idx, FField.Parent.Count - 1);
  end;
end;

procedure TFollowField.btnDownClick(Sender: TObject);
var
  Idx: Integer;
begin
  if Assigned(FOnMove) then begin
    Idx:= FField.Parent.IndexOfObj(Self);
    FOnMove(self, Idx, Idx + 1);
  end;
end;

procedure TFollowField.btnTopClick(Sender: TObject);
var
  Idx: Integer;
begin
  if Assigned(FOnMove) then begin
    Idx:= FField.Parent.IndexOfObj(Self);
    FOnMove(self, Idx, 0);
  end;
end;

procedure TFollowField.btnUpClick(Sender: TObject);
var
  Idx: Integer;
begin
  if Assigned(FOnMove) then begin
    Idx:= FField.Parent.IndexOfObj(Self);
    FOnMove(self, Idx, Idx - 1);
  end;
end;

procedure TFollowField.EventOfMouseEnter(Sender: TObject);
begin
  //Width:= DEFAULT_FOLLOW_WIDTH;
end;

procedure TFollowField.EventOfMouseLeave(Sender: TObject);
begin
  //Width:= 100;
end;

procedure TFollowField.EventOfResize(Sender: TObject);
var
  I, H, W: Integer;
begin
  H:= 0;
  W:= 0;
  for I := 0 to FChildren.Count - 1 do begin
    H:= Max(H, TFrameFollow(FChildren[I]).Height);
    TFrameFollow(FChildren[I]).Left:= W;
    Inc(W, TFrameFollow(FChildren[I]).Width);
  end;
  Height:= H + DEFAULT_FOLLOW_HEIGHT;
  Width:= Max(W, DEFAULT_FOLLOW_WIDTH);
  btnEdit.Left:= Width - 49;
  btnDelete.Left:= Width - 23;
  FBmp.Width:= Width - 164;
  if Assigned(FonResize) then FonResize(Self);
end;

{ TFollowField Methods of Children }

function TFollowField.AddChild(CaseValue: string): TFrameFollow;
begin
  Result:= NewFollow(CaseValue);
end;

function TFollowField.GetChildCount: Integer;
begin
  Result:= FChildren.Count;
end;

function TFollowField.GetChildName: string;
var
  Idx: Integer;
begin
  Idx:= 1;
  while Assigned(FindComponent('EField' + IntToStr(Idx))) do Inc(Idx);
  Result:= 'EField' + IntToStr(Idx);
end;

function TFollowField.GetChildren(Index: Integer): TFrameFollow;
begin
  Result:= TFrameFollow(FChildren[Index]);
end;

function TFollowField.IndexOfCaseValue(CaseValue: string): Integer;
var
  I: Integer;
begin
  Result:= -1;
  for I := 0 to FChildren.Count - 1 do
    if TFrameFollow(FChildren[I]).lbTitle.Caption = CaseValue then begin
      Result:= I;
      Exit;
    end;
end;

function TFollowField.InsertChild(Index: Integer;
  CaseValue: string): TFrameFollow;
begin
  Result:= NewFollow(CaseValue, Index);
end;

function TFollowField.NewFollow(caseValue: string; Index: Integer): TFrameFollow;
begin
  Result:= TFrameFollow.Create(Self);
  Result.ParentBackground:= False;
//  Result.Color:= GetGrandColor;
  Result.Name:= GetChildName;
  Result.LbTitle.Caption:= caseValue;
  Result.ModuleImages:= TFrameFollow(Parent).ModuleImages;
  Result.BarImages:= TFrameFollow(Parent).BarImages;
  if Index < 0 then FChildren.Add(Pointer(Result))
  else FChildren.Insert(Index, Pointer(Result));
  Result.Parent:= Self;
  Result.Top:= DEFAULT_FOLLOW_HEIGHT + 1;
  Result.Height:= 37;
  Result.Width:= DEFAULT_FOLLOW_WIDTH;
  Result.OnEdit:= FonEdit;
  Result.onResize:= EventOfResize;
  btnC_E.Visible:= True;
end;

procedure TFollowField.RemoveChild(Index: Integer);
begin
  TFrameFollow(FChildren[Index]).Free;
  FChildren.Delete(Index);
  EventOfResize(self);
  btnC_E.Visible:= FChildren.Count > 0;
end;

procedure TFollowField.SetField(const Value: TVonFollowItem);
begin
  FField:= Value;
end;

procedure TFollowField.SetImageIndex(const Value: Integer);
begin
  FImageIndex := Value;
  if Assigned(FModuleImages) then
    FModuleImages.GetBitmap(FImageIndex, FBMp.Picture.Bitmap);
end;

procedure TFollowField.SetModuleImages(const Value: TImageList);
begin
  FModuleImages := Value;
  FModuleImages.GetBitmap(FImageIndex, FBMp.Picture.Bitmap);
end;

procedure TFollowField.SetonDelete(const Value: TNotifyEvent);
begin
  FonDelete:= Value;
end;

procedure TFollowField.SetonEdit(const Value: TNotifyEvent);
begin
  FonEdit:= Value;
end;

procedure TFollowField.SetOnMove(const Value: TMoveEvent);
begin
  FonMove:= Value;
end;

procedure TFollowField.SetonResize(const Value: TNotifyEvent);
begin
  FonResize := Value;
end;

initialization
R:= 127; G:= 127; B:= 127; GrandRate:= 27; RR:= 27; BR:= 27; GR:= 27;

end.
