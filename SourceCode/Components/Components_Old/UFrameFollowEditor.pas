unit UFrameFollowEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  ExtCtrls, Menus, Dialogs, ImgList, UVonSystemFuns, UDlgListSelection, jpeg;

type
  TFollowField = class;
  EDrawLinkerKind = (DLK_DRAW, DLK_CLEAR);
  EDrawLinkerKinds = set of EDrawLinkerKind;
  TEventDrawLinker = procedure (HighField, LowerField: TFollowField; Kinds: EDrawLinkerKinds) of object;

  TFrameFollowEditor = class(TFrame)
    ScrollBox1: TScrollBox;
    imgMenu: TImageList;
    pmItem: TPopupMenu;
    Method1: TMenuItem;
    If1: TMenuItem;
    Sign1: TMenuItem;
    Case1: TMenuItem;
    Loop1: TMenuItem;
    Import1: TMenuItem;
    imgField: TImageList;
    imgBack: TImage;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    procedure Method1Click(Sender: TObject);
    procedure pmItemPopup(Sender: TObject);
    procedure If1Click(Sender: TObject);
    procedure Sign1Click(Sender: TObject);
    procedure Case1Click(Sender: TObject);
    procedure Loop1Click(Sender: TObject);
    procedure Import1Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
  private
    { Private declarations }
    FCurrentField: TFollowField;
    procedure EventOnGetImage(Sender: TObject);
    procedure EventOnDrawLinker(HighField, LowerField: TFollowField; Kinds: EDrawLinkerKinds);
  public
    { Public declarations }
    /// <summary>构造函数，创建下级节点列表</summary>
    constructor Create(AOwner: TComponent); override;
    /// <summary>析构函数</summary>
    destructor Destroy; override;
  end;

  TFollowField = class(TImage)
  private
    FOwner: TComponent;
    FChildren: TList;          //caseValue-childObj
    FDown: Boolean;
    FOldPointer: TPoint;
    FOldPosition: TPoint;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure btnDeleteClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnBottomClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure btnTopClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    function GetFieldName: string;
    function Append(AKind: Integer): TFollowField;
    function Insert(AKind: Integer): TFollowField;
  private
    FHint: string;
    FCaseValue: string;
    FMaxChildCount: Integer;
    FKindImages: TCustomImageList;
    FKind: Integer;
    FParentField: TFollowField;
    FOnDelete: TNotifyEvent;
    FOnEdit: TNotifyEvent;
    FOnGetImage: TNotifyEvent;
    FOnDraw: TEventDrawLinker;
    FCaseVaue: string;
    FOnNewChild: TNotifyEvent;
    FID: Integer;
    procedure SetHint(const Value: string);
    procedure SetKind(const Value: Integer);
    procedure SetMaxChildCount(const Value: Integer);
    procedure SetParentField(const Value: TFollowField);
    procedure SetSupportMenu(const Value: TPopupMenu);
    procedure SetKindImages(const Value: TCustomImageList);
    procedure SetOnDelete(const Value: TNotifyEvent);
    procedure SetOnEdit(const Value: TNotifyEvent);
    procedure SetOnGetImage(const Value: TNotifyEvent);
    procedure SetOnDraw(const Value: TEventDrawLinker);
    procedure SetCaseValue(const Value: string);
    procedure SetOnNewChild(const Value: TNotifyEvent);
    function GetSupportMenu: TPopupMenu;
    procedure SetID(const Value: Integer);
  public
    /// <summary>构造函数，创建下级节点列表</summary>
    constructor Create(AOwner: TComponent); override;
    /// <summary>析构函数</summary>
    destructor Destroy; override;
    procedure RedrawLinker;
    function AddSibling(AKind: Integer): TFollowField;
    function AddChild(ACaseValue: string; AKind: Integer): TFollowField;
//    function InsertChild(CaseValue: string): TFollowField;
//    /// <summary>清除所有子节点，调用数据库删除命令</summary>
//    procedure RemoveAll;
//    procedure RemoveChild(Index: Integer);
//    function IndexOfCaseValue(CaseValue: string): Integer;
//    property Children[Index: Integer]: TFollowField read GetChildren;
  published
    property ID: Integer read FID write SetID;
    property Kind: Integer read FKind write SetKind;
    property Hint: string read FHint write SetHint;
    property CaseValue: string read FCaseValue write SetCaseValue;
    property ParentField: TFollowField read FParentField write SetParentField;
    /// <summary>最大的分支连接数，0表示无数量限制，默认是-1，即表示没有分支</summary>
    property MaxChildCount: Integer read FMaxChildCount write SetMaxChildCount;
    property SupportMenu: TPopupMenu read GetSupportMenu write SetSupportMenu;
    property KindImages: TCustomImageList read FKindImages write SetKindImages;
    property OnGetImage: TNotifyEvent read FOnGetImage write SetOnGetImage;
    property OnNewChild: TNotifyEvent read FOnNewChild write SetOnNewChild;
    property OnEdit: TNotifyEvent read FOnEdit write SetOnEdit;
    property OnDelete: TNotifyEvent read FOnDelete write SetOnDelete;
    property OnDraw: TEventDrawLinker read FOnDraw write SetOnDraw;
  end;

implementation

{$R VonFollowField.res}
{$R *.dfm}

{ TFollowField }

constructor TFollowField.Create(AOwner: TComponent);
begin
  inherited;
  FOwner:= AOwner;
  FMaxChildCount:= -1;
  Self.Name:= GetFieldName;
  FChildren:= TList.Create;
  Kind:= -1;  //New
  Picture.Bitmap.Handle:= GetResBitmap('NONE_FOLLOW');
end;

destructor TFollowField.Destroy;
begin
  FChildren.Free;
  inherited;
end;

function TFollowField.GetFieldName: string;
var
  Idx: Integer;
begin
  Idx:= 1;
  while Assigned(FOwner.FindComponent('EField' + IntToStr(Idx))) do Inc(Idx);
  Result:= 'EField' + IntToStr(Idx);
end;

function TFollowField.GetSupportMenu: TPopupMenu;
begin
  Result:= PopupMenu;
end;

{ TFollowField Methods of moving }

procedure TFollowField.btnDeleteClick(Sender: TObject);
begin
  if Assigned(FonDelete) then FonDelete(Self);
end;

procedure TFollowField.btnEditClick(Sender: TObject);
begin
  if Assigned(FonEdit) then FonEdit(Self);
end;

function TFollowField.AddChild(ACaseValue: string;
  AKind: Integer): TFollowField;
begin
  Result:= TFollowField.Create(FOwner);
  Result.AutoSize:= True;
  Result.Parent:= Parent;
  Result.OnGetImage:= FOnGetImage;
  Result.OnDraw:= FOnDraw;
  Result.SupportMenu:= PopupMenu;
  Result.Kind:= AKind;
  Result.CaseValue:= ACaseValue;
  Result.Top:= Self.Top + FChildren.Count * 32;
  Result.Left:= Self.Left - Result.Width - 32;
  Result.FOldPosition:= Point(Result.Left, Result.Top);
  Result.ParentField:= Self;
  Self.FOldPosition:= Point(Self.Left, Self.Top);
end;

function TFollowField.AddSibling(AKind: Integer): TFollowField;
begin
  if FKind = -1 then Result:= Insert(AKind)
  else Result:= Append(AKind);
end;

function TFollowField.Insert(AKind: Integer): TFollowField;
var
  szField: TFollowField;
begin
  Result:= TFollowField.Create(FOwner);                      //Copy self to newField
  Result.AutoSize:= True;
  Result.Parent:= Parent;
  Result.OnGetImage:= FOnGetImage;
  Result.OnDraw:= FOnDraw;
  Result.SupportMenu:= PopupMenu;
  Result.Kind:= AKind;
  Result.Top:= Top;
  Result.Left:= Left;
  Result.FOldPosition:= Point(Result.Left, Result.Top);
  Result.CaseValue:= CaseValue;
  CaseValue:= '';
  szField:= ParentField;
  Self.ParentField:= Result;
  Result.ParentField:= szField;
  Top:= Top + Result.Height + 32;
  RedrawLinker;
end;

function TFollowField.Append(AKind: Integer): TFollowField;
begin
  Result:= TFollowField.Create(FOwner);                      //Copy self to newField
  Result.AutoSize:= True;
  Result.Parent:= Parent;
  Result.OnGetImage:= FOnGetImage;
  Result.OnDraw:= FOnDraw;
  Result.SupportMenu:= PopupMenu;
  Result.Kind:= AKind;
  Result.Top:= Top + Result.Height + 32;
  Result.Left:= Left;
  Result.FOldPosition:= Point(Result.Left, Result.Top);
  if Self.FChildren.Count > 0 then
    TFollowField(Self.FChildren[0]).ParentField:= Result;
  Result.ParentField:= Self;
  RedrawLinker;
end;

procedure TFollowField.btnBottomClick(Sender: TObject);
var
  Idx: Integer;
begin
//  if Assigned(FOnMove) then begin
//    Idx:= Inputor.FItems.IndexOf(Pointer(Self));
//    FOnMove(Idx, Inputor.FItems.Count - 1);
//  end;
end;

procedure TFollowField.btnDownClick(Sender: TObject);
var
  Idx: Integer;
begin
//  if Assigned(FOnMove) then begin
//    Idx:= Inputor.FItems.IndexOf(Pointer(Self));
//    FOnMove(Idx, Idx + 1);
//  end;
end;

procedure TFollowField.btnTopClick(Sender: TObject);
var
  Idx: Integer;
begin
//  if Assigned(FOnMove) then begin
//    Idx:= Inputor.FItems.IndexOf(Pointer(Self));
//    FOnMove(Idx, 0);
//  end;
end;

procedure TFollowField.btnUpClick(Sender: TObject);
var
  Idx: Integer;
begin
//  if Assigned(FOnMove) then begin
//    Idx:= Inputor.FItems.IndexOf(Pointer(Self));
//    FOnMove(Idx, Idx - 1);
//  end;
end;

(* Methods of move by mouse *)

procedure TFollowField.MouseDown(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  inherited;
  FDown := True;
  FOldPointer := Point(X, Y);
  FOldPosition := Point(Left, Top);
  Paint;
end;

procedure TFollowField.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  NewPoint: TPoint;

  function findnearest(X, Y: Integer): TPoint;
  begin
    Result.X := (X div 5) * 5 + Round((X mod 5) / 5) * 5;
    Result.Y := (Y div 5) * 5 + Round((Y mod 5) / 5) * 5;
  end;

begin
  inherited;
  if FDown then
  begin
    NewPoint := findnearest(Left - FOldPointer.X + X, Top - FOldPointer.Y + Y);
    with Self do
      SetBounds(NewPoint.X, NewPoint.Y, Width, height);
    Paint;
  end;
end;

procedure TFollowField.MouseUp(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  inherited;
  FDown := False;
  RedrawLinker;
  FOldPosition := Point(Left, Top);
end;

procedure TFollowField.RedrawLinker;
var
  I: Integer;
begin
  if not Assigned(FOnDraw) then Exit;
  if Assigned(Self.ParentField) then
    FOnDraw(Self.ParentField, Self, [DLK_CLEAR, DLK_DRAW]);
  for I := 0 to FChildren.Count - 1 do
    FOnDraw(Self, TFollowField(FChildren[I]), [DLK_CLEAR, DLK_DRAW]);
  FOldPosition:= Point(Left, Top);
end;

(* Properties *)

procedure TFollowField.SetCaseValue(const Value: string);
begin
  FCaseValue := Value;
end;

procedure TFollowField.SetHint(const Value: string);
begin
  FHint := Value;
end;

procedure TFollowField.SetID(const Value: Integer);
begin
  FID := Value;
end;

procedure TFollowField.SetKind(const Value: Integer);
begin
  FKind := Value;
  if Assigned(FOnGetImage) then
    FOnGetImage(Self);
end;

procedure TFollowField.SetKindImages(const Value: TCustomImageList);
begin
  FKindImages := Value;
end;

procedure TFollowField.SetMaxChildCount(const Value: Integer);
begin
  FMaxChildCount := Value;
end;

procedure TFollowField.SetonDelete(const Value: TNotifyEvent);
begin
  FonDelete := Value;
end;

procedure TFollowField.SetOnDraw(const Value: TEventDrawLinker);
begin
  FOnDraw := Value;
end;

procedure TFollowField.SetonEdit(const Value: TNotifyEvent);
begin
  FonEdit := Value;
end;

procedure TFollowField.SetOnGetImage(const Value: TNotifyEvent);
begin
  FOnGetImage := Value;
end;

procedure TFollowField.SetOnNewChild(const Value: TNotifyEvent);
begin
  FOnNewChild := Value;
end;

procedure TFollowField.SetParentField(const Value: TFollowField);
begin
  if Assigned(FParentField) then begin
    if Assigned(FOnDraw) then FOnDraw(FParentField, Self, [DLK_CLEAR]);       //清除 OrgChild 连接线
    FParentField.FChildren.Remove(Pointer(Self));
  end;
  if Assigned(Value)then begin
    Value.FChildren.Add(Pointer(Self));
    if Assigned(FOnDraw) then FOnDraw(Value, Self, [DLK_DRAW]);                //清除 OrgChild 连接线
  end;
  FParentField := Value;
end;

procedure TFollowField.SetSupportMenu(const Value: TPopupMenu);
begin
  PopupMenu := Value;
  Kind:= FKind;
end;

{ TFrameFollowEditor }

constructor TFrameFollowEditor.Create(AOwner: TComponent);
begin
  inherited;
  with TFollowField.Create(Self)do begin
    AutoSize:= True;
    Parent:= ScrollBox1;
    Left:= 0;
    Top:= 0;
    SupportMenu:= pmItem;
    OnGetImage:= EventOnGetImage;
    OnDraw:= EventOnDrawLinker;
    with imgBack.Picture.Bitmap do begin
      Width:= 1024;//Self.Width;
      Height:= 1024;//Self.Height;
      Canvas.Brush.Color:= clGreen;
      Canvas.FillRect(Rect(0, 0, Width, Height));
    end;
  end;
end;

destructor TFrameFollowEditor.Destroy;
begin

  inherited;
end;

procedure TFrameFollowEditor.EventOnDrawLinker(HighField, LowerField: TFollowField; Kinds: EDrawLinkerKinds);
var
  I: Integer;
  procedure Line(X1, Y1, X2, Y2: Integer; Color: TColor);
  begin
      imgBack.Picture.Bitmap.Canvas.Pen.Color:= Color;
      imgBack.Picture.Bitmap.Canvas.MoveTo(X1, Y1);
      imgBack.Picture.Bitmap.Canvas.LineTo(X2, Y2);
      imgBack.Picture.Bitmap.Canvas.Font.Color:= Color;
      imgBack.Picture.Bitmap.Canvas.TextOut((X2 + X1) div 2, (Y1 + Y2) div 2, LowerField.CaseValue);
  end;
begin
  if DLK_CLEAR in Kinds then
    Line((HighField.Width div 2) + HighField.FOldPosition.X, (HighField.Height div 2) + HighField.FOldPosition.Y,
      (LowerField.Width div 2) + LowerField.FOldPosition.X, (LowerField.Height div 2) + LowerField.FOldPosition.Y, clGreen);
  if DLK_DRAW in Kinds then begin
    if LowerField.CaseValue = '' then
      Line((HighField.Width div 2) + HighField.Left, (HighField.Height div 2) + HighField.Top,
        (LowerField.Width div 2) + LowerField.Left, (LowerField.Height div 2) + LowerField.Top, clBlack)
    else
      Line((HighField.Width div 2) + HighField.Left, (HighField.Height div 2) + HighField.Top,
        (LowerField.Width div 2) + LowerField.Left, (LowerField.Height div 2) + LowerField.Top, clBlue);
  end;
end;

procedure TFrameFollowEditor.EventOnGetImage(Sender: TObject);
begin
  with TFollowField(Sender) do
    case Kind of
    -1: Picture.Bitmap.Handle:= GetResBitmap('NONE_FOLLOW');
    0 : Picture.Bitmap.Handle:= GetResBitmap('METHOD_FOLLOW');
    1 : Picture.Bitmap.Handle:= GetResBitmap('IF_FOLLOW');
    2 : Picture.Bitmap.Handle:= GetResBitmap('SIGN_FOLLOW');
    3 : Picture.Bitmap.Handle:= GetResBitmap('CASE_FOLLOW');
    4 : Picture.Bitmap.Handle:= GetResBitmap('LOOP_FOLLOW');
    end;
end;

procedure TFrameFollowEditor.pmItemPopup(Sender: TObject);
begin
  FCurrentField:= TFollowField(TPopupMenu(Sender).PopupComponent);
  FCurrentField.PopupMenu.Items[9].Visible:= (FCurrentField.FMaxChildCount = 0)or(FCurrentField.FMaxChildCount >= FCurrentField.FChildren.Count);
  FCurrentField.PopupMenu.Items[10].Visible:= FCurrentField.PopupMenu.Items[9].Visible;
end;

procedure TFrameFollowEditor.If1Click(Sender: TObject);
begin
  with FCurrentField.AddSibling(1) do begin
    MaxChildCount:= 2;
    AddChild('TRUE', -1);
    AddChild('FALSE', -1);
  end;
end;

procedure TFrameFollowEditor.Case1Click(Sender: TObject);
begin
  with FCurrentField.AddSibling(3) do begin
    MaxChildCount:= 0;
    AddChild('else', -1);
  end;
end;

procedure TFrameFollowEditor.Import1Click(Sender: TObject);
begin
  FCurrentField.AddSibling(5);
end;

procedure TFrameFollowEditor.Loop1Click(Sender: TObject);
begin
  with FCurrentField.AddSibling(4) do begin
    MaxChildCount:= 1;
    AddChild('loop', -1);
  end;
end;

procedure TFrameFollowEditor.Method1Click(Sender: TObject);
begin
  FCurrentField.AddSibling(0);
end;

procedure TFrameFollowEditor.N11Click(Sender: TObject);
var
  S: string;
begin
  if InputQuery('录入', '转入条件', S) then
    FCurrentField.AddChild(S, FCurrentField.FKind);
end;

procedure TFrameFollowEditor.Sign1Click(Sender: TObject);
begin
  with FCurrentField.AddSibling(2) do begin
    MaxChildCount:= 3;
    AddChild('+', -1);
    AddChild('0', -1);
    AddChild('-', -1);
  end;
end;

end.
