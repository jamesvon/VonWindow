(*******************************************************************************
  UFrameFollowInputor 流程设计控件
================================================================================
  系统有三组图标
  1、流程展示图标      尺寸可以大一些，宽一些（需要在程序中给予赋值）
  2、流程添加图标      尺寸需要标准一些，用于菜单弹出，点击添加（通过菜单图标定义）
  3、系统图标          作为展开、收缩与功能提示图标（这个在资源文件中，不用定义）
--------------------------------------------------------------------------------
* FrameFollowInputor1.ModuleImages:= imgTask;
  FrameFollowInputor1.pmMenu.Images:= imgData; //可通过FrameFollowInputor1.pmMenu来进行定义
*******************************************************************************)
unit UFrameFollowInputor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ButtonGroup, Math, Buttons, Menus, ImgList,
  UVonSystemFuns, UVonConfig, ComCtrls, ToolWin, System.ImageList;

type
  TFrameFollowInputor = class;
  TFollowField = class;
  TMoveEvent = procedure (Org, Dest: Integer) of object;
  TSaveEvent = procedure (Sender: TFollowField) of object;
  TMenuEvent = procedure (Sender: TFrameFollowInputor; Kind: Integer) of object;

  TFollowField = class(TPanel)
  private
    FChildren: TList;
    FMenu: TPopupMenu;
    FBmp: TImage;
    FMenuImages: TImageList;
    FCollapsEMenu: TMenuItem;
    FExpandMenu: TMenuItem;
    FonDelete: TSaveEvent;
    FonEdit: TSaveEvent;
    FModuleImages: TImageList;
    FField: TVonFollowItem;
    FOnE_C: TNotifyEvent;
    FOnMove: TMoveEvent;
    FImageIndex: Integer;
    FonResize: TNotifyEvent;
    FKey: string;
    FID: Integer;
    FInputor: TFrameFollowInputor;
    procedure btnTopClick(Sender: TObject);
    procedure btnUpClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure btnBottomClick(Sender: TObject);
    procedure btnCollapsClick(Sender: TObject);
    procedure btnExpandClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure SetField(const Value: TVonFollowItem);
    procedure SetImageIndex(const Value: Integer);
    procedure SetModuleImages(const Value: TImageList);
    procedure SetonDelete(const Value: TSaveEvent);
    procedure SetonEdit(const Value: TSaveEvent);
    procedure SetOnMove(const Value: TMoveEvent);
    function GetChildName: string;
    procedure SetonResize(const Value: TNotifyEvent);
    function GetChildCount: Integer;
    function GetChildren(Index: Integer): TFrameFollowInputor;
    function NewFollow(caseValue: string; Index: Integer = -1): TFrameFollowInputor;
    procedure SetKey(const Value: string);
    procedure SetInputor(const Value: TFrameFollowInputor);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Clear;
    procedure EventOfResize(Sender: TObject);
    procedure EventOfUpdateCaseValue(Sender: TFrameFollowInputor; Index: Integer);
    function AddChild(CaseValue: string): TFrameFollowInputor;
    function InsertChild(Index: Integer; CaseValue: string): TFrameFollowInputor;
    /// <summary>清除所有子节点，调用数据库删除命令</summary>
    procedure RemoveAll;
    procedure RemoveChild(Index: Integer);
    function IndexOfCaseValue(CaseValue: string): Integer;
    property Children[Index: Integer]: TFrameFollowInputor read GetChildren;
  published
    property ChildCount: Integer read GetChildCount;
    property ID: Integer read FID write FID;
    property Key: string read FKey write SetKey;
    property Inputor: TFrameFollowInputor read FInputor write SetInputor;
    //property Field: TVonFollowItem read FField write SetField;
    property ImageIndex: Integer read FImageIndex write SetImageIndex;
    property ModuleImages: TImageList read FModuleImages write SetModuleImages;
    property OnEdit: TSaveEvent read FonEdit write SetonEdit;
    property OnDelete: TSaveEvent read FonDelete write SetonDelete;
    property OnMove: TMoveEvent read FOnMove write SetOnMove;
    property onResize: TNotifyEvent read FonResize write SetonResize;
  end;

  TFrameFollowInputor = class(TFrame)
    plBack: TPanel;
    plTitle: TPanel;
    ImageList1: TImageList;
    ECaseValue: TComboBox;
    EbtnAddSubFollow: TComboBoxEx;
    btnDelete: TSpeedButton;
    btnAddSibling: TSpeedButton;
    procedure ECaseValueExit(Sender: TObject);
    procedure pmChildrenPopup(Sender: TObject);
    procedure EbtnAddSubFollowChange(Sender: TObject);
    procedure btnAddSiblingClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure ECaseValueChange(Sender: TObject);
  private
    FModuleImages: TImageList;
    FonDelete: TSaveEvent;
    FonEdit: TSaveEvent;
    FonSave: TSaveEvent;
    FonResize: TNotifyEvent;
    FonUpdateCaseValue: TMenuEvent;
    FOnMenu: TMenuEvent;
    procedure SetModuleImages(const Value: TImageList);
    procedure SetCaseValue(const Value: string);
    procedure SetCaseOption(const Value: string);
    procedure SetonDelete(const Value: TSaveEvent);
    procedure SetonEdit(const Value: TSaveEvent);
    procedure SetonResize(const Value: TNotifyEvent);
    procedure SetonSave(const Value: TSaveEvent);
    procedure SetOnMenu(const Value: TMenuEvent);
    procedure SetonUpdateCaseValue(const Value: TMenuEvent);
  private
    { Private declarations }
    FItems: TList;
    FOrgMenuEvent: TList;
    function GetChildName: string;
    function NewField(Index, Kind: Integer; Key: string): TFollowField;
    procedure EventOfDelete(Sender: TFollowField);
    procedure EventOfResize(Sender: TObject);
    procedure EventOfMove(Org, Dest: Integer);
    function GetCaseValue: string;
    function GetCaseOption: string;
  protected
    /// <summary>只完成界面级清除，释放空间用，不调用删除存储</summary>
    procedure Clear;
  public
    { Public declarations }
    /// <summary>构造函数，创建下级节点列表</summary>
    constructor Create(AOwner: TComponent); override;
    /// <summary>析构函数</summary>
    destructor Destroy; override;
    /// <summary>追加一个节点</summary>
    function AddField(Kind: Integer; Key: string): TFollowField;
    /// <summary>插入一个节点</summary>
    function InsertField(Index, Kind: Integer; Key: string): TFollowField;
    /// <summary>删除一个节点</summary>
    procedure DeleteField(Index: Integer);
    /// <summary>移动一个节点</summary>
    procedure Move(Org, Dest: Integer);
    /// <summary>清除所有子节点，调用数据库删除命令</summary>
    procedure RemoveAll;
    procedure SaveToFollow(AFollow: TVonFollow);
    procedure LoadFromFollow(AFollow: TVonFollow);
  published
    /// <summary>标题内容，即流程进入条件值内容</summary>
    property CaseValue: string read GetCaseValue write SetCaseValue;
    /// <summary>标题内容选项</summary>
    property CaseOption: string read GetCaseOption write SetCaseOption;
    /// <summary>流程图标集</summary>
    property ModuleImages: TImageList read FModuleImages write SetModuleImages;
    /// <summary>删除本流程时产生的，也就是说删除自己时，内部会调用OnRemove事件，删除下属子流程</summary>
    property onDelete: TSaveEvent read FonDelete write SetonDelete;
    /// <summary>保存流程信息</summary>
    property onSave: TSaveEvent read FonSave write SetonSave;
    /// <summary>编辑流程回调函数/summary>
    property onEdit: TSaveEvent read FonEdit write SetonEdit;
    /// <summary>更新进入条件时触发事件</summary>
    property onUpdateCaseValue: TMenuEvent read FonUpdateCaseValue write SetonUpdateCaseValue;
    /// <summary>下级流程添加菜单事件</summary>
    property OnMenu: TMenuEvent read FOnMenu write SetOnMenu;
    /// <summary>重新计算高度和宽度</summary>
    property onResize: TNotifyEvent read FonResize write SetonResize;
  end;

implementation

var R, G, B, GrandRate: byte;
  RR, BR, GR: integer;

function GetGrandColor: TColor;
  function GetColor(CR: Byte; var VR: integer): Byte;
  var valueRatio: Double;
  begin
      if(CR + VR > 255)or(CR + VR < 0)then VR:= - VR;
      Result:= Round(CR + VR);
  end;
begin
  if R = G then G:= G + GrandRate div 3;
  if B = G then B:= B + GrandRate div 3;
  if B = R then R:= R + GrandRate div 3;
  R:= GetColor(R, RR);
  B:= GetColor(B, BR);
  G:= GetColor(G, GR);
  Result:= RGB(R, G, B);
end;

{$R *.dfm}

var
  FMenuImages : TImageList;

{ TFrameFollowInputor }

constructor TFrameFollowInputor.Create(AOwner: TComponent);
begin
  inherited;
  FOrgMenuEvent:= TList.Create;
  FItems:= TList.Create;
  Color:= GetGrandColor;
end;

destructor TFrameFollowInputor.Destroy;
begin
  FItems.Free;
  inherited;
end;

{ TFrameFollowInputor Methods }

procedure TFrameFollowInputor.Clear;
var
  I: Integer;
begin     //释放空间用，不调用删除存储
  for I := 0 to FItems.Count - 1 do
    TFollowField(FItems[I]).Free;
  FItems.Clear;
end;

function TFrameFollowInputor.AddField(Kind: Integer; Key: string): TFollowField;
begin
  Result:= NewField(-1, Kind, Key);
end;

procedure TFrameFollowInputor.DeleteField(Index: Integer);
begin
  TFollowField(FItems[Index]).RemoveAll;                                        //删除所有下属子节点
  if Assigned(FonDelete) then FonDelete(TFollowField(FItems[Index]));           //数据库删除Info信息
//  Height:= Height - TFollowField(FItems[Index]).Height;
  TFollowField(FItems[Index]).Free;
  FItems.Delete(Index);
  EventOfResize(Self);
end;

function TFrameFollowInputor.InsertField(Index, Kind: Integer; Key: string): TFollowField;
begin
  Result:= NewField(Index, Kind, Key);
end;

procedure TFrameFollowInputor.LoadFromFollow(AFollow: TVonFollow);
begin

end;

procedure TFrameFollowInputor.Move(Org, Dest: Integer);
begin
  EventOfMove(Org, Dest);
end;

function TFrameFollowInputor.NewField(Index, Kind: Integer; Key: string): TFollowField;
begin    //设置显示组件信息
  Result:= TFollowField.Create(Self);
  Result.Parent:= plBack;
  Result.Name:= GetChildName;
  Result.ImageIndex:= Kind;
  Result.ModuleImages:= FModuleImages;
  Result.Key:= Key;
  Result.Inputor:= Self;
  Result.OnEdit:= FonEdit;
  Result.OnMove:= EventOfMove;
  Result.OnDelete:= EventOfDelete;
  Result.onResize:= EventOfResize;
  if Index = -1 then FItems.Add(Pointer(Result))
  else FItems.Insert(Index, Pointer(Result));
  if Assigned(FonSave) then FonSave(Result);   //存储Info信息
  Result.EventOfResize(nil);
end;

procedure TFrameFollowInputor.pmChildrenPopup(Sender: TObject);
var
  I: Integer;
begin

end;

procedure TFrameFollowInputor.RemoveAll;
begin
  while FItems.Count > 0 do begin
    TFollowField(FItems[0]).RemoveAll;                                        //删除所有下属子节点
    if Assigned(FonDelete) then FonDelete(TFollowField(FItems[0]));           //数据库删除Info信息
    TFollowField(FItems[0]).Free;
    FItems.Delete(0);
  end;
  EventOfResize(Self);
end;

{ TFrameFollowInputor Events }

procedure TFrameFollowInputor.EbtnAddSubFollowChange(Sender: TObject);
begin
  if Assigned(FOnMenu) then FOnMenu(Self, EbtnAddSubFollow.ItemIndex);
end;

procedure TFrameFollowInputor.ECaseValueChange(Sender: TObject);
begin
  if(ECaseValue.Style = csDropDownList)and Assigned(FonUpdateCaseValue)then
    FonUpdateCaseValue(Self, ECaseValue.ItemIndex);
end;

procedure TFrameFollowInputor.ECaseValueExit(Sender: TObject);
begin
  if Assigned(FonUpdateCaseValue) then
    FonUpdateCaseValue(Self, ECaseValue.ItemIndex);
end;

procedure TFrameFollowInputor.EventOfDelete(Sender: TFollowField);
begin
  DeleteField(FItems.IndexOf(Pointer(Sender)));
end;

procedure TFrameFollowInputor.EventOfMove(Org, Dest: Integer);
  procedure MoveField(F, E: Integer);
  var
    I, Z: Integer;
  begin
    //if lbTitle.Visible then Z:= lbTitle.Height else Z:= 0;
    Z:= 0;
    if F <> 0 then with TFollowField(FItems[F - 1])do
      Z:= Top + Height;
    for I := F to E do
      with TFollowField(FItems[I])do begin
        Top:= Z;
        Inc(Z, Height);
        if Assigned(FonSave) then FonSave(FItems[I]);
    end;
  end;
begin
  if(Org < 0)or(Org >= FItems.Count)or(Dest < 0)or(Dest >= FItems.Count) then Exit;
  FItems.Move(Org, Dest);
  if Org > Dest then MoveField(Dest, Org)
  else MoveField(Org, Dest);
end;

procedure TFrameFollowInputor.EventOfResize(Sender: TObject);
var
  I, H, W: Integer;
begin
  H:= 0;
  W:= FModuleImages.Width + 26;
  for I := 0 to FItems.Count - 1 do begin
    TFollowField(FItems[I]).Top:= H;
    Inc(H, TFollowField(FItems[I]).Height);
    W:= Max(W, TFollowField(FItems[I]).Width);
  end;
  for I := 0 to FItems.Count - 1 do
    with TFollowField(FItems[I]) do
      Left:= (W - Width) div 2;
  //plBack.Height:= H;
  if plTitle.Visible then Height:= H + plTitle.Height
  else Height:= H;
  Width:= W;
  if Assigned(FonResize) then FonResize(self);
end;

function TFrameFollowInputor.GetCaseOption: string;
begin
  Result:= ECaseValue.Items.Text;
end;

function TFrameFollowInputor.GetCaseValue: string;
begin
  Result:= ECaseValue.Text;
end;

function TFrameFollowInputor.GetChildName: string;
var
  Idx: Integer;
begin
  Idx:= 1;
  while Assigned(FindComponent('EField' + IntToStr(Idx))) do Inc(Idx);
  Result:= 'EField' + IntToStr(Idx);
end;

procedure TFrameFollowInputor.SaveToFollow(AFollow: TVonFollow);
begin

end;

{ TFrameFollowInputor Properties }

procedure TFrameFollowInputor.SetCaseOption(const Value: string);
var
  OrgValue: string;
begin
  OrgValue:= ECaseValue.Text;
  if Value = '' then ECaseValue.Style:= csDropDown
  else begin
    if Pos(',', Value) > 0 then begin
      ECaseValue.Items.Delimiter:= ',';
      ECaseValue.Items.DelimitedText:= Value;
    end else if Pos('|', Value) > 0 then begin
      ECaseValue.Items.Delimiter:= '|';
      ECaseValue.Items.DelimitedText:= Value;
    end else ECaseValue.Items.Text:= Value;
    if ECaseValue.Items.Count > 1 then
      ECaseValue.Style:= csDropDownList
    else ECaseValue.Style:= csDropDown;
    ECaseValue.ItemIndex:= ECaseValue.Items.IndexOf(OrgValue);
    if Assigned(FonUpdateCaseValue) then
      FonUpdateCaseValue(Self, ECaseValue.ItemIndex);
  end;
end;

procedure TFrameFollowInputor.SetCaseValue(const Value: string);
begin
  if ECaseValue.Style = csDropDownList then
    ECaseValue.ItemIndex := ECaseValue.Items.IndexOf(Value)
  else ECaseValue.Text := Value;
end;

procedure TFrameFollowInputor.SetModuleImages(const Value: TImageList);
begin
  FModuleImages := Value;
end;

procedure TFrameFollowInputor.SetonDelete(const Value: TSaveEvent);
begin
  FonDelete := Value;
end;

procedure TFrameFollowInputor.SetonEdit(const Value: TSaveEvent);
var
  I: Integer;
begin
  FonEdit := Value;
  for I := 0 to FItems.Count - 1 do
    TFollowField(FItems[I]).OnEdit:= Value;
end;

procedure TFrameFollowInputor.SetOnMenu(const Value: TMenuEvent);
begin
  FOnMenu := Value;
end;

procedure TFrameFollowInputor.SetonResize(const Value: TNotifyEvent);
begin
  FonResize := Value;
end;

procedure TFrameFollowInputor.SetonSave(const Value: TSaveEvent);
begin
  FonSave := Value;
end;

procedure TFrameFollowInputor.SetonUpdateCaseValue(const Value: TMenuEvent);
begin
  FonUpdateCaseValue := Value;
end;

procedure TFrameFollowInputor.btnDeleteClick(Sender: TObject);
begin
  if TFollowField(Parent).FChildren.Count > 1 then
    TFollowField(Parent).RemoveChild(TFollowField(Parent).FChildren.IndexOf(Pointer(Self)));
end;

procedure TFrameFollowInputor.btnAddSiblingClick(Sender: TObject);
var
  F: Extended;
  newField: TFrameFollowInputor;
begin
  if TryStrToFloat(CaseValue, F) then
    newField:= TFollowField(Parent).AddChild(FloatToStr(F + 1))
  else newField:= TFollowField(Parent).AddChild(CaseValue);
  newField.CaseOption:= CaseOption;
  TFollowField(Parent).EventOfResize(nil);
end;

{ TFollowField }

constructor TFollowField.Create(AOwner: TComponent);
  function AddMenu(Item: TMenuItem; ItemCaption: string; ImgIdx: Integer; Method: TNotifyEvent): TMenuItem;
  var
    szBMP, szMask: TBitmap;
  begin
    Item.Caption:= ItemCaption;
    Item.OnClick:= Method;
    Item.ImageIndex:= ImgIdx;
    Result:= Item;
  end;
begin
  inherited;
  FChildren:= TList.Create;
  ShowCaption:= False;
  BevelInner:= bvNone;
  BevelOuter:= bvNone;
  {$region 'Create a menu(FMenu) of field to move position.置顶,上移,下移,置底,编辑,删除,收缩,扩展'}
  FMenuImages:= TImageList.Create(Self);
  FMenuImages.GetResource(rtBitmap, 'FULLOPERATOR', 16, [lrDefaultColor, lrDefaultSize], clPurple);
  FMenu:= TPopupMenu.Create(Self);
  FMenu.Images:= FMenuImages;
  FMenu.Items.Add(AddMenu(TMenuItem.Create(Self), '置顶', 0, btnTopClick));
  FMenu.Items.Add(AddMenu(TMenuItem.Create(Self), '上移', 1, btnUpClick));
  FMenu.Items.Add(AddMenu(TMenuItem.Create(Self), '下移', 2, btnDownClick));
  FMenu.Items.Add(AddMenu(TMenuItem.Create(Self), '置底', 3, btnBottomClick));
  FMenu.Items.Add(AddMenu(TMenuItem.Create(Self), '-', 0, nil));
  FMenu.Items.Add(AddMenu(TMenuItem.Create(Self), '编辑', 13, btnEditClick));
  FMenu.Items.Add(AddMenu(TMenuItem.Create(Self), '删除', 15, btnDeleteClick));
  FMenu.Items.Add(AddMenu(TMenuItem.Create(Self), '-', 0, nil));
  FMenu.Items.Add(AddMenu(TMenuItem.Create(Self), '收缩', 18, btnCollapsClick));
  FMenu.Items.Add(AddMenu(TMenuItem.Create(Self), '扩展', 19, btnExpandClick));
  FMenu.Items[7].Visible:= False;
  FMenu.Items[8].Visible:= False;
  FMenu.Items[9].Visible:= False;
  {$endregion}
  FBmp:= TImage.Create(Self);
  FBmp.Parent:= Self;
  FBmp.Align:= alTop;
  FBmp.Center:= True;
  FBmp.Transparent:= True;
  FBmp.OnClick:= btnEditClick;
  FBmp.PopupMenu:= FMenu;
  Width:= 26;
end;

destructor TFollowField.Destroy;
begin
  Clear;
  FChildren.Free;
  inherited;
end;

procedure TFollowField.Clear;
begin
  while FChildren.Count > 0 do begin
    TFrameFollowInputor(FChildren[0]).Free;
    FChildren.Delete(0);
  end;
end;

procedure TFollowField.btnCollapsClick(Sender: TObject);
begin
  Height:= FModuleImages.Height + 2;
  Width:= FModuleImages.Width + 26;
  if Assigned(FonResize) then FonResize(Self);
  FMenu.Items[8].Visible:= False;
  FMenu.Items[9].Visible:= True;
end;

procedure TFollowField.btnExpandClick(Sender: TObject);
var
  I, H: Integer;
begin
  H:= 0;
  FMenu.Items[8].Visible:= True;
  FMenu.Items[9].Visible:= False;
  EventOfResize(Self);
end;

procedure TFollowField.btnDeleteClick(Sender: TObject);
begin
  if Assigned(FonDelete) then FonDelete(Self);
end;

procedure TFollowField.btnEditClick(Sender: TObject);
begin
  if Assigned(FonEdit) then FonEdit(Self);
end;

{ TFollowField Methods of moving }

procedure TFollowField.btnBottomClick(Sender: TObject);
var
  Idx: Integer;
begin
  if Assigned(FOnMove) then begin
    Idx:= Inputor.FItems.IndexOf(Pointer(Self));
    FOnMove(Idx, Inputor.FItems.Count - 1);
  end;
end;

procedure TFollowField.btnDownClick(Sender: TObject);
var
  Idx: Integer;
begin
  if Assigned(FOnMove) then begin
    Idx:= Inputor.FItems.IndexOf(Pointer(Self));
    FOnMove(Idx, Idx + 1);
  end;
end;

procedure TFollowField.btnTopClick(Sender: TObject);
var
  Idx: Integer;
begin
  if Assigned(FOnMove) then begin
    Idx:= Inputor.FItems.IndexOf(Pointer(Self));
    FOnMove(Idx, 0);
  end;
end;

procedure TFollowField.btnUpClick(Sender: TObject);
var
  Idx: Integer;
begin
  if Assigned(FOnMove) then begin
    Idx:= Inputor.FItems.IndexOf(Pointer(Self));
    FOnMove(Idx, Idx - 1);
  end;
end;

procedure TFollowField.EventOfResize(Sender: TObject);
var
  I, H, W: Integer;
begin
  H:= 0;
  W:= 0;
  for I := 0 to FChildren.Count - 1 do begin
    H:= Max(H, TFrameFollowInputor(FChildren[I]).Height);
    TFrameFollowInputor(FChildren[I]).Left:= W;
    Inc(W, TFrameFollowInputor(FChildren[I]).Width);
  end;
  Height:= H + ModuleImages.Height + 4;
  Width:= Max(W, FModuleImages.Width + 26);
  if Assigned(FonResize) then FonResize(Self);
end;

procedure TFollowField.EventOfUpdateCaseValue(Sender: TFrameFollowInputor;
  Index: Integer);
var
  I: Integer;
begin
  for I := 0 to FChildren.Count - 1 do
    if TFrameFollowInputor(FChildren[I]) = Sender then Continue
    else with TFrameFollowInputor(FChildren[I]) do
    if(ECaseValue.Style = csDropDownList)and(ECaseValue.ItemIndex = Index)then begin
      if Sender.ECaseValue.ItemIndex = Sender.ECaseValue.Items.Count - 1 then
        Sender.ECaseValue.ItemIndex := 0
      else Sender.ECaseValue.ItemIndex := Index + 1;
      Inc(Index);
    end;
end;

{ TFollowField Methods of Children }

function TFollowField.AddChild(CaseValue: string): TFrameFollowInputor;
begin
  Result:= NewFollow(CaseValue);
end;

function TFollowField.GetChildCount: Integer;
begin
  Result:= FChildren.Count;
end;

function TFollowField.GetChildName: string;
var
  Idx: Integer;
begin
  Idx:= 1;
  while Assigned(FindComponent('EField' + IntToStr(Idx))) do Inc(Idx);
  Result:= 'EField' + IntToStr(Idx);
end;

function TFollowField.GetChildren(Index: Integer): TFrameFollowInputor;
begin
  Result:= TFrameFollowInputor(FChildren[Index]);
end;

function TFollowField.IndexOfCaseValue(CaseValue: string): Integer;
var
  I: Integer;
begin
  Result:= -1;
  for I := 0 to FChildren.Count - 1 do
    if TFrameFollowInputor(FChildren[I]).ECaseValue.Text = CaseValue then begin
      Result:= I;
      Exit;
    end;
end;

function TFollowField.InsertChild(Index: Integer;
  CaseValue: string): TFrameFollowInputor;
begin
  Result:= NewFollow(CaseValue, Index);
end;

function TFollowField.NewFollow(caseValue: string; Index: Integer): TFrameFollowInputor;
begin
  Result:= TFrameFollowInputor.Create(Self);
  Result.ParentBackground:= False;
  Result.Name:= GetChildName;
  Result.CaseValue:= caseValue;
  Result.CaseOption:= Inputor.CaseOption;
  Result.ModuleImages:= Inputor.ModuleImages;
  Result.OnMenu:= Inputor.OnMenu;
  Result.EbtnAddSubFollow.Images:= Inputor.EbtnAddSubFollow.Images;
  Result.EbtnAddSubFollow.ItemsEx.Assign(Inputor.EbtnAddSubFollow.ItemsEx);
  if Index < 0 then FChildren.Add(Pointer(Result))
  else FChildren.Insert(Index, Pointer(Result));
  Result.Parent:= Self;
  Result.Top:= FModuleImages.Height + 3;
  Result.Height:= Result.plTitle.Height;
  Result.Width:= FModuleImages.Width + 26;
  Result.btnAddSibling.Visible:= True;
  Result.btnDelete.Visible:= True;
  Result.OnEdit:= FonEdit;
  Result.OnSave:= TFrameFollowInputor(Parent.Parent).FonSave;
  Result.onResize:= EventOfResize;
  Result.onUpdateCaseValue:= EventOfUpdateCaseValue;
  FMenu.Items[7].Visible:= True;
  FMenu.Items[8].Visible:= True;
  FMenu.Items[9].Visible:= False;
end;

procedure TFollowField.RemoveAll;
begin

end;

procedure TFollowField.RemoveChild(Index: Integer);
begin
  TFrameFollowInputor(FChildren[Index]).Free;
  FChildren.Delete(Index);
  EventOfResize(self);
  FMenu.Items[7].Visible:= (FChildren.Count > 0);
  FMenu.Items[8].Visible:= (FChildren.Count > 0)and(Height <> FModuleImages.Height + 2);
  FMenu.Items[9].Visible:= (FChildren.Count > 0)and(Height = FModuleImages.Height + 2);
end;

procedure TFollowField.SetField(const Value: TVonFollowItem);
begin
  FField:= Value;
end;

procedure TFollowField.SetImageIndex(const Value: Integer);
begin
  FImageIndex := Value;
  if Assigned(FModuleImages) then
    FModuleImages.GetBitmap(FImageIndex, FBmp.Picture.Bitmap);
end;

procedure TFollowField.SetInputor(const Value: TFrameFollowInputor);
begin
  FInputor := Value;
end;

procedure TFollowField.SetKey(const Value: string);
begin
  FKey := Value;
  if Length(Value) > 6 then FKey:= Copy(Value, 1, 5) + '...';
  FBmp.Canvas.Brush.Style:= bsClear;
  FBmp.Canvas.TextOut(50, (FBmp.Height - FBmp.Canvas.TextHeight(Value)) div 2, FKey);
  FKey := Value;
end;

procedure TFollowField.SetModuleImages(const Value: TImageList);
begin
  FModuleImages := Value;
  FModuleImages.GetBitmap(FImageIndex, FBmp.Picture.Bitmap);
  FBmp.Height:= FModuleImages.Height;
  Width:= FModuleImages.Width + Width;
  Height:= FModuleImages.Height + 2;
end;

procedure TFollowField.SetonDelete(const Value: TSaveEvent);
begin
  FonDelete:= Value;
end;

procedure TFollowField.SetonEdit(const Value: TSaveEvent);
begin
  FonEdit:= Value;
end;

procedure TFollowField.SetOnMove(const Value: TMoveEvent);
begin
  FonMove:= Value;
end;

procedure TFollowField.SetonResize(const Value: TNotifyEvent);
begin
  FonResize := Value;
end;

initialization
  R:= 127; G:= 127; B:= 127; GrandRate:= 27; RR:= 27; BR:= 27; GR:= 27;         //初始化颜色值
  FMenuImages := TImageList.Create(nil);                                        //位置及操作图标集
  FMenuImages.GetResource(rtBitmap, 'FOLLOWTITLEICONS', 13, [], clFuchsia);

finalization
  FMenuImages.Free;

end.
