object FrameGridBase: TFrameGridBase
  Left = 0
  Top = 0
  Width = 852
  Height = 534
  TabOrder = 0
  object splitItem: TSplitter
    Left = 664
    Top = 21
    Height = 513
    Align = alRight
    ExplicitLeft = 440
    ExplicitTop = 240
    ExplicitHeight = 100
  end
  object plItems: TPanel
    Left = 667
    Top = 21
    Width = 185
    Height = 513
    Align = alRight
    Caption = 'plItems'
    ShowCaption = False
    TabOrder = 0
    object SplitLegend: TSplitter
      Left = 1
      Top = 119
      Width = 183
      Height = 3
      Cursor = crVSplit
      Align = alTop
      ExplicitLeft = 181
      ExplicitTop = 98
      ExplicitWidth = 367
    end
    object lstField: TValueListEditor
      Left = 1
      Top = 122
      Width = 183
      Height = 390
      Align = alClient
      TabOrder = 0
      TitleCaptions.Strings = (
        #39033#30446
        #20869#23481)
      ExplicitTop = 101
      ExplicitHeight = 411
      ColWidths = (
        81
        96)
    end
    object ELegendGroup: TComboBox
      Left = 1
      Top = 1
      Width = 183
      Height = 21
      Align = alTop
      Style = csDropDownList
      TabOrder = 1
      OnChange = ELegendGroupChange
    end
    object lstLegend: TListBox
      Left = 1
      Top = 22
      Width = 183
      Height = 97
      Align = alTop
      ItemHeight = 13
      TabOrder = 2
      OnDrawItem = lstLegendDrawItem
    end
  end
  object Bar: TStatusBar
    Left = 0
    Top = 0
    Width = 852
    Height = 21
    Align = alTop
    Panels = <
      item
        Alignment = taCenter
        Text = #21151#33021
        Width = 50
      end
      item
        Width = 50
      end>
  end
  object Grid: TDBGrid
    Left = 0
    Top = 21
    Width = 664
    Height = 513
    Align = alClient
    DataSource = GridSource
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object gridMenu: TPopupMenu
    Left = 16
    Top = 96
    object N1: TMenuItem
      Caption = #23548#20986
      object menuExportCsv: TMenuItem
        Caption = #26684#24335#25991#26412
      end
      object menuExportExcel: TMenuItem
        Caption = 'Excel'
      end
      object menuExportXml: TMenuItem
        Caption = 'XML'
      end
      object menuExportHtml: TMenuItem
        Caption = #32593#39029
      end
    end
    object N3: TMenuItem
      Caption = #25171#21360
      object menuPrintIO: TMenuItem
        Caption = #30452#25509#25171#21360
      end
      object menuPrintExcel: TMenuItem
        Caption = 'Excel'#25171#21360
      end
      object menuPrintHtml: TMenuItem
        Caption = #32593#39029#25171#21360
      end
    end
    object menuPrintSetup: TMenuItem
      Caption = #25171#21360#26426
    end
    object menuColumnSetting: TMenuItem
      Caption = #35774#32622
    end
    object menuCondition: TMenuItem
      Caption = #26174#31034#26597#35810#26465#20214
    end
    object menuEdit: TMenuItem
      Caption = #26174#31034#24405#20837#31383#21475
    end
    object menuSelectionOption: TMenuItem
      Caption = #34892#36873#25321#27169#24335
      OnClick = menuSelectionOptionClick
    end
    object menuDetails: TMenuItem
      Caption = #26597#30475#26126#32454
      OnClick = menuDetailsClick
    end
  end
  object GridSource: TDataSource
    Left = 16
    Top = 48
  end
end
