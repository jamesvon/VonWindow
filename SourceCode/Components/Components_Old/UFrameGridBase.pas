unit UFrameGridBase;

interface

uses
  WinAPI.Windows, WinAPI.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Menus, Grids, ValEdit, StdCtrls, ExtCtrls, DB,
  UVonSystemFuns, DBGrids;

type
  /// <summary>统计类型</summary>
  /// <param name="ES_SUM">合计</param>
  /// <param name="ES_AVG">平均值</param>
  /// <param name="ES_COUNT">计数</param>
  /// <param name="ES_MAX">最大值</param>
  /// <param name="ES_MIN">最小值</param>
  /// <param name="ES_STDEV">数据的标准差</param>
  /// <param name="ES_STDEVP">总体标准差</param>
  /// <param name="ES_VAR">统计变异数</param>
  /// <param name="ES_VARP">总体变异数</param>
  TEnumStatistic = (ES_SUM, ES_AVG, ES_COUNT, ES_MAX, ES_MIN, ES_STDEV, ES_STDEVP, ES_VAR, ES_VARP);

  TVonColumn = record
    Title: string;
    FieldName: string;
    FieldType: TFieldType;
    DisplayWidth: Integer;
  end;
  PVonColumn = ^TVonColumn;

  TVonLegend = record
    Legend: string;
    Condition: string;
    BackColor: TColor;
    FontColor: TColor;
  end;
  PVonLegend = ^TVonLegend;

  TVonStatistic = record
    FieldName: string;
    HintText: string;
    Condition: string;
    Kind: TEnumStatistic;
  end;
  PVonStatistic = ^TVonStatistic;

  TFrameGridBase = class(TFrame)
    plItems: TPanel;
    SplitLegend: TSplitter;
    lstField: TValueListEditor;
    splitItem: TSplitter;
    gridMenu: TPopupMenu;
    N1: TMenuItem;
    menuExportCsv: TMenuItem;
    menuExportExcel: TMenuItem;
    menuExportXml: TMenuItem;
    menuExportHtml: TMenuItem;
    N3: TMenuItem;
    menuPrintIO: TMenuItem;
    menuPrintExcel: TMenuItem;
    menuPrintHtml: TMenuItem;
    menuPrintSetup: TMenuItem;
    menuColumnSetting: TMenuItem;
    menuCondition: TMenuItem;
    menuEdit: TMenuItem;
    menuSelectionOption: TMenuItem;
    Bar: TStatusBar;
    ELegendGroup: TComboBox;
    lstLegend: TListBox;
    GridSource: TDataSource;
    Grid: TDBGrid;
    menuDetails: TMenuItem;
    procedure ELegendGroupChange(Sender: TObject);
    procedure lstLegendDrawItem(Control: TWinControl; Index: Integer; Rect: TRect;
      State: TOwnerDrawState);
    procedure menuSelectionOptionClick(Sender: TObject);
    procedure menuDetailsClick(Sender: TObject);
  private
    { Private declarations }
    FTableName: string;
    FColumns: TList;
    FStatistic: TList;
    FCondition: TList;
    FDataset: TDataSet;  //缓存变量，无需要创建和释放，是 ELegend 项目中的 Object
    procedure ClearColumn;
    procedure LoadColumns(TableName: string);
    procedure SaveColumns;
    procedure ReStatistic;
    procedure SetDataset(const Value: TDataSet);
  protected
    procedure RedrewTable; virtual; abstract;
    function GetRowCount: Integer; virtual; abstract;
    function GetData(Col, Row: Integer): Extended; virtual; abstract;
    function GetArray(Col: Integer): TArrayExtended; virtual; abstract;
    procedure DisplayColumns; virtual; abstract;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    /// <summary>删除一个图例组</summary>
    procedure ClearConditionGroup(Group: string);
    /// <summary>注册条件颜色代码</summary>
    /// <param name="Group">条件分组名称</param>
    /// <param name="Condition">颜色条件</param>
    /// <param name="BackColor">背景颜色</param>
    /// <param name="FontColor">文字颜色</param>
    procedure RegCondition(Group, Legend, Condition: string; BackColor, FontColor: TColor);
    /// <summary>删除一个统计项目</summary>
    procedure ClearStatistic;
    /// <param name="HintText">提示内容</param>
    /// <param name="Kind">统计类型，COUNT,SUM,AVG,MIN,MAX</param>
    /// <param name="Condition">统计条件</param>
    procedure RegStatistic(Fieldname, HintText: string; Kind: TEnumStatistic; Condition: string);
    /// <summary>得到字段所在列</summary>
    /// <param name="字段名称"></param>
    /// <returns>列序号</returns>
    function IndexOf(FieldName: string): Integer;
  published
    property RowCount: Integer read GetRowCount;
    property Dataset: TDataSet read FDataset write SetDataset;
  end;

implementation

{$R *.dfm}

{ TFrameGridBase }

constructor TFrameGridBase.Create(AOwner: TComponent);
begin
  inherited;
  FColumns:= TList.Create;
  FStatistic:= TList.Create;
end;

destructor TFrameGridBase.Destroy;
begin
  SaveColumns;
  FStatistic.Free;
  FColumns.Free;
  inherited;
end;

(* Properties and menu events *)

procedure TFrameGridBase.SetDataset(const Value: TDataSet);
begin
  FDataset := Value;
end;

procedure TFrameGridBase.menuDetailsClick(Sender: TObject);
begin
  menuDetails.Checked:= not menuDetails.Checked;
  plItems.Visible:= menuDetails.Checked;
  splitItem.Visible:= menuDetails.Checked;
  splitItem.Left:= 0;
end;

procedure TFrameGridBase.menuSelectionOptionClick(Sender: TObject);
begin
  menuSelectionOption.Checked:= not menuSelectionOption.Checked;
  if not menuSelectionOption.Checked then
    Grid.Options:= Grid.Options - [dgRowSelect] + [dgEditing]
  else Grid.Options:= Grid.Options - [dgEditing] + [dgRowSelect]
end;

(* Columns *)

procedure TFrameGridBase.ClearColumn;
var
  I: Integer;
begin
  for I := 0 to FColumns.Count - 1 do
    FreeMem(FColumns[I]);
  FColumns.Clear;
end;

procedure TFrameGridBase.LoadColumns(TableName: string);
var
  columnFilename: string;
  fs: TFileStream;
  P: PVonColumn;
begin
  columnFilename:= ExtractFilePath(Application.ExeName) + 'RES/' + TableName + '.column';
  ClearColumn;
  if FileExists(columnFilename) then begin
    fs:= TFileStream.Create(columnFilename, fmOpenRead);
    while fs.Position < fs.Size do begin
      New(P);
      P.Title:= ReadStringFromStream(fs);
      P.FieldName:= ReadStringFromStream(fs);
      P.FieldType:= TFieldType(ReadIntFromStream(fs));
      P.DisplayWidth:= ReadIntFromStream(fs);
      FColumns.Add(P);
    end;
    fs.Free;
    DisplayColumns;
  end;
end;

procedure TFrameGridBase.lstLegendDrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
var
  I:Integer;
  S: string;
  P: PVonLegend;
begin
  P:= PVonLegend(lstLegend.Items.Objects[Index]);
  with lstLegend.Canvas do begin
    Brush.Color:= P.BackColor;
    Font.Color:= P.FontColor;
    S:= lstLegend.Items[Index];
    TextRect(Rect, S, [tfLeft]);
  end;
end;

procedure TFrameGridBase.SaveColumns;
var
  columnFilename: string;
  fs: TFileStream;
  P: PVonColumn;
  I: Integer;
begin
  if FTableName <> '' then begin
    columnFilename:= ExtractFilePath(Application.ExeName) + 'RES/' + FTableName + '.column';
    fs:= TFileStream.Create(columnFilename, fmCreate);
    for I := 0 to FColumns.Count - 1 do begin
      P:= PVonColumn(FColumns[I]);
      WriteStringToStream(P.Title, fs);
      WriteStringToStream(P.FieldName, fs);
      WriteIntToStream(Ord(P.FieldType), fs);
      WriteIntToStream(P.DisplayWidth, fs);
    end;
    fs.Free;
  end;
end;

function TFrameGridBase.IndexOf(FieldName: string): Integer;
var
  I: Integer;
  P: PVonColumn;
begin
  for I := 0 to FColumns.Count - 1 do begin
    P:= PVonColumn(FColumns[I]);
    if P.FieldName = FieldName then begin
      Result:= I;
      Exit;
    end;
  end;
  Result:= -1;
end;

(* Condition *)

procedure TFrameGridBase.ELegendGroupChange(Sender: TObject);
var
  I:Integer;
  P: PVonLegend;
begin
  FCondition:= TList(ELegendGroup.Items.Objects[ELegendGroup.ItemIndex]);
  lstLegend.Clear;
  for I := 0 to FCondition.Count - 1 do begin
    P:= PVonLegend(FCondition[I]);
    lstLegend.AddItem(P.Legend, TObject(P));
  end;
  lstLegend.AddItem('所有', nil);
  lstLegend.Height:= lstLegend.ItemHeight * lstLegend.Count + 4;
  lstLegend.Enabled:= True;
  lstLegend.Visible:= True;
  lstLegend.Top:= MaxInt;
  SplitLegend.Visible:= True;
  lstLegend.Top:= MaxInt;
  RedrewTable;
end;

procedure TFrameGridBase.ClearConditionGroup(Group: string);
var
  groupIdx: Integer;

  procedure ClearCondition(Idx: Integer);
  var
    I: Integer;
  begin
    with TList(ELegendGroup.Items.Objects[Idx]) do begin
      for I := 0 to Count - 1 do
        FreeMem(Items[I]);
      Clear;
    end;
  end;
begin
  if Group <> '' then begin
    groupIdx:= ELegendGroup.Items.IndexOf(Group);
    if groupIdx >= 0 then ClearCondition(groupIdx);
  end else begin
    for groupIdx := 0 to ELegendGroup.Items.Count - 1 do
      ClearCondition(groupIdx);
    ELegendGroup.Items.Clear;
  end;
  lstLegend.Clear;
  lstLegend.Visible:= False;
  SplitLegend.Visible:= False;
end;

procedure TFrameGridBase.RegCondition(Group, Legend, Condition: string; BackColor, FontColor: TColor);
var
  P: PVonLegend;
  Idx: Integer;
  groupLegend: TList;
begin
  if Legend = '' then Exit;
  if Condition = '' then Exit;
  Idx:= ELegendGroup.Items.IndexOf(Group);
  if Idx >= 0 then groupLegend:= TList(ELegendGroup.Items.Objects[Idx])
  else begin
    groupLegend:= TList.Create;
    ELegendGroup.AddItem(Group, groupLegend);
  end;
  New(P);
  P.Legend:= Legend;
  P.Condition:= Condition;
  P.BackColor:= BackColor;
  P.FontColor:= FontColor;
  groupLegend.Add(P);
  if ELegendGroup.Text = Group then begin
    lstLegend.AddItem(Legend, TObject(P));
    lstLegend.Height:= lstLegend.ItemHeight * lstLegend.Count + 4;
  end;
end;

(* Statistic *)

procedure TFrameGridBase.ClearStatistic;
var
  I: Integer;
begin
  for I := 0 to FStatistic.Count - 1 do
    FreeMem(FStatistic[I]);
  FStatistic.Clear;
  while Bar.Panels.Count > 2 do
    Bar.Panels.Delete(2);
end;

procedure TFrameGridBase.RegStatistic(Fieldname, HintText: string;
  Kind: TEnumStatistic; Condition: string);
var
  P: PVonStatistic;
begin
  New(P);
  P.FieldName:= Fieldname;
  P.HintText:= HintText;
  P.Kind:= Kind;
  P.Condition:= Condition;
  FStatistic.Add(P);
  Bar.Panels.Add;
end;

procedure TFrameGridBase.ReStatistic;
var
  I, Idx: Integer;
  P: PVonStatistic;
  szValues: TArrayExtended;
  statisticValue: Extended;
begin
  Bar.Panels[1].Text:= Format('%d行', [RowCount]);
  for I := 0 to FStatistic.Count - 1 do begin
    P:= PVonStatistic(FStatistic[I]);
    Idx:= IndexOf(P.FieldName);
    if Idx < 0 then Continue;
    szValues:= GetArray(Idx);
    case P.Kind of
    ES_SUM: begin
      statisticValue:= 0;
      for Idx:= 0 to SizeOf(szValues) - 1 do
        statisticValue:= statisticValue + szValues[Idx];
    end;
    ES_AVG: begin
      statisticValue:= 0;
      for Idx:= 0 to SizeOf(szValues) - 1 do
        statisticValue:= statisticValue + szValues[Idx];
      statisticValue:= statisticValue / SizeOf(szValues);
    end;
    ES_COUNT: begin statisticValue:= SizeOf(szValues); end;
    ES_MAX: begin
      statisticValue:= MinCurrency;
      for Idx:= 0 to SizeOf(szValues) - 1 do
        if szValues[Idx] > statisticValue then
          statisticValue:= szValues[Idx];
    end;
    ES_MIN: begin
      statisticValue:= MaxCurrency;
      for Idx:= 0 to SizeOf(szValues) - 1 do
        if szValues[Idx] < statisticValue then
          statisticValue:= szValues[Idx];
    end;
    ES_STDEV: begin end;
    ES_STDEVP: begin end;
    ES_VAR: begin end;
    ES_VARP: begin end;
    end;
    Bar.Panels[I].Text:= Format(P.HintText, [statisticValue]);
  end;
end;

end.
