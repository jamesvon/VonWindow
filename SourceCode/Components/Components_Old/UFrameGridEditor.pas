(*******************************************************************************
* UFrameGridEditor 一个数据表格的编辑控件，支持命令添加和赋值                  *
********************************************************************************
* 控件支持两种数据存储，一种是表格内文字数据，另一种是浮点隐含数据存储，两种数 *
* 据都可以通过字符串读取属性（Strings）读取，也可以通过浮点属性（Values）读取  *
*==============================================================================*
* RegistColumn(FieldName, Title: string; FieldWidth: Integer; CanEdit: Boolean)*
*   注册一个表格内展示数据                                                     *
*   FieldName 字段名，Title 列标题，FieldWidth 列宽，CanEdit 是否允许编辑      *
*------------------------------------------------------------------------------*
*    RegistColumn('ContractNo', '合同号', 86, False);  注册一个不可编辑的合同号*
*    RegistColumn('Price', '单价', 66, True);          注册一个可以编辑的单价项*
*==============================================================================*
* RegistField(FieldName: string)                                               *
*    注册一个表格内隐含数据 FieldName 字段名                                   *
*------------------------------------------------------------------------------*
*    RegistField('ID');  注册一个 ID 隐含值                                    *
*    RegistColumn('ContractValue');          注册一个 ContractValue 隐含变量   *
*==============================================================================*
*    表格内数据可以通过 Strings 和 Values 属性进行当前行的数据读取和赋值，也可 *
* 以通过 AddRow 和 DeleteRow 函数添加或删除行，通过 Clear 清除表内容。         *
*******************************************************************************)
unit UFrameGridEditor;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DB, GraphUtil;

type
  TEventOnDeleteRow = function(ARow: Integer): Boolean of object;
  TFrameGridEditor = class(TFrame)
    grid: TStringGrid;
    procedure gridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure gridFixedCellClick(Sender: TObject; ACol, ARow: Integer);
    procedure gridDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure gridSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
  private
    { Private declarations }
    FFields: TStringList;
    FColumns: TStringList;
    FCanDeleteRow: Boolean;
    FOnDelete: TEventOnDeleteRow;
    FOnProcess: TNotifyEvent;
    FCanAppend: Boolean;
    FDisplayIndex: Boolean;
    function GetStrings(ARow: Integer; FieldName: string): string;
    function GetValues(ARow: Integer; FieldName: string): Extended;
    procedure SetStrings(ARow: Integer; FieldName: string; const Value: string);
    procedure SetValues(ARow: Integer; FieldName: string; const Value: Extended);
    procedure SetCanDeleteRow(const Value: Boolean);
    function GetRowCount: Integer;
    function GetCells(ARow: Integer; FieldName: string): string;
    procedure SetCells(ARow: Integer; FieldName: string; const Value: string);
    procedure SetOnDelete(const Value: TEventOnDeleteRow);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    /// <summary>注册一列</summary>
    /// <param name="FieldName">字段名称</param>
    /// <param name="Title">列标题</param>
    /// <param name="FieldWidth">列宽</param>
    /// <param name="CanEdit">是否允许编辑</param>
    procedure RegistColumn(FieldName, Title: string; FieldWidth: Integer; CanEdit: Boolean);
    /// <summary>注册一个浮点数值字段</summary>
    /// <param name="FieldName">字段名称</param>
    procedure RegistField(FieldName: string);
    /// <summary>清除所有表格内容信息</summary>
    procedure Clear;
    /// <summary>显示数据集内容</summary>
    procedure Display(Data: TDataSet);
    /// <summary>插入一行</summary>
    /// <param name="ARow">插入行位置，如果为零则表示追加一行</param>
    procedure InsertRow(ARow: Integer = 0);
    /// <summary>删除一行</summary>
    /// <param name="ARow">要删除的行位置</param>
    procedure DeleteRow(ARow: Integer);
    /// <summary>追加一行数据信息</summary>
    /// <param name="Dateset">数据集，可以通过外部循环完成全部数据的加载</param>
    procedure AddRecord(Dateset: TDataSet);
    /// <summary>得到字段值所在的行号</summary>
    /// <param name="FieldName">字段名称</param>
    /// <param name="Value">字段值</param>
    /// <returns>所在行号</returns>
    function RowOf(FieldName, Value: string): Integer; overload;
    /// <summary>得到字段值所在的行号</summary>
    /// <param name="FieldName">字段名称</param>
    /// <param name="Value">字段值</param>
    /// <returns>所在行号</returns>
    function RowOf(FieldName: string; Value: Extended): Integer; overload;
    /// <summary>值信息，先字段，后单元</summary>
    /// <param name="ARow">行位置</param>
    /// <param name="FieldName">字段名称</param>
    /// <returns>浮点值</returns>
    property Values[ARow: Integer; FieldName: string]: Extended read GetValues write SetValues;
    /// <summary>单元信息</summary>
    /// <param name="ARow">行位置</param>
    /// <param name="FieldName">字段名称</param>
    /// <returns>单元格内容</returns>
    property Cells[ARow: Integer; FieldName: string]: string read GetCells write SetCells;
    /// <summary>内容信息，先单元，后字段</summary>
    /// <param name="ARow">行位置</param>
    /// <param name="FieldName">字段名称</param>
    /// <returns>字符内容</returns>
    property Strings[ARow: Integer; FieldName: string]: string read GetStrings write SetStrings;
    /// <summary>是否允许删除行，如果允许删除则第一列显示删除，如果不允许，则显示行号</summary>
    property CanDeleteRow: Boolean read FCanDeleteRow write SetCanDeleteRow;
    /// <summary>有效行数，有效行号从1开始</summary>
    property RowCount: Integer read GetRowCount;
    /// <summary>是否允许添加新行</summary>
    property CanAppend: Boolean read FCanAppend write FCanAppend;
    /// <summary>显示行号</summary>
    property DisplayIndex: Boolean read FDisplayIndex write FDisplayIndex;
    property OnDelete: TEventOnDeleteRow read FOnDelete write SetOnDelete;
    property OnProcess: TNotifyEvent read FOnProcess write FOnProcess;
  end;

implementation

{$R *.dfm}

{ TFrameGridEditor }

constructor TFrameGridEditor.Create(AOwner: TComponent);
begin
  inherited;
  FFields:= TStringList.Create;
  FColumns:= TStringList.Create;
  grid.RowCount:= 2;
  grid.Cells[0, 0]:= '操作';
end;

destructor TFrameGridEditor.Destroy;
begin
  FFields.Free;
  FColumns.Free;
  inherited;
end;

procedure TFrameGridEditor.Display(Data: TDataSet);
var
  I, Idx, RID: Integer;
begin
  Clear;
  RID:= 1;
  while not Data.Eof do begin
    InsertRow;
    for I := 0 to Data.FieldCount - 1 do begin
      Idx:= FColumns.IndexOf(Data.Fields[I].FieldName);
      if Idx >= 0 then grid.Cells[Idx + 1, RID]:= Data.Fields[I].AsString;
      Idx:= FFields.IndexOf(Data.Fields[I].FieldName);
      if Idx >= 0 then PExtended(grid.Objects[Idx, RID])^:= Data.Fields[I].AsExtended;
    end;
    if Assigned(FOnProcess) then FOnProcess(Self);
    Inc(RID);
    Data.Next;
  end;
end;

(* Property method *)

function TFrameGridEditor.GetCells(ARow: Integer; FieldName: string): string;
var
  Idx: Integer;
begin
  Result:= '';
  if ARow < 1 then ARow:= grid.Row;
  if ARow > grid.RowCount - 1 then Exit;
  Idx:= FColumns.IndexOf(FieldName);
  if Idx >= 0 then Result:= grid.Cells[Idx + 1, ARow];
end;

function TFrameGridEditor.GetRowCount: Integer;
begin
  if grid.RowCount > 2 then Result:= grid.RowCount - 1
  else if grid.Cells[0, 1] <> '' then Result:= grid.RowCount - 1
  else Result:= 0;
end;

function TFrameGridEditor.GetStrings(ARow: Integer; FieldName: string): string;
var
  Idx: Integer;
begin
  Result:= '';
  if ARow < 1 then ARow:= grid.Row;
  if ARow > grid.RowCount - 1 then Exit;
  Idx:= FColumns.IndexOf(FieldName);
  if Idx >= 0 then begin Result:= grid.Cells[Idx + 1, ARow]; Exit; end;
  Idx:= FFields.IndexOf(FieldName);
  if Idx >= 0 then
    Result:= FloatToStr(PExtended(grid.Objects[Idx, ARow])^);
end;

function TFrameGridEditor.GetValues(ARow: Integer; FieldName: string): Extended;
var
  Idx: Integer;
begin
  Result:= 0;
  if ARow < 1 then ARow:= grid.Row;
  if ARow > grid.RowCount - 1 then Exit;
  Idx:= FFields.IndexOf(FieldName);
  if Idx >= 0 then begin
    Result:= PExtended(grid.Objects[Idx, ARow])^;
    Exit;
  end;
  Idx:= FColumns.IndexOf(FieldName);
  if Idx >= 0 then TryStrToFloat(grid.Cells[Idx + 1, ARow], Result);
end;

procedure TFrameGridEditor.SetCanDeleteRow(const Value: Boolean);
begin
  FCanDeleteRow := Value;
  if FCanDeleteRow then grid.Cells[0, 0]:= '操作'
  else grid.Cells[0, 0]:= '序号';
end;

procedure TFrameGridEditor.SetCells(ARow: Integer; FieldName: string;
  const Value: string);
var
  Idx: Integer;
begin
  if ARow < 1 then ARow:= grid.Row;
  if ARow > grid.RowCount - 1 then Exit;
  Idx:= FColumns.IndexOf(FieldName);
  if Idx >= 0 then grid.Cells[Idx + 1, ARow]:= Value;
end;

procedure TFrameGridEditor.SetOnDelete(const Value: TEventOnDeleteRow);
begin
  FOnDelete := Value;
end;

procedure TFrameGridEditor.SetStrings(ARow: Integer; FieldName: string;
  const Value: string);
var
  Idx: Integer;
begin
  if ARow < 1 then ARow:= grid.RowCount - 1;
  if ARow > grid.RowCount - 1 then Exit;
  Idx:= FColumns.IndexOf(FieldName);
  if Idx >= 0 then grid.Cells[Idx + 1, ARow]:= Value;
  Idx:= FFields.IndexOf(FieldName);
  if Idx >= 0 then PExtended(grid.Objects[Idx, ARow])^:= StrToFloat(Value);
end;

procedure TFrameGridEditor.SetValues(ARow: Integer; FieldName: string;
  const Value: Extended);
var
  Idx: Integer;
begin
  if ARow < 1 then ARow:= grid.RowCount - 1;
  if ARow > grid.RowCount - 1 then Exit;
  Idx:= FColumns.IndexOf(FieldName);
  if Idx >= 0 then grid.Cells[Idx + 1, ARow]:= FloatToStr(Value);
  Idx:= FFields.IndexOf(FieldName);
  if Idx >= 0 then PExtended(grid.Objects[Idx, ARow])^:= Value;
end;

(* 注册表格内容信息和表格隐含字段信息 *)

procedure TFrameGridEditor.RegistColumn(FieldName, Title: string; FieldWidth: Integer; CanEdit: Boolean);
var
  Idx: Integer;
begin
  Idx:= FColumns.IndexOfName(FieldName);
  if Idx < 0 then Idx:= FColumns.Add(FieldName);
  grid.ColCount:= FColumns.Count + 1;
  if CanEdit then begin
    FColumns.Objects[Idx]:= TObject(1);
    grid.Col:= Idx + 1;
  end else FColumns.Objects[Idx]:= nil;
  grid.Cells[Idx + 1, 0]:= Title;
  grid.ColWidths[Idx + 1]:= FieldWidth;
end;

procedure TFrameGridEditor.RegistField(FieldName: string);
var
  P: PExtended;
  I, aCol: Integer;
begin
  aCol:= FFields.Add(FieldName);
  for I := 1 to grid.RowCount - 1 do begin
    New(P); P^:= 0;
    grid.Objects[aCol, I]:= TObject(P);
  end;
end;

function TFrameGridEditor.RowOf(FieldName, Value: string): Integer;
var
  Idx, R: Integer;
  F: Extended;
begin
  Idx:= FColumns.IndexOf(FieldName);
  if(Idx >= 0)and(grid.Cells[0, 1] <> '')then
    for Result := 1 to grid.RowCount - 1 do
      if SameText(grid.Cells[Idx + 1, Result], Value) then Exit;
  Idx:= FFields.IndexOf(FieldName);
  if(Idx >= 0)and(grid.Cells[0, 1] <> '')then
    for Result := 1 to grid.RowCount - 1 do
      if TryStrToFloat(Value, F)and(PExtended(grid.Objects[Idx, Result])^ = F)then Exit;
  Result := -1;
end;

function TFrameGridEditor.RowOf(FieldName: string; Value: Extended): Integer;
var
  Idx, R: Integer;
  F: Extended;
begin
  Idx:= FColumns.IndexOf(FieldName);
  if(Idx >= 0)and(grid.Cells[0, 1] <> '')then
    for Result := 1 to grid.RowCount - 1 do
      if TryStrToFloat(grid.Cells[Idx + 1, Result], F)and(F = Value) then Exit;
  Idx:= FFields.IndexOf(FieldName);
  if(Idx >= 0)and(grid.Cells[0, 1] <> '')then
    for Result := 1 to grid.RowCount - 1 do
      if PExtended(grid.Objects[Idx, Result])^  = Value then Exit;
  Result := -1;
end;

(* Events *)

procedure TFrameGridEditor.gridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Text: string;
  F: Extended;
  //0：上 1：右 2：下 3：左 4: 中
  procedure Drew(Colors: array of TColor);
    procedure DrewLine(X1, Y1, X2, Y2: Integer; AColor: TColor);
    begin
      with grid.Canvas do begin
        Pen.Color:= AColor;
        MoveTo(X1, Y1);
        LineTo(X2, Y2);
      end;
    end;
  begin
    with grid.Canvas do begin
      Brush.Color:= Colors[4];
      FillRect(Rect);
      DrewLine(Rect.Left, Rect.Top, Rect.Right, Rect.Top, Colors[0]);
      DrewLine(Rect.Right, Rect.Top, Rect.Right, Rect.Bottom, Colors[1]);
      DrewLine(Rect.Right, Rect.Bottom, Rect.Left, Rect.Bottom, Colors[2]);
      DrewLine(Rect.Left, Rect.Bottom, Rect.Left, Rect.Top, Colors[3]);
    end;
  end;
begin
  Text:= grid.Cells[ACol, ARow];
  grid.Canvas.Brush.Style := bsClear;
  if(gdFixed in State)and(ARow = 0)then
    GradientFillCanvas(grid.Canvas, clSilver, clWindow, Rect, gdVertical)
  else if(gdFixed in State)and(ARow > 0)then
    GradientFillCanvas(grid.Canvas, $00EAEAEA, clWindow, Rect, gdVertical)
  else if gdSelected in State then
    Drew([clBlack, clBlack, clBlack, clBlack, clWindow])
  else if gdFocused in State then
    Drew([clBlack, clBlack, clBlack, clBlack, clBlue])
  else if gdRowSelected in State then
    Drew([clBlack, clBlack, clBlack, clBlack, clGray])
  else if gdHotTrack in State then
    Drew([clBlack, clBlack, clBlack, clBlack, clHighlight])
  else if gdPressed in State then
    Drew([clBlack, clBlack, clBlack, clBlack, clSkyBlue])
  else if(FColumns.Objects[ACol - 1] = nil)and(ARow mod 2 = 1)then
    Drew([clWhite, clWhite, clSilver, clSilver, $00EAEAEA])
  else if(FColumns.Objects[ACol - 1] = nil)and(ARow mod 2 = 0)then
    Drew([clWhite, clWhite, clSilver, clSilver, clSilver])
  else if ARow mod 2 = 1 then
    Drew([clSilver, clSilver, clGray, clGray, clWindow])
  else Drew([clSilver, clSilver, clGray, clGray, clWhite]);
//  tfBottom, tfCalcRect, tfCenter, tfEditControl, tfEndEllipsis,
//  tfPathEllipsis, tfExpandTabs, tfExternalLeading, tfLeft, tfModifyString,
//  tfNoClip, tfNoPrefix, tfRight, tfRtlReading, tfSingleLine, tfTop,
//  tfVerticalCenter, tfWordBreak
  if TryStrToFloat(Text, F) then
    grid.Canvas.TextRect(Rect, Text, [tfVerticalCenter, tfRight, tfSingleLine])
  else grid.Canvas.TextRect(Rect, Text, [tfVerticalCenter, tfCenter, tfSingleLine]);
end;

procedure TFrameGridEditor.gridFixedCellClick(Sender: TObject; ACol,
  ARow: Integer);
begin
  if(ACol = 0)and(Grid.Cells[0, ARow] = '删除')then begin
    if not Assigned(FOnDelete) then DeleteRow(ARow)
    else if FOnDelete(ARow) then DeleteRow(ARow);
  end;
end;

procedure TFrameGridEditor.gridSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  CanSelect:= Integer(FColumns.Objects[ACol - 1]) = 1;
end;

procedure TFrameGridEditor.gridSetEditText(Sender: TObject; ACol, ARow: Integer;
  const Value: string);
var
  I: Integer;
begin
  if(grid.RowCount = ARow + 1)and(grid.Cells[ACol, ARow] <> '')then begin
    if FCanDeleteRow then grid.Cells[0, ARow]:= '删除';
    grid.RowCount:= grid.RowCount + 1;
  end else if(ARow < grid.RowCount - 1)and(Value = '')then begin
    for I := 1 to grid.ColCount - 1 do
      if grid.Cells[I, ARow] <> '' then Exit;
    DeleteRow(ARow);
  end;
end;

(* Methods *)

procedure TFrameGridEditor.AddRecord(Dateset: TDataSet);
var
  I: Integer;
  AFld: TField;
begin
  if Dateset.Eof then Exit;
  InsertRow;
  for I := 0 to FColumns.Count - 1 do begin
    AFld:= Dateset.FindField(FColumns[I]);
    if Assigned(AFld) then grid.Cells[I + 1, grid.Row]:= AFld.AsString;
  end;
  for I := 0 to FFields.Count - 1 do begin
    AFld:= Dateset.FindField(FFields[I]);
    if Assigned(AFld) then
      PExtended(grid.Objects[I, grid.Row])^:= AFld.AsExtended;
  end;
end;

procedure TFrameGridEditor.InsertRow(ARow: Integer);
var
  I, C: Integer;
  P: PExtended;
begin
  grid.RowCount:= grid.RowCount + 1;
  for C := 0 to FFields.Count - 1 do begin
    New(P); P^:= 0;
    grid.Objects[C, grid.RowCount - 1]:= TObject(P);
  end;
  if ARow = 0 then ARow:= grid.RowCount - 1;
  for I := grid.RowCount - 1 downto ARow + 1 do begin
    for C := 1 to grid.ColCount - 1 do
      grid.Cells[C, I]:= grid.Cells[C, I - 1];
    for C := 0 to FFields.Count - 1 do
      PExtended(grid.Objects[C, I])^:= PExtended(grid.Objects[C, I - 1])^;
  end;
  if FCanDeleteRow then begin
    grid.Cells[0, grid.RowCount - 1]:= '删除';
    grid.RowCount:= grid.RowCount + 1;
    for I := 0 to FFields.Count - 1 do begin
      New(P);  P^:= 0;
      grid.Objects[I, grid.RowCount - 1]:= TObject(P);
    end;
  end else grid.Cells[0, grid.RowCount - 1]:= IntToStr(grid.RowCount - 1);
  grid.Row:= ARow;
end;

procedure TFrameGridEditor.Clear;
var
  R, C: Integer;
  P: PExtended;
begin
  for R := grid.RowCount - 1 Downto 1 do
    DeleteRow(R);
  for C := 0 to FFields.Count - 1 do begin
    New(P); P^:= 0;
    grid.Objects[C, grid.RowCount - 1]:= TObject(P);
  end;
end;

procedure TFrameGridEditor.DeleteRow(ARow: Integer);
var
  R, C: Integer;
  P: PExtended;
begin
  if ARow < 1 then Exit;
  for C := 0 to FFields.Count - 1 do begin
    P:= PExtended(grid.Objects[C, ARow]);
    Dispose(P);
  end;
  for R := ARow to grid.RowCount - 2 do begin
    for C := 1 to grid.ColCount - 1 do
      grid.Cells[C, R]:= grid.Cells[C, R + 1];
    for C := 0 to FFields.Count - 1 do
      grid.Objects[C, R]:= grid.Objects[C, R + 1];
  end;
  grid.Rows[grid.RowCount - 1].Clear;
  if grid.RowCount > 2 then
    grid.RowCount:= grid.RowCount - 1;
  grid.Cells[0, grid.RowCount - 1]:= '';
end;

end.
