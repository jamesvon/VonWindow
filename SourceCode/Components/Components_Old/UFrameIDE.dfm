object FrameIDE: TFrameIDE
  Left = 0
  Top = 0
  Width = 595
  Height = 526
  TabOrder = 0
  object Panel1: TPanel
    Left = 496
    Top = 0
    Width = 99
    Height = 526
    Align = alRight
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    ExplicitHeight = 474
    object Label1: TLabel
      Left = 8
      Top = 143
      Width = 36
      Height = 13
      Caption = #23618#21517#31216
    end
    object Label2: TLabel
      Left = 8
      Top = 189
      Width = 36
      Height = 13
      Caption = #23618#39068#33394
    end
    object Label3: TLabel
      Left = 8
      Top = 327
      Width = 48
      Height = 13
      Caption = #21306#22495#21517#31216
    end
    object Label4: TLabel
      Left = 8
      Top = 373
      Width = 48
      Height = 13
      Caption = #21306#22495#39068#33394
    end
    object ELevelName: TEdit
      Left = 8
      Top = 162
      Width = 73
      Height = 21
      TabOrder = 0
    end
    object btnLevelAdd: TButton
      Left = 8
      Top = 236
      Width = 75
      Height = 25
      Caption = #28155#21152#23618
      TabOrder = 1
      OnClick = btnLevelAddClick
    end
    object ColorListBox1: TColorListBox
      Left = 1
      Top = 1
      Width = 97
      Height = 136
      Align = alTop
      Selected = clScrollBar
      Style = [cbCustomColors]
      TabOrder = 2
      OnClick = ColorListBox1Click
    end
    object ELevelColor: TColorBox
      Left = 8
      Top = 208
      Width = 73
      Height = 22
      Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor, cbPrettyNames, cbCustomColors]
      TabOrder = 3
    end
    object btnLevelEdit: TButton
      Left = 8
      Top = 267
      Width = 75
      Height = 25
      Caption = #20462#25913#23618
      TabOrder = 4
      OnClick = btnLevelEditClick
    end
    object btnLevelDelete: TButton
      Left = 8
      Top = 298
      Width = 75
      Height = 25
      Caption = #21024#38500#23618
      TabOrder = 5
      OnClick = btnLevelDeleteClick
    end
    object btnAreaAdd: TButton
      Left = 8
      Top = 420
      Width = 75
      Height = 25
      Caption = #28155#21152#21306#22495
      TabOrder = 6
      OnClick = btnAreaAddClick
    end
    object EAreaName: TEdit
      Left = 8
      Top = 346
      Width = 73
      Height = 21
      TabOrder = 7
      Text = 'Edit1'
    end
    object EAreaColor: TColorBox
      Left = 8
      Top = 392
      Width = 73
      Height = 22
      Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor, cbPrettyNames, cbCustomColors]
      TabOrder = 8
    end
    object btnAreaEdit: TButton
      Left = 8
      Top = 451
      Width = 75
      Height = 25
      Caption = #20462#25913#21306#22495
      TabOrder = 9
      OnClick = btnAreaEditClick
    end
    object btnAreaDelete: TButton
      Left = 8
      Top = 482
      Width = 75
      Height = 25
      Caption = #21024#38500#21306#22495
      TabOrder = 10
    end
  end
  object ScrollBox1: TScrollBox
    Left = 0
    Top = 0
    Width = 496
    Height = 526
    Align = alClient
    TabOrder = 1
    ExplicitLeft = 256
    ExplicitTop = 208
    ExplicitWidth = 185
    ExplicitHeight = 41
  end
end
