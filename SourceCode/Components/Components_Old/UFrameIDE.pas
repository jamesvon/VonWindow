unit UFrameIDE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, UVonConfig;

type
  TFrameIDE = class(TFrame)
    Panel1: TPanel;
    Label1: TLabel;
    ELevelName: TEdit;
    btnLevelAdd: TButton;
    ColorListBox1: TColorListBox;
    Label2: TLabel;
    ELevelColor: TColorBox;
    btnLevelEdit: TButton;
    btnLevelDelete: TButton;
    btnAreaAdd: TButton;
    Label3: TLabel;
    EAreaName: TEdit;
    Label4: TLabel;
    EAreaColor: TColorBox;
    btnAreaEdit: TButton;
    btnAreaDelete: TButton;
    ScrollBox1: TScrollBox;
    procedure btnAreaAddClick(Sender: TObject);
    procedure btnLevelAddClick(Sender: TObject);
    procedure btnLevelEditClick(Sender: TObject);
    procedure btnLevelDeleteClick(Sender: TObject);
    procedure ColorListBox1Click(Sender: TObject);
    procedure btnAreaEditClick(Sender: TObject);
  private
    { Private declarations }
    FList: TVonConfig;
    FAreaList: TStringList;
    procedure Clear();
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

  TArea = class(TPanel)
  private
    FDown: Boolean;
    FOldX: TPoint;
    FOldY: TPoint;
    ShapeList: Array [1 .. 8] of TShape;
    FRectList: array [1 .. 8] of TRect;
    FPosList: array [1 .. 8] of Integer;
    FOnClick: TNotifyEvent;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure aaa(var a: TWMERASEBKGND); message WM_ERASEBKGND;
    procedure WmNcHitTest(var Msg: TWmNcHitTest); message wm_NcHitTest;
    procedure WmSize(var Msg: TWmSize); message wm_Size;
    procedure WmLButtonDown(var Msg: TWmLButtonDown); message wm_LButtonDown;
    procedure WmMove(var Msg: TWmMove); message Wm_Move;
    procedure SetOnClick(const Value: TNotifyEvent);
  public
    FCanvas: TCanvas;
    FControl: TControl;
    procedure Paint; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property OnClick: TNotifyEvent read FOnClick write SetOnClick;
  end;

implementation

{$R *.dfm}

{ TArea }

procedure TArea.WmNcHitTest(var Msg: TWmNcHitTest);
var
  Pt: TPoint;
  I: Integer;
begin
  Pt := Point(Msg.XPos, Msg.YPos);
  Pt := ScreenToClient(Pt);
  Msg.Result := 0;
  for I := 1 to 8 do
    if PtInRect(FRectList[I], Pt) then
      Msg.Result := FPosList[I];
  if Msg.Result = 0 then
    inherited;
end;

procedure TArea.WmSize(var Msg: TWmSize);
var
  R: TRect;
begin
  FRectList[1] := Rect(0, 0, 5, 5);
  FRectList[2] := Rect(Width div 2 - 3, 0, Width div 2 + 2, 5);
  FRectList[3] := Rect(Width - 5, 0, Width, 5);
  FRectList[4] := Rect(Width - 5, height div 2 - 3, Width, height div 2 + 2);
  FRectList[5] := Rect(Width - 5, height - 5, Width, height);
  FRectList[6] := Rect(Width div 2 - 3, height - 5, Width div 2 + 2, height);
  FRectList[7] := Rect(0, height - 5, 5, height);
  FRectList[8] := Rect(0, height div 2 - 3, 5, height div 2 + 2);
  Paint;
end;

procedure TArea.WmLButtonDown(var Msg: TWmLButtonDown);
begin
  inherited;
end;

procedure TArea.WmMove(var Msg: TWmMove);
var
  R: TRect;
begin
  R := BoundsRect;
  InflateRect(R, -2, -2);
  Paint;
end;

constructor TArea.Create(AOwner: TComponent);
var
  I: Integer;
begin
  inherited;
  FullRepaint := false;
  FPosList[1] := htTopLeft;
  FPosList[2] := htTop;
  FPosList[3] := htTopRight;
  FPosList[4] := htRight;
  FPosList[5] := htBottomRight;
  FPosList[6] := htBottom;
  FPosList[7] := htBottomLeft;
  FPosList[8] := htLeft;
  for I := 1 to 8 do
  begin
    ShapeList[I] := TShape.Create(Self);
    ShapeList[I].Parent := Self;
    ShapeList[I].Brush.Color := clBlack;
    ShapeList[I].Visible := False;
  end;
end;

destructor TArea.Destroy;
begin
  FControl.Free;
  inherited;
end;

procedure TArea.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited;
  FDown := True;
  FOldX := Point(X, Y);
  SetFocus;
  Paint;
  if Assigned(FOnClick) then
    FOnClick(self);
end;

procedure TArea.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  NewPoint: TPoint;

  function findnearest(X, Y: Integer): TPoint;
  begin
    Result.X := (X div 5) * 5 + Round((X mod 5) / 5) * 5;
    Result.Y := (Y div 5) * 5 + Round((Y mod 5) / 5) * 5;
  end;

begin
  inherited;
  if FDown then
  begin
    NewPoint := findnearest(Left + X - FOldX.X, Top + Y - FOldX.Y);
    with Self do
      SetBounds(NewPoint.X, NewPoint.Y, Width, height);
    Paint;
  end;
end;

procedure TArea.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited;
  FDown := False;
end;

procedure TArea.Paint;
var
  I: Integer;
begin
  inherited;
  FControl.Width := BoundsRect.right - BoundsRect.Left - 10;
  FControl.height := BoundsRect.Bottom - BoundsRect.Top - 10;
  Canvas.Lock;
  Canvas.Brush.Color := Self.Color;
  Canvas.FillRect(Rect(0, 0, BoundsRect.right, BoundsRect.Bottom));
  TWinControl(FControl).PaintTo(Canvas.Handle, 5, 5);
  if Focused then
  begin
    Canvas.Brush.Color := clBlack;
    Canvas.Pen.Color := clBlack;
  end
  else
  begin
    Canvas.Brush.Color := Self.Color;
    Canvas.Pen.Color := Self.Color;
  end;
  for I := 1 to 8 do
    with FRectList[I] do
      Canvas.RecTangle(Left, Top, right, Bottom);
  Canvas.Unlock;
end;

procedure TArea.SetOnClick(const Value: TNotifyEvent);
begin
  FOnClick := Value;
end;

procedure TArea.aaa(var a: TWMERASEBKGND);
begin

end;

procedure TArea.CMExit(var Message: TCMExit);
begin
  Inherited;
  Paint;
end;

{ TFrameArea }

procedure TFrameIDE.btnAreaAddClick(Sender: TObject);
var
  szA: TArea;
  AControl : TPanel;
  ARect : TRect;
begin
  if FAreaList.IndexOf(EAreaName.Text) >= 0 then
    raise Exception.Create('区域名称不可以重复，请更名后添加。');
  szA:= TArea.Create(nil);
  with szA do begin
    FControl := TPanel.Create(Self);
    Color:= EAreaColor.Selected;
    FControl.Visible := False;
    FControl.Parent := szA;
    Parent := ScrollBox1;
    AControl := TPanel(FControl);
    //AControl.DoubleBuffered:= True;
    //AControl.ShowCaption:= False;
    AControl. Left := -10000;
    FControl.Visible := True;
    AControl.Name := EAreaName.Text;
    ARect := AControl.BoundsRect;
    InflateRect(ARect,5,5);
    BoundsRect := ARect;
    ShowHint := True;
    Left := 12;
    Top := 323;
    BevelOuter := bvNone;
    Self.Cursor := crDefault;
    SetFocus;
  end;
  FAreaList.AddObject(EAreaName.Text, szA);
end;

procedure TFrameIDE.btnAreaEditClick(Sender: TObject);
begin
  if FAreaList.IndexOf(EAreaName.Text) >= 0 then
    raise Exception.Create('区域名称不可以重复，请更名后添加。');
end;

procedure TFrameIDE.btnLevelAddClick(Sender: TObject);
begin
  if ColorListBox1.Items.IndexOf(ELevelName.Text) >= 0 then
    raise Exception.Create('这个层已经存在，请更换层名称进行存储。');
  ColorListBox1.Items.AddObject(ELevelName.Text, TObject(ELevelColor.Selected));
  ScrollBox1.Color:= ELevelColor.Selected;
end;

procedure TFrameIDE.btnLevelDeleteClick(Sender: TObject);
begin
  if ColorListBox1.Count = 0 then Exit;
  FList.EraseSection(ColorListBox1.Items[ColorListBox1.ItemIndex]);
  ColorListBox1.DeleteSelected;
end;

procedure TFrameIDE.btnLevelEditClick(Sender: TObject);
var
  Idx: Integer;
begin
  Idx:= ColorListBox1.Items.IndexOf(ELevelName.Text);
  if(Idx >= 0)and(Idx <> ColorListBox1.ItemIndex)then
    raise Exception.Create('这个层已经存在，请更换层名称进行存储。');
  if(ELevelName.Text <> ColorListBox1.Items[Idx])then begin
    FList.RenameSection(ColorListBox1.Items[ColorListBox1.ItemIndex], ELevelName.Text);
    ColorListBox1.Items[ColorListBox1.ItemIndex]:= ELevelName.Text;
  end;
  ColorListBox1.Items.Objects[ColorListBox1.ItemIndex]:= TObject(ELevelColor.Color);
end;

procedure TFrameIDE.Clear;
var
  I: Integer;
begin
  for I := 0 to FAreaList.Count - 1 do
    FAreaList.Objects[I].Free;
  FAreaList.Clear;
end;

procedure TFrameIDE.ColorListBox1Click(Sender: TObject);
begin
  ELevelName.Text:= ColorListBox1.Items[ColorListBox1.ItemIndex];
  ELevelColor.Selected:= ColorListBox1.Colors[ColorListBox1.ItemIndex];
  ScrollBox1.Color:= ELevelColor.Selected;
end;

constructor TFrameIDE.Create(AOwner: TComponent);
begin
  inherited;
  FList:= TVonConfig.Create('');
  FAreaList:= TStringList.Create;
end;

destructor TFrameIDE.Destroy;
begin
  FList.Free;
  inherited;
end;

end.
