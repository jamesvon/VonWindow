unit UFrameParamInputor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ExtCtrls, UVonSystemFuns, UVonConfig, UVonParamField, StdCtrls, ADODB;

type
  TFrameParamInputor = class(TFrame)
    ScrollBox1: TScrollBox;
    fpBackground: TFlowPanel;
    procedure FrameResize(Sender: TObject);
  private
    { Private declarations }
    FSetting: TVonParams;
    FTimer: TTimer;
    FConn: TADOConnection;
    procedure SetSetting(const Value: TVonParams);
    function GetFields(Index: Integer): TFieldBase;
    function GetFieldTypes(Index: Integer): TVonParamType;
    function GetNameField(Key: string): TFieldBase;
    function GetNameFieldType(Key: string): TVonParamType;
    function GetNameValue(Key: string): string;
    function GetValues(Index: Integer): string;
    procedure BackgroundResize(Event: TNotifyEvent);
    procedure EventOfBackgroundResize(Sender: TObject);
    procedure SetValues(Index: Integer; const Value: string);
    procedure SetNameValue(Key: string; const Value: string);
    procedure SetConn(const Value: TADOConnection);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Clear;
    property NameValue[Key: string]: string read GetNameValue write SetNameValue;
    property NameField[Key: string]: TFieldBase read GetNameField;
    property NameFieldType[Key: string]: TVonParamType read GetNameFieldType;
    property Values[Index: Integer]: string read GetValues write SetValues;
    property Fields[Index: Integer]: TFieldBase read GetFields;
    property FieldTypes[Index: Integer]: TVonParamType read GetFieldTypes;
  published
    property Setting: TVonParams read FSetting write SetSetting;
    property Conn: TADOConnection read FConn write SetConn;
  end;

implementation

{$R *.dfm}

{ TFrameParamInputor }

procedure TFrameParamInputor.Clear;
begin
  fpBackground.DestroyComponents;
end;

constructor TFrameParamInputor.Create(AOwner: TComponent);
begin
  inherited;

end;

destructor TFrameParamInputor.Destroy;
begin

  inherited;
end;

procedure TFrameParamInputor.BackgroundResize(Event: TNotifyEvent);
begin
  if not Assigned(FTimer) then FTimer:= TTimer.Create(nil);
  FTimer.Enabled:= False;
  FTimer.Interval:= 1000;
  FTimer.OnTimer:= Event;
  FTimer.Enabled:= True;
end;

procedure TFrameParamInputor.EventOfBackgroundResize(Sender: TObject);
begin
  if ScrollBox1.HorzScrollBar.IsScrollBarVisible then
    fpBackground.Height:= ScrollBox1.Height - 23
  else fpBackground.Height:= ScrollBox1.Height;
  FreeAndNil(FTimer);
end;

procedure TFrameParamInputor.FrameResize(Sender: TObject);
begin
  BackgroundResize(EventOfBackgroundResize);
end;

function TFrameParamInputor.GetFields(Index: Integer): TFieldBase;
begin
  if (Index < 0)or(Index >= fpBackground.ControlCount) then
    raise Exception.Create(RES_OUT_OF_RANG);
  Result:= TFieldBase(fpBackground.Controls[Index]);
end;

function TFrameParamInputor.GetFieldTypes(Index: Integer): TVonParamType;
begin
  if (Index < 0)or(Index >= fpBackground.ControlCount) then
    raise Exception.Create(RES_OUT_OF_RANG);
  Result:= TFieldBase(fpBackground.Controls[Index]).FieldType;
end;

function TFrameParamInputor.GetNameField(Key: string): TFieldBase;
begin
  Result:= GetFields(FSetting.IndexOfName(Key));
end;

function TFrameParamInputor.GetNameFieldType(Key: string): TVonParamType;
begin
  Result:= GetFieldTypes(FSetting.IndexOfName(Key));
end;

function TFrameParamInputor.GetNameValue(Key: string): string;
begin
  Result:= GetValues(FSetting.IndexOfName(Key));
end;

function TFrameParamInputor.GetValues(Index: Integer): string;
begin
  if (Index < 0)or(Index >= fpBackground.ControlCount) then
    raise Exception.Create(RES_OUT_OF_RANG);
  Result:= TFieldBase(fpBackground.Controls[Index]).Value;
end;

procedure TFrameParamInputor.SetConn(const Value: TADOConnection);
var
  I: Integer;
begin
  FConn := Value;
  for I := 0 to fpBackground.ControlCount - 1 do
    TFieldBase(fpBackground.Controls[I]).Conn:= FConn;
end;

procedure TFrameParamInputor.SetNameValue(Key: string; const Value: string);
begin
  SetValues(FSetting.IndexOfName(Key), Value);
end;

procedure TFrameParamInputor.SetSetting(const Value: TVonParams);
var
  I: Integer;
  R, G, B: Byte;
  Rate: Double;
begin
  Clear;
  FSetting := Value;
  R:= GetRValue(Color); G:= GetGValue(Color); B:= GetBValue(Color); Rate:= 0.618;
  for I := 0 to FSetting.Count - 1 do
    with TFieldBase.CreateByType(fpBackground, FSetting.Types[I], FConn) do begin
      Parent:= fpBackground;
      Width:= 320;
      Indent:= fpBackground.ControlCount;
      DataName:= FSetting.Names[I];
      Caption:= FSetting.Prompts[I];
      Option:= FSetting.Options[I];
      DefaultValue:= FSetting.DefaultValue[I];
      //SetColor(R, G, B, Rate);
      Show;
    end;
end;

procedure TFrameParamInputor.SetValues(Index: Integer; const Value: string);
begin
  if (Index < 0)or(Index >= fpBackground.ControlCount) then
    raise Exception.Create(RES_OUT_OF_RANG);
  TFieldBase(fpBackground.Controls[Index]).Value:= Value;
end;

end.
