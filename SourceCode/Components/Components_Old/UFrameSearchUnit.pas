unit UFrameSearchUnit;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBGrids, DB, Data.Win.ADODB, Grids, ValEdit, ComCtrls, ExtCtrls, UVonConfig,
  UVonLog, UFrameGridBar, Buttons, DBCtrls;

type
  TFrameSearchUnit = class(TFrame)
    Image1: TImage;
    PageControl1: TPageControl;
    tsGrid: TTabSheet;
    tsSearch: TTabSheet;
    tsItem: TTabSheet;
    vlData: TValueListEditor;
    vlCondition: TValueListEditor;
    DQData: TADOQuery;
    FrameGridBar1: TFrameGridBar;
    DBNavigator1: TDBNavigator;
    procedure vlConditionGetPickList(Sender: TObject; const KeyName: string;
      Values: TStrings);
    procedure PageControl1Change(Sender: TObject);
    procedure vlConditionStringsChange(Sender: TObject);
    procedure FrameGridBar1GridCellClick(Column: TColumn);
    procedure DBNavigator1Click(Sender: TObject; Button: TNavigateBtn);
    procedure DQDataAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    FSetting: TList;
    FConditionModified: Boolean;
    FOnQuery: TNotifyEvent;
    FLinkerCondition: TFrameSearchUnit;
    FOnLink: TNotifyEvent;
    function GetValues(Index: Integer): string;
    procedure SetValues(Index: Integer; const Value: string);
    function GetConditionSQL: string;
    procedure SetOnQuery(const Value: TNotifyEvent);
    procedure SetLinkerCondition(const Value: TFrameSearchUnit);
    procedure SetOnLink(const Value: TNotifyEvent);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy;
    procedure ClearCondition;
    procedure RegistSqlCondition(Prompt, ConditionFormat, SQLText: string);
    procedure RegistItemCondition(Prompt, ConditionFormat: string; Items: TStrings);
    property Values[Index: Integer]: string read GetValues write SetValues;
  published
    property ConditionSQL: string read GetConditionSQL;
    property OnQuery: TNotifyEvent read FOnQuery write SetOnQuery;
    property OnLink: TNotifyEvent read FOnLink write SetOnLink;
    property LinkerCondition: TFrameSearchUnit read FLinkerCondition write SetLinkerCondition;
  end;

implementation

type
  PConditionRecord = ^TConditionRecord;
  TConditionRecord = Record
    Items: TStrings;
    Prompt: string;
    ConditionFormat: string;
  end;

{$R *.dfm}

{ TFrameSearchUnit }

procedure TFrameSearchUnit.ClearCondition;
var
  I: Integer;
begin     //清除所有查询条件
  for I := 0 to FSetting.Count - 1 do
    FreeMem(FSetting[I]);
  FSetting.Clear;
  vlCondition.Strings.Clear;
end;

constructor TFrameSearchUnit.Create(AOwner: TComponent);
begin
  inherited;
  FSetting:= TList.Create;
  FrameGridBar1.AutoColumn:= False;
end;

procedure TFrameSearchUnit.DBNavigator1Click(Sender: TObject;
  Button: TNavigateBtn);
begin
  PageControl1Change(nil);
end;

destructor TFrameSearchUnit.Destroy;
begin
  FSetting.Free;
end;

procedure TFrameSearchUnit.DQDataAfterScroll(DataSet: TDataSet);
begin
  if Assigned(FOnLink) then
    FOnLink(Self);
end;

procedure TFrameSearchUnit.FrameGridBar1GridCellClick(Column: TColumn);
begin     //
  if Assigned(FLinkerCondition)and Assigned(FLinkerCondition.OnQuery)then
    FLinkerCondition.OnQuery(nil);
end;

function TFrameSearchUnit.GetConditionSQL: string;
var
  I, Idx: Integer;
begin
  Result:= '';
  for I := 0 to FSetting.Count - 1 do
    if(vlCondition.Cells[1, I + 1] = '所有')or(vlCondition.Cells[1, I + 1] = '')then Continue
    else if Assigned(PConditionRecord(FSetting[I]).Items) then begin
      Idx:= PConditionRecord(FSetting[I]).Items.IndexOf(vlCondition.Cells[1, I + 1]);
      if Idx < 0 then raise Exception.Create(vlCondition.Cells[0, I + 1] + '的值错误');
      if Assigned(PConditionRecord(FSetting[I]).Items.Objects[Idx]) then
        Result:= Result + ' AND ' + Format(PConditionRecord(FSetting[I]).ConditionFormat,
          [IntToStr(Integer(PConditionRecord(FSetting[I]).Items.Objects[Idx]))])
      else Result:= Result + ' AND ' + Format(PConditionRecord(FSetting[I]).ConditionFormat,
        [vlCondition.Cells[1, I + 1]]);
    end else Result:= Result + ' AND ' + Format(PConditionRecord(FSetting[I]).ConditionFormat,
        [vlCondition.Cells[1, I + 1]]);
  if Result <> '' then Delete(Result, 1, 5);
end;

function TFrameSearchUnit.GetValues(Index: Integer): string;
begin
  Result:= vlCondition.Cells[1, Index + 1];
end;

procedure TFrameSearchUnit.PageControl1Change(Sender: TObject);
var
  I: Integer;
begin
  if(PageControl1.ActivePageIndex > 0)and FConditionModified then begin
    DQData.AfterScroll:= nil;
    if Assigned(FOnQuery) then
      FOnQuery(Self);
    FConditionModified:= False;
    DQData.AfterScroll:= DQDataAfterScroll;
  end;
  if(PageControl1.ActivePageIndex > 1)and DQData.Active then begin
    vlData.Strings.Clear;
    for I := 0 to FrameGridBar1.Grid.Columns.Count - 1 do
      vlData.InsertRow(FrameGridBar1.Grid.Columns[I].Title.Caption,
        FrameGridBar1.Grid.Columns[I].Field.AsString, True);
  end;
end;

procedure TFrameSearchUnit.RegistItemCondition(Prompt, ConditionFormat: string; Items: TStrings);
var
  R: PConditionRecord;
begin
  New(R);
  R.Prompt:= Prompt;
  R.ConditionFormat:= ConditionFormat;
  R.Items:= Items;
  if Assigned(Items) then
    vlCondition.Strings.AddObject(Prompt + '=所有', nil)
  else vlCondition.Strings.AddObject(Prompt + '=', nil);
  FSetting.Add(R);
end;

procedure TFrameSearchUnit.RegistSqlCondition(Prompt, ConditionFormat, SQLText: string);
var
  FItems: TStringList;
begin
  if Assigned(DQData.Connection) then
    with TADOQuery.Create(nil)do try
      Connection:= DQData.Connection;
      SQL.Text:= SQLText;
      Open;
      FItems:= TStringList.Create;
      while not EOF do begin
        if Fields[0].FieldName = 'ID' then
          FItems.AddObject(Fields[1].AsString, TObject(Fields[0].AsInteger))
        else FItems.Add(Fields[0].AsString);
        Next;
      end;
      RegistItemCondition(Prompt, ConditionFormat, FItems);
    finally
      Free;
    end;
end;

procedure TFrameSearchUnit.SetLinkerCondition(const Value: TFrameSearchUnit);
begin
  FLinkerCondition := Value;
end;

procedure TFrameSearchUnit.SetOnLink(const Value: TNotifyEvent);
begin
  FOnLink := Value;
end;

procedure TFrameSearchUnit.SetOnQuery(const Value: TNotifyEvent);
begin
  FOnQuery := Value;
end;

procedure TFrameSearchUnit.SetValues(Index: Integer; const Value: string);
begin
  vlCondition.Cells[1, Index + 1]:= Value;
end;

procedure TFrameSearchUnit.vlConditionGetPickList(Sender: TObject;
  const KeyName: string; Values: TStrings);
begin
  if Assigned(PConditionRecord(FSetting[vlCondition.Row - 1]).Items) then
    Values.Assign(PConditionRecord(FSetting[vlCondition.Row - 1]).Items);
end;

procedure TFrameSearchUnit.vlConditionStringsChange(Sender: TObject);
begin
  FConditionModified:= True;
end;

end.
