(*******************************************************************************
* UFrameStringGridBar v1.0 written by James Von (jamesvon@163.com) *************
********************************************************************************
* TFrameStringGridBar                                                          *
*------------------------------------------------------------------------------*
*     一个表格控件,该控件支持表格式文字录入,支持表头设置,以及不同类型的数据录入*
*------------------------------------------------------------------------------*
* InitColumn(ColumnSetting: TStrings); 设置列信息                              *
*   ColumnSetting的格式说明:
*   项目名称=TYPE(<type>)WIDTH(<width>)SQL(<sql>)LIST(<list_item>)
*     <type>:
*       -- 0,SYS:系统设置项目
*            FIXCOL(<FixColCount>)     固定列数
*       -- 1,STR:文字型项目
*       -- 2,NUM:数字型项目
*       -- 3,SQL:根据SQL语句提取下拉项目,如果有两个字段,显示第一个字段,记录第二个
*       -- 4,LST:下拉列表(name=value),显示名称,记录Value
*       -- 5,DAT:日期项目
*       -- 6,TIM:时间项目
*     <list_item>: 以 | 为间隔的数据列表
*******************************************************************************)
unit UFrameStringGridBar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ComCtrls, ImgList, ToolWin, Grids, UVonConfig, StdCtrls, ADODB,
  System.ImageList;

type
  TFrameStringGridBar = class(TFrame)
    ToolBar1: TToolBar;
    ImageList1: TImageList;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    grid: TStringGrid;
    EText: TEdit;
    EItem: TComboBox;
    EDate: TDateTimePicker;
    ENum: TEdit;
    ETime: TDateTimePicker;
    procedure gridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure ETextExit(Sender: TObject);
    procedure ENumExit(Sender: TObject);
    procedure EItemExit(Sender: TObject);
    procedure EDateExit(Sender: TObject);
    procedure ETimeExit(Sender: TObject);
  private
    { Private declarations }
    FRowCount: Integer;
    FInculedDelete: Boolean;
    FCol: Integer;
    FRow: Integer;
    FColCount: Integer;
    FConnection: TADOConnection;
    FSetting : TVonSetting;
    function GetCells(ACol, ARow: Integer): string;
    procedure SetCells(ACol, ARow: Integer; const Value: string);
    procedure SetCol(const Value: Integer);
    procedure SetColCount(const Value: Integer);
    procedure SetInculedDelete(const Value: Boolean);
    procedure SetRow(const Value: Integer);
    procedure SetRowCount(const Value: Integer);
    procedure SetConnection(const Value: TADOConnection);
    function GetCol: Integer;
    function GetRow: Integer;
    procedure EndOfEdit(Sender: TWinControl; Value: string);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure InitColumn(ColumnSetting: TStrings);
    ///<summary>设置本字符表格的内容</summary>
    procedure SetData(Data: TVonTable; WithTitle: Boolean);
    ///<summary>得到本字符表格的内容</summary>
    procedure GetData(Data: TVonTable; WithTitle: Boolean);
    property Cells[ACol, ARow: Integer]: string read GetCells write SetCells;
    procedure Clear;
    procedure AddColumn(Title, Setting: string);
  published
    property RowCount: Integer read FRowCount write SetRowCount;
    property ColCount: Integer read FColCount write SetColCount;
    property InculedDelete: Boolean read FInculedDelete write SetInculedDelete;
    property Col: Integer read GetCol write SetCol;
    property Row: Integer read GetRow write SetRow;
    property Connection: TADOConnection read FConnection write SetConnection;
  end;

implementation

uses UPlatformDB;

{$R *.dfm}

{ TFrameStringGridBar }

procedure TFrameStringGridBar.Clear;
var
  R, C: Integer;
begin
  EText.Visible:= False;
  EItem.Visible:= False;
  EDate.Visible:= False;
  ENum.Visible:= False;
  ETime.Visible:= False;
  for R := 1 to grid.RowCount - 1 do
    for C := 1 to grid.ColCount - 1 do
      grid.Cells[C, R]:= '';
  grid.RowCount:= 2;
  grid.ColCount:= 1;
  FColCount:= 0;
  FRowCount:= 0;
end;

(* Event when exit the component *)

constructor TFrameStringGridBar.Create(AOwner: TComponent);
begin
  inherited;
  FSetting := TVonSetting.Create;
end;

destructor TFrameStringGridBar.Destroy;
begin
  FSetting.Free;
  inherited;
end;

procedure TFrameStringGridBar.EDateExit(Sender: TObject);
begin
  EndOfEdit(EDate, DateToStr(EDate.Date));
end;

procedure TFrameStringGridBar.EItemExit(Sender: TObject);
begin
  EndOfEdit(EItem, EItem.Text);
end;

procedure TFrameStringGridBar.EndOfEdit(Sender: TWinControl; Value: string);
var
  R, C: Integer;
begin
  grid.MouseToCell(Sender.Left + Sender.Width div 2 - grid.Left,
    Sender.Top + Sender.Height div 2 - grid.Top, C, R);
  SetCells(C, R - 1, Value);
  Sender.Visible:= False;
end;

procedure TFrameStringGridBar.ENumExit(Sender: TObject);
var
  szNum: Extended;
begin
  if TryStrToFloat(ENum.Text, szNum) then EndOfEdit(ENum, ENum.Text)
  else EndOfEdit(ENum, '0');
end;

procedure TFrameStringGridBar.ETextExit(Sender: TObject);
begin
  EndOfEdit(EText, EText.Text)
end;

procedure TFrameStringGridBar.ETimeExit(Sender: TObject);
begin
  EndOfEdit(ETime, TimeToStr(ETime.Time))
end;

procedure TFrameStringGridBar.gridSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
  CellRect: TRect;
begin
  CellRect:= grid.CellRect(ACol, ARow);
  if grid.Objects[ACol, 0] = EText then begin
    {$region 'string Edit'}
    EText.Text:= grid.Cells[ACol, ARow];
    EText.SetBounds(CellRect.Left + grid.Left + 1, CellRect.Top + grid.Top + 1,
      CellRect.Right - CellRect.Left + 2, CellRect.Bottom - CellRect.Top + 2);
    EText.Visible:= True;
    EText.SetFocus;
    {$endregion}
  end else if grid.Objects[ACol, 0] = EDate then begin
    {$region 'date Edit'}
    EDate.Date:= StrToDate(grid.Cells[ACol, ARow]);
    EDate.SetBounds(CellRect.Left + grid.Left + 1, CellRect.Top + grid.Top + 1,
      CellRect.Right - CellRect.Left + 2, CellRect.Bottom - CellRect.Top + 2);
    EDate.Visible:= True;
    EDate.SetFocus;
    {$endregion}
  end else if grid.Objects[ACol, 0] = ETime then begin
    {$region 'date Edit'}
    ETime.Date:= StrToTime(grid.Cells[ACol, ARow]);
    ETime.SetBounds(CellRect.Left + grid.Left + 1, CellRect.Top + grid.Top + 1,
      CellRect.Right - CellRect.Left + 2, CellRect.Bottom - CellRect.Top + 2);
    ETime.Visible:= True;
    ETime.SetFocus;
    {$endregion}
  end else if Assigned(grid.Objects[ACol, 0]) and(grid.Objects[ACol, 0].Classname = 'TStringList')then begin
    {$region 'item Edit'}
    EItem.SetBounds(CellRect.Left + grid.Left + 1, CellRect.Top + grid.Top + 1,
      CellRect.Right - CellRect.Left + 2, CellRect.Bottom - CellRect.Top + 2);
    EItem.Items.Assign(TStringList(grid.Objects[ACol, 0]));
    EItem.Visible:= True;
    EItem.ItemIndex:= EItem.Items.IndexOf(grid.Cells[ACol, ARow]);
    EItem.SetFocus;
    {$endregion}
  end;
end;

(* Properties *)

function TFrameStringGridBar.GetCells(ACol, ARow: Integer): string;
begin
  Result:= grid.Cells[ACol, ARow + 1];
end;

function TFrameStringGridBar.GetCol: Integer;
begin
  Result:= Grid.Col;
end;

function TFrameStringGridBar.GetRow: Integer;
begin
  Result:= Grid.Row - 1;
end;

procedure TFrameStringGridBar.SetCells(ACol, ARow: Integer;
  const Value: string);
begin
  if(Value <> '')and(grid.RowCount - 1 = ARow + 1)then
    grid.RowCount:= grid.RowCount + 1;
  grid.Cells[ACol, ARow + 1]:= Value;
end;

procedure TFrameStringGridBar.SetCol(const Value: Integer);
begin
  Grid.Col := Value;
end;

procedure TFrameStringGridBar.SetColCount(const Value: Integer);
begin
  FColCount := Value;
  if FColCount < 1 then grid.ColCount:= 1
  else grid.ColCount:= FColCount;
end;

procedure TFrameStringGridBar.SetConnection(const Value: TADOConnection);
begin
  FConnection := Value;
end;

procedure TFrameStringGridBar.SetRow(const Value: Integer);
begin
  FRow := Value;
end;

procedure TFrameStringGridBar.SetRowCount(const Value: Integer);
begin
  grid.RowCount:= FRowCount + 1;
end;

(* Methods *)

procedure TFrameStringGridBar.AddColumn(Title, Setting: string);
var
  Idx: Integer;
  szList: TStringList;
begin
  FSetting.Text[VSK_SEMICOLON]:= Setting;
    if FSetting.NameValue['TYPE'] = 'SYS' then begin      //系统设置项目
      //grid.ColCount:= grid.ColCount - 1;
    end else begin  //项目名称=TYPE(<type>)WIDTH(<width>)SQL(<sql>)LIST(<list_item>)
      ColCount:= ColCount + 1;
      grid.Cells[FColCount - 1, 0]:= Title;
      if FSetting.IndexOfName('WIDTH') >= 0 then
        grid.ColWidths[FColCount - 1]:= StrToInt(FSetting.NameValue['WIDTH']);
      if FSetting.NameValue['TYPE'] = '' then
        grid.Objects[FColCount - 1, 0]:= EText
      else if FSetting.NameValue['TYPE'] = 'STR' then
        grid.Objects[FColCount - 1, 0]:= EText
      else if FSetting.NameValue['TYPE'] = 'NUM' then
        grid.Objects[FColCount - 1, 0]:= ENum
      else if FSetting.NameValue['TYPE'] = 'SQL' then begin
        {$region 'SQL-Item'}
        szList:= TStringList.Create;
        grid.Objects[FColCount - 1, 0]:= szList;
        with TADOQuery.Create(nil) do try
          Connection:= FConnection;
          SQL.Text:= FSetting.NameValue['SQL'];
          Open;
          while not EOF do begin
            if FieldCount > 1 then
              szList.Add(Fields[0].AsString + '=' + Fields[1].AsString)
            else szList.Add(Fields[0].AsString + '=' + Fields[0].AsString);
            Next;
          end;
        finally
          Free;
        end;
        {$endregion}
      end else if FSetting.NameValue['TYPE'] = 'OPT' then begin
        {$region 'Option-Item'}
        szList:= TStringList.Create;
        grid.Objects[FColCount - 1, 0]:= szList;
        FPlatformDB.GetListOption(FVonSetting.NameValue['OPT'], szList);
        {$endregion}
      end else if FSetting.NameValue['TYPE'] = 'LST' then begin
        {$region 'List-Item'}
        szList:= TStringList.Create;
        grid.Objects[FColCount - 1, 0]:= szList;
        szList.Delimiter:= '|';
        szList.DelimitedText:= FSetting.NameValue['LIST'];
        {$endregion}
      end else if FSetting.NameValue['TYPE'] = 'DAT' then
        grid.Objects[FColCount - 1, 0]:= EDate
      else if FSetting.NameValue['TYPE'] = 'TIM' then
        grid.Objects[FColCount - 1, 0]:= ETime;
    end;
end;

procedure TFrameStringGridBar.InitColumn(ColumnSetting: TStrings);
var
  I: Integer;
begin
  ColCount:= 0;
  RowCount:= 0;
  for I:= 0 to ColumnSetting.Count - 1 do
    AddColumn(ColumnSetting.Names[I], ColumnSetting.ValueFromIndex[I])
end;

procedure TFrameStringGridBar.SetData(Data: TVonTable; WithTitle: Boolean);
var
  R, C, ROffset: Integer;
begin
  Clear;
  if WithTitle then begin
    grid.RowCount:= Data.RowCount;
    ROffset:= 0;
  end else begin
    ROffset:= 1;
    grid.RowCount:= Data.RowCount + 1;
  end;
  grid.ColCount:= Data.ColCount;
  for R := 0 to Data.RowCount - 1 do
    for C := 0 to Data.ColCount - 1 do
      grid.Cells[C, R + ROffset]:= Data.Cells[C, R];
end;

procedure TFrameStringGridBar.GetData(Data: TVonTable; WithTitle: Boolean);
var
  R, C, ROffset: Integer;
begin
  Data.Clear;
  Data.ColCount:= grid.ColCount;
  if WithTitle then begin
    ROffset:= 0;
    Data.RowCount:= grid.RowCount;
    for C := 0 to grid.ColCount - 1 do
      Data.Cells[C, 0]:= grid.Cells[C, 0];
  end else begin
    ROffset:= 1;
    Data.RowCount:= grid.RowCount - 1;
  end;
  for R := 1 to grid.RowCount - 1 do
    for C := 0 to grid.ColCount - 1 do
      Data.Cells[C, R - ROffset]:= grid.Cells[C, R];
end;

procedure TFrameStringGridBar.SetInculedDelete(const Value: Boolean);
begin
  FInculedDelete := Value;
end;

end.
