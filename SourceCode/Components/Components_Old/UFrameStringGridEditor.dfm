object FrameStriingGridEditor: TFrameStriingGridEditor
  Left = 0
  Top = 0
  Width = 451
  Height = 305
  Align = alClient
  TabOrder = 0
  object grid: TStringGrid
    Left = 0
    Top = 0
    Width = 451
    Height = 305
    Align = alClient
    DefaultColWidth = 30
    DefaultRowHeight = 18
    DefaultDrawing = False
    DoubleBuffered = False
    DrawingStyle = gdsGradient
    FixedColor = clPurple
    GradientEndColor = clBlack
    GradientStartColor = 15395562
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing, goFixedColClick]
    ParentDoubleBuffered = False
    TabOrder = 0
    OnDrawCell = gridDrawCell
    OnFixedCellClick = gridFixedCellClick
    OnKeyUp = gridKeyUp
    OnSelectCell = gridSelectCell
    OnSetEditText = gridSetEditText
  end
end
