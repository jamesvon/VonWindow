(*******************************************************************************
* UFrameGridEditor 一个数据表格的编辑控件，支持命令添加和赋值                  *
********************************************************************************
* 控件支持两种数据存储，一种是表格内文字数据，另一种是浮点隐含数据存储，两种数 *
* 据都可以通过字符串读取属性（Strings）读取，也可以通过浮点属性（Values）读取  *
*==============================================================================*
* RegistColumn(FieldName, Title: string; FieldWidth: Integer; CanEdit: Boolean)*
*   注册一个表格内展示数据                                                     *
*   FieldName 字段名，Title 列标题，FieldWidth 列宽，CanEdit 是否允许编辑      *
*------------------------------------------------------------------------------*
*    RegistColumn('ContractNo', '合同号', 86, False);  注册一个不可编辑的合同号*
*    RegistColumn('Price', '单价', 66, True);          注册一个可以编辑的单价项*
*==============================================================================*
* RegistField(FieldName: string)                                               *
*    注册一个表格内隐含数据 FieldName 字段名                                   *
*------------------------------------------------------------------------------*
*    RegistField('ID');  注册一个 ID 隐含值                                    *
*    RegistColumn('ContractValue');          注册一个 ContractValue 隐含变量   *
*==============================================================================*
*    表格内数据可以通过 Strings 和 Values 属性进行当前行的数据读取和赋值，也可 *
* 以通过 AddRow 和 DeleteRow 函数添加或删除行，通过 Clear 清除表内容。         *
*******************************************************************************)
unit UFrameStringGridEditor;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DB, GraphUtil, UVonOffice;

type
  TEventOnDeleteRow = function(ARow: Integer): Boolean of object;
  TFrameStriingGridEditor = class(TFrame)
    grid: TStringGrid;
    procedure gridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure gridFixedCellClick(Sender: TObject; ACol, ARow: Integer);
    procedure gridDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure gridSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure gridKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FFields: TStringList;
    FSystemColCount: Integer;
    FColumns: TStringList;
    FCanDeleteRow: Boolean;
    FOnDelete: TEventOnDeleteRow;
    FOnProcess: TNotifyEvent;
    FCanAppend: Boolean;
    FDisplayIndex: Boolean;
    FCanInsertRow: Boolean;
    function GetStrings(ARow: Integer; FieldName: string): string;
    function GetValues(ARow: Integer; FieldName: string): Extended;
    procedure SetStrings(ARow: Integer; FieldName: string; const Value: string);
    procedure SetValues(ARow: Integer; FieldName: string; const Value: Extended);
    procedure SetCanDeleteRow(const Value: Boolean);
    function GetRowCount: Integer;
    function GetCells(ARow: Integer; FieldName: string): string;
    procedure SetCells(ARow: Integer; FieldName: string; const Value: string);
    procedure SetOnDelete(const Value: TEventOnDeleteRow);
    procedure SetCanInsertRow(const Value: Boolean);
    procedure RemoveCol(aCol: Integer);
    procedure InsertCol(aCol: Integer; Title: string);
    procedure SetDisplayIndex(const Value: Boolean);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    /// <summary>注册一列</summary>
    /// <param name="FieldName">字段名称</param>
    /// <param name="Title">列标题</param>
    /// <param name="FieldWidth">列宽</param>
    /// <param name="CanEdit">是否允许编辑</param>
    procedure RegistColumn(FieldName, Title: string; FieldWidth: Integer; CanEdit: Boolean);
    /// <summary>注册一个浮点数值字段</summary>
    /// <param name="FieldName">字段名称</param>
    procedure RegistField(FieldName: string);
    /// <summary>清除所有表格内容信息</summary>
    procedure Clear;
    /// <summary>显示数据集内容</summary>
    procedure Display(Data: TDataSet); overload;
    /// <summary>显示数据集内容</summary>
    procedure Display(Data: TStrings); overload;
    /// <summary>显示数据集内容</summary>
    procedure ExportFile(Filename: string);
    /// <summary>插入一行</summary>
    /// <param name="ARow">插入行位置，如果为零则表示追加一行</param>
    function InsertRow(ARow: Integer = 0): Integer;
    /// <summary>删除一行</summary>
    /// <param name="ARow">要删除的行位置</param>
    procedure DeleteRow(ARow: Integer);
    /// <summary>追加一行数据信息</summary>
    /// <param name="Dateset">数据集，可以通过外部循环完成全部数据的加载</param>
    procedure AddRecord(Dateset: TDataSet);
    /// <summary>得到字段值所在的行号</summary>
    /// <param name="FieldName">字段名称</param>
    /// <param name="Value">字段值</param>
    /// <returns>所在行号</returns>
    function RowOf(FieldName, Value: string): Integer; overload;
    /// <summary>得到字段值所在的行号</summary>
    /// <param name="FieldName">字段名称</param>
    /// <param name="Value">字段值</param>
    /// <returns>所在行号</returns>
    function RowOf(FieldName: string; Value: Extended): Integer; overload;
    /// <summary>值信息，先字段，后单元</summary>
    /// <param name="ARow">行位置</param>
    /// <param name="FieldName">字段名称</param>
    /// <returns>浮点值</returns>
    property Values[ARow: Integer; FieldName: string]: Extended read GetValues write SetValues;
    /// <summary>单元信息</summary>
    /// <param name="ARow">行位置</param>
    /// <param name="FieldName">字段名称</param>
    /// <returns>单元格内容</returns>
    property Cells[ARow: Integer; FieldName: string]: string read GetCells write SetCells;
    /// <summary>内容信息，先单元，后字段</summary>
    /// <param name="ARow">行位置</param>
    /// <param name="FieldName">字段名称</param>
    /// <returns>字符内容</returns>
    property Strings[ARow: Integer; FieldName: string]: string read GetStrings write SetStrings;
    /// <summary>有效行数，有效行号从1开始</summary>
    property RowCount: Integer read GetRowCount;
    /// <summary>是否允许删除行，如果允许删除则第一列显示删除，如果不允许，则不显示</summary>
    property CanDeleteRow: Boolean read FCanDeleteRow write SetCanDeleteRow;
    /// <summary>是否允许插入行，如果允许插入则第一列显示插入，如果不允许，则不显示</summary>
    property CanInsertRow: Boolean read FCanInsertRow write SetCanInsertRow;
    /// <summary>是否允许添加新行</summary>
    property CanAppend: Boolean read FCanAppend write FCanAppend;
    /// <summary>显示行号</summary>
    property DisplayIndex: Boolean read FDisplayIndex write SetDisplayIndex;
    property OnDelete: TEventOnDeleteRow read FOnDelete write SetOnDelete;
    property OnProcess: TNotifyEvent read FOnProcess write FOnProcess;
  end;

implementation

{$R *.dfm}

{ TFrameGridEditor }

constructor TFrameStriingGridEditor.Create(AOwner: TComponent);
begin
  inherited;
  FFields:= TStringList.Create;
  FColumns:= TStringList.Create;
  grid.RowCount:= 2;
  FSystemColCount:= 0;
end;

destructor TFrameStriingGridEditor.Destroy;
begin
  FFields.Free;
  FColumns.Free;
  inherited;
end;

procedure TFrameStriingGridEditor.Display(Data: TStrings);
var
  I, Idx, RID: Integer;
begin
  RID:= InsertRow;
  for I := 0 to Data.Count - 1 do begin
    Idx:= FColumns.IndexOf(Data.Names[I]);
    if Idx >= 0 then grid.Cells[Idx + FSystemColCount, RID]:= Data.ValueFromIndex[I];
    Idx:= FFields.IndexOf(Data.Names[I]);
    if Idx >= 0 then PExtended(grid.Objects[Idx, RID])^:= StrToFloat(Data.ValueFromIndex[I]);
  end;
  if Assigned(FOnProcess) then FOnProcess(Self);
end;

procedure TFrameStriingGridEditor.Display(Data: TDataSet);
var
  I, Idx, RID: Integer;
begin
  Clear;
  RID:= 1;
  while not Data.Eof do begin
    InsertRow;
    for I := 0 to Data.FieldCount - 1 do begin
      Idx:= FColumns.IndexOf(Data.Fields[I].FieldName);
      if Idx >= 0 then grid.Cells[Idx + FSystemColCount, RID]:= Data.Fields[I].AsString;
      Idx:= FFields.IndexOf(Data.Fields[I].FieldName);
      if Idx >= 0 then PExtended(grid.Objects[Idx, RID])^:= Data.Fields[I].AsExtended;
    end;
    if Assigned(FOnProcess) then FOnProcess(Self);
    Inc(RID);
    Data.Next;
  end;
end;

procedure TFrameStriingGridEditor.ExportFile(Filename: string);
var
  aRow, aCol: Integer;
  office: TVonExcelWriter;
begin
  office:= TVonExcelWriter.Create(Filename);
  for aRow := 0 to grid.RowCount - 1 do
    for aCol := 0 to grid.ColCount - 1 do
      office.WriteCell(aCol, aRow, grid.Cells[aCol, aRow]);
  office.Free;
end;

(* Cells and fields method *)

function TFrameStriingGridEditor.GetCells(ARow: Integer; FieldName: string): string;
var
  Idx: Integer;
begin
  Result:= '';
  if ARow < 1 then ARow:= grid.Row;
  if(CanAppend and(ARow >= grid.RowCount - 1))or(ARow > grid.RowCount - 1) then Exit;
  Idx:= FColumns.IndexOf(FieldName);
  if Idx >= 0 then Result:= grid.Cells[Idx + FSystemColCount, ARow];
end;

function TFrameStriingGridEditor.GetRowCount: Integer;
begin
  if grid.RowCount > 2 then Result:= grid.RowCount - 1
  else if grid.Cells[0, 1] <> '' then Result:= grid.RowCount - 1
  else Result:= 0;
end;

function TFrameStriingGridEditor.GetStrings(ARow: Integer; FieldName: string): string;
var
  Idx: Integer;
begin
  Result:= '';
  if ARow < 1 then ARow:= grid.Row;
  if ARow > grid.RowCount - 1 then Exit;
  Idx:= FColumns.IndexOf(FieldName);
  if Idx >= 0 then begin Result:= grid.Cells[Idx + FSystemColCount, ARow]; Exit; end;
  Idx:= FFields.IndexOf(FieldName);
  if Idx >= 0 then
    Result:= FloatToStr(PExtended(grid.Objects[Idx, ARow])^);
end;

function TFrameStriingGridEditor.GetValues(ARow: Integer; FieldName: string): Extended;
var
  Idx: Integer;
begin
  Result:= 0;
  if ARow < 1 then ARow:= grid.Row;
  if ARow > grid.RowCount - 1 then Exit;
  Idx:= FFields.IndexOf(FieldName);
  if Idx >= 0 then begin
    Result:= PExtended(grid.Objects[Idx, ARow])^;
    Exit;
  end;
  Idx:= FColumns.IndexOf(FieldName);
  if Idx >= 0 then TryStrToFloat(grid.Cells[Idx + FSystemColCount, ARow], Result);
end;

procedure TFrameStriingGridEditor.SetCells(ARow: Integer; FieldName: string;
  const Value: string);
var
  Idx: Integer;
begin
  if ARow < 1 then ARow:= grid.Row;
  if FCanAppend then
    while(ARow >= grid.RowCount - 1)do InsertRow(grid.RowCount);
  if ARow > grid.RowCount - 1 then Exit;
  Idx:= FColumns.IndexOf(FieldName);
  if Idx >= 0 then grid.Cells[Idx + FSystemColCount, ARow]:= Value;
end;

procedure TFrameStriingGridEditor.SetStrings(ARow: Integer; FieldName: string;
  const Value: string);
var
  Idx: Integer;
begin
  if ARow < 1 then ARow:= grid.RowCount - 1;
  if FCanAppend then
    while(ARow >= grid.RowCount - 1)do InsertRow(grid.RowCount);
  if ARow > grid.RowCount - 1 then Exit;
  Idx:= FColumns.IndexOf(FieldName);
  if Idx >= 0 then grid.Cells[Idx + FSystemColCount, ARow]:= Value;
  Idx:= FFields.IndexOf(FieldName);
  if Idx >= 0 then PExtended(grid.Objects[Idx, ARow])^:= StrToFloat(Value);
end;

procedure TFrameStriingGridEditor.SetValues(ARow: Integer; FieldName: string;
  const Value: Extended);
var
  Idx: Integer;
begin
  if ARow < 1 then ARow:= grid.RowCount - 1;
  if FCanAppend then
    while(ARow >= grid.RowCount - 1)do InsertRow(grid.RowCount);
  if ARow > grid.RowCount - 1 then Exit;
  Idx:= FColumns.IndexOf(FieldName);
  if Idx >= 0 then grid.Cells[Idx + grid.FixedCols, ARow]:= FloatToStr(Value);
  Idx:= FFields.IndexOf(FieldName);
  if Idx >= 0 then PExtended(grid.Objects[Idx, ARow])^:= Value;
end;

(* properties *)

procedure TFrameStriingGridEditor.RemoveCol(aCol: Integer);
var
  I: Integer;
begin
  for I := aCol to grid.ColCount - 2 do
    grid.Cols[I].Assign(grid.Cols[I + 1]);
  grid.ColCount:= grid.ColCount - 1;
end;

procedure TFrameStriingGridEditor.InsertCol(aCol: Integer; Title: string);
var
  I: Integer;
begin
  for I := grid.ColCount - 1 downto aCol + 1 do
    grid.Cols[I].Assign(grid.Cols[I - 1]);
  grid.ColCount:= grid.ColCount + 1;
  if aCol < FSystemColCount then
    for I := 0 to grid.RowCount - 1 do
      grid.Cells[aCol, I]:= Title;
end;

procedure TFrameStriingGridEditor.SetCanDeleteRow(const Value: Boolean);
begin
  FCanDeleteRow := Value;
end;

procedure TFrameStriingGridEditor.SetCanInsertRow(const Value: Boolean);
begin
  FCanInsertRow := Value;
end;

procedure TFrameStriingGridEditor.SetDisplayIndex(const Value: Boolean);
begin
  FDisplayIndex := Value;
end;

procedure TFrameStriingGridEditor.SetOnDelete(const Value: TEventOnDeleteRow);
begin
  FOnDelete := Value;
end;

(* 注册表格内容信息和表格隐含字段信息 *)

procedure TFrameStriingGridEditor.RegistColumn(FieldName, Title: string; FieldWidth: Integer; CanEdit: Boolean);
var
  Idx: Integer;
begin
  Idx:= FColumns.IndexOfName(FieldName);
  if Idx < 0 then Idx:= FColumns.Add(FieldName);
  grid.ColCount:= FColumns.Count + FSystemColCount;
  if CanEdit then begin
    FColumns.Objects[Idx]:= TObject(1);
    grid.Col:= Idx + FSystemColCount;
  end else FColumns.Objects[Idx]:= nil;
  grid.Cells[Idx + FSystemColCount, 0]:= Title;
  grid.ColWidths[Idx + FSystemColCount]:= FieldWidth;
end;

procedure TFrameStriingGridEditor.RegistField(FieldName: string);
var
  P: PExtended;
  I, aCol: Integer;
begin
  aCol:= FFields.Add(FieldName);
  for I := 1 to grid.RowCount - 1 do begin
    New(P); P^:= 0;
    grid.Objects[aCol, I]:= TObject(P);
  end;
end;

function TFrameStriingGridEditor.RowOf(FieldName, Value: string): Integer;
var
  Idx, R: Integer;
  F: Extended;
begin
  Idx:= FColumns.IndexOf(FieldName);
  if(Idx >= 0)and(grid.Cells[0, 1] <> '')then
    for Result := 1 to grid.RowCount - 1 do
      if SameText(grid.Cells[Idx + FSystemColCount, Result], Value) then Exit;
  Idx:= FFields.IndexOf(FieldName);
  if(Idx >= 0)and(grid.Cells[0, 1] <> '')then
    for Result := 1 to grid.RowCount - 1 do
      if TryStrToFloat(Value, F)and(PExtended(grid.Objects[Idx, Result])^ = F)then Exit;
  Result := -1;
end;

function TFrameStriingGridEditor.RowOf(FieldName: string; Value: Extended): Integer;
var
  Idx, R: Integer;
  F: Extended;
begin
  Idx:= FColumns.IndexOf(FieldName);
  if(Idx >= 0)and(grid.Cells[0, 1] <> '')then
    for Result := 1 to grid.RowCount - 1 do
      if TryStrToFloat(grid.Cells[Idx + FSystemColCount, Result], F)and(F = Value) then Exit;
  Idx:= FFields.IndexOf(FieldName);
  if(Idx >= 0)and(grid.Cells[0, 1] <> '')then
    for Result := 1 to grid.RowCount - 1 do
      if PExtended(grid.Objects[Idx, Result])^  = Value then Exit;
  Result := -1;
end;

(* Events *)

procedure TFrameStriingGridEditor.gridDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Text: string;
  F: Extended;
  //0：上 1：右 2：下 3：左 4: 中
  procedure Drew(Colors: array of TColor);
    procedure DrewLine(X1, Y1, X2, Y2: Integer; AColor: TColor);
    begin
      with grid.Canvas do begin
        Pen.Color:= AColor;
        MoveTo(X1, Y1);
        LineTo(X2, Y2);
      end;
    end;
  begin
    with grid.Canvas do begin
      Brush.Color:= Colors[4];
      FillRect(Rect);
      DrewLine(Rect.Left, Rect.Top, Rect.Right, Rect.Top, Colors[0]);
      DrewLine(Rect.Right, Rect.Top, Rect.Right, Rect.Bottom, Colors[1]);
      DrewLine(Rect.Right, Rect.Bottom, Rect.Left, Rect.Bottom, Colors[2]);
      DrewLine(Rect.Left, Rect.Bottom, Rect.Left, Rect.Top, Colors[3]);
    end;
  end;
begin
  Text:= grid.Cells[ACol, ARow];
  grid.Canvas.Brush.Style := bsClear;
  if(gdFixed in State)and(ARow = 0)then
    GradientFillCanvas(grid.Canvas, clSilver, clWindow, Rect, gdVertical)
  else if(gdFixed in State)and(ARow > 0)then
    GradientFillCanvas(grid.Canvas, $00EAEAEA, clWindow, Rect, gdVertical)
  else if gdSelected in State then
    Drew([clBlack, clBlack, clBlack, clBlack, clWindow])
  else if gdFocused in State then
    Drew([clBlack, clBlack, clBlack, clBlack, clBlue])
  else if gdRowSelected in State then
    Drew([clBlack, clBlack, clBlack, clBlack, clGray])
  else if gdHotTrack in State then
    Drew([clBlack, clBlack, clBlack, clBlack, clHighlight])
  else if gdPressed in State then
    Drew([clBlack, clBlack, clBlack, clBlack, clSkyBlue])
  else if(FColumns.Objects[ACol - FSystemColCount] = nil)and(ARow mod 2 = 1)then
    Drew([clWhite, clWhite, clSilver, clSilver, $00EAEAEA])
  else if(FColumns.Objects[ACol - FSystemColCount] = nil)and(ARow mod 2 = 0)then
    Drew([clWhite, clWhite, clSilver, clSilver, clSilver])
  else if ARow mod 2 = 1 then
    Drew([clSilver, clSilver, clGray, clGray, clWindow])
  else Drew([clSilver, clSilver, clGray, clGray, clWhite]);
//  tfBottom, tfCalcRect, tfCenter, tfEditControl, tfEndEllipsis,
//  tfPathEllipsis, tfExpandTabs, tfExternalLeading, tfLeft, tfModifyString,
//  tfNoClip, tfNoPrefix, tfRight, tfRtlReading, tfSingleLine, tfTop,
//  tfVerticalCenter, tfWordBreak
  if TryStrToFloat(Text, F) then
    grid.Canvas.TextRect(Rect, Text, [tfVerticalCenter, tfRight, tfSingleLine])
  else grid.Canvas.TextRect(Rect, Text, [tfVerticalCenter, tfCenter, tfSingleLine]);
end;

procedure TFrameStriingGridEditor.gridFixedCellClick(Sender: TObject; ACol,
  ARow: Integer);
begin
  if(ACol < FSystemColCount)and(Grid.Cells[ACol, ARow] = '删除')then begin
    if not Assigned(FOnDelete) then DeleteRow(ARow)
    else if FOnDelete(ARow) then DeleteRow(ARow);
  end else if(ACol < FSystemColCount)and(Grid.Cells[ACol, ARow] = '插入')then begin
    InsertRow(ARow);
  end;
end;

procedure TFrameStriingGridEditor.gridKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if FCanInsertRow and(ssCtrl in Shift)and(key = VK_INSERT) then begin
    InsertRow(grid.Row);
    grid.EditorMode:= True;
  end;
  if FCanDeleteRow and(ssCtrl in Shift)and(key = VK_DELETE) then DeleteRow(grid.Row);

end;

procedure TFrameStriingGridEditor.gridSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  CanSelect:= Integer(FColumns.Objects[ACol - FSystemColCount]) = 1;
end;

procedure TFrameStriingGridEditor.gridSetEditText(Sender: TObject; ACol, ARow: Integer;
  const Value: string);
var
  I: Integer;
begin
  if ACol < FSystemColCount then begin
    if grid.Cells[0, ARow] = '序号' then grid.Cells[ACol, ARow]:= intToStr(ARow)
    else grid.Cells[ACol, ARow]:= grid.Cells[0, ARow];
  end;
  if(grid.RowCount = ARow + 1)and(grid.Cells[ACol, ARow] <> '')then begin
    if FCanDeleteRow then grid.Cells[0, ARow]:= '删除';
    grid.RowCount:= grid.RowCount + 1;
  end else if(ARow < grid.RowCount - 1)and(Value = '')then begin
    for I := 1 to grid.ColCount - 1 do
      if grid.Cells[I, ARow] <> '' then Exit;
    DeleteRow(ARow);
  end;
end;

(* Methods *)

procedure TFrameStriingGridEditor.AddRecord(Dateset: TDataSet);
var
  I: Integer;
  AFld: TField;
begin
  if Dateset.Eof then Exit;
  InsertRow;
  for I := 0 to FColumns.Count - 1 do begin
    AFld:= Dateset.FindField(FColumns[I]);
    if Assigned(AFld) then grid.Cells[I + FSystemColCount, grid.Row]:= AFld.AsString;
  end;
  for I := 0 to FFields.Count - 1 do begin
    AFld:= Dateset.FindField(FFields[I]);
    if Assigned(AFld) then
      PExtended(grid.Objects[I, grid.Row])^:= AFld.AsExtended;
  end;
end;

function TFrameStriingGridEditor.InsertRow(ARow: Integer): Integer;
var
  I, C: Integer;
  P: PExtended;
begin
  Result:= grid.RowCount - 1;
  if(not FCanAppend)and(grid.Cells[FSystemColCount, grid.RowCount - 1] = '')then Exit;
  grid.RowCount:= grid.RowCount + 1;
  for C := 0 to FFields.Count - 1 do begin
    New(P); P^:= 0;
    grid.Objects[C, grid.RowCount - 1]:= TObject(P);
  end;
  for C := 0 to FSystemColCount - 1 do begin
    if grid.Cells[C, 0] = '序号' then grid.Cells[C, grid.RowCount - 1]:= IntToStr(grid.RowCount - 1)
    else grid.Cells[C, grid.RowCount - 1]:= grid.Cells[C, 0];
  end;
  if ARow = 0 then ARow:= grid.RowCount - 1;
  for I := grid.RowCount - 1 downto ARow + 1 do begin
    for C := FSystemColCount to grid.ColCount - 1 do begin
      grid.Cells[C, I]:= grid.Cells[C, I - 1];
      grid.Cells[C, I - 1]:= '';
    end;
    for C := 0 to FFields.Count - 1 do begin
      PExtended(grid.Objects[C, I])^:= PExtended(grid.Objects[C, I - 1])^;
      PExtended(grid.Objects[C, I - 1])^:= 0;
    end;
  end;
//  if FCanDeleteRow then begin
//    grid.Cells[0, grid.RowCount - 1]:= '删除';
//    grid.RowCount:= grid.RowCount + 1;
//    for I := 0 to FFields.Count - 1 do begin
//      New(P);  P^:= 0;
//      grid.Objects[I, grid.RowCount - 1]:= TObject(P);
//    end;
//  end else grid.Cells[0, grid.RowCount - 1]:= IntToStr(grid.RowCount - 1);
  grid.Row:= ARow;
  Result:= ARow;
end;

procedure TFrameStriingGridEditor.Clear;
var
  R, C: Integer;
  P: PExtended;
begin
  for R := grid.RowCount - 1 Downto 1 do
    DeleteRow(R);
  for C := 0 to FFields.Count - 1 do begin
    New(P); P^:= 0;
    grid.Objects[C, grid.RowCount - 1]:= TObject(P);
    grid.Cells[C, grid.RowCount - 1]:= '';
  end;
end;

procedure TFrameStriingGridEditor.DeleteRow(ARow: Integer);
var
  R, C: Integer;
  P: PExtended;
begin
  if ARow < 1 then Exit;
  for C := 0 to FFields.Count - 1 do begin
    P:= PExtended(grid.Objects[C, ARow]);
    Dispose(P);
  end;
  for R := ARow to grid.RowCount - 2 do begin
    for C := FSystemColCount to grid.ColCount - 1 do begin
      grid.Cells[C, R]:= grid.Cells[C, R + 1];
      //grid.Cells[C, R + 1]:= '';
    end;
    for C := 0 to FFields.Count - 1 do begin
      grid.Objects[C, R]:= grid.Objects[C, R + 1];
      grid.Objects[C, R + 1]:= nil;
    end;
  end;
  if grid.RowCount > 2 then
    grid.RowCount:= grid.RowCount - 1;
  grid.Cells[FSystemColCount, grid.RowCount - 1]:= '';
end;

end.
