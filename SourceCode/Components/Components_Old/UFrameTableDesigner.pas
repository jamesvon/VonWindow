unit UFrameTableDesigner;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, Grids, StdCtrls, Buttons, ColorGrd, ZcGridStyle,
  ZcUniClass, ZcBaseCtrls, ZJGrid, ZcDataGrid, ImgList, Menus, ExtCtrls,
  ZcCalcExpress, ZcFormulas, ZcColorCtrls, ZcGridClasses,
  ZcSheetControl;

type
  TFrameTableDesigner = class(TFrame)
    imgBar: TImageList;
    imgLine: TImageList;
    imgColor: TImageList;
    imgBorder: TImageList;
    EjunLicense1: TEjunLicense;
    CoolBar1: TCoolBar;
    barTable: TToolBar;
    barWord: TToolBar;
    ColorDialog1: TColorDialog;
    PopupPanelBorder: TZcPopupPanel;
    PopupPanel: TZcPopupPanel;
    ColorBox: TZcColorBox;
    tbFontColor: TZcColorToolButton;
    tbBackgroundColor: TZcColorToolButton;
    tbBorderLine: TZcToolButton;
    btnTitle: TToolButton;
    btnPrompt: TToolButton;
    btnName: TToolButton;
    ToolButton6: TToolButton;
    btnAddRow: TToolButton;
    btnDelRow: TToolButton;
    btnAddColumn: TToolButton;
    btnDelColumn: TToolButton;
    ToolButton10: TToolButton;
    btnMerge: TToolButton;
    btnSplit: TToolButton;
    ToolButton1: TToolButton;
    btnWrap: TToolButton;
    btnLock: TToolButton;
    btnHLeft: TToolButton;
    btnHCenter: TToolButton;
    btnHRight: TToolButton;
    btnVTop: TToolButton;
    btnVCenter: TToolButton;
    btnVBottom: TToolButton;
    ToolButton28: TToolButton;
    btnHint: TToolButton;
    ToolButton2: TToolButton;
    cmbFonts: TComboBox;
    cmbFontSize: TComboBox;
    ToolBar4: TToolBar;
    btnC1: TToolButton;
    btnC2: TToolButton;
    btnC3: TToolButton;
    btnC4: TToolButton;
    btnC5: TToolButton;
    btnC6: TToolButton;
    btnC7: TToolButton;
    btnC8: TToolButton;
    btnC9: TToolButton;
    btnC10: TToolButton;
    btnC11: TToolButton;
    btnC12: TToolButton;
    btnC13: TToolButton;
    btnC14: TToolButton;
    btnC15: TToolButton;
    btnC16: TToolButton;
    btnC17: TBitBtn;
    ToolBar1: TToolBar;
    btnB2: TToolButton;
    btnB3: TToolButton;
    btnB4: TToolButton;
    btnB5: TToolButton;
    btnB6: TToolButton;
    btnB7: TToolButton;
    btnB8: TToolButton;
    btnB9: TToolButton;
    btnB0: TToolButton;
    btnB10: TToolButton;
    btnB11: TToolButton;
    btnB12: TToolButton;
    btnB13: TToolButton;
    btnB14: TToolButton;
    btnB15: TToolButton;
    btnB1: TToolButton;
    ToolBar3: TToolBar;
    btnL1: TToolButton;
    btnL2: TToolButton;
    btnL3: TToolButton;
    btnL4: TToolButton;
    btnL5: TToolButton;
    btnL6: TToolButton;
    btnL7: TToolButton;
    btnL8: TToolButton;
    btnL9: TToolButton;
    btnL10: TToolButton;
    btnL11: TToolButton;
    btnL12: TToolButton;
    btnL13: TToolButton;
    btnL14: TToolButton;
    btnOK: TToolButton;
    btnFontBlob: TToolButton;
    ToolButton5: TToolButton;
    btnFontItalic: TToolButton;
    btnFontUnderline: TToolButton;
    ToolButton9: TToolButton;
    ToolButton3: TToolButton;
    pmSheet: TPopupMenu;
    mInsertRow: TMenuItem;
    mInsertColumn: TMenuItem;
    mDeleteRow: TMenuItem;
    mDeleteColumn: TMenuItem;
    N22: TMenuItem;
    mSetColumnDefaultWidth: TMenuItem;
    mSetRowDefaultHeight: TMenuItem;
    plEditor: TPanel;
    EUnit: TEdit;
    EFormula: TEdit;
    pmControl: TPopupMenu;
    mAddSheet: TMenuItem;
    mDeleteSheet: TMenuItem;
    btnPostil: TToolButton;
    ToolButton4: TToolButton;
    btnTypeDefault: TToolButton;
    btnTypeCheck: TToolButton;
    btnTypeRadio: TToolButton;
    btnTypeCombo: TToolButton;
    btnTypeDropDown: TToolButton;
    btnTypeNum: TToolButton;
    btnTypeCalendar: TToolButton;
    btnTypeCurrency: TToolButton;
    ToolButton7: TToolButton;
    ECellName: TEdit;
    ERowName: TEdit;
    EColName: TEdit;
    EFmt: TComboBox;
    EjunSheetControl1: TEjunSheetControl;
    ToolButton8: TToolButton;
    tbZoom: TTrackBar;
    N1: TMenuItem;
    mDeleteRowBlock: TMenuItem;
    mDeleteColBlock: TMenuItem;
    btnSave: TToolButton;
    btnLoad: TToolButton;
    ToolButton13: TToolButton;
    procedure btnTitleClick(Sender: TObject);
    procedure btnPromptClick(Sender: TObject);
    procedure btnAddRowClick(Sender: TObject);
    procedure btnDelRowClick(Sender: TObject);
    procedure btnAddColumnClick(Sender: TObject);
    procedure btnDelColumnClick(Sender: TObject);
    procedure btnWrapClick(Sender: TObject);
    procedure btnLockClick(Sender: TObject);
    procedure btnMergeClick(Sender: TObject);
    procedure btnSplitClick(Sender: TObject);
    procedure btnNameClick(Sender: TObject);
    procedure btnHCenterClick(Sender: TObject);
    procedure btnHLeftClick(Sender: TObject);
    procedure btnHRightClick(Sender: TObject);
    procedure btnVTopClick(Sender: TObject);
    procedure btnVCenterClick(Sender: TObject);
    procedure btnVBottomClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure btnC17Click(Sender: TObject);
    procedure tbBackgroundColorClick(Sender: TObject);
    procedure tbBackgroundColorDropDown(Sender: TObject);
    procedure ColorBoxClick(Sender: TObject);
    procedure PopupPanelClose(Sender: TObject);
    procedure tbBorderLineDropDown(Sender: TObject);
    procedure cmbFontsDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure cmbFontsChange(Sender: TObject);
    procedure tbFontColorClick(Sender: TObject);
    procedure tbFontColorDropDown(Sender: TObject);
    procedure btnFontBlobClick(Sender: TObject);
    procedure btnFontItalicClick(Sender: TObject);
    procedure btnFontUnderlineClick(Sender: TObject);
    procedure cmbFontSizeChange(Sender: TObject);
    procedure EFormulaKeyPress(Sender: TObject; var Key: Char);
    procedure EFormulaChange(Sender: TObject);
    procedure EjunSheetControl1CellValueChanged(Sender: TObject);
    procedure EjunSheetControl1Change(Sender: TObject);
    procedure EjunSheetControl1CurrentChanged(Sender: TObject; Col,
      Row: Integer);
    procedure EjunSheetControl1EditChange(Sender: TObject);
    procedure EjunSheetControl1EndSelect(Sender: TObject);
    procedure EjunSheetControl1NewSheet(Sender: TObject;
      ASheet: TEjunGridSheet);
    procedure EjunSheetControl1SelectionChange(Sender: TObject;
      const ARange: TRect);
    procedure mInsertRowClick(Sender: TObject);
    procedure mInsertColumnClick(Sender: TObject);
    procedure mDeleteRowClick(Sender: TObject);
    procedure mDeleteColumnClick(Sender: TObject);
    procedure mSetColumnDefaultWidthClick(Sender: TObject);
    procedure mSetRowDefaultHeightClick(Sender: TObject);
    procedure EFormulaEnter(Sender: TObject);
    procedure btnHintClick(Sender: TObject);
    procedure mAddSheetClick(Sender: TObject);
    procedure mDeleteSheetClick(Sender: TObject);
    procedure plEditorCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure btnPostilClick(Sender: TObject);
    procedure btnTypeDefaultClick(Sender: TObject);
    procedure btnTypeCheckClick(Sender: TObject);
    procedure btnTypeRadioClick(Sender: TObject);
    procedure btnTypeComboClick(Sender: TObject);
    procedure btnTypeDropDownClick(Sender: TObject);
    procedure btnTypeNumClick(Sender: TObject);
    procedure btnTypeCalendarClick(Sender: TObject);
    procedure btnTypeCurrencyClick(Sender: TObject);
    procedure ECellNameExit(Sender: TObject);
    procedure EColNameExit(Sender: TObject);
    procedure ERowNameExit(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure tbZoomChange(Sender: TObject);
    procedure mDeleteRowBlockClick(Sender: TObject);
    procedure mDeleteColBlockClick(Sender: TObject);
    procedure EFmtChange(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
  private
    { Private declarations }
    FUserColor: TColor;
    FActiveSheet: TEjunGridSheet;
    FOnSheetChanged: TNotifyEvent;
    function GetBorderColor: TColor;
    function GetBorderLine: TZcGridBorderStyle;
    procedure FillFontNameComboBox;
    function GetActiveSheet: TEjunGridSheet;
    procedure DisplayCellFormat(Fmt: string);
    procedure CopyCell(srcCol, srcRow, destCol, destRow: Integer);
    procedure SetOnSheetChanged(const Value: TNotifyEvent);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  published
    property ActiveSheet: TEjunGridSheet read GetActiveSheet;
    property OnSheetChanged: TNotifyEvent read FOnSheetChanged write SetOnSheetChanged;
  end;

implementation

uses UDLG_TableDesigner, StrUtils, ZcStdCells, Types;

{$R *.dfm}     

constructor TFrameTableDesigner.Create(AOwner: TComponent);
begin
  inherited;
  FillFontNameComboBox;
end;

(* 表头，表栏，表名设置 *)

procedure TFrameTableDesigner.btnTitleClick(Sender: TObject);
var
  sRect, dRect: TRect;
  i: Integer;

  procedure SetColName(ACol: Integer; AName: string);
  begin
    EjunSheetControl1.ActiveSheet.Columns[ACol].Name:= AName;
  end;
begin     //调用对话框设计标题格式
  with TFDLG_TableDesigner.Create(nil) do try
    FrameTableDesigner1.EjunSheetControl1.ShowNewTab:= False;
    FrameTableDesigner1.EjunSheetControl1.ActiveSheet.Caption:= '表头设计';
    FrameTableDesigner1.EjunSheetControl1.ActiveSheet.RowCount:= EjunSheetControl1.ActiveSheet.FixedRowCount + 1;
    FrameTableDesigner1.EjunSheetControl1.ActiveSheet.ColCount:= EjunSheetControl1.ActiveSheet.ColCount - EjunSheetControl1.ActiveSheet.FixedColCount + 1;
    sRect:= Rect(EjunSheetControl1.ActiveSheet.FixedColCount, 0, EjunSheetControl1.ActiveSheet.ColCount - 1, EjunSheetControl1.ActiveSheet.FixedRowCount - 1);
    dRect:= Rect(1, 1, sRect.Right - sRect.Left + 1, sRect.Bottom - sRect.Top + 1);
    FrameTableDesigner1.EjunSheetControl1.ActiveSheet.CopyCells(EjunSheetControl1.ActiveSheet, sRect, dRect, pmPasteNormal);
    if ShowModal = mrOK then begin
      EjunSheetControl1.ActiveSheet.FixedRowCount:= FrameTableDesigner1.EjunSheetControl1.ActiveSheet.RowCount - 1;
      EjunSheetControl1.ActiveSheet.ColCount:= EjunSheetControl1.ActiveSheet.FixedColCount + FrameTableDesigner1.EjunSheetControl1.ActiveSheet.ColCount - 1;
      sRect:= Rect(1, 1, FrameTableDesigner1.EjunSheetControl1.ActiveSheet.ColCount, FrameTableDesigner1.EjunSheetControl1.ActiveSheet.RowCount);
      dRect:= Rect(EjunSheetControl1.ActiveSheet.FixedColCount, 0, sRect.Right - sRect.Left, sRect.Bottom - sRect.Top);
      EjunSheetControl1.ActiveSheet.Split(dRect);
      EjunSheetControl1.ActiveSheet.CopyCells(FrameTableDesigner1.EjunSheetControl1.ActiveSheet, sRect, dRect, pmPasteNormal, True);
      for i:= EjunSheetControl1.ActiveSheet.FixedColCount to EjunSheetControl1.ActiveSheet.ColCount - 1 do
        if EjunSheetControl1.ActiveSheet.Columns[i].Name <> '' then Continue
        else if EjunSheetControl1.ActiveSheet.Cells[i, EjunSheetControl1.ActiveSheet.FixedRowCount - 1].RowSpan = 1 then
          SetColName(i, EjunSheetControl1.ActiveSheet.Cells[i, EjunSheetControl1.ActiveSheet.FixedRowCount - 1].AsString)
        else SetColName(i, 'C' + IntToStr(i));
    end;
  finally
    Free;
  end;
end;

procedure TFrameTableDesigner.btnPromptClick(Sender: TObject);
var
  sRect, dRect: TRect;              
  i: Integer;

  procedure SetRowName(ARow: Integer; AName: string);
  begin
    EjunSheetControl1.ActiveSheet.Rows[ARow].Caption:= AName;
  end;
begin     //调用对话框设计左边栏目格式
  //TZcGridPasteMode = (pmPasteNormal, pmPasteValue, pmPasteStyle, pmStyleBrush);
  with TFDLG_TableDesigner.Create(nil) do try
    FrameTableDesigner1.EjunSheetControl1.ShowNewTab:= False;  
    FrameTableDesigner1.EjunSheetControl1.ActiveSheet.Caption:= '栏目设计';
    FrameTableDesigner1.EjunSheetControl1.ActiveSheet.RowCount:= EjunSheetControl1.ActiveSheet.RowCount - EjunSheetControl1.ActiveSheet.FixedRowCount + 1;
    FrameTableDesigner1.EjunSheetControl1.ActiveSheet.ColCount:= EjunSheetControl1.ActiveSheet.FixedColCount + 1;
    sRect:= Rect(0, EjunSheetControl1.ActiveSheet.FixedRowCount, EjunSheetControl1.ActiveSheet.FixedColCount - 1, EjunSheetControl1.ActiveSheet.RowCount - 1);
    dRect:= Rect(1, 1, sRect.Right - sRect.Left + 1, sRect.Bottom - sRect.Top + 1);
    FrameTableDesigner1.EjunSheetControl1.ActiveSheet.CopyCells(EjunSheetControl1.ActiveSheet, sRect, dRect, pmPasteNormal);
    if ShowModal = mrOK then begin
      EjunSheetControl1.ActiveSheet.FixedColCount:= FrameTableDesigner1.EjunSheetControl1.ActiveSheet.ColCount - 1;
      EjunSheetControl1.ActiveSheet.RowCount:= EjunSheetControl1.ActiveSheet.FixedRowCount + FrameTableDesigner1.EjunSheetControl1.ActiveSheet.RowCount - 1;
      sRect:= Rect(1, 1, FrameTableDesigner1.EjunSheetControl1.ActiveSheet.ColCount, FrameTableDesigner1.EjunSheetControl1.ActiveSheet.RowCount);
      dRect:= Rect(0, EjunSheetControl1.ActiveSheet.FixedRowCount, sRect.Right - sRect.Left, sRect.Bottom - sRect.Top);
      EjunSheetControl1.ActiveSheet.Split(dRect);
      EjunSheetControl1.ActiveSheet.CopyCells(FrameTableDesigner1.EjunSheetControl1.ActiveSheet, sRect, dRect, pmPasteNormal, True);  
      for i:= EjunSheetControl1.ActiveSheet.FixedRowCount to EjunSheetControl1.ActiveSheet.RowCount - 1 do
        if EjunSheetControl1.ActiveSheet.Rows[i].Caption <> '' then Continue
        else if EjunSheetControl1.ActiveSheet.Cells[EjunSheetControl1.ActiveSheet.FixedColCount - 1, i].RowSpan = 1 then
          SetRowName(i, EjunSheetControl1.ActiveSheet.Cells[EjunSheetControl1.ActiveSheet.FixedColCount - 1, i].AsString)
        else SetRowName(i, 'R' + IntToStr(i));
    end;
  finally
    Free;
  end;
end;

procedure TFrameTableDesigner.btnNameClick(Sender: TObject);
var
  sRect, dRect: TRect;
begin     //调用对话框设计表格左上角格式
  with TFDLG_TableDesigner.Create(nil) do try   
    FrameTableDesigner1.EjunSheetControl1.ShowNewTab:= False;
    FrameTableDesigner1.EjunSheetControl1.ActiveSheet.Caption:= '头栏设计';
    FrameTableDesigner1.btnAddRow.Visible:= False;
    FrameTableDesigner1.btnDelRow.Visible:= False;
    FrameTableDesigner1.btnAddColumn.Visible:= False;
    FrameTableDesigner1.btnDelColumn.Visible:= False;
    FrameTableDesigner1.ToolButton10.Visible:= False;
    FrameTableDesigner1.EjunSheetControl1.ActiveSheet.RowCount:= EjunSheetControl1.ActiveSheet.FixedRowCount + 1;
    FrameTableDesigner1.EjunSheetControl1.ActiveSheet.ColCount:= EjunSheetControl1.ActiveSheet.FixedColCount + 1;
    sRect:= Rect(0, 0, EjunSheetControl1.ActiveSheet.FixedColCount - 1, EjunSheetControl1.ActiveSheet.FixedRowCount - 1);
    dRect:= Rect(1, 1, sRect.Right - sRect.Left + 1, sRect.Bottom - sRect.Top + 1);
    FrameTableDesigner1.EjunSheetControl1.ActiveSheet.CopyCells(EjunSheetControl1.ActiveSheet, sRect, dRect, pmPasteNormal);
    if ShowModal = mrOK then begin
      EjunSheetControl1.ActiveSheet.Split(dRect);
      EjunSheetControl1.ActiveSheet.CopyCells(FrameTableDesigner1.EjunSheetControl1.ActiveSheet, dRect, sRect, pmPasteNormal);
    end;
  finally
    Free;
  end;
end;

(* 行列添加与删除 *)

procedure TFrameTableDesigner.btnAddRowClick(Sender: TObject);
begin     //添加行
  EjunSheetControl1.ActiveSheet.RowCount:= EjunSheetControl1.ActiveSheet.RowCount + 1;
end;

procedure TFrameTableDesigner.btnDelRowClick(Sender: TObject);
begin     //删除行
  if EjunSheetControl1.ActiveSheet.RowCount > 2 then
    EjunSheetControl1.ActiveSheet.RowCount:= EjunSheetControl1.ActiveSheet.RowCount - 1;
end;

procedure TFrameTableDesigner.btnAddColumnClick(Sender: TObject);
begin     //添加列
  EjunSheetControl1.ActiveSheet.ColCount:= EjunSheetControl1.ActiveSheet.ColCount + 1;
end;

procedure TFrameTableDesigner.btnDelColumnClick(Sender: TObject);
begin     //删除列
  if EjunSheetControl1.ActiveSheet.ColCount > 2 then
    EjunSheetControl1.ActiveSheet.ColCount:= EjunSheetControl1.ActiveSheet.ColCount - 1;
end;

(* 换行与锁定 *)

procedure TFrameTableDesigner.btnWrapClick(Sender: TObject);
begin     //设置单元格换行格式
  EjunSheetControl1.ActiveSheet.Selection.SetWrapText(btnWrap.Down);
end;

procedure TFrameTableDesigner.btnLoadClick(Sender: TObject);
begin
  with TOpenDialog.Create(nil)do try
    Filter:= '表格文件|*.VONX';
    if Execute then
      EjunSheetControl1.LoadFromFile(Filename);
  finally
    Free;
  end;
end;

procedure TFrameTableDesigner.btnLockClick(Sender: TObject);
begin     //设置单元格锁定状态
  EjunSheetControl1.ActiveSheet.Selection.SetLocked(btnLock.Down);
end;

(* 单元格合并 *)

procedure TFrameTableDesigner.btnMergeClick(Sender: TObject);
begin     //合并单元格
  EjunSheetControl1.ActiveSheet.Selection.Merge;
end;

procedure TFrameTableDesigner.btnSaveClick(Sender: TObject);
begin
  with TSaveDialog.Create(nil)do try
    Filter:= '表格文件|*.VONX';
    if Execute then
      EjunSheetControl1.SaveToFile(Filename);
  finally
    Free;
  end;
end;

procedure TFrameTableDesigner.btnSplitClick(Sender: TObject);
begin     //分解合并的单元格
  EjunSheetControl1.ActiveSheet.Selection.Split;
end;

(* 对齐方式 *)

procedure TFrameTableDesigner.btnHCenterClick(Sender: TObject);
begin     //水平居中
  //枚举值含义  haGeneral 常规对齐，效果和左对齐效果相同  haLeft 左对齐
  //            haCenter  居中对齐  haRight 右对齐        haFill 填充对齐
  //            haJustify 调整对齐  haCenter_across_selection
  EjunSheetControl1.ActiveSheet.Selection.HorzAlign:= haCenter;
end;

procedure TFrameTableDesigner.btnHLeftClick(Sender: TObject);
begin     //水平居左
  EjunSheetControl1.ActiveSheet.Selection.HorzAlign:= haLeft;
end;

procedure TFrameTableDesigner.btnHRightClick(Sender: TObject);
begin     //水平居右
  EjunSheetControl1.ActiveSheet.Selection.HorzAlign:= haRight;
end;

procedure TFrameTableDesigner.btnVTopClick(Sender: TObject);
begin     //垂直居上
  //枚举值含义  vaTop 置顶对齐  vaCenter 居中对齐  vaBottom 置底对齐  vaJustify 调整对齐s
  EjunSheetControl1.ActiveSheet.Selection.VertAlign:= vaTop;
end;

procedure TFrameTableDesigner.btnVCenterClick(Sender: TObject);
begin     //垂直居中
  EjunSheetControl1.ActiveSheet.Selection.VertAlign:= vaCenter;
end;

procedure TFrameTableDesigner.btnVBottomClick(Sender: TObject);
begin     //垂直置底
  EjunSheetControl1.ActiveSheet.Selection.VertAlign:= vaBottom;
end;

(* Word *)

procedure TFrameTableDesigner.tbFontColorClick(Sender: TObject);
begin     //使用当前使用的颜色
  EjunSheetControl1.ActiveSheet.Selection.FontColor := tbFontColor.Color;
end;      

procedure TFrameTableDesigner.tbFontColorDropDown(Sender: TObject);
begin     //通过下拉框选择字体颜色
  ColorBox.ActiveColor := tbFontColor.Color;
  PopupPanel.Popup(tbFontColor, taLeftJustify);
end;

procedure TFrameTableDesigner.btnFontBlobClick(Sender: TObject);
begin     //加粗
  if btnFontBlob.Down then
    EjunSheetControl1.ActiveSheet.Selection.FontStyle:= EjunSheetControl1.ActiveSheet.Selection.Font.Style + [fsBold]
  else
    EjunSheetControl1.ActiveSheet.Selection.FontStyle:= EjunSheetControl1.ActiveSheet.Selection.Font.Style - [fsBold];
end;

procedure TFrameTableDesigner.btnFontItalicClick(Sender: TObject);
begin     //斜体
  if btnFontItalic.Down then
    EjunSheetControl1.ActiveSheet.Selection.FontStyle:= EjunSheetControl1.ActiveSheet.Selection.Font.Style + [fsItalic]
  else
    EjunSheetControl1.ActiveSheet.Selection.FontStyle:= EjunSheetControl1.ActiveSheet.Selection.Font.Style - [fsItalic];
end;

procedure TFrameTableDesigner.btnFontUnderlineClick(Sender: TObject);
begin     //下划线
  if btnFontUnderline.Down then
    EjunSheetControl1.ActiveSheet.Selection.FontStyle:= EjunSheetControl1.ActiveSheet.Selection.Font.Style + [fsUnderline]
  else
    EjunSheetControl1.ActiveSheet.Selection.FontStyle:= EjunSheetControl1.ActiveSheet.Selection.Font.Style - [fsUnderline];
end;   

procedure TFrameTableDesigner.cmbFontsChange(Sender: TObject);
begin     //设定单元格字体
  EjunSheetControl1.ActiveSheet.Selection.FontName:= cmbFonts.Text;
end;  

procedure TFrameTableDesigner.cmbFontSizeChange(Sender: TObject);
begin     //设定单元格字体大小
  EjunSheetControl1.ActiveSheet.Selection.FontSize:= StrToInt(cmbFontSize.Text);
end;

(* 设置单元格背景 *)

procedure TFrameTableDesigner.tbBackgroundColorClick(Sender: TObject);
begin     //使用默认单元格颜色作为背景
  EjunSheetControl1.ActiveSheet.Selection.BgColor := tbBackgroundColor.Color;
end;

procedure TFrameTableDesigner.tbBackgroundColorDropDown(Sender: TObject);
begin     //弹出颜色下拉框设置
  ColorBox.ActiveColor := tbBackgroundColor.Color;
  PopupPanel.Popup(tbBackgroundColor, taLeftJustify);
end;

procedure TFrameTableDesigner.ColorBoxClick(Sender: TObject);
begin     //选择单元格颜色
  PopupPanel.Close;
end;

procedure TFrameTableDesigner.PopupPanelClose(Sender: TObject);
begin     //设置单元格背景颜色
  if PopupPanel.Caller = tbBackgroundColor then begin
    EjunSheetControl1.ActiveSheet.Selection.BgColor := ColorBox.ActiveColor;
    tbBackgroundColor.Color := ColorBox.ActiveColor;
  end else if PopupPanel.Caller = tbFontColor then begin
    EjunSheetControl1.ActiveSheet.Selection.FontColor := ColorBox.ActiveColor;
    tbFontColor.Color := ColorBox.ActiveColor;
  end;
end;

procedure TFrameTableDesigner.SetOnSheetChanged(const Value: TNotifyEvent);
begin
  FOnSheetChanged := Value;
end;

(* 设置单元格边框 *)

procedure TFrameTableDesigner.btnOKClick(Sender: TObject);
begin      
  //(gbLeft, gbTop, gbRight, gbBottom, gbDiagonalDown, gbDiagonalDown1, gbDiagonalDown2, gbDiagonalUp, gbDiagonalUp1, gbDiagonalUp2);
  if btnB0.Down then EjunSheetControl1.ActiveSheet.Selection.SetBorder(OuterBorderSides + DiagonalBorderSides, gbsNone, clNone, True, True)
  else if btnB1.Down then EjunSheetControl1.ActiveSheet.Selection.SetBorder(OuterBorderSides, GetBorderLine, GetBorderColor, True, True)
  else if btnB2.Down then EjunSheetControl1.ActiveSheet.Selection.SetBorder([gbLeft, gbTop, gbRight, gbBottom], GetBorderLine, GetBorderColor)
  else if btnB3.Down then EjunSheetControl1.ActiveSheet.Selection.SetBorder([], GetBorderLine, GetBorderColor, True, True)
  else if btnB4.Down then EjunSheetControl1.ActiveSheet.Selection.SetBorder([], GetBorderLine, GetBorderColor, True, False)
  else if btnB5.Down then EjunSheetControl1.ActiveSheet.Selection.SetBorder([], GetBorderLine, GetBorderColor, False, True)
  else if btnB6.Down then EjunSheetControl1.ActiveSheet.Selection.SetBorder([gbTop], GetBorderLine, GetBorderColor)
  else if btnB7.Down then EjunSheetControl1.ActiveSheet.Selection.SetBorder([gbBottom], GetBorderLine, GetBorderColor)
  else if btnB8.Down then EjunSheetControl1.ActiveSheet.Selection.SetBorder([gbLeft], GetBorderLine, GetBorderColor)
  else if btnB9.Down then EjunSheetControl1.ActiveSheet.Selection.SetBorder([gbRight], GetBorderLine, GetBorderColor)
  else if btnB10.Down then EjunSheetControl1.ActiveSheet.Selection.SetBorder([gbDiagonalDown1], GetBorderLine, GetBorderColor)
  else if btnB11.Down then EjunSheetControl1.ActiveSheet.Selection.SetBorder([gbDiagonalDown], GetBorderLine, GetBorderColor)
  else if btnB12.Down then EjunSheetControl1.ActiveSheet.Selection.SetBorder([gbDiagonalDown2], GetBorderLine, GetBorderColor)
  else if btnB13.Down then EjunSheetControl1.ActiveSheet.Selection.SetBorder([gbDiagonalUp1], GetBorderLine, GetBorderColor)
  else if btnB14.Down then EjunSheetControl1.ActiveSheet.Selection.SetBorder([gbDiagonalUp], GetBorderLine, GetBorderColor)
  else if btnB15.Down then EjunSheetControl1.ActiveSheet.Selection.SetBorder([gbDiagonalUp2], GetBorderLine, GetBorderColor);
  PopupPanelBorder.Close;
end;

function TFrameTableDesigner.GetBorderColor: TColor;
begin
  if btnC1.Down then Result:= 0
  else if btnC2.Down then Result:= 128
  else if btnC3.Down then Result:= 32768
  else if btnC4.Down then Result:= 32896
  else if btnC5.Down then Result:= 8388608
  else if btnC6.Down then Result:= 8388736
  else if btnC7.Down then Result:= 8421376
  else if btnC8.Down then Result:= 12632256
  else if btnC9.Down then Result:= 8421504
  else if btnC10.Down then Result:= 255
  else if btnC11.Down then Result:= 65280
  else if btnC12.Down then Result:= 65535
  else if btnC13.Down then Result:= 16711680
  else if btnC14.Down then Result:= 16711935
  else if btnC15.Down then Result:= 16776960
  else if btnC16.Down then Result:= 16777215
  else Result:= FUserColor;
end;

function TFrameTableDesigner.GetBorderLine: TZcGridBorderStyle;
begin
  if btnL1.Down then Result:= gbsNone
  else if btnL2.Down then Result:= gbsThin
  else if btnL3.Down then Result:= gbsMedium
  else if btnL4.Down then Result:= gbsDashed
  else if btnL5.Down then Result:= gbsDotted
  else if btnL6.Down then Result:= gbsThick
  else if btnL7.Down then Result:= gbsDouble
  else if btnL8.Down then Result:= gbsHair
  else if btnL9.Down then Result:= gbsMedium_dashed
  else if btnL10.Down then Result:= gbsDash_dot
  else if btnL11.Down then Result:= gbsMedium_dash_dot
  else if btnL12.Down then Result:= gbsDash_dot_dot
  else if btnL13.Down then Result:= gbsMedium_dash_dot_dot
  else if btnL14.Down then Result:= gbsSlanted_dash_dot;
end;

procedure TFrameTableDesigner.btnC17Click(Sender: TObject);
begin     //用户选择颜色
  if ColorDialog1.Execute then begin
    FUserColor:= ColorDialog1.Color;
    btnC17.Glyph.Canvas.Brush.Color:= Color;
    btnC17.Glyph.Canvas.FillRect(Rect(1,1,14,14));
    btnC1.Down:= False;
    btnC2.Down:= False;
    btnC3.Down:= False;
    btnC4.Down:= False;
    btnC5.Down:= False;
    btnC6.Down:= False;
    btnC7.Down:= False;
    btnC8.Down:= False;
    btnC9.Down:= False;
    btnC10.Down:= False;
    btnC11.Down:= False;
    btnC12.Down:= False;
    btnC13.Down:= False;
    btnC14.Down:= False;
    btnC15.Down:= False;
    btnC16.Down:= False;
  end;
end;

procedure TFrameTableDesigner.tbBorderLineDropDown(Sender: TObject);
begin     //弹出边框线下拉设置
  PopupPanelBorder.Popup(tbBorderLine, taLeftJustify);
end;

procedure TFrameTableDesigner.FillFontNameComboBox;
begin     //填充字体名称
  cmbFonts.Items := Screen.Fonts;
  if Assigned(EjunSheetControl1.ActiveSheet) then
    cmbFonts.ItemIndex := cmbFonts.Items.IndexOf(EjunSheetControl1.ActiveSheet.Selection.FontName);
end;

procedure TFrameTableDesigner.cmbFontsDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
begin     //显示字体名称（以本字体样式展示）
  TComboBox(Control).Canvas.Font.Name := TComboBox(Control).Items[Index];
  TComboBox(Control).Canvas.Font.Size := 9;
  TComboBox(Control).Canvas.TextRect(Rect, Rect.Left, Rect.Top, TComboBox(Control).Items[Index]);
end;

(* 编辑区事件 *)

procedure TFrameTableDesigner.EFormulaEnter(Sender: TObject);
begin
  if(FActiveSheet <> nil) then
  begin
    // 让表格进入编辑状态，但不设置焦点，因为焦点在公式输入框中。
    FActiveSheet.ShowEditor(False);
  end;
end;

procedure TFrameTableDesigner.EFormulaKeyPress(Sender: TObject;
  var Key: Char);
var
  szKey: Word;
begin
  szKey:= Ord(Key);
  if (szKey = VK_RETURN) and (FActiveSheet <> nil) then // 按回车键，将回车键发送给表格
    FActiveSheet.KeyDown(szKey, [ssShift]);
end;

procedure TFrameTableDesigner.EFormulaChange(Sender: TObject);
begin
  if (FActiveSheet <> nil) then begin
    EjunSheetControl1.OnEditChange := nil; // 先将事件屏蔽，防止循环触发事件
    try
      FActiveSheet.EditorText:= EFormula.Text;
    finally
      FActiveSheet.OnEditChange:= EjunSheetControl1EditChange;
    end;
  end;
end;

(* EjunSheetControl Event *)

procedure TFrameTableDesigner.EjunSheetControl1CellValueChanged(
  Sender: TObject);
var
  Cell: TZcCell;
begin
  Cell := TZcCell(Sender);
  if Cell.IsCurrent and (Cell.SheetIndex = EjunSheetControl1.ActiveSheetIndex) then
  begin
    EFormula.Text := Cell.EditText;
  end;
end;

procedure TFrameTableDesigner.EjunSheetControl1Change(Sender: TObject);
begin
  if FActiveSheet <> EjunSheetControl1.ActiveSheet then begin
    FActiveSheet := EjunSheetControl1.ActiveSheet;
    if Assigned(FOnSheetChanged) then FOnSheetChanged(self);
  end;
end;

procedure TFrameTableDesigner.EjunSheetControl1CurrentChanged(
  Sender: TObject; Col, Row: Integer);
begin
  if FActiveSheet <> nil then begin
    cmbFontSize.ItemIndex := cmbFontSize.Items.IndexOf(IntToStr(FActiveSheet.Cells[Col, Row].Font.Size));
    cmbFonts.ItemIndex := cmbFonts.Items.IndexOf(FActiveSheet.Cells[Col, Row].Font.Name);
    btnWrap.Down:= FActiveSheet.CurCell.Style.WrapText;
    btnLock.Down:= FActiveSheet.CurCell.Style.Locked;
    btnFontBlob.Down:= fsBold in FActiveSheet.CurCell.Font.Style; 
    btnFontItalic.Down:= fsItalic in FActiveSheet.CurCell.Font.Style;
    btnFontUnderline.Down:= fsUnderline in FActiveSheet.CurCell.Font.Style;
    EUnit.Text := FActiveSheet.CurCell.RefName;    // 显示当前单元格坐标
    EFormula.Text := FActiveSheet.CurCell.EditText;// 显示当前单元格中的编辑文本
    case FActiveSheet.CurCell.Style.HorzAlign of
      haGeneral, haLeft: btnHLeft.Down:= True;
      haCenter: btnHCenter.Down:= True;
      haRight: btnHRight.Down:= True;
      haFill, haJustify, haCenter_across_selection: btnHLeft.Down:= True;
    end;                                            
    case FActiveSheet.CurCell.Style.VertAlign of
      vaTop: btnVTop.Down:= True;
      vaCenter: btnVCenter.Down:= True;
      vaBottom: btnVBottom.Down:= True;
      vaJustify: btnVTop.Down:= True;
    end;
  end;
end;

procedure TFrameTableDesigner.EjunSheetControl1EditChange(Sender: TObject);
begin
  if FActiveSheet <> nil then begin
    EFormula.OnChange:= nil;     // 先把编辑框的事件设为nil，防止循环触发事件
    try
      EFormula.Text:= FActiveSheet.EditorText;
    finally
      EFormula.OnChange:= EFormulaChange;
    end;
  end;
end;

procedure TFrameTableDesigner.EjunSheetControl1EndSelect(Sender: TObject);
begin     // 显示当前选中单元格坐标
  FActiveSheet.Editing:= True;
  EUnit.Text := FActiveSheet.CurCell.RefName;
  ECellName.Text := FActiveSheet.CurCell.Name;
  EColName.Text := FActiveSheet.Columns[FActiveSheet.CurCol].Name;
  ERowName.Text := FActiveSheet.Rows[FActiveSheet.CurRow].Caption;
  btnTypeDefault.Down:= FActiveSheet.CellType[FActiveSheet.CurCol, FActiveSheet.CurRow] = cellDefault;
  btnTypeDefault.Down:= FActiveSheet.CellType[FActiveSheet.CurCol, FActiveSheet.CurRow] = cellTextBox;
  btnTypeCheck.Down:= FActiveSheet.CellType[FActiveSheet.CurCol, FActiveSheet.CurRow] = cellCheckBox;
  btnTypeRadio.Down:= FActiveSheet.CellType[FActiveSheet.CurCol, FActiveSheet.CurRow] = cellRadioBox;
  btnTypeCombo.Down:= FActiveSheet.CellType[FActiveSheet.CurCol, FActiveSheet.CurRow] = cellComboBox;
  btnTypeDropDown.Down:= FActiveSheet.CellType[FActiveSheet.CurCol, FActiveSheet.CurRow] = cellDropDownList;
  btnTypeNum.Down:= FActiveSheet.CellType[FActiveSheet.CurCol, FActiveSheet.CurRow] = cellNumberSpin;
  btnTypeCalendar.Down:= FActiveSheet.CellType[FActiveSheet.CurCol, FActiveSheet.CurRow] = cellCalendar;
  btnTypeCurrency.Down:= FActiveSheet.CellType[FActiveSheet.CurCol, FActiveSheet.CurRow] = cellCurrency;
  DisplayCellFormat(FActiveSheet.CurCell.Style.FormatString);
end;

procedure TFrameTableDesigner.EjunSheetControl1NewSheet(Sender: TObject;
  ASheet: TEjunGridSheet);
begin
  ASheet.UseLockedCellColor := True;
  ASheet.OptionsEx := ASheet.OptionsEx - [goxCanSelectLocked];
end;

procedure TFrameTableDesigner.EjunSheetControl1SelectionChange(
  Sender: TObject; const ARange: TRect);
begin     // 显示选择框范围大小
  EUnit.Text := Format('%dR x %dC', [ARange.Bottom - ARange.Top + 1, ARange.Right - ARange.Left + 1]);
end;

(* 表格控制菜单 *)

procedure TFrameTableDesigner.mInsertRowClick(Sender: TObject);
begin
  FActiveSheet.InsertRow(FActiveSheet.CurRow);
  FActiveSheet.Calculate;
end;

procedure TFrameTableDesigner.mInsertColumnClick(Sender: TObject);
begin
  FActiveSheet.InsertCol(FActiveSheet.CurCol);
  FActiveSheet.Calculate;
end;

procedure TFrameTableDesigner.mDeleteRowClick(Sender: TObject);
begin
  FActiveSheet.DeleteRow(FActiveSheet.CurRow, FActiveSheet.Selection.RowCount);
  FActiveSheet.Calculate;
end;

procedure TFrameTableDesigner.mDeleteColumnClick(Sender: TObject);
begin
  FActiveSheet.DeleteCol(FActiveSheet.CurCol, FActiveSheet.Selection.ColCount);
  FActiveSheet.Calculate;
end;

procedure TFrameTableDesigner.mSetColumnDefaultWidthClick(Sender: TObject);
var
  s: string;
begin
  s := IntToStr(FActiveSheet.DefaultColWidth);
  if InputQuery('设置默认列宽', '请输入列宽', s) then
    FActiveSheet.DefaultColWidth := StrToInt(s);
end;

procedure TFrameTableDesigner.mSetRowDefaultHeightClick(Sender: TObject);
var
  s: string;
begin
  s := IntToStr(FActiveSheet.DefaultRowHeight);
  if InputQuery('设置默认行高', '请输入行高', s) then
    FActiveSheet.DefaultRowHeight := StrToInt(s);
end;

procedure TFrameTableDesigner.btnHintClick(Sender: TObject);
var
  S: string;
begin
  S:= FActiveSheet.CurCell.Hint;
  if InputQuery('录入', '单元提示内容', S) then
    FActiveSheet.CurCell.Hint:= S;
end;

procedure TFrameTableDesigner.mAddSheetClick(Sender: TObject);
begin
  EjunSheetControl1.NewSheet('Sheet' + IntToStr(EjunSheetControl1.SheetCount + 1));
end;

procedure TFrameTableDesigner.mDeleteSheetClick(Sender: TObject);
begin
  EjunSheetControl1.Delete(FActiveSheet.TabIndex);
end;

procedure TFrameTableDesigner.plEditorCanResize(Sender: TObject;
  var NewWidth, NewHeight: Integer; var Resize: Boolean);
begin
  EFormula.Width:= NewWidth - EFormula.Left;
end;


function TFrameTableDesigner.GetActiveSheet: TEjunGridSheet;
begin
  Result:= EjunSheetControl1.ActiveSheet;
end;

procedure TFrameTableDesigner.btnPostilClick(Sender: TObject);
var
  S: string;
begin
  S:= FActiveSheet.CurCell.Postil;
  if InputQuery('录入', '单元脚注内容', S) then
    FActiveSheet.CurCell.Postil:= S;
end;

procedure TFrameTableDesigner.btnTypeDefaultClick(Sender: TObject);
begin
  with FActiveSheet do
    CellType[CurCol, CurRow]:= cellDefault;
end;

procedure TFrameTableDesigner.btnTypeCheckClick(Sender: TObject);
var
  S: string;
begin    
  S:= '';
  with FActiveSheet do begin
    if CellType[CurCol, CurRow] = cellCheckBox then
      S:= CheckCells[CurCol, CurRow].Caption;
    if InputQuery('录入', '录入项目内容，以逗号间隔', S) then begin
      CellType[CurCol, CurRow]:= cellCheckBox;
      CheckCells[CurCol, CurRow].Caption:= S;
    end;             
  end;
end;

procedure TFrameTableDesigner.btnTypeRadioClick(Sender: TObject);
var
  S: string;
  szList: TStringList;
  i: Integer;
begin
  S:= '';
  with FActiveSheet do begin
    if CellType[CurCol, CurRow] = cellRadioBox then begin
      for i:= 0 to RadioCells[CurCol, CurRow].ColumnCount - 1 do
        S:= S + ',' + RadioCells[CurCol, CurRow].Items[i];
      Delete(S, 1, 1);
    end;
    if InputQuery('录入', '录入项目内容，以逗号间隔', S) then begin
      CellType[CurCol, CurRow]:= cellRadioBox;
      RadioCells[CurCol, CurRow].ClearItems(); // 清空单选框内容
      szList:= TStringList.Create;
      szList.Delimiter:= ',';
      szList.DelimitedText:= S;
      for i:= 0 to szList.Count - 1 do
        RadioCells[CurCol, CurRow].AddItem(szList[i]);
      RadioCells[CurCol, CurRow].ColumnCount:= szList.Count;
      szList.Free;
    end;
  end;
end;

procedure TFrameTableDesigner.btnTypeComboClick(Sender: TObject);
var
  S: string;
  szList: TStringList;
  i: Integer;
begin
  with FActiveSheet do begin
    if CellType[CurCol, CurRow] = cellComboBox then begin
      for i:= 0 to ComboCells[CurCol, CurRow].ItemCount - 1 do
        S:= S + ',' + ComboCells[CurCol, CurRow].Items[i];
      Delete(S, 1, 1);
    end;
    if InputQuery('录入', '录入项目内容，以逗号间隔', S) then begin
      CellType[CurCol, CurRow]:= cellComboBox;
      ComboCells[CurCol, CurRow].ClearItems(); // 清空单选框内容
      szList:= TStringList.Create;
      szList.Delimiter:= ',';
      szList.DelimitedText:= S;
      for i:= 0 to szList.Count - 1 do
        ComboCells[CurCol, CurRow].AddItem(szList[i]);
      ComboCells[CurCol, CurRow].AllowInsertText:= true;
      szList.Free;
    end;
  end;
end;

procedure TFrameTableDesigner.btnTypeDropDownClick(Sender: TObject);
var
  S: string;
  szList: TStringList;
  i: Integer;
begin
  with FActiveSheet do begin   
    if CellType[CurCol, CurRow] = cellDropDownList then begin
      for i:= 0 to ComboCells[CurCol, CurRow].ItemCount - 1 do
        S:= S + ',' + ComboCells[CurCol, CurRow].Items[i];
      Delete(S, 1, 1);
    end;
    if InputQuery('录入', '录入项目内容，以逗号间隔', S) then begin
      CellType[CurCol, CurRow]:= cellDropDownList;
      ComboCells[CurCol, CurRow].ClearItems(); // 清空单选框内容
      szList:= TStringList.Create;
      szList.Delimiter:= ',';
      szList.DelimitedText:= S;
      for i:= 0 to szList.Count - 1 do
        ComboCells[CurCol, CurRow].AddItem(szList[i]);
      ComboCells[CurCol, CurRow].AllowInsertText:= False;
      szList.Free;
    end;           
  end;
end;

procedure TFrameTableDesigner.btnTypeNumClick(Sender: TObject);
begin
  with FActiveSheet do
    CellType[CurCol, CurRow]:= cellNumberSpin;
end;

procedure TFrameTableDesigner.btnTypeCalendarClick(Sender: TObject);
begin
  with FActiveSheet do
    CellType[CurCol, CurRow]:= cellCalendar;
end;

procedure TFrameTableDesigner.btnTypeCurrencyClick(Sender: TObject);
begin
  with FActiveSheet do
    CellType[CurCol, CurRow]:= cellCurrency;
end;

procedure TFrameTableDesigner.ECellNameExit(Sender: TObject);
begin
  if not ECellName.Modified then Exit;
  FActiveSheet.Cells[FActiveSheet.CurCol, FActiveSheet.CurRow].Name:= ECellName.Text;
end;

procedure TFrameTableDesigner.EColNameExit(Sender: TObject);
begin
  if not EColName.Modified then Exit;
  FActiveSheet.Columns[FActiveSheet.CurCol].Name:= EColName.Text;
end;

procedure TFrameTableDesigner.ERowNameExit(Sender: TObject);
begin
  if not ERowName.Modified then Exit;
  FActiveSheet.Rows[FActiveSheet.CurRow].Caption:= ERowName.Text;
end;

procedure TFrameTableDesigner.EFmtChange(Sender: TObject);
begin
  case EFmt.ItemIndex of
  0: FActiveSheet.Selection.SetFormatString('');                    //常规
  1: FActiveSheet.Selection.SetFormatString('##0.00');              //数字
  2: FActiveSheet.Selection.SetFormatString('0.');                  //整数
  3: FActiveSheet.Selection.SetFormatString('￥#,##0.00;[红色]￥-#,##0.00');                  //货币
  4: FActiveSheet.Selection.SetFormatString('_-￥* #,##0_-;-￥* #,##0_-;_-￥* "-"_-;_-@_-');  //会计专用
  5: FActiveSheet.Selection.SetFormatString('yyyy-m-d;@');          //短日期
  6: FActiveSheet.Selection.SetFormatString('yyyy"年"m"月"d"日";@');//长日期
  7: FActiveSheet.Selection.SetFormatString('h:mm:ss;@');           //时间
  8: FActiveSheet.Selection.SetFormatString('0.00%');               //百分比
  9: FActiveSheet.Selection.SetFormatString('#?/10000');            //分数
  10:FActiveSheet.Selection.SetFormatString('0.00E+00');            //科学记数
  11:FActiveSheet.Selection.SetFormatString('@');                   //文本
  else FActiveSheet.Selection.SetFormatString(EFmt.Text);
  end;
end;

procedure TFrameTableDesigner.DisplayCellFormat(Fmt: string);
begin
  if FActiveSheet.CurCell.Style.FormatString = '' then EFmt.ItemIndex:= 0
  else if FActiveSheet.CurCell.Style.FormatString = '##0.00' then EFmt.ItemIndex:= 1
  else if FActiveSheet.CurCell.Style.FormatString = '0.' then EFmt.ItemIndex:= 2
  else if FActiveSheet.CurCell.Style.FormatString = '￥#,##0.00;[红色]￥-#,##0.00' then EFmt.ItemIndex:= 3
  else if FActiveSheet.CurCell.Style.FormatString = '_-￥* #,##0_-;-￥* #,##0_-;_-￥* "-"_-;_-@_-' then EFmt.ItemIndex:= 4
  else if FActiveSheet.CurCell.Style.FormatString = 'yyyy-m-d;@' then EFmt.ItemIndex:= 5
  else if FActiveSheet.CurCell.Style.FormatString = 'yyyy"年"m"月"d"日";@' then EFmt.ItemIndex:= 6
  else if FActiveSheet.CurCell.Style.FormatString = 'h:mm:ss;@' then EFmt.ItemIndex:= 7
  else if FActiveSheet.CurCell.Style.FormatString = '0.00%' then EFmt.ItemIndex:= 8
  else if FActiveSheet.CurCell.Style.FormatString = '#?/10000' then EFmt.ItemIndex:= 9
  else if FActiveSheet.CurCell.Style.FormatString = '0.00E+00' then EFmt.ItemIndex:= 10
  else if FActiveSheet.CurCell.Style.FormatString = '@' then EFmt.ItemIndex:= 11
  else begin EFmt.ItemIndex:= -1; EFmt.Text:= FActiveSheet.CurCell.Style.FormatString; end;
end;

procedure TFrameTableDesigner.N1Click(Sender: TObject);
begin
  EjunSheetControl1.Delete(FActiveSheet.TabIndex);
end;

procedure TFrameTableDesigner.tbZoomChange(Sender: TObject);
begin
  EjunSheetControl1.ActiveSheet.ViewZoom:= Round(tbZoom.Position /(tbZoom.Max + tbZoom.Min) * 2 * 100);
end;

procedure TFrameTableDesigner.mDeleteRowBlockClick(Sender: TObject);
var       //删除行块
  X, Y, Dy: Integer;
begin
  Dy:= FActiveSheet.Selection.RowCount;
  for X:= FActiveSheet.Selection.Range.Left to FActiveSheet.Selection.Range.Right do
    for Y:= FActiveSheet.Selection.Range.Top + Dy to FActiveSheet.RowCount do
      CopyCell(X, Y, X, Y - Dy);
  FActiveSheet.Calculate;
end;

procedure TFrameTableDesigner.mDeleteColBlockClick(Sender: TObject);
var       //删除列块
  X, Y, Dx: Integer;
begin
  Dx:= FActiveSheet.Selection.ColCount;
  for Y:= FActiveSheet.Selection.Range.Top to FActiveSheet.Selection.Range.Bottom do
    for X:= FActiveSheet.Selection.Range.Left + Dx to FActiveSheet.RowCount do
      CopyCell(X, Y, X - Dx, Y);
  FActiveSheet.Calculate;
end;

procedure TFrameTableDesigner.CopyCell(srcCol, srcRow, destCol, destRow: Integer);
var       //复制表中单元格
  i: Integer;
begin
  FActiveSheet.Cells[destCol, destRow].Assign(FActiveSheet.Cells[srcCol, srcRow]);
  FActiveSheet.Cells[destCol, destRow].Style.Assign(FActiveSheet.Cells[srcCol, srcRow].Style);
  FActiveSheet.CellType[destCol, destRow]:= FActiveSheet.CellType[srcCol, srcRow];
  case FActiveSheet.CellType[srcCol, srcRow] of
  cellRadioBox: begin
      FActiveSheet.RadioCells[destCol, destRow].ClearItems;
      for i:= 0 to FActiveSheet.RadioCells[srcCol, srcRow].ItemCount - 1 do
        FActiveSheet.RadioCells[destCol, destRow].AddItem(FActiveSheet.RadioCells[srcCol, srcRow].Items[i]);
    end;
  cellCheckBox: FActiveSheet.CheckCells[destCol, destRow].Caption:= FActiveSheet.CheckCells[srcCol, srcRow].Caption;
  cellComboBox, cellDropDownList: begin
      FActiveSheet.ComboCells[destCol, destRow].ClearItems;
      for i:= 0 to FActiveSheet.ComboCells[srcCol, srcRow].ItemCount - 1 do
        FActiveSheet.ComboCells[destCol, destRow].AddItem(FActiveSheet.ComboCells[srcCol, srcRow].Items[i]);
    end;
  end;
end;

end.
