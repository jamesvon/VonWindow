(*******************************************************************************
  含树结构表格组件(TFrameTreeTable)使用说明：
    property SQLToLoadChildren: string            查询子节点用SQL语句
    property DBConn: TADOConnection               数据库连接
    property DisplayFieldNames: string            显示字段名称，以逗号间隔
    property ImgFieldName: string                 图标字段名称
    property KeyFieldName: string                 主键字段名称
    property ParentFieldName: string              父级关联字段名称
    property OnUpdateOrder: TZcGridTreeNodeEvent  移动节点时回调事件
    property AllowCheckChild: Boolean             同时选中子节点控制
    property DisplayCheckBox: Boolean             是否显示选项
    property DisplayTool: Boolean                 是否显示工具
    property AutoExpand: Boolean                  是否自动展开
*******************************************************************************)
unit UFrameTreeTable;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZcGridStyle, ZcCalcExpress, ZcFormulas, ZcTrees, ComCtrls, ToolWin,
  ZcBaseCtrls, ZJGrid, ZcDataGrid, ZcExGrid, ImgList, ZcGridClasses, DB, ADODB,
  System.ImageList;

type
  TFrameTreeTable = class(TFrame)
    imgButton: TImageList;
    imgTree: TImageList;
    ToolBar1: TToolBar;
    btnTreeNew: TToolButton;
    ToolButton2: TToolButton;
    btnTreeAdd: TToolButton;
    btnTreeAddChild: TToolButton;
    btnTreeEdit: TToolButton;
    ToolButton7: TToolButton;
    btnTreeDelete: TToolButton;
    ToolButton1: TToolButton;
    btnTreeMoveToTop: TToolButton;
    btnTreeMoveToUp: TToolButton;
    btnTreeMoveToDown: TToolButton;
    btnTreeMoveToBottom: TToolButton;
    ToolButton5: TToolButton;
    btnTreeMoveToLeft: TToolButton;
    btnTreeMoveToRight: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    procedure btnTreeMoveToTopClick(Sender: TObject);
    procedure btnTreeMoveToUpClick(Sender: TObject);
    procedure btnTreeMoveToDownClick(Sender: TObject);
    procedure btnTreeMoveToBottomClick(Sender: TObject);
    procedure btnTreeMoveToLeftClick(Sender: TObject);
    procedure btnTreeMoveToRightClick(Sender: TObject);
    procedure btnTreeAddClick(Sender: TObject);
    procedure btnTreeAddChildClick(Sender: TObject);
    procedure treeItemTreeNodeExpand(Sender: TObject; ANode: TZcTreeNode);
    procedure treeItemTreeNodeCheckChanged(Sender: TObject; ANode: TZcTreeNode);
  private
    { Private declarations }
    FDisplayFields: TStringList;
    FImgFieldName: string;
    FOnUpdateOrder: TZcGridTreeNodeEvent;
    FKeyFieldName: string;
    FAllowCheckChild: Boolean;
    FParentFieldName: string;
    FDBConn: TADOConnection;
    FSQLToLoadChildren: string;
    FAutoExpand: Boolean;
    FOnCheckNode: TZcGridTreeNodeEvent;
    function GetDisplayCheckBox: Boolean;
    function GetDisplayFieldNames: string;
    function GetDisplayTool: Boolean;
    procedure SetAllowCheckChild(const Value: Boolean);
    procedure SetDisplayCheckBox(const Value: Boolean);
    procedure SetDisplayFieldNames(const Value: string);
    procedure SetDisplayTool(const Value: Boolean);
    procedure SetImgFieldName(const Value: string);
    procedure SetKeyFieldName(const Value: string);
    procedure SetOnUpdateOrder(const Value: TZcGridTreeNodeEvent);
    procedure SetParentFieldName(const Value: string);
    procedure SetDBConn(const Value: TADOConnection);
    procedure SetSQLToLoadChildren(const Value: string);
    procedure SetAutoExpand(const Value: Boolean);
    procedure SetOnCheckNode(const Value: TZcGridTreeNodeEvent);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Load(ParentNode: TZcTreeNode);
    function AddNode: TZcTreeNode;
    function AddChild: TZcTreeNode;
    procedure SelectNode(ANode: TZcTreeNode; CheckState: TZcTreeCheckState);
    procedure SetNode(ANode: TZcTreeNode; Data: TDataSet);
    function FindNodeByKey(KeyValue: Integer): TZcTreeNode;
  published
    /// <summary>查询子节点用SQL语句</summary>
    property SQLToLoadChildren: string read FSQLToLoadChildren
      write SetSQLToLoadChildren;
    /// <summary>数据库连接</summary>
    property DBConn: TADOConnection read FDBConn write SetDBConn;
    /// <summary>显示字段名称，以逗号间隔</summary>
    property DisplayFieldNames: string read GetDisplayFieldNames
      write SetDisplayFieldNames;
    /// <summary>图标字段名称</summary>
    property ImgFieldName: string read FImgFieldName write SetImgFieldName;
    /// <summary>主键字段名称</summary>
    property KeyFieldName: string read FKeyFieldName write SetKeyFieldName;
    /// <summary>父级关联字段名称</summary>
    property ParentFieldName: string read FParentFieldName
      write SetParentFieldName;
    /// <summary>移动节点时回调事件</summary>
    property OnUpdateOrder: TZcGridTreeNodeEvent read FOnUpdateOrder
      write SetOnUpdateOrder;
    /// <summary>当选中某个节点事件</summary>
    property OnCheckNode: TZcGridTreeNodeEvent read FOnCheckNode write SetOnCheckNode;
    /// <summary>同时选中子节点控制</summary>
    property AllowCheckChild: Boolean read FAllowCheckChild
      write SetAllowCheckChild;
    /// <summary>是否显示选项</summary>
    property DisplayCheckBox: Boolean read GetDisplayCheckBox
      write SetDisplayCheckBox;
    /// <summary>是否显示工具</summary>
    property DisplayTool: Boolean read GetDisplayTool write SetDisplayTool;
    /// <summary>是否自动展开</summary>
    property AutoExpand: Boolean read FAutoExpand write SetAutoExpand;
  end;

var
  RootID: Integer;

implementation

{$R *.dfm}
{ TFrame1 }

function TFrameTreeTable.AddChild: TZcTreeNode;
var
  I: Integer;
  ANode: TZcTreeNode;
begin
  if Assigned(treeItem.CurNode) then
  begin
    ANode:= treeItem.CurNode;
    ANode.ExpandAll;
    Result := ANode.AddChild();
  end
  else
  begin
    Result := treeItem.GridTree.AddChild();
  end;
end;

function TFrameTreeTable.AddNode: TZcTreeNode;
var
  pNode: TZcTreeNode;
  I: Integer;
begin
  if Assigned(treeItem.CurNode) and Assigned(treeItem.CurNode.Parent) then
  begin
    Result := treeItem.CurNode.Parent.AddChild();
  end
  else
  begin
    Result := treeItem.GridTree.AddChild();
  end;
end;

procedure TFrameTreeTable.btnTreeAddChildClick(Sender: TObject);
begin
  AddChild();
end;

procedure TFrameTreeTable.btnTreeAddClick(Sender: TObject);
begin
  AddNode();
end;

procedure TFrameTreeTable.btnTreeMoveToBottomClick(Sender: TObject);
begin // Move to bottom
  if not Assigned(treeItem.CurNode) then
    Exit;
  repeat
  until not treeItem.CurNode.DownMove;
  if Assigned(FOnUpdateOrder) then
    FOnUpdateOrder(Self, treeItem.CurNode.FirstSibling);
end;

procedure TFrameTreeTable.btnTreeMoveToDownClick(Sender: TObject);
begin // Move down
  if not Assigned(treeItem.CurNode) then
    Exit;
  if treeItem.CurNode.DownMove then
    if Assigned(FOnUpdateOrder) then
      FOnUpdateOrder(Self, treeItem.CurNode.PrevSibling);
end;

procedure TFrameTreeTable.btnTreeMoveToLeftClick(Sender: TObject);
var
  szNode: TZcTreeNode;
begin // Move to left
  if not Assigned(treeItem.CurNode) then
    Exit;
  szNode := treeItem.CurNode.NextSibling;
  if treeItem.CurNode.UpLevel then
    if Assigned(FOnUpdateOrder) then
    begin
      FOnUpdateOrder(Self, treeItem.CurNode);
      if Assigned(szNode) then
        FOnUpdateOrder(Self, szNode);
    end;
end;

procedure TFrameTreeTable.btnTreeMoveToRightClick(Sender: TObject);
var
  szNode: TZcTreeNode;
begin // Move to right
  if not Assigned(treeItem.CurNode) then
    Exit;
  szNode := treeItem.CurNode.NextSibling;
  if treeItem.CurNode.DownLevel then
    if Assigned(FOnUpdateOrder) then
    begin
      FOnUpdateOrder(Self, treeItem.CurNode);
      if Assigned(szNode) then
        FOnUpdateOrder(Self, szNode);
    end;
end;

procedure TFrameTreeTable.btnTreeMoveToTopClick(Sender: TObject);
begin // Move to top
  if not Assigned(treeItem.CurNode) then
    Exit;
  repeat
  until not treeItem.CurNode.UpMove;
  if Assigned(FOnUpdateOrder) then
    FOnUpdateOrder(Self, treeItem.CurNode);
end;

procedure TFrameTreeTable.btnTreeMoveToUpClick(Sender: TObject);
begin // Move up
  if not Assigned(treeItem.CurNode) then
    Exit;
  treeItem.CurNode.UpMove;
  if Assigned(FOnUpdateOrder) then
    FOnUpdateOrder(Self, treeItem.CurNode);
end;

constructor TFrameTreeTable.Create(AOwner: TComponent);
begin
  inherited;
  FDisplayFields := TStringList.Create;
end;

destructor TFrameTreeTable.Destroy;
begin
  FDisplayFields.Free;
  inherited;
end;

function TFrameTreeTable.FindNodeByKey(KeyValue: Integer): TZcTreeNode;
  function FindChildren(ANode: TZcTreeNode): TZcTreeNode;
  begin
    while Assigned(ANode) do begin
      if ANode.ID = KeyValue then begin
        Result:= ANode;
        Exit;
      end;
      Result:= FindChildren(ANode.FirstChild);
      if Assigned(Result) then Exit;
      ANode:= ANode.NextSibling;
    end;
    Result:= nil;
  end;
begin
  Result:= FindChildren(treeItem.GridTree.FirstSibling);
end;

function TFrameTreeTable.GetDisplayCheckBox: Boolean;
begin
  Result := treeItem.AlwaysShowTreeCheckBox;
end;

function TFrameTreeTable.GetDisplayFieldNames: string;
begin
  Result := FDisplayFields.DelimitedText;
end;

function TFrameTreeTable.GetDisplayTool: Boolean;
begin
  Result := ToolBar1.Visible;
end;

procedure TFrameTreeTable.Load(ParentNode: TZcTreeNode);
var
  szNode: TZcTreeNode;
  szPID: Integer;
  szQuery: TADOQuery;
begin
  if Assigned(ParentNode) then
    szPID := ParentNode.ID
  else
    szPID := RootID;
  szQuery := TADOQuery.Create(nil);
  with szQuery do
    try
      Connection := FDBConn;
      SQL.Text := FSQLToLoadChildren;
      Prepared := True;
      szQuery.Parameters[0].Value := szPID;
      Open;
      treeItem.BeginUpdate;
      while not EOF do
      begin
        if Assigned(ParentNode) then
          szNode := ParentNode.AddChild()
        else
          szNode := treeItem.GridTree.AddChild();
        SetNode(szNode, szQuery);
        if FAutoExpand then Load(szNode)
        else szNode.SupposeHasChildren := True;
        Next;
      end;
    finally
      Free;
      treeItem.EndUpdate;
    end;
end;

procedure TFrameTreeTable.SelectNode(ANode: TZcTreeNode;
  CheckState: TZcTreeCheckState);

  procedure CheckIt(PNode: TZcTreeNode);
  var
    cNode: TZcTreeNode;
  begin
    PNode.CheckState:= CheckState;
    if FAllowCheckChild then begin
      cNode:= PNode.FirstChild;
      while Assigned(cNode) do begin
        CheckIt(cNode);
        cNode:= cNode.NextSibling;
      end;
    end;
  end;
begin
  if Assigned(ANode) then CheckIt(ANode)
  else begin
    ANode:= treeItem.GridTree.FirstSibling;
    while Assigned(ANode) do begin
      CheckIt(ANode);
      ANode:= ANode.NextSibling;
    end;
  end;
end;

procedure TFrameTreeTable.SetAllowCheckChild(const Value: Boolean);
begin
  FAllowCheckChild := Value;
end;

procedure TFrameTreeTable.SetAutoExpand(const Value: Boolean);
begin
  FAutoExpand := Value;
end;

procedure TFrameTreeTable.SetDBConn(const Value: TADOConnection);
begin
  FDBConn := Value;
end;

procedure TFrameTreeTable.SetDisplayCheckBox(const Value: Boolean);
begin
  treeItem.AlwaysShowTreeCheckBox := Value;
end;

procedure TFrameTreeTable.SetDisplayFieldNames(const Value: string);
begin
  FDisplayFields.Delimiter := ',';
  FDisplayFields.DelimitedText := Value;
end;

procedure TFrameTreeTable.SetDisplayTool(const Value: Boolean);
begin
  ToolBar1.Visible := Value;
end;

procedure TFrameTreeTable.SetImgFieldName(const Value: string);
begin
  FImgFieldName := Value;
end;

procedure TFrameTreeTable.SetKeyFieldName(const Value: string);
begin
  FKeyFieldName := Value;
end;

procedure TFrameTreeTable.SetNode(ANode: TZcTreeNode; Data: TDataSet);
var
  I: Integer;
begin
  if FImgFieldName <> '' then
    ANode.ImageIndex := Data.FieldByName(FImgFieldName).AsInteger;
  ANode.ID := Data.FieldByName(FKeyFieldName).AsInteger;
  ANode.Caption := Data.FieldByName(FDisplayFields[0]).AsString;
  for I := 1 to FDisplayFields.Count - 1 do
  begin
    treeItem.Cells[I + 1, ANode.RowIndex].Style.FormatString := '@';
    treeItem.Cells[I + 1, ANode.RowIndex].AsString :=
      Data.FieldByName(FDisplayFields[I]).AsString;
  end;
end;

procedure TFrameTreeTable.SetOnCheckNode(const Value: TZcGridTreeNodeEvent);
begin
  FOnCheckNode := Value;
end;

procedure TFrameTreeTable.SetOnUpdateOrder(const Value: TZcGridTreeNodeEvent);
begin
  FOnUpdateOrder := Value;
end;

procedure TFrameTreeTable.SetParentFieldName(const Value: string);
begin
  FParentFieldName := Value;
end;

procedure TFrameTreeTable.SetSQLToLoadChildren(const Value: string);
begin
  FSQLToLoadChildren := Value;
end;

procedure TFrameTreeTable.treeItemTreeNodeCheckChanged(Sender: TObject;
  ANode: TZcTreeNode);

  procedure CheckChildren(pNode: TZcTreeNode);
  var
    child: TZcTreeNode;
  begin
    child:= pNode.FirstChild;
    while Assigned(child) do begin
      if child.Checked <> pNode.Checked then begin
        child.Checked:= pNode.Checked;
        if Assigned(FOnCheckNode) then
          FOnCheckNode(self, child);
      end;
      CheckChildren(child);
      child:= child.NextSibling;
    end;
  end;

  procedure CheckParent(cNode: TZcTreeNode);
  var
    pNode, child: TZcTreeNode;
    chk: boolean;
  begin
    pNode:= cNode.Parent;
    if not Assigned(pNode) then Exit;
    chk:= true;
    child:= pNode.FirstChild;
    while Assigned(child)and chk do begin
      chk:= chk and child.Checked;
      child:= child.NextSibling;
    end;
    if pNode.Checked <> chk then begin
      pNode.Checked:= chk;
      if Assigned(FOnCheckNode) then
        FOnCheckNode(self, pNode);
    end;
    CheckParent(pNode);
  end;
begin
  treeItem.OnTreeNodeCheckChanged:= nil;
  if Assigned(FOnCheckNode) then FOnCheckNode(self, ANode);
  if FAllowCheckChild then CheckChildren(ANode);
  if FAllowCheckChild then CheckParent(ANode);
  treeItem.OnTreeNodeCheckChanged:= treeItemTreeNodeCheckChanged;
end;

procedure TFrameTreeTable.treeItemTreeNodeExpand(Sender: TObject;
  ANode: TZcTreeNode);
begin
  if not ANode.HasChildren then
    Load(ANode);
end;

initialization

RootID := 0; // 树形结构根节点虚拟 ID 为 0，也就是说所有更节点的值为 0.

end.
