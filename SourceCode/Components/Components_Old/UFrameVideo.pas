unit UFrameVideo;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  UFrameScreenItem, Vcl.ExtCtrls,
  Vcl.Imaging.pngimage, Vcl.Imaging.jpeg, Vcl.ComCtrls, System.ImageList,
  Vcl.ImgList, Vcl.ToolWin, UVonVideo, Vcl.StdCtrls, UVonConfig;

type
  TCameraInfo = class
    FSetting: TVonSetting;
    FMachineType: string;
    FMachineName: string;
    FID: TGUID;
    FMachineCode: string;
    FIdx: TGUID;
    FPosition: string;
    FMachineKind: string;
  private
    function GetSettings(Name: string): string;
    procedure SetSettings(Name: string; const Value: string);
  public
    constructor Create;
    destructor Destroy; override;
    procedure SetSettingText(text: string);
    function GetSettingText: string;
    property Settings[Name: string]: string read GetSettings write SetSettings;
  published
    /// <summary>序号</summary>
    property ID: TGUID read FID write FID;
    /// <summary>名称</summary>
    property MachineName: string read FMachineName write FMachineName;
    /// <summary>设备编码</summary>
    property MachineCode: string read FMachineCode write FMachineCode;
    /// <summary>设备类型</summary>
    property MachineKind: string read FMachineKind write FMachineKind;
    /// <summary>型号</summary>
    property MachineType: string read FMachineType write FMachineType;
    /// <summary>安装位置</summary>
    property Position: string read FPosition write FPosition;
    /// <summary>所属隧道</summary>
    property Idx: TGUID read FIdx write FIdx;
  end;

  TFrameVideo = class(TFrameScreenItem)
    imgVideo: TImageList;
    plContainer: TPanel;
    ToolBar1: TToolBar;
    lbName: TLabel;
    btnSplit1: TToolButton;
    btnHD_Nml: TToolButton;
    btnRecord: TToolButton;
    btnPicture: TToolButton;
    btnCaption: TToolButton;
    btnSplit2: TToolButton;
    btnPZTUp: TToolButton;
    btnPZTLeft: TToolButton;
    btnPZTRight: TToolButton;
    btnPZTDown: TToolButton;
    procedure btnHD_NmlClick(Sender: TObject);
    procedure btnRecordClick(Sender: TObject);
    procedure btnPictureClick(Sender: TObject);
    procedure btnCaptionClick(Sender: TObject);
  private
    { Private declarations }
    FVideoIndex: Integer;
    FCameraInfo: TCameraInfo;
    procedure SetVideoIndex(const Value: Integer);
    procedure SetCameraInfo(const Value: TCameraInfo);
  public
    { Public declarations }
    procedure CloseMedia;
    property VideoIndex: Integer read FVideoIndex write SetVideoIndex;
    property CameraInfo: TCameraInfo read FCameraInfo write SetCameraInfo;
  end;

var
  FrameVideo: TFrameVideo;

implementation

uses UPlatformDB;

{ TCameraInfo }

constructor TCameraInfo.Create;
begin
  inherited;
  FSetting:= TVonSetting.Create;
end;

destructor TCameraInfo.Destroy;
begin
  FSetting.Free;
  inherited;
end;

function TCameraInfo.GetSettings(Name: string): string;
begin
  Result:= FSetting[Name];
end;

function TCameraInfo.GetSettingText: string;
begin
  Result:= FSetting.Text[EVonSettingKind.VSK_PARENTHESES];
end;

procedure TCameraInfo.SetSettings(Name: string; const Value: string);
begin
  FSetting[Name]:= Value;
end;

procedure TCameraInfo.SetSettingText(text: string);
begin
  FSetting.Text[EVonSettingKind.VSK_PARENTHESES]:= text;
end;

{$R *.dfm}

procedure TFrameVideo.btnCaptionClick(Sender: TObject);
var
  S: AnsiString;
begin
  inherited;
  S:= FPlatformDB.AppPath + 'Temp\Caption_' + FormatDatetime('yymmdd_hhnnss', Now) + '.jpg';
  Capture(FVideoIndex, PAnsichar(S));
end;

procedure TFrameVideo.btnHD_NmlClick(Sender: TObject);
var
  S: AnsiString;
begin
  inherited;
  btnHD_Nml.ImageIndex:= 1 - btnHD_Nml.ImageIndex;
  if btnHD_Nml.ImageIndex = 0 then begin
    S:= FCameraInfo.Settings['NomalRtsp'];
    btnHD_Nml.Hint:= '高清播放';
  end else begin
    S:= FCameraInfo.Settings['HighRtsp'];
    btnHD_Nml.Hint:= '标清播放';
  end;
  OpenVideo(FVideoIndex, plContainer.Handle, StrToInt(FCameraInfo.Settings['FrameInterval']),
    plContainer.Width, plContainer.Height, PAnsichar(S));
end;

procedure TFrameVideo.btnPictureClick(Sender: TObject);
var
  S: AnsiString;
begin
  inherited;
  btnPicture.ImageIndex:= 9 - btnPicture.ImageIndex;
  if btnPicture.ImageIndex = 4 then begin
    StopSavePicture(FVideoIndex);
    btnPicture.Hint:= '连续抓拍';
  end else begin
    S:= FPlatformDB.AppPath + 'Temp\';
    StartSavePicture(FVideoIndex, 100, PAnsichar(S));
    btnPicture.Hint:= '停止抓拍';
  end;
end;

procedure TFrameVideo.btnRecordClick(Sender: TObject);
var
  S: AnsiString;
begin
  inherited;
  btnRecord.ImageIndex:= 5 - btnRecord.ImageIndex;
  if btnRecord.ImageIndex = 2 then begin
    StopSaveVideo(FVideoIndex);
    btnRecord.Hint:= '录像';
  end else begin
    S:= FPlatformDB.AppPath + 'Temp\Caption_' + FormatDatetime('yymmdd_hhnnss', Now) + '.MP4';
    StartSaveVideo(FVideoIndex, PAnsichar(S));
    btnRecord.Hint:= '停止录像';
  end;
end;

procedure TFrameVideo.CloseMedia;
begin
  CloseVideo(FVideoIndex);
end;

procedure TFrameVideo.SetCameraInfo(const Value: TCameraInfo);
var
  S: AnsiString;
begin
  FCameraInfo := Value;
  S:= FCameraInfo.Settings['NomalRtsp'];
  lbName.Caption:= FCameraInfo.MachineName;
  OpenVideo(FVideoIndex, plContainer.Handle, StrToInt(FCameraInfo.Settings['FrameInterval']),
    plContainer.Width, plContainer.Height, PAnsichar(S));
  btnPZTUp.Visible:= FCameraInfo.Settings['SupportPZT'] <> '';
  btnPZTDown.Visible:= btnPZTUp.Visible;
  btnPZTLeft.Visible:= btnPZTUp.Visible;
  btnPZTRight.Visible:= btnPZTUp.Visible;
  btnSplit2.Visible:= btnPZTUp.Visible;
end;

procedure TFrameVideo.SetVideoIndex(const Value: Integer);
begin
  FVideoIndex := Value;
end;

end.
