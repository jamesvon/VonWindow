unit UVonComponentHelper;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, UFrameGridBar, StdCtrls, ComCtrls, Spin, ExtCtrls, UThDB,
  UThInfo, UFrameHouseware;

type
  TVonEditHelp = class
  private

  public

  end;

  TVonComboBoxHelp = class
  private
    list: TStrings;
    procedure EventOfKeyPress(Sender: TObject; var Key: char);
  public
    constructor Create(Box: TComboBox);
    destructor Destroy;
  end;

implementation

{ TVonComboBoxHelp }

constructor TVonComboBoxHelp.Create(Box: TComboBox);
begin
  list:= TStrings(Box.Items.NewInstance);
  Box.OnKeyPress:= EventOfKeyPress;
end;

destructor TVonComboBoxHelp.Destroy;
begin
  list.Free;
end;

procedure TVonComboBoxHelp.EventOfKeyPress(Sender: TObject; var Key: char);
begin

end;

end.
