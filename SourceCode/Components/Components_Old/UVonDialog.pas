unit UVonDialog;

interface

uses
{$IF DEFINED(CLR)}
  System.ComponentModel.Design.Serialization, WinUtils,
{$ENDIF}
{$IF DEFINED(LINUX)}
  WinUtils,
{$ENDIF}
  Winapi.Windows, Winapi.Messages, System.SysUtils, Winapi.CommDlg, Vcl.Printers,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.StdCtrls, Vcl.ExtCtrls,
  Winapi.ShlObj, Winapi.CommCtrl, Vcl.ComCtrls, System.UITypes, Vcl.Buttons;

type
  TVonDatetimeOptions = (voDatetime, voYear, voMonth, voDay, voHour, voMinute, voSecond);
  TVonDatetimeDialog = class
  private
    FOwner: TComponent;
    FDatetime: TDatetime;
    FOptions: TVonDatetimeOptions;
    function GetDatetime: TDatetime;
    procedure SetDatetime(const Value: TDatetime);
{$IF DEFINED(CLR)}
  protected
    function LaunchDialog(DialogData: IntPtr): Bool; override;
{$ENDIF}
  public
    constructor Create(AOwner: TComponent);
    destructor Destroy; override;
    function Execute: Boolean;
  published
    property Options: TVonDatetimeOptions read FOptions write FOptions default TVonDatetimeOptions.voDatetime;
    property Datetime: TDatetime read GetDatetime write SetDatetime;
  end;

implementation

uses System.DateUtils;

{ TVonDialog }

constructor TVonDatetimeDialog.Create(AOwner: TComponent);
begin
  FOwner:= AOwner;
  FDatetime:= Now;
end;

destructor TVonDatetimeDialog.Destroy;
begin
  inherited;
end;

function TVonDatetimeDialog.Execute: Boolean;
var
  FForm: TForm;
  FDateTimePicker: TDateTimePicker;
  FMonthCalendar: TMonthCalendar;
  lpPoint: TPoint;
  procedure CreateCalendar;
  begin
    FMonthCalendar:= TMonthCalendar.Create(FForm);
    FMonthCalendar.Parent:= FForm;
    FMonthCalendar.Align:= alTop;
    FMonthCalendar.Height:= 190;
    FMonthCalendar.Date:= DateOf(FDatetime);
  end;
  procedure CreateTime;
  begin
    FDateTimePicker:= TDateTimePicker.Create(FForm);
    FDateTimePicker.Parent:= FForm;
    FDateTimePicker.Align:= alTop;
    FDateTimePicker.Kind:= TDateTimeKind.dtkTime;
    FDateTimePicker.DateTime:= TimeOf(FDatetime);
  end;
  procedure SetSize;
  begin
    if GetCursorPos(lpPoint)then
      case FOptions of
      voDatetime: FForm.SetBounds(lpPoint.X, lpPoint.Y, 222, 190 + 32 + 32);
      voYear, voMonth, voDay: FForm.SetBounds(lpPoint.X, lpPoint.Y, 222, 190 + 32);
      voHour, voMinute, voSecond: FForm.SetBounds(lpPoint.X, lpPoint.Y, 222, 32 + 32);
      end;
  end;
begin
  FForm:= TForm.Create(FOwner);
  try
    SetSize;
    case FOptions of
    voDatetime: begin
        CreateCalendar;
        CreateTime;
        FDateTimePicker.Top:= FMonthCalendar.Height;
        FDateTimePicker.Width:= FMonthCalendar.Width;
      end;
    voYear: CreateCalendar;
    voMonth: CreateCalendar;
    voDay: CreateCalendar;
    voHour: CreateTime;
    voMinute: CreateTime;
    voSecond: CreateTime;
    end;
    with TBitBtn.Create(FForm) do begin
      Parent:= FForm;
      Top:= FForm.Height - 32;
      Left:= 60;
      Kind:= TBitBtnKind.bkOK;
      Caption:= 'ȷ��';
      ModalResult:= mrOK;
    end;
    with TBitBtn.Create(FForm) do begin
      Parent:= FForm;
      Top:= FForm.Height - 32;
      Left:= 140;
      Kind:= TBitBtnKind.bkCancel;
      Caption:= '����';
      ModalResult:= mrCancel;
    end;
    FForm.BorderStyle := bsNone;
    FForm.Position := poDesigned;
    Result:= FForm.ShowModal = mrOK;
    if Result then
      FDatetime:= DateOf(FMonthCalendar.Date) + TimeOf(FDateTimePicker.Time);
  finally
    FForm.Free;
  end;
end;

function TVonDatetimeDialog.GetDatetime: TDatetime;
begin
  case FOptions of
  voDatetime: Result:= FDatetime;
  voYear: Result:= EncodeDate(YearOf(FDatetime), 1, 1);
  voMonth: Result:= EncodeDate(YearOf(FDatetime), MonthOf(FDatetime), 1);
  voDay: Result:= DateOf(FDatetime);
  voHour: Result:= EncodeTime(HourOf(FDatetime), 0, 0, 0);
  voMinute: Result:= EncodeTime(HourOf(FDatetime), MinuteOf(FDatetime), 0, 0);
  voSecond: Result:= EncodeTime(HourOf(FDatetime), MinuteOf(FDatetime), SecondOf(FDatetime), 0);
  end;
end;

procedure TVonDatetimeDialog.SetDatetime(const Value: TDatetime);
begin
  FDatetime:= Value;
end;

end.
