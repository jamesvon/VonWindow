unit UVonParamField;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Dialogs,
  Forms, Math, Buttons, StdCtrls, ExtCtrls, ComCtrls, UVonConfig, WinSock, Spin,
  Mask, AdoConEd, CheckLst, UVonSystemFuns, UDlgRichEditor, UDlgImage, EncdDecd,
  ADODB;

type
  /// <summary>字段基础类，在这里面会自动构建一个label来显示提示内容</summary>
  TFieldBase = class (TPanel)
  private
    FIndent: Integer;
    lb: TLabel;
    FFieldType: TVonParamType;
    FOption: string;
    FDefaultValue: string;
    FCaption: string;
    FDataName: string;
    FConn: TADOConnection;
    procedure SetIndent(const Value: Integer);
    function SetIt(AComp: TControl; Al: TAlign): TControl;
    procedure SetConn(const Value: TADOConnection);
  protected
    function GetValue: string; virtual; abstract;
    procedure SetCaption(const Value: string); virtual;
    procedure SetDataName(const Value: string); virtual;
    procedure SetDefaultValue(const Value: string); virtual;
    procedure SetOption(const Value: string); virtual;
    procedure SetValue(const Value: string); virtual; abstract;
  public
    class function CreateByType(AOwner: TComponent; AType: TVonParamType;
      AConn: TADOConnection): TFieldBase; static;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetColor(var R, G, B: Byte; var Rate: Double);
  published
    property Indent: Integer read FIndent write SetIndent;
    property Conn: TADOConnection read FConn write SetConn;
    property DataName: string read FDataName write SetDataName;
    property Caption: string read FCaption write SetCaption;
    property Option: string read FOption write SetOption;
    property FieldType: TVonParamType read FFieldType;
    property DefaultValue: string read FDefaultValue write SetDefaultValue;
    property Value: string read GetValue write SetValue;
  end;
  /// <summary>整数类型字段，用TSpinEdit类型来进行输入</summary>
  TIntField = class(TFieldBase)
  private
    Edit: TSpinEdit;
    function GetIntValue: Integer;
    procedure SetIntValue(const Value: Integer);
  protected
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property IntValue: Integer read GetIntValue write SetIntValue;
  end;
  /// <summary>浮点类型字段</summary>
  TFloatField = class(TFieldBase)
  private
    Edit: TEdit;
    procedure EventOfKeyPress(Sender: TObject; var Key: char);
    function GetFloatValue: Extended;
    procedure SetFloatValue(const Value: Extended);
  protected
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property FloatValue: Extended read GetFloatValue write SetFloatValue;
  end;
  /// <summary>文本字段</summary>
  TTextField = class(TFieldBase)
  private
    Edit: TEdit;
    procedure EventOfExist(Sender: TObject);
  protected
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    constructor Create(AOwner: TComponent); override;
  end;
  /// <summary>掩码文本字段</summary>
  TMaskField = class(TFieldBase)
  private
    Edit: TMaskEdit;
  protected
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
    procedure SetOption(const Value: string); override;
  public
    constructor Create(AOwner: TComponent); override;
  end;
  /// <summary>日期时间字段</summary>
  TDateTimeField = class(TFieldBase)
  private
    EDate: TDateTimePicker;
    ETime: TDateTimePicker;
    function GetDatetime: TDatetime;
    procedure SetDatetime(const Value: TDatetime);
  protected
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
    procedure SetOption(const Value: string); override;
  public
    constructor Create(AOwner: TComponent); override;
    property Datetime: TDatetime read GetDatetime write SetDatetime;
  end;
  /// <summary>含按钮的录入字段</summary>
  TBtnField = class(TFieldBase)
  private
    EEdit: TEdit;
    Btn: TSpeedButton;
    procedure EventOfConnClick(Sender: TObject);
    procedure EventOfGuidClick(Sender: TObject);
    procedure SetOnClick(const Value: TNotifyEvent);
    function GetOnClick: TNotifyEvent;
  protected
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property OnClick: TNotifyEvent read GetOnClick write SetOnClick;
  end;
  /// <summary>大文本字段</summary>
  TMemoField = class(TFieldBase)
  private
    Edit: TMemo;
  protected
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    constructor Create(AOwner: TComponent); override;
  end;
  /// <summary>富文本录入字段</summary>
  TRichField = class(TFieldBase)
  private
    Edit: TRichEdit;
    procedure EventOfDblClick(Sender: TObject);
  protected
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property RichEdit: TRichEdit read Edit;
  end;
  /// <summary>单选录入字段</summary>
  TSelectionField = class(TFieldBase)
  private
    Edit: TComboBox;
  protected
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
    procedure SetOption(const Value: string); override;
  public
    constructor Create(AOwner: TComponent); override;
  end;
  /// <summary>选择录入项</summary>
  TChoiceField = class(TFieldBase)
  private
    Edit: TComboBox;
    FIsSQL: Boolean;
    function GetChoiced: Integer;
    procedure SetChoiced(const Value: Integer);
  protected
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
    procedure SetOption(const Value: string); override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property Choiced: Integer read GetChoiced write SetChoiced;
  end;
  /// <summary>布尔型字段</summary>
  TBoolField = class(TFieldBase)
  private
    Edit: TCheckBox;
    function GetChoiced: Boolean;
    procedure SetChoiced(const Value: Boolean);
  protected
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property Choiced: Boolean read GetChoiced write SetChoiced;
  end;
  /// <summary>多选型字段</summary>
  TCheckListField = class(TFieldBase)
  private
    Lst: TCheckListBox;
    FDelimiter: char;
    function GetChoiced: Int64;
    procedure SetChoiced(const Value: Int64);
    procedure SetDelimiter(const Value: char);
  protected
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
    procedure SetOption(const Value: string); override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property Choiced: Int64 read GetChoiced write SetChoiced;
    property Delimiter: char read FDelimiter write SetDelimiter;
  end;
  /// <summary>图形字段</summary>
  TBmpField = class(TFieldBase)
  private
    Img: TImage;
    procedure EventOfDblClick(Sender: TObject);
    function GetBitmap: TBitmap;
    procedure SetBitmap(const Value: TBitmap);
  protected
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property Bitmap: TBitmap read GetBitmap write SetBitmap;
  end;
  /// <summary>树形结构字段</summary>
  TTreeField = class(TFieldBase)
  private
    FTree: TTreeView;
    function GetChoiced: Int64;
    procedure SetChoiced(const Value: Int64);
  protected
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
    procedure SetOption(const Value: string); override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property Tree: TTreeView read FTree;
    property Choiced: Int64 read GetChoiced write SetChoiced;
  end;
  /// <summary>组合录入字段</summary>
  TGroupField = class(TFieldBase)
  private
    lst: TMemo;
    lp: TPanel;
    Btn: TSpeedButton;
    FFmt: string;
    function GetLins: TStrings;
    procedure EventOfClick(Sender: TObject);
  protected
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
    procedure SetOption(const Value: string); override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property Lines: TStrings read GetLins;
  end;

implementation

uses DateUtils;

{ TFieldBase }

class function TFieldBase.CreateByType(AOwner: TComponent; AType: TVonParamType;
  AConn: TADOConnection): TFieldBase;
begin
  case AType of
  PT_Text: Result:= TTextField.Create(AOwner);
  PT_MASK: Result:= TMaskField.Create(AOwner);
  PT_Int:  Result:= TIntField.Create(AOwner);
  PT_Float:Result:= TFloatField.Create(AOwner);
  PT_Conn: begin Result:= TBtnField.Create(AOwner); with TBtnField(Result) do Btn.OnClick:= EventOfConnClick; end;
  PT_GUID: begin Result:= TBtnField.Create(AOwner); with TBtnField(Result) do Btn.OnClick:= EventOfGuidClick; end;
  PT_Date: begin Result:= TDateTimeField.Create(AOwner); with TDateTimeField(Result) do ETime.Visible:= False; end;
  PT_Time: begin Result:= TDateTimeField.Create(AOwner); with TDateTimeField(Result) do EDate.Visible:= False; end;
  PT_DateTime: Result:= TDateTimeField.Create(AOwner);
  PT_Memo: Result:= TMemoField.Create(AOwner);
  PT_RichText: Result:= TRichField.Create(AOwner);
  PT_Selection: Result:= TSelectionField.Create(AOwner);
  PT_Choice: Result:= TChoiceField.Create(AOwner);
  PT_Bool: Result:= TBoolField.Create(AOwner);
  PT_CheckList: Result:= TCheckListField.Create(AOwner);
  PT_Bitmap: Result:= TBmpField.Create(AOwner);
  PT_TREE: Result:= TTreeField.Create(AOwner);
  PT_GROUP: Result:= TGroupField.Create(AOwner);
  PT_Dialog: Result:= TBtnField.Create(AOwner);
  end;
  Result.Conn:= AConn;
  Result.FFieldType:= AType;
end;

constructor TFieldBase.Create(AOwner: TComponent);
begin
  inherited;
  Height:= 23;
  BevelOuter:= bvLowered;
  ShowCaption:= False;
  lb:= TLabel(SetIt(TLabel.Create(Self), alLeft));
  lb.Margins.Bottom:= 5;
  lb.Margins.Left:= 5;
  lb.Margins.Right:= 5;
  lb.Margins.Top:= 5;
  lb.AlignWithMargins:= True;
  lb.Layout:= tlCenter;
end;

function TFieldBase.SetIt(AComp: TControl; Al: TAlign): TControl;
begin
  Result:= AComp;
  AComp.Parent:= Self;
  AComp.Align:= al;
end;

destructor TFieldBase.Destroy;
begin
  DestroyComponents;
  inherited;
end;

procedure TFieldBase.SetCaption(const Value: string);
begin
  FCaption := Value;
  lb.Caption:= Value;
end;

procedure TFieldBase.SetColor(var R, G, B: Byte; var Rate: Double);
var
  valueRatio: Double;
begin
  valueRatio:= Random(256);
  R:= Round(R + valueRatio * Rate);
  valueRatio:= G + Random(256) * (1 - Rate);
  G:= Round(G + valueRatio * Rate);
  valueRatio:= B + Random(256) * Rate;
  B:= Round(B + valueRatio * Rate);
  Rate:= 1 - Rate;
  Self.ParentColor:= False;
  Self.ParentBackground:= False;
  Color:= RGB(R, G, B);
end;

procedure TFieldBase.SetConn(const Value: TADOConnection);
begin
  FConn := Value;
end;

procedure TFieldBase.SetDataName(const Value: string);
begin
  FDataName := Value;
end;

procedure TFieldBase.SetDefaultValue(const Value: string);
begin
  FDefaultValue := Value;
  SetValue(Value);
end;

procedure TFieldBase.SetIndent(const Value: Integer);
begin
  FIndent := Value;
end;

procedure TFieldBase.SetOption(const Value: string);
begin
  FOption := Value;
end;

{ TIntField }

constructor TIntField.Create(AOwner: TComponent);
begin
  inherited;
  Edit:= TSpinEdit(SetIt(TSpinEdit.Create(Self), alClient));
end;

function TIntField.GetIntValue: Integer;
begin
  Result:= Edit.Value;
end;

function TIntField.GetValue: string;
begin
  Result:= IntToStr(Edit.Value);
end;

procedure TIntField.SetIntValue(const Value: Integer);
begin
  Edit.Value:= Value;
end;

procedure TIntField.SetValue(const Value: string);
var
  V: Integer;
begin
  V:= 0;
  TryStrToInt(Value, V);
  Edit.Value:= V;
end;

{ TFloatField }

constructor TFloatField.Create(AOwner: TComponent);
begin
  inherited;
  Edit:= TEdit(SetIt(TEdit.Create(Self), alClient));
  Edit.OnKeyPress:= EventOfKeyPress;
end;

procedure TFloatField.EventOfKeyPress(Sender: TObject; var Key: char);
var
  V: Extended;
begin
  case Key of
  #8, #10: begin end;
  else if not TryStrToFloat(TEdit(Sender).Text + Key, V) then Key:= #0;
  end;
end;

function TFloatField.GetFloatValue: Extended;
begin
  Result:= 0;
  TryStrToFloat(Edit.Text, Result);
end;

function TFloatField.GetValue: string;
var
  iDight: Integer;
begin
  if Edit.Text = '' then  Edit.Text:= '0';
  Result:= Edit.Text;
  if TryStrToInt(FOption, iDight) then
    Result:= FloatToStr(RoundTo(StrToFloat(Edit.Text), iDight));
end;

procedure TFloatField.SetFloatValue(const Value: Extended);
begin
  Edit.Text:= FloatToStr(Value);
end;

procedure TFloatField.SetValue(const Value: string);
begin
  Edit.Text:= Value;
  if Edit.Text = '' then  Edit.Text:= '0';
end;

{ TMaskField }

constructor TMaskField.Create(AOwner: TComponent);
begin
  inherited;
  Edit:= TMaskEdit(SetIt(TMaskEdit.Create(Self), alClient));
end;

function TMaskField.GetValue: string;
begin
  Result:= Edit.Text;
end;

procedure TMaskField.SetOption(const Value: string);
begin
  inherited;
  Edit.EditMask:= Value;
end;

procedure TMaskField.SetValue(const Value: string);
begin
  Edit.Text:= Value;
end;

{ TTextField }

constructor TTextField.Create(AOwner: TComponent);
begin
  inherited;
  Edit:= TEdit(SetIt(TEdit.Create(Self), alClient));
  Edit.OnExit:= EventOfExist;
end;

function IsVaildEmailAddr(EmailAddr: String): boolean;
var
  Number, I: integer;
  TempStr: String;
begin
	TempStr:= EmailAddr;
	Number:= 0;
	for I:= 1 to Length(TempStr) do
	begin
		if (TempStr[I] = '@') then
		INC(Number);//Number存放看输入的字符串有几个@
	end;
	if ((Number = 0)or(Number > 1)) then
	Result:= False //如果没有@或者有多个那么肯定是无效的Email地址
	else
	begin
		if ((TempStr[1] = '@')or(TempStr[length(TempStr)] = '@')) then
		Result:= False //如果@在最开始或者最后也是无效的Email地址
		else
		begin
			I:= pos('@', TempStr); //取得@的位置
			delete(TempStr, 1, I); //删除@和以前的内容
			if (Length(TempStr) < 3) then
			Result:= False //如果剩余部分长度小于3则也是无效的地址
			else
			begin
				if ((pos('.', TempStr) = 0)or(pos('.', TempStr) = length(TempStr))
				or (pos('.', TempStr) = 1))then
				Result:= False //如果剩余字符中没有. 或者.在最后 或者.在最开始都是无效的email地址
				else
				Result:= True; //排除以上所有情况, 剩下的就都符合一个email的格式, 不知我的注释如何, 楼主
			end;
		end;
	end;
end;

procedure TTextField.EventOfExist(Sender: TObject);
var S: string;
  procedure FaildChecked;
  begin
    SetFocus;
    TEdit(Sender).Color:= clRed;
  end;
begin
  S:= TEdit(Sender).Text;
  TEdit(Sender).Color:= clWindow;
  if(Option = 'IP')and (Longword(inet_addr(PAnsiChar(AnsiString(S))))=INADDR_NONE) then FaildChecked;
  if(Option = 'EMAIL')and not IsVaildEmailAddr(S)then FaildChecked;
  if(Option = 'IDENT')and(CheckIdentification(S) <> '')then FaildChecked;
end;

function TTextField.GetValue: string;
begin
  Result:= Edit.Text;
end;

procedure TTextField.SetValue(const Value: string);
begin
  Edit.Text:= Value;
end;

{ TConnField }

constructor TBtnField.Create(AOwner: TComponent);
var
  ht:hBitmap;
begin
  inherited;
  EEdit:= TEdit(SetIt(TEdit.Create(Self), alClient));
  Btn:= TSpeedButton(SetIt(TSpeedButton.Create(Self), alRight));
  ht:=LoadBitmap(hinstance,'BROWER');
  Btn.Glyph.Handle:= ht;
  //Btn.Caption:= '...';
end;

procedure TBtnField.EventOfConnClick(Sender: TObject);
var
  InitialConnStr: WideString;
begin
  with TConnEditForm.Create(Application) do
    try
      InitialConnStr := EEdit.Text;
      EEdit.Text := Edit(InitialConnStr);
    finally
      Free;
    end;
end;

procedure TBtnField.EventOfGuidClick(Sender: TObject);
begin
  EEdit.Text := GetGUIDStr();
end;

function TBtnField.GetOnClick: TNotifyEvent;
begin
  Result:= Btn.OnClick;
end;

function TBtnField.GetValue: string;
begin
  Result:= EEdit.Text;
end;

procedure TBtnField.SetOnClick(const Value: TNotifyEvent);
begin
  Btn.OnClick := Value;
end;

procedure TBtnField.SetValue(const Value: string);
begin
  EEdit.Text:= Value;
end;

{ TDateTimeField }

constructor TDateTimeField.Create(AOwner: TComponent);
begin
  inherited;
  EDate:= TDateTimePicker(SetIt(TDateTimePicker.Create(Self), alClient));
  EDate.Kind:= dtkDate;
  ETime:= TDateTimePicker(SetIt(TDateTimePicker.Create(Self), alRight));
  ETime.Kind:= dtkTime;
end;

function TDateTimeField.GetDatetime: TDatetime;
begin
  Result:= 0;
  if EDate.Visible then Result:= DateOf(EDate.DateTime);
  if ETime.Visible then Result:= Result + TimeOf(ETime.DateTime);
end;

function TDateTimeField.GetValue: string;
begin
  Result:= DateTimeToStr(Datetime);
end;

procedure TDateTimeField.SetDatetime(const Value: TDatetime);
begin
  EDate.DateTime:= DateOf(Value);
  ETime.DateTime:= TimeOf(Value);
end;

procedure TDateTimeField.SetOption(const Value: string);
begin
  inherited;
  FVonSetting.Text[VSK_SEMICOLON]:= Value;
  if FVonSetting.NameValue['SHORT'] = 'TRUE' then
    EDate.DateFormat:= dfLong;
  if FVonSetting.NameValue['DATEFMT'] <> '' then
    EDate.Format:= FVonSetting.NameValue['DATEFMT'];
  if FVonSetting.NameValue['TIMEFMT'] <> '' then
    ETime.Format:= FVonSetting.NameValue['DATEFMT'];
  if FVonSetting.NameValue['MINDATE'] <> '' then
    EDate.MinDate:= StrToDate(FVonSetting.NameValue['MINDATE']);
  if FVonSetting.NameValue['MAXDATE'] <> '' then
    EDate.MaxDate:= StrToDate(FVonSetting.NameValue['MAXDATE']);
end;

procedure TDateTimeField.SetValue(const Value: string);
var
  dt: TDatetime;
begin
  if TryStrToDateTime(Value, dt)then Datetime:= dt;
end;

{ TMemoField }

constructor TMemoField.Create(AOwner: TComponent);
begin
  inherited;
  lb.Align:= alTop;
  Height:= 126;
  Edit:= TMemo(SetIt(TMemo.Create(Self), alClient));
  Edit.ScrollBars:= ssBoth;
end;

function TMemoField.GetValue: string;
begin
  Result:= Edit.Text;
end;

procedure TMemoField.SetValue(const Value: string);
begin
  Edit.Text:= Value;
end;

{ TRichField }

constructor TRichField.Create(AOwner: TComponent);
begin
  inherited;
  lb.Align:= alTop;
  Height:= 126;
  Edit:= TRichEdit(SetIt(TRichEdit.Create(Self), alClient));
  Edit.ScrollBars:= ssBoth;
  Edit.ReadOnly:= True;
  Edit.OnDblClick:= EventOfDblClick;
  Edit.TextHint:= '双击这里进行编辑';
  Edit.Hint:= '双击这里进行编辑';
  Edit.ShowHint:= True;
end;

procedure TRichField.EventOfDblClick(Sender: TObject);
  procedure CopyRich(Org, Dest: TRichEdit);
  var
    st: TMemoryStream;
  begin
    st:= TMemoryStream.Create;
    Org.Lines.SaveToStream(st);
    st.Position:=0;
    Dest.Lines.LoadFromStream(st);
    st.Free;
  end;
begin
  with TFDlgRichEditor.Create(nil)do try
    CopyRich(Edit, Editor);
    ShowModal;
    CopyRich(Editor, Edit);
  finally
    Free;
  end;
end;

function TRichField.GetValue: string;
var
  ms: TMemoryStream;
  ss: TStringStream;
begin
  ms:= TMemoryStream.Create;
  ss:= TStringStream.Create;
  Edit.Lines.SaveToStream(ms);
  ms.Position:= 0;
  EncodeStream(ms, ss);//将内存流编码为base64字符流
  Result:= ss.DataString;
  ms.Free;
  ss.Free;
end;

procedure TRichField.SetValue(const Value: string);
var
  ms: TMemoryStream;
  ss: TStringStream;
begin
  ms:= TMemoryStream.Create;
  ss:= TStringStream.Create(Value);
  DecodeStream(ss, ms);//将内存流编码为base64字符流
  ms.Position:= 0;
  Edit.Lines.LoadFromStream(ms);
  ms.Free;
  ss.Free;
end;

{ TSelectionField }

constructor TSelectionField.Create(AOwner: TComponent);
begin
  inherited;
  Edit:= TComboBox(SetIt(TComboBox.Create(Self), alClient));
end;

function TSelectionField.GetValue: string;
begin
  Result:= Edit.Text;
end;

procedure TSelectionField.SetOption(const Value: string);
var
  szlst: TStringList;
  szKeyField: string;
begin
  inherited;
  szlst:= TStringList.Create;
  szlst.Text:= Value;
  if (szlst.Count > 0)and(szlst.IndexOfName('SQL') >= 0) then
    with TADOQuery.Create(nil) do try
      Connection:= FConn;
      SQL.Text:= szlst.Values['SQL'];
      Open;
      szKeyField:= szlst.Values['ID'];
      if szKeyField = '' then szKeyField:= 'ID';
      Self.Edit.Clear;
      while not EOF do begin
        Self.Edit.Items.Add(FieldByName(szlst.Values['DISPLAY']).AsString);
        Next;
      end;
    finally
      Free;
  end else Edit.Items.Assign(szlst);
  szlst.Free;
end;

procedure TSelectionField.SetValue(const Value: string);
begin
  Edit.Text:= Value;
end;

{ TChoiceField }

constructor TChoiceField.Create(AOwner: TComponent);
begin
  inherited;
  Edit:= TComboBox(SetIt(TComboBox.Create(Self), alClient));
  Edit.Style:= csDropDownList;
end;

function TChoiceField.GetChoiced: Integer;
begin
  if FIsSQL then Result:= Integer(Edit.Items.Objects[Edit.ItemIndex])
  else Result:= Edit.ItemIndex;
end;

function TChoiceField.GetValue: string;
begin
  Result:= Edit.Text;
end;

procedure TChoiceField.SetChoiced(const Value: Integer);
begin
  if FIsSQL then Edit.ItemIndex:= Edit.Items.IndexOfObject(TObject(Value))
  else Edit.ItemIndex:= Value;
end;

procedure TChoiceField.SetOption(const Value: string);
var
  szlst: TStringList;
  szKeyField: string;
begin
  inherited;
  szlst:= TStringList.Create;
  szlst.Text:= Value;
  FIsSQL:= (szlst.Count > 0)and(szlst.IndexOfName('SQL') >= 0);
  if FIsSQL then
    with TADOQuery.Create(nil) do try
      Connection:= FConn;
      SQL.Text:= szlst.Values['SQL'];
      Open;
      szKeyField:= szlst.Values['ID'];
      if szKeyField = '' then szKeyField:= 'ID';
      Self.Edit.Clear;
      while not EOF do begin
        Self.Edit.Items.AddObject(FieldByName(szlst.Values['DISPLAY']).AsString,
          TObject(FieldByName(szKeyField).AsInteger));
        Next;
      end;
    finally
      Free;
  end else Edit.Items.Assign(szlst);
  szlst.Free;
end;

procedure TChoiceField.SetValue(const Value: string);
begin
  Edit.ItemIndex:= Edit.Items.IndexOf(Value);
end;

{ TBoolField }

constructor TBoolField.Create(AOwner: TComponent);
begin
  inherited;
  //lb.Visible:= False;
  Edit:= TCheckBox(SetIt(TCheckBox.Create(Self), alClient));
  Edit.Caption:= lb.Caption;
end;

function TBoolField.GetChoiced: Boolean;
begin
  Result:= Edit.Checked;
end;

function TBoolField.GetValue: string;
begin
  Result:= BoolToStr(Edit.Checked, True);
end;

procedure TBoolField.SetChoiced(const Value: Boolean);
begin
  Edit.Checked:= Value;
end;

procedure TBoolField.SetValue(const Value: string);
begin
  Edit.Checked:= SameText(Value, 'true') or (Value = '1');
end;

{ TCheckListField }

constructor TCheckListField.Create(AOwner: TComponent);
begin
  inherited;
  Lst:= TCheckListBox(SetIt(TCheckListBox.Create(Self), alClient));
  FDelimiter:= ',';
  Height:= 126;
end;

function TCheckListField.GetChoiced: Int64;
begin
  Result:= GetMutiChkByChkList(Lst);
end;

function TCheckListField.GetValue: string;
var
  I: Integer;
begin
  Result:= '';
  for I := 0 to Lst.Count - 1 do
    if Lst.Checked[I] then Result:= Result + FDelimiter + Lst.Items[I];
  if Result <> '' then
    Delete(Result, 1, 1);
end;

procedure TCheckListField.SetChoiced(const Value: Int64);
begin
  SetMutiChkToChkList(Value, Lst);
end;

procedure TCheckListField.SetDelimiter(const Value: char);
begin
  FDelimiter := Value;
end;

procedure TCheckListField.SetOption(const Value: string);
var
  szlst: TStringList;
begin
  inherited;
  szlst:= TStringList.Create;
  szlst.Text:= Value;
  if(szlst.Count > 0)and(szlst.IndexOfName('SQL') >= 0) then
    with TADOQuery.Create(nil) do try
      Connection:= FConn;
      SQL.Text:= szlst.Values['SQL'];
      Open;
      lst.Clear;
      while not EOF do begin
        lst.Items.Add(FieldByName(szlst.Values['DISPLAY']).AsString);
        Next;
      end;
    finally
  end else lst.Items.Assign(szlst);
  szlst.Free;
  Height:= Min(100, Lst.Count * Lst.ItemHeight) + 8;
end;

procedure TCheckListField.SetValue(const Value: string);
var
  I, Idx: Integer;
begin
  Lst.CheckAll(cbUnchecked);
  with SplitStr(Value, FDelimiter) do
    for I := 0 to Count - 1 do begin
      Idx:= Lst.Items.IndexOf(Strings[I]);
      if Idx >= 0 then Lst.Checked[Idx]:= True;
    end;
end;

{ TBmpField }

constructor TBmpField.Create(AOwner: TComponent);
begin
  inherited;
  lb.Align:= alTop;
  Img:= TImage(SetIt(TImage.Create(Self), alClient));
  Img.OnDblClick:= EventOfDblClick;
  Img.Hint:= '双击这里进行编辑';
  Img.ShowHint:= true;
  Height:= 160;
end;

procedure TBmpField.EventOfDblClick(Sender: TObject);
begin
  with TFDlgImage.Create(nil) do try
    Bitmap:= Img.Picture.Bitmap;
    ShowModal;
    Img.Picture.Bitmap.Assign(Bitmap);
    Self.Height:= Min(450, Max(160, Img.Picture.Bitmap.Height));
  finally
    Free;
  end;
end;

function TBmpField.GetBitmap: TBitmap;
begin
  Result:= Img.Picture.Bitmap;
end;

function TBmpField.GetValue: string;
begin
  Result:= BitmapToString(Img.Picture.Bitmap);
end;

procedure TBmpField.SetBitmap(const Value: TBitmap);
begin
  Img.Picture.Bitmap.Assign(Value);
  Height:= Min(450, Max(160, Img.Picture.Bitmap.Height));
end;

procedure TBmpField.SetValue(const Value: string);
begin
  StringToBitmap(Value, Img.Picture.Bitmap);
  Height:= Min(450, Max(160, Img.Picture.Bitmap.Height));
end;

{ TTreeField }

constructor TTreeField.Create(AOwner: TComponent);
begin
  inherited;
  lb.Align:= alTop;
  FTree:= TTreeView(SetIt(TTreeView.Create(Self), alClient));
  Height:= 126;
end;

function TTreeField.GetChoiced: Int64;
begin
  if not Assigned(FTree.Selected) then
    Result:= Int64(FTree.Selected.Data)
  else Result:= 0;
end;

function TTreeField.GetValue: string;
begin
  if Assigned(FTree.Selected) then
    Result:= FTree.Selected.Text
  else Result:= '';
end;

procedure TTreeField.SetChoiced(const Value: Int64);
  function FindNode(ANode: TTreeNode): Boolean;
  begin
    Result:= False;
    while Assigned(ANode) do begin
      if Int64(ANode.Data) = Value then begin
        Result:= True;
        Exit;
      end;
      if FindNode(ANode.getFirstChild) then Exit;
      ANode:= ANode.getNextSibling;
    end;
  end;
begin
  if FTree.Items.Count = 0 then Exit;
  FindNode(FTree.Items[0]);
end;

procedure TTreeField.SetOption(const Value: string);
var
  szlst: TStringList;
  ss: TStringStream;
  procedure AddNodes(ANode: TTreeNode; PID: Integer);
  var
    newNode: TTreeNode;
  begin
    with TADOQuery.Create(nil) do try
      Connection:= FConn;
      SQL.Text:= szlst.Values['SQL'];
      Parameters[0].Value:= PID;
      Open;
      while not EOF do begin
        if Assigned(ANode) then
          newNode:= FTree.Items.AddChild(ANode, FieldByName(szlst.Values['DISPLAY']).AsString)
        else newNode:= FTree.Items.Add(ANode, FieldByName(szlst.Values['DISPLAY']).AsString);
        if szlst.Values['IMAGE'] <> '' then
          newNode.ImageIndex:= FieldByName(szlst.Values['IMAGE']).AsInteger;
        if szlst.Values['SELECTED'] <> '' then
          newNode.SelectedIndex:= FieldByName(szlst.Values['SELECTED']).AsInteger
        else newNode.SelectedIndex:= newNode.ImageIndex;
        if szlst.Values['ID'] <> '' then
          newNode.Data:= Pointer(FieldByName(szlst.Values['ID']).AsInteger)
        else
          newNode.Data:= Pointer(FieldByName('ID').AsInteger);
        AddNodes(newNode, Integer(newNode.Data));
        Next;
      end;
    finally
      Free;
    end;
  end;
begin
  inherited;
  szlst:= TStringList.Create;
  szlst.Text:= Value;
  if(szlst.Count > 0)and(szlst.IndexOfName('SQL') >= 0) then AddNodes(nil, 0)
  else begin
    ss:= TStringStream.Create(Value);
    ss.Position:= 0;
    FTree.LoadFromStream(ss);
    ss.Free;
  end;
  szlst.Free;
end;

procedure TTreeField.SetValue(const Value: string);
  function FindNode(ANode: TTreeNode): Boolean;
  begin
    Result:= False;
    while Assigned(ANode) do begin
      if ANode.Text = Value then begin
        ANode.Selected:= True;
        Result:= True;
        Exit;
      end;
      if FindNode(ANode.getFirstChild) then Exit;
      ANode:= ANode.getNextSibling;
    end;
  end;
begin
  if FTree.Items.Count = 0 then Exit;
  FindNode(FTree.Items[0]);
end;

{ TGroupField }

constructor TGroupField.Create(AOwner: TComponent);
begin
  inherited;
  lb.Align:= alTop;
  lst:= TMemo(SetIt(TMemo.Create(Self), alClient));
  lp:= TPanel(SetIt(TMemo.Create(Self), alBottom));
  lp.Height:= 23;
  lp.BevelOuter:= bvNone;
  lp.ShowCaption:= False;
  Height:= 126;
  Btn:= TSpeedButton(SetIt(TSpeedButton.Create(Self), alRight));
  Btn.Caption:= 'Add';
  Btn.OnClick:= EventOfClick;
  Btn.Parent:= lp;
  Btn.Align:= alRight;
end;

procedure TGroupField.EventOfClick(Sender: TObject);
var
  I: Integer;
  S: string;
begin
  S:= FFmt;
  if S = '' then
    for I := 1 to lp.ControlCount - 1 do
      S:= S + TComboBox(lp.Controls[I]).Text
  else
    for I := 1 to lp.ControlCount - 1 do
      S:= StringReplace(S, '%' + IntToStr(I), TComboBox(lp.Controls[I]).Text, []);
  lst.Lines.Add(S);
end;

function TGroupField.GetLins: TStrings;
begin
  Result:= lst.Lines;
end;

function TGroupField.GetValue: string;
begin
  Result:= lst.Lines.Text;
end;

procedure TGroupField.SetOption(const Value: string);
var
  szList: TStringList;
  idx, cmpIdx: Integer;
  szCmp: TCombobox;
begin
  inherited;
  szList:= TStringList.Create;
  szList.Text:= Value;
  idx:= szList.IndexOfName('FMT');
  if idx >= 0 then
    FFmt:= szList.ValueFromIndex[idx];
  idx:= 1;
  while szList.IndexOfName('FLD' + IntToStr(Idx)) >= 0 do begin
    szCmp:= TCombobox.Create(self);
    szCmp.Parent:= lp;
    szCmp.Align:= alLeft;
    szCmp.Width:= 2;
    szCmp.Left:= lp.Width;
    szCmp.Items.Delimiter:= ',';
    szCmp.Items.DelimitedText:= szList.Values['FLD' + IntToStr(Idx)];
    Inc(Idx);
  end;
  for idx := 1 to lp.ControlCount - 1 do
    TCombobox(lp.Controls[Idx]).Width:= (lp.Width - btn.Width - lp.ControlCount) div(lp.ControlCount - 1);
end;

procedure TGroupField.SetValue(const Value: string);
begin
  lst.Lines.Text:= Value;
end;

end.
