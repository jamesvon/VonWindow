unit UVonVideo;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.Imaging.pngimage, Vcl.ExtCtrls, UTunnelInfo, UTunnelDB, UVonSystemFuns;

const DLL_FILENAME = 'VONVIDEO.DLL';

type
  TVonCameraWin = class(TPanel)
  private
    FSupportPZT: boolean;
    FMainInterval: Integer;
    FLowRtsp: AnsiString;
    FFrameInterval: Integer;
    FHighRtsp: AnsiString;
    FContinuous: Boolean;
    FRecorded: Boolean;
    FIsHigh: Boolean;
    FWinIdx: Integer;
    FMachineIdx: TGuid;
    FFixed: Boolean;
    procedure SetContinuous(const Value: Boolean);
    procedure SetIsHigh(const Value: Boolean);
    procedure SetRecorded(const Value: Boolean);
  public
    constructor Create(AIdx: Integer; AParent: TWinControl);
    destructor Destroy; override;
    ///<summary>设置设备信息</summary>
    procedure SetMachine(AInfo: TMachineInfo);
//    ///<summary>录像</summary>
//    procedure RecordIt;
//    ///<summary>停止录像</summary>
//    procedure StopRecord;
//    ///<summary>连拍</summary>
//    procedure ContinuousIt;
//    ///<summary>停止连拍</summary>
//    procedure StopContinuous;
    ///<summary>抓拍</summary>
    procedure Capture;
    procedure PZTUp(Speed: Integer);
    procedure PZTDown(Speed: Integer);
    procedure PZTLeft(Speed: Integer);
    procedure PZTRight(Speed: Integer);
  published
    ///<summary>设备序号</summary>
    property MachineIdx: TGuid read FMachineIdx write FMachineIdx;
    ///<summary>位置序号</summary>
    property WinIdx: Integer read FWinIdx write FWinIdx;
    ///<summary>高清连接</summary>
    property HighRtsp: AnsiString read FHighRtsp write FHighRtsp;
    ///<summary>标清连接</summary>
    property LowRtsp: AnsiString read FLowRtsp write FLowRtsp;
    ///<summary>支持PZT</summary>
    property SupportPZT: boolean read FSupportPZT write FSupportPZT;
    ///<summary>帧间隔</summary>
    property FrameInterval: Integer read FFrameInterval write FFrameInterval;
    ///<summary>主帧间隔</summary>
    property MainInterval: Integer read FMainInterval write FMainInterval;
    ///<summary>高清状态</summary>
    property IsHigh: Boolean read FIsHigh write SetIsHigh;
    ///<summary>连续拍照</summary>
    property Continuous: Boolean read FContinuous write SetContinuous;
    ///<summary>录像状态</summary>
    property Recorded: Boolean read FRecorded write SetRecorded;
    ///<summary>位置内容固定</summary>
    property Fixed: Boolean read FFixed write FFixed;
  end;

implementation

//  ///<summary>初始化</summary>
//_declspec(dllexport)bool Init(int mediaCount, char* logFile) { if (SYS_SCREEN != NULL) delete(SYS_SCREEN); SYS_SCREEN = new von::SDLScreen(mediaCount); return true; }
  function Init(mediaCount: Integer; logFile: PAnsiChar): boolean; cdecl; external DLL_FILENAME;
/////<summary>结束使用，释放空间</summary>
//_declspec(dllexport)void Release() { if (SYS_SCREEN != NULL) delete(SYS_SCREEN); }
  procedure Release(); cdecl; external DLL_FILENAME;
/////<summary>得到进程状态</summary>
//_declspec(dllexport)int VideoGtatus(int mediaIdx) {
//    if (SYS_SCREEN == NULL) return 0;
//    return (int)SYS_SCREEN->getGtatus(mediaIdx);
//}
  function VideoGtatus(mediaIdx: Integer): Integer; cdecl; external DLL_FILENAME;
/////<summary>打开视频</summary>
//_declspec(dllexport)void OpenVideo(int mediaIdx, void* wndHandel, int frameInterval, int width, int height, char* url) { SYS_SCREEN->Display(mediaIdx, wndHandel, frameInterval, width, height, url); }
  procedure OpenVideo(mediaIdx: Integer; wndHandel: HWND; frameInterval: Integer; width: Integer; height: Integer; url: PAnsichar); cdecl; external DLL_FILENAME;
/////<summary>关闭进程</summary>
//_declspec(dllexport)void CloseVideo(int mediaIdx) { SYS_SCREEN->Stop(mediaIdx); }
  procedure CloseVideo(mediaIdx: Integer); cdecl; external DLL_FILENAME;
///// <summary>设置媒体宽度和高度</summary>
//_declspec(dllexport)void ResizeVideo(int mediaIdx, int width, int height) { if (SYS_SCREEN != NULL) SYS_SCREEN->ResizeVideo(mediaIdx, width, height); }
  procedure ResizeVideo(mediaIdx: Integer; width: Integer; height: Integer); cdecl; external DLL_FILENAME;
/////<summary>摄像头云台控制</summary>
//_declspec(dllexport)void PTZVideo(int mediaIdx, int cmd, int data, int param1, int param2) { if (SYS_SCREEN != NULL) SYS_SCREEN->PTZVideo(mediaIdx, cmd, data, param1, param2); }
/////设置视频存放路径录制视频
//_declspec(dllexport)void StartSaveVideo(int mediaIdx, char* path) { if (SYS_SCREEN != NULL) SYS_SCREEN->StartSaveVideo(mediaIdx, path); }
  procedure StartSaveVideo(mediaIdx: Integer; path: PAnsichar); cdecl; external DLL_FILENAME;
/////停止录制视频
//_declspec(dllexport)void StopSaveVideo(int mediaIdx) { if (SYS_SCREEN != NULL) SYS_SCREEN->StopSaveVideo(mediaIdx); }
  procedure StopSaveVideo(mediaIdx: Integer); cdecl; external DLL_FILENAME;
//    ///开始抓拍主帧
//    _declspec(dllexport)void StartSavePicture(int mediaIdx, int interval, char* path) { if (SYS_SCREEN != NULL) SYS_SCREEN->StartSavePicture(mediaIdx, path); }
  procedure StartSavePicture(mediaIdx: Integer; interval: Integer; path: PAnsichar); cdecl; external DLL_FILENAME;
//    ///停止抓拍主帧
//    _declspec(dllexport)void StopSavePicture(int mediaIdx) { if (SYS_SCREEN != NULL) SYS_SCREEN->StopSavePicture(mediaIdx); }
  procedure StopSavePicture(mediaIdx: Integer); cdecl; external DLL_FILENAME;
//    ///临时抓拍
//    _declspec(dllexport)void Capture(int mediaIdx, char* path) { if (SYS_SCREEN != NULL) SYS_SCREEN->Capture(mediaIdx, path);
  procedure Capture(mediaIdx: Integer; filename: PAnsichar); cdecl; external DLL_FILENAME;
//    //丢帧率 丢失帧数/合计帧数
//    _declspec(dllexport)double LostRate(int mediaIdx) { if (SYS_SCREEN != NULL) SYS_SCREEN->LostRate(mediaIdx); }
  function LostRate(mediaIdx: Integer): double; cdecl; external DLL_FILENAME;
//    //断线率 断线时间合计/总播放时间
//    _declspec(dllexport)double ReconnectRate(int mediaIdx) { if (SYS_SCREEN != NULL) SYS_SCREEN->ReconnectRate(mediaIdx); }
  function ReconnectRate(mediaIdx: Integer): double; cdecl; external DLL_FILENAME;

var
  VonVideoInited: Boolean;


{ TCameraWin }

constructor TVonCameraWin.Create(AIdx: Integer; AParent: TWinControl);
begin
  inherited Create(nil);
  FWinIdx:= AIdx;
  Parent:= AParent;
end;

destructor TVonCameraWin.Destroy;
begin
  UVonVideo.CloseVideo(FWinIdx);
  inherited;
end;

procedure TVonCameraWin.SetMachine(AInfo: TMachineInfo);
begin
  if AInfo.ID = FMachineIdx then Exit;
  if FContinuous then UVonVideo.StopSavePicture(FWinIdx);
  if FRecorded then UVonVideo.StopSaveVideo(FWinIdx);
  UVonVideo.CloseVideo(FWinIdx);
  FMachineIdx:= AInfo.ID;
  FHighRtsp:= AInfo.Settings['高清连接'];
  FLowRtsp:= AInfo.Settings['标清连接'];
  FSupportPZT:= AInfo.Settings['支持云台'] = '1';
  FMainInterval:= StrToInt(AInfo.Settings['主帧间隔']);
  FFrameInterval:= StrToInt(AInfo.Settings['辅帧间隔']);
  FContinuous:= False;
  FRecorded:= False;
  FIsHigh:= False;
  UVonVideo.OpenVideo(FWinIdx, Self.Handle, FFrameInterval, Self.Width, Self.Height, PAnsiChar(FLowRtsp));
  //UVonVideo.CloseVideo(FWinIdx);
end;

procedure TVonCameraWin.Capture;
var
  S: AnsiString;
begin     //抓拍
  S:= FTunnelDB.AppPath + 'TEMP/' + FormatDateTime('yyyymmdd_hhnnss', Now) + '.JPG';
  UVonVideo.Capture(FWinIdx, PAnsiChar(S));
end;

procedure TVonCameraWin.SetContinuous(const Value: Boolean);
var
  S: AnsiString;
begin     //连拍
  if FContinuous = Value then Exit;
  FContinuous := Value;
  if FContinuous then begin
    S:= FTunnelDB.AppPath + 'TEMP/' + GuidToStr(FMachineIdx);
    if not DirectoryExists(S) then CreateDirectory(S);
    UVonVideo.StartSavePicture(FWinIdx, MainInterval, PAnsiChar(S));
  end else UVonVideo.StopSavePicture(FWinIdx);
end;

procedure TVonCameraWin.SetIsHigh(const Value: Boolean);
begin     //高清标清切换
  if FIsHigh = Value then Exit;
  FIsHigh := Value;
  UVonVideo.CloseVideo(FWinIdx);
  if FIsHigh then
    UVonVideo.OpenVideo(FWinIdx, Self.Handle, FFrameInterval, Self.Width, Self.Height, PAnsiChar(FHighRtsp))
  else
    UVonVideo.OpenVideo(FWinIdx, Self.Handle, FFrameInterval, Self.Width, Self.Height, PAnsiChar(FLowRtsp));
end;

procedure TVonCameraWin.SetRecorded(const Value: Boolean);
var
  S: AnsiString;
begin     //录像
  if FRecorded = Value then Exit;
  FRecorded := Value;
  if FRecorded then begin
    S:= FTunnelDB.AppPath + 'TEMP/' + GuidToStr(FMachineIdx);
    if not DirectoryExists(S) then CreateDirectory(S);
    UVonVideo.StartSaveVideo(FWinIdx, PAnsiChar(S));
  end else UVonVideo.StopSaveVideo(FWinIdx);
end;

procedure TVonCameraWin.PZTDown(Speed: Integer);
begin
  if not FSupportPZT then Exit;

end;

procedure TVonCameraWin.PZTLeft(Speed: Integer);
begin
  if not FSupportPZT then Exit;

end;

procedure TVonCameraWin.PZTRight(Speed: Integer);
begin
  if not FSupportPZT then Exit;

end;

procedure TVonCameraWin.PZTUp(Speed: Integer);
begin
  if not FSupportPZT then Exit;

end;

initialization
  //RegCameraWin('HKCameraWin', THKCameraWin);;

finalization
  //if VonVideoInited then
//  Release();


end.

