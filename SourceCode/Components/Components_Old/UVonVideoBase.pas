unit UVonVideoBase;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.Imaging.pngimage, Vcl.ExtCtrls, UTunnelInfo, UTunnelDB, UVonSystemFuns;

{$DEFINE HK}
Const
  {$IFDEF HK}
  CameraWinName = 'HKCameraWin';
  {$ELSE}
  CameraWinName = 'VonCameraWin';
  {$ENDIF}

type
  ///<summary>云台动作方向</summary>
  PAN_DIRECTION = (PAN_DIR_UP = 1, PAN_DIR_DOWN, PAN_DIR_LEFT, PAN_DIR_RIGHT, PAN_DIR_UP_LEFT, PAN_DIR_UP_RIGHT, PAN_DIR_DOWN_LEFT, PAN_DIR_DOWN_RIGHT);

  TCameraWinBaseClass = class of TCameraWinBase;
  TCameraWinBase = class(TPanel)
  private
    FMachineInfo: TObject;
    FWinIdx: Integer;
    FSupportPZT: boolean;
    FLowStream: AnsiString;
    FMachineName: string;
    FHighStream: AnsiString;
    FFixed: Boolean;
    FMachineSetting: TStringList;
    procedure SetIsHigh(const Value: Boolean);
    procedure SetMachineName(const Value: string);
    procedure SetMachineSetting(const Value: TStringList);
    function GetSupportPZT: boolean;
    function GetFrameInterval: Integer;
    function GetMainInterval: Integer;
  protected
    FIsHigh: Boolean;
    FShooting: Boolean;
    FRecorded: Boolean;
  public
    constructor Create(AIdx: Integer; AParent: TWinControl);
    destructor Destroy; override;
    class procedure InitWindows(WindowsCount: Integer; LogFilePath: string); virtual; abstract;
    class procedure ReleaseWindows; virtual; abstract;
    procedure StopDisplay; virtual; abstract;
    procedure DisplayHigh; virtual; abstract;
    procedure DisplayLower; virtual; abstract;
    procedure RecordVideo(Path: string); virtual; abstract;
    procedure StopRecord; virtual; abstract;
    procedure ShootingPicture(Interval: Integer; Path: string); virtual; abstract;
    procedure StopShooting; virtual; abstract;
    ///<summary>抓拍</summary>
    procedure Capture(Filename: string); virtual; abstract;
    ///<summary>云台动作</summary>
    procedure PZT(DIR: PAN_DIRECTION; Speed: Integer); virtual; abstract;
    ///<summary>拉近</summary>
    procedure ZoomIn(Speed: Integer); virtual; abstract;
    ///<summary>拉远</summary>
    procedure ZoomOut(Speed: Integer); virtual; abstract;
    ///<summary>聚焦-</summary>
    procedure FocusNear(Speed: Integer); virtual; abstract;
    ///<summary>聚焦+</summary>
    procedure FocusFar(Speed: Integer); virtual; abstract;
    ///<summary>光圈+</summary>
    procedure IrisOpen(Speed: Integer); virtual; abstract;
    ///<summary>光圈-</summary>
    procedure IrisClose(Speed: Integer); virtual; abstract;
  published
    ///<summary>显示序号</summary>
    property WinIdx: Integer read FWinIdx;
    /// <summary>序号</summary>
    property MachineInfo: TObject read FMachineInfo write FMachineInfo;
    /// <summary>名称</summary>
    property MachineName: string read FMachineName write SetMachineName;
    /// <summary>设备参数</summary>
    property MachineSetting: TStringList read FMachineSetting write SetMachineSetting;
    ///<summary>高清连接</summary>
    property HighStream: AnsiString read FHighStream write FHighStream;
    ///<summary>标清连接</summary>
    property LowStream: AnsiString read FLowStream write FLowStream;
    ///<summary>支持PZT</summary>
    property SupportPZT: boolean read GetSupportPZT;
    ///<summary>帧间隔</summary>
    property FrameInterval: Integer read GetFrameInterval;
    ///<summary>主帧间隔</summary>
    property MainInterval: Integer read GetMainInterval;

    ///<summary>高清状态</summary>
    property IsHigh: Boolean read FIsHigh write SetIsHigh;
    ///<summary>连续拍照</summary>
    property Shooting: Boolean read FShooting;
    ///<summary>录像状态</summary>
    property Recorded: Boolean read FRecorded;
    ///<summary>内容是否固定，不参与轮训</summary>
    property Fixed: Boolean read FFixed write FFixed;
  end;

  /// <summary>注册一个视频显示窗口类</summary>
  procedure RegCameraWin(Name: string; ObjClass: TCameraWinBaseClass);
  /// <summary>获取视频窗口类</summary>
  function GetCameraWinClass(Name: string): TCameraWinBaseClass;
  /// <summary>创建一个视频显示窗口类实例</summary>
  function CreateCameraWin(Name: string; AIdx: Integer; AParent: TWinControl): TCameraWinBase;

implementation

type
  TCameraWinClass = class
  private
    FObjClass: TCameraWinBaseClass;
  published
    property ObjClass: TCameraWinBaseClass read FObjClass write FObjClass;
  end;
var
  FCameraWinList: TStringList;

  procedure RegCameraWin(Name: string; ObjClass: TCameraWinBaseClass);
  var
    szObj: TCameraWinClass;
  begin
    if FCameraWinList.IndexOf(Name) >= 0 then raise Exception.Create('Dbl camera win registed.');
    szObj:= TCameraWinClass.Create;
    szObj.ObjClass:= ObjClass;
    FCameraWinList.AddObject(Name, szObj);
  end;

  function GetCameraWinClass(Name: string): TCameraWinBaseClass;
  var
    Idx: Integer;
  begin
    Idx:= FCameraWinList.IndexOf(Name);
    if Idx < 0 then raise Exception.Create('The camera win not found.');
    Result:= TCameraWinClass(FCameraWinList[Idx]).ObjClass;
  end;

  function CreateCameraWin(Name: string; AIdx: Integer; AParent: TWinControl): TCameraWinBase;
  var
    Idx: Integer;
  begin
    Idx:= FCameraWinList.IndexOf(Name);
    if Idx < 0 then raise Exception.Create('The camera win not found.');
    Result:= TCameraWinClass(FCameraWinList.Objects[Idx]).ObjClass.Create(AIdx, AParent);// FCameraWinList.Objects[Idx].Create;
  end;

{ TCameraWinBase }

constructor TCameraWinBase.Create(AIdx: Integer; AParent: TWinControl);
begin
  inherited Create(nil);
  FMachineSetting:= TStringList.Create;
  FWinIdx:= AIdx;
  Parent:= AParent;
  BevelInner:= bvNone;
  BevelOuter:= bvNone;
  BevelKind:= bkNone;
  ShowCaption:= False;
  ShowHint:= True;
end;

destructor TCameraWinBase.Destroy;
begin
  StopDisplay;
  //if Assigned(FMachineInfo) then FMachineInfo.Free;
  FMachineSetting.Free;
  inherited;
end;

function TCameraWinBase.GetFrameInterval: Integer;
begin
  Result:= StrToInt(FMachineSetting.Values['辅帧间隔']);
end;

function TCameraWinBase.GetMainInterval: Integer;
begin
  Result:= StrToInt(FMachineSetting.Values['主帧间隔']);
end;

function TCameraWinBase.GetSupportPZT: boolean;
begin
  Result:= FMachineSetting.Values['支持云台'] = '1';
end;

procedure TCameraWinBase.SetIsHigh(const Value: Boolean);
begin
  if FIsHigh = Value then Exit;
  FIsHigh := Value;
  if Value then DisplayHigh else DisplayLower;
end;

procedure TCameraWinBase.SetMachineName(const Value: string);
begin
  FMachineName := Value;
  Hint:= Value;
end;

procedure TCameraWinBase.SetMachineSetting(const Value: TStringList);
begin
  FMachineSetting.Assign(Value);
end;

initialization
  FCameraWinList:= TStringList.Create;
finalization
  FCameraWinList.Free;
end.
