unit UDlgBrowser;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ImgList, ShellApi,
  System.ImageList, UVonSystemFuns, UVonLog, Vcl.StdCtrls, SHDocVw, Vcl.OleCtrls;

type
  TFDlgBrower = class(TForm)
    WebBrowser1: TWebBrowser;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ImageList1: TImageList;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
  private
    { Private declarations }
    FFilename: string;
  public
    { Public declarations }
    procedure OpenHtmlFile(Filename: string);
    procedure OpenUrl(Url: string);
    procedure Print;
  end;

  procedure PrintHtml(Filename: string);
  procedure OpenHtml(Filename: string);

var
  FDlgBrower: TFDlgBrower;

implementation

procedure PrintHtml(Filename: string);
begin
  with TFDlgBrower.Create(nil) do begin
    Show;
    OpenHtmlFile(Filename);
    while webbrowser1.busy do
      Application.ProcessMessages;
    while webbrowser1.ReadyState <> READYSTATE_COMPLETE do
      Application.ProcessMessages;
    //Print;
  end;
end;

procedure OpenHtml(Filename: string);
begin
  with TFDlgBrower.Create(nil) do begin
    OpenHtmlFile(Filename);
    Show;
  end;
end;

{$R *.dfm}

{ TFDlgBrower }

procedure TFDlgBrower.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:= caFree;
end;

procedure TFDlgBrower.OpenHtmlFile(Filename: string);
begin
  FFilename:= Filename;
  WriteLog(LOG_DEBUG, 'TFDlgBrower.OpenHtmlFile', 'file://' + LongToShortFilePath(Filename));
  WriteLog(LOG_DEBUG, 'TFDlgBrower.OpenHtmlFile', 'file://' + Filename);
  WebBrowser1.Navigate(LongToShortFilePath(Filename)); //'file://' +
end;

procedure TFDlgBrower.OpenUrl(Url: string);
begin
  WebBrowser1.Navigate(Url);
end;

procedure TFDlgBrower.Print;
var
  vaIn: OleVariant;
  vaOut: OleVariant;
begin
  (WebBrowser1.Application as IWebBrowser2).ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_PROMPTUSER, vaIn, vaOut);
end;

procedure TFDlgBrower.ToolButton1Click(Sender: TObject);
begin
  Print;
end;

procedure TFDlgBrower.ToolButton2Click(Sender: TObject);
var
  vaIn: OleVariant;
  vaOut: OleVariant;
begin
  (WebBrowser1.Application as IWebBrowser2).ExecWB(OLECMDID_PRINTPREVIEW, OLECMDEXECOPT_PROMPTUSER, vaIn, vaOut);
end;

procedure TFDlgBrower.ToolButton3Click(Sender: TObject);
var
  vaIn: OleVariant;
  vaOut: OleVariant;
begin
  (WebBrowser1.Application as IWebBrowser2).ExecWB(OLECMDID_PAGESETUP, OLECMDEXECOPT_PROMPTUSER, vaIn, vaOut);
end;

procedure TFDlgBrower.ToolButton4Click(Sender: TObject);
var
  S: string;
begin
  S:= ReadAppConfig('SYSTEM', 'WebBrowser');
  ShellExecute(0, 'Open', PChar(S), PChar(FFilename), '', 0);
end;

procedure TFDlgBrower.ToolButton5Click(Sender: TObject);
begin
  WebBrowser1.Navigate('www.baidu.com');
  //WebBrowser1.
end;

//Initialization
//  OleInitialize(nil);
//finalization
//  try
//    OleUninitialize;
//  except
//  end;

end.
