unit UDlgCamera;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, IdBaseComponent, IdComponent, IdUDPBase, UVonLog,
  IdUDPClient, ImgList, ComCtrls, ToolWin, UVFW, Spin, Menus, IniFiles, JPEG,
  System.ImageList;

type
  EDisplayKind = (DK_TEXT, DK_VIEW, DK_BOTTOM, DK_TOP, DK_LEFT, DK_RIGHT);

  TFDlgCamera = class(TForm)
    ToolBar1: TToolBar;
    ImageList1: TImageList;
    tbPause: TToolButton;
    tbOpen: TToolButton;
    tbStop: TToolButton;
    ToolButton1: TToolButton;
    tbCamera: TToolButton;
    SaveDialog1: TSaveDialog;
    Image1: TImage;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    EZoom: TSpinEdit;
    btnDisplayKind: TToolButton;
    pmDisplayKind: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    btnFont: TToolButton;
    ToolButton3: TToolButton;
    btnColor: TToolButton;
    ToolButton5: TToolButton;
    btnUpload: TToolButton;
    ToolButton7: TToolButton;
    btnSetting: TToolButton;
    FontDialog1: TFontDialog;
    ColorDialog1: TColorDialog;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tbPauseClick(Sender: TObject);
    procedure tbOpenClick(Sender: TObject);
    procedure tbStopClick(Sender: TObject);
    procedure tbCameraClick(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure btnFontClick(Sender: TObject);
    procedure btnColorClick(Sender: TObject);
    procedure EZoomChange(Sender: TObject);
  private
    { Private declarations }
    BmpInfo: TBitmapInfo;
    CaptureHandle: THandle;
    DisplayKind: EDisplayKind;
    FVideoText: string;
    FIniFilename: string;
    FTextFontName: string;
    FTextFontSize: Integer;
    FTextFontColor: TColor;
    FTextFontStyle: TFontStyles;
    FTextBackgroundColor: TColor;
    procedure CompareFrame(lpVHdr: PVIDEOHDR);
    procedure DisplayText(bmp: TBitmap; text: string);
    procedure SetIniFilename(const Value: string);
  public
    { Public declarations }
    procedure VideoText(Text: string);
  published
    property IniFilename: string read FIniFilename write SetIniFilename;
  end;

type
  tmpPanel = class(TPanel)
  public
    property Canvas;
  end;

function DlgCamera(Bmp: TBitmap): Boolean;
procedure DlgVideoOpen();
procedure DlgCameraOpen();
procedure DlgTextOpen();

var
  FDlgCamera: TFDlgCamera;

implementation

uses StrUtils;

var
  idx: Integer;

  function FrameCallBack(hWnd: hWnd; lpVHdr: PVIDEOHDR): DWORD; stdcall;
  begin
    FDlgCamera.CompareFrame(lpVHdr);
    Result := LongInt(True);
  end;

  function DlgCamera(Bmp: TBitmap): Boolean;
  var
    tmpFilename: string;
  begin
    tmpFilename := ExtractFilePath(Application.ExeName) + 'tmp.bmp';
    with TFDlgCamera.Create(nil) do
      try
        CaptureHandle := capCreateCaptureWindowA('My Own Capture Window',
          WS_CHILD or WS_VISIBLE, Image1.Left, Image1.Top, Image1.Width,
          Image1.Height, Handle, 0);
        if CaptureHandle <> 0 then
        begin
          SendMessage(CaptureHandle, WM_CAP_SET_CALLBACK_VIDEOSTREAM, 0, 0);
          SendMessage(CaptureHandle, WM_CAP_SET_CALLBACK_ERROR, 0, 0);
          SendMessage(CaptureHandle, WM_CAP_SET_CALLBACK_STATUSA, 0, 0);
          SendMessage(CaptureHandle, WM_CAP_DRIVER_CONNECT, 0, 0);
          SendMessage(CaptureHandle, WM_CAP_SET_SCALE, 1, 0);
          SendMessage(CaptureHandle, WM_CAP_SET_PREVIEWRATE, 66, 0);
          SendMessage(CaptureHandle, WM_CAP_SET_OVERLAY, 1, 0);
          SendMessage(CaptureHandle, WM_CAP_SET_PREVIEW, 1, 0);
        end;
        tbOpen.Enabled := false;
        tbStop.Enabled := true;
        Result := ShowModal = mrOK;
        SendMessage(CaptureHandle, WM_CAP_FILE_SAVEDIBW, 0,        // WM_CAP_FILE_SAVEDIBA
          LongInt(PCHAR(tmpFilename)));
        if CaptureHandle <> 0 then
        begin
          SendMessage(CaptureHandle, WM_CAP_DRIVER_DISCONNECT, 0, 0);
          CaptureHandle := 0;
        end;
        Bmp.LoadFromFile(tmpFilename);
      finally
        Free;
      end;
  end;

  procedure DlgCameraOpen();
  begin
    if not Assigned(FDlgCamera) then FDlgCamera := TFDlgCamera.Create(nil);
    FDlgCamera.Image1.Visible:= True;
    FDlgCamera.DisplayKind:= DK_VIEW;
    FDlgCamera.Show;
  end;

  procedure DlgTextOpen();
  begin
    if not Assigned(FDlgCamera) then FDlgCamera := TFDlgCamera.Create(nil);
    FDlgCamera.Image1.Visible:= True;
    FDlgCamera.btnDisplayKind.ImageIndex:= 4;
    FDlgCamera.DisplayKind:= DK_TEXT;
    FDlgCamera.Show;
    FDlgCamera.N6Click(FDlgCamera.N1);
  end;

  procedure DlgVideoOpen();
  begin
    DlgCameraOpen();
    FDlgCamera.tbOpenClick(nil);
  end;

{$R *.dfm}

{ TFDlgCamera }

procedure TFDlgCamera.btnColorClick(Sender: TObject);
begin
  with ColorDialog1 do begin
    Color:= FTextBackgroundColor;
    if Execute then
      if DisplayKind = DK_TEXT then begin
        FTextBackgroundColor:= Color;
        with TIniFile.Create(FIniFilename)do try
          WriteInteger('Camera', 'TextBackgroundColor', FTextBackgroundColor);
        finally
          Free;
        end;
      end;
  end;
end;

procedure TFDlgCamera.btnFontClick(Sender: TObject);
begin
  with FontDialog1 do begin
    Font.Name:= FTextFontName;
    Font.Size:= FTextFontSize;
    Font.Color:= FTextFontColor;
    Font.Style:= FTextFontStyle;
    if Execute then begin
      FTextFontName:= Font.Name;
      FTextFontSize:= Font.Size;
      FTextFontColor:= Font.Color;
      FTextFontStyle:= Font.Style;
      with TIniFile.Create(FIniFilename)do try
        WriteString('Camera', 'TextFontName', FTextFontName);
        WriteInteger('Camera', 'TextFontSize', FTextFontSize);
        WriteInteger('Camera', 'TextFontColor', FTextFontColor);
        WriteBool('Camera', 'TextFontBold', fsBold in FTextFontStyle);
        WriteBool('Camera', 'TextFontItalic', fsItalic in FTextFontStyle);
        WriteBool('Camera', 'TextFontUnderline', fsUnderline in FTextFontStyle);
        WriteBool('Camera', 'TextFontStrikeOut', fsStrikeOut in FTextFontStyle);
      finally
        Free;
      end;
    end;
  end;
end;

procedure TFDlgCamera.CompareFrame(lpVHdr: PVIDEOHDR);
var
  hd:Thandle;
  jpg:TJpegImage;
  memStream :TMemoryStream;
  Bitmap:TBitmap;
begin //将数据显在Image，
  Bitmap:=TBitmap.Create;
  Bitmap.Width :=BMPINFO.bmiHeader.biWidth;
  // New size of Bitmap
  Bitmap.Height:=BMPINFO.bmiHeader.biHeight;
  hd:= DrawDibOpen;
  DrawDibDraw(hd,Bitmap.canvas.handle,0,0,BMPINFO.BmiHeader.biwidth,BMPINFO.bmiheader.biheight,@BMPINFO.bmiHeader,lpVHdr^.lpData,0,0,BMPINFO.bmiHeader.biWidth, BMPINFO.bmiHeader.biheight,0);
  DrawDibClose(hd); //发送数据
  memStream := TMemoryStream.Create;
  jpg := TJpegImage.Create;
  jpg.Assign(Bitmap);
  jpg.CompressionQuality := 10; //jpg压缩质量
  jpg.JPEGNeeded;
  jpg.Compress;
  jpg.SaveToStream(memStream);
  jpg.Free; //因为UDP数据包有大小限制，这里如果超出部分，就没有传输，完全可以发几次发出去
//  Form1.IdUDPClient1.BroadcastEnabled:=true;//用广播功能
//  if memStream.Size>Form1.IdUDPClient1.BufferSize then
//  //向192.168.0.X网段广播，端口 9001
//  Form1.IdUDPClient1.SendBuffer('192.168.0.255',9001,memStream.Memory^,Form1.IdUDPClient1.BufferSize) else Form1.IdUDPClient1.SendBuffer('192.168.0.255',9001,memStream.Memory^,memStream.Size);
  memStream.Free;
  Bitmap.Free;
End;
//var
//  hd: THandle;
//  MyBmp: TBitmap;
//begin
//  MyBmp := TBitmap.Create;
//  MyBmp.Width := BmpInfo.bmiHeader.biWidth;
//  MyBmp.Height := BmpInfo.bmiHeader.biHeight;
//  MyBmp.Canvas.FillRect(rect(0, 0, MyBmp.Width, MyBmp.Height));
//  hd := DrawDibOpen;
//  DrawDibDraw(hd, MyBmp.Canvas.Handle, 0, 0, BmpInfo.bmiHeader.biWidth,
//    BmpInfo.bmiHeader.biHeight, @BmpInfo.bmiHeader, lpVHdr^.lpData, 0, 0,
//    BmpInfo.bmiHeader.biWidth, BmpInfo.bmiHeader.biHeight, 0);
//  DrawDibClose(hd);
//  DisplayText(MyBmp, FVideoText);
//  Inc(Idx);
////  MyBmp.SaveToFile('C:\A' + IntToStr(Idx) + '.bmp');
////  Image1.Picture.Assign(MyBmp);
//  MyBmp.Free;
//end;

procedure TFDlgCamera.DisplayText(bmp: TBitmap; text: string);
var
  lst: TStringList;
  i, mPos, szPos: Integer;
  BF: TBlendFunction;
  txtBmp: TBitmap;

  procedure SetBmpFont(ACanvas: TCanvas);
  begin
    ACanvas.Font.Name:= FTextFontName;
    ACanvas.Font.Color:= FTextFontColor;
    ACanvas.Font.Size:= FTextFontSize;
    ACanvas.Font.Style:= FTextFontStyle;
  end;
begin
  txtBmp:= TBitmap.Create;
  BF.BlendOp := AC_SRC_OVER;      // 值为零,必须的且只能为零
  BF.BlendFlags := 0;
  BF.AlphaFormat := 0;
  BF.SourceConstantAlpha := 128;  // 透明度:0~255,255为不透明
  case DisplayKind of
  DK_TEXT  : begin
      Image1.Height:= 105;
      Image1.Width:= 600;
      bmp.Canvas.Brush.Color:= FTextBackgroundColor;
      bmp.Canvas.FillRect(Rect(0, 0, 600, 105));
      SetBmpFont(bmp.Canvas);
      bmp.Canvas.TextOut(0, 0, Copy(text, 1, 16));
      bmp.Canvas.TextOut(0, 50, Copy(text, 17, MaxInt));
    end;
  DK_VIEW  : begin
      Image1.Height:= BmpInfo.bmiHeader.biHeight * EZoom.Value div 100;
      Image1.Width:= BmpInfo.bmiHeader.biWidth * EZoom.Value div 100;
      if Image1.Height = BmpInfo.bmiHeader.biHeight then Image1.Picture.Bitmap.Assign(bmp)
      else Image1.Picture.Bitmap.Canvas.StretchDraw(Rect(0, 0, Image1.Width, Image1.Height), bmp);
    end;
  DK_BOTTOM: begin
      Image1.Height:= BmpInfo.bmiHeader.biHeight * EZoom.Value div 100;
      Image1.Width:= BmpInfo.bmiHeader.biWidth * EZoom.Value div 100;
      txtBmp.Width:= Image1.Width;
      SetBmpFont(txtBmp.Canvas);
      txtBmp.Height:= Round(txtBmp.Canvas.TextHeight('国') * 2.3);
      mPos:= Pos(' ', text); szPos:= mPos - 1;
      while txtBmp.Canvas.TextWidth(Copy(text, 1, mPos - 1)) < txtBmp.Width do begin
        if mPos = 0 then Break
        else if szPos = mPos then Break
        else szPos:= mPos;
        mPos:= PosEx(' ', text, mPos + 1);
      end;
      txtBmp.Canvas.Brush.Color:= FTextBackgroundColor;
      txtBmp.Canvas.FillRect(Rect(0, 0, txtBmp.Width, txtBmp.Height));
      txtBmp.Canvas.TextOut(0, 0, Copy(text, 1, szPos));
      txtBmp.Canvas.TextOut(0, txtBmp.Height div 2, Copy(text, szPos + 1, MaxInt));
      WinApi.Windows.AlphaBlend(bmp.Canvas.Handle, 0, bmp.Height - txtBmp.Height, bmp.Width,
        txtBmp.Height, txtBmp.Canvas.Handle, 0, 0, txtBmp.Width, txtBmp.Height, BF);
//      WinApi.Windows.AlphaBlend(bmp.Canvas.Handle, 10, 10, ImgLogo.Width,
//        ImgLogo.Height, ImgLogo.Canvas.Handle, 0, 0, ImgLogo.Width, ImgLogo.Height, BF);
      bmp.Canvas.Brush.Style:= bsClear;
      SetBmpFont(bmp.Canvas);
      bmp.Canvas.Font.Size:= 9;
      bmp.Canvas.TextOut(bmp.Width - 200, 10, DateTimeToStr(Now));
      if Image1.Height = BmpInfo.bmiHeader.biHeight then Image1.Picture.Bitmap.Assign(bmp)
      else Image1.Picture.Bitmap.Canvas.StretchDraw(Rect(0, 0, Image1.Width, Image1.Height), bmp);
    end;
  DK_TOP   : begin
      Image1.Height:= BmpInfo.bmiHeader.biHeight * EZoom.Value div 100;
      Image1.Width:= BmpInfo.bmiHeader.biWidth * EZoom.Value div 100;
      txtBmp.Width:= bmp.Width;
      SetBmpFont(txtBmp.Canvas);
      txtBmp.Height:= Round(txtBmp.Canvas.TextHeight('国') * 2.3);
      mPos:= Pos(' ', text); szPos:= mPos - 1;
      while txtBmp.Canvas.TextWidth(Copy(text, 1, mPos - 1)) < txtBmp.Width do begin
        if mPos = 0 then Break
        else if szPos = mPos then Break
        else szPos:= mPos;
        mPos:= PosEx(' ', text, mPos + 1);
      end;
      txtBmp.Canvas.Brush.Color:= FTextBackgroundColor;
      txtBmp.Canvas.FillRect(Rect(0, 0, txtBmp.Width, txtBmp.Height));
      txtBmp.Canvas.TextOut(0, 0, Copy(text, 1, szPos));
      txtBmp.Canvas.TextOut(0, txtBmp.Height div 2, Copy(text, szPos + 1, MaxInt));
      WinApi.Windows.AlphaBlend(bmp.Canvas.Handle, 0, 0, bmp.Width,
        txtBmp.Height, txtBmp.Canvas.Handle, 0, 0, txtBmp.Width, txtBmp.Height, BF);
//      WinApi.Windows.AlphaBlend(bmp.Canvas.Handle, 10, bmp.Height - ImgLogo.Height - 10, ImgLogo.Width,
//        ImgLogo.Height, ImgLogo.Canvas.Handle, 0, 0, ImgLogo.Width, ImgLogo.Height, BF);
      bmp.Canvas.Brush.Style:= bsClear;
      SetBmpFont(bmp.Canvas);
      bmp.Canvas.Font.Size:= 9;
      bmp.Canvas.TextOut(bmp.Width - 160, bmp.Height - 30, DateTimeToStr(Now));
      if Image1.Height = BmpInfo.bmiHeader.biHeight then Image1.Picture.Bitmap.Assign(bmp)
      else Image1.Picture.Bitmap.Canvas.StretchDraw(Rect(0, 0, Image1.Width, Image1.Height), bmp);
    end;
  DK_LEFT  : begin
      Image1.Height:= BmpInfo.bmiHeader.biHeight * EZoom.Value div 100;
      Image1.Width:= BmpInfo.bmiHeader.biWidth * EZoom.Value div 100;
      txtBmp.Width:= bmp.Width;
      txtBmp.Height:= bmp.Height;
      SetBmpFont(txtBmp.Canvas);
      txtBmp.Canvas.Brush.Color:= FTextBackgroundColor;
      txtBmp.Canvas.FillRect(Rect(0, 0, txtBmp.Width, txtBmp.Height));
      lst:= TStringList.Create;
      lst.Delimiter:= ' ';
      lst.DelimitedText:= text;
      txtBmp.Height:= Round(txtBmp.Canvas.TextHeight('国') * lst.Count * 1.3);
      szPos:= 0;
      for I := 0 to lst.Count - 1 do begin
        txtBmp.Canvas.TextOut(0, Round(i * txtBmp.Height / lst.Count), lst[i]);
        mPos:= txtBmp.Canvas.TextWidth(lst[i]);
        if mPos > szPos then szPos:= mPos;
      end;
      txtBmp.Width:= szPos;
      lst.Free;
//      WinApi.Windows.AlphaBlend(bmp.Canvas.Handle, 0, ImgLogo.Height + 10, txtBmp.Width,
//        txtBmp.Height, txtBmp.Canvas.Handle, 0, 0, txtBmp.Width, txtBmp.Height, BF);
//      WinApi.Windows.AlphaBlend(bmp.Canvas.Handle, 10, 10, ImgLogo.Width,
//        ImgLogo.Height, ImgLogo.Canvas.Handle, 0, 0, ImgLogo.Width, ImgLogo.Height, BF);
      bmp.Canvas.Brush.Style:= bsClear;
      SetBmpFont(bmp.Canvas);
      bmp.Canvas.Font.Size:= 9;
      bmp.Canvas.TextOut(bmp.Width - 150, 10, DateTimeToStr(Now));
      if Image1.Height = BmpInfo.bmiHeader.biHeight then Image1.Picture.Bitmap.Assign(bmp)
      else Image1.Picture.Bitmap.Canvas.StretchDraw(Rect(0, 0, Image1.Width, Image1.Height), bmp);
    end;
  DK_RIGHT : begin
      Image1.Height:= BmpInfo.bmiHeader.biHeight * EZoom.Value div 100;
      Image1.Width:= BmpInfo.bmiHeader.biWidth * EZoom.Value div 100;
      txtBmp.Width:= bmp.Width;
      txtBmp.Height:= bmp.Height;
      SetBmpFont(txtBmp.Canvas);
      txtBmp.Canvas.Brush.Color:= FTextBackgroundColor;
      txtBmp.Canvas.FillRect(Rect(0, 0, txtBmp.Width, txtBmp.Height));
      lst:= TStringList.Create;
      lst.Delimiter:= ' ';
      lst.DelimitedText:= text;
      txtBmp.Height:= Round(txtBmp.Canvas.TextHeight('国') * lst.Count * 1.3);
      szPos:= 0;
      for I := 0 to lst.Count - 1 do begin
        txtBmp.Canvas.TextOut(0, Round(i * txtBmp.Height / lst.Count), lst[i]);
        mPos:= txtBmp.Canvas.TextWidth(lst[i]);
        if mPos > szPos then szPos:= mPos;
      end;
      txtBmp.Width:= szPos;
      lst.Free;
//      WinApi.Windows.AlphaBlend(bmp.Canvas.Handle, bmp.Width - txtBmp.Width, ImgLogo.Height + 10, txtBmp.Width,
//        txtBmp.Height, txtBmp.Canvas.Handle, 0, 0, txtBmp.Width, txtBmp.Height, BF);
//      WinApi.Windows.AlphaBlend(bmp.Canvas.Handle, 10, 10, ImgLogo.Width,
//        ImgLogo.Height, ImgLogo.Canvas.Handle, 0, 0, ImgLogo.Width, ImgLogo.Height, BF);
      bmp.Canvas.Brush.Style:= bsClear;
      SetBmpFont(bmp.Canvas);
      bmp.Canvas.Font.Size:= 9;
      bmp.Canvas.TextOut(bmp.Width - 150, 10, DateTimeToStr(Now));
      if Image1.Height = BmpInfo.bmiHeader.biHeight then Image1.Picture.Bitmap.Assign(bmp)
      else Image1.Picture.Bitmap.Canvas.StretchDraw(Rect(0, 0, Image1.Width, Image1.Height), bmp);
    end;
  end;
  txtBmp.Free;
end;

procedure TFDlgCamera.EZoomChange(Sender: TObject);
begin
  capPreviewRate(CaptureHandle, Round(EZoom.Value / 100));
end;

procedure TFDlgCamera.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  FDlgCamera:= nil;
end;

procedure TFDlgCamera.N6Click(Sender: TObject);
begin
  if Sender = N1 then DisplayKind:= DK_TEXT
  else if Sender = N2 then DisplayKind:= DK_VIEW
  else if Sender = N3 then DisplayKind:= DK_BOTTOM
  else if Sender = N4 then DisplayKind:= DK_TOP
  else if Sender = N5 then DisplayKind:= DK_LEFT
  else if Sender = N6 then DisplayKind:= DK_RIGHT;
  btnDisplayKind.ImageIndex:= Ord(DisplayKind) + 4;
end;

procedure TFDlgCamera.SetIniFilename(const Value: string);
begin
  FIniFilename := Value;
  with TIniFile.Create(FIniFilename)do try
    FTextFontName:= ReadString('Camera', 'TextFontName', '宋体');
    FTextFontSize:= ReadInteger('Camera', 'TextFontSize', 24);
    FTextFontColor:= ReadInteger('Camera', 'TextFontColor', clBlack);
    FTextBackgroundColor:= ReadInteger('Camera', 'TextBackgroundColor', clWhite);
  finally
    Free;
  end;
end;

(* 摄像控制 *)

procedure TFDlgCamera.tbPauseClick(Sender: TObject);
begin
  if CaptureHandle <> 0 then
  begin
    capPreview(CaptureHandle, tbPause.ImageIndex = 0);
    //SendMessage(CaptureHandle, WM_CAP_SET_PREVIEW, tbPause.ImageIndex, 0);
    tbPause.ImageIndex := 1 - tbPause.ImageIndex;
  end;
end;

procedure TFDlgCamera.tbOpenClick(Sender: TObject);
var
  CapParms: TCAPTUREPARMS;
begin     // 定义视频输入格式
  FillChar(BmpInfo.bmiHeader, SizeOf(TBitmapInfoHeader), 0);
  with BmpInfo.bmiHeader do
  begin
    biBitCount := 24;
    biClrImportant := 0;
    biClrUsed := 0;
    biCompression := BI_RGB;
    biHeight := 1;
    biPlanes := 1;
    biSize := SizeOf(TBitmapInfoHeader);
    biSizeImage := 0;
    biWidth := 1;
    biXPelsPerMeter := 0;
    biYPelsPerMeter := 0;
  end;
  CaptureHandle := capCreateCaptureWindowA('My Own Capture Window',
          WS_CHILD or WS_VISIBLE, Image1.Left, Image1.Top, Image1.Width,
          Image1.Height, Handle, 0);
  if CaptureHandle = 0 then
  begin
    ShowMessage('创建窗口失败!');
    Exit;
  end;
  SendMessage(CaptureHandle, WM_CAP_SET_VIDEOFORMAT, SizeOf(TBitmapInfo), LongInt(@BmpInfo));
  capSetCallbackOnFrame(CaptureHandle, @FrameCallBack); // 帧格式
  capSetCallbackOnStatus(CaptureHandle, nil);
  CapParms.dwRequestMicroSecPerFrame := 1;
  CapParms.fLimitEnabled := FALSE;
  CapParms.fCaptureAudio := FALSE;
  CapParms.fMCIControl := FALSE;
  CapParms.fYield := True;
  CapParms.vKeyAbort := VK_ESCAPE;
  CapParms.fAbortLeftMouse := FALSE;
  CapParms.fAbortRightMouse := FALSE; // 让设置生效
  capCaptureSetSetup(CaptureHandle, @CapParms, SizeOf(TCAPTUREPARMS)); // 改变需要改变的参数
  capPreviewRate(CaptureHandle, 33); // 设置预览视频的频率
  capCaptureSequenceNoFile(CaptureHandle); // 不保存文件
  if not capDriverConnect(CaptureHandle, 0)then // 连接摄像头.0代表第一个摄像头
  begin
    ShowMessage('打开摄像头失败!');
    Exit;
  end;
  capGetVideoFormat(CaptureHandle, @BMPINFO,sizeof(TBitmapInfo));
  Image1.Width:= BMPINFO.bmiHeader.biWidth;
  Image1.Height:= BMPINFO.bmiHeader.biHeight;
  capPreviewScale(CaptureHandle, True);
  capPreviewRate(CaptureHandle, 1);
  capOverlay(CaptureHandle, True);
  capPreview(CaptureHandle, true);
//  btnOnlyTextClick(nil);
  tbPause.Enabled:= true;
  tbStop.Enabled:= true;
  tbOpen.Enabled:= false;
end;

procedure TFDlgCamera.tbStopClick(Sender: TObject);
begin
  if CaptureHandle <> 0 then
  begin
    SendMessage(CaptureHandle, WM_CAP_STOP, 0, 0);
    SendMessage(CaptureHandle, WM_CAP_DRIVER_DISCONNECT, 0, 0);
    tbPause.Enabled:= false;
    tbOpen.Enabled := true;
    tbStop.Enabled := false;
  end;
end;

procedure TFDlgCamera.tbCameraClick(Sender: TObject);
var
  s: AnsiString;
begin
  with SaveDialog1 do
    if Execute then begin
      S:=  Filename;
      SendMessage(CaptureHandle, WM_CAP_FILE_SAVEDIBA, 0, LongInt(PAnsiCHAR(S)));
    end;
end;

procedure TFDlgCamera.VideoText(Text: string);
var
  idx: Integer;
  bmp: TBitmap;
begin
  FVideoText:= Text;
  WriteLog(LOG_Debug, 'VideoText', Text);
  if DisplayKind = DK_TEXT then begin
    bmp:= TBitmap.Create;
    bmp.Height:= 105;
    bmp.Width:= 600;
    bmp.Canvas.Lock;
    bmp.Canvas.Brush.Color := FTextBackgroundColor;
    bmp.Canvas.FillRect(Rect(0, 0, 600, 105));
    bmp.Canvas.Font.Name:= FTextFontName;
    bmp.Canvas.Font.Color:= FTextFontColor;
    bmp.Canvas.Font.Size:= FTextFontSize;
    idx:= PosEx(' ', text, 18);
    bmp.Canvas.TextOut(0, 0, Copy(text, 1, idx));
    bmp.Canvas.TextOut(0, 50, Copy(text, idx + 1, MaxInt));
    bmp.Canvas.Unlock;
    Image1.Left:= 0;
    Image1.Top:= 28;
    Image1.Height:= 105;
    Image1.Width:= 600;
    Image1.Picture.Assign(bmp);
//    WriteLog(LOG_Debug, 'VideoColor', format('Brush.Color:%d, Font.Name:%s, Font.Size:%d，Font.Color:%d',
//      [FTextBackgroundColor, FTextFontName, FTextFontSize, FTextFontColor]));
//    WriteLog(LOG_Debug, 'Image', format('Left:%d, Top:%d, Width:%d, Height:%d, V:%s',
//      [Image1.Left, Image1.Top, Image1.Width, Image1.Height, BoolToStr(Image1.Visible)]));
//    if Image1.Visible then Image1.Repaint
//    else WriteLog(LOG_Debug, 'Image', 'Failed');
    bmp.Free;
  end;
  Self.Repaint;
end;

end.
