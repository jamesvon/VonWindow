unit UDlgConnection;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, DB, Data.Win.ADODB;

type
  TFDlgConnection = class(TForm)
    ELoginName: TLabeledEdit;
    ELoginPWD: TLabeledEdit;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    conTest: TADOConnection;
    procedure btnOKClick(Sender: TObject);
  private
    FDatabase: string;
    FServer: string;
    procedure SetDatabase(const Value: string);
    procedure SetServer(const Value: string);
    { Private declarations }
  public
    { Public declarations }
  published
    property Server: string read FServer write SetServer;
    property Database: string read FDatabase write SetDatabase;
  end;

var
  FDlgConnection: TFDlgConnection;

implementation

{$R *.dfm}

{ TFDlgConnection }

procedure TFDlgConnection.btnOKClick(Sender: TObject);
begin
  // Provider=SQLOLEDB.1;Password=111111;Persist Security Info=True;User ID=sa;Initial Catalog=master;Data Source=127.0.0.1;
  conTest.ConnectionString:= Format('Provider=SQLOLEDB.1;Data Source=%s;' +
    'Initial Catalog=%s;Persist Security Info=True;User ID=%s;Password=%s;',
    [FServer, FDatabase, ELoginName.Text, ELoginPWD.Text]);
  try
    conTest.Connected:= true;
    ModalResult:= mrOK;
  except
    ShowMessage('����ʧ�ܣ������³��ԡ�');
  end;
end;

procedure TFDlgConnection.SetDatabase(const Value: string);
begin
  FDatabase := Value;
end;

procedure TFDlgConnection.SetServer(const Value: string);
begin
  FServer := Value;
end;

end.
