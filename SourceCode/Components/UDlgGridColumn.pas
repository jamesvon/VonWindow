unit UDlgGridColumn;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, CheckLst, DBGrids, ComCtrls, ExtCtrls, Spin, DB;

type
  TEventOfGetColumnName = function (ColumnName: string): string of object;

  TFDlgGridColumn = class(TForm)
    GroupBox1: TGroupBox;
    CheckListBox1: TCheckListBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel1: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    Label1: TLabel;
    EFieldCaption: TEdit;
    btnDelColumn: TSpeedButton;
    Label2: TLabel;
    EWidth: TSpinEdit;
    btnUpdate: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure CheckListBox1Click(Sender: TObject);
    procedure btnUpdateClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnDelColumnClick(Sender: TObject);
  private
//    FGrid: TFrameGridBar;
//    procedure SetBarGrid(const Value: TFrameGridBar);
    { Private declarations }
  public
    { Public declarations }
//    property Grid: TFrameGridBar read FGrid write SetBarGrid;
  end;

var
  FDlgGridColumn: TFDlgGridColumn;

//procedure FilterColumn(BarGrid: TFrameGridBar); overload;
procedure FilterColumn(View: TListView); overload;

implementation

//procedure FilterColumn(BarGrid: TFrameGridBar);
//var
//  i, Idx: Integer;
//begin
//  Idx:= 0;
//  with TFDlgGridColumn.Create(nil) do
//    try
//      Grid:= BarGrid;
//      if ShowModal = mrOK then
//        Grid.SaveTitles;
//    finally
//      Free;
//    end;
//end;

procedure FilterColumn(View: TListView);
var
  i: Integer;
begin
  with TFDlgGridColumn.Create(nil) do
    try
      CheckListBox1.Items.Clear;
      for i := 0 to View.Columns.Count - 1 do
        CheckListBox1.Checked
          [CheckListBox1.Items.AddObject(View.Columns[i].DisplayName,
          View.Columns[i])] := not(View.Columns[i].Width = 0);
      if ShowModal = mrOK then
      begin
        View.Items.BeginUpdate;
        for i := 0 to CheckListBox1.Items.Count - 1 do
        begin
          (* Move column title *)
          (CheckListBox1.Items.Objects[i] as TListColumn).Index := i;
          if CheckListBox1.Checked[i] then
          begin
            if (CheckListBox1.Items.Objects[i] as TListColumn).Width = 0 then
              (CheckListBox1.Items.Objects[i] as TListColumn).Width :=
                -1 end else (CheckListBox1.Items.Objects[i] as TListColumn)
                .Width := 0;
          end;
          View.Items.EndUpdate;
        end;
    finally
      Free;
    end;
end;


{$R *.dfm}

//procedure TFDlgGridColumn.SetBarGrid(const Value: TFrameGridBar);
//var
//  i, j, Idx: Integer;
//  found: Boolean;
//  tmpColumn: TColumn;
//begin
//  FGrid := Value;
//  CheckListBox1.Items.Clear;
//  for I := 0 to FGrid.Grid.Columns.Count - 1 do begin
//    Idx:= CheckListBox1.Items.AddObject(FGrid.Grid.Columns[I].Title.Caption, FGrid.Grid.Columns[I]);
//    CheckListBox1.Checked[Idx] := FGrid.Grid.Columns[I].Visible;
//    FGrid.Grid.Columns[I].Field.Tag:= I + 1;
//  end;
//  for I := 0 to FGrid.GridSource.DataSet.Fields.Count - 1 do begin
//    if FGrid.GridSource.DataSet.Fields[I].Tag > 0 then Continue;
//    tmpColumn:= FGrid.Grid.Columns.Add;
//    tmpColumn.Field:= FGrid.GridSource.DataSet.Fields[i];
//    tmpColumn.Title.Caption:= FGrid.GridSource.DataSet.Fields[i].FieldName;
//    Idx:= CheckListBox1.Items.AddObject(tmpColumn.Title.Caption, tmpColumn);
//    tmpColumn.Visible:= False;
//    CheckListBox1.Checked[Idx] := false;
//  end;


//  for i := 0 to FGrid.GridSource.DataSet.Fields.Count - 1 do begin
//    found:= False;
//    for J := 0 to FGrid.Grid.Columns.Count - 1 do
//      if FGrid.Grid.Columns[J].Field = FGrid.GridSource.DataSet.Fields[I] then begin
//        Idx:= CheckListBox1.Items.AddObject(FGrid.Grid.Columns[J].Title.Caption, FGrid.Grid.Columns[J]);
//        CheckListBox1.Checked[Idx] := FGrid.Grid.Columns[J].Visible;
//        found:= True;
//        Break;
//      end;
//    if not found then begin
//      tmpColumn:= FGrid.Grid.Columns.Add;
//      tmpColumn.Field:= FGrid.GridSource.DataSet.Fields[i];
//      tmpColumn.Title.Caption:= FGrid.GridSource.DataSet.Fields[i].FieldName;
//      Idx:= CheckListBox1.Items.AddObject(tmpColumn.Title.Caption, tmpColumn);
//      tmpColumn.Visible:= False;
//      CheckListBox1.Checked[Idx] := false;
//    end;
//  end;
//end;

procedure TFDlgGridColumn.SpeedButton1Click(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to CheckListBox1.Items.Count - 1 do begin
    CheckListBox1.Checked[i] := true;
    (CheckListBox1.Items.Objects[CheckListBox1.ItemIndex] as TColumn).Visible:= True;
  end;
end;

procedure TFDlgGridColumn.SpeedButton2Click(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to CheckListBox1.Items.Count - 1 do begin
    CheckListBox1.Checked[i] := False;
    (CheckListBox1.Items.Objects[I] as TColumn).Visible:= False;
  end;
end;

procedure TFDlgGridColumn.SpeedButton3Click(Sender: TObject);
begin // Top
  if CheckListBox1.ItemIndex > 0 then
  begin
    (CheckListBox1.Items.Objects[CheckListBox1.ItemIndex] as TColumn).Index:= 0;
    CheckListBox1.Items.Move(CheckListBox1.ItemIndex, 0);
    CheckListBox1.ItemIndex := 0;
  end;
end;

procedure TFDlgGridColumn.SpeedButton6Click(Sender: TObject);
begin // Bottom
  if CheckListBox1.ItemIndex < CheckListBox1.Items.Count - 1 then
  begin
    (CheckListBox1.Items.Objects[CheckListBox1.ItemIndex] as TColumn).Index:=
      CheckListBox1.Items.Count - 1;
    CheckListBox1.Items.Move(CheckListBox1.ItemIndex,
      CheckListBox1.Items.Count - 1);
    CheckListBox1.ItemIndex := CheckListBox1.Items.Count - 1;
  end;
end;

procedure TFDlgGridColumn.SpeedButton4Click(Sender: TObject);
var
  CurrentID: Integer;
begin // Up
  CurrentID := CheckListBox1.ItemIndex;
  if CurrentID > 0 then
  begin
    (CheckListBox1.Items.Objects[CheckListBox1.ItemIndex] as TColumn).Index:=
      (CheckListBox1.Items.Objects[CheckListBox1.ItemIndex] as TColumn).Index - 1;
    CheckListBox1.Items.Move(CurrentID, CurrentID - 1);
    CheckListBox1.ItemIndex := CurrentID - 1;
  end;
end;

procedure TFDlgGridColumn.SpeedButton5Click(Sender: TObject);
var
  CurrentID: Integer;
begin // Down
  CurrentID := CheckListBox1.ItemIndex;
  if (CurrentID < CheckListBox1.Items.Count - 1) and (CurrentID > -1) then
  begin
    (CheckListBox1.Items.Objects[CheckListBox1.ItemIndex] as TColumn).Index:=
      (CheckListBox1.Items.Objects[CheckListBox1.ItemIndex] as TColumn).Index + 1;
    CheckListBox1.Items.Move(CurrentID, CurrentID + 1);
    CheckListBox1.ItemIndex := CurrentID + 1;
  end;
end;

procedure TFDlgGridColumn.CheckListBox1Click(Sender: TObject);
begin
  with CheckListBox1.Items.Objects[CheckListBox1.ItemIndex] as TColumn do begin
    EFieldCaption.Text := CheckListBox1.Items[CheckListBox1.ItemIndex];
    EWidth.Value := Width;
    Visible:= CheckListBox1.Checked[CheckListBox1.ItemIndex];
  end;
end;

procedure TFDlgGridColumn.btnUpdateClick(Sender: TObject);
begin
  CheckListBox1.Items[CheckListBox1.ItemIndex] := EFieldCaption.Text;
  with CheckListBox1.Items.Objects[CheckListBox1.ItemIndex] as TColumn do begin
    Title.Caption := EFieldCaption.Text;
    Width := EWidth.Value;
  end;
end;

procedure TFDlgGridColumn.FormCreate(Sender: TObject);
begin
  // if UserInfo.IsSpecial then btnDelColumn.Visible:= True;
end;

procedure TFDlgGridColumn.btnDelColumnClick(Sender: TObject);
begin
  if CheckListBox1.ItemIndex < 0 then
    Exit;
  if not Assigned(CheckListBox1.Items.Objects[CheckListBox1.ItemIndex]) then Exit;
  with CheckListBox1.Items.Objects[CheckListBox1.ItemIndex] as TColumn do
    TDBGrid(Grid).Columns.Delete(Index);
  CheckListBox1.Items.Delete(CheckListBox1.ItemIndex);
end;

end.
