object FDlgRichEditor: TFDlgRichEditor
  Left = 0
  Top = 0
  Caption = 'FDlgRichEditor'
  ClientHeight = 394
  ClientWidth = 643
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 359
    Width = 643
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    DesignSize = (
      643
      35)
    object BitBtn1: TBitBtn
      Left = 470
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #30830#23450
      Kind = bkOK
      NumGlyphs = 2
      TabOrder = 0
    end
    object BitBtn2: TBitBtn
      Left = 558
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #25918#24323
      Kind = bkCancel
      NumGlyphs = 2
      TabOrder = 1
    end
  end
  inline FrameRichEditor1: TFrameRichEditor
    Left = 0
    Top = 0
    Width = 643
    Height = 359
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Default'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitLeft = -302
    ExplicitTop = 65
    inherited Ruler: TPanel
      Top = 52
      Width = 643
      inherited Bevel1: TBevel
        Width = 643
      end
    end
    inherited Editor: TRichEdit
      Top = 78
      Width = 643
    end
    inherited StandardToolBar: TToolBar
      Width = 643
      Height = 52
      inherited FontSize: TEdit
        Width = 43
        ExplicitWidth = 43
      end
      inherited ToolButton16: TToolButton
        Left = 4
        Wrap = True
        ExplicitLeft = 4
        ExplicitHeight = 30
      end
      inherited LeftAlign: TToolButton
        Left = 4
        Top = 30
        ExplicitLeft = 4
        ExplicitTop = 30
      end
      inherited CenterAlign: TToolButton
        Left = 27
        Top = 30
        ExplicitLeft = 27
        ExplicitTop = 30
      end
      inherited RightAlign: TToolButton
        Left = 50
        Top = 30
        ExplicitLeft = 50
        ExplicitTop = 30
      end
      inherited ToolButton20: TToolButton
        Left = 73
        Top = 30
        ExplicitLeft = 73
        ExplicitTop = 30
      end
      inherited BulletsButton: TToolButton
        Left = 81
        Top = 30
        ExplicitLeft = 81
        ExplicitTop = 30
      end
      inherited ToolButton3: TToolButton
        Left = 104
        Top = 30
        ExplicitLeft = 104
        ExplicitTop = 30
      end
      inherited ToolButton4: TToolButton
        Left = 112
        Top = 30
        ExplicitLeft = 112
        ExplicitTop = 30
      end
      inherited ToolButton7: TToolButton
        Left = 135
        Top = 30
        ExplicitLeft = 135
        ExplicitTop = 30
      end
      inherited ColorBox2: TColorBox
        Left = 143
        Top = 30
        ExplicitLeft = 143
        ExplicitTop = 30
      end
      inherited ColorBox1: TColorBox
        Left = 235
        Top = 30
        ExplicitLeft = 235
        ExplicitTop = 30
      end
      inherited ToolButton6: TToolButton
        Left = 327
        Top = 30
        ExplicitLeft = 327
        ExplicitTop = 30
      end
      inherited EDateTime: TComboBox
        Left = 335
        Top = 30
        ExplicitLeft = 335
        ExplicitTop = 30
      end
    end
    inherited StatusBar: TStatusBar
      Top = 340
      Width = 643
    end
  end
end
