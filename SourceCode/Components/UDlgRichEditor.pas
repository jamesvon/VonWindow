unit UDlgRichEditor;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrameRichEditor, ComCtrls, StdCtrls, Buttons, ExtCtrls;

type
  TFDlgRichEditor = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    FrameRichEditor1: TFrameRichEditor;
    procedure FrameRichEditor1SaveButtonClick(Sender: TObject);
  private
    FSaved: Boolean;
    function GetEditor: TRichEdit;
    { Private declarations }
  public
    { Public declarations }
  published
    property Saved: Boolean read FSaved;
    property Editor: TRichEdit read GetEditor;
  end;

var
  FDlgRichEditor: TFDlgRichEditor;

implementation

{$R *.dfm}

procedure TFDlgRichEditor.FrameRichEditor1SaveButtonClick(Sender: TObject);
begin
  FSaved := true;
end;

function TFDlgRichEditor.GetEditor: TRichEdit;
begin
  Result := FrameRichEditor1.Editor;
end;

end.
