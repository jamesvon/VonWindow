unit UFrameArea;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, UVonClass, Buttons, ComCtrls, ToolWin, ImgList,
  Spin, Jpeg, PngImage, ExtDlgs, System.ImageList;

type
  TArea = class;

  TFrameArea = class(TFrame)
    plDesign: TPanel;
    ScrollBox1: TScrollBox;
    ImageList1: TImageList;
    Panel2: TPanel;
    Label1: TLabel;
    ELevelName: TEdit;
    ELevelColor: TColorBox;
    Label2: TLabel;
    Panel3: TPanel;
    Label3: TLabel;
    EAreaName: TEdit;
    Label4: TLabel;
    EAreaColor: TColorBox;
    ToolBar3: TToolBar;
    btnAreaAdd: TToolButton;
    btnAreaEdit: TToolButton;
    btnAreaDelete: TToolButton;
    btnAreaFont: TToolButton;
    btnAreaBringToTop: TToolButton;
    btnAreaBringToFront: TToolButton;
    btnAreaSendToBack: TToolButton;
    btnAreaSendToBottom: TToolButton;
    FontDialog1: TFontDialog;
    ELevelList: TColorListBox;
    ToolBar1: TToolBar;
    btnLevelAdd: TToolButton;
    btnLevelEdit: TToolButton;
    btnLevelDelete: TToolButton;
    ToolBar5: TToolBar;
    btnLevelToTop: TToolButton;
    btnLevelGoUp: TToolButton;
    btnLevelGoDown: TToolButton;
    btnLevelToBottom: TToolButton;
    ToolBar4: TToolBar;
    btnAreaPos1: TToolButton;
    btnAreaPos2: TToolButton;
    btnAreaPos3: TToolButton;
    btnAreaPos4: TToolButton;
    btnAreaPos5: TToolButton;
    btnAreaPos6: TToolButton;
    btnAreaPos7: TToolButton;
    btnAreaPos8: TToolButton;
    btnAreaPos9: TToolButton;
    Panel4: TPanel;
    btnBackground: TButton;
    Label5: TLabel;
    ERate: TSpinEdit;
    Label6: TLabel;
    ESnapPixel: TSpinEdit;
    Label7: TLabel;
    OpenPictureDialog1: TOpenPictureDialog;
    plView: TPanel;
    Panel6: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    EViewRate: TSpinEdit;
    ELevels: TColorListBox;
    btnStar: TBitBtn;
    procedure btnAreaAddClick(Sender: TObject);
    procedure btnLevelAddClick(Sender: TObject);
    procedure btnLevelEditClick(Sender: TObject);
    procedure btnLevelDeleteClick(Sender: TObject);
    procedure ELevelListClick(Sender: TObject);
    procedure btnAreaEditClick(Sender: TObject);
    procedure btnAreaDeleteClick(Sender: TObject);
    procedure btnMoveToTopClick(Sender: TObject);
    procedure btnAreaFontClick(Sender: TObject);
    procedure btnLevelToTopClick(Sender: TObject);
    procedure btnLevelGoUpClick(Sender: TObject);
    procedure btnLevelGoDownClick(Sender: TObject);
    procedure btnLevelToBottomClick(Sender: TObject);
    procedure btnAreaBringToTopClick(Sender: TObject);
    procedure btnAreaBringToFrontClick(Sender: TObject);
    procedure btnAreaSendToBackClick(Sender: TObject);
    procedure btnAreaSendToBottomClick(Sender: TObject);
    procedure btnAreaPosClick(Sender: TObject);
    procedure ERateChange(Sender: TObject);
    procedure ERateExit(Sender: TObject);
    procedure ERateKeyPress(Sender: TObject; var Key: Char);
    procedure btnBackgroundClick(Sender: TObject);
    procedure btnStarMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ELevelsClick(Sender: TObject);
    procedure EViewRateChange(Sender: TObject);
  private
    { Private declarations }
    FList: TVonConfig;
    FAreaList: TStringList;
    FCurrentLevel: Integer;
    FMaxID: Integer;
    FViewRate: Integer;
    FCurrentArea: TArea;
    FBackground: string;
    FOrgBackground: TPicture;
    FImage: TImage;
    procedure Clear();
    procedure EventOfSelectArea(Sender: TObject);
    procedure SaveLevelInfo;
    function GetText: string;
    procedure SetText(const Value: string);
    procedure SetBackground(const Value: string);
    function GetDisplayStar: Boolean;
    procedure SetDisplayStar(const Value: Boolean);
    function GetStarX: Integer;
    function GetStarY: Integer;
    procedure SetStarX(const Value: Integer);
    procedure SetStarY(const Value: Integer);
    procedure SetCurrentLevel(const Value: Integer);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property DisplayStar: Boolean read GetDisplayStar write SetDisplayStar;
    property CurrentLevel: Integer read FCurrentLevel write SetCurrentLevel;
    property StarX: Integer read GetStarX write SetStarX;
    property StarY: Integer read GetStarY write SetStarY;
    property Text: string read GetText write SetText;
    property Background: string read FBackground write SetBackground;
  end;

  TArea = class(TPanel)
  private
    FDown: Boolean;
    FOldX: TPoint;
    FOldY: TPoint;
    ShapeList: Array [1 .. 8] of TShape;
    FRectList: array [1 .. 8] of TRect;
    FPosList: array [1 .. 8] of Integer;
    FSnapSize: Integer;
    FTextPos: Integer;
    FRate: Integer;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure aaa(var a: TWMERASEBKGND); message WM_ERASEBKGND;
    procedure WmNcHitTest(var Msg: TWmNcHitTest); message wm_NcHitTest;
    procedure WmSize(var Msg: TWmSize); message wm_Size;
    procedure WmLButtonDown(var Msg: TWmLButtonDown); message wm_LButtonDown;
    procedure WmMove(var Msg: TWmMove); message Wm_Move;
    procedure SetSnapSize(const Value: Integer);
    function GetText: string;
    procedure SetText(const Value: string);
    procedure SetTextPos(const Value: Integer);
    procedure SetRate(const Value: Integer);
  public
    FCanvas: TCanvas;
    procedure Paint; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Rate: Integer read FRate write SetRate;
    property TextPos: Integer read FTextPos write SetTextPos default 4;
    property SnapSize: Integer read FSnapSize write SetSnapSize default 5;
    property Text: string read GetText write SetText;
  end;

implementation

{$R *.dfm}

{ TArea }

procedure TArea.WmNcHitTest(var Msg: TWmNcHitTest);
var
  Pt: TPoint;
  I: Integer;
begin
  Pt := Point(Msg.XPos, Msg.YPos);
  Pt := ScreenToClient(Pt);
  Msg.Result := 0;
  for I := 1 to 8 do
    if PtInRect(FRectList[I], Pt) then
      Msg.Result := FPosList[I];
  if Msg.Result = 0 then
    inherited;
end;

procedure TArea.WmSize(var Msg: TWmSize);
var
  R: TRect;
begin
  FRectList[1] := Rect(0, 0, 5, 5);
  FRectList[2] := Rect(Width div 2 - 3, 0, Width div 2 + 2, 5);
  FRectList[3] := Rect(Width - 5, 0, Width, 5);
  FRectList[4] := Rect(Width - 5, height div 2 - 3, Width, height div 2 + 2);
  FRectList[5] := Rect(Width - 5, height - 5, Width, height);
  FRectList[6] := Rect(Width div 2 - 3, height - 5, Width div 2 + 2, height);
  FRectList[7] := Rect(0, height - 5, 5, height);
  FRectList[8] := Rect(0, height div 2 - 3, 5, height div 2 + 2);
  Paint;
end;

procedure TArea.WmLButtonDown(var Msg: TWmLButtonDown);
begin
  inherited;
end;

procedure TArea.WmMove(var Msg: TWmMove);
var
  R: TRect;
begin
  R := BoundsRect;
  InflateRect(R, -2, -2);
  Paint;
end;

constructor TArea.Create(AOwner: TComponent);
var
  I: Integer;
begin
  inherited;
  ShowCaption:= False;
  FullRepaint := False;
  FSnapSize:= 5;
  FRate:= 100;
  FPosList[1] := htTopLeft;
  FPosList[2] := htTop;
  FPosList[3] := htTopRight;
  FPosList[4] := htRight;
  FPosList[5] := htBottomRight;
  FPosList[6] := htBottom;
  FPosList[7] := htBottomLeft;
  FPosList[8] := htLeft;
  for I := 1 to 8 do
  begin
    ShapeList[I] := TShape.Create(Self);
    ShapeList[I].Parent := Self;
    ShapeList[I].Brush.Color := clBlack;
    ShapeList[I].Visible := False;
  end;
end;

destructor TArea.Destroy;
begin
  inherited;
end;

function TArea.GetText: string;
var
  S: string;
begin
  FVonSetting.Clear;
  FVonSetting.Add('LEFT', IntToStr(Round(Left / FRate * 100) + TScrollBox(Parent).HorzScrollBar.ScrollPos));
  FVonSetting.Add('TOP', IntToStr(Round(Top / FRate * 100) + TScrollBox(Parent).VertScrollBar.ScrollPos));
  FVonSetting.Add('WIDTH', IntToStr(Round(Width / FRate * 100)));
  FVonSetting.Add('HEIGHT', IntToStr(Round(Height / FRate * 100)));
  FVonSetting.Add('COLOR', IntToStr(Color));
  FVonSetting.Add('Name', Caption);
  FVonSetting.Add('TextPos', IntToStr(FTextPos));
  FVonSetting.Add('FontName', Font.Name);
  FVonSetting.Add('FontColor', IntToStr(Font.Color));
  FVonSetting.Add('FontSize', IntToStr(Round(Font.Size / FRate * 100)));
  S:= '';
  if fsBold in Font.Style then S:= S + 'B';
  if fsItalic in Font.Style then S:= S + 'I';
  if fsUnderline in Font.Style then S:= S + 'U';
  if fsStrikeOut in Font.Style then S:= S + 'S';
  FVonSetting.Add('FontStyle', S);
  Result:= FVonSetting.Text[VSK_SEMICOLON];
end;

procedure TArea.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited;
  FDown := True;
  FOldX := Point(X, Y);
  SetFocus;
  Paint;
  if Assigned(OnClick) then
    OnClick(self);
end;

procedure TArea.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  NewPoint: TPoint;

  function findnearest(X, Y: Integer): TPoint;
  begin
    Result.X := (X div 5) * 5 + Round((X mod 5) / 5) * 5;
    Result.Y := (Y div 5) * 5 + Round((Y mod 5) / 5) * 5;
  end;

begin
  inherited;
  if FDown then
  begin
    NewPoint := findnearest(Left + X - FOldX.X, Top + Y - FOldX.Y);
    with Self do
      SetBounds(NewPoint.X, NewPoint.Y, Width, height);
    Paint;
  end;
end;

procedure TArea.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited;
  FDown := False;
end;

procedure TArea.Paint;
var
  I, baseH: Integer;
  txtList: TStringList;

  procedure DisplayName(text: string);
  var
    mPos, oPos: Integer;
  begin
    mPos:= 1;
    while(mPos <= Length(text))and(Canvas.TextWidth(Copy(text, 1, mPos)) < Width)do
      Inc(mPos);
    txtList.Add(Copy(text, 1, mPos - 1));
    text:= Copy(text, mPos, MaxInt);
    if text <> '' then DisplayName(text);
  end;
begin
  inherited;
  txtList:= TStringList.Create;
  Left:= Round(Left / FSnapSize * FSnapSize);
  Top:= Round(Top / FSnapSize * FSnapSize);
  Width := Round((BoundsRect.right - BoundsRect.Left) / FSnapSize * FSnapSize);
  Height := Round((BoundsRect.Bottom - BoundsRect.Top) / FSnapSize * FSnapSize);
  Canvas.Lock;
  Canvas.Brush.Color := Self.Color;
  Canvas.Pen.Color := Self.Color;
  Canvas.FillRect(Rect(0, 0, BoundsRect.right, BoundsRect.Bottom));
  if Focused then
  begin
    Canvas.Brush.Color := $FFFFFF - Self.Color;
    Canvas.Pen.Color := $FFFFFF - Self.Color;
  end
  else
  begin
    Canvas.Brush.Color := Self.Color;
    Canvas.Pen.Color := Self.Color;
  end;
  for I := 1 to 8 do
    with FRectList[I] do
      Canvas.RecTangle(Left, Top, right, Bottom);
  Canvas.Brush.Color := Self.Color;
  Canvas.Font.Assign(Font);
  DisplayName(Caption);
  baseH:= Round(Canvas.TextHeight('W') * 1.5);
  case FTextPos of
  0: for I := 0 to txtList.Count - 1 do Canvas.TextOut(0, I * baseH, txtList[I]);
  1: for I := 0 to txtList.Count - 1 do Canvas.TextOut((Width - Canvas.TextWidth(txtList[I])) div 2, Round(I * baseH), txtList[I]);
  2: for I := 0 to txtList.Count - 1 do Canvas.TextOut(Width - Canvas.TextWidth(txtList[I]), Round(I * baseH), txtList[I]);
  3: for I := 0 to txtList.Count - 1 do Canvas.TextOut(0, Round((Height - txtList.Count * baseH)/2 + I * baseH), txtList[I]);
  4: for I := 0 to txtList.Count - 1 do Canvas.TextOut((Width - Canvas.TextWidth(txtList[I])) div 2, Round((Height - txtList.Count * baseH)/2 + I * baseH), txtList[I]);
  5: for I := 0 to txtList.Count - 1 do Canvas.TextOut(Width - Canvas.TextWidth(txtList[I]), Round((Height - txtList.Count * baseH)/2 + I * baseH), txtList[I]);
  6: for I := 0 to txtList.Count - 1 do Canvas.TextOut(0, Height - txtList.Count * baseH + I * baseH, txtList[I]);
  7: for I := 0 to txtList.Count - 1 do Canvas.TextOut((Width - Canvas.TextWidth(txtList[I])) div 2, Height - txtList.Count * baseH + I * baseH, txtList[I]);
  8: for I := 0 to txtList.Count - 1 do Canvas.TextOut(Width - Canvas.TextWidth(txtList[I]), Height - txtList.Count * baseH + I * baseH, txtList[I]);
  end;
  txtList.Free;
  Canvas.Unlock;
end;

procedure TArea.SetRate(const Value: Integer);
begin
  Left:= Round(Left / FRate * Value);
  Top:= Round(Top / FRate * Value);
  Width:= Round(Width / FRate * Value);
  Height:= Round(Height / FRate * Value);
  Font.Size:= Round(Font.Size / FRate * Value);
  FRate := Value;
  Paint;
end;

procedure TArea.SetSnapSize(const Value: Integer);
begin
  FSnapSize := Value;
end;

procedure TArea.SetText(const Value: string);
var
  S: string;
begin
  FVonSetting.Clear;
  FVonSetting.Text[VSK_SEMICOLON]:= Value;
  Left:= Round(StrToInt(FVonSetting.NameValue['LEFT']) * FRate /100) - TScrollBox(Parent).HorzScrollBar.ScrollPos;
  Top:= Round(StrToInt(FVonSetting.NameValue['TOP']) * FRate /100) - TScrollBox(Parent).VertScrollBar.ScrollPos;
  Width:= Round(StrToInt(FVonSetting.NameValue['WIDTH']) * FRate /100);
  Height:=Round(StrToInt(FVonSetting.NameValue['HEIGHT']) * FRate /100);
  Color:= StrToInt(FVonSetting.NameValue['COLOR']);
  Caption:= FVonSetting.NameValue['Name'];
  FTextPos:= StrToInt(FVonSetting.NameValue['TextPos']);
  Font.Name:= FVonSetting.NameValue['FontName'];
  Font.Color:= StrToInt(FVonSetting.NameValue['FontColor']);
  Font.Size:= Round(StrToInt(FVonSetting.NameValue['FontSize']) * FRate /100);
  Font.Style:= [];
  S:= FVonSetting.NameValue['FontStyle'];
  if Pos('B', S) > 0 then Font.Style:= Font.Style + [fsBold];
  if Pos('I', S) > 0 then Font.Style:= Font.Style + [fsItalic];
  if Pos('U', S) > 0 then Font.Style:= Font.Style + [fsUnderline];
  if Pos('S', S) > 0 then Font.Style:= Font.Style + [fsStrikeOut];
end;

procedure TArea.SetTextPos(const Value: Integer);
begin
  FTextPos := Value;
end;

procedure TArea.aaa(var a: TWMERASEBKGND);
begin

end;

procedure TArea.CMExit(var Message: TCMExit);
begin
  Inherited;
  Paint;
end;

{ TFrameArea }

procedure TFrameArea.btnAreaAddClick(Sender: TObject);
var
  ARect : TRect;
begin
  if ELevelList.Count = 0 then
    raise Exception.Create('需要先添加层再添加区域。');
  if FAreaList.IndexOf(EAreaName.Text) >= 0 then
    raise Exception.Create('区域名称不可以重复，请更名后添加。');
  FCurrentArea:= TArea.Create(nil);
  with FCurrentArea do begin
    SnapSize:= ESnapPixel.Value;
    FRate:= ERate.Value;
    Caption:= EAreaName.Text;
    Color:= EAreaColor.Selected;
    Parent := ScrollBox1;
    ARect := BoundsRect;
    InflateRect(ARect,5,5);
    BoundsRect := ARect;
    ShowHint := True;
    Left := 12;
    Top := 323;
    BevelOuter := bvNone;
    Self.Cursor := crDefault;
    SetFocus;
    if btnAreaPos1.Down then TextPos:= 0
    else if btnAreaPos2.Down then TextPos:= 1
    else if btnAreaPos3.Down then TextPos:= 2
    else if btnAreaPos4.Down then TextPos:= 3
    else if btnAreaPos5.Down then TextPos:= 4
    else if btnAreaPos6.Down then TextPos:= 5
    else if btnAreaPos7.Down then TextPos:= 6
    else if btnAreaPos8.Down then TextPos:= 7
    else if btnAreaPos9.Down then TextPos:= 8;
    Font.Assign(FontDialog1.Font);
    Name:= 'AREA_' + IntToStr(FMaxID);
    Inc(FMaxID);
    OnClick:= EventOfSelectArea;
  end;
  FAreaList.AddObject(EAreaName.Text, FCurrentArea);
end;

procedure TFrameArea.btnAreaBringToFrontClick(Sender: TObject);
var
  Idx:Integer;
begin
  FCurrentArea.BringToFront;
  Idx:= FAreaList.IndexOfObject(FCurrentArea);
  FAreaList.Move(Idx, Idx + 1);
end;

procedure TFrameArea.btnAreaBringToTopClick(Sender: TObject);
var
  Idx:Integer;
begin
  FCurrentArea.BringToFront;
  Idx:= FAreaList.IndexOfObject(FCurrentArea);
  FAreaList.Move(Idx, FAreaList.Count - 1);
end;

procedure TFrameArea.btnAreaDeleteClick(Sender: TObject);
begin
  if Assigned(FCurrentArea) then begin
    FAreaList.Delete(FAreaList.IndexOf(FCurrentArea.Caption));
    FreeAndNil(FCurrentArea);
  end;
end;

procedure TFrameArea.btnAreaEditClick(Sender: TObject);
var
  Idx: Integer;
begin
  if not Assigned(FCurrentArea) then Exit;
  Idx:= FAreaList.IndexOf(FCurrentArea.Caption);
  if(FCurrentArea.Caption <> EAreaName.Text)and(FAreaList.IndexOf(EAreaName.Text) >= 0)then
    raise Exception.Create('区域名称不可以重复，请更名后添加。');
  FCurrentArea.Caption:= EAreaName.Text;
  FCurrentArea.Color:= EAreaColor.Selected;
end;

procedure TFrameArea.btnAreaFontClick(Sender: TObject);
begin
  if not Assigned(FCurrentArea) then Exit;
  with FontDialog1 do
    if Execute then
      FCurrentArea.Font.Assign(Font);
end;

procedure TFrameArea.btnAreaPosClick(Sender: TObject);
begin     //
  if not Assigned(FCurrentArea) then Exit;  
  FCurrentArea.TextPos:= TToolButton(Sender).ImageIndex - 9;
end;

procedure TFrameArea.btnAreaSendToBackClick(Sender: TObject);
var
  Idx:Integer;
begin
  FCurrentArea.SendToBack;
  Idx:= FAreaList.IndexOfObject(FCurrentArea);
  if Idx = 0 then Exit;
  FAreaList.Move(Idx, Idx - 1);
end;

procedure TFrameArea.btnAreaSendToBottomClick(Sender: TObject);
var
  Idx:Integer;
begin
  FCurrentArea.SendToBack;
  Idx:= FAreaList.IndexOfObject(FCurrentArea);
  if Idx = 0 then Exit;
  FAreaList.Move(Idx, 0);
end;

procedure TFrameArea.btnBackgroundClick(Sender: TObject);
begin
  if OpenPictureDialog1.Execute then
    Background:= OpenPictureDialog1.FileName;
end;

procedure TFrameArea.btnLevelAddClick(Sender: TObject);
begin
  if ELevelList.Items.IndexOf(ELevelName.Text) >= 0 then
    raise Exception.Create('这个层已经存在，请更换层名称进行存储。');
  SaveLevelInfo;
  Clear;
  FCurrentLevel:= ELevelList.Items.AddObject(ELevelName.Text, TObject(ELevelColor.Selected));
  ScrollBox1.Color:= ELevelColor.Selected;
end;

procedure TFrameArea.btnLevelDeleteClick(Sender: TObject);
begin
  if ELevelList.Count = 0 then Exit;
  FList.EraseSection(ELevelList.Items[FCurrentLevel]);
  ELevelList.Items.Delete(FCurrentLevel);
  FCurrentLevel:= -1;
  ELevelListClick(nil);
end;

procedure TFrameArea.btnLevelEditClick(Sender: TObject);
var
  Idx: Integer;
begin;
  if FCurrentLevel < 0 then Exit;
  Idx:= ELevelList.Items.IndexOf(ELevelName.Text);
  if(Idx >= 0)and(FCurrentLevel <> Idx)then
    raise Exception.Create('这个层已经存在，请更换层名称进行存储。');
  if(ELevelName.Text <> ELevelList.Items[FCurrentLevel])then begin
    FList.RenameSection(ELevelList.Items[FCurrentLevel], ELevelName.Text);
    ELevelList.Items[FCurrentLevel]:= ELevelName.Text;
  end;
  ELevelList.Items.Objects[FCurrentLevel]:= TObject(ELevelColor.Selected);
  ScrollBox1.Color:= ELevelColor.Selected;
end;

procedure TFrameArea.btnLevelGoDownClick(Sender: TObject);
begin
  ELevelList.Items.Move(FCurrentLevel, FCurrentLevel + 1);
end;

procedure TFrameArea.btnLevelGoUpClick(Sender: TObject);
begin
  if FCurrentLevel = 0 then Exit;
  ELevelList.Items.Move(FCurrentLevel, FCurrentLevel - 1);
end;

procedure TFrameArea.btnLevelToBottomClick(Sender: TObject);
begin
  ELevelList.Items.Move(FCurrentLevel, ELevelList.Count - 1);
end;

procedure TFrameArea.btnLevelToTopClick(Sender: TObject);
begin
  ELevelList.Items.Move(FCurrentLevel, 0);
end;

procedure TFrameArea.btnMoveToTopClick(Sender: TObject);
begin
  SaveLevelInfo;
end;

procedure TFrameArea.Clear;
var
  I: Integer;
begin
  for I := 0 to FAreaList.Count - 1 do
    FAreaList.Objects[I].Free;
  FAreaList.Clear;
  FMaxID:= 0;
end;

procedure TFrameArea.ELevelListClick(Sender: TObject);
var
  I: Integer;
begin
  if ELevelList.ItemIndex < 0 then Exit;
  SaveLevelInfo;
  if FCurrentLevel <> ELevelList.ItemIndex then begin
    Clear;
    FCurrentLevel:= ELevelList.ItemIndex;
    if FList.SectionExists(ELevelList.Items[FCurrentLevel]) then
      with FList.GetDirectionSection(ELevelList.Items[FCurrentLevel]) do
        for I := 0 to Count - 1 do begin
          FCurrentArea:= TArea.Create(nil);
          FCurrentArea.Parent:= ScrollBox1;
          FCurrentArea.Text:= ValueFromIndex[I];
          FCurrentArea.Rate:= ERate.Value;
          FCurrentArea.OnClick:= EventOfSelectArea;
          FAreaList.AddObject(FCurrentArea.Caption, FCurrentArea);
        end;
  end;
  ELevelName.Text:= ELevelList.Items[FCurrentLevel];
  ELevelColor.Selected:= ELevelList.Colors[FCurrentLevel];
  ScrollBox1.Color:= ELevelColor.Selected;
end;

procedure TFrameArea.ELevelsClick(Sender: TObject);
var
  I: Integer;
  szArea: TPanel;
  S: string;
begin
  if ELevels.ItemIndex < 0 then Exit;
  if FCurrentLevel <> ELevels.ItemIndex then begin
    Clear;
    FCurrentLevel:= ELevels.ItemIndex;
    ScrollBox1.Color:= ELevels.Selected;
    if FList.SectionExists(ELevels.Items[FCurrentLevel]) then
      with FList.GetDirectionSection(ELevels.Items[FCurrentLevel]) do
        for I := 0 to Count - 1 do begin
          szArea:= TPanel.Create(nil);
          szArea.Parent:= ScrollBox1;
          FVonSetting.Clear;
          FVonSetting.Text[VSK_SEMICOLON]:= ValueFromIndex[I];
          szArea.Left:= Round(StrToInt(FVonSetting.NameValue['LEFT']) * EViewRate.Value /100);
          szArea.Top:= Round(StrToInt(FVonSetting.NameValue['TOP']) * EViewRate.Value /100);
          szArea.Width:= Round(StrToInt(FVonSetting.NameValue['WIDTH']) * EViewRate.Value /100);
          szArea.Height:=Round(StrToInt(FVonSetting.NameValue['HEIGHT']) * EViewRate.Value /100);
          szArea.ParentBackground:= False;
          szArea.Color:= StrToInt(FVonSetting.NameValue['COLOR']);
          szArea.Caption:= FVonSetting.NameValue['Name'];
          szArea.Font.Name:= FVonSetting.NameValue['FontName'];
          szArea.Font.Color:= StrToInt(FVonSetting.NameValue['FontColor']);
          szArea.Font.Size:= Round(StrToInt(FVonSetting.NameValue['FontSize']) * EViewRate.Value /100);
          szArea.Font.Style:= [];
          S:= FVonSetting.NameValue['FontStyle'];
          if Pos('B', S) > 0 then szArea.Font.Style:= szArea.Font.Style + [fsBold];
          if Pos('I', S) > 0 then szArea.Font.Style:= szArea.Font.Style + [fsItalic];
          if Pos('U', S) > 0 then szArea.Font.Style:= szArea.Font.Style + [fsUnderline];
          if Pos('S', S) > 0 then szArea.Font.Style:= szArea.Font.Style + [fsStrikeOut];
          FAreaList.AddObject(szArea.Caption, szArea);
        end;
  end;
  btnStar.BringToFront;
  btnStar.Caption:= '';
end;

procedure TFrameArea.ERateChange(Sender: TObject);
var
  i: Integer;
begin
  for I := 0 to FAreaList.Count - 1 do
    TArea(FAreaList.Objects[i]).Rate:= ERate.Value;
  if Assigned(FImage) then begin
    FImage.Width:= Round(FOrgBackground.Width * ERate.Value / 100);
    FImage.Height:= Round(FOrgBackground.Height * ERate.Value / 100);
    FImage.Canvas.StretchDraw(Rect(0, 0, FImage.Width, FImage.Height), FOrgBackground.Graphic);
  end;
end;

procedure TFrameArea.ERateExit(Sender: TObject);
begin
  ERate.OnChange:= ERateChange;
  ERateChange(Sender);
end;

procedure TFrameArea.ERateKeyPress(Sender: TObject; var Key: Char);
begin
  ERate.OnChange:= nil;
end;

constructor TFrameArea.Create(AOwner: TComponent);
begin
  inherited;
  FList:= TVonConfig.Create('');
  FOrgBackground:= TPicture.Create;
  FAreaList:= TStringList.Create;
  FCurrentLevel:= -1;
  FMaxID:= 0;
  FViewRate:= 100;
  btnStar.Visible:= False;
end;

destructor TFrameArea.Destroy;
begin
  FOrgBackground.Free;
  FList.Free;
  inherited;
end;

procedure TFrameArea.EventOfSelectArea(Sender: TObject);
begin
  FCurrentArea:= TArea(Sender);
  with FCurrentArea do begin
    EAreaName.Text:= Caption;
    EAreaColor.Selected:= Color;
  end;
end;

procedure TFrameArea.EViewRateChange(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to FAreaList.Count - 1 do
    with TPanel(FAreaList.Objects[I]) do begin
      Left:= Round(Left / FViewRate * EViewRate.Value);
      Top:= Round(Top / FViewRate * EViewRate.Value);
      Width:= Round(Width / FViewRate * EViewRate.Value);
      Height:=Round(Height / FViewRate * EViewRate.Value);
      Font.Size:= Round(Font.Size / FViewRate * EViewRate.Value);
      Repaint;
    end;
  with btnStar do begin
    Left:= Round(Left / FViewRate * EViewRate.Value);
    Top:= Round(Top / FViewRate * EViewRate.Value);
  end;
  FImage.Width:= Round(FOrgBackground.Width * EViewRate.Value / 100);
  FImage.Height:= Round(FOrgBackground.Height * EViewRate.Value / 100);
  FImage.Canvas.StretchDraw(Rect(0, 0, FImage.Width, FImage.Height), FOrgBackground.Graphic);
  FViewRate:= EViewRate.Value;
end;

function TFrameArea.GetDisplayStar: Boolean;
begin
  Result:= btnStar.Visible;
end;

function TFrameArea.GetStarX: Integer;
begin
  Result:= btnStar.Left + ScrollBox1.HorzScrollBar.ScrollPos;
end;

function TFrameArea.GetStarY: Integer;
begin
  Result:= btnStar.Top + ScrollBox1.VertScrollBar.ScrollPos;
end;

function TFrameArea.GetText: string;
var
  I: Integer;
begin
  if ELevelList.Count > 0 then SaveLevelInfo;
  FList.EraseSection('LEVELS');
  for I := 0 to ELevelList.Count - 1 do
    FList.WriteInteger('LEVELS', ELevelList.Items[I], ELevelList.Colors[I]);
  FList.WriteString('LEVELS', '_Background', FBackground);
  Result:= FList.Text;
end;

procedure TFrameArea.SaveLevelInfo;
var
  I: Integer;
  szArea: TArea;
begin
  if FCurrentLevel = -1 then Exit;
  FList.EraseSection(ELevelList.Items[FCurrentLevel]);
  for I := 0 to FAreaList.Count - 1 do begin
    szArea:= TArea(FAreaList.Objects[I]);
    FList.WriteString(ELevelList.Items[FCurrentLevel],
      Format('%.3d', [I]), szArea.Text);
  end;
end;

procedure TFrameArea.SetBackground(const Value: string);
begin
  FBackground := Value;
  if Assigned(FImage) then FreeAndNil(FImage);
  if Value = '' then Exit;
  if Value <> '' then begin
    FImage:= TImage.Create(nil);
    FImage.Parent:= ScrollBox1;
    FImage.SendToBack;
    FOrgBackground.LoadFromFile(Value);
    FImage.Width:= Round(FOrgBackground.Width * ERate.Value / 100);
    FImage.Height:= Round(FOrgBackground.Height * ERate.Value / 100);
    FImage.Canvas.StretchDraw(Rect(0, 0, FImage.Width, FImage.Height), FOrgBackground.Graphic);
  end;
end;

procedure TFrameArea.SetCurrentLevel(const Value: Integer);
begin
  FCurrentLevel := Value;
  ELevelList.ItemIndex:= Value;
  ELevelListClick(nil);
end;

procedure TFrameArea.SetDisplayStar(const Value: Boolean);
begin
  btnStar.Visible:= Value;
  ELevels.Visible:= Value;
  plView.Visible:= Value;
  plDesign.Visible:= not Value;
  btnStar.BringToFront;
end;

procedure TFrameArea.SetStarX(const Value: Integer);
begin
  btnStar.Left:= Value;
end;

procedure TFrameArea.SetStarY(const Value: Integer);
begin
  btnStar.Top:= Value;
end;

procedure TFrameArea.SetText(const Value: string);
var
  I: Integer;
begin
  FList.Clear;
  FList.Text:= Value;
  Clear;
  Background:= FList.ReadString('LEVELS', '_Background', '');
  FList.DeleteKey('LEVELS', '_Background');
  if FList.SectionExists('LEVELS') then
    with FList.GetDirectionSection('LEVELS') do
      for I := 0 to Count - 1 do begin
        if plDesign.Visible then
          ELevelList.Items.AddObject(Names[I], TObject(StrToInt(ValueFromIndex[I])));
        if plView.Visible then
          ELevels.Items.AddObject(Names[I], TObject(StrToInt(ValueFromIndex[I])));
      end;
  if plDesign.Visible and(ELevelList.Count > 0)then begin
    ELevelList.ItemIndex:= 0;
    ELevelListClick(nil);
  end;
  if plView.Visible and(ELevels.Count > 0)then begin
    ELevels.ItemIndex:= 0;
    ELevelsClick(nil);
  end;
end;

procedure TFrameArea.btnStarMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  if ssLeft in Shift then begin
    btnStar.Left:= btnStar.Left + X - 12;
    btnStar.Top:= btnStar.Top + Y - 12;
  end;
end;

end.
