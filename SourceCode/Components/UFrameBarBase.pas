unit UFrameBarBase;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs;

resourcestring
  WIN_HINT = 'The system will expire after %d days.';
  WIN_MULTI_MORE = '您打开的窗口太多，请关闭一些窗口后再打开。';
  RES_NO_RIGHT = '您的权限不能使用本功能';

type
  TFrameBarBaseClass = class of TFrameBarBase;
  TFrameBarBase = class(TFrame)
  private
    procedure SetMainForm(const Value: TForm);
  protected
    FMainForm: TForm;
    procedure init; virtual;
    procedure SetIcon(Icon: TIcon); virtual; abstract;
  public
    function OpenedAForm(FormTitle: string; FormClass: TFormClass): TForm; virtual; abstract;
    procedure CloseForm(AForm: TForm); virtual; abstract;
    procedure ChangeFormState(AForm: TForm); virtual; abstract;
    procedure DisplayWinChildrenBtns; virtual; abstract;
    procedure SetTitle(const Title: string); virtual; abstract;
    procedure SetVersion(const Version: string); virtual; abstract;
    procedure SetSysHint(const SysHint: string); virtual; abstract;
    function GetFormCount: Integer; virtual; abstract;
    function GetFormByName(UsingName: string): TForm; virtual; abstract;
    function GetFormByIndex(Index: Integer): TForm; virtual; abstract;
  published
    property MainForm: TForm read FMainForm write SetMainForm;
    property FormCount: Integer read GetFormCount;
  end;

procedure RegBarComponent(Name: string; FormClass: TFrameBarBaseClass);
procedure GetBarList(Items: TStrings);
function CreateBarComponent(Name: string): TFrameBarBase;

implementation

type
  //TFrameClass = class of TFrame;
  TVonBanner = class
    /// <summary>功能说明</summary>
    BannerName: string;
    /// <summary>窗口类</summary>
    UsingClass: TFrameBarBaseClass;
  end;

var
  FBannerComponents: TStringList;

procedure GetBarList(Items: TStrings);
begin
  Items.Assign(FBannerComponents);
end;

procedure RegBarComponent(Name: string; FormClass: TFrameBarBaseClass);
var
  newBanner: TVonBanner;
begin
  newBanner:= TVonBanner.Create;
  newBanner.BannerName:= Name;
  newBanner.UsingClass:= FormClass;
  FBannerComponents.AddObject(Name, newBanner);
end;

function CreateBarComponent(Name: string): TFrameBarBase;
var
  Idx: Integer;
begin
  Idx:= FBannerComponents.IndexOf(Name);
  if Idx < 0 then Result:= nil
  else Result:= TVonBanner(FBannerComponents.Objects[Idx]).UsingClass.Create(nil);
end;

{$R *.dfm}

{ TFrameBannerBase }

procedure TFrameBarBase.init;
begin

end;

procedure TFrameBarBase.SetMainForm(const Value: TForm);
var
  szIcon: TIcon;
begin
  FMainForm := Value;
  szIcon := TIcon.Create;
  szIcon.LoadFromResourceName(MainInstance, 'MAINICON');
  SetIcon(szIcon);
  szIcon.Free;
  SetTitle(FMainForm.Caption);
  init;
end;

initialization
  FBannerComponents:= TStringList.Create;

finalization
  FBannerComponents.Free;

end.
