unit UFrameBarDeepBlue;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls,
  Vcl.ToolWin, Vcl.Tabs, Vcl.StdCtrls, Vcl.Buttons, Vcl.Imaging.jpeg,
  Vcl.ExtCtrls, System.ImageList, Vcl.ImgList, UFrameBarBase;

type
  TFrameBarDeepBlue = class(TFrameBarBase)
    imgWinBar: TImageList;
    Panel1: TPanel;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Panel2: TPanel;
    btnWinIcon: TSpeedButton;
    lbWinCaption: TLabel;
    lbWinVersion: TLabel;
    lbSystemHint: TLabel;
    tabWinChildren: TTabSet;
    ToolBar1: TToolBar;
    btnWinCascade: TToolButton;
    btnWinHTitle: TToolButton;
    btnWinVTitle: TToolButton;
    tbSeparator1: TToolButton;
    btnWinMin: TToolButton;
    btnWinMax: TToolButton;
    btnWinClose: TToolButton;
    ToolButton1: TToolButton;
    btnWinHelp: TToolButton;
    btnWinAbout: TToolButton;
    btnWinHome: TToolButton;
    btnWinExit: TToolButton;
    btnMainMin: TToolButton;
    procedure btnWinAboutClick(Sender: TObject);
    procedure btnWinCascadeClick(Sender: TObject);
    procedure btnWinCloseClick(Sender: TObject);
    procedure btnWinExitClick(Sender: TObject);
    procedure btnWinHelpClick(Sender: TObject);
    procedure btnWinHomeClick(Sender: TObject);
    procedure btnWinHTitleClick(Sender: TObject);
    procedure btnWinIconClick(Sender: TObject);
    procedure btnWinMaxClick(Sender: TObject);
    procedure btnWinMinClick(Sender: TObject);
    procedure btnWinVTitleClick(Sender: TObject);
    procedure lbWinCaptionDblClick(Sender: TObject);
    procedure lbWinCaptionMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure tabWinChildrenChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure tabWinChildrenGetImageIndex(Sender: TObject; TabIndex: Integer;
      var ImageIndex: Integer);
    procedure tabWinChildrenMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnMainMinClick(Sender: TObject);
    procedure tabWinChildrenMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
    procedure Init; override;
    procedure ChangeFormState(AForm: TForm); override;
    procedure CloseForm(AForm: TForm); override;
    procedure DisplayWinChildrenBtns; override;
    function GetFormByIndex(Index: Integer): TForm; override;
    function GetFormByName(UsingName: string): TForm; override;
    function GetFormCount: Integer; override;
    function OpenedAForm(FormTitle: string; FormClass: TFormClass): TForm; override;
    procedure SetIcon(Icon: TIcon); override;
    procedure SetSysHint(const SysHint: string); override;
    procedure SetTitle(const Value: string); override;
    procedure SetVersion(const Value: string); override;
  public
    { Public declarations }
  end;

var
  FrameBarDeepBlue: TFrameBarDeepBlue;

implementation

uses UPlatformMain;

{$R *.dfm}

{ TFrameBannerDeepBlue }

function TFrameBarDeepBlue.OpenedAForm(FormTitle: string; FormClass: TFormClass): TForm;
var
  Idx: Integer;
begin
  if tabWinChildren.Width - tabWinChildren.ItemRect(tabWinChildren.TabIndex).Right < 100 then
    raise Exception.Create(WIN_MULTI_MORE)
  else
  begin
    Idx:= tabWinChildren.Tabs.IndexOf(FormTitle);
    if Idx < 0 then begin
      Application.CreateForm(FormClass, Result);
      tabWinChildren.TabIndex := tabWinChildren.Tabs.AddObject(FormTitle, Result);
      //tabWinChildren.Width := tabWinChildren.Canvas.TextWidth(tabWinChildren.Tabs.Text) + 5;
      if Assigned(Result.OnActivate) then
        Result.OnActivate(Result);
    end else Result:= tabWinChildren.Tabs.Objects[Idx] as TForm;
  end;
end;

procedure TFrameBarDeepBlue.SetIcon(Icon: TIcon);
var
  bmp1: TBitmap;
begin
//  bmp1:= TBitmap.Create;
//  Icon.AssignTo(bmp1);
//  btnWinIcon.Glyph.SetSize(btnWinIcon.Width, btnWinIcon.Height);
//  btnWinIcon.Glyph.Canvas.Brush.Color := bmp1.Canvas.Pixels[1,1];
//  btnWinIcon.Glyph.Canvas.CopyMode:= cmMergeCopy;
//  btnWinIcon.Glyph.TransparentColor := bmp1.Canvas.Pixels[1,1];
//  btnWinIcon.Glyph.Canvas.StretchDraw(Rect(0, 0, btnWinIcon.Width, btnWinIcon.Height), bmp1);
//  bmp1.Free;
  btnWinIcon.Glyph.Assign(Icon);
end;

procedure TFrameBarDeepBlue.SetSysHint(const SysHint: string);
begin
  lbSystemHint.Caption := SysHint;
end;

procedure TFrameBarDeepBlue.SetTitle(const Value: string);
begin
  lbWinCaption.Caption := Value;
end;

procedure TFrameBarDeepBlue.SetVersion(const Value: string);
begin
  lbWinVersion.Caption := Value;
end;

procedure TFrameBarDeepBlue.ChangeFormState(AForm: TForm);
begin
  case AForm.WindowState of
    wsNormal:
      btnWinMax.ImageIndex := 2;
    wsMinimized:
      btnWinMax.ImageIndex := 1;
    wsMaximized:
      btnWinMax.ImageIndex := 1;
  end;
  //tabWinChildren.TabIndex := tabWinChildren.Tabs.IndexOfObject(AForm);
end;

procedure TFrameBarDeepBlue.CloseForm(AForm: TForm);
var
  Idx: Integer;
begin
  if not Assigned(AForm) then Idx := tabWinChildren.TabIndex
  else Idx := tabWinChildren.Tabs.IndexOfObject(AForm);
  if Idx >= 0 then
    tabWinChildren.Tabs.Delete(Idx);
  DisplayWinChildrenBtns;
end;

{$endregion}

function TFrameBarDeepBlue.GetFormByIndex(Index: Integer): TForm;
begin
  if Index >= tabWinChildren.Tabs.Count then Result:= nil
  else Result := TForm(tabWinChildren.Tabs.Objects[Index]);
end;

function TFrameBarDeepBlue.GetFormByName(UsingName: string): TForm;
var
  Idx: Integer;
begin
  Idx := tabWinChildren.Tabs.IndexOf(UsingName);
  if Idx >= 0 then begin
    Result := TForm(tabWinChildren.Tabs.Objects[Idx]);
    tabWinChildren.TabIndex:= Idx;
  end else Result:= nil;
end;

function TFrameBarDeepBlue.GetFormCount: Integer;
begin
  Result:= tabWinChildren.Tabs.Count;
end;

procedure TFrameBarDeepBlue.Init;
begin
  inherited;
  btnWinIcon.PopupMenu:= TFPlatformMain(MainForm).pmWinIcon;
end;

procedure TFrameBarDeepBlue.lbWinCaptionDblClick(Sender: TObject);
begin
  case MainForm.WindowState of
    wsNormal:
      MainForm.WindowState := wsMaximized;
    wsMinimized:
      MainForm.WindowState := wsNormal;
    wsMaximized:
      MainForm.WindowState := wsNormal;
  end;
end;

procedure TFrameBarDeepBlue.lbWinCaptionMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
//  if MainForm.WindowState = wsMaximized then
//    Exit;
  if not (ssLeft in Shift) then Exit;

  ReleaseCapture;
  MainForm.Perform(WM_SYSCOMMAND, $F012, 0);
end;

procedure TFrameBarDeepBlue.btnWinCascadeClick(Sender: TObject);
begin
  if (MainForm.FormStyle = fsMDIForm) and (MainForm.ClientHandle <> 0) then
    SendMessage(MainForm.ClientHandle, WM_MDICASCADE, 0, 0);
end;

procedure TFrameBarDeepBlue.btnWinCloseClick(Sender: TObject);
begin
  if Assigned(MainForm.ActiveMDIChild) then
    MainForm.ActiveMDIChild.Close
  else
    MainForm.Close;
end;

procedure TFrameBarDeepBlue.btnWinExitClick(Sender: TObject);
begin
  MainForm.Close;
end;

procedure TFrameBarDeepBlue.btnMainMinClick(Sender: TObject);
begin
  inherited;
  MainForm.WindowState:= wsMinimized;
end;

procedure TFrameBarDeepBlue.btnWinAboutClick(Sender: TObject);
begin
  TFPlatformMain(FMainForm).OpenASystemWindow('��������', 'ABOUT');
end;

procedure TFrameBarDeepBlue.btnWinHelpClick(Sender: TObject);
begin
  TFPlatformMain(FMainForm).OpenASystemWindow('��������', 'HELP');
end;

procedure TFrameBarDeepBlue.btnWinHomeClick(Sender: TObject);
begin
  TFPlatformMain(FMainForm).OpenASystemWindow('������վ', 'WEB');
end;

procedure TFrameBarDeepBlue.btnWinHTitleClick(Sender: TObject);
begin
  if (MainForm.FormStyle = fsMDIForm) and (MainForm.ClientHandle <> 0) then
    SendMessage(MainForm.ClientHandle, WM_MDITILE, MDITILE_VERTICAL, 0);
end;

procedure TFrameBarDeepBlue.btnWinIconClick(Sender: TObject);
begin
  with btnWinIcon do
    if Assigned(PopupMenu) then
      PopupMenu.Popup(MainForm.Left + Width, MainForm.Top + Height);
end;

procedure TFrameBarDeepBlue.btnWinMaxClick(Sender: TObject);
var
  szForm: TForm;
begin
  if Assigned(MainForm.ActiveMDIChild) then
    szForm := MainForm.ActiveMDIChild
  else szForm := MainForm;
  case szForm.WindowState of
    wsNormal:
      szForm.WindowState := wsMaximized;
    wsMinimized:
      szForm.WindowState := wsNormal;
    wsMaximized:
      szForm.WindowState := wsNormal;
  end;
end;

procedure TFrameBarDeepBlue.btnWinMinClick(Sender: TObject);
begin
  MainForm.WindowState := wsMinimized;
end;

procedure TFrameBarDeepBlue.btnWinVTitleClick(Sender: TObject);
begin
  if (MainForm.FormStyle = fsMDIForm) and (MainForm.ClientHandle <> 0) then
    SendMessage(MainForm.ClientHandle, WM_MDITILE, MDITILE_HORIZONTAL, 0);
end;

procedure TFrameBarDeepBlue.tabWinChildrenChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
  DisplayWinChildrenBtns;
  if NewTab < 0 then
    Exit;
  (tabWinChildren.Tabs.Objects[NewTab] as TForm).Show;
end;

procedure TFrameBarDeepBlue.tabWinChildrenGetImageIndex(Sender: TObject;
  TabIndex: Integer; var ImageIndex: Integer);
begin
  if TabIndex = tabWinChildren.TabIndex then
    ImageIndex := 3
  else
    ImageIndex := -1;
end;

procedure TFrameBarDeepBlue.tabWinChildrenMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  P: TPoint;
  R: TRect;
  Idx: Integer;
begin
  P.X := X;
  P.Y := Y;
  Idx := tabWinChildren.ItemAtPos(P);
  if Idx < 0 then
    Exit;
  if Idx <> tabWinChildren.TabIndex then
    Exit;
  R := tabWinChildren.ItemRect(Idx);
  if (R.Left < X) and (R.Left + 18 > X) then
    (tabWinChildren.Tabs.Objects[Idx] as TForm).Close;
end;

procedure TFrameBarDeepBlue.tabWinChildrenMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  if FMainForm.WindowState = wsMaximized then
    Exit;
  ReleaseCapture;
  SendMessage(FPlatformMain.Handle,WM_SYSCOMMAND,SC_MOVE or HTCAPTION,0);
end;

procedure TFrameBarDeepBlue.DisplayWinChildrenBtns;
begin
  btnWinCascade.Visible := tabWinChildren.Tabs.Count > 1;
  btnWinHTitle.Visible := btnWinCascade.Visible;
  btnWinVTitle.Visible := btnWinCascade.Visible;
  tbSeparator1.Visible := btnWinCascade.Visible;
end;

initialization
  RegBarComponent('�����Ƽ�', TFrameBarDeepBlue);

end.
