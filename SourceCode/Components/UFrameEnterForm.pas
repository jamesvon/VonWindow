(*******************************************************************************
* TFrameEnterForm 支持自定义设计录入界面
*===============================================================================
* TEnterFormItemBase 录入组件的基类，在此基类外面支持未来的自定义组件
*- 组件管理函数 ----------------------------------------------------------------
*   [P]RegistEnterFormComponent        注册一个录入组件
*     ComName: string;                     组件类型名称
*     ItemClass: TEnterFormItemClass;      组件实现类(继承于TEnterFormItemBase)
*     AttributeXml: string);               组件属性描述(基于TFrameEnterForm的Xml)
*   [f]GetEnterFormComponentXml        得到一个注册录入组件属性的xml内容
*     ComName: string                      组件类型名称
*   [f]GetEnterFormComponentClass      得到一个组件的实现类
*     ComName: string                      组件类型名称
*- 样例代码 --------------------------------------------------------------------
RegistEnterFormComponent('整数', TEnterForm_Int,
  '<整数:Max Title="最大值" /><整数:Min Title="最小值" /><整数:Inc Title="步长" />');
RegistEnterFormComponent('多行文本', TEnterForm_Memo,
  '<整数:Max Title="最多文字" /><单选:Out Title="数据格式" Items="多行输出,单行输出">多行输出</Out>');
*===============================================================================
* TEnterFormNode 录入组件信息类
*- 属性 ----------------------------------------------------------------
    AttributeCount: Integer;     [R]本节点属性数量
    ItemCount: Integer;          [R]子节点数量
    NameSpace: string;           [RW]命名空间，在这里实际是组件类型名称
    ItemName: string;            [RW]组件名称
    NodeObj: TEnterFormItemBase; [R]组件实例(继承于TEnterFormItemBase)
    Xml: string;                 [RW]组件属性描述及值设定
    Attribute[AttrName: string]: string;        [R]本节点属性文本内容
    AttrIntValue[AttrName: string]: Integer;    [R]本节点属性整数内容
    AttrFloatValue[AttrName: string]: Extended; [R]本节点属性浮点内容
    AttrBoolValue[AttrName: string]: Boolean;   [R]本节点属性布尔内容
    AttributeName[Index: Integer]: string;      [R]本节点属性某序号属性名称
    AttributeValue[Index: Integer]: string;     [R]本节点属性某序号属性内容
    Item[ItemName: string]: TEnterFormNode;     [R]本节点下级节点信息
    ItemKey[Index: Integer]: string;            [R]本节点某序号下级节点名称
    ItemObj[Index: Integer]: TEnterFormNode;    [R]本节点某序号下级节点信息
*- 方法 ----------------------------------------------------------------
    [P] Clear                     删除所有属性和下级节点信息
    [p] Display                   显示组件内容
    [f] ReadXml                   读取xml内容，转换为实际节点信息
    [f] GetDataValue              读取某一路径节点值，路径以小数点间隔
    [p] AddAttribute              添加某一属性
    [p] DeleteAttribute           删除某一属性
    [f] AddChild                  添加一个下级节点
    [p] DeleteChild               删除某一下级节点
*===============================================================================
* TFrameEnterForm   录入界面
*- 使用样例 --------------------------------------------------------------------
  FrameEnterForm1.Xml:= '<段落:代理机构信息 Title="代理机构信息" ' +
    'CanExpand="False"><段落:代理人地址 Alignment="120" Title="代理人地址" ' +
    'CanExpand="False" /></段落:代理机构信息>';

*******************************************************************************)
unit UFrameEnterForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.ComCtrls, Vcl.Samples.Spin, Vcl.Mask, Vcl.CheckLst, DB, Data.Win.ADODB,
  UVonSystemfuns, UVonClass, UPlatformDB, Xml.xmldom, Xml.XMLIntf,
  Xml.Win.msxmldom, Xml.XMLDoc;

type
  TTreeMoveKind = (mk_Top, mk_Up, mk_Down, mk_Bottom, mk_Left, mk_Right);
  TEnterFormItemBase = class;
  /// <summary>控件基类</summary>
  TEnterFormNode = class(TObject)
  private
    FAttributes: TStringList;
    FItemName: string;
    FItems: TStringList;
    FNodeObj: TEnterFormItemBase;
    FNodeValue: string;
    FNameSpace: string;
    function GetAttribute(AttrName: string): string;
    function GetAttributeCount: Integer;
    function GetAttributeName(Index: Integer): string;
    function GetAttributeValue(Index: Integer): string;
    function GetItem(ItemName: string): TEnterFormNode;
    function GetItemCount: Integer;
    function GetItemKey(Index: Integer): string;
    function GetItemObj(Index: Integer): TEnterFormNode;
    function GetXml: string;
    procedure SetXml(const Value: string);
    function GetAttrBoolValue(AttrName: string): Boolean;
    function GetAttrFloatValue(AttrName: string): Extended;
    function GetAttrIntValue(AttrName: string): Integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure ClearAttribute;
    procedure Display(AParent: TWinControl);
    function ReadXml(P: PChar): PChar;
    function GetDataValue(Path: string): string;
    procedure AddAttribute(AttrName, AttrValue: string);
    procedure DeleteAttribute(AttrName: string);
    function AddChild(Key: string): TEnterFormNode; overload;
    procedure AddChild(Node: TEnterFormNode); overload;
    procedure DeleteChild(Node: TEnterFormNode);
    procedure MoveChildTo(Node: TEnterFormNode; Kind: TTreeMoveKind);
    procedure SetXmlNode(const Value: IXMLNode);
    procedure GetXmlNode(const ANode: IXMLNode);
    /// <summary>[R]本节点属性文本内容</summary>
    property Attribute[AttrName: string]: string read GetAttribute;
    /// <summary>[R]本节点属性整数内容</summary>
    property AttrIntValue[AttrName: string]: Integer read GetAttrIntValue;
    /// <summary>[R]本节点属性浮点内容</summary>
    property AttrFloatValue[AttrName: string]: Extended read GetAttrFloatValue;
    /// <summary>[R]本节点属性布尔内容</summary>
    property AttrBoolValue[AttrName: string]: Boolean read GetAttrBoolValue;
    /// <summary>[R]本节点属性某序号属性名称</summary>
    property AttributeName[Index: Integer]: string read GetAttributeName;
    /// <summary>[R]本节点属性某序号属性内容</summary>
    property AttributeValue[Index: Integer]: string read GetAttributeValue;
    /// <summary>[R]本节点下级节点信息</summary>
    property Item[ItemName: string]: TEnterFormNode read GetItem;
    /// <summary>[R]本节点某序号下级节点名称</summary>
    property ItemKey[Index: Integer]: string read GetItemKey;
    /// <summary>[R]本节点某序号下级节点信息</summary>
    property ItemObj[Index: Integer]: TEnterFormNode read GetItemObj;
  published
    /// <summary>[R]本节点属性数量</summary>
    property AttributeCount: Integer read GetAttributeCount;
    /// <summary>[R]子节点数量</summary>
    property ItemCount: Integer read GetItemCount;
    /// <summary>[RW]命名空间，在这里实际是组件类型名称</summary>
    property NameSpace: string read FNameSpace write FNameSpace;
    /// <summary>[RW]组件名称</summary>
    property ItemName: string read FItemName write FItemName;
    /// <summary>[R]组件实例(继承于TEnterFormItemBase)</summary>
    property NodeObj: TEnterFormItemBase read FNodeObj;
    /// <summary>[RW]组件属性描述及值设定</summary>
    property Xml: string read GetXml write SetXml;
  end;

  TEnterFormItemBase = class abstract(TPanel)
  private
    FNode: TEnterFormNode;
  protected
    procedure Display; virtual; abstract;
    function GetValue: string; virtual; abstract;
    procedure SetValue(const Value: string); virtual; abstract;
  published
    property Node: TEnterFormNode read FNode write FNode;
    property Value: string read GetValue write SetValue;
  end;

  TFrameEnterForm = class(TFrame)
    ScrollBox1: TScrollBox;
  private
    Nodes: TStringList;
    function GetItem(ItemName: string): TEnterFormNode;
    function GetItemCount: Integer;
    function GetItemKey(Index: Integer): string;
    function GetItemObj(Index: Integer): TEnterFormNode;
    function GetXml: string;
    procedure SetXml(const Value: string);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Clear;
    function GetDataValue(Path: string): string;
    property Item[ItemName: string]: TEnterFormNode read GetItem;
    property ItemKey[Index: Integer]: string read GetItemKey;
    property ItemObj[Index: Integer]: TEnterFormNode read GetItemObj;
  published
    property ItemCount: Integer read GetItemCount;
    property Xml: string read GetXml write SetXml;
  end;

  TEnterForm_Section = class(TEnterFormItemBase)
  private
    lbTitle: TLabel;
  protected
    procedure Display; override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    destructor Destroy; override;
  end;

  TEnterForm_Date = class(TEnterFormItemBase)
  private
    lbTitle: TLabel;
    eEdit: TDateTimePicker;
  protected
    procedure Display; override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    destructor Destroy; override;
  end;

  TEnterForm_Text = class(TEnterFormItemBase)
  private
    lbTitle: TLabel;
    eEdit: TEdit;
  protected
    procedure Display; override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    destructor Destroy; override;
  end;

  TEnterForm_Zone = class(TEnterFormItemBase)
  private
    lbTitle: TLabel;
    eState: TComboBox;
    eProvince: TComboBox;
    eCity: TComboBox;
    eCounty: TComboBox;
    procedure EventOfState(Sender: TObject);
    procedure EventOfProvince(Sender: TObject);
    procedure EventOfCity(Sender: TObject);
  protected
    procedure Display; override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    destructor Destroy; override;
  end;

  TEnterForm_Time = class(TEnterFormItemBase)
  private
    lbTitle: TLabel;
    eEdit: TDateTimePicker;
  protected
    procedure Display; override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    destructor Destroy; override;
  end;

  TEnterForm_Selection = class(TEnterFormItemBase)
  private
    lbTitle: TLabel;
    eEdit: TComboBox;
  protected
    procedure Display; override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    destructor Destroy; override;
  end;

  TEnterForm_Memo = class(TEnterFormItemBase)
  private
    lbTitle: TLabel;
    eEdit: TMemo;
  protected
    procedure Display; override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    destructor Destroy; override;
  end;

  TEnterForm_Int = class(TEnterFormItemBase)
  private
    lbTitle: TLabel;
    eEdit: TSpinEdit;
  protected
    procedure Display; override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    destructor Destroy; override;
  end;

  TEnterForm_Guid = class(TEnterFormItemBase)
  private
    lbTitle: TLabel;
    eEdit: TButtonedEdit;
    procedure EventOfClick(Sender: TObject);
  protected
    procedure Display; override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    destructor Destroy; override;
  end;

  TEnterForm_Float = class(TEnterFormItemBase)
  private
    lbTitle: TLabel;
    eEdit: TEdit;
  protected
    procedure Display; override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    destructor Destroy; override;
  end;

  TEnterForm_Dim = class(TEnterFormItemBase)
  private
    lbTitle: TLabel;
    eEdit: TTreeView;
  protected
    procedure Display; override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    destructor Destroy; override;
  end;

  TEnterForm_Choice = class(TEnterFormItemBase)
  private
    lbTitle: TLabel;
    eRG: TRadioGroup;
    eEdit: TEdit;
    FMaxLength: Integer;
    procedure EventOfRGSelect(Sender: TObject);
    procedure EventOfResize(Sender: TObject);
  protected
    procedure Display; override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    destructor Destroy; override;
  end;

  TEnterForm_CheckList = class(TEnterFormItemBase)
  private
    FMaxLength: Integer;
    lbTitle: TLabel;
    eEdit: TCheckListBox;
    procedure EventOfResize(Sender: TObject);
  protected
    procedure Display; override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  public
    destructor Destroy; override;
  end;

  TEnterForm_Bool = class(TEnterFormItemBase)
  private
    eEdit: TCheckBox;
  protected
    destructor Destroy; override;
    procedure Display; override;
    function GetValue: string; override;
    procedure SetValue(const Value: string); override;
  end;

  TEnterFormItemClass = class of TEnterFormItemBase;

  procedure RegistEnterFormComponent(ComName: string;
    ItemClass: TEnterFormItemClass; AttributeXml: string);
  function GetEnterFormComponentXml(ComName: string): string;
  function GetEnterFormComponentClass(ComName: string): TEnterFormItemClass;

var
  EnterFormComponentList : TStringList;

implementation

type
  TEnterFormComponentInfo = class(TObject)
    AttributeXml: string;
    ComName: string;
    ItemClass: TEnterFormItemClass;
  end;

procedure RegistEnterFormComponent(ComName: string; ItemClass: TEnterFormItemClass; AttributeXml: string);
var
  Info: TEnterFormComponentInfo;
begin
  Info:= TEnterFormComponentInfo.Create;
  Info.ComName:= ComName;
  Info.AttributeXml:= '<template>' + AttributeXml + '</template>';
  Info.ItemClass:= ItemClass;
  EnterFormComponentList.AddObject(ComName, Info);
end;

function GetEnterFormComponentInfo(ComName: string): TEnterFormComponentInfo;
var
  Idx: Integer;
begin
  Idx:= EnterFormComponentList.IndexOf(ComName);
  if Idx < 0 then Result:= nil
  else Result:= TEnterFormComponentInfo(EnterFormComponentList.Objects[Idx]);
end;

function GetEnterFormComponentXml(ComName: string): string;
var
  obj: TEnterFormComponentInfo;
begin
  obj:= GetEnterFormComponentInfo(ComName);
  if Assigned(obj) then Result:= obj.AttributeXml else Result:= '';
end;

function GetEnterFormComponentClass(ComName: string): TEnterFormItemClass;
var
  obj: TEnterFormComponentInfo;
begin
  obj:= GetEnterFormComponentInfo(ComName);
  if Assigned(obj) then Result:= obj.ItemClass else Result:= nil;
end;

{$R *.dfm}
{
******************************** TEnterFormNode ********************************
}
constructor TEnterFormNode.Create;
begin
  FAttributes:= TStringList.Create;
  FItems:= TStringList.Create;
end;

destructor TEnterFormNode.Destroy;
begin
  Clear;
  FItems.Free;
  FAttributes.Free;
  if Assigned(FNodeObj) then FNodeObj.Free;
end;

(* Methods *)

procedure TEnterFormNode.DeleteAttribute(AttrName: string);
var
  Idx: Integer;
begin
  Idx:= FAttributes.IndexOfName(AttrName);
  if Idx < 0 then Exit;
  FAttributes.Delete(Idx);
end;

procedure TEnterFormNode.DeleteChild(Node: TEnterFormNode);
var
  Idx: Integer;
begin
  Idx:= FItems.IndexOfObject(Node);
  if Idx < 0 then Exit;
  Node.Free;
  FItems.Delete(Idx);
end;

procedure TEnterFormNode.AddAttribute(AttrName, AttrValue: string);
begin
  FAttributes.Values[AttrName]:= AttrValue;
end;

function TEnterFormNode.AddChild(Key: string): TEnterFormNode;
var
  Idx: Integer;
  newNode: TEnterFormNode;
begin
  Idx:= FItems.IndexOf(Key);
  if Idx >= 0 then Result:= GetItemObj(Idx)
  else begin
    Result:= TEnterFormNode.Create;
    Result.FItemName:= Key;
    FItems.AddObject(Key, Result);
  end;
end;

procedure TEnterFormNode.AddChild(Node: TEnterFormNode);
var
  Idx: Integer;
  newNode: TEnterFormNode;
begin
  Idx:= FItems.IndexOf(Node.ItemName);
  if Idx >= 0 then raise Exception.Create(RES_INFO_DOUBLICATION);
  FItems.AddObject(Node.ItemName, Node);
end;

procedure TEnterFormNode.Clear;
var
  I: Integer;
begin
  for I := 0 to FItems.Count - 1 do
    FItems.Objects[I].Free;
  FItems.Clear;
  FAttributes.Clear;
end;

procedure TEnterFormNode.MoveChildTo(Node: TEnterFormNode; Kind: TTreeMoveKind);
var
  src: Integer;
begin
  src:= FItems.IndexOfObject(Node);
  case Kind of
  mk_Top: FItems.Move(src, 0);
  mk_Up: FItems.Move(src, src - 1);
  mk_Down: FItems.Move(src, src + 1);
  mk_Bottom: FItems.Move(src, FItems.Count - 1);
  mk_Left, mk_Right: FItems.Delete(src);
  end;
end;

procedure TEnterFormNode.ClearAttribute;
begin
  FAttributes.Clear;
end;

(* Properties *)

procedure TEnterFormNode.Display(AParent: TWinControl);
var
  Idx: Integer;
  info: TEnterFormComponentInfo;
begin
  Idx:= EnterFormComponentList.IndexOf(NameSpace);
  if Idx >= 0 then begin
    info:= TEnterFormComponentInfo(EnterFormComponentList.Objects[Idx]);
    FNodeObj:= info.ItemClass.Create(nil);
    FNodeObj.Parent:= AParent;
    FNodeObj.Node:= Self;
    FNodeObj.Display;
    FNodeObj.Value:= FNodeValue;
  end;
end;

function TEnterFormNode.GetAttrBoolValue(AttrName: string): Boolean;
var
  S: string;
begin
  S:= FAttributes.Values[AttrName];
  Result:= (S <> '')and((S[1] = 'T')or(S[1] = 't'));
end;

function TEnterFormNode.GetAttrFloatValue(AttrName: string): Extended;
begin
  if not TryStrToFloat(FAttributes.Values[AttrName], Result) then Result:= 0;
end;

function TEnterFormNode.GetAttribute(AttrName: string): string;
begin
  Result:= FAttributes.Values[AttrName];
end;

function TEnterFormNode.GetAttributeCount: Integer;
begin
  Result:= FAttributes.Count;
end;

function TEnterFormNode.GetAttributeName(Index: Integer): string;
begin
  Result:= FAttributes.Names[Index];
end;

function TEnterFormNode.GetAttributeValue(Index: Integer): string;
begin
  Result:= FAttributes.ValueFromIndex[Index];
end;

function TEnterFormNode.GetAttrIntValue(AttrName: string): Integer;
begin
  if not TryStrToInt(FAttributes.Values[AttrName], Result) then Result:= 0;
end;

function TEnterFormNode.GetDataValue(Path: string): string;
var
  I, Idx: Integer;
  szName: string;
begin
  Idx:= Pos('.', Path);
  if Idx < 1 then szName:= Path else begin
    szName:= Copy(Path, 1, Idx - 1);
    Path:= Copy(Path, Idx + 1, MaxInt);
  end;
  for I := 0 to ItemCount - 1 do
    if ItemObj[I].ItemName = szName then begin
      Result:= ItemObj[I].GetDataValue(Path);
      Exit;
    end;
end;

function TEnterFormNode.GetItem(ItemName: string): TEnterFormNode;
begin
  Result:= GetItemObj(FItems.IndexOf(ItemName));
end;

function TEnterFormNode.GetItemCount: Integer;
begin
  Result:= FItems.Count;
end;

function TEnterFormNode.GetItemKey(Index: Integer): string;
begin
  Result:= FItems[Index];
end;

function TEnterFormNode.GetItemObj(Index: Integer): TEnterFormNode;
begin
  Result:= TEnterFormNode(FItems.Objects[Index]);
end;

(* XML *)

function TEnterFormNode.GetXml: string;
var
  i: Integer;
  AttrStr, itemValue, tagName: string;
begin
  AttrStr:= '';
  if NameSpace = '' then tagName:= '段落:' + ItemName
  else tagName:= NameSpace + ':' + ItemName;
  for i:= 0 to FAttributes.Count - 1 do
    AttrStr:= AttrStr + ' ' + FAttributes.Names[i] + '="' + FAttributes.ValueFromIndex[i] + '"';
  if Assigned(NodeObj) then itemValue:= NodeObj.Value else itemValue:= '';
  if(itemValue = '')and(ItemCount = 0)then
    Result:= '<' + tagName + AttrStr + ' />'
  else begin
    Result:= '<' + tagName + AttrStr + '>';
    for i:= 0 to FItems.Count - 1 do
      Result:= Result + #13#10 + TEnterFormNode(FItems.Objects[i]).XML;
    Result:= Result + itemValue + '</' + tagName + '>';
  end;
end;

procedure TEnterFormNode.GetXmlNode(const ANode: IXMLNode);
var
  I: Integer;
  child: IXMLNode;
begin
  ANode.ReadOnly:= False;
  for I := 0 to FAttributes.Count - 1 do
    ANode.SetAttribute(FAttributes.Names[I], FAttributes.ValueFromIndex[I]);
  for I := 0 to FItems.Count - 1 do
    with ItemObj[I] do begin
      child:= ANode.OwnerDocument.CreateElement(ItemName, NameSpace);
      GetXmlNode(child);
      ANode.ChildNodes.Add(child);
    end;
end;

function TEnterFormNode.ReadXml(P: PChar): PChar;
var
  IsName, IsValue, IsAttr, IsEnd: Boolean;
  N, S: string;
  newChild: TEnterFormNode;

  function GetQuoteValue(): string;
  var
    ch: Char;
  begin
    ch:= P[0];
    Inc(P);
    Result:= '';
    repeat
      Result:= Result + P[0];
      Inc(P);
    until(P = nil)or(P[0] = ch);
  end;

  procedure SaveAttr(AttrName, AttrValue: string);
  begin
    AddAttribute(AttrName, AttrValue);
    S:= '';
    IsAttr:= False;
  end;

  procedure SaveName;
  var
    Idx: Integer;
  begin
    Idx:= Pos(':', S);
    if Idx = 0 then begin
      FItemName:= S;
      FNameSpace:= '段落';
    end else begin
      FItemName:= Copy(S, Idx + 1, MaxInt);
      FNameSpace:= Copy(S, 1, Idx - 1);
    end;
    S:= ''; IsName:= False;
  end;
begin
  Clear;
  while(P[0] <> '<')and(P <> nil)do Inc(P);
  Inc(P);
  IsName:= True;
  IsValue:= False;
  IsEnd:= False;
  IsAttr:= False;
  while P <> nil do begin
    case P[0] of
    '<': begin
        if IsValue then begin FNodeValue:= S; IsValue:= False; S:= ''; end;
        if P[1] <> '/' then begin
          newChild:= TEnterFormNode.Create;
          P:= newChild.ReadXml(P);
          FItems.AddObject(newChild.ItemName, newChild);
          Continue;
        end;
      end;
    ' ': if IsName then SaveName
      else if IsAttr then SaveAttr(N, S);
    '=': begin
        N:= S; S:= ''; IsAttr:= True;
      end;
    #13, #10, #9: begin Inc(P); Continue; end;
    '''', '"': S:= S + GetQuoteValue;
    '/': if P[1] = '/' then S:= S + '/'
      else IsEnd:= True;
    '>': begin
        if IsName then SaveName;
        if IsAttr then SaveAttr(N, S);
        if IsEnd then begin Inc(P); Result:= P; Exit; end
        else IsValue:= True;
        S:= '';
      end;
    else S:= S + P[0];
    end;
    Inc(P);
  end;
  Result:= P;
end;

procedure TEnterFormNode.SetXml(const Value: string);
begin
  ReadXml(PChar(Value));
end;

procedure TEnterFormNode.SetXmlNode(const Value: IXMLNode);
var
  I: Integer;
  child: TEnterFormNode;
begin
  ItemName:= Value.NodeName;
  NameSpace:= Value.NamespaceURI;
  FAttributes.Clear;
  for I := 0 to Value.AttributeNodes.Count - 1 do
    AddAttribute(Value.AttributeNodes[I].NodeName, Value.AttributeNodes[I].NodeValue);
  for I := 0 to Value.ChildNodes.Count - 1 do begin
    child:= TEnterFormNode.Create;
    child.SetXmlNode(Value.ChildNodes[I]);
    FItems.AddObject(child.ItemName, child);
  end;
end;

{
****************************** TEnterForm_Section ******************************
}
destructor TEnterForm_Section.Destroy;
begin
  if Assigned(lbTitle) then lbTitle.Free;
  inherited;
end;

procedure TEnterForm_Section.Display;
var
  I, alignment: Integer;
begin
  AutoSize:= True;
  alignment:= Node.AttrIntValue['Alignment'];
  if Node.Attribute['Title'] <> '' then begin
    lbTitle:= TLabel.Create(Self);
    lbTitle.Caption:= Node.Attribute['Title'];
    lbTitle.Parent:= Self;
    lbTitle.Color:= NextColor();
    lbTitle.Transparent:= False;
    if alignment = 0 then begin
      lbTitle.Align:= alTop;
      lbTitle.Top:= MaxInt;
    end else begin
      lbTitle.Align:= alLeft;
      lbTitle.Left:= MaxInt;
      lbTitle.Width:= alignment;
    end;
  end;
  for I := 0 to Node.ItemCount - 1 do begin
    Node.ItemObj[I].Display(Self);
    if alignment = 0 then begin
      Node.ItemObj[I].FNodeObj.Align:= alTop;
      Node.ItemObj[I].FNodeObj.Top:= MaxInt;
    end else begin
      Node.ItemObj[I].FNodeObj.Align:= alLeft;
      Node.ItemObj[I].FNodeObj.Left:= MaxInt;
    end;
  end;
end;

function TEnterForm_Section.GetValue: string;
begin

end;

procedure TEnterForm_Section.SetValue(const Value: string);
begin

end;

{
******************************* TEnterForm_Date ********************************
}
destructor TEnterForm_Date.Destroy;
begin
  if Assigned(lbTitle) then lbTitle.Free;
  if Assigned(eEdit) then eEdit.Free;
  inherited;
end;

procedure TEnterForm_Date.Display;
var
  S: string;
  A: Integer;
begin
  S:= Node.Attribute['Title'];
  if S <> '' then begin
    lbTitle:= TLabel.Create(nil);
    lbTitle.Parent:= Self;
    lbTitle.Caption:= S;
    lbTitle.Layout:= TTextLayout.tlCenter;
    A:= Node.AttrIntValue['Alignment'];
    if A = 0 then begin
      lbTitle.Align:= alTop;
      Height:= 23 + 15;
    end else begin
      lbTitle.Align:= alLeft;
      lbTitle.Width:= A;
      Height:= 23;
    end;
  end else Height:= 23;
  eEdit:= TDateTimePicker.Create(nil);
  eEdit.Kind:= dtkDate;
  eEdit.Parent:= Self;
  eEdit.Align:= alClient;
end;

function TEnterForm_Date.GetValue: string;
begin
  if Assigned(eEdit) then Result:= DateToStr(eEdit.Date) else Result:= '';;
end;

procedure TEnterForm_Date.SetValue(const Value: string);
begin
  if Assigned(eEdit)and(Value <> '')then eEdit.Date:= StrToDate(Value);
end;

{
******************************* TEnterForm_Text ********************************
}
destructor TEnterForm_Text.Destroy;
begin
  if Assigned(lbTitle) then lbTitle.Free;
  if Assigned(eEdit) then eEdit.Free;
  inherited;
end;

procedure TEnterForm_Text.Display;
var
  S: string;
  A: Integer;
begin
  S:= Node.Attribute['Title'];
  if S <> '' then begin
    lbTitle:= TLabel.Create(nil);
    lbTitle.Parent:= Self;
    lbTitle.Caption:= S;
    lbTitle.Layout:= TTextLayout.tlCenter;
    A:= Node.AttrIntValue['Alignment'];
    if A = 0 then begin
      lbTitle.Align:= alTop;
      Height:= 23 + 15;
    end else begin
      lbTitle.Align:= alLeft;
      lbTitle.Width:= A;
      Height:= 23;
    end;
  end else Height:= 23;
  eEdit:= TEdit.Create(nil);
  eEdit.Parent:= Self;
  eEdit.Align:= alClient;
end;

function TEnterForm_Text.GetValue: string;
begin
  Result:= eEdit.Text;
end;

procedure TEnterForm_Text.SetValue(const Value: string);
begin
  eEdit.Text:= Value;
end;

{
******************************* TEnterForm_Zone ********************************
}
destructor TEnterForm_Zone.Destroy;
begin
  if Assigned(lbTitle) then lbTitle.Free;
  if Assigned(eState) then eState.Free;
  if Assigned(eProvince) then eProvince.Free;
  if Assigned(eCity) then eCity.Free;
  if Assigned(eCounty) then eCounty.Free;
  inherited;
end;

procedure TEnterForm_Zone.Display;
var
  S: string;
  A: Integer;
  procedure SetAlign(AComm: TControl);
  begin
    AComm.Parent:= Self;
    if AutoSize then begin
      AComm.Align:= alTop;
      AComm.Top:= MaxInt;
    end else begin
      AComm.Align:= alLeft;
      AComm.Left:= MaxInt;
    end;
  end;
begin
  AutoSize:= Node.AttrIntValue['Alignment'] = 0;
  if not AutoSize then Height:= 23;
  S:= Node.Attribute['Title'];
  if S <> '' then begin
    lbTitle:= TLabel.Create(nil);
    SetAlign(lbTitle);
    lbTitle.Caption:= S;
    if not AutoSize then lbTitle.Width:= Node.AttrIntValue['Alignment'];
  end;
  S:= Node.Attribute['Foreign'];              //---State
  if S <> '' then begin
    eState:= TComboBox.Create(nil);
    SetAlign(eState);
    eState.Items.Delimiter:= ',';
    eState.Items.DelimitedText:= S;
    eState.Items.Insert(0, '中国');
    eState.ItemIndex:= 0;
    eState.OnChange:= EventOfState;
  end;
  eProvince:= TComboBox.Create(nil);           //---Province
  SetAlign(eProvince);
  eProvince.OnChange:= EventOfProvince;
  EventOfState(nil);
  A:= Node.AttrIntValue['Type'];
  if A > 0 then begin
    eCity:= TComboBox.Create(nil);             //---City
    SetAlign(eCity);
    eCity.OnChange:= EventOfCity;
  end;
  if A > 1 then begin
    eCounty:= TComboBox.Create(nil);           //---County
    SetAlign(eCounty);
  end;
end;

procedure TEnterForm_Zone.EventOfCity(Sender: TObject);
begin
  if Assigned(eCounty)and eCounty.Enabled then
    with TADOQuery.Create(nil) do try
      Connection:= FPlatformDB.ADOConn;
      SQL.Text:= 'SELECT ID,ItemName FROM SYS_Zone WHERE PID=' +
        IntToStr(Integer(eCity.Items.Objects[eCity.ItemIndex]));
      Open;
      eCounty.Clear;
      while not EOF do begin
        eCounty.Items.AddObject(Fields[1].AsString, TObject(Fields[0].AsInteger));
        Next;
      end;
    finally
      Free;
    end;
end;

procedure TEnterForm_Zone.EventOfProvince(Sender: TObject);
begin
  if Assigned(eCity)and eCity.Enabled then
    with TADOQuery.Create(nil) do try
      Connection:= FPlatformDB.ADOConn;
      SQL.Text:= 'SELECT ID,ItemName FROM SYS_Zone WHERE PID=' +
        IntToStr(Integer(eProvince.Items.Objects[eProvince.ItemIndex]));
      Open;
      eCity.Clear;
      while not EOF do begin
        eCity.Items.AddObject(Fields[1].AsString, TObject(Fields[0].AsInteger));
        Next;
      end;
    finally
      Free;
    end
end;

procedure TEnterForm_Zone.EventOfState(Sender: TObject);
begin
  eProvince.Enabled:= (not Assigned(eState))or(eState.Text = '中国');
  if Assigned(eCity) then eCity.Enabled:= eProvince.Enabled;
  if Assigned(eCounty) then eCounty.Enabled:= eProvince.Enabled;
  if eProvince.Enabled then
    with TADOQuery.Create(nil) do try
      Connection:= FPlatformDB.ADOConn;
      SQL.Text:= 'SELECT ID,ItemName FROM SYS_Zone WHERE PID=0';
      Open;
      eProvince.Clear;
      while not EOF do begin
        eProvince.Items.AddObject(Fields[1].AsString, TObject(Fields[0].AsInteger));
        Next;
      end;
    finally
      Free;
    end;
end;

function TEnterForm_Zone.GetValue: string;
begin
  Result:= '';
  if Assigned(eState) then Result:= eState.Text;
  if eProvince.Enabled then
    Result:= Result + eProvince.Text + eCity.Text + eCounty.Text;
end;

procedure TEnterForm_Zone.SetValue(const Value: string);
var
  ItemValue: string;

  function SetItemValue(cb: TComboBox): boolean;
  var
    I: Integer;
  begin
    for I := 0 to cb.Items.Count - 1 do
      if cb.Items[i] = Copy(ItemValue, 1, Length(cb.Items[i])) then begin
        cb.ItemIndex:= I;
        cb.OnChange(cb);
        ItemValue:= Copy(ItemValue, Length(cb.Items[i]) + 1, MaxInt);
        Exit;
      end;
  end;
begin
  ItemValue:= Value;
  if Assigned(eState) then begin
    if Copy(ItemValue, 1, 2) = '中国' then begin
      eState.ItemIndex:= 0;
      ItemValue:= Copy(ItemValue, 3, MaxInt);
    end else eState.Text:= ItemValue;
  end;
  eProvince.Enabled:= (not Assigned(eState))or(eState.Text = '中国');
  if eProvince.Enabled then if not SetItemValue(eProvince)then Exit;
  if Assigned(eCity) then begin
    eCity.Enabled:= eProvince.Enabled;
    if eCity.Enabled then if not SetItemValue(eCity)then Exit;
  end;
  if Assigned(eCounty) then begin
    eCounty.Enabled:= eProvince.Enabled;
    if eCounty.Enabled then SetItemValue(eCounty);
  end;
end;

{
******************************* TEnterForm_Time ********************************
}
destructor TEnterForm_Time.Destroy;
begin
  if Assigned(lbTitle) then lbTitle.Free;
  if Assigned(eEdit) then eEdit.Free;
  inherited;
end;

procedure TEnterForm_Time.Display;
var
  S: string;
  A: Integer;
begin
  S:= Node.Attribute['Title'];
  if S <> '' then begin
    lbTitle:= TLabel.Create(nil);
    lbTitle.Parent:= Self;
    lbTitle.Caption:= S;
    lbTitle.Layout:= TTextLayout.tlCenter;
    A:= Node.AttrIntValue['Alignment'];
    if A = 0 then begin
      lbTitle.Align:= alTop;
      Height:= 23 + 15;
    end else begin
      lbTitle.Align:= alLeft;
      lbTitle.Width:= A;
      Height:= 23;
    end;
  end else Height:= 23;
  eEdit:= TDateTimePicker.Create(nil);
  eEdit.Parent:= Self;
  eEdit.Kind:= dtkTime;
  eEdit.Align:= alClient;
end;

function TEnterForm_Time.GetValue: string;
begin
  if Assigned(eEdit) then Result:= TimeToStr(eEdit.Date) else Result:= '';;
end;

procedure TEnterForm_Time.SetValue(const Value: string);
begin
  if Assigned(eEdit)and(Value <> '')then eEdit.Date:= StrToTime(Value);
end;

{
***************************** TEnterForm_Selection *****************************
}
destructor TEnterForm_Selection.Destroy;
begin
  if Assigned(lbTitle) then lbTitle.Free;
  if Assigned(eEdit) then eEdit.Free;
  inherited;
end;

procedure TEnterForm_Selection.Display;
var
  S: string;
  A: Integer;
begin
  S:= Node.Attribute['Title'];
  if S <> '' then begin
    lbTitle:= TLabel.Create(nil);
    lbTitle.Parent:= Self;
    lbTitle.Caption:= S;
    lbTitle.Layout:= TTextLayout.tlCenter;
    A:= Node.AttrIntValue['Alignment'];
    if A = 0 then begin
      lbTitle.Align:= alTop;
      Height:= 23 + 15;
    end else begin
      lbTitle.Align:= alLeft;
      lbTitle.Width:= A;
      Height:= 23;
    end;
  end else Height:= 23;
  eEdit:= TComboBox.Create(nil);
  eEdit.Parent:= Self;
  eEdit.Align:= alClient;
  eEdit.Items.Delimiter:= ',';
  eEdit.Items.DelimitedText:= Node.Attribute['Items'];
end;

function TEnterForm_Selection.GetValue: string;
begin
  if Node.Attribute['Type'] = '输出序号' then
    Result:= IntToStr(eEdit.ItemIndex)
  else Result:= eEdit.Text;
end;

procedure TEnterForm_Selection.SetValue(const Value: string);
begin
  if Node.Attribute['Type'] = '输出序号' then
    eEdit.ItemIndex:= StrToInt(Value)
  else if Node.Attribute['Type'] = '输出文字' then
    eEdit.ItemIndex:= eEdit.Items.IndexOf(Value)
  else eEdit.Text:= Value;
end;

{
******************************* TEnterForm_Memo ********************************
}
destructor TEnterForm_Memo.Destroy;
begin
  if Assigned(lbTitle) then lbTitle.Free;
  if Assigned(eEdit) then eEdit.Free;
  inherited;
end;

procedure TEnterForm_Memo.Display;
var
  S: string;
  A: Integer;
begin
  S:= Node.Attribute['Title'];
  if S <> '' then begin
    lbTitle:= TLabel.Create(nil);
    lbTitle.Parent:= Self;
    lbTitle.Caption:= S;
    lbTitle.Layout:= TTextLayout.tlCenter;
    A:= Node.AttrIntValue['Alignment'];
    if A = 0 then begin
      lbTitle.Align:= alTop;
      Height:= 123 + 15;
    end else begin
      lbTitle.Align:= alLeft;
      lbTitle.Width:= A;
      Height:= 123;
    end;
  end else Height:= 123;
  eEdit:= TMemo.Create(nil);
  eEdit.Parent:= Self;
  eEdit.Align:= alClient;
end;

function TEnterForm_Memo.GetValue: string;
begin
  if Node.Attribute['Type'] = '单行输出' then begin
    eEdit.Lines.Delimiter:= ',';
    Result:= eEdit.Lines.DelimitedText;
  end else Result:= eEdit.Text;
end;

procedure TEnterForm_Memo.SetValue(const Value: string);
begin
  if Node.Attribute['Type'] = '单行输出' then begin
    eEdit.Lines.Delimiter:= ',';
    eEdit.Lines.DelimitedText:= Value;
  end else eEdit.Lines.Text:= Value;
end;

{
******************************** TEnterForm_Int ********************************
}
destructor TEnterForm_Int.Destroy;
begin
  if Assigned(eEdit) then eEdit.Free;
  inherited;
end;

procedure TEnterForm_Int.Display;
var
  S: string;
  A: Integer;
begin
  S:= Node.Attribute['Title'];
  if S <> '' then begin
    lbTitle:= TLabel.Create(nil);
    lbTitle.Parent:= Self;
    lbTitle.Caption:= S;
    lbTitle.Layout:= TTextLayout.tlCenter;
    A:= Node.AttrIntValue['Alignment'];
    if A = 0 then begin
      lbTitle.Align:= alTop;
      Height:= 23 + 15;
    end else begin
      lbTitle.Align:= alLeft;
      lbTitle.Width:= A;
      Height:= 23;
    end;
  end else Height:= 23;
  eEdit:= TSpinEdit.Create(nil);
  eEdit.Parent:= Self;
  eEdit.Align:= alClient;
end;

function TEnterForm_Int.GetValue: string;
begin
  Result:= IntToStr(eEdit.Value);
end;

procedure TEnterForm_Int.SetValue(const Value: string);
begin
  if Assigned(eEdit)and(Value <> '')then
    eEdit.Value:= StrToInt(Value);
end;

{
******************************* TEnterForm_Guid ********************************
}
destructor TEnterForm_Guid.Destroy;
begin
  if Assigned(lbTitle) then lbTitle.Free;
  if Assigned(eEdit) then eEdit.Free;
  inherited;
end;

procedure TEnterForm_Guid.Display;
var
  S: string;
  A: Integer;
begin
  S:= Node.Attribute['Title'];
  if S <> '' then begin
    lbTitle:= TLabel.Create(nil);
    lbTitle.Parent:= Self;
    lbTitle.Caption:= S;
    A:= Node.AttrIntValue['Alignment'];
    if A = 0 then begin
      lbTitle.Align:= alTop;
      Height:= 23 + 15;
    end else begin
      lbTitle.Align:= alLeft;
      lbTitle.Width:= A;
      Height:= 23;
    end;
  end else Height:= 23;
  eEdit:= TButtonedEdit.Create(nil);
  eEdit.Images:= FPlatformDB.ImgSmall;
  eEdit.RightButton.ImageIndex:= 31;
  eEdit.RightButton.Visible:= True;
  eEdit.OnRightButtonClick:= EventOfClick;
  eEdit.Parent:= Self;
  eEdit.Align:= alClient;
end;

procedure TEnterForm_Guid.EventOfClick(Sender: TObject);
var
  G: TGUID;
begin
  CreateGUID(G);
  eEdit.Text:= GuidToStr(G);
end;

function TEnterForm_Guid.GetValue: string;
begin
  Result:= eEdit.Text;
end;

procedure TEnterForm_Guid.SetValue(const Value: string);
begin
  eEdit.Text:= Value;
end;

{
******************************* TEnterForm_Float *******************************
}
destructor TEnterForm_Float.Destroy;
begin
  if Assigned(lbTitle) then lbTitle.Free;
  if Assigned(eEdit) then eEdit.Free;
  inherited;
end;

procedure TEnterForm_Float.Display;
var
  S: string;
  A: Integer;
begin
  S:= Node.Attribute['Title'];
  if S <> '' then begin
    lbTitle:= TLabel.Create(nil);
    lbTitle.Parent:= Self;
    lbTitle.Caption:= S;
    A:= Node.AttrIntValue['Alignment'];
    if A = 0 then begin
      lbTitle.Align:= alTop;
      Height:= 23 + 15;
    end else begin
      lbTitle.Align:= alLeft;
      lbTitle.Width:= A;
      Height:= 23;
    end;
  end else Height:= 23;
  eEdit:= TEdit.Create(nil);
  eEdit.Parent:= Self;
  eEdit.Align:= alClient;
end;

function TEnterForm_Float.GetValue: string;
begin
  if eEdit.Text = '' then Result:= '0' else Result:= eEdit.Text;
end;

procedure TEnterForm_Float.SetValue(const Value: string);
begin
  if not Assigned(eEdit)then Exit;
  if(Value <> '')then eEdit.Text:= Value else eEdit.Text:= '0';
end;

{
******************************** TEnterForm_Dim ********************************
}
destructor TEnterForm_Dim.Destroy;
begin
  if Assigned(lbTitle) then lbTitle.Free;
  if Assigned(eEdit) then eEdit.Free;
  inherited;
end;

procedure TEnterForm_Dim.Display;
var
  S: string;
  A: Integer;
begin
  S:= Node.Attribute['Title'];
  if S <> '' then begin
    lbTitle:= TLabel.Create(nil);
    lbTitle.Parent:= Self;
    lbTitle.Caption:= S;
    A:= Node.AttrIntValue['Alignment'];
    if A = 0 then begin
      lbTitle.Align:= alTop;
      Height:= 123 + 15;
    end else begin
      lbTitle.Align:= alLeft;
      lbTitle.Width:= A;
      Height:= 123;
    end;
  end else Height:= 123;
  eEdit:= TTreeView.Create(nil);
  eEdit.Parent:= Self;
  eEdit.Align:= alClient;
end;

function TEnterForm_Dim.GetValue: string;
begin
  if Assigned(eEdit)and Assigned(eEdit.Selected) then
    Result:= eEdit.Selected.Text;
end;

procedure TEnterForm_Dim.SetValue(const Value: string);
var
  I: Integer;
begin
  for I := 0 to eEdit.Items.Count - 1 do
    if eEdit.Items[I].Text = Value then begin
      eEdit.Items[I].Selected:= True;
      Exit;
    end;
end;

{
****************************** TEnterForm_Choice *******************************
}
destructor TEnterForm_Choice.Destroy;
begin
  if Assigned(lbTitle) then lbTitle.Free;
  if Assigned(eRG) then eRG.Free;
  inherited;
end;

procedure TEnterForm_Choice.Display;
var
  S: string;
  A: Integer;
begin
  Self.OnResize:= EventOfResize;
  S:= Node.Attribute['Title'];
//  if S <> '' then begin
//    lbTitle:= TLabel.Create(nil);
//    lbTitle.Parent:= Self;
//    lbTitle.Caption:= S;
//    A:= Node.AttrIntValue['Alignment'];
//    if A = 0 then begin
//      lbTitle.Align:= alTop;
//      Height:= 123 + 15;
//    end else begin
//      lbTitle.Align:= alLeft;
//      lbTitle.Width:= A;
//      Height:= 123;
//    end;
//  end else Height:= 123;
  eRG:= TRadioGroup.Create(nil);
  eRG.Parent:= Self;
  eRG.Align:= alClient;
  eRG.Caption:= S;
  eRG.Items.Delimiter:= ',';
  eRG.Items.DelimitedText:= Node.Attribute['Items'];
  if Node.Attribute['Type'] = '有其他项' then begin
    eRG.Items.Add('其他');
    eRG.OnClick:= EventOfRGSelect;
  end;
  for A := 0 to eRG.Items.Count - 1 do
    if FMaxLength < Length(eRG.Items[A]) then
      FMaxLength := Length(eRG.Items[A]);
end;

procedure TEnterForm_Choice.EventOfResize(Sender: TObject);
var
  fw: Integer;
begin
  if not Assigned(eRG) then Exit;
  fw:= Self.Canvas.TextWidth('国') * (FMaxLength + 1) + 20;
  eRG.Columns:= Round(eRG.Width / fw);
  Height:= ((eRG.Items.Count + eRG.Columns - 1) div eRG.Columns) * 20 + 20;
end;

procedure TEnterForm_Choice.EventOfRGSelect(Sender: TObject);
begin
  if eRG.ItemIndex = eRG.Items.Count - 1 then begin
    if Assigned(eEdit) then Exit
    else begin
      eEdit:= TEdit.Create(nil);
      eEdit.Parent:= Self;
      eEdit.Align:= alBottom;
    end;
  end else if Assigned(eEdit) then FreeAndNil(eEdit);
end;

function TEnterForm_Choice.GetValue: string;
begin
  if Assigned(eEdit) then Result:= eEdit.Text
  else if Node.Attribute['Type'] = '输出序号' then
    Result:= IntToStr(eRG.ItemIndex)
  else Result:= eRG.Items[eRG.ItemIndex];
end;

procedure TEnterForm_Choice.SetValue(const Value: string);
var
  Idx: Integer;
begin
  if Node.Attribute['Type'] = '输出序号' then begin
    if TryStrToInt(Value, Idx) then
      eRG.ItemIndex:= Idx;
  end else if Node.Attribute['Type'] = '输出文字' then
    eRG.ItemIndex:= eRG.Items.IndexOf(Value)
  else begin
    eRG.ItemIndex:= eRG.Items.IndexOf(Value);
    if eRG.ItemIndex = -1 then begin
      eRG.ItemIndex:= eRG.Items.Count - 1;
      eEdit.Text:= Value;
    end;
  end;
end;

{
***************************** TEnterForm_CheckList *****************************
}
destructor TEnterForm_CheckList.Destroy;
begin
  if Assigned(lbTitle) then lbTitle.Free;
  if Assigned(eEdit) then eEdit.Free;
  inherited;
end;

procedure TEnterForm_CheckList.Display;
var
  S: string;
  A: Integer;
begin
  Self.OnResize:= EventOfResize;
  S:= Node.Attribute['Title'];
  if S <> '' then begin
    lbTitle:= TLabel.Create(nil);
    lbTitle.Parent:= Self;
    lbTitle.Caption:= S;
    A:= Node.AttrIntValue['Alignment'];
    if A = 0 then begin
      lbTitle.Align:= alTop;
      Height:= 123 + 15;
    end else begin
      lbTitle.Align:= alLeft;
      lbTitle.Width:= A;
      Height:= 123;
    end;
  end else Height:= 123;
  eEdit:= TCheckListBox.Create(nil);
  eEdit.Parent:= Self;
  eEdit.Align:= alClient;
  eEdit.Items.Delimiter:= ',';
  eEdit.Items.DelimitedText:= Node.Attribute['Items'];
  FMaxLength:= 0;
  for A := 0 to eEdit.Items.Count - 1 do
    if FMaxLength < Length(eEdit.Items[A]) then
      FMaxLength := Length(eEdit.Items[A]);
end;

procedure TEnterForm_CheckList.EventOfResize(Sender: TObject);
var
  fw: Integer;
begin
  if not Assigned(eEdit) then Exit;
  fw:= Self.Canvas.TextWidth('国') * (FMaxLength + 1) + 20;
  eEdit.Columns:= Round(eEdit.Width / fw);
  Height:= ((eEdit.Items.Count + eEdit.Columns - 1) div eEdit.Columns) * 20;
end;

function TEnterForm_CheckList.GetValue: string;
var
  I: Integer;
  IsIndexOut: Boolean;
begin
  Result:= ''; IsIndexOut:= Node.Attribute['Type'] = '值序列';
  for I := 0 to eEdit.Count - 1 do
    if eEdit.Checked[I] then begin
      if IsIndexOut then Result:= Result + ',' + IntToStr(I)
      else Result:= Result + ',' + eEdit.Items[I];
    end;
  if Result <> '' then Delete(Result, 1, 1);
end;

procedure TEnterForm_CheckList.SetValue(const Value: string);
var
  I: Integer;
  szList: TStringList;
  IsIndexOut: Boolean;
begin
  IsIndexOut:= Node.Attribute['Type'] = '值序列';
  szList:= TStringList.Create;
  szList.Delimiter:= ',';
  szList.DelimitedText:= Value;
  eEdit.CheckAll(cbUnchecked);
  for I := 0 to szList.Count - 1 do
    if IsIndexOut then eEdit.Checked[StrToInt(szList[I])]:= True
    else eEdit.Checked[eEdit.Items.IndexOf(szList[I])]:= True;
end;

{
******************************* TEnterForm_Bool ********************************
}
destructor TEnterForm_Bool.Destroy;
begin
  if Assigned(eEdit) then eEdit.Free;
  inherited;
end;

procedure TEnterForm_Bool.Display;
begin
  eEdit:= TCheckBox.Create(nil);
  eEdit.Caption:= Node.Attribute['Title'];
  eEdit.Parent:= Self;
  eEdit.Align:= alLeft;
  Height:= 15;
end;

function TEnterForm_Bool.GetValue: string;
begin
  if Assigned(eEdit) then
    Result:= BoolToStr(eEdit.Checked, true)
  else Result:= 'false';
end;

procedure TEnterForm_Bool.SetValue(const Value: string);
begin
  if Assigned(eEdit) then
    eEdit.Checked:= (Value <> '')and((Value[1] = 't')or(Value[1] = 'T'));
end;

{ TFrameEnterForm }

procedure TFrameEnterForm.Clear;
var
  I: Integer;
begin
  for I := 0 to Nodes.Count - 1 do
    Nodes.Objects[I].Free;
  Nodes.Clear;
end;

constructor TFrameEnterForm.Create(AOwner: TComponent);
begin
  inherited;
  Nodes:= TStringList.Create;
end;

destructor TFrameEnterForm.Destroy;
begin
  Nodes.Free;
  inherited;
end;

function TFrameEnterForm.GetDataValue(Path: string): string;
var
  I, Idx: Integer;
  szName: string;
begin
  Idx:= Pos('.', Path);
  if Idx < 1 then szName:= Path else begin
    szName:= Copy(Path, 1, Idx - 1);
    Path:= Copy(Path, Idx + 1, MaxInt);
  end;
  for I := 0 to Nodes.Count - 1 do
    if ItemObj[I].ItemName = szName then begin
      Result:= ItemObj[I].GetDataValue(Path);
      Exit;
    end;
end;

function TFrameEnterForm.GetItem(ItemName: string): TEnterFormNode;
var
  Idx: Integer;
begin
  Idx:= Nodes.IndexOf(ItemName);
  if Idx < 0 then Result:= nil
  else Result:= GetItemObj(Idx);
end;

function TFrameEnterForm.GetItemCount: Integer;
begin
  Result:= Nodes.Count;
end;

function TFrameEnterForm.GetItemKey(Index: Integer): string;
begin
  Result:= Nodes[Index];
end;

function TFrameEnterForm.GetItemObj(Index: Integer): TEnterFormNode;
begin
  Result:= TEnterFormNode(Nodes.Objects[Index]);
end;

function TFrameEnterForm.GetXml: string;
var
  I: Integer;
  node: IXMLNode;
  XMLDoc: TXMLDocument;
begin
  XMLDoc:= TXMLDocument.Create(Self);
  XMLDoc.Active:= true;
  for I := 0 to Nodes.Count - 1 do
    with TEnterFormNode(Nodes.Objects[I]) do begin
      node:= XMLDoc.AddChild(ItemName, NameSpace);
      GetXmlNode(node);
    end;
  Result:= XMLDoc.XML.Text;
  XMLDoc.Free;
end;

procedure TFrameEnterForm.SetXml(const Value: string);
var
  I: Integer;
  ANode: TEnterFormNode;
  XMLDoc: TXMLDocument;
  root: IXMLNode;
begin
  Clear;
  XMLDoc:= TXMLDocument.Create(Self);
  XMLDoc.LoadFromXML(Value);
  root:= XMLDoc.DocumentElement;
  if Assigned(root) then
    for I := 0 to root.ChildNodes.Count - 1 do begin
      ANode:= TEnterFormNode.Create;
      ANode.SetXmlNode(root.ChildNodes[I]);
      Nodes.AddObject(ANode.ItemName, ANode);
      ANode.Display(ScrollBox1);
      ANode.NodeObj.Align:= alTop;
      ANode.NodeObj.Top:= MaxInt;
    end;
  XMLDoc.Free;
end;

initialization

EnterFormComponentList := TStringList.Create;
RegistEnterFormComponent('段落', TEnterForm_Section,
  '<CanExpand xmlns="布尔" Title="支持缩放">true</CanExpand>');
RegistEnterFormComponent('单行文本', TEnterForm_Text,
  '<Mask xmlns="单行文本" Title="掩码" /><Max xmlns="整数" Title="最大长度" /><Password xmlns="布尔" Title="支持密码" />');
RegistEnterFormComponent('浮点', TEnterForm_Float,
  '<Max xmlns="浮点" Title="最大值" /><Min xmlns="浮点" Title="最小值" /><Frac xmlns="整数" Title="小数点位数" />');
RegistEnterFormComponent('整数', TEnterForm_Int,
  '<Max xmlns="整数" Title="最大值" /><Min xmlns="整数" Title="最小值" /><Inc xmlns="整数" Title="步长" />');
RegistEnterFormComponent('日期', TEnterForm_Date, '');
RegistEnterFormComponent('时间', TEnterForm_Time, '');
RegistEnterFormComponent('GUID', TEnterForm_Guid, '');
RegistEnterFormComponent('多行文本', TEnterForm_Memo,
  '<Type xmlns="单选" Title="类型" type="输出文字" Items="多行输出,单行输出">多行输出</Type>' +
  '<Max xmlns="整数" Title="最多文字" />');
RegistEnterFormComponent('选择', TEnterForm_Selection,
  '<Type xmlns="单选" Title="类型" type="输出文字" Items="有其他项,输出序号,输出文字">输出序号</Type>' +
  '<Items xmlns="多行文本" Title="可选内容" type="单行输出" Out="多行输出" />');
RegistEnterFormComponent('单选', TEnterForm_Choice,
  '<Type xmlns="单选" Title="类型" type="输出文字" Items="有其他项,输出序号,输出文字">输出序号</Type>' +
  '<Items xmlns="多行文本" Title="可选内容" type="单行输出" Out="多行输出" />');
RegistEnterFormComponent('布尔', TEnterForm_Bool, '');
RegistEnterFormComponent('复选', TEnterForm_CheckList,
  '<Type xmlns="单选" Title="类型" type="输出文字" Items="值序列,内容序列">内容序列</Type>' +
  '<Items xmlns="多行文本" Title="可选内容" type="单行输出" Out="多行输出" />');
RegistEnterFormComponent('维度', TEnterForm_Dim,
  '<Dim xmlns="单行文本" Title="名称" />' +
  '<Level xmlns="整数" Title="支持层级" />');
RegistEnterFormComponent('地区', TEnterForm_Zone,
  '<Foreign xmlns="单行文本" Title="国家名称" />' +
  '<Type xmlns="单选" Title="类型" Items="省,市,县">2</Kind>');

finalization

EnterFormComponentList.Free;

end.
