(*******************************************************************************
* UFrameGridBar 一个数据自感应控件，能够显示数据链接的一个数据项目，支持 Excel *
*   的导出和打印，能够手动编辑列表名称，能够调整列宽，列序，能自动保存调整结果 *
********************************************************************************
* TConditionItem 内部类，条件项目                                              *
********************************************************************************
* TFrameGridBar 表格控件                                                       *
*------------------------------------------------------------------------------*
* Hint: 打印时，页面标题内容                                                   *
* HelpKeyword: 本表格栏目信息存储文件名称                                      *
*------------------------------------------------------------------------------*
* 增加显示行颜色控制 Condition:颜色条件, Color:行颜色
* RegConditionColor(Condition: string; Color: TColor)                          *
*    FrameGridBar1.RegConditionColor('ID>15&ID<23', clYellow);                 *
*    FrameGridBar1.RegConditionColor('Variety=管线管', clRed);                 *
*    FrameGridBar1.RegConditionColor('Source=??组批', clRed); 三四两个字是组批 *
*    FrameGridBar1.RegConditionColor('RecordNo%3.1', clBlue); 三行第一行颜色   *
*    FrameGridBar1.RegConditionColor('RecordNo%3.2', clGreen);三行第二行颜色   *
*    FrameGridBar1.RegConditionColor('InNum>OutNum', clRed); 第一个数据>第二个数据
*------------------------------------------------------------------------------*
* RegStatistic(Fieldname, HintText: string; Kind: TEnumStatistic; Condition: string);
*    FrameGridBar1.RegStatistic('StoredNum', '管线管数量', ES_SUM, 'Variety=管线管');  *
*    FrameGridBar1.RegStatistic('StoredNum', '库存数', ES_SUM, '');            *
********************************************************************************
* TFrameGridBarDataSource 表格控件辅助感知                                     *
*------------------------------------------------------------------------------*
*     能够自动根据查询结果，去数据库字典 SYS_DataDictionary 中查找到匹配的转换 *
* 为显示名称，完成中英文的转换。                                               *
*******************************************************************************)
unit UFrameGridBar;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, StdCtrls, ComCtrls, ToolWin, DBGrids, Grids, DB, data.win.ADODB,
  IniFiles, ExtCtrls, Contnrs, RTLConsts, OLE_Excel_TLB, Buttons, Menus, UVonOffice,
  WinApi.ShellApi, UPlatformDB, ValEdit, WinApi.ADOInt, System.ImageList, Vcl.WinXCtrls; // , QuickRpt, QRCtrls;

type
  /// <summary>统计类型</summary>
  /// <param name="ES_SUM">合计</param>
  /// <param name="ES_AVG">平均值</param>
  /// <param name="ES_COUNT">计数</param>
  /// <param name="ES_MAX">最大值</param>
  /// <param name="ES_MIN">最小值</param>
  /// <param name="ES_STDEV">数据的标准差</param>
  /// <param name="ES_STDEVP">总体标准差</param>
  /// <param name="ES_VAR">统计变异数</param>
  /// <param name="ES_VARP">总体变异数</param>
  TEnumStatistic = (ES_SUM, ES_AVG, ES_COUNT, ES_MAX, ES_MIN, ES_STDEV, ES_STDEVP, ES_VAR, ES_VARP);
  TEnumFormat = (ES_Int, ES_Real2, ES_Real3, ES_Real, ES_Currency,ES_n,ES_g);

  /// <summary>条件项目</summary>
  TConditionItem = class
  private
    FItemFieldName: string;
    FCondition: AnsiChar;
    FResultValue: Cardinal;
    FForamtValue: Cardinal;
    FIsAnd: Boolean;
    FConditionValue: string;
    FNext: TConditionItem;
    FValueFormat:Cardinal;
    FLegend: string;
    FTitle: string;
    procedure SetResultValue(const Value: Cardinal);
    procedure SetCondition(const Value: AnsiChar);
    procedure SetConditionValue(const Value: string);
    procedure SetIsAnd(const Value: Boolean);
    procedure SetItemFieldName(const Value: string);
    procedure SetNext(const Value: TConditionItem);
    procedure SetLegend(const Value: string);
    procedure SetTitle(const Value: string);
    procedure SetValueFormat(const Value: Cardinal);
    procedure SetForamtValue(const Value: Cardinal);
  published //(((ID>12&ID<123)|ID=232)&Key=sss)|(key=ddd&v=ds)
    /// <summary>条件字段</summary>
    property ItemFieldName: string read FItemFieldName write SetItemFieldName;
    /// <summary>条件逻辑符，常用逻辑符号等于，大于，小于，不等于</summary>
    property Condition: AnsiChar read FCondition write SetCondition;
    /// <summary>条件值</summary>
    property ConditionValue: string read FConditionValue write SetConditionValue;
    /// <summary>满足条件的结果</summary>
    property ResultValue: Cardinal read FResultValue write SetResultValue;
    /// <summary>后续条件是否是逻辑与关系</summary>
    property IsAnd: Boolean read FIsAnd write SetIsAnd;
    /// <summary>后续条件</summary>
    property Next: TConditionItem read FNext write SetNext;
    /// <summary>提示内容</summary>
    property Legend: string read FLegend write SetLegend;
    /// <summary>标题</summary>
    property Title: string read FTitle write SetTitle;
    /// <summary>显示格式</summary>
    property ValueFormat: Cardinal read FValueFormat write SetValueFormat;
    /// <summary>满足条件的结果</summary>
    property ForamtValue: Cardinal read FForamtValue write SetForamtValue;
  end;

  TFrameGridBarDataSource = class;

  /// <summary>表格控件</summary>
  TFrameGridBar = class(TFrame)
    ImageList1: TImageList;
    GridSource: TDataSource;
    SaveDialog1: TSaveDialog;
    Grid: TDBGrid;
    StatusBar1: TStatusBar;
    gridMenu: TPopupMenu;
    N1: TMenuItem;
    menuExportCsv: TMenuItem;
    menuExportExcel: TMenuItem;
    menuExportXml: TMenuItem;
    N3: TMenuItem;
    menuPrintSetup: TMenuItem;
    menuColumnSetting: TMenuItem;
    menuEdit: TMenuItem;
    menuCondition: TMenuItem;
    menuSelectionOption: TMenuItem;
    menuPrintIO: TMenuItem;
    menuPrintExcel: TMenuItem;
    menuPrintHtml: TMenuItem;
    menuExportHtml: TMenuItem;
    split1: TSplitter;
    plItem: TPanel;
    lstLegend: TColorListBox;
    lstField: TValueListEditor;
    Excel11: TMenuItem;
    procedure GridTitleClick(Column: TColumn);
    procedure GridColumnMoved(Sender: TObject; FromIndex, ToIndex: Integer);
    procedure GridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure StatusBar1Click(Sender: TObject);
    procedure menuExportCsvClick(Sender: TObject);
    procedure menuExportExcelClick(Sender: TObject);
    procedure menuExportXmlClick(Sender: TObject);
    procedure menuExportHtmlClick(Sender: TObject);
    procedure menuPrintIOClick(Sender: TObject);
    procedure menuPrintExcelClick(Sender: TObject);
    procedure menuPrintHtmlClick(Sender: TObject);
    procedure menuPrintSetupClick(Sender: TObject);
    procedure menuColumnSettingClick(Sender: TObject);
    procedure menuSelectionOptionClick(Sender: TObject);
    procedure menuConditionClick(Sender: TObject);
    procedure menuEditClick(Sender: TObject);
    procedure lstLegendClick(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure plItemResize(Sender: TObject);
    procedure SimpleExcelClick(Sender: TObject);
  private
    { Private declarations }
    FHelper: TFrameGridBarDataSource;
    FConditionColors: TList;
    FStatistic: TList;
    FSectionName, FFormGridName: string;
    FColumnToSort: string;
    FOrderType: Integer;
    FHasTitle: boolean;
    FCanDelColumn: boolean;
    FAdoConnection: TADOConnection;
    FAutoColumn: Boolean;
    procedure SetAdoConnection(const Value: TADOConnection);
    procedure SetAutoColumn(const Value: Boolean);
    function GetCellName(Col, Row: Integer): string;
    procedure SetCanDelColumn(const Value: boolean);
    procedure WriteExcel(excelApp: TExcelApplication; filename: string);
    procedure WriteText(filename: string);
    procedure WriteXml(filename: string);
    procedure WriteHtml(filename: string);
    procedure EventOfColumnWidthChanged(sender: TObject);
    function SplitCondition(PCondition: PChar; Item: TConditionItem): PChar;
    function GetFieldValue(itemValue: string): string;
    function CheckCondition(Item: TConditionItem): Boolean;
    function GetConditionFilter(Item: TConditionItem): string;
    procedure DisplayItemPanel(Display: Boolean);
    procedure FilterColumn();
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    /// <summary>手动从文件中加载列标题</summary>
    procedure LoadTitles(FormName: string = '');
    /// <summary>将列标题内容存储到文件中</summary>
    procedure SaveTitles(FormName: string = '');
    /// <summary>注册条件颜色代码</summary>
    /// <param name="Condition">颜色条件</param>
    /// <param name="Color">颜色</param>
    /// <param name="HintText">颜色提示</param>
    procedure RegConditionColor(Condition: string; Color: TColor; HintText: string);
    procedure ClearCondition;
    /// <summary>注册统计字段</summary>
    /// <param name="Fieldname">统计字段</param>
    /// <param name="HintText">提示内容</param>
    /// <param name="Kind">统计类型，COUNT,SUM,AVG,MIN,MAX</param>
    /// <param name="Condition">统计条件</param>
    procedure RegStatistic(Fieldname, HintText: string; Kind: TEnumStatistic; Condition: string);
    procedure RegStatisticFormat(Fieldname, HintText: string; Kind: TEnumStatistic; Condition: string;ValueFormat:TEnumFormat);
    procedure ClearStatistic;
    procedure DisplayValue(col: integer; value: string);
    procedure DisplayFormat(col: integer; value: string;ValueFormat:string);
  published
    property AutoColumn: Boolean read FAutoColumn write SetAutoColumn;
    property CanDelColumn: boolean read FCanDelColumn write SetCanDelColumn;
    property AdoConnection: TADOConnection read FAdoConnection write SetAdoConnection;
  end;

  /// <summary>表格控件辅助感知</summary>
  TFrameGridBarDataSource = class(TDataLink)
  private
    FConn: TADOConnection;
    FGrid: TFrameGridBar;
    FDisibled: Boolean;
    procedure ReStatistic;
    procedure DisplayFields;
  protected
    procedure ActiveChanged; override;
//    procedure BuildAggMap;
    procedure DataSetChanged; override;
//    procedure DataEvent(Event: TDataEvent; Info: Longint); override;
    procedure DataSetScrolled(Distance: Integer); override;
//    procedure FocusControl(Field: TFieldRef); override;
//    procedure EditingChanged; override;
//    procedure LayoutChanged; override;
//    procedure RecordChanged(Field: TField); override;
//    procedure UpdateData; override;
  public
    /// <summary>表格控件辅助感知</summary>
    constructor Create(fmGrid: TFrameGridBar);
    function GetFieldTitle(AFieldName: string): string;
    procedure DisibleGrid;
  end;

implementation

uses UDlgGridColumn, System.win.ComObj, UDlgBrowser;

type
  TMyGrid = class(TCustomDBGrid);

{$R *.dfm}

type TTempDBGrid = class(TDBGrid)
end;

procedure TFrameGridBar.FilterColumn();
var
  i, j, Idx: Integer;
  found: Boolean;
  tmpColumn: TColumn;
begin
  Idx:= 0;
  with TFDlgGridColumn.Create(nil) do try
    CheckListBox1.Items.Clear;
    for I := 0 to Grid.Columns.Count - 1 do begin
      Idx:= CheckListBox1.Items.AddObject(Grid.Columns[I].Title.Caption, Grid.Columns[I]);
      CheckListBox1.Checked[Idx] := Grid.Columns[I].Visible;
      Grid.Columns[I].Field.Tag:= I + 1;
    end;
//    for I := 0 to GridSource.DataSet.Fields.Count - 1 do begin
//      if GridSource.DataSet.Fields[I].Tag > 0 then Continue;
//      tmpColumn:= Grid.Columns.Add;
//      tmpColumn.Field:= GridSource.DataSet.Fields[i];
//      tmpColumn.Title.Caption:= GridSource.DataSet.Fields[i].FieldName;
//      Idx:= CheckListBox1.Items.AddObject(tmpColumn.Title.Caption, tmpColumn);
//      tmpColumn.Visible:= False;
//      CheckListBox1.Checked[Idx] := false;
//    end;
//    for i := 0 to GridSource.DataSet.Fields.Count - 1 do begin
//      found:= False;
//      for J := 0 to Grid.Columns.Count - 1 do
//        if Grid.Columns[J].Field = GridSource.DataSet.Fields[I] then begin
//          Idx:= CheckListBox1.Items.AddObject(Grid.Columns[J].Title.Caption, Grid.Columns[J]);
//          CheckListBox1.Checked[Idx] := Grid.Columns[J].Visible;
//          found:= True;
//          Break;
//        end;
//      if not found then begin
//        tmpColumn:= Grid.Columns.Add;
//        tmpColumn.Field:= GridSource.DataSet.Fields[i];
//        tmpColumn.Title.Caption:= GridSource.DataSet.Fields[i].FieldName;
//        Idx:= CheckListBox1.Items.AddObject(tmpColumn.Title.Caption, tmpColumn);
//        tmpColumn.Visible:= False;
//        CheckListBox1.Checked[Idx] := false;
//      end;
//    end;
      if ShowModal = mrOK then
        SaveTitles;
    finally
      Free;
    end;
end;

{ TFrameGridBar }

constructor TFrameGridBar.Create(AOwner: TComponent);
begin       // 构造函数
  inherited;
  //DisplayValue(0, IntToStr(StatusBar1.Panels.Count));
  FConditionColors:= TList.Create;
  FStatistic:= TList.Create;
  FHelper:= TFrameGridBarDataSource.Create(self);
  FHasTitle := False;
  FAutoColumn:= True;
  FFormGridName:= AOwner.Name;
  lstField.DrawingStyle:= gdsClassic;
end;

destructor TFrameGridBar.Destroy;
begin      // 析构函数
  if Assigned(FHelper) then
    FHelper.Free;
  FStatistic.Free;
  ClearCondition;
  FConditionColors.Free;
  inherited;
end;

procedure TFrameGridBar.DisplayFormat(col: integer; value: string;ValueFormat:string);
begin
  if value <> '' then begin
    StatusBar1.Canvas.Font.Height:= StatusBar1.font.Height;
    StatusBar1.Panels[col].Width:= StatusBar1.Canvas.TextWidth(value) + ABS(StatusBar1.font.Height)+10;
  end;
//  StatusBar1.Panels[Col].Text:=value;
  StatusBar1.Panels[Col].Text:=Format(ValueFormat,[StrToFloat(value)]);
end;

procedure TFrameGridBar.DisplayItemPanel(Display: Boolean);
begin
  if Display then begin
    if plItem.Width < 64 then plItem.Width:= 120;
  end else begin
    plItem.Left:= split1.Left + split1.Width + 2;
    plItem.Width:= 5;
  end;
  lstLegend.Visible:= (lstLegend.Count > 0) and Display;
  lstField.Visible:= Display;
end;

procedure TFrameGridBar.plItemResize(Sender: TObject);
begin
  if plItem.Width < 64 then DisplayItemPanel(False);
  if plItem.Width > 64 then DisplayItemPanel(true);
  lstField.ColWidths[0]:= lstField.Width div 10 * 4;
  Grid.AlignWithMargins:= False;
  split1.AlignWithMargins:= False;
  plItem.AlignWithMargins:= False;
end;

procedure TFrameGridBar.FrameResize(Sender: TObject);
begin
  if Width < 450 then DisplayItemPanel(False);
end;

procedure TFrameGridBar.DisplayValue(col: integer; value: string);
begin
  if value <> '' then begin
    StatusBar1.Canvas.Font.Height:= StatusBar1.font.Height;
    StatusBar1.Panels[col].Width:= StatusBar1.Canvas.TextWidth(value) + ABS(StatusBar1.font.Height);
  end;
  StatusBar1.Panels[Col].Text:=value;
//  StatusBar1.Panels[Col].Text:=Format('%n',[StrToFloat(value)]);
end;

(* Methods of button *)

//格式文本导出
procedure TFrameGridBar.menuExportCsvClick(Sender: TObject);
var
  DBMark: TBookmark;
begin
  SaveDialog1.DefaultExt := '';
  SaveDialog1.Filter := '格式文件|*.csv';
  if not SaveDialog1.Execute then Exit;
  DBMark:= GridSource.DataSet.GetBookmark;
  GridSource.DataSet.DisableControls;
  GridSource.DataSet.First;
  Screen.Cursor := crHourGlass;
  try
    WriteText(ChangeFileExt(SaveDialog1.filename, '.csv'));
  finally
    GridSource.DataSet.GotoBookmark(DBMark);
    GridSource.DataSet.EnableControls;
    Screen.Cursor := crDefault;
  end;
  ShowMessage('导出完成！');
end;
//Excel导出
procedure TFrameGridBar.menuExportExcelClick(Sender: TObject);
var
  excelApp: TExcelApplication;
  DBMark: TBookmark;
begin
  SaveDialog1.DefaultExt := '';
  SaveDialog1.Filter := 'Excel文件|*.xls';
  if not SaveDialog1.Execute then Exit;
  DBMark:= GridSource.DataSet.GetBookmark;
  GridSource.DataSet.DisableControls;
  GridSource.DataSet.First;
  Screen.Cursor := crHourGlass;
  try
    excelApp := TExcelApplication.Create(nil);
    WriteExcel(excelApp, ChangeFileExt(SaveDialog1.filename, '.xls'));
    excelApp.Disconnect;
  finally
    GridSource.DataSet.GotoBookmark(DBMark);
    GridSource.DataSet.EnableControls;
    Screen.Cursor := crDefault;
  end;
  ShowMessage('导出完成！');
end;
//html导出
procedure TFrameGridBar.menuExportHtmlClick(Sender: TObject);
var
  excelApp: TExcelApplication;
  DBMark: TBookmark;
  S: string;
begin
  SaveDialog1.DefaultExt := '';
  SaveDialog1.Filter := 'web文件|*.html';
  if not SaveDialog1.Execute then Exit;
  DBMark:= GridSource.DataSet.GetBookmark;
  GridSource.DataSet.DisableControls;
  GridSource.DataSet.First;
  Screen.Cursor := crHourGlass;
  try
    S:= ChangeFileExt(SaveDialog1.filename, '.html');
    WriteHtml(S);
    OpenHtml(S);
  finally
    GridSource.DataSet.GotoBookmark(DBMark);
    GridSource.DataSet.EnableControls;
    Screen.Cursor := crDefault;
  end;
  ShowMessage('导出完成！');
end;
//Xml导出
procedure TFrameGridBar.menuExportXmlClick(Sender: TObject);
var
  excelApp: TExcelApplication;
  DBMark: TBookmark;
begin
  SaveDialog1.DefaultExt := '';
  SaveDialog1.Filter := 'XML文件|*.xml';
  if not SaveDialog1.Execute then Exit;
  DBMark:= GridSource.DataSet.GetBookmark;
  GridSource.DataSet.DisableControls;
  GridSource.DataSet.First;
  Screen.Cursor := crHourGlass;
  try
    WriteXml(ChangeFileExt(SaveDialog1.filename, '.xml'));
  finally
    GridSource.DataSet.GotoBookmark(DBMark);
    GridSource.DataSet.EnableControls;
    Screen.Cursor := crDefault;
  end;
  ShowMessage('导出完成！');
end;
//直接打印
procedure TFrameGridBar.menuPrintIOClick(Sender: TObject);
var
  FPrn: TextFile;
  DBMark: TBookmark;
  S: string;
  I: Integer;
begin
  AssignFile(FPrn, 'PRN');
  Append(FPrn);
  DBMark:= GridSource.DataSet.GetBookmark;
  GridSource.DataSet.DisableControls;
  GridSource.DataSet.First;
  Screen.Cursor := crHourGlass;
  try
    S:= Grid.Columns[0].Title.Caption;
    for I := 1 to Grid.Columns.Count - 1 do
      S:= S + char(9) + Grid.Columns[I].Title.Caption;
    Writeln(FPrn, S);
    S:= Grid.Columns[0].Title.Caption; Grid.Columns[0].Title.Caption;
    while not GridSource.DataSet.EOF do begin
      S:= Grid.Columns[0].Field.AsString;
      for I := 1 to Grid.Columns.Count - 1 do
        S:= S + char(9) + Grid.Columns[I].Field.AsString;
      Writeln(FPrn, S);
      GridSource.DataSet.Next;
    end;
    System.CloseFile(FPrn);
  finally
    GridSource.DataSet.GotoBookmark(DBMark);
    GridSource.DataSet.EnableControls;
    Screen.Cursor := crDefault;
  end;
  ShowMessage('打印完成！');
end;
//Excel打印
procedure TFrameGridBar.menuPrintExcelClick(Sender: TObject);
var
  excelApp: TExcelApplication;
  DBMark: TBookmark;
begin
  DBMark:= GridSource.DataSet.GetBookmark;
  GridSource.DataSet.DisableControls;
  GridSource.DataSet.First;
  Screen.Cursor := crHourGlass;
  try
    excelApp := TExcelApplication.Create(nil);
    WriteExcel(excelApp, ExtractFilePath(Application.ExeName) + 'Temp\Temp.xls');
    excelApp.ActiveWorkbook.PrintPreview(True, 0);
    excelApp.Disconnect;
  finally
    GridSource.DataSet.GotoBookmark(DBMark);
    GridSource.DataSet.EnableControls;
    Screen.Cursor := crDefault;
  end;
  ShowMessage('打印完成！');
end;
//html打印
procedure TFrameGridBar.menuPrintHtmlClick(Sender: TObject);
var
  excelApp: TExcelApplication;
  DBMark: TBookmark;
  S: string;
begin
  DBMark:= GridSource.DataSet.GetBookmark;
  GridSource.DataSet.DisableControls;
  GridSource.DataSet.First;
  Screen.Cursor := crHourGlass;
  try
    S:= ExtractFilePath(Application.ExeName) + 'Temp\Temp.html';
    WriteHtml(S);
//    ShellExecute(Application.Handle, 'print', PChar(S), '', '', SW_SHOW);
    PrintHtml(S);
  finally
    GridSource.DataSet.GotoBookmark(DBMark);
    GridSource.DataSet.EnableControls;
    Screen.Cursor := crDefault;
  end;
  ShowMessage('打印完成！');
end;
//打印机设置
procedure TFrameGridBar.menuPrintSetupClick(Sender: TObject);
begin
  with TPrinterSetupDialog.Create(nil)do try
    Execute();
  finally
    Free;
  end;
end;
//列表筛选
procedure TFrameGridBar.menuColumnSettingClick(Sender: TObject);
begin     // 列表筛选
  if not Assigned(GridSource.DataSet) then Exit;
  if not GridSource.DataSet.Active then Exit;
  FilterColumn();
end;
//行选择
procedure TFrameGridBar.menuSelectionOptionClick(Sender: TObject);
begin
  menuSelectionOption.Checked:= not menuSelectionOption.Checked;
  if not menuSelectionOption.Checked then
    Grid.Options:= Grid.Options - [dgRowSelect] + [dgEditing]
  else Grid.Options:= Grid.Options - [dgEditing] + [dgRowSelect]
end;

procedure TFrameGridBar.menuConditionClick(Sender: TObject);
begin
  menuCondition.Checked:= not menuCondition.Checked;
end;

procedure TFrameGridBar.menuEditClick(Sender: TObject);
begin
  menuEdit.Checked:= not menuEdit.Checked;
end;

(* Exports *)

procedure TFrameGridBar.WriteExcel(excelApp: TExcelApplication; filename: string);
var // 导出Excel
  ACol, ARow: Integer;
  book: _Workbook;
  sheet: _Worksheet;
begin
  // CreateOleObject('Excel.Application') as _Application;//
  excelApp.Visible[1] := True;
  book := excelApp.Workbooks.Add(EmptyParam, 0);
  sheet := book.Sheets.Item[1] as _Worksheet;
  sheet.Cells.Item[1, 1].Value2 := Hint;
  sheet.Range[GetCellName(1, 1), GetCellName(Grid.Columns.Count, 1)].Merge(4);
  with sheet.Cells, Grid, GridSource.DataSet do // Column title
    for ACol := 0 to Grid.Columns.Count - 1 do
      if Columns[ACol].Visible then begin
        Item[2, ACol + 1].Value2 := Columns[ACol].Title.Caption;
        if Grid.Columns[ACol].Width > 1000 then
          Item[2, ACol + 1].ColumnWidth:= 125
        else Item[2, ACol + 1].ColumnWidth:= Grid.Columns[ACol].Width div 8;
      end;
  ARow := 3;
  with sheet.Cells, Grid, GridSource.DataSet do // Data
    while not EOF do
    begin
      for ACol := 0 to Columns.Count - 1 do
        if Columns[ACol].Visible then
          Item[ARow, ACol + 1].Value2 := Columns[ACol].Field.AsString;
      Next;
      Inc(ARow);
    end;
  if FStatistic.Count > 0 then begin
    sheet.Cells.Item[ARow, 1].Value2 := '汇总';
    for ACol := 1 to StatusBar1.Panels.Count - 1 do
      sheet.Cells.Item[ARow, ACol + 1].Value2 := StatusBar1.Panels[Acol].Text;
  end;
  if filename <> '' then
    book.SaveAs(filename, xlAddIn, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, 0, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, 0);
end;

procedure TFrameGridBar.SimpleExcelClick(Sender: TObject);
var
  DBMark: TBookmark;
  i: integer;
  Col, row: word;
  ABookMark: TBookMark;
  xls: TVonExcelWriter;
  bWriteTitle:Boolean;

begin
  bWriteTitle:=true;
  SaveDialog1.DefaultExt := '';
  SaveDialog1.Filter := 'Excel文件|*.xls';
  if not SaveDialog1.Execute then Exit;
  xls:= TVonExcelWriter.Create(ChangeFileExt(SaveDialog1.filename, '.xls'));
  Try
    //写列头
    Col := 0; Row := 0;
    if bWriteTitle then begin
      for i := 0 to Grid.Columns.Count - 1 do begin
        if Grid.Columns[i].Visible then begin
          xls.WriteCell(Col, Row, Grid.Columns[i].Title.Caption);
          Inc(Col);
        end;
      end;
    end;
    Inc(Row); Col := 0;
    //写数据集中的数据
    GridSource.DataSet.DisableControls;
    ABookMark := GridSource.DataSet.GetBookmark;
    GridSource.DataSet.First;
    while not GridSource.DataSet.Eof do begin
      for i := 0 to Grid.Columns.Count - 1 do begin
        if Grid.Columns[i].Visible then begin
          case Grid.Columns[i].Field.DataType of
            ftSmallint, ftInteger, ftWord, ftAutoInc, ftBytes:
              xls.WriteCell(Col, Row, GridSource.DataSet.Fields[Grid.Columns[i].Field.Index].AsInteger);
            ftFloat, ftCurrency, ftBCD:
              xls.WriteCell(Col, Row, GridSource.DataSet.Fields[Grid.Columns[i].Field.Index].AsFloat)
          else
            xls.WriteCell(Col, Row, GridSource.DataSet.Fields[Grid.Columns[i].Field.Index].AsString);
          end;
          Inc(Col);
        end;
      end;
      Inc(Row); Col := 0;
      GridSource.DataSet.Next;
    end;
    //写文件尾
    if GridSource.DataSet.BookmarkValid(ABookMark) then
      GridSource.DataSet.GotoBookmark(ABookMark);
  finally
    xls.Free;
    GridSource.DataSet.EnableControls;
  end;

end;

procedure TFrameGridBar.WriteHtml(filename: string);
var // 导出
  ACol, ARow: Integer;
  F: TextFile;
  S: string;
begin
  AssignFile(F, filename);
  Rewrite(F);
  Writeln(F, '<!DOCTYPE html><html><head><title>' + Hint + '</title></head><body>');
  Writeln(F, '<div id="title">' + Hint + '</div>');
  Writeln(F, '<table>');
  with Grid, GridSource.DataSet do begin// Column title
    S := '<tr><td>' + Columns[0].Title.Caption + '</td>';
    for ACol := 1 to Grid.Columns.Count - 1 do
      if Columns[ACol].Visible then
        S := S + '<td>' + Columns[ACol].Title.Caption + '</td>';
  end;
  Writeln(F, S + '</tr>');
  with Grid, GridSource.DataSet do // Data
    while not EOF do
    begin
      S := '<tr><td>' + Columns[0].Field.AsString + '</td>';
      for ACol := 1 to Grid.Columns.Count - 1 do
        if Columns[ACol].Visible then
          S := S + '<td>' + Columns[ACol].Field.AsString + '</td>';
      Writeln(F, S + '</tr>');
      Next;
    end;
  Writeln(F, S + '</table></body></html>');
  CloseFile(F);
end;

procedure TFrameGridBar.WriteText(filename: string);
var // 导出
  ACol, ARow: Integer;
  F: TextFile;
  S,sTem: string;
  function replace(s,sa,sb:string):string;
  var
      i,k:integer;
      temp:string;
  begin
      k:=length(sa);
      i:=pos(sa,s);
      temp:=s;
      if i<>0 then begin
        if i=Length(s) then temp:=copy(s,1,i-1)+sb
          else temp:=copy(s,1,i-1)+sb+copy(s,i+k,length(s)-i-k+1);
      end;
      Result:=temp;
  end;
begin
  AssignFile(F, filename);
  Rewrite(F);
  S := '';
  with Grid, GridSource.DataSet do // Column title
    for ACol := 0 to Grid.Columns.Count - 1 do
      if Columns[ACol].Visible then
        S := S + ',"' + Columns[ACol].Title.Caption + '"';
  Delete(S, 1, 1);
  Writeln(F, S);
  ARow := 3;
  with Grid, GridSource.DataSet do // Data
    while not EOF do
    begin
      S := '';
//      for ACol := 0 to Columns.Count - 1 do
//        if Columns[ACol].Visible then
//          S := S + ',"' + Columns[ACol].Field.AsString + '"';
//      System.Delete(S, 1, 1);
      for ACol := 0 to Columns.Count - 1 do begin
        if Columns[ACol].Visible then begin
          sTem:=Columns[ACol].Field.AsString;
          if pos('"',sTem)>0 then begin
            sTem:=replace(sTem,'"','""');
          end;
          if pos(',',sTem)>0 then begin
            sTem:='"'+sTem+'"';
          end;
          S:=S+','+ sTem;
        end;
      end;
      System.Delete(S, 1, 1);
      Writeln(F, S);
      Next;
      Inc(ARow);
    end;
  CloseFile(F);
end;

procedure TFrameGridBar.WriteXml(filename: string);
begin
  if GridSource.DataSet is TCustomADODataSet then
    TCustomADODataSet(GridSource.DataSet).SaveToFile(filename, pfXML);
end;

(* Events of grid *)

procedure TFrameGridBar.GridColumnMoved(Sender: TObject; FromIndex,
  ToIndex: Integer);
begin
  SaveTitles();
end;

function TFrameGridBar.GetFieldValue(itemValue: string): string;
var
  szFld: TField;
begin
  if itemValue = 'RecordNo' then
    Result:= IntToStr(GridSource.DataSet.RecNo)
  else begin
    szFld:= GridSource.DataSet.FindField(itemValue);
    if Assigned(szFld) then
      case szFld.DataType of
      ftTimeStamp, ftOraTimeStamp, ftDate, ftTime, ftDateTime:
        Result:= FloatToStr(szFld.AsExtended);
      ftBoolean: Result:= BoolToStr(szFld.AsBoolean, true);
      else
        Result:= szFld.AsString;
      end
    else Result:= itemValue;
  end;
end;

function TFrameGridBar.CheckCondition(Item: TConditionItem): Boolean;
var aValue, bValue: Extended;
  I: Integer;
  S1, S2: string;
begin
  case Item.FCondition of
  '[': if TryStrToFloat(GetFieldValue(Item.ItemFieldName), aValue)and
         TryStrToFloat(GetFieldValue(Item.ConditionValue), bValue)then
         Result:= aValue <= bValue;
  '<': if TryStrToFloat(GetFieldValue(Item.ItemFieldName), aValue)and
         TryStrToFloat(GetFieldValue(Item.ConditionValue), bValue)then
         Result:= aValue < bValue;
  '%': Result:= (ROUND(StrToFloat(GetFieldValue(Item.ItemFieldName))) mod
      StrToInt(Copy(Item.ConditionValue, 1, Pos('.', Item.ConditionValue) - 1))) =
      StrToInt(Copy(Item.ConditionValue, Pos('.', Item.ConditionValue) + 1, MaxInt));
  '=': begin
      S1:= GetFieldValue(Item.ConditionValue);
      S2:= GetFieldValue(Item.ItemFieldName);
      for I:= 1 to Length(S1) do
        if S1[I] = '?' then Continue
        else if Length(S2) < I then Exit
        else if S1[I] <> S2[I] then Exit;
      Result:= True;
    end;
  '>': if TryStrToFloat(GetFieldValue(Item.ItemFieldName), aValue)and
         TryStrToFloat(GetFieldValue(Item.ConditionValue), bValue)then
         Result:= aValue > bValue;
  ']': if TryStrToFloat(GetFieldValue(Item.ItemFieldName), aValue)and
         TryStrToFloat(GetFieldValue(Item.ConditionValue), bValue)then
         Result:= aValue >= bValue;
  '!': Result:= GetFieldValue(Item.ItemFieldName) <>
                GetFieldValue(Item.ConditionValue);
  end;
  if Assigned(Item.Next) then begin
    if Item.IsAnd then Result:= Result and CheckCondition(Item.Next)
    else Result:= Result or CheckCondition(Item.Next);
  end;
end;

function TFrameGridBar.GetConditionFilter(Item: TConditionItem): string;
var
  isDateField: Boolean;
  szFld: TField;
  bValue: Double;
begin
  if Item.ItemFieldName = 'RecordNo' then Exit;
  isDateField:= False;
  szFld:= GridSource.DataSet.FindField(Item.ItemFieldName);
  if Assigned(szFld) then begin
    Result:= szFld.FieldName;
    case szFld.DataType of
    ftTimeStamp, ftOraTimeStamp, ftDate, ftTime, ftDateTime: isDateField:= True;
    else isDateField:= False;
    end;
  end else Result:= '''' + Item.ItemFieldName + '''';
  case Item.FCondition of
  '[': Result:= Result + ' <= ';
  '<': Result:= Result + ' < ';
  '%': Result:= Result + ' % ' + Copy(Item.ConditionValue, 1, Pos('.', Item.ConditionValue) - 1)
    + '=' + Copy(Item.ConditionValue, Pos('.', Item.ConditionValue) + 1, MaxInt);
  '=': Result:= Result + ' = ';
  '>': Result:= Result + ' > ';
  ']': Result:= Result + ' >= ';
  '!': Result:= Result + ' <> ';
  end;
  szFld:= GridSource.DataSet.FindField(Item.ConditionValue);
  if Assigned(szFld) then Result:= Result + szFld.FieldName
  else if TryStrToFloat(Item.ConditionValue, bValue) then begin
    if not isDateField then Result:= Result + Item.ConditionValue
    else Result:= Result + '''' + DateTimeToStr(TDateTime(bValue)) + '''';
//  end else Result:= Result + '''' + Item.ConditionValue + '''';
  end
  else begin
    if SameText(Item.ConditionValue, 'Null') then
      Result:= Result + Item.ConditionValue
    else Result:= Result + '''' + Item.ConditionValue + '''';
  end;
  if Assigned(Item.Next) then begin
    if Item.IsAnd then Result:= Result + ' and ' + GetConditionFilter(Item.Next)
    else Result:= Result + ' or ' + GetConditionFilter(Item.Next);
  end;
end;

procedure TFrameGridBar.GridDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  I: Integer;
  luminance: Double;
begin
  for I := 0 to FConditionColors.Count - 1 do begin
    if CheckCondition(TConditionItem(FConditionColors[I])) then
      with TDBGrid(Sender).Canvas do begin
        Brush.Color := TConditionItem(FConditionColors[I]).ResultValue;
        luminance:= GetRValue(Brush.Color) * 0.25 + GetGValue(Brush.Color) * 0.625
          + GetBValue(Brush.Color) * 0.125;
        if luminance < 128 then Font.Color:= clWindow
        else Font.Color:= clBlack;
      end;
    TDBGrid(Sender).DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;
end;

procedure TFrameGridBar.GridTitleClick(Column: TColumn);
begin // 排序
  if Column.FieldName = FColumnToSort then
    FOrderType := 1 - FOrderType;
  FColumnToSort := Column.FieldName;
  case FOrderType of
  0: (Grid.DataSource.DataSet as TCustomADODataSet).Sort := FColumnToSort + ' ASC';
  1: (Grid.DataSource.DataSet as TCustomADODataSet).Sort := FColumnToSort + ' DESC';
  end;
  if Assigned((Grid.DataSource.DataSet as TCustomADODataSet).AfterScroll) then
    (Grid.DataSource.DataSet as TCustomADODataSet).AfterScroll(Grid.DataSource.DataSet);
end;

procedure TFrameGridBar.EventOfColumnWidthChanged(sender: TObject);
begin
  SaveTitles();
end;

(* Titles *)

procedure TFrameGridBar.LoadTitles(FormName: string);
var
  TitleFilename: string;
  I: Integer;
begin     // 提取标题信息
  if not FAutoColumn then Exit;
  if FormName = '' then FormName:= FSectionName;
  if FormName = '' then FormName:= FFormGridName + '.' + Name;;
  if(FormName = FSectionName)and FHasTitle then Exit;
  FSectionName := FormName;

  FHasTitle:= False;
  TitleFilename := ExtractFilePath(Application.ExeName) + 'RES\' + FSectionName
    + '.Titles';
  Grid.Columns.Clear;
  if FileExists(TitleFilename) then
    Grid.Columns.LoadFromFile(TitleFilename)
  else if Assigned(GridSource.DataSet) and GridSource.DataSet.Active then begin
    for I := 0 to GridSource.DataSet.FieldCount - 1 do
      with Grid.Columns.Add do begin
        Title.Caption:= GridSource.DataSet.Fields[I].FieldName;
        FieldName:= GridSource.DataSet.Fields[I].FieldName;
      end;
    FHasTitle:= True;
  end;
  //LoadColumnSetting;
end;

procedure TFrameGridBar.SaveTitles(FormName: string);
begin
  if not FAutoColumn then Exit;
  if FormName = '' then FormName:= FSectionName;
  if FormName = '' then FormName:= FFormGridName;
  if FormName = '' then Exit;
  if not DirectoryExists(ExtractFilePath(Application.ExeName) + 'RES\') then
    CreateDir(ExtractFilePath(Application.ExeName) + 'RES\');
  if (FSectionName <> '') then
    Grid.Columns.SaveToFile(ExtractFilePath(Application.ExeName) + 'RES\' +
      FormName + '.Titles');
  //SaveColumnSetting;
end;

(* Styles of Condition *)

function TFrameGridBar.SplitCondition(PCondition: PChar; Item: TConditionItem): PChar;
var
  S: string;
begin
  S:= '';
  Result:= PCondition;
  while Result^ <> #0 do begin
    case Result^ of      //= <> >= <= => =< < >
    '=': if Item.Condition = '>' then Item.Condition:= ']'
      else if Item.Condition = '<' then Item.Condition:= '['
      else if Item.Condition = '!' then Item.Condition:= '!'
      else begin
        Item.Condition:= '=';
        Item.ItemFieldName:= S;
        S:= '';
      end;
    '>': if Item.Condition = '=' then Item.Condition:= ']'
      else if Item.Condition = '<' then Item.Condition:= '!'
      else begin
        Item.Condition:= '>';
        Item.ItemFieldName:= S;
        S:= '';
      end;
    '<': if Item.Condition = '=' then Item.Condition:= '[' else begin
        Item.Condition:= '<';
        Item.ItemFieldName:= S;
        S:= '';
      end;
    '!': begin
        Item.Condition:= '!';
        Item.ItemFieldName:= S;
        S:= '';
      end;
    '%': begin
        Item.Condition:= '%';
        Item.ItemFieldName:= S;
        S:= '';
      end;
    '&', '|': begin
        Item.ConditionValue:= S;
        Item.IsAnd:= Result^ = '&';
        Item.Next:= TConditionItem.Create;
        Result:= SplitCondition(Result + 1, Item.Next) - 1;
        S:= '';
      end
    else S:= S + Result^
    end;
    Inc(Result);
  end;
  if S <> '' then Item.FConditionValue:= S;
end;

procedure TFrameGridBar.RegConditionColor(Condition: string; Color: TColor; HintText: string);
var
  Item: TConditionItem;
begin
  if Condition = '' then Exit;
  Item:= TConditionItem.Create;
  Item.ResultValue:= Color;
  Item.Legend:= HintText;
  FConditionColors.Add(Item);
  SplitCondition(PChar(Condition), Item);
  if lstLegend.Count = 0 then
    lstLegend.AddItem('所有', TObject(clWhite));
  lstLegend.AddItem(HintText, TObject(Color));
  lstLegend.Height:= lstLegend.ItemHeight * lstLegend.Count + 4;
  lstLegend.Enabled:= True;
  lstLegend.Visible:= True;
end;

procedure TFrameGridBar.RegStatisticFormat(Fieldname, HintText: string;
  Kind: TEnumStatistic; Condition: string; ValueFormat:TEnumFormat);
var
  Item: TConditionItem;
  sFormat:string;
begin
  if Fieldname = '' then Exit;
  Item:= TConditionItem.Create;
  Item.ResultValue:= Ord(Kind);
  Item.Legend:= Fieldname;
  Item.Title:= HintText;
  Item.ValueFormat:=Ord(ValueFormat);
  Item.ForamtValue:= Ord(Kind);
  FStatistic.Add(Item);
  SplitCondition(PChar(Condition), Item);
  DisplayValue(StatusBar1.Panels.Add.Index, HintText);
  DisplayValue(StatusBar1.Panels.Add.Index, '0');
end;

procedure TFrameGridBar.ClearCondition;
var
  I: Integer;
begin
  for I := 0 to FConditionColors.Count - 1 do
    TConditionItem(FConditionColors[I]).Free;
  FConditionColors.Clear;
  lstLegend.Clear;
  lstLegend.Visible:= False;
end;

procedure TFrameGridBar.lstLegendClick(Sender: TObject);
begin
  if Assigned(GridSource.DataSet)and GridSource.DataSet.Active then begin
    if lstLegend.ItemIndex = 0 then GridSource.DataSet.Filtered:= False
    else begin
      GridSource.DataSet.Filtered:= False;
      GridSource.DataSet.Filter:= GetConditionFilter(TConditionItem(FConditionColors[lstLegend.ItemIndex - 1]));
      GridSource.DataSet.Filtered:= True;
    end;
  end;
end;

procedure TFrameGridBar.RegStatistic(Fieldname, HintText: string; Kind: TEnumStatistic; Condition: string);
var
  Item: TConditionItem;
begin
  if Fieldname = '' then Exit;
  Item:= TConditionItem.Create;
  Item.ResultValue:= Ord(Kind);
  Item.Legend:= Fieldname;
  Item.Title:= HintText;
  FStatistic.Add(Item);
  SplitCondition(PChar(Condition), Item);
  DisplayValue(StatusBar1.Panels.Add.Index, HintText);
  DisplayValue(StatusBar1.Panels.Add.Index, '0');
end;

procedure TFrameGridBar.ClearStatistic;
var
  I: Integer;
begin
  for I := 0 to FStatistic.Count - 1 do
    TConditionItem(FStatistic[I]).Free;
  FStatistic.Clear;
end;

function TFrameGridBar.GetCellName(Col, Row: Integer): string;
var // 得到单元Excel位置,Cell(2,3)->B3
  szInt: Integer;
  szCol: string;

  function Cvt: char;
  begin
    Result := char(65 + (szInt - 1) mod 26);
    szInt := (szInt - 1) div 26;
  end;

begin
  szCol := '';
  szInt := Col;
  while szInt > 0 do
    szCol := Cvt + szCol;
  Result := szCol + IntToStr(Row);
end;

procedure TFrameGridBar.SetAdoConnection(const Value: TADOConnection);
begin
  FAdoConnection := Value;
  FHelper.FConn:= FAdoConnection;
end;

procedure TFrameGridBar.SetAutoColumn(const Value: Boolean);
begin
  FAutoColumn := Value;
end;

procedure TFrameGridBar.SetCanDelColumn(const Value: boolean);
begin
  FCanDelColumn := Value;
end;

procedure TFrameGridBar.StatusBar1Click(Sender: TObject);
var
  Pt: TPoint;
  R: TRect;
begin     //先粗略取坐标，看是点在哪个格子上的
  GetCursorPos(Pt);
  R := Rect(0, 0, StatusBar1.Panels[0].Width, StatusBar1.Height);
  if PtInRect(R, StatusBar1.ScreenToClient(Pt)) then
    gridMenu.Popup(Pt.X + 5, Pt.Y + 10);
end;

{ TFrameGridBarDataSource }

constructor TFrameGridBarDataSource.Create(fmGrid: TFrameGridBar);
begin
  inherited Create;
  FGrid:= fmGrid;
  DataSource := fmGrid.GridSource;
end;

procedure TFrameGridBarDataSource.DataSetChanged;
begin
  inherited;
  if(not FDisibled)and Active and Assigned(DataSet)and DataSet.Active and DataSet.Bof then
    ReStatistic;
  FDisibled:= False;
end;

procedure TFrameGridBarDataSource.DataSetScrolled(Distance: Integer);
begin
  inherited;

  DisplayFields;
end;

procedure TFrameGridBarDataSource.DisibleGrid;
begin
  FGrid.GridSource.DataSet.DisableControls;
  FDisibled:= True;
end;

procedure TFrameGridBarDataSource.DisplayFields;
var
  I: Integer;
begin
  FGrid.lstField.Strings.Clear;
  for i:= 0 to FGrid.Grid.Columns.Count - 1 do
    if(FGrid.Grid.Columns[i].Width > 0)and Assigned(FGrid.Grid.Columns[i].Field)then
      FGrid.lstField.InsertRow(FGrid.Grid.Columns[i].Title.Caption,
        FGrid.Grid.Columns[i].Field.AsString, true);
end;

function TFrameGridBarDataSource.GetFieldTitle(AFieldName: string): string;
begin
  Result:= AFieldName;
  if Assigned(FConn) then
    with FConn.Execute('SELECT FldName FROM SYS_DataDictionary WHERE FldCode='''
      + AFieldName + '''') do
      if not EOF then Result:= Fields[0].Value;
end;

procedure TFrameGridBarDataSource.ReStatistic;
var
  I, mPos: Integer;
  fldWidth, fldTitle, fldSetting: string;
  szVal, szVal1, szVal2: Extended;
  bk: TBookMark;
  sFormat:string;
begin
  if DataSet.RecordCount = 0 then begin
    FGrid.DisplayValue(1, '0行');
    for I := 0 to FGrid.FStatistic.Count - 1 do
      FGrid.DisplayValue(3 + I * 2, '');
    Exit;
  end;
  FGrid.DisplayValue(1, Format('%d 行',[FGrid.GridSource.DataSet.RecordCount]));
  if FGrid.FStatistic.Count = 0 then Exit;
  bk:= DataSet.GetBookmark;
  DataSet.DisableControls;
  try
    for I := 0 to FGrid.FStatistic.Count - 1 do
      with TConditionItem(FGrid.FStatistic[i]) do begin
        DataSet.GotoBookmark(bk);
        szVal:= 0; szVal1:= 0; szVal2:= 0;
        FGrid.DisplayValue(2 + I * 2, Title);
        {$region '根据统计类型计算统计结果'}
        case TEnumStatistic(ResultValue) of
        ES_SUM: begin
          while not DataSet.Eof do begin
            if(Condition = '')or FGrid.CheckCondition(TConditionItem(FGrid.FStatistic[i])) then
              szVal:= szVal + DataSet.FieldByName(Legend).AsExtended;
            DataSet.Next;
          end;
          if ValueFormat>0 then begin
            case TEnumFormat(ValueFormat) of
              ES_Int: sFormat:='%d';
              ES_Real:sFormat:='%f';
              ES_Real2:sFormat:='%0.2f';
              ES_Real3:sFormat:='%0.3f';
              ES_Currency:sFormat:='%m';
              ES_n:sFormat:='%0.3n';
              ES_g:sFormat:='%g';
            end;
            FGrid.DisplayFormat(3 + I * 2, FloatToStr(szVal),sFormat);
          end
          else
          FGrid.DisplayValue(3 + I * 2, FloatToStr(szVal));
        end;
        ES_AVG: begin
          while not FGrid.GridSource.DataSet.Eof do begin
            if(Condition = '')or FGrid.CheckCondition(TConditionItem(FGrid.FStatistic[i])) then begin
              szVal:= szVal + DataSet.FieldByName(Legend).AsExtended;
              szVal1:= szVal1 + 1;
            end;
            DataSet.Next;
          end;
          FGrid.DisplayValue(3 + I * 2, Format('%.3f', [szVal / szVal1]));
        end;
        ES_COUNT: begin
          while not FGrid.GridSource.DataSet.Eof do begin
            if(Condition = '')or FGrid.CheckCondition(TConditionItem(FGrid.FStatistic[i])) then
              szVal:= szVal + 1;
            DataSet.Next;
          end;
          FGrid.DisplayValue(3 + I * 2, FloatToStr(szVal));
        end;
        ES_MAX: begin
          szVal:= MinCurrency;
          while not FGrid.GridSource.DataSet.Eof do begin
            if(Condition = '')or FGrid.CheckCondition(TConditionItem(FGrid.FStatistic[i])) then begin
              if DataSet.FieldByName(Legend).AsExtended > szVal then
              szVal:= DataSet.FieldByName(Legend).AsExtended;
            end;
            FGrid.GridSource.DataSet.Next;
          end;
          FGrid.DisplayValue(3 + I * 2, FloatToStr(szVal));
        end;
        ES_MIN: begin
          szVal:= MaxCurrency;
          while not FGrid.GridSource.DataSet.Eof do begin
            if(Condition = '')or FGrid.CheckCondition(TConditionItem(FGrid.FStatistic[i])) then begin
              if DataSet.FieldByName(Legend).AsExtended < szVal then
              szVal:= DataSet.FieldByName(Legend).AsExtended;
            end;
            DataSet.Next;
          end;
          FGrid.DisplayValue(3 + I * 2, FloatToStr(szVal));
        end;
//          ES_STDEV:
//          ES_STDEVP:
//          ES_VAR:
//          ES_VARP:
        end;
        {$endregion}
      end;
  finally
    DataSet.GotoBookmark(bk);
    DataSet.FreeBookmark(bk);
    DataSet.EnableControls;
  end;
end;

procedure TFrameGridBarDataSource.ActiveChanged;
begin
  inherited;
  if Active and Assigned(FGrid.GridSource.DataSet)and FGrid.GridSource.DataSet.Active then begin
    if not Assigned(FConn) then
      FConn:= TCustomADODataSet(FGrid.GridSource.DataSet).Connection;
    {$region '智能添加标题列表'}
    FGrid.LoadTitles;
    ReStatistic;
    DisplayFields;
    {$endregion}
  end;
//  if Assigned(FGrid.GridSource)and FGrid.GridSource.DataSet.Active and FGrid.AutoColumn then
//    FGrid.SaveColumnSetting;
end;

{ TConditionItem }

procedure TConditionItem.SetCondition(const Value: AnsiChar);
begin
  FCondition := Value;
end;

procedure TConditionItem.SetConditionValue(const Value: string);
begin
  FConditionValue := Value;
end;

procedure TConditionItem.SetForamtValue(const Value: Cardinal);
begin
  FForamtValue:=Value;
end;

procedure TConditionItem.SetIsAnd(const Value: Boolean);
begin
  FIsAnd := Value;
end;

procedure TConditionItem.SetItemFieldName(const Value: string);
begin
  FItemFieldName := Value;
end;

procedure TConditionItem.SetLegend(const Value: string);
begin
  FLegend := Value;
end;

procedure TConditionItem.SetNext(const Value: TConditionItem);
begin
  FNext := Value;
end;

procedure TConditionItem.SetResultValue(const Value: Cardinal);
begin
  FResultValue:= Value;
end;

procedure TConditionItem.SetTitle(const Value: string);
begin
  FTitle := Value;
end;
procedure TConditionItem.SetValueFormat(const Value: Cardinal);
begin
  FValueFormat := Value;
end;

end.
