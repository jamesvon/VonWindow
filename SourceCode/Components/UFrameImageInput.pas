unit UFrameImageInput;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls, UVonGraphicEx,
  ExtDlgs, jpeg, UVonGraphicAPI, Clipbrd, System.ImageList;

type
  TFrameImageInput = class(TFrame)
    ToolBar1: TToolBar;
    tbCamera: TToolButton;
    tbZoomOut: TToolButton;
    tbZoomIn: TToolButton;
    tbZoomZeao: TToolButton;
    ImageList1: TImageList;
    ToolButton5: TToolButton;
    OpenPictureDialog1: TOpenPictureDialog;
    ToolButton6: TToolButton;
    EInputStr: TEdit;
    Image1: TImage;
    tbLoadFromFile: TToolButton;
    ToolButton1: TToolButton;
    lbPrompt: TLabel;
    Timer1: TTimer;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    procedure tbEditedClick(Sender: TObject);
    procedure tbZoomOutClick(Sender: TObject);
    procedure tbZoomInClick(Sender: TObject);
    procedure tbZoomZeaoClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure Image1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure Image1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FrameResize(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
  private
    { Private declarations }
    FModified: Boolean;
    FPictureType: string;
    FOrgBmp: TBitmap;
    FZoom: Double;
    X0, Y0: Integer;
    X1, Y1: Integer;
    procedure SetModified(const Value: Boolean);
    procedure SetPictureType(const Value: string);
    procedure SetBmp(const Value: TBitmap);
    procedure SetInputStr(const Value: string);
    procedure SetPrompt(const Value: string);
    function GetPrompt: string;
    function GetInputStr: string;
    procedure DrewArea;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure DrawImage;
    property Prompt: string read GetPrompt write SetPrompt;
    property InputStr: string read GetInputStr write SetInputStr;
    property Modified: Boolean read FModified write SetModified;
    property PictureType: string read FPictureType write SetPictureType;
    property Bitmap: TBitmap read FOrgBmp write SetBmp;
  end;

implementation

uses UDlgCamera;

{$R *.dfm}

constructor TFrameImageInput.Create(AOwner: TComponent);
begin
  inherited;
  FOrgBmp := TBitmap.Create;
  FOrgBmp.Width := Self.Width;
  FOrgBmp.Height := Self.Height;
end;

destructor TFrameImageInput.Destroy;
begin
  FOrgBmp.Free;
  inherited;
end;

procedure TFrameImageInput.DrawImage;
var
  X, Y: Integer;
  destRect, srcRect: TRect;
  tmpBmp: TBitmap;
begin
  tmpBmp := TBitmap.Create;
  tmpBmp.Width := Round(FOrgBmp.Width * (1 + FZoom / 10));
  tmpBmp.Height := Round(FOrgBmp.Height * (1 + FZoom / 10));
  tmpBmp.Canvas.StretchDraw(Rect(0, 0, tmpBmp.Width, tmpBmp.Height), FOrgBmp);
  Image1.Picture.Bitmap.Width := Self.Width;
  Image1.Picture.Bitmap.Height := Self.Height - 24;
  Image1.Canvas.FillRect(Rect(0, 0, Image1.Width, Image1.Height));
  if (Image1.Height = 0) or (tmpBmp.Height = 0) then
    Exit;
  X := (Image1.Width - tmpBmp.Width) div 2;
  Y := (Image1.Height - tmpBmp.Height) div 2;
  Image1.Picture.Bitmap.Canvas.CopyMode := cmSrcCopy;
  Image1.Picture.Bitmap.Canvas.Draw(X, Y, tmpBmp);
  tmpBmp.Free;
end;

function TFrameImageInput.GetInputStr: string;
begin
  Result := EInputStr.Text;
end;

function TFrameImageInput.GetPrompt: string;
begin
  Result := lbPrompt.Caption;
end;

procedure TFrameImageInput.SetBmp(const Value: TBitmap);
begin
  FOrgBmp.Assign(Value);
  FZoom := 0;
  DrawImage;
end;

procedure TFrameImageInput.SetInputStr(const Value: string);
begin
  EInputStr.Text := Value;
end;

procedure TFrameImageInput.SetModified(const Value: Boolean);
begin
  FModified := Value;
end;

procedure TFrameImageInput.SetPictureType(const Value: string);
begin
  FPictureType := Value;
end;

procedure TFrameImageInput.SetPrompt(const Value: string);
begin
  lbPrompt.Visible := Value <> '';
  lbPrompt.Caption := Value;
end;

procedure TFrameImageInput.tbEditedClick(Sender: TObject);
begin
  DlgCamera(FOrgBmp);
  FZoom := 0;
  DrawImage;
end;

procedure TFrameImageInput.tbZoomOutClick(Sender: TObject);
begin
  if FZoom < -9 then
    Exit;
  FZoom := FZoom - 0.2;
  DrawImage;
end;

procedure TFrameImageInput.tbZoomInClick(Sender: TObject);
begin
  if FZoom > 9 then
    Exit;
  FZoom := FZoom + 0.2;
  DrawImage;
end;

procedure TFrameImageInput.tbZoomZeaoClick(Sender: TObject);
begin
  FZoom := 0;
  DrawImage;
end;

procedure TFrameImageInput.DrewArea;
begin
  if (X0 = X1) OR (Y0 = Y1) then
    Exit;
  Image1.Canvas.Pen.Mode := pmNotXor;
  Image1.Canvas.MoveTo(X0, Y0);
  Image1.Canvas.LineTo(X1, Y0);
  Image1.Canvas.LineTo(X1, Y1);
  Image1.Canvas.LineTo(X0, Y1);
  Image1.Canvas.LineTo(X0, Y0);
end;

procedure TFrameImageInput.ToolButton1Click(Sender: TObject);
begin
  with OpenPictureDialog1 do
    if Execute then
    begin
      LoadGraphicFile(Filename, 0, 0, FOrgBmp);
      FZoom := 0;
      DrawImage;
      Modified := True;
    end;
end;

procedure TFrameImageInput.ToolButton2Click(Sender: TObject);
var
  pic: TPicture;
begin     //ճ��
  if (ClipBoard.HasFormat(CF_PICTURE)) or (ClipBoard.HasFormat(CF_METAFILEPICT)) then
  begin
    pic := TPicture.Create;
    pic.Assign(ClipBoard);
    FOrgBmp.Height:= pic.Height;
    FOrgBmp.Width:= pic.Width;
    FOrgBmp.Canvas.Draw(0, 0, pic.Graphic);
    FZoom := 0;
    DrawImage;
    Modified := True;
    pic.Free;
  end;
end;

procedure TFrameImageInput.Image1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if ssLeft in Shift then
  begin
    X0 := X;
    Y0 := Y;
    X1 := X;
    Y1 := Y;
  end;
end;

procedure TFrameImageInput.Image1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  if ssLeft in Shift then
  begin
    DrewArea;
    X1 := X;
    Y1 := Y;
    DrewArea;
  end;
end;

procedure TFrameImageInput.Image1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  DrewArea;
  FOrgBmp.Width := X - X0 + 1;
  FOrgBmp.Height := Y - Y0 + 1;
  FOrgBmp.Canvas.FillRect(Rect(0, 0, X - X0 + 1, Y - Y0 + 1));
  FOrgBmp.Canvas.CopyRect(Rect(0, 0, X - X0 + 1, Y - Y0 + 1), Image1.Canvas,
    Rect(X0, Y0, X + 1, Y + 1));
  DrawImage;
end;

procedure TFrameImageInput.FrameResize(Sender: TObject);
begin
  DrawImage;
end;

end.
