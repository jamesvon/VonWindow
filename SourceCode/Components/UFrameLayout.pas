{*  @author jamesvon   @version 1.0.0.1
* UFrameLayout 一个进行组件布局的IDE设计界面组件，支持在页面内放置从
*   TLayoutItemBase继承的控件，同时支持在页面中进行画线
*==============================================================================*
* TFrameLayout 可布局控件设计组件                                              *
*==============================================================================*
    (p) ShowLinkLine: TLinkerLineStyle 控件间连线方式                        --------+
    (p) CanMove: Boolean               是否支持控件拖动                              |
    (p) CanResize: Boolean             是否支持改变控件尺寸                          |
    (p) BackgroundFile: TFilename      场景图片文件名                                |
    (p) Selected: TLayoutItemBase      当前选择的项目                                |
    (p) ItemImageFilename: string      组件图片文件名，内含正常和已选两个图标        |
    (p) Items[Index: Integer]: TLayoutItemBase                                       |
*------------------------------------------------------------------------------*     |
    (e) OnSelected: TNotifyEvent       控件选择后处理事件                            |
    (e) OnMoved: TNotifyEvent          控件移动后处理事件                            |
    (e) OnBarClick: TNotifyEvent       toolbar按钮点击处理事件                       |
    (e) OnItemResize: TNotifyEvent     控件尺寸变化时处理事件                        |
*------------------------------------------------------------------------------*     |
    (f) AddItem(Item: TLayoutItemBase): Integer;                                     |
    (m) DeleteItem(Index: Integer);                                                  |
    (m) DeleteItem(Item: TLayoutItemBase);                                           |
    (m) ClearItems;                                                                  |
    (m) Link(FromItem: TLayoutItemBase; ToItem: TLayoutItemBase): Integer;           |
    (m) DeleteLink(FromItem: TLayoutItemBase; ToItem: TLayoutItemBase);              |
    (m) EreasLink(Item: TLayoutItemBase; Kind: TLinkerLineKind): Integer;    -----+  |
    (m) EreasLine(Index: Integer);                                                |  |
    (m) EreasLine(X1, Y1, X2, Y2: Integer);                                       |  |
*==============================================================================*  |  |
* TLinkerLineKind 管理链接类型             <--------------------------------------+  |
    LS_ALL        所有链接                                                           |
    LS_FROMHERE   只包含从这里链接出去的链接                                         |
    LS_TOHERE     只包含从外面链接到这里的链接                                       |
*==============================================================================*     |
* TLinkerLineStyle 链接画线类型            <-----------------------------------------+
    LS_None       不链接
    LS_Straight   直线链接两个控件
    LS_Broken     折线链接两个控件
*==============================================================================*
* TLayoutItemBase 可布局控件的基类，基类完成拖动以及尺寸拖动的基本操作         *
*==============================================================================*
    (m) AddToolButton(ACaption: string; ImageIndex: Integer); //添加一个菜单按钮
    (m) DeleteBtn(Index: Integer);      //删除一个菜单按钮
*------------------------------------------------------------------------------*
    (p) obj: TObject                控件缓存对象
    (p) AutoSize: Boolean           工具图片自动调整控件大小
    (p) GridRect: Integer           移动时对齐的网格大小，默认值=5
    (p) CanMove: Boolean            是否支持拖动
    (p) CanResize: Boolean          是否支持尺寸大小
    (p) BarImages: TCustomImageList 按钮图片集
    (p) Background: TBitmap         背景
    (p) OnBarClick: TNotifyEvent    toolbar按钮点击处理事件
    (p) OnMoved: TNotifyEvent       控件移动后处理事件
*==============================================================================*
* TLayoutContentItem -> TLayoutItemBase 支持文字展示的可布局控件               *
*==============================================================================*
    (p) TextPos: TTextPosition      文字展示方式                               ---+
    (p) Text: string                要展示的文字，支持自动换行                    |
*==============================================================================*  |
* TTextPosition    文字展示方式            <--------------------------------------+
   +----------------+------------------+-----------------+
   | TP_LEFT_TOP    | TP_MIDDLE_TOP    | TP_RIGHT_TOP    |
   +----------------+------------------+-----------------+
   | TP_LEFT_CENTER | TP_MIDDLE_CENTER | TP_RIGHT_CENTER |
   +----------------+------------------+-----------------+
   | TP_LEFT_BOTTOM | TP_MIDDLE_BOTTOM | TP_RIGHT_BOTTOM |
   +----------------+------------------+-----------------+
*******************************************************************************}
unit UFrameLayout;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ButtonGroup, ComCtrls, ImgList, System.ImageList,
  UVonClass, UVonGraphicEx;

type
  TLinkerLineStyle = (LS_None, LS_Straight, LS_Broken, LS_ArcBroken, LS_Arrow);
  PLine = ^TLine;
  TFrameLayout = class;
  /// <summary>可拖放控件基类</summary>
  TLayoutItemBase = class(TCustomPanel)
  private
    /// <summary>Linkers from other items</summary>
    FSource: TList;
    FDestination: TList;
    FMoving: Boolean;
    FResizing: Boolean;
    FOldX: TPoint;
    FOldY: TPoint;
    ShapeList: Array [1 .. 8] of TShape;
    FRectList: array [1 .. 8] of TRect;
    FPosList: array [1 .. 8] of Integer;
    FBackground: TBitmap;
    FAutoSize: Boolean;
    FOnMoved: TNotifyEvent;
    FCanMove: Boolean;
    FCanResize: Boolean;
    FGridRect: Integer;
    FOnBarClick: TNotifyEvent;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); override;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure WmNcHitTest(var Msg: TWmNcHitTest); message wm_NcHitTest;
    procedure WmSize(var Msg: TWmSize); message wm_Size;
    procedure WmLButtonDown(var Msg: TWmLButtonDown); message wm_LButtonDown;
    procedure WmRButtonDown(var Msg: TWmRButtonDown); message wm_RButtonDown;
    procedure WmMove(var Msg: TWmMove); message Wm_Move;
    procedure SetBackground(const Value: TBitmap);
    procedure SetAutoSize(const Value: Boolean);
    procedure SetOnMoved(const Value: TNotifyEvent);
    procedure SetCanMove(const Value: Boolean);
    procedure SetCanResize(const Value: Boolean);
    procedure SetGridRect(const Value: Integer);
  private
    FObj: TObject;
    FX: Extended;
    FY: Extended;
    FRate: Extended;
    FOnRightClick: TNotifyEvent;
    FLayout: TFrameLayout;
    FAutoFreeObj: Boolean;
    FDesignerInfo: string;
    FImageIdx: Integer;
    procedure EventOfItemBtnClick(Sender: TObject);
    procedure SetObj(const Value: TObject);
    procedure SetX(const Value: Extended);
    procedure SetY(const Value: Extended);
    procedure SetRate(const Value: Extended);
    procedure SetOnRightClick(const Value: TNotifyEvent);
    function GetSourceCount: Integer;
    function GetSource(Index: Integer): TLayoutItemBase;
    function GetDestinationCount: Integer;
    function GetDestination(Index: Integer): TLayoutItemBase;
    procedure SetImageIdx(const Value: Integer);
  protected
    procedure Paint; override;
  public
    /// <summary>构造函数</summary>
    constructor Create(AOwner: TFrameLayout);
    /// <summary>析构函数</summary>
    destructor Destroy; override;
    /// <summary>得到一个指向自己的链接</summary>
    property Source[Index: Integer]: TLayoutItemBase read GetSource;
    /// <summary>得到一个自己指向的链接</summary>
    property Destination[Index: Integer]: TLayoutItemBase read GetDestination;
  published
    /// <summary>缓存对象</summary>
    property Obj: TObject read FObj write SetObj;
    /// <summary>释放时一并释放缓存对象</summary>
    property AutoFreeObj: Boolean read FAutoFreeObj write FAutoFreeObj;
    /// <summary>自动调整大小</summary>
    property AutoSize: Boolean read FAutoSize write SetAutoSize;
    /// <summary>移动时对齐的网格大小</summary>
    property GridRect: Integer read FGridRect write SetGridRect default 5;
    /// <summary>是否支持拖动</summary>
    property CanMove: Boolean read FCanMove write SetCanMove;
    /// <summary>是否支持尺寸大小</summary>
    property CanResize: Boolean read FCanResize write SetCanResize;
    /// <summary>背景</summary>
    property Background: TBitmap read FBackground write SetBackground;
    /// <summary>控件移动后处理事件</summary>
    property OnMoved: TNotifyEvent read FOnMoved write SetOnMoved;
    /// <summary>点击鼠标右键事件</summary>
    property OnRightClick: TNotifyEvent read FOnRightClick write SetOnRightClick;
    /// <summary>控件X位置</summary>
    property X: Extended read FX write SetX;
    /// <summary>控件Y位置</summary>
    property Y: Extended read FY write SetY;
    /// <summary>显示位置比例</summary>
    property Rate: Extended read FRate write SetRate;
    /// <summary>来源连接数量</summary>
    property SourceCount: Integer read GetSourceCount;
    /// <summary>目标连接数量</summary>
    property DestinationCount: Integer read GetDestinationCount;
    /// <summary>所属的展示层</summary>
    property Layout: TFrameLayout read FLayout;
    /// <summary>图标在Image List中的索引位置</summary>
    property ImageIdx: Integer read FImageIdx write SetImageIdx;
    /// <summary>文字</summary>
    property Caption;
    /// <summary>设计信息mary>
    property DesignerInfo: string read FDesignerInfo write FDesignerInfo;
    /// <summary>尺寸变化事件</summary>
    property OnResize;
    property OnClick;
  end;

  TLine = record
    FromItem, ToItem: TLayoutItemBase;
    Radius: Extended;
    Color: TColor;
  end;

  TLayoutItemClass = class of TLayoutItemBase;

  /// <summary>支持控件布局的frame</summary>
  TFrameLayout = class(TFrame)
    ScrollBox1: TScrollBox;
    ImageList1: TImageList;
    ImgBackground: TImage;
  private
    { Private declarations }
    FItemList: TList;
    FLineList: TList;
    FOnSelected: TNotifyEvent;
    FOnMoved: TNotifyEvent;
    FCanMove: Boolean;
    FShowLinkLine: TLinkerLineStyle;
    FCanResize: Boolean;
    procedure SetCanMove(const Value: Boolean);
    procedure SetOnMoved(const Value: TNotifyEvent);
    procedure SetOnSelected(const Value: TNotifyEvent);
    procedure SetShowLinkLine(const Value: TLinkerLineStyle);
    procedure SetCanResize(const Value: Boolean);
    procedure SetOnBarClick(const Value: TNotifyEvent);
    procedure SetOnItemResize(const Value: TNotifyEvent);
  private
    FOnBarClick: TNotifyEvent;
    FOnItemResize: TNotifyEvent;
    FSelected: TLayoutItemBase;
    FRate: Extended;
    FIsUpdate: Boolean;
    FBackground: TBitmap;
    FOnMouseRightClick: TNotifyEvent;
    FItemImageFilename: string;
    FItemImage, FItemSelectedImage: TBitmap;
    FOnItemRemove: TNotifyEvent;
    procedure EventOfItemMoved(Sender: TObject);
    procedure EventOfItemBarClick(Sender: TObject);
    procedure EventOfItemRightClick(Sender: TObject);
    procedure EventOfItemResize(Sender: TObject);
    function GetItems(Index: Integer): TLayoutItemBase;
    procedure SetRate(const Value: Extended);
    procedure SetBackground(const Value: TBitmap);
    procedure SetOnMouseRightClick(const Value: TNotifyEvent);
    procedure SetItemImageFilename(const Value: string);
    function GetItemCount: Integer;
    procedure SetSelected(const Value: TLayoutItemBase);
    function GetLinkCount(Item: TLayoutItemBase): Integer;
  public
    /// <summary>构造函数</summary>
    constructor Create(AOwner: TComponent); override;
    /// <summary>析构函数</summary>
    destructor Destroy; override;
    /// <summary>添加一个组件</summary>
    /// <returns>组件序号</returns>
    /// <param name="Item">组件，继承于TLayoutItemBase</param>
    function AddItem(Item: TLayoutItemBase): Integer; overload;
    /// <summary>添加一个组件</summary>
    /// <returns>组件实例</returns>
    /// <param name="ItemClass">组件类</param>
    function AddItem(ItemClass: TLayoutItemClass): TLayoutItemBase; overload;
    /// <summary>清除所有控件及连线</summary>
    procedure Clear;
    /// <summary>重画内容</summary>
    procedure DrawLines;
    /// <summary>删除一个组件</summary>
    /// <param name="Index">序号</param>
    procedure DeleteItem(Index: Integer); overload;
    /// <summary>删除一个组件</summary>
    /// <param name="Item">组件</param>
    procedure DeleteItem(Item: TLayoutItemBase); overload;
    /// <summary>建立两个组件的链接</summary>
    function Link(FromItem: TLayoutItemBase; ToItem: TLayoutItemBase;
      Radius: Extended = 0; Color: TColor = clBlack): Integer;
    /// <summary>删除一个组件出向或进向的链接</summary>
    procedure DeleteLinker(Item: TLayoutItemBase); overload;
    /// <summary>删除一个组件的链接</summary>
    procedure DeleteLinker(Item1, Item2: TLayoutItemBase); overload;
    /// <summary>查找某一个名称的组件</summary>
    function FindItem(ItemName: string): TLayoutItemBase;
    procedure EventOfItemClick(Sender: TObject);
    procedure BeginUpdate;
    procedure EndUpdate;
    /// <summary>获取某一序号组件</summary>
    property Items[Index: Integer]: TLayoutItemBase read GetItems;
    property LinkCount[Item: TLayoutItemBase]: Integer read GetLinkCount;
  published
    /// <summary>控件间连线方式</summary>
    property ShowLinkLine: TLinkerLineStyle read FShowLinkLine write SetShowLinkLine default LS_None;
    /// <summary>是否支持控件拖动</summary>
    property CanMove: Boolean read FCanMove write SetCanMove;
    /// <summary>是否支持改变控件尺寸</summary>
    property CanResize: Boolean read FCanResize write SetCanResize;
    /// <summary>显示位置比例</summary>
    property Rate: Extended read FRate write SetRate;
    /// <summary>场景图片文件名</summary>
    property Background: TBitmap read FBackground write SetBackground;
    /// <summary>项目背景图，支持2个图片组合</summary>
    property ItemImageFilename: string read FItemImageFilename write SetItemImageFilename;
    /// <summary>当前选择的控件，没有则为nil</summary>
    property Selected: TLayoutItemBase read FSelected write SetSelected;
    /// <summary>控件选择后处理事件</summary>
    property OnSelected: TNotifyEvent read FOnSelected write SetOnSelected;
    /// <summary>控件选择后处理事件</summary>
    property OnMouseRightClick: TNotifyEvent read FOnMouseRightClick write SetOnMouseRightClick;
    /// <summary>控件移动后处理事件</summary>
    property OnMoved: TNotifyEvent read FOnMoved write SetOnMoved;
    /// <summary>toolbar按钮点击处理事件</summary>
    property OnBarClick: TNotifyEvent read FOnBarClick write SetOnBarClick;
    /// <summary>控件尺寸变化时处理事件</summary>
    property OnItemResize: TNotifyEvent read FOnItemResize write SetOnItemResize;
    /// <summary>节点删除事件</summary>
    property OnItemRemove: TNotifyEvent read FOnItemRemove write FOnItemRemove;
    /// <summary>控件数量</summary>
    property ItemCount: Integer read GetItemCount;
  end;

  TTextPosition = (TP_LEFT_TOP, TP_LEFT_CENTER, TP_LEFT_BOTTOM,
                   TP_MIDDLE_TOP, TP_MIDDLE_CENTER, TP_MIDDLE_BOTTOM,
                   TP_RIGHT_TOP, TP_RIGHT_CENTER, TP_RIGHT_BOTTOM, TP_FILL);

  TLayoutBarToolItem = class(TLayoutItemBase)
  private
    FToolBar: TToolBar;
    procedure SetOnBarClick(const Value: TNotifyEvent);
    procedure SetBarImages(const Value: TCustomImageList);
    function GetBarImages: TCustomImageList;
    procedure Paint; override;
  public
    /// <summary>构造函数</summary>
    constructor Create(AOwner: TFrameLayout);
    /// <summary>析构函数</summary>
    destructor Destroy; override;
    /// <summary>添加一个菜单按钮</summary>
    procedure AddToolButton(ACaption: string; ImageIndex: Integer);
    /// <summary>删除一个菜单按钮</summary>
    procedure DeleteBtn(Index: Integer);
  published
    /// <summary>按钮图片集</summary>
    property BarImages: TCustomImageList read GetBarImages write SetBarImages;
    /// <summary>toolbar按钮点击处理事件</summary>
    property OnBarClick: TNotifyEvent read FOnBarClick write SetOnBarClick;
  end;

  TLayoutContentItem = class(TLayoutItemBase)
  private
    FTextPos: TTextPosition;
    FText: string;
    procedure SetTextPos(Value: TTextPosition);
    procedure SetText(Value: string);
  protected
    procedure Paint; override;
  published
    property TextPos: TTextPosition read FTextPos write SetTextPos default TP_MIDDLE_CENTER;
    property Text: string read FText write SetText;
  end;

  TLayoutTextRectItem = class(TLayoutItemBase)
  private
    FTextPos: TTextPosition;
    FText: string;
    FFontStyle: TFontStyles;
    FFontName: string;
    FFontColor: TColor;
    FFontSize: Integer;
    procedure SetTextPos(Value: TTextPosition);
    procedure SetText(Value: string);
  protected
    procedure Paint; override;
  published
    property TextPos: TTextPosition read FTextPos write SetTextPos default TP_MIDDLE_CENTER;
    property Text: string read FText write SetText;
    property FontName: string read FFontName write FFontName;
    property FontSize: Integer read FFontSize write FFontSize;
    property FontStyle: TFontStyles read FFontStyle write FFontStyle;
    property FontColor: TColor read FFontColor write FFontColor;
  end;

  TLayoutImageItem = class(TLayoutItemBase)
  private
    FAdaption: Boolean;
    FFilename: string;
    FAutoStretch: Boolean;
  protected
    procedure Paint; override;
  published
    property Filename: string read FFilename write FFilename;
    property AutoStretch: Boolean read FAutoStretch write FAutoStretch;
    property Adaption: Boolean read FAdaption write FAdaption;
  end;

implementation

uses Math;

{$R *.dfm}

{ TLayoutItemBase }

constructor TLayoutItemBase.Create(AOwner: TFrameLayout);
var
  I: Integer;
begin
  inherited Create(nil);
  FLayout:= AOwner;
  FSource:= TList.Create;
  FDestination:= TList.Create;
  FBackground:= TBitmap.Create;
  FGridRect:= 5;
  FRate:= 1;
  ShowCaption:= False;
  FullRepaint := False;
end;

destructor TLayoutItemBase.Destroy;
begin
  if FAutoFreeObj and Assigned(FObj) then FObj.Free;
  FBackground.Free;
  FSource.Free;
  FDestination.Free;
  inherited;
end;

(* 边界+移动处理事件 *)

procedure TLayoutItemBase.WmNcHitTest(var Msg: TWmNcHitTest);
var
  Pt: TPoint;
  I: Integer;
begin
  //if not CanResize then Exit;
  Pt := Point(Msg.XPos, Msg.YPos);
  Pt := ScreenToClient(Pt);
  Msg.Result := 0;
  for I := 1 to 8 do
    if PtInRect(FRectList[I], Pt) then
      Msg.Result := FPosList[I];
  if Msg.Result = 0 then
    inherited;
  if FResizing and Assigned(OnResize) then begin
    FX:= Round((Left + Width / 2) * FRate);
    FY:= Round((Top + Height / 2) * FRate);
    OnResize(Self);
  end;
  FResizing:= False;
end;

procedure TLayoutItemBase.WmRButtonDown(var Msg: TWmRButtonDown);
begin
  inherited;
  if Assigned(FOnRightClick) then
    FOnRightClick(Self);
end;

procedure TLayoutItemBase.WmSize(var Msg: TWmSize);
begin
  if not CanResize then Exit;
  FRectList[1] := Rect(0, 0, 5, 5);
  FRectList[2] := Rect(Width div 2 - 3, 0, Width div 2 + 2, 5);
  FRectList[3] := Rect(Width - 5, 0, Width, 5);
  FRectList[4] := Rect(Width - 5, height div 2 - 3, Width, height div 2 + 2);
  FRectList[5] := Rect(Width - 5, height - 5, Width, height);
  FRectList[6] := Rect(Width div 2 - 3, height - 5, Width div 2 + 2, height);
  FRectList[7] := Rect(0, height - 5, 5, height);
  FRectList[8] := Rect(0, height div 2 - 3, 5, height div 2 + 2);
  //Paint;
  //FResizing:= True;
end;

procedure TLayoutItemBase.WmLButtonDown(var Msg: TWmLButtonDown);
begin
  inherited;
end;

procedure TLayoutItemBase.WmMove(var Msg: TWmMove);
var
  R: TRect;
begin
  inherited;
//  if not CanResize then Exit;
//  R := BoundsRect;
//  InflateRect(R, -2, -2);
//  Left:= (Left div 5) * 5;   FX:= (Left + Width / 2) * FRate;
//  Top:= (Top div 5) * 5;     FY:= (Top + Height / 2) * FRate;
end;

procedure TLayoutItemBase.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited;
  if not FCanMove then Exit;
  FMoving := True;
  FOldX := Point(X, Y);
  SetFocus;
  Paint;
end;

procedure TLayoutItemBase.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  NewPoint: TPoint;

  function findnearest(X, Y: Integer): TPoint;
  begin
    Result.X := (X div FGridRect) * FGridRect;// + Round((X mod 5) / 5) * 5;
    Result.Y := (Y div FGridRect) * FGridRect;// + Round((Y mod 5) / 5) * 5;
  end;

begin
  inherited;
  if FCanMove and FMoving then
  begin
    NewPoint := findnearest(Left + X - FOldX.X, Top + Y - FOldX.Y);
    with Self do
      SetBounds(NewPoint.X, NewPoint.Y, Width, height);
  end;
end;

procedure TLayoutItemBase.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited;
  if FCanMove and FMoving then begin
    Left:= (Left div 5) * 5;   Top:= (Top div 5) * 5;
    FX:= (Left + Width / 2 + TScrollBox(Parent).HorzScrollBar.ScrollPos) * FRate;
    FY:= (Top + Height / 2 + TScrollBox(Parent).VertScrollBar.ScrollPos) * FRate;
    if Assigned(FOnMoved) then FOnMoved(Self);
    Paint;
  end;
  FMoving := False;
end;

procedure TLayoutItemBase.CMExit(var Message: TCMExit);
begin
  Inherited;
  if not CanResize then Exit;
  Paint;
end;

(* Methods *)

procedure TLayoutItemBase.EventOfItemBtnClick(Sender: TObject);
begin
  if Assigned(FOnBarClick) then
    FOnBarClick(Sender);
end;

procedure TLayoutItemBase.Paint;
var
  I, baseH: Integer;
begin
  inherited;
  Canvas.Brush.Color := FBackground.Canvas.Pixels[0,0];
  Canvas.Brush.Style := bsClear;
  Canvas.Pen.Color := Self.Color;
  Canvas.FillRect(Rect(0, 0, BoundsRect.right, BoundsRect.Bottom));
  if AutoSize then begin
    if FBackground.Width > 0 then Width := FBackground.Width else Width:= 20;
    if FBackground.Height > 0 then Height := FBackground.Height else Height:= 20;
    FBackground.Transparent := True;
    FBackground.TransParentColor := FBackground.Canvas.Pixels[0,0];
    Canvas.Draw(0, 0, FBackground);
  end else begin
    Width := ((BoundsRect.right - BoundsRect.Left) div 5) * 5;
    Height := ((BoundsRect.Bottom - BoundsRect.Top) div 5) * 5;
    FBackground.Transparent := True;
    FBackground.TransParentColor := FBackground.Canvas.Pixels[0,0];
    Canvas.Draw((Width - FBackground.Width) div 2, (Height - FBackground.Height) div 2, FBackground);
  end;

  if Focused then
  begin
    Canvas.Brush.Color := $FFFFFF - Self.Color;
    Canvas.Pen.Color := $FFFFFF - Self.Color;
  end
  else
  begin
    Canvas.Brush.Color := Self.Color;
    Canvas.Pen.Color := Self.Color;
  end;
  if FCanResize then
    for I := 1 to 8 do
      with FRectList[I] do
        Canvas.RecTangle(Left, Top, right, Bottom);
end;

(* 属性 *)

function TLayoutItemBase.GetDestination(Index: Integer): TLayoutItemBase;
begin
  Result := TLayoutItemBase(FDestination[Index]);
end;

function TLayoutItemBase.GetDestinationCount: Integer;
begin
  Result := FDestination.Count;
end;

function TLayoutItemBase.GetSource(Index: Integer): TLayoutItemBase;
begin
  Result := TLayoutItemBase(FSource[Index]);
end;

function TLayoutItemBase.GetSourceCount: Integer;
begin
  Result := FSource.Count;
end;

procedure TLayoutItemBase.SetAutoSize(const Value: Boolean);
var
  I: Integer;
begin
  FAutoSize := Value;
  if FAutoSize then CanResize:= False
end;

procedure TLayoutItemBase.SetBackground(const Value: TBitmap);
begin
  FBackground.Assign(Value);
  FBackground.Transparent:= True;
  if AutoSize then begin
    Locked:= True;
    if Value.Height > 0 then Height:= Value.Height else Height:= 20;
    if Value.Width > 0 then Width:= Value.Width else Width:= 20;
    Locked:= False;
    Paint;
  end;
end;

procedure TLayoutItemBase.SetCanMove(const Value: Boolean);
begin
  FCanMove := Value;
end;

procedure TLayoutItemBase.SetCanResize(const Value: Boolean);
var
  I: Integer;
begin
  FCanResize := Value;
  if FCanResize then begin
    FAutoSize:= False;
    FPosList[1] := htTopLeft;
    FPosList[2] := htTop;
    FPosList[3] := htTopRight;
    FPosList[4] := htRight;
    FPosList[5] := htBottomRight;
    FPosList[6] := htBottom;
    FPosList[7] := htBottomLeft;
    FPosList[8] := htLeft;
    for I := 1 to 8 do
    begin
      ShapeList[I] := TShape.Create(Self);
      ShapeList[I].Parent := Self;
      ShapeList[I].Brush.Color := not Color;
      ShapeList[I].Visible := False;
    end;
  end else begin
    for I := 1 to 8 do
      if Assigned(ShapeList[I]) then ShapeList[I].Free;
  end;
end;

procedure TLayoutItemBase.SetGridRect(const Value: Integer);
begin
  FGridRect := Value;
end;

procedure TLayoutItemBase.SetImageIdx(const Value: Integer);
begin
  //if FImageIdx = Value then Exit;
  FImageIdx := Value;
  if FImageIdx < Layout.ImageList1.Count then
    Layout.ImageList1.GetBitmap(FImageIdx, Background);
end;

procedure TLayoutItemBase.SetObj(const Value: TObject);
begin
  FObj := Value;
end;

procedure TLayoutItemBase.SetOnMoved(const Value: TNotifyEvent);
begin
  FOnMoved := Value;
end;

procedure TLayoutItemBase.SetOnRightClick(const Value: TNotifyEvent);
begin
  FOnRightClick := Value;
end;

procedure TLayoutItemBase.SetRate(const Value: Extended);
begin
  if FRate = Value then Exit;
  FRate := Value;
  Left:= Round(FX / FRate - Width / 2 - TScrollBox(Parent).HorzScrollBar.ScrollPos);
  Top:= Round(FY / FRate - Height / 2 - TScrollBox(Parent).VertScrollBar.ScrollPos);
end;

procedure TLayoutItemBase.SetX(const Value: Extended);
begin
  FX := Value;
  Left:= Round(FX / FRate - Width / 2 - TScrollBox(Parent).HorzScrollBar.ScrollPos);
end;

procedure TLayoutItemBase.SetY(const Value: Extended);
begin
  FY := Value;
  Top:= Round(FY / FRate - Height / 2 - TScrollBox(Parent).VertScrollBar.ScrollPos);
end;

{ TFrameLayout }  (************************************************************)

constructor TFrameLayout.Create(AOwner: TComponent);
begin
  inherited;
  FItemList:= TList.Create;
  FLineList:= TList.Create;
  FBackground:= TBitmap.Create;
  FRate:= 1;
end;

destructor TFrameLayout.Destroy;
begin
  if Assigned(FItemImage) then FItemImage.Free;
  if Assigned(FItemSelectedImage) then FItemSelectedImage.Free;
  FBackground.Free;
  FLineList.Free;
  FItemList.Free;
  inherited;
end;

(* Methods *)

function TFrameLayout.AddItem(Item: TLayoutItemBase): Integer;
var
  ARect : TRect;
begin
  if Assigned(FSelected)and Assigned(FItemImage) then begin
    FSelected.Background.Assign(FItemImage);
    if not FIsUpdate then FSelected.Repaint;
  end;
  FSelected:= Item;
  if Assigned(FItemSelectedImage) then
    FSelected.Background.Assign(FItemSelectedImage);
  with Item do begin
    if not FIsUpdate then Parent := ScrollBox1;
    ARect := BoundsRect;
    InflateRect(ARect,5,5);
    BoundsRect := ARect;
    ShowHint := True;
    BevelOuter := bvNone;
    Self.Cursor := crDefault;
    CanMove:= Self.CanMove;
    CanResize:= Self.CanResize;
    //SetFocus;
    Rate:= Self.FRate;
    OnMoved:= EventOfItemMoved;
    OnClick:= EventOfItemClick;
    OnRightClick:= EventOfItemRightClick;
    OnBarClick:= EventOfItemBarClick;
    OnResize:= EventOfItemResize;
    Result:= FItemList.Add(Item);
    if not FIsUpdate then Paint;
  end;
end;

function TFrameLayout.AddItem(ItemClass: TLayoutItemClass): TLayoutItemBase;
begin
  Result:= ItemClass.Create(Self);
  Result.Rate:= FRate;
  AddItem(Result);
end;

procedure TFrameLayout.BeginUpdate;
begin
  FIsUpdate:= True;
end;

procedure TFrameLayout.EndUpdate;
var
  I: Integer;
begin
  FIsUpdate:= False;
  for I := FItemList.Count - 1 downto 0 do
    TLayoutItemBase(FItemList[I]).Parent:= ScrollBox1;
end;

procedure TFrameLayout.Clear;
var
  I: Integer;
begin
  for I := FItemList.Count - 1 downto 0 do begin
    if Assigned(FOnItemRemove) then
      FOnItemRemove(FItemList[I]);
    TLayoutItemBase(FItemList[I]).Free;
  end;
  FItemList.Clear;
  FSelected:= nil;
  for I := FLineList.Count - 1 downto 0 do
    FreeMem(FLineList[I]);
  FLineList.Clear;
  DrawLines;
end;

procedure TFrameLayout.DeleteItem(Index: Integer);
var
  I: Integer;
  Item: TLayoutItemBase;
begin
  Item:= TLayoutItemBase(FItemList[Index]);
  DeleteItem(Item);
  DrawLines;
end;

procedure TFrameLayout.DeleteItem(Item: TLayoutItemBase);
begin
  if Selected = Item then FSelected:= nil;
  DeleteLinker(Item);
  if Assigned(FOnItemRemove) then
    FOnItemRemove(Item);
  FItemList.Remove(Item);
  Item.Free;
  DrawLines;
end;

procedure TFrameLayout.DeleteLinker(Item: TLayoutItemBase);
var
  I: Integer;
begin
  for I := FLineList.Count - 1 downto 0 do
    if PLine(FLineList[I]).FromItem = Item then begin
      FreeMem(FLineList[I]); FLineList.Delete(I);
    end else if PLine(FLineList[I]).ToItem = Item then begin
      FreeMem(FLineList[I]); FLineList.Delete(I);
    end;
  DrawLines;
end;

procedure TFrameLayout.DeleteLinker(Item1, Item2: TLayoutItemBase);
var
  I: Integer;
  pl: PLine;
begin
  for I := FLineList.Count - 1 downto 0 do begin
    pl:= PLine(FLineList[I]);
    if((pl.FromItem = Item1)and(pl.ToItem = Item2))or
      ((pl.ToItem = Item1)and(pl.FromItem = Item2))then begin
      FreeMem(FLineList[I]); FLineList.Delete(I);
    end;
  end;
  DrawLines;
end;

procedure TFrameLayout.DrawLines;
var
  I, K: Integer;
  pl: PLine;
  A, szD: Extended;
  /// <summary>移到某个地方</summary>
  procedure MoveIt(X, Y: Extended);
  begin
    ImgBackground.Canvas.MoveTo(Round(X * FRate), Round(Y * FRate));
  end;
  /// <summary>连接到某个地方</summary>
  procedure LineIt(X, Y: Extended);
  begin
    ImgBackground.Canvas.LineTo(Round(X * FRate), Round(Y * FRate));
  end;
  /// <summary>转弯</summary>
  procedure ArcClock(X0, Y0, X1, Y1, R: Extended; StartAngle, SweepAngle: Single);
  begin
    ImgBackground.Canvas.LineTo(Round(X1 * FRate), Round(Y1 * FRate));
    ImgBackground.Canvas.AngleArc(Round(X0 * FRate), Round(Y0 * FRate), Round(R * FRate), StartAngle, SweepAngle);
  end;
  /// <summary>逆时针即左转</summary>
  procedure ArcAntiClock(X1, Y1, X2, Y2, R: Extended);
  begin
    MoveIt(X1, Y1);
    if(X2 < X1)and(Y2 < Y1)then ArcClock(X1 - R, Y2 + R, X1, Y2 + R, R, 0, 90);    //.2\17
    if(X2 < X1)and(Y2 > Y1)then ArcClock(X2 + R, Y1 + R, X2 + R, Y1, R, 90, 90);   //2/1.{
    if(X2 > X1)and(Y2 > Y1)then ArcClock(X1 + R, Y2 - R, X1, Y2 - R, R, 180, 90);  //1\2`L
    if(X2 > X1)and(Y2 < Y1)then ArcClock(X2 - R, Y1 - R, X2 - R, Y1, R, 270, 90);  //`1/2}
    LineIt(X2, Y2);
  end;
  /// <summary>顺时针即右转</summary>
  procedure ArcClockwise(X1, Y1, X2, Y2, R: Extended);
  begin
    MoveIt(X1, Y1);
    if(X2 < X1)and(Y2 < Y1)then ArcClock(X2 + R, Y1 - R, X2 + R, Y1, R, 270, -90); //2\1`L
    if(X2 < X1)and(Y2 > Y1)then ArcClock(X1 - R, Y2 - R, X1, Y2 - R, R, 0, -90);   //`2/1}
    if(X2 > X1)and(Y2 > Y1)then ArcClock(X2 - R, Y1 + R, X2 - R, Y1, R, 90, -90);  //.1\27
    if(X2 > X1)and(Y2 < Y1)then ArcClock(X1 + R, Y2 + R, X1, Y2 + R, R, 180, -90); //1/2.{
    LineIt(X2, Y2);
  end;
begin
  if Assigned(FBackground) then
    ImgBackground.Picture.Bitmap.Assign(FBackground)
  else begin
    ImgBackground.Picture.Bitmap.Width:= ImgBackground.Width;
    ImgBackground.Picture.Bitmap.Height:= ImgBackground.Height;
    ImgBackground.Picture.Bitmap.Canvas.FillRect(Rect(0, 0, ImgBackground.Width, ImgBackground.Height));
  end;
  if FShowLinkLine = TLinkerLineStyle.LS_None then Exit;
  for I := 0 to FLineList.Count - 1 do begin
    pl:= PLine(FLineList[I]);
    with ImgBackground.Canvas do begin
      Pen.Color:= pl.Color;
      Pen.Width:= 5;
      case FShowLinkLine of
      TLinkerLineStyle.LS_Straight: begin    //直线连接
          MoveIt(pl.FromItem.X, pl.FromItem.Y);
          LineIt(pl.ToItem.X, pl.ToItem.Y);
        end;
      TLinkerLineStyle.LS_Broken: begin      //折线链接
          {$region '折线链接'}
          MoveIt(pl.FromItem.X, pl.FromItem.Y);
          if(pl.ToItem.X = pl.FromItem.X)or(pl.ToItem.Y = pl.FromItem.Y)or(pl.Radius = 0)then
            LineIt(pl.ToItem.X, pl.ToItem.Y) //直线
          else if pl.Radius > 0 then begin   //左转
            if(pl.ToItem.Y - pl.FromItem.Y)/(pl.ToItem.X - pl.FromItem.X) > 0 then
              LineIt(pl.FromItem.X, pl.ToItem.Y)
            else LineIt(pl.ToItem.X, pl.FromItem.Y);
          end else begin                     //右转
            if(pl.ToItem.Y - pl.FromItem.Y)/(pl.ToItem.X - pl.FromItem.X) > 0 then
              LineIt(pl.ToItem.X, pl.FromItem.Y)
            else LineIt(pl.FromItem.X, pl.ToItem.Y);
          end;
          LineIt(pl.ToItem.X, pl.ToItem.Y);
          {$endregion}
        end;
      TLinkerLineStyle.LS_ArcBroken: begin   //弧线连接
          {$region '弧线连接'}
          MoveIt(pl.FromItem.X, pl.FromItem.Y);
          if(pl.ToItem.X = pl.FromItem.X)or(pl.ToItem.Y = pl.FromItem.Y)or(pl.Radius = 0)then     //直线
            LineIt(pl.ToItem.X, pl.ToItem.Y)
          else if pl.Radius > 0 then         //左转
            ArcAntiClock(pl.FromItem.X, pl.FromItem.Y, pl.ToItem.X, pl.ToItem.Y, pl.Radius)
          else ArcClockwise(pl.FromItem.X, pl.FromItem.Y, pl.ToItem.X, pl.ToItem.Y, - pl.Radius);
          {$endregion}
        end;
      TLinkerLineStyle.LS_Arrow: begin       //箭头连接
          MoveIt(pl.FromItem.X, pl.FromItem.Y);
          LineIt(pl.ToItem.X, pl.ToItem.Y);
          A:= ArcSin((pl.ToItem.Y - pl.FromItem.Y)/(pl.ToItem.X - pl.FromItem.X));
          if(A < pi /2)and(A > -pi / 2) then begin
            szD:= pl.ToItem.Top + pl.ToItem.Width * Tangent(A) + pl.ToItem.Height div 2;
            MoveTo(pl.ToItem.Left, Round(szD));
            LineTo(pl.ToItem.Left - Round(10 * Cos(A - 0.08726646259971647884618453842443)),
                   Round(szD + 10 * Sin(A - 0.08726646259971647884618453842443)));
            LineTo(pl.ToItem.Left - Round(10 * Cos(A - 0.08726646259971647884618453842443)),
                   Round(szD + 10 * Sin(A + 0.08726646259971647884618453842443)));
            LineTo(pl.ToItem.Left, Round(szD));
          end;
        end;
      end;
    end;
  end;
end;

function TFrameLayout.Link(FromItem: TLayoutItemBase; ToItem: TLayoutItemBase;
  Radius: Extended; Color: TColor): Integer;
var
  pl: PLine;
begin
  new(Pl);
  Pl.FromItem:= FromItem;
  pl.ToItem:= ToItem;
  pl.Radius:= Radius;
  pl.Color:= Color;
  FLineList.Add(pl);
  DrawLines;
end;

function TFrameLayout.FindItem(ItemName: string): TLayoutItemBase;
var
  I: Integer;
begin
  for I := 0 to FItemList.Count - 1 do
    if SameText(TLayoutItemBase(FItemList[I]).Name, ItemName)then begin
      Result:= TLayoutItemBase(FItemList[I]);
      Exit;
    end;
  Result:= nil;
end;

(* Events of items *)

procedure TFrameLayout.EventOfItemBarClick(Sender: TObject);
begin
  if Assigned(FOnBarClick) then FOnBarClick(Sender);
end;

procedure TFrameLayout.EventOfItemClick(Sender: TObject);
begin
  if Assigned(FSelected)and Assigned(FItemImage) then begin
    FSelected.Background.Assign(FItemImage);
    FSelected.Repaint;
  end;
  FSelected:= TLayoutItemBase(Sender);
  if Assigned(FItemSelectedImage) then
    FSelected.Background.Assign(FItemSelectedImage);
  if Assigned(FOnSelected) then FOnSelected(Sender);
  FSelected.Repaint;
end;

procedure TFrameLayout.EventOfItemMoved(Sender: TObject);
begin
  if Assigned(FOnMoved) then FOnMoved(Sender);
  DrawLines;
end;

procedure TFrameLayout.EventOfItemResize(Sender: TObject);
begin
  if Assigned(FOnItemResize) then FOnItemResize(Sender);
  DrawLines;
end;

procedure TFrameLayout.EventOfItemRightClick(Sender: TObject);
begin
  if Assigned(FOnMouseRightClick) then
    FOnMouseRightClick(Sender);
end;

(* Properties *)

function TFrameLayout.GetLinkCount(Item: TLayoutItemBase): Integer;
var
  I: Integer;
begin
  Result:= 0;
  for I := 0 to FLineList.Count - 1 do
    if(PLine(FLineList[I]).FromItem = Item)or(PLine(FLineList[I]).ToItem = Item)then
      Inc(Result);
end;

function TFrameLayout.GetItemCount: Integer;
begin
  Result:= FItemList.Count;
end;

function TFrameLayout.GetItems(Index: Integer): TLayoutItemBase;
begin
  Result := TLayoutItemBase(FItemList[Index]);
end;

procedure TFrameLayout.SetBackground(const Value: TBitmap);
begin
  FBackground.Assign(Value);
  ImgBackground.Picture.Bitmap.Assign(FBackground);
end;

procedure TFrameLayout.SetCanMove(const Value: Boolean);
var
  I: Integer;
begin
  FCanMove := Value;
  for I := 0 to FItemList.Count - 1 do
    TLayoutItemBase(FItemList[i]).CanMove:= Value;
end;

procedure TFrameLayout.SetCanResize(const Value: Boolean);
var
  I: Integer;
begin
  FCanResize := Value;
  for I := 0 to FItemList.Count - 1 do
    TLayoutItemBase(FItemList[i]).CanResize:= Value;
end;

procedure TFrameLayout.SetItemImageFilename(const Value: string);
var
  bmp: TBitmap;
  I: Integer;
begin
  FItemImageFilename := Value;
  if Assigned(FItemImage) then FItemImage.Free;
  if Assigned(FItemSelectedImage) then FItemSelectedImage.Free;
  FItemImage:= TBitmap.Create;
  FItemSelectedImage:= TBitmap.Create;
  bmp:= TBitmap.Create;
  bmp.LoadFromFile(Value);
  FItemImage.Width:= bmp.Width div 2;
  FItemImage.Height:= bmp.Height;
  FItemSelectedImage.Width:= FItemImage.Width;
  FItemSelectedImage.Height:= FItemImage.Height;
  FItemImage.Canvas.CopyRect(Rect(0, 0, FItemImage.Width, FItemImage.Height),
    bmp.Canvas, Rect(0, 0, FItemImage.Width, FItemImage.Height));
  FItemSelectedImage.Canvas.CopyRect(Rect(0, 0, FItemImage.Width, FItemImage.Height),
    bmp.Canvas, Rect(FItemImage.Width, 0, bmp.Width, bmp.Height));
  bmp.Free;
  for I := 0 to FItemList.Count - 1 do
    TLayoutItemBase(FItemList[I]).Background.Assign(FItemImage);
  if Assigned(FSelected) then
    FSelected.Background.Assign(FItemSelectedImage);
end;

procedure TFrameLayout.SetOnBarClick(const Value: TNotifyEvent);
begin
  FOnBarClick := Value;
end;

procedure TFrameLayout.SetOnItemResize(const Value: TNotifyEvent);
begin
  FOnItemResize := Value;
end;

procedure TFrameLayout.SetOnMouseRightClick(const Value: TNotifyEvent);
begin
  FOnMouseRightClick := Value;
end;

procedure TFrameLayout.SetOnMoved(const Value: TNotifyEvent);
begin
  FOnMoved := Value;
end;

procedure TFrameLayout.SetOnSelected(const Value: TNotifyEvent);
begin
  FOnSelected := Value;
end;

procedure TFrameLayout.SetRate(const Value: Extended);
var
  I: Integer;
begin
  if FRate = Value then Exit;
  for I := 0 to FItemList.Count - 1 do
    TLayoutItemBase(FItemList[I]).Rate:= Value;
  FRate := Value;
  DrawLines;
end;

procedure TFrameLayout.SetSelected(const Value: TLayoutItemBase);
begin
  FSelected := Value;
end;

procedure TFrameLayout.SetShowLinkLine(const Value: TLinkerLineStyle);
begin
  FShowLinkLine := Value;
  DrawLines;
end;

{ TLayoutContentItem }

procedure TLayoutContentItem.Paint;
var
  I, fontH, BaseH: Integer;
  txtList: TStringList;

  procedure DisplayName(Idx: Integer);
  var
    mPos, oPos: Integer;
    szS: string;
  begin
    mPos:= 1;   szS:= txtList[Idx];
    while(mPos <= Length(szS))and(Width > 16)do begin
      if(Canvas.TextWidth(Copy(szS, 1, mPos)) > Width)then begin
        txtList[Idx]:= Copy(szS, 1, mPos - 1);
        szS:= Copy(szS, mPos, MaxInt);
        txtList.Insert(Idx + 1, szS);
      end else Inc(mPos);
    end;
    if mPos = 1 then Exit;
  end;
begin
  Locked:= true;
  Canvas.Lock;
  inherited;
  txtList:= TStringList.Create;
  txtList.Text:= FText;
  if Focused then
  begin
    Canvas.Brush.Color := $FFFFFF - Self.Color;
    Canvas.Pen.Color := $FFFFFF - Self.Color;
  end
  else
  begin
    Canvas.Brush.Color := Self.Color;
    Canvas.Pen.Color := Self.Color;
  end;
  Canvas.Brush.Style := bsClear;
  Canvas.Font.Assign(Font);
  for I := txtList.Count - 1 downto 0 do
    DisplayName(I);
  fontH:= Round(Canvas.TextHeight('W') * 1.5);
  case FTextPos of
  TP_LEFT_TOP: for I := 0 to txtList.Count - 1 do Canvas.TextOut(0, I * fontH + BaseH, txtList[I]);
  TP_MIDDLE_TOP: for I := 0 to txtList.Count - 1 do Canvas.TextOut((Width - Canvas.TextWidth(txtList[I])) div 2, Round(I * fontH) + BaseH, txtList[I]);
  TP_RIGHT_TOP: for I := 0 to txtList.Count - 1 do Canvas.TextOut(Width - Canvas.TextWidth(txtList[I]), Round(I * fontH) + BaseH, txtList[I]);
  TP_LEFT_CENTER: for I := 0 to txtList.Count - 1 do Canvas.TextOut(0, Round((Height - BaseH - txtList.Count * fontH)/2 + I * fontH) + BaseH, txtList[I]);
  TP_MIDDLE_CENTER: for I := 0 to txtList.Count - 1 do Canvas.TextOut((Width - Canvas.TextWidth(txtList[I])) div 2, Round((Height - BaseH- txtList.Count * fontH)/2 + I * fontH) + BaseH, txtList[I]);
  TP_RIGHT_CENTER: for I := 0 to txtList.Count - 1 do Canvas.TextOut(Width - Canvas.TextWidth(txtList[I]), Round((Height - BaseH - txtList.Count * fontH)/2 + I * fontH) + BaseH, txtList[I]);
  TP_LEFT_BOTTOM: for I := 0 to txtList.Count - 1 do Canvas.TextOut(0, Height - txtList.Count * fontH + I * fontH, txtList[I]);
  TP_MIDDLE_BOTTOM: for I := 0 to txtList.Count - 1 do Canvas.TextOut((Width - Canvas.TextWidth(txtList[I])) div 2, Height - txtList.Count * fontH + I * fontH, txtList[I]);
  TP_RIGHT_BOTTOM: for I := 0 to txtList.Count - 1 do Canvas.TextOut(Width - Canvas.TextWidth(txtList[I]), Height - txtList.Count * fontH + I * fontH, txtList[I]);
  end;
  Canvas.Unlock;
  txtList.Free;
  Locked:= False;
end;

procedure TLayoutContentItem.SetText(Value: string);
begin
  FText:= Value;
end;

procedure TLayoutContentItem.SetTextPos(Value: TTextPosition);
begin
  FTextPos:= Value;
end;

{ TLayoutBarToolItem }

constructor TLayoutBarToolItem.Create(AOwner: TFrameLayout);
begin
  inherited;
  FToolBar:= TToolBar.Create(self);
  FToolBar.Parent:= Self;
  FToolBar.AutoSize:= true;
end;

destructor TLayoutBarToolItem.Destroy;
begin
  FToolBar.Free;
  inherited;
end;

procedure TLayoutBarToolItem.AddToolButton(ACaption: string; ImageIndex: Integer);
var
  btn: TToolButton;
begin
  btn:= TToolButton.Create(Self);
  if ACaption = '-' then btn.Style:= tbsSeparator
  else begin
    btn.Style:= tbsButton;
    btn.Caption:= ACaption;
    btn.Hint:= ACaption;
    btn.ImageIndex:= ImageIndex;
    btn.OnClick:= EventOfItemBtnClick;
  end;
  btn.Parent:= FToolBar;
end;

procedure TLayoutBarToolItem.DeleteBtn(Index: Integer);
begin
  FToolBar.RemoveControl(FToolBar.Controls[Index]);
end;

function TLayoutBarToolItem.GetBarImages: TCustomImageList;
begin
  Result:= FToolBar.Images;
end;

procedure TLayoutBarToolItem.SetBarImages(const Value: TCustomImageList);
begin
  FToolBar.Images := Value;
end;

procedure TLayoutBarToolItem.SetOnBarClick(const Value: TNotifyEvent);
begin
  FOnBarClick := Value;
end;

procedure TLayoutBarToolItem.Paint;
var
  I, baseH: Integer;
begin
  inherited;
  Canvas.Brush.Color := Self.Color;
  Canvas.Pen.Color := Self.Color;
  Canvas.FillRect(Rect(0, 0, BoundsRect.right, BoundsRect.Bottom));
  if AutoSize then begin
    Width := FBackground.Width;
    if FToolBar.ControlCount > 0 then begin
      Height:= FBackground.Height + FToolBar.Height;
      Canvas.Draw(0, FToolBar.Height, FBackground);
    end else begin
      Height := FBackground.Height;
      Canvas.Draw(0, 0, FBackground);
    end;
  end else begin
    Width := ((BoundsRect.right - BoundsRect.Left) div 5) * 5;
    if FToolBar.ControlCount > 0 then begin
      Height := ((BoundsRect.Bottom - BoundsRect.Top) div 5) * 5;
      Canvas.Draw((Width - FBackground.Width) div 2, (Height - FToolBar.Height - FBackground.Height) div 2 + FToolBar.Height, FBackground);
    end else begin
      Height := ((BoundsRect.Bottom - BoundsRect.Top) div 5) * 5;
      Canvas.Draw((Width - FBackground.Width) div 2, (Height - FBackground.Height) div 2, FBackground);
    end;
  end;

  if Focused then
  begin
    Canvas.Brush.Color := $FFFFFF - Self.Color;
    Canvas.Pen.Color := $FFFFFF - Self.Color;
  end
  else
  begin
    Canvas.Brush.Color := Self.Color;
    Canvas.Pen.Color := Self.Color;
  end;
  if FCanResize then
    for I := 1 to 8 do
      with FRectList[I] do
        Canvas.RecTangle(Left, Top, right, Bottom);
end;

{ TLayoutTextRectItem }

procedure TLayoutTextRectItem.Paint;
var
  bmp: TBitmap;
  R: TRect;
  TextFormat: TTextFormat;
  procedure WriteText(TextFormat: TTextFormat);
  begin
    bmp.Height:= Self.Height;
    bmp.Width:= Self.Width;
    R:= Rect(0, 0, bmp.Width, bmp.Height);
    bmp.Canvas.TextRect(R, FText, TextFormat);
  end;
begin
  inherited;
  bmp:= TBitmap.Create;
  bmp.Canvas.Font.Name:= FontName;
  bmp.Canvas.Font.Size:= FontSize;
  bmp.Canvas.Font.Style:= FontStyle;
  bmp.Canvas.Font.Color:= FontColor;
//  TTextFormats = (tfBottom, tfCalcRect, tfCenter, tfEditControl, tfEndEllipsis,
//    tfPathEllipsis, tfExpandTabs, tfExternalLeading, tfLeft, tfModifyString,
//    tfNoClip, tfNoPrefix, tfRight, tfRtlReading, tfSingleLine, tfTop,
//    tfVerticalCenter, tfWordBreak, tfHidePrefix, tfNoFullWidthCharBreak,
//    tfPrefixOnly, tfTabStop, tfWordEllipsis, tfComposited);
  case FTextPos of
  TP_LEFT_TOP: WriteText([tfTop, tfLeft]);
  TP_MIDDLE_TOP: WriteText([tfTop, tfCenter]);
  TP_RIGHT_TOP: WriteText([tfTop, tfRight]);
  TP_LEFT_CENTER: WriteText([tfVerticalCenter, tfLeft]);
  TP_MIDDLE_CENTER: WriteText([tfVerticalCenter, tfCenter]);
  TP_RIGHT_CENTER: WriteText([tfVerticalCenter, tfRight]);
  TP_LEFT_BOTTOM: WriteText([tfBottom, tfLeft]);
  TP_MIDDLE_BOTTOM: WriteText([tfBottom, tfCenter]);
  TP_RIGHT_BOTTOM: WriteText([tfBottom, tfRight]);
  TP_FILL: begin
      bmp.Height:= bmp.Canvas.TextHeight(FText);
      bmp.Width:= bmp.Canvas.TextWidth(FText);
      bmp.Canvas.TextOut(0, 0, FText);
    end;
  end;
  Self.Canvas.StretchDraw(Rect(0, 0, Self.Width, Self.Height), bmp);
  bmp.Free;
end;

procedure TLayoutTextRectItem.SetText(Value: string);
begin
  FText:= Value;
end;

procedure TLayoutTextRectItem.SetTextPos(Value: TTextPosition);
begin
  FTextPos:= Value;
end;

{ TLayoutImageItem }

procedure TLayoutImageItem.Paint;
var
  pic: TPicture;
begin
  inherited;
  pic:= TPicture.Create;
  pic.LoadFromFile(FFilename);
  if FAdaption then begin
    Self.Width:= pic.Width;
    Self.Height:= pic.Height;
    Self.Canvas.Draw(0, 0, pic.Graphic);
  end else if FAutoStretch then begin
    Self.Canvas.StretchDraw(Rect(0, 0, Self.Width, Self.Height), pic.Graphic);
  end else begin
    if Self.Width / pic.Width > Self.Height / pic.Height then
      Self.Canvas.StretchDraw(Rect(0, 0, Round(pic.Width * Self.Height / pic.Height), Self.Height), pic.Graphic)
    else Self.Canvas.StretchDraw(Rect(0, 0, Self.Width , Round(pic.Height * Self.Width / pic.Width)), pic.Graphic)
  end;
  pic.Free;
end;

end.
