object FrameLonLat: TFrameLonLat
  Left = 0
  Top = 0
  Width = 140
  Height = 47
  TabOrder = 0
  object EC: TEdit
    Left = 0
    Top = 21
    Width = 58
    Height = 26
    Align = alClient
    TabOrder = 0
    OnChange = EFChange
    OnKeyPress = ECKeyPress
    ExplicitHeight = 21
  end
  object EF: TEdit
    Left = 58
    Top = 21
    Width = 41
    Height = 26
    Align = alRight
    TabOrder = 1
    Visible = False
    OnChange = EFChange
    OnKeyPress = EFKeyPress
    ExplicitHeight = 21
  end
  object ES: TEdit
    Left = 99
    Top = 21
    Width = 41
    Height = 26
    Align = alRight
    TabOrder = 2
    Visible = False
    OnChange = EFChange
    OnKeyPress = ESKeyPress
    ExplicitLeft = 105
    ExplicitTop = 27
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 140
    Height = 21
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 3
    object Label1: TLabel
      Left = 0
      Top = 0
      Width = 41
      Height = 21
      Align = alClient
      Caption = 'Label1'
      Layout = tlCenter
      ExplicitWidth = 31
      ExplicitHeight = 13
    end
    object rbS: TRadioButton
      Left = 107
      Top = 0
      Width = 33
      Height = 21
      Align = alRight
      Caption = #31186
      TabOrder = 0
      OnClick = rbCClick
    end
    object rbF: TRadioButton
      Left = 74
      Top = 0
      Width = 33
      Height = 21
      Align = alRight
      Caption = #20998
      TabOrder = 1
      OnClick = rbCClick
    end
    object rbC: TRadioButton
      Left = 41
      Top = 0
      Width = 33
      Height = 21
      Align = alRight
      Caption = #24230
      Checked = True
      TabOrder = 2
      TabStop = True
      OnClick = rbCClick
    end
  end
end
