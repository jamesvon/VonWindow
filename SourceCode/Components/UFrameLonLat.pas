unit UFrameLonLat;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Vcl.ExtCtrls;

type
  TFrameLonLat = class(TFrame)
    EC: TEdit;
    EF: TEdit;
    ES: TEdit;
    Panel1: TPanel;
    rbS: TRadioButton;
    rbF: TRadioButton;
    rbC: TRadioButton;
    Label1: TLabel;
    procedure rbCClick(Sender: TObject);
    procedure EFChange(Sender: TObject);
    procedure ECKeyPress(Sender: TObject; var Key: Char);
    procedure EFKeyPress(Sender: TObject; var Key: Char);
    procedure ESKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    FKind: Integer;
    FL: Integer;
    FM: Integer;
    FS: Extended;
    procedure SetKind(const Value: Integer);
    procedure SetL(const Value: Integer);
    procedure SetM(const Value: Integer);
    procedure SetS(const Value: Extended);
    function GetValue: Extended;
    procedure SetValue(const Value: Extended);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    property Kind: Integer read FKind write SetKind;
    property L: Integer read FL write SetL;
    property M: Integer read FM write SetM;
    property S: Extended read FS write SetS;
    property Value: Extended read GetValue write SetValue;
  end;

implementation

uses UVonSystemFuns;

{$R *.dfm}
{ TFrameFrameLonLat }

constructor TFrameLonLat.Create(AOwner: TComponent);
var
  S: string;
begin
  inherited;
  S := ReadAppConfig('SYSTEM', 'LonFlatKind');
  rbC.Checked := S = '度';
  rbF.Checked := S = '度分';
  rbS.Checked := S = '度分秒';
  rbC.Enabled := S = '';
  rbF.Enabled := S = '';
  rbS.Enabled := S = '';
  rbCClick(nil);
end;

procedure TFrameLonLat.ECKeyPress(Sender: TObject; var Key: Char);
begin
  case FKind of
    0: // 按经度录入
      begin
        if Pos(Key, '1234567890.'#8) < 1 then
          Key := #0;
        if TEdit(Sender).Text = '' then
          Exit;
      end
  else
    if Pos(Key, '1234567890'#8) < 1 then
      Key := #0;
  end;
end;

procedure TFrameLonLat.EFKeyPress(Sender: TObject; var Key: Char);
begin
  case FKind of
    1: // 按经分录入
      ESKeyPress(Sender, Key);
  else
    if Pos(Key, '1234567890'#8) < 1 then
      Key := #0;
  end;
end;

procedure TFrameLonLat.ESKeyPress(Sender: TObject; var Key: Char);
begin // 按经分秒录入
  if Pos(Key, '1234567890.'#8) < 1 then
    Key := #0;
  if TEdit(Sender).Text = '' then
    Exit;
end;

function TFrameLonLat.GetValue: Extended;
begin
  Result := FL + FM / 60 + FS / 3600;
end;

procedure TFrameLonLat.EFChange(Sender: TObject);
var
  val: Extended;
begin
  if TEdit(Sender).Text = '' then
    Exit;
  case FKind of
    0:
      begin
        val := StrToFloat(EC.Text);
        FL := TrunC(val);
        FM := TrunC((val - FL) * 60);
        FS := ((val - FL) * 60 - FM) * 60;
      end;
    1:
      begin
        FL := StrToInt(EC.Text);
        val := StrToFloat(EF.Text);
        FM := TrunC(val);
        FS := (val - FM) * 60;
      end;
    2:
      begin
        FL := StrToInt(EC.Text);
        FM := StrToInt(EF.Text);
        FS := StrToFloat(ES.Text);
      end;
  end;
  if FL < 0 then
    raise Exception.Create(Label1.Caption + '值不能小于0');
  if (Label1.Caption = '经度') and (FL > 180) then
    raise Exception.Create(Label1.Caption + '值不能大于180');
  if (Label1.Caption = '纬度') and (FL > 90) then
    raise Exception.Create(Label1.Caption + '值不能大于90');
end;

procedure TFrameLonLat.SetKind(const Value: Integer);
begin
  rbC.Checked := Value = 0;
  rbF.Checked := Value = 1;
  rbS.Checked := Value = 2;
  rbCClick(nil);
end;

procedure TFrameLonLat.SetL(const Value: Integer);
begin
  FL := Value;
  rbCClick(nil);
end;

procedure TFrameLonLat.SetM(const Value: Integer);
begin
  FM := Value;
  rbCClick(nil);
end;

procedure TFrameLonLat.SetS(const Value: Extended);
begin
  FS := Value;
  rbCClick(nil);
end;

procedure TFrameLonLat.SetValue(const Value: Extended);
begin
  FL := TrunC(Value);
  FM := TrunC((Value - FL) * 60);
  FS := Value * 3600 - FL * 3600 - FM * 60;
  rbCClick(nil);
end;

procedure TFrameLonLat.rbCClick(Sender: TObject);
begin
  EC.OnChange := nil;
  EF.OnChange := nil;
  ES.OnChange := nil;
  if rbC.Checked then
  begin
    EF.Visible := False;
    ES.Visible := False;
    FKind := 0;
  end
  else if rbF.Checked then
  begin
    EF.Visible := true;
    EF.Width := (Self.Width div 3) * 2;
    ES.Visible := False;
    FKind := 1;
  end
  else if rbS.Checked then
  begin
    EF.Visible := true;
    EF.Width := (Self.Width div 3);
    ES.Visible := true;
    ES.Width := (Self.Width div 3);
    EF.Left := 0;
    FKind := 2;
  end;
  case FKind of
    0:
      begin
        EC.Text := FloatToStr(FL + FM / 60 + FS / 3600);
      end;
    1:
      begin
        EC.Text := IntToStr(FL);
        EF.Text := FloatToStr(FM + FS / 60);
      end;
    2:
      begin
        EC.Text := IntToStr(FL);
        EF.Text := IntToStr(FM);
        ES.Text := FloatToStr(FS);
      end;
  end;
  EC.OnChange := EFChange;
  EF.OnChange := EFChange;
  ES.OnChange := EFChange;
end;

end.
