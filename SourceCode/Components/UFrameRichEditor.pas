unit UFrameRichEditor;

interface

uses
  SysUtils, WinApi.Windows, WinApi.Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Menus, ComCtrls, ClipBrd,
  ToolWin, ActnList, ImgList, WinApi.RichEdit, WinApi.ShellAPI, System.Actions,
  System.ImageList;

type
  TFrameRichEditor = class(TFrame)
    MainMenu: TMainMenu;
    FileNewItem: TMenuItem;
    FileOpenItem: TMenuItem;
    FileSaveItem: TMenuItem;
    FileSaveAsItem: TMenuItem;
    FilePrintItem: TMenuItem;
    EditUndoItem: TMenuItem;
    EditCutItem: TMenuItem;
    EditCopyItem: TMenuItem;
    EditPasteItem: TMenuItem;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    PrintDialog: TPrintDialog;
    Ruler: TPanel;
    FontDialog1: TFontDialog;
    FirstInd: TLabel;
    LeftInd: TLabel;
    RulerLine: TBevel;
    RightInd: TLabel;
    N5: TMenuItem;
    miEditFont: TMenuItem;
    Editor: TRichEdit;
    StandardToolBar: TToolBar;
    OpenButton: TToolButton;
    SaveButton: TToolButton;
    PrintButton: TToolButton;
    ToolButton5: TToolButton;
    UndoButton: TToolButton;
    CutButton: TToolButton;
    CopyButton: TToolButton;
    PasteButton: TToolButton;
    ToolButton10: TToolButton;
    FontName: TComboBox;
    FontSize: TEdit;
    UpDown1: TUpDown;
    BoldButton: TToolButton;
    ItalicButton: TToolButton;
    UnderlineButton: TToolButton;
    ToolButton16: TToolButton;
    LeftAlign: TToolButton;
    CenterAlign: TToolButton;
    RightAlign: TToolButton;
    ToolButton20: TToolButton;
    BulletsButton: TToolButton;
    ToolbarImages: TImageList;
    ActionList1: TActionList;
    ActionList2: TActionList;
    FileNewCmd: TAction;
    FileOpenCmd: TAction;
    FileSaveCmd: TAction;
    FilePrintCmd: TAction;
    FileExitCmd: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    Bevel1: TBevel;
    EditCutCmd: TAction;
    EditCopyCmd: TAction;
    EditPasteCmd: TAction;
    EditUndoCmd: TAction;
    EditFontCmd: TAction;
    FileSaveAsCmd: TAction;
    StatusBar: TStatusBar;
    ToolButton3: TToolButton;
    ColorBox1: TColorBox;
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    EDateTime: TComboBox;
    ColorBox2: TColorBox;

    procedure SelectionChange(Sender: TObject);
    procedure FileNew(Sender: TObject);
    procedure FileOpen(Sender: TObject);
    procedure FileSave(Sender: TObject);
    procedure FileSaveAs(Sender: TObject);
    procedure FilePrint(Sender: TObject);
    procedure EditUndo(Sender: TObject);
    procedure EditCut(Sender: TObject);
    procedure EditCopy(Sender: TObject);
    procedure EditPaste(Sender: TObject);
    procedure SelectFont(Sender: TObject);
    procedure RulerResize(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure BoldButtonClick(Sender: TObject);
    procedure ItalicButtonClick(Sender: TObject);
    procedure FontSizeChange(Sender: TObject);
    procedure AlignButtonClick(Sender: TObject);
    procedure FontNameChange(Sender: TObject);
    procedure UnderlineButtonClick(Sender: TObject);
    procedure BulletsButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure RulerItemMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure RulerItemMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure FirstIndMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure LeftIndMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure RightIndMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure RichEditChange(Sender: TObject);
    procedure ActionList2Update(Action: TBasicAction; var Handled: Boolean);
    procedure ColorBox1Change(Sender: TObject);
    procedure EDateTimeChange(Sender: TObject);
    procedure ColorBox2Change(Sender: TObject);
    procedure EditorEnter(Sender: TObject);
    procedure EditorExit(Sender: TObject);
  private
    FFileName: string;
    FUpdating: Boolean;
    FDragOfs: Integer;
    FDragging: Boolean;
    FEnanbleShortKey: Boolean;
    function CurrText: TTextAttributes;
    procedure GetFontNames;
    procedure SetFileName(const FileName: String);
    procedure CheckFileSave;
    procedure SetupRuler;
    procedure SetEditRect;
    procedure UpdateCursorPos;
    procedure WMDropFiles(var Msg: TWMDropFiles); message WM_DROPFILES;
    procedure SetModified(Value: Boolean);
    procedure LoadDate;
    function GetModified: Boolean;
    procedure SetEnanbleShortKey(const Value: Boolean);
  public
    constructor Create(AOwner: TComponent); override;
    procedure PerformFileOpen(const AFileName: string);
  published
    property Modified: Boolean read GetModified write SetModified;
    property EnanbleShortKey: Boolean read FEnanbleShortKey
      write SetEnanbleShortKey;
  end;

implementation

resourcestring
  sSaveChanges = 'Save changes to %s?';
  sOverWrite = 'OK to overwrite %s';
  sUntitled = 'Untitled';
  sModified = 'Modified';
  sColRowInfo = 'Line: %3d   Col: %3d';

const
  RulerAdj = 4 / 3;
  GutterWid = 6;

{$R *.dfm}

procedure TFrameRichEditor.SelectionChange(Sender: TObject);
begin
  with Editor.Paragraph do
    try
      FUpdating := True;
      FirstInd.Left := Trunc(FirstIndent * RulerAdj) - 4 + GutterWid;
      LeftInd.Left := Trunc((LeftIndent + FirstIndent) * RulerAdj) - 4 +
        GutterWid;
      RightInd.Left := Ruler.ClientWidth - 6 -
        Trunc((RightIndent + GutterWid) * RulerAdj);
      BoldButton.Down := fsBold in Editor.SelAttributes.Style;
      ItalicButton.Down := fsItalic in Editor.SelAttributes.Style;
      UnderlineButton.Down := fsUnderline in Editor.SelAttributes.Style;
      BulletsButton.Down := Boolean(Numbering);
      FontSize.Text := IntToStr(Editor.SelAttributes.Size);
      FontName.Text := Editor.SelAttributes.Name;
      case Ord(Alignment) of
        0:
          LeftAlign.Down := True;
        1:
          RightAlign.Down := True;
        2:
          CenterAlign.Down := True;
      end;
      UpdateCursorPos;
    finally
      FUpdating := False;
    end;
end;

function TFrameRichEditor.CurrText: TTextAttributes;
begin
  if Editor.SelLength > 0 then
    Result := Editor.SelAttributes
  else
    Result := Editor.DefAttributes;
end;

function EnumFontsProc(var LogFont: TLogFont; var TextMetric: TTextMetric;
  FontType: Integer; Data: Pointer): Integer; stdcall;
begin
  TStrings(Data).Add(LogFont.lfFaceName);
  Result := 1;
end;

procedure TFrameRichEditor.GetFontNames;
var
  DC: HDC;
begin
  DC := GetDC(0);
  EnumFonts(DC, nil, @EnumFontsProc, Pointer(FontName.Items));
  ReleaseDC(0, DC);
  FontName.Sorted := True;
end;

procedure TFrameRichEditor.SetFileName(const FileName: String);
begin
  FFileName := FileName;
  Caption := Format('%s - %s', [ExtractFileName(FileName), Application.Title]);
end;

procedure TFrameRichEditor.CheckFileSave;
var
  SaveResp: Integer;
begin
  if not Editor.Modified then
    Exit;
  SaveResp := MessageDlg(Format(sSaveChanges, [FFileName]), mtConfirmation,
    mbYesNoCancel, 0);
  case SaveResp of
    idYes:
      FileSave(Self);
    idNo: { Nothing }
      ;
    idCancel:
      Abort;
  end;
end;

procedure TFrameRichEditor.SetupRuler;
var
  I: Integer;
  S: String;
begin
  SetLength(S, 201);
  I := 1;
  while I < 200 do
  begin
    S[I] := #9;
    S[I + 1] := '|';
    Inc(I, 2);
  end;
  Ruler.Caption := S;
end;

procedure TFrameRichEditor.SetEditRect;
var
  R: TRect;
begin
  with Editor do
  begin
    R := Rect(GutterWid, 0, ClientWidth - GutterWid, ClientHeight);
    SendMessage(Handle, EM_SETRECT, 0, Longint(@R));
  end;
end;

procedure TFrameRichEditor.SetEnanbleShortKey(const Value: Boolean);
begin
  FEnanbleShortKey := Value;
  if FEnanbleShortKey then
    ActionList2.State := TActionListState.asNormal
  else
    ActionList2.State := TActionListState.asSuspended;
end;

{ Event Handlers }

constructor TFrameRichEditor.Create(AOwner: TComponent);
begin
  inherited;
  OpenDialog.InitialDir := ExtractFilePath(ParamStr(0));
  SaveDialog.InitialDir := OpenDialog.InitialDir;
  SetFileName(sUntitled);
  GetFontNames;
  SetupRuler;
  SelectionChange(Self);
  LoadDate;
  CurrText.Name := DefFontData.Name;
  CurrText.Size := -MulDiv(DefFontData.Height, 72, Screen.PixelsPerInch);
  EnanbleShortKey:= False;
end;

procedure TFrameRichEditor.FileNew(Sender: TObject);
begin
  SetFileName(sUntitled);
  Editor.Lines.Clear;
  Editor.Modified := False;
  SetModified(False);
end;

procedure TFrameRichEditor.PerformFileOpen(const AFileName: string);
begin
  Editor.Lines.LoadFromFile(AFileName);
  SetFileName(AFileName);
  Editor.SetFocus;
  Editor.Modified := False;
  SetModified(False);
end;

procedure TFrameRichEditor.FileOpen(Sender: TObject);
begin
  CheckFileSave;
  if OpenDialog.Execute then
  begin
    PerformFileOpen(OpenDialog.FileName);
    Editor.ReadOnly := ofReadOnly in OpenDialog.Options;
  end;
end;

procedure TFrameRichEditor.FileSave(Sender: TObject);
begin
  if FFileName = sUntitled then
    FileSaveAs(Sender)
  else
  begin
    Editor.Lines.SaveToFile(FFileName);
    Editor.Modified := False;
    SetModified(False);
  end;
end;

procedure TFrameRichEditor.FileSaveAs(Sender: TObject);
begin
  if SaveDialog.Execute then
  begin
    if FileExists(SaveDialog.FileName) then
      if MessageDlg(Format(sOverWrite, [SaveDialog.FileName]), mtConfirmation,
        mbYesNoCancel, 0) <> idYes then
        Exit;
    Editor.Lines.SaveToFile(SaveDialog.FileName);
    SetFileName(SaveDialog.FileName);
    Editor.Modified := False;
    SetModified(False);
  end;
end;

procedure TFrameRichEditor.FilePrint(Sender: TObject);
begin
  if PrintDialog.Execute then
    Editor.Print(FFileName);
end;

procedure TFrameRichEditor.EditUndo(Sender: TObject);
begin
  with Editor do
    if HandleAllocated then
      SendMessage(Handle, EM_UNDO, 0, 0);
end;

procedure TFrameRichEditor.EditCut(Sender: TObject);
begin
  Editor.CutToClipboard;
end;

procedure TFrameRichEditor.EditorEnter(Sender: TObject);
begin
  EnanbleShortKey:= true;
end;

procedure TFrameRichEditor.EditorExit(Sender: TObject);
begin
  EnanbleShortKey:= False;
end;

procedure TFrameRichEditor.EditCopy(Sender: TObject);
begin
  Editor.CopyToClipboard;
end;

procedure TFrameRichEditor.EditPaste(Sender: TObject);
begin
  Editor.PasteFromClipboard;
end;

procedure TFrameRichEditor.SelectFont(Sender: TObject);
begin
  FontDialog1.Font.Assign(Editor.SelAttributes);
  if FontDialog1.Execute then
    CurrText.Assign(FontDialog1.Font);
  SelectionChange(Self);
  Editor.SetFocus;
end;

procedure TFrameRichEditor.RulerResize(Sender: TObject);
begin
  RulerLine.Width := Ruler.ClientWidth - (RulerLine.Left * 2);
end;

procedure TFrameRichEditor.FormResize(Sender: TObject);
begin
  SetEditRect;
  SelectionChange(Sender);
  if Showing then
    EditPasteCmd.ShortCut := 16470
  else
    EditPasteCmd.ShortCut := 0;
end;

procedure TFrameRichEditor.FormPaint(Sender: TObject);
begin
  SetEditRect;
end;

procedure TFrameRichEditor.BoldButtonClick(Sender: TObject);
begin
  if FUpdating then
    Exit;
  if BoldButton.Down then
    CurrText.Style := CurrText.Style + [fsBold]
  else
    CurrText.Style := CurrText.Style - [fsBold];
end;

procedure TFrameRichEditor.ItalicButtonClick(Sender: TObject);
begin
  if FUpdating then
    Exit;
  if ItalicButton.Down then
    CurrText.Style := CurrText.Style + [fsItalic]
  else
    CurrText.Style := CurrText.Style - [fsItalic];
end;

procedure TFrameRichEditor.FontSizeChange(Sender: TObject);
begin
  if FUpdating then
    Exit;
  CurrText.Size := StrToInt(FontSize.Text);
end;

procedure TFrameRichEditor.AlignButtonClick(Sender: TObject);
begin
  if FUpdating then
    Exit;
  Editor.Paragraph.Alignment := TAlignment(TControl(Sender).Tag);
end;

procedure TFrameRichEditor.FontNameChange(Sender: TObject);
begin
  if FUpdating then
    Exit;
  CurrText.Name := FontName.Items[FontName.ItemIndex];
end;

procedure TFrameRichEditor.UnderlineButtonClick(Sender: TObject);
begin
  if FUpdating then
    Exit;
  if UnderlineButton.Down then
    CurrText.Style := CurrText.Style + [fsUnderline]
  else
    CurrText.Style := CurrText.Style - [fsUnderline];
end;

procedure TFrameRichEditor.BulletsButtonClick(Sender: TObject);
begin
  if FUpdating then
    Exit;
  Editor.Paragraph.Numbering := TNumberingStyle(BulletsButton.Down);
end;

procedure TFrameRichEditor.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  try
    CheckFileSave;
  except
    CanClose := False;
  end;
end;

{ Ruler Indent Dragging }

procedure TFrameRichEditor.RulerItemMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  FDragOfs := (TLabel(Sender).Width div 2);
  TLabel(Sender).Left := TLabel(Sender).Left + X - FDragOfs;
  FDragging := True;
end;

procedure TFrameRichEditor.RulerItemMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if FDragging then
    TLabel(Sender).Left := TLabel(Sender).Left + X - FDragOfs
end;

procedure TFrameRichEditor.FirstIndMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  FDragging := False;
  Editor.Paragraph.FirstIndent := Trunc((FirstInd.Left + FDragOfs - GutterWid) /
    RulerAdj);
  LeftIndMouseUp(Sender, Button, Shift, X, Y);
end;

procedure TFrameRichEditor.LeftIndMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FDragging := False;
  Editor.Paragraph.LeftIndent := Trunc((LeftInd.Left + FDragOfs - GutterWid) /
    RulerAdj) - Editor.Paragraph.FirstIndent;
  SelectionChange(Sender);
end;

procedure TFrameRichEditor.RightIndMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  FDragging := False;
  Editor.Paragraph.RightIndent :=
    Trunc((Ruler.ClientWidth - RightInd.Left + FDragOfs - 2) / RulerAdj) - 2 *
    GutterWid;
  SelectionChange(Sender);
end;

procedure TFrameRichEditor.UpdateCursorPos;
var
  CharPos: TPoint;
begin
  CharPos.Y := SendMessage(Editor.Handle, EM_EXLINEFROMCHAR, 0,
    Editor.SelStart);
  CharPos.X := (Editor.SelStart - SendMessage(Editor.Handle, EM_LINEINDEX,
    CharPos.Y, 0));
  Inc(CharPos.Y);
  Inc(CharPos.X);
  StatusBar.Panels[0].Text := Format(sColRowInfo, [CharPos.Y, CharPos.X]);
end;

procedure TFrameRichEditor.FormShow(Sender: TObject);
begin
  UpdateCursorPos;
  DragAcceptFiles(Handle, True);
  RichEditChange(nil);
  Editor.SetFocus;
  { Check if we should load a file from the command line }
  if (ParamCount > 0) and FileExists(ParamStr(1)) then
    PerformFileOpen(ParamStr(1));
end;

procedure TFrameRichEditor.WMDropFiles(var Msg: TWMDropFiles);
var
  CFileName: array [0 .. MAX_PATH] of Char;
begin
  try
    if DragQueryFile(Msg.Drop, 0, CFileName, MAX_PATH) > 0 then
    begin
      CheckFileSave;
      PerformFileOpen(CFileName);
      Msg.Result := 0;
    end;
  finally
    DragFinish(Msg.Drop);
  end;
end;

procedure TFrameRichEditor.RichEditChange(Sender: TObject);
begin
  SetModified(Editor.Modified);
end;

procedure TFrameRichEditor.SetModified(Value: Boolean);
begin
  if Value then
    StatusBar.Panels[1].Text := sModified
  else
  begin
    Editor.Modified := False;
    StatusBar.Panels[1].Text := '';
  end;
end;

procedure TFrameRichEditor.ActionList2Update(Action: TBasicAction;
  var Handled: Boolean);
begin
  { Update the status of the edit commands }
  EditCutCmd.Enabled := Editor.SelLength > 0;
  EditCopyCmd.Enabled := EditCutCmd.Enabled;
  if Editor.HandleAllocated then
  begin
    EditUndoCmd.Enabled := Editor.Perform(EM_CANUNDO, 0, 0) <> 0;
    EditPasteCmd.Enabled := Editor.Perform(EM_CANPASTE, 0, 0) <> 0;
  end;
end;

procedure TFrameRichEditor.ColorBox1Change(Sender: TObject);
begin
  if FUpdating then
    Exit;
  CurrText.Color := ColorBox1.Selected;
end;

procedure TFrameRichEditor.ColorBox2Change(Sender: TObject);
var
  Fmt: TCharFormat2;
begin
  if FUpdating then
    Exit;
  Fmt.cbSize := SizeOf(Fmt);
  // 这里放传递的结构大小 ,系统通过这个知道传递的是 CharFormat 还是 CharFormat2
  Fmt.dwMask := CFM_BACKCOLOR; // 告诉系统只有字体颜色和背景颜色两个字段的值有效
  // Fmt.crTextColor := CurrText.Color;        // 设置字体颜色
  Fmt.crBackColor := ColorBox2.Selected; // 设置字体背景色
  Editor.Perform(EM_SETCHARFORMAT, SCF_SELECTION, Integer(@Fmt));
  // 发 EM_SETCHARFORMAT 消息给 RichEdit
end;

procedure TFrameRichEditor.EDateTimeChange(Sender: TObject);
begin
  Editor.Lines.Add(EDateTime.Text);
end;

procedure TFrameRichEditor.LoadDate;
begin
  with EDateTime.Items do
  begin
    Add(FormatDateTime('yyyy-mm-dd', Now));
    Add(FormatDateTime('yyyy-m-d', Now));
    Add(FormatDateTime('yy-mm-dd', Now));
    Add(FormatDateTime('yy-m-d', Now));
    Add(FormatDateTime('yyyy年mm月dd日', Now));
    Add(FormatDateTime('yy年mm月dd日', Now));
    Add(FormatDateTime('d,mmm,yyyy', Now));
    Add(FormatDateTime('d,mmmm,yyyy', Now));
    Add(FormatDateTime('ddd,mmm,yyyy', Now));
    Add(FormatDateTime('dddd,mmmm,yyyy', Now));
    Add(FormatDateTime('hh:nn', Now));
    Add(FormatDateTime('hh:nn:ss', Now));
    Add(FormatDateTime('hh:nn am/pm', Now));
    Add(FormatDateTime('hh:nn:ss am/pm', Now));
    Add(FormatDateTime('h:n', Now));
    Add(FormatDateTime('h:n:s', Now));
    Add(FormatDateTime('h:n a/p', Now));
    Add(FormatDateTime('h:n:s a/p', Now));
    Add(FormatDateTime('ampmh:n', Now));
    Add(FormatDateTime('ampmh点n分', Now));
  end;
end;

function TFrameRichEditor.GetModified: Boolean;
begin
  Result := Editor.Modified;
end;

end.
