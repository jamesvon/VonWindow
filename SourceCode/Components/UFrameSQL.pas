unit UFrameSQL;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls, ComCtrls, Tabs, ExtCtrls, UFrameGridBar, StdCtrls, ToolWin,
  ImgList, DB, Data.Win.ADODB, DBGrids, WinApi.ShellAPI, System.ImageList;

type
  TFrameSql = class(TFrame)
    plBase: TPanel;
    imgsBar: TImageList;
    ToolBar2: TToolBar;
    tbnOpen: TToolButton;
    tbnSave: TToolButton;
    ToolButton3: TToolButton;
    btnExec: TToolButton;
    ESQL: TMemo;
    Splitter2: TSplitter;
    FrameGridBar1: TFrameGridBar;
    Splitter1: TSplitter;
    DQData: TADOQuery;
    DBRichEdit1: TDBRichEdit;
    DBImage1: TDBImage;
    DBMemo1: TDBMemo;
    Panel1: TPanel;
    TabSet1: TTabSet;
    DBNavigator1: TDBNavigator;
    lbTitle: TLabel;
    imgCorner: TImage;
    procedure ToolBar2MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure tbnOpenClick(Sender: TObject);
    procedure tbnSaveClick(Sender: TObject);
    procedure btnExecClick(Sender: TObject);
    procedure TabSet1Change(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure FrameGridBar1GridCellClick(Column: TColumn);
    procedure ESQLDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ESQLDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure imgCornerMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
    OLDWndProc: TWndMethod;
    procedure DragFileProc(var Message: TMessage); message WM_DropFiles;
  public
    { Public declarations }
    procedure Init;
  end;

implementation

{$R *.dfm}

procedure TFrameSql.btnExecClick(Sender: TObject);
begin
  with DQData do begin
    Close;
    DBMemo1.DataField:= '';
    DBRichEdit1.DataField:= '';
    DBImage1.DataField:= '';
    TabSet1.TabIndex:= 0;
    SQL.Assign(ESQL.Lines);
    Open;
  end;
end;

procedure TFrameSql.DragFileProc(var Message: TMessage);
var
  FileNum: Word;
  p: array[0..254] of char;
begin
  if Message.Msg = WM_DropFiles then
  begin
    ESQL.Clear;
    FileNum := DragQueryFile(Message.WParam, $FFFFFFFF, nil, 0);
    // 取得拖放文件总数
    for FileNum := 0 to FileNum - 1 do
    begin
      DragQueryFile(Message.WParam, FileNum, p, 255);
      // 取得拖放文件名
      //Self.MemoDrag.Lines.add(StrPas(p));
      //对文件的处理
      ESQL.Lines.LoadFromFile(StrPas(p));
    end;
  end else OLDWndProc(Message); // 其他消息,调用原来的处理程序
end;

procedure TFrameSql.ESQLDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  P: TPoint;
  OldFont : HFont;
  Hand : THandle;
  TM : TTextMetric;
  Rect : TRect;
begin
  Hand := GetDC(ESql.Handle);
  if Source.ClassName = 'TTreeView'  then begin
//    try
//      OldFont := SelectObject(Hand, ESql.Font.Handle);
//      try
//        GetTextMetrics(Hand, TM);
//        ESql.Perform(EM_GETRECT, 0, longint(@Rect));
//        P.Y:= GetScrollPos(ESql.Handle, SB_VERT) + (Y - Rect.Top) div (TM.tmHeight);
//        P.X:= GetScrollPos(ESql.Handle, SB_HORZ) + (X - Rect.Left) div (TM.tmAveCharWidth);
//        ESql.CaretPos:= P;
//      finally
//        SelectObject(Hand, OldFont);
//      end;
//    finally
//      ReleaseDC(ESql.Handle, Hand);
//    end;
    ESql.SelText := TTreeView(Source).Selected.Text;
  end;
end;

procedure TFrameSql.ESQLDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
//
end;

procedure TFrameSql.FrameGridBar1GridCellClick(Column: TColumn);
begin
  DBMemo1.DataField:= '';
  DBRichEdit1.DataField:= '';
  DBImage1.DataField:= '';
  if TabSet1.TabIndex = 0 then
  case Column.Field.DataType of
    ftBytes, ftVarBytes, ftBlob, ftMemo, ftFmtMemo, ftParadoxOle, ftDBaseOle,
    ftTypedBinary, ftCursor, ftOraBlob, ftOraClob, ftVariant, ftInterface,
    ftIDispatch, ftFMTBcd, ftWideMemo, ftByte, ftConnection, ftParams, ftStream,
    ftTimeStampOffset, ftObject: TabSet1.TabIndex:= 2;
    ftGraphic: TabSet1.TabIndex:= 3;
  end;
  if DBMemo1.Visible then DBMemo1.DataField:= FrameGridBar1.Grid.SelectedField.FieldName;
  if DBRichEdit1.Visible then DBRichEdit1.DataField:= FrameGridBar1.Grid.SelectedField.FieldName;
  if DBImage1.Visible then DBImage1.DataField:= FrameGridBar1.Grid.SelectedField.FieldName;
end;

procedure TFrameSql.imgCornerMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if ssLeft in Shift then begin
    ReleaseCapture;
    Perform(WM_SYSCOMMAND, $F008, 0);
  end;
end;

procedure TFrameSql.Init;
begin
  DragAcceptFiles(ESQL.Handle, True);
  OLDWndProc := ESQL.WindowProc;
  // 保存原来的 WindowProc
  ESQL.WindowProc := Self.DragFileProc;
end;

procedure TFrameSql.TabSet1Change(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
  Splitter1.Visible:= NewTab > 0;
  DBMemo1.Visible:= NewTab = 1;
  DBRichEdit1.Visible:= NewTab = 2;
  DBImage1.Visible:= NewTab = 3;
  TabSet1.Left:= MaxInt;
end;

procedure TFrameSql.tbnOpenClick(Sender: TObject);
begin
  with TOpenDialog.Create(nil)do try
    Filter:= '独立SQL文件|*.SQL|批量SQL文件|*.SQLS|文本文件|*.TXT|数据文件|*.DAT|所有文件|*.*';
    if Execute then
      ESQL.Lines.LoadFromFile(Filename);
  finally
    Free;
  end;
end;

procedure TFrameSql.tbnSaveClick(Sender: TObject);
begin
  with TSaveDialog.Create(nil)do try
    Filter:= '独立SQL文件|*.SQL|批量SQL文件|*.SQLS|文本文件|*.TXT|数据文件|*.DAT|所有文件|*.*';
    if Execute then
      ESQL.Lines.SaveToFile(Filename);
  finally
    Free;
  end;
end;

procedure TFrameSql.ToolBar2MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  ReleaseCapture;
  Perform(WM_SYSCOMMAND, $F012, 0);
end;

end.
