(*******************************************************************************
  使用 function Add(ComName, Prompty, Params: string;
      ComType: TVonListInputDataType; AWidth: Integer): Integer;
    来添加控件，控件的数据类型在 UVonSystemFuns 文件中有定义
    /// <summary>录入数据类型</summary>
    /// <param name="LIDT_Text">单行文字录入（TMaskEdit）</param>
    /// <param name="LIDT_Int">整数录入(TSpinEdit)</param>
    /// <param name="LIDT_Float">浮点数据录入（TEdit）</param>
    /// <param name="LIDT_Conn">数据库链接录入(TButtonedEdit)</param>
    /// <param name="LIDT_Date">日期数据录入（TPickDatetime）</param>
    /// <param name="LIDT_Time">时间数据录入（TPickDatetime）</param>
    /// <param name="LIDT_Memo">多行数据录入(TMemo)</param>
    /// <param name="LIDT_RichText">富文本数据录入(FrameRichImputor)</param>
    /// <param name="LIDT_Selection">选择数据录入（TComBoBox）</param>
    /// <param name="LIDT_Choice">单选数据录入(TRadioGroup)</param>
    /// <param name="LIDT_Bool">布尔数据录入(TCheckBox)0：False，1：True</param>
    /// <param name="LIDT_CheckList">复选列表录入(TCheckList)</param>
    /// <param name="LIDT_Bitmap">图片录入(FrameImageImputor)</param>
    /// <param name="LIDT_GUID">GUID生成器(TButtonedEdit)</param>
    /// <param name="LIDT_Dialog">...</param>
    TVonListInputDataType = (LIDT_Text, LIDT_Int, LIDT_Float, LIDT_Conn,
      LIDT_Date, LIDT_Time, LIDT_Memo, LIDT_RichText, LIDT_Selection, LIDT_Choice,
      LIDT_Bool, LIDT_CheckList, LIDT_Bitmap, LIDT_Dialog, LIDT_GUID);
  通过属性 Data[I] 或 NameData[Name: string] 来读取结果
*******************************************************************************)
unit UFrameSettingInputor;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, ExtCtrls, Spin, Data.win.AdoConEd, ImgList, ComCtrls,
  UFrameRichEditor, UFrameImageInput, UVonSystemFuns, Mask, System.ImageList;

const
  LargeCompHeight = 80;

type
  TEventOfEditData = procedure(Key, Value: string) of object;

  TVonListInputDataBase = class(TPanel)
  private
    FPanel: TPanel;
    FLb: TLabel;
    FObj: TWinControl;
    FDataType: TVonListInputDataType;
    FParams: string;
    FIdentName: string;
    FImgList: TImageList;
    FLabelWidth: Integer;
    procedure EventFloatKeyPress(Sender: TObject; var Key: Char);
    procedure EventConnectDB(Sender: TObject);
    procedure EventDialog(Sender: TObject);
    procedure EventSelect(Sender: TObject);
    procedure EventEdit(Sender: TObject);
    procedure EventCheckIt(Sender: TObject);
    procedure EventCreateGuid(Sender: TObject);
  private
    FOnEdit: TEventOfEditData;
    FOnDialog: TEventOfEditData;
    FOnSelect: TEventOfEditData;
    FData: Integer;
    FPromptValue: string;
    FHelp: string;
    procedure SetInt(const Value: Int64);
    procedure SetString(const Value: string);
    function GetInt: Int64;
    function GetString: string;
    procedure SetImgList(const Value: TImageList);
    function GetDate: TDatetime;
    function GetTime: TDatetime;
    procedure SetDate(const Value: TDatetime);
    procedure SetTime(const Value: TDatetime);
    function GetPersistent: TPersistent;
    procedure SetPersistent(const Value: TPersistent);
    procedure SetLabelWidth(const Value: Integer);
    procedure SetOnEdit(const Value: TEventOfEditData);
    procedure SetOnDialog(const Value: TEventOfEditData);
    procedure SetOnSelect(const Value: TEventOfEditData);
    function GetDouble: Extended;
    procedure SetDouble(const Value: Extended);
    procedure SetData(const Value: Integer);
    procedure SetPromptValue(const Value: string);
    procedure SetHelp(const Value: string);
  public
    constructor Create(AOwner: TComponent; IdentName: string);
    destructor Destroy; override;
    procedure AddControl(ADataType: TVonListInputDataType;
      Prompt, Params: string; AWidth, AHeight: Integer);
  published
    property DataType: TVonListInputDataType read FDataType;
    property ImgList: TImageList read FImgList write SetImgList;
    property AsString: string read GetString write SetString;
    property AsInt: Int64 read GetInt write SetInt;
    property AsDouble: Extended read GetDouble write SetDouble;
    property AsDate: TDatetime read GetDate write SetDate;
    property AsTime: TDatetime read GetTime write SetTime;
    property AsPersistent: TPersistent read GetPersistent write SetPersistent;
    property LabelWidth: Integer read FLabelWidth write SetLabelWidth;
    property PromptValue: string read FPromptValue write SetPromptValue;
    property Data: Integer read FData write SetData;
    property Help: string read FHelp write SetHelp;
    property OnEdit: TEventOfEditData read FOnEdit write SetOnEdit;
    property OnDialog: TEventOfEditData read FOnDialog write SetOnDialog;
    property OnSelect: TEventOfEditData read FOnSelect write SetOnSelect;
  end;

  TFrameSettingInputor = class(TFrame)
    ScrollBox1: TScrollBox;
    ImageList1: TImageList;
    lbTitle: TLabel;
    procedure ScrollBox1Resize(Sender: TObject);
    procedure FrameCanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure ScrollBox1MouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
  private
    { Private declarations }
    FList: TStringList;
    FLabelWidth: Integer;
    FLargeCount: Integer;
    FCurrentHeight: Integer;
    FOnDialog: TEventOfEditData;
    FOnEdit: TEventOfEditData;
    FOnSelect: TEventOfEditData;
    function GetCount: Integer;
    function GetData(Index: Integer): TVonListInputDataBase;
    function GetNameData(Name: string): TVonListInputDataBase;
    function GetNames(Index: Integer): string;
    procedure SetLabelWidth(const Value: Integer);
    procedure SetOnDialog(const Value: TEventOfEditData);
    procedure SetOnEdit(const Value: TEventOfEditData);
    procedure SetOnSelect(const Value: TEventOfEditData);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Clear;
    ///  <summary></summary>
    ///  <param name="ComName">组件名称</param>
    ///  <param name="Prompty">内容提示</param>
    ///  <param name="Params">内容参数</param>
    ///  <param name="Help">内容帮助</param>
    ///  <param name="ComType">填写类型</param>
    ///  <param name="AWidth">内容宽度</param>
    ///  <param name="AHeight">内容高度</param>
    function Add(ComName, Prompty, Params, Help: string; ComType: TVonListInputDataType;
      AWidth: Integer = 0; AHeight: Integer = 0): Integer;
    procedure Delete(Index: Integer);
    property Data[Index: Integer]: TVonListInputDataBase read GetData;
    property NameData[Name: string]: TVonListInputDataBase read GetNameData;
    property Names[Index: Integer]: string read GetNames;
    property LabelWidth: Integer read FLabelWidth write SetLabelWidth;
  published
    property Count: Integer read GetCount;
    property OnEdit: TEventOfEditData read FOnEdit write SetOnEdit;
    property OnDialog: TEventOfEditData read FOnDialog write SetOnDialog;
    property OnSelect: TEventOfEditData read FOnSelect write SetOnSelect;
  end;

implementation

uses DateUtils, Math;

{$R *.dfm}
{ TVonListInputDataBase }

procedure TVonListInputDataBase.AddControl(ADataType: TVonListInputDataType;
  Prompt, Params: string; AWidth, AHeight: Integer);

  function defaultInt: Integer;
  begin
    Result:= 0;
    TryStrToInt(Params, Result);
  end;
  function defaultDate: TDateTime;
  begin
    Result:= DateOf(Now);
    TryStrToDate(Params, Result);
  end;
  function defaultTime: TDateTime;
  begin
    Result:= TimeOf(Now);
    TryStrToTime(Params, Result);
  end;
  procedure AddCom(obj: TWinControl; al: TAlign; DefaultHeight: Integer);
  begin
    if al <> alNone then begin
      FLb := TLabel.Create(nil);
      FLb.Parent := Self;
      FLb.Align := al;
      FLb.Layout := tlCenter;
      FLb.AutoSize := False;
      FLb.WordWrap := True;
      FLb.Transparent := False;
      FLb.AlignWithMargins := True;
      FLb.Margins.SetBounds(0, 1, 0, 1);
      FLb.Color := $00FDF7DF;
      FLb.Caption := Prompt;
      FLb.OnClick := EventSelect;
      FLb.Width := Round(AWidth * 0.38);
      FLb.Name:= Name + '_1';
    end;
    FObj := obj;
    FObj.Parent := Self;
    FObj.Align := alClient;
    FObj.Name:= Name + '_2';
    Height := DefaultHeight;
  end;
begin
  FDataType := ADataType;
  FParams:= Params;
  FPromptValue:= Prompt;
//  Width:= AWidth;
  ShowCaption:= False;
//  Exit;
  case DataType of
    LIDT_Text:
      begin
        AddCom(TMaskEdit.Create(nil), alLeft, 34);
        TMaskEdit(FObj).EditMask := Params;
        TMaskEdit(FObj).Text := '';
        TMaskEdit(FObj).OnChange := EventEdit;
      end;
    LIDT_Int:
      begin
        AddCom(TSpinEdit.Create(nil), alLeft, 34);
        TSpinEdit(FObj).Value:= defaultInt;
        TSpinEdit(FObj).OnChange := EventEdit;
      end;
    LIDT_Float:
      begin
        AddCom(TEdit.Create(nil), alLeft, 34);
        TEdit(FObj).Text := Format('%0.' + Params + 'f', [0.0]);
        TEdit(FObj).OnKeyPress := EventFloatKeyPress;
        TEdit(FObj).OnChange := EventEdit;
      end;
    LIDT_Conn:
      begin
        AddCom(TButtonedEdit.Create(nil), alLeft, 24);
        TButtonedEdit(FObj).Text := Params;
        TButtonedEdit(FObj).Images:= FImgList;
        TButtonedEdit(FObj).RightButton.ImageIndex := 0;
        TButtonedEdit(FObj).RightButton.Visible := True;
        TButtonedEdit(FObj).OnRightButtonClick := EventConnectDB;
        TButtonedEdit(FObj).OnChange := EventEdit;
      end;
    LIDT_Date:
      begin
        AddCom(TDateTimePicker.Create(nil), alLeft, 34);
        TDateTimePicker(FObj).Date := defaultDate;
        TDateTimePicker(FObj).Kind := dtkDate;
        TDateTimePicker(FObj).DateFormat := dfLong;
        TDateTimePicker(FObj).OnChange := EventEdit;
      end;
    LIDT_Time:
      begin
        AddCom(TDateTimePicker.Create(nil), alLeft, 34);
        TDateTimePicker(FObj).Time := defaultTime;
        TDateTimePicker(FObj).Kind := dtkTime;
        TDateTimePicker(FObj).OnChange := EventEdit;
      end;
    LIDT_Memo:
      begin
        AddCom(TMemo.Create(nil), alTop, AHeight);
        TMemo(FObj).Text := Params;
        TMemo(FObj).OnChange := EventEdit;
      end;
    LIDT_RichText:
      begin
        AddCom(TFrameRichEditor.Create(nil), alTop, AHeight);
        TFrameRichEditor(FObj).Editor.OnChange := EventEdit;
      end;
    LIDT_Selection:
      begin
        AddCom(TComboBox.Create(nil), alLeft, 34);
        TComboBox(FObj).OnChange := EventEdit;
        TComboBox(FObj).Items.Delimiter:= ',';
        TComboBox(FObj).Items.DelimitedText:= Params;
        TComboBox(FObj).ItemIndex:= 0;
      end;
    LIDT_Choice:
      begin
        AddCom(TRadioGroup.Create(nil), alNone, AHeight);
        TRadioGroup(FObj).Caption:= Prompt;
        TRadioGroup(FObj).Items.Delimiter:= ',';
        TRadioGroup(FObj).Items.DelimitedText:= Params;
        TRadioGroup(FObj).Columns:= Min(5, TRadioGroup(FObj).Items.Count);
        TRadioGroup(FObj).ItemIndex:= defaultInt;
        Height:= Round(TRadioGroup(FObj).Items.Count / TRadioGroup(FObj).Columns) * 20 + 25;
      end;
    LIDT_Bool:
      begin
        AddCom(TCheckBox.Create(nil), alLeft, 34);
        TCheckBox(FObj).OnClick := EventCheckIt;
        TCheckBox(FObj).Caption:= '';
      end;
    LIDT_CheckList:
      begin
        AddCom(TCheckListBox.Create(nil), alTop, AHeight);
        TCheckListBox(FObj).Columns:= 2;
        TCheckListBox(FObj).Items.Delimiter:= ',';
        TCheckListBox(FObj).Items.DelimitedText:= Params;
        Height:= Round(TCheckListBox(FObj).Items.Count / 2 + 0.5) * TCheckListBox(FObj).ItemHeight + 35;
        TCheckListBox(FObj).OnClickCheck := EventEdit;
      end;
    LIDT_Bitmap:
      begin
        AddCom(TFrameImageInput.Create(nil), alTop, AHeight);
        TFrameImageInput(FObj).OnClick := EventEdit;
      end;
    LIDT_Dialog:
      begin
        AddCom(TButtonedEdit.Create(nil), alLeft, 34);
        TButtonedEdit(FObj).OnRightButtonClick := EventDialog;
        TButtonedEdit(FObj).OnChange := EventEdit;
      end;
    LIDT_GUID:
      begin
        AddCom(TButtonedEdit.Create(nil), alLeft, 34);
        TButtonedEdit(FObj).Text := Params;
        TButtonedEdit(FObj).Images:= FImgList;
        TButtonedEdit(FObj).RightButton.ImageIndex := 1;
        TButtonedEdit(FObj).RightButton.Visible := True;
        TButtonedEdit(FObj).OnRightButtonClick := EventCreateGuid;
        TButtonedEdit(FObj).OnChange := EventEdit;
      end;
    else
      raise Exception.Create('Unknow component to Create');
  end;
end;

constructor TVonListInputDataBase.Create(AOwner: TComponent; IdentName: string);
begin
  Inherited Create(nil);
  FIdentName := IdentName;
  Color:= clGradientActiveCaption;
end;

destructor TVonListInputDataBase.Destroy;
begin
  if Assigned(FObj) then
    FreeAndNil(FObj);
  if Assigned(FLb) then
    FreeAndNil(FLb);
  inherited;
end;

{ TVonListInputDataBase.Properties }

procedure TVonListInputDataBase.SetImgList(const Value: TImageList);
begin
  FImgList := Value;
  case DataType of
    LIDT_Conn:
      begin
        TButtonedEdit(FObj).Images := FImgList;
        TButtonedEdit(FObj).RightButton.ImageIndex := 0;
        TButtonedEdit(FObj).RightButton.Visible := True;
      end;
    LIDT_GUID:
      begin
        TButtonedEdit(FObj).Images := FImgList;
        TButtonedEdit(FObj).RightButton.ImageIndex := 1;
        TButtonedEdit(FObj).RightButton.Visible := True;
      end;
  end;
end;

procedure TVonListInputDataBase.SetLabelWidth(const Value: Integer);
begin
  if not Assigned(FLb) then Exit;
  if Value = 0 then
    FLb.Width := Round(Self.Width * 0.382)
  else
    FLb.Width := Value;
end;

procedure TVonListInputDataBase.SetOnEdit(const Value: TEventOfEditData);
begin
  FOnEdit := Value;
end;

procedure TVonListInputDataBase.SetOnSelect(const Value: TEventOfEditData);
begin
  FOnSelect := Value;
end;

procedure TVonListInputDataBase.SetOnDialog(const Value: TEventOfEditData);
begin
  FOnDialog := Value;
end;

{ TVonListInputDataBase: set or get a value }

procedure TVonListInputDataBase.SetData(const Value: Integer);
begin
  FData:= Value;
end;

procedure TVonListInputDataBase.SetDate(const Value: TDatetime);
var
  idx: Integer;
begin
  case DataType of
    LIDT_Text:
      TMaskEdit(FObj).Text := DateToStr(Value);
    LIDT_Int:
      TSpinEdit(FObj).Value := Trunc(Value);
    LIDT_Float:
      TEdit(FObj).Text := FloatToStr(Value);
    LIDT_Date:
      TDateTimePicker(FObj).Date := Value;
    LIDT_Time:
      TDateTimePicker(FObj).Time := Value;
    LIDT_Memo:
      TMemo(FObj).Text := DateToStr(Value);
    LIDT_RichText:
      TFrameRichEditor(FObj).Editor.Text := DateToStr(Value);
    LIDT_Selection: begin
      idx:= TComboBox(FObj).Items.IndexOf(DateToStr(Value));
      if idx >= 0 then TComboBox(FObj).ItemIndex := idx
      else TComboBox(FObj).Text:= DateToStr(Value);
    end;
    LIDT_Choice:
      TRadioGroup(FObj).ItemIndex := TRadioGroup(FObj)
        .Items.IndexOf(DateToStr(Value));
    LIDT_CheckList:
      with TCheckListBox(FObj) do
      begin
        CheckAll(cbUnchecked);
        idx := Items.IndexOf(DateToStr(Value));
        if idx >= 0 then
          Checked[idx] := True;
      end;
  end;
end;

procedure TVonListInputDataBase.SetDouble(const Value: Extended);
var
  i: Integer;
  szInt: Int64;
begin
  case DataType of
    LIDT_Text:
      TMaskEdit(FObj).Text := FloatToStr(Value);
    LIDT_Float:
      TEdit(FObj).Text := FloatToStr(Value);
    LIDT_Date:
      TDateTimePicker(FObj).Date := Value;
    LIDT_Time:
      TDateTimePicker(FObj).Time := Value; // (24*60*60*1000);
    LIDT_Memo:
      TMemo(FObj).Text := FloatToStr(Value);
    LIDT_RichText:
      TFrameRichEditor(FObj).Editor.Text := FloatToStr(Value);
    else SetInt(Trunc(Value));
  end;
end;

procedure TVonListInputDataBase.SetHelp(const Value: string);
begin
  FHelp := Value;
  FObj.ShowHint:= Value <> '';
  FObj.Hint:= Value;
end;

procedure TVonListInputDataBase.SetInt(const Value: Int64);
var
  i: Integer;
  szInt: Int64;
begin
  case DataType of
    LIDT_Text:
      TMaskEdit(FObj).Text := IntToStr(Value);
    LIDT_Int:
      TSpinEdit(FObj).Value := Value;
    LIDT_Float:
      TEdit(FObj).Text := IntToStr(Value);
    LIDT_Conn:
      TButtonedEdit(FObj).Text := IntToStr(Value);
    LIDT_Date:
      TDateTimePicker(FObj).Date := Value;
    LIDT_Time:
      TDateTimePicker(FObj).Time := Value / 86400000; // (24*60*60*1000);
    LIDT_Memo:
      TMemo(FObj).Text := IntToStr(Value);
    LIDT_RichText:
      TFrameRichEditor(FObj).Editor.Text := IntToStr(Value);
    LIDT_Selection:
      TComboBox(FObj).ItemIndex := Value;
    LIDT_Choice:
      TRadioGroup(FObj).ItemIndex := Value;
    LIDT_Bool:
      TCheckBox(FObj).Checked := Value = 1;
    LIDT_CheckList:
      with TCheckListBox(FObj) do
      begin
        szInt := Value;
        for i := 0 to Items.Count - 1 do
        begin
          Checked[i] := (szInt and $1) > 0;
          szInt := szInt shr 1;
        end;
      end;
    LIDT_Bitmap, LIDT_Dialog:
      begin
      end;
  end;
end;

procedure TVonListInputDataBase.SetPersistent(const Value: TPersistent);
begin
  case DataType of
    LIDT_Memo:
      TMemo(FObj).Lines.Assign(Value);
    LIDT_RichText:
      TFrameRichEditor(FObj).Editor.Assign(Value);
  end;
end;

procedure TVonListInputDataBase.SetPromptValue(const Value: string);
begin
  FPromptValue := Value;
end;

procedure TVonListInputDataBase.SetString(const Value: string);
var
  i: Integer;
  lst: TStringList;
begin
  try
  case DataType of
    LIDT_Text:
      TMaskEdit(FObj).Text := Value;
    LIDT_Int:
      TSpinEdit(FObj).Value := StrToInt(Value);
    LIDT_Float:
      TEdit(FObj).Text := Value;
    LIDT_Conn:
      TButtonedEdit(FObj).Text := Value;
    LIDT_Date:
      TDateTimePicker(FObj).Date := StrToDate(Value);
    LIDT_Time:
      TDateTimePicker(FObj).Time := StrToTime(Value);
    LIDT_Memo:
      TMemo(FObj).Text := Value;
    LIDT_RichText:
      TFrameRichEditor(FObj).Editor.Text := Value;
    LIDT_Selection:     begin
      i:= TComboBox(FObj).Items.IndexOf(Value);
      if i >= 0 then TComboBox(FObj).ItemIndex := i
      else TComboBox(FObj).Text:= Value;
    end;
    LIDT_Choice:
      TRadioGroup(FObj).ItemIndex := StrToInt(Value);
    LIDT_Bool:
      TCheckBox(FObj).Checked := Value = '1';
    LIDT_CheckList:
      with TCheckListBox(FObj) do
      begin
        lst := TStringList.Create;
        lst.Delimiter := ',';
        lst.DelimitedText := Value;
        CheckAll(cbUnchecked);
        for i := 0 to lst.Count - 1 do
          Checked[Items.IndexOf(lst[i])] := True;
      end;
    LIDT_Bitmap, LIDT_Dialog:
      begin
      end;
    LIDT_GUID:
      TButtonedEdit(FObj).Text := Value;
  end;
  except
    On E: Exception do ShowMessage(PromptValue + ' error:' + E.Message);
  end;
end;

procedure TVonListInputDataBase.SetTime(const Value: TDatetime);
var
  idx: Integer;
begin
  case DataType of
    LIDT_Text:
      TMaskEdit(FObj).Text := DateToStr(Value);
    LIDT_Int:
      TSpinEdit(FObj).Value := Trunc(Frac(Value) * 86400000);
    LIDT_Float:
      TEdit(FObj).Text := FloatToStr(Value);
    LIDT_Date:
      TDateTimePicker(FObj).Date := Value;
    LIDT_Time:
      TDateTimePicker(FObj).Time := Value;
    LIDT_Memo:
      TMemo(FObj).Text := DateToStr(Value);
    LIDT_RichText:
      TFrameRichEditor(FObj).Editor.Text := DateToStr(Value);
    LIDT_Selection: begin
      idx:= TComboBox(FObj).Items.IndexOf(DateToStr(Value));
      if idx >= 0 then TComboBox(FObj).ItemIndex := idx
      else TComboBox(FObj).Text:= DateToStr(Value);
    end;
    LIDT_Choice:
      TRadioGroup(FObj).ItemIndex := TRadioGroup(FObj)
        .Items.IndexOf(DateToStr(Value));
    LIDT_CheckList:
      with TCheckListBox(FObj) do
      begin
        CheckAll(cbUnchecked);
        idx := Items.IndexOf(DateToStr(Value));
        if idx >= 0 then
          Checked[idx] := True;
      end;
    LIDT_Bitmap, LIDT_Dialog:
      begin
      end;
  end;
end;

function TVonListInputDataBase.GetDate: TDatetime;
var
  i: Integer;
  szBit: Int64;
begin
  Result := 0;
  case DataType of
    LIDT_Text:
      TryStrToDate(TMaskEdit(FObj).Text, Result);
    LIDT_Int:
      Result := TSpinEdit(FObj).Value;
    LIDT_Float:
      TryStrToDate(TEdit(FObj).Text, Result);
    LIDT_Date:
      Result := TDateTimePicker(FObj).Date;
    LIDT_Time:
      Result := TDateTimePicker(FObj).Time;
    LIDT_Memo:
      TryStrToDate(TMemo(FObj).Text, Result);
    LIDT_Selection:
      TryStrToDate(TComboBox(FObj).Text, Result);
    LIDT_Choice:
      TryStrToDate(TRadioGroup(FObj).Items[TRadioGroup(FObj)
        .ItemIndex], Result);
  end;
end;

function TVonListInputDataBase.GetDouble: Extended;
var
  i: Integer;
  szBit: Int64;
begin
  Result := 0;
  case DataType of
    LIDT_Text:
      TryStrToFloat(TMaskEdit(FObj).Text, Result);
    LIDT_Float:
      TryStrToFloat(TEdit(FObj).Text, Result);
    LIDT_Date:
      Result := TDateTimePicker(FObj).Date;
    LIDT_Time:
      Result := Frac(TDateTimePicker(FObj).Time);
    else Result:= GetInt;
  end;
end;

function TVonListInputDataBase.GetInt: Int64;
var
  i: Integer;
  szBit: Int64;
begin
  Result := 0;
  case DataType of
    LIDT_Text:
      TryStrToInt64(TMaskEdit(FObj).Text, Result);
    LIDT_Int:
      Result := TSpinEdit(FObj).Value;
    LIDT_Float:
      TryStrToInt64(TEdit(FObj).Text, Result);
    LIDT_Date:
      Result := Trunc(TDateTimePicker(FObj).Date);
    LIDT_Time:
      Result := Trunc(Frac(TDateTimePicker(FObj).Time) * 86400000);
    LIDT_Memo:
      TryStrToInt64(TMemo(FObj).Text, Result);
    LIDT_Selection:
      Result := TComboBox(FObj).ItemIndex;
    LIDT_Choice:
      Result := TRadioGroup(FObj).ItemIndex;
    LIDT_Bool:
      if TCheckBox(FObj).Checked then
        Result := 1
      else
        Result := 0;
    LIDT_CheckList:
      with TCheckListBox(FObj) do
      begin
        szBit := 1;
        for i := 0 to Items.Count - 1 do
        begin
          if Checked[i] then
            Result := Result or szBit;
          szBit := szBit shl 1;
        end;
      end;
  end;
end;

function TVonListInputDataBase.GetPersistent: TPersistent;
begin
  case DataType of
    LIDT_Memo:
      Result := TMemo(FObj).Lines;
    LIDT_RichText:
      Result := TFrameRichEditor(FObj).Editor.Lines;
  end;
end;

function TVonListInputDataBase.GetString: string;
var
  i: Integer;
begin
  case DataType of
    LIDT_Text:
      Result := TMaskEdit(FObj).Text;
    LIDT_Int:
      Result := IntToStr(TSpinEdit(FObj).Value);
    LIDT_Float:
      Result := TEdit(FObj).Text;
    LIDT_Conn:
      Result := TButtonedEdit(FObj).Text;
    LIDT_Date:
      Result := DateToStr(TDateTimePicker(FObj).Date);
    LIDT_Time:
      Result := TimeToStr(TDateTimePicker(FObj).Time);
    LIDT_Memo:
      Result := TMemo(FObj).Text;
    LIDT_RichText:
      Result := TFrameRichEditor(FObj).Editor.Text;
    LIDT_Selection:
      Result := TComboBox(FObj).Text;
    LIDT_Choice:
      Result := IntToStr(TRadioGroup(FObj).ItemIndex);
    LIDT_Bool:
      if TCheckBox(FObj).Checked then
        Result := '1'
      else
        Result := '0';
    LIDT_CheckList:
      with TCheckListBox(FObj) do
      begin
        Result := '';
        for i := 0 to Items.Count - 1 do
          if Checked[i] then
            Result := Result + ',' + Items[i];
        if Result <> '' then
          Delete(Result, 1, 1);
      end;
    LIDT_GUID:
      Result := TButtonedEdit(FObj).Text;
  end;
end;

function TVonListInputDataBase.GetTime: TDatetime;
var
  i: Integer;
  szBit: Int64;
begin
  Result := 0;
  case DataType of
    LIDT_Text:
      TryStrToTime(TMaskEdit(FObj).Text, Result);
    LIDT_Int:
      Result := TSpinEdit(FObj).Value;
    LIDT_Float:
      TryStrToTime(TEdit(FObj).Text, Result);
    LIDT_Date:
      Result := TDateTimePicker(FObj).Date;
    LIDT_Time:
      Result := TDateTimePicker(FObj).Time;
    LIDT_Memo:
      TryStrToDate(TMemo(FObj).Text, Result);
    LIDT_Selection:
      TryStrToTime(TComboBox(FObj).Text, Result);
    LIDT_Choice:
      TryStrToTime(TRadioGroup(FObj).Items[TRadioGroup(FObj)
        .ItemIndex], Result);
  end;
end;

{ TVonListInputDataBase: Event of component }

procedure TVonListInputDataBase.EventCheckIt(Sender: TObject);
var
  mPos: Integer;
begin
  with TCheckBox(Sender) do
  begin
    mPos := Pos(',', Hint);
    if Checked then
      Caption := Copy(Hint, mPos + 1, MaxInt)
    else
      Caption := Copy(Hint, 1, mPos - 1);
  end;
  if Assigned(FOnEdit) then
    FOnEdit(FIdentName, AsString);
end;

procedure TVonListInputDataBase.EventConnectDB(Sender: TObject);
var
  InitialConnStr: WideString;
begin
  with TConnEditForm.Create(Application) do
    try
      InitialConnStr := TButtonedEdit(FObj).Text;
      TButtonedEdit(FObj).Text := Edit(InitialConnStr);
    finally
      Free;
    end;
end;

procedure TVonListInputDataBase.EventCreateGuid(Sender: TObject);
var
  G: TGuid;
begin
  CreateGUID(G);
  TButtonedEdit(FObj).Text := GUIDToString(G);
end;

procedure TVonListInputDataBase.EventDialog(Sender: TObject);
begin
  if Assigned(FOnDialog) then
    FOnDialog(FIdentName, AsString);
end;

procedure TVonListInputDataBase.EventEdit(Sender: TObject);
begin
  if Assigned(FOnEdit) then
    FOnEdit(FIdentName, AsString);
end;

procedure TVonListInputDataBase.EventFloatKeyPress(Sender: TObject;
  var Key: Char);
var
  f: Extended;
begin
  if not TryStrToFloat(TCustomEdit(Sender).Text + Key, f) then
    Key := #0;
end;

procedure TVonListInputDataBase.EventSelect(Sender: TObject);
begin
  if Assigned(FOnSelect) then
    FOnSelect(FIdentName, AsString);
end;

{ TFrameDataList }

function TFrameSettingInputor.Add(ComName, Prompty, Params, Help: string;
  ComType: TVonListInputDataType; AWidth: Integer; AHeight: Integer): Integer;
var
  Cmp: TVonListInputDataBase;
  flag: Integer;
begin
  Result := FList.IndexOf(ComName);
  if Result >= 0 then raise Exception.Create('Can not create doublication component.');
  Cmp := TVonListInputDataBase.Create(nil, ComName);
  Cmp.Parent:= ScrollBox1;// FlowPanel1;
  Cmp.ImgList:= ImageList1;
  if AWidth = 0 then begin
    if ScrollBox1.Width < 600 then AWidth:= ScrollBox1.Width - 28
    else AWidth:= Round((ScrollBox1.Width - 28) / (ScrollBox1.Width div 600));
  end;
  if AHeight = 0 then AHeight:= LargeCompHeight;
  Cmp.Name:= 'CMP_' + IntToStr(FList.Count);
  Cmp.AddControl(ComType, Prompty, Params, AWidth, AHeight);
  Cmp.Width:= AWidth;
  Cmp.Top:= FCurrentHeight;
  Cmp.Help:= Help;
  Inc(FCurrentHeight, Cmp.Height);
  //Cmp.Top:= 3000;
  case ComType of
    LIDT_CheckList, LIDT_Choice, LIDT_Memo, LIDT_RichText, LIDT_Bitmap:
      begin
        Inc(FLargeCount);
        flag := 1;
      end;
  else
    flag := 0;
  end;
  Result := FList.AddObject(ComName, Cmp);
//  Cmp.ImgList := ImageList1;
//  Cmp.OnEdit := FOnEdit;
//  Cmp.OnDialog := FOnDialog;
//  Cmp.OnSelect := FOnSelect;
//  Cmp.ImgList := ImageList1;
//  Cmp.SetPrompt(Prompty);
//  Cmp.SetParams(Params);
end;

procedure TFrameSettingInputor.Clear;
var
  i: Integer;
begin
  for i := FList.Count - 1 Downto 0 do
    FList.Objects[i].Free;
  FList.Clear;
  FLargeCount:= 0;
  FCurrentHeight:= 0;
end;

constructor TFrameSettingInputor.Create(AOwner: TComponent);
begin
  inherited;
  FList := TStringList.Create;
  FLargeCount := 0;
  FCurrentHeight:= 0;
end;

procedure TFrameSettingInputor.Delete(Index: Integer);
begin
  if (Index < 0) or (Index > FList.Count) then
    Exit;
  FList.Objects[Index].Free;
  FList.Delete(Index);
end;

destructor TFrameSettingInputor.Destroy;
begin
  Clear;
  FList.Free;
  inherited;
end;

procedure TFrameSettingInputor.FrameCanResize(Sender: TObject;
  var NewWidth, NewHeight: Integer; var Resize: Boolean);
var
  szH, szC, i: Integer;
begin
  szH := 0;
  szC := 0;
  if ScrollBox1.VertScrollBar.Range > NewHeight then
    Exit;
  for i := 0 to FList.Count - 1 do
    with TVonListInputDataBase(FList.Objects[i]) do
      case DataType of
        LIDT_CheckList, LIDT_Choice, LIDT_Memo, LIDT_RichText, LIDT_Bitmap:
          begin
            Height := Height + (NewHeight - ScrollBox1.VertScrollBar.Range)
              div FLargeCount;
          end;
      end;
end;

function TFrameSettingInputor.GetCount: Integer;
begin
  Result := FList.Count;
end;

function TFrameSettingInputor.GetData(Index: Integer): TVonListInputDataBase;
begin
  Result := TVonListInputDataBase(FList.Objects[Index]);
end;

function TFrameSettingInputor.GetNameData(Name: string): TVonListInputDataBase;
var
  idx: Integer;
begin
  idx := FList.IndexOf(Name);
  if idx < 0 then
    Result := nil
  else
    Result := GetData(idx);
end;

function TFrameSettingInputor.GetNames(Index: Integer): string;
begin
  Result := FList[Index];
end;

procedure TFrameSettingInputor.ScrollBox1MouseWheel(Sender: TObject;
  Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint;
  var Handled: Boolean);
begin
  if ScrollBox1.VertScrollBar.Visible then
    ScrollBox1.VertScrollBar.Position :=
      Round(ScrollBox1.VertScrollBar.Position - WheelDelta);
end;

procedure TFrameSettingInputor.ScrollBox1Resize(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to FList.Count - 1 do
    TVonListInputDataBase(FList.Objects[i]).Resize;
end;

procedure TFrameSettingInputor.SetLabelWidth(const Value: Integer);
var
  i: Integer;
begin
  FLabelWidth := Value;
  for i := 0 to FList.Count - 1 do
    TVonListInputDataBase(FList.Objects[i]).LabelWidth :=
      Round(FLabelWidth * 0.2);
end;

procedure TFrameSettingInputor.SetOnDialog(const Value: TEventOfEditData);
begin
  FOnDialog := Value;
end;

procedure TFrameSettingInputor.SetOnEdit(const Value: TEventOfEditData);
begin
  FOnEdit := Value;
end;

procedure TFrameSettingInputor.SetOnSelect(const Value: TEventOfEditData);
begin
  FOnSelect := Value;
end;

end.
