(*******************************************************************************
* TFrameTree 支持SQL自行展开，支持checkBox，支持关联选取
*-------------------------------------------------------------------------------
* 使用说明：
    FieldList         显示内容字段列表，以逗号间隔
    TableName         数据表名称
    ConditionSQL      查询条件SQL语句，不含WHERE关键字
    DisplayOrder      顺序字段
    DBConn            连接ADO
    DisplayFieldName  显示字段名称，用逗号间隔
    ImgFieldName      图标字段，<LEVEL>为特定字段，表示按层级显示图标
    KeyFieldName      主键字段
    ParentFieldName   父节点关联字段
    OnSelectedNode    选择节点时触发的事件
    OnUpdateOrder     更新节点时触发的事件
    AllowCheckChild   是否支持级选
    DisplayCheckBox   是否显示选择
    DisplayTool       是否显示工具条
    OnCheckNode       当选择项目时触发事件
    AutoExpand        是否自动展开
    ImageOffset       图标序号偏移量，配合 ImgFieldName 字段使用
    SelectedOffset    选择时图标序号偏移量，配合 ImgFieldName 字段使用
< Example >========= 参考代码 UPlatformRole ====================================
    FieldList := '*';         // FieldList := 'RoleName,RoleKind';
    TableName:= 'SYS_Role';
    ConditionSQL:= 'SpecialFlag<=' + szSpecial;
    DisplayFieldName := 'RoleName';
    DisplayOrder := 'ListOrder';
    ImgFieldName := 'LinkType';
    KeyFieldName := 'ID';
    ParentFieldName := 'PID';
*******************************************************************************)
unit UFrameTree;

interface

uses
  WinApi.CommCtrl, WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls,
  Forms, Dialogs, ComCtrls, ImgList, ToolWin, Data.win.ADODB, Grids, ValEdit, ExtCtrls, TypInfo,
  UVonLog, UPlatformDB, System.ImageList, Generics.Collections, Generics.Defaults, StdCtrls, Vcl.WinXCtrls;

type
  TFrameTree = class(TFrame)
    imgTree: TImageList;
    imgButton: TImageList;
    tree: TTreeView;
    split1: TSplitter;
    plItem: TPanel;
    ToolBar1: TToolBar;
    btnTreeAdd: TToolButton;
    btnTreeAddChild: TToolButton;
    btnTreeClone: TToolButton;
    btnTreeEdit: TToolButton;
    ToolButton4: TToolButton;
    btnTreeDel: TToolButton;
    ToolButton6: TToolButton;
    btnTreeTop: TToolButton;
    btnTreeUp: TToolButton;
    btnTreeDown: TToolButton;
    btnTreeBottom: TToolButton;
    ToolButton11: TToolButton;
    btnTreeLeft: TToolButton;
    btnTreeRight: TToolButton;
    ToolButton1: TToolButton;
    btnExpand: TToolButton;
    lstField1: TValueListEditor;
    procedure treeClick(Sender: TObject);
    procedure btnTreeTopClick(Sender: TObject);
    procedure btnTreeUpClick(Sender: TObject);
    procedure btnTreeDownClick(Sender: TObject);
    procedure btnTreeBottomClick(Sender: TObject);
    procedure treeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure treeGetSelectedIndex(Sender: TObject; Node: TTreeNode);
    procedure btnTreeLeftClick(Sender: TObject);
    procedure btnTreeRightClick(Sender: TObject);
    procedure plItemResize(Sender: TObject);
    procedure btnExpandClick(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure treeChange(Sender: TObject; Node: TTreeNode);
  private
    { Private declarations }
    FKeyFieldName: string;
    FDisplayFieldName: string;
    FParentFieldName: string;
    FOnSelectedNode: TTVChangedEvent;
    FImgFieldName: string;
    FOnUpdateOrder: TTVChangedEvent;
    FAllowCheckChild: Boolean;
    FDisplayCheckBox: Boolean;
    FOnCheckNode: TTVChangedEvent;
    FAutoExpand: Boolean;
    FDBConn: TADOConnection;
    FImageOffset: Integer;
    FSelectedOffset: Integer;
    FTableName: string;
    FFieldList: string;
    FConditionSQL: string;
    FDisplayItem: Boolean;
    FDisplayOrder: string;
    FStateOffset: Integer;
    FImgFieldRate: Integer;
    procedure SetDisplayFieldName(const Value: string);
    procedure SetKeyFieldName(const Value: string);
    procedure SetParentFieldName(const Value: string);
    procedure SetOnSelectedNode(const Value: TTVChangedEvent);
    procedure SetImgFieldName(const Value: string);
    procedure SetOnUpdateOrder(const Value: TTVChangedEvent);
    procedure tvToggleCheckbox(TreeView: TTreeView; Node: TTreeNode;
      isclick: Boolean = false);
    procedure ToggleCheckbox(Node: TTreeNode; OffsetParent: Boolean);
    procedure SetAllowCheckChild(const Value: Boolean);
    procedure SetDisplayCheckBox(const Value: Boolean);
    procedure SetDisplayTool(const Value: Boolean);
    function GetDisplayTool: Boolean;
    procedure SetOnCheckNode(const Value: TTVChangedEvent);
    procedure SetAutoExpand(const Value: Boolean);
    procedure SetDBConn(const Value: TADOConnection);
    procedure SetImageOffset(const Value: Integer);
    procedure SetSelectedOffset(const Value: Integer);
    procedure SetConditionSQL(const Value: string);
    procedure SetFieldList(const Value: string);
    procedure SetTableName(const Value: string);
    /// <summary>显示全部字段内容在右边详细区域内</summary>
    procedure Display(AID: string);
    function GetDisplayItem: Boolean;
    procedure SetDisplayItem(const Value: Boolean);
    procedure SetDisplayOrder(const Value: string);
    procedure DisplayItemPanel(Display: Boolean);
    procedure SetNodeImage(ANode: TTreeNode; ImgIdx: Integer);
    function GetID(Node: TTreeNode): string;
    procedure SetID(Node: TTreeNode; const Value: string);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    procedure Load(ParentNode: TTreeNode; PID: string);
    function AddNode(ANode: TTreeNode; Value: string; ImgIdx: Integer)
      : TTreeNode;
    function AddChild(PNode: TTreeNode; Value: string; ImgIdx: Integer)
      : TTreeNode;
    function Check(KeyValue: Integer; Checked: Boolean = true): Boolean;
    function IsChecked(Node: TTreeNode): Boolean;
    procedure SetChecked(Node: TTreeNode; Checked: Boolean; DoEvent: boolean = true);
    procedure SelectAll(Checked: Boolean; DoEvent: boolean = true);
    function FindNodeByID(ID: string): TTreeNode;
    property ID[Node: TTreeNode]: string read GetID write SetID;
  published
    /// <summary>显示内容字段列表，以逗号间隔</summary>
    property FieldList: string read FFieldList write SetFieldList;
    /// <summary>数据表名称</summary>
    property TableName: string read FTableName write SetTableName;
    /// <summary>查询条件SQL语句，不含WHERE关键字</summary>
    property ConditionSQL: string read FConditionSQL write SetConditionSQL;
    /// <summary>顺序字段</summary>
    property DisplayOrder: string read FDisplayOrder write SetDisplayOrder;
    /// <summary>连接ADO</summary>
    property DBConn: TADOConnection read FDBConn write SetDBConn;
    /// <summary>显示字段名称，用逗号间隔</summary>
    property DisplayFieldName: string read FDisplayFieldName
      write SetDisplayFieldName;
    /// <summary>图标字段</summary>
    property ImgFieldName: string read FImgFieldName write SetImgFieldName;
    /// <summary>主键字段</summary>
    property KeyFieldName: string read FKeyFieldName write SetKeyFieldName;
    /// <summary>父节点关联字段</summary>
    property ParentFieldName: string read FParentFieldName
      write SetParentFieldName;
    /// <summary>选择节点时触发的事件</summary>
    property OnSelectedNode: TTVChangedEvent read FOnSelectedNode
      write SetOnSelectedNode;
    /// <summary>更新节点时触发的事件</summary>
    property OnUpdateOrder: TTVChangedEvent read FOnUpdateOrder
      write SetOnUpdateOrder;
    /// <summary>是否支持级选</summary>
    property AllowCheckChild: Boolean read FAllowCheckChild
      write SetAllowCheckChild;
    /// <summary>是否显示选择</summary>
    property DisplayCheckBox: Boolean read FDisplayCheckBox
      write SetDisplayCheckBox;
    /// <summary>是否显示工具条</summary>
    property DisplayTool: Boolean read GetDisplayTool write SetDisplayTool;
    /// <summary>是否显示数据明细</summary>
    property DisplayItem: Boolean read FDisplayItem write SetDisplayItem;
    /// <summary>当选择项目时触发事件</summary>
    property OnCheckNode: TTVChangedEvent read FOnCheckNode
      write SetOnCheckNode;
    /// <summary>是否自动展开</summary>
    property AutoExpand: Boolean read FAutoExpand write SetAutoExpand;
    /// <summary>图标字段偏移量</summary>
    property ImgFieldRate: Integer read FImgFieldRate write FImgFieldRate;
    /// <summary>状态图标序号偏移量</summary>
    property StateOffset: Integer read FStateOffset write FStateOffset;
    /// <summary>图标序号偏移量</summary>
    property ImageOffset: Integer read FImageOffset write SetImageOffset;
    /// <summary>选择时图标序号偏移量</summary>
    property SelectedOffset: Integer read FSelectedOffset write SetSelectedOffset;
  end;

implementation

const
  TVIS_CHECKED = $2000;

type
  PNodeItemData = ^TNodeItemData;
  TNodeItemData = record
    ID: string;
  end;

{$R *.dfm}

{ TFrameTree }

function TFrameTree.Check(KeyValue: Integer; Checked: Boolean): Boolean;
var
  I: Integer;
begin
  for I := 0 to tree.Items.Count - 1 do
    if Integer(tree.Items[I].Data) = KeyValue then begin
      SetChecked(tree.Items[I], Checked);
      Break;
    end;
end;

constructor TFrameTree.Create(AOwner: TComponent);
begin
  inherited;
  FKeyFieldName:= 'ID';          //主键字段
  FParentFieldName:= 'PID';      //父节点关联字段
  FAllowCheckChild:= False;      //是否支持级选
  FDisplayCheckBox:= False;      //是否显示选择
  FAutoExpand:= False;           //是否自动展开
  FImgFieldName:= '<LEVEL>';     //图标序号字段名
  FImgFieldRate:= 1;             //图标字段偏移量
  FStateOffset:= 0;              //状态图标序号偏移量
  FImageOffset:= 0;              //图标序号偏移量
  FSelectedOffset:= 0;           //选择时图标序号偏移量
  ToolBar1.Visible:= True;       //是否显示工具条
end;

procedure TFrameTree.SetNodeImage(ANode: TTreeNode; ImgIdx: Integer);
begin
  if ImgFieldName = '<LEVEL>' then begin
    ANode.ImageIndex := ANode.Level * FImgFieldRate + FImageOffset;
    ANode.SelectedIndex := ANode.Level * FImgFieldRate + FSelectedOffset;
    if FStateOffset > 0 then
      ANode.StateIndex := ANode.Level * FImgFieldRate + FStateOffset;
  end else if ImgFieldName <> '' then begin
    ANode.ImageIndex := ImgIdx * FImgFieldRate + FImageOffset;
    ANode.SelectedIndex := ImgIdx * FImgFieldRate + FSelectedOffset;
    if FStateOffset > 0 then
      ANode.StateIndex := ImgIdx * FImgFieldRate + FStateOffset;
  end else begin
    ANode.ImageIndex := FImageOffset;
    ANode.SelectedIndex := FSelectedOffset;
    if FStateOffset > 0 then
      ANode.StateIndex := FStateOffset;
  end;
end;

function TFrameTree.AddChild(PNode: TTreeNode; Value: string; ImgIdx: Integer)
  : TTreeNode;
var
  P: PNodeItemData;
begin
  Result := tree.Items.AddChild(PNode, Value);
  New(P);
  Result.Data:= P;
  SetNodeImage(Result, ImgIdx);
end;

function TFrameTree.AddNode(ANode: TTreeNode; Value: string; ImgIdx: Integer)
  : TTreeNode;
var
  P: PNodeItemData;
begin
  Result := tree.Items.Add(ANode, Value);
  New(P);
  Result.Data:= P;
  SetNodeImage(Result, ImgIdx);
end;

(* 节点移动事件 *)

procedure TFrameTree.btnExpandClick(Sender: TObject);
begin
  if btnExpand.ImageIndex = 11 then DisplayItemPanel(False)
  else DisplayItemPanel(True);
end;

procedure TFrameTree.btnTreeBottomClick(Sender: TObject);
var
  szNode: TTreeNode;
begin
  if not Assigned(tree.Selected) then
    Exit;
  szNode := tree.Selected.getNextSibling;
  if not Assigned(szNode) then
    Exit;
  tree.Selected.MoveTo(szNode, naAdd);
  if Assigned(FOnUpdateOrder) then
    while Assigned(szNode) do begin
      FOnUpdateOrder(self, szNode);
      szNode:= szNode.getNextSibling();
    end;
end;

procedure TFrameTree.btnTreeDownClick(Sender: TObject);
var
  szNode1, szNode2: TTreeNode;
begin
  if not Assigned(tree.Selected) then
    Exit;
  szNode1 := tree.Selected.getNextSibling;
  if not Assigned(szNode1) then
    Exit;
  szNode2 := szNode1.getNextSibling;
  if not Assigned(szNode2) then begin
    tree.Selected.MoveTo(szNode1, naAdd);
    if Assigned(FOnUpdateOrder) then
        FOnUpdateOrder(self, szNode1);
  end else begin
    tree.Selected.MoveTo(szNode2, naInsert);
    if Assigned(FOnUpdateOrder) then
        FOnUpdateOrder(self, szNode2);
  end;
  if Assigned(FOnUpdateOrder) then
    FOnUpdateOrder(self, tree.Selected);
end;

procedure TFrameTree.btnTreeLeftClick(Sender: TObject);
var
  szNode1, szNode2: TTreeNode;
begin
  if not Assigned(tree.Selected) then
    Exit;
  szNode2 := tree.Selected.Parent;
  if not Assigned(szNode2) then
    Exit;
  szNode1 := tree.Selected.getNextSibling;
  tree.Selected.MoveTo(szNode2, naAdd);
  SetNodeImage(tree.Selected, Trunc((tree.Selected.ImageIndex - FImageOffset) / FImgFieldRate));
  if Assigned(FOnUpdateOrder) then
    FOnUpdateOrder(self, tree.Selected);
  if Assigned(FOnUpdateOrder) and Assigned(szNode1) then
    FOnUpdateOrder(self, szNode1);
end;

procedure TFrameTree.btnTreeRightClick(Sender: TObject);
var
  szNode1, szNode2: TTreeNode;
begin
  if not Assigned(tree.Selected) then
    Exit;
  szNode1 := tree.Selected.getPrevSibling;
  if not Assigned(szNode1) then
    Exit;
  szNode2 := tree.Selected.getNextSibling;
  tree.Selected.MoveTo(szNode1, naAddChild);
  SetNodeImage(tree.Selected, Trunc((tree.Selected.ImageIndex - FImageOffset) / FImgFieldRate));
  if Assigned(FOnUpdateOrder) then
    FOnUpdateOrder(self, tree.Selected);
  if Assigned(FOnUpdateOrder) and Assigned(szNode2) then
    FOnUpdateOrder(self, szNode2);
end;

procedure TFrameTree.btnTreeTopClick(Sender: TObject);
var
  szNode: TTreeNode;
begin
  if not Assigned(tree.Selected) then
    Exit;
  szNode := tree.Selected.getPrevSibling();
  if not Assigned(szNode) then
    Exit;
  tree.Selected.MoveTo(szNode, naAddFirst);
  if Assigned(FOnUpdateOrder) then
    while Assigned(szNode) do begin
      FOnUpdateOrder(self, szNode);
      szNode:= szNode.getPrevSibling();
    end;
end;

procedure TFrameTree.btnTreeUpClick(Sender: TObject);
var
  szNode: TTreeNode;
begin
  if not Assigned(tree.Selected) then
    Exit;
  if tree.Selected.Index = 0 then
    Exit;
  szNode := tree.Selected.getPrevSibling();
  tree.Selected.MoveTo(szNode, naInsert);
  if Assigned(FOnUpdateOrder) then begin
      FOnUpdateOrder(self, szNode);
      FOnUpdateOrder(self, tree.Selected);
    end;
end;

(* About display *)

procedure TFrameTree.DisplayItemPanel(Display: Boolean);
begin
  if Display then begin
    if plItem.Width < 64 then plItem.Width:= 180;
    btnExpand.ImageIndex:= 11;
    lstField1.Visible:= True;
    lstField1.ColWidths[0]:= Round(lstField1.Width * 0.4) ;
  end else begin
    btnExpand.ImageIndex:= 12;
    plItem.Width:= 23;
    lstField1.Visible:= False;
    plItem.Left:= split1.Left + split1.Width + 2;
  end;
end;

procedure TFrameTree.plItemResize(Sender: TObject);
begin
  if plItem.Width < 64 then DisplayItemPanel(False) else DisplayItemPanel(true);
end;

procedure TFrameTree.FrameResize(Sender: TObject);
begin
  DisplayItemPanel(Self.Width > 240);
  lstField1.FixedColor:= Self.Color;
end;

procedure TFrameTree.Display(AID: string);
var
  I: Integer;
begin
  with TADOQuery.Create(nil) do
  try
    Connection := FDBConn;
    SQL.Text := 'SELECT ' + FFieldList + ' FROM ' + FTableName + ' WHERE ' +
      FKeyFieldName + '=''' + AID + '''';
    if FConditionSQL <> '' then
      SQL.Text:= SQL.Text + ' AND ' + FConditionSQL;
    Open;
//    if lstField1.RowCount < FieldCount then begin
      lstField1.Strings.Clear;
      for I := 0 to FieldCount - 1 do
        lstField1.Strings.Add(Fields[I].FieldName + '=' + Fields[I].AsString);
//    end
  finally
    Free;
  end;
end;

(* Methods *)

function TFrameTree.FindNodeByID(ID: string): TTreeNode;
  function FindData(PNode: TTreeNode): TTreeNode;
  var
    szNode: TTreeNode;
  begin
    Result := nil;
    if not Assigned(PNode) then
      Exit;
    if PNodeItemData(PNode.Data).ID = ID then
    begin
      Result := PNode;
      Exit;
    end;
    if (PNode.HasChildren) and (PNode.Count = 0) then
      Load(PNode, PNodeItemData(PNode.Data).ID);
    if PNode.Count > 0 then
      Result := FindData(PNode.getFirstChild);
    szNode := PNode.GetNext;
    while (not Assigned(Result)) and Assigned(szNode) do
    begin
      Result := FindData(szNode);
      szNode := szNode.GetNext;
    end;
  end;
begin
  Result := FindData(tree.Items[0]);
end;

procedure TFrameTree.Load(ParentNode: TTreeNode; PID: string);
var
  szNode: TTreeNode;
  szS: string;
  P: PNodeItemData;
begin
  with TADOQuery.Create(nil) do
  try
    Connection := FDBConn;
    szS:= '[' + FKeyFieldName + '],[' + FDisplayFieldName + ']';
    if(FImgFieldName <> '')and(FImgFieldName <> '<LEVEL>')then
      szS:= szS + ',[' + FImgFieldName + ']';
    SQL.Text := 'SELECT ' + szS + ' FROM ' + FTableName + ' WHERE ' +
      FParentFieldName + '=''' + PID + '''';
    if FConditionSQL <> '' then SQL.Add(' AND ' + FConditionSQL);
    if FDisplayOrder <> '' then SQL.Add(' ORDER BY ' + FDisplayOrder);
    WriteLog(LOG_DEBUG, 'TFrameTree', SQL.Text);
    Open;
    while not EOF do
    begin
      szNode := tree.Items.AddChild(ParentNode, FieldByName(DisplayFieldName)
        .AsString);
      with szNode do
      begin
        if ImgFieldName <> '' then
          SetNodeImage(szNode, FieldByName(ImgFieldName).AsInteger);
        new(P);
        P.ID:= FieldByName(KeyFieldName).AsString;
        Data := P;
        SetChecked(szNode, False, false);
        if FAutoExpand then begin
          Load(szNode, FieldByName(KeyFieldName).AsString);
          Expanded := true;
        end else HasChildren := True;
      end;
      Next;
    end;
  finally
    Free;
  end;
end;

(* Properties *)

function TFrameTree.GetDisplayItem: Boolean;
begin
  Result:= lstField1.Visible;
end;

function TFrameTree.GetDisplayTool: Boolean;
begin
  Result := ToolBar1.Visible;
end;

function TFrameTree.GetID(Node: TTreeNode): string;
begin
  Result:= PNodeItemData(Node.Data).ID;
end;

procedure TFrameTree.SetDBConn(const Value: TADOConnection);
begin
  FDBConn := Value;
end;

procedure TFrameTree.SetDisplayCheckBox(const Value: Boolean);
begin
  FDisplayCheckBox := Value;
  if FDisplayCheckBox then
    SetWindowLong(tree.Handle, GWL_STYLE, GetWindowLong(tree.Handle, GWL_STYLE)
      or TVS_CHECKBOXES)
  else
    SetWindowLong(tree.Handle, GWL_STYLE, GetWindowLong(tree.Handle, GWL_STYLE)
      and (not TVS_CHECKBOXES))
end;

procedure TFrameTree.SetDisplayFieldName(const Value: string);
begin
  FDisplayFieldName := Value;
end;

procedure TFrameTree.SetDisplayItem(const Value: Boolean);
begin
  lstField1.Visible:= Value;
end;

procedure TFrameTree.SetDisplayOrder(const Value: string);
begin
  FDisplayOrder := Value;
end;

procedure TFrameTree.SetDisplayTool(const Value: Boolean);
begin
  ToolBar1.Visible := Value;
end;

procedure TFrameTree.SetFieldList(const Value: string);
begin
  FFieldList := Value;
end;

procedure TFrameTree.SetID(Node: TTreeNode; const Value: string);
begin
  PNodeItemData(Node.Data).ID:= Value;
end;

procedure TFrameTree.SetImageOffset(const Value: Integer);
begin
  FImageOffset := Value;
end;

procedure TFrameTree.SetImgFieldName(const Value: string);
begin
  FImgFieldName := Value;
end;

procedure TFrameTree.SetKeyFieldName(const Value: string);
begin
  FKeyFieldName := Value;
end;

procedure TFrameTree.SetOnCheckNode(const Value: TTVChangedEvent);
begin
  FOnCheckNode := Value;
end;

procedure TFrameTree.SetOnSelectedNode(const Value: TTVChangedEvent);
begin
  FOnSelectedNode := Value;
end;

procedure TFrameTree.SetOnUpdateOrder(const Value: TTVChangedEvent);
begin
  FOnUpdateOrder := Value;
end;

procedure TFrameTree.SetParentFieldName(const Value: string);
begin
  FParentFieldName := Value;
end;

procedure TFrameTree.SetSelectedOffset(const Value: Integer);
begin
  FSelectedOffset := Value;
end;

procedure TFrameTree.SetTableName(const Value: string);
begin
  FTableName := Value;
end;

procedure TFrameTree.SetConditionSQL(const Value: string);
begin
  FConditionSQL := Value;
end;

(* Events *)

procedure TFrameTree.ToggleCheckbox(Node: TTreeNode; OffsetParent: Boolean);
var
  ParentNode, szNode: TTreeNode;
  st, szAllow: Boolean;
begin
  st := IsChecked(Node);
  szAllow := True;
  treeExpanding(tree, Node, szAllow);
  szNode := Node.getFirstChild; // 遍历子树,选中 则子节点 全部 为选中；
  while Assigned(szNode) do
  begin
    if IsChecked(szNode) <> st then
      SetChecked(szNode, st);
    ToggleCheckbox(szNode, false);
    szNode := szNode.getNextSibling;
  end;
  if not OffsetParent then
    Exit;
  ParentNode := Node.Parent; // 遍历父节点中的子树，如果子节点都选中了，则父节点也选中
  while Assigned(ParentNode) do
  begin
    st := True;
    szNode := ParentNode.getFirstChild;
    while Assigned(szNode) and st do
    begin
      st := st and IsChecked(szNode);
      szNode := szNode.getNextSibling;
    end;
    if IsChecked(ParentNode) <> st then
      SetChecked(ParentNode, st);
    ParentNode := ParentNode.Parent;
  end;
end;

procedure TFrameTree.treeChange(Sender: TObject; Node: TTreeNode);
begin
  if not Assigned(Node) then Exit;
  Display(PNodeItemData(Node.Data).ID);
  if Assigned(Node) and Assigned(FOnSelectedNode) then
    FOnSelectedNode(Sender, Node);
end;

procedure TFrameTree.treeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if not Assigned(Node) then
    Exit;
  if not Node.HasChildren then
    Exit;
  if Node.Count > 0 then
    Exit;
  Node.HasChildren := false;
  Load(Node, PNodeItemData(Node.Data).ID);
end;

procedure TFrameTree.treeGetSelectedIndex(Sender: TObject; Node: TTreeNode);
begin
  Node.SelectedIndex := Node.ImageIndex + FSelectedOffset
end;

function TFrameTree.IsChecked(Node: TTreeNode): Boolean;
var
  TvItem: TTVItem;
begin
  if not Assigned(Node) then
    Exit;
  TvItem.Mask := TVIF_STATE;
  TvItem.hItem := Node.ItemId;
  TreeView_GetItem(Node.TreeView.Handle, TvItem);
  Result := (TvItem.State and TVIS_CHECKED) = TVIS_CHECKED;
end;

procedure TFrameTree.SelectAll(Checked: Boolean; DoEvent: Boolean);
var
  I: Integer;
begin
  for I := 0 to tree.Items.Count - 1 do
    SetChecked(tree.Items[I], Checked, DoEvent);
end;

procedure TFrameTree.SetAllowCheckChild(const Value: Boolean);
begin
  FAllowCheckChild := Value;
end;

procedure TFrameTree.SetAutoExpand(const Value: Boolean);
begin
  FAutoExpand := Value;
end;

procedure TFrameTree.SetChecked(Node: TTreeNode; Checked, DoEvent: Boolean);
var
  TvItem: TTVItem;
begin
  FillChar(TvItem, SizeOf(TvItem), 0);
  with TvItem do
  begin
    hItem := Node.ItemId;
    Mask := TVIF_STATE;
    StateMask := TVIS_STATEIMAGEMASK;
    if Checked then
      TvItem.State := TVIS_CHECKED
    else TvItem.State := TVIS_CHECKED shr 1;
    TreeView_SetItem(tree.Handle, TvItem);
    if DoEvent and Assigned(FOnCheckNode) then
      FOnCheckNode(self, Node);
  end;
end;

procedure TFrameTree.tvToggleCheckbox(TreeView: TTreeView; Node: TTreeNode;
  isclick: Boolean = false);
var
  ParentNode, szNode: TTreeNode;
  flg1, szAllow: Boolean;
begin
  szAllow := True;
  treeExpanding(tree, Node, szAllow);
  with TreeView do
  begin
    if IsChecked(Node) then
    begin
      szNode := Node.getFirstChild; // 遍历子树,选中 则子节点 全部 为选中；
      while szNode <> nil do
      begin
        SetChecked(szNode, True);
        tvToggleCheckbox(TreeView, szNode, True);
        szNode := szNode.getNextSibling;
      end;

      ParentNode := Node.Parent; // 父；
      if ParentNode <> nil then
      begin
        if not IsChecked(ParentNode) then
        begin
          szNode := ParentNode.getFirstChild; // 遍历子树；
          flg1 := false;
          while szNode <> nil do
          begin
            if (not IsChecked(szNode)) then // true,有未选中
              flg1 := True;
            if flg1 then // 已有、退出loop;
              szNode := nil
            else
              szNode := szNode.getNextSibling;
          end;
          SetChecked(ParentNode, not flg1);
          tvToggleCheckbox(TreeView, ParentNode, false);
        end;
      end; // end parentNode 不等于空
    end
    else if not IsChecked(Node) then
    begin
      ParentNode := Node.Parent; // 父；
      if ParentNode <> nil then
      begin
        if IsChecked(ParentNode) then
        begin
          SetChecked(ParentNode, false);
          tvToggleCheckbox(TreeView, ParentNode);
        end;
      end; // end parentnode

      if (isclick) then
      begin
        szNode := Node.getFirstChild; // 遍历子树,未选中 则子节点 全部 为未选中；
        while szNode <> nil do
        begin
          SetChecked(szNode, false);
          tvToggleCheckbox(TreeView, szNode, True);
          szNode := szNode.getNextSibling;
        end;
      end;

    end;
  end;
end;

procedure TFrameTree.treeClick(Sender: TObject);
var
  P: TPoint;
  treenode: TTreeNode;
begin
  GetCursorPos(P);
  P := tree.ScreenToClient(P);
  if (htOnStateIcon in tree.GetHitTestInfoAt(P.X, P.Y)) then
  begin
    treenode := tree.GetNodeAt(P.X, P.Y);
    if Assigned(FOnCheckNode) then
      FOnCheckNode(self, treenode);
    if FAllowCheckChild then
      ToggleCheckbox(treenode, True);
    // tvToggleCheckbox(tree, treenode, true);
  end;
end;

end.
