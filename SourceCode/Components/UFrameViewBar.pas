{ Copyright:      Jamesvon  mailto:Jamesvon@163.COM
  Author:         James von
  Remarks:        freeware, but this Copyright must be included
  known Problems: none
  Version:        1.0
  Description:    A frame of listview with a bar
  Include:        UDB(Get configration filename)

  * THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS
  * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
  * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
  * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
  * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
  * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  Hint: 打印时，页面标题内容
  HelpKeyword: 本表格栏目信息存储文件名称
}
unit UFrameViewBar;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, StdCtrls, ComCtrls, ToolWin, DB, Data.Win.ADODB, IniFiles, Menus,
  System.ImageList;

type
  TEventToGetImageIdx = procedure(Item: TListItem; var IamgeIdx: integer)
    of object;

  TFrameViewBar = class(TFrame)
    ToolBar: TToolBar;
    ToolBtnExport: TToolButton;
    ToolBtnPrint: TToolButton;
    ToolBtnPrinter: TToolButton;
    ToolButton12: TToolButton;
    ToolBtnSearch: TToolButton;
    ToolButton14: TToolButton;
    ToolBtnColumns: TToolButton;
    ToolButton16: TToolButton;
    ToolBtnView: TToolButton;
    ToolButton18: TToolButton;
    LCount: TLabel;
    ImageList1: TImageList;
    lstView: TListView;
    ToolBtnIcon: TToolButton;
    ToolBtnSmallIcon: TToolButton;
    ToolBtnList: TToolButton;
    ToolBtnReport: TToolButton;
    ToolButton5: TToolButton;
    PopupMenu1: TPopupMenu;
    SaveDialog1: TSaveDialog;
    procedure ToolBtnExportClick(Sender: TObject);
    procedure ToolBtnPrintClick(Sender: TObject);
    procedure ToolBtnPrinterClick(Sender: TObject);
    procedure GridSourceStateChange(Sender: TObject);
    procedure lstViewColumnClick(Sender: TObject; Column: TListColumn);
    procedure ToolBtnColumnsClick(Sender: TObject);
    procedure lstViewCompare(Sender: TObject; Item1, Item2: TListItem;
      Data: integer; var Compare: integer);
    procedure ToolBtnIconClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure lstViewSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
  private
    { Private declarations }
    FOldState: TDataSetState;
    FSectionName: string; // 窗口名称
    FCurrentCount: integer; // 当前显示数量
    FColumnToSort: integer; // 排序变量
    FOrderType: integer; // 排序方式
    FKeyFieldName: string; // 数据库主键
    FEventBackcall: TEventToGetImageIdx; // 回叫函数
    procedure SaveTitles; // 保存列名称
    procedure SetData(Node: TListItem);
    procedure DisplayItemCount;
  private
    { KeyDataSet events declarations }
    FKeyDataSet: TDataSet;
    FKeyDataSetOperate: Char;
    procedure KeyDataSetBeforeDelete(DataSet: TDataSet);
    procedure KeyDataSetAfterEdit(DataSet: TDataSet);
    procedure KeyDataSetAfterInsert(DataSet: TDataSet);
    procedure KeyDataSetAfterOpen(DataSet: TDataSet);
    procedure KeyDataSetAfterPost(DataSet: TDataSet);
    function GetCellName(Col, Row: integer): string;
    procedure WriteExcel(filename: string);
    procedure WriteText(filename: string);
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure LoadTitles(FormName: string = '');
    function GetTagValue(Item: TListItem; FieldName: string): string;
  published
    property EventToGetImageIdx: TEventToGetImageIdx read FEventBackcall
      write FEventBackcall;
  end;

implementation

uses UDlgGridColumn, OLE_Excel_TLB;

{$R *.dfm}
(* Events of bar *)

procedure TFrameViewBar.ToolBtnExportClick(Sender: TObject);
begin
  SaveDialog1.DefaultExt := '';
  SaveDialog1.Filter := 'Excel文件|*.xls|格式文件|*.csv|文本文件|*.txt';
  if not SaveDialog1.Execute then
    Exit;
  case SaveDialog1.FilterIndex of
    0:
      WriteExcel(ChangeFileExt(SaveDialog1.filename, '.xls'));
    1:
      WriteExcel(ChangeFileExt(SaveDialog1.filename, '.xls'));
    2:
      WriteText(ChangeFileExt(SaveDialog1.filename, '.csv'));
    3:
      WriteText(ChangeFileExt(SaveDialog1.filename, '.txt'));
  end;
end;

function TFrameViewBar.GetCellName(Col, Row: integer): string;
var // 得到单元Excel位置,Cell(2,3)->B3
  szInt: integer;
  szCol: string;

  function Cvt: Char;
  begin
    Result := Char(65 + (szInt - 1) mod 26);
    szInt := (szInt - 1) div 26;
  end;

begin
  szCol := '';
  szInt := Col;
  while szInt > 0 do
    szCol := Cvt + szCol;
  Result := szCol + IntToStr(Row);
end;

procedure TFrameViewBar.ToolBtnPrintClick(Sender: TObject);
begin // 打印
  // ViewPrint(lstView, '列表信息', False);
end;

procedure TFrameViewBar.ToolBtnPrinterClick(Sender: TObject);
begin // 预览
  // ViewPrint(lstView, '列表信息', True);
end;

procedure TFrameViewBar.WriteExcel(filename: string);
var // 导出
  ACol, ARow: integer;
  excelApp: ExcelApplication;
  book: _Workbook;
  sheet: _Worksheet;
begin
  Screen.Cursor := crHourGlass;
  excelApp := CoExcelApplication.Create;
  excelApp.Visible[0] := True;
  book := excelApp.Workbooks.Add(EmptyParam, 0);
  sheet := book.Sheets.Item[1] as _Worksheet;
  sheet.Cells.Item[1, 1].Value2 := lstView.Hint;
  sheet.Range[GetCellName(1, 1), GetCellName(lstView.Columns.Count, 1)
    ].Merge(4);
  with sheet.Cells, lstView do // Column title
    for ACol := 0 to lstView.Columns.Count - 1 do
      if lstView.Columns[ACol].Width > 0 then
        Item[2, ACol + 1].Value2 := Columns[ACol].Caption;
  with sheet.Cells, lstView do // Data
    for ARow := 0 to lstView.Items.Count - 1 do
    begin
      Item[ARow + 3, 1].Value2 := lstView.Items[ARow].Caption;
      for ACol := 1 to lstView.Items[ARow].SubItems.Count - 1 do
        if Columns[ACol].Width > 0 then
          Item[ARow + 3, ACol + 1].Value2 := lstView.Items[ARow].SubItems
            [ACol - 1];
    end;
  book.SaveAs(filename, xlAddIn, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, 0, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, 0);
  // TExcelApplication(excelApp).Disconnect;
  Screen.Cursor := crDefault;
end;

procedure TFrameViewBar.WriteText(filename: string);
var // 导出
  ACol, ARow: integer;
  F: TextFile;
  S: string;
begin
  AssignFile(F, filename);
  Rewrite(F);
  Screen.Cursor := crHourGlass;
  S := '';
  for ACol := 0 to lstView.Columns.Count - 1 do
    if lstView.Columns[ACol].Width > 0 then
      S := S + ',"' + lstView.Columns[ACol].Caption + '"';
  Delete(S, 1, 1);
  Writeln(F, S);
  ARow := 3;
  with lstView do // Data
    for ARow := 0 to lstView.Items.Count - 1 do
    begin
      S := '"' + lstView.Items[ARow].Caption + '"';
      for ACol := 0 to lstView.Items[ARow].SubItems.Count - 1 do
        if Columns[ACol].Width > 0 then
          S := S + ',"' + lstView.Items[ARow].SubItems[ACol] + '"';
      Writeln(F, S);
    end;
  CloseFile(F);
  Screen.Cursor := crDefault;
end;

procedure TFrameViewBar.ToolBtnColumnsClick(Sender: TObject);
begin // 列表筛选
  FilterColumn(lstView);
end;

procedure TFrameViewBar.ToolBtnIconClick(Sender: TObject);
begin // 图标显示方式变更
  (* if not (Sender as TToolButton).Down then begin
    (Sender as TToolButton).Down:= True;
    Exit;
    end;     // *)
  ToolBtnIcon.Down := (Sender as TToolButton) = ToolBtnIcon;
  ToolBtnSmallIcon.Down := (Sender as TToolButton) = ToolBtnSmallIcon;
  ToolBtnList.Down := (Sender as TToolButton) = ToolBtnList;
  ToolBtnReport.Down := (Sender as TToolButton) = ToolBtnReport;
  if ToolBtnIcon.Down then
    lstView.ViewStyle := vsIcon;
  if ToolBtnSmallIcon.Down then
    lstView.ViewStyle := vsSmallIcon;
  if ToolBtnList.Down then
    lstView.ViewStyle := vsList;
  if ToolBtnReport.Down then
    lstView.ViewStyle := vsReport;
end;

procedure TFrameViewBar.DisplayItemCount;
begin
  LCount.Caption := '当前列表中有' + IntToStr(FKeyDataSet.RecordCount) + '数据';
end;

(* DataSource events *)

procedure TFrameViewBar.GridSourceStateChange(Sender: TObject);
begin // 当数据状态发生变化时
  // //调用主数据库
  // if GridSource.DataSet.DataSource <> nil then
  // FKeyDataSet:= GridSource.DataSet.DataSource.DataSet
  // else FKeyDataSet:= GridSource.DataSet;
  // //设置数据库关联函数
  // FKeyDataSet.AfterOpen:= KeyDataSetAfterOpen;
  // FKeyDataSet.AfterEdit:= KeyDataSetAfterEdit;
  // FKeyDataSet.AfterInsert:= KeyDataSetAfterInsert;
  // FKeyDataSet.BeforeDelete:= KeyDataSetBeforeDelete;
  // FKeyDataSet.AfterPost:= KeyDataSetAfterPost;
  // GridSource.OnStateChange:= nil;
  // if FKeyDataSet <> GridSource.DataSet then KeyDataSetAfterOpen(FKeyDataSet);
end;

(* Frame Events *)

constructor TFrameViewBar.Create(AOwner: TComponent);
var
  TempCharPos: integer;
begin // 控件初始化
  inherited;
  FOrderType := 1; // 主要排序字段序号
  FSectionName := AOwner.Name;
  TempCharPos := Pos('_', FSectionName);
  if TempCharPos > 0 then
    FSectionName := Copy(FSectionName, 1, TempCharPos - 1);
  LoadTitles; // 加载标题
  FOldState := dsInactive; // 初始化数据库原始变更状态
end;

destructor TFrameViewBar.Destroy;
begin // 卸载控件
  if (FSectionName <> '') then
    SaveTitles; // 保存用户标题修改结果
  inherited;
end;

procedure TFrameViewBar.LoadTitles(FormName: string);
var // 加载标题
  TitleFilename: string;
  i: integer;
  szValue: string;

  function Locate(Flag: string): string;
  var
    mPos: integer;
  begin
    mPos := Pos(Flag, szValue);
    if mPos > 0 then
      Result := Copy(szValue, 1, mPos - 1)
    else
      Result := szValue;
    szValue := Copy(szValue, mPos + 1, Length(szValue) - mPos);
  end;

begin
  if FormName <> '' then
    FSectionName := FormName;
  TitleFilename := ExtractFilePath(Application.ExeName) + 'Res\' + FSectionName
    + '.Titles';
  if not FileExists(TitleFilename) then
    Exit;
  lstView.Columns.Clear;
  with TStringList.Create do
    try
      LoadFromFile(TitleFilename);
      for i := 0 to Count - 1 do
      begin
        szValue := Strings[i]; // Tag=DisplayName,Width,Index
        with lstView.Columns.Add do
        begin
          Tag := StrToInt(Locate('=')); // Tag      = Field index
          Caption := Locate(','); // Caption  = Column caption
          Width := StrToInt(Locate(',')); // Width    = Column width
          Index := StrToInt(szValue); // Index    = Column index
        end;
      end;
    finally
      Free;
    end;
end;

procedure TFrameViewBar.SaveTitles;
var // 保存标题信息
  i: integer;
begin
  with TStringList.Create do
    try
      for i := 0 to lstView.Columns.Count - 1 do
        with lstView.Columns[i] do
          Add(Format('%d=%s,%d,%d', [Tag, DisplayName, Width, Index]));
      SaveToFile(ExtractFilePath(Application.ExeName) + 'Res\' + FSectionName +
        '.Titles');
    finally
      Free;
    end;
end;

(* ListView Events *)

procedure TFrameViewBar.lstViewColumnClick(Sender: TObject;
  Column: TListColumn);
begin // 排序
  if FColumnToSort = Column.Index then
    FOrderType := -FOrderType;
  FColumnToSort := Column.Index;
  (Sender as TCustomListView).AlphaSort;
end;

procedure TFrameViewBar.lstViewCompare(Sender: TObject; Item1, Item2: TListItem;
  Data: integer; var Compare: integer);
var
  ix: integer;

  function CompareIt(V1, V2: string): integer;
  var
    A1, A2: Extended;
  begin
    if TryStrToFloat(V1, A1) and TryStrToFloat(V2, A2) then
    begin
      if A2 > A1 then
        Result := 1
      else if A2 < A1 then
        Result := -1
      else
        Result := 0;
    end
    else
      Result := CompareText(V1, V2);

  end;

begin // 排序显示
  if FColumnToSort = 0 then
    Compare := FOrderType * CompareIt(Item1.Caption, Item2.Caption)
  else
  begin
    ix := FColumnToSort - 1;
    Compare := FOrderType * CompareIt(Item1.SubItems[ix], Item2.SubItems[ix]);
  end;
end;

// to delete

procedure TFrameViewBar.SetData(Node: TListItem);
var // 显示数据
  i, ImageIdx: integer;
  KeyField: TField;
begin
  // Node.Caption:= GridSource.DataSet.Fields[lstView.Columns[0].Tag].AsString;
  // ImageIdx:= 0;
  (* if KeyFieldName <> '' then begin
    KeyField:= GridSource.DataSet.FieldByName(KeyFieldName);
    case KeyField.DataType of
    ftUnknown, ftParadoxOle, ftDBaseOle, ftTypedBinary, ftCursor,
    ftADT, ftArray, ftReference, ftDataSet, ftOraBlob, ftOraClob,
    ftVariant, ftInterface, ftIDispatch, ftFMTBcd:              Node.Data:= nil;  //Unknown
    ftString, ftFixedChar, ftWideString:                        Node.Data:= Pointer(KeyField.AsString);   //string
    ftSmallint, ftInteger, ftWord, ftLargeint, ftAutoInc:       Node.Data:= Pointer(KeyField.AsInteger);  //Integer
    ftFloat, ftCurrency, ftBCD:                                 Node.Data:= Pointer(KeyField.AsString); //Float
    ftGuid:                                                     Node.Data:= Pointer(KeyField.AsString);   //uniqueidentifier
    ftDate, ftTime, ftDateTime, ftTimeStamp:                    Node.Data:= Pointer(KeyField.AsString); //Date,Time,Datetime
    ftBoolean:                                                  Node.Data:= Pointer(KeyField.AsBoolean);  //bit
    ftBytes, ftVarBytes, ftBlob, ftMemo, ftGraphic, ftFmtMemo:  Node.Data:= nil;
    end;
    end;   // *)
  // for i:= 1 to lstView.Columns.Count - 1 do begin
  // if Node.SubItems.Count < i then
  // Node.SubItems.Add(GridSource.DataSet.Fields[lstView.Columns[i].Tag].AsString)
  // else
  // Node.SubItems[i - 1]:= GridSource.DataSet.Fields[lstView.Columns[i].Tag].AsString;
  // end;
  // if Assigned(FEventBackcall) then
  // FEventBackcall(Node, ImageIdx);
  // Node.ImageIndex:= ImageIdx;
  // Node.StateIndex:= 0;
end;

procedure TFrameViewBar.PopupMenu1Popup(Sender: TObject);
var // 鼠标右键弹出菜单管理
  i: integer;
  FNewMenu: TMenuItem;
begin
  PopupMenu1.Items.Clear;
  if lstView.Selected = nil then
    Exit;
  for i := 0 to lstView.Selected.SubItems.Count - 1 do
  begin
    FNewMenu := TMenuItem.Create(nil);
    FNewMenu.Caption := lstView.Columns[i + 1].Caption + ': ' +
      lstView.Selected.SubItems[i];
    PopupMenu1.Items.Add(FNewMenu);
  end;
end;

function TFrameViewBar.GetTagValue(Item: TListItem; FieldName: string): string;
var // 根据数据库字段序号得到字段内容
  i, TagID: integer;
begin
  // TagID:= GridSource.DataSet.FieldByName(FieldName).Index;
  with lstView do
    for i := 0 to Columns.Count - 1 do
      if Columns[i].Tag = TagID then
      begin
        if Columns[i].Index = 0 then
          Result := Item.Caption
        else
          Result := Item.SubItems[i - 1];
        Break;
      end;
end;

(* Key dataset events *)

procedure TFrameViewBar.KeyDataSetAfterEdit(DataSet: TDataSet);
begin // 当数据修改后处理
  FKeyDataSetOperate := 'E';
end;

procedure TFrameViewBar.KeyDataSetBeforeDelete(DataSet: TDataSet);
begin // 当数据修改后处理
  lstView.Selected.Delete;
  DisplayItemCount;
end;

procedure TFrameViewBar.KeyDataSetAfterInsert(DataSet: TDataSet);
begin // 当数据添加后
  FKeyDataSetOperate := 'I';
end;

procedure TFrameViewBar.KeyDataSetAfterOpen(DataSet: TDataSet);
var
  OrgViewStyle: TViewStyle;
begin // 当数据库打开后
  OrgViewStyle := lstView.ViewStyle; // 保存当前显示风格
  lstView.Clear;
  while not FKeyDataSet.EOF do
  begin
    SetData(lstView.Items.Add);
    FKeyDataSet.Next;
  end;
  lstView.ViewStyle := OrgViewStyle; // 恢复显示状态
  DisplayItemCount;
end;

procedure TFrameViewBar.KeyDataSetAfterPost(DataSet: TDataSet);
begin
  case FKeyDataSetOperate of
    'E':
      SetData(lstView.Selected);
    'I':
      SetData(lstView.Items.Add);
  end;
  DisplayItemCount;
end;

procedure TFrameViewBar.lstViewSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
begin
  if Assigned(FKeyDataSet) then
    FKeyDataSet.Locate('ID', GetTagValue(Item, 'ID'), []);
end;

end.
