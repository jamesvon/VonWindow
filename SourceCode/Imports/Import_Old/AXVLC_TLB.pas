unit AXVLC_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 2017/4/22 16:59:51 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Program Files (x86)\VideoLAN\VLC\axvlc.dll (1)
// LIBID: {DF2BBE39-40A8-433B-A279-073F48DA94B6}
// LCID: 0
// Helpfile: 
// HelpString: VideoLAN VLC ActiveX Plugin
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// SYS_KIND: SYS_WIN32
// Errors:
//   Hint: Member 'file' of 'IVLCLogo' changed to 'file_'
//   Hint: Member 'repeat' of 'IVLCLogo' changed to 'repeat_'
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Vcl.OleCtrls, Vcl.OleServer, Winapi.ActiveX;
  


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  AXVLCMajorVersion = 1;
  AXVLCMinorVersion = 0;

  LIBID_AXVLC: TGUID = '{DF2BBE39-40A8-433B-A279-073F48DA94B6}';

  IID_IVLCAudio: TGUID = '{9E0BD17B-2D3C-4656-B94D-03084F3FD9D4}';
  IID_IVLCChapter: TGUID = '{5AF314CF-8849-4A79-A3FC-8DE6625D9E72}';
  IID_IVLCInput: TGUID = '{49E0DBD1-9440-466C-9C97-95C67190C603}';
  IID_IVLCTitle: TGUID = '{B5DEF5A1-FFB6-4E68-B3D8-A12AC60FDA54}';
  IID_IVLCLogo: TGUID = '{8A4A20C2-93F3-44E8-8644-BEB2E3487E84}';
  IID_IVLCDeinterlace: TGUID = '{BC97469F-CB11-4037-8DCE-5FC9F5F85307}';
  IID_IVLCMarquee: TGUID = '{8D076AD6-9B6F-4150-A0FD-5D7E8C8CB02C}';
  IID_IVLCPlaylist: TGUID = '{54613049-40BF-4035-9E70-0A9312C0188D}';
  IID_IVLCPlaylistItems: TGUID = '{FD37FE32-82BC-4A25-B056-315F4DBB194D}';
  IID_IVLCSubtitle: TGUID = '{465E787A-0556-452F-9477-954E4A940003}';
  IID_IVLCVideo: TGUID = '{0AAEDF0B-D333-4B27-A0C6-BBF31413A42E}';
  IID_IPictureDisp: TGUID = '{7BF80981-BF32-101A-8BBB-00AA00300CAB}';
  IID_IVLCControl2: TGUID = '{2D719729-5333-406C-BF12-8DE787FD65E3}';
  IID_IVLCMediaDescription: TGUID = '{796A2C2D-5B11-4FB5-9077-56D5E674972B}';
  DIID_DVLCEvents: TGUID = '{DF48072F-5EF8-434E-9B40-E2F3AE759B5F}';
  CLASS_VLCPlugin2: TGUID = '{9BE31822-FDAD-461B-AD51-BE1D1C159921}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum VLCPlaylistMode
type
  VLCPlaylistMode = TOleEnum;
const
  VLCPlayListInsert = $00000001;
  VLCPlayListInsertAndGo = $00000009;
  VLCPlayListReplace = $00000002;
  VLCPlayListReplaceAndGo = $0000000A;
  VLCPlayListAppend = $00000004;
  VLCPlayListAppendAndGo = $0000000C;
  VLCPlayListCheckInsert = $00000010;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IVLCAudio = interface;
  IVLCAudioDisp = dispinterface;
  IVLCChapter = interface;
  IVLCChapterDisp = dispinterface;
  IVLCInput = interface;
  IVLCInputDisp = dispinterface;
  IVLCTitle = interface;
  IVLCTitleDisp = dispinterface;
  IVLCLogo = interface;
  IVLCLogoDisp = dispinterface;
  IVLCDeinterlace = interface;
  IVLCDeinterlaceDisp = dispinterface;
  IVLCMarquee = interface;
  IVLCMarqueeDisp = dispinterface;
  IVLCPlaylist = interface;
  IVLCPlaylistDisp = dispinterface;
  IVLCPlaylistItems = interface;
  IVLCPlaylistItemsDisp = dispinterface;
  IVLCSubtitle = interface;
  IVLCSubtitleDisp = dispinterface;
  IVLCVideo = interface;
  IVLCVideoDisp = dispinterface;
  IPictureDisp = interface;
  IVLCControl2 = interface;
  IVLCControl2Disp = dispinterface;
  IVLCMediaDescription = interface;
  IVLCMediaDescriptionDisp = dispinterface;
  DVLCEvents = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  VLCPlugin2 = IVLCControl2;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//

  eVLCPlaylistMode = VLCPlaylistMode; 

// *********************************************************************//
// Interface: IVLCAudio
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9E0BD17B-2D3C-4656-B94D-03084F3FD9D4}
// *********************************************************************//
  IVLCAudio = interface(IDispatch)
    ['{9E0BD17B-2D3C-4656-B94D-03084F3FD9D4}']
    function Get_mute: WordBool; safecall;
    procedure Set_mute(muted: WordBool); safecall;
    function Get_volume: Integer; safecall;
    procedure Set_volume(volume: Integer); safecall;
    procedure toggleMute; safecall;
    function Get_track: Integer; safecall;
    procedure Set_track(track: Integer); safecall;
    function Get_count: Integer; safecall;
    function description(trackID: Integer): WideString; safecall;
    function Get_channel: Integer; safecall;
    procedure Set_channel(channel: Integer); safecall;
    property mute: WordBool read Get_mute write Set_mute;
    property volume: Integer read Get_volume write Set_volume;
    property track: Integer read Get_track write Set_track;
    property count: Integer read Get_count;
    property channel: Integer read Get_channel write Set_channel;
  end;

// *********************************************************************//
// DispIntf:  IVLCAudioDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9E0BD17B-2D3C-4656-B94D-03084F3FD9D4}
// *********************************************************************//
  IVLCAudioDisp = dispinterface
    ['{9E0BD17B-2D3C-4656-B94D-03084F3FD9D4}']
    property mute: WordBool dispid 1610743808;
    property volume: Integer dispid 1610743810;
    procedure toggleMute; dispid 1610743812;
    property track: Integer dispid 1610743813;
    property count: Integer readonly dispid 1610743815;
    function description(trackID: Integer): WideString; dispid 1610743816;
    property channel: Integer dispid 1610743817;
  end;

// *********************************************************************//
// Interface: IVLCChapter
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5AF314CF-8849-4A79-A3FC-8DE6625D9E72}
// *********************************************************************//
  IVLCChapter = interface(IDispatch)
    ['{5AF314CF-8849-4A79-A3FC-8DE6625D9E72}']
    function Get_count: Integer; safecall;
    function countForTitle(title: Integer): Integer; safecall;
    function Get_track: Integer; safecall;
    procedure Set_track(track: Integer); safecall;
    function description(title: Integer; chapter: Integer): WideString; safecall;
    procedure next; safecall;
    procedure prev; safecall;
    property count: Integer read Get_count;
    property track: Integer read Get_track write Set_track;
  end;

// *********************************************************************//
// DispIntf:  IVLCChapterDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5AF314CF-8849-4A79-A3FC-8DE6625D9E72}
// *********************************************************************//
  IVLCChapterDisp = dispinterface
    ['{5AF314CF-8849-4A79-A3FC-8DE6625D9E72}']
    property count: Integer readonly dispid 1610743808;
    function countForTitle(title: Integer): Integer; dispid 1610743809;
    property track: Integer dispid 1610743810;
    function description(title: Integer; chapter: Integer): WideString; dispid 1610743812;
    procedure next; dispid 1610743813;
    procedure prev; dispid 1610743814;
  end;

// *********************************************************************//
// Interface: IVLCInput
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {49E0DBD1-9440-466C-9C97-95C67190C603}
// *********************************************************************//
  IVLCInput = interface(IDispatch)
    ['{49E0DBD1-9440-466C-9C97-95C67190C603}']
    function Get_length: Double; safecall;
    function Get_position: Double; safecall;
    procedure Set_position(position: Double); safecall;
    function Get_time: Double; safecall;
    procedure Set_time(time: Double); safecall;
    function Get_state: Integer; safecall;
    function Get_rate: Double; safecall;
    procedure Set_rate(rate: Double); safecall;
    function Get_fps: Double; safecall;
    function Get_hasVout: WordBool; safecall;
    function Get_title: IVLCTitle; safecall;
    function Get_chapter: IVLCChapter; safecall;
    property length: Double read Get_length;
    property position: Double read Get_position write Set_position;
    property time: Double read Get_time write Set_time;
    property state: Integer read Get_state;
    property rate: Double read Get_rate write Set_rate;
    property fps: Double read Get_fps;
    property hasVout: WordBool read Get_hasVout;
    property title: IVLCTitle read Get_title;
    property chapter: IVLCChapter read Get_chapter;
  end;

// *********************************************************************//
// DispIntf:  IVLCInputDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {49E0DBD1-9440-466C-9C97-95C67190C603}
// *********************************************************************//
  IVLCInputDisp = dispinterface
    ['{49E0DBD1-9440-466C-9C97-95C67190C603}']
    property length: Double readonly dispid 1610743808;
    property position: Double dispid 1610743809;
    property time: Double dispid 1610743811;
    property state: Integer readonly dispid 1610743813;
    property rate: Double dispid 1610743814;
    property fps: Double readonly dispid 1610743816;
    property hasVout: WordBool readonly dispid 1610743817;
    property title: IVLCTitle readonly dispid 1610743818;
    property chapter: IVLCChapter readonly dispid 1610743819;
  end;

// *********************************************************************//
// Interface: IVLCTitle
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B5DEF5A1-FFB6-4E68-B3D8-A12AC60FDA54}
// *********************************************************************//
  IVLCTitle = interface(IDispatch)
    ['{B5DEF5A1-FFB6-4E68-B3D8-A12AC60FDA54}']
    function Get_count: Integer; safecall;
    function Get_track: Integer; safecall;
    procedure Set_track(track: Integer); safecall;
    function description(track: Integer): WideString; safecall;
    property count: Integer read Get_count;
    property track: Integer read Get_track write Set_track;
  end;

// *********************************************************************//
// DispIntf:  IVLCTitleDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B5DEF5A1-FFB6-4E68-B3D8-A12AC60FDA54}
// *********************************************************************//
  IVLCTitleDisp = dispinterface
    ['{B5DEF5A1-FFB6-4E68-B3D8-A12AC60FDA54}']
    property count: Integer readonly dispid 1610743808;
    property track: Integer dispid 1610743809;
    function description(track: Integer): WideString; dispid 1610743811;
  end;

// *********************************************************************//
// Interface: IVLCLogo
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {8A4A20C2-93F3-44E8-8644-BEB2E3487E84}
// *********************************************************************//
  IVLCLogo = interface(IDispatch)
    ['{8A4A20C2-93F3-44E8-8644-BEB2E3487E84}']
    procedure enable; safecall;
    procedure disable; safecall;
    procedure file_(const fname: WideString); safecall;
    function Get_delay: Integer; safecall;
    procedure Set_delay(val: Integer); safecall;
    function Get_repeat_: Integer; safecall;
    procedure Set_repeat_(val: Integer); safecall;
    function Get_opacity: Integer; safecall;
    procedure Set_opacity(val: Integer); safecall;
    function Get_position: WideString; safecall;
    procedure Set_position(const val: WideString); safecall;
    function Get_x: Integer; safecall;
    procedure Set_x(val: Integer); safecall;
    function Get_y: Integer; safecall;
    procedure Set_y(val: Integer); safecall;
    property delay: Integer read Get_delay write Set_delay;
    property repeat_: Integer read Get_repeat_ write Set_repeat_;
    property opacity: Integer read Get_opacity write Set_opacity;
    property position: WideString read Get_position write Set_position;
    property x: Integer read Get_x write Set_x;
    property y: Integer read Get_y write Set_y;
  end;

// *********************************************************************//
// DispIntf:  IVLCLogoDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {8A4A20C2-93F3-44E8-8644-BEB2E3487E84}
// *********************************************************************//
  IVLCLogoDisp = dispinterface
    ['{8A4A20C2-93F3-44E8-8644-BEB2E3487E84}']
    procedure enable; dispid 1610743808;
    procedure disable; dispid 1610743809;
    procedure file_(const fname: WideString); dispid 1610743810;
    property delay: Integer dispid 1610743811;
    property repeat_: Integer dispid 1610743813;
    property opacity: Integer dispid 1610743815;
    property position: WideString dispid 1610743817;
    property x: Integer dispid 1610743819;
    property y: Integer dispid 1610743821;
  end;

// *********************************************************************//
// Interface: IVLCDeinterlace
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BC97469F-CB11-4037-8DCE-5FC9F5F85307}
// *********************************************************************//
  IVLCDeinterlace = interface(IDispatch)
    ['{BC97469F-CB11-4037-8DCE-5FC9F5F85307}']
    procedure enable(const mode: WideString); safecall;
    procedure disable; safecall;
  end;

// *********************************************************************//
// DispIntf:  IVLCDeinterlaceDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {BC97469F-CB11-4037-8DCE-5FC9F5F85307}
// *********************************************************************//
  IVLCDeinterlaceDisp = dispinterface
    ['{BC97469F-CB11-4037-8DCE-5FC9F5F85307}']
    procedure enable(const mode: WideString); dispid 1610743808;
    procedure disable; dispid 1610743809;
  end;

// *********************************************************************//
// Interface: IVLCMarquee
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {8D076AD6-9B6F-4150-A0FD-5D7E8C8CB02C}
// *********************************************************************//
  IVLCMarquee = interface(IDispatch)
    ['{8D076AD6-9B6F-4150-A0FD-5D7E8C8CB02C}']
    procedure enable; safecall;
    procedure disable; safecall;
    function Get_text: WideString; safecall;
    procedure Set_text(const val: WideString); safecall;
    function Get_color: Integer; safecall;
    procedure Set_color(val: Integer); safecall;
    function Get_opacity: Integer; safecall;
    procedure Set_opacity(val: Integer); safecall;
    function Get_position: WideString; safecall;
    procedure Set_position(const val: WideString); safecall;
    function Get_refresh: Integer; safecall;
    procedure Set_refresh(val: Integer); safecall;
    function Get_size: Integer; safecall;
    procedure Set_size(val: Integer); safecall;
    function Get_timeout: Integer; safecall;
    procedure Set_timeout(val: Integer); safecall;
    function Get_x: Integer; safecall;
    procedure Set_x(val: Integer); safecall;
    function Get_y: Integer; safecall;
    procedure Set_y(val: Integer); safecall;
    property text: WideString read Get_text write Set_text;
    property color: Integer read Get_color write Set_color;
    property opacity: Integer read Get_opacity write Set_opacity;
    property position: WideString read Get_position write Set_position;
    property refresh: Integer read Get_refresh write Set_refresh;
    property size: Integer read Get_size write Set_size;
    property timeout: Integer read Get_timeout write Set_timeout;
    property x: Integer read Get_x write Set_x;
    property y: Integer read Get_y write Set_y;
  end;

// *********************************************************************//
// DispIntf:  IVLCMarqueeDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {8D076AD6-9B6F-4150-A0FD-5D7E8C8CB02C}
// *********************************************************************//
  IVLCMarqueeDisp = dispinterface
    ['{8D076AD6-9B6F-4150-A0FD-5D7E8C8CB02C}']
    procedure enable; dispid 1610743808;
    procedure disable; dispid 1610743809;
    property text: WideString dispid 1610743810;
    property color: Integer dispid 1610743812;
    property opacity: Integer dispid 1610743814;
    property position: WideString dispid 1610743816;
    property refresh: Integer dispid 1610743818;
    property size: Integer dispid 1610743820;
    property timeout: Integer dispid 1610743822;
    property x: Integer dispid 1610743824;
    property y: Integer dispid 1610743826;
  end;

// *********************************************************************//
// Interface: IVLCPlaylist
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {54613049-40BF-4035-9E70-0A9312C0188D}
// *********************************************************************//
  IVLCPlaylist = interface(IDispatch)
    ['{54613049-40BF-4035-9E70-0A9312C0188D}']
    function Get_itemCount: Integer; safecall;
    function Get_isPlaying: WordBool; safecall;
    function Get_currentItem: Integer; safecall;
    function add(const uri: WideString; name: OleVariant; options: OleVariant): Integer; safecall;
    procedure play; safecall;
    procedure playItem(itemId: Integer); safecall;
    procedure pause; safecall;
    procedure togglePause; safecall;
    procedure stop; safecall;
    procedure next; safecall;
    procedure prev; safecall;
    procedure clear; safecall;
    procedure removeItem(item: Integer); safecall;
    function Get_items: IVLCPlaylistItems; safecall;
    property itemCount: Integer read Get_itemCount;
    property isPlaying: WordBool read Get_isPlaying;
    property currentItem: Integer read Get_currentItem;
    property items: IVLCPlaylistItems read Get_items;
  end;

// *********************************************************************//
// DispIntf:  IVLCPlaylistDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {54613049-40BF-4035-9E70-0A9312C0188D}
// *********************************************************************//
  IVLCPlaylistDisp = dispinterface
    ['{54613049-40BF-4035-9E70-0A9312C0188D}']
    property itemCount: Integer readonly dispid 1610743808;
    property isPlaying: WordBool readonly dispid 1610743809;
    property currentItem: Integer readonly dispid 1610743810;
    function add(const uri: WideString; name: OleVariant; options: OleVariant): Integer; dispid 1610743811;
    procedure play; dispid 1610743812;
    procedure playItem(itemId: Integer); dispid 1610743813;
    procedure pause; dispid 1610743814;
    procedure togglePause; dispid 1610743815;
    procedure stop; dispid 1610743816;
    procedure next; dispid 1610743817;
    procedure prev; dispid 1610743818;
    procedure clear; dispid 1610743819;
    procedure removeItem(item: Integer); dispid 1610743820;
    property items: IVLCPlaylistItems readonly dispid 1610743821;
  end;

// *********************************************************************//
// Interface: IVLCPlaylistItems
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {FD37FE32-82BC-4A25-B056-315F4DBB194D}
// *********************************************************************//
  IVLCPlaylistItems = interface(IDispatch)
    ['{FD37FE32-82BC-4A25-B056-315F4DBB194D}']
    function Get_count: Integer; safecall;
    procedure clear; safecall;
    procedure remove(itemId: Integer); safecall;
    property count: Integer read Get_count;
  end;

// *********************************************************************//
// DispIntf:  IVLCPlaylistItemsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {FD37FE32-82BC-4A25-B056-315F4DBB194D}
// *********************************************************************//
  IVLCPlaylistItemsDisp = dispinterface
    ['{FD37FE32-82BC-4A25-B056-315F4DBB194D}']
    property count: Integer readonly dispid 1610743808;
    procedure clear; dispid 1610743809;
    procedure remove(itemId: Integer); dispid 1610743810;
  end;

// *********************************************************************//
// Interface: IVLCSubtitle
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {465E787A-0556-452F-9477-954E4A940003}
// *********************************************************************//
  IVLCSubtitle = interface(IDispatch)
    ['{465E787A-0556-452F-9477-954E4A940003}']
    function Get_track: Integer; safecall;
    procedure Set_track(spu: Integer); safecall;
    function Get_count: Integer; safecall;
    function description(nameID: Integer): WideString; safecall;
    property track: Integer read Get_track write Set_track;
    property count: Integer read Get_count;
  end;

// *********************************************************************//
// DispIntf:  IVLCSubtitleDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {465E787A-0556-452F-9477-954E4A940003}
// *********************************************************************//
  IVLCSubtitleDisp = dispinterface
    ['{465E787A-0556-452F-9477-954E4A940003}']
    property track: Integer dispid 1610743808;
    property count: Integer readonly dispid 1610743810;
    function description(nameID: Integer): WideString; dispid 1610743811;
  end;

// *********************************************************************//
// Interface: IVLCVideo
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {0AAEDF0B-D333-4B27-A0C6-BBF31413A42E}
// *********************************************************************//
  IVLCVideo = interface(IDispatch)
    ['{0AAEDF0B-D333-4B27-A0C6-BBF31413A42E}']
    function Get_fullscreen: WordBool; safecall;
    procedure Set_fullscreen(fullscreen: WordBool); safecall;
    function Get_width: Integer; safecall;
    function Get_height: Integer; safecall;
    function Get_aspectRatio: WideString; safecall;
    procedure Set_aspectRatio(const aspect: WideString); safecall;
    function Get_subtitle: Integer; safecall;
    procedure Set_subtitle(spu: Integer); safecall;
    function Get_crop: WideString; safecall;
    procedure Set_crop(const geometry: WideString); safecall;
    function Get_teletext: Integer; safecall;
    procedure Set_teletext(page: Integer); safecall;
    procedure toggleFullscreen; safecall;
    function takeSnapshot: IPictureDisp; safecall;
    procedure toggleTeletext; safecall;
    function Get_marquee: IVLCMarquee; safecall;
    function Get_logo: IVLCLogo; safecall;
    function Get_deinterlace: IVLCDeinterlace; safecall;
    property fullscreen: WordBool read Get_fullscreen write Set_fullscreen;
    property width: Integer read Get_width;
    property height: Integer read Get_height;
    property aspectRatio: WideString read Get_aspectRatio write Set_aspectRatio;
    property subtitle: Integer read Get_subtitle write Set_subtitle;
    property crop: WideString read Get_crop write Set_crop;
    property teletext: Integer read Get_teletext write Set_teletext;
    property marquee: IVLCMarquee read Get_marquee;
    property logo: IVLCLogo read Get_logo;
    property deinterlace: IVLCDeinterlace read Get_deinterlace;
  end;

// *********************************************************************//
// DispIntf:  IVLCVideoDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {0AAEDF0B-D333-4B27-A0C6-BBF31413A42E}
// *********************************************************************//
  IVLCVideoDisp = dispinterface
    ['{0AAEDF0B-D333-4B27-A0C6-BBF31413A42E}']
    property fullscreen: WordBool dispid 1610743808;
    property width: Integer readonly dispid 1610743810;
    property height: Integer readonly dispid 1610743811;
    property aspectRatio: WideString dispid 1610743812;
    property subtitle: Integer dispid 1610743814;
    property crop: WideString dispid 1610743816;
    property teletext: Integer dispid 1610743818;
    procedure toggleFullscreen; dispid 1610743820;
    function takeSnapshot: IPictureDisp; dispid 1610743821;
    procedure toggleTeletext; dispid 1610743822;
    property marquee: IVLCMarquee readonly dispid 1610743823;
    property logo: IVLCLogo readonly dispid 1610743824;
    property deinterlace: IVLCDeinterlace readonly dispid 1610743825;
  end;

// *********************************************************************//
// Interface: IPictureDisp
// Flags:     (4096) Dispatchable
// GUID:      {7BF80981-BF32-101A-8BBB-00AA00300CAB}
// *********************************************************************//
  IPictureDisp = interface(IDispatch)
    ['{7BF80981-BF32-101A-8BBB-00AA00300CAB}']
  end;

// *********************************************************************//
// Interface: IVLCControl2
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2D719729-5333-406C-BF12-8DE787FD65E3}
// *********************************************************************//
  IVLCControl2 = interface(IDispatch)
    ['{2D719729-5333-406C-BF12-8DE787FD65E3}']
    function Get_AutoLoop: WordBool; safecall;
    procedure Set_AutoLoop(AutoLoop: WordBool); safecall;
    function Get_AutoPlay: WordBool; safecall;
    procedure Set_AutoPlay(AutoPlay: WordBool); safecall;
    function Get_BaseURL: WideString; safecall;
    procedure Set_BaseURL(const url: WideString); safecall;
    function Get_StartTime: Integer; safecall;
    procedure Set_StartTime(seconds: Integer); safecall;
    function Get_MRL: WideString; safecall;
    procedure Set_MRL(const MRL: WideString); safecall;
    function Get_VersionInfo: WideString; safecall;
    function getVersionInfo: WideString; safecall;
    function Get_Visible: WordBool; safecall;
    procedure Set_Visible(Visible: WordBool); safecall;
    function Get_volume: Integer; safecall;
    procedure Set_volume(volume: Integer); safecall;
    function Get_BackColor: LongWord; safecall;
    procedure Set_BackColor(BackColor: LongWord); safecall;
    function Get_Toolbar: WordBool; safecall;
    procedure Set_Toolbar(Visible: WordBool); safecall;
    function Get_audio: IVLCAudio; safecall;
    function Get_input: IVLCInput; safecall;
    function Get_playlist: IVLCPlaylist; safecall;
    function Get_subtitle: IVLCSubtitle; safecall;
    function Get_video: IVLCVideo; safecall;
    function Get_FullscreenEnabled: WordBool; safecall;
    procedure Set_FullscreenEnabled(enabled: WordBool); safecall;
    function Get_mediaDescription: IVLCMediaDescription; safecall;
    function Get_Branding: WordBool; safecall;
    procedure Set_Branding(Visible: WordBool); safecall;
    property AutoLoop: WordBool read Get_AutoLoop write Set_AutoLoop;
    property AutoPlay: WordBool read Get_AutoPlay write Set_AutoPlay;
    property BaseURL: WideString read Get_BaseURL write Set_BaseURL;
    property StartTime: Integer read Get_StartTime write Set_StartTime;
    property MRL: WideString read Get_MRL write Set_MRL;
    property VersionInfo: WideString read Get_VersionInfo;
    property Visible: WordBool read Get_Visible write Set_Visible;
    property volume: Integer read Get_volume write Set_volume;
    property BackColor: LongWord read Get_BackColor write Set_BackColor;
    property Toolbar: WordBool read Get_Toolbar write Set_Toolbar;
    property audio: IVLCAudio read Get_audio;
    property input: IVLCInput read Get_input;
    property playlist: IVLCPlaylist read Get_playlist;
    property subtitle: IVLCSubtitle read Get_subtitle;
    property video: IVLCVideo read Get_video;
    property FullscreenEnabled: WordBool read Get_FullscreenEnabled write Set_FullscreenEnabled;
    property mediaDescription: IVLCMediaDescription read Get_mediaDescription;
    property Branding: WordBool read Get_Branding write Set_Branding;
  end;

// *********************************************************************//
// DispIntf:  IVLCControl2Disp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {2D719729-5333-406C-BF12-8DE787FD65E3}
// *********************************************************************//
  IVLCControl2Disp = dispinterface
    ['{2D719729-5333-406C-BF12-8DE787FD65E3}']
    property AutoLoop: WordBool dispid 1610743808;
    property AutoPlay: WordBool dispid 1610743810;
    property BaseURL: WideString dispid 1610743812;
    property StartTime: Integer dispid 1610743814;
    property MRL: WideString dispid 1610743816;
    property VersionInfo: WideString readonly dispid 1610743818;
    function getVersionInfo: WideString; dispid 1610743819;
    property Visible: WordBool dispid 1610743820;
    property volume: Integer dispid 1610743822;
    property BackColor: LongWord dispid 1610743824;
    property Toolbar: WordBool dispid 1610743826;
    property audio: IVLCAudio readonly dispid 1610743828;
    property input: IVLCInput readonly dispid 1610743829;
    property playlist: IVLCPlaylist readonly dispid 1610743830;
    property subtitle: IVLCSubtitle readonly dispid 1610743831;
    property video: IVLCVideo readonly dispid 1610743832;
    property FullscreenEnabled: WordBool dispid 1610743833;
    property mediaDescription: IVLCMediaDescription readonly dispid 1610743835;
    property Branding: WordBool dispid 1610743836;
  end;

// *********************************************************************//
// Interface: IVLCMediaDescription
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {796A2C2D-5B11-4FB5-9077-56D5E674972B}
// *********************************************************************//
  IVLCMediaDescription = interface(IDispatch)
    ['{796A2C2D-5B11-4FB5-9077-56D5E674972B}']
    function Get_title: WideString; safecall;
    function Get_artist: WideString; safecall;
    function Get_genre: WideString; safecall;
    function Get_copyright: WideString; safecall;
    function Get_album: WideString; safecall;
    function Get_trackNumber: WideString; safecall;
    function Get_description: WideString; safecall;
    function Get_rating: WideString; safecall;
    function Get_date: WideString; safecall;
    function Get_setting: WideString; safecall;
    function Get_url: WideString; safecall;
    function Get_language: WideString; safecall;
    function Get_nowPlaying: WideString; safecall;
    function Get_publisher: WideString; safecall;
    function Get_encodedBy: WideString; safecall;
    function Get_artworkURL: WideString; safecall;
    function Get_trackID: WideString; safecall;
    property title: WideString read Get_title;
    property artist: WideString read Get_artist;
    property genre: WideString read Get_genre;
    property copyright: WideString read Get_copyright;
    property album: WideString read Get_album;
    property trackNumber: WideString read Get_trackNumber;
    property description: WideString read Get_description;
    property rating: WideString read Get_rating;
    property date: WideString read Get_date;
    property setting: WideString read Get_setting;
    property url: WideString read Get_url;
    property language: WideString read Get_language;
    property nowPlaying: WideString read Get_nowPlaying;
    property publisher: WideString read Get_publisher;
    property encodedBy: WideString read Get_encodedBy;
    property artworkURL: WideString read Get_artworkURL;
    property trackID: WideString read Get_trackID;
  end;

// *********************************************************************//
// DispIntf:  IVLCMediaDescriptionDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {796A2C2D-5B11-4FB5-9077-56D5E674972B}
// *********************************************************************//
  IVLCMediaDescriptionDisp = dispinterface
    ['{796A2C2D-5B11-4FB5-9077-56D5E674972B}']
    property title: WideString readonly dispid 1610743808;
    property artist: WideString readonly dispid 1610743809;
    property genre: WideString readonly dispid 1610743810;
    property copyright: WideString readonly dispid 1610743811;
    property album: WideString readonly dispid 1610743812;
    property trackNumber: WideString readonly dispid 1610743813;
    property description: WideString readonly dispid 1610743814;
    property rating: WideString readonly dispid 1610743815;
    property date: WideString readonly dispid 1610743816;
    property setting: WideString readonly dispid 1610743817;
    property url: WideString readonly dispid 1610743818;
    property language: WideString readonly dispid 1610743819;
    property nowPlaying: WideString readonly dispid 1610743820;
    property publisher: WideString readonly dispid 1610743821;
    property encodedBy: WideString readonly dispid 1610743822;
    property artworkURL: WideString readonly dispid 1610743823;
    property trackID: WideString readonly dispid 1610743824;
  end;

// *********************************************************************//
// DispIntf:  DVLCEvents
// Flags:     (4096) Dispatchable
// GUID:      {DF48072F-5EF8-434E-9B40-E2F3AE759B5F}
// *********************************************************************//
  DVLCEvents = dispinterface
    ['{DF48072F-5EF8-434E-9B40-E2F3AE759B5F}']
    procedure MediaPlayerNothingSpecial; dispid 200;
    procedure MediaPlayerOpening; dispid 201;
    procedure MediaPlayerBuffering(cache: Integer); dispid 202;
    procedure MediaPlayerPlaying; dispid 203;
    procedure MediaPlayerPaused; dispid 204;
    procedure MediaPlayerForward; dispid 205;
    procedure MediaPlayerBackward; dispid 206;
    procedure MediaPlayerEncounteredError; dispid 207;
    procedure MediaPlayerEndReached; dispid 208;
    procedure MediaPlayerStopped; dispid 209;
    procedure MediaPlayerTimeChanged(time: Integer); dispid 210;
    procedure MediaPlayerPositionChanged(position: Single); dispid 211;
    procedure MediaPlayerSeekableChanged(seekable: WordBool); dispid 212;
    procedure MediaPlayerPausableChanged(pausable: WordBool); dispid 213;
    procedure MediaPlayerMediaChanged; dispid 214;
    procedure MediaPlayerTitleChanged(title: SYSINT); dispid 215;
    procedure MediaPlayerLengthChanged(length: Integer); dispid 216;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TVLCPlugin2
// Help String      : VLC control
// Default Interface: IVLCControl2
// Def. Intf. DISP? : No
// Event   Interface: DVLCEvents
// TypeFlags        : (34) CanCreate Control
// *********************************************************************//
  TVLCPlugin2MediaPlayerBuffering = procedure(ASender: TObject; cache: Integer) of object;
  TVLCPlugin2MediaPlayerTimeChanged = procedure(ASender: TObject; time: Integer) of object;
  TVLCPlugin2MediaPlayerPositionChanged = procedure(ASender: TObject; position: Single) of object;
  TVLCPlugin2MediaPlayerSeekableChanged = procedure(ASender: TObject; seekable: WordBool) of object;
  TVLCPlugin2MediaPlayerPausableChanged = procedure(ASender: TObject; pausable: WordBool) of object;
  TVLCPlugin2MediaPlayerTitleChanged = procedure(ASender: TObject; title: SYSINT) of object;
  TVLCPlugin2MediaPlayerLengthChanged = procedure(ASender: TObject; length: Integer) of object;

  TVLCPlugin2 = class(TOleControl)
  private
    FOnMediaPlayerNothingSpecial: TNotifyEvent;
    FOnMediaPlayerOpening: TNotifyEvent;
    FOnMediaPlayerBuffering: TVLCPlugin2MediaPlayerBuffering;
    FOnMediaPlayerPlaying: TNotifyEvent;
    FOnMediaPlayerPaused: TNotifyEvent;
    FOnMediaPlayerForward: TNotifyEvent;
    FOnMediaPlayerBackward: TNotifyEvent;
    FOnMediaPlayerEncounteredError: TNotifyEvent;
    FOnMediaPlayerEndReached: TNotifyEvent;
    FOnMediaPlayerStopped: TNotifyEvent;
    FOnMediaPlayerTimeChanged: TVLCPlugin2MediaPlayerTimeChanged;
    FOnMediaPlayerPositionChanged: TVLCPlugin2MediaPlayerPositionChanged;
    FOnMediaPlayerSeekableChanged: TVLCPlugin2MediaPlayerSeekableChanged;
    FOnMediaPlayerPausableChanged: TVLCPlugin2MediaPlayerPausableChanged;
    FOnMediaPlayerMediaChanged: TNotifyEvent;
    FOnMediaPlayerTitleChanged: TVLCPlugin2MediaPlayerTitleChanged;
    FOnMediaPlayerLengthChanged: TVLCPlugin2MediaPlayerLengthChanged;
    FIntf: IVLCControl2;
    function  GetControlInterface: IVLCControl2;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
    function Get_audio: IVLCAudio;
    function Get_input: IVLCInput;
    function Get_playlist: IVLCPlaylist;
    function Get_subtitle: IVLCSubtitle;
    function Get_video: IVLCVideo;
    function Get_mediaDescription: IVLCMediaDescription;
  public
    function getVersionInfo: WideString;
    property  ControlInterface: IVLCControl2 read GetControlInterface;
    property  DefaultInterface: IVLCControl2 read GetControlInterface;
    property VersionInfo: WideString index -1 read GetWideStringProp;
    property audio: IVLCAudio read Get_audio;
    property input: IVLCInput read Get_input;
    property playlist: IVLCPlaylist read Get_playlist;
    property subtitle: IVLCSubtitle read Get_subtitle;
    property video: IVLCVideo read Get_video;
    property mediaDescription: IVLCMediaDescription read Get_mediaDescription;
  published
    property Anchors;
    property  TabStop;
    property  Align;
    property  DragCursor;
    property  DragMode;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property AutoLoop: WordBool index -1 read GetWordBoolProp write SetWordBoolProp stored False;
    property AutoPlay: WordBool index -1 read GetWordBoolProp write SetWordBoolProp stored False;
    property BaseURL: WideString index -1 read GetWideStringProp write SetWideStringProp stored False;
    property StartTime: Integer index -1 read GetIntegerProp write SetIntegerProp stored False;
    property MRL: WideString index -1 read GetWideStringProp write SetWideStringProp stored False;
    property Visible: WordBool index -1 read GetWordBoolProp write SetWordBoolProp stored False;
    property volume: Integer index -1 read GetIntegerProp write SetIntegerProp stored False;
    property BackColor: Integer index -1 read GetIntegerProp write SetIntegerProp stored False;
    property Toolbar: WordBool index -1 read GetWordBoolProp write SetWordBoolProp stored False;
    property FullscreenEnabled: WordBool index -1 read GetWordBoolProp write SetWordBoolProp stored False;
    property Branding: WordBool index -1 read GetWordBoolProp write SetWordBoolProp stored False;
    property OnMediaPlayerNothingSpecial: TNotifyEvent read FOnMediaPlayerNothingSpecial write FOnMediaPlayerNothingSpecial;
    property OnMediaPlayerOpening: TNotifyEvent read FOnMediaPlayerOpening write FOnMediaPlayerOpening;
    property OnMediaPlayerBuffering: TVLCPlugin2MediaPlayerBuffering read FOnMediaPlayerBuffering write FOnMediaPlayerBuffering;
    property OnMediaPlayerPlaying: TNotifyEvent read FOnMediaPlayerPlaying write FOnMediaPlayerPlaying;
    property OnMediaPlayerPaused: TNotifyEvent read FOnMediaPlayerPaused write FOnMediaPlayerPaused;
    property OnMediaPlayerForward: TNotifyEvent read FOnMediaPlayerForward write FOnMediaPlayerForward;
    property OnMediaPlayerBackward: TNotifyEvent read FOnMediaPlayerBackward write FOnMediaPlayerBackward;
    property OnMediaPlayerEncounteredError: TNotifyEvent read FOnMediaPlayerEncounteredError write FOnMediaPlayerEncounteredError;
    property OnMediaPlayerEndReached: TNotifyEvent read FOnMediaPlayerEndReached write FOnMediaPlayerEndReached;
    property OnMediaPlayerStopped: TNotifyEvent read FOnMediaPlayerStopped write FOnMediaPlayerStopped;
    property OnMediaPlayerTimeChanged: TVLCPlugin2MediaPlayerTimeChanged read FOnMediaPlayerTimeChanged write FOnMediaPlayerTimeChanged;
    property OnMediaPlayerPositionChanged: TVLCPlugin2MediaPlayerPositionChanged read FOnMediaPlayerPositionChanged write FOnMediaPlayerPositionChanged;
    property OnMediaPlayerSeekableChanged: TVLCPlugin2MediaPlayerSeekableChanged read FOnMediaPlayerSeekableChanged write FOnMediaPlayerSeekableChanged;
    property OnMediaPlayerPausableChanged: TVLCPlugin2MediaPlayerPausableChanged read FOnMediaPlayerPausableChanged write FOnMediaPlayerPausableChanged;
    property OnMediaPlayerMediaChanged: TNotifyEvent read FOnMediaPlayerMediaChanged write FOnMediaPlayerMediaChanged;
    property OnMediaPlayerTitleChanged: TVLCPlugin2MediaPlayerTitleChanged read FOnMediaPlayerTitleChanged write FOnMediaPlayerTitleChanged;
    property OnMediaPlayerLengthChanged: TVLCPlugin2MediaPlayerLengthChanged read FOnMediaPlayerLengthChanged write FOnMediaPlayerLengthChanged;
  end;

procedure Register;

resourcestring
  dtlServerPage = 'Samples';

  dtlOcxPage = 'Samples';

implementation

uses System.Win.ComObj;

procedure TVLCPlugin2.InitControlData;
const
  CEventDispIDs: array [0..16] of DWORD = (
    $000000C8, $000000C9, $000000CA, $000000CB, $000000CC, $000000CD,
    $000000CE, $000000CF, $000000D0, $000000D1, $000000D2, $000000D3,
    $000000D4, $000000D5, $000000D6, $000000D7, $000000D8);
  CControlData: TControlData2 = (
    ClassID:      '{9BE31822-FDAD-461B-AD51-BE1D1C159921}';
    EventIID:     '{DF48072F-5EF8-434E-9B40-E2F3AE759B5F}';
    EventCount:   17;
    EventDispIDs: @CEventDispIDs;
    LicenseKey:   nil (*HR:$80004002*);
    Flags:        $00000000;
    Version:      500);
begin
  ControlData := @CControlData;
  TControlData2(CControlData).FirstEventOfs := UIntPtr(@@FOnMediaPlayerNothingSpecial) - UIntPtr(Self);
end;

procedure TVLCPlugin2.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as IVLCControl2;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TVLCPlugin2.GetControlInterface: IVLCControl2;
begin
  CreateControl;
  Result := FIntf;
end;

function TVLCPlugin2.Get_audio: IVLCAudio;
begin
  Result := DefaultInterface.audio;
end;

function TVLCPlugin2.Get_input: IVLCInput;
begin
  Result := DefaultInterface.input;
end;

function TVLCPlugin2.Get_playlist: IVLCPlaylist;
begin
  Result := DefaultInterface.playlist;
end;

function TVLCPlugin2.Get_subtitle: IVLCSubtitle;
begin
  Result := DefaultInterface.subtitle;
end;

function TVLCPlugin2.Get_video: IVLCVideo;
begin
  Result := DefaultInterface.video;
end;

function TVLCPlugin2.Get_mediaDescription: IVLCMediaDescription;
begin
  Result := DefaultInterface.mediaDescription;
end;

function TVLCPlugin2.getVersionInfo: WideString;
begin
  Result := DefaultInterface.getVersionInfo;
end;

procedure Register;
begin
  RegisterComponents(dtlOcxPage, [TVLCPlugin2]);
end;

end.
