(* ******************************************************************************
  TDxHtmlParser  HTML解析单元库   通过网上“不得闲”源代码进行改进而得
  ================================================================================
  Copyright(c) 2008-2010、
  **************************************************** *)
unit DxHtmlParser;

interface

uses Windows, MSHTML, ActiveX, DxHtmlElement, Forms, SysUtils, Classes,
  IdHTTP, IdAntiFreeze, IdGlobal, IdGlobalProtocols;

type
  TDxHtmlParser = class
  private
    FHtmlDoc: IHTMLDocument2;
    FHTML: string;
    FWebTables: TDxTableCollection;
    FWebElements: TDxWebElementCollection;
    FWebComb: TDxWebCombobox;
    IdHTTP1: TIdHTTP;
    IdAntiFreeze1: TIdAntiFreeze;
    FURL: WideString;
    FEncodingName: WideString;
    procedure SetHTML(const Value: string);
    function GetWebCombobox(AName: string): TDxWebCombobox;
    procedure SetURL(const Value: WideString);
  public
    constructor Create;
    destructor Destroy; override;
    property WebCombobox[Name: string]: TDxWebCombobox read GetWebCombobox;
  published
    property HTML: string read FHTML write SetHTML;
    property WebTables: TDxTableCollection read FWebTables;
    property WebElements: TDxWebElementCollection read FWebElements;
    property URL: WideString read FURL write SetURL;
    property EncodingName: WideString read FEncodingName;
  end;

implementation

{ TDxHtmlParser }

constructor TDxHtmlParser.Create;
begin
  CoInitialize(nil);
  // 创建IHTMLDocument2接口
  CoCreateInstance(CLASS_HTMLDocument, nil, CLSCTX_INPROC_SERVER,
    IID_IHTMLDocument2, FHtmlDoc);
  Assert(FHtmlDoc <> nil, '构建HTMLDocument接口失败');
  FHtmlDoc.Set_designMode('On'); // 设置为设计模式，不执行脚本
  while not(FHtmlDoc.readyState = 'complete') do
  begin
    sleep(1);
    Application.ProcessMessages;
  end;
  FWebTables := TDxTableCollection.Create(FHtmlDoc);
  FWebElements := TDxWebElementCollection.Create(nil);
  FWebComb := TDxWebCombobox.Create(nil);
end;

destructor TDxHtmlParser.Destroy;
begin
  FWebTables.Free;
  FWebElements.Free;
  FWebComb.Free;
  CoUninitialize;
  inherited;
end;

function TDxHtmlParser.GetWebCombobox(AName: string): TDxWebCombobox;
begin
  if FWebElements.Collection <> nil then
  begin
    FWebComb.CombInterface := FWebElements.ElementByName[AName]
      as IHTMLSelectElement;
    Result := FWebComb;
  end
  else
    Result := nil;
end;

procedure TDxHtmlParser.SetHTML(const Value: string);
begin
  if FHTML <> Value then
  begin
    FHTML := Value;
    FHtmlDoc.body.innerHTML := FHTML;
    FWebElements.Collection := FHtmlDoc.all;
  end;
end;

procedure TDxHtmlParser.SetURL(const Value: WideString);
var
  LResponse: TMemoryStream;
  szContent: TStringStream;
  LEncoding: TIdTextEncoding;
begin
  LResponse := TMemoryStream.Create;
  with TIdHTTP.Create(nil) do try
    HandleRedirects := True;
    Request.UserAgent := 'Mozilla/5.0';
    try
      FURL:= Value;
      Get(Value, LResponse);
    finally
      Disconnect;
    end;
    LResponse.Position := 0;
    if Response.CharSet <> '' then
    begin
      LEncoding := CharsetToEncoding(Response.CharSet);
      FEncodingName:= LEncoding.EncodingName;
      szContent:= TStringStream.Create('', LEncoding);
    end
    else
    begin
      szContent:= TStringStream.Create();
    end;
    szContent.CopyFrom(LResponse, LResponse.Size);
    Html:= szContent.DataString;
    szContent.Free;
  finally
    Free;
    FreeAndNil(LResponse);
  end;
end;

end.
