unit HHCTRLLib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 34747 $
// File generated on 2015/2/16 11:20:38 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Windows\System32\hhctrl.ocx (1)
// LIBID: {ADB880A2-D8FF-11CF-9377-00AA003B7A11}
// LCID: 0
// Helpfile: 
// HelpString: HHCtrl 4.0 Type Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}
interface

uses Windows, ActiveX, Classes, Graphics, OleCtrls, OleServer, StdVCL, Variants;
  


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  HHCTRLLibMajorVersion = 4;
  HHCTRLLibMinorVersion = 0;

  LIBID_HHCTRLLib: TGUID = '{ADB880A2-D8FF-11CF-9377-00AA003B7A11}';

  DIID__HHCtrlEvents: TGUID = '{ADB880A3-D8FF-11CF-9377-00AA003B7A11}';
  IID_IHHCtrl: TGUID = '{ADB880A1-D8FF-11CF-9377-00AA003B7A11}';
  CLASS_HHCtrl: TGUID = '{52A2AAAE-085D-4187-97EA-8C30DB990436}';
  CLASS_OldHHCtrl2: TGUID = '{41B23C28-488E-4E5C-ACE2-BB0BBABE99E8}';
  CLASS_OldHHCtrl1: TGUID = '{ADB880A6-D8FF-11CF-9377-00AA003B7A11}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  _HHCtrlEvents = dispinterface;
  IHHCtrl = interface;
  IHHCtrlDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  HHCtrl = IHHCtrl;
  OldHHCtrl2 = IHHCtrl;
  OldHHCtrl1 = IHHCtrl;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//

  UINT_PTR = LongWord; 
  LONG_PTR = Integer; 

// *********************************************************************//
// DispIntf:  _HHCtrlEvents
// Flags:     (4096) Dispatchable
// GUID:      {ADB880A3-D8FF-11CF-9377-00AA003B7A11}
// *********************************************************************//
  _HHCtrlEvents = dispinterface
    ['{ADB880A3-D8FF-11CF-9377-00AA003B7A11}']
    procedure Click(const ParamString: WideString); dispid 0;
  end;

// *********************************************************************//
// Interface: IHHCtrl
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {ADB880A1-D8FF-11CF-9377-00AA003B7A11}
// *********************************************************************//
  IHHCtrl = interface(IDispatch)
    ['{ADB880A1-D8FF-11CF-9377-00AA003B7A11}']
    procedure Set_Image(const path: WideString); safecall;
    function Get_Image: WideString; safecall;
    procedure Click; safecall;
    procedure HHClick; safecall;
    procedure Print; safecall;
    procedure syncURL(const pszUrl: WideString); safecall;
    procedure TCard(wParam: UINT_PTR; lParam: LONG_PTR); safecall;
    procedure TextPopup(const pszText: WideString; const pszFont: WideString; horzMargins: SYSINT; 
                        vertMargins: SYSINT; clrForeground: LongWord; clrBackground: LongWord); safecall;
    property Image: WideString read Get_Image write Set_Image;
  end;

// *********************************************************************//
// DispIntf:  IHHCtrlDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {ADB880A1-D8FF-11CF-9377-00AA003B7A11}
// *********************************************************************//
  IHHCtrlDisp = dispinterface
    ['{ADB880A1-D8FF-11CF-9377-00AA003B7A11}']
    property Image: WideString dispid 1;
    procedure Click; dispid 5;
    procedure HHClick; dispid 9;
    procedure Print; dispid 6;
    procedure syncURL(const pszUrl: WideString); dispid 4;
    procedure TCard(wParam: UINT_PTR; lParam: LONG_PTR); dispid 7;
    procedure TextPopup(const pszText: WideString; const pszFont: WideString; horzMargins: SYSINT; 
                        vertMargins: SYSINT; clrForeground: LongWord; clrBackground: LongWord); dispid 8;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : THHCtrl
// Help String      : HHCtrl Class
// Default Interface: IHHCtrl
// Def. Intf. DISP? : No
// Event   Interface: _HHCtrlEvents
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  THHCtrlClick = procedure(ASender: TObject; const ParamString: WideString) of object;

  THHCtrl = class(TOleControl)
  private
    FOnClick: THHCtrlClick;
    FIntf: IHHCtrl;
    function  GetControlInterface: IHHCtrl;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
  public
    procedure Click;
    procedure HHClick;
    procedure Print;
    procedure syncURL(const pszUrl: WideString);
    procedure TCard(wParam: UINT_PTR; lParam: LONG_PTR);
    procedure TextPopup(const pszText: WideString; const pszFont: WideString; horzMargins: SYSINT; 
                        vertMargins: SYSINT; clrForeground: LongWord; clrBackground: LongWord);
    property  ControlInterface: IHHCtrl read GetControlInterface;
    property  DefaultInterface: IHHCtrl read GetControlInterface;
  published
    property Anchors;
    property  TabStop;
    property  Align;
    property  DragCursor;
    property  DragMode;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  Visible;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property Image: WideString index 1 read GetWideStringProp write SetWideStringProp stored False;
    property OnClick: THHCtrlClick read FOnClick write FOnClick;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TOldHHCtrl2
// Help String      : Deprecated HHCtrl Class 2
// Default Interface: IHHCtrl
// Def. Intf. DISP? : No
// Event   Interface: _HHCtrlEvents
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TOldHHCtrl2Click = procedure(ASender: TObject; const ParamString: WideString) of object;

  TOldHHCtrl2 = class(TOleControl)
  private
    FOnClick: TOldHHCtrl2Click;
    FIntf: IHHCtrl;
    function  GetControlInterface: IHHCtrl;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
  public
    procedure Click;
    procedure HHClick;
    procedure Print;
    procedure syncURL(const pszUrl: WideString);
    procedure TCard(wParam: UINT_PTR; lParam: LONG_PTR);
    procedure TextPopup(const pszText: WideString; const pszFont: WideString; horzMargins: SYSINT; 
                        vertMargins: SYSINT; clrForeground: LongWord; clrBackground: LongWord);
    property  ControlInterface: IHHCtrl read GetControlInterface;
    property  DefaultInterface: IHHCtrl read GetControlInterface;
  published
    property Anchors;
    property  TabStop;
    property  Align;
    property  DragCursor;
    property  DragMode;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  Visible;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property Image: WideString index 1 read GetWideStringProp write SetWideStringProp stored False;
    property OnClick: TOldHHCtrl2Click read FOnClick write FOnClick;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TOldHHCtrl1
// Help String      : Deprecated HHCtrl Class 1
// Default Interface: IHHCtrl
// Def. Intf. DISP? : No
// Event   Interface: _HHCtrlEvents
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TOldHHCtrl1Click = procedure(ASender: TObject; const ParamString: WideString) of object;

  TOldHHCtrl1 = class(TOleControl)
  private
    FOnClick: TOldHHCtrl1Click;
    FIntf: IHHCtrl;
    function  GetControlInterface: IHHCtrl;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
  public
    procedure Click;
    procedure HHClick;
    procedure Print;
    procedure syncURL(const pszUrl: WideString);
    procedure TCard(wParam: UINT_PTR; lParam: LONG_PTR);
    procedure TextPopup(const pszText: WideString; const pszFont: WideString; horzMargins: SYSINT; 
                        vertMargins: SYSINT; clrForeground: LongWord; clrBackground: LongWord);
    property  ControlInterface: IHHCtrl read GetControlInterface;
    property  DefaultInterface: IHHCtrl read GetControlInterface;
  published
    property Anchors;
    property  TabStop;
    property  Align;
    property  DragCursor;
    property  DragMode;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  Visible;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property Image: WideString index 1 read GetWideStringProp write SetWideStringProp stored False;
    property OnClick: TOldHHCtrl1Click read FOnClick write FOnClick;
  end;

procedure Register;

resourcestring
  dtlServerPage = 'Samples';

  dtlOcxPage = 'Samples';

implementation

uses ComObj;

procedure THHCtrl.InitControlData;
const
  CEventDispIDs: array [0..0] of DWORD = (
    $00000000);
  CControlData: TControlData2 = (
    ClassID: '{52A2AAAE-085D-4187-97EA-8C30DB990436}';
    EventIID: '{ADB880A3-D8FF-11CF-9377-00AA003B7A11}';
    EventCount: 1;
    EventDispIDs: @CEventDispIDs;
    LicenseKey: nil (*HR:$8007000E*);
    Flags: $00000000;
    Version: 401);
begin
  ControlData := @CControlData;
  TControlData2(CControlData).FirstEventOfs := Cardinal(@@FOnClick) - Cardinal(Self);
end;

procedure THHCtrl.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as IHHCtrl;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function THHCtrl.GetControlInterface: IHHCtrl;
begin
  CreateControl;
  Result := FIntf;
end;

procedure THHCtrl.Click;
begin
  DefaultInterface.Click;
end;

procedure THHCtrl.HHClick;
begin
  DefaultInterface.HHClick;
end;

procedure THHCtrl.Print;
begin
  DefaultInterface.Print;
end;

procedure THHCtrl.syncURL(const pszUrl: WideString);
begin
  DefaultInterface.syncURL(pszUrl);
end;

procedure THHCtrl.TCard(wParam: UINT_PTR; lParam: LONG_PTR);
begin
  DefaultInterface.TCard(wParam, lParam);
end;

procedure THHCtrl.TextPopup(const pszText: WideString; const pszFont: WideString; 
                            horzMargins: SYSINT; vertMargins: SYSINT; clrForeground: LongWord; 
                            clrBackground: LongWord);
begin
  DefaultInterface.TextPopup(pszText, pszFont, horzMargins, vertMargins, clrForeground, 
                             clrBackground);
end;

procedure TOldHHCtrl2.InitControlData;
const
  CEventDispIDs: array [0..0] of DWORD = (
    $00000000);
  CControlData: TControlData2 = (
    ClassID: '{41B23C28-488E-4E5C-ACE2-BB0BBABE99E8}';
    EventIID: '{ADB880A3-D8FF-11CF-9377-00AA003B7A11}';
    EventCount: 1;
    EventDispIDs: @CEventDispIDs;
    LicenseKey: nil (*HR:$8007000E*);
    Flags: $00000000;
    Version: 401);
begin
  ControlData := @CControlData;
  TControlData2(CControlData).FirstEventOfs := Cardinal(@@FOnClick) - Cardinal(Self);
end;

procedure TOldHHCtrl2.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as IHHCtrl;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TOldHHCtrl2.GetControlInterface: IHHCtrl;
begin
  CreateControl;
  Result := FIntf;
end;

procedure TOldHHCtrl2.Click;
begin
  DefaultInterface.Click;
end;

procedure TOldHHCtrl2.HHClick;
begin
  DefaultInterface.HHClick;
end;

procedure TOldHHCtrl2.Print;
begin
  DefaultInterface.Print;
end;

procedure TOldHHCtrl2.syncURL(const pszUrl: WideString);
begin
  DefaultInterface.syncURL(pszUrl);
end;

procedure TOldHHCtrl2.TCard(wParam: UINT_PTR; lParam: LONG_PTR);
begin
  DefaultInterface.TCard(wParam, lParam);
end;

procedure TOldHHCtrl2.TextPopup(const pszText: WideString; const pszFont: WideString; 
                                horzMargins: SYSINT; vertMargins: SYSINT; clrForeground: LongWord; 
                                clrBackground: LongWord);
begin
  DefaultInterface.TextPopup(pszText, pszFont, horzMargins, vertMargins, clrForeground, 
                             clrBackground);
end;

procedure TOldHHCtrl1.InitControlData;
const
  CEventDispIDs: array [0..0] of DWORD = (
    $00000000);
  CControlData: TControlData2 = (
    ClassID: '{ADB880A6-D8FF-11CF-9377-00AA003B7A11}';
    EventIID: '{ADB880A3-D8FF-11CF-9377-00AA003B7A11}';
    EventCount: 1;
    EventDispIDs: @CEventDispIDs;
    LicenseKey: nil (*HR:$8007000E*);
    Flags: $00000000;
    Version: 401);
begin
  ControlData := @CControlData;
  TControlData2(CControlData).FirstEventOfs := Cardinal(@@FOnClick) - Cardinal(Self);
end;

procedure TOldHHCtrl1.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as IHHCtrl;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TOldHHCtrl1.GetControlInterface: IHHCtrl;
begin
  CreateControl;
  Result := FIntf;
end;

procedure TOldHHCtrl1.Click;
begin
  DefaultInterface.Click;
end;

procedure TOldHHCtrl1.HHClick;
begin
  DefaultInterface.HHClick;
end;

procedure TOldHHCtrl1.Print;
begin
  DefaultInterface.Print;
end;

procedure TOldHHCtrl1.syncURL(const pszUrl: WideString);
begin
  DefaultInterface.syncURL(pszUrl);
end;

procedure TOldHHCtrl1.TCard(wParam: UINT_PTR; lParam: LONG_PTR);
begin
  DefaultInterface.TCard(wParam, lParam);
end;

procedure TOldHHCtrl1.TextPopup(const pszText: WideString; const pszFont: WideString; 
                                horzMargins: SYSINT; vertMargins: SYSINT; clrForeground: LongWord; 
                                clrBackground: LongWord);
begin
  DefaultInterface.TextPopup(pszText, pszFont, horzMargins, vertMargins, clrForeground, 
                             clrBackground);
end;

procedure Register;
begin
  RegisterComponents(dtlOcxPage, [THHCtrl, TOldHHCtrl2, TOldHHCtrl1]);
end;

end.
