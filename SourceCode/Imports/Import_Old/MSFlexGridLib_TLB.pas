unit MSFlexGridLib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 34747 $
// File generated on 2013/4/10 18:48:53 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\Program Files (x86)\WorldCard\Bin\MSFLXGRD.OCX (1)
// LIBID: {5E9E78A0-531B-11CF-91F6-C2863C385E30}
// LCID: 0
// Helpfile: D:\Program Files (x86)\WorldCard\Bin\MSHFlx98.chm 
// HelpString: Microsoft FlexGrid Control 6.0 (SP6)
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}
interface

uses Windows, ActiveX, Classes, Graphics, OleCtrls, OleServer, StdVCL, Variants;
  


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  MSFlexGridLibMajorVersion = 1;
  MSFlexGridLibMinorVersion = 0;

  LIBID_MSFlexGridLib: TGUID = '{5E9E78A0-531B-11CF-91F6-C2863C385E30}';

  IID_IVBDataObject: TGUID = '{2334D2B1-713E-11CF-8AE5-00AA00C00905}';
  CLASS_DataObject: TGUID = '{2334D2B2-713E-11CF-8AE5-00AA00C00905}';
  IID_IVBDataObjectFiles: TGUID = '{2334D2B3-713E-11CF-8AE5-00AA00C00905}';
  CLASS_DataObjectFiles: TGUID = '{2334D2B4-713E-11CF-8AE5-00AA00C00905}';
  IID_IRowCursor: TGUID = '{9F6AA700-D188-11CD-AD48-00AA003C9CB6}';
  IID_IMSFlexGrid: TGUID = '{5F4DF280-531B-11CF-91F6-C2863C385E30}';
  DIID_DMSFlexGridEvents: TGUID = '{609602E0-531B-11CF-91F6-C2863C385E30}';
  CLASS_MSFlexGrid: TGUID = '{6262D3A0-531B-11CF-91F6-C2863C385E30}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum OLEDragConstants
type
  OLEDragConstants = TOleEnum;
const
  flexOLEDragManual = $00000000;
  flexOLEDragAutomatic = $00000001;

// Constants for enum OLEDropConstants
type
  OLEDropConstants = TOleEnum;
const
  flexOLEDropNone = $00000000;
  flexOLEDropManual = $00000001;

// Constants for enum DragOverConstants
type
  DragOverConstants = TOleEnum;
const
  flexEnter = $00000000;
  flexLeave = $00000001;
  flexOver = $00000002;

// Constants for enum ClipBoardConstants
type
  ClipBoardConstants = TOleEnum;
const
  flexCFText = $00000001;
  flexCFBitmap = $00000002;
  flexCFMetafile = $00000003;
  flexCFDIB = $00000008;
  flexCFPalette = $00000009;
  flexCFEMetafile = $0000000E;
  flexCFFiles = $0000000F;
  flexCFRTF = $FFFFBF01;

// Constants for enum OLEDropEffectConstants
type
  OLEDropEffectConstants = TOleEnum;
const
  flexOLEDropEffectNone = $00000000;
  flexOLEDropEffectCopy = $00000001;
  flexOLEDropEffectMove = $00000002;
  flexOLEDropEffectScroll = $80000000;

// Constants for enum ErrorConstants
type
  ErrorConstants = TOleEnum;
const
  flexerrIllegaFunctionCall = $00000005;
  flexerrObjIllegalUse = $000001A9;
  flexerrClipInvalidFormat = $000001CD;
  flexerrDataObjectLocked = $000002A0;
  flexerrExpectedAnArgument = $000002A1;
  flexerrRecursiveOLEDrag = $000002A2;
  flexerrUserFormatNotBinArray = $000002A3;
  flexerrDataNotSetForFormat = $000002A4;
  flexerrUnknownError = $00000258;
  flexerrSubscript = $0000017D;
  flexerrBadValue = $0000017C;
  flexerrGetNotSupported = $0000018A;
  flexerrSetNotPermitted = $00000183;
  flexerrOutOfMemory = $00000007;
  flexerrVB30000 = $00007530;
  flexerrVB30001 = $00007531;
  flexerrVB30002 = $00007532;
  flexerrVB30004 = $00007534;
  flexerrVB30005 = $00007535;
  flexerrVB30006 = $00007536;
  flexerrVB30008 = $00007538;
  flexerrVB30009 = $00007539;
  flexerrVB30010 = $0000753A;
  flexerrVB30011 = $0000753B;
  flexerrVB30013 = $0000753D;
  flexerrVB30014 = $0000753E;
  flexerrVB30015 = $0000753F;
  flexerrVB30016 = $00007540;
  flexerrVB30017 = $00007541;

// Constants for enum AppearanceSettings
type
  AppearanceSettings = TOleEnum;
const
  flexFlat = $00000000;
  flex3D = $00000001;

// Constants for enum BorderStyleSettings
type
  BorderStyleSettings = TOleEnum;
const
  flexBorderNone = $00000000;
  flexBorderSingle = $00000001;

// Constants for enum FocusRectSettings
type
  FocusRectSettings = TOleEnum;
const
  flexFocusNone = $00000000;
  flexFocusLight = $00000001;
  flexFocusHeavy = $00000002;

// Constants for enum HighLightSettings
type
  HighLightSettings = TOleEnum;
const
  flexHighlightNever = $00000000;
  flexHighlightAlways = $00000001;
  flexHighlightWithFocus = $00000002;

// Constants for enum ScrollBarsSettings
type
  ScrollBarsSettings = TOleEnum;
const
  flexScrollBarNone = $00000000;
  flexScrollBarHorizontal = $00000001;
  flexScrollBarVertical = $00000002;
  flexScrollBarBoth = $00000003;

// Constants for enum TextStyleSettings
type
  TextStyleSettings = TOleEnum;
const
  flexTextFlat = $00000000;
  flexTextRaised = $00000001;
  flexTextInset = $00000002;
  flexTextRaisedLight = $00000003;
  flexTextInsetLight = $00000004;

// Constants for enum FillStyleSettings
type
  FillStyleSettings = TOleEnum;
const
  flexFillSingle = $00000000;
  flexFillRepeat = $00000001;

// Constants for enum GridLineSettings
type
  GridLineSettings = TOleEnum;
const
  flexGridNone = $00000000;
  flexGridFlat = $00000001;
  flexGridInset = $00000002;
  flexGridRaised = $00000003;

// Constants for enum SelectionModeSettings
type
  SelectionModeSettings = TOleEnum;
const
  flexSelectionFree = $00000000;
  flexSelectionByRow = $00000001;
  flexSelectionByColumn = $00000002;

// Constants for enum MergeCellsSettings
type
  MergeCellsSettings = TOleEnum;
const
  flexMergeNever = $00000000;
  flexMergeFree = $00000001;
  flexMergeRestrictRows = $00000002;
  flexMergeRestrictColumns = $00000003;
  flexMergeRestrictAll = $00000004;

// Constants for enum PictureTypeSettings
type
  PictureTypeSettings = TOleEnum;
const
  flexPictureColor = $00000000;
  flexPictureMonochrome = $00000001;

// Constants for enum AllowUserResizeSettings
type
  AllowUserResizeSettings = TOleEnum;
const
  flexResizeNone = $00000000;
  flexResizeColumns = $00000001;
  flexResizeRows = $00000002;
  flexResizeBoth = $00000003;

// Constants for enum MousePointerSettings
type
  MousePointerSettings = TOleEnum;
const
  flexDefault = $00000000;
  flexArrow = $00000001;
  flexCross = $00000002;
  flexIBeam = $00000003;
  flexIcon = $00000004;
  flexSize = $00000005;
  flexSizeNESW = $00000006;
  flexSizeNS = $00000007;
  flexSizeNWSE = $00000008;
  flexSizeEW = $00000009;
  flexUpArrow = $0000000A;
  flexHourglass = $0000000B;
  flexNoDrop = $0000000C;
  flexArrowHourGlass = $0000000D;
  flexArrowQuestion = $0000000E;
  flexSizeAll = $0000000F;
  flexCustom = $00000063;

// Constants for enum SortSettings
type
  SortSettings = TOleEnum;
const
  flexSortNone = $00000000;
  flexSortGenericAscending = $00000001;
  flexSortGenericDescending = $00000002;
  flexSortNumericAscending = $00000003;
  flexSortNumericDescending = $00000004;
  flexSortStringNoCaseAscending = $00000005;
  flexSortStringNoCaseDescending = $00000006;
  flexSortStringAscending = $00000007;
  flexSortStringDescending = $00000008;

// Constants for enum AlignmentSettings
type
  AlignmentSettings = TOleEnum;
const
  flexAlignLeftTop = $00000000;
  flexAlignLeftCenter = $00000001;
  flexAlignLeftBottom = $00000002;
  flexAlignCenterTop = $00000003;
  flexAlignCenterCenter = $00000004;
  flexAlignCenterBottom = $00000005;
  flexAlignRightTop = $00000006;
  flexAlignRightCenter = $00000007;
  flexAlignRightBottom = $00000008;
  flexAlignGeneral = $00000009;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IVBDataObject = interface;
  IVBDataObjectDisp = dispinterface;
  IVBDataObjectFiles = interface;
  IVBDataObjectFilesDisp = dispinterface;
  IRowCursor = interface;
  IRowCursorDisp = dispinterface;
  IMSFlexGrid = interface;
  IMSFlexGridDisp = dispinterface;
  DMSFlexGridEvents = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  DataObject = IVBDataObject;
  DataObjectFiles = IVBDataObjectFiles;
  MSFlexGrid = IMSFlexGrid;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  PSmallint1 = ^Smallint; {*}


// *********************************************************************//
// Interface: IVBDataObject
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {2334D2B1-713E-11CF-8AE5-00AA00C00905}
// *********************************************************************//
  IVBDataObject = interface(IDispatch)
    ['{2334D2B1-713E-11CF-8AE5-00AA00C00905}']
    procedure Clear; safecall;
    function GetData(sFormat: Smallint): OleVariant; safecall;
    function GetFormat(sFormat: Smallint): WordBool; safecall;
    procedure SetData(vValue: OleVariant; vFormat: OleVariant); safecall;
    function Get_Files: IVBDataObjectFiles; safecall;
    property Files: IVBDataObjectFiles read Get_Files;
  end;

// *********************************************************************//
// DispIntf:  IVBDataObjectDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {2334D2B1-713E-11CF-8AE5-00AA00C00905}
// *********************************************************************//
  IVBDataObjectDisp = dispinterface
    ['{2334D2B1-713E-11CF-8AE5-00AA00C00905}']
    procedure Clear; dispid 1;
    function GetData(sFormat: Smallint): OleVariant; dispid 2;
    function GetFormat(sFormat: Smallint): WordBool; dispid 3;
    procedure SetData(vValue: OleVariant; vFormat: OleVariant); dispid 4;
    property Files: IVBDataObjectFiles readonly dispid 5;
  end;

// *********************************************************************//
// Interface: IVBDataObjectFiles
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {2334D2B3-713E-11CF-8AE5-00AA00C00905}
// *********************************************************************//
  IVBDataObjectFiles = interface(IDispatch)
    ['{2334D2B3-713E-11CF-8AE5-00AA00C00905}']
    function Get_Item(lIndex: Integer): WideString; safecall;
    function Get_Count: Integer; safecall;
    procedure Add(const bstrFilename: WideString; vIndex: OleVariant); safecall;
    procedure Clear; safecall;
    procedure Remove(vIndex: OleVariant); safecall;
    function _NewEnum: IUnknown; safecall;
    property Item[lIndex: Integer]: WideString read Get_Item; default;
    property Count: Integer read Get_Count;
  end;

// *********************************************************************//
// DispIntf:  IVBDataObjectFilesDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {2334D2B3-713E-11CF-8AE5-00AA00C00905}
// *********************************************************************//
  IVBDataObjectFilesDisp = dispinterface
    ['{2334D2B3-713E-11CF-8AE5-00AA00C00905}']
    property Item[lIndex: Integer]: WideString readonly dispid 0; default;
    property Count: Integer readonly dispid 1;
    procedure Add(const bstrFilename: WideString; vIndex: OleVariant); dispid 2;
    procedure Clear; dispid 3;
    procedure Remove(vIndex: OleVariant); dispid 4;
    function _NewEnum: IUnknown; dispid -4;
  end;

// *********************************************************************//
// Interface: IRowCursor
// Flags:     (4432) Hidden Dual OleAutomation Dispatchable
// GUID:      {9F6AA700-D188-11CD-AD48-00AA003C9CB6}
// *********************************************************************//
  IRowCursor = interface(IDispatch)
    ['{9F6AA700-D188-11CD-AD48-00AA003C9CB6}']
  end;

// *********************************************************************//
// DispIntf:  IRowCursorDisp
// Flags:     (4432) Hidden Dual OleAutomation Dispatchable
// GUID:      {9F6AA700-D188-11CD-AD48-00AA003C9CB6}
// *********************************************************************//
  IRowCursorDisp = dispinterface
    ['{9F6AA700-D188-11CD-AD48-00AA003C9CB6}']
  end;

// *********************************************************************//
// Interface: IMSFlexGrid
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {5F4DF280-531B-11CF-91F6-C2863C385E30}
// *********************************************************************//
  IMSFlexGrid = interface(IDispatch)
    ['{5F4DF280-531B-11CF-91F6-C2863C385E30}']
    function Get_Rows: Integer; safecall;
    procedure Set_Rows(Rows: Integer); safecall;
    function Get_Cols: Integer; safecall;
    procedure Set_Cols(Cols: Integer); safecall;
    function Get_FixedRows: Integer; safecall;
    procedure Set_FixedRows(FixedRows: Integer); safecall;
    function Get_FixedCols: Integer; safecall;
    procedure Set_FixedCols(FixedCols: Integer); safecall;
    function Get_Version: Smallint; safecall;
    function Get_FormatString: WideString; safecall;
    procedure Set_FormatString(const FormatString: WideString); safecall;
    function Get_TopRow: Integer; safecall;
    procedure Set_TopRow(TopRow: Integer); safecall;
    function Get_LeftCol: Integer; safecall;
    procedure Set_LeftCol(LeftCol: Integer); safecall;
    function Get_Row: Integer; safecall;
    procedure Set_Row(Row: Integer); safecall;
    function Get_Col: Integer; safecall;
    procedure Set_Col(Col: Integer); safecall;
    function Get_RowSel: Integer; safecall;
    procedure Set_RowSel(RowSel: Integer); safecall;
    function Get_ColSel: Integer; safecall;
    procedure Set_ColSel(ColSel: Integer); safecall;
    function Get_Text: WideString; safecall;
    procedure Set_Text(const Text: WideString); safecall;
    function Get_BackColor: OLE_COLOR; safecall;
    procedure Set_BackColor(BackColor: OLE_COLOR); safecall;
    function Get_ForeColor: OLE_COLOR; safecall;
    procedure Set_ForeColor(ForeColor: OLE_COLOR); safecall;
    function Get_BackColorFixed: OLE_COLOR; safecall;
    procedure Set_BackColorFixed(BackColorFixed: OLE_COLOR); safecall;
    function Get_ForeColorFixed: OLE_COLOR; safecall;
    procedure Set_ForeColorFixed(ForeColorFixed: OLE_COLOR); safecall;
    function Get_BackColorSel: OLE_COLOR; safecall;
    procedure Set_BackColorSel(BackColorSel: OLE_COLOR); safecall;
    function Get_ForeColorSel: OLE_COLOR; safecall;
    procedure Set_ForeColorSel(ForeColorSel: OLE_COLOR); safecall;
    function Get_BackColorBkg: OLE_COLOR; safecall;
    procedure Set_BackColorBkg(BackColorBkg: OLE_COLOR); safecall;
    function Get_WordWrap: WordBool; safecall;
    procedure Set_WordWrap(WordWrap: WordBool); safecall;
    function Get_Font: IFontDisp; safecall;
    procedure _Set_Font(const Font: IFontDisp); safecall;
    function Get_FontWidth: Single; safecall;
    procedure Set_FontWidth(FontWidth: Single); safecall;
    function Get_CellFontName: WideString; safecall;
    procedure Set_CellFontName(const CellFontName: WideString); safecall;
    function Get_CellFontSize: Single; safecall;
    procedure Set_CellFontSize(CellFontSize: Single); safecall;
    function Get_CellFontBold: WordBool; safecall;
    procedure Set_CellFontBold(CellFontBold: WordBool); safecall;
    function Get_CellFontItalic: WordBool; safecall;
    procedure Set_CellFontItalic(CellFontItalic: WordBool); safecall;
    function Get_CellFontUnderline: WordBool; safecall;
    procedure Set_CellFontUnderline(CellFontUnderline: WordBool); safecall;
    function Get_CellFontStrikeThrough: WordBool; safecall;
    procedure Set_CellFontStrikeThrough(CellFontStrikeThrough: WordBool); safecall;
    function Get_CellFontWidth: Single; safecall;
    procedure Set_CellFontWidth(CellFontWidth: Single); safecall;
    function Get_TextStyle: TextStyleSettings; safecall;
    procedure Set_TextStyle(TextStyle: TextStyleSettings); safecall;
    function Get_TextStyleFixed: TextStyleSettings; safecall;
    procedure Set_TextStyleFixed(TextStyleFixed: TextStyleSettings); safecall;
    function Get_ScrollTrack: WordBool; safecall;
    procedure Set_ScrollTrack(ScrollTrack: WordBool); safecall;
    function Get_FocusRect: FocusRectSettings; safecall;
    procedure Set_FocusRect(FocusRect: FocusRectSettings); safecall;
    function Get_HighLight: HighLightSettings; safecall;
    procedure Set_HighLight(HighLight: HighLightSettings); safecall;
    function Get_Redraw: WordBool; safecall;
    procedure Set_Redraw(Redraw: WordBool); safecall;
    function Get_ScrollBars: ScrollBarsSettings; safecall;
    procedure Set_ScrollBars(ScrollBars: ScrollBarsSettings); safecall;
    function Get_MouseRow: Integer; safecall;
    function Get_MouseCol: Integer; safecall;
    function Get_CellLeft: Integer; safecall;
    function Get_CellTop: Integer; safecall;
    function Get_CellWidth: Integer; safecall;
    function Get_CellHeight: Integer; safecall;
    function Get_RowHeightMin: Integer; safecall;
    procedure Set_RowHeightMin(RowHeightMin: Integer); safecall;
    function Get_FillStyle: FillStyleSettings; safecall;
    procedure Set_FillStyle(FillStyle: FillStyleSettings); safecall;
    function Get_GridLines: GridLineSettings; safecall;
    procedure Set_GridLines(GridLines: GridLineSettings); safecall;
    function Get_GridLinesFixed: GridLineSettings; safecall;
    procedure Set_GridLinesFixed(GridLinesFixed: GridLineSettings); safecall;
    function Get_GridColor: OLE_COLOR; safecall;
    procedure Set_GridColor(GridColor: OLE_COLOR); safecall;
    function Get_GridColorFixed: OLE_COLOR; safecall;
    procedure Set_GridColorFixed(GridColorFixed: OLE_COLOR); safecall;
    function Get_CellBackColor: OLE_COLOR; safecall;
    procedure Set_CellBackColor(CellBackColor: OLE_COLOR); safecall;
    function Get_CellForeColor: OLE_COLOR; safecall;
    procedure Set_CellForeColor(CellForeColor: OLE_COLOR); safecall;
    function Get_CellAlignment: Smallint; safecall;
    procedure Set_CellAlignment(CellAlignment: Smallint); safecall;
    function Get_CellTextStyle: TextStyleSettings; safecall;
    procedure Set_CellTextStyle(CellTextStyle: TextStyleSettings); safecall;
    function Get_CellPictureAlignment: Smallint; safecall;
    procedure Set_CellPictureAlignment(CellPictureAlignment: Smallint); safecall;
    function Get_Clip: WideString; safecall;
    procedure Set_Clip(const Clip: WideString); safecall;
    procedure Set_Sort(Param1: Smallint); safecall;
    function Get_SelectionMode: SelectionModeSettings; safecall;
    procedure Set_SelectionMode(SelectionMode: SelectionModeSettings); safecall;
    function Get_MergeCells: MergeCellsSettings; safecall;
    procedure Set_MergeCells(MergeCells: MergeCellsSettings); safecall;
    function Get_AllowBigSelection: WordBool; safecall;
    procedure Set_AllowBigSelection(AllowBigSelection: WordBool); safecall;
    function Get_AllowUserResizing: AllowUserResizeSettings; safecall;
    procedure Set_AllowUserResizing(AllowUserResizing: AllowUserResizeSettings); safecall;
    function Get_BorderStyle: BorderStyleSettings; safecall;
    procedure Set_BorderStyle(BorderStyle: BorderStyleSettings); safecall;
    function Get_hWnd: Integer; safecall;
    function Get_Enabled: WordBool; safecall;
    procedure Set_Enabled(Enabled: WordBool); safecall;
    function Get_Appearance: AppearanceSettings; safecall;
    procedure Set_Appearance(Appearance: AppearanceSettings); safecall;
    function Get_MousePointer: MousePointerSettings; safecall;
    procedure Set_MousePointer(MousePointer: MousePointerSettings); safecall;
    function Get_MouseIcon: IPictureDisp; safecall;
    procedure _Set_MouseIcon(const MouseIcon: IPictureDisp); safecall;
    function Get_PictureType: PictureTypeSettings; safecall;
    procedure Set_PictureType(PictureType: PictureTypeSettings); safecall;
    function Get_Picture: IPictureDisp; safecall;
    function Get_CellPicture: IPictureDisp; safecall;
    procedure _Set_CellPicture(const CellPicture: IPictureDisp); safecall;
    procedure AboutBox; stdcall;
    function Get_TextArray(index: Integer): WideString; safecall;
    procedure Set_TextArray(index: Integer; const TextArray: WideString); safecall;
    function Get_ColAlignment(index: Integer): Smallint; safecall;
    procedure Set_ColAlignment(index: Integer; ColAlignment: Smallint); safecall;
    function Get_ColWidth(index: Integer): Integer; safecall;
    procedure Set_ColWidth(index: Integer; ColWidth: Integer); safecall;
    function Get_RowHeight(index: Integer): Integer; safecall;
    procedure Set_RowHeight(index: Integer; RowHeight: Integer); safecall;
    function Get_MergeRow(index: Integer): WordBool; safecall;
    procedure Set_MergeRow(index: Integer; MergeRow: WordBool); safecall;
    function Get_MergeCol(index: Integer): WordBool; safecall;
    procedure Set_MergeCol(index: Integer; MergeCol: WordBool); safecall;
    procedure Set_RowPosition(index: Integer; Param2: Integer); safecall;
    procedure Set_ColPosition(index: Integer; Param2: Integer); safecall;
    function Get_RowData(index: Integer): Integer; safecall;
    procedure Set_RowData(index: Integer; RowData: Integer); safecall;
    function Get_ColData(index: Integer): Integer; safecall;
    procedure Set_ColData(index: Integer; ColData: Integer); safecall;
    function Get_TextMatrix(Row: Integer; Col: Integer): WideString; safecall;
    procedure Set_TextMatrix(Row: Integer; Col: Integer; const TextMatrix: WideString); safecall;
    procedure AddItem(const Item: WideString; index: OleVariant); safecall;
    procedure RemoveItem(index: Integer); safecall;
    procedure Clear; stdcall;
    procedure Refresh; stdcall;
    function Get_DataSource: IRowCursor; safecall;
    procedure Set_DataSource(const DataSource: IRowCursor); safecall;
    function Get_RowIsVisible(index: Integer): WordBool; safecall;
    function Get_ColIsVisible(index: Integer): WordBool; safecall;
    function Get_RowPos(index: Integer): Integer; safecall;
    function Get_ColPos(index: Integer): Integer; safecall;
    function Get_GridLineWidth: Smallint; safecall;
    procedure Set_GridLineWidth(GridLineWidth: Smallint); safecall;
    function Get_FixedAlignment(index: Integer): Smallint; safecall;
    procedure Set_FixedAlignment(index: Integer; FixedAlignment: Smallint); safecall;
    function Get_FontName: WideString; safecall;
    procedure Set_FontName(const FontName: WideString); safecall;
    function Get_FontSize: Single; safecall;
    procedure Set_FontSize(FontSize: Single); safecall;
    function Get_FontBold: WordBool; safecall;
    procedure Set_FontBold(FontBold: WordBool); safecall;
    function Get_FontItalic: WordBool; safecall;
    procedure Set_FontItalic(FontItalic: WordBool); safecall;
    function Get_FontStrikethru: WordBool; safecall;
    procedure Set_FontStrikethru(FontStrikethru: WordBool); safecall;
    function Get_FontUnderline: WordBool; safecall;
    procedure Set_FontUnderline(FontUnderline: WordBool); safecall;
    function Get_RightToLeft: WordBool; safecall;
    procedure Set_RightToLeft(RightToLeft: WordBool); safecall;
    function Get_OLEDropMode: OLEDropConstants; safecall;
    procedure Set_OLEDropMode(psOLEDropMode: OLEDropConstants); safecall;
    procedure OLEDrag; safecall;
    property Rows: Integer read Get_Rows write Set_Rows;
    property Cols: Integer read Get_Cols write Set_Cols;
    property FixedRows: Integer read Get_FixedRows write Set_FixedRows;
    property FixedCols: Integer read Get_FixedCols write Set_FixedCols;
    property Version: Smallint read Get_Version;
    property FormatString: WideString read Get_FormatString write Set_FormatString;
    property TopRow: Integer read Get_TopRow write Set_TopRow;
    property LeftCol: Integer read Get_LeftCol write Set_LeftCol;
    property Row: Integer read Get_Row write Set_Row;
    property Col: Integer read Get_Col write Set_Col;
    property RowSel: Integer read Get_RowSel write Set_RowSel;
    property ColSel: Integer read Get_ColSel write Set_ColSel;
    property Text: WideString read Get_Text write Set_Text;
    property BackColor: OLE_COLOR read Get_BackColor write Set_BackColor;
    property ForeColor: OLE_COLOR read Get_ForeColor write Set_ForeColor;
    property BackColorFixed: OLE_COLOR read Get_BackColorFixed write Set_BackColorFixed;
    property ForeColorFixed: OLE_COLOR read Get_ForeColorFixed write Set_ForeColorFixed;
    property BackColorSel: OLE_COLOR read Get_BackColorSel write Set_BackColorSel;
    property ForeColorSel: OLE_COLOR read Get_ForeColorSel write Set_ForeColorSel;
    property BackColorBkg: OLE_COLOR read Get_BackColorBkg write Set_BackColorBkg;
    property WordWrap: WordBool read Get_WordWrap write Set_WordWrap;
    property Font: IFontDisp read Get_Font write _Set_Font;
    property FontWidth: Single read Get_FontWidth write Set_FontWidth;
    property CellFontName: WideString read Get_CellFontName write Set_CellFontName;
    property CellFontSize: Single read Get_CellFontSize write Set_CellFontSize;
    property CellFontBold: WordBool read Get_CellFontBold write Set_CellFontBold;
    property CellFontItalic: WordBool read Get_CellFontItalic write Set_CellFontItalic;
    property CellFontUnderline: WordBool read Get_CellFontUnderline write Set_CellFontUnderline;
    property CellFontStrikeThrough: WordBool read Get_CellFontStrikeThrough write Set_CellFontStrikeThrough;
    property CellFontWidth: Single read Get_CellFontWidth write Set_CellFontWidth;
    property TextStyle: TextStyleSettings read Get_TextStyle write Set_TextStyle;
    property TextStyleFixed: TextStyleSettings read Get_TextStyleFixed write Set_TextStyleFixed;
    property ScrollTrack: WordBool read Get_ScrollTrack write Set_ScrollTrack;
    property FocusRect: FocusRectSettings read Get_FocusRect write Set_FocusRect;
    property HighLight: HighLightSettings read Get_HighLight write Set_HighLight;
    property Redraw: WordBool read Get_Redraw write Set_Redraw;
    property ScrollBars: ScrollBarsSettings read Get_ScrollBars write Set_ScrollBars;
    property MouseRow: Integer read Get_MouseRow;
    property MouseCol: Integer read Get_MouseCol;
    property CellLeft: Integer read Get_CellLeft;
    property CellTop: Integer read Get_CellTop;
    property CellWidth: Integer read Get_CellWidth;
    property CellHeight: Integer read Get_CellHeight;
    property RowHeightMin: Integer read Get_RowHeightMin write Set_RowHeightMin;
    property FillStyle: FillStyleSettings read Get_FillStyle write Set_FillStyle;
    property GridLines: GridLineSettings read Get_GridLines write Set_GridLines;
    property GridLinesFixed: GridLineSettings read Get_GridLinesFixed write Set_GridLinesFixed;
    property GridColor: OLE_COLOR read Get_GridColor write Set_GridColor;
    property GridColorFixed: OLE_COLOR read Get_GridColorFixed write Set_GridColorFixed;
    property CellBackColor: OLE_COLOR read Get_CellBackColor write Set_CellBackColor;
    property CellForeColor: OLE_COLOR read Get_CellForeColor write Set_CellForeColor;
    property CellAlignment: Smallint read Get_CellAlignment write Set_CellAlignment;
    property CellTextStyle: TextStyleSettings read Get_CellTextStyle write Set_CellTextStyle;
    property CellPictureAlignment: Smallint read Get_CellPictureAlignment write Set_CellPictureAlignment;
    property Clip: WideString read Get_Clip write Set_Clip;
    property Sort: Smallint write Set_Sort;
    property SelectionMode: SelectionModeSettings read Get_SelectionMode write Set_SelectionMode;
    property MergeCells: MergeCellsSettings read Get_MergeCells write Set_MergeCells;
    property AllowBigSelection: WordBool read Get_AllowBigSelection write Set_AllowBigSelection;
    property AllowUserResizing: AllowUserResizeSettings read Get_AllowUserResizing write Set_AllowUserResizing;
    property BorderStyle: BorderStyleSettings read Get_BorderStyle write Set_BorderStyle;
    property hWnd: Integer read Get_hWnd;
    property Enabled: WordBool read Get_Enabled write Set_Enabled;
    property Appearance: AppearanceSettings read Get_Appearance write Set_Appearance;
    property MousePointer: MousePointerSettings read Get_MousePointer write Set_MousePointer;
    property MouseIcon: IPictureDisp read Get_MouseIcon write _Set_MouseIcon;
    property PictureType: PictureTypeSettings read Get_PictureType write Set_PictureType;
    property Picture: IPictureDisp read Get_Picture;
    property CellPicture: IPictureDisp read Get_CellPicture write _Set_CellPicture;
    property TextArray[index: Integer]: WideString read Get_TextArray write Set_TextArray;
    property ColAlignment[index: Integer]: Smallint read Get_ColAlignment write Set_ColAlignment;
    property ColWidth[index: Integer]: Integer read Get_ColWidth write Set_ColWidth;
    property RowHeight[index: Integer]: Integer read Get_RowHeight write Set_RowHeight;
    property MergeRow[index: Integer]: WordBool read Get_MergeRow write Set_MergeRow;
    property MergeCol[index: Integer]: WordBool read Get_MergeCol write Set_MergeCol;
    property RowPosition[index: Integer]: Integer write Set_RowPosition;
    property ColPosition[index: Integer]: Integer write Set_ColPosition;
    property RowData[index: Integer]: Integer read Get_RowData write Set_RowData;
    property ColData[index: Integer]: Integer read Get_ColData write Set_ColData;
    property TextMatrix[Row: Integer; Col: Integer]: WideString read Get_TextMatrix write Set_TextMatrix;
    property DataSource: IRowCursor read Get_DataSource write Set_DataSource;
    property RowIsVisible[index: Integer]: WordBool read Get_RowIsVisible;
    property ColIsVisible[index: Integer]: WordBool read Get_ColIsVisible;
    property RowPos[index: Integer]: Integer read Get_RowPos;
    property ColPos[index: Integer]: Integer read Get_ColPos;
    property GridLineWidth: Smallint read Get_GridLineWidth write Set_GridLineWidth;
    property FixedAlignment[index: Integer]: Smallint read Get_FixedAlignment write Set_FixedAlignment;
    property FontName: WideString read Get_FontName write Set_FontName;
    property FontSize: Single read Get_FontSize write Set_FontSize;
    property FontBold: WordBool read Get_FontBold write Set_FontBold;
    property FontItalic: WordBool read Get_FontItalic write Set_FontItalic;
    property FontStrikethru: WordBool read Get_FontStrikethru write Set_FontStrikethru;
    property FontUnderline: WordBool read Get_FontUnderline write Set_FontUnderline;
    property RightToLeft: WordBool read Get_RightToLeft write Set_RightToLeft;
    property OLEDropMode: OLEDropConstants read Get_OLEDropMode write Set_OLEDropMode;
  end;

// *********************************************************************//
// DispIntf:  IMSFlexGridDisp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {5F4DF280-531B-11CF-91F6-C2863C385E30}
// *********************************************************************//
  IMSFlexGridDisp = dispinterface
    ['{5F4DF280-531B-11CF-91F6-C2863C385E30}']
    property Rows: Integer dispid 4;
    property Cols: Integer dispid 5;
    property FixedRows: Integer dispid 6;
    property FixedCols: Integer dispid 7;
    property Version: Smallint readonly dispid 1;
    property FormatString: WideString dispid 2;
    property TopRow: Integer dispid 8;
    property LeftCol: Integer dispid 9;
    property Row: Integer dispid 10;
    property Col: Integer dispid 11;
    property RowSel: Integer dispid 12;
    property ColSel: Integer dispid 13;
    property Text: WideString dispid 0;
    property BackColor: OLE_COLOR dispid -501;
    property ForeColor: OLE_COLOR dispid -513;
    property BackColorFixed: OLE_COLOR dispid 14;
    property ForeColorFixed: OLE_COLOR dispid 15;
    property BackColorSel: OLE_COLOR dispid 16;
    property ForeColorSel: OLE_COLOR dispid 17;
    property BackColorBkg: OLE_COLOR dispid 18;
    property WordWrap: WordBool dispid 19;
    property Font: IFontDisp dispid -512;
    property FontWidth: Single dispid 84;
    property CellFontName: WideString dispid 77;
    property CellFontSize: Single dispid 78;
    property CellFontBold: WordBool dispid 79;
    property CellFontItalic: WordBool dispid 80;
    property CellFontUnderline: WordBool dispid 81;
    property CellFontStrikeThrough: WordBool dispid 82;
    property CellFontWidth: Single dispid 83;
    property TextStyle: TextStyleSettings dispid 20;
    property TextStyleFixed: TextStyleSettings dispid 21;
    property ScrollTrack: WordBool dispid 22;
    property FocusRect: FocusRectSettings dispid 23;
    property HighLight: HighLightSettings dispid 24;
    property Redraw: WordBool dispid 25;
    property ScrollBars: ScrollBarsSettings dispid 26;
    property MouseRow: Integer readonly dispid 27;
    property MouseCol: Integer readonly dispid 28;
    property CellLeft: Integer readonly dispid 29;
    property CellTop: Integer readonly dispid 30;
    property CellWidth: Integer readonly dispid 31;
    property CellHeight: Integer readonly dispid 32;
    property RowHeightMin: Integer dispid 33;
    property FillStyle: FillStyleSettings dispid -511;
    property GridLines: GridLineSettings dispid 34;
    property GridLinesFixed: GridLineSettings dispid 35;
    property GridColor: OLE_COLOR dispid 36;
    property GridColorFixed: OLE_COLOR dispid 37;
    property CellBackColor: OLE_COLOR dispid 38;
    property CellForeColor: OLE_COLOR dispid 39;
    property CellAlignment: Smallint dispid 40;
    property CellTextStyle: TextStyleSettings dispid 41;
    property CellPictureAlignment: Smallint dispid 43;
    property Clip: WideString dispid 45;
    property Sort: Smallint writeonly dispid 46;
    property SelectionMode: SelectionModeSettings dispid 47;
    property MergeCells: MergeCellsSettings dispid 48;
    property AllowBigSelection: WordBool dispid 51;
    property AllowUserResizing: AllowUserResizeSettings dispid 52;
    property BorderStyle: BorderStyleSettings dispid -504;
    property hWnd: Integer readonly dispid -515;
    property Enabled: WordBool dispid -514;
    property Appearance: AppearanceSettings dispid -520;
    property MousePointer: MousePointerSettings dispid 53;
    property MouseIcon: IPictureDisp dispid 54;
    property PictureType: PictureTypeSettings dispid 50;
    property Picture: IPictureDisp readonly dispid 49;
    property CellPicture: IPictureDisp dispid 42;
    procedure AboutBox; dispid -552;
    property TextArray[index: Integer]: WideString dispid 55;
    property ColAlignment[index: Integer]: Smallint dispid 56;
    property ColWidth[index: Integer]: Integer dispid 57;
    property RowHeight[index: Integer]: Integer dispid 58;
    property MergeRow[index: Integer]: WordBool dispid 59;
    property MergeCol[index: Integer]: WordBool dispid 60;
    property RowPosition[index: Integer]: Integer writeonly dispid 61;
    property ColPosition[index: Integer]: Integer writeonly dispid 62;
    property RowData[index: Integer]: Integer dispid 63;
    property ColData[index: Integer]: Integer dispid 64;
    property TextMatrix[Row: Integer; Col: Integer]: WideString dispid 65;
    procedure AddItem(const Item: WideString; index: OleVariant); dispid 66;
    procedure RemoveItem(index: Integer); dispid 67;
    procedure Clear; dispid 68;
    procedure Refresh; dispid -550;
    property DataSource: IRowCursor dispid 76;
    property RowIsVisible[index: Integer]: WordBool readonly dispid 85;
    property ColIsVisible[index: Integer]: WordBool readonly dispid 86;
    property RowPos[index: Integer]: Integer readonly dispid 87;
    property ColPos[index: Integer]: Integer readonly dispid 88;
    property GridLineWidth: Smallint dispid 89;
    property FixedAlignment[index: Integer]: Smallint dispid 90;
    property FontName: WideString dispid 91;
    property FontSize: Single dispid 92;
    property FontBold: WordBool dispid 93;
    property FontItalic: WordBool dispid 94;
    property FontStrikethru: WordBool dispid 95;
    property FontUnderline: WordBool dispid 96;
    property RightToLeft: WordBool dispid -611;
    property OLEDropMode: OLEDropConstants dispid 1551;
    procedure OLEDrag; dispid 1552;
  end;

// *********************************************************************//
// DispIntf:  DMSFlexGridEvents
// Flags:     (4112) Hidden Dispatchable
// GUID:      {609602E0-531B-11CF-91F6-C2863C385E30}
// *********************************************************************//
  DMSFlexGridEvents = dispinterface
    ['{609602E0-531B-11CF-91F6-C2863C385E30}']
    procedure Click; dispid -600;
    procedure KeyDown(var KeyCode: Smallint; Shift: Smallint); dispid -602;
    procedure DblClick; dispid -601;
    procedure KeyPress(var KeyAscii: Smallint); dispid -603;
    procedure KeyUp(var KeyCode: Smallint; Shift: Smallint); dispid -604;
    procedure MouseDown(Button: Smallint; Shift: Smallint; x: OLE_XPOS_PIXELS; y: OLE_YPOS_PIXELS); dispid -605;
    procedure MouseMove(Button: Smallint; Shift: Smallint; x: OLE_XPOS_PIXELS; y: OLE_YPOS_PIXELS); dispid -606;
    procedure MouseUp(Button: Smallint; Shift: Smallint; x: OLE_XPOS_PIXELS; y: OLE_YPOS_PIXELS); dispid -607;
    procedure SelChange; dispid 69;
    procedure RowColChange; dispid 70;
    procedure EnterCell; dispid 71;
    procedure LeaveCell; dispid 72;
    procedure Scroll; dispid 73;
    procedure Compare(Row1: Integer; Row2: Integer; var Cmp: Smallint); dispid 74;
    procedure OLEStartDrag(var Data: DataObject; var AllowedEffects: Integer); dispid 1550;
    procedure OLEGiveFeedback(var Effect: Integer; var DefaultCursors: WordBool); dispid 1551;
    procedure OLESetData(var Data: DataObject; var DataFormat: Smallint); dispid 1552;
    procedure OLECompleteDrag(var Effect: Integer); dispid 1553;
    procedure OLEDragOver(var Data: DataObject; var Effect: Integer; var Button: Smallint; 
                          var Shift: Smallint; var x: Single; var y: Single; var State: Smallint); dispid 1554;
    procedure OLEDragDrop(var Data: DataObject; var Effect: Integer; var Button: Smallint; 
                          var Shift: Smallint; var x: Single; var y: Single); dispid 1555;
  end;

// *********************************************************************//
// The Class CoDataObject provides a Create and CreateRemote method to          
// create instances of the default interface IVBDataObject exposed by              
// the CoClass DataObject. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoDataObject = class
    class function Create: IVBDataObject;
    class function CreateRemote(const MachineName: string): IVBDataObject;
  end;

// *********************************************************************//
// The Class CoDataObjectFiles provides a Create and CreateRemote method to          
// create instances of the default interface IVBDataObjectFiles exposed by              
// the CoClass DataObjectFiles. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoDataObjectFiles = class
    class function Create: IVBDataObjectFiles;
    class function CreateRemote(const MachineName: string): IVBDataObjectFiles;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TMSFlexGrid
// Help String      : Microsoft FlexGrid Control 6.0 (SP6)
// Default Interface: IMSFlexGrid
// Def. Intf. DISP? : No
// Event   Interface: DMSFlexGridEvents
// TypeFlags        : (38) CanCreate Licensed Control
// *********************************************************************//
  TMSFlexGridCompare = procedure(ASender: TObject; Row1: Integer; Row2: Integer; var Cmp: Smallint) of object;
  TMSFlexGridOLEStartDrag = procedure(ASender: TObject; var Data: DataObject; 
                                                        var AllowedEffects: Integer) of object;
  TMSFlexGridOLEGiveFeedback = procedure(ASender: TObject; var Effect: Integer; 
                                                           var DefaultCursors: WordBool) of object;
  TMSFlexGridOLESetData = procedure(ASender: TObject; var Data: DataObject; var DataFormat: Smallint) of object;
  TMSFlexGridOLECompleteDrag = procedure(ASender: TObject; var Effect: Integer) of object;
  TMSFlexGridOLEDragOver = procedure(ASender: TObject; var Data: DataObject; var Effect: Integer; 
                                                       var Button: Smallint; var Shift: Smallint; 
                                                       var x: Single; var y: Single; 
                                                       var State: Smallint) of object;
  TMSFlexGridOLEDragDrop = procedure(ASender: TObject; var Data: DataObject; var Effect: Integer; 
                                                       var Button: Smallint; var Shift: Smallint; 
                                                       var x: Single; var y: Single) of object;

  TMSFlexGrid = class(TOleControl)
  private
    FOnSelChange: TNotifyEvent;
    FOnRowColChange: TNotifyEvent;
    FOnEnterCell: TNotifyEvent;
    FOnLeaveCell: TNotifyEvent;
    FOnScroll: TNotifyEvent;
    FOnCompare: TMSFlexGridCompare;
    FOnOLEStartDrag: TMSFlexGridOLEStartDrag;
    FOnOLEGiveFeedback: TMSFlexGridOLEGiveFeedback;
    FOnOLESetData: TMSFlexGridOLESetData;
    FOnOLECompleteDrag: TMSFlexGridOLECompleteDrag;
    FOnOLEDragOver: TMSFlexGridOLEDragOver;
    FOnOLEDragDrop: TMSFlexGridOLEDragDrop;
    FIntf: IMSFlexGrid;
    function  GetControlInterface: IMSFlexGrid;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
    function Get_TextArray(index: Integer): WideString;
    procedure Set_TextArray(index: Integer; const TextArray: WideString);
    function Get_ColAlignment(index: Integer): Smallint;
    procedure Set_ColAlignment(index: Integer; ColAlignment: Smallint);
    function Get_ColWidth(index: Integer): Integer;
    procedure Set_ColWidth(index: Integer; ColWidth: Integer);
    function Get_RowHeight(index: Integer): Integer;
    procedure Set_RowHeight(index: Integer; RowHeight: Integer);
    function Get_MergeRow(index: Integer): WordBool;
    procedure Set_MergeRow(index: Integer; MergeRow: WordBool);
    function Get_MergeCol(index: Integer): WordBool;
    procedure Set_MergeCol(index: Integer; MergeCol: WordBool);
    procedure Set_RowPosition(index: Integer; Param2: Integer);
    procedure Set_ColPosition(index: Integer; Param2: Integer);
    function Get_RowData(index: Integer): Integer;
    procedure Set_RowData(index: Integer; RowData: Integer);
    function Get_ColData(index: Integer): Integer;
    procedure Set_ColData(index: Integer; ColData: Integer);
    function Get_TextMatrix(Row: Integer; Col: Integer): WideString;
    procedure Set_TextMatrix(Row: Integer; Col: Integer; const TextMatrix: WideString);
    function Get_DataSource: IRowCursor;
    procedure Set_DataSource(const DataSource: IRowCursor);
    function Get_RowIsVisible(index: Integer): WordBool;
    function Get_ColIsVisible(index: Integer): WordBool;
    function Get_RowPos(index: Integer): Integer;
    function Get_ColPos(index: Integer): Integer;
    function Get_FixedAlignment(index: Integer): Smallint;
    procedure Set_FixedAlignment(index: Integer; FixedAlignment: Smallint);
  public
    procedure AboutBox;
    procedure AddItem(const Item: WideString); overload;
    procedure AddItem(const Item: WideString; index: OleVariant); overload;
    procedure RemoveItem(index: Integer);
    procedure Clear;
    procedure Refresh;
    procedure OLEDrag;
    property  ControlInterface: IMSFlexGrid read GetControlInterface;
    property  DefaultInterface: IMSFlexGrid read GetControlInterface;
    property Version: Smallint index 1 read GetSmallintProp;
    property Font: TFont index -512 read GetTFontProp write _SetTFontProp;
    property MouseRow: Integer index 27 read GetIntegerProp;
    property MouseCol: Integer index 28 read GetIntegerProp;
    property CellLeft: Integer index 29 read GetIntegerProp;
    property CellTop: Integer index 30 read GetIntegerProp;
    property CellWidth: Integer index 31 read GetIntegerProp;
    property CellHeight: Integer index 32 read GetIntegerProp;
    property Sort: Smallint index 46 write SetSmallintProp;
    property hWnd: Integer index -515 read GetIntegerProp;
    property MouseIcon: TPicture index 54 read GetTPictureProp write _SetTPictureProp;
    property Picture: TPicture index 49 read GetTPictureProp;
    property CellPicture: TPicture index 42 read GetTPictureProp write _SetTPictureProp;
    property TextArray[index: Integer]: WideString read Get_TextArray write Set_TextArray;
    property ColAlignment[index: Integer]: Smallint read Get_ColAlignment write Set_ColAlignment;
    property ColWidth[index: Integer]: Integer read Get_ColWidth write Set_ColWidth;
    property RowHeight[index: Integer]: Integer read Get_RowHeight write Set_RowHeight;
    property MergeRow[index: Integer]: WordBool read Get_MergeRow write Set_MergeRow;
    property MergeCol[index: Integer]: WordBool read Get_MergeCol write Set_MergeCol;
    property RowPosition[index: Integer]: Integer write Set_RowPosition;
    property ColPosition[index: Integer]: Integer write Set_ColPosition;
    property RowData[index: Integer]: Integer read Get_RowData write Set_RowData;
    property ColData[index: Integer]: Integer read Get_ColData write Set_ColData;
    property TextMatrix[Row: Integer; Col: Integer]: WideString read Get_TextMatrix write Set_TextMatrix;
    property RowIsVisible[index: Integer]: WordBool read Get_RowIsVisible;
    property ColIsVisible[index: Integer]: WordBool read Get_ColIsVisible;
    property RowPos[index: Integer]: Integer read Get_RowPos;
    property ColPos[index: Integer]: Integer read Get_ColPos;
    property FixedAlignment[index: Integer]: Smallint read Get_FixedAlignment write Set_FixedAlignment;
    property FontName: WideString index 91 read GetWideStringProp write SetWideStringProp;
    property FontSize: Single index 92 read GetSingleProp write SetSingleProp;
    property FontBold: WordBool index 93 read GetWordBoolProp write SetWordBoolProp;
    property FontItalic: WordBool index 94 read GetWordBoolProp write SetWordBoolProp;
    property FontStrikethru: WordBool index 95 read GetWordBoolProp write SetWordBoolProp;
    property FontUnderline: WordBool index 96 read GetWordBoolProp write SetWordBoolProp;
  published
    property Anchors;
    property  ParentColor;
    property  ParentFont;
    property  TabStop;
    property  Align;
    property  DragCursor;
    property  DragMode;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  Visible;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property  OnMouseUp;
    property  OnMouseMove;
    property  OnMouseDown;
    property  OnKeyUp;
    property  OnKeyPress;
    property  OnKeyDown;
    property  OnDblClick;
    property  OnClick;
    property Rows: Integer index 4 read GetIntegerProp write SetIntegerProp stored False;
    property Cols: Integer index 5 read GetIntegerProp write SetIntegerProp stored False;
    property FixedRows: Integer index 6 read GetIntegerProp write SetIntegerProp stored False;
    property FixedCols: Integer index 7 read GetIntegerProp write SetIntegerProp stored False;
    property FormatString: WideString index 2 read GetWideStringProp write SetWideStringProp stored False;
    property TopRow: Integer index 8 read GetIntegerProp write SetIntegerProp stored False;
    property LeftCol: Integer index 9 read GetIntegerProp write SetIntegerProp stored False;
    property Row: Integer index 10 read GetIntegerProp write SetIntegerProp stored False;
    property Col: Integer index 11 read GetIntegerProp write SetIntegerProp stored False;
    property RowSel: Integer index 12 read GetIntegerProp write SetIntegerProp stored False;
    property ColSel: Integer index 13 read GetIntegerProp write SetIntegerProp stored False;
    property Text: WideString index 0 read GetWideStringProp write SetWideStringProp stored False;
    property BackColor: TColor index -501 read GetTColorProp write SetTColorProp stored False;
    property ForeColor: TColor index -513 read GetTColorProp write SetTColorProp stored False;
    property BackColorFixed: TColor index 14 read GetTColorProp write SetTColorProp stored False;
    property ForeColorFixed: TColor index 15 read GetTColorProp write SetTColorProp stored False;
    property BackColorSel: TColor index 16 read GetTColorProp write SetTColorProp stored False;
    property ForeColorSel: TColor index 17 read GetTColorProp write SetTColorProp stored False;
    property BackColorBkg: TColor index 18 read GetTColorProp write SetTColorProp stored False;
    property WordWrap: WordBool index 19 read GetWordBoolProp write SetWordBoolProp stored False;
    property FontWidth: Single index 84 read GetSingleProp write SetSingleProp stored False;
    property CellFontName: WideString index 77 read GetWideStringProp write SetWideStringProp stored False;
    property CellFontSize: Single index 78 read GetSingleProp write SetSingleProp stored False;
    property CellFontBold: WordBool index 79 read GetWordBoolProp write SetWordBoolProp stored False;
    property CellFontItalic: WordBool index 80 read GetWordBoolProp write SetWordBoolProp stored False;
    property CellFontUnderline: WordBool index 81 read GetWordBoolProp write SetWordBoolProp stored False;
    property CellFontStrikeThrough: WordBool index 82 read GetWordBoolProp write SetWordBoolProp stored False;
    property CellFontWidth: Single index 83 read GetSingleProp write SetSingleProp stored False;
    property TextStyle: TOleEnum index 20 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property TextStyleFixed: TOleEnum index 21 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property ScrollTrack: WordBool index 22 read GetWordBoolProp write SetWordBoolProp stored False;
    property FocusRect: TOleEnum index 23 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property HighLight: TOleEnum index 24 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property Redraw: WordBool index 25 read GetWordBoolProp write SetWordBoolProp stored False;
    property ScrollBars: TOleEnum index 26 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property RowHeightMin: Integer index 33 read GetIntegerProp write SetIntegerProp stored False;
    property FillStyle: TOleEnum index -511 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property GridLines: TOleEnum index 34 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property GridLinesFixed: TOleEnum index 35 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property GridColor: TColor index 36 read GetTColorProp write SetTColorProp stored False;
    property GridColorFixed: TColor index 37 read GetTColorProp write SetTColorProp stored False;
    property CellBackColor: TColor index 38 read GetTColorProp write SetTColorProp stored False;
    property CellForeColor: TColor index 39 read GetTColorProp write SetTColorProp stored False;
    property CellAlignment: Smallint index 40 read GetSmallintProp write SetSmallintProp stored False;
    property CellTextStyle: TOleEnum index 41 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property CellPictureAlignment: Smallint index 43 read GetSmallintProp write SetSmallintProp stored False;
    property Clip: WideString index 45 read GetWideStringProp write SetWideStringProp stored False;
    property SelectionMode: TOleEnum index 47 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property MergeCells: TOleEnum index 48 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property AllowBigSelection: WordBool index 51 read GetWordBoolProp write SetWordBoolProp stored False;
    property AllowUserResizing: TOleEnum index 52 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property BorderStyle: TOleEnum index -504 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property Enabled: WordBool index -514 read GetWordBoolProp write SetWordBoolProp stored False;
    property Appearance: TOleEnum index -520 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property MousePointer: TOleEnum index 53 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property PictureType: TOleEnum index 50 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property DataSource: IRowCursor read Get_DataSource write Set_DataSource stored False;
    property GridLineWidth: Smallint index 89 read GetSmallintProp write SetSmallintProp stored False;
    property RightToLeft: WordBool index -611 read GetWordBoolProp write SetWordBoolProp stored False;
    property OLEDropMode: TOleEnum index 1551 read GetTOleEnumProp write SetTOleEnumProp stored False;
    property OnSelChange: TNotifyEvent read FOnSelChange write FOnSelChange;
    property OnRowColChange: TNotifyEvent read FOnRowColChange write FOnRowColChange;
    property OnEnterCell: TNotifyEvent read FOnEnterCell write FOnEnterCell;
    property OnLeaveCell: TNotifyEvent read FOnLeaveCell write FOnLeaveCell;
    property OnScroll: TNotifyEvent read FOnScroll write FOnScroll;
    property OnCompare: TMSFlexGridCompare read FOnCompare write FOnCompare;
    property OnOLEStartDrag: TMSFlexGridOLEStartDrag read FOnOLEStartDrag write FOnOLEStartDrag;
    property OnOLEGiveFeedback: TMSFlexGridOLEGiveFeedback read FOnOLEGiveFeedback write FOnOLEGiveFeedback;
    property OnOLESetData: TMSFlexGridOLESetData read FOnOLESetData write FOnOLESetData;
    property OnOLECompleteDrag: TMSFlexGridOLECompleteDrag read FOnOLECompleteDrag write FOnOLECompleteDrag;
    property OnOLEDragOver: TMSFlexGridOLEDragOver read FOnOLEDragOver write FOnOLEDragOver;
    property OnOLEDragDrop: TMSFlexGridOLEDragDrop read FOnOLEDragDrop write FOnOLEDragDrop;
  end;

procedure Register;

resourcestring
  dtlServerPage = 'ActiveX';

  dtlOcxPage = 'ActiveX';

implementation

uses ComObj;

class function CoDataObject.Create: IVBDataObject;
begin
  Result := CreateComObject(CLASS_DataObject) as IVBDataObject;
end;

class function CoDataObject.CreateRemote(const MachineName: string): IVBDataObject;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_DataObject) as IVBDataObject;
end;

class function CoDataObjectFiles.Create: IVBDataObjectFiles;
begin
  Result := CreateComObject(CLASS_DataObjectFiles) as IVBDataObjectFiles;
end;

class function CoDataObjectFiles.CreateRemote(const MachineName: string): IVBDataObjectFiles;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_DataObjectFiles) as IVBDataObjectFiles;
end;

procedure TMSFlexGrid.InitControlData;
const
  CEventDispIDs: array [0..11] of DWORD = (
    $00000045, $00000046, $00000047, $00000048, $00000049, $0000004A,
    $0000060E, $0000060F, $00000610, $00000611, $00000612, $00000613);
  CLicenseKey: array[0..36] of Word = ( $0037, $0032, $0045, $0036, $0037, $0031, $0032, $0030, $002D, $0035, $0039
    , $0035, $0039, $002D, $0031, $0031, $0063, $0066, $002D, $0039, $0031
    , $0046, $0036, $002D, $0043, $0032, $0038, $0036, $0033, $0043, $0033
    , $0038, $0035, $0045, $0033, $0030, $0000);
  CTFontIDs: array [0..0] of DWORD = (
    $FFFFFE00);
  CTPictureIDs: array [0..2] of DWORD = (
    $00000036, $00000031, $0000002A);
  CControlData: TControlData2 = (
    ClassID: '{6262D3A0-531B-11CF-91F6-C2863C385E30}';
    EventIID: '{609602E0-531B-11CF-91F6-C2863C385E30}';
    EventCount: 12;
    EventDispIDs: @CEventDispIDs;
    LicenseKey: @CLicenseKey;
    Flags: $0000000F;
    Version: 401;
    FontCount: 1;
    FontIDs: @CTFontIDs;
    PictureCount: 3;
    PictureIDs: @CTPictureIDs);
begin
  ControlData := @CControlData;
  TControlData2(CControlData).FirstEventOfs := Cardinal(@@FOnSelChange) - Cardinal(Self);
end;

procedure TMSFlexGrid.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as IMSFlexGrid;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TMSFlexGrid.GetControlInterface: IMSFlexGrid;
begin
  CreateControl;
  Result := FIntf;
end;

function TMSFlexGrid.Get_TextArray(index: Integer): WideString;
begin
    Result := DefaultInterface.TextArray[index];
end;

procedure TMSFlexGrid.Set_TextArray(index: Integer; const TextArray: WideString);
  { Warning: The property TextArray has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.TextArray := TextArray;
end;

function TMSFlexGrid.Get_ColAlignment(index: Integer): Smallint;
begin
    Result := DefaultInterface.ColAlignment[index];
end;

procedure TMSFlexGrid.Set_ColAlignment(index: Integer; ColAlignment: Smallint);
begin
  DefaultInterface.ColAlignment[index] := ColAlignment;
end;

function TMSFlexGrid.Get_ColWidth(index: Integer): Integer;
begin
    Result := DefaultInterface.ColWidth[index];
end;

procedure TMSFlexGrid.Set_ColWidth(index: Integer; ColWidth: Integer);
begin
  DefaultInterface.ColWidth[index] := ColWidth;
end;

function TMSFlexGrid.Get_RowHeight(index: Integer): Integer;
begin
    Result := DefaultInterface.RowHeight[index];
end;

procedure TMSFlexGrid.Set_RowHeight(index: Integer; RowHeight: Integer);
begin
  DefaultInterface.RowHeight[index] := RowHeight;
end;

function TMSFlexGrid.Get_MergeRow(index: Integer): WordBool;
begin
    Result := DefaultInterface.MergeRow[index];
end;

procedure TMSFlexGrid.Set_MergeRow(index: Integer; MergeRow: WordBool);
begin
  DefaultInterface.MergeRow[index] := MergeRow;
end;

function TMSFlexGrid.Get_MergeCol(index: Integer): WordBool;
begin
    Result := DefaultInterface.MergeCol[index];
end;

procedure TMSFlexGrid.Set_MergeCol(index: Integer; MergeCol: WordBool);
begin
  DefaultInterface.MergeCol[index] := MergeCol;
end;

procedure TMSFlexGrid.Set_RowPosition(index: Integer; Param2: Integer);
begin
  DefaultInterface.RowPosition[index] := Param2;
end;

procedure TMSFlexGrid.Set_ColPosition(index: Integer; Param2: Integer);
begin
  DefaultInterface.ColPosition[index] := Param2;
end;

function TMSFlexGrid.Get_RowData(index: Integer): Integer;
begin
    Result := DefaultInterface.RowData[index];
end;

procedure TMSFlexGrid.Set_RowData(index: Integer; RowData: Integer);
begin
  DefaultInterface.RowData[index] := RowData;
end;

function TMSFlexGrid.Get_ColData(index: Integer): Integer;
begin
    Result := DefaultInterface.ColData[index];
end;

procedure TMSFlexGrid.Set_ColData(index: Integer; ColData: Integer);
begin
  DefaultInterface.ColData[index] := ColData;
end;

function TMSFlexGrid.Get_TextMatrix(Row: Integer; Col: Integer): WideString;
begin
    Result := DefaultInterface.TextMatrix[Row, Col];
end;

procedure TMSFlexGrid.Set_TextMatrix(Row: Integer; Col: Integer; const TextMatrix: WideString);
  { Warning: The property TextMatrix has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.TextMatrix := TextMatrix;
end;

function TMSFlexGrid.Get_DataSource: IRowCursor;
begin
    Result := DefaultInterface.DataSource;
end;

procedure TMSFlexGrid.Set_DataSource(const DataSource: IRowCursor);
begin
  DefaultInterface.Set_DataSource(DataSource);
end;

function TMSFlexGrid.Get_RowIsVisible(index: Integer): WordBool;
begin
    Result := DefaultInterface.RowIsVisible[index];
end;

function TMSFlexGrid.Get_ColIsVisible(index: Integer): WordBool;
begin
    Result := DefaultInterface.ColIsVisible[index];
end;

function TMSFlexGrid.Get_RowPos(index: Integer): Integer;
begin
    Result := DefaultInterface.RowPos[index];
end;

function TMSFlexGrid.Get_ColPos(index: Integer): Integer;
begin
    Result := DefaultInterface.ColPos[index];
end;

function TMSFlexGrid.Get_FixedAlignment(index: Integer): Smallint;
begin
    Result := DefaultInterface.FixedAlignment[index];
end;

procedure TMSFlexGrid.Set_FixedAlignment(index: Integer; FixedAlignment: Smallint);
begin
  DefaultInterface.FixedAlignment[index] := FixedAlignment;
end;

procedure TMSFlexGrid.AboutBox;
begin
  DefaultInterface.AboutBox;
end;

procedure TMSFlexGrid.AddItem(const Item: WideString);
begin
  DefaultInterface.AddItem(Item, EmptyParam);
end;

procedure TMSFlexGrid.AddItem(const Item: WideString; index: OleVariant);
begin
  DefaultInterface.AddItem(Item, index);
end;

procedure TMSFlexGrid.RemoveItem(index: Integer);
begin
  DefaultInterface.RemoveItem(index);
end;

procedure TMSFlexGrid.Clear;
begin
  DefaultInterface.Clear;
end;

procedure TMSFlexGrid.Refresh;
begin
  DefaultInterface.Refresh;
end;

procedure TMSFlexGrid.OLEDrag;
begin
  DefaultInterface.OLEDrag;
end;

procedure Register;
begin
  RegisterComponents(dtlOcxPage, [TMSFlexGrid]);
end;

end.
