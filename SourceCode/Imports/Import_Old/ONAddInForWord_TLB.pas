unit ONAddInForWord_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 2016/10/6 17:19:24 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Program Files\MICROS~1\Office15\ONWORD~1.DLL (1)
// LIBID: {A717753E-C3A6-4650-9F60-472EB56A7061}
// LCID: 0
// Helpfile: 
// HelpString: ONAddInForWord 1.0 Type Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Vcl.OleServer, Winapi.ActiveX;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ONAddInForWordMajorVersion = 1;
  ONAddInForWordMinorVersion = 0;

  LIBID_ONAddInForWord: TGUID = '{A717753E-C3A6-4650-9F60-472EB56A7061}';

  CLASS_Connect: TGUID = '{C580A1B2-5915-4DC3-BE93-8A51F4CAB320}';
type

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Connect = IUnknown;


// *********************************************************************//
// The Class CoConnect provides a Create and CreateRemote method to          
// create instances of the default interface IUnknown exposed by              
// the CoClass Connect. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoConnect = class
    class function Create: IUnknown;
    class function CreateRemote(const MachineName: string): IUnknown;
  end;

implementation

uses System.Win.ComObj;

class function CoConnect.Create: IUnknown;
begin
  Result := CreateComObject(CLASS_Connect) as IUnknown;
end;

class function CoConnect.CreateRemote(const MachineName: string): IUnknown;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Connect) as IUnknown;
end;

end.
