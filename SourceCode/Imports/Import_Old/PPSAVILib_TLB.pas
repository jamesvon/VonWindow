unit PPSAVILib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 34747 $
// File generated on 2013/4/10 17:50:45 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\Program Files (x86)\WorldCard\Bin\PPSAVI.exe (1)
// LIBID: {1E2CC7B8-F693-4DD5-8CEA-6755B0AB7CEF}
// LCID: 0
// Helpfile: 
// HelpString: PPSAVI 1.0 Type Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  PPSAVILibMajorVersion = 1;
  PPSAVILibMinorVersion = 0;

  LIBID_PPSAVILib: TGUID = '{1E2CC7B8-F693-4DD5-8CEA-6755B0AB7CEF}';

  IID_IAVIWnd: TGUID = '{395D1374-B95A-4CCD-916A-6321A9BE0633}';
  CLASS_AVIWnd: TGUID = '{309EFB5C-0D9E-4E73-A7D3-69F89BF7BD1A}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IAVIWnd = interface;
  IAVIWndDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  AVIWnd = IAVIWnd;


// *********************************************************************//
// Interface: IAVIWnd
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {395D1374-B95A-4CCD-916A-6321A9BE0633}
// *********************************************************************//
  IAVIWnd = interface(IDispatch)
    ['{395D1374-B95A-4CCD-916A-6321A9BE0633}']
    procedure SetAVIFilePathName(const filename: WideString); safecall;
    procedure ShowAVI(zShow: Integer; x: Integer; y: Integer; offsetX: Integer; offsetY: Integer; 
                      parent: Integer; uFlag: Integer); safecall;
    procedure ShowAVIEx(zShow: Integer; x: Integer; y: Integer; offsetX: Integer; offsetY: Integer; 
                        parent: Integer; uFlag: Integer; lAlpha: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IAVIWndDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {395D1374-B95A-4CCD-916A-6321A9BE0633}
// *********************************************************************//
  IAVIWndDisp = dispinterface
    ['{395D1374-B95A-4CCD-916A-6321A9BE0633}']
    procedure SetAVIFilePathName(const filename: WideString); dispid 1;
    procedure ShowAVI(zShow: Integer; x: Integer; y: Integer; offsetX: Integer; offsetY: Integer; 
                      parent: Integer; uFlag: Integer); dispid 2;
    procedure ShowAVIEx(zShow: Integer; x: Integer; y: Integer; offsetX: Integer; offsetY: Integer; 
                        parent: Integer; uFlag: Integer; lAlpha: Integer); dispid 3;
  end;

// *********************************************************************//
// The Class CoAVIWnd provides a Create and CreateRemote method to          
// create instances of the default interface IAVIWnd exposed by              
// the CoClass AVIWnd. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAVIWnd = class
    class function Create: IAVIWnd;
    class function CreateRemote(const MachineName: string): IAVIWnd;
  end;

implementation

uses ComObj;

class function CoAVIWnd.Create: IAVIWnd;
begin
  Result := CreateComObject(CLASS_AVIWnd) as IAVIWnd;
end;

class function CoAVIWnd.CreateRemote(const MachineName: string): IAVIWnd;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AVIWnd) as IAVIWnd;
end;

end.
