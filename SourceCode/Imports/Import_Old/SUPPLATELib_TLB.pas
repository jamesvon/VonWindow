﻿unit SUPPLATELib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 34747 $
// File generated on 2015/1/17 21:52:03 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Program Files (x86)\SupPlate车牌识别系统\SupPlateDemo\SupPlate.ocx (1)
// LIBID: {66908B16-CA82-48C7-93C0-DEC43C6FA5C5}
// LCID: 0
// Helpfile: C:\Program Files (x86)\SupPlate\SupPlateDemo\SupPlate.hlp
// HelpString: SupPlate ActiveX Control module
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}
interface

uses Windows, ActiveX, Classes, Graphics, OleCtrls, OleServer, StdVCL, Variants;
  


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  SUPPLATELibMajorVersion = 1;
  SUPPLATELibMinorVersion = 0;

  LIBID_SUPPLATELib: TGUID = '{66908B16-CA82-48C7-93C0-DEC43C6FA5C5}';

  DIID__DSupPlate: TGUID = '{4CAC05DD-6E8F-485F-87EF-228EF8278612}';
  DIID__DSupPlateEvents: TGUID = '{4AE163D0-B66E-45B9-A44A-6A8C634A66C2}';
  CLASS_SupPlate: TGUID = '{9BFF5271-2917-463D-8C77-9FB6D2B597A3}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  _DSupPlate = dispinterface;
  _DSupPlateEvents = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  SupPlate = _DSupPlate;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  PInteger1 = ^Integer; {*}


// *********************************************************************//
// DispIntf:  _DSupPlate
// Flags:     (4112) Hidden Dispatchable
// GUID:      {4CAC05DD-6E8F-485F-87EF-228EF8278612}
// *********************************************************************//
  _DSupPlate = dispinterface
    ['{4CAC05DD-6E8F-485F-87EF-228EF8278612}']
    procedure SendComOrder(lOrder: Integer); dispid 3;
    procedure AllowDetect(lOrder: Integer; bDetect: WordBool); dispid 4;
    procedure RecogniseBuf(var lpImageBuf: Integer; nImgWidth: Integer; nImgHeight: Integer); dispid 11;
    function GetDirRoadNum(lDirIndex: Integer): Integer; dispid 6;
    procedure TesttHandleBreak(lDirIndex: Integer; lRoadIndex: Integer); dispid 7;
    procedure SetVideoDetectParam; dispid 2;
    function GetPeccancyCount(lDirIndex: Integer; lRoadIndex: Integer): Integer; dispid 9;
    function GetSpeed(lDirIndex: Integer; lRoadIndex: Integer): Single; dispid 10;
    function SetKinescopeStat(nOrder: Integer): Integer; dispid 26;
    procedure RecognisePicFile(const strPicFile: WideString); dispid 12;
    procedure SetPlateMode(nPlateMode: Integer); dispid 13;
    function GetDetectFlux(lDirIndex: Integer; lRoadIndex: Integer): Integer; dispid 8;
    function GetDirNum: Integer; dispid 5;
    function SaveImageFile(const strSavePicFile: WideString; var lpSaveImageBuf: Integer; 
                           nSaveImgW: Integer; nSaveImgH: Integer): Integer; dispid 14;
    function GetPlateImage(nPlateInndex: Integer; var lpPlateImageBuf: Integer; 
                           nPlateImgW: Integer; nPlateImgH: Integer): Integer; dispid 15;
    function GetRecogImgWH(const strPicFile: WideString; var nImageW: Integer; var nImageH: Integer): Integer; dispid 16;
    function GetRecogImgBuf(const strPicFile: WideString; var lpImgBuf: Integer): Integer; dispid 17;
    function GetPlateNumber(nPlateIndex: Integer): WideString; dispid 18;
    function GetPlateColorNumber(nPlateIndex: Integer): Integer; dispid 19;
    function GetPlateColorString(nPlateIndex: Integer; nLanguageType: Integer): WideString; dispid 20;
    function GetPlateRect(nPlateIndex: Integer; var nPlateLeft: Integer; var nPlateRight: Integer; 
                          var nPlateTop: Integer; var nPlateBottom: Integer): Integer; dispid 21;
    function GetNearImage(var lpNearImg: Integer): Integer; dispid 22;
    function GetFarImage(nFarImgIndex: Integer; var lpFarImg: Integer): Integer; dispid 23;
    function DlgSetVideoDetectMode: Integer; dispid 24;
    procedure RecogKinescope(const strKinescope: WideString); dispid 25;
    procedure OnOcrDBManage; dispid 27;
    procedure GetPassedCarInfo(const PassedCarInfo: IUnknown); dispid 28;
    procedure SetRecogParam; dispid 29;
    procedure RegistSupPlate; dispid 30;
    function SetVideoDetectMode(nDetectMode: Integer): Integer; dispid 31;
    function GetModulPath: WideString; dispid 32;
    procedure RecogniseCamera; dispid 33;
    procedure SetCameraPin; dispid 34;
    procedure SetCameraFilter; dispid 35;
    procedure FinishSetLightStat; dispid 36;
    function GetRoadLightStat(nDirIndex: Integer; nRoadIndex: Integer): Integer; dispid 37;
    procedure SetRoadLightStat(nDirIndex: Integer; nRoadIndex: Integer; bIsRedLight: Integer); dispid 38;
    function GetDirRoadID(nDirIndex: Integer; var lDirID: Integer; nRoadIndex: Integer; 
                          var lRoadID: Integer): Integer; dispid 39;
    procedure RecogKinescopeRealTime(const strKinescope: WideString); dispid 40;
    procedure KinescopeDetectCar(const strKinescope: WideString; nMode: Integer); dispid 41;
    procedure SetRecogSourceCompressble(nCompress: Integer); dispid 42;
    function GetAreaRecogNum: Integer; dispid 43;
    procedure SetAreaRecogNum(nAreaNum: Integer); dispid 44;
    procedure AreaRecog(nAreaIndex: Integer); dispid 45;
    procedure AreaRecogInput(nAreaIndex: Integer; dAreaLeft: Double; dAreaRight: Double; 
                             dAreaTop: Double; dAreaBottom: Double); dispid 46;
    function PutAreaRecogPic(const strPicFile: WideString): Integer; dispid 47;
    function PutAreaRecogBuf(var lpImageBuf: Integer; nImageW: Integer; nImageH: Integer): Integer; dispid 48;
    procedure AreaRecogStart; dispid 49;
    procedure DlgSetAreaRecog; dispid 50;
    procedure AboutBox; dispid -552;
    property DisplayIndex: Integer dispid 1;
  end;

// *********************************************************************//
// DispIntf:  _DSupPlateEvents
// Flags:     (4096) Dispatchable
// GUID:      {4AE163D0-B66E-45B9-A44A-6A8C634A66C2}
// *********************************************************************//
  _DSupPlateEvents = dispinterface
    ['{4AE163D0-B66E-45B9-A44A-6A8C634A66C2}']
    procedure ImageSizeInfo(const ImageSize: IUnknown); dispid 1;
    procedure DetectInfo(const DetectInfo: IUnknown); dispid 2;
    procedure AfterRecogPlateEvent(nGetPlateCount: Integer); dispid 3;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TSupPlate
// Help String      : SupPlate Control
// Default Interface: _DSupPlate
// Def. Intf. DISP? : Yes
// Event   Interface: _DSupPlateEvents
// TypeFlags        : (34) CanCreate Control
// *********************************************************************//
  TSupPlateImageSizeInfo = procedure(ASender: TObject; const ImageSize: IUnknown) of object;
  TSupPlateDetectInfo = procedure(ASender: TObject; const DetectInfo: IUnknown) of object;
  TSupPlateAfterRecogPlateEvent = procedure(ASender: TObject; nGetPlateCount: Integer) of object;

  TSupPlate = class(TOleControl)
  private
    FOnImageSizeInfo: TSupPlateImageSizeInfo;
    FOnDetectInfo: TSupPlateDetectInfo;
    FOnAfterRecogPlateEvent: TSupPlateAfterRecogPlateEvent;
    FIntf: _DSupPlate;
    function  GetControlInterface: _DSupPlate;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
  public
    procedure SendComOrder(lOrder: Integer);
    procedure AllowDetect(lOrder: Integer; bDetect: WordBool);
    procedure RecogniseBuf(var lpImageBuf: Integer; nImgWidth: Integer; nImgHeight: Integer);
    function GetDirRoadNum(lDirIndex: Integer): Integer;
    procedure TesttHandleBreak(lDirIndex: Integer; lRoadIndex: Integer);
    procedure SetVideoDetectParam;
    function GetPeccancyCount(lDirIndex: Integer; lRoadIndex: Integer): Integer;
    function GetSpeed(lDirIndex: Integer; lRoadIndex: Integer): Single;
    function SetKinescopeStat(nOrder: Integer): Integer;
    procedure RecognisePicFile(const strPicFile: WideString);
    procedure SetPlateMode(nPlateMode: Integer);
    function GetDetectFlux(lDirIndex: Integer; lRoadIndex: Integer): Integer;
    function GetDirNum: Integer;
    function SaveImageFile(const strSavePicFile: WideString; var lpSaveImageBuf: Integer; 
                           nSaveImgW: Integer; nSaveImgH: Integer): Integer;
    function GetPlateImage(nPlateInndex: Integer; var lpPlateImageBuf: Integer; 
                           nPlateImgW: Integer; nPlateImgH: Integer): Integer;
    function GetRecogImgWH(const strPicFile: WideString; var nImageW: Integer; var nImageH: Integer): Integer;
    function GetRecogImgBuf(const strPicFile: WideString; var lpImgBuf: Integer): Integer;
    function GetPlateNumber(nPlateIndex: Integer): WideString;
    function GetPlateColorNumber(nPlateIndex: Integer): Integer;
    function GetPlateColorString(nPlateIndex: Integer; nLanguageType: Integer): WideString;
    function GetPlateRect(nPlateIndex: Integer; var nPlateLeft: Integer; var nPlateRight: Integer; 
                          var nPlateTop: Integer; var nPlateBottom: Integer): Integer;
    function GetNearImage(var lpNearImg: Integer): Integer;
    function GetFarImage(nFarImgIndex: Integer; var lpFarImg: Integer): Integer;
    function DlgSetVideoDetectMode: Integer;
    procedure RecogKinescope(const strKinescope: WideString);
    procedure OnOcrDBManage;
    procedure GetPassedCarInfo(const PassedCarInfo: IUnknown);
    procedure SetRecogParam;
    procedure RegistSupPlate;
    function SetVideoDetectMode(nDetectMode: Integer): Integer;
    function GetModulPath: WideString;
    procedure RecogniseCamera;
    procedure SetCameraPin;
    procedure SetCameraFilter;
    procedure FinishSetLightStat;
    function GetRoadLightStat(nDirIndex: Integer; nRoadIndex: Integer): Integer;
    procedure SetRoadLightStat(nDirIndex: Integer; nRoadIndex: Integer; bIsRedLight: Integer);
    function GetDirRoadID(nDirIndex: Integer; var lDirID: Integer; nRoadIndex: Integer; 
                          var lRoadID: Integer): Integer;
    procedure RecogKinescopeRealTime(const strKinescope: WideString);
    procedure KinescopeDetectCar(const strKinescope: WideString; nMode: Integer);
    procedure SetRecogSourceCompressble(nCompress: Integer);
    function GetAreaRecogNum: Integer;
    procedure SetAreaRecogNum(nAreaNum: Integer);
    procedure AreaRecog(nAreaIndex: Integer);
    procedure AreaRecogInput(nAreaIndex: Integer; dAreaLeft: Double; dAreaRight: Double; 
                             dAreaTop: Double; dAreaBottom: Double);
    function PutAreaRecogPic(const strPicFile: WideString): Integer;
    function PutAreaRecogBuf(var lpImageBuf: Integer; nImageW: Integer; nImageH: Integer): Integer;
    procedure AreaRecogStart;
    procedure DlgSetAreaRecog;
    procedure AboutBox;
    property  ControlInterface: _DSupPlate read GetControlInterface;
    property  DefaultInterface: _DSupPlate read GetControlInterface;
  published
    property Anchors;
    property  TabStop;
    property  Align;
    property  DragCursor;
    property  DragMode;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  Visible;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property DisplayIndex: Integer index 1 read GetIntegerProp write SetIntegerProp stored False;
    property OnImageSizeInfo: TSupPlateImageSizeInfo read FOnImageSizeInfo write FOnImageSizeInfo;
    property OnDetectInfo: TSupPlateDetectInfo read FOnDetectInfo write FOnDetectInfo;
    property OnAfterRecogPlateEvent: TSupPlateAfterRecogPlateEvent read FOnAfterRecogPlateEvent write FOnAfterRecogPlateEvent;
  end;

procedure Register;

resourcestring
  dtlServerPage = 'Samples';

  dtlOcxPage = 'Samples';

implementation

uses ComObj;

procedure TSupPlate.InitControlData;
const
  CEventDispIDs: array [0..2] of DWORD = (
    $00000001, $00000002, $00000003);
  CControlData: TControlData2 = (
    ClassID: '{9BFF5271-2917-463D-8C77-9FB6D2B597A3}';
    EventIID: '{4AE163D0-B66E-45B9-A44A-6A8C634A66C2}';
    EventCount: 3;
    EventDispIDs: @CEventDispIDs;
    LicenseKey: nil (*HR:$80004005*);
    Flags: $00000000;
    Version: 401);
begin
  ControlData := @CControlData;
  TControlData2(CControlData).FirstEventOfs := Cardinal(@@FOnImageSizeInfo) - Cardinal(Self);
end;

procedure TSupPlate.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as _DSupPlate;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TSupPlate.GetControlInterface: _DSupPlate;
begin
  CreateControl;
  Result := FIntf;
end;

procedure TSupPlate.SendComOrder(lOrder: Integer);
begin
  DefaultInterface.SendComOrder(lOrder);
end;

procedure TSupPlate.AllowDetect(lOrder: Integer; bDetect: WordBool);
begin
  DefaultInterface.AllowDetect(lOrder, bDetect);
end;

procedure TSupPlate.RecogniseBuf(var lpImageBuf: Integer; nImgWidth: Integer; nImgHeight: Integer);
begin
  DefaultInterface.RecogniseBuf(lpImageBuf, nImgWidth, nImgHeight);
end;

function TSupPlate.GetDirRoadNum(lDirIndex: Integer): Integer;
begin
  Result := DefaultInterface.GetDirRoadNum(lDirIndex);
end;

procedure TSupPlate.TesttHandleBreak(lDirIndex: Integer; lRoadIndex: Integer);
begin
  DefaultInterface.TesttHandleBreak(lDirIndex, lRoadIndex);
end;

procedure TSupPlate.SetVideoDetectParam;
begin
  DefaultInterface.SetVideoDetectParam;
end;

function TSupPlate.GetPeccancyCount(lDirIndex: Integer; lRoadIndex: Integer): Integer;
begin
  Result := DefaultInterface.GetPeccancyCount(lDirIndex, lRoadIndex);
end;

function TSupPlate.GetSpeed(lDirIndex: Integer; lRoadIndex: Integer): Single;
begin
  Result := DefaultInterface.GetSpeed(lDirIndex, lRoadIndex);
end;

function TSupPlate.SetKinescopeStat(nOrder: Integer): Integer;
begin
  Result := DefaultInterface.SetKinescopeStat(nOrder);
end;

procedure TSupPlate.RecognisePicFile(const strPicFile: WideString);
begin
  DefaultInterface.RecognisePicFile(strPicFile);
end;

procedure TSupPlate.SetPlateMode(nPlateMode: Integer);
begin
  DefaultInterface.SetPlateMode(nPlateMode);
end;

function TSupPlate.GetDetectFlux(lDirIndex: Integer; lRoadIndex: Integer): Integer;
begin
  Result := DefaultInterface.GetDetectFlux(lDirIndex, lRoadIndex);
end;

function TSupPlate.GetDirNum: Integer;
begin
  Result := DefaultInterface.GetDirNum;
end;

function TSupPlate.SaveImageFile(const strSavePicFile: WideString; var lpSaveImageBuf: Integer; 
                                 nSaveImgW: Integer; nSaveImgH: Integer): Integer;
begin
  Result := DefaultInterface.SaveImageFile(strSavePicFile, lpSaveImageBuf, nSaveImgW, nSaveImgH);
end;

function TSupPlate.GetPlateImage(nPlateInndex: Integer; var lpPlateImageBuf: Integer; 
                                 nPlateImgW: Integer; nPlateImgH: Integer): Integer;
begin
  Result := DefaultInterface.GetPlateImage(nPlateInndex, lpPlateImageBuf, nPlateImgW, nPlateImgH);
end;

function TSupPlate.GetRecogImgWH(const strPicFile: WideString; var nImageW: Integer; 
                                 var nImageH: Integer): Integer;
begin
  Result := DefaultInterface.GetRecogImgWH(strPicFile, nImageW, nImageH);
end;

function TSupPlate.GetRecogImgBuf(const strPicFile: WideString; var lpImgBuf: Integer): Integer;
begin
  Result := DefaultInterface.GetRecogImgBuf(strPicFile, lpImgBuf);
end;

function TSupPlate.GetPlateNumber(nPlateIndex: Integer): WideString;
begin
  Result := DefaultInterface.GetPlateNumber(nPlateIndex);
end;

function TSupPlate.GetPlateColorNumber(nPlateIndex: Integer): Integer;
begin
  Result := DefaultInterface.GetPlateColorNumber(nPlateIndex);
end;

function TSupPlate.GetPlateColorString(nPlateIndex: Integer; nLanguageType: Integer): WideString;
begin
  Result := DefaultInterface.GetPlateColorString(nPlateIndex, nLanguageType);
end;

function TSupPlate.GetPlateRect(nPlateIndex: Integer; var nPlateLeft: Integer; 
                                var nPlateRight: Integer; var nPlateTop: Integer; 
                                var nPlateBottom: Integer): Integer;
begin
  Result := DefaultInterface.GetPlateRect(nPlateIndex, nPlateLeft, nPlateRight, nPlateTop, 
                                          nPlateBottom);
end;

function TSupPlate.GetNearImage(var lpNearImg: Integer): Integer;
begin
  Result := DefaultInterface.GetNearImage(lpNearImg);
end;

function TSupPlate.GetFarImage(nFarImgIndex: Integer; var lpFarImg: Integer): Integer;
begin
  Result := DefaultInterface.GetFarImage(nFarImgIndex, lpFarImg);
end;

function TSupPlate.DlgSetVideoDetectMode: Integer;
begin
  Result := DefaultInterface.DlgSetVideoDetectMode;
end;

procedure TSupPlate.RecogKinescope(const strKinescope: WideString);
begin
  DefaultInterface.RecogKinescope(strKinescope);
end;

procedure TSupPlate.OnOcrDBManage;
begin
  DefaultInterface.OnOcrDBManage;
end;

procedure TSupPlate.GetPassedCarInfo(const PassedCarInfo: IUnknown);
begin
  DefaultInterface.GetPassedCarInfo(PassedCarInfo);
end;

procedure TSupPlate.SetRecogParam;
begin
  DefaultInterface.SetRecogParam;
end;

procedure TSupPlate.RegistSupPlate;
begin
  DefaultInterface.RegistSupPlate;
end;

function TSupPlate.SetVideoDetectMode(nDetectMode: Integer): Integer;
begin
  Result := DefaultInterface.SetVideoDetectMode(nDetectMode);
end;

function TSupPlate.GetModulPath: WideString;
begin
  Result := DefaultInterface.GetModulPath;
end;

procedure TSupPlate.RecogniseCamera;
begin
  DefaultInterface.RecogniseCamera;
end;

procedure TSupPlate.SetCameraPin;
begin
  DefaultInterface.SetCameraPin;
end;

procedure TSupPlate.SetCameraFilter;
begin
  DefaultInterface.SetCameraFilter;
end;

procedure TSupPlate.FinishSetLightStat;
begin
  DefaultInterface.FinishSetLightStat;
end;

function TSupPlate.GetRoadLightStat(nDirIndex: Integer; nRoadIndex: Integer): Integer;
begin
  Result := DefaultInterface.GetRoadLightStat(nDirIndex, nRoadIndex);
end;

procedure TSupPlate.SetRoadLightStat(nDirIndex: Integer; nRoadIndex: Integer; bIsRedLight: Integer);
begin
  DefaultInterface.SetRoadLightStat(nDirIndex, nRoadIndex, bIsRedLight);
end;

function TSupPlate.GetDirRoadID(nDirIndex: Integer; var lDirID: Integer; nRoadIndex: Integer; 
                                var lRoadID: Integer): Integer;
begin
  Result := DefaultInterface.GetDirRoadID(nDirIndex, lDirID, nRoadIndex, lRoadID);
end;

procedure TSupPlate.RecogKinescopeRealTime(const strKinescope: WideString);
begin
  DefaultInterface.RecogKinescopeRealTime(strKinescope);
end;

procedure TSupPlate.KinescopeDetectCar(const strKinescope: WideString; nMode: Integer);
begin
  DefaultInterface.KinescopeDetectCar(strKinescope, nMode);
end;

procedure TSupPlate.SetRecogSourceCompressble(nCompress: Integer);
begin
  DefaultInterface.SetRecogSourceCompressble(nCompress);
end;

function TSupPlate.GetAreaRecogNum: Integer;
begin
  Result := DefaultInterface.GetAreaRecogNum;
end;

procedure TSupPlate.SetAreaRecogNum(nAreaNum: Integer);
begin
  DefaultInterface.SetAreaRecogNum(nAreaNum);
end;

procedure TSupPlate.AreaRecog(nAreaIndex: Integer);
begin
  DefaultInterface.AreaRecog(nAreaIndex);
end;

procedure TSupPlate.AreaRecogInput(nAreaIndex: Integer; dAreaLeft: Double; dAreaRight: Double; 
                                   dAreaTop: Double; dAreaBottom: Double);
begin
  DefaultInterface.AreaRecogInput(nAreaIndex, dAreaLeft, dAreaRight, dAreaTop, dAreaBottom);
end;

function TSupPlate.PutAreaRecogPic(const strPicFile: WideString): Integer;
begin
  Result := DefaultInterface.PutAreaRecogPic(strPicFile);
end;

function TSupPlate.PutAreaRecogBuf(var lpImageBuf: Integer; nImageW: Integer; nImageH: Integer): Integer;
begin
  Result := DefaultInterface.PutAreaRecogBuf(lpImageBuf, nImageW, nImageH);
end;

procedure TSupPlate.AreaRecogStart;
begin
  DefaultInterface.AreaRecogStart;
end;

procedure TSupPlate.DlgSetAreaRecog;
begin
  DefaultInterface.DlgSetAreaRecog;
end;

procedure TSupPlate.AboutBox;
begin
  DefaultInterface.AboutBox;
end;

procedure Register;
begin
  RegisterComponents(dtlOcxPage, [TSupPlate]);
end;

end.
