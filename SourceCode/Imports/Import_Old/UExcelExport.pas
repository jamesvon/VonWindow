(*******************************************************************************
  TExcelExport Excel文档报告输出类
================================================================================
  通过 RegTable(Prefix: string; ADataSet: TCustomADODataSet; AKind: EExportTableKind);
  注册查询结果和展现方式，系统支持多种数据展示方式。

  "ETK_ONLY" 单一显示数据源
  "ETK_ROWS" 多行填充数据源
  "ETK_COLS" 多列填充数据源
  "ETK_ROWS" 多行扩展数据源
  "ETK_COLS" 多列扩展数据源
  "ETK_PAGE" 多页显示数据源
  "ETK_RBLK" 母页行段数据源
  "ETK_CBLK" 母页行段数据源

  with TExcelExport.Create do try
    RegTable('ROW', ADOTable1, ETK_CBLK);
    RegTable('COL', ADOTable2, ETK_ECOL);
    New('F:\VonApplications\Test\A.xltx');
    WriteData;
    SaveFile();
  finally
    Free;
  end;
--------------------------------------------------------------------------------

*******************************************************************************)
unit UExcelExport;

interface

uses SysUtils, Variants, Classes, Graphics, Controls, OLE_Excel_TLB, ComObj,
  Forms, ADODB, DB, UVonLog;

type
  /// <summary>信息表类型</summary>
  /// <param name="ETK_ONLY">单一显示数据源</param>
  /// <param name="ETK_ROWS">多行填充数据源</param>
  /// <param name="ETK_COLS">多列填充数据源</param>
  /// <param name="ETK_EROW">多行扩展数据源</param>
  /// <param name="ETK_ECOL">多列扩展数据源</param>
  /// <param name="ETK_PAGE">多页显示数据源</param>
  /// <param name="ETK_RBLK">母页行段数据源</param>
  /// <param name="ETK_CBLK">母页行段数据源</param>
  EExportTableKind = (ETK_ONLY, ETK_ROWS, ETK_COLS, ETK_EROW, ETK_ECOL, ETK_PAGE, ETK_RBLK, ETK_CBLK);
  TExcelExport = class
  private
    FExcelApp: TExcelApplication;
    FExcelBook: TExcelWorkbook;
    FExcelSheet: TExcelWorksheet;
    FFilename: string;
    FRegistList: TStringList;
    FOpened: Boolean;
    FSetted: Boolean;
    procedure WriteONLYData(Prefix: string; ADataSet: TCustomADODataSet);
    procedure WriteROWSData(Prefix: string; ADataSet: TCustomADODataSet);
    procedure WriteCOLSData(Prefix: string; ADataSet: TCustomADODataSet);
    procedure WriteEROWData(Prefix: string; ADataSet: TCustomADODataSet);
    procedure WriteECOLData(Prefix: string; ADataSet: TCustomADODataSet);
    procedure WritePAGEData(Prefix: string; ADataSet: TCustomADODataSet);
    procedure WriteRBLKData(Prefix: string; ADataSet: TCustomADODataSet);
    procedure WriteCBLKData(Prefix: string; ADataSet: TCustomADODataSet);
    function GetCurrentSheet: TExcelWorksheet;
  public
    constructor Create;
    destructor Destroy;
    /// <summary>注册外部数据源</summary>
    /// <param name="Prefix">合并域前缀</param>
    /// <param name="ADataSet">数据源</param>
    /// <param name="AKind">信息表类型</param>
    procedure RegTable(Prefix: string; ADataSet: TCustomADODataSet; AKind: EExportTableKind);
    /// <summary>打开一个Word文档</summary>
    procedure Open(Filename: string);
    /// <summary>通过Word模板新建一个文件</summary>
    procedure New(Filename: string);
    /// <summary>打印当前文件</summary>
    procedure Print;
    /// <summary>另存当前文件，如文件名不写则表示用原文件名存储</summary>
    procedure SaveFile(Filename: string = '');
    /// <summary>关闭当前word文档</summary>
    procedure Close;
    /// <summary>开始写入注册数据</summary>
    procedure WriteData;
  published
    /// <summary>文件是否已经打开</summary>
    property Opened: Boolean read FOpened;
    /// <summary>数据源是否已经注册</summary>
    property Setted: Boolean read FSetted;
    /// <summary>当前工作表</summary>
    property CurrentSheet: TExcelWorksheet read GetCurrentSheet;
  end;

implementation

{ TExcelExport }

constructor TExcelExport.Create;
begin
  FExcelApp:= TExcelApplication.Create(nil);
  FExcelBook:= TExcelWorkBook.Create(nil);
  FExcelSheet:= TExcelWorksheet.Create(nil);
  FRegistList:= TStringList.Create;
end;

destructor TExcelExport.Destroy;
begin
  FRegistList.Free;
  FExcelApp.Quit;
  FExcelBook.Free;
  FExcelApp.Free;
end;

(* Operation *)

procedure TExcelExport.Close;
begin
  if not FOpened then Exit;
  FExcelBook.Close;
  FOpened:= False;
end;

procedure TExcelExport.New(Filename: string);
begin
  FFilename:= ChangeFilePath(ChangeFileExt(Filename, '.xls'),
    ExtractFilePath(Application.ExeName) + 'Temp\');
  FExcelApp.Connect;
  FExcelApp.Visible[0]:= True;
  FExcelBook.ConnectTo(FExcelApp.Workbooks.Add(Filename, 0));
  FOpened:= True;
end;

procedure TExcelExport.Open(Filename: string);
begin
  FFilename:= Filename;
  FExcelApp.Connect;
  FExcelApp.Visible[0]:= True;
  FExcelBook.ConnectTo(FExcelApp.Workbooks.Open(Filename, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, 0));
  FExcelSheet.ConnectTo(FExcelApp.ActiveSheet as _WorkSheet);
  FOpened:= True;
end;

procedure TExcelExport.Print;
begin
  if not FOpened then Exit;
  FExcelSheet.PrintPreview;
end;

procedure TExcelExport.SaveFile(Filename: string);
begin
  if not FOpened then Exit;
  if Filename = '' then Filename:= FFilename;
  CurrentSheet.SaveAs(Filename);
  FExcelApp.Disconnect;
end;

(* Properties *)

function TExcelExport.GetCurrentSheet: TExcelWorksheet;
begin
  FExcelSheet.ConnectTo(FExcelApp.ActiveSheet as _WorkSheet);
  Result:= FExcelSheet;
end;

(* Export *)

procedure TExcelExport.RegTable(Prefix: string; ADataSet: TCustomADODataSet;
  AKind: EExportTableKind);
begin
  FRegistList.AddObject(Prefix + '=' + IntToStr(Ord(AKind)), ADataSet);
  FSetted:= True;
end;

procedure TExcelExport.WriteData;
var
  I: Integer;
begin
  if not Opened then Exit;
  if not Setted then Exit;
  for I := 0 to FRegistList.Count - 1 do
    case EExportTableKind(StrToInt(FRegistList.ValueFromIndex[I])) of
    ETK_ONLY:  WriteONLYData(FRegistList.Names[I], TCustomADODataSet(FRegistList.Objects[I]));
    ETK_ROWS:  WriteROWSData(FRegistList.Names[I], TCustomADODataSet(FRegistList.Objects[I]));
    ETK_COLS:  WriteCOLSData(FRegistList.Names[I], TCustomADODataSet(FRegistList.Objects[I]));
    ETK_EROW:  WriteEROWData(FRegistList.Names[I], TCustomADODataSet(FRegistList.Objects[I]));
    ETK_ECOL:  WriteECOLData(FRegistList.Names[I], TCustomADODataSet(FRegistList.Objects[I]));
    ETK_PAGE:  WritePAGEData(FRegistList.Names[I], TCustomADODataSet(FRegistList.Objects[I]));
    ETK_RBLK:  WriteRBLKData(FRegistList.Names[I], TCustomADODataSet(FRegistList.Objects[I]));
    ETK_CBLK:  WriteCBLKData(FRegistList.Names[I], TCustomADODataSet(FRegistList.Objects[I]));
    end;
end;

procedure TExcelExport.WriteECOLData(Prefix: string; ADataSet: TCustomADODataSet);
var
  I, J, Idx: Integer;
  Expanded: Boolean;
begin
  Idx:= -1;
  with ADataSet do while not EOF do begin
    Expanded:= False;
    for I := 0 to Fields.Count - 1 do
      for J:= 1 to FExcelBook.Names.Count do
        if FExcelBook.Names.Item(J, EmptyParam, EmptyParam).Name_ = Prefix + '_' + Fields[I].FieldName then begin
          if not Expanded then begin
            Inc(Idx);
            Expanded:= True;
          end;
          if Idx > 0 then
            FExcelBook.Names.Item(J, EmptyParam, EmptyParam).RefersToRange.Offset[0, Idx].Insert(xlShiftToRight, xlFormatFromLeftOrAbove);
          FExcelBook.Names.Item(J, EmptyParam, EmptyParam).RefersToRange.Offset[0, Idx].Value2:= Fields[I].AsString;
        end;
    Next;
  end;
end;

procedure TExcelExport.WriteEROWData(Prefix: string; ADataSet: TCustomADODataSet);
var
  I, J, Idx, R: Integer;
  Expanded: Boolean;
begin
  Idx:= -1;
  with ADataSet do while not EOF do begin
    Expanded:= False;
    for I := 0 to Fields.Count - 1 do
      for J:= 1 to FExcelBook.Names.Count do
        if FExcelBook.Names.Item(J, EmptyParam, EmptyParam).Name_ = Prefix + '_' + Fields[I].FieldName then begin
          if not Expanded then begin
            Inc(Idx);
            Expanded:= True;
            if Idx > 0 then begin
              R:= FExcelBook.Names.Item(J, EmptyParam, EmptyParam).RefersToRange.Row;
              (FExcelApp.ActiveSheet as _Worksheet).Rows.Item[R + Idx, EmptyParam].Insert(xlShiftDown, xlFormatFromLeftOrAbove);
              (FExcelApp.ActiveSheet as _Worksheet).Rows.Item[R, EmptyParam].Copy((FExcelApp.ActiveSheet as _Worksheet).Rows.Item[R + Idx, EmptyParam]);
            end;
            //FExcelSheet.Rows.[J].Insert;//.Item[J + Idx, EmptyParam].Insert(xlShiftDown, xlFormatFromLeftOrAbove);
          end;
          FExcelBook.Names.Item(J, EmptyParam, EmptyParam).RefersToRange.Offset[Idx, 0].Value2:= Fields[I].AsString;
        end;
    Next;
  end;
end;

procedure TExcelExport.WriteRBLKData(Prefix: string; ADataSet: TCustomADODataSet);
var
  I, J, Idx: Integer;
  Expanded: Boolean;
  baseRange: ExcelRange;
begin
  Idx:= -1;
  FExcelSheet.ConnectTo(FExcelApp.ActiveSheet as _Worksheet);
  baseRange:= FExcelSheet.UsedRange[0];
  with ADataSet do while not EOF do begin
    Expanded:= False;
    for I := 0 to Fields.Count - 1 do
      for J:= 1 to FExcelBook.Names.Count do
        if FExcelBook.Names.Item(J, EmptyParam, EmptyParam).Name_ = Prefix + '_' + Fields[I].FieldName then begin
          if not Expanded then begin
            Inc(Idx);
            Expanded:= True;
          end;
          if Idx > 0 then
            baseRange.Copy(baseRange.Offset[Idx * baseRange.Rows.Count, 0]);
          FExcelBook.Names.Item(J, EmptyParam, EmptyParam).RefersToRange.Offset[Idx * baseRange.Rows.Count, 0].Value2:= Fields[I].AsString;
        end;
    Next;
  end;
end;

procedure TExcelExport.WriteCBLKData(Prefix: string; ADataSet: TCustomADODataSet);
var
  I, J, Idx: Integer;
  Expanded: Boolean;
  baseRange: ExcelRange;
begin
  Idx:= -1;
  FExcelSheet.ConnectTo(FExcelApp.ActiveSheet as _Worksheet);
  baseRange:= FExcelSheet.UsedRange[0];
  with ADataSet do while not EOF do begin
    Expanded:= False;
    for I := 0 to Fields.Count - 1 do
      for J:= 1 to FExcelBook.Names.Count do
        if FExcelBook.Names.Item(J, EmptyParam, EmptyParam).Name_ = Prefix + '_' + Fields[I].FieldName then begin
          if not Expanded then begin
            Inc(Idx);
            Expanded:= True;
          end;
          if Idx > 0 then
            baseRange.Copy(baseRange.Offset[0, Idx * baseRange.Columns.Count]);
          FExcelBook.Names.Item(J, EmptyParam, EmptyParam).RefersToRange.Offset[0, Idx * baseRange.Columns.Count].Value2:= Fields[I].AsString;
        end;
    Next;
  end;
end;

procedure TExcelExport.WriteCOLSData(Prefix: string; ADataSet: TCustomADODataSet);
var
  I, J, Idx: Integer;
  Expanded: Boolean;
begin
  Idx:= -1;
  with ADataSet do while not EOF do begin
    Expanded:= False;
    for I := 0 to Fields.Count - 1 do
      for J:= 1 to FExcelBook.Names.Count do
        if FExcelBook.Names.Item(J, EmptyParam, EmptyParam).Name_ = Prefix + '_' + Fields[I].FieldName then begin
          if not Expanded then begin
            Inc(Idx);
            Expanded:= True;
          end;
          FExcelBook.Names.Item(J, EmptyParam, EmptyParam).RefersToRange.Offset[0, Idx].Value2:= Fields[I].AsString;
        end;
    Next;
  end;
end;

procedure TExcelExport.WriteONLYData(Prefix: string; ADataSet: TCustomADODataSet);
var
  I, J: Integer;
begin
  with ADataSet do
    for I := 0 to Fields.Count - 1 do
      for J:= 1 to FExcelBook.Names.Count do
        if FExcelBook.Names.Item(J, EmptyParam, EmptyParam).Name_ = Prefix + '_' + Fields[I].FieldName then begin
          FExcelBook.Names.Item(J, EmptyParam, EmptyParam).RefersToRange.Value2:= Fields[I].AsString;
          Break;
        end;
end;

procedure TExcelExport.WritePAGEData(Prefix: string; ADataSet: TCustomADODataSet);
var
  I, J, Idx: Integer;
  Expanded: Boolean;
begin
  Idx:= -1;
  with ADataSet do while not EOF do begin
    Expanded:= False;
    for I := 0 to Fields.Count - 1 do
      for J:= 1 to FExcelBook.Names.Count do
        if FExcelBook.Names.Item(J, EmptyParam, EmptyParam).Name_ = Prefix + '_' + Fields[I].FieldName then begin
          if not Expanded then begin
            Inc(Idx);
            Expanded:= True;
          end;
          if Idx > 0 then begin
            //FExcelBook.Sheets.Copy();
          end;
          FExcelBook.Names.Item(J, EmptyParam, EmptyParam).RefersToRange.Offset[Idx, 0].Value2:= Fields[I].AsString;
        end;
    Next;
  end;
end;

procedure TExcelExport.WriteROWSData(Prefix: string; ADataSet: TCustomADODataSet);
var
  I, J, Idx: Integer;
  Expanded: Boolean;
begin
  Idx:= -1;
  with ADataSet do while not EOF do begin
    Expanded:= False;
    for I := 0 to Fields.Count - 1 do
      for J:= 1 to FExcelBook.Names.Count do
        if FExcelBook.Names.Item(J, EmptyParam, EmptyParam).Name_ = Prefix + '_' + Fields[I].FieldName then begin
          if not Expanded then begin
            Inc(Idx);
            Expanded:= True;
          end;
          FExcelBook.Names.Item(J, EmptyParam, EmptyParam).RefersToRange.Offset[Idx, 0].Value2:= Fields[I].AsString;
        end;
    Next;
  end;
end;

end.


