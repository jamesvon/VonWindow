unit UHKCamera;

interface

uses Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, IdURI,
  Vcl.Imaging.pngimage, Vcl.ExtCtrls, UVonVideoBase, UVonSystemFuns;

type
  THKCameraWin = class(TCameraWinBase)
  private
    procedure DisplayWithParam(Param: string);
  protected
    procedure DisplayHigh; override;
    procedure DisplayLower; override;
    procedure StopDisplay; override;
    procedure RecordVideo(Path: string); override;
    procedure StopRecord; override;
    procedure ShootingPicture(Interval: Integer; Path: string); override;
    procedure StopShooting; override;
  public
    class procedure InitWindows(WindowsCount: Integer; LogFilePath: string); override;
    class procedure ReleaseWindows; override;
    ///<summary>抓拍</summary>
    procedure Capture(Filename: string); override;
    ///<summary>云台动作</summary>
    procedure PZT(DIR: PAN_DIRECTION; Speed: Integer); override;
    ///<summary>拉近</summary>
    procedure ZoomIn(Speed: Integer); override;
    ///<summary>拉远</summary>
    procedure ZoomOut(Speed: Integer); override;
    ///<summary>聚焦-</summary>
    procedure FocusNear(Speed: Integer); override;
    ///<summary>聚焦+</summary>
    procedure FocusFar(Speed: Integer); override;
    ///<summary>光圈+</summary>
    procedure IrisOpen(Speed: Integer); override;
    ///<summary>光圈-</summary>
    procedure IrisClose(Speed: Integer); override;
  end;

implementation

{$region 'Import from DLL'}
const DLL_FILENAME = 'HK_PTZ.dll';
  //云台上仰
  HK_PAN_UP = 21;
  //云台下俯
  HK_PAN_DOWN = 22;
  // 云台左转
  HK_PAN_LEFT = 23;
  // 云台右转
  HK_PAN_RIGHT = 24;
  // 云台上仰和左转
  HK_PAN_UP_LEFT = 25;
  // 云台上仰和右转
  HK_PAN_UP_RIGHT = 26;
  // 云台下俯和左转
  HK_PAN_DOWN_LEFT = 27;
  // 云台下俯和右转
  HK_PAN_DOWN_RIGHT = 28;

  ///<summary>初始化</summary>
  function HK_Init(mediaCount: Integer; logFile: PAnsichar): Boolean; cdecl; external DLL_FILENAME name 'Init';
  ///<summary>结束使用，释放空间</summary>
  procedure HK_Release(); cdecl; external DLL_FILENAME name 'Release';
  ///<summary>打开视频</summary>
  function HK_Connect(mediaIdx: Integer; IP: PAnsichar; Port: Integer; UserName,
    PWD: PAnsichar; DisplayHandle: HWND; StreamType: Integer): Boolean; cdecl; external DLL_FILENAME name 'Connect';
  ///<summary>关闭视频</summary>
  procedure HK_Disconnect(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'Disconnect';

  ///<summary>聚焦</summary>
  procedure HK_ZoomIn(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'ZoomIn';
  ///<summary>开始聚焦</summary>
  procedure HK_ZoomInBegin(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'ZoomInBegin';
  ///<summary>结束聚焦</summary>
  procedure HK_ZoomInEnd(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'ZoomInEnd';
  ///<summary>远焦</summary>
  procedure HK_ZoomOut(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'ZoomOut';
  ///<summary>开始远焦</summary>
  procedure HK_ZoomOutBegin(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'ZoomOutBegin';
  ///<summary>结束远焦</summary>
  procedure HK_ZoomOutEnd(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'ZoomOutEnd';

  ///<summary>光圈变大</summary>
  procedure HK_IrisOpen(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'IrisOpen';
  ///<summary>开始光圈变大</summary>
  procedure HK_IrisOpenBegin(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'IrisOpenBegin';
  ///<summary>结束光圈变大</summary>
  procedure HK_IrisOpenEnd(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'IrisOpenEnd';
  ///<summary>光圈变小</summary>
  procedure HK_IrisClose(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'IrisClose';
  ///<summary>开始光圈变小</summary>
  procedure HK_IrisCloseBegin(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'IrisCloseBegin';
  ///<summary>结束光圈变小</summary>
  procedure HK_IrisCloseEnd(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'IrisCloseEnd';

  ///<summary>聚焦拉近</summary>
  procedure HK_FocusNear(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'FocusNear';
  ///<summary>开始聚焦拉近</summary>
  procedure HK_FocusNearBegin(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'FocusNearBegin';
  ///<summary>结束聚焦拉近</summary>
  procedure HK_FocusNearEnd(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'FocusNearEnd';
  ///<summary>聚焦拉远</summary>
  procedure HK_FocusFar(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'FocusFar';
  ///<summary>开始聚焦拉远</summary>
  procedure HK_FocusFarBegin(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'FocusFarBegin';
  ///<summary>结束聚焦拉远</summary>
  procedure HK_FocusFarEnd(mediaIdx: Integer); cdecl; external DLL_FILENAME name 'FocusFarEnd';

  ///<summary>云台转动</summary>
  procedure HK_PanMove(mediaIdx: Integer; Direction, Speed: Integer); cdecl; external DLL_FILENAME name 'PanMove';
  ///<summary>开始云台转动</summary>
  procedure HK_PanMoveBegin(mediaIdx: Integer; Direction, Speed: Integer); cdecl; external DLL_FILENAME name 'PanMoveBegin';
  ///<summary>结束云台转动</summary>
  procedure HK_PanMoveEnd(mediaIdx: Integer; Direction: Integer); cdecl; external DLL_FILENAME name 'PanMoveEnd';

  ///<summary>开始录像</summary>
  procedure HK_RecordVideo(mediaIdx: integer; filename: PAnsiChar); cdecl; external DLL_FILENAME name 'Record';
  ///<summary>停止录像</summary>
  procedure HK_StopRecord(mediaIdx: integer); cdecl; external DLL_FILENAME name 'StopRecord';
  ///<summary>开始抓拍</summary>
  procedure HK_Shooting(mediaIdx: integer; interval: integer; filename: PAnsiChar); cdecl; external DLL_FILENAME name 'Shooting';
  ///<summary>停止抓拍</summary>
  procedure HK_StopShooting(mediaIdx: integer); cdecl; external DLL_FILENAME name 'StopShooting';
  ///<summary>抓拍</summary>
  procedure HK_Capture(mediaIdx: integer; filename: PAnsiChar); cdecl; external DLL_FILENAME name 'Capture';
{$endregion}

{ THKCameraWin }

class procedure THKCameraWin.InitWindows(WindowsCount: Integer; LogFilePath: string);
var
  S: AnsiString;
begin
  inherited;
  S:= LogFilePath;
  if LogFilePath <> '' then HK_Init(WindowsCount, PAnsiChar(S))
  else HK_Init(WindowsCount, nil);
end;

class procedure THKCameraWin.ReleaseWindows;
begin
  inherited;
  HK_Release;
end;

procedure THKCameraWin.Capture(Filename: string);
var
  S: AnsiString;
begin
  inherited;
  S:= Filename;
  HK_Capture(WinIdx, PAnsiChar(S));
end;

procedure THKCameraWin.DisplayHigh;
begin
  inherited;
  DisplayWithParam(MachineSetting.Values['高清配置']);
  FIsHigh:= True;;
end;

procedure THKCameraWin.DisplayLower;
begin
  inherited;
  DisplayWithParam(MachineSetting.Values['标清配置']);
  FIsHigh:= False;
end;

procedure THKCameraWin.DisplayWithParam(Param: string);
var
  IP: AnsiString;
  Port: Integer;
  UserName, PWD: AnsiString;
  StreamType: Integer;
  lst: TStringList;
begin
  inherited;
  lst:= TStringList.Create;
  try
     lst.Delimiter:= ',';
     lst.DelimitedText:= Param;
     UserName:= lst.Values['Username'];
     PWD:= lst.Values['Password'];
     IP:= lst.Values['Host'];
     Port:= StrToInt(lst.Values['Port']);
     StreamType:= StrToInt(lst.Values['StreamType']);
  finally
    lst.Free;
  end;
  HK_Connect(WinIdx, PAnsiChar(IP), Port, PAnsiChar(UserName), PAnsiChar(PWD), Self.Handle, StreamType);
end;

procedure THKCameraWin.FocusFar(Speed: Integer);
begin
  inherited;
  if Speed > 0 then HK_FocusFarBegin(WinIdx)
  else HK_FocusFarEnd(WinIdx);
end;

procedure THKCameraWin.FocusNear(Speed: Integer);
begin
  inherited;
  if Speed > 0 then HK_FocusNearBegin(WinIdx)
  else HK_FocusNearEnd(WinIdx);
end;

procedure THKCameraWin.IrisClose(Speed: Integer);
begin
  inherited;
  if Speed > 0 then HK_IrisCloseBegin(WinIdx)
  else HK_IrisCloseEnd(WinIdx);
end;

procedure THKCameraWin.IrisOpen(Speed: Integer);
begin
  inherited;
  HK_IrisOpen(WinIdx)
end;

procedure THKCameraWin.PZT(DIR: PAN_DIRECTION; Speed: Integer);
begin
  inherited;
  case DIR of
  // 云台上仰
  PAN_DIR_UP: if Speed > 0 then HK_PanMoveBegin(WinIdx, HK_PAN_UP, Speed) else HK_PanMoveEnd(WinIdx, HK_PAN_UP);
  //云台下俯
  PAN_DIR_DOWN: if Speed > 0 then HK_PanMoveBegin(WinIdx, HK_PAN_DOWN, Speed) else HK_PanMoveEnd(WinIdx, HK_PAN_DOWN);
  // 云台左转
  PAN_DIR_LEFT: if Speed > 0 then HK_PanMoveBegin(WinIdx, HK_PAN_LEFT, Speed) else HK_PanMoveEnd(WinIdx, HK_PAN_LEFT);
  // 云台右转
  PAN_DIR_RIGHT: if Speed > 0 then HK_PanMoveBegin(WinIdx, HK_PAN_RIGHT, Speed) else HK_PanMoveEnd(WinIdx, HK_PAN_RIGHT);
  // 云台上仰和左转
  PAN_DIR_UP_LEFT: if Speed > 0 then HK_PanMoveBegin(WinIdx, HK_PAN_UP_LEFT, Speed) else HK_PanMoveEnd(WinIdx, HK_PAN_UP_LEFT);
  // 云台上仰和右转
  PAN_DIR_UP_RIGHT: if Speed > 0 then HK_PanMoveBegin(WinIdx, HK_PAN_UP_RIGHT, Speed) else HK_PanMoveEnd(WinIdx, HK_PAN_UP_RIGHT);
  // 云台下俯和左转
  PAN_DIR_DOWN_LEFT: if Speed > 0 then HK_PanMoveBegin(WinIdx, HK_PAN_DOWN_LEFT, Speed) else HK_PanMoveEnd(WinIdx, HK_PAN_DOWN_LEFT);
  // 云台下俯和右转
  PAN_DIR_DOWN_RIGHT: if Speed > 0 then HK_PanMoveBegin(WinIdx, HK_PAN_DOWN_RIGHT, Speed) else HK_PanMoveEnd(WinIdx, HK_PAN_DOWN_RIGHT);
  end;
end;

procedure THKCameraWin.RecordVideo(Path: string);
var
  S: AnsiString;
begin
  inherited;
  S:= Path;
  HK_RecordVideo(WinIdx, PAnsiChar(S));
  FRecorded:= True;
end;

procedure THKCameraWin.ShootingPicture(Interval: Integer; Path: string);
var
  S: AnsiString;
begin
  inherited;
  S:= Path;
  HK_Shooting(WinIdx, Interval, PAnsiChar(S));
  FShooting:= True;
end;

procedure THKCameraWin.StopDisplay;
begin
  inherited;
  HK_Disconnect(WinIdx);
end;

procedure THKCameraWin.StopRecord;
begin
  inherited;
  HK_StopRecord(WinIdx);
  FRecorded:= False;
end;

procedure THKCameraWin.StopShooting;
begin
  inherited;
  HK_StopShooting(WinIdx);
  FShooting:= False;
end;

procedure THKCameraWin.ZoomIn(Speed: Integer);
begin
  inherited;
  if Speed > 0 then HK_ZoomInBegin(WinIdx)
  else HK_ZoomInEnd(WinIdx);
end;

procedure THKCameraWin.ZoomOut(Speed: Integer);
begin
  inherited;
  if Speed > 0 then HK_ZoomOutBegin(WinIdx)
  else HK_ZoomOutEnd(WinIdx);
end;

initialization
  RegCameraWin('HKCameraWin', THKCameraWin);
  THKCameraWin.InitWindows(17, ExtractFilePath(Application.ExeName) + 'Log\HK_');
finalization
  THKCameraWin.ReleaseWindows;

end.
