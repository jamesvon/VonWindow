unit UIeWeb;

interface

uses
  WinAPI.Windows, WinAPI.Messages, SysUtils, Variants, Classes, Forms, Dialogs,
  System.Win.ComObj, SHDocVw, MSHTML;

type
  TVonIE = class
  private
    FURL: string;
    procedure SetURL(const Value: string);
  private
    FIE: Variant;
    WinHanlde: HWnd;
    procedure Refresh;
    function GetIDoc: IHTMLDOCUMENT2;
  public
    constructor Create(AURL: string);
  published
    property URL: string read FURL write SetURL;
    property IDoc: IHTMLDOCUMENT2 read GetIDoc;
  end;

implementation

{ TVonIE }

constructor TVonIE.Create(AURL: string);
begin
  URL := AURL;
end;

function TVonIE.GetIDoc: IHTMLDOCUMENT2;
var intfDisp: IDispatch;
begin
  Result := nil;
  if TVarData(FIE.Document).VType = varDispatch then
  begin
     intfDisp := IDispatch(TVarData(FIE.Document).VDispatch);
     intfDisp.QueryInterface(IHTMLDOCUMENT2, Result);
     intfDisp := nil;
  end;
end;
//var
//  doc: IHTMLDOCUMENT2;
//begin
//  doc :=  as IHTMLDOCUMENT2;
//  Result:= (doc);//(FIE.Document as IHTMLDOCUMENT2);
//end;

procedure TVonIE.Refresh;
begin
  if (VarIsEmpty(FIE)) then
  begin
    FIE := CreateOleObject('InternetExplorer.Application');
    FIE.Visible := true; // 可见
    FIE.left := 0;
    FIE.top := 0;
    FIE.width := 1024; // 高度
    FIE.height := 768; // 宽度
    FIE.menubar := true; // 取消菜单栏
    FIE.addressbar := true; // 取消地址栏
    FIE.toolbar := true; // 取消工具栏
    FIE.statusbar := true; // 取消状态栏
    FIE.resizable := true; // 不允许用户改变窗口大小
    FIE.Navigate(FURL);

    while FIE.Busy do
    begin
      Application.ProcessMessages;
    end;

    while FIE.ReadyState <> READYSTATE_COMPLETE do
    begin
      Application.ProcessMessages;
    end;
  end
  else
  begin
    WinHanlde := FindWIndow('IEFrame', nil);
    if (0 <> WinHanlde) then
    begin
      FIE.Navigate(FURL);
      SetForegroundWindow(WinHanlde);
    end
    else
      ShowMessage('Can''t open IE !');
  end;
end;

procedure TVonIE.SetURL(const Value: string);
begin
  if FUrl = Value then Exit;
  FURL := Value;
  Refresh;
end;

end.
