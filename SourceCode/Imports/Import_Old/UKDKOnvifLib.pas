unit UKDKOnvifLib;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics;

const
  KDK_DLL = 'KDKOnvifLib.dll';

//typedef struct chanalInfo
//{
//	int index;
//	char * profile;
//	int width;
//	int height;
//	char* url;
//};
//typedef struct cameraInfo
//{
//	int index;
//	char * addr;
//	char * EPAddres;
//	int chanalcount;
//	chanalInfo* chanals;
//};
//	///<summary>启动自动搜索功能，返回设备数量</summary>
//	_declspec(dllexport)int KDKOnvifDeviceCount();
//	///<summary>得到指定设备信息</summary>
//	_declspec(dllexport)cameraInfo* KDKOnvifDeviceCamera(int cameraIndex);
//	///<summary>得到指定设备码流信息</summary>
//	_declspec(dllexport)chanalInfo* KDKOnvifDeviceChanal(int cameraIndex, int chanalIndex);
//	///<summary>按照rtsp路径显示视频</summary>
//	_declspec(dllexport)int KDKOnvifDisplay(HWND handle, char * rtsp);
//	///<summary>关闭视频</summary>
//	_declspec(dllexport)void KDKOnvifStopDisplay(int displaybyURLindex);
//	///<summary>视频截图</summary>
//	_declspec(dllexport)void KDKOnvifSavePic(int displayIndex, char* filename);
//	///<summary>录像</summary>
//	_declspec(dllexport)void KDKOnvifRecord(int displaybyURLIndex, char* filename);
//	///<summary>停止录像</summary>
//	_declspec(dllexport)void KDKOnvifStopRecord(int displaybyURLIndex);
//	///<summary>初始化</summary>
//	_declspec(dllexport)int SDL_init();
//	///<summary>退出系统</summary>
//	_declspec(dllexport)void SDL_quit();

type
  POnvifChanalInfo = ^TOnvifChanalInfo;
  TOnvifChanalInfo = record
    index: Integer;
    profile: PAnsichar;
    width: Integer;
    height: Integer;
    url: PAnsichar;
  end;

  POnvifCameraInfo = ^TOnvifCameraInfo;
  TOnvifCameraInfo = record
    index: Integer;
    addr: PAnsichar;
    EPAddres: PAnsichar;
    chanalcount: Integer;
    chanal: array of POnvifChanalInfo;
  end;

/// <summary>初始化</summary>
function SDL_init(): Integer; cdecl; external KDK_DLL;
/// <summary>释放空间</summary>
procedure SDL_quit(); cdecl; external KDK_DLL;
/// <summary>得到设备数量</summary>
function KDKOnvifDeviceCount(): Integer; cdecl; external KDK_DLL;
/// <summary>得到设备摄像头信息</summary>
function KDKOnvifDeviceCamera(cameraIndex: Integer): POnvifCameraInfo; cdecl; external KDK_DLL;
/// <summary>得到设备通道码流信息</summary>
function KDKOnvifDeviceChanal(cameraIndex: Integer; chanalIndex: Integer): POnvifChanalInfo; cdecl; external KDK_DLL;
/// <summary>显示指定摄像头图像</summary>
function KDKOnvifDisplay(handle: HWND; rtsp: PAnsiChar): Integer; cdecl; external KDK_DLL;
/// <summary>关闭摄像头显示</summary>
procedure KDKOnvifStopDisplay(displayIndex: Integer); cdecl; external KDK_DLL;
/// <summary>视频截图</summary>
procedure KDKOnvifSavePic(displayIndex: Integer; filename: PAnsiChar); cdecl; external KDK_DLL;
/// <summary>开始录像</summary>
procedure KDKOnvifRecord(displayIndex: Integer; filename: PAnsiChar); cdecl; external KDK_DLL;
/// <summary>结束录像</summary>
procedure KDKOnvifStopRecord(displayIndex: Integer); cdecl; external KDK_DLL;

implementation

end.
