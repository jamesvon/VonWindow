unit UKDKUsbCameraLib;

interface

uses Winapi.Windows, System.Classes, Vcl.Controls, Vcl.Graphics, Vcl.ExtCtrls,
  UVonLog;

const
  DLL_FILE = 'KDKUsbCamera.dll';

  // typedef void(*FunToDisplay)(const int id, const unsigned char* data, int width, int height);
  /// //<summary>初始化</summary>
  // DLL_DECALEAR bool KDKUSBInit(FunToDisplay callback);
  /// //<summary>系统终止，结束侦听和心跳进程</summary>
  // DLL_DECALEAR void KDKUSBRelease();
  /// //<summary>系统终止，结束侦听和心跳进程</summary>
  // DLL_DECALEAR void KDKUSBClose(int id);
  /// // <summary>登录到服务器</summary>
  // DLL_DECALEAR bool KDKUSBDisplay(int id, int width, int height);
  /// // <summary>退出</summary>
  // DLL_DECALEAR void KDKUSBRecord(int id, char* path);
  /// // <summary>连接状态</summary>
  // DLL_DECALEAR bool KDKUSBStopRecord(int id);
type
  TFunToDisplay = procedure(id: Integer; data: Pointer; width: Integer;
    height: Integer) of object;
  PFunToDisplay = ^TFunToDisplay;
  // typedef void(*FunToDisplay)(const int id, const unsigned char* data, int width, int height);
  /// <summary>初始化</summary>
function KDKUSBInit(callback: PFunToDisplay): boolean; cdecl; external DLL_FILE;
/// <summary>系统终止，结束侦听和心跳进程</summary>
procedure KDKUSBRelease(); cdecl; external DLL_FILE;
/// <summary>系统终止，结束侦听和心跳进程</summary>
procedure KDKUSBClose(id: Integer); cdecl; external DLL_FILE;
/// <summary>退出</summary>
procedure KDKUSBRecord(id: Integer; path: PAnsichar); cdecl; external DLL_FILE;
/// <summary>连接状态</summary>
function KDKUSBStopRecord(id: Integer): boolean; cdecl; external DLL_FILE;
/// <summary>登录到服务器</summary>
function KDKUSBDisplay(id, width, height: Integer): boolean; cdecl;
  external DLL_FILE;
function KDKUSBOpen(id: Integer; hdc: THandle; width, height: Integer): boolean;
  cdecl; external DLL_FILE;

// _declspec(dllexport)int cameraInit(int cameracount);
// _declspec(dllexport)int cameraOpen(int cameraindex, int cameranum, int width, int height);
// _declspec(dllexport)int setcameraWidthHeight(int cameraindex, int width, int height);
function cameraInit(const cameracount: Integer): Integer; cdecl;
  external DLL_FILE;
function cameraOpen(const cameraindex, cameranum, width, heigh: Integer)
  : Integer; cdecl; external DLL_FILE;
function setcameraWidthHeight(const cameraindex, width, height: Integer)
  : Integer; cdecl; external DLL_FILE;

var
  VideoWidth, VideoHeight: Integer;

function OpenVideo(id: Integer): boolean;
procedure DisplayToPlanel(Bmp: TBitmap; plScreen: TPanel);

implementation

function OpenVideo(id: Integer): boolean;
begin
  Result := KDKUSBDisplay(id, VideoWidth, VideoHeight);
end;

procedure DisplayToPlanel(Bmp: TBitmap; plScreen: TPanel);
var
  PanelCanvas: TControlCanvas;
  R: TRect;
begin
  PanelCanvas := TControlCanvas.Create;
  PanelCanvas.Control := plScreen;
  PanelCanvas.Lock;
  R := Rect(0, 0, plScreen.width,
    Trunc(plScreen.width / Bmp.width * Bmp.height));
  if R.Bottom > plScreen.height then
    R := Rect(0, 0, Trunc(plScreen.height / Bmp.height * Bmp.width),
      plScreen.height);
  PanelCanvas.StretchDraw(R, Bmp);
  PanelCanvas.Unlock;
  PanelCanvas.Free;
end;

end.
