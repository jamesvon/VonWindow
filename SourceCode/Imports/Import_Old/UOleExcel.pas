unit UOleExcel;

interface

uses SysUtils, Variants, Classes, Graphics, Controls, OLE_Excel_TLB, ComObj,
  ADODB, DB, UVonLog;

type
  TOleExcel = class
  private
    FExcelApp: TExcelApplication;
    FExcelBook: TExcelWorkbook;
    FExcelSheet: TExcelWorksheet;
    FCurrentCell: ExcelRange;
    FTitle: string;
    FVisible: Boolean;
    FCurrentSheet: TExcelWorksheet;
    FOpend: boolean;
    function GetSheets(Index: Integer): _Worksheet;
    procedure SetTitle(const Value: string);
    procedure SetVisible(const Value: Boolean);
  public
    constructor Create;
    destructor Destroy;
    procedure Open(Filename: string);
    procedure New(TemplateFilename: string);
    procedure Close;
    procedure GotoSheet(Index: Integer);
    procedure AppendSheet;
    procedure CopySheetToNext(Index: Integer);
    procedure CopySheetToPrior(Index: Integer);
    property Sheets[Index: Integer]: _Worksheet read GetSheets;
  published
    property Visible: Boolean read FVisible write SetVisible;
    property Title: string read FTitle write SetTitle;
    property Opend: boolean read FOpend;
    property CurrentSheet: TExcelWorksheet read FCurrentSheet;
    property CurrentCell: ExcelRange read FCurrentCell;
  end;

implementation

{ TOleExcel }

procedure TOleExcel.AppendSheet;
var
  Temp_Worksheet: _WorkSheet;
begin
  Temp_Worksheet:= FExcelBook.WorkSheets.Add(EmptyParam,EmptyParam,EmptyParam,EmptyParam,0) as _WorkSheet;
  FExcelSheet.ConnectTo(Temp_WorkSheet);
End;

procedure TOleExcel.Close;
begin
  FExcelBook.Close;
end;

procedure TOleExcel.CopySheetToNext(Index: Integer);
begin

end;

procedure TOleExcel.CopySheetToPrior(Index: Integer);
begin

end;

constructor TOleExcel.Create;
begin
  FExcelApp:= TExcelApplication.Create(nil);
  FExcelApp.Connect;
  FExcelBook:= TExcelWorkbook.Create(nil);
  FExcelSheet:= TExcelWorksheet.Create(nil);
end;

destructor TOleExcel.Destroy;
begin
  FExcelSheet.Free;
  FExcelBook.Free;
  FExcelApp.Quit;
  FExcelApp.Free;
end;

function TOleExcel.GetSheets(Index: Integer): _Worksheet;
begin
  Result:= FExcelApp.WorkSheets.Item[Index] as _Worksheet;
end;

procedure TOleExcel.GotoSheet(Index: Integer);
begin
  FExcelSheet.ConnectTo(FExcelApp.WorkSheets.Item[Index] as _Worksheet);
  FExcelSheet.Activate;
end;

procedure TOleExcel.New(TemplateFilename: string);
begin
  FExcelBook.ConnectTo(FExcelApp.Workbooks.Add(TemplateFilename, 0));
end;

procedure TOleExcel.Open(Filename: string);
begin
  FExcelBook.ConnectTo(FExcelApp.Workbooks.Open(Filename, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, 0));
end;

procedure TOleExcel.SetTitle(const Value: string);
begin
  FTitle := Value;
  FExcelApp.Caption := FTitle;
end;

procedure TOleExcel.SetVisible(const Value: Boolean);
begin
  FVisible := Value;
  FExcelApp.Visible[0]:= FVisible;
end;

end.
