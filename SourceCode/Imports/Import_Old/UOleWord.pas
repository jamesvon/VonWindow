{Copyright:      Jamesvon  mailto:Jamesvon@163.COM
 Author:         James von
 Remarks:        freeware, but this Copyright must be included
 known Problems: none
 Version:        1.0
 Description:    The MS Word application for MS office

 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
}
unit UOleWord;

//{$DEFINE PASTE}
//{$DEFINE HTML}

interface

uses SysUtils, Classes, Variants, ComObj, Windows, OLE_Office_TLB, UReportWriter,
  Word_TLB;

type
  TOLEWord = class(TInterfacedObject, IOLEOffice)
  private
    FWordApp: TWordApplication;
    FWordDoc: _Document;
    FReportArea: string;
    FReportKind: string;
    FParagraphNames: TStringList;
    function GetLastRange: Rangee;
    function GetVisible: Boolean;
    procedure SetVisible(const Value: Boolean);
    function GetReportArea: string;
    procedure SetReportArea(const Value: string);
    function GetReportKind: string;
    procedure SetReportKind(const Value: string);
  public
    constructor Create();
    destructor Destroy; override;
  public
    procedure Open(templateFilename: string);
    procedure Save(Filename: string; DocAutoClose, AppAutoClose: Boolean);
    procedure GetParagraphNames(Values: TStrings);
    procedure Paste(Params: string);
    procedure WriteFolder(FolderName: string; FolderPart: integer);
    procedure WritePicture(AFilename: string);
    procedure WriteText(AStyle, AText: string);
    procedure WriteCRLF;
    procedure WriteParagraph(ParagraphName, AText: string);
    function  WriteChart(TypeKind, ChartWidth, ChartHeight: Integer; TypeValue, Control: string): IDispatch;
    procedure DoAnything(Flag: char; Value: string);
  published
    property Visible: Boolean read GetVisible write SetVisible;
    property ReportArea: string read GetReportArea write SetReportArea;
    property ReportKind: string read GetReportKind write SetReportKind;
  end;

implementation

{ TOLEWord }

constructor TOLEWord.Create;
begin
  FParagraphNames:= TStringList.Create;
  FWordApp:= TWordApplication.Create(nil);
  FWordApp.Visible:= True;
{  SaveChanges:= False;
  OriginalFormat:= EmptyParam;
  RouteDocument:= EmptyParam;
  for i:= 1 to FWordApp.Documents.Count do begin
    szIdx:= i;
    szDoc:= FWordApp.Documents.Item(szIdx);
    szDoc.Close(SaveChanges, OriginalFormat, RouteDocument);
  end; }
end;

function TOLEWord.WriteChart(TypeKind, ChartWidth, ChartHeight: Integer; TypeValue, Control: string): IDispatch;
var
  szInlineShape: InlineShape;
  ClassType, FileName, LinkToFile, DisplayAsIcon,
  IconFileName, IconIndex, IconLabel, Range: OleVariant;
  szPosition: Rangee;
begin    //创建图表
  (* Init Params *)
  ClassType:= 'MSGraph.Chart';
  Range:= '正文';
  FWordApp.Selection.Set_Style(Range);
  szPosition:= GetLastRange;
  FileName:= EmptyParam; LinkToFile:= False; DisplayAsIcon:= False;
  IconFileName:= EmptyParam; IconIndex:= EmptyParam; IconLabel:= EmptyParam;
  Range:= szPosition;
  (* Create chart object *)
  szInlineShape:= szPosition.InlineShapes.AddOLEObject(ClassType, FileName, LinkToFile,
    DisplayAsIcon, IconFileName, IconIndex, IconLabel, Range);
  //szInlineShape
  szInlineShape.Width:= ChartWidth;
  szInlineShape.Height:= ChartHeight;
  (* Convert Chart to Graph *)
  Result:= szInlineShape.OLEFormat.Object_;
  WriteLog(LOG_DEBUG, 'WriteChart', 'TypeKind=' + IntToStr(TypeKind) + ',CHARTTYPE=' + TypeValue);

  case TypeKind of                                              //Set chart type
  0: Chart(Result).ChartType:= StrToInt64(TypeValue);
  1: Chart(Result).ApplyCustomType(xlBuiltIn, TypeValue);
  2: Chart(Result).ApplyCustomType(xlUserDefined, TypeValue);
  end;  //*)
  szPosition.ParagraphFormat.Alignment:= wdAlignParagraphCenter;//Center
  GetLastRange.Select;
  if Control = 'Y' then WriteCRLF;
  GetLastRange.Select;
end;

destructor TOLEWord.Destroy;
begin
//  FWordDoc.Free;
  FWordApp.Free;
  FParagraphNames.Free;
  inherited;
end;

function TOLEWord.GetLastRange: Rangee;
var
  EndUnit, EndPos: OleVariant;
begin
{  EndPos := FWordDoc.Content.End_;
  if EndPos > 0 then
    Dec(EndPos);
  Result := FWordDoc.Range(EndPos, EndPos);        }
  EndUnit:= wdStory;
  EndPos:= EmptyParam;
  FWordApp.Selection.EndKey(EndUnit, EndPos);
  Result:= FWordApp.Selection.Range;
end;

procedure TOLEWord.GetParagraphNames(Values: TStrings);
begin     //得到模板中风格类型名称列表
  Values.Assign(FParagraphNames);
end;

function TOLEWord.GetVisible: Boolean;
begin
  Result:= FWordApp.Visible;
end;

procedure TOLEWord.DoAnything(Flag: char; Value: string);
var
  szUnit, szCount, szExtend: OleVariant;
  vl: Integer;
begin     //移动当前位置
  if not TryStrToInt(Value, vl) then vl:= 0;
  case Flag of
  'T': begin szUnit:= wdStory; FWordApp.Selection.HomeKey(szUnit, szExtend); end; //Move to top
  'U': begin szUnit:= wdLine; szCount:= vl;
       FWordApp.Selection.MoveUp(szUnit, szCount, szExtend); end; //Move up
  'P': begin szUnit:= wdParagraph; szCount:= vl;
       FWordApp.Selection.MoveUp(szUnit, szCount, szExtend); end; //Move up with Paragraph
  'G': begin szUnit:= wdParagraph; szCount:= vl;
       FWordApp.Selection.MoveDown(szUnit, szCount, szExtend); end; //Move down with Paragraph
  'D': begin szUnit:= wdLine; szCount:= vl;
       FWordApp.Selection.MoveDown(szUnit, szCount, szExtend); end; //Move down
  'B': begin szUnit:= wdStory; FWordApp.Selection.EndKey(szUnit, szExtend); end; //Move to bottom
  'H': begin szUnit:= wdLine; FWordApp.Selection.HomeKey(szUnit, szExtend); end; //Move home
  'L': begin szUnit:= wdCharacter; szCount:= vl;
       FWordApp.Selection.MoveLeft(szUnit, szCount, szExtend); end; //Move up
  'R': begin szUnit:= wdCharacter; szCount:= vl;
       FWordApp.Selection.MoveRight(szUnit, szCount, szExtend); end; //Move down
  'E': begin szUnit:= wdLine; FWordApp.Selection.EndKey(szUnit, szExtend); end; //Move end
  end;
end;

procedure TOLEWord.Open(templateFilename: string);
var
  oTemplate: OleVariant;
  oNewTemplate: OleVariant;
  oDocumentType: OleVariant;
  oVisible: OleVariant;
begin     //通过模板打开Word
  if templateFilename = '' then oTemplate:= ExtractFilePath(Application.ExeName) + '\Template\Report.dot'
  else oTemplate:= templateFilename;
  WriteLog(LOG_DEBUG, 'WriteWordReport', 'Open the template file "' + oTemplate + '".');
  oNewTemplate:= EmptyParam;
  oDocumentType:= EmptyParam;
  //oVisible:= False;
  oVisible:= True;
  FWordDoc:= FWordApp.Documents.Add(oTemplate, oNewTemplate, oDocumentType, oVisible);
  FWordDoc.AutoHyphenation:= False;
  FWordDoc.AutoFormatOverride:= False;
  FWordDoc.SpellingChecked:= False;
  FWordDoc.GrammarChecked:= False;
  GetLastRange.Select;         
end;

procedure TOLEWord.Paste(Params: string);
var
  szStart, szEnd, szRange, sz_Style: OleVariant;
begin     //粘贴
  szStart:= FWordDoc.Content.End_ - 1;
  szEnd:= FWordDoc.Content.End_ - 1;
  FWordDoc.Range(szStart, szEnd).Paste;
  szRange:= FWordApp.Selection.Tables.Item(1);
  if Params <> '' then begin
    sz_Style:= Params;
    szRange.Set_Style(sz_Style);
  end;
  GetLastRange.Select;
  szRange:= EmptyParam;
  FWordApp.ActiveDocument.UndoClear;
end;

procedure TOLEWord.Save(Filename: String; DocAutoClose, AppAutoClose: Boolean);
var
  szFN: OleVariant;
  szBasic: OleVariant;
begin     //保存并关闭
  if FWordDoc.TablesOfContents.Count > 0 then begin   //判断有无目录区域
    FWordDoc.TablesOfContents.Item(1).Update;         //更新目录
    szBasic:= FWordApp.WordBasic;      //WordBasic.UpdateTableOfContents
    szBasic.UpdateTableOfContents;
  end;
  if Filename = '' then Exit;
  szFN:= Filename;
  szBasic:= EmptyParam;
  FWordDoc.SaveAs2000(szFN, szBasic, szBasic, szBasic, szBasic, szBasic, szBasic, szBasic, szBasic, szBasic, szBasic);
  if not DocAutoClose then Exit;
  szFN:= True;
  FWordDoc.Close(szFN, szBasic, szBasic);
  if not AppAutoClose then Exit;
  FWordApp.Disconnect;
  FWordApp.Quit;
end;

procedure TOLEWord.SetVisible(const Value: Boolean);
begin
  FWordApp.Visible:= Value;
end;

procedure TOLEWord.WriteCRLF;
begin     //插入回车
  FWordApp.Selection.TypeParagraph;
  GetLastRange.Select;
end;

procedure TOLEWord.WriteData(AData: TOleDataPage; PageID: Integer; StyleName: string);
var
  DefaultTableBehavior: OleVariant;
  AutoFitBehavior: OleVariant;
  sz_Style: OleVariant;
  szTable: Table;
  ARow, ACol: Integer;
  CurrentData: TOleData;
begin
  DefaultTableBehavior:= False;
  AutoFitBehavior:= False;
  CurrentData:= AData.PageByIndex[PageID];
  szTable:= FWordDoc.Tables.Add(FWordApp.Selection.Range, CurrentData.RowCount,
    CurrentData.ColCount, DefaultTableBehavior, AutoFitBehavior);
  for ARow:= 1 to CurrentData.RowCount do
    for ACol:= 1 to CurrentData.ColCount do begin
      szTable.Cell(ARow, ACol).Select;
      FWordApp.Selection.Text:= CurrentData.Cells[ACol, ARow];
    end;
  if StyleName <> '' then begin
    sz_Style:= StyleName;
    szTable.Set_Style(sz_Style);
  end;
  GetLastRange.Select;
end;

procedure TOLEWord.WriteParagraph(ParagraphName, AText: string);
var
  szRange: OleVariant;
  szSelect: Rangee;
begin     //写入文字
  szSelect:= FWordApp.Selection.Range;    //myRange.SetRange Start:=myRange.Start, End:=Selection.End
  FWordApp.Selection.TypeText(AText);
  FWordApp.Selection.SetRange(szSelect.Start, FWordApp.Selection.Range.End_);
  szRange:= FWordApp.Selection;
  if ParagraphName <> '' then
    szRange.Style:= ParagraphName;
  GetLastRange.Select;
  FWordApp.Selection.TypeParagraph;       //FWordApp.Selection
  GetLastRange.Select;
end;  

procedure TOLEWord.WriteText(AStyle, AText: string);   
var
  szRange: OleVariant;
  szSelect: Rangee;
begin     //写入文字
  szSelect:= FWordApp.Selection.Range;    //myRange.SetRange Start:=myRange.Start, End:=Selection.End
  FWordApp.Selection.TypeText(AText);
  FWordApp.Selection.SetRange(szSelect.Start, FWordApp.Selection.Range.End_);
  szRange:= FWordApp.Selection;
  if AStyle <> '' then
    szRange.Style:= AStyle;
  GetLastRange.Select;
end;

procedure TOLEWord.WriteTable(StyleName: string; ASheet: TSpreadsheet; PageID, TableWidth: Integer);
type
  PColRow = ^TColRow;
  TColRow = record
    Col: Integer;
    Row: Integer;
  end;
var
  szSheetRang, szCell: _Range;
  szOleData, szOle1, szOle2, szOle3, sz_Style: OleVariant;
  szRC: PColRow;
  szTable: Table;
  szRow, szCol, szColMergeCount, szRowMergeCount: Integer;
  TitleHeigth: Single;
  MeegeList: TStringList;
  APage, i, j: Integer;
  widthRate: Double;
{$IFDEF HTML}
  szHTML: string;
  vonClip: TVonClipboard;
{$ENDIF}        

  procedure SetSheetData(const wordSel: WordSelection; const AValue: Variant; const ATxt: string);
  begin
    if VarIsNull(AValue) then wordSel.Text:= '-'
    else if VarIsStr(AValue)or VarIsNumeric(AValue) then wordSel.Text:= ATxt
    else wordSel.Text:= '';
  end;

  procedure DrawCellBorder(ABorder: Word_TLB.Border; SBorder: OWC11_TLB.Border);
  begin
    ABorder.Visible:= true; 
    case SBorder.Get_LineStyle  of    //.Get_LineStyle
    xlContinuous:  ABorder.LineStyle:= wdLineStyleSingle;//begin ABorder.Visible:= true; end; //ABorder.Weight:= SBorder.Get_Weight; ABorder.Style:= msoLineSingle; end;//
    xlDash:       ABorder.LineStyle:= wdLineStyleDashSmallGap;//begin ABorder.LineStyle:= wdLineStyleDashSmallGap; end;//.LineWidth:= SBorder.Get_Weight; end; //ABorder.Style:= msoLineThinThick; end;//
    xlDashDot:    ABorder.LineStyle:= wdLineStyleDashDot;//begin ABorder.LineWidth:= SBorder.Get_Weight; end; //ABorder.Style:= msoLineThickBetweenThin; end;//
    xlDashDotDot: ABorder.LineStyle:= wdLineStyleDashDotDot;//begin ABorder.LineWidth:= SBorder.Get_Weight; end; //ABorder.Style:= msoLineSingle; end;//
    xlDot:        ABorder.LineStyle:= wdLineStyleDot;//begin ABorder.LineWidth:= SBorder.Get_Weight; end; //ABorder.Style:= msoLineThickThin; end;//
    xlLineStyleNone:       ABorder.LineStyle:= wdLineStyleNone;//begin ABorder.Visible:= false; end;
    else begin
        ABorder.LineStyle:= wdLineStyleSingle;
        WriteLog(LOG_WARN, 'DrawCellBorder', 'There has found a new style of line, Style ID is ' + IntToHex(SBorder.Get_LineStyle, 8));
      end;
    end;
//    ABorder.ForeColor.RGB:= SBorder.Get_Color;
  end;
begin     //根据Sheet的内容粘贴进文档
  (ASheet.Sheets.Item[PageID] as WorkSheet).Activate;
  szSheetRang:= ASheet.ActiveSheet.UsedRange;
{$IFDEF HTML}
  szHTML:= ASheet.HTMLData;
  Delete(szHTML, 1, 5);
  szHTML:= '<HTML lang="GB2312" ' + szHTML;
  szHTML:= 'Version:0.9' + CrLf + 'StartHTML:-1' + CrLf + 'EndHTML:-1' + CrLf +
    'StartFragment:000081' + CrLf + 'EndFragment:' +
    Format('%.6d', [Length(szHTML) + 83]) + CrLf + szHTML + CrLf;
  vonClip:= TVonClipboard.Create;
  try
    vonClip.SetHtmlBuf(PAnsiChar(szHTML));
    FWordApp.Selection.Range.Paste;
    szTable:= FWordApp.ActiveDocument.Tables.Item(FWordApp.ActiveDocument.Tables.Count);
    szTable.PreferredWidthType:= wdPreferredWidthPoints;
    szTable.PreferredWidth:= TableWidth;
    szTable.Spacing:= -1;
    szTable.Rows.Alignment:= wdAlignRowCenter;
    GetLastRange.Select;
    WriteCRLF;
    GetLastRange.Select;
    vonClip.Clear;
  finally
    vonClip.Free;
  end;
  Exit;
{$ENDIF}
{$IFDEF PASTE}
  szSheetRang.Copy(EmptyParam);
  FWordApp.Selection.Range.Paste;
  szTable:= FWordApp.ActiveDocument.Tables.Item(FWordApp.ActiveDocument.Tables.Count);
  szTable.PreferredWidthType:= wdPreferredWidthPoints;
  szTable.PreferredWidth:= TableWidth;
  szTable.Spacing:= -1;
  szTable.Rows.Alignment:= wdAlignRowCenter;
  GetLastRange.Select;
  WriteCRLF;
  GetLastRange.Select;
  Clipboard.Clear;
  Exit;
{$ENDIF}
  MeegeList:= TStringList.Create;
  szOleData:= False;
  szTable:= FWordApp.ActiveDocument.Tables.Add(FWordApp.Selection.Range,
    ASheet.ActiveSheet.UsedRange.Rows.Count,
    ASheet.ActiveSheet.UsedRange.Columns.Count, szOleData, szOleData);
  szTable.PreferredWidthType:= wdPreferredWidthPoints;
  widthRate:= szTable.PreferredWidth / szSheetRang.Width;
  (* Styles *)
  szTable.Select;
  for szCol:= 1 to szSheetRang.Columns.Count do
    szTable.Columns.Item(szCol).Width:= szSheetRang.Cells.Item[1, szCol].Width * widthRate;
  for szRow:= 1 to szSheetRang.Rows.Count do begin
    szCol:= 1;
    (* Every Cell *)
    while szCol <= szSheetRang.Columns.Count do begin
      (* Merge *)
      szColMergeCount:= szSheetRang.Cells.Item[szRow, szCol].MergeArea.Columns.Count;
      szRowMergeCount:= szSheetRang.Cells.Item[szRow, szCol].MergeArea.Rows.Count;
      if MeegeList.IndexOf(IntToStr(szRow) + 'X' + IntToStr(szCol)) >= 0 then begin
        Inc(szCol);
        Continue;
      end;
      i:= MeegeList.IndexOf(IntToStr(szRow) + 'X' + IntToStr(szCol - 1));
      if i >= 0 then begin
        new(szRC);
        szRC.Col:= PColRow(MeegeList.Objects[i]).Col + 1; //得到上一个格的实际位置
        szRC.Row:= szRow;
      end else begin
        new(szRC);
        szRC.Col:= szCol;
        szRC.Row:= szRow;
      end;
      szTable.Cell(szRC.Row, szRC.Col).Select;
      szSheetRang.Cells.Item[szRow, szCol].Select;
      if(szColMergeCount > 1)or(szRowMergeCount > 1)then begin
        FWordApp.Selection.Cells.Item(1).Merge(szTable.Cell(szRC.Row + szRowMergeCount - 1, szRC.Col + szColMergeCount - 1));   //合并
        for i:= szCol to szCol + szColMergeCount - 1 do
          for j:= szRow to szRow + szRowMergeCount - 1 do
            MeegeList.AddObject(IntToStr(j) + 'X' + IntToStr(i), TObject(szRC));       
      end else begin
        MeegeList.AddObject(IntToStr(szRow) + 'X' + IntToStr(szCol), TObject(szRC));
      end;       
      (* Border and Text and font *)
      szTable.Cell(szRC.Row, szRC.Col).Select;
      if szRC.Row = 1 then
        DrawCellBorder(FWordApp.Selection.Borders.Item(wdBorderTop), ASheet.Selection.Borders.Item[xlEdgeTop]);
      if szRC.Col = 1 then
        DrawCellBorder(FWordApp.Selection.Borders.Item(wdBorderLeft), ASheet.Selection.Borders.Item[xlEdgeLeft]);
      DrawCellBorder(FWordApp.Selection.Borders.Item(wdBorderBottom), ASheet.Selection.Borders.Item[xlEdgeBottom]);
      DrawCellBorder(FWordApp.Selection.Borders.Item(wdBorderRight), ASheet.Selection.Borders.Item[xlEdgeRight]);
      SetSheetData(FWordApp.Selection, szSheetRang.Cells.Item[szRow, szCol].Value2, szSheetRang.Cells.Item[szRow, szCol].Text);
      //FWordApp.Selection.Text:= szSheetRang.Cells.Item[szRow, szCol].Text;//ASheet.Selection.Item[szRow, szCol].Text;//.Value2;
      FWordApp.Selection.Font.Name:= ASheet.Selection.Font.Get_Name;
      FWordApp.Selection.Font.Size:= ASheet.Selection.Font.Get_Size;
      FWordApp.Selection.Font.Bold:= ASheet.Selection.Font.Get_Bold;
      FWordApp.Selection.Font.Italic:= ASheet.Selection.Font.Get_Italic;
      (* Alignment *)
      case ASheet.Selection.Get_VerticalAlignment of
      xlVAlignCenter: FWordApp.Selection.Cells.VerticalAlignment:= wdCellAlignVerticalCenter;
      xlVAlignBottom: FWordApp.Selection.Cells.VerticalAlignment:= wdCellAlignVerticalBottom;
      xlVAlignTop   : FWordApp.Selection.Cells.VerticalAlignment:= wdCellAlignVerticalTop;
      end;
      case ASheet.Selection.Get_HorizontalAlignment of
      xlHAlignGeneral: FWordApp.Selection.ParagraphFormat.Alignment:= wdHorizontalLineAlignRight;
      xlHAlignCenter : FWordApp.Selection.ParagraphFormat.Alignment:= wdHorizontalLineAlignCenter;
      xlHAlignLeft   : FWordApp.Selection.ParagraphFormat.Alignment:= wdHorizontalLineAlignLeft;
      xlHAlignRight  : FWordApp.Selection.ParagraphFormat.Alignment:= wdHorizontalLineAlignRight;
      end;            
      FWordApp.Selection.ParagraphFormat.LineSpacingRule:= wdLineSpaceSingle;   //单倍行距
      FWordApp.Selection.Cells.Item(1).Shading.BackgroundPatternColor:= ASheet.Selection.Interior.Get_Color;
      inc(szCol);
    end;
  end;
  szTable.PreferredWidthType:= wdPreferredWidthPercent;
  szTable.PreferredWidth:= 100;   
  if StyleName <> '' then begin
    sz_Style:= StyleName;
    szTable.Set_Style(sz_Style);
  end;
  MeegeList.Free;
  GetLastRange.Select;
  WriteCRLF;
  GetLastRange.Select;
end;

procedure TOLEWord.WritePicture(AFilename: string);
var
  LinkToFile, SaveWithDocument, Range: OleVariant;
begin
  LinkToFile:= False;
  SaveWithDocument:= True;
  Range:= EmptyParam;
  FWordApp.Selection.InlineShapes.AddPicture(GetRealFileName(AFileName), LinkToFile, SaveWithDocument, Range);
  GetLastRange.Select;
end;

procedure TOLEWord.WriteFolder(FolderName: string; FolderPart: integer);
begin

end;

function TOLEWord.GetCalc: TOleDataCalc;
begin
  Result:= FCalc;
end;

procedure TOLEWord.SetCalc(const Value: TOleDataCalc);
begin
  FCalc:= Value;
end;

procedure TOLEWord.EndOfChart(aGraph: GraphApplication);
begin

end;

function TOLEWord.GetTaskInfo: TReportTaskInfo;
begin
  Result:= FTaskInfo;
end;

procedure TOLEWord.SetTaskInfo(const Value: TReportTaskInfo);
begin
  FTaskInfo:= Value;
end;

function TOLEWord.GetReportArea: string;
begin
  Result:= FReportArea;
end;

function TOLEWord.GetReportKind: string;
begin
  Result:= FReportKind;
end;

procedure TOLEWord.SetReportArea(const Value: string);
begin
  FReportArea:= Value;
end;

procedure TOLEWord.SetReportKind(const Value: string);
begin
  FReportKind:= Value;
end;

function TOLEWord.GetEventOfUserFunction: TCalc_DoUserFunction_Event;
begin
  Result:= UserFunEvent;
end;

procedure TOLEWord.UserFunEvent(FunctionName: string;
  CurrentCalcObject: TCalc_Unit; var Value: string);
begin     //添加一个标题
  if FunctionName = 'PARAGRAPH' then Value:= FunParagraphName(CurrentCalcObject)
  else raise Exception.Create('Not found the function ' + FunctionName);
end;

function TOLEWord.FunParagraphName(CurrentCalcObject: TCalc_Unit): string;
var
  szRange: OleVariant;
  szSelect: Rangee;
begin
  szSelect:= FWordApp.Selection.Range;    //myRange.SetRange Start:=myRange.Start, End:=Selection.End
  FWordApp.Selection.TypeText(CurrentCalcObject.Units[1].Value);
  FWordApp.Selection.SetRange(szSelect.Start, FWordApp.Selection.Range.End_);
  szRange:= FWordApp.Selection;
  if CurrentCalcObject.Units[0].Value <> '' then
    szRange.Style:= CurrentCalcObject.Units[0].Value;
  GetLastRange.Select;
end;

end.
