unit UPing;

interface

uses Winapi.Winsock, Winapi.Windows;

function ping(ip: string): Boolean;

implementation

type
//***************************************************************
  PIPOptionInformation = ^TIPOptionInformation;
  TIPOptionInformation = packed record
    TTL: Byte;
    TOS: Byte;
    Flags: Byte;
    OptionsSize: Byte;
    OptionsData: PChar;
  end;

  PIcmpEchoReply = ^TIcmpEchoReply;

  TIcmpEchoReply = packed record
    Address: DWORD;
    Status: DWORD;
    RTT: DWORD;
    DataSize: Word;
    Reserved: Word;
    Data: Pointer;
    Options: TIPOptionInformation;
  end;

  TIcmpCreateFile = function: THandle; stdcall;

  TIcmpCloseHandle = function(IcmpHandle: THandle): Boolean; stdcall;

  TIcmpSendEcho = function(IcmpHandle: THandle; DestinationAddress: //
  DWORD; RequestData: Pointer; RequestSize: Word; RequestOptions:  //
  PIPOptionInformation; ReplyBuffer: Pointer; ReplySize: DWord; Timeout: DWord): DWord; stdcall;

  Tping = class(Tobject)
    private
  { Private declarations }
      hICMP: THANDLE;
      hICMPdll: HMODULE;//新添加
      IcmpCreateFile: TIcmpCreateFile;
      IcmpCloseHandle: TIcmpCloseHandle;
      IcmpSendEcho: TIcmpSendEcho;
    public
//    procedure pinghost(ip: string; var info: string; var IsConnectedOk: Boolean);
    procedure pinghost(ip: string; var IsConnectedOk: Boolean);
    constructor create;
    destructor destroy; override;
  { Public declarations }
  end;

var
  VonPing: Tping;

function ping(ip: string): Boolean;
begin
  VonPing.pinghost(IP, Result);
end;

{ Tping  }

constructor Tping.create;
begin
  inherited create;
  hICMPdll := LoadLibrary('icmp.dll');
  @ICMPCreateFile := GetProcAddress(hICMPdll, 'IcmpCreateFile');
  @IcmpCloseHandle := GetProcAddress(hICMPdll, 'IcmpCloseHandle');
  @IcmpSendEcho := GetProcAddress(hICMPdll, 'IcmpSendEcho');
  hICMP := IcmpCreateFile;
end;

destructor Tping.destroy;
begin
  FreeLibrary(hIcmpDll);
  inherited destroy;
end;

procedure Tping.pinghost(ip: string; var IsConnectedOk: Boolean);
//procedure Tping.pinghost(ip: string; var info: string; var IsConnectedOk: Boolean);
var
// IP Options for packet to send
  IPOpt: TIPOptionInformation;
  FIPAddress: DWORD;
  pReqData, pRevData: PChar;
// ICMP Echo reply buffer
  pIPE: PIcmpEchoReply;
  FSize: DWORD;
  MyString: string;
  FTimeOut: DWORD;
  BufferSize: DWORD;
  isConnected: Integer;
begin
  if ip <> '' then begin
    // FIPAddress := inet_addr(PChar(ip));//Delphi 7
    IsConnectedOk := False;
    FIPAddress := inet_addr(PAnsiChar(AnsiString(ip)));
    isConnected := 0;
    FSize := 40;
    BufferSize := SizeOf(TICMPEchoReply) + FSize;
    GetMem(pRevData, FSize);
    GetMem(pIPE, BufferSize);
    FillChar(pIPE^, SizeOf(pIPE^), 0);
    pIPE^.Data := pRevData;
    MyString := 'Test Net – Sos Admin';
    pReqData := PChar(MyString);
    FillChar(IPOpt, Sizeof(IPOpt), 0);
    IPOpt.TTL := 64;
    FTimeOut := 1500;  //连接时间，1500MS 1.5S后停止
    try
      isConnected := IcmpSendEcho(hICMP, FIPAddress, pReqData, Length(MyString), //
                @IPOpt, pIPE, BufferSize, FTimeOut);
      if isConnected = 1 then begin
//        info := '连通';
        IsConnectedOk := True;
      end
      else begin
//        info := '不连通';
        IsConnectedOk := False;
      end;
//      info := info + ' ' + ip + ' ?time= ' + IntToStr(pIPE^.RTT);
// ?? ?if pReqData^ = pIPE^.Options.OptionsData^then
// ?? ? ?info := ip + ' ' + IntToStr(pIPE^.DataSize) + '　　　' + IntToStr(pIPE^.RTT);
    except
//      info := 'Can not find host!';
      FreeMem(pRevData);
      FreeMem(pIPE);
      Exit;
    end;
    FreeMem(pRevData);
    FreeMem(pIPE);
  end;
end;

initialization
  VonPing:= Tping.create;

finalization
  VonPing.Free;

end.
