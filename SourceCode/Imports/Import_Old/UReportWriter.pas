unit UReportWriter;

interface

uses
  Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ADODB, DB, DateUtils, UDB, Windows;

type      
  IOLEOffice = interface ['{9FF62916-4DD6-40C1-B224-35B02AE6852A}']
    procedure Open(TemplateFilename: string);
    procedure Save(Filename: string; DocAutoClose, AppAutoClose: Boolean);
    procedure GetParagraphNames(Values: TStrings);
    procedure WriteFolder(FolderName: string; FolderPart: Integer);
    procedure WritePicture(AFilename: string);
    procedure WriteCRLF;                       
    procedure Paste(Params: string);
    procedure WriteText(AStyle, AText: string);
    procedure WriteParagraph(ParagraphName, AText: string);
    function  WriteChart(TypeKind, ChartWidth, ChartHeight: Integer; TypeValue, Control: string): IDispatch;
    procedure DoAnything(Flag: char; Value: string);
    { Property procedures }
    procedure SetVisible(const Value: Boolean);
    function GetVisible: Boolean;
    procedure SetReportKind(const Value: string);
    function GetReportKind: string;
    procedure SetReportArea(const Value: string);
    function GetReportArea: string;
    { Property declarations }
    property Visible: Boolean read GetVisible write SetVisible;
    property ReportKind: string read GetReportKind write SetReportKind;
    property ReportArea: string read GetReportArea write SetReportArea;
  end;
  
  TReportWriter = class
  private
    { Private declarations }
    FUserDimNames: TStringList;     //默认维度名称
    FUserDimCodes: TStringList;     //默认维度编码
    FConnection: TADOConnection;    //数据库连接器
    FCurrentDimCodes: string;
    function GetUserDimCode(DimID: Integer): string;
    function GetUserDimName(DimID: Integer): string;
    procedure SetConnection(const Value: TADOConnection);
    procedure SetReportDate(const Value: TDate);
    procedure SetUserDimCode(DimID: Integer; const Value: string);
    procedure SetUserDimName(DimID: Integer; const Value: string);
    procedure SetCurrentDimCodes(const Value: string);
  private
    { Report parameter declarations }
    FReport: IOLEOffice;            //Office生成器实例
    FUserValues: TStringList;
    FReportDate: TDate;
    FReportYear, FReportMonth: Word;
    FDisplay: array[0..3]of boolean;  //0:Selected, 1:DisplayWord, 2:DisplayTable, 3:DisplayChart
    FCurrentFolder: Integer;
    FHasData: Boolean;
    FFolderPart: Integer;
    FTempList: TStringList;
    FCurrentRow, FCurrentCol: Integer;
    pi: Double;

    function WriteFolder: Boolean;
    procedure WriteWord;
    procedure WriteLink;
    procedure WriteData;
    procedure WriteTable;
    procedure WriteChart;
    procedure WritePicture;
    procedure WriteEvent;
    function GetCell(const ColOffset, RowOffSet: Integer): string;
    function GenteralSQL(SettingValue: string; DimID: Integer): TADOQuery;
    procedure SetSQLParams(AParams: TParameters);
    procedure MakePicture;                  
    procedure SetPictureFontAngle(Canvas: TCanvas; Angle: Double);
    procedure DrawPictureTitle(Canvas: TCanvas; R: Double; X0, Y0: Integer; Text: WideString);
    procedure DrawPictureData(Canvas: TCanvas; R, Angle: Double; X0, Y0: Integer; Text: WideString);
    procedure DrawPictureColumn(Canvas: TCanvas; R, Angle: Double; X0, Y0: Integer; Text: WideString);
    procedure MakeCockpit;
  public
    constructor Create(Report: IOLEOffice; TemplateName: string; RptDate: TDate);
    destructor  Destroy; override;
    function Writer(ReportID: Integer; TaskID: Integer = 0): Boolean;
    procedure SaveReport(Filename: string; DocAutoClose, AppAutoClose: Boolean);
    property UserDimName[DimID: Integer]: string read GetUserDimName write SetUserDimName;
    property UserDimCode[DimID: Integer]: string read GetUserDimCode write SetUserDimCode;
  published
    property ReportDate: TDate read FReportDate write SetReportDate;
    property Connection: TADOConnection read FConnection write SetConnection;
    property CurrentDimCodes: string read FCurrentDimCodes write SetCurrentDimCodes;
  end;

implementation

uses StrUtils, IniFiles, Math;

{ TReportWriter }

constructor TReportWriter.Create(Report: IOLEOffice; TemplateName: string; RptDate: TDate);
var
  DocFileName: string;
begin       //构造函数
  pi:= arcsin(1) * 2;
  DocFileName:= ExtractFilePath(Application.ExeName) + 'Template\' + TemplateName; //定义模板文件
  (* Init folder control *)
  FDisplay[0]:= True;  //0:Selected, 1:DisplayWord, 2:DisplayTable, 3:DisplayChart
  FDisplay[1]:= True;
  FDisplay[2]:= True;
  FDisplay[3]:= True;
  FFolderPart:= 0;
  (* Init variable *)
  FUserDimNames:= TStringList.Create;
  FUserDimCodes:= TStringList.Create;
  FUserValues:= TStringList.Create;
//  FModuleParams:= TStringList.Create;
  FDisplay[0]:= True;
  FDisplay[1]:= True;
  FDisplay[2]:= True;
  FDisplay[3]:= True;
  ReportDate:= RptDate;
  FReport.Open(DocFileName);            //打开模板，准备填写报告
end;

destructor TReportWriter.Destroy;
begin      //析构函数
//  FReportInfo.Free;
//  FTaskDataInfo.Free;
//  FTaskInfo.Free;
//  FCalc.Free;
//  FPage.Free;
//  FUserValues.Free;
//  FReportParams.Free;
//  FUserDimNames.Free;
//  FUserDimCodes.Free;
//  FSheet.Free;
//  FReport:= nil;
//  FTempList.Free;
//  FFolderSQL.Free;
//  FModuleParams.Free;
  inherited;
end;

function TReportWriter.Writer(ReportID: Integer; TaskID: Integer = 0): Boolean;
var
  AQuery: TADOQuery;
  doChildren: boolean;
  orgDisplay: array[0..3]of boolean;
  orgFolderID, orgPart: integer;
  orgFolderSQL: TConfigFile;
begin     //生成报告  ReportID: 报告序号  TaskID: 母任务序号，缺省值为0
  AQuery:= TADOQuery.Create(nil);
  orgFolderSQL:= TConfigFile.Create('');
  orgDisplay[0]:= True;
  orgDisplay[1]:= True;
  orgDisplay[2]:= True;
  orgDisplay[3]:= True;
  orgFolderID:= 0;
  orgPart:= 0;
  doChildren:= True;
  Result:= True;
  with AQuery do try   //创建报告内容查询SQL
    Connection:= FConnection;
    SQL.Text:= 'SELECT * FROM [RPT_ReportTask] WHERE [ReportID]=:RID AND [ParentID]=:PID ORDER BY [ParentID], [ListOrder]';
    Parameters[0].Value:= ReportID;
    Parameters[1].Value:= TaskID;
    Open;
    while not EOF do try
      if not FTaskInfo.LoadFromDB(AQuery)then
        raise Exception.Create('数据提取出现异常，' + LastInfo);
      FReport.TaskInfo:= FTaskInfo;
      FTaskDataInfo.ValueData:= ReportDate;
      FTaskDataInfo.TaskIdx:= TaskID;
      FTaskDataInfo.DimCodes:= CurrentDimCodes;
//      FHasData:= FTaskDataInfo.LoadFromDB;
      if FTaskInfo.Selected then begin
        case FTaskInfo.Kind of
        0: begin
            RecordDisplayStatus(FCurrentFolder, orgFolderID, FDisplay, orgDisplay, FFolderPart, orgPart, FFolderSQL, orgFolderSQL);//记录原始状态
            doChildren:= WriteFolder;
           end;
        1: if FDisplay[1] then WriteWord;
        2: WriteLink;
        3: WriteData;
        4: if FDisplay[2] then WriteTable;
        5: if FDisplay[3] then WriteChart;
        6: WritePicture;
        7: WriteEvent;
        end;
        if doChildren then
          Result:= Writer(ReportID, FTaskInfo.ID);
        if not Result then Exit;       //执行子节点
        if FTaskInfo.Kind = 0 then
          RecordDisplayStatus(orgFolderID, FCurrentFolder, orgDisplay, FDisplay, orgPart, FFolderPart, orgFolderSQL, FFolderSQL);   //将缓存状态恢复到原始状态
      end;
      Next;
    except
      on E: Exception do begin
        Result:= False;
        if MessageDlg(Format('系统在报告生成过程中,执行到序号为%d时出现异常，' + #13#10 + '%s' + #13#10 + '是否继续？',
          [FTaskInfo.ID, E.Message]),
          mtError, mbAbortIgnore, 0) <> mrIgnore then Exit;
      end;
    end;
  finally
    Free;
    orgFolderSQL.Free;
  end;
end;

procedure TReportWriter.SaveReport(Filename: string; DocAutoClose, AppAutoClose: Boolean);
begin     //存储报告
  if Filename = '' then FReport.Save(FReportInfo.ReportName, DocAutoClose, AppAutoClose)
  else FReport.Save(Filename, DocAutoClose, AppAutoClose);
end;

procedure TReportWriter.RecordDisplayStatus(
  CurrentFolderID: Integer; var RecordFolderID: Integer;
  CurrentStatus: array of boolean; var RecordStatus: array of boolean;
  CurrentFolderPart: integer; var RecordFolderPart: Integer;
  CurrentSQL: TConfigFile; var RecordSQL: TConfigFile);
begin     //还原或缓存状态   CurrentStatus：当前状态，RecordStatus：缓存状态
  RecordFolderID:= CurrentFolderID;
  RecordStatus[0]:= CurrentStatus[0];
  RecordStatus[1]:= CurrentStatus[1];
  RecordStatus[2]:= CurrentStatus[2];
  RecordStatus[3]:= CurrentStatus[3];
  RecordFolderPart:= CurrentFolderPart;
  RecordSQL.Text:= CurrentSQL.Text;
//  RecordParams.Text:= CurrentParams.Text;
end;

(* 目录 *)

function TReportWriter.WriteFolder: Boolean;
var
  ctrlValue: Cardinal;
  FModuleParams: TStringList;
begin
  FCurrentFolder:= FTaskInfo.ID;
  WriteLog(LOG_DEBUG, 'WriteFolder', '加载目录->' + FTaskInfo.NodeCaption);
  FModuleParams:= TStringList.Create;
  with TConfigFile.Create(FTaskInfo.Params)do try
    ReadSectionValues('VALUES', FModuleParams);
    Result:= not(FCalc.Calc('=' + ReadString('SYSTEM', 'CONDITION', '')) = '0');
    if not Result then Exit;
  finally
    FModuleParams.Free;
    Free;
  end;
  ctrlValue:= StrToInt(GetSettingValue(FTaskInfo.Settings, 'CONTROL', '0'));     //临时借用
  FDisplay[0]:= (ctrlValue and 1) = 0;    //0001
  FDisplay[1]:= (ctrlValue and 2) = 0;    //0010
  FDisplay[2]:= (ctrlValue and 4) = 0;    //0100
  FDisplay[3]:= (ctrlValue and 8) = 0;    //1000
  FFolderPart:= StrToInt(GetSettingValue(FTaskInfo.Settings, 'FOLDERPART', '0'));//本模块是否是独立章节
  FFolderSQL.Text:= FTaskInfo.Control;
  FReport.WriteFolder(FTaskInfo.NodeCaption, FFolderPart);
end;

(* 文字 *)

procedure TReportWriter.WriteWord;
begin     //写文字到报告
  WriteLog(LOG_DEBUG, 'WriteWord', '录入文字->(' + FTaskInfo.Control + ')' + FTaskInfo.Settings);
  if UpperCase(FTaskInfo.Params) = 'TRUE' then
    FReport.WriteParagraph(FTaskInfo.Control, FCalc.Calc(FTaskInfo.Settings))
  else
    FReport.WriteText(FTaskInfo.Control, FCalc.Calc(FTaskInfo.Settings));
end;

(* 引用 *)

procedure TReportWriter.WriteLink;
begin

end;

(* 数据准备 *)   

function TReportWriter.GenteralSQL(SettingValue: string;
  DimID: Integer): TADOQuery;
var      //生成维度扩展信息SQL
  AQuery: TADOQuery;
  i: Integer;
begin
  AQuery:= TADOQuery.Create(nil);
  AQuery.Connection:= FConnection;
  AQuery.SQL.Add('SELECT * FROM RPT_DimItems WHERE DimID=' + IntToStr(DimID));
  if Pos('=', SettingValue) > 0 then
    with GetSettingSubValues(SettingValue, ';') do try
      for i:= 0 to Count - 1 do begin
        if Strings[i] = '' then Continue;
        AQuery.SQL.Add('AND');
        if Copy(Names[i], 1, 8) = 'PROPERTY' then
          AQuery.SQL.Add(Names[i] + '=' + ValueFromIndex[i]);
        if Copy(Names[i], 1, 4) = 'KIND' then
          AQuery.SQL.Add(Names[i] + ' & ' + ValueFromIndex[i] + '>0');
      end;
    finally
      Free;
  end else if(SettingValue[Length(SettingValue)] = '_')or(SettingValue[Length(SettingValue)] = '%') then
    AQuery.SQL.Add('AND [Code] LIKE ''' + SettingValue + '''')
  else AQuery.SQL.Add('AND [Code]=''' + SettingValue + '''');
  Result:= AQuery;
end;

procedure TReportWriter.ExpandDim0(Page: TOLEData; Col, Row: Integer;
  Param1, Param2, Param3: string; YM: Char; Expand: integer);
var       //时间扩展
  Count, i, Value1, Value2: Integer;
  ADate: TDate;
  Fmt: string;
  data: array of string;

  procedure WriteDate;
  var
    j: Integer;
  begin
    case Expand of
    0: begin
        for j:= 0 to Count - 2 do begin      //水平扩展
          Page.InsertCol(Col + j);
          Page.Cells[Col + j, Row]:= data[j];
        end;
        Page.Cells[Col + Count - 1, Row]:= data[Count - 1];
      end;
    2: for j:= 0 to Count - 1 do            //水平填充
        Page.Cells[Col + j, Row]:= data[j];
    1: begin
        for j:= 0 to Count - 2 do begin
          Page.InsertRow(Row + j);            //垂直扩展
          Page.Cells[Col, Row + j]:= data[j];
        end;
        Page.Cells[Col, Row + Count - 1]:= data[Count - 1];
      end;
    3: for j:= 0 to Count - 1 do            //垂直填充
        Page.Cells[Col, Row + j]:= data[j];
    end;
  end;
begin
//YEAR(NowOffset,(+-)Num) / MONTH(NowOffset,(+-)JanOffset)             / SEASON(NowOffset,(+-)SeasonCount)
//YEAR(-2,3) | YEAR(0,-3) / MONTH(0,0) | MONTH(0,-1) | MONTH(0,-12)    / SEASON(0,3)
//=06,07,08  | =08,07,06  / 8..1       | 1..8        |07/1..08/1..08/8 / 3,2,1
//------------------------------------------------------------------------------
//QUERY(QUERYNAME)
  if not TryStrToInt(Param1, Value1)then Value1:= 0;
  if not TryStrToInt(Param2, Value2)then Value2:= 0;
  Count:= 0;
  case YM of
  'Y', 'y': begin            //Year expand
      if Param3 = '' then Fmt:= '%d' else Fmt:= Param3;
      Value1:= FReportYear - Value1;
      Count:= ABS(Value2);
      SetLength(data, Count);
      for i:= 0 to Count - 1 do
        if Param2[1] <> '-' then data[i]:= Format(Fmt, [Value1 + i])
        else data[i]:= Format(Fmt, [Value1 - i]);
      WriteDate;
    end;
  'M', 'm': begin            //Month expand
      if Param3 = '' then Fmt:= 'm' else Fmt:= Param3;
      Count:= ABS(MonthOf(FReportDate)- Value1 + Value2);
      if(Param2 = '')or(Param2[1] <> '-')then                                //StartMonth
        ADate:= IncMonth(FReportDate, - MonthOf(FReportDate) - Value2 + 1)
      else ADate:= IncMonth(FReportDate, - Value1);
      SetLength(data, Count);
      for i:= 0 to Count - 1 do
        if(Param2 = '')or(Param2[1] <> '-')then data[i]:= FormatDateTime(Fmt, IncMonth(ADate, i))
        else data[i]:= FormatDateTime(Fmt, IncMonth(ADate, -i));
      WriteDate;
    end;
  'S', 's': begin           //Season expand
      if Param3 = '' then Fmt:= '%d' else Fmt:= Param3;
      Count:= ABS(Value2);  //Season date of first date and back offset of season
      ADate:= IncMonth(FReportDate, Value1);
      if Count = 0 then Count:= MonthOf(ADate) div 3;
      if Count = 0 then Exit;
      ADate:= IncMonth(ADate, - (MonthOf(ADate) mod 3));
      if (Param2 <> '')and(Param2[1] = '-') then ADate:= IncMonth(ADate, - Count * 3 + 3); //StartMonth
      SetLength(data, Count);
      for i:= 0 to Count - 1 do
        if(Param2 <> '')and(Param2[1] <> '-')then
          data[i]:= Format(Fmt, [(MonthOf(IncMonth(ADate, i * 3)) - 1)div 3 + 1])
        else data[i]:= Format(Fmt, [MonthOf(IncMonth(ADate, -i * 3) - 1)div 3 + 1]);
      WriteDate;
    end;
    'Q', 'q': begin           //Query
      if not FFolderSQL.SectionExists(Param1)then begin
        WriteLog(LOG_ERROR, 'WriteData', Format(GetLanguageWord('REPORT_WRITEQUERYERROR',
          '序号为%d的报告中，%s查询语句未定义。'), [0, Param1]));
        Exit;
      end;
      with TADOQuery.Create(nil) do try
        Connection:= DM.ADOConn;
        FFolderSQL.ReadSectionValues(Param1, SQL);
        Prepared:= True;
        SetSQLParams(Parameters);
        WriteLog(LOG_DEBUG, 'WriteSQLData', Format(GetLanguageWord('REPORT_WRITEQUERY',
          '序号为%d的报告中，%s查询语句被执行。%s'), [0, Param1, SQL.Text]));
        Open;
        WriteLog(LOG_DEBUG, 'WriteSQLData', 'Found ' + IntToStr(RecordCount) + ' records.');
        Value1:= 0;
        while not EOF do begin
          case Expand of
          2: Page.InsertCol(Col + Value1);    //水平扩展
          3: Page.InsertRow(Row + Value1);    //垂直扩展
          end;
          for i:= 0 to Fields.Count - 1 do
            case Expand of
            0, 2: Page.Cells[Col + Value1, Row + i]:= Fields[i].AsString;  //水平填充
            1, 3: Page.Cells[Col + i, Row + Value1]:= Fields[i].AsString;  //垂直填充
            end;
          Inc(Value1);
          Next;
        end;     
        WriteLog(LOG_DEBUG, 'WriteSQLData', 'The data is '#13#10 + Page.DataText);
      finally
        Free;
      end;
    end;
  end;
  SetLength(data, 0);
end;

procedure TReportWriter.WriteData;
var       //执行数据准备
  i, j, k, APage, ExpandCount, DIMID, EndCol, EndRow: Integer;
  lstCtrl: TConfigFile;
  lst: TStringList;         
  Arrt: array[0..12]of string;
  Expand: integer;
  AColQuery, ARowQuery, AExeQuery: TADOQuery;
  AColInfo, ARowInfo: TReportItemInfo;
  orgCalcStatus: Boolean;
  FDoFilter, FLoop: Boolean;
  szDate: TDateTime;

  procedure ExecuteDataSQL();
  var
    k: Integer;
    fValue: Double;
    szData: string;
  begin
    with AExeQuery do begin
      WriteLog(LOG_DEBUG, 'ExecuteDataSQL', SQL.Text);
      Open;
      i:= 0;
      while not EOF do begin              //Loop records
        if i < RecordCount -1  then
          case AColInfo.Expand of
          0: FCurrentData.InsertCol(FCurrentCol + i);     //H and Expand
          1: FCurrentData.InsertRow(FCurrentRow + i);     //V and expand
          end;
        for k:= 0 to Fields.Count - 1 do begin //Loop fields
          szData:= Fields[k].AsString;
          if TryStrToFloat(szData, fValue) then
            szData:= Format(AColInfo.Fmt, [fValue * AColInfo.Rate]);
          case AColInfo.Expand of
          0, 2: FCurrentData.Cells[FCurrentCol + i, FCurrentRow + k]:= szData;   //H
          1, 3: FCurrentData.Cells[FCurrentCol + k, FCurrentRow + i]:= szData;   //V
          end;
        end;
        Inc(i);
        Next;
      end;
    end;
  end;

begin
  //提取数据
  orgCalcStatus:= FPage.AutoCalc;
  FPage.AutoCalc:= False;
  FPage.Text:= FTaskInfo.Settings;
  lstCtrl:= TConfigFile.Create(FTaskInfo.Control);     //Expand
  lst:= TStringList.Create;
  for APage:= 1 to FPage.Count do begin
    FCurrentData:= FPage.PageByIndex[APage];
    (* 提取扩展设定内容 *)
    lstCtrl.ReadSectionValues(IntToStr(APage), lst);   //Expand settings
    for j:= 0 to lst.Count - 1 do begin                //Loop ervery expand setting
      AnalyzeCellName(lst.Names[j], FCurrentCol, FCurrentRow);
      Expand:= StrToInt(GetSettingValue(lst.ValueFromIndex[j], '扩展', '0'));           //确定扩展方向
      ExpandCount:= StrToInt(GetSettingValue(lst.ValueFromIndex[j], '扩展数量', '0'));  //确定扩展数量
      DIMID:= StrToInt(GetSettingValue(lst.ValueFromIndex[j], 'DIM', '0'));             //得到要扩展的维度（0..10）0:year or month
      case DIMID of
      0: begin //Date //Year(0, 3)//Year(0, -3)//Month(0, <一月的偏差数>)
           i:= GetSettingArrayValue(lst.ValueFromIndex[j], 'DIMVALUE', Arrt);
           case i of
           1: ExpandDim0(FCurrentData, FCurrentCol, FCurrentRow, '', '', '', Arrt[0][1], Expand);
           2: ExpandDim0(FCurrentData, FCurrentCol, FCurrentRow, Arrt[1], '', '', Arrt[0][1], Expand);
           3: ExpandDim0(FCurrentData, FCurrentCol, FCurrentRow, Arrt[1], Arrt[2], '', Arrt[0][1], Expand);
           4: ExpandDim0(FCurrentData, FCurrentCol, FCurrentRow, Arrt[1], Arrt[2], Arrt[3], Arrt[0][1], Expand);
           else raise Exception.Create('数据准备[' + FTaskInfo.NodeCaption +
             '][' + lst.Strings[j] + ']扩展设置参数数量有误。' + #13#10 +  
             'DIMVALUE(QUERY,<QueryName>)' + #13#10 +
             'DIMVALUE(YEAR,<NowOffset>,(+-)<Num>,[<Format>])' + #13#10 +
             'DIMVALUE(MONTH,<NowOffset>,(+-)<JanOffset>,[<Format>])' + #13#10 +
             'DIMVALUE(SEASON,<NowOffset>,(+-)<SeasonCount>,[<Format>])');
           end;
        end;
      else with GenteralSQL(GetSettingValue(lst.ValueFromIndex[j], 'DIMVALUE', ''), DIMID) do try
          Open;
          i:= 0;
          while not EOF do begin
            case Expand of
            0: begin
                FCurrentData.InsertCol(FCurrentCol + i);
                FCurrentData.Cells[FCurrentCol + i, FCurrentRow]:= Fields[3].AsString;
              end;
            1: begin
                FCurrentData.InsertRow(FCurrentRow + i);
                FCurrentData.Cells[FCurrentCol, FCurrentRow + i]:= Fields[3].AsString;
              end;
            2: FCurrentData.Cells[FCurrentCol + i, FCurrentRow]:= Fields[3].AsString;
            3: FCurrentData.Cells[FCurrentCol, FCurrentRow + i]:= Fields[3].AsString;
            end;
            Inc(i);
            Next;
          end;
        finally
          Free;
        end;
      end;
    end;
  end;
  (* 填写数据 *)
  AColQuery:= TADOQuery.Create(nil);
  ARowQuery:= TADOQuery.Create(nil);
  AExeQuery:= TADOQuery.Create(nil);
  AColQuery.Connection:= FConnection;
  ARowQuery.Connection:= FConnection;
  AExeQuery.Connection:= FConnection;
  AColInfo:= TReportItemInfo.Create;
  ARowInfo:= TReportItemInfo.Create;
  AColInfo.Calc:= FCalc;
  try
    for APage:= 1 to FPage.Count do begin
      FCurrentData:= FPage.PageByIndex[APage];
      (* Row and column settings *)
      AColQuery.Close;
      ARowQuery.Close;
      AColQuery.SQL.Text:= 'SELECT * FROM [RPT_ReportItem] WHERE [TaskID]=:TID AND [PageID]=:PID AND [Row] = 0 AND [Col] > 0';
      ARowQuery.SQL.Text:= 'SELECT * FROM [RPT_ReportItem] WHERE [TaskID]=:TID AND [PageID]=:PID AND [Row] > 0 AND [Col] = 0';
      AColQuery.Parameters[0].Value:= FTaskInfo.ID;
      ARowQuery.Parameters[0].Value:= FTaskInfo.ID;
      AColQuery.Parameters[1].Value:= APage;
      ARowQuery.Parameters[1].Value:= APage;
      AColQuery.Open;
      ARowQuery.Open;
      while not ARowQuery.Eof do begin        //Loop rows
        if not ARowInfo.LoadFromDB(ARowQuery)then
          raise Exception.Create('行设置信息读取失败，请检查您的设置！' + #13#10 + LastInfo);
        FCurrentRow:= ARowInfo.Row;
        FCurrentData.CurrentRow:= AColInfo.Row;
        AColQuery.First;
        while not AColQuery.Eof do begin      //Loop columns
          if not AColInfo.LoadFromDB(AColQuery)then
            raise Exception.Create('列设置信息读取失败，请检查您的设置！' + #13#10 + LastInfo);
          FCurrentCol:= AColInfo.Col;
          FCurrentData.CurrentCol:= AColInfo.Col;
          for i:= 1 to 10 do AColInfo.DimValue[i]:= AColInfo.DimValue[i] + ARowInfo.DimValue[i];      //合并行列设置
          if not AColInfo.GenteralSQL(AExeQuery, FUserDimCodes, FReportDate)then
            raise Exception.Create('行列设置信息有矛盾，请检查您的设置！' + #13#10 + LastInfo);
          ExecuteDataSQL;
          AColQuery.Next;
        end;
        ARowQuery.Next;
      end;
      (* Cell settings *)
      AColQuery.Close;
      AColQuery.SQL.Text:= 'SELECT * FROM [RPT_ReportItem] WHERE [TaskID]=:TID AND [PageID]=:PID AND [Row] > 0 AND [Col] > 0';
      AColQuery.Parameters[0].Value:= FTaskInfo.ID;
      AColQuery.Parameters[1].Value:= APage;
      AColQuery.Open;
      while not AColQuery.EOF do begin
        if not AColInfo.LoadFromDB(AColQuery)then
          raise Exception.Create('单元格设置信息读取失败，请检查您的设置！' + #13#10 + LastInfo);
        FCurrentRow:= AColInfo.Row;
        FCurrentCol:= AColInfo.Col;
        FCurrentData.CurrentRow:= AColInfo.Row;
        FCurrentData.CurrentCol:= AColInfo.Col;
        FLoop:= True;
        szDate:= FReportDate;
        while FLoop do begin   
          FCurrentData.CurrentRow:= FCurrentRow;
          FCurrentData.CurrentCol:= FCurrentCol;
          FLoop:= False;
          //指定月份(0回溯，1分月，2分季，1..12月3..14，列提取，行提取，本月内)
          case AColInfo.Define of
          15: begin
              FCurrentData.AutoCalc:= true;
              FLoop:= TryStrToDate(FCurrentData.Cells[FCurrentCol + AColInfo.Offset, FCurrentRow], szDate);
              WriteLog(LOG_DEBUG, 'COL_Expand', FCurrentData.Cells[FCurrentCol + AColInfo.Offset, FCurrentRow] + ' -> Date is ' + BoolToStr(FLoop, true));
              if not Floop then Break;
              if AColInfo.Expand < 2 then AColInfo.Expand:= 2 + AColInfo.Expand;
              FCurrentData.AutoCalc:= False;
            end;
          16: begin
              FCurrentData.AutoCalc:= true;
              FLoop:= TryStrToDate(FCurrentData.Cells[FCurrentCol, FCurrentRow + AColInfo.Offset], szDate);
              WriteLog(LOG_DEBUG, 'ROW_Expand', FCurrentData.Cells[FCurrentCol, FCurrentRow + AColInfo.Offset] + ' -> Date is ' + BoolToStr(FLoop, true));
              if not Floop then Break;
              if AColInfo.Expand < 2 then AColInfo.Expand:= 2 + AColInfo.Expand;
              FCurrentData.AutoCalc:= False;
            end;
          end;
          if not AColInfo.GenteralSQL(AExeQuery, FUserDimCodes, szDate)then
            raise Exception.Create('行列设置信息有矛盾，请检查您的设置！' + #13#10 + LastInfo);
          ExecuteDataSQL;
          case AColInfo.Define of
          15: Inc(FCurrentRow);
          16: Inc(FCurrentCol);
          end;
        end;
        AColQuery.Next;
      end;
    end;
  finally
    AColInfo.Free;;
    ARowInfo.Free;
    AExeQuery.Free;
    AColQuery.Free;
    ARowQuery.Free;
    FPage.AutoCalc:= orgCalcStatus;
  end;
  FDoFilter:= lstCtrl.ReadBool('DoOrder', 'FilterIsFirst', true);
  for k:= 0 to 1 do begin
    if FDoFilter then
    (* 筛选 *)
      for APage:= 1 to FPage.Count do begin
        FCurrentData:= FPage.PageByIndex[APage];
        lstCtrl.ReadSectionValues('FILTER' + IntToStr(APage), lst);   //Filter settings
        for i:= 1 to lst.Count do
          with GetSettingSubValues(lst.Values[IntToStr(i)], ',')do try
            if Strings[3] = '行筛选' then
              FCurrentData.FilterRow(StrToInt(Strings[2]), StrToInt(Strings[4]),
                StrToInt(Strings[0]), StrToInt(Strings[1]), Strings[5])
            else
              FCurrentData.FilterCol(StrToInt(Strings[2]), StrToInt(Strings[4]),
                StrToInt(Strings[0]), StrToInt(Strings[1]), Strings[5]);
          finally
            Free;
          end;
      end
    (* 排序 *)
    else for APage:= 1 to FPage.Count do begin
      FCurrentData:= FPage.PageByIndex[APage];
      lstCtrl.ReadSectionValues('SORT' + IntToStr(APage), lst);   //Sort settings
      for i:= 1 to lst.Count do
        with GetSettingSubValues(lst.Values[IntToStr(i)], ',')do try
          if Strings[6] = '升序' then j:= 0 else j:= 1;
          if Strings[4] = '行排序' then
            FCurrentData.SortRow(StrToInt(Strings[5]), StrToInt(Strings[0]),
              StrToInt(Strings[1]), StrToInt(Strings[2]), StrToInt(Strings[3]), j)
          else FCurrentData.SortCol(StrToInt(Strings[5]), StrToInt(Strings[0]),
              StrToInt(Strings[1]), StrToInt(Strings[2]), StrToInt(Strings[3]), j);
        finally
          Free;
        end;
    end;
    FDoFilter:= not FDoFilter;
  end;
  lst.Free;
end;

(* 表格 *)

procedure TReportWriter.ExpandDim0(Sheet: WorkSheet; Col, Row: Integer;
  Param1, Param2, Param3: string; YM: Char; Expand: integer);
var       //时间扩展
  Count, i, Value1, Value2: Integer;
  ADate: TDate;
  Fmt: string;
  data: array of string;
  shiftVar: OleVariant;

  procedure WriteDate;
  var
    j: Integer;
  begin
    case Expand of
    0: for j:= 0 to Count - 1 do            //水平填充
        Sheet.Cells.Item[Row, Col + j].Value2:= data[j];
    1: for j:= 0 to Count - 1 do            //垂直填充
        Sheet.Cells.Item[Row + j, Col].Value2:= data[j];
    2: for j:= 0 to Count - 1 do begin      //水平扩展
        shiftVar:= xlShiftToRight;
        if j > 0 then Sheet.Columns.Item[EmptyParam, Col].Insert(shiftVar);
        Sheet.Cells.Item[Row, Col + j].Value2:= data[j];
      end;
    3: for j:= 0 to Count - 1 do begin      //垂直扩展
        shiftVar:= xlShiftDown;
        if j > 0 then Sheet.Rows.Item[Row, EmptyParam].Insert(shiftVar);
        Sheet.Cells.Item[Row + j, Col].Value2:= data[j];
      end;
    end;
  end;
begin
//YEAR(NowOffset,(+-)Num) / MONTH(NowOffset,(+-)JanOffset)             / SEASON(NowOffset,(+-)SeasonOffset)
//YEAR(-2,3) | YEAR(0,-3) / MONTH(0,0) | MONTH(0,-1) | MONTH(0,-12)    / SEASON(0,1)
//=06,07,08  | =08,07,06  / 8..1       | 1..8        |07/1..08/1..08/8 / 3,2,1
  TryStrToInt(Param1, Value1);
  TryStrToInt(Param2, Value2);
  Count:= 0;
  case YM of
  'Y': begin            //Year expand
      if Param3 = '' then Fmt:= '%d' else Fmt:= Param3;
      Value1:= FReportYear - Value1;
      Count:= ABS(Value2);
      SetLength(data, Count);
      for i:= 0 to Count - 1 do
        if Param2[1] <> '-' then data[i]:= Format(Fmt, [Value1 + i])
        else data[i]:= Format(Fmt, [Value1 - i]);
      WriteDate;
    end;
  'M': begin            //Month expand
      if Param3 = '' then Fmt:= 'm' else Fmt:= Param3;
      Count:= MonthsBetween(IncMonth(FReportDate, - Value1),
        IncMonth(FReportDate, - MonthOf(FReportDate) - Value2));  //NowOffset-JanOffset
      if Param2[1] <> '-' then                                //StartMonth
        ADate:= IncMonth(FReportDate, - MonthOf(FReportDate) - Value2 + 1)
      else ADate:= IncMonth(FReportDate, - Value1);
      SetLength(data, Count);
      for i:= 0 to Count - 1 do
        if Param2[1] <> '-' then data[i]:= FormatDateTime(Fmt, IncMonth(ADate, i))
        else data[i]:= FormatDateTime(Fmt, IncMonth(ADate, -i));
      WriteDate;
    end;
  'S': begin            //Season expand
      if Param3 = '' then Fmt:= '%d' else Fmt:= Param3;
      Count:= MonthsBetween(IncMonth(FReportDate, - (MonthOf(FReportDate) mod 3) + Value1 * 3),
        IncMonth(FReportDate, - MonthOf(FReportDate) + Value2 * 3)) div 3;//Season date of first date and back offset of season
      if Param2[1] <> '-' then                                //StartMonth
        ADate:= IncMonth(FReportDate, - (MonthOf(FReportDate) mod 3) + Value2 * 3 + 3)
      else ADate:= IncMonth(FReportDate, - MonthOf(FReportDate) + Value2 * 3 + 3);
      SetLength(data, Count);
      for i:= 0 to Count - 1 do
        if Param2[1] <> '-' then data[i]:= Format(Fmt, [(MonthOf(IncMonth(ADate, i * 3))div 3)])
        else data[i]:= Format(Fmt, [(MonthOf(IncMonth(ADate, -i * 3))div 3)]);
      WriteDate;
    end;  
  'Q': begin           //Query
      if not FFolderSQL.SectionExists(Param1)then begin
        WriteLog(LOG_ERROR, 'WriteTable', Format(GetLanguageWord('REPORT_WRITEQUERY', '序号为%d的报告中，%s查询语句未定义。'),
        [0, Param1]));
        Exit;
      end;
      with TADOQuery.Create(nil) do try
        Connection:= DM.ADOConn;
        FFolderSQL.ReadSectionValues(Param1, SQL);
        SetSQLParams(Parameters);
        for i:= 0 to Parameters.Count - 1 do
          Parameters[0].Value:= FReportParams.ReadString(IntToStr(FCurrentFolder), Parameters[0].Name, '');
        Open;
        Value1:= 0;
        while not EOF do begin
          case Expand of
          2: begin                                                                //水平扩展                                           //垂直扩展
              shiftVar:= xlShiftToRight;
              if i > 0 then Sheet.Columns.Item[EmptyParam, Col].Insert(shiftVar);
            end;
          3: begin                                                                //垂直扩展
              shiftVar:= xlShiftDown;
              if i > 0 then Sheet.Rows.Item[Row, EmptyParam].Insert(shiftVar);
            end;
          end;
          for i:= 0 to Fields.Count - 1 do
            case Expand of
            0, 2: Sheet.Cells.Item[Row + i, Col + Value1].Value2:= Fields[i].AsString; //水平填充
            1, 3: Sheet.Cells.Item[Col + i, Row + Value1].Value2:= Fields[i].AsString; //垂直填充
            end;
          Inc(Value1);
          Next;
        end;
      finally
        Free;
      end;
    end;
  end;
  SetLength(data, 0);
end;

procedure TReportWriter.WriteTable; 
var       //写表格
  i, j, k, APage, ACol, ARow, ExpandCount, DIMID: Integer;
  dtPage, dtCol, dtRow, dtColCount, dtRowCount: Integer;
  lstCtrl: TConfigFile;
  lst: TStringList;
  Arrt: array[0..12]of string;
  currentSheet: WorkSheet;
  Expand: integer;
  AColQuery, ARowQuery, AExeQuery: TADOQuery;
  AColInfo, ARowInfo: TReportItemInfo;
  S: string; 
  shiftVar: OleVariant;

  procedure ExecuteDataSQL();
  var
    k: Integer;
    fValue: Double;
    szData: string;
  begin
    with AExeQuery do begin
      WriteLog(LOG_DEBUG, 'ExecuteDataSQL', SQL.Text);
      Open;
      i:= 0;
      while not EOF do begin              //Loop records
        if i < RecordCount - 1 then
          InsertARC(currentSheet, ACol + i, ARow + i, Expand);    //Expand Row or Col
        for k:= 0 to Fields.Count - 1 do begin //Loop fields
          szData:= Fields[k].AsString;
          if TryStrToFloat(szData, fValue) then
            szData:= Format(AColInfo.Fmt, [fValue * AColInfo.Rate]);
          case Expand of
          0, 2: currentSheet.Cells.Item[ARow + k, ACol + i].Value2:= szData;   //H
          1, 3: currentSheet.Cells.Item[ARow + i, ACol + k].Value2:= szData;   //V
          end;
        end;
        Inc(i);
        Next;
      end;
    end;
  end;
begin     //写表格
  FSheet.XMLData:= FTaskInfo.Settings;      //Set table XML data
  //提取数据
  lstCtrl:= TConfigFile.Create(FTaskInfo.Control);     //Expand
  lst:= TStringList.Create;
  for APage:= 1 to FSheet.Sheets.Count do begin
    currentSheet:= FSheet.Sheets.item[APage] as WorkSheet;
    (* 提取扩展设定内容 *)
    lstCtrl.ReadSectionValues(IntToStr(APage), lst);   //Expand settings
    for j:= 0 to lst.Count - 1 do begin                //Loop ervery expand setting
      AnalyzeCellName(lst.Names[j], ACol, ARow);
      Expand:= StrToInt(GetSettingValue(lst.ValueFromIndex[j], '扩展', '0'));           //确定扩展方向
      ExpandCount:= StrToInt(GetSettingValue(lst.ValueFromIndex[j], '扩展数量', '0'));  //确定扩展数量
      DIMID:= StrToInt(GetSettingValue(lst.ValueFromIndex[j], 'DIM', '0'));             //得到要扩展的维度（0..10）0:year or month
      case DIMID of
      0: begin //Date //Year(0, 3)//Year(0, -3)//Month(0, <一月的偏差数>)
           i:= GetSettingArrayValue(lst.ValueFromIndex[j], 'DIMVALUE', Arrt);
           case i of
           2: ExpandDim0(currentSheet, ACol, ARow, Arrt[1], '1', '', Arrt[0][1], Expand); 
           3: ExpandDim0(currentSheet, ACol, ARow, Arrt[1], Arrt[2], '', Arrt[0][1], Expand);
           4: ExpandDim0(currentSheet, ACol, ARow, Arrt[1], Arrt[2], Arrt[3], Arrt[0][1], Expand);
           else raise Exception.Create('数据准备[' + FTaskInfo.NodeCaption +
             '][' + lst.Strings[j] + ']扩展设置参数数量有误。' + #13#10 +
             'YEAR(<NowOffset>,(+-)<Num>,[<Format>])' + #13#10 +
             'MONTH(<NowOffset>,(+-)<JanOffset>,[<Format>])' + #13#10 +
             'SEASON(<NowOffset>,(+-)<SeasonOffset>,[<Format>])');
           end;
        end;
      else with GenteralSQL(GetSettingValue(lst.ValueFromIndex[j], 'DIMVALUE', ''), DIMID) do
        try
          Open;
          i:= 0;
          while not EOF do begin
            case Expand of
            0, 2: currentSheet.Cells.Item[ARow, ACol + i].Value2:= Fields[3].AsString;
            1, 3: currentSheet.Cells.Item[ARow + i, ACol].Value2:= Fields[3].AsString;
            end;
            Inc(i);
            Next;
          end;
        finally
          Free;
        end;
      end;
    end;
  end;
  (* 填写数据 *)
  AColQuery:= TADOQuery.Create(nil);
  ARowQuery:= TADOQuery.Create(nil);
  AExeQuery:= TADOQuery.Create(nil);
  AColQuery.Connection:= FConnection;
  ARowQuery.Connection:= FConnection;
  AExeQuery.Connection:= FConnection;
  AColInfo:= TReportItemInfo.Create;
  ARowInfo:= TReportItemInfo.Create;
  try
    for APage:= 1 to FSheet.Sheets.Count do begin
      AColQuery.Close;
      ARowQuery.Close;
      AColQuery.SQL.Text:= 'SELECT * FROM [RPT_ReportItem] WHERE [TaskID]=:TID AND [PageID]=:PID AND [Row] = 0 AND [Col] > 0';
      ARowQuery.SQL.Text:= 'SELECT * FROM [RPT_ReportItem] WHERE [TaskID]=:TID AND [PageID]=:PID AND [Row] > 0 AND [Col] = 0';
      AColQuery.Parameters[0].Value:= FTaskInfo.ID;
      ARowQuery.Parameters[0].Value:= FTaskInfo.ID;
      AColQuery.Parameters[1].Value:= APage;
      ARowQuery.Parameters[1].Value:= APage;
      AColQuery.Open;
      ARowQuery.Open;
      while not ARowQuery.Eof do begin        //Loop rows
        if not ARowInfo.LoadFromDB(ARowQuery)then
          raise Exception.Create('行设置信息读取失败，请检查您的设置！' + #13#10 + LastInfo);
        ARow:= ARowInfo.Row;
        AColQuery.First;
        while not AColQuery.Eof do begin          //Loop columns
          if not AColInfo.LoadFromDB(AColQuery)then
            raise Exception.Create('列设置信息读取失败，请检查您的设置！' + #13#10 + LastInfo);
          for i:= 1 to 10 do AColInfo.DimValue[i]:= AColInfo.DimValue[i] + ARowInfo.DimValue[i];      //合并行列设置
          if not AColInfo.GenteralSQL(AExeQuery, FUserDimCodes, FReportDate)then
            raise Exception.Create('行列设置信息有矛盾，请检查您的设置！' + #13#10 + LastInfo);
          ACol:= AColInfo.Col;
          Expand:= AColInfo.Expand;
          ExecuteDataSQL;
          AColQuery.Next;
        end;
        ARowQuery.Next;
      end;
      (* Cell settings *)
      AColQuery.Close;
      AColQuery.SQL.Text:= 'SELECT * FROM [RPT_ReportItem] WHERE [TaskID]=:TID AND [PageID]=:PID AND [Row] > 0 AND [Col] > 0';
      AColQuery.Parameters[0].Value:= FTaskInfo.ID;
      AColQuery.Parameters[1].Value:= APage;
      AColQuery.Open;
      while not AColQuery.EOF do begin
        if not AColInfo.LoadFromDB(AColQuery)then
          raise Exception.Create('单元格设置信息读取失败，请检查您的设置！' + #13#10 + LastInfo);
        if not AColInfo.GenteralSQL(AExeQuery, FUserDimCodes, FReportDate)then
          raise Exception.Create('行列设置信息有矛盾，请检查您的设置！' + #13#10 + LastInfo);
        ARow:= AColInfo.Row;
        ACol:= AColInfo.Col;
        Expand:= AColInfo.Expand;
        ExecuteDataSQL;
        AColQuery.Next;
      end;
    end;
  finally
    AColInfo.Free;
    ARowInfo.Free;
    AExeQuery.Free;
    AColQuery.Free;
    ARowQuery.Free;
  end;
  (* 导入数据 *)   
  lstCtrl:= TConfigFile.Create(FTaskInfo.Params);     //Import
  for APage:= 1 to FSheet.Sheets.Count do begin
    currentSheet:= FSheet.Sheets.item[APage] as WorkSheet;
    (* 提取扩展设定内容 *)
    lstCtrl.ReadSectionValues(IntToStr(APage), lst);   //Import settings
    for i:= 0 to lst.Count - 1 do begin
      AnalyzeRCName(lst.Names[i], ACol, ARow);         //Get col and row of sheet to import
      if GetSettingArrayValue(lst.ValueFromIndex[i], 'Page', Arrt) <> 6 then Continue;
      dtPage:= StrToInt(Arrt[0]);
      dtColCount:= StrToInt(Arrt[3]);
      dtRowCount:= StrToInt(Arrt[4]);
      Expand:= StrToInt(Arrt[5]);
      with FPage.PageByIndex[dtPage] do begin
        if dtColCount = 0 then dtColCount:= ColCount;
        if dtRowCount = 0 then dtRowCount:= RowCount;
        j:= 0;    
        for dtRow:= StrToInt(Arrt[2])to dtRowCount do begin
          k:= 0;
          if dtRow < dtRowCount then
            InsertARC(currentSheet, ACol + k, ARow + j, Expand); //水平扩展 垂直扩展
          for dtCol:= StrToInt(Arrt[1])to dtColCount do begin
            case Expand of
            0, 2: if Cells[dtCol, dtRow] = '' then
                currentSheet.Cells.Item[ARow + k, ACol + j].Value2:= EmptyParam
              else currentSheet.Cells.Item[ARow + k, ACol + j].Value2:= Cells[dtCol, dtRow];
            1, 3: if Cells[dtCol, dtRow] = '' then
                currentSheet.Cells.Item[ARow + j, ACol + k].Value2:= EmptyParam
              else currentSheet.Cells.Item[ARow + j, ACol + k].Value2:= Cells[dtCol, dtRow];
            end;
            Inc(k);
          end;
          Inc(j);
        end;
      end;
    end;
  end;
  lstCtrl.Free;
end;

(* 事件 *)

procedure TReportWriter.WriteEvent;
var
  i, vl, aPage: Integer;
  ch: char;
  szLst: TStringList;
  szStr: string;
begin
  FTempList.Text:= FTaskInfo.Params;
  szLst:= TStringList.Create;
  try
  for i:= 0 to FTempList.Count - 1 do begin
    ch:= FTempList.Names[i][1];
    szLst.Delimiter:= ',';
    szLst.DelimitedText:= FTempList.ValueFromIndex[i];
    if not TryStrToInt(szLst[0], vl) then vl:= 0;
    WriteLog(LOG_DEBUG, 'WriteEvent', FTempList.Names[i] + '=' + FTempList.ValueFromIndex[i]);
    case ch of
    'T', 'E', 'H', 'B', 'U', 'D', 'L', 'R', 'P', 'G', 'N', 'O', 'S', 'Q':
      FReport.DoAnything(ch, FTempList.ValueFromIndex[i]);
    'M': if vl = 0 then for APage:= 1 to FSheet.Sheets.Count do begin //显示一个表格
          FReport.WriteParagraph('正文', (FSheet.Sheets.Item[APage]as WorkSheet).Name);
          if szLst.Count > 2 then FReport.WriteTable(szLst[2], FSheet, APage, StrToInt(szLst[1]))
          else FReport.WriteTable('', FSheet, APage, StrToInt(szLst[1]));
        end else begin
          if szLst.Count > 2 then FReport.WriteTable(szLst[2], FSheet, vl, StrToInt(szLst[1]))
          else FReport.WriteTable('', FSheet, vl, StrToInt(szLst[1]));
        end;
    'C': if vl = 0 then for APage:= 1 to FSheet.Sheets.Count do begin //粘贴一个表格
          FReport.WriteParagraph('正文', (FSheet.Sheets.Item[APage]as WorkSheet).Name);
          (FSheet.Sheets.Item[APage] as WorkSheet).UsedRange.Copy(EmptyParam);
          if szLst.Count > 2 then FReport.Paste(szLst[2])
          else FReport.Paste('');
        end else begin
          (FSheet.Sheets.Item[vl] as WorkSheet).UsedRange.Copy(EmptyParam);
          if szLst.Count > 2 then FReport.Paste(szLst[2])
          else FReport.Paste('');
        end;
    'I': FReport.WritePicture(FTempList.ValueFromIndex[i]);           //添加一个图片
    'A': begin                                                        //显示一个数据准备
        FPage.AutoCalc:= True;
        if vl = 0 then for APage:= 1 to FPage.Count do begin
          FReport.WriteParagraph('正文', FPage.Name[APage]);
          if szLst.Count > 1 then FReport.WriteData(FPage, APage, szLst[1])
          else FReport.WriteData(FPage, APage, '');
        end else begin
          if szLst.Count > 1 then FReport.WriteData(FPage, vl, szLst[1])
          else FReport.WriteData(FPage, vl, '');
        end;
      end;
    '@': begin
        szStr:= FTempList.Names[i];
        Delete(szStr, 1, 1);
        FReportParams.WriteString(IntToStr(FCurrentFolder), szStr, FCalc.Calc(FTempList.ValueFromIndex[i]));
      end;
    end;
  end;
  finally
  szLst.Free;
  end;
end;

(* 图表 *)

procedure TReportWriter.WriteChart;
var
  aGraph: GraphApplication;
  iSrc, iExpand, iPage, iTitleCol, iTitleRow, iNameCol, iNameRow, iColCount, iRowCount: Integer;
  ASheet: WorkSheet;
  AData: TOleData;
  ACol, ARow: Integer;
  AChartType, AChartTitle: string;

  procedure SetGraphData(const dataCol, dataRow: Integer; const AValue: Variant);
  var
    S: OleVariant;
    D: Double;
  begin
    //  WriteLog(LOG_DEBUG, 'SetGraphData', Format('[%d,%d]=%s', [dataCol, dataRow, AValue]));
    if VarIsNull(AValue) then S:= '-'
    else if VarIsStr(AValue) then begin
      case iExpand of
      0: aGraph.DataSheet.Cells.Item[dataRow, dataCol].Value:= AValue;
      1: aGraph.DataSheet.Cells.Item[dataCol, dataRow].Value:= AValue;
      end;
      Exit;
    end else if VarIsNumeric(AValue) then begin
      D:= AValue;
      S:= d;
    end else S:= 0;
//    if(dataRow <> 0)and(dataCol <> 0)and(
    case iExpand of
    0: aGraph.DataSheet.Cells.Item[dataRow, dataCol].Value:= S;
    1: aGraph.DataSheet.Cells.Item[dataCol, dataRow].Value:= S;
    end;
  end;
begin
  AChartType:= GetSettingValue(FTaskInfo.Settings, '图表类型', '0');
  AChartTitle:= FCalc.Calc(FTaskInfo.Control);
  aGraph:= Chart(FReport.WriteChart(
    StrToInt(GetSettingValue(FTaskInfo.Settings, '类型来源', '0')),
    StrToInt(GetSettingValue(FTaskInfo.Settings, '图宽', '360')),
    StrToInt(GetSettingValue(FTaskInfo.Settings, '图高', '218')),
    AChartType, GetSettingValue(FTaskInfo.Settings, '换行', 'Y'))).GraphApplication;
  aGraph.DataSheet.Cells.Clear;
  iSrc:= StrToInt(GetSettingValue(FTaskInfo.Params, '数据源', '0'));
  iExpand:= StrToInt(GetSettingValue(FTaskInfo.Params, '数据展示', '0'));
  iPage:= StrToInt(GetSettingValue(FTaskInfo.Params, '页号', '1'));
  iTitleCol:= StrToInt(GetSettingValue(FTaskInfo.Params, '标题列位置', '0'));
  iTitleRow:= StrToInt(GetSettingValue(FTaskInfo.Params, '标题行位置', '0'));
  iNameCol:= StrToInt(GetSettingValue(FTaskInfo.Params, '条目列位置', '0'));
  iNameRow:= StrToInt(GetSettingValue(FTaskInfo.Params, '条目行位置', '0'));
  iColCount:= StrToInt(GetSettingValue(FTaskInfo.Params, '数据列数量', '0'));
  iRowCount:= StrToInt(GetSettingValue(FTaskInfo.Params, '数据行数量', '0'));
  if iPage <= 0 then iPage:= 1;
  if iTitleCol <= 0 then iTitleCol:= 1;
  if iTitleRow <= 0 then iTitleRow:= 1;
  if iNameCol <= 0 then iNameCol:= 1;
  if iNameRow <= 0 then iNameRow:= 1;
  case iSrc of
  0: begin  //From data
      AData:= FPage.PageByIndex[iPage];
      if iColCount <= 0 then iColCount:= AData.ColCount - iTitleCol + 1;
      if iRowCount <= 0 then iRowCount:= AData.RowCount - iNameRow + 1;
      WriteLog(LOG_DEBUG, 'WriteChart',
        format('Write data from data %d title(%d,%d) and column(%d,%d) and data(%d,%d)',
        [iPage, iTitleCol, iTitleRow, iNameCol, iNameRow, iColCount, iRowCount]));
      for ACol:= 1 to iColCount do       //Headers
        SetGraphData(ACol + 1, 1, AData.Cells[iTitleCol + ACol - 1, iTitleRow]);
      for ARow:= 1 to iRowCount do       //Columns
        SetGraphData(1, ARow + 1, AData.Cells[iNameCol, iNameRow + ARow - 1]);
      for ACol:= 0 to iColCount - 1 do   //Datas
        for ARow:= 0 to iRowCount - 1 do
          SetGraphData(ACol + 2, ARow + 2, AData.Cells[iTitleCol + ACol, iNameRow + ARow]);
    end;
  1: begin  //From Table
      ASheet:= FSheet.Sheets.Item[iPage] as WorkSheet;
      if iColCount <= 0 then iColCount:= ASheet.UsedRange.Columns.Count - iTitleCol + 1;
      if iRowCount <= 0 then iRowCount:= ASheet.UsedRange.Rows.Count - iNameRow + 1;
      WriteLog(LOG_DEBUG, 'WriteChart',
        format('Write data from table %d title(%d,%d) and column(%d,%d) and data(%d,%d)',
        [iPage, iTitleCol, iTitleRow, iNameCol, iNameRow, iColCount, iRowCount]));
      for ACol:= 1 to iColCount do       //Headers
        SetGraphData(ACol + 1, 1, ASheet.Cells.Item[iTitleRow, iTitleCol + ACol - 1].Value[xlRangeValueDefault]);
      for ARow:= 1 to iRowCount do       //Columns
        SetGraphData(1, ARow + 1, ASheet.Cells.Item[iNameRow + ARow - 1, iNameCol].Value[xlRangeValueDefault]);
      for ACol:= 0 to iColCount - 1 do   //Datas
        for ARow:= 0 to iRowCount - 1 do
          SetGraphData(ACol + 2, ARow + 2, ASheet.Cells.Item[iNameRow + ARow, iTitleCol + ACol].Value[xlRangeValueDefault]);
    end;
  end;
  case StrToInt(GetSettingValue(FTaskInfo.Settings, '类型来源', '0')) of                                              //Set chart type
  0: aGraph.Chart.ChartType:= StrToInt64(AChartType);
  1: aGraph.Chart.ApplyCustomType(xlBuiltIn, AChartType);
  2: aGraph.Chart.ApplyCustomType(xlUserDefined, AChartType);
  end;
  aGraph.Chart.HasTitle:= AChartTitle <> '';
  if aGraph.Chart.HasTitle then aGraph.Chart.ChartTitle.Text:= AChartTitle;
  FReport.EndOfChart(aGraph);
  aGraph.Update;
  aGraph.Quit;
end;

(* 函数 *) 

procedure TReportWriter.UserFunEvent(FunctionName: string;
  CurrentCalcObject: TCalc_Unit; var Value: string);
begin
  if FunctionName = 'PARAMS' then Value:= funGetParams(CurrentCalcObject)
  else if FunctionName = 'OFFSET' then Value:= funDate(CurrentCalcObject)
  else if FunctionName = 'AUTHOR' then Value:= DM.UserInfo.UserName
  else if FunctionName = 'REPORTNAME' then Value:= FReportInfo.ReportName
  else if FunctionName = 'REPORTDATE' then Value:= DateToStr(ReportDate)
  else if FunctionName = 'MONTH' then Value:= IntToStr(MonthOf(ReportDate))
  else if FunctionName = 'YEAR' then Value:= IntToStr(YearOf(ReportDate))
  else if FunctionName = 'TABLE' then Value:= funTable(CurrentCalcObject)
  else if FunctionName = 'DATA' then Value:= funData(CurrentCalcObject)
  else if FunctionName = 'USERDIMNAME' then Value:= funUserDimName(CurrentCalcObject)
  else if FunctionName = 'USERDIMCODE' then Value:= funUserDimCode(CurrentCalcObject)
  else if FunctionName = 'PRINT' then Value:= funPrint(CurrentCalcObject)
  else if Assigned(FOnDoUserFunction) then
    FOnDoUserFunction(FunctionName, CurrentCalcObject, Value);
end;

function TReportWriter.funDate(CurrentCalcObject: TCalc_Unit): string;
var
  OffsetValue: Integer;
begin    //计算回溯值 Offset([<Date>,] <OffsetValue>[, <DateFormat>])
  with CurrentCalcObject do
  if Count = 1 then                                   //Offset(<OffsetValue>)
    Result:= FormatDateTime('yyyy年mm月dd日', IncMonth(FReportDate, StrToInt(Units[0].Value)))
  else if Count = 1 then begin
    if TryStrToInt(Units[1].Value, OffsetValue) then  //Offset(<Date>, <OffsetValue>)
      Result:= FormatDateTime('yyyy年mm月dd日', IncMonth(StrToDate(Units[0].Value), StrToInt(Units[1].Value)))
    else                                              //Offset(<OffsetValue>, <DateFormat>)
      Result:= FormatDateTime(Units[1].Value, IncMonth(FReportDate, StrToInt(Units[0].Value)))
  end else                                            //Offset(<Date>, <OffsetValue>, <DateFormat>)
    Result:= FormatDateTime(Units[2].Value, IncMonth(StrToDate(Units[0].Value), StrToInt(Units[1].Value)));
end;

function TReportWriter.funGetParams(CurrentCalcObject: TCalc_Unit): string;
begin
  if CurrentCalcObject.Count <> 1 then
    raise Exception.Create('函数错误，"PARAMS"函数语法结构为：PARAMS(<parameter_name>).');
  Result:= FReportParams.ReadString(IntToStr(FCurrentFolder), CurrentCalcObject.Units[0].Value, '')
end;

function TReportWriter.GetUserDimCode(DimID: Integer): string;
var
  Idx: Integer;
begin
  Idx:= FUserDimCodes.IndexOfName(IntToStr(DimID));
  if Idx >= 0 then Result:= FUserDimCodes.ValueFromIndex[Idx]
  else Result:= '';
end;

function TReportWriter.GetUserDimName(DimID: Integer): string;
var
  Idx: Integer;
begin
  Idx:= FUserDimNames.IndexOfName(IntToStr(DimID));
  if Idx >= 0 then Result:= FUserDimNames.ValueFromIndex[Idx]
  else Result:= '';
end;

procedure TReportWriter.SetConnection(const Value: TADOConnection);
begin
  FConnection := Value;
end;

procedure TReportWriter.SetUserDimCode(DimID: Integer; const Value: string);
var
  Idx: Integer;
begin                                   
  Idx:= FUserDimCodes.IndexOfName(IntToStr(DimID));
  if Idx >= 0 then FUserDimCodes.ValueFromIndex[Idx]:= Value
  else FUserDimCodes.Add(IntToStr(DimID) + '=' + Value);
end;

procedure TReportWriter.SetUserDimName(DimID: Integer; const Value: string);
var
  Idx: Integer;
begin           
  Idx:= FUserDimNames.IndexOfName(IntToStr(DimID));
  if Idx >= 0 then FUserDimNames.ValueFromIndex[Idx]:= Value
  else FUserDimNames.Add(IntToStr(DimID) + '=' + Value);
end;

procedure TReportWriter.SetOnDoUserFunction(
  const Value: TCalc_DoUserFunction_Event);
begin
  FOnDoUserFunction := Value;
end;

procedure TReportWriter.SetReportDate(const Value: TDate);
begin
  FReportDate := Value;
  FReportYear:= YearOf(Value);
  FReportMonth:= MonthOf(Value);
end;

function TReportWriter.funTable(CurrentCalcObject: TCalc_Unit): string;
begin    //得到表格中的内容 Table(<Page>, <Col>, <Row>)
  with CurrentCalcObject do
  if Count <> 3 then raise Exception.Create('The "TABLE" Syntax is Table(<Page>, <Col>, <Row>).');
  with CurrentCalcObject do
    Result:= (FSheet.Sheets.Item[StrToInt(Units[0].Value)] as Worksheet).Cells.
      Item[StrToInt(Units[2].Value), StrToInt(Units[1].Value)].Value2;
end;

function TReportWriter.funData(CurrentCalcObject: TCalc_Unit): string;
begin    //得到数据准备中的内容 Data(<Page>, <Col>, <Row>)
  with CurrentCalcObject do
  if Count <> 3 then raise Exception.Create('The "DATA" Syntax is Data(<Page>, <Col>, <Row>).');
  with CurrentCalcObject do
    Result:= FPage.PageByIndex[StrToInt(Units[0].Value)].
     Cells[StrToInt(Units[1].Value), StrToInt(Units[2].Value)];
end;

function TReportWriter.funUserDimName(CurrentCalcObject: TCalc_Unit): string;
begin    //得到用户默认维度内容 UserDimName(<DimID>)
  with CurrentCalcObject do
  if Count <> 1 then raise Exception.Create('The "USERDIMNAME" Syntax is UserDimName(<DimID>).');
  Result:= UserDimName[StrToInt(CurrentCalcObject.Units[0].Value)];
end;

function TReportWriter.funUserDimCode(CurrentCalcObject: TCalc_Unit): string;
begin    //得到用户默认维度编码 UserDimCode(<DimID>)
  with CurrentCalcObject do
  if Count <> 1 then raise Exception.Create('The "USERDIMCODE" Syntax is UserDimCode(<DimID>).');
  Result:= UserDimCode[StrToInt(CurrentCalcObject.Units[0].Value)];
end; 

function TReportWriter.funGetValue(CurrentCalcObject: TCalc_Unit): string;
begin    //得到数用户设置中的内容 GetValue(<ValueName>)
  if CurrentCalcObject.Count <> 1 then raise Exception.Create('The "GETVALUE" Syntax is GetValue(<ValueName>).');
  with CurrentCalcObject do
    Result:= FUserValues.Values[Units[0].Value];
end;

function TReportWriter.funSetValue(CurrentCalcObject: TCalc_Unit): string;
begin    //设置数用户设置中的内容 SetValue(<ValueName>, <Value>)
  Result:= '';
  if CurrentCalcObject.Count <> 1 then raise Exception.Create('The "SETVALUE" Syntax is SetValue(<ValueName>,<Value>).');
  with CurrentCalcObject do
    FUserValues.Values[Units[0].Value]:= Units[1].Value;
end;  

function TReportWriter.funPrint(CurrentCalcObject: TCalc_Unit): string;
var
  szCalc: TOleDataCalc;
begin
  Result:= '';
  if CurrentCalcObject.Count <> 2 then raise Exception.Create('The "PRINT" Syntax is Print(<StyleName>,<Content>).');
  with CurrentCalcObject do try
    szCalc:= TOleDataCalc.Create;
    szCalc.Data:= FCalc.Data;
    szCalc.Sheet:= FCalc.Sheet;
    szCalc.OnDoUserFunction:= FCalc.OnDoUserFunction;            
    FReport.WriteText(FTaskInfo.Control, FCalc.TempCalc.CaleValue);
    FCalc.TempCalc.CaleValue:= '';
    FReport.WriteText(Units[0].Value, szCalc.Calc('=' + Units[1].Value));
  finally
    szCalc.Free;
  end;
end;

/// End of functions

procedure TReportWriter.SetSQLParams(AParams: TParameters);
var
  i, j: Integer;
  ctn: boolean;

  function SetParamValue(ParamName: string; AParam: TParameter; ParamValue: Variant): Boolean;
  begin
    Result:= AParam.Name = ParamName;
    if Result then begin
      WriteLog(LOG_DEBUG, 'SetSQLParams', ParamName + '=' + ParamValue);
      AParam.Value:= ParamValue;
    end;
  end;
begin     //给执行的SQL语句添加默认参数值
  for i:= 0 to AParams.Count - 1 do begin
    if SetParamValue('RDATE', AParams[i], DateToStr(FReportDate))then Continue;
    if SetParamValue('RYEAR', AParams[i], FReportYear)then Continue;
    if SetParamValue('RMONTH', AParams[i], FReportMonth)then Continue;
    for j:= 1 to FUserDimCodes.Count - 1 do begin
      ctn:= SetParamValue('USERDIMCODE' + IntToStr(j), AParams[i], FUserDimCodes.Values[IntToStr(j)]);
      if ctn then Break;
    end;
    if not ctn then
      AParams[i].Value:= FReportParams.ReadString(IntToStr(FCurrentFolder), AParams[i].Name, '');
  end;
end;

procedure TReportWriter.InsertARC(currSheet: WorkSheet; Col, Row, Expand: Integer);
var
  shiftVar: OleVariant;
begin
  case Expand of
  2: begin                        //H and Expand
      shiftVar:= xlShiftToRight;
      currSheet.Columns.Item[EmptyParam, Col + 1].Insert(shiftVar);
      currSheet.Columns.Item[EmptyParam, Col].Copy(currSheet.Columns.Item[EmptyParam, Col + 1]);
    end;
  3: begin                        //V and expand
      shiftVar:= xlShiftDown;
      currSheet.Rows.Item[Row + 1, EmptyParam].Insert(shiftVar);
      currSheet.Rows.Item[Row, EmptyParam].Copy(currSheet.Rows.Item[Row + 1, EmptyParam]);
    end;
  end;
end;

function TReportWriter.GetCell(const ColOffset,
  RowOffSet: Integer): string;
var
  orgCalcStatus: Boolean;
begin
  orgCalcStatus:= FPage.AutoCalc;
  FPage.AutoCalc:= True;
  Result:= FCurrentData.Cells[FCurrentCol + ColOffset, FCurrentRow - RowOffSet];
  FPage.AutoCalc:= orgCalcStatus;
end;

(* 生成图片 *)

procedure TReportWriter.WritePicture;
var
  AType: string;
begin
  AType:= GetSettingValue(FTaskInfo.Settings, '图片类型', '罗盘');
  if AType = '罗盘' then MakePicture
  else if AType = '仪表' then MakeCockpit;
end;

procedure TReportWriter.MakeCockpit;
const CENTERRING : array[0..23, 0..1] of Shortint =
  ((2,2), (3,1), (3,0), (3,-1), (2,-2), (1,-3), (0,-3), (-1,-3), (-2,-2),
  (-3,-1), (-3,0), (-3,1), (-2,2), (-1,3), (0,3), (1,3),
  (1,1), (1,0), (1,-1), (0,-1), (-1,-1), (-1,0), (-1,1), (0,1));
var
  ATitle, ValueUnit: string;
  szValue, R, R1, X0, Y0, X1, X2, Y1, Y2,
  IntervalCount, MinInterval, MaxInterval,
  i: Integer;
  bmpRect: TRect;
  SAngle, Angle, IntervalValue, DataValue, XRate, YRate: Double;
  bmp, orgBmp: Graphics.TBitmap;
begin
  inherited;
  //Load bitmap file and calc rate and set image size
  orgBmp:= Graphics.TBitmap.Create;
  orgBmp.LoadFromFile(GetRealFilename(GetSettingValue(FTaskInfo.Params, '背景', '')));
  bmpRect:= Rect(0, 0, orgBmp.Width, orgBmp.Height);
  //Begin to draw cochpit
  bmp:= Graphics.TBitmap.Create;
  bmp.Width:= orgBmp.Width;
  bmp.Height:= orgBmp.Height;
  bmp.Canvas.FillRect(bmpRect);
  WriteLog(LOG_DEBUG, 'InitViewComponent_Coskpid', 'Loaded bitmap.');
  //Title
  ATitle:= FCalc.Calc(FTaskInfo.Control);
  case StrToInt(GetSettingValue(FTaskInfo.Settings, 'TitleKind', '0'))of
  0: begin  //水平
      szValue:= bmp.Canvas.TextWidth(ATitle) div 2;
      bmp.Canvas.TextOut(
        StrToInt(GetSettingValue(FTaskInfo.Settings, 'TitleX', '0')) - szValue,
        StrToInt(GetSettingValue(FTaskInfo.Settings, 'TitleY', '0')), ATitle);
    end;
  1: begin  //垂直
      szValue:= bmp.Canvas.TextHeight(ATitle) div 2;
      bmp.Canvas.TextOut(
        StrToInt(GetSettingValue(FTaskInfo.Settings, 'TitleX', '0')),
        StrToInt(GetSettingValue(FTaskInfo.Settings, 'TitleY', '0')) - szValue, ATitle);
    end;
  end;
  WriteLog(LOG_DEBUG, 'InitViewComponent_Coskpid', 'Begin to draw Interval.');
  //Interval
  MinInterval:= StrToInt(GetSettingValue(FTaskInfo.Settings, 'Min', '0'));
  MaxInterval:= StrToInt(GetSettingValue(FTaskInfo.Settings, 'Max', '100'));
  ValueUnit:= GetSettingValue(FTaskInfo.Settings, 'MaxUnit', '100');
  if GetSettingValue(FTaskInfo.Settings, 'IntervalUnit', '0') = '数量' then  //直接指定数量
    IntervalCount:= StrToInt(GetSettingValue(FTaskInfo.Settings, 'Interval', '10'))
  else IntervalCount:= (MaxInterval - MinInterval) div StrToInt(GetSettingValue(FTaskInfo.Settings, 'Interval', '10')); //指定刻度间隔量
  IntervalValue:= RoundTo((MaxInterval - MinInterval) / IntervalCount, -2);
  case StrToInt(GetSettingValue(FTaskInfo.Settings, 'Kind', '0')) of
  0: begin  //圆周
      X0:= StrToInt(GetSettingValue(FTaskInfo.Settings, 'CentorX', '0'));
      Y0:= StrToInt(GetSettingValue(FTaskInfo.Settings, 'CentorY', '0'));
      X1:= StrToInt(GetSettingValue(FTaskInfo.Settings, 'StartX', '0'));
      Y1:= StrToInt(GetSettingValue(FTaskInfo.Settings, 'StartY', '0'));
      X2:= StrToInt(GetSettingValue(FTaskInfo.Settings, 'EndX', '0'));
      Y2:= StrToInt(GetSettingValue(FTaskInfo.Settings, 'EndY', '0'));
      R:= Round((SQRT((X1 - X0) * (X1 - X0) + (Y1 - Y0) * (Y1 - Y0)) +
        SQRT((X2 - X0) * (X2 - X0) + (Y2 - Y0) * (Y2 - Y0))) / 2);
      case StrToInt(GetSettingValue(FTaskInfo.Settings, 'ScaleKind', '0')) of
      0: R1:= R - StrToInt(GetSettingValue(FTaskInfo.Settings, 'ScalePos', '0'));  //内侧
      1: R1:= R + StrToInt(GetSettingValue(FTaskInfo.Settings, 'ScalePos', '0'));  //外侧
      end;
      //画刻度
      SAngle:= ArcTan((Y0 - Y1) / (X0 - X1));
      Angle:= (ArcTan((Y0 - Y2) / (X0 - X2)) - SAngle + 3.14159265) / IntervalCount;    // +
      for i:= 0 to IntervalCount do begin
        ATitle:= FloatToStr(MinInterval + IntervalValue * i) + GetSettingValue(FTaskInfo.Params, '计量单位', '');
        szValue:= bmp.Canvas.TextWidth(ATitle);
        bmp.Canvas.TextOut(X0 - Round(R1 * Cos(SAngle + Angle * i)) - szValue div 2,
          Y0 - Round(R1 * Sin(SAngle + Angle * i)), ATitle);
      end;
      //画指针
      Angle:= (ArcTan((Y0 - Y2) / (X0 - X2)) - SAngle + 3.14159265) / (MaxInterval - MinInterval);
      case StrToInt(GetSettingValue(FTaskInfo.Params, '数据源', '0')) of
      0: begin
          if TryStrToFloat(FPage.PageByIndex[
            StrToInt(GetSettingValue(FTaskInfo.Params, '页号', '1'))].Cells[
            StrToInt(GetSettingValue(FTaskInfo.Params, '合计列位置', '0')),
            StrToInt(GetSettingValue(FTaskInfo.Params, '合计行位置', '0'))],
            DataValue) then IntervalValue:= DataValue - MinInterval
          else Exit;
        end;
      1: begin         
          if TryStrToFloat((FSheet.Sheets.Item[
            StrToInt(GetSettingValue(FTaskInfo.Params, '页号', '1'))] as WorkSheet).Cells.Item[
            StrToInt(GetSettingValue(FTaskInfo.Params, '合计行位置', '0')),
            StrToInt(GetSettingValue(FTaskInfo.Params, '合计列位置', '0'))].Value2,
            DataValue) then IntervalValue:= DataValue - MinInterval
          else Exit;
        end;
      end;
      X1:= X0 - Round(R * Cos(SAngle + Angle * IntervalValue));
      Y1:= Y0 - Round(R * Sin(SAngle + Angle * IntervalValue));
      for i:= 0 to 23 do begin
        bmp.Canvas.MoveTo(X1, Y1);
        bmp.Canvas.LineTo(X0 + CENTERRING[i][0], Y0 + CENTERRING[i][1]);
      end;
      //写数值
      X2:= StrToInt(GetSettingValue(FTaskInfo.Settings, 'ValueX', '0'));
      Y2:= StrToInt(GetSettingValue(FTaskInfo.Settings, 'ValueY', '0'));
      ATitle:= FloatToStr(DataValue) + GetSettingValue(FTaskInfo.Params, '计量单位', '');
      szValue:= bmp.Canvas.TextWidth(ATitle) div 2;
      if X2 = 0 then X1:= X0 - Round(R / 2 * Cos(SAngle + Angle * IntervalValue)) - szValue
      else X1:= X2 - szValue;
      szValue:= bmp.Canvas.TextHeight(ATitle) div 2;
      if Y2 = 0 then Y1:= Y0 - Round(R / 2 * Sin(SAngle + Angle * IntervalValue)) - szValue
      else Y1:= Y2 - szValue;
      bmp.Canvas.TextOut(X1, Y1, ATitle);
    end;
  1: begin  //垂直
    end;
  2: begin  //水平   
    end;
  end;
  orgBmp.Canvas.CopyMode:= cmSrcAnd;
  orgBmp.Canvas.CopyRect(bmpRect, bmp.Canvas, bmpRect);           
  if GetSettingValue(FTaskInfo.Params, '输出', '') = '' then
    orgBmp.SaveToFile(ExtractFilePath(Application.ExeName) + 'PIC.BMP')
  else orgBmp.SaveToFile(GetRealFileName(GetSettingValue(FTaskInfo.Params, '输出', '')));
  orgBmp.Free;
  WriteLog(LOG_DEBUG, 'InitViewComponent_Coskpid', 'End of cockpid.');
end;

procedure TReportWriter.MakePicture;
var
  iSrc, iExpand, iPage, iTitleCol, iTitleRow, iNameCol, iNameRow, iColCount, iRowCount,
  X0, Y0: Integer;
  R, iStartArt, WordAngle, tmpAngle: Double;
  ASheet: WorkSheet;
  AData, szData: TOleData;
  i, ACol, ARow: Integer;
  ATitle: WideString;
  bmpFile, bmpBlock: Graphics.TBitmap;
  rt: TRect;
  sumValue: string;

  procedure SetGraphData(const dataCol, dataRow: Integer; const AValue: Variant);
  var
    S: OleVariant;
    D: Double;
  begin
    //  WriteLog(LOG_DEBUG, 'SetGraphData', Format('[%d,%d]=%s', [dataCol, dataRow, AValue]));
    if VarIsNull(AValue) then S:= '-'
    else if VarIsStr(AValue) then begin
      case iExpand of
      0: szData.Cells[dataCol, dataRow]:= AValue;
      1: szData.Cells[dataRow, dataCol]:= AValue;
      end;
      Exit;
    end else if VarIsNumeric(AValue) then begin
      D:= AValue;
      S:= d;
    end else
      S:= 0;
    case iExpand of
    0: szData.Cells[dataCol, dataRow]:= S;
    1: szData.Cells[dataRow, dataCol]:= S;
    end;
  end;    //*)
begin
  ATitle:= FCalc.Calc(FTaskInfo.Control);
  iStartArt:= StrToInt(GetSettingValue(FTaskInfo.Params, '起始角', '45')) / 180 * pi;
  bmpFile:= Graphics.TBitmap.Create;
  bmpFile.Width:= StrToInt(GetSettingValue(FTaskInfo.Params, '图宽', '360'));
  bmpFile.Height:= StrToInt(GetSettingValue(FTaskInfo.Params, '图高', '360'));
  i:= StrToInt(GetSettingValue(FTaskInfo.Settings, '字号', '9'));
  rt:= Rect(0, 0, bmpFile.Width, bmpFile.Height);
  bmpFile.Canvas.FillRect(rt);
  //Title
  bmpFile.Canvas.Font.Name:= '宋体';
  bmpFile.Canvas.Font.Color:= clBlack;
  R:= (bmpFile.Width div 2) - 30;
  X0:= bmpFile.Width div 2;
  Y0:= bmpFile.Height - bmpFile.Width div 2;
  WordAngle:= Arctan(bmpFile.Canvas.TextWidth('图') / 2 / R) * 3;
  //Write title
  bmpFile.Canvas.Font.Size:= i div 3 * 5;
  DrawPictureTitle(bmpFile.Canvas, R + 45, X0, Y0, ATitle);
  //Get data
  szData:= TOleData.Create();
  iSrc:= StrToInt(GetSettingValue(FTaskInfo.Params, '数据源', '0'));
  iExpand:= StrToInt(GetSettingValue(FTaskInfo.Params, '数据展示', '0'));
  iPage:= StrToInt(GetSettingValue(FTaskInfo.Params, '页号', '1'));
  iTitleCol:= StrToInt(GetSettingValue(FTaskInfo.Params, '标题列位置', '0'));
  iTitleRow:= StrToInt(GetSettingValue(FTaskInfo.Params, '标题行位置', '0'));
  iNameCol:= StrToInt(GetSettingValue(FTaskInfo.Params, '条目列位置', '0'));
  iNameRow:= StrToInt(GetSettingValue(FTaskInfo.Params, '条目行位置', '0'));
  iColCount:= StrToInt(GetSettingValue(FTaskInfo.Params, '数据列数量', '0'));
  iRowCount:= StrToInt(GetSettingValue(FTaskInfo.Params, '数据行数量', '0'));
  if iPage <= 0 then iPage:= 1;
  if iTitleCol <= 0 then iTitleCol:= 1;
  if iTitleRow <= 0 then iTitleRow:= 1;
  if iNameCol <= 0 then iNameCol:= 1;
  if iNameRow <= 0 then iNameRow:= 1;
  case iSrc of
  0: begin
      AData:= FPage.PageByIndex[iPage];
      if iColCount <= 0 then iColCount:= AData.ColCount - iTitleCol + 1;
      if iRowCount <= 0 then iRowCount:= AData.RowCount - iNameRow + 1;
      WriteLog(LOG_DEBUG, 'WriteChart',
        format('Write data from data %d title(%d,%d) and column(%d,%d) and data(%d,%d)',
        [iPage, iTitleCol, iTitleRow, iNameCol, iNameRow, iColCount, iRowCount]));
      for ACol:= 1 to iColCount do       //Headers
        SetGraphData(ACol + 1, 1, AData.Cells[iTitleCol + ACol - 1, iTitleRow]);
      for ARow:= 1 to iRowCount do       //Columns
        SetGraphData(1, ARow + 1, AData.Cells[iNameCol, iNameRow + ARow - 1]);
      for ACol:= 0 to iColCount - 1 do   //Datas
        for ARow:= 0 to iRowCount - 1 do
          SetGraphData(ACol + 2, ARow + 2, AData.Cells[iTitleCol + ACol, iNameRow + ARow]);
      sumValue:= AData.Cells[StrToInt(GetSettingValue(FTaskInfo.Params, '合计列位置', '0')),
        StrToInt(GetSettingValue(FTaskInfo.Params, '合计行位置', '0'))];
    end;
  1: begin
      ASheet:= FSheet.Sheets.Item[iPage] as WorkSheet;
      if iColCount <= 0 then iColCount:= ASheet.UsedRange.Columns.Count - iTitleCol + 1;
      if iRowCount <= 0 then iRowCount:= ASheet.UsedRange.Rows.Count - iNameRow + 1;
      WriteLog(LOG_DEBUG, 'WriteChart',
        format('Write data from table %d title(%d,%d) and column(%d,%d) and data(%d,%d)',
        [iPage, iTitleCol, iTitleRow, iNameCol, iNameRow, iColCount, iRowCount]));
      for ACol:= 1 to iColCount do       //Headers
        SetGraphData(ACol + 1, 1, ASheet.Cells.Item[iTitleRow, iTitleCol + ACol - 1].Value[xlRangeValueDefault]);
      for ARow:= 1 to iRowCount do       //Columns
        SetGraphData(1, ARow + 1, ASheet.Cells.Item[iNameRow + ARow - 1, iNameCol].Value[xlRangeValueDefault]);
      for ACol:= 0 to iColCount - 1 do   //Datas
        for ARow:= 0 to iRowCount - 1 do
          SetGraphData(ACol + 2, ARow + 2, ASheet.Cells.Item[iNameRow + ARow, iTitleCol + ACol].Value[xlRangeValueDefault]);
      sumValue:= ASheet.Cells.Item[StrToInt(GetSettingValue(FTaskInfo.Params, '合计行位置', '0')),
        StrToInt(GetSettingValue(FTaskInfo.Params, '合计列位置', '0'))].Value2;
    end;
  end;
  //Write data                            
  bmpFile.Canvas.Font.Size:= i;
  WordAngle:= Arctan(bmpFile.Canvas.TextHeight('图') / 2 / (R - 60)) * 2;
  for ACol:= 2 to szData.ColCount do begin
    bmpFile.Canvas.MoveTo(Round(X0 - (R - 5) * cos(iStartArt + (ACol - 2) * szData.RowCount * WordAngle * 1.5 + WordAngle * 1.2)),
           Round(Y0 - (R - 5) * sin(iStartArt + (ACol - 2) * szData.RowCount * WordAngle * 1.5 + WordAngle * 1.2)));
    bmpFile.Canvas.LineTo(Round(X0 - (R - 120) * cos(iStartArt + (ACol - 2) * szData.RowCount * WordAngle * 1.5 + WordAngle * 1.2)),
           Round(Y0 - (R - 120) * sin(iStartArt + (ACol - 2) * szData.RowCount * WordAngle * 1.5 + WordAngle * 1.2)));
    for ARow:= 1 to szData.RowCount do begin
      i:= (ACol - 2) * szData.RowCount + ARow - 1;
      DrawPictureData(bmpFile.Canvas, R - 56, iStartArt + WordAngle * i * 1.5, X0, Y0, szData.Cells[ACol, ARow]);
      DrawPictureColumn(bmpFile.Canvas, R - 64, iStartArt + WordAngle * i * 1.5 + WordAngle / 2, X0, Y0, szData.Cells[1, ARow]);
    end;
  end;
  bmpFile.Canvas.Font.Style:= [fsBold];
  bmpFile.Canvas.TextOut(X0 - bmpFile.Canvas.TextWidth(sumValue) div 2, Y0, sumValue + GetSettingValue(FTaskInfo.Params, '计量单位', ''));
  bmpBlock:= Graphics.TBitmap.Create;
  if GetSettingValue(FTaskInfo.Params, '背景', '') = '' then bmpBlock.Assign(bmpFile)
  else begin
    bmpBlock.LoadFromFile(GetRealFileName(GetSettingValue(FTaskInfo.Params, '背景', '')));
    bmpBlock.Canvas.CopyMode:= cmSrcAnd;
    bmpBlock.Canvas.CopyRect(rt, bmpFile.Canvas, rt);
  end;
  if GetSettingValue(FTaskInfo.Params, '输出', '') = '' then
    bmpBlock.SaveToFile(ExtractFilePath(Application.ExeName) + 'PIC.BMP')
  else bmpBlock.SaveToFile(GetRealFileName(GetSettingValue(FTaskInfo.Params, '输出', '')));
  bmpBlock.Free;
  bmpFile.Free;
end;

procedure TReportWriter.DrawPictureColumn(Canvas: TCanvas; R,
  Angle: Double; X0, Y0: Integer; Text: WideString);
var
  wordAngle, tmpAngle: Double;
  isRecerve, isHorizontal, isInner: Boolean;
  I, ln, WordHeight, WordWidth: Integer;
  S: string;
begin     //在图片中写行提示
  if Text = '' then Exit;
  WordWidth:= Canvas.TextWidth('图');                     //字宽
  WordHeight:= Canvas.TextHeight('图');                   //字高
  // Protected
  if Angle > 2 * pi then Angle:= Angle - 2 * pi;
  if Angle > 2 * pi then Angle:= Angle - 2 * pi;
  if Angle < 0 then Angle:= 2 * pi + Angle;   //So angle is -180 -> 180
  if Angle < 0 then Angle:= 2 * pi + Angle;   //So angle is -180 -> 180
  // Define settings
  if(Angle <= pi * 1.75)and(Angle > pi * 1.25)then begin            //1.25-1.75
    isRecerve:= True; isHorizontal:= False; isInner:= False;
    SetPictureFontAngle(Canvas, 1.5 * pi - Angle);
  end else if(Angle <= pi * 1.25)and(Angle > pi * 0.75)then begin   //0.75-1.25
    isRecerve:= True; isHorizontal:= true;  isInner:= True;
    SetPictureFontAngle(Canvas, pi - Angle);
  end else if(Angle <= pi * 0.75)and(Angle > pi / 4)then begin      //.25-.75
    isRecerve:= False; isHorizontal:= False; isInner:= True;
    R:= R + WordWidth;
    SetPictureFontAngle(Canvas, 0.5 * pi - Angle);
  end else if(Angle <= pi / 4)or(Angle > pi * 1.75)then begin       // < .25 or > 1.75
    isRecerve:= False; isHorizontal:= True; isInner:= False;
    R:= R + WordWidth;
    SetPictureFontAngle(Canvas, - Angle);
  end;
  ln:= Length(Text);
  for I := 0 to ln - 1 do begin
    if isRecerve then S:= Text[ln - i] else S:= Text[I + 1];
    if IsHorizontal then tmpAngle:= ArcSin(WordWidth / 2 / R)
    else tmpAngle:= ArcSin(WordHeight / 2 / R);
    R:= R - WordWidth;
    if isInner then tmpAngle:= Angle - tmpAngle
    else tmpAngle:= Angle + tmpAngle;
    Canvas.TextOut(Round(X0 - R * cos(tmpAngle)), Round(Y0 - R * sin(tmpAngle)), S);
  end;
end;

procedure TReportWriter.DrawPictureData(Canvas: TCanvas; R, Angle: Double;
  X0, Y0: Integer; Text: WideString);
var
  textAngle, wordAngle: Double;
  LogFont: TLogFont;
  theFont: TFont;
  I, ln: Integer;
begin     //在图片中写数
  if Text = '' then Exit;
  wordAngle:= ArcSin(Canvas.TextHeight('图') / 2 / R);
  SetPictureFontAngle(Canvas, pi - Angle - wordAngle);
  Canvas.TextOut(Round(X0 - R * cos(Angle)), Round(Y0 - R * sin(Angle)), Text);
end;

procedure TReportWriter.DrawPictureTitle(Canvas: TCanvas; R: Double;
  X0, Y0: Integer; Text: WideString);
var
  textAngle, wordAngle, tempAngle: Double;
  LogFont: TLogFont;
  theFont: TFont;
  I, ln: Integer;
begin     //标识图片中的标题
  if Text = '' then Exit;
  wordAngle:= ArcSin(Canvas.TextWidth('图') / 2 / R);
  ln:= Length(Text);
  textAngle:= pi / 2 - ArcSin(Canvas.TextWidth('图') / R) * ln * 0.75;          //文字位置角度
  R:= R + Canvas.TextHeight('图');
  with Canvas do
    for I := 0 to ln - 1 do begin
      tempAngle:= textAngle + wordAngle * i * 3;          //文字位置（角度）
      SetPictureFontAngle(Canvas, 0.5 * pi - tempAngle - wordAngle);
      TextOut(Round(X0 - R * cos(tempAngle)), Round(Y0 - R * sin(tempAngle)), Text[I + 1]);
    end;
end;

procedure TReportWriter.SetPictureFontAngle(Canvas: TCanvas; Angle: Double);
var
  LogFont: TLogFont;
  theFont: TFont;
begin     //设置文字的倾斜角
  theFont:= TFont.Create;
  theFont.Assign(Canvas.Font);
  GetObject(theFont.handle, sizeof(LogFont), @LogFont);
  LogFont.lfEscapement:= Round(Angle / pi * 1800);
  LogFont.lfOrientation:= 0;
  theFont.handle:= CreateFontIndirect(Logfont);
  Canvas.Font.Assign(theFont);
  theFont.Free;
end;

procedure TReportWriter.SetCurrentDimCodes(const Value: string);
begin
  FCurrentDimCodes := Value;
end;

end.
