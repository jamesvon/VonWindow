unit UVonDrawing;

interface

uses SysUtils, Classes, UVonSystemFuns, Graphics, UVonConfig, Printers;

type
  TBaseStyle = class

  protected
    procedure LoadFromConfig(Cfg: TVonConfig; Section, Key: string); virtual; abstract;
    procedure SaveConfig(Cfg: TVonConfig; Section, Key: string); virtual; abstract;
    procedure Draw(Canvas: TCanvas; X, Y: Integer; Text: string); virtual; abstract;
  published

  end;

  TTextStyle = class
  private
    Fbmp: TBitmap;
  private
    FHAlign: Integer;
    FBackColor: Integer;
    FFontSize: Integer;
    FFontItalic: boolean;
    FFontStrikeOut: boolean;
    FFontBold: boolean;
    FRotate: Integer;
    FFontName: string;
    FWordSpacing: Integer;
    FFontUnderline: boolean;
    FFontColor: Integer;
    FWordWrap: boolean;
    FWidth: Integer;
    FHeight: Integer;
    FLineCount: Integer;
    FLineSpacing: Integer;
    FVAlign: Integer;
    procedure RotateBmp(RotateType: Integer; srcBmp, destBmp: TBitmap);
  public
    constructor Create;
    destructor Destroy;
    procedure Draw(Canvas: TCanvas; X, Y: Integer; Text: string);
    procedure LoadFromConfig(Cfg: TVonConfig; Section, Key: string);
    procedure SaveConfig(Cfg: TVonConfig; Section, Key: string);
  published
    /// <summary>字体名称<summary>
    property FontName: string read FFontName write FFontName;
    /// <summary>字体大小<summary>
    property FontSize: Integer read FFontSize write FFontSize;
    /// <summary>粗体<summary>
    property FontBold: boolean read FFontBold write FFontBold;
    property FontItalic: boolean read FFontItalic write FFontItalic;
    property FontUnderline: boolean read FFontUnderline write FFontUnderline;
    property FontStrikeOut: boolean read FFontStrikeOut write FFontStrikeOut;
    /// <summary>每行允许的文字数量，0表示不限制<summary>
    property LineCount: Integer read FLineCount write FLineCount;
    /// <summary>行间距<summary>
    property LineSpacing: Integer read FLineSpacing write FLineSpacing;
    /// <summary>文字设定宽度，0表示自由宽度<summary>
    property Width: Integer read FWidth write FWidth;
    /// <summary>文字高度，0表示自由高度<summary>
    property Height: Integer read FHeight write FHeight;
    /// <summary>字间距<summary>
    property WordSpacing: Integer read FWordSpacing write FWordSpacing;
    /// <summary>旋转角度<summary>
    property Rotate: Integer read FRotate write FRotate;
    /// <summary>水平对其方式<summary>
    property HAlign: Integer read FHAlign write FHAlign;
    /// <summary>垂直对其方式<summary>
    property VAlign: Integer read FVAlign write FVAlign;
    /// <summary>是否允许自动换行<summary>
    property WordWrap: boolean read FWordWrap write FWordWrap;
    /// <summary>字体颜色<summary>
    property FontColor: Integer read FFontColor write FFontColor;
    /// <summary>背景颜色<summary>
    property BackColor: Integer read FBackColor write FBackColor;
  end;

  TVonBmp = class
  private
    FContent: TVonConfig;
  public
    procedure AddText(Key: string; X, Y: Integer; Style: TTextStyle);
    procedure AddBackground(Key: string; X, Y: Integer; ilename: string);
    procedure AddShape(Key, Filename: string; X, Y: Integer; ShapeKind: string; Params: array of Extended);
  published
    property Content: TVonConfig read FContent write FContent;
  end;

implementation

{ TTextStyle }

constructor TTextStyle.Create;
begin
  Fbmp:= TBitmap.Create;
end;

destructor TTextStyle.Destroy;
begin
  Fbmp.Free;
end;
procedure TTextStyle.Draw(Canvas: TCanvas; X, Y: Integer; Text: string);
var
  szBmp: TBitmap;
  lst: TStringList;
  i, J, maxCnt, w, H, PX, PY: Integer;
  C: Extended;
begin
  szBmp:= TBitmap.Create;
  lst:= TStringList.Create;
  lst.Text:= Text;
  with szBmp.Canvas do try
    Brush.Color:= BackColor;
    with Font do begin
      Name:= FFontName;
      Size:= FFontSize;
      Color:= FontColor;
      Style:= [];
      if FFontBold then Style:= Style + [fsBold];
      if FFontItalic then Style:= Style + [fsItalic];
      if FFontUnderline then Style:= Style + [fsUnderline];
      if FFontStrikeOut then Style:= Style + [fsStrikeOut];
    end;
    //整理文字，多行处理，限定行字数，自动换行
    I:= 0; maxCnt:= LineCount; w:= TextWidth('国'); H:= TextHeight('国');
    repeat
      if (LineCount > 0)and(Length(lst[I]) > LineCount) then begin
        if WordWrap then lst.Insert(I + 1, Copy(lst[I], LineCount + 1, MaxInt));
        lst[I]:= Copy(lst[I], 1, LineCount);
      end;
      Inc(I);
      if Length(lst[I]) > maxCnt then maxCnt:= Length(lst[I]);
    until I >= lst.Count;
    //根据整理好的文字定义出原始图形尺寸
    szBmp.Width:= maxCnt * W;
    if maxCnt > 0 then szBmp.Width:= szBmp.Width + (maxCnt - 1) * WordSpacing;
    szBmp.Height:= Round(lst.Count * H);
    if lst.Count > 0 then szBmp.Height:= szBmp.Height + (lst.Count - 1) * LineSpacing;
    //开始画图
    FillRect(Rect(0, 0, szBmp.Width, szBmp.Height));
    PY:= 0;
    case FHAlign of
    0: for i:= 1 to lst.Count - 1 do begin                                      //左对齐
        PX:= 0;
        for J := 1 to Length(lst[I]) do begin
          TextOut(PX, PY, lst[I][J]);
          Inc(PX, WordSpacing + TextWidth(lst[I][J]));
        end;
        Inc(PY, H);
      end;
    1: for i:= 1 to lst.Count - 1 do begin                                      //居中
        PX:= Round(szBmp.Width - (Length(lst[I]) / 2) * (w + WordSpacing) - WordSpacing);
        for J := 1 to Length(lst[I]) do begin
          TextOut(PX, PY, lst[I][J]);
          Inc(PX, WordSpacing + TextWidth(lst[I][J]));
        end;
        Inc(PY, H);
      end;
    2: for i:= 1 to lst.Count - 1 do begin                                      //右对齐
        PX:= szBmp.Width;
        for J := Length(lst[I]) downto 1 do begin
          Dec(PX, TextWidth(lst[I][J]));
          TextOut(PX, PY, lst[I][J]);
          Dec(PX, WordSpacing);
        end;
        Inc(PY, H);
      end;
    3: for i:= 1 to lst.Count - 1 do begin                                      //两端对齐
        PX:= 0; C:= szBmp.Width / Length(lst[I]);
        for J := Length(lst[I]) downto 1 do begin
          TextOut(PX, PY, lst[I][J]);
          PX:= Round(C * J);
        end;
        Inc(PY, H);
      end;
    end;
    if FRotate <> 0 then RotateBmp(FRotate, szBmp, Fbmp)                        //旋转
    else Fbmp.Assign(szBmp);
  finally
    szBmp.Free;
  end;
end;

procedure TTextStyle.RotateBmp(RotateType: Integer; srcBmp, destBmp: TBitmap);
var
  X, Y: Integer;
  procedure InitDest(W, H: Integer);
  begin
    destBmp.Width:= W;
    destBmp.Height:= H;
    destBmp.Canvas.FillRect(Rect(0, 0, W, H));
  end;
begin
  case RotateType of
  0: begin                                //不旋转
      InitDest(srcBmp.Width, srcBmp.Height);
      destBmp.Assign(srcBmp);
    end;
  1: begin                                //90度旋转
      InitDest(srcBmp.Height, srcBmp.Width);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[srcBmp.Height - 1 - Y, X]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  2: begin                                //180度旋转
      InitDest(srcBmp.Width, srcBmp.Height);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[srcBmp.Width - 1 - X, srcBmp.Height - Y - 1]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  3: begin                                //270度旋转
      InitDest(srcBmp.Height, srcBmp.Width);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[Y, srcBmp.Width - 1 - X]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  4: begin                                //水平翻转
      InitDest(srcBmp.Width, srcBmp.Height);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[srcBmp.Width - 1 - X, Y]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  5: begin                                //90度翻转
      InitDest(srcBmp.Height, srcBmp.Width);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[Y,X]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  6: begin                                //垂直翻转
      InitDest(srcBmp.Width, srcBmp.Height);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[X, srcBmp.Height - Y - 1]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  7: begin                                //270度翻转
      InitDest(srcBmp.Height, srcBmp.Width);
      for X := 0 to srcBmp.Width - 1 do
        for Y := 0 to srcBmp.Height - 1 do
          destBmp.Canvas.Pixels[srcBmp.Height - Y - 1, srcBmp.Width - 1 - X]:= srcBmp.Canvas.Pixels[X,Y];
    end;
  end;
end;

procedure TTextStyle.LoadFromConfig(Cfg: TVonConfig; Section, Key: string);
begin
  HAlign:= Cfg.ReadInteger(Section, Key + '_HAlign', 0);
  VAlign:= Cfg.ReadInteger(Section, Key + '_VAlign', 0);
  FontSize:= Cfg.ReadInteger(Section, Key + '_FontSize', 10);
  FontItalic:= Cfg.ReadBool(Section, Key + '_FontItalic', false);
  FontStrikeOut:= Cfg.ReadBool(Section, Key + '_FontStrikeOut', false);
  FontUnderline:= Cfg.ReadBool(Section, Key + '_FontUnderline', false);
  FontBold:= Cfg.ReadBool(Section, Key + '_FontBold', false);
  Rotate:= Cfg.ReadInteger(Section, Key + '_Rotate', 0);
  FontName:= Cfg.ReadString(Section, Key + '_FontName', '宋体');
  FontColor:= Cfg.ReadInteger(Section, Key + '_FontColor', 0);
  BackColor:= Cfg.ReadInteger(Section, Key + '_BackColor', 0);
  WordWrap:= Cfg.ReadBool(Section, Key + '_WordWrap', false);
  Width:= Cfg.ReadInteger(Section, Key + '_Width', 0);
  Height:= Cfg.ReadInteger(Section, Key + '_Height', 0);
  WordSpacing:= Cfg.ReadInteger(Section, Key + '_WordSpacing', 0);
  LineCount:= Cfg.ReadInteger(Section, Key + '_LineCount', 0);
  LineSpacing:= Cfg.ReadInteger(Section, Key + '_LineSpacing', 0);
end;

procedure TTextStyle.SaveConfig(Cfg: TVonConfig; Section, Key: string);
begin
  Cfg.WriteInteger(Section, Key + '_HAlign', HAlign);
  Cfg.WriteInteger(Section, Key + '_VAlign', VAlign);
  Cfg.WriteInteger(Section, Key + '_FontSize', FontSize);
  Cfg.WriteBool(Section, Key + '_FontItalic', FontItalic);
  Cfg.WriteBool(Section, Key + '_FontStrikeOut', FontStrikeOut);
  Cfg.WriteBool(Section, Key + '_FontUnderline', FontUnderline);
  Cfg.WriteBool(Section, Key + '_FontBold', FontBold);
  Cfg.WriteInteger(Section, Key + '_Rotate', Rotate);
  Cfg.WriteString(Section, Key + '_FontName', FontName);
  Cfg.WriteInteger(Section, Key + '_FontColor', FontColor);
  Cfg.WriteInteger(Section, Key + '_BackColor', BackColor);
  Cfg.WriteBool(Section, Key + '_WordWrap', WordWrap);
  Cfg.WriteInteger(Section, Key + '_Width', Width);
  Cfg.WriteInteger(Section, Key + '_Height', Height);
  Cfg.WriteInteger(Section, Key + '_WordSpacing', WordSpacing);
  Cfg.WriteInteger(Section, Key + '_LineCount', LineCount);
  Cfg.WriteInteger(Section, Key + '_LineSpacing', LineSpacing);
end;

end.
