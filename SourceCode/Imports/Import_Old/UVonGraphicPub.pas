unit UVonGraphicPub;

interface

uses windows, Graphics, Math, Classes, Registry, SysUtils, ExtCtrls;//,
//  scapegoatControl;

var
  WuCha: Integer;
  BlurSize: Integer;
  scaleSize:double;

const
  MaxPixelCount = 32768;
  regKey='Update';

type

  pRGBArray = ^TRGBArray;
  TRGBArray = array[0..MaxPixelCount - 1] of TRGBTriple;

procedure doReadyOfRect(bmp:tbitmap; var TmpRegion: array of TPoint);

procedure RGBChange(SrcBmp, DestBmp: Tbitmap; RedChange, GreenChange,
  BlueChange: integer; aRect: array of Tpoint; ty: boolean);

procedure BrightnessChange(SrcBmp, DestBmp: Tbitmap; ValueChange: integer;
  aRect: array of Tpoint; Ty: boolean);

procedure GrayAverage(Tdp: TscapegoatControl);

procedure GrayWeightAverage(Tdp: TscapegoatControl);

procedure ContrastChange(AddFlag: Boolean; Tdp: TscapegoatControl);

procedure Saturation(SrcBmp, DestBmp: TBitmap; Amount: Integer; aRect: array of Tpoint; Ty: boolean);

procedure NotColor(Tdp: TscapegoatControl);

procedure Exposure(Tdp: TscapegoatControl);

procedure Emboss(Tdp: TscapegoatControl);

procedure Engrave(Tdp:TscapegoatControl);

procedure Blur(Tdp:TscapegoatControl);

procedure Sharpen(Tdp:TscapegoatControl);

procedure Purple(Tdp:TscapegoatControl);

procedure Spooky(Tdp:TscapegoatControl);


procedure SizeImg(var DestBmp: Tbitmap; sourceBmp: Tbitmap; ZiseValue:
  Double);

function IsStrANumber(NumStr : string) : boolean;
function IsStrFloat(numStr:string):boolean;

function GetMaxRectOfRegion(Region:array of tpoint):Trect;

procedure changeImgSize(img:Timage;scale:double);

procedure cutImg(img:Timage;aRect:trect);

implementation

procedure cutImg(img:Timage;aRect:TRect);
var
  tmpBmp:Tbitmap;
begin
  tmpBmp:=tbitmap.Create;
  tmpBmp.Width :=arect.Right-arect.Left;
  tmpBmp.Height :=arect.Bottom-arect.Top;
  try
    tmpBmp.Canvas.CopyRect(rect(0, 0, tmpBmp.Width, tmpBmp.Height),
      img.Picture.Bitmap.Canvas,arect);

    img.Picture.Bitmap.Assign(tmpBmp);
  finally
    tmpBmp.free;
  end;

end;

procedure doReadyOfRect(bmp:tbitmap;var TmpRegion:array of Tpoint);
begin
  TmpRegion[0].X :=0;
  TmpRegion[0].y :=0;
  TmpRegion[1].X :=bmp.Width ;
  TmpRegion[1].y :=0;
  TmpRegion[2].X :=bmp.Width ;
  TmpRegion[2].y :=bmp.height ;
  TmpRegion[3].X :=0;
  TmpRegion[3].y :=bmp.height;
end;

procedure changeImgSize(img:Timage;scale:double);
begin
  if scale=1 then
  begin
    img.AutoSize:=true;
    img.Stretch :=true;
  end
  else
  begin
    img.AutoSize:=false;
    img.Stretch :=true;
    img.Width :=round(img.Picture.Width * scale);
    img.Height :=round(img.Picture.Height * scale);
  end;
end;

function IsStrFloat(numStr:string):boolean;
begin
  result := true;
  try
    if StrTofloat(NumStr)<=0 then
      Result:=False;
  except
    result := false;
  end;
end;

function IsStrANumber(NumStr : string) : Boolean;
begin
  result := true;
  try
    if StrToInt(NumStr)<0 then
      Result:=False;
  except
    result := false;
  end;
end;


//改变图象的大小

procedure SizeImg(var DestBmp: Tbitmap; sourceBmp: Tbitmap; ZiseValue:
  Double);
begin
  DestBmp.Width := round(sourceBmp.Width * ZiseValue);
  DestBmp.Height := round(sourceBmp.Height * ZiseValue);

  DestBmp.Canvas.Stretchdraw(rect(0, 0, DestBmp.Width, DestBmp.Height),
    sourceBmp);
end;

//调整RGB,SrcBmp输入，DestBmp输出
procedure RGBChange(SrcBmp, DestBmp: Tbitmap; RedChange, GreenChange,
  BlueChange: integer;aRect:array of Tpoint;ty:boolean);
var
  SrcRow, DestRow: pRGBArray;
  i, j: integer;
  TmpRect:Trect;
  rgn:HRGN;
begin
  if Ty then
    rgn:=CreateEllipticRgn(aRect[0].X, aRect[0].y,aRect[1].X, aRect[1].y)
  else
    rgn:=CreatepolygonRgn(aRect[0], High(aRect) + 1, ALTERNATE);
  TmpRect:=GetMaxRectOfRegion(aRect);

  for i := TmpRect.top to TmpRect.Bottom-1 do
  begin
    SrcRow := SrcBmp.ScanLine[i];
    DestRow := DestBmp.ScanLine[i];
    for j := TmpRect.Left to TmpRect.Right-1 do
    begin
      if PtInRegion(rgn,j,i) then
      begin
        if RedChange > 0 then
          DestRow[j].rgbtRed := Min(255, SrcRow[j].rgbtRed + RedChange)
        else
          DestRow[j].rgbtRed := Max(0, SrcRow[j].rgbtRed + RedChange);

        if GreenChange > 0 then
          DestRow[j].rgbtGreen := Min(255, SrcRow[j].rgbtGreen + GreenChange)
        else
          DestRow[j].rgbtGreen := Max(0, SrcRow[j].rgbtGreen + GreenChange);

        if BlueChange > 0 then
          DestRow[j].rgbtBlue := Min(255, SrcRow[j].rgbtBlue + BlueChange)
        else
          DestRow[j].rgbtBlue := Max(0, SrcRow[j].rgbtBlue + BlueChange);
      end;
    end;
  end;
end;


//调整亮度，SrcBmp输入，DestBmp输出
procedure BrightnessChange(SrcBmp, DestBmp: Tbitmap;
  ValueChange: integer;  aRect:array of Tpoint;Ty:boolean);
var
  i, j: integer;
  SrcRow, DestRow: pRGBArray;
  TmpRect:Trect;
  rgn:HRGN;
begin
  if Ty then
    rgn:=CreateEllipticRgn(aRect[0].X, aRect[0].y,aRect[1].X, aRect[1].y)
  else
    rgn:=CreatepolygonRgn(aRect[0], High(aRect) + 1, ALTERNATE);

  TmpRect:=GetMaxRectOfRegion(aRect);

  for i := TmpRect.Top  to TmpRect.Bottom - 1 do
  begin
    SrcRow := SrcBmp.ScanLine[i];
    DestRow := DestBmp.ScanLine[i];
    for j := TmpRect.Left  to TmpRect.Right - 1 do
    begin
      if PtInRegion(rgn,j,i) then
      begin
        if ValueChange > 0 then
        begin
          DestRow[j].rgbtRed := Min(255, SrcRow[j].rgbtRed + ValueChange);
          DestRow[j].rgbtGreen := Min(255, SrcRow[j].rgbtGreen + ValueChange);
          DestRow[j].rgbtBlue := Min(255, SrcRow[j].rgbtBlue + ValueChange);
        end
        else
        begin
          DestRow[j].rgbtRed := Max(0, SrcRow[j].rgbtRed + ValueChange);
          DestRow[j].rgbtGreen := Max(0, SrcRow[j].rgbtGreen + ValueChange);
          DestRow[j].rgbtBlue := Max(0, SrcRow[j].rgbtBlue + ValueChange);
        end;
      end;
    end;
  end;

  deleteobject(rgn);
end;


//调整对比度
procedure ContrastChange(AddFlag: Boolean;Tdp:TscapegoatControl);
var
  X, Y: Integer;
  SrcRow: pRGBArray;
  TmpRect:Trect;
  rgn:HRGN;
begin
  aSetrect(tdp);

  if tdp.YXFlg then
    rgn:=CreateEllipticRgn(TmpRegion[0].X, TmpRegion[0].y,TmpRegion[1].X, TmpRegion[1].y)
  else
    rgn:=CreatepolygonRgn(TmpRegion[0], High(TmpRegion) + 1, ALTERNATE);

  TmpRect:=GetMaxRectOfRegion(TmpRegion);

  //增加对比度
  if AddFlag then
  begin
    for y := TmpRect.top to TmpRect.Bottom-1 do
    begin
      SrcRow := tdp.AControl.Picture.Bitmap.ScanLine[y];

      for x := TmpRect.Left to TmpRect.Right-1 do
      begin
        if PtInRegion(rgn,x,y) then
        begin
          if SrcRow[x].rgbtRed >= 128 then
            SrcRow[x].rgbtRed := min(255, SrcRow[x].rgbtRed + 3)
          else
            SrcRow[x].rgbtRed := max(0, SrcRow[x].rgbtRed - 3);

          if SrcRow[x].rgbtGreen >= 128 then
            SrcRow[x].rgbtGreen := min(255, SrcRow[x].rgbtGreen + 3)
          else
            SrcRow[x].rgbtGreen := max(0, SrcRow[x].rgbtGreen - 3);

          if SrcRow[x].rgbtBlue >= 128 then
            SrcRow[x].rgbtBlue := min(255, SrcRow[x].rgbtBlue + 3)
          else
            SrcRow[x].rgbtBlue := max(0, SrcRow[x].rgbtBlue - 3);
        end;
      end;
    end;
  end
    //减小对比度
  else
  begin
    for y := TmpRect.top to TmpRect.Bottom-1 do
    begin
      SrcRow := tdp.AControl.Picture.Bitmap.ScanLine[y];

      for x := TmpRect.Left to TmpRect.Right-1 do
      begin
        if PtInRegion(rgn,x,y) then
        begin
          if SrcRow[x].rgbtRed >= 128 then
            Max(128, SrcRow[x].rgbtRed - 3)
          else
            SrcRow[x].rgbtRed := min(128, SrcRow[x].rgbtRed + 3);

          if SrcRow[x].rgbtGreen >= 128 then
            Max(128, SrcRow[x].rgbtGreen - 3)
          else
            SrcRow[x].rgbtGreen := min(128, SrcRow[x].rgbtGreen + 3);

          if SrcRow[x].rgbtBlue >= 128 then
            Max(128, SrcRow[x].rgbtBlue - 3)
          else
            SrcRow[x].rgbtBlue := min(128, SrcRow[x].rgbtBlue + 3);
        end;
      end;
    end;
  end;
  tdp.AControl.Refresh;
end;


procedure Saturation(SrcBmp, DestBmp: TBitmap; Amount: Integer;aRect:array of Tpoint;Ty:boolean);
var
  Grays: array[0..767] of Integer;
  Alpha: array[0..255] of Word;
  Gray, X, Y: Integer;
  SrcRow, DestRow: pRGBArray;
  I: Byte;
  TmpRect:Trect;
  rgn:HRGN;
begin
  for I := 0 to 255 do
    Alpha[I] := (I * Amount) shr 8;
  x := 0;

  for I := 0 to 255 do
  begin
    Gray := I - Alpha[I];
    Grays[X] := Gray; Inc(X);
    Grays[X] := Gray; Inc(X);
    Grays[X] := Gray; Inc(X);
  end;

  if Ty then
    rgn:=CreateEllipticRgn(aRect[0].X, aRect[0].y,aRect[1].X, aRect[1].y)
  else
    rgn:=CreatepolygonRgn(aRect[0], High(aRect) + 1, ALTERNATE);

  TmpRect:=GetMaxRectOfRegion(aRect);

  for y := TmpRect.top to TmpRect.Bottom-1 do
  begin
    SrcRow := SrcBmp.ScanLine[Y];
    DestRow := DestBmp.ScanLine[Y];
    for x := TmpRect.Left to TmpRect.Right-1 do
    begin
      if PtInRegion(rgn,x,y) then
      begin
        Gray := Grays[SrcRow[x].rgbtRed + SrcRow[x].rgbtGreen +
          SrcRow[x].rgbtBlue];
        DestRow[x].rgbtRed := Byte(Gray + Alpha[SrcRow[x].rgbtRed]);
        DestRow[x].rgbtGreen := Byte(Gray + Alpha[SrcRow[x].rgbtGreen]);
        DestRow[x].rgbtBlue := Byte(Gray + Alpha[SrcRow[x].rgbtBlue]);
      end;
    end;
  end;
end;

//平均灰度化
procedure GrayAverage(Tdp:TscapegoatControl);
var
  i, j: integer;
  SrcRow: pRGBArray;
  AverageValue: integer;
  rgn:HRGN;
  TmpRect:Trect;
begin
  aSetrect(tdp);

  if tdp.YXFlg then
    rgn:=CreateEllipticRgn(TmpRegion[0].X, TmpRegion[0].y,TmpRegion[1].X, TmpRegion[1].y)
  else
    rgn:=CreatepolygonRgn(TmpRegion[0], High(TmpRegion) + 1, ALTERNATE);

  TmpRect:=GetMaxRectOfRegion(TmpRegion);
  
  for i := TmpRect.top to TmpRect.Bottom -1 do
  begin
    SrcRow := tdp.AControl.Picture.Bitmap.ScanLine[i];
    for j := TmpRect.Left  to TmpRect.Right -1 do
    begin
      if PtInRegion(rgn,j,i) then
      begin
        AverageValue := (SrcRow[j].rgbtRed + SrcRow[j].rgbtGreen +
          SrcRow[j].rgbtBlue) div 3;

        SrcRow[j].rgbtRed := AverageValue;
        SrcRow[j].rgbtGreen := AverageValue;
        SrcRow[j].rgbtBlue := AverageValue;
      end;
    end;
  end;
  tdp.AControl.Refresh;
end;

//加权平均灰度化
procedure GrayWeightAverage(Tdp:TscapegoatControl);
var
  i, j: integer;
  SrcRow: pRGBArray;
  WeightAverageValue: integer;
  TmpRect:Trect;
  rgn:HRGN;
begin
  asetRect(Tdp);
  
  if tdp.YXFlg then
    rgn:=CreateEllipticRgn(TmpRegion[0].X, TmpRegion[0].y,TmpRegion[1].X, TmpRegion[1].y)
  else
    rgn:=CreatepolygonRgn(TmpRegion[0], High(TmpRegion) + 1, ALTERNATE);

  TmpRect:=GetMaxRectOfRegion(TmpRegion);

  for i := TmpRect.top to TmpRect.Bottom -1do
  begin
    SrcRow := tdp.AControl.Picture.Bitmap.ScanLine[i];
    for j := TmpRect.Left to TmpRect.Right-1 do
    begin
      if PtInRegion(rgn,j,i) then
      begin
        WeightAverageValue := trunc(SrcRow[j].rgbtRed * 0.31 + SrcRow[j].rgbtGreen
          * 0.59 + SrcRow[j].rgbtBlue * 0.11);

        SrcRow[j].rgbtRed := WeightAverageValue;
        SrcRow[j].rgbtGreen := WeightAverageValue;
        SrcRow[j].rgbtBlue := WeightAverageValue;
      end;
    end;
  end;
  tdp.AControl.Refresh;
end;

//反色，即：底片效果
procedure NotColor(Tdp:TscapegoatControl);
var
  i, j: integer;
  SrcRow: pRGBArray;
  TmpRect:Trect;
  rgn:HRGN;
begin
  asetrect(tdp);

  if tdp.YXFlg then
    rgn:=CreateEllipticRgn(TmpRegion[0].X, TmpRegion[0].y,TmpRegion[1].X, TmpRegion[1].y)
  else
    rgn:=CreatepolygonRgn(TmpRegion[0], High(TmpRegion) + 1, ALTERNATE);

  TmpRect:=GetMaxRectOfRegion(TmpRegion);

  for i := TmpRect.top to TmpRect.Bottom-1 do
  begin
//    SrcRow := SrcBmp.ScanLine[i];
    SrcRow := tdp.AControl.Picture.Bitmap.ScanLine[i];

    for j := TmpRect.Left to TmpRect.Right-1 do
    begin
      if PtInRegion(rgn,j,i) then
      begin
        SrcRow[j].rgbtRed := not SrcRow[j].rgbtRed;
        SrcRow[j].rgbtGreen := not SrcRow[j].rgbtGreen;
        SrcRow[j].rgbtBlue := not SrcRow[j].rgbtBlue;
      end;
    end;
  end;
  Tdp.AControl.Refresh;
end;

//曝光
procedure Exposure(Tdp:TscapegoatControl);
var
  i, j: integer;
  SrcRow: pRGBArray;
  TmpRect:Trect;
  rgn:HRGN;
begin
  asetrect(tdp);
  
  if tdp.YXFlg then
    rgn:=CreateEllipticRgn(TmpRegion[0].X, TmpRegion[0].y,TmpRegion[1].X, TmpRegion[1].y)
  else
    rgn:=CreatepolygonRgn(TmpRegion[0], High(TmpRegion) + 1, ALTERNATE);

  TmpRect:=GetMaxRectOfRegion(TmpRegion);

  for i := TmpRect.top to TmpRect.Bottom-1 do
  begin
    SrcRow := tdp.AControl.Picture.Bitmap.ScanLine[i];
    for j := TmpRect.Left to TmpRect.Right-1 do
    begin
      if PtInRegion(rgn,j,i) then
      begin
        if SrcRow[j].rgbtRed < 128 then
          SrcRow[j].rgbtRed := not SrcRow[j].rgbtRed;

        if SrcRow[j].rgbtGreen < 128 then
          SrcRow[j].rgbtGreen := not SrcRow[j].rgbtGreen;

        if SrcRow[j].rgbtBlue < 128 then
          SrcRow[j].rgbtBlue := not SrcRow[j].rgbtBlue;
      end;
    end;
  end;
  Tdp.AControl.Refresh;
end;

//浮雕
procedure Emboss(Tdp:TscapegoatControl);
var
  i, j: integer;
  SrcRow: pRGBArray;
  SrcNextRow: pRGBArray;
  Value: integer;
  TmpRect:Trect;
  rgn:HRGN;
begin
  asetrect(tdp);
  
  if tdp.YXFlg then
    rgn:=CreateEllipticRgn(TmpRegion[0].X, TmpRegion[0].y,TmpRegion[1].X, TmpRegion[1].y)
  else
    rgn:=CreatepolygonRgn(TmpRegion[0], High(TmpRegion) + 1, ALTERNATE);

  TmpRect:=GetMaxRectOfRegion(TmpRegion);

  for i := TmpRect.top to TmpRect.Bottom-2 do
  begin
    SrcRow := tdp.AControl.Picture.Bitmap.ScanLine[i];
    SrcNextRow := tdp.AControl.Picture.Bitmap.ScanLine[i+1];
    for j := TmpRect.Left to TmpRect.Right-1 do
    begin
      if PtInRegion(rgn,j,i) then
      begin
        Value := SrcRow[j].rgbtRed - SrcNextRow[j + 1].rgbtRed + 128;
        Value := Max(0, Value);
        Value := Min(255, Value);
        SrcRow[j].rgbtRed := value;

        Value := SrcRow[j].rgbtGreen - SrcNextRow[j + 1].rgbtGreen + 128;
        Value := Max(0, Value);
        Value := Min(255, Value);
        SrcRow[j].rgbtGreen := value;

        Value := SrcRow[j].rgbtBlue - SrcNextRow[j + 1].rgbtBlue + 128;
        Value := Max(0, Value);
        Value := Min(255, Value);
        SrcRow[j].rgbtBlue := value;
      end;
    end;
  end;
  Tdp.AControl.Refresh;
end;

//雕刻
procedure Engrave(Tdp:TscapegoatControl);
var
  i, j: integer;
  SrcRow: pRGBArray;
  SrcNextRow: pRGBArray;
  Value: integer;
  TmpRect:Trect;
  rgn:HRGN;
begin
  asetrect(tdp);
  
  if tdp.YXFlg then
    rgn:=CreateEllipticRgn(TmpRegion[0].X, TmpRegion[0].y,TmpRegion[1].X, TmpRegion[1].y)
  else
    rgn:=CreatepolygonRgn(TmpRegion[0], High(TmpRegion) + 1, ALTERNATE);

  TmpRect:=GetMaxRectOfRegion(TmpRegion);

  for i := TmpRect.top to TmpRect.Bottom-2 do
  begin
    SrcRow := tdp.AControl.Picture.Bitmap.ScanLine[i];
    SrcNextRow := tdp.AControl.Picture.Bitmap.ScanLine[i+1];

    for j := TmpRect.Left to TmpRect.Right-2 do
    begin
      if PtInRegion(rgn,j,i) then
      begin
        Value := SrcNextRow[j + 1].rgbtRed - SrcRow[j].rgbtRed + 128;
        Value := Max(0, Value);
        Value := Min(255, Value);
        SrcRow[j].rgbtRed := value;

        Value := SrcNextRow[j + 1].rgbtGreen - SrcRow[j].rgbtGreen + 128;
        Value := Max(0, Value);
        Value := Min(255, Value);
        SrcRow[j].rgbtGreen := value;

        Value := SrcNextRow[j + 1].rgbtBlue - SrcRow[j].rgbtBlue + 128;
        Value := Max(0, Value);
        Value := Min(255, Value);
        SrcRow[j].rgbtBlue := value;
      end;
    end;
  end;
  Tdp.AControl.Refresh;
end;


function GetMaxRectOfRegion(Region:array of tpoint):Trect;
var
  i:integer;
  MaxX,MaxY,MinX,MinY:integer;
begin
  MaxX:=0;
  MaxY:=0;
  MinX:=65535;
  MinY:=65535;

  for i:=0 to length(Region)-1 do
  begin
    maxX:=max(maxX,Region[i].X);
    maxY:=max(maxY,Region[i].Y);
    minX:=min(minX,Region[i].X);
    miny:=min(MinY,Region[i].Y);
  end;

  result:=rect(minx,miny,maxX,MaxY);

end;

//模糊
procedure Blur(Tdp:TscapegoatControl);
var
  i, j: integer;
  SrcRow: pRGBArray;
  SrcNextRow: pRGBArray;
  SrcPreRow: pRGBArray;
  Value: integer;
  TmpRect:Trect;
  rgn:HRGN;
begin
  asetrect(tdp);

  if tdp.YXFlg then
    rgn:=CreateEllipticRgn(TmpRegion[0].X, TmpRegion[0].y,TmpRegion[1].X, TmpRegion[1].y)
  else
    rgn:=CreatepolygonRgn(TmpRegion[0], High(TmpRegion) + 1, ALTERNATE);

  TmpRect:=GetMaxRectOfRegion(TmpRegion);

  TmpRect.Top:=max(TmpRect.Top,1);
  TmpRect.Bottom:=min(TmpRect.Bottom,tdp.AControl.Picture.Bitmap.Height-1 );
  TmpRect.Left:=max(TmpRect.Left,1);
  TmpRect.Right:=min(TmpRect.Right,tdp.AControl.Picture.Bitmap.Width);

  for i := TmpRect.top + 1 to TmpRect.Bottom-1 do
  begin
    SrcRow := tdp.AControl.Picture.Bitmap.ScanLine[i];
    SrcPreRow := tdp.AControl.Picture.Bitmap.ScanLine[i-1];
    SrcNextRow := tdp.AControl.Picture.Bitmap.ScanLine[i+1];

    for j := TmpRect.Left to TmpRect.Right do
    begin
      if PtInRegion(rgn,j,i) then
      begin
        Value := (SrcPreRow[j - 1].rgbtRed + SrcRow[j - 1].rgbtRed + SrcNextRow[j
          - 1].rgbtRed + SrcPreRow[j].rgbtRed + SrcRow[j].rgbtRed +
            SrcNextRow[j].rgbtRed + SrcPreRow[j + 1].rgbtRed + SrcRow[j +
          1].rgbtRed + SrcNextRow[j + 1].rgbtRed) div 9;

        SrcRow[j].rgbtRed := value;

        Value := (SrcPreRow[j - 1].rgbtGreen + SrcRow[j - 1].rgbtGreen +
          SrcNextRow[j - 1].rgbtGreen + SrcPreRow[j].rgbtGreen +
          SrcRow[j].rgbtGreen + SrcNextRow[j].rgbtGreen + SrcPreRow[j +
          1].rgbtGreen + SrcRow[j + 1].rgbtGreen + SrcNextRow[j + 1].rgbtGreen)
          div 9;

        SrcRow[j].rgbtGreen := value;


        Value := (SrcPreRow[j - 1].rgbtBlue + SrcRow[j - 1].rgbtBlue + SrcNextRow[j
          - 1].rgbtBlue + SrcPreRow[j].rgbtBlue + SrcRow[j].rgbtBlue +
            SrcNextRow[j].rgbtBlue + SrcPreRow[j + 1].rgbtBlue + SrcRow[j +
          1].rgbtBlue + SrcNextRow[j + 1].rgbtBlue) div 9;

        SrcRow[j].rgbtBlue := value;
      end;
    end;
  end;
  Tdp.AControl.Refresh;
end;

//锐化

procedure Sharpen(Tdp:TscapegoatControl);
var
  i, j: integer;
  SrcRow: pRGBArray;
  SrcPreRow: pRGBArray;
  Value: integer;
  TmpRect:Trect;
  rgn:HRGN;
begin
  asetrect(tdp);
  if tdp.YXFlg then
    rgn:=CreateEllipticRgn(TmpRegion[0].X, TmpRegion[0].y,TmpRegion[1].X, TmpRegion[1].y)
  else
    rgn:=CreatepolygonRgn(TmpRegion[0], High(TmpRegion) + 1, ALTERNATE);

  TmpRect:=GetMaxRectOfRegion(TmpRegion);

  for i := TmpRect.top+1 to TmpRect.Bottom-1 do
  begin

    SrcRow := tdp.AControl.Picture.Bitmap.ScanLine[i];
    SrcPreRow := tdp.AControl.Picture.Bitmap.ScanLine[i - 1];

    for j := TmpRect.Left+1 to TmpRect.Right-1 do
    begin
      if PtInRegion(rgn,j,i) then
      begin
        Value := SrcRow[j].rgbtRed + (SrcRow[j].rgbtRed - SrcPreRow[j - 1].rgbtRed)
          div 2;
        Value := Max(0, Value);
        Value := Min(255, Value);
        SrcRow[j].rgbtRed := value;

        Value := SrcRow[j].rgbtGreen + (SrcRow[j].rgbtGreen - SrcPreRow[j -
          1].rgbtGreen) div 2;
        Value := Max(0, Value);
        Value := Min(255, Value);
        SrcRow[j].rgbtGreen := value;

        Value := SrcRow[j].rgbtBlue + (SrcRow[j].rgbtBlue - SrcPreRow[j -
          1].rgbtBlue) div 2;
        Value := Max(0, Value);
        Value := Min(255, Value);
        SrcRow[j].rgbtBlue := value;
      end;
    end;
  end;
  Tdp.AControl.Refresh;
end;

//紫色滤镜
procedure Purple(Tdp:TscapegoatControl);
var
  i, j: integer;
  SrcRow: pRGBArray;
  TmpRect:Trect;
  rgn:HRGN;
begin
  asetrect(tdp);
  if tdp.YXFlg then
    rgn:=CreateEllipticRgn(TmpRegion[0].X, TmpRegion[0].y,TmpRegion[1].X, TmpRegion[1].y)
  else
    rgn:=CreatepolygonRgn(TmpRegion[0], High(TmpRegion) + 1, ALTERNATE);

  TmpRect:=GetMaxRectOfRegion(TmpRegion);

  for i := TmpRect.top to TmpRect.Bottom -1do
  begin
    SrcRow:=tdp.AControl.Picture.Bitmap.ScanLine[i];
    for j := TmpRect.Left to TmpRect.Right -1do
    begin
      if PtInRegion(rgn,j,i) then
      begin
        SrcRow[j].rgbtRed := (SrcRow[j].rgbtGreen + SrcRow[j].rgbtRed) div 2;
        SrcRow[j].rgbtGreen := (SrcRow[j].rgbtBlue + SrcRow[j].rgbtGreen) div 2;
        SrcRow[j].rgbtBlue := (SrcRow[j].rgbtRed + SrcRow[j].rgbtBlue) div 2;
      end;
    end;
  end;
  
  Tdp.AControl.Refresh;
end;

procedure Spooky(Tdp:TscapegoatControl);
var
  i, j: integer;
  SrcRow: pRGBArray;
  TmpRect:Trect;
  rgn:HRGN;
begin
  asetrect(tdp);
  
  if tdp.YXFlg then
    rgn:=CreateEllipticRgn(TmpRegion[0].X, TmpRegion[0].y,TmpRegion[1].X, TmpRegion[1].y)
  else
    rgn:=CreatepolygonRgn(TmpRegion[0], High(TmpRegion) + 1, ALTERNATE);

  TmpRect:=GetMaxRectOfRegion(TmpRegion);

  for i := TmpRect.top to TmpRect.Bottom-1 do
  begin
    SrcRow:=tdp.AControl.Picture.Bitmap.ScanLine[i];
    for j := TmpRect.Left to TmpRect.Right-1 do
    begin
      if PtInRegion(rgn,j,i) then
      begin
        SrcRow[j].rgbtGreen := (SrcRow[j].rgbtRed + SrcRow[j].rgbtGreen) div 2;
        SrcRow[j].rgbtBlue := (SrcRow[j].rgbtRed + SrcRow[j].rgbtBlue) div 2;
      end;
    end;
  end;
  Tdp.AControl.Refresh;
end;


end.

