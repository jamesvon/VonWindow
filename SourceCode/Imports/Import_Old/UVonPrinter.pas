unit UVonPrinter;

interface

uses SysUtils, Variants, Classes, Graphics, Controls, ComObj, Forms, ADODB, DB,
  Windows, Dialogs, UVonLog, UVonConfig, Printers;

procedure PrintText(X, Y: Extended; Txt: string; FontSize: Integer = 12);

implementation

function CharHeight: Word;
var
  Metrics: TTextMetric;
begin
  GetTextMetrics(Printer.Canvas.Handle, Metrics);
  Result := Metrics.tmHeight;
end;

// 取得字符的平均宽度
function AvgCharWidth: Word;
var
  Metrics: TTextMetric;
begin
  GetTextMetrics(Printer.Canvas.Handle, Metrics);
  Result := Metrics.tmAveCharWidth;
end;

// 取得纸张的物理尺寸---单位：点
function GetPhicalPaper: TPoint;
var
  PageSize: TPoint;
begin
  // PageSize.X; 纸张物理宽度-单位:点
  // PageSize.Y; 纸张物理高度-单位:点
  Escape(Printer.Handle, GETPHYSPAGESIZE, 0, nil, @PageSize);
  Result := PageSize;
end;

// 2.取得纸张的逻辑宽度--可打印区域
// 取得纸张的逻辑尺寸
function PaperLogicSize: TPoint;
var
  APoint: TPoint;
begin
  APoint.X := Printer.PageWidth;
  APoint.Y := Printer.PageHeight;
  Result := APoint;
end;

// 纸张水平对垂直方向的纵横比例
function HVLogincRatio: Extended;
var
  AP: TPoint;
begin
  AP := PaperLogicSize;
  Result := AP.Y / AP.X;
end;

// 取得纸张的横向偏移量-单位：点
function GetOffSetX: Integer;
begin
  Result := GetDeviceCaps(Printer.Handle, PhysicalOffSetX);
end;

// 取得纸张的纵向偏移量-单位：点
function GetOffSetY: Integer;
begin
  Result := GetDeviceCaps(Printer.Handle, PhysicalOffSetY);
end;

// 毫米单位转换为英寸单位
function MmToInch(Length: Extended): Extended;
begin
  Result := Length / 25.4;
end;

// 英寸单位转换为毫米单位
function InchToMm(Length: Extended): Extended;
begin
  Result := Length * 25.4;
end;

// 取得水平方向每英寸打印机的点数
function HPointsPerInch: Integer;
begin
  Result := GetDeviceCaps(Printer.Handle, LOGPIXELSX);
end;

// 取得纵向方向每英寸打印机的光栅数
function VPointsPerInch: Integer;
begin
  Result := GetDeviceCaps(Printer.Handle, LOGPIXELSY)
end;

// 横向点单位转换为毫米单位
function XPointToMm(Pos: Integer): Extended;
begin
  Result := Pos * 25.4 / HPointsPerInch;
end;

// 纵向点单位转换为毫米单位
function YPointToMm(Pos: Integer): Extended;
begin
  Result := Pos * 25.4 / VPointsPerInch;
end;

// 设置纸张高度-单位：mm
procedure SetPaperHeight(Value: Integer);
var
  Device: array [0 .. 255] of char;
  Driver: array [0 .. 255] of char;
  Port: array [0 .. 255] of char;
  hDMode: THandle;
  PDMode: PDEVMODE;
begin
  // 自定义纸张最小高度127mm
  if Value < 127 then
    Value := 127;
  // 自定义纸张最大高度432mm
  if Value > 432 then
    Value := 432;
  Printer.PrinterIndex := Printer.PrinterIndex;
  Printer.GetPrinter(Device, Driver, Port, hDMode);
  if hDMode <> 0 then
  begin
    PDMode := GlobalLock(hDMode);
    if PDMode <> nil then
    begin
      PDMode^.dmFields := PDMode^.dmFields or DM_PAPERSIZE or DM_PAPERLENGTH;
      PDMode^.dmPaperSize := DMPAPER_USER;
      PDMode^.dmPaperLength := Value * 10;
      PDMode^.dmFields := PDMode^.dmFields or DMBIN_MANUAL;
      PDMode^.dmDefaultSource := DMBIN_MANUAL;
      GlobalUnlock(hDMode);
    end;
  end;
  Printer.PrinterIndex := Printer.PrinterIndex;
end;

// 设置纸张宽度：单位--mm
Procedure SetPaperWidth(Value: Integer);
var
  Device: array [0 .. 255] of char;
  Driver: array [0 .. 255] of char;
  Port: array [0 .. 255] of char;
  hDMode: THandle;
  PDMode: PDEVMODE;
begin
  // 自定义纸张最小宽度76mm
  if Value < 76 then
    Value := 76;
  // 自定义纸张最大宽度216mm
  if Value > 216 then
    Value := 216;
  Printer.PrinterIndex := Printer.PrinterIndex;
  Printer.GetPrinter(Device, Driver, Port, hDMode);
  if hDMode <> 0 then
  begin
    PDMode := GlobalLock(hDMode);
    if PDMode <> nil then
    begin
      PDMode^.dmFields := PDMode^.dmFields or DM_PAPERSIZE or DM_PAPERWIDTH;
      PDMode^.dmPaperSize := DMPAPER_USER;
      // 将毫米单位转换为0.1mm单位
      PDMode^.dmPaperWidth := Value * 10;
      PDMode^.dmFields := PDMode^.dmFields or DMBIN_MANUAL;
      PDMode^.dmDefaultSource := DMBIN_MANUAL;
      GlobalUnlock(hDMode);
    end;
  end;
  Printer.PrinterIndex := Printer.PrinterIndex;
end;

// 在 (Xmm, Ymm)处按指定配置文件信息和字体输出字符串
procedure PrintText(X, Y: Extended; Txt: string; FontSize: Integer = 12);
var
  OrX, OrY: Extended;
  Px, Py: Integer;
  AP: TPoint;
  FileName: string;
  OffSetX, OffSetY: Integer;
begin
  // 如果没有配置文件，则生成
  OffSetX:= 0;
  OffSetY:= 0;
  X := X + OffSetX;
  Y := Y + OffSetY;
  Px := Round(Round(X * HPointsPerInch * 10000 / 25.4) / 10000);
  Py := Round(Round(Y * VPointsPerInch * 10000 / 25.4) / 10000);
  Py := Py - GetOffSetY; // 因为是绝对坐标, 因此, 不用换算成相对于Y轴坐标
  Px := Px + 2 * AvgCharWidth;
  Printer.Canvas.Font.Name := '宋体';
  Printer.Canvas.Font.Size := FontSize;
  // Printer.Canvas.Font.Color := clGreen;
  Printer.Canvas.TextOut(Px, Py, Txt);
end;

end.
