unit UVonSMS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  IdGlobal, UVonLog, IdAntiFreezeBase, IdAntiFreeze;

type
  TVonSMS = class
  private
    FHttp: TIdHTTP;
    FParams: TStringList;
    AResponseContent: TStream;
    FURL: string;
    FParamFormat: string;
    procedure SetURLFormat(const Value: string);
    function GetURLFormat: string;
  public
    constructor Create;
    destructor Destroy;
    /// <summary>发送短信</summary>
    /// <param name="mobile">手机号码</param>
    /// <param name="msg">短信内容</param>
    /// <remark></remark>
    /// <see></see>
    procedure SendSMS(mobile, msg: string);
    procedure test(Msg: string);
  published
    /// <summary>短信Format格式，第一个默认参数是手机号码，第二个默认参数是短信内容</summary>
    /// <remark>http://210.5.158.31/hy?uid=501441&auth=d836f5c155809a69c4c123d6ac8e5eeb&mobile=%s&msg=%s&expid=0&encode=utf-8</remark>
    property URLFormat: string read GetURLFormat write SetURLFormat;
  end;

implementation

{ TVonSMS }

constructor TVonSMS.Create;
begin
  FHttp:= TIdHTTP.Create(Application);
  FParams:= TStringList.Create;
  FParams.Delimiter:= '&';
  AResponseContent:= TStringStream.Create;
end;

destructor TVonSMS.Destroy;
begin
  AResponseContent.Free;
  FParams.Free;
  FHttp.Free;
end;

function TVonSMS.GetURLFormat: string;
begin
  Result:= FURL;
  if FParamFormat <> '' then
    Result:= Result + '?' + FParamFormat;
end;

procedure TVonSMS.SendSMS(mobile, msg: string);
var
  I: Integer;
  msgToSend: RawByteString;
  S: string;
begin
  msgToSend:= UTF8Encode(Format(FParamFormat, [mobile, msg]));
  S:= '';
  for I := 1 to Length(msgToSend) do
    if (Pos(msgToSend[I], '*<>#%"{}|\^[]`') > 0) or (Ord(msgToSend[I]) < 33) or (Ord(msgToSend[I]) > 128) then
      S := S + '%' + IntToHex(Ord(msgToSend[I]), 2)  {do not localize}
    else S:= S + msgToSend[I];
  FParams.DelimitedText:= S;
  FHttp.Post(FURL, FParams, AResponseContent);
end;

procedure TVonSMS.SetURLFormat(const Value: string);
var
  Idx: Integer;
begin
  Idx:= Pos('?', Value);
  if Idx > 0 then begin
    FURL:= Copy(Value, 1, Idx - 1);
    FParamFormat:= Copy(Value, Idx + 1, MaxInt);
  end else begin
    FURL:= Value;
    FParamFormat:= '';
  end;
end;

procedure TVonSMS.test(Msg: string);
begin
  ShowMessage(FHttp.Get('https://www.baidu.com/s?ie=utf-8&f=3&rsv_bp=1&tn=baidu&wd=' +
    'indesign%E6%95%99%E7%A8%8B&oq=Indesign&rsv_pq=f1785f6900004338&rsv_t=' +
    '33acPZ6tNdUmENq81AtbpC%2BaTTqtddKA%2FdiEigFYTeCL%2Fh4kTm8bDE163RM&rsv_' +
    'enter=0&rsv_sug3=7&rsv_sug1=7&rsv_sug7=100&rsv_sug2=1&prefixsug=Indesign' +
    '&rsp=2&rsv_sug4=2284&rsv_sug=1'));

end;

end.
