unit UVonWord;

interface

uses SysUtils, Variants, Classes, Controls, Dialogs, OLE_Word_TLB;

type
  TVonWord = class
  private
    FWordApp: TWordApplication;
    FWordDoc: TWordDocument;
    FLastStyle: string;
    function GetLastRange: Rangee;
  public
    constructor Create;
    destructor Destroy;
    procedure New(TempFile: string);
    procedure Open(Filename: string);
    procedure Save(Filename: string; Converted: boolean = true);
    procedure ReplaceWord(orgValue, destValue: string);
    procedure WriteText(styleName, Text: string);
    procedure SetStyle(styleName: string);
    procedure NewPage;
    function NewTable(RowCount, ColCount: Integer): Table;
  published
    property WordApp: TWordApplication read FWordApp;
  end;

implementation

{ TVonWord }

procedure TVonWord.ReplaceWord(orgValue, destValue: string);
var
  FindText, MatchCase, MatchWholeWord, MatchWildcards, MatchSoundsLike,
  MatchAllWordForms, szForward, Wrap, Format, ReplaceWith, Replace: OleVariant;
begin
  FindText:= orgValue; ReplaceWith:= destValue;
  FWordApp.ActiveDocument.Range(EmptyParam, EmptyParam).Find.Execute(FindText,
    MatchCase, MatchWholeWord, MatchWildcards, MatchSoundsLike, MatchAllWordForms,
    szForward, Wrap, Format, ReplaceWith, Replace, EmptyParam, EmptyParam, EmptyParam, EmptyParam);
end;

procedure TVonWord.Save(Filename: string; Converted: boolean);
var
  szOle: OleVariant;
begin
  szOle:= FileName;
  if FileExists(Filename) then begin
    if Converted then
      DeleteFile(Filename)
    else if MessageDlg('文件已经存在是否覆盖？', mtWarning, mbYesNo, 0) = mrYES then
      DeleteFile(Filename)
    else Exit;
  end;
  FWordDoc.SaveAs(szOle);
end;

procedure TVonWord.SetStyle(styleName: string);
var
  szOle: OleVariant;
begin
  if styleName = '' then szOle:= '正文'
  else begin
    szOle:= styleName;
    FLastStyle:= styleName;
  end;
  FWordApp.Selection.Set_Style(FWordApp.ActiveDocument.Styles.Item(szOle));
end;

procedure TVonWord.WriteText(styleName, Text: string);
var
  szOle: OleVariant;
begin
  GetLastRange.Select;
  if styleName <> '' then begin
    szOle:= styleName;
    FWordApp.Selection.Set_Style(FWordApp.ActiveDocument.Styles.Item(szOle));
  end;
  FWordApp.Selection.TypeText(Text);
  SetStyle(FLastStyle);
end;

constructor TVonWord.Create;
begin
  FWordApp:= TWordApplication.Create(nil);
  FWordApp.Visible:= True;
  FLastStyle:= '正文';
end;

destructor TVonWord.Destroy;
begin
end;

function TVonWord.GetLastRange: Rangee;
var
  EndUnit, EndPos: OleVariant;
begin
  EndUnit:= wdStory;
  EndPos:= EmptyParam;
  FWordApp.Selection.EndKey(EndUnit, EndPos);
  Result:= FWordApp.Selection.Range;
end;

procedure TVonWord.New(TempFile: string);
var
  szOle: OleVariant;
begin
  szOle:= TempFile;
  FWordDoc:= FWordApp.Documents.Add(szOle, EmptyParam, EmptyParam, EmptyParam);
end;

procedure TVonWord.NewPage;
var
  szOle: OleVariant;
begin
  szOle:= wdPageBreak;
  FWordApp.Selection.InsertBreak(szOle);
end;

function TVonWord.NewTable(RowCount, ColCount: Integer): Table;
begin
  Result:= FWordApp.ActiveDocument.Tables.Add(FWordApp.Selection.Range,
    RowCount, ColCount, EmptyParam, EmptyParam);
end;

procedure TVonWord.Open(Filename: string);
var
  szOle: OleVariant;
begin
  szOle:= Filename;
//  var FileName: OleVariant; var ConfirmConversions: OleVariant;
//  var ReadOnly: OleVariant; var AddToRecentFiles: OleVariant;
//  var PasswordDocument: OleVariant; var PasswordTemplate: OleVariant;
//  var Revert: OleVariant; var WritePasswordDocument: OleVariant;
//  var WritePasswordTemplate: OleVariant; var Format: OleVariant;
//  var Encoding: OleVariant; var Visible: OleVariant; var OpenAndRepair: OleVariant;
//  var DocumentDirection: OleVariant; var NoEncodingDialog: OleVariant;
//  var XMLTransform: OleVariant)
  FWordDoc:= FWordApp.Documents.Open(szOle, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam,
    EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam);
end;

end.
