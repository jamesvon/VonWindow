unit UpdateLib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 34747 $
// File generated on 2013/4/10 18:46:20 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Windows\SysWOW64\wuwebv.dll (1)
// LIBID: {058F2991-6ADD-4948-89AF-82F58BCF2CCD}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  UpdateLibMajorVersion = 1;
  UpdateLibMinorVersion = 0;

  LIBID_UpdateLib: TGUID = '{058F2991-6ADD-4948-89AF-82F58BCF2CCD}';

  IID_IWUWebControl: TGUID = '{FB451BA6-8EF1-4816-BAEF-418315514939}';
  CLASS_WUWebControl: TGUID = '{12A66224-5E8A-4679-8941-0B9B960BF5EA}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum tagOSVersionField
type
  tagOSVersionField = TOleEnum;
const
  e_majorVer = $00000000;
  e_minorVer = $00000001;
  e_buildNumber = $00000002;
  e_platform = $00000003;
  e_SPMajor = $00000004;
  e_SPMinor = $00000005;
  e_productSuite = $00000006;
  e_productType = $00000007;
  e_systemMetric = $00000008;
  e_SPVersionString = $00000009;
  e_controlVersionString = $0000000A;
  e_VistaProductType = $0000000B;
  e_maxOSVersionFieldValue = $0000000B;

// Constants for enum tagOptInErrorContext
type
  tagOptInErrorContext = TOleEnum;
const
  e_ectxtNoError = $00000000;
  e_ectxtOptInError = $00000001;
  e_ectxtAutomaticUpdatesNotificationLevelError = $00000002;

// Constants for enum tagOptInRestriction
type
  tagOptInRestriction = TOleEnum;
const
  e_RstCheckAll = $00000000;
  e_RstNone = $00000000;
  e_RstManagedMachine = $00000001;
  e_RstDisabledByPolicy = $00000002;
  e_RstNotAdminCapable = $00000003;
  e_RstServiceDisabled = $00000004;
  e_RstMaxOptInRestriction = $00000004;

// Constants for enum tagAutomaticUpdatesNotificationLevelSetting
type
  tagAutomaticUpdatesNotificationLevelSetting = TOleEnum;
const
  e_aunlNotConfigured = $00000000;
  e_aunlDisabled = $00000001;
  e_aunlNotifyBeforeDownload = $00000002;
  e_aunlNotifyBeforeInstallation = $00000003;
  e_aunlScheduledInstallation = $00000004;

// Constants for enum tagAutomaticUpdatesReadOnly
type
  tagAutomaticUpdatesReadOnly = TOleEnum;
const
  e_auroNotReadOnly = $00000000;
  e_auroControlledByPolicy = $00000001;
  e_auroManaged = $00000002;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IWUWebControl = interface;
  IWUWebControlDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  WUWebControl = IWUWebControl;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  POleVariant1 = ^OleVariant; {*}
  PUserType1 = ^_FILETIME; {*}

  OSVersionField = tagOSVersionField; 

  _FILETIME = record
    dwLowDateTime: LongWord;
    dwHighDateTime: LongWord;
  end;

  OptInErrorContext = tagOptInErrorContext; 
  OptInRestriction = tagOptInRestriction; 
  AutomaticUpdatesNotificationLevelSetting = tagAutomaticUpdatesNotificationLevelSetting; 
  AutomaticUpdatesReadOnly = tagAutomaticUpdatesReadOnly; 

// *********************************************************************//
// Interface: IWUWebControl
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {FB451BA6-8EF1-4816-BAEF-418315514939}
// *********************************************************************//
  IWUWebControl = interface(IDispatch)
    ['{FB451BA6-8EF1-4816-BAEF-418315514939}']
    function GetOSVersionInfo(osField: OSVersionField; systemMetric: Integer): OleVariant; safecall;
    function Get_UserLocale: LongWord; safecall;
    procedure LaunchWindowsUpdateApplication; safecall;
    procedure OptInToUpdateService(const serviceID: WideString; optInOptions: LongWord); safecall;
    function GetUpdateServiceOptInStatus(const serviceID: WideString): WordBool; safecall;
    procedure ElevatedOptInToUpdateService(const serviceID: WideString; 
                                           var pvarAuthCabByteStream: OleVariant; 
                                           var pftAuthCabServerTime: _FILETIME; 
                                           optInOptions: LongWord; 
                                           out pOptInErrorContext: OptInErrorContext); safecall;
    function CheckOptInRestrictions(restriction: OptInRestriction): OptInRestriction; safecall;
    function GetAutomaticUpdatesNotificationLevel: AutomaticUpdatesNotificationLevelSetting; safecall;
    function GetAutomaticUpdatesReadOnly: AutomaticUpdatesReadOnly; safecall;
    function GetErrorContext: OptInErrorContext; safecall;
    function GetIncludeRecommendedUpdates: WordBool; safecall;
    function GetIncludeRecommendedUpdatesReadOnly: AutomaticUpdatesReadOnly; safecall;
    function GetFeaturedUpdatesEnabled: WordBool; safecall;
    function GetFeaturedUpdatesEnabledReadOnly: AutomaticUpdatesReadOnly; safecall;
    function GetNonAdministratorsElevated: WordBool; safecall;
    function GetNonAdministratorsElevatedReadOnly: AutomaticUpdatesReadOnly; safecall;
    property UserLocale: LongWord read Get_UserLocale;
  end;

// *********************************************************************//
// DispIntf:  IWUWebControlDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {FB451BA6-8EF1-4816-BAEF-418315514939}
// *********************************************************************//
  IWUWebControlDisp = dispinterface
    ['{FB451BA6-8EF1-4816-BAEF-418315514939}']
    function GetOSVersionInfo(osField: OSVersionField; systemMetric: Integer): OleVariant; dispid 1610743813;
    property UserLocale: LongWord readonly dispid 1610743810;
    procedure LaunchWindowsUpdateApplication; dispid 1610743841;
    procedure OptInToUpdateService(const serviceID: WideString; optInOptions: LongWord); dispid 1610743842;
    function GetUpdateServiceOptInStatus(const serviceID: WideString): WordBool; dispid 1610743843;
    procedure ElevatedOptInToUpdateService(const serviceID: WideString; 
                                           var pvarAuthCabByteStream: OleVariant; 
                                           var pftAuthCabServerTime: {??_FILETIME}OleVariant; 
                                           optInOptions: LongWord; 
                                           out pOptInErrorContext: OptInErrorContext); dispid 1610743844;
    function CheckOptInRestrictions(restriction: OptInRestriction): OptInRestriction; dispid 1610743845;
    function GetAutomaticUpdatesNotificationLevel: AutomaticUpdatesNotificationLevelSetting; dispid 1610743846;
    function GetAutomaticUpdatesReadOnly: AutomaticUpdatesReadOnly; dispid 1610743847;
    function GetErrorContext: OptInErrorContext; dispid 1610743848;
    function GetIncludeRecommendedUpdates: WordBool; dispid 1610743849;
    function GetIncludeRecommendedUpdatesReadOnly: AutomaticUpdatesReadOnly; dispid 1610743850;
    function GetFeaturedUpdatesEnabled: WordBool; dispid 1610743851;
    function GetFeaturedUpdatesEnabledReadOnly: AutomaticUpdatesReadOnly; dispid 1610743852;
    function GetNonAdministratorsElevated: WordBool; dispid 1610743853;
    function GetNonAdministratorsElevatedReadOnly: AutomaticUpdatesReadOnly; dispid 1610743854;
  end;

// *********************************************************************//
// The Class CoWUWebControl provides a Create and CreateRemote method to          
// create instances of the default interface IWUWebControl exposed by              
// the CoClass WUWebControl. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoWUWebControl = class
    class function Create: IWUWebControl;
    class function CreateRemote(const MachineName: string): IWUWebControl;
  end;

implementation

uses ComObj;

class function CoWUWebControl.Create: IWUWebControl;
begin
  Result := CreateComObject(CLASS_WUWebControl) as IWUWebControl;
end;

class function CoWUWebControl.CreateRemote(const MachineName: string): IWUWebControl;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_WUWebControl) as IWUWebControl;
end;

end.
