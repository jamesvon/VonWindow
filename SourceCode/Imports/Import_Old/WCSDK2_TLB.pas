unit WCSDK2_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 34747 $
// File generated on 2013/4/10 17:56:32 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\Program Files (x86)\WorldCard\Bin\WCSDK2.tlb (1)
// LIBID: {68A0BC84-0FC5-46A6-AA5D-9753DC685820}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  WCSDK2MajorVersion = 1;
  WCSDK2MinorVersion = 0;

  LIBID_WCSDK2: TGUID = '{68A0BC84-0FC5-46A6-AA5D-9753DC685820}';

  IID_IIApplication: TGUID = '{287F4873-2319-4978-9E43-7E80E08A33CB}';
  CLASS_IApplication: TGUID = '{EF06C99C-30DD-4CE2-97E1-DF2ED2C715B8}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IIApplication = interface;
  IIApplicationDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  IApplication = IIApplication;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  PInteger1 = ^Integer; {*}
  PWideString1 = ^WideString; {*}


// *********************************************************************//
// Interface: IIApplication
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {287F4873-2319-4978-9E43-7E80E08A33CB}
// *********************************************************************//
  IIApplication = interface(IDispatch)
    ['{287F4873-2319-4978-9E43-7E80E08A33CB}']
    function InitSDK(const strLicense: WideString; const strProductKey: WideString; 
                     const strProductName: WideString; hWnd: Integer): Integer; safecall;
    function ScanCard(const strFilename: WideString): Integer; safecall;
    function CalibrateScanner: Integer; safecall;
    procedure UnInitSDK; safecall;
    function ScanCardOnWnd(const strFilename: WideString; hDrawWnd: Integer; x1: SYSINT; 
                           y1: SYSINT; x2: SYSINT; y2: SYSINT): Integer; safecall;
    function RecogCard(const strFilename: WideString; wCharSet: Word; bWithHK: Integer; 
                       wChineseOutputCharSet: Word; bShowEditDlg: Integer): Integer; safecall;
    function Get_FieldType(nIndex: Integer): Integer; safecall;
    function Get_FieldCount: Integer; safecall;
    function Get_FieldString(nIndex: Integer): WideString; safecall;
    function Get_CharSetInSeq(dwSeq: LongWord): Word; safecall;
    function Get_CharSetCount: Integer; safecall;
    function SetMainCaller(hWnd: Integer): Integer; safecall;
    function ScanPhoto(const strFilename: WideString): Integer; safecall;
    function IsMachineConnected(var plConnected: Integer): Integer; safecall;
    function IsPaperOn(var plPaperOn: Integer): Integer; safecall;
    function IsNeedCalibrate(var plNeed: Integer): Integer; safecall;
    procedure SetFieldLayout(wLayout: Word); safecall;
    function GetActiveType(var lType: Integer): Integer; safecall;
    function Duet_InitSDK(const strLicense: WideString; const strProductKey: WideString; 
                          const strProductName: WideString; hWnd: Integer): Integer; safecall;
    function Duet_IsMachineConnected(var plConnected: Integer): Integer; safecall;
    procedure Duet_ScanCard(var pVal: WideString); safecall;
    function Duet_SetPreviewHwnd(hWnd: Integer): Integer; safecall;
    function Duet_StartPreview: Integer; safecall;
    function Duet_SetSecondaryCaller(hWnd: Integer): Integer; safecall;
    function Duet_SetPreviewWndPos(x: Integer; y: Integer; w: Integer; h: Integer): Integer; safecall;
    function Duet_UnInitSDK: Integer; safecall;
    function Duet_GetScanedCardBitMap(dwCardID: Integer): WideString; safecall;
    function Duet_StopPreview: Integer; safecall;
    function RecogRect(const strFilename: WideString; wCharSet: Word; bWithHK: Integer; 
                       wChineseOutputCharSet: Word; x1: Word; y1: Word; x2: Word; y2: Word): Integer; safecall;
    function Get_RectResult: WideString; safecall;
    function RecogDoubleSideCard(const strFrontFile: WideString; const strBackFile: WideString; 
                                 wFrontCharSet: Word; wBackCharSet: Word; bWithHK: Integer; 
                                 wChineseOutputCharSet: Word; bShowEditDlg: Integer): Integer; safecall;
    function SetCapture: Integer; safecall;
    procedure EnableCapitalizeName(bEnable: Integer); safecall;
    function IsPaperJam(var plPaperJam: Integer): Integer; safecall;
    property FieldType[nIndex: Integer]: Integer read Get_FieldType;
    property FieldCount: Integer read Get_FieldCount;
    property FieldString[nIndex: Integer]: WideString read Get_FieldString;
    property CharSetInSeq[dwSeq: LongWord]: Word read Get_CharSetInSeq;
    property CharSetCount: Integer read Get_CharSetCount;
    property RectResult: WideString read Get_RectResult;
  end;

// *********************************************************************//
// DispIntf:  IIApplicationDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {287F4873-2319-4978-9E43-7E80E08A33CB}
// *********************************************************************//
  IIApplicationDisp = dispinterface
    ['{287F4873-2319-4978-9E43-7E80E08A33CB}']
    function InitSDK(const strLicense: WideString; const strProductKey: WideString; 
                     const strProductName: WideString; hWnd: Integer): Integer; dispid 1;
    function ScanCard(const strFilename: WideString): Integer; dispid 2;
    function CalibrateScanner: Integer; dispid 3;
    procedure UnInitSDK; dispid 4;
    function ScanCardOnWnd(const strFilename: WideString; hDrawWnd: Integer; x1: SYSINT; 
                           y1: SYSINT; x2: SYSINT; y2: SYSINT): Integer; dispid 5;
    function RecogCard(const strFilename: WideString; wCharSet: {??Word}OleVariant; 
                       bWithHK: Integer; wChineseOutputCharSet: {??Word}OleVariant; 
                       bShowEditDlg: Integer): Integer; dispid 6;
    property FieldType[nIndex: Integer]: Integer readonly dispid 7;
    property FieldCount: Integer readonly dispid 8;
    property FieldString[nIndex: Integer]: WideString readonly dispid 9;
    property CharSetInSeq[dwSeq: LongWord]: {??Word}OleVariant readonly dispid 10;
    property CharSetCount: Integer readonly dispid 11;
    function SetMainCaller(hWnd: Integer): Integer; dispid 12;
    function ScanPhoto(const strFilename: WideString): Integer; dispid 13;
    function IsMachineConnected(var plConnected: Integer): Integer; dispid 14;
    function IsPaperOn(var plPaperOn: Integer): Integer; dispid 15;
    function IsNeedCalibrate(var plNeed: Integer): Integer; dispid 16;
    procedure SetFieldLayout(wLayout: {??Word}OleVariant); dispid 17;
    function GetActiveType(var lType: Integer): Integer; dispid 18;
    function Duet_InitSDK(const strLicense: WideString; const strProductKey: WideString; 
                          const strProductName: WideString; hWnd: Integer): Integer; dispid 20;
    function Duet_IsMachineConnected(var plConnected: Integer): Integer; dispid 21;
    procedure Duet_ScanCard(var pVal: WideString); dispid 22;
    function Duet_SetPreviewHwnd(hWnd: Integer): Integer; dispid 23;
    function Duet_StartPreview: Integer; dispid 24;
    function Duet_SetSecondaryCaller(hWnd: Integer): Integer; dispid 25;
    function Duet_SetPreviewWndPos(x: Integer; y: Integer; w: Integer; h: Integer): Integer; dispid 26;
    function Duet_UnInitSDK: Integer; dispid 27;
    function Duet_GetScanedCardBitMap(dwCardID: Integer): WideString; dispid 28;
    function Duet_StopPreview: Integer; dispid 29;
    function RecogRect(const strFilename: WideString; wCharSet: {??Word}OleVariant; 
                       bWithHK: Integer; wChineseOutputCharSet: {??Word}OleVariant; 
                       x1: {??Word}OleVariant; y1: {??Word}OleVariant; x2: {??Word}OleVariant; 
                       y2: {??Word}OleVariant): Integer; dispid 30;
    property RectResult: WideString readonly dispid 31;
    function RecogDoubleSideCard(const strFrontFile: WideString; const strBackFile: WideString; 
                                 wFrontCharSet: {??Word}OleVariant; 
                                 wBackCharSet: {??Word}OleVariant; bWithHK: Integer; 
                                 wChineseOutputCharSet: {??Word}OleVariant; bShowEditDlg: Integer): Integer; dispid 32;
    function SetCapture: Integer; dispid 33;
    procedure EnableCapitalizeName(bEnable: Integer); dispid 34;
    function IsPaperJam(var plPaperJam: Integer): Integer; dispid 35;
  end;

// *********************************************************************//
// The Class CoIApplication provides a Create and CreateRemote method to          
// create instances of the default interface IIApplication exposed by              
// the CoClass IApplication. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoIApplication = class
    class function Create: IIApplication;
    class function CreateRemote(const MachineName: string): IIApplication;
  end;

implementation

uses ComObj;

class function CoIApplication.Create: IIApplication;
begin
  Result := CreateComObject(CLASS_IApplication) as IIApplication;
end;

class function CoIApplication.CreateRemote(const MachineName: string): IIApplication;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_IApplication) as IIApplication;
end;

end.
