unit JTTS_ACTIVEXLib_TLB;

interface

uses
  WinApi.Windows, WinApi.Messages;

{$ALIGN 8}

type

  PPByte = ^PByte;
  ERRCODE = (ERR_NONE, ERR_ALREADYINIT, ERR_NOTINIT, ERR_MEMORY,
    ERR_INVALIDHWND, ERR_INVALIDFUNC, ERR_OPENLIB, ERR_READLIB, ERR_PLAYING,
    ERR_DONOTHING, ERR_INVALIDTEXT, ERR_CREATEFILE, ERR_WRITEFILE, ERR_FORMAT,
    ERR_INVALIDSESSION, ERR_TOOMANYSESSION, ERR_MORETEXT, ERR_CONFIG,
    ERR_OPENDEVICE, ERR_RESETDEVICE, ERR_PAUSEDEVICE, ERR_RESTARTDEVICE,
    ERR_STARTTHREAD, ERR_BEGINOLE, ERR_NOTSUPPORT, ERR_SECURITY, ERR_CONVERT,
    ERR_PARAM, ERR_INPROGRESS, ERR_INITSOCK, ERR_CREATESOCK, ERR_CONNECTSOCK,
    ERR_TOOMANYCON, ERR_CONREFUSED, ERR_SEND, ERR_RECEIVE, ERR_SERVERSHUTDOWN,
    ERR_OUTOFTIME, ERR_CONFIGTTS, ERR_SYNTHTEXT, ERR_CONFIGVERSION, ERR_EXPIRED,
    ERR_NEEDRESTART, ERR_CODEPAGE, ERR_ENGINE, ERR_CREATEEVENT, ERR_PLAYMODE,
    ERR_OPENFILE, ERR_USERABORT);

const


  // 系统的设置选项

  // 支持多语种这里列出的是系统内建的语言定义，需要安装相应音库才能真正支持.
  // 但目前并非所有语言都有相应的音库,对于这里没有列出的语言，将来也可能会发
  // 布相应的音库，同时会分配一个数值，只要安装此音库后，就可以使用。对于没
  // 有列出的语言，如果想使用，可以直接使用数值可以通过Lang系列函数得到所有
  // 系统中定义的（包括将来扩展的）语言数值及其描述的信息.对于系统中真正支持
  // 的语言，可以通过jTTS_GetVoiceCount, jTTS_GetVoiceAttribute函数得到所有
  // 安装的音库，并从其属性中知道其语言.
  LANGUAGE_MANDARIN = 0; // 汉语普通话
  LANGUAGE_CANTONESE = 1; // 广东话
  LANGUAGE_CHINESE = LANGUAGE_MANDARIN;

  LANGUAGE_US_ENGLISH = 10; // 美国英语
  LANGUAGE_BRITISH_ENGLISH = 11; // 英国英语
  LANGUAGE_ENGLISH = LANGUAGE_US_ENGLISH;

  LANGUAGE_FRENCH = 20; // 法语
  LANGUAGE_CANADIAN_FRENCH = 21; // 加拿大法语

  LANGUAGE_SPANISH = 30; // 西班牙语
  LANGUAGE_LATINAMERICAN_SPANISH = 31; // 拉丁美洲西班牙语

  LANGUAGE_PORTUGUESE = 40; // 葡萄牙语
  LANGUAGE_BRAZILIAN_PORTUGUESE = 41; // 巴西葡萄牙语

  LANGUAGE_DUTCH = 50; // 荷兰语
  LANGUAGE_BELGIAN_DUTCH = 51; // 比利时荷兰语

  LANGUAGE_GERMAN = 60; // 德语
  LANGUAGE_ITALIAN = 70; // 意大利语
  LANGUAGE_SWEDISH = 80; // 瑞典语
  LANGUAGE_NORWEGIAN = 90; // 挪威语
  LANGUAGE_DANISH = 100; // 丹麦语
  LANGUAGE_POLISH = 110; // 波兰语
  LANGUAGE_GREEK = 120; // 希腊语
  LANGUAGE_HUNGARIAN = 130; // 匈牙利语
  LANGUAGE_CZECH = 140; // 捷克语
  LANGUAGE_TURKISH = 150; // 土耳其语

  LANGUAGE_RUSSIAN = 500; // 俄语

  LANGUAGE_ARABIC = 600; // 阿拉伯语

  LANGUAGE_JAPANESE = 700; // 日语
  LANGUAGE_KOREAN = 710; // 韩语

  LANGUAGE_VIETNAMESE = 720; // 越南语
  LANGUAGE_MALAY = 730; // 马来语
  LANGUAGE_THAI = 740; // 泰语


  // 支持多领域
  //
  // 这里列出的是系统内建的领域定义，需要安装相应音库的资源包才能真正支持。
  // 对于这里没有列出的领域，将来也可能会发布相应的资源包，同时会分配一个
  // 数值，只要安装此资源包后，就可以使用。对于没有列出的领域，如果想使用，
  // 可以直接使用数值.
  //
  // 可以通过Domain系列函数得到所有系统中定义的（包括将来扩展的）领域数值
  // 及其描述的信息. 对于系统中真正支持的语言，可以通过jTTS_GetVoiceCount,
  // jTTS_GetVoiceAttribute函数得到所有安装的音库，并从其属性中知道其支持
  // 的领域

  DOMAIN_COMMON = 0; // 通用领域，新闻
  DOMAIN_FINANCE = 1; // 金融证券
  DOMAIN_WEATHER = 2; // 天气预报
  DOMAIN_SPORTS = 3; // 体育赛事
  DOMAIN_TRAFFIC = 4; // 公交信息
  DOMAIN_TRAVEL = 5; // 旅游餐饮

  DOMAIN_MIN = 0;
  DOMAIN_MAX = 31;

  // 支持的CODEPAGE
  CODEPAGE_GB = 936; // 包括GB18030, GBK, GB2312
  CODEPAGE_BIG5 = 950;
  CODEPAGE_SHIFTJIS = 932;
  CODEPAGE_ISO8859_1 = 1252;
  CODEPAGE_UNICODE = 1200;
  CODEPAGE_UNICODE_BIGE = 1201; // BIG Endian
  CODEPAGE_UTF8 = 65001;

  // 支持TAG
  TAG_AUTO = $00; // 自动判断
  TAG_JTTS = $01; // 仅处理含有jTTS 3.0支持的TAG: \read=\
  TAG_SSML = $02; // 仅处理含有SSML 的TAG: <voice gender="female" />
  TAG_NONE = $03; // 没有TAG

  // DigitMode
  DIGIT_AUTO_NUMBER = 0;
  DIGIT_TELEGRAM = 1;
  DIGIT_NUMBER = 2;
  DIGIT_AUTO_TELEGRAM = 3;
  DIGIT_AUTO = DIGIT_AUTO_NUMBER;

  // PuncMode
  PUNC_OFF = 0; // 不读符号，自动判断回车换行是否分隔符
  PUNC_ON = 1; // 读符号，  自动判断回车换行是否分隔符
  PUNC_OFF_RTN = 2; // 不读符号，强制将回车换行作为分隔符
  PUNC_ON_RTN = 3; // 读符号，  强制将回车换行作为分隔符

  // EngMode
  ENG_AUTO = 0; // 自动方式
  ENG_SAPI = 1; // 此版本无效，等同于ENG_AUTO
  ENG_LETTER = 2; // 强制单字母方式
  ENG_LETTER_PHRASE = 3; // 强制采用字母＋自录音词汇的方式

  // Gender
  GENDER_FEMALE = 0;
  GENDER_MALE = 1;
  GENDER_NEUTRAL = 2;

  // AGE
  AGE_BABY = 0; // 0 - 3
  AGE_CHILD = 1; // 3 - 12
  AGE_YOUNG = 2; // 12 - 18
  AGE_ADULT = 3; // 18 - 60
  AGE_OLD = 4; // 60 -

  // PITCH
  PITCH_MIN = 0;
  PITCH_MAX = 9;

  // VOLUME
  VOLUME_MIN = 0;
  VOLUME_MAX = 9;

  // SPEED
  SPEED_MIN = 0;
  SPEED_MAX = 9;

  // jTTS_Play状态
  STATUS_NOTINIT = 0;
  STATUS_READING = 1;
  STATUS_PAUSE = 2;
  STATUS_IDLE = 3;

  // jTTS_PlayToFile的文件格式
  FORMAT_WAV = 0; // PCM Native (和音库一致，目前为16KHz, 16Bit)
  FORMAT_VOX_6K = 1; // OKI ADPCM, 6KHz, 4bit (Dialogic Vox)
  FORMAT_VOX_8K = 2; // OKI ADPCM, 8KHz, 4bit (Dialogic Vox)
  FORMAT_ALAW_8K = 3; // A律, 8KHz, 8Bit
  FORMAT_uLAW_8K = 4; // u律, 8KHz, 8Bit
  FORMAT_WAV_8K8B = 5; // PCM, 8KHz, 8Bit
  FORMAT_WAV_8K16B = 6; // PCM, 8KHz, 16Bit
  FORMAT_WAV_16K8B = 7; // PCM, 16KHz, 8Bit
  FORMAT_WAV_16K16B = 8; // PCM, 16KHz, 16Bit
  FORMAT_WAV_11K8B = 9; // PCM, 11.025KHz, 8Bit
  FORMAT_WAV_11K16B = 10; // PCM, 11.025KHz, 16Bit

  FORMAT_FIRST = 0;
  FORMAT_LAST = 10;

  // jTTS_Play / jTTS_PlayToFile / jTTS_SessionStart 函数支持的dwFlag定义

  // 此项仅对jTTS_PlayToFile适用
  PLAYTOFILE_DEFAULT = $0000; // 默认值,写文件时只增加FORMAT_WAV_...格式的文件头
  PLAYTOFILE_NOHEAD = $0001; // 所有的格式都不增加文件头
  PLAYTOFILE_ADDHEAD = $0002;
  // 增加FORMAT_WAV_...格式和FORMAT_ALAW_8K,FORMAT_uLAW_8K格式的文件头

  PLAYTOFILE_MASK = $000F;

  // 此项仅对jTTS_Play适用
  PLAY_RETURN = $0000; // 如果正在播放，返回错误
  PLAY_INTERRUPT = $0010; // 如果正在播放，打断原来的播放，立即播放新的内容

  PLAY_MASK = $00F0;

  // 播放的内容
  PLAYCONTENT_TEXT = $0000; // 播放内容为文本
  PLAYCONTENT_TEXTFILE = $0100; // 播放内容为文本文件
  PLAYCONTENT_AUTOFILE = $0200; // 播放内容为文件，根据后缀名采用外界Filter DLL抽取
  // 无法判断的当作文本文件

  PLAYCONTENT_MASK = $0F00;

  // 播放的模式，同时用于SessionStart
  PLAYMODE_DEFAULT = $0000; // 在jTTS_Play下缺省异步，在jTTS_PlayToFile下缺省同步
  // jTTS_SessionStart下为主动获取数据方式

  PLAYMODE_ASYNC = $1000; // 异步播放，函数立即退出
  PLAYMODE_SYNC = $2000; // 同步播放，播放完成后退出

  PLAYMODE_MASK = $F000;

  // jTTS_FindVoice返回的匹配级别
  MATCH_LANGUAGE = 0; // 满足LANGUAGE，
  MATCH_GENDER = 1; // 满足LANGUAGE, GENDER
  MATCH_AGE = 2; // 满足LANGUAGE, GENDER, AGE
  MATCH_NAME = 3; // 满足LANGUAGE, GENDER，AGE，NAME
  MATCH_DOMAIN = 4; // 满足LANGUAGE, GENDER，AGE，NAME, DOMAIN，也即满足所有条件
  MATCH_ALL = 4; // 满足所有条件

  // InsertInfo信息
  INFO_MARK = 0;
  INFO_VISEME = 1;

  // LICENSETYPE_
  LICENSETYPE_FORMAL = 0;
  LICENSETYPE_TRAIL_NODONGLE = 1;
  LICENSETYPE_TRAIL_UNMATCHSN = 2;
  LICENSETYPE_SOFTENCRY = 10;

  // VERSION_
  VERSION_PROFESSIONAL = 0;
  VERSION_STANDARD = 1;
  VERSION_DESKTOP = 2;

  // 各种信息串的长度
  VOICENAME_LEN = 32;
  VOICEID_LEN = 40;
  VENDOR_LEN = 32;
  DLLNAME_LEN = 256;
  ATTRNAME_LEN = 32;
  XMLLANG_LEN = 256;

type
  JTTS_PARAM = (

    PARAM_CODEPAGE, // CODEPAGE_xxx
    PARAM_VOICEID, // Voice ID
    PARAM_PITCH, // PITCH_MIN - PITCH_MAX
    PARAM_VOLUME, // VOLUME_MIN - VOLUME_MAX
    PARAM_SPEED, // SPEED_MIN - SPEED_MAX
    PARAM_PUNCMODE, // PUNC_xxx
    PARAM_DIGITMODE, // DIGIT_xxx
    PARAM_ENGMODE, // ENG_xxx
    PARAM_TAGMODE, // TAG_xxx
    PARAM_DOMAIN, // DOMAIN_xxx
    PARAM_TRYTIMES, PARAM_LOADBALANCE, PARAM_VOICESTYLE, PARAM_BACKAUDIO

    );

const
  JTTS_VERSION4 = $0004; // version 4.0

type

  JTTS_CONFIG = record
    wVersion: Word; // JTTS_VERSION4
    nCodePage: Word;
    szVoiceID: array [0 .. VOICEID_LEN - 1] of AnsiChar; // 使用的音色
    nDomain: Smallint;
    nPitch: Smallint;
    nVolume: Smallint;
    nSpeed: Smallint;
    nPuncMode: Smallint;
    nDigitMode: Smallint;
    nEngMode: Smallint;
    nTagMode: Smallint;
    nTryTimes: Smallint;
    bLoadBalance: DWord;
    nVoiceStyle: Smallint;
    nBackAudio: Smallint;
    nBackAudioVolume: Smallint;
    wBackAudioFlag: Word;
    nVoiceBufSize: Smallint;
    nInsertInfoSize: Smallint;
    nReserved: array [0 .. 15] of Smallint; // 保留
  end;

  PJTTS_CONFIG = ^JTTS_CONFIG;

  JTTS_VOICEATTRIBUTE = record
    szName: array [0 .. VOICENAME_LEN - 1] of AnsiChar; // 只能为英文名称
    szVoiceID: array [0 .. VOICEID_LEN - 1] of AnsiChar; // 音色的唯一标识
    nGender: Smallint; // GENDER_xxx
    nAge: Smallint; // AGE_xx
    dwDomainArray: DWord; // 由低位向高位，分别表示DOMAIN_xxx
    nLanguage: DWord; // 支持的语言, LANGUAGE_xxx
    szVendor: array [0 .. VENDOR_LEN - 1] of AnsiChar; // 提供厂商
    szDLLName: array [0 .. DLLNAME_LEN - 1] of AnsiChar; // 对应的DLL
    dwVersionMS: DWord; // 引擎的版本号，对应"3.75.0.31"的前两节
    // e.g. 0x00030075 = "3.75"
    dwVersionLS: DWord; // e.g. 0x00000031 = "0.31"
  end;

  PJTTS_VOICEATTRIBUTE = ^JTTS_VOICEATTRIBUTE;

  INSERTINFO = record
    nTag: Integer; // 有二种：INFO_MARK, INFO_VISEME
    dwValue: DWord; // 具体信息：
    // MARK时，高24位mark文本偏移，低8位文本长度
    // VISEME时，表示唇型
    dwBytes: DWord; // 在语音流的什么地方插入，必须按顺序增加
  end;

  PINSERTINFO = ^INSERTINFO;
  PPINSERTINFO = ^PINSERTINFO;

  JTTS_LICENSEINFO = record
    wLicenseType: Word; // LICENSETYPE_xxx
    wVersion: Word; // 版本: 专业版，标准版, 桌面版等
    bService: BOOL; // 是否支持服务器功能
    bVoiceBanner: BOOL; // 是否有版权信息的插入语音
    nTotalLines: LongInt; // 可以同时并发的线程数目, 0: 没有限制
    nExpiredTime: LongInt; // 终止时间，从1970-1-1开始的天数. 0: 没有终止使用的时间限制
    nRestartTime: LongInt; // 重启操作系统的间隔(小时). 0: 没有重启限制
  end;

  PJTTS_LICENSEINFO = ^JTTS_LICENSEINFO;

  JTTS_LANGATTRIBUTE = record
    nValue: Integer;
    szName: array [0 .. ATTRNAME_LEN - 1] of AnsiChar;
    szEngName: array [0 .. ATTRNAME_LEN - 1] of AnsiChar;
    szXmlLang: array [0 .. XMLLANG_LEN - 1] of AnsiChar;
  end;

  PJTTS_LANGATTRIBUTE = ^JTTS_LANGATTRIBUTE;

  JTTS_DOMAINATTRIBUTE = record
    nValue: Integer;
    szName: array [0 .. ATTRNAME_LEN - 1] of AnsiChar;
    szEngName: array [0 .. ATTRNAME_LEN - 1] of AnsiChar;
  end;

  PJTTS_DOMAINATTRIBUTE = ^JTTS_DOMAINATTRIBUTE;


  // 系统通知消息及回调函数部分

const
  // 系统通知消息
  WM_JTTS_NOTIFY = WM_USER + $4999;

type
  // 回调函数
  JTTS_CALLBACKPROC = function(wParam: Word; lParam: LongInt; dwUserData: DWord)
    : BOOL; stdcall;

  JTTS_DATACALLBACKPROC = function(dwSessionID: DWord; wParam: Word;
    lParam: LongInt; pVoiceData: PByte; dwLen: DWord; PINSERTINFO: PINSERTINFO;
    nInsertInfo: Integer; dwUserData: DWord): BOOL; stdcall;

const

  // WM_JTTS_NOTIFY / JTTS_CALLBACKPROC / JTTS_DATACALLBACKPROC中wParam的定义

  NOTIFY_BEGIN = 0; // lParam: 没有使用
  NOTIFY_END = 1; // lParam: 没有使用
  NOTIFY_SENTEND = 2; // lParam: 已经读完的字节数
  NOTIFY_SENTBEGIN = 3; // lParam: 将要读到的位置（以字节数计算）
  NOTIFY_MARK = 4; // lParam: 标记Mark串的位置和长度, 参考宏GETMARKOFFSET和GETMARKLEN
  NOTIFY_VISEME = 5; // lParam: 标记唇型信息，现版本中未使用
  NOTIFY_CHANGECONFIG = 6; // 系统主动使用与传入Config不同的配置
  NOTIFY_DATA = 7; // 仅用于 JTTS_DATACALLBACKPROC，传递合成数据和InsertInfo信息
  // lParam not use; dwLen是pVoiceData长度; pVoiceData语音数据缓冲;
  // nInsertInfo, pInsertInfo是插入数据的数目和内容

  // 为和老版本兼容而保留
  NOTIFY_PROGRESS = 2;
  NOTIFY_PROGRESS2 = 3;

  jTTS_ML_DLL = 'jTTS_ML.dll';

  // 通过NOTIFY_MARK的lParam得到具体的Mark String的偏移量和长度
function GETMARKOFFSET(lParam: lParam): LongInt;
function GETMARKLEN(lParam: lParam): LongInt;




// ------------------------------------------------------------------------
// 系统函数

function jTTS_Init(const pcszLibPath: PAnsiChar; const pcszSerialNo: PAnsiChar)
  : ERRCODE; stdcall; external jTTS_ML_DLL;
function jTTS_End(): ERRCODE; stdcall; external jTTS_ML_DLL;

function jTTS_GetLicenseInfo(pLicenseInfo: PJTTS_LICENSEINFO): ERRCODE; stdcall;
  external jTTS_ML_DLL;

function jTTS_GetLangCount(): Integer; stdcall; external jTTS_ML_DLL;
function jTTS_GetLangAttribute(nIndex: Integer; pAttribute: PJTTS_LANGATTRIBUTE)
  : ERRCODE; stdcall; external jTTS_ML_DLL;
function jTTS_GetLangAttributeByValue(nValue: Integer;
  pAttribute: PJTTS_LANGATTRIBUTE): ERRCODE; stdcall; external jTTS_ML_DLL;

function jTTS_GetDomainCount(): Integer; stdcall; external jTTS_ML_DLL;
function jTTS_GetDomainAttribute(nIndex: Integer;
  pAttribute: PJTTS_DOMAINATTRIBUTE): ERRCODE; stdcall; external jTTS_ML_DLL;
function jTTS_GetDomainAttributeByValue(nValue: Integer;
  pAttribute: PJTTS_DOMAINATTRIBUTE): ERRCODE; stdcall; external jTTS_ML_DLL;

// -------------------------------------------------------------
// 音库信息函数
function jTTS_GetVoiceCount(): Integer; stdcall; external jTTS_ML_DLL;
function jTTS_GetVoiceAttribute(nIndex: Integer;
  pAttribute: PJTTS_VOICEATTRIBUTE): ERRCODE; stdcall; external jTTS_ML_DLL;
function jTTS_GetVoiceAttributeByID(const pszVoiceID: PAnsiChar;
  pAttribute: PJTTS_VOICEATTRIBUTE): ERRCODE; stdcall; external jTTS_ML_DLL;
function jTTS_IsVoiceSupported(const pszVoiceID: PAnsiChar): BOOL; stdcall;
  external jTTS_ML_DLL;
function jTTS_FindVoice(nLanguage: Integer; nGender: Integer; nAge: Integer;
  szName: PAnsiChar; nDomain: Integer; pszVoiceID: PAnsiChar;
  pwMatchFlag: PWORD): ERRCODE; stdcall; external jTTS_ML_DLL;

function jTTS_PreLoad(const pszVoiceID: PAnsiChar): ERRCODE; stdcall;
  external jTTS_ML_DLL;

// ------------------------------------------------------------------------
// 设置函数
function jTTS_Set(const pConfig: PJTTS_CONFIG): ERRCODE; stdcall;
  external jTTS_ML_DLL;
function jTTS_Get(const pConfig: PJTTS_CONFIG): ERRCODE; stdcall;
  external jTTS_ML_DLL;
function jTTS_SetParam(nParam: JTTS_PARAM; dwValue: DWord): ERRCODE; stdcall;
  external jTTS_ML_DLL;
function jTTS_GetParam(nParam: JTTS_PARAM; pdwValue: PDWORD): ERRCODE; stdcall;
  external jTTS_ML_DLL;

// ------------------------------------------------------------------------
// 播放函数
function jTTS_SetPlay(uDeviceID: UINT; hwnd: hwnd;
  lpfnCallback: JTTS_CALLBACKPROC; dwUserData: DWord): ERRCODE; stdcall;
  external jTTS_ML_DLL;
function jTTS_Play(const pcszText: PAnsiChar; dwFlag: DWord): ERRCODE; stdcall;
  external jTTS_ML_DLL;
function jTTS_Stop(): ERRCODE; stdcall; external jTTS_ML_DLL;
function jTTS_Pause(): ERRCODE; stdcall; external jTTS_ML_DLL;
function jTTS_Resume(): ERRCODE; stdcall; external jTTS_ML_DLL;
function jTTS_GetStatus(): Integer; stdcall; external jTTS_ML_DLL;

// ------------------------------------------------------------------------
// 播放到文件函数
function jTTS_PlayToFile(const pcszText: PAnsiChar;
  const pcszFileName: PAnsiChar; nFormat: UINT; const pConfig: PJTTS_CONFIG;
  dwFlag: DWord; lpfnCallback: JTTS_CALLBACKPROC; dwUserData: DWord): ERRCODE;
  stdcall; external jTTS_ML_DLL;

// -------------------------------------------------------------
// 合成过程底层函数
function jTTS_SessionStart(const pcszText: PAnsiChar; pdwSessionID: PDWORD;
  nFormat: UINT; const pConfig: PJTTS_CONFIG; dwFlag: DWord;
  pnBitsPerSample: PInteger; pnSamplesPerSec: PInteger): ERRCODE; stdcall;
  external jTTS_ML_DLL;

function jTTS_SessionGetData(dwSessionID: DWord; nBufIndex: Integer;
  ppVoiceData: PPByte; pdwLen: PDWORD; nReserveLen: Integer;
  PPINSERTINFO: PPINSERTINFO; pnInsertInfo: PInteger): ERRCODE; stdcall;
  external jTTS_ML_DLL;

function jTTS_SessionStop(dwSessionID: DWord): ERRCODE; stdcall;
  external jTTS_ML_DLL;
function jTTS_SessionGetReadBytes(dwSessionID: DWord; pdwBytes: PDWORD)
  : ERRCODE; stdcall; external jTTS_ML_DLL;
function jTTS_SessionExec(dwSessionID: DWord;
  lpfnDataCallback: JTTS_DATACALLBACKPROC; dwUserData: DWord): ERRCODE; stdcall;
  external jTTS_ML_DLL;

implementation

function GETMARKOFFSET(lParam: lParam): LongInt;
begin
  GETMARKOFFSET := lParam shr 8;
end;

function GETMARKLEN(lParam: lParam): LongInt;
begin
  GETMARKLEN := lParam and $FF;
end;

end.
