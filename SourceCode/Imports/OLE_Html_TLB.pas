unit OLE_Html_TLB;

interface

uses sysUtils, Forms, Classes, WinApi.ActiveX, MSHTML, Variants;

type
  /// <summary>键值对</summary>
  THtmlItem = class(TObject)
  private
    FKeyName: string;
    FKeyValue: string;
    procedure SetKeyName(const Value: string);
    procedure SetKeyValue(const Value: string);
  published
    property KeyName: string read FKeyName write SetKeyName;
    property KeyValue: string read FKeyValue write SetKeyValue;
  end;

  /// <summary>键值对集合</summary>
  THtmlItemCollection = class(TObject)
  private
    FList: TList;
    function GetCount: Integer;
  protected
    procedure SetItem(AKeyName: string; AKeyValue: string); overload;
    procedure SetItem(Index: Integer; AKeyValue: string); overload;
    function GetItem(AKeyName: string): string; overload;
    function GetItem(Index: Integer): string; overload;
    function Find(AKeyName: string): Integer;
  public
    constructor Create;
    destructor Destroy;
    procedure Clear;
    procedure AddItem(AKeyName: string; AKeyValue: string);
    procedure DelItem(Index: Integer); overload;
    procedure DelItem(AKeyName: string); overload;
  published
    property Count: Integer read GetCount;
  end;

  /// <summary>键-对象</summary>
  THtmlObject = class(TObject)
  private
    FKeyName: string;
    FObject: TObject;
    FKeyObject: TObject;
    procedure SetKeyName(const Value: string);
    procedure SetKeyObject(const Value: TObject);
  end;

  /// <summary>键-对象集合，键允许重复</summary>
  THtmlObjectCollection = class(TObject)
  private
    FList: TList;
    function GetCount: Integer;
  protected
    function GetObjects(Index: Integer): TObject;
    procedure SetObjects(Index: Integer; const Value: TObject);
    function Find(AKeyName: string; FromIndex: Integer = 0): Integer;
  public
    constructor Create;
    destructor Destroy;
    procedure Clear;
    procedure AddItem(AKeyName: string; Value: TObject);
    procedure DelItem(Index: Integer);
  published
    property Count: Integer read GetCount;
  end;

  /// <summary>键-对象</summary>
  THtmlInnerHtml = class(TObject)
  private
    function GetText: string; virtual; abstract;
    procedure SetText(const Value: string); virtual; abstract;
  published
    property Text: string read GetText write SetText;
  end;

  /// <summary>html 节点属性</summary>
  THtmlAttribute = THtmlItem;

  /// <summary>html 节点属性集合</summary>
  THtmlAttributeCollection = class(THtmlItemCollection)
  public
    property Values[KeyName: string]: string read GetItem write SetItem;
    property ValueIndex[Index: Integer]: string read GetItem write SetItem;
  end;

  /// <summary>html 节点</summary>
  THtmlElement = class(THtmlObject)
  private
    FChildren: THtmlObjectCollection;
    FAttribute: THtmlItemCollection;
    function GetAttrCount: Integer;
    function GetAttributes(Index: Integer): string;
    function GetChildCount: Integer;
    function GetChildren(Index: Integer): THtmlElement;
    procedure SetAttributes(Index: Integer; const Value: string);
    procedure SetChildren(Index: Integer; const Value: THtmlElement);
    function GetAttrValue(KeyName: string): string;
    procedure SetAttrValue(KeyName: string; const Value: string);
    function GetAttrName(Index: Integer): string;
    procedure SetAttrName(Index: Integer; const Value: string);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    /// <summary>添加下级节点</summary>
    procedure AddChild(Child: THtmlElement);
    /// <summary>删除下级节点</summary>
    procedure DeleteChild(Index: Integer);
    /// <summary>查找下级节点</summary>
    function FindChild(tagName: string; FromIndex: Integer = 0): Integer;
    /// <summary>添加属性</summary>
    procedure AddAttribute(AKeyName, AKeyValue: string);
    /// <summary>删除属性</summary>
    procedure DeleteAttribute(Index: Integer); overload;
    /// <summary>删除属性</summary>
    procedure DeleteAttribute(KeyName: string); overload;
    /// <summary>索引下级节点</summary>
    property Children[Index: Integer]: THtmlElement read GetChildren write SetChildren;
    /// <summary>索引属性</summary>
    property Attributes[Index: Integer]: string read GetAttributes write SetAttributes;
    /// <summary>键值属性</summary>
    property AttrValue[KeyName: string]: string read GetAttrValue write SetAttrValue;
    property AttrName[Index: Integer]: string read GetAttrName write SetAttrName;
  published
    property tagName: string read FKeyName write SetKeyName;
    property ChildCount: Integer read GetChildCount;
    property AttrCount: Integer read GetAttrCount;
  end;

  THtmlDocument = class(TObject)
  private
    FParsed: Boolean;
    FHtmlDoc: IHTMLDocument2;
    FBody: THtmlElement;
    FTitle: string;
    Fcookie: WideString;
    FalinkColor: OleVariant;
    FfgColor: OleVariant;
    Fdomain: WideString;
    Fcharset: WideString;
    FvlinkColor: OleVariant;
    Fexpando: WordBool;
    FlinkColor: OleVariant;
    Furl: WideString;
    FdefaultCharset: WideString;
    FbgColor: OleVariant;
    function GetHtmlText: string;
    procedure SetHtmlText(const Value: string);
    procedure SetParsed(const Value: Boolean);
    procedure SetTitle(const Value: string);
    procedure SetalinkColor(const Value: OleVariant);
    procedure SetbgColor(const Value: OleVariant);
    procedure Setcharset(const Value: WideString);
    procedure Setcookie(const Value: WideString);
    procedure SetdefaultCharset(const Value: WideString);
    procedure Setdomain(const Value: WideString);
    procedure Setexpando(const Value: WordBool);
    procedure SetfgColor(const Value: OleVariant);
    procedure SetlinkColor(const Value: OleVariant);
    procedure Seturl(const Value: WideString);
    procedure SetvlinkColor(const Value: OleVariant);
  public
    constructor Create;
    destructor Destroy;
    procedure Parse;
    procedure Close;
    procedure LoadFromFile(AFilename: string);
    procedure LoadFromURL(AURL: string);
    procedure SaveToFile(AFilename: string);
  published
    property HtmlText: string read GetHtmlText write SetHtmlText;
    property Body: THtmlElement read FBody;
    property Parsed: Boolean read FParsed write SetParsed;
    property Title: string read FTitle write SetTitle;
    property alinkColor: OleVariant read FalinkColor write SetalinkColor;
    property bgColor: OleVariant read FbgColor write SetbgColor;
    property fgColor: OleVariant read FfgColor write SetfgColor;
    property linkColor: OleVariant read FlinkColor write SetlinkColor;
    property vlinkColor: OleVariant read FvlinkColor write SetvlinkColor;
    property url: WideString read Furl write Seturl;
    property domain: WideString read Fdomain write Setdomain;
    property cookie: WideString read Fcookie write Setcookie;
    property expando: WordBool read Fexpando write Setexpando;
    property charset: WideString read Fcharset write Setcharset;
    property defaultCharset: WideString read FdefaultCharset write SetdefaultCharset;
//    property images: IHTMLElementCollection read Get_images;
//    property applets: IHTMLElementCollection read Get_applets;
//    property links: IHTMLElementCollection read Get_links;
//    property forms: IHTMLElementCollection read Get_forms;
//    property anchors: IHTMLElementCollection read Get_anchors;
//    property scripts: IHTMLElementCollection read Get_scripts;
//    property selection: IHTMLSelectionObject read Get_selection;
//    property frames: IHTMLFramesCollection2 read Get_frames;
//    property embeds: IHTMLElementCollection read Get_embeds;
//    property plugins: IHTMLElementCollection read Get_plugins;
//    property location: IHTMLLocation read Get_location;
  end;

implementation

{ THtmlItem }

procedure THtmlItem.SetKeyName(const Value: string);
begin
  FKeyName := Value;
end;

procedure THtmlItem.SetKeyValue(const Value: string);
begin
  FKeyValue := Value;
end;

{ THtmlItemCollection }

procedure THtmlItemCollection.AddItem(AKeyName, AKeyValue: string);
var
  item: THtmlItem;
begin
  item:= THtmlItem.Create;
  item.KeyName:= AKeyName;
  item.KeyValue:= AKeyValue;
  FList.Add(item);
end;

procedure THtmlItemCollection.Clear;
var
  I: Integer;
begin
  for I := 0 to FList.Count - 1 do
    TObject(FList[I]).Free;
  FList.Clear;
end;

constructor THtmlItemCollection.Create;
begin
  FList:= TList.Create;
end;

procedure THtmlItemCollection.DelItem(Index: Integer);
begin
  THtmlItem(FList[Index]).Free;
  FList.Delete(Index);
end;

procedure THtmlItemCollection.DelItem(AKeyName: string);
var
  I: Integer;
begin
  for I := 0 to FList.Count - 1 do
    if SameText(THtmlItem(FList[I]).KeyName, AKeyName)then begin
      THtmlItem(FList[I]).Free;
      FList.Delete(I);
      Exit;
    end;
end;

destructor THtmlItemCollection.Destroy;
begin
  Clear;
  FList.Free;
end;

function THtmlItemCollection.Find(AKeyName: string): Integer;
var
  I: Integer;
begin
  Result:= -1;
  for I := 0 to FList.Count - 1 do
    if SameText(THtmlItem(FList[I]).KeyName, AKeyName)then begin
      Result:= I;
      Exit;
    end;
end;

function THtmlItemCollection.GetCount: Integer;
begin
  Result:= FList.Count;
end;

function THtmlItemCollection.GetItem(Index: Integer): string;
begin
  Result:= THtmlItem(FList[Index]).KeyValue;
end;

function THtmlItemCollection.GetItem(AKeyName: string): string;
var
  I: Integer;
begin
  Result:= '';
  for I := 0 to FList.Count - 1 do
    with THtmlItem(FList[I]) do
      if SameText(KeyName, AKeyName)then begin
        Result:= KeyValue;
        Exit;
      end;
end;

procedure THtmlItemCollection.SetItem(Index: Integer; AKeyValue: string);
begin
  THtmlItem(FList[Index]).KeyValue:= AKeyValue;
end;

procedure THtmlItemCollection.SetItem(AKeyName, AKeyValue: string);
var
  I: Integer;
begin
  for I := 0 to FList.Count - 1 do
    with THtmlItem(FList[I]) do
      if SameText(KeyName, AKeyName)then begin
        KeyValue:= AKeyValue;
        Exit;
      end;

end;

{ THtmlObject }

procedure THtmlObject.SetKeyName(const Value: string);
begin
  FKeyName:= Value;
end;

procedure THtmlObject.SetKeyObject(const Value: TObject);
begin
  FKeyObject := Value;
end;

{ THtmlCollection }

procedure THtmlObjectCollection.AddItem(AKeyName: string; Value: TObject);
var
  item: THtmlObject;
begin
  item:= THtmlObject.Create;
  item.FKeyName:= AKeyName;
  item.FKeyObject:= Value;
  FList.Add(item);
end;

procedure THtmlObjectCollection.Clear;
var
  I: Integer;
begin
  for I := 0 to FList.Count - 1 do
    TObject(FList[I]).Free;
  FList.Clear;
end;

constructor THtmlObjectCollection.Create;
begin
  FList:= TList.Create;
end;

procedure THtmlObjectCollection.DelItem(Index: Integer);
begin
  THtmlObject(FList[Index]).Free;
  FList.Delete(Index);
end;

destructor THtmlObjectCollection.Destroy;
begin
  Clear;
  FList.Free;
end;

function THtmlObjectCollection.Find(AKeyName: string; FromIndex: Integer): Integer;
var
  I: Integer;
begin
  Result:= -1;
  for I := FromIndex to FList.Count - 1 do
    if SameText(THtmlObject(FList[I]).FKeyName, AKeyName)then begin
      Result:= I;
      Exit;
    end;
end;

function THtmlObjectCollection.GetCount: Integer;
begin
  Result:= FList.Count;
end;

function THtmlObjectCollection.GetObjects(Index: Integer): TObject;
begin
  Result:= THtmlObject(FList[Index]).FKeyObject;
end;

procedure THtmlObjectCollection.SetObjects(Index: Integer; const Value: TObject);
begin
  THtmlObject(FList[Index]).FKeyObject:= Value;
end;

{ THtmlElement }

procedure THtmlElement.AddAttribute(AKeyName, AKeyValue: string);
begin
  FAttribute.AddItem(AKeyName, AKeyValue);
end;

procedure THtmlElement.AddChild(Child: THtmlElement);
begin
  FChildren.AddItem(Child.tagName, Child);
end;

procedure THtmlElement.Clear;
begin
  FChildren.Clear;
  FAttribute.Clear;
end;

constructor THtmlElement.Create;
begin
  inherited;
  FChildren:= THtmlObjectCollection.Create;
  FAttribute:= THtmlItemCollection.Create;
end;

procedure THtmlElement.DeleteAttribute(Index: Integer);
begin
  FAttribute.DelItem(Index);
end;

procedure THtmlElement.DeleteAttribute(KeyName: string);
begin
  FAttribute.DelItem(KeyName);
end;

procedure THtmlElement.DeleteChild(Index: Integer);
begin
  FChildren.DelItem(Index);
end;

destructor THtmlElement.Destroy;
begin
  FChildren.Free;
  FAttribute.Free;
  inherited;
end;

function THtmlElement.FindChild(tagName: string; FromIndex: Integer): Integer;
begin
  Result:= FChildren.Find(tagName, FromIndex);
end;

function THtmlElement.GetAttrCount: Integer;
begin
  Result:= FAttribute.Count;
end;

function THtmlElement.GetAttributes(Index: Integer): string;
begin
  Result:= FAttribute.GetItem(Index);
end;

function THtmlElement.GetAttrName(Index: Integer): string;
begin
  Result:= THtmlItem(FAttribute.FList[Index]).KeyName;
end;

function THtmlElement.GetAttrValue(KeyName: string): string;
begin
  Result:= FAttribute.GetItem(KeyName);
end;

function THtmlElement.GetChildCount: Integer;
begin
  Result:= FChildren.Count;
end;

function THtmlElement.GetChildren(Index: Integer): THtmlElement;
begin
  Result:= THtmlElement(FChildren.GetObjects(Index));
end;

procedure THtmlElement.SetAttributes(Index: Integer;
  const Value: string);
begin
  FAttribute.SetItem(Index, Value);
end;

procedure THtmlElement.SetAttrName(Index: Integer; const Value: string);
begin

end;

procedure THtmlElement.SetAttrValue(KeyName: string; const Value: string);
begin
  FAttribute.SetItem(KeyName, Value);
end;

procedure THtmlElement.SetChildren(Index: Integer; const Value: THtmlElement);
begin
  FChildren.SetObjects(Index, Value);
end;

{ THtmlDocument }

procedure THtmlDocument.Close;
begin
  FParsed:= False;
  FHtmlDoc.close;
  FBody.Clear;
end;

constructor THtmlDocument.Create;
begin
  CoInitialize(nil);
  FBody:= THtmlElement.Create;
//  FHtmlDoc:= CoHTMLDocument.Create;

  //创建IHTMLDocument2接口
  CoCreateInstance(CLASS_HTMLDocument, nil, CLSCTX_INPROC_SERVER, IID_IHTMLDocument2, FHtmlDoc);
  Assert(Assigned(FHtmlDoc),'构建HTMLDocument接口失败');
  FHtmlDoc.designMode:= 'On';                                                //设置为设计模式，不执行脚本
  while not (FHtmlDoc.readyState = 'complete') do begin
    sleep(1);
    Application.ProcessMessages;
  end;
end;

destructor THtmlDocument.Destroy;
begin
  FBody.Free;
end;

function THtmlDocument.GetHtmlText: string;
begin
  Result:= FHtmlDoc.body.innerHTML;
end;

procedure THtmlDocument.LoadFromFile(AFilename: string);
var
  fm: TFileStream;
  fs: TStringStream;
  EmptyParam: OleVariant;
begin
//  FHtmlDoc.open(AFilename, EmptyParam, EmptyParam, EmptyParam);
//  Exit;
  fm:= TFileStream.Create(AFilename, fmOpenRead);
  fs:= TStringStream.Create;
  fs.CopyFrom(fm, fm.Size);
  FHtmlDoc.body.innerHTML:= fs.DataString;
  fs.Free;
  fm.Free;
end;

procedure THtmlDocument.LoadFromURL(AURL: string);
begin
  FHtmlDoc.open(AURL, '', '', false);
end;

procedure THtmlDocument.Parse;
  procedure ParseNode(Node: THtmlElement; ANode: IHTMLDOMNode);
  var
    childNode: IHTMLDOMNode;
    child: THtmlElement;
    attr: IHTMLDOMAttribute;
    I: Integer;
  begin
    Node.tagName:= ANode.nodeName;
    if Assigned(ANode.attributes) then
    for I:= 0 to (ANode.attributes AS IHTMLAttributeCollection).length - 1 do begin
      attr:= ((ANode.attributes AS IHTMLAttributeCollection).item(I) as IHTMLDOMAttribute);
      if attr.specified and (attr.nodeValue <> null) then
      Node.AddAttribute(attr.nodeName, attr.nodeValue);
    end;
    childNode:= ANode.firstChild;
    while Assigned(childNode) do begin
      child:= THtmlElement.Create;
      Node.AddChild(child);
      ParseNode(child, childNode);
      childNode:= childNode.nextSibling;
    end;
  end;
begin
  FTitle:= FHtmlDoc.title;
  FalinkColor:= FHtmlDoc.alinkColor;
  FbgColor:= FHtmlDoc.bgColor;
  FfgColor:= FHtmlDoc.fgColor;
  FlinkColor:= FHtmlDoc.linkColor;
  FvlinkColor:= FHtmlDoc.vlinkColor;
  Furl:= FHtmlDoc.url;
//  Fdomain:= FHtmlDoc.domain;
  Fcookie:= FHtmlDoc.cookie;
  Fexpando:= FHtmlDoc.expando;
  Fcharset:= FHtmlDoc.charset;
  FdefaultCharset:= FHtmlDoc.defaultCharset;
  Body.TagName:= 'body';
  ParseNode(Body, FHtmlDoc.body as IHTMLDOMNode);
//  Body.Parse(FHtmlDoc.body);
end;

procedure THtmlDocument.SaveToFile(AFilename: string);
var
  fs: TFileStream;
  ss: TStringStream;
begin
  fs:= TFileStream.Create(AFilename, fmCreate);
  ss:= TStringStream.Create;
  ss.WriteString(FHtmlDoc.body.innerHTML);
  ss.Position:= 0;
  fs.CopyFrom(ss, ss.Size);
  fs.Free;
  ss.Free;
end;

procedure THtmlDocument.SetalinkColor(const Value: OleVariant);
begin
  FalinkColor := Value;
end;

procedure THtmlDocument.SetbgColor(const Value: OleVariant);
begin
  FbgColor := Value;
end;

procedure THtmlDocument.Setcharset(const Value: WideString);
begin
  Fcharset := Value;
end;

procedure THtmlDocument.Setcookie(const Value: WideString);
begin
  Fcookie := Value;
end;

procedure THtmlDocument.SetdefaultCharset(const Value: WideString);
begin
  FdefaultCharset := Value;
end;

procedure THtmlDocument.Setdomain(const Value: WideString);
begin
  Fdomain := Value;
end;

procedure THtmlDocument.Setexpando(const Value: WordBool);
begin
  Fexpando := Value;
end;

procedure THtmlDocument.SetfgColor(const Value: OleVariant);
begin
  FfgColor := Value;
end;

procedure THtmlDocument.SetHtmlText(const Value: string);
begin
  FHtmlDoc.body.innerHTML:= Value;
end;

procedure THtmlDocument.SetlinkColor(const Value: OleVariant);
begin
  FlinkColor := Value;
end;

procedure THtmlDocument.SetParsed(const Value: Boolean);
begin
  FParsed := Value;
end;

procedure THtmlDocument.SetTitle(const Value: string);
begin
  FTitle := Value;
end;

procedure THtmlDocument.Seturl(const Value: WideString);
begin
  Furl := Value;
end;

procedure THtmlDocument.SetvlinkColor(const Value: OleVariant);
begin
  FvlinkColor := Value;
end;

end.
