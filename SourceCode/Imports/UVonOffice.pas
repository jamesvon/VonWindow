unit UVonOffice;

interface

uses Classes, SysUtils;

type
  TVonExcelWriter = class
  private
    fs : TFileStream;
    FActived: boolean;
  public
    constructor Create(filename: string);
    destructor Destroy(); override;
    procedure WriteCell(Col, Row: Integer; S: String); overload;
    procedure WriteCell(Col, Row: Integer; S: AnsiString); overload;
    procedure WriteCell(Col, Row: Integer; D: Extended); overload;
    procedure WriteCell(Col, Row: Integer; D: Double); overload;
    procedure WriteCell(Col, Row: Integer; D: TDateTime); overload;
    procedure WriteCell(Col, Row: Integer; I: Integer); overload;
  published
    property Actived: boolean read FActived;
  end;

//var
//  arXlsBegin: array[0..5] of Word = ($809, 8, 0, $10, 0, 0);
//  arXlsEnd: array[0..1] of Word = ($0A, 00);
//  arXlsString: array[0..5] of Word = ($204, 0, 0, 0, 0, 0);
//  arXlsNumber: array[0..4] of Word = ($203, 14, 0, 0, 0);
//  arXlsInteger: array[0..4] of Word = ($27E, 10, 0, 0, 0);
//  arXlsBlank: array[0..4] of Word = ($201, 6, 0, 0, $17);
implementation

{ TVonExcelWriter }

destructor TVonExcelWriter.Destroy;
const arXlsEnd: array[0..1] of Word = ($0A, 00);
begin
  fs.WriteBuffer(arXlsEnd, SizeOf(arXlsEnd));       //写文件尾
  fs.Free;
  FActived:= False;
end;

procedure TVonExcelWriter.WriteCell(Col, Row: Integer; D: Extended);
var
  v: Double;
begin
  v:= D; WriteCell(Col, Row, V);
end;

constructor TVonExcelWriter.Create(filename: string);
const arXlsBegin: array[0..5] of Word = ($809, 8, 0, $10, 0, 0);
begin
  fs := TFileStream.Create(filename, fmCreate);
  fs.WriteBuffer(arXlsBegin, SizeOf(arXlsBegin));   //写文件头
  FActived:= True;
end;

procedure TVonExcelWriter.WriteCell(Col, Row, I: Integer);
var
  V: Integer;
  arXlsInteger: array[0..4] of Word;
begin
  if not FActived then Exit;
  arXlsInteger[0] := $27E;
  arXlsInteger[1] := 10;
  arXlsInteger[2] := Row;
  arXlsInteger[3] := Col;
  arXlsInteger[4] := 0;
  fs.WriteBuffer(arXlsInteger, SizeOf(arXlsInteger));
  V := (I shl 2) or 2;
  fs.WriteBuffer(V, 4);
end;

procedure TVonExcelWriter.WriteCell(Col, Row: Integer; S: String);
var
  V: AnsiString;
begin
  v:= S; WriteCell(Col, Row, V);
end;

procedure TVonExcelWriter.WriteCell(Col, Row: Integer; S: AnsiString);
var
  L: Word;
  arXlsString: array[0..5] of Word;
begin
  if not FActived then Exit;
  L := Length(S);
  arXlsString[0] := $204;
  arXlsString[1] := 8 + L;
  arXlsString[2] := Row;
  arXlsString[3] := Col;
  arXlsString[4] := 0;
  arXlsString[5] := L;
  fs.WriteBuffer(arXlsString, SizeOf(arXlsString));
  fs.WriteBuffer(Pointer(S)^, L);
end;

procedure TVonExcelWriter.WriteCell(Col, Row: Integer; D: Double);
var
  arXlsNumber: array[0..4] of Word;
begin
  if not FActived then Exit;
  arXlsNumber[0] := $203;
  arXlsNumber[1] := 14;
  arXlsNumber[2] := Row;
  arXlsNumber[3] := Col;
  arXlsNumber[4] := 0;
  fs.WriteBuffer(arXlsNumber, SizeOf(arXlsNumber));
  fs.WriteBuffer(D, 8);
end;

procedure TVonExcelWriter.WriteCell(Col, Row: Integer; D: TDateTime);
var
  S: string;
begin
  if not FActived then Exit;
  if Frac(D) = 0 then WriteCell(Col, Row, DateToStr(D))
  else WriteCell(Col, Row, DateTimeToStr(D));
end;

end.
