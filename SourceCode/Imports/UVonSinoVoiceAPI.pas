unit UVonSinoVoiceAPI;

interface

uses SysUtils, Dialogs, Classes, WinApi.Windows, WinApi.Messages, JTTS_ACTIVEXLib_TLB;

procedure InitSinoVoice(h: THandle);
procedure FreeSinoVoice;
procedure Say(Text: string);
procedure StopSay;
procedure PauseSay;
procedure ContinueSay;
procedure GetVoiceTypes(Items: TStrings);
procedure GetTextCodes(Items: TStrings);
procedure SetVoiceType(TypeIdx: Integer);

var
  CanUseVoice: Boolean;
  gjttsConf: JTTS_CONFIG;

implementation

uses UVonSystemFuns, UVonLog;

var
  gVoiceIDs: TStrings;
  gSessionID: DWORD;
  gLogfont: LOGFONT;
  gFileName: Ansistring;
  gCharset: Cardinal;

procedure SetVoiceType(TypeIdx: Integer);
begin
  lstrcpyA(gjttsConf.szVoiceID, PAnsiChar(gVoiceIDs[TypeIdx]));
  jTTS_Set(@gjttsConf);
end;

procedure InitSinoVoice(h: THandle);
var
  err: ERRCODE;
  voiceCount: Integer;
  i: Integer;
begin
  CanUseVoice := False;
  err := jTTS_Init(nil, '');
  if err <> ERR_NONE then
  begin
    WriteLog(LOG_FAIL, 'JSS', 'jTTS��ʼ����:' + IntToStr(Integer(err)));
    Exit;
  end;

  gVoiceIDs := TStringList.Create();

  jTTS_SetPlay(UINT(-1), h, nil, 0);
  CanUseVoice := True;
  FOnSay := @Say;
  FOnStopSay := @StopSay;
  // lstrcpyA(gjttsConf.szVoiceID, PAnsiChar(gVoiceIDs[0]));
  jTTS_Set(@gjttsConf);
end;

procedure GetTextCodes(Items: TStrings);
begin
  with Items do
  begin
    AddObject('gb', nil);
    AddObject('big5', nil);
    AddObject('ShiftJIS', nil);
    AddObject('iso8859-1', nil);
    AddObject('Unicode', nil);
    AddObject('Unicode bige', nil);
    AddObject('utf8', nil);
  end;
end;

procedure GetVoiceTypes(Items: TStrings);
var
  i, voiceCount: Integer;
  voiceAttr: JTTS_VOICEATTRIBUTE;
  langAttr: JTTS_LANGATTRIBUTE;
  str: string;
begin
  if not CanUseVoice then
    Exit;

  voiceCount := jTTS_GetVoiceCount();
  for i := 0 to voiceCount - 1 do
  begin
    jTTS_GetVoiceAttribute(i, @voiceAttr);
    gVoiceIDs.Add(string(voiceAttr.szVoiceID));
    jTTS_GetLangAttributeByValue(voiceAttr.nLanguage, @langAttr);
    str := string(voiceAttr.szName) + '(' + string(langAttr.szName);
    if voiceAttr.nGender = GENDER_FEMALE then
      str := str + ' Ů��'
    else if voiceAttr.nGender = GENDER_MALE then
      str := str + ' ����';
    str := str + ')';
    Items.AddObject(str, nil);
  end;

end;

procedure Say(Text: string);
var
  err: ERRCODE;
  AText: Ansistring;
begin
  if not CanUseVoice then
    Exit;
  jTTS_Stop();
  AText := Text;
  err := jTTS_Play(PAnsiChar(AText), PLAYCONTENT_TEXT or PLAY_INTERRUPT or
    PLAYMODE_ASYNC);
  if err <> ERR_NONE then
    ShowMessage('jTTS_Play����:' + IntToStr(Integer(err)));
end;

procedure StopSay;
begin
  if not CanUseVoice then
    Exit;
  jTTS_Stop();
end;

procedure PauseSay;
begin
  if not CanUseVoice then
    Exit;
  jTTS_Pause();
end;

procedure ContinueSay;
begin
  if not CanUseVoice then
    Exit;
  jTTS_Resume();
end;

procedure FreeSinoVoice;
begin
  if not CanUseVoice then
    Exit;
  jTTS_End();
end;

initialization
  WriteLog(LOG_DEBUG, 'UVonSinoVoiceAPI', 'initialization ... ...');
  with gjttsConf do
  begin
    wVersion := JTTS_VERSION4;
    nCodePage := CODEPAGE_GB;
    ZeroMemory(@szVoiceID, SizeOf(szVoiceID));
    nDomain := DOMAIN_COMMON;
    nPitch := 5;
    nVolume := 5;
    nSpeed := SPEED_MAX;
    nPuncMode := PUNC_OFF;
    nDigitMode := DIGIT_TELEGRAM;
    nEngMode := ENG_LETTER;
    nTagMode := TAG_AUTO;
    ZeroMemory(@nReserved, SizeOf(nReserved));
  end;

finalization

FreeSinoVoice;

end.
