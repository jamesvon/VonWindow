unit UDlgOrganization;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, UFrameLonLat, ExtCtrls, DB, data.win.ADODB, UPlatformInfo,
  UPlatformDB, Mask, UVonLog;

type
  TEventOfUploadOrganization = procedure (Info: TOrganizationInfo);

  /// <summary>组织结构管理，如果Exe</summary>
  TFDlgOrganization = class(TForm)
    DQZone: TADOQuery;
    Label1: TLabel;
    lbKind: TLabel;
    EOrgName: TLabeledEdit;
    EState: TComboBox;
    ECounty: TComboBox;
    EManager: TLabeledEdit;
    ETelphong: TLabeledEdit;
    EAddress: TLabeledEdit;
    btnOK: TBitBtn;
    BitBtn2: TBitBtn;
    ECity: TComboBox;
    EKind: TComboBox;
    lbCity: TLabel;
    lbCounty: TLabel;
    lbOrgID: TLabel;
    ECode: TMaskEdit;
    lbCode: TLabel;
    FrameLonLat1: TFrameLonLat;
    FrameLonLat2: TFrameLonLat;
    procedure FormCreate(Sender: TObject);
    procedure EStateChange(Sender: TObject);
    procedure ECityChange(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
    FIsSelf: Boolean;
  public
    { Public declarations }
    procedure FormToInfo(TInfo: TOrganizationInfo);
    procedure InfoToForm(TInfo: TOrganizationInfo);
  end;

var
  FDlgOrganization: TFDlgOrganization;

implementation

uses UVonSystemFuns;

{$R *.dfm}

procedure TFDlgOrganization.FormCreate(Sender: TObject);
var
  szID: Integer;
  szOrgID: string;
begin
  lbKind.Caption:= FPlatformDB.GetListOption('单位性质', Ekind.Items);
  lbKind.Visible:= lbKind.Caption <> '';
  Ekind.Visible:= lbKind.Visible;
  with DQZone do
  begin
    Connection:= FPlatformDB.ADOConn;
    Parameters[0].Value:= 0;
    Open;
    while not Eof do
    begin
      EState.Items.AddObject(FieldByName('ItemName').AsString,
        TObject(FieldByName('ID').AsInteger));
      Next;
    end;
    Close;
  end;
  FrameLonLat1.Visible:= SameText(GetRuntimeValue('组织结构是否有经纬度'), 'True');
  FrameLonLat2.Visible:= FrameLonLat1.Visible;
  FIsSelf:= FPlatformDB.TaskParams = '-1';
  if FIsSelf then
    InfoToForm(FPlatformDB.OrgInfo);
end;

procedure TFDlgOrganization.BitBtn2Click(Sender: TObject);
begin
  if FIsSelf then Close
  else ModalResult:= mrCancel;
end;

procedure TFDlgOrganization.btnOKClick(Sender: TObject);
begin
  if FIsSelf then begin
    FormToInfo(FPlatformDB.OrgInfo);
    FPlatformDB.OrgInfo.SaveData(FPlatformDB.ADOConn);
    Close;
  end else ModalResult:= mrOK;
end;

procedure TFDlgOrganization.ECityChange(Sender: TObject);
begin
  with DQZone do
    try
      Parameters[0].Value := Integer(ECity.Items.Objects[ECity.ItemIndex]);
      Open;
      ECounty.Visible := True;
      ECounty.Items.Clear;
      while not Eof do
      begin
        ECounty.Items.AddObject(FieldByName('ItemName').AsString,
          TObject(FieldByName('ID').AsInteger));
        Next;
      end;
      ECounty.ItemIndex := 0;
    finally
      Close;
    end;
end;

procedure TFDlgOrganization.EStateChange(Sender: TObject);
begin
  with DQZone do
    try
      Parameters[0].Value := Integer(EState.Items.Objects[EState.ItemIndex]);
      Open;
      ECity.Visible := True;
      ECity.Items.Clear;
      while not Eof do
      begin
        ECity.Items.AddObject(FieldByName('ItemName').AsString,
          TObject(FieldByName('ID').AsInteger));
        Next;
      end;
      ECity.ItemIndex := 0;
    finally
      Close;
    end;
end;

procedure TFDlgOrganization.FormToInfo(TInfo: TOrganizationInfo);
begin
  with TInfo do
  begin
    OrgName := EOrgName.Text;
    if EState.Visible then
      State := EState.Text
    else
      State := '';
    if ECity.Visible then
      City := ECity.Text
    else
      City := '';
    if ECounty.Visible then
      County := ECounty.Text
    else
      County := '';
    OrgCode := ECode.Text;
    Kind := EKind.ItemIndex;
    Address := EAddress.Text;
    Manager := EManager.Text;
    Telphong := ETelphong.Text;
    Lon := FrameLonLat1.L;
    LonM := FrameLonLat1.M;
    LonS := FrameLonLat1.S;
    Lat := FrameLonLat2.L;
    LatM := FrameLonLat2.M;
    LatS := FrameLonLat2.S;
  end;
end;

procedure TFDlgOrganization.InfoToForm(TInfo: TOrganizationInfo);
begin
  ECode.Text := TInfo.OrgCode;
  EOrgName.Text := TInfo.OrgName;
  lbOrgID.Caption := GuidToStr(TInfo.OrgID);
  if TInfo.State <> '' then
  begin
    EState.ItemIndex := EState.Items.IndexOf(TInfo.State);
    EStateChange(nil);
    if TInfo.City <> '' then
    begin
      ECity.ItemIndex := ECity.Items.IndexOf(TInfo.City);
      ECityChange(nil);
      if TInfo.County <> '' then
        ECounty.ItemIndex := ECounty.Items.IndexOf(TInfo.County);
    end;
  end;
  EKind.ItemIndex:= TInfo.Kind;
  EAddress.Text := TInfo.Address;
  EManager.Text := TInfo.Manager;
  ETelphong.Text := TInfo.Telphong;
  FrameLonLat1.L := TInfo.Lon;
  FrameLonLat1.M := TInfo.LonM;
  FrameLonLat1.S := TInfo.LonS;
  FrameLonLat2.L := TInfo.Lat;
  FrameLonLat2.M := TInfo.LatM;
  FrameLonLat2.S := TInfo.LatS;
end;

initialization
  //WriteLog(LOG_DEBUG, 'UDlgOrganization', 'RegisteRuntime 组织结构是否有经纬度');
  RegisteRuntime('组织结构是否有经纬度', '<HasLongLat xmlns="布尔" Title="有经纬度"></HasLongLat>', 'true');
end.
