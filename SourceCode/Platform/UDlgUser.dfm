object FDlgUser: TFDlgUser
  Left = 0
  Top = 0
  Caption = 'FDlgUser'
  ClientHeight = 280
  ClientWidth = 655
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object spExtend: TSplitter
    Left = 353
    Top = 0
    Height = 280
    ExplicitLeft = 346
    ExplicitHeight = 415
  end
  object spDepartment: TSplitter
    Left = 541
    Top = 0
    Height = 280
    ExplicitLeft = 475
    ExplicitHeight = 415
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 153
    Height = 280
    Align = alLeft
    Caption = 'Panel2'
    Locked = True
    ShowCaption = False
    TabOrder = 0
    DesignSize = (
      153
      280)
    object lbWork: TLabel
      Left = 6
      Top = 182
      Width = 24
      Height = 13
      Caption = #23703#20301
    end
    object lbGroupName: TLabel
      Left = 6
      Top = 228
      Width = 24
      Height = 13
      Caption = #29677#27425
    end
    object lbJob: TLabel
      Left = 6
      Top = 136
      Width = 24
      Height = 13
      Caption = #32844#21153
    end
    object ELoginName: TLabeledEdit
      Left = 6
      Top = 22
      Width = 137
      Height = 21
      EditLabel.Width = 36
      EditLabel.Height = 13
      EditLabel.Caption = #30331#24405#21517
      TabOrder = 0
    end
    object EDisplayName: TLabeledEdit
      Left = 6
      Top = 65
      Width = 137
      Height = 21
      EditLabel.Width = 36
      EditLabel.Height = 13
      EditLabel.Caption = #26174#31034#21517
      TabOrder = 1
    end
    object ELoginPwd: TLabeledEdit
      Left = 6
      Top = 109
      Width = 137
      Height = 21
      EditLabel.Width = 48
      EditLabel.Height = 13
      EditLabel.Caption = #30331#24405#21475#20196
      TabOrder = 2
    end
    object EWork: TComboBox
      Left = 6
      Top = 201
      Width = 137
      Height = 21
      Style = csDropDownList
      TabOrder = 3
    end
    object EGroupName: TComboBox
      Left = 6
      Top = 247
      Width = 137
      Height = 21
      Style = csDropDownList
      TabOrder = 4
    end
    object EJob: TComboBox
      Left = 6
      Top = 155
      Width = 137
      Height = 21
      Style = csDropDownList
      TabOrder = 5
    end
    object Button1: TButton
      Left = 72
      Top = 423
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = #20445#23384
      ImageIndex = 29
      ImageMargins.Left = 5
      ModalResult = 1
      TabOrder = 6
      ExplicitTop = 344
    end
    object Button2: TButton
      Left = 72
      Top = 454
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = #25918#24323
      ImageIndex = 21
      ImageMargins.Left = 5
      ModalResult = 2
      TabOrder = 7
      ExplicitTop = 375
    end
  end
  object gbDepartment: TGroupBox
    Left = 356
    Top = 0
    Width = 185
    Height = 280
    Align = alLeft
    Caption = #25152#23646#37096#38376
    TabOrder = 1
    ExplicitHeight = 201
    object treeDepartment: TTreeView
      Left = 2
      Top = 15
      Width = 181
      Height = 263
      Align = alClient
      Indent = 19
      TabOrder = 0
      ExplicitHeight = 184
    end
  end
  object gbSubRight: TGroupBox
    Left = 544
    Top = 0
    Width = 111
    Height = 280
    Align = alLeft
    Caption = #31169#26377#26435#38480
    TabOrder = 2
    object lstRight: TCheckListBox
      Left = 2
      Top = 15
      Width = 107
      Height = 263
      Align = alClient
      ItemHeight = 13
      TabOrder = 0
    end
  end
  object gbExtends: TGroupBox
    Left = 153
    Top = 0
    Width = 200
    Height = 280
    Align = alLeft
    Caption = #25193#23637#20449#24687
    TabOrder = 3
    ExplicitHeight = 201
    object lstExtends: TValueListEditor
      Left = 2
      Top = 15
      Width = 196
      Height = 263
      Align = alClient
      DefaultColWidth = 50
      TabOrder = 0
      TitleCaptions.Strings = (
        #39033#30446
        #20869#23481)
      ExplicitHeight = 184
      ColWidths = (
        50
        140)
    end
  end
end
