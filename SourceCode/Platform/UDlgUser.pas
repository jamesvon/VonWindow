(* *****************************************************************************
* UDlgUser written by James Von (jamesvon@163.com) 2017.4     ******************
********************************************************************************
* 人员信息录入子窗口
*------------------------------------------------------------------------------*
*     人员信息中的部分内容是否录入，要看系统选项中是否有录入内容，有则可以录入
* 人员职务：在系统选项中如果存在《人员职务》项目则允许录入
* 人员岗位：在系统选项中如果存在《人员岗位》项目则允许录入
* 人员班次：在系统选项中如果存在《人员班次》项目则允许录入
* 人员扩展信息：在系统选项中如果存在《人员扩展信息》项目则允许录入
*     人员信息中的部分内容是否录入，要看系统参数中的运行参数的控制和内容设置
* 人员信息是否有部门：系统参数中的运行参数，允许则可以选择所属部门
* 人员特殊权限内容：系统参数中的运行参数，设置内容后就可以进行选择了
*=============================================================================*)
unit UDlgUser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, UVonLog,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.CheckLst,
  Vcl.ComCtrls, UFrameTree, Vcl.ExtCtrls, Data.DB, Data.Win.ADODB,
  UVonSystemFuns, UPlatformDB, UPlatformInfo, Vcl.Grids, Vcl.ValEdit;

type
  TFDlgUser = class(TForm)
    Panel2: TPanel;
    lbWork: TLabel;
    lbGroupName: TLabel;
    lbJob: TLabel;
    ELoginName: TLabeledEdit;
    EDisplayName: TLabeledEdit;
    ELoginPwd: TLabeledEdit;
    EWork: TComboBox;
    EGroupName: TComboBox;
    EJob: TComboBox;
    gbDepartment: TGroupBox;
    treeDepartment: TTreeView;
    gbSubRight: TGroupBox;
    lstRight: TCheckListBox;
    Button1: TButton;
    Button2: TButton;
    gbExtends: TGroupBox;
    lstExtends: TValueListEditor;
    spExtend: TSplitter;
    spDepartment: TSplitter;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure LoadDepartment(Node: TTreeNode; PID: Integer);
  public
    { Public declarations }
    procedure FormToInfo(FInfo: TUserInfo);
    procedure InfoToForm(FInfo: TUserInfo);
  end;

var
  FDlgUser: TFDlgUser;

implementation

{$R *.dfm}

procedure TFDlgUser.FormCreate(Sender: TObject);
var
  szSpecial: string;
begin
  lbJob.Caption:= FPlatformDB.GetDropList('人员职务', EJob, '');
  lbJob.Visible:= lbJob.Caption <> ''; EJob.Visible:= lbJob.Visible;
  lbWork.Caption:= FPlatformDB.GetDropList('人员岗位', EWork, '');
  lbWork.Visible:= lbWork.Caption <> ''; EWork.Visible:= lbWork.Visible;
  lbGroupName.Caption:= FPlatformDB.GetDropList('人员班次', EGroupName, '');
  lbGroupName.Visible:= lbGroupName.Caption <> ''; EGroupName.Visible:= lbGroupName.Visible;
{$REGION 'Load department'}
  treeDepartment.Visible:= GetRuntimeValue('人员信息是否有部门') = 'True';
  gbDepartment.Visible:= treeDepartment.Visible;
  if treeDepartment.Visible then begin
    treeDepartment.Images:= FPlatformDB.ImgSmall;
    LoadDepartment(nil, 0);
  end;
  spDepartment.Visible:= treeDepartment.Visible;
{$ENDREGION}
{$REGION 'Load sub rights'}
  szSpecial:= GetRuntimeValue('人员特殊权限内容');
  gbSubRight.Visible:= szSpecial <> '';
  if gbSubRight.Visible then begin
    lstRight.Items.Delimiter:= ',';
    lstRight.Items.DelimitedText:= szSpecial;
  end;
{$ENDREGION}
{$REGION 'Load extends'}
  gbExtends.Caption:= FPlatformDB.GetListOption('人员扩展信息', lstExtends.Strings, 3);
  gbExtends.Visible:= gbExtends.Caption <> ''; spExtend.Visible:= gbExtends.Visible;
{$ENDREGION}
end;

procedure TFDlgUser.FormToInfo(FInfo: TUserInfo);
var
  I: Integer;
begin
  with FInfo do begin
    LoginName := ELoginName.Text;
    DisplayName := EDisplayName.Text;
    if (ID = 0) or (ELoginPwd.Text <> '') then
      ChangePWD(ELoginPwd.Text);
    if treeDepartment.Visible then begin
      if not Assigned(treeDepartment.Selected) then
        raise Exception.Create('未指定所属部门，不允许添加。');
      DepartmentIdx:= Integer(treeDepartment.Selected.Data);
    end;
    if gbExtends.Visible then
      Extends.Assign(lstExtends.Strings);
    if gbSubRight.Visible then begin
      for I := 0 to 9 do
        SubRight[I]:= 0;
      for I := 0 to lstRight.Count - 1 do
        if lstRight.Checked[I] then
          SubRight[9 - (I div 8)]:= SubRight[I div 8] or (1 shl (I mod 8));
    end;
    if lbJob.Visible then Job:= EJob.Text;
    if lbGroupName.Visible then GroupName:= EGroupName.Text;
    if lbWork.Visible then Work:= EWork.Text;
  end;
end;

procedure TFDlgUser.InfoToForm(FInfo: TUserInfo);
var
  I: Integer;
begin
  with FInfo do begin
    ELoginName.Text := LoginName;
    EDisplayName.Text := DisplayName;
    ELoginPwd.Text := '';
    if treeDepartment.Visible then begin
      for I := 0 to treeDepartment.Items.Count - 1 do
        if Integer(treeDepartment.Items[I].Data) = DepartmentIdx then begin
          treeDepartment.Items[I].Selected:= True;
          Break;
        end;
    end;
    if gbExtends.Visible then
      lstExtends.Strings.Assign(Extends);
    if gbSubRight.Visible then begin
      for I := 0 to lstRight.Count - 1 do
        lstRight.Checked[I]:= (SubRight[I div 8] and (1 shl (I mod 8))) > 0;
    end;
    if lbJob.Visible then EJob.Text:= Job;
    if lbGroupName.Visible then EGroupName.Text:= GroupName;
    if lbWork.Visible then EWork.Text:= Work;
  end;
end;

procedure TFDlgUser.LoadDepartment(Node: TTreeNode; PID: Integer);
var
  newNode: TTreeNode;
begin
  with TADOQuery.Create(nil)do try
    Connection:= FPlatformDB.ADOConn;
    SQL.Text:= 'SELECT ID,OrgName FROM SYS_Organization WHERE PID=' + IntToStr(PID);
    Open;
    while not EOF do begin
      newNode:= treeDepartment.Items.AddChild(Node, FieldByName('OrgName').AsString);
      newNode.Data:= Pointer(FieldByName('ID').AsInteger);
      newNode.ImageIndex:= 110;
      newNode.SelectedIndex:= 19;
      LoadDepartment(newNode, FieldByName('ID').AsInteger);
      Next;
    end;
  finally
    Free;
  end;
end;

initialization
  //WriteLog(LOG_DEBUG, 'UDlgUser', 'RegisteRuntime 人员信息是否有部门, 人员特殊权限内容');
  RegisteRuntime('人员信息是否有部门', '<HasOrg xmlns="布尔" Title="有部门"></HasOrg>', 'false');
  RegisteRuntime('人员特殊权限内容', '<SubRight xmlns="多行文本" Title="特殊权限" type="单行输出"></SubRight>', '');
  //RegisteRuntime('人员扩展信息', '<多行文本:Extend Title="扩展信息" type="单行输出"></Extend>', '');
end.
