﻿(**************************************************************
* Copyright:      Jamesvon  mailto:Jamesvon@163.COM
* Author:         James von
* Remarks:        SYS project files, Copyright by jamesvon
* known Problems: none
* Version:        1.0
* Description:    组织信息 主程序
* Include:        
*=============================================================*
* TFOrganization description                           
*-------------------------------------------------------------*
*
**************************************************************)

unit UOrganization;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, ExtCtrls, DB, Data.Win.ADODB, Vcl.ComCtrls, UVonLog,
  UVonSystemFuns, UPlatformInfo, UPlatformDB, UDlgOrganization, UFrameGridBar,
  UFrameTree;

type
  TFOrganization = class(TForm)
    DQOrganization: TADOQuery;
    DQZone: TADOQuery;
    FrameTree1: TFrameTree;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FrameTree1btnTreeAddClick(Sender: TObject);
    procedure FrameTree1btnTreeAddChildClick(Sender: TObject);
    procedure FrameTree1btnTreeCloneClick(Sender: TObject);
    procedure FrameTree1btnTreeEditClick(Sender: TObject);
    procedure FrameTree1btnTreeDelClick(Sender: TObject);
  private
    { Private declarations }
    FInfo: TOrganizationInfo;
    procedure EnevtOfUpdateOrder(Sender: TObject; Node: TTreeNode);
  public
    { Public declarations }
  end;

var
  FOrganization: TFOrganization;

implementation

{$R *.dfm}

procedure TFOrganization.FormCreate(Sender: TObject);
begin
  FInfo:= TOrganizationInfo.Create;
  DQOrganization.Connection:= FPlatformDB.ADOConn;
  DQZone.Connection:= FPlatformDB.ADOConn;

  FrameTree1.DisplayCheckBox := False;
  FrameTree1.DisplayTool := True;
  FrameTree1.DBConn := FPlatformDB.ADOConn;
  FrameTree1.FieldList := '*';
  FrameTree1.TableName:= 'SYS_Organization';
  FrameTree1.ConditionSQL:= '';
  FrameTree1.DisplayFieldName := 'OrgName';
  FrameTree1.DisplayOrder := 'ListOrder';
  FrameTree1.ImgFieldName := 'Kind';
  FrameTree1.KeyFieldName := 'ID';
  FrameTree1.ParentFieldName := 'PID';
  FrameTree1.ImgFieldRate := 2;
  FrameTree1.ImageOffset := 0;
  FrameTree1.SelectedOffset := 1;
  FrameTree1.OnUpdateOrder := EnevtOfUpdateOrder;
  FrameTree1.Load(nil, IntToStr(FPlatformDB.OrgInfo.ID));
end;

procedure TFOrganization.FormDestroy(Sender: TObject);
begin
  FInfo.Free;
end;

(* Events of tree button *)

procedure TFOrganization.FrameTree1btnTreeAddChildClick(Sender: TObject);
var
  newNode: TTreeNode;
  szGuid: TGUID;
begin
  if Assigned(FrameTree1.tree.Selected)then
    FInfo.PID := StrToInt(FrameTree1.ID[FrameTree1.tree.Selected])
  else
    FInfo.PID := FPlatformDB.OrgInfo.ID;
  with FInfo do begin
    ID := 0;
    CreateGUID(szGuid);
    OrgID:= szGuid;
  end;
  with TFDlgOrganization.Create(nil)do try
    if ShowModal = mrOK then begin
      FormToInfo(FInfo);
      newNode := FrameTree1.AddChild(FrameTree1.tree.Selected, FInfo.OrgName, FInfo.Kind);
      FInfo.ListOrder:= newNode.Index;
      DQOrganization.Close;
      DQOrganization.Parameters[0].Value:= 0;
      DQOrganization.Open;
      FInfo.SaveToDB(DQOrganization);
      FrameTree1.ID[newNode]:= IntToStr(FInfo.ID);
    end;
  finally
    Free;
  end;
end;

procedure TFOrganization.FrameTree1btnTreeAddClick(Sender: TObject);
var
  newNode: TTreeNode;
  szGuid: TGUID;
begin
  if Assigned(FrameTree1.tree.Selected) and
    Assigned(FrameTree1.tree.Selected.Parent) then
    FInfo.PID := StrToInt(FrameTree1.ID[FrameTree1.tree.Selected.Parent])
  else
    FInfo.PID := FPlatformDB.OrgInfo.ID;
  with FInfo do begin
    ID := 0;
    CreateGUID(szGuid);
    OrgID:= szGuid;
  end;
  with TFDlgOrganization.Create(nil)do try
    if ShowModal = mrOK then begin
      FormToInfo(FInfo);
      newNode := FrameTree1.AddNode(FrameTree1.tree.Selected, FInfo.OrgName, FInfo.Kind);
      FInfo.ListOrder:= newNode.Index;
      DQOrganization.Close;
      DQOrganization.Parameters[0].Value:= 0;
      DQOrganization.Open;
      FInfo.SaveToDB(DQOrganization);
      FrameTree1.ID[newNode]:= IntToStr(FInfo.ID);
    end;
  finally
    Free;
  end;
end;

procedure TFOrganization.FrameTree1btnTreeCloneClick(Sender: TObject);
var
  newNode: TTreeNode;
  szGuid: TGUID;
  S: string;
begin
  if not Assigned(FrameTree1.tree.Selected) then Exit;
  DQOrganization.Close;
  DQOrganization.Parameters[0].Value:= StrToInt(FrameTree1.ID[FrameTree1.tree.Selected]);
  DQOrganization.Open;
  FInfo.LoadFromDB(DQOrganization);
  S:= FInfo.OrgName;
  if not InputQuery('录入', '名称', S) then Exit;
  with FInfo do begin
    ID := 0;
    OrgName := S;
    CreateGUID(szGuid);
    OrgID:= szGuid;
  end;
  newNode := FrameTree1.AddNode(FrameTree1.tree.Selected, S, FInfo.Kind);
  FInfo.ListOrder:= newNode.Index;
  FInfo.SaveToDB(DQOrganization);
  FrameTree1.ID[newNode]:= IntToStr(FInfo.ID);
end;


procedure TFOrganization.FrameTree1btnTreeDelClick(Sender: TObject);
begin
  if not Assigned(FrameTree1.tree.Selected) then
    raise Exception.Create(RES_NODE_NONESELECTED);
  if FrameTree1.tree.Selected.HasChildren then
    raise Exception.Create(RES_NODE_HASCHILDREN);
  if MessageDlg(RES_DLG_DEL_PROMPT, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  if MessageDlg(RES_DLG_DEL_WARNING, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  DQOrganization.Close;
  DQOrganization.Parameters[0].Value:= StrToInt(FrameTree1.ID[FrameTree1.tree.Selected]);
  DQOrganization.Open;
  DQOrganization.Delete;
  FrameTree1.tree.Selected.Delete;
end;

procedure TFOrganization.FrameTree1btnTreeEditClick(Sender: TObject);
begin
  if not Assigned(FrameTree1.tree.Selected) then Exit;
  DQOrganization.Close;
  DQOrganization.Parameters[0].Value:= StrToInt(FrameTree1.ID[FrameTree1.tree.Selected]);
  DQOrganization.Open;
  FInfo.LoadFromDB(DQOrganization);
  with TFDlgOrganization.Create(nil)do try
    InfoToForm(FInfo);
    if ShowModal = mrOK then begin
      FormToInfo(FInfo);
      with FrameTree1.tree.Selected do begin
        Text:= FInfo.OrgName;
        ImageIndex := FInfo.Kind + FrameTree1.ImageOffset;
        SelectedIndex := FInfo.Kind + FrameTree1.SelectedOffset;
      end;
      FInfo.SaveToDB(DQOrganization);
    end;
  finally
    Free;
  end;
end;

procedure TFOrganization.EnevtOfUpdateOrder(Sender: TObject; Node: TTreeNode);
begin
  with DQOrganization do
    try
      Parameters[0].Value := StrToInt(FrameTree1.ID[Node]);
      Open;
      Edit;
      if Assigned(Node.Parent) then
        FieldByName('PID').AsInteger := StrToInt(FrameTree1.ID[Node.Parent])
      else
        FieldByName('PID').AsInteger := 0;
      FieldByName('ListOrder').AsInteger := Node.Index;
      Post;
    finally
      Close;
    end;
  if Assigned(Node.getNextSibling()) then
    EnevtOfUpdateOrder(Sender, Node.getNextSibling);
end;

initialization
  //WriteLog(LOG_DEBUG, 'UDlgUser', 'RegAppConfig SYSTEM-OrgCodeFormat=组织结构编码规则, SYSTEM-OrgHasLonLate=组织结构信息中是否包含经纬度');
  RegAppConfig('SYSTEM', 'OrgCodeFormat', '组织结构编码规则', LIDT_TEXT, '', '2224');
  RegAppConfig('SYSTEM', 'OrgHasLonLate', '组织结构信息中是否包含经纬度', LIDT_Bool, '', 'false');
finalization
end.

