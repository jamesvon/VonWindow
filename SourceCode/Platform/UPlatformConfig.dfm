object FPlatformConfig: TFPlatformConfig
  Left = 0
  Top = 0
  Caption = 'FPlatformConfig'
  ClientHeight = 416
  ClientWidth = 382
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCanResize = FormCanResize
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inline FrameSettingInputor1: TFrameSettingInputor
    Left = 0
    Top = 0
    Width = 382
    Height = 375
    Align = alClient
    TabOrder = 1
    ExplicitWidth = 382
    ExplicitHeight = 375
    inherited lbTitle: TLabel
      Width = 382
    end
    inherited ScrollBox1: TScrollBox
      Width = 382
      Height = 362
      ExplicitWidth = 382
      ExplicitHeight = 362
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 375
    Width = 382
    Height = 41
    Align = alBottom
    Caption = 'Panel1'
    ShowCaption = False
    TabOrder = 0
    DesignSize = (
      382
      41)
    object BitBtn1: TBitBtn
      Left = 8
      Top = 6
      Width = 75
      Height = 25
      Caption = #30830#23450
      Kind = bkOK
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BitBtn1Click
    end
    object BitBtn2: TBitBtn
      Left = 296
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #25918#24323
      Kind = bkCancel
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BitBtn2Click
    end
  end
end
