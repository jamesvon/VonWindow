unit UPlatformConfig;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, UFrameSettingInputor, UVonSystemFuns,
  IniFiles, UVonLog;

type
  TFPlatformConfig = class(TForm)
    FrameSettingInputor1: TFrameSettingInputor;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FPlatformConfig: TFPlatformConfig;

implementation

{$R *.dfm}

procedure TFPlatformConfig.BitBtn1Click(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to FAppConfigList.Count - 1 do
    FAppConfigList.Values[I, 6] := FrameSettingInputor1.Data[I].AsString;
  Close;
end;

procedure TFPlatformConfig.BitBtn2Click(Sender: TObject);
begin
  Close;
end;

procedure TFPlatformConfig.FormCanResize(Sender: TObject;
  var NewWidth, NewHeight: Integer; var Resize: Boolean);
begin
  WriteLog(LOG_DEBUG, 'PlatformConfig FormCanResize', Caption);
  FrameSettingInputor1.LabelWidth := NewWidth;
end;

procedure TFPlatformConfig.FormCreate(Sender: TObject);
var
  I, Idx, tp: Integer;
begin
  for I := 0 to FAppConfigList.Count - 1 do
  try
    if TryStrToInt(FAppConfigList.Values[I, 3], tp) then
      Idx := FrameSettingInputor1.Add(FAppConfigList.Values[I, 0] + '_' +
        FAppConfigList.Values[I, 1], FAppConfigList.Values[I, 2],
        FAppConfigList.Values[I, 4], FAppConfigList.Values[I, 5],
        TVonListInputDataType(tp), 600)
    else ShowMessage(FAppConfigList.Values[I, 0] + '_' +
      FAppConfigList.Values[I, 1] + ' error:' + FAppConfigList.Values[I, 3]);
    FrameSettingInputor1.Data[Idx].AsString := FAppConfigList.Values[I, 6];
    WriteLog(LOG_Debug, 'PlatformConfig', FAppConfigList.Values[I, 0] + '_' +
      FAppConfigList.Values[I, 1] + ':' + FAppConfigList.Values[I, 2] + '+' +
      FAppConfigList.Values[I, 3] + '/' + FAppConfigList.Values[I, 4] + '-' +
      FAppConfigList.Values[I, 5] + '?' + FAppConfigList.Values[I, 6]);
  except
    On E: Exception do ShowMessage(FAppConfigList.Values[I, 0] + '_' +
      FAppConfigList.Values[I, 1] + ' error:' + E.Message);
  end;
  WriteLog(LOG_DEBUG, 'PlatformConfig over', Caption);
end;

end.
