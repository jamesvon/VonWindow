unit UPlatformDB;
{$define debug}
interface

uses
  SysUtils, Controls, Classes, Forms, Winapi.Windows, DB, data.win.ADODB,
  StdCtrls, UVonMenuInfo, UVonSystemFuns, UPlatformInfo, UVonClass, IniFiles,
  ImgList, Dialogs, UVonLog, UOfficeExport, System.ImageList, winapi.ShellAPI,
  Winapi.Messages, UVonCrypt;

type
  TFPlatformDB = class(TDataModule)
    ADOConn: TADOConnection;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    ImgSmall: TImageList;
    imgLarge: TImageList;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure ADOConnWillExecute(Connection: TADOConnection;
      var CommandText: WideString; var CursorType: TCursorType;
      var LockType: TADOLockType; var CommandType: TCommandType;
      var ExecuteOptions: TExecuteOptions; var EventStatus: TEventStatus;
      const Command: _Command; const Recordset: _Recordset);
  private
    { Private declarations }
    FObjs, FTempList: TStringList;
    function ReadSysParamValue(AName, AKind, AField, ADefaultValue: string;
      withFlag: Boolean = false): string;
    procedure WriteSysParamValue(AName, AKind, AField, AValue: string);
    function GetCurrDate: TDatetime;
    function GetCurrTime: TDatetime;
  protected
    procedure InitEnvironment; virtual;
    procedure LoadRuntime;
    procedure LoadUsings;
  public
    { Public declarations }
    /// <summary>系统名称</summary>
    ApplicationTitle: string;
    /// <summary>当前版本</summary>
    CurrentVersion: string;
    /// <summary>官网地址</summary>
    WebUrl: string;
    /// <summary>登录人员权限基本信息</summary>
    LoginInfo: TLoginInfo;
    /// <summary>当前执行的任务或菜单的控制值</summary>
    TaskControlID: Integer;
    /// <summary>当前执行的任务或菜单的功能参数</summary>
    TaskParams,
    /// <summary>当前执行的任务或菜单的执行参数</summary>
    ExecParams: string;
    /// <summary>系统工作路径</summary>
    AppPath,
    /// <summary>配置文件名称</summary>
    ConfigFilename,
    /// <summary>当前语种名称</summary>
    LanguageName: string;
    /// <summary>使用者组织结构信息</summary>
    OrgInfo: TOrganizationInfo;
    /// <summary>录入使用者组织结构信息录入窗口</summary>
    function LoadOrg(OrgID: TGuid; OrgName: string): Boolean;
    /// <summary>得到一组系统选项列表，Style=0表示正常加载，Style=1表示只加载Name，Style=2表示只加载Value，Style=3表示将内容加载为Name，Style=4表示将内容加载为Name=object</summary>
    function GetListOption(OptName: string; Items: TStrings; Style: Integer = 0): string;
    /// <summary>得到一组下拉控件列表，NoneValue 默认内容是“所有”，不为空时，系统会添加到第一个</summary>
    function GetDropList(OptName: string; CMBox: TComboBox; NoneValue: string = '所有'): string;
    /// <summary>得到一组SQL得到的下拉控件列表，NoneValue 默认内容是“所有”，不为空时，系统会添加到第一个</summary>
    procedure GetSqlDropList(SqlText, KeyName, CaptionField: string; CMBox: TComboBox; NoneValue: string = '所有'); overload;
    /// <summary>得到一组SQL得到的下拉控件列表，NoneValue 默认内容是“所有”，不为空时，系统会添加到第一个</summary>
    procedure GetSqlDropList(SqlText: string; CMBox: TComboBox; NoneValue: string = '所有'); overload;
    /// <summary>存储一组系统选项列表</summary>
    procedure SetListOption(OptName, OptInfo: string; Items: TStrings);
    /// <summary>将一组以逗号间隔，以等号分隔的键值对，复制到Items中，如果键值中没有等号，系统自动补充其值为空</summary>
    procedure SetListToItems(ListText: string; Items: TStrings);
    /// <summary>得到一个打开文件对话框</summary>
    /// <param name="Filter">文件筛选选项</param>
    /// <param name="InitDir">初始化目录，支持全路径和相对路径，相对路径直接写工作目录的子目录名称即可，如果该目录不存在，系统会自动创建该目录</param>
    /// <returns>打开文件对话框</returns>
    function GetOpenDlg(Filter: string; InitDir: string = ''): TOpenDialog;
    /// <summary>得到一个存储文件对话框</summary>
    /// <param name="Filter">文件筛选选项</param>
    /// <param name="InitDir">初始化目录，支持全路径和相对路径，相对路径直接写工作目录的子目录名称即可，如果该目录不存在，系统会自动创建该目录</param>
    /// <returns>存储文件对话框</returns>
    function GetSaveDlg(Filter: string; InitDir: string = ''): TSaveDialog;
    /// <summary>提取一个系统字符串参数内容</summary>
    /// <param name="AName">参数大名称</param>
    /// <param name="AKind">参数类型</param>
    /// <param name="withFlag">是否启用前后缀标记</param>
    /// <returns>系统字符串参数内容</returns>
    function ReadParamValue(AName, AKind: string;
      withFlag: Boolean = false): string;
    /// <summary>提取一个系统整数参数内容</summary>
    /// <param name="AName">参数大名称</param>
    /// <param name="AKind">参数类型</param>
    /// <param name="withFlag">是否启用前后缀标记</param>
    /// <returns>系统整数参数内容</returns>
    function ReadParamInt(AName, AKind: string;
      withFlag: Boolean = false): Integer;
    /// <summary>提取一个系统浮点参数内容</summary>
    /// <param name="AName">参数大名称</param>
    /// <param name="AKind">参数类型</param>
    /// <param name="withFlag">是否启用前后缀标记</param>
    /// <returns>系统浮点参数内容</returns>
    function ReadParamFloat(AName, AKind: string;
      withFlag: Boolean = false): Extended;
    /// <summary>设置一个系统字符串参数内容</summary>
    /// <param name="AName">参数大名称</param>
    /// <param name="AKind">参数类型</param>
    /// <param name="value">内容</param>
    procedure WriteParamValue(AName, AKind: string; value: string);
    /// <summary>设置一个系统整数参数内容</summary>
    /// <param name="AName">参数大名称</param>
    /// <param name="AKind">参数类型</param>
    /// <param name="value">内容</param>
    procedure WriteParamInt(AName, AKind: string; value: Integer);
    /// <summary>设置一个系统浮点参数内容</summary>
    /// <param name="AName">参数大名称</param>
    /// <param name="AKind">参数类型</param>
    /// <param name="value">内容</param>
    procedure WriteParamFloat(AName, AKind: string; value: double);
    /// <summary>向系统缓存一个对象，如果对象为空，则表示摘除这个对象</summary>
    /// <param name="ObjName">对象名称，这个名称是唯一的，如果在发现则替换</param>
    /// <param name="Obj">对象</param>
    /// <param name="Param">缓存参数</param>
    procedure SetObject(ObjName: string; Obj: TObject; Param: string = '');
    /// <summary>从系统缓存一个对象中得到对象</summary>
    /// <param name="ObjName">对象名称</param>
    /// <returns>对象，没有则返回nil</returns>
    function GetObject(ObjName: string): TObject;
    /// <summary>从系统缓存一个对象中得到对象的参数</summary>
    /// <param name="ObjName">对象名称</param>
    /// <returns>缓存参数</returns>
    function GetObjectParam(ObjName: string): string;
    /// <summary>得到一个新的单据号</summary>
    /// <param name="TicketName">单据名称</param>
    /// <returns>单据号</returns>
    function GetTicketNo(TicketName: string): string;
    /// <summary>打开一个SQL语句，如果没有实例则创建一个，由调用方负责释放</summary>
    function OpenSQL(SQLText: string; Query: TADOQuery = nil): TADOQuery;
    /// <summary>打开一个SQL语句，如果没有实例则创建一个，由调用方负责释放</summary>
    function GenerateSQL(SQLText: string): TADOQuery;
    /// <summary>获取一个临时文件名称</summary>
    function GetTempFilename(FileExt: string): string;
    /// <summary>根据设置替换设置内容</summary>
    /// <param name="ADataset">查询结果</param>
    /// <param name="Setting">设置，要替换的字段名称用&lt;&gt;，名称里面支持简单计算</param>
    /// <returns>替换结果</returns>
    function ReplaceSettingField(ADataset: TDataset; Setting: string): string;
  published
    /// <summary>得到服务器当前日期</summary>
    property CurrDate: TDatetime read GetCurrDate;
    /// <summary>得到服务器当前日期时间</summary>
    property CurrTime: TDatetime read GetCurrTime;
  end;

  /// <summary>注册系统功能</summary>
  /// <param name="UsingName">功能名称</param>
  /// <param name="UsingInfo">功能说明</param>
  /// <param name="FormClass">窗口类</param>
  /// <param name="HelpID">帮助序号</param>
  /// <param name="UsingParam">可用参数</param>
procedure RegAppCommand(UsingName, UsingInfo: string; FormClass: TFormClass;
  HelpID: Integer; UsingParam: string);

var
  /// <summary>数据层</summary>
  FPlatformDB: TFPlatformDB;
  /// <summary>应用系统当前提供的窗口列表，</summary>
  FAppCommandList: TStringList;
  /// <summary>应用系统设定的功能列表</summary>
  FAppUsingList,
  /// <summary>应用系统设定的菜单列表</summary>
  FAppMenuList,
  /// <summary>应用系统设定的按钮列表</summary>
  FAppTaskList: TVonArraySetting;
  /// <summary>菜单文件名</summary>
  MENU_FILE: string;
  /// <summary>系统内部名称</summary>
  SYS_NAME: string;
  /// <summary>系统证书三个主要参数，CERT_KEY: 秘钥，CERT_CIPHER：加密算法, CERT_HASH：摘要算法</summary>
  CERT_KEY, CERT_CIPHER, CERT_HASH: string;
  /// <summary>系统证书文件名</summary>
  CERT_FILE: string;

implementation

uses UPlatformLogin, StrUtils, DateUtils, UDlgOrganization;

var
  FCHMHelpFilename: string;

procedure RegAppCommand(UsingName, UsingInfo: string; FormClass: TFormClass;
  HelpID: Integer; UsingParam: string);
var
  ATask: TAppCmdInfo;
begin
  ATask := TAppCmdInfo.Create;
  ATask.UsingInfo := UsingInfo;
  ATask.UsingClass := FormClass;
  ATask.HelpID := HelpID;
  ATask.UsingParams := UsingParam;
  FAppCommandList.AddObject(UsingName, ATask);
end;

{$R *.dfm}

procedure TFPlatformDB.DataModuleCreate(Sender: TObject);
var
  S: string;
  I: Integer;
begin
  InitEnvironment;
  FPlatformDB := Self;
  LoginInfo := TLoginInfo.Create;
  OrgInfo := TOrganizationInfo.Create;
  OrgInfo.Conn:= ADOConn;
  FObjs:= TStringList.Create;
  FTempList:= TStringList.Create;
  if LoadNewResourceModule(CHINESE) <> 0 then
    ReinitializeForms;
  CurrentVersion:= Get_ApplicationVersion(Application.ExeName);
  AppPath := ExtractFilePath(Application.ExeName);              //系统工作目录
  if FileExists(AppPath + 'Styles\LargeImage.BMP') then
    LoadImages(AppPath + 'Styles\LargeImage.BMP', imgLarge);
  if FileExists(AppPath + 'Styles\SmallImage.BMP') then
    LoadImages(AppPath + 'Styles\SmallImage.BMP', ImgSmall);
  ConfigFilename := ChangeFileExt(Application.ExeName, '.INI'); //配置文件文件名称
  with TIniFile.Create(ConfigFilename) do
    try
      LanguageName := ReadString('USER', 'Language', 'CN');     //当前使用的语种名称
      S := ReadString('SYSTEM', 'DBConnStr', '');
      for I := 0 to FAppConfigList.Count - 1 do
        FAppConfigList.Values[I, 6] := ReadString(FAppConfigList.Values[I, 0],
          FAppConfigList.Values[I, 1], FAppConfigList.Values[I, 5]);
    finally
      Free;
    end;
end;

procedure TFPlatformDB.DataModuleDestroy(Sender: TObject);
var
  I: Integer;
begin
  FObjs.Free;
  OrgInfo.Free;
  LoginInfo.Free;
  with TIniFile.Create(FPlatformDB.ConfigFilename) do
    try
      for I := 0 to FAppConfigList.Count - 1 do
        WriteString(FAppConfigList.Values[I, 0], FAppConfigList.Values[I, 1],
          FAppConfigList.Values[I, 6]);
    finally
      Free;
    end;
end;

procedure TFPlatformDB.InitEnvironment;
var
  p: DWORD;
begin
  with FormatSettings do
  begin
    CurrencyString := '￥';
    CurrencyFormat := 0;
    CurrencyDecimals := 2;
    DateSeparator := '-';
    TimeSeparator := ':';
    ListSeparator := ',';
    ShortDateFormat := 'yyyy-M-d';
    LongDateFormat := 'yyyy年M月d日';
    TimeAMString := '上午';
    TimePMString := '下午';
    ShortTimeFormat := 'h:mm';
    LongTimeFormat := 'h:mm:ss';
    ShortMonthNames[1] := '一月';
    ShortMonthNames[2] := '二月';
    ShortMonthNames[3] := '三月';
    ShortMonthNames[4] := '四月';
    ShortMonthNames[5] := '五月';
    ShortMonthNames[6] := '六月';
    ShortMonthNames[7] := '七月';
    ShortMonthNames[8] := '八月';
    ShortMonthNames[9] := '九月';
    ShortMonthNames[10] := '十月';
    ShortMonthNames[11] := '十一月';
    ShortMonthNames[12] := '十二月';
    LongMonthNames[1] := '一月';
    LongMonthNames[2] := '二月';
    LongMonthNames[3] := '三月';
    LongMonthNames[4] := '四月';
    LongMonthNames[5] := '五月';
    LongMonthNames[6] := '六月';
    LongMonthNames[7] := '七月';
    LongMonthNames[8] := '八月';
    LongMonthNames[9] := '九月';
    LongMonthNames[10] := '十月';
    LongMonthNames[11] := '十一月';
    LongMonthNames[12] := '十二月';
    ShortDayNames[1] := '星期日';
    ShortDayNames[2] := '星期一';
    ShortDayNames[3] := '星期二';
    ShortDayNames[4] := '星期三';
    ShortDayNames[5] := '星期四';
    ShortDayNames[6] := '星期五';
    ShortDayNames[7] := '星期六';
    LongDayNames[1] := '星期日';
    LongDayNames[2] := '星期一';
    LongDayNames[3] := '星期二';
    LongDayNames[4] := '星期三';
    LongDayNames[5] := '星期四';
    LongDayNames[6] := '星期五';
    LongDayNames[7] := '星期六';
    ThousandSeparator := ',';
    DecimalSeparator := '.';
    TwoDigitYearCenturyWindow := $32;
    NegCurrFormat := $02;
  end;
  SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_SSHORTDATE,pchar('yyyy/MM/dd')); //设置短日期格式
  SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_SLONGDATE,pchar('yyyy''年''M''月''d''日''')); //设置长日期格式为 yyyy'年'M'月'd'日'，“年月日”字符必须用单引号括起来。Delphi字符串里必须用两个单引号。
  SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_STIMEFORMAT,pchar('HH:mm:ss')); //设置时间格式，24小时制
  SendMessageTimeOut(HWND_BROADCAST,WM_SETTINGCHANGE,0,0,SMTO_ABORTIFHUNG,10,p);//设置完成后必须调用，通知其他程序格式已经更改，否则即使是程序自身也不能使用新设置的格式
end;

function TFPlatformDB.GetTempFilename(FileExt: string): string;
var
  I: Integer;
begin
  if not DirectoryExists(AppPath + 'Temp') then
    CreateDir(AppPath + 'Temp');
  Result:= AppPath + 'Temp\Temp.' + FileExt;
  I:= 0;
  while FileExists(Result) do
    if DeleteFile(PChar(Result)) then Exit
    else begin
      Inc(I);
      Result:= AppPath + 'Temp\Temp_' + IntToStr(I) + '.' + FileExt;
    end;
end;
{$REGION 'System functions'}

  function TFPlatformDB.GetCurrDate: TDatetime;
  begin
    Result:= DateOf(GetCurrTime);
  end;

  function TFPlatformDB.GetCurrTime: TDatetime;
  begin
    Result:= ADOConn.Execute('SELECT GETDATE()').Fields[0].Value;
  end;

  function TFPlatformDB.GetTicketNo(TicketName: string): string;
  var
    head, trail, fmt: string;
    val: Integer;
    kd: Extended;
    dt: TDate;
    isNewID: Boolean;
  begin
    with TADOQuery.Create(nil) do try
      Connection:= FPlatformDB.ADOConn;
      //SYS_Params]
      //[Name]          单据名称
      //[Kind]          TICKET
      //[Header]        前缀
      //[StrValue]      格式
      //[IntValue]      当前编号
      //[FloatValue]    关联信息   0:无关，连续序号，1.年序号，如果小数不是当前年份则重新计数；2.月序号；3.周序号；4.日序号；
      //[Trailler]      后缀
      //[Backup]        备份代码
      SQL.Text:= 'DECLARE @head nvarchar(256),@val int,@trail nvarchar(256),@fmt nvarchar(256),@knd float ' +
        'UPDATE SYS_Params SET @head=[Header],@val=ISNULL([IntValue],0),@trail=[Trailler],@fmt=[backup],@knd=[FloatValue],' +
        '[IntValue]=IsNull([IntValue],1)+1 WHERE [Name]=:TicketName AND [Kind]=''TICKET'' SELECT @head,@val,@trail,@fmt,@knd,GetDate()';
      Parameters[0].Value:= TicketName;                                                         //  0    1     2     3     4   5
      Open;
      if not EOF then begin
        head:= Fields[0].AsString;
        val:= Fields[1].AsInteger;
        trail:= Fields[2].AsString;
        fmt:= Fields[3].AsString;
        kd:= Fields[4].AsExtended;
        dt:= Fields[5].AsDateTime;
        isNewID:= False;
        case Trunc(kd) of
        1: begin isNewID:= YearOf(dt) <> ROUND(Frac(kd) * 10000); kd:= 1 + YearOf(dt) / 10000; end;     //Year   1-9999
        2: begin isNewID:= MonthOf(dt) <> ROUND(Frac(kd) * 100); kd:= 2 + MonthOf(dt) / 100; end;       //Month  1-12
        3: begin isNewID:= WeekOf(dt) <> ROUND(Frac(kd) * 10); kd:= 3 + WeekOf(dt) / 10; end;           //Week   1-7
        4: begin isNewID:= DayOf(dt) <> ROUND(Frac(kd) * 100); kd:= 4 + DayOf(dt) / 100; end;           //Day    1-31
        end;
        if isNewID then begin
          ADOConn.Execute('UPDATE SYS_Params SET [IntValue]=2,[FloatValue]='
          + FloatToStr(kd) + ' WHERE [Name]=''' + TicketName + ''' AND [Kind]=''TICKET''');
          val:= 1;
        end;
        fmt:= Fields[3].AsString;
        if fmt = '' then fmt:= '%.6d';
        Result:= head + Format(fmt, [val, YearOf(dt), YearOf(dt)mod 100, MonthOf(dt), DayOf(dt)]) + trail;
      end else begin
        FPlatformDB.ADOConn.Execute('INSERT INTO[SYS_Params]([Name],[Kind],[Header],[StrValue],[IntValue],[FloatValue],[Trailler],[Backup])' +
          'VALUES(''' + TicketName + ''', ''TICKET'','''','''',2,0,'''',''%.6d'')');
        Result:= '000001';
      end;
    finally
      Free;
    end;
  end;

  function TFPlatformDB.ReplaceSettingField(ADataset: TDataset;
    Setting: string): string;
  var
    P: PChar;
    IsField, HasOpt: Boolean;
    Str1, Str2: string;
    Opt: Char;
    function StrValue(strOrField: string): string;
    var
      aField: TField;
    begin
      Result:= strOrField;
      aField:= ADataset.Fields.FindField(Result);
      if Assigned(aField) then Result:= aField.AsString;
    end;
    function Calc: string;
    begin
      Str1:= StrValue(Str1);
      Str2:= StrValue(Str2);
      case Opt of
      '+': Result:= FloatToStr(StrToFloat(Str1) + StrToFloat(Str2));
      '-': Result:= FloatToStr(StrToFloat(Str1) - StrToFloat(Str2));
      '*': Result:= Format('%.3f', [StrToFloat(Str1) * StrToFloat(Str2)]);
      '/': Result:= Format('%.3f', [StrToFloat(Str1) / StrToFloat(Str2)]);
      end;
    end;
  begin
    P:= PChar(Setting); IsField:= False; HasOpt:= False; Result:= ''; Str1:= ''; Str2:= '';
    while P^ <> #0 do begin
      case P^ of
      '<': begin
          HasOpt:= False; Str1:= ''; Str2:= ''; IsField:= True;
        end;
      '>': begin
          if HasOpt then Result:= Result + Calc
          else Result:= Result + StrValue(Str1);
        end;
      '+', '-', '*', '/': begin
          if HasOpt then Str1:= Calc
          else HasOpt:= True;
          Opt:= P^;
        end;
      else
        if IsField then begin
          if HasOpt then Str2:= Str2 + P^
          else Str1:= Str1 + P^
        end else Result:= Result + P^;
      end;
      Inc(P);
    end;
  end;

  procedure TFPlatformDB.ADOConnWillExecute(Connection: TADOConnection;
    var CommandText: WideString; var CursorType: TCursorType;
    var LockType: TADOLockType; var CommandType: TCommandType;
    var ExecuteOptions: TExecuteOptions; var EventStatus: TEventStatus;
    const Command: _Command; const Recordset: _Recordset);
  begin
    if not Connection.Connected then
     Connection.Open();
  end;

  function TFPlatformDB.GenerateSQL(SQLText: string): TADOQuery;
  begin
    Result:= TADOQuery.Create(nil);
    Result.Connection:= ADOConn;
    Result.SQL.Text:= SQLText;
  end;

  function TFPlatformDB.OpenSQL(SQLText: string; Query: TADOQuery): TADOQuery;
  begin
    if not Assigned(Query) then
      Result:= TADOQuery.Create(nil)
    else begin Result:= Query; Result.Close; end;
    with Result do begin
      Close;
      Connection:= ADOConn;
      SQL.Text:= SQLText;
      Open;
    end;
  end;
{$ENDREGION}
{$REGION 'DropList and option'}
  function TFPlatformDB.GetDropList(OptName: string; CMBox: TComboBox; NoneValue: string): string;
  begin
    Result:= GetListOption(OptName, CMBox.Items);
    if NoneValue <> '' then
      CMBox.Items.Insert(0, NoneValue);
    CMBox.ItemIndex:= 0;
  end;

  function TFPlatformDB.GetListOption(OptName: string; Items: TStrings; Style: Integer = 0): string;
  var
    I: Integer;
  begin
    with TADOQuery.Create(nil) do
      try
        Connection := ADOConn;
        SQL.Text :=
          'SELECT OptInfo, OptValues FROM Sys_Option WHERE OptName=:OPT';
        Parameters[0].value := OptName;
        Open;
        if not EOF then
        begin
          Items.Assign(Fields[1]);
          case Style of
          1: for I := 0 to Items.Count - 1 do Items[I]:= Items.Names[I];
          2: for I := 0 to Items.Count - 1 do Items[I]:= Items.ValueFromIndex[I];
          3: for I := 0 to Items.Count - 1 do Items[I]:= Items[I] + '=';          //用于TValueListEditor的提示项目
          4: for I := 0 to Items.Count - 1 do begin
              Items.Objects[I]:= TObject(StrToInt(Items.ValueFromIndex[I]));
              Items[I]:= Items.Names[I];
            end;
          end;
          Result := Fields[0].AsString;
        end
        else
        begin
          Items.Clear;
          Result := '';
        end;
      finally
        Free;
      end;
  end;

  procedure TFPlatformDB.SetListOption(OptName, OptInfo: string; Items: TStrings);
  var
    AInfo: TOptionsInfo;
  begin
    AInfo:= TOptionsInfo.Create;
    try
      AInfo.Conn:= ADOConn;
      AInfo.OptName:= OptName;
      AInfo.OptKind:= 0;
      AInfo.OptInfo:= OptInfo;
      AInfo.SaveToDB;
      AInfo.SetContent(Items);
    finally
      AInfo.Free;
    end;
  end;

  procedure TFPlatformDB.SetListToItems(ListText: string; Items: TStrings);
  var
    lst: TStringList;
    I: Integer;
  begin
    lst:= TStringList.Create;
    lst.Delimiter:= ',';
    lst.DelimitedText:= ListText;
    Items.Clear;
    for I := 0 to lst.Count - 1 do
      if Pos('=', lst[I]) > 0 then
        Items.Add(lst[I])
      else Items.Add(lst[I] + '=');
  end;

  procedure TFPlatformDB.GetSqlDropList(SqlText, KeyName, CaptionField: string;
    CMBox: TComboBox; NoneValue: string);
  var
    PGID: PGUID;
  begin
    with TADOQuery.Create(nil)do try
      Connection:= ADOConn;
      SQL.Text:= SqlText;
      Open;
      if NoneValue = '' then CMBox.Clear else CMBox.Items.Text:= NoneValue;
      while not EOF do begin
        if KeyName = '' then
          CMBox.AddItem(FieldByName(CaptionField).AsString, nil)
        else if FieldByName(KeyName).DataType = ftGuid then begin
          new(PGID);
          PGID^:= FieldByName(KeyName).AsGuid;
          CMBox.AddItem(FieldByName(CaptionField).AsString, TObject(PGID))
        end else
          CMBox.AddItem(FieldByName(CaptionField).AsString, TObject(FieldByName(KeyName).AsInteger));
        Next;
      end;
      CMBox.ItemIndex:= 0;
    finally
      Free;
    end;
  end;

  procedure TFPlatformDB.GetSqlDropList(SqlText: string; CMBox: TComboBox;
    NoneValue: string);
  var
    PGID: PGUID;
  begin
    with TADOQuery.Create(nil)do try
      Connection:= ADOConn;
      SQL.Text:= SqlText;
      Open;
      if NoneValue = '' then CMBox.Clear else CMBox.Items.Text:= NoneValue;
      while not EOF do begin
        if Fields[0].DataType = ftGuid then begin
          new(PGID);
          PGID^:= Fields[0].AsGuid;
          CMBox.AddItem(Fields[1].AsString, TObject(PGID))
        end else CMBox.AddItem(Fields[1].AsString, TObject(Fields[0].AsInteger));
        Next;
      end;
      CMBox.ItemIndex:= 0;
    finally
      Free;
    end;
  end;

{$ENDREGION}
{$REGION 'Dialog'}

  function TFPlatformDB.GetOpenDlg(Filter: string; InitDir: string = '')
    : TOpenDialog;
  begin
    Result := OpenDialog1;
    Result.Filter := Filter;
    if InitDir = '' then
      Result.InitialDir := ExtractFilePath(Application.ExeName)
    else if Pos(':', InitDir) <= 0 then
      Result.InitialDir := ExtractFilePath(Application.ExeName) + InitDir
    else
      Result.InitialDir := InitDir;
    if not DirectoryExists(Result.InitialDir) then
      CreateDir(Result.InitialDir);
  end;

  function TFPlatformDB.GetSaveDlg(Filter: string; InitDir: string = '')
    : TSaveDialog;
  begin
    Result := SaveDialog1;
    Result.Filter := Filter;
    if InitDir = '' then
      Result.InitialDir := ExtractFilePath(Application.ExeName)
    else if Pos(':', InitDir) <= 0 then
      Result.InitialDir := ExtractFilePath(Application.ExeName) + InitDir
    else
      Result.InitialDir := InitDir;
    if not DirectoryExists(Result.InitialDir) then
      CreateDir(Result.InitialDir);
  end;

{$ENDREGION}
{$REGION 'Platform method'}

function TFPlatformDB.LoadOrg(OrgID: TGuid; OrgName: string): Boolean;
  var
    query: TADOQuery;
  begin
    OrgInfo.ID:= 0;
    OrgInfo.OrgID:= OrgID;
    OrgInfo.OrgName:= OrgName;
    query:= TADOQuery.Create(nil);
    with query do
    begin
      Result:= False;
      Connection:= ADOConn;
      SQL.Text:= 'SELECT * FROM SYS_Organization WHERE OrgID=:OID';
      Parameters[0].Value:= GUIDToString(OrgInfo.OrgID);
      Open;
      if not EOF then
        Result:= FPlatformDB.OrgInfo.LoadFromDB(query)
      else with TFDlgOrganization.Create(nil) do try
        InfoToForm(OrgInfo);
        while ShowModal = mrOK do begin
          if FPlatformDB.OrgInfo.OrgName <> EOrgName.Text then
            if DlgInfo('警告', '名称与系统登记不一致是否继续？',
              mbAbortIgnore, 0) <> mrIgnore then Continue;
          FormToInfo(FPlatformDB.OrgInfo);
          Result:= FPlatformDB.OrgInfo.SaveToDB(query);
          Exit;
        end;
      finally
        Free;
      end;
    end;
  end;

  procedure TFPlatformDB.LoadRuntime;
  var
    Idx: Integer;
  begin
    with TADOQuery.Create(nil)do try
      Connection:= FPlatformDB.ADOConn;
      SQL.Text:= 'SELECT [Name],[StrValue]FROM SYS_Params WHERE Kind=''RUNTIME''';
      Open;
      while not EOF do begin
        Idx:= system_runtime_value.IndexOfName(Fields[0].AsString);
        if Idx >= 0 then
          system_runtime_value.ValueFromIndex[Idx]:= Fields[1].AsString
        else
          system_runtime_value.Values[Fields[0].AsString]:= Fields[1].AsString;
        Next;
      end;
    finally
      Free;
    end;
  end;

  procedure TFPlatformDB.LoadUsings;
  var
    fs: TFileStream;
    vInt: RVonInt;
  begin
    if not FileExists(AppPath + MENU_FILE) then
      Exit;
    fs := TFileStream.Create(AppPath + MENU_FILE, fmOpenRead);
    fs.Read(vInt.Bytes[0], 4);
    if vInt.Int > 0 then
      FAppUsingList.LoadFromStream(fs, vInt.Int);
    fs.Read(vInt.Bytes[0], 4);
    if vInt.Int > 0 then
      FAppMenuList.LoadFromStream(fs, vInt.Int);
    fs.Read(vInt.Bytes[0], 4);
    if vInt.Int > 0 then
      FAppTaskList.LoadFromStream(fs, vInt.Int);
    fs.Free;
  end;
{$ENDREGION}
{$REGION 'Object buffers'}
  function TFPlatformDB.GetObject(ObjName: string): TObject;
  var
    Idx: Integer;
  begin
    Idx:= FObjs.IndexOfName(ObjName);
    if Idx < 0 then result:= nil
    else Result:= FObjs.Objects[Idx];
  end;

  function TFPlatformDB.GetObjectParam(ObjName: string): string;
  var
    Idx: Integer;
  begin
    Idx:= FObjs.IndexOfName(ObjName);
    if Idx < 0 then result:= ''
    else Result:= FObjs.ValueFromIndex[Idx];
  end;

  procedure TFPlatformDB.SetObject(ObjName: string; Obj: TObject; Param: string);
  var
    Idx: Integer;
  begin
    Idx:= FObjs.IndexOfName(ObjName);
    if Idx < 0 then FObjs.AddObject(ObjName + '=' + Param, Obj)
    else begin
      if Assigned(Obj) then begin
        FObjs.ValueFromIndex[Idx]:= Param;
        FObjs.Objects[Idx]:= Obj;
      end else FObjs.Delete(Idx);
    end;
  end;
{$ENDREGION}
{$REGION 'Params methods'}

function TFPlatformDB.ReadParamFloat(AName, AKind: string;
  withFlag: Boolean): Extended;
begin
  Result := StrToFloat(ReadSysParamValue(AName, AKind, 'FloatValue', '0', withFlag));
end;

function TFPlatformDB.ReadParamInt(AName, AKind: string;
  withFlag: Boolean): Integer;
begin
  Result := StrToInt(ReadSysParamValue(AName, AKind, 'IntValue', '0', withFlag));
end;

function TFPlatformDB.ReadParamValue(AName, AKind: string;
  withFlag: Boolean): string;
begin
  Result := ReadSysParamValue(AName, AKind, 'StrValue', '', withFlag);
end;

function TFPlatformDB.ReadSysParamValue(AName, AKind, AField,
  ADefaultValue: string; withFlag: Boolean): string;
var
  lst: TStringList;
begin
  Result := '';
  with TADOQuery.Create(nil) do
    try
      Connection := ADOConn;
      lst := TStringList.Create;
      ADOConn.GetTableNames(lst);
      if lst.IndexOf('SYS_Params') < 0 then
        ADOConn.Execute('create table SYS_Params (Name nvarchar(50) not null,' +
          'Kind nvarchar(50) not null, Header nvarchar(50) null, ' +
          'StrValue nvarchar(50) null, IntValue int null, FloatValue float null, '
          + 'Trailler nvarchar(50) null, "Backup" nvarchar(50) null, ' +
          'constraint PK_SYS_PARAMS primary key (Name, Kind))');
      lst.Free;
      SQL.Text := 'SELECT [Name], [Kind], [Header], [' + AField +
        '], [Trailler] FROM [SYS_Params] WHERE [Name]=:NM AND [Kind]=:KD';
      Parameters[0].value := AName;
      Parameters[1].value := AKind;
      Open;
      if not EOF then
      begin
        if withFlag then
          Result := Fields[2].AsString + Fields[3].AsString + Fields[4].AsString
        else
          Result := Fields[3].AsString;
      end
      else
        Result := ADefaultValue;
    finally
      Free;
    end;
end;

procedure TFPlatformDB.WriteParamFloat(AName, AKind: string; value: double);
begin
  WriteSysParamValue(AName, AKind, 'FloatValue', FloatToStr(value));
end;

procedure TFPlatformDB.WriteParamInt(AName, AKind: string; value: Integer);
begin
  WriteSysParamValue(AName, AKind, 'IntValue', IntToStr(value));
end;

procedure TFPlatformDB.WriteParamValue(AName, AKind, value: string);
begin
  WriteSysParamValue(AName, AKind, 'StrValue', value);
end;

procedure TFPlatformDB.WriteSysParamValue(AName, AKind, AField, AValue: string);
var
  lst: TStringList;
begin
  with TADOQuery.Create(nil) do
    try
      Connection := ADOConn;
      lst := TStringList.Create;
      ADOConn.GetTableNames(lst);
      if lst.IndexOf('SYS_Params') < 0 then
        ADOConn.Execute('create table SYS_Params (Name nvarchar(50) not null,' +
          'Kind nvarchar(50) not null, Header nvarchar(50) null, ' +
          'StrValue nvarchar(50) null, IntValue int null, FloatValue float null, '
          + 'Trailler nvarchar(50) null, "Backup" nvarchar(50) null, ' +
          'constraint PK_SYS_PARAMS primary key (Name, Kind))');
      lst.Free;
      SQL.Text := 'SELECT [Name], [Kind], [Header], [' + AField +
        '], [Trailler] FROM [SYS_Params] WHERE [Name]=:NM AND [Kind]=:KD';
      Parameters[0].value := AName;
      Parameters[1].value := AKind;
      Open;
      if EOF then
        Append
      else
        Edit;
      Fields[0].AsString := AName;
      Fields[1].AsString := AKind;
      Fields[3].AsString := AValue;
      Post;
    finally
      Free;
    end;
end;

{$ENDREGION}
initialization
  //WriteLog(LOG_DEBUG, 'TFPlatformDB', 'Create any memery');

FAppCommandList := TStringList.Create; // 应用系统当前提供的窗口列表
FAppUsingList := TVonArraySetting.Create; // 应用系统设定的功能列表
FAppMenuList := TVonArraySetting.Create; // 应用系统设定的菜单列表
FAppTaskList := TVonArraySetting.Create; // 应用系统设定的按钮列表
MENU_FILE:= 'MENU.DAT';

finalization

FAppTaskList.Free; // 应用系统设定的按钮列表
FAppMenuList.Free; // 应用系统设定的按钮列表
FAppCommandList.Free; // 应用系统当前提供的窗口列表
FAppUsingList.Free; // 应用系统设定的功能列表

end.
