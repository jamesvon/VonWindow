(* ******************************************************************************
  * UPlatformDockForm v1.0 written by James Von (jamesvon@163.com) ***************
  ********************************************************************************
  * 主窗口Dock区域窗口设计基类
  *------------------------------------------------------------------------------*
  *     建立模块（需要从UPlatformDockForm单元的TFPlatformDockForm来继承），用
  * RegDockForm函数进行注册，使用以下两个函数来创建TFPlatformDockForm类：
  * TFPlatformDockForm.CreateDockForm(DockFormName, FormCaption: string)
  * TFPlatformDockForm.CreateDockForm(DockFormID: Integer; FormCaption: string)
  *     具体实现方法可以参照：
  *     UPlatformMenuBase.TFPlatformMenuBase
  *     UPlatformMenu1.TFPlatformMenu1
  *============================================================================= *)
unit UPlatformDockForm;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, UVonLog;

type
  TFPlatformDockForm = class(TForm)
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
  private
    { Private declarations }
  protected

  public
    { Public declarations }
    class function CreateDockForm(DockFormName, FormCaption: string)
      : TFPlatformDockForm; overload; static;
    class function CreateDockForm(DockFormID: Integer; FormCaption: string)
      : TFPlatformDockForm; overload; static;
  end;

function RegDockForm(AName, ANote: string; AClass: TFormClass): Integer;

var
  FPlatformDockForm: TFPlatformDockForm;
  DockFormList: TStringList;

implementation

type
  TClassInfo = class
    FormClass: TFormClass;
  end;

function RegDockForm(AName, ANote: string; AClass: TFormClass): Integer;
var
  Info: TClassInfo;
begin
  Info := TClassInfo.Create;
  Info.FormClass := AClass;
  DockFormList.AddObject(AName + '=' + ANote, Info);
end;

{$R *.dfm}

class function TFPlatformDockForm.CreateDockForm(DockFormName,
  FormCaption: string): TFPlatformDockForm;
var
  Idx: Integer;
begin
  Result := nil;
  Idx := DockFormList.IndexOfName(DockFormName);
  if Idx < 0 then Result := CreateDockForm(0, FormCaption)
  else Result := CreateDockForm(Idx, FormCaption);
end;

class function TFPlatformDockForm.CreateDockForm(DockFormID: Integer;
  FormCaption: string): TFPlatformDockForm;
begin
  Result := nil;
  if (DockFormID < 0) or (DockFormID >= DockFormList.Count) then
    Exit;
  Result := TClassInfo(DockFormList.Objects[DockFormID]).FormClass.Create(nil)
    as TFPlatformDockForm;
  // Application.CreateForm(TClassInfo(DockFormList.Objects[DockFormID]).FormClass, Result);
  Result.Caption := FormCaption;
  Result.Show;
end;

procedure TFPlatformDockForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  ManualFloat(Rect(0, 0, 0, 0));
  // Action := caFree;
  Action := caHide;
end;

procedure TFPlatformDockForm.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;

var
  indexForm: Integer;

initialization
  //WriteLog(LOG_DEBUG, 'TFPlatformDockForm', 'Create any memery');
  DockFormList := TStringList.Create;

finalization
  for indexForm := 0 to DockFormList.Count - 1 do
    DockFormList.Objects[indexForm].Free;
  DockFormList.Free;

end.
