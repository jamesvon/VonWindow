unit UPlatformInfo;

interface

uses Forms, SysUtils, Classes, DB, data.win.ADODB, UVonSystemFuns, UVonLog, IniFiles, Graphics,
  UVonClass, UVonCalculator, UVonCrypt;

resourcestring
  RES_CHECK_ERR = '数据含有%s错误，不能进行存储。';
  RES_NODAT_ERR = '无法对空列表进行提取数据的操作。';

const SYS_Prefix = 'SYS_';

type
  TClassStructClass<T> = class
  private
    FObjClass: T;
  published
    property ObjClass: T read FObjClass write FObjClass;
  end;
  /// <summary>应用功能注册信息类</summary>
  TAppCmdInfo = class
    /// <summary>功能说明</summary>
    UsingInfo: string;
    /// <summary>窗口类</summary>
    UsingClass: TFormClass;
    /// <summary>帮助序号</summary>
    HelpID: Integer;
    /// <summary>可用参数</summary>
    UsingParams: string;
  end;

  IIdentityInterface = class
    /// <summary>检测当前信息的逻辑和字段的有效性</summary>
    function CheckInfo: boolean; virtual; abstract;
    /// <summary>从一个查询组件中提取当前信息</summary>
    function LoadFromDB(DataSet: TCustomADODataSet): Boolean; virtual; abstract;
    /// <summary>将当前信息存储到数据库组件中</summary>
    function SaveToDB(DataSet: TCustomADODataSet): Boolean; virtual; abstract;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: Integer): Boolean; virtual; abstract;
    /// <summary>SQL直接存储</summary>
    function SaveData(AConn: TADOConnection): Boolean; virtual; abstract;
  end;

  IGuidInterface = class
    /// <summary>检测当前信息的逻辑和字段的有效性</summary>
    function CheckInfo: boolean; virtual; abstract;
    /// <summary>从一个查询组件中提取当前信息</summary>
    function LoadFromDB(DataSet: TCustomADODataSet): Boolean; virtual; abstract;
    /// <summary>将当前信息存储到数据库组件中</summary>
    function AppendToDB(DataSet: TCustomADODataSet): Boolean; virtual; abstract;
    /// <summary>将当前信息存储到数据库组件中</summary>
    function UpdateToDB(DataSet: TCustomADODataSet): Boolean; virtual; abstract;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: Integer): Boolean; virtual; abstract;
    /// <summary>SQL直接存储</summary>
    function AppendData(AConn: TADOConnection): Boolean; virtual; abstract;
    /// <summary>SQL直接存储</summary>
    function UpdateData(AConn: TADOConnection): Boolean; virtual; abstract;
  end;

  IInfoInterface<T> = class
    /// <summary>检测当前信息的逻辑和字段的有效性</summary>
    function CheckInfo: boolean; virtual; abstract;
    /// <summary>从一个查询组件中提取当前信息</summary>
    function LoadFromDB(DataSet: TCustomADODataSet): Boolean; virtual; abstract;
    /// <summary>将当前信息存储到数据库组件中</summary>
    function AppendToDB(DataSet: TCustomADODataSet): Boolean; virtual; abstract;
    /// <summary>将当前信息存储到数据库组件中</summary>
    function UpdateToDB(DataSet: TCustomADODataSet): Boolean; virtual; abstract;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: T): Boolean; virtual; abstract;
    /// <summary>SQL直接存储</summary>
    function AppendData(AConn: TADOConnection): Boolean; virtual; abstract;
    /// <summary>SQL直接存储</summary>
    function UpdateData(AConn: TADOConnection): Boolean; virtual; abstract;
  end;

  /// <summary>信息基类</summary>
  TInfoBase = class
  protected
    FConn: TADOConnection;
  public
    /// <summary>检测当前信息的逻辑和字段的有效性</summary>
    function CheckInfo: boolean; virtual;
    /// <summary>从一个查询组件中提取当前信息</summary>
    function LoadFromDB(DataSet: TCustomADODataSet): Boolean; virtual;
    /// <summary>将当前信息存储到数据库组件中</summary>
    function SaveToDB(DataSet: TCustomADODataSet): Boolean; virtual;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: Integer): Boolean; virtual;
    /// <summary>SQL直接存储</summary>
    function SaveData(AConn: TADOConnection): Boolean; virtual;
  published
    property Conn: TADOConnection read FConn write FConn;
  end;
  /// <summary>人员分组信息</summary>
  TUserGroupInfo = class
  private
    { Private Members }
    FID: integer;
    FGroupName: string;
    FConn: TADOConnection;
    function CheckInfo: boolean;
    procedure SetGroupName(const Value: string);
    procedure SetID(const Value: integer);
  public
    function LoadFromDB(DataSet: TCustomADODataSet): boolean;
    function SaveToDB(DataSet: TCustomADODataSet): boolean;
  published
    property ID: integer read FID write SetID;
    property GroupName: string read FGroupName write SetGroupName;
  end;

  TEventToChangePassword = procedure (username: string) of object;
  /// <summary>人员级别</summary>
  /// <param name="UG_HOST">平台管理员</param>
  /// <param name="UG_ADMIN">系统管理员</param>
  /// <param name="UG_MANAGER">系统主管</param>
  /// <param name="UG_NORMAL">普通成员</param>
  EUSER_GRADE = (UG_HOST, UG_ADMIN, UG_MANAGER, UG_NORMAL);
  /// <summary>人员信息</summary>
  TUserInfo = class
  private
    { Private Members }
    FID: integer;
    FGroupIdx: integer;
    FLoginName: string;
    FDisplayName: string;
    FLoginPWD: string;
    FConn: TADOConnection;
    FExtends: TStringList;
    FSubRight: array[0..9]of byte;
    FDepartmentIdx: Integer;
    FJob: string;
    FWork: string;
    FGroupName: string;
    function CheckInfo: boolean;
    procedure SetDisplayName(const Value: string);
    procedure SetGroupIdx(const Value: integer);
    procedure SetID(const Value: integer);
    procedure SetLoginName(const Value: string);
    procedure SetDepartmentIdx(const Value: Integer);
    procedure SetGroupName(const Value: string);
    procedure SetJob(const Value: string);
    procedure SetWork(const Value: string);
    function GetSubRight(Index: Integer): Byte;
    procedure SetSubRight(Index: Integer; const Value: Byte);
  public
    constructor Create;
    destructor Destroy; override;
    function LoadFromDB(DataSet: TCustomADODataSet): boolean;
    function SaveToDB(DataSet: TCustomADODataSet): boolean;
    procedure ChangePWD(PWD: string); virtual;
    procedure EmptyPassword;      
    property SubRight[Index: Integer]: Byte read GetSubRight write SetSubRight;
  published
    property ID: integer read FID write SetID;
    property GroupIdx: integer read FGroupIdx write SetGroupIdx;
    property LoginName: string read FLoginName write SetLoginName;
    property DisplayName: string read FDisplayName write SetDisplayName;
    property Extends: TStringList read FExtends;
    property DepartmentIdx: Integer read FDepartmentIdx write SetDepartmentIdx;
    property GroupName: string read FGroupName write SetGroupName;
    property Work: string read FWork write SetWork;
    property Job: string read FJob write SetJob;
  end;
  /// <summary>角色信息</summary>
  TRoleInfo = class
  private
    { Private Members }
    FID: integer;
    FRoleName: string;
    FPID: integer;
    FRoleInfo: string;
    FLinkType: integer;
    FConn: TADOConnection;
    FListOrder: integer;
    FSpecialFlag: integer;
    function CheckInfo: boolean;
    procedure SetID(const Value: integer);
    procedure SetLinkType(const Value: integer);
    procedure SetPID(const Value: integer);
    procedure SetRoleInfo(const Value: string);
    procedure SetRoleName(const Value: string);
    procedure SetListOrder(const Value: integer);
    procedure SetSpecialFlag(const Value: integer);
    function GetUserGrade: EUSER_GRADE;
    procedure SetUserGrade(const Value: EUSER_GRADE);
  public
    function LoadFromDB(DataSet: TCustomADODataSet): boolean;
    function SaveToDB(DataSet: TCustomADODataSet): boolean;
  published
    property ID: integer read FID write SetID;
    property RoleName: string read FRoleName write SetRoleName;
    property PID: integer read FPID write SetPID;
    property RoleInfo: string read FRoleInfo write SetRoleInfo;
    /// <summary>层级关系：0=没关系 1=继承上级 2=包含下级</summary>
    property LinkType: integer read FLinkType write SetLinkType;
    property ListOrder: integer read FListOrder write SetListOrder;
    /// <summary>特殊标记: 0:普通级 100:管理员 200:应用管理员 500:系统管理员</summary>
    property SpecialFlag: integer read FSpecialFlag write SetSpecialFlag;
    property UserGrade: EUSER_GRADE read GetUserGrade write SetUserGrade;
  end;
  /// <summary>权限信息</summary>
  TRightInfo = class
  private
    { Private Members }
    FID: integer;
    FRoleIdx: integer;
    FUsingName: string;
    FConn: TADOConnection;
    FControlID: integer;
    function CheckInfo: boolean;
    procedure SetID(const Value: integer);
    procedure SetRoleIdx(const Value: integer);
    procedure SetUsingName(const Value: string);
    procedure SetControlID(const Value: integer);
  public
    function LoadFromDB(DataSet: TCustomADODataSet): boolean;
    function SaveToDB(DataSet: TCustomADODataSet): boolean;
  published
    property ID: integer read FID write SetID;
    property RoleIdx: integer read FRoleIdx write SetRoleIdx;
    property UsingName: string read FUsingName write SetUsingName;
    property ControlID: integer read FControlID write SetControlID;
  end;
  /// <summary>人员拥有的角色信息</summary>
  TUserRoleInfo = class
  private
    { Private Members }
    FID: integer;
    FUserIdx: integer;
    FRoleIdx: integer;
    FConn: TADOConnection;
    function CheckInfo: boolean;
    procedure SetID(const Value: integer);
    procedure SetRoleIdx(const Value: integer);
    procedure SetUserIdx(const Value: integer);
  public
    function LoadFromDB(DataSet: TCustomADODataSet): boolean;
    function SaveToDB(DataSet: TCustomADODataSet): boolean;
  published
    property ID: integer read FID write SetID;
    property UserIdx: integer read FUserIdx write SetUserIdx;
    property RoleIdx: integer read FRoleIdx write SetRoleIdx;
  end;
  /// <summary>登录信息（自持登录窗口的调用）</summary>
  TLoginInfo = class(TUserInfo)
  private
    { Private Members }
    FConn: TADOConnection;
    FRoleList: TVonNameValueData;
    FRightList: TStringHash;
    FIsManager: boolean;
    FIsSystem: boolean;
    FIsAdmin: boolean;
    procedure LoadRoles(RoleIdx: integer);
    procedure LoadSubRoles(RoleIdx: integer);
  public
    RightStr: string;
    constructor Create;
    destructor Destroy; override;
    function Login(LoginName, LoginPwd: string; Conn: TADOConnection): boolean;
    function CheckRight(AUsingName: string; AControlID: integer): boolean;
    procedure ChangePWD(newPwd: string); override;
    procedure AddRole(Role: TRoleInfo);
  published
    //property UserInfo: TUserInfo read FUserInfo;
    property RoleList: TVonNameValueData read FRoleList;
    property IsSystem: boolean read FIsSystem;
    property IsAdmin: boolean read FIsAdmin;
    property IsManager: boolean read FIsManager;
  end;
  /// <summary>系统选项信息</summary>
  TOptionsInfo = class
  private
    { Private Members }
    FID: integer;
    FOptName: string;
    FOptKind: integer;
    FOptInfo: string;
    FQuery: TADOQuery;
    FConn: TADOConnection;
    function CheckInfo: boolean;
    procedure SetID(const Value: integer);
    procedure SetOptInfo(const Value: string);
    procedure SetOptKind(const Value: integer);
    procedure SetOptName(const Value: string);
    procedure SetQuery(const Value: TADOQuery);
    procedure SetConn(const Value: TADOConnection);
  public
    function LoadFromDB(OptionName: string): boolean;
    function SaveToDB: integer;
    procedure SetContent(Value: TPersistent); overload;
    procedure GetContent(Value: TPersistent); overload;
    procedure SetContent(Value: TStream); overload;
    procedure GetContent(Value: TStream); overload;
  published
    property ID: integer read FID write SetID;
    property OptName: string read FOptName write SetOptName;
    property OptKind: integer read FOptKind write SetOptKind;
    property OptInfo: string read FOptInfo write SetOptInfo;
    property Query: TADOQuery read FQuery write SetQuery;
    property Conn: TADOConnection read FConn write SetConn;
  end;
  /// <summary>组织结构信息</summary>
  TOrganizationInfo = class
  private
	{ Private Members }
    FID: integer;
    FOrgID: TGuid;
    FOrgName: string;
    FPID: integer;
    FState: string;
    FCity: string;
    FCounty: string;
    FKind: integer;
    FAddress: string;
    FManager: string;
    FTelphong: string;
    FListOrder: integer;
    FLon: integer;
    FLat: integer;
    FLonM: integer;
    FLatM: integer;
    FLonS: Currency;
    FLatS: Currency;
    FInfo: TVonConfig;
    FMap: TVonConfig;
    FCRCCode: string;
    FConn: TADOConnection;
    FOrgCode: string;
    function CheckInfo: boolean;
    function GetInfo: TVonConfig;
    function GetMap: TVonConfig;
    procedure SetAddress(const Value: string);
    procedure SetCity(const Value: string);
    procedure SetOrgID(const Value: TGuid);
    procedure SetOrgName(const Value: string);
    procedure SetCounty(const Value: string);
    procedure SetCRCCode(const Value: string);
    procedure SetKind(const Value: integer);
    procedure SetLat(const Value: integer);
    procedure SetLatM(const Value: integer);
    procedure SetLatS(const Value: Currency);
    procedure SetListOrder(const Value: integer);
    procedure SetLon(const Value: integer);
    procedure SetLonM(const Value: integer);
    procedure SetLonS(const Value: Currency);
    procedure SetManager(const Value: string);
    procedure SetPID(const Value: integer);
    procedure SetState(const Value: string);
    procedure SetTelphong(const Value: string);
    procedure SetID(const Value: Integer);
    procedure SetOrgCode(const Value: string);
  public
    constructor Create;
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: Integer): Boolean;
    /// <summary>存储信息</summary>
    function SaveToDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接存储</summary>
    function SaveData(AConn: TADOConnection): Boolean;
    procedure Clear;
    destructor Destroy; override;
  published
    property Conn: TADOConnection read FConn write FConn;
    property ID: Integer read FID write SetID;
    property OrgCode: string read FOrgCode write SetOrgCode;
    property OrgID: TGuid read FOrgID write SetOrgID;
    property OrgName: string read FOrgName write SetOrgName;
    property PID: integer read FPID write SetPID;
    property State: string read FState write SetState;
    property City: string read FCity write SetCity;
    property County: string read FCounty write SetCounty;
    property Kind: integer read FKind write SetKind;
    property Address: string read FAddress write SetAddress;
    property Manager: string read FManager write SetManager;
    property Telphong: string read FTelphong write SetTelphong;
    property ListOrder: integer read FListOrder write SetListOrder;
    property Lon: integer read FLon write SetLon;
    property Lat: integer read FLat write SetLat;
    property LonM: integer read FLonM write SetLonM;
    property LatM: integer read FLatM write SetLatM;
    property LonS: Currency read FLonS write SetLonS;
    property LatS: Currency read FLatS write SetLatS;
    property Info: TVonConfig read GetInfo;
    property Map: TVonConfig read GetMap;
    property CRCCode: string read FCRCCode write SetCRCCode;
  end;
  /// <summary>通用查询模板</summary>
  TQueryTempInfo = class
  private
	{ Private Members }
    FConn: TADOConnection;
    FParamList: TVonTable;
    FStatisticList: TVonTable;
    FStyle: TVonConfig;
    FSQLHeader: string;
    FSQLTrailler: string;
    FTempName: string;
    FTitle: string;
    FID: integer;
    FSQLCondition: string;
    FExportTemp: string;
    function CheckInfo: boolean;
    procedure SetID(const Value: integer);
    procedure SetSQLCondition(const Value: string);
    procedure SetSQLHeader(const Value: string);
    procedure SetSQLTrailler(const Value: string);
    procedure SetTempName(const Value: string);
    procedure SetTitle(const Value: string);
    function GetParamList: TVonTable;
    function GetStyle: TVonConfig;
    procedure SetExportTemp(const Value: string);
    function GetStatisticList: TVonTable;
  public
    destructor Destroy;
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>存储信息</summary>
    function SaveToDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接存储</summary>
    function SaveData(AConn: TADOConnection): Boolean;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: Integer): Boolean;
    function LoadTemp(AConn: TADOConnection; ATempName: string): Boolean;
  published
    /// <summary>序号</summary>
    property ID: integer read FID write SetID;
    /// <summary>模板名称</summary>
    property TempName: string read FTempName write SetTempName;
    /// <summary>标题</summary>
    property Title: string read FTitle write SetTitle;
    /// <summary>导出模板</summary>
    property ExportTemp: string read FExportTemp write SetExportTemp;
    /// <summary>头部SQL</summary>
    property SQLHeader: string read FSQLHeader write SetSQLHeader;
    /// <summary>条件SQL</summary>
    property SQLCondition: string read FSQLCondition write SetSQLCondition;
    /// <summary>尾部SQL</summary>
    property SQLTrailler: string read FSQLTrailler write SetSQLTrailler;
    /// <summary>条件参数</summary>
    property ParamList: TVonTable read GetParamList;
    /// <summary>展现方式</summary>
    property Style: TVonConfig read GetStyle;
    /// <summary>统计参数</summary>
    property StatisticList: TVonTable read GetStatisticList;
  end;
  /// <summary>数据库表字典</summary>
  TDataDictionaryInfo = class
  private
	{ Private Members }
    FConn: TADOConnection;
    FFldCode: string;
    FLinkFld: string;
    FTblName: string;
    FID: integer;
    FLinkTbl: string;
    FFldType: string;
    FFldName: string;
    function CheckInfo: boolean;
    procedure SetFldCode(const Value: string);
    procedure SetFldName(const Value: string);
    procedure SetFldType(const Value: string);
    procedure SetID(const Value: integer);
    procedure SetLinkFld(const Value: string);
    procedure SetLinkTbl(const Value: string);
    procedure SetTblName(const Value: string);
  public
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>存储信息</summary>
    function SaveToDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: Integer): Boolean;
    /// <summary>SQL直接存储</summary>
    function SaveData(AConn: TADOConnection): Boolean;
  published
    /// <summary>序号</summary>
    property ID: integer read FID write SetID;
    /// <summary>表名称</summary>
    property TblName: string read FTblName write SetTblName;
    /// <summary>中文名称</summary>
    property FldName: string read FFldName write SetFldName;
    /// <summary>字段名称</summary>
    property FldCode: string read FFldCode write SetFldCode;
    /// <summary>字段类型</summary>
    property FldType: string read FFldType write SetFldType;
    /// <summary>连接表</summary>
    property LinkTbl: string read FLinkTbl write SetLinkTbl;
    /// <summary>连接字段</summary>
    property LinkFld: string read FLinkFld write SetLinkFld;
  end;
  /// <summary>模板管理</summary>
  TTemplateInfo = class
  private
    FConn: TADOConnection;
    FSvrFlag: string;
    FVer: integer;
    FTempName: string;
    FID: integer;
    FKind: string;
    FOutType: string;
    FFileType: string;
    procedure SetFileType(const Value: string);
    procedure SetID(const Value: integer);
    procedure SetKind(const Value: string);
    procedure SetOutType(const Value: string);
    procedure SetTempName(const Value: string);
    procedure SetVer(const Value: integer);
  public
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>存储信息</summary>
    function SaveToDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: Integer): Boolean;
    /// <summary>SQL直接存储</summary>
    function SaveData(AConn: TADOConnection): Boolean;
    /// <summary>检查数据的有效性</summary>
    function CheckInfo: boolean;
    procedure LoadContent(Value: TStream);
    procedure SaveContent(Value: TStream);
    procedure LoadParams(Value: TStrings);
    procedure SaveParams(Value: TStrings);
  published
    /// <summary>序号</summary>
    property ID: integer read FID write SetID;
    /// <summary>模板名称</summary>
    property TempName: string read FTempName write SetTempName;
    /// <summary>模板类型</summary>
    property Kind: string read FKind write SetKind;
    /// <summary>文件类型</summary>
    property FileType: string read FFileType write SetFileType;
    /// <summary>输出类型</summary>
    property OutType: string read FOutType write SetOutType;
    /// <summary>版本</summary>
    property Ver: integer read FVer write SetVer;
  end;
  /// <summary>工作流管理</summary>
  TFlowInfo = class
  private
    FConn: TADOConnection;
    FAppMap: TVonConfig;
    FAppKind: string;
    FID: integer;
    FAppName: string;
    function GetAppMap: TVonConfig;
  public
    destructor Destroy; override;
    procedure Clear;
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: Integer): Boolean;
    /// <summary>存储信息</summary>
    function SaveToDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接存储</summary>
    function SaveData(AConn: TADOConnection): Boolean;
    /// <summary>SQL直接存储</summary>
    function SaveMap(AConn: TADOConnection = nil): Boolean;
    /// <summary>SQL直接存储</summary>
    function LoadMap(AConn: TADOConnection = nil): Boolean;
    /// <summary>检查数据的有效性</summary>
    function CheckInfo: boolean;
  published
    /// <summary>序号</summary>
    property ID: integer read FID write FID;
    /// <summary>应用名称</summary>
    property AppName: string read FAppName write FAppName;
    /// <summary>应用类型</summary>
    property AppKind: string read FAppKind write FAppKind;
    /// <summary>得到状态展示图表</summary>
    property AppMap: TVonConfig read GetAppMap;
  end;
  /// <summary>流程管理</summary>
  TFlowSettingInfo = class
  private
    FConn: TADOConnection;
    FConditon: string;
    FUserOrRole: integer;
    FMethodName: string;
    FLinker: integer;
    FID: integer;
    FFlowIdx: integer;
    FOutStatus: string;
    FInStatus: string;
  public
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: Integer): Boolean;
    /// <summary>存储信息</summary>
    function SaveToDB(DataSet : TCustomADODataSet): Boolean;
    /// <summary>SQL直接存储</summary>
    function SaveData(AConn: TADOConnection): Boolean;
    /// <summary>检查数据的有效性</summary>
    function CheckInfo: boolean;
  published
    /// <summary>序号</summary>
    property ID: integer read FID write FID;
    /// <summary>流程索引</summary>
    property FlowIdx: integer read FFlowIdx write FFlowIdx;
    /// <summary>进入状态</summary>
    property InStatus: string read FInStatus write FInStatus;
    /// <summary>进入条件</summary>
    property Conditon: string read FConditon write FConditon;
    /// <summary>事件名称</summary>
    property MethodName: string read FMethodName write FMethodName;
    /// <summary>处理结果</summary>
    property OutStatus: string read FOutStatus write FOutStatus;
    /// <summary>处理人类型</summary>
    property UserOrRole: integer read FUserOrRole write FUserOrRole;
    /// <summary>下一步处理人</summary>
    property Linker: integer read FLinker write FLinker;
  end;

  /// <summary>工作流</summary>
  TTaskFollowerInfo = class(TInfoBase)
  private
    FTaskData: TStringList;
    FTaskName: string;
    FPre_ID: integer;
    FPre_Src: string;
    FExecutor: string;
    FDestTaskIdx: integer;
    FID: integer;
    FDest: string;
    FPre_TaskIdx: integer;
    FTaskStatus: string;
    FTaskTime: TDatetime;
    FTaskInputor: string;
    FOverTime: TDatetime;
    FActionName: string;
    FAppName: string;
    FDestID: integer;
    function GetTaskData: TStringList;
  public
    destructor Destroy; override;
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean; override;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: Integer): Boolean; override;
    /// <summary>存储信息</summary>
    function SaveToDB(DataSet : TCustomADODataSet): Boolean; override;
    /// <summary>SQL直接存储</summary>
    function SaveData(AConn: TADOConnection): Boolean; override;
    /// <summary>检查数据的有效性</summary>
    function CheckInfo: boolean; override;
  published
    /// <summary>序号</summary>
    property ID: integer read FID write FID;
    /// <summary>应用名称</summary>
    property AppName: string read FAppName write FAppName;
    /// <summary>任务名称</summary>
    property TaskName: string read FTaskName write FTaskName;
    /// <summary>任务状态</summary>
    property TaskStatus: string read FTaskStatus write FTaskStatus;
    /// <summary>前置任务</summary>
    property Pre_TaskIdx: integer read FPre_TaskIdx write FPre_TaskIdx;
    /// <summary>前置数据源</summary>
    property Pre_Src: string read FPre_Src write FPre_Src;
    /// <summary>前置数据序号</summary>
    property Pre_ID: integer read FPre_ID write FPre_ID;
    /// <summary>任务下达时间</summary>
    property TaskTime: TDatetime read FTaskTime write FTaskTime;
    /// <summary>任务下达人</summary>
    property TaskInputor: string read FTaskInputor write FTaskInputor;
    /// <summary>执行动作</summary>
    property ActionName: string read FActionName write FActionName;
    /// <summary>目标数据</summary>
    property Dest: string read FDest write FDest;
    /// <summary>目标数据序号</summary>
    property DestID: integer read FDestID write FDestID;
    /// <summary>目标任务序号</summary>
    property DestTaskIdx: integer read FDestTaskIdx write FDestTaskIdx;
    /// <summary>执行时间</summary>
    property OverTime: TDatetime read FOverTime write FOverTime;
    /// <summary>执行人</summary>
    property Executor: string read FExecutor write FExecutor;
    /// <summary>任务数据</summary>
    property TaskData: TStringList read GetTaskData;
  end;
  /// <summary>附件基本信息</summary>
  TAttachmentBase = class
  private
    FVersion: Integer;
    FInDate: TDatetime;
    FOrgIdx: Integer;
    FAttachName: string;
    FFileExt: string;
    FInputer: string;
    FConn: TADOConnection;
    FID: Integer;
  public
    procedure FileToDB(TableName, Filename: string);
    procedure DBToFile(TableName, Filename: string);
  published
    property Conn: TADOConnection read FConn write FConn;
    property ID : Integer read FID write FID;
    property OrgIdx : Integer read FOrgIdx write FOrgIdx;
    property AttachName : string read FAttachName write FAttachName;
    property FileExt : string read FFileExt write FFileExt;
    property Version : Integer read FVersion write FVersion;
    property InDate : TDatetime read FInDate write FInDate;
    property Inputer : string read FInputer write FInputer;
  end;
  /// <summary>维度管理</summary>
  TDimItemInfo = class
  private
    FConn: TADOConnection;
    FParentIdx: integer;
    FListOrder: integer;
    FID: integer;
    FDimName: string;
    procedure SetDimName(const Value: string);
    procedure SetID(const Value: integer);
    procedure SetListOrder(const Value: integer);
    procedure SetParentIdx(const Value: integer);
  public
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet: TCustomADODataSet): boolean;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: integer): boolean;
    /// <summary>存储信息</summary>
    function SaveToDB(DataSet: TCustomADODataSet): boolean;
    /// <summary>SQL直接存储</summary>
    function SaveData(AConn: TADOConnection): boolean;
    /// <summary>检查数据的有效性</summary>
    function CheckInfo: boolean;
  published
    /// <summary>序号</summary>
    property ID: integer read FID write SetID;
    /// <summary>维度名称</summary>
    property DimName: string read FDimName write SetDimName;
    /// <summary>父序号</summary>
    property ParentIdx: integer read FParentIdx write SetParentIdx;
    /// <summary>显示序号</summary>
    property ListOrder: integer read FListOrder write SetListOrder;
  end;
  /// <summary>状态流管理</summary>
  TFollowerInfo = class(TInfoBase)
  private
    FCondition: string;
    FID: integer;
    FOutStatus: string;
    FInStatus: string;
    FActionName: string;
    FAppName: string;
  public
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean; override;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: Integer): Boolean; override;
    /// <summary>存储信息</summary>
    function SaveToDB(DataSet : TCustomADODataSet): Boolean; override;
    /// <summary>SQL直接存储</summary>
    function SaveData(AConn: TADOConnection): Boolean; override;
    /// <summary>检查数据的有效性</summary>
    function CheckInfo: boolean; override;
  published
    /// <summary>序号</summary>
    property ID: integer read FID write FID;
    /// <summary>应用名称</summary>
    property AppName: string read FAppName write FAppName;
    /// <summary>前状态</summary>
    property InStatus: string read FInStatus write FInStatus;
    /// <summary>处理事件</summary>
    property ActionName: string read FActionName write FActionName;
    /// <summary>限定条件</summary>
    property Condition: string read FCondition write FCondition;
    /// <summary>处理后状态</summary>
    property OutStatus: string read FOutStatus write FOutStatus;
  end;
  /// <summary>状态图信息</summary>
  TFollowerItemInfo = class(TInfoBase)
  private
    FImg: TBitmap;
    FXPos: integer;
    FYPos: integer;
    FID: integer;
    FKind: integer;
    FItemName: string;
    FAppName: string;
    function GetImg: TBitmap;
  public
    destructor Destroy; override;
    /// <summary>提取信息</summary>
    function LoadFromDB(DataSet : TCustomADODataSet): Boolean; override;
    /// <summary>SQL直接读取</summary>
    function LoadData(AConn: TADOConnection; AID: Integer): Boolean; override;
    /// <summary>存储信息</summary>
    function SaveToDB(DataSet : TCustomADODataSet): Boolean; override;
    /// <summary>SQL直接存储</summary>
    function SaveData(AConn: TADOConnection): Boolean; override;
    /// <summary>检查数据的有效性</summary>
    function CheckInfo: boolean; override;
  published
    /// <summary>序号</summary>
    property ID: integer read FID write FID;
    /// <summary>应用名称</summary>
    property AppName: string read FAppName write FAppName;
    /// <summary>节点名称</summary>
    property ItemName: string read FItemName write FItemName;
    /// <summary>节点类型</summary>
    property Kind: integer read FKind write FKind;
    /// <summary>位置X</summary>
    property XPos: integer read FXPos write FXPos;
    /// <summary>位置Y</summary>
    property YPos: integer read FYPos write FYPos;
    /// <summary>图标</summary>
    property Img: TBitmap read GetImg;
  end;
  /// <summary>含流程处理的业务信息基类</summary>
  TFlowBase = class(TInfoBase)
  protected
    FCurrentStatus: string;
    FMethod: string;
    FLinker: integer;
    FDepartmentIdx: integer;
    FOrgStatus: string;
    FChecker: string;
    FCheckTime: TDatetime;
    FIsOver: boolean;
    FID: Integer;
    FRoleOrUser: integer;
    FNote: string;
    function DoFunction(FunctionName: string; CurrentCalcObject: TCalc_Unit): string; virtual; abstract;
    procedure SaveToHistory(MethodName: string); virtual; abstract;
  published
    /// <summary>项目号</summary>
    property ID: Integer read FID write FID;
    /// <summary>填报部门</summary>
    property DepartmentIdx: integer read FDepartmentIdx write FDepartmentIdx;
    /// <summary>原始状态</summary>
    property OrgStatus: string read FOrgStatus write FOrgStatus;
    /// <summary>当前状态</summary>
    property CurrentStatus: string read FCurrentStatus write FCurrentStatus;
    /// <summary>下一步处理人类型</summary>
    property RoleOrUser: integer read FRoleOrUser write FRoleOrUser;
    /// <summary>下一步处理人</summary>
    property Linker: integer read FLinker write FLinker;
    /// <summary>审核人</summary>
    property Checker: string read FChecker write FChecker;
    /// <summary>审核人</summary>
    property CheckTime: TDatetime read FCheckTime write FCheckTime;
    /// <summary>处理事件</summary>
    property Method: string read FMethod write FMethod;
    /// <summary>处理备注</summary>
    property Note: string read FNote write FNote;
  end;
  /// <summary>业务流程处理基类</summary>
  TFlowCtrlBase = class(TVonCalculator)
  private
    FInfo: TFlowBase;
    FConn: TADOConnection;
    FMethodList: TStringList;
  protected
    procedure BackToEditor(Info: TFlowBase); virtual; abstract;
    function DoOtherFunction(FunctionName: string; CurrentCalcObject: TCalc_Unit): string; override;
    procedure DoOtherKindFlow(MethodName,OutStatus: string; UserOrRole,Linker: Integer; Info: TFlowBase); virtual; abstract;
    //procedure DoOtherFlow();
  public
    constructor Create(Conn: TADOConnection);
    destructor Destroy; override;
    function GetMethodList(AppName, KindName: string; info: TFlowBase): TStrings;
    procedure FlowMethod(MethodIdx: Integer; Info: TFlowBase);
  end;

procedure SetMemo(const KeyID: integer; const TableName, FieldName: string;
  Value: TPersistent; Conn: TADOConnection); overload;
procedure SetMemo(const KeyField, KeyValue, TableName, FieldName: string;
  Value: TPersistent; Conn: TADOConnection); overload;
function GetMemo(const KeyID: integer; const TableName, FieldName: string;
  Value: TPersistent; Conn: TADOConnection): Boolean; overload;
function GetMemo(const KeyField, KeyValue, TableName, FieldName: string;
  Value: TPersistent; Conn: TADOConnection): Boolean; overload;
procedure SetStream(const KeyID: integer; const TableName, FieldName: string;
  Value: TStream; Conn: TADOConnection);     overload;
procedure SetStream(const KeyField, KeyValue, TableName, FieldName: string;
  Value: TStream; Conn: TADOConnection);     overload;
procedure GetStream(const KeyID: integer; const TableName, FieldName: string;
  Value: TStream; Conn: TADOConnection);     overload;
procedure GetStream(const KeyField, KeyValue, TableName, FieldName: string;
  Value: TStream; Conn: TADOConnection);     overload;
procedure SetLargeStr(const KeyID: integer; const TableName, FieldName,
  Value: string; Conn: TADOConnection);      overload;
procedure SetLargeStr(const KeyField, KeyValue, TableName, FieldName,
  Value: string; Conn: TADOConnection);      overload;
function GetLargeStr(const KeyID: integer; const TableName, FieldName: string;
  Conn: TADOConnection): string;             overload;
function GetLargeStr(const KeyField, KeyValue, TableName, FieldName: string;
  Conn: TADOConnection): string;             overload;
procedure LoadFromFile(const KeyField, KeyValue, TableName, FieldName, Filename: string;
  Conn: TADOConnection);
procedure SaveToFile(const KeyField, KeyValue, TableName, FieldName, Filename: string;
  Conn: TADOConnection);

var
  SYS_DEFAULT_USER: string = 'security';
  EventOfChangePsaaword: TEventToChangePassword;

implementation

uses UPlatformLogin, UPlatformDB, DCPBase64, DCPcrypt2;

function Get_UserPWD(UserName, UserPWD: string): string;
var
  Hash: TDCP_hash; // the hash to use
  HashDigest: TBytes; // the result of hashing the passphrase with the salt
  S: AnsiString;
  encoding: TEncoding;
begin
  Hash := GetHash('sha1');
  with Hash do
    try
      SetLength(HashDigest, HashSize div 8 + 1);
      Hash.Init;
      Hash.UpdateStr(UserName + '@JAMESVON REPORT 5 WITH [' +
        UserPWD + '] to LOGIN'); // hash the salt
      Hash.Final(HashDigest[0]); // store the hash in HashDigest
      SetLength(S, ((HashSize div 8 + 2) div 3) * 4);
      Base64Encode(@HashDigest[0], @S[1], Length(HashDigest));
      Result := S;
      // Base64Encode(HashDigest, @S, Length(S));
      // Result:= ArrayToStr(HashDigest);
      // TEncoding.GetBufferEncoding(HashDigest, encoding);
      // Result:= encoding.GetString(HashDigest);
    finally
      Free;
    end;
end;

procedure SetMemo(const KeyID: integer; const TableName, FieldName: string;
  Value: TPersistent; Conn: TADOConnection);
begin
  SetMemo('ID', IntToStr(KeyID), TableName, FieldName, Value, Conn);
end;

procedure SetMemo(const KeyField, KeyValue, TableName, FieldName: string;
  Value: TPersistent; Conn: TADOConnection);
begin
  with TADOQuery.Create(nil) do
    try
      Connection := Conn;
      SQL.Text := 'SELECT [' + KeyField + '],[' + FieldName + '] FROM ' +
        TableName + ' WHERE ' + KeyField + '=:KEY';
      Parameters[0].Value:= KeyValue;
      Open;
      if not EOF then
      begin
        Edit;
        FieldByName(FieldName).Assign(Value);
        Post;
      end;
    finally
      Free;
    end;
end;

function GetMemo(const KeyID: integer; const TableName, FieldName: string;
  Value: TPersistent; Conn: TADOConnection): Boolean;
begin
  Result:= GetMemo('ID', IntToStr(KeyID), TableName, FieldName, Value, Conn);
end;

function GetMemo(const KeyField, KeyValue, TableName, FieldName: string;
  Value: TPersistent; Conn: TADOConnection): Boolean;
begin
  with TADOQuery.Create(nil) do
    try
      Connection := Conn;
      SQL.Text := 'SELECT [' + FieldName + '] FROM ' + TableName + ' WHERE ' +
        KeyField + '=:KEY';
      Prepared:= True;
//      if Parameters.Count = 0 then Parameters.AddParameter.Value:= KeyValue
//      else
      Parameters[0].Value:= KeyValue;
      Open;
      Result:= (not EOF)and(not Fields[0].IsNull);
      if Result then
        Value.Assign(Fields[0]);
    finally
      Free;
    end;
end;

procedure SetStream(const KeyID: integer; const TableName, FieldName: string;
  Value: TStream; Conn: TADOConnection);
begin
  SetStream('ID', IntToStr(KeyID), TableName, FieldName, Value, Conn);
end;

procedure SetStream(const KeyField, KeyValue, TableName, FieldName: string;
  Value: TStream; Conn: TADOConnection);
begin
  with TADOQuery.Create(nil) do
    try
      Connection := Conn;
      SQL.Text := 'SELECT [' + FieldName + '] FROM ' + TableName + ' WHERE ' +
        KeyField + '=''' + KeyValue + '''';
      Open;
      if not EOF then
      begin
        Edit;
        (FieldByName(FieldName) as TBlobField).LoadFromStream(Value);
        Post;
      end;
    finally
      Free;
    end;
end;

procedure GetStream(const KeyID: integer; const TableName, FieldName: string;
  Value: TStream; Conn: TADOConnection);
begin
  GetStream('ID', IntToStr(KeyID), TableName, FieldName, Value, Conn);
end;

procedure GetStream(const KeyField, KeyValue, TableName, FieldName: string;
  Value: TStream; Conn: TADOConnection);
begin
  with TADOQuery.Create(nil) do
    try
      Connection := Conn;
      SQL.Text := 'SELECT [' + FieldName + '] FROM ' + TableName + ' WHERE ' +
        KeyField + '=''' + KeyValue + '''';
      Open;
      if not EOF then
        (FieldByName(FieldName) as TBlobField).SaveToStream(Value);
    finally
      Free;
    end;
end;

procedure SetLargeStr(const KeyID: integer; const TableName, FieldName,
  Value: string; Conn: TADOConnection);
begin
  SetLargeStr('ID', IntToStr(KeyID), TableName, FieldName, Value, Conn);
end;

procedure SetLargeStr(const KeyField, KeyValue, TableName, FieldName,
  Value: string; Conn: TADOConnection);
begin
  with TADOQuery.Create(nil) do
    try
      Connection := Conn;
      SQL.Text := 'SELECT [' + FieldName + '] FROM ' + TableName + ' WHERE ' +
        KeyField + '=''' + KeyValue + '''';
      Open;
      if not EOF then
      begin
        Edit;
        FieldByName(FieldName).AsString := Value;
        Post;
      end;
    finally
      Free;
    end;
end;

function GetLargeStr(const KeyID: integer; const TableName, FieldName: string;
  Conn: TADOConnection): string;
begin
  Result := GetLargeStr('ID', IntToStr(KeyID), TableName, FieldName, Conn);
end;

function GetLargeStr(const KeyField, KeyValue, TableName, FieldName: string;
  Conn: TADOConnection): string;
begin
  with TADOQuery.Create(nil) do
    try
      Connection := Conn;
      SQL.Text := 'SELECT [' + FieldName + '] FROM ' + TableName + ' WHERE ' +
        KeyField + '=''' + KeyValue + '''';
      Open;
      if not EOF then
        Result := FieldByName(FieldName).AsString;
    finally
      Free;
    end;
end;

procedure LoadFromFile(const KeyField, KeyValue, TableName, FieldName, Filename: string;
  Conn: TADOConnection);
var
  fs: TFileStream;
begin
  if Pos(':', Filename) = 0 then
    fs:= TFileStream.Create(FPlatformDB.AppPath + 'Temp\' + Filename, fmOpenRead)
  else fs:= TFileStream.Create(Filename, fmOpenRead);
  SetStream(KeyField, KeyValue, TableName, FieldName, fs, Conn);
  fs.Free;
end;

procedure SaveToFile(const KeyField, KeyValue, TableName, FieldName, Filename: string;
  Conn: TADOConnection);
var
  fs: TFileStream;
begin
  if Pos(':', Filename) = 0 then
    fs:= TFileStream.Create(FPlatformDB.AppPath + 'Temp\' + Filename, fmCreate)
  else fs:= TFileStream.Create(Filename, fmCreate);
  GetStream(KeyField, KeyValue, TableName, FieldName, fs, Conn);
  fs.Free;
end;

{ TInfoBase }

function TInfoBase.CheckInfo: boolean;
begin
  Result:= True;
end;

function TInfoBase.LoadData(AConn: TADOConnection; AID: Integer): Boolean;
begin
  FConn:= AConn;
end;

function TInfoBase.LoadFromDB(DataSet: TCustomADODataSet): Boolean;
begin
  FConn:= DataSet.Connection;
end;

function TInfoBase.SaveData(AConn: TADOConnection): Boolean;
begin
  FConn:= AConn;
end;

function TInfoBase.SaveToDB(DataSet: TCustomADODataSet): Boolean;
begin
  FConn:= DataSet.Connection;
end;

{ TUserGroupInfo }

function TUserGroupInfo.CheckInfo: boolean;
begin
  Result := False;
  if FGroupName = '' then
    raise Exception.Create(Format(RES_CHECK_ERR, ['名称必须录入，数据无效。']));
  Result := True;
end;

function TUserGroupInfo.LoadFromDB(DataSet: TCustomADODataSet): boolean;
begin
  Result := False;
  FConn := DataSet.Connection;
  with DataSet do
  begin
    if IsEmpty then
      raise Exception.Create(RES_NODAT_ERR);
    FID := FieldByName('ID').AsInteger;
    FGroupName := FieldByName('GroupName').AsString;
  end;
  Result := True;
end;

function TUserGroupInfo.SaveToDB(DataSet: TCustomADODataSet): boolean;
begin
  Result := False;
  FConn := DataSet.Connection;
  if not CheckInfo then
    Exit;
  with DataSet do
  begin
    if ID = 0 then
      Append
    else
      Edit;
    FieldByName('GroupName').AsString := FGroupName;
    Post;
    ID := FieldByName('ID').AsInteger;
  end;
  Result := True;
end;

procedure TUserGroupInfo.SetGroupName(const Value: string);
begin
  FGroupName := Value;
end;

procedure TUserGroupInfo.SetID(const Value: integer);
begin
  FID := Value;
end;

{ TUserInfo }

procedure TUserInfo.ChangePWD(PWD: string);
begin
  FLoginPWD := Get_UserPWD(LowerCase(FLoginName), PWD);
end;

function TUserInfo.CheckInfo: boolean;
begin
  Result := False;
  if FLoginName = '' then
    raise Exception.Create(Format(RES_CHECK_ERR, ['登录名称必须录入，数据无效。']));
  Result := True;
end;

constructor TUserInfo.Create;
begin
  FExtends := TStringList.Create;
  //SetLength(FSubRight, 10);
end;

destructor TUserInfo.Destroy;
begin
  //SetLength(FSubRight, 0);
  FExtends.Free;
  inherited;
end;

function TUserInfo.LoadFromDB(DataSet: TCustomADODataSet): boolean;
var
  szRight: TValueBuffer;
  I: Integer;
begin
  Result := False;
  FConn := DataSet.Connection;
  with DataSet do
  begin
    if IsEmpty then
      raise Exception.Create(RES_NODAT_ERR);
    FID := FieldByName('ID').AsInteger;
    FGroupIdx := FieldByName('GroupIdx').AsInteger;
    FLoginName := FieldByName('LoginName').AsString;
    FDisplayName := FieldByName('DisplayName').AsString;
    FLoginPWD := FieldByName('LoginPWD').AsString;
    FExtends.Assign(FieldByName('Extends'));
    szRight:= TVarBytesField(FieldByName('SubRight')).AsBytes;
    FSubRight[0]:= 0; FSubRight[1]:= 0; FSubRight[2]:= 0; FSubRight[3]:= 0; FSubRight[4]:= 0;  
    FSubRight[5]:= 0; FSubRight[6]:= 0; FSubRight[7]:= 0; FSubRight[8]:= 0; FSubRight[9]:= 0;
    for I := 0 to Length(szRight) - 1 do
      FSubRight[I]:= szRight[I];
    FDepartmentIdx := FieldByName('DepartmentIdx').AsInteger;
    FGroupName := FieldByName('GroupName').AsString;
    FWork := FieldByName('Work').AsString;
    FJob := FieldByName('Job').AsString;
  end;
  Result := True;
end;

procedure TUserInfo.EmptyPassword;
begin
  FLoginPWD:= '';
end;

function TUserInfo.GetSubRight(Index: Integer): Byte;
begin
  Result:= FSubRight[Index];
end;

function TUserInfo.SaveToDB(DataSet: TCustomADODataSet): boolean;
begin
  Result := False;
  FConn := DataSet.Connection;
  if not CheckInfo then
    Exit;
  with DataSet do
  begin
    if ID = 0 then
      Append
    else
      Edit;
    FieldByName('GroupIdx').AsInteger := FGroupIdx;
    FieldByName('LoginName').AsString := FLoginName;
    FieldByName('DisplayName').AsString := FDisplayName;
    FieldByName('LoginPWD').AsString := FLoginPWD;
    FieldByName('Extends').Assign(FExtends);
    FieldByName('SubRight').SetData(@FSubRight[0], False);
    FieldByName('DepartmentIdx').AsInteger := FDepartmentIdx;
    FieldByName('GroupName').AsString := FGroupName;
    FieldByName('Work').AsString := FWork;
    FieldByName('Job').AsString := FJob;
    Post;
    ID := FieldByName('ID').AsInteger;
  end;
  Result := True;
end;

procedure TUserInfo.SetDepartmentIdx(const Value: Integer);
begin
  FDepartmentIdx := Value;
end;

procedure TUserInfo.SetDisplayName(const Value: string);
begin
  FDisplayName := Value;
end;

procedure TUserInfo.SetGroupIdx(const Value: integer);
begin
  FGroupIdx := Value;
end;

procedure TUserInfo.SetGroupName(const Value: string);
begin
  FGroupName := Value;
end;

procedure TUserInfo.SetID(const Value: integer);
begin
  FID := Value;
end;

procedure TUserInfo.SetJob(const Value: string);
begin
  FJob := Value;
end;

procedure TUserInfo.SetLoginName(const Value: string);
begin
  FLoginName := LowerCase(Value);
end;

procedure TUserInfo.SetSubRight(Index: Integer; const Value: Byte);
begin
  FSubRight[Index]:= Value;
end;

procedure TUserInfo.SetWork(const Value: string);
begin
  FWork := Value;
end;

{ TRoleInfo }

function TRoleInfo.CheckInfo: boolean;
begin
  Result := False;
  if FRoleName = '' then
    raise Exception.Create(Format(RES_CHECK_ERR, ['角色名称必须录入，数据无效。']));
  Result := True;
end;

function TRoleInfo.LoadFromDB(DataSet: TCustomADODataSet): boolean;
begin
  Result := False;
  FConn := DataSet.Connection;
  with DataSet do
  begin
    if IsEmpty then
      raise Exception.Create(RES_NODAT_ERR);
    FID := FieldByName('ID').AsInteger;
    FRoleName := FieldByName('RoleName').AsString;
    FPID := FieldByName('PID').AsInteger;
    FRoleInfo := FieldByName('RoleInfo').AsString;
    FLinkType := FieldByName('LinkType').AsInteger;
    FListOrder := FieldByName('ListOrder').AsInteger;
    FSpecialFlag := FieldByName('SpecialFlag').AsInteger;
  end;
  Result := True;
end;

function TRoleInfo.SaveToDB(DataSet: TCustomADODataSet): boolean;
begin
  Result := False;
  FConn := DataSet.Connection;
  if not CheckInfo then
    Exit;
  with DataSet do
  begin
    if ID = 0 then
      Append
    else
      Edit;
    FieldByName('RoleName').AsString := FRoleName;
    FieldByName('PID').AsInteger := FPID;
    FieldByName('RoleInfo').AsString := FRoleInfo;
    FieldByName('LinkType').AsInteger := FLinkType;
    FieldByName('ListOrder').AsInteger := FListOrder;
    FieldByName('SpecialFlag').AsInteger := FSpecialFlag;
    Post;
    ID := FieldByName('ID').AsInteger;
  end;
  Result := True;
end;

procedure TRoleInfo.SetID(const Value: integer);
begin
  FID := Value;
end;

procedure TRoleInfo.SetLinkType(const Value: integer);
begin
  FLinkType := Value;
end;

procedure TRoleInfo.SetListOrder(const Value: integer);
begin
  FListOrder := Value;
end;

procedure TRoleInfo.SetPID(const Value: integer);
begin
  FPID := Value;
end;

procedure TRoleInfo.SetRoleInfo(const Value: string);
begin
  FRoleInfo := Value;
end;

procedure TRoleInfo.SetRoleName(const Value: string);
begin
  FRoleName := Value;
end;

function TRoleInfo.GetUserGrade: EUSER_GRADE;
begin
  case FSpecialFlag of
    500:
      Result := UG_HOST;
    200:
      Result := UG_ADMIN;
    100:
      Result := UG_MANAGER;
  else
    Result := UG_NORMAL;
  end;
end;

procedure TRoleInfo.SetSpecialFlag(const Value: integer);
begin
  FSpecialFlag := Value;
end;

procedure TRoleInfo.SetUserGrade(const Value: EUSER_GRADE);
begin
  case Value of
    UG_HOST:
      FSpecialFlag := 500;
    UG_ADMIN:
      FSpecialFlag := 200;
    UG_MANAGER:
      FSpecialFlag := 100;
  else
    FSpecialFlag := 0;
  end;
end;

{ TRightInfo }

function TRightInfo.CheckInfo: boolean;
begin
  Result := False;
  if FUsingName = '' then
    raise Exception.Create(Format(RES_CHECK_ERR, ['角色名称必须录入，数据无效。']));
  Result := True;
end;

function TRightInfo.LoadFromDB(DataSet: TCustomADODataSet): boolean;
begin
  Result := False;
  FConn := DataSet.Connection;
  with DataSet do
  begin
    if IsEmpty then
      raise Exception.Create(RES_NODAT_ERR);
    FID := FieldByName('ID').AsInteger;
    FRoleIdx := FieldByName('RoleIdx').AsInteger;
    FUsingName := FieldByName('UsingName').AsString;
    FControlID := FieldByName('ControlID').AsInteger;
  end;
  Result := True;
end;

function TRightInfo.SaveToDB(DataSet: TCustomADODataSet): boolean;
begin
  Result := False;
  FConn := DataSet.Connection;
  if not CheckInfo then
    Exit;
  with DataSet do
  begin
    if ID = 0 then
      Append
    else
      Edit;
    FieldByName('RoleIdx').AsInteger := FRoleIdx;
    FieldByName('UsingName').AsString := FUsingName;
    FieldByName('ControlID').AsInteger := FControlID;
    Post;
    ID := FieldByName('ID').AsInteger;
  end;
  Result := True;
end;

procedure TRightInfo.SetControlID(const Value: integer);
begin
  FControlID := Value;
end;

procedure TRightInfo.SetID(const Value: integer);
begin
  FID := Value;
end;

procedure TRightInfo.SetRoleIdx(const Value: integer);
begin
  FRoleIdx := Value;
end;

procedure TRightInfo.SetUsingName(const Value: string);
begin
  FUsingName := Value;
end;

{ TUserRoleInfo }

function TUserRoleInfo.CheckInfo: boolean;
begin
  Result := True;
end;

function TUserRoleInfo.LoadFromDB(DataSet: TCustomADODataSet): boolean;
begin
  Result := False;
  FConn := DataSet.Connection;
  with DataSet do
  begin
    if IsEmpty then
      raise Exception.Create(RES_NODAT_ERR);
    FID := FieldByName('ID').AsInteger;
    FUserIdx := FieldByName('UserIdx').AsInteger;
    FRoleIdx := FieldByName('RoleIdx').AsInteger;
  end;
  Result := True;
end;

function TUserRoleInfo.SaveToDB(DataSet: TCustomADODataSet): boolean;
begin
  Result := False;
  FConn := DataSet.Connection;
  if not CheckInfo then
    Exit;
  with DataSet do
  begin
    if ID = 0 then
      Append
    else
      Edit;
    FieldByName('UserIdx').AsInteger := FUserIdx;
    FieldByName('RoleIdx').AsInteger := FRoleIdx;
    Post;
    ID := FieldByName('ID').AsInteger;
  end;
  Result := True;
end;

procedure TUserRoleInfo.SetID(const Value: integer);
begin
  FID := Value;
end;

procedure TUserRoleInfo.SetRoleIdx(const Value: integer);
begin
  FRoleIdx := Value;
end;

procedure TUserRoleInfo.SetUserIdx(const Value: integer);
begin
  FUserIdx := Value;
end;

{ TLoginInfo }

procedure TLoginInfo.AddRole(Role: TRoleInfo);
var
  szQuery: TADOQuery;
  szRight: TRightInfo;
begin
  FRoleList.Add(Role.RoleName, Role.RoleInfo, Role, Pointer(Role.ID));
  case Role.SpecialFlag of
    100:
      FIsManager := True; // 管理员
    200:
      FIsAdmin := True; // 应用管理员
    500:
      FIsSystem := True; // 系统管理员
  end;
  szQuery := TADOQuery.Create(nil);
  with szQuery do
    try
      Connection := FConn;
      SQL.Text := 'SELECT * FROM [SYS_Right] WHERE [RoleIdx]=:RID';
      // [ID], [RoleIdx], [UsingName], [ControlID]
      Parameters[0].Value := Role.ID;
      Open;
      RightStr := '';
      while not EOF do
      begin
        RightStr := RightStr + #13#10 + FieldByName('UsingName').AsString + ','
          + FieldByName('ControlID').AsString;
        FRightList.Add(FieldByName('ControlID').AsString + '@' +
          FieldByName('UsingName').AsString, 1);
        WriteLog(LOG_DEBUG, 'AddRole', FieldByName('ControlID').AsString + '@' +
          FieldByName('UsingName').AsString);
        Next;
      end;
    finally
      if RightStr <> '' then
        System.Delete(RightStr, 1, 2);
      Free;
    end;
end;

procedure TLoginInfo.ChangePWD(newPwd: string);
begin
  inherited ChangePWD(newPwd);
  with TADOQuery.Create(nil) do
    try
      Connection := FConn;
      SQL.Text := 'UPDATE SYS_User SET LoginPWD=:PWD WHERE ID=:ID';
      Parameters[0].Value := FLoginPWD;
      Parameters[1].Value := ID;
      ExecSQL;
    finally
      Free;
    end;
end;

function TLoginInfo.CheckRight(AUsingName: string; AControlID: integer)
  : boolean;
begin
  if(AUsingName = '退出命令')or(AUsingName = '更改口令')or(AUsingName = '帮助命令')or(AControlID < 0)then Result:= true
  else if FIsSystem and (AControlID = 0) then Result:= true
  else if FIsAdmin and (AControlID = 0) then Result:= true
  else if FIsManager and (AControlID = 0) then Result:= true
  else Result := FRightList.ValueOf(IntToStr(AControlID) + '@' + AUsingName) > 0;
end;

constructor TLoginInfo.Create;
begin
  inherited;
  FRoleList := TVonNameValueData.Create;
  FRightList := TStringHash.Create;
  ID := 0;
end;

destructor TLoginInfo.Destroy;
var
  I: integer;
begin
  for I := 0 to FRoleList.Count - 1 do
    TRoleInfo(FRoleList.Objects[I]).Free;
  FRightList.Free;
  FRoleList.Free;
  inherited;
end;

procedure TLoginInfo.LoadRoles(RoleIdx: integer);
var
  szQuery: TADOQuery;
  szRole: TRoleInfo;
begin
  szQuery := TADOQuery.Create(nil);
  with szQuery do
    try
      Connection := FConn;
      SQL.Text := 'SELECT * FROM [SYS_Role] WHERE [ID]=:RID';
      Parameters[0].Value := RoleIdx;
      Open;
      if FRoleList.IndexOfData(Pointer(FieldByName('ID'))) >= 0 then
        Exit;
      szRole := TRoleInfo.Create;
      szRole.LoadFromDB(szQuery);
      AddRole(szRole);
      if (szRole.LinkType = 1) and (szRole.PID > 0)and
        (FRoleList.IndexOfData(Pointer(szRole.PID)) < 0) then
        LoadRoles(szRole.PID)
      else if szRole.LinkType = 2 then
        LoadSubRoles(szRole.ID);
    finally
      Free;
    end;
end;

procedure TLoginInfo.LoadSubRoles(RoleIdx: integer);
var
  szQuery: TADOQuery;

  procedure AddIt;
  var
    szRole: TRoleInfo;
  begin
    szRole := TRoleInfo.Create;
    szRole.LoadFromDB(szQuery);
    AddRole(szRole);
    if (szRole.LinkType = 1) and (FRoleList.IndexOfData(Pointer(szRole.PID)
      ) < 0) then
      LoadRoles(szRole.PID)
    else if szRole.LinkType = 2 then
      LoadSubRoles(szRole.ID);
  end;

begin
  szQuery := TADOQuery.Create(nil);
  with szQuery do
    try
      Connection := FConn;
      SQL.Text := 'SELECT * FROM [SYS_Role] WHERE [PID]=:RID';
      Parameters[0].Value := RoleIdx;
      Open;
      while not EOF do
      begin
        if FRoleList.IndexOfData(Pointer(FieldByName('ID').AsInteger)) < 0 then
          AddIt;
        Next;
      end;
    finally
      Free;
    end;
end;

function TLoginInfo.Login(LoginName, LoginPwd: string;
  Conn: TADOConnection): boolean;
var
  Q: TADOQuery;
begin
  FConn := Conn;
  FIsManager := False; // 管理员
  FIsAdmin := False; // 应用管理员
  FIsSystem := False; // 系统管理员
  Result := False;
{$REGION 'Load info when he or she can login'}
  Q := TADOQuery.Create(nil);
  with Q do
    try
      Connection := Conn;
      SQL.Text := 'SELECT * FROM SYS_User WHERE LoginName=:LNM';
      Parameters[0].Value := LowerCase(LoginName);
      Open;
      if EOF then Exit;
      if FieldByName('LoginPWD').AsString = '' then begin
        LoadFromDB(Q);
        if Assigned(EventOfChangePsaaword) then
          EventOfChangePsaaword(LoginName)
        else ChangePassword(LoginName);
        Result := True;
        //raise Exception.Create('No registe a event to change psaaword.');
      end else if FieldByName('LoginPWD').AsString = Get_UserPWD
        (LowerCase(LoginName), LoginPwd) then
        Result := LoadFromDB(Q);
      if not Result then
        Exit;
    finally
      Free;
    end;
{$ENDREGION}
{$REGION 'Load roles and rights of user'}
  Q := TADOQuery.Create(nil);
  FRightList.Clear;
  with Q do
    try
      Connection := Conn;
      SQL.Text := 'SELECT * FROM [SYS_UserRole] WHERE [UserIdx]=:UID';
      Parameters[0].Value := ID;
      Open;
      while not EOF do
      begin
        LoadRoles(FieldByName('RoleIdx').AsInteger);
        Next;
      end;
    finally
      Free;
    end;
{$ENDREGION}
end;

{ TOptionsInfo }

function TOptionsInfo.CheckInfo: boolean;
begin
  Result := False;
  if OptName = '' then
  begin
    WriteInfo('名称必须录入，数据无效。');
    Exit;
  end;
  Result := True;
end;

procedure TOptionsInfo.GetContent(Value: TPersistent);
begin
  GetMemo(ID, 'SYS_Option', 'OptValues', Value, FConn);
end;

procedure TOptionsInfo.GetContent(Value: TStream);
begin
  GetStream(ID, 'SYS_Option', 'OptValues', Value, FConn);
end;

function TOptionsInfo.LoadFromDB(OptionName: string): boolean;
var
  isCreated: boolean;
begin
  Result := False;
  isCreated := False;
  if not Assigned(FQuery) then
  begin
    FQuery := TADOQuery.Create(nil);
    FQuery.Connection := FConn;
    FQuery.SQL.Text := 'SELECT * FROM SYS_Option WHERE [OptName]=:NM';
    isCreated := True;
  end;
  with FQuery do
    try
      Parameters[0].Value := OptionName;
      Open;
      if IsEmpty then
        Exit;
      ID := FieldByName('ID').AsInteger;
      OptName := FieldByName('OptName').AsString;
      OptKind := FieldByName('OptKind').AsInteger;
      OptInfo := FieldByName('OptInfo').AsString;
    finally
      Close;
      if isCreated then
        FreeAndNil(FQuery);
    end;
  Result := True;
end;

function TOptionsInfo.SaveToDB: integer;
begin
  Result := -1;
  if not CheckInfo then
    Exit;
  if not Assigned(FQuery) then
  begin
    FQuery := TADOQuery.Create(nil);
    FQuery.Connection := FConn;
    FQuery.SQL.Text := 'SELECT * FROM SYS_Option WHERE [OptName]=:NM';
  end;
  with FQuery do
    try
      Parameters[0].Value := OptName;
      Open;
      if EOF then
      begin
        Append;
        Result := 1;
      end
      else
      begin
        Edit;
        Result := 0;
      end;
      FieldByName('OptName').AsString := OptName;
      FieldByName('OptKind').AsInteger := OptKind;
      FieldByName('OptInfo').AsString := OptInfo;
      Post;
      ID := FieldByName('ID').AsInteger;
    finally
      Close;
    end;
end;

procedure TOptionsInfo.SetContent(Value: TPersistent);
begin
  SetMemo(ID, 'SYS_Option', 'OptValues', Value, FConn);
end;

procedure TOptionsInfo.SetConn(const Value: TADOConnection);
begin
  FConn := Value;
end;

procedure TOptionsInfo.SetContent(Value: TStream);
begin
  SetStream(ID, 'SYS_Option', 'OptValues', Value, FConn);
end;

procedure TOptionsInfo.SetID(const Value: integer);
begin
  FID := Value;
end;

procedure TOptionsInfo.SetOptInfo(const Value: string);
begin
  FOptInfo := Value;
end;

procedure TOptionsInfo.SetOptKind(const Value: integer);
begin
  FOptKind := Value;
end;

procedure TOptionsInfo.SetOptName(const Value: string);
begin
  FOptName := Value;
end;

procedure TOptionsInfo.SetQuery(const Value: TADOQuery);
begin
  FQuery := Value;
end;

{ TOrganizationInfo }

function TOrganizationInfo.CheckInfo: boolean;
begin
  Result:= False;
  if OrgName = '' then begin WriteInfo('名称必须录入，数据无效。'); Exit; end;
  Result:= True;
end;

procedure TOrganizationInfo.Clear;
begin
  FID:= 0;
  CreateGuid(FOrgID);
  FOrgName:= '';
  FPID:= 0;
  FState:= '';
  FCity:= '';
  FCounty:= '';
  FKind:= 0;
  FAddress:= '';
  FManager:= '';
  FTelphong:= '';
  FListOrder:= 0;
  FLon:= 0;
  FLat:= 0;
  FLonM:= 0;
  FLatM:= 0;
  FLonS:= 0;
  FLatS:= 0;
  if Assigned(FInfo)then FInfo.Text:= '';
  if Assigned(FMap)then FMap.Text:= '';
  FCRCCode:= '';
end;

constructor TOrganizationInfo.Create;
begin
  CreateGUID(FOrgID);
end;

destructor TOrganizationInfo.Destroy;
begin
  if Assigned(FInfo) then;
    FInfo.Free;
  if Assigned(FMap) then;
    FMap.Free;
  inherited;
end;

function TOrganizationInfo.GetInfo: TVonConfig;
begin
  if not Assigned(FInfo) then begin
    FInfo:= TVonConfig.Create;
    if FOrgID <> TGUID.Empty then GetMemo('OrgID', GUIDToString(FOrgID), 'SYS_Organization', 'Info', FInfo, FConn);
  end;
  Result:= FInfo;
end;

function TOrganizationInfo.GetMap: TVonConfig;
begin
  if not Assigned(FMap) then begin
    FMap:= TVonConfig.Create;
    if FOrgID <> TGUID.Empty then GetMemo('OrgID', GUIDToString(FOrgID), 'SYS_Organization', 'Map', FMap, FConn);
  end;
  Result:= FMap;
end;

function TOrganizationInfo.LoadData(AConn: TADOConnection;
  AID: Integer): Boolean;
var
  DQData: TADOQuery;
begin
  DQData:= TADOQuery.Create(nil);
  try
    FConn:= AConn;
    DQData.Connection:= AConn;
    DQData.SQL.Text:= 'SELECT [ID],[OrgID],[OrgCode],[OrgName],[PID],[State],' +
      '[City],[County],[Kind],[Address],[Manager],[Telphong],[ListOrder],' +
      '[Lon],[Lat],[LonM],[LatM],[LonS],[LatS],CRCCode FROM SYS_Organization ' +
      'WHERE ID=' + IntToStr(AID);
    DQData.Open;
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

function TOrganizationInfo.LoadFromDB(DataSet: TCustomADODataSet): Boolean;
begin
  Result:= False;
  FConn:= DataSet.Connection;
  with DataSet do begin
    if IsEmpty then begin
      WriteInfo('无法对空列表进行提取数据的操作。');
      Exit;
    end;
    FID:= FieldByName('ID').AsInteger;
    FOrgID:= StringToGuid(FieldByName('OrgID').AsString);
    FOrgCode:= FieldByName('OrgCode').AsString;
    FOrgName:= FieldByName('OrgName').AsString;
    FPID:= FieldByName('PID').AsInteger;
    FState:= FieldByName('State').AsString;
    FCity:= FieldByName('City').AsString;
    FCounty:= FieldByName('County').AsString;
    FKind:= FieldByName('Kind').AsInteger;
    FAddress:= FieldByName('Address').AsString;
    FManager:= FieldByName('Manager').AsString;
    FTelphong:= FieldByName('Telphong').AsString;
    FListOrder:= FieldByName('ListOrder').AsInteger;
    FLon:= FieldByName('Lon').AsInteger;
    FLat:= FieldByName('Lat').AsInteger;
    FLonM:= FieldByName('LonM').AsInteger;
    FLatM:= FieldByName('LatM').AsInteger;
    FLonS:= FieldByName('LonS').AsCurrency;
    FLatS:= FieldByName('LatS').AsCurrency;
    if Assigned(FInfo)then FreeAndNil(FInfo);
    if Assigned(FMap)then FreeAndNil(FMap);
    FCRCCode:= FieldByName('CRCCode').AsString;
  end;
  Result:= True;
end;

function TOrganizationInfo.SaveData(AConn: TADOConnection): Boolean;
begin
  FConn:= AConn;
  if FID = 0 then begin
    AConn.Execute('INSERT INTO SYS_Organization([OrgID],[OrgCode],[OrgName],' +
      '[PID],[State],[City],[County],[Kind],[Address],[Manager],[Telphong],' +
      '[ListOrder],[Lon],[Lat],[LonM],[LatM],[LonS],[LatS],[CRCCode])VALUES(''' +
      GuidToString(FOrgID) + ''',''' + FOrgCode + ''',''' + FOrgName + ''',' +
      IntToStr(FPID) + ',''' + FState + ''',''' + FCity + ''',''' + FCounty +
      ''',' + IntToStr(FKind) + ',''' + FAddress + ''',''' + FManager + ''',''' +
      FTelphong + ''',' + IntToStr(FListOrder) + ',' + IntToStr(FLon) + ',' +
      IntToStr(FLat) + ',' + IntToStr(FLonM) + ',' + IntToStr(FLatM) + ',' +
      CurrToStr(FLonS) + ',' + CurrToStr(FLatS) + ',''' + FCRCCode + ''')');
    FID:= FCOnn.Execute('select SCOPE_IDENTITY()').Fields[0].Value;
  end else AConn.Execute('UPDATE SYS_Organization SET [OrgID]=''' +
    GuidToString(FOrgID) + ''',[OrgCode]=''' + FOrgCode + ''',[OrgName]=''' +
    FOrgName + ''',[PID]=' + IntToStr(FPID) + ',[State]=''' + FState +
    ''',[City]=''' + FCity + ''',[County]=''' + FCounty + ''',[Kind]=' +
    IntToStr(FKind) + ',[Address]=''' + FAddress + ''',[Manager]=''' + FManager +
    ''',[Telphong]=''' + FTelphong + ''',[ListOrder]=' + IntToStr(FListOrder) +
    ',[Lon]=' + IntToStr(FLon) + ',[Lat]=' + IntToStr(FLat) + ',[LonM]=' +
    IntToStr(FLonM) + ',[LatM]=' + IntToStr(FLatM) + ',[LonS]=' +
    CurrToStr(FLonS) + ',[LatS]=' + CurrToStr(FLatS) + ',[CRCCode]=''' +
    FCRCCode + ''' WHERE ID=' + IntToStr(FID));
  Result:= FID > 0;
  if Assigned(FInfo) then
    SetMemo(FID, 'SYS_Organization', 'Info', FInfo, FConn);
  if Assigned(FMap) then
    SetMemo(FID, 'SYS_Organization', 'Map', FMap, FConn);
end;

function TOrganizationInfo.SaveToDB(DataSet: TCustomADODataSet): Boolean;
begin
  Result:= False;
  FConn:= DataSet.Connection;
  if not CheckInfo then Exit;
  with DataSet do begin
    if ID = 0 then Append else Edit;
    FieldByName('OrgID').AsString:= GUIDToString(FOrgID);
    FieldByName('OrgCode').AsString:= FOrgCode;
    FieldByName('OrgName').AsString:= FOrgName;
    if FPID < 0 then FieldByName('PID').AsInteger:= 0
    else FieldByName('PID').AsInteger:= FPID;
    FieldByName('State').AsString:= FState;
    FieldByName('City').AsString:= FCity;
    FieldByName('County').AsString:= FCounty;
    FieldByName('Kind').AsInteger:= FKind;
    FieldByName('Address').AsString:= FAddress;
    FieldByName('Manager').AsString:= FManager;
    FieldByName('Telphong').AsString:= FTelphong;
    FieldByName('ListOrder').AsInteger:= FListOrder;
    FieldByName('Lon').AsInteger:= FLon;
    FieldByName('Lat').AsInteger:= FLat;
    FieldByName('LonM').AsInteger:= FLonM;
    FieldByName('LatM').AsInteger:= FLatM;
    FieldByName('LonS').AsCurrency:= FLonS;
    FieldByName('LatS').AsCurrency:= FLatS;
    FieldByName('CRCCode').AsString:= FCRCCode;
    Post;
    FID:= FieldByName('ID').AsInteger;
    if Assigned(FInfo)then
      SetMemo('OrgID', '''' + GUIDToString(FOrgID) + '''', 'SYS_Organization', 'Info', FInfo, FConn);
    if Assigned(FMap)then
      SetMemo('OrgID', '''' + GUIDToString(FOrgID) + '''', 'SYS_Organization', 'Map', FMap, FConn);
  end;
  Result:= True;
end;

procedure TOrganizationInfo.SetAddress(const Value: string);
begin
  FAddress := Value;
end;

procedure TOrganizationInfo.SetCity(const Value: string);
begin
  FCity := Value;
end;

procedure TOrganizationInfo.SetOrgCode(const Value: string);
begin
  FOrgCode := Value;
end;

procedure TOrganizationInfo.SetOrgID(const Value: TGuid);
begin
  FOrgID := Value;
end;

procedure TOrganizationInfo.SetOrgName(const Value: string);
begin
  FOrgName := Value;
end;

procedure TOrganizationInfo.SetCounty(const Value: string);
begin
  FCounty := Value;
end;

procedure TOrganizationInfo.SetCRCCode(const Value: string);
begin
  FCRCCode := Value;
end;

procedure TOrganizationInfo.SetID(const Value: Integer);
begin
  FID := Value;
end;

procedure TOrganizationInfo.SetKind(const Value: integer);
begin
  FKind := Value;
end;

procedure TOrganizationInfo.SetLat(const Value: integer);
begin
  FLat := Value;
end;

procedure TOrganizationInfo.SetLatM(const Value: integer);
begin
  FLatM := Value;
end;

procedure TOrganizationInfo.SetLatS(const Value: Currency);
begin
  FLatS := Value;
end;

procedure TOrganizationInfo.SetListOrder(const Value: integer);
begin
  FListOrder := Value;
end;

procedure TOrganizationInfo.SetLon(const Value: integer);
begin
  FLon := Value;
end;

procedure TOrganizationInfo.SetLonM(const Value: integer);
begin
  FLonM := Value;
end;

procedure TOrganizationInfo.SetLonS(const Value: Currency);
begin
  FLonS := Value;
end;

procedure TOrganizationInfo.SetManager(const Value: string);
begin
  FManager := Value;
end;

procedure TOrganizationInfo.SetPID(const Value: integer);
begin
  FPID := Value;
end;

procedure TOrganizationInfo.SetState(const Value: string);
begin
  FState := Value;
end;

procedure TOrganizationInfo.SetTelphong(const Value: string);
begin
  FTelphong := Value;
end;

{ TQueryTempInfo }

function TQueryTempInfo.CheckInfo: boolean;
begin
  Result:= False;
  if TempName = '' then begin WriteInfo('模板名称必须录入，数据无效。'); Exit; end;
  Result:= True;
end;

destructor TQueryTempInfo.Destroy;
begin
  if Assigned(FParamList) then FParamList.Free;
  if Assigned(FStyle) then FStyle.Free;
  if Assigned(FStatisticList) then FStatisticList.Free;
end;

function TQueryTempInfo.GetParamList: TVonTable;
begin
  if not Assigned(FParamList) then begin
    FParamList:= TVonTable.Create;
    if FID > 0 then
      FParamList.Text:= GetLargeStr(ID, 'SYS_QueryTemp', 'ParamList', FConn);
  end;
  Result:= FParamList;
end;

function TQueryTempInfo.GetStatisticList: TVonTable;
begin
  if not Assigned(FStatisticList) then begin
    FStatisticList:= TVonTable.Create;
    if FID > 0 then
      FStatisticList.Text:= GetLargeStr(ID, 'SYS_QueryTemp', 'Statistic', FConn);
  end;
  Result:= FStatisticList;
end;

function TQueryTempInfo.GetStyle: TVonConfig;
begin
  if not Assigned(FStyle) then begin
    FStyle:= TVonConfig.Create;
    if FID > 0 then
      GetMemo(ID, 'SYS_QueryTemp', 'Style', FStyle, FConn);
  end;
  Result:= FStyle;
end;

function TQueryTempInfo.LoadData(AConn: TADOConnection; AID: Integer): Boolean;
var
  DQData: TADOQuery;
begin
  DQData:= TADOQuery.Create(nil);
  try
    FConn:= AConn;
    DQData.Connection:= AConn;
    DQData.SQL.Text:= 'SELECT * FROM SYS_QueryTemp WHERE ID=' + IntToStr(AID);
    DQData.Open;
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

function TQueryTempInfo.LoadTemp(AConn: TADOConnection; ATempName: string): Boolean;
var
  DQData: TADOQuery;
begin
  DQData:= TADOQuery.Create(nil);
  try
    FConn:= AConn;
    DQData.Connection:= AConn;
    DQData.SQL.Text:= 'SELECT * FROM SYS_QueryTemp WHERE TempName=''' + ATempName + '''';
    DQData.Open;
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

function TQueryTempInfo.LoadFromDB(DataSet: TCustomADODataSet): Boolean;
begin
  Result:= False;
  FConn:= DataSet.Connection;
  with DataSet do begin
    if IsEmpty then begin
      WriteInfo(RES_NO_DATA);
      Exit;
    end;
    FID:= FieldByName('ID').AsInteger;
    FTempName:= FieldByName('TempName').AsString;
    FTitle:= FieldByName('Title').AsString;
    FExportTemp:= FieldByName('ExportTemp').AsString;
    FSQLHeader:= FieldByName('SQLHeader').AsString;
    FSQLCondition:= FieldByName('SQLCondition').AsString;
    FSQLTrailler:= FieldByName('SQLTrailler').AsString;
    if Assigned(FParamList)then FreeAndNil(FParamList);
    if Assigned(FStyle)then FreeAndNil(FStyle);
    if Assigned(FStatisticList)then FreeAndNil(FStatisticList);
  end;
  Result:= True;
end;

function TQueryTempInfo.SaveData(AConn: TADOConnection): Boolean;
begin
  Result:= False;
  FConn:= AConn;
  if not CheckInfo then Exit;
  if ID = 0 then
    FConn.Execute('INSERT ERP_QueryTemp([ID],[TempName],[Title],[ExportTemp],'
      + '[SQLHeader],[SQLCondition],[SQLTrailler])VALUES(''' + IntToStr(FID)
      + ''',''' + FTempName + ''',''' + FTitle + ''',''' + FExportTemp + ''','''
      + FSQLHeader + ''',''' + FSQLCondition + ''',''' + FSQLTrailler + ''')')
  else FConn.Execute('UPDATE ERP_QueryTemp SET [ID]=''' + IntToStr(FID)
    + ''',[TempName]=''' + FTempName + ''',[Title]=''' + FTitle
    + ''',[ExportTemp]=''' + FExportTemp + ''',[SQLHeader]=''' + FSQLHeader
    + ''',[SQLCondition]=''' + FSQLCondition + ''',[SQLTrailler]='''
    + FSQLTrailler + '''');
  if Assigned(FParamList)then
    SetLargeStr(ID, 'ERP_QueryTemp', 'ParamList', FParamList.Text, FConn);
  if Assigned(FStyle)then
    SetMemo(ID, 'ERP_QueryTemp', 'Style', FStyle, FConn);
  if Assigned(FStatisticList)then
    SetLargeStr(ID, 'ERP_QueryTemp', 'Statistic', FStatisticList.Text, FConn);
  Result:= True;
end;

function TQueryTempInfo.SaveToDB(DataSet: TCustomADODataSet): Boolean;
begin
  Result:= False;
  FConn:= DataSet.Connection;
  if not CheckInfo then Exit;
  with DataSet do begin
    if ID = 0 then Append else Edit;
    FieldByName('TempName').AsString:= FTempName;
    FieldByName('Title').AsString:= FTitle;
    FieldByName('ExportTemp').AsString:= FExportTemp;
    FieldByName('SQLHeader').AsString:= FSQLHeader;
    FieldByName('SQLCondition').AsString:= FSQLCondition;
    FieldByName('SQLTrailler').AsString:= FSQLTrailler;
    Post;
    FID:= FieldByName('ID').AsInteger;
    if Assigned(FParamList)then
      SetLargeStr(ID, 'SYS_QueryTemp', 'ParamList', FParamList.Text, FConn);
    if Assigned(FStyle)then
      SetMemo(ID, 'SYS_QueryTemp', 'Style', FStyle, FConn);
    if Assigned(FStatisticList)then
      SetLargeStr(ID, 'SYS_QueryTemp', 'Statistic', FStatisticList.Text, FConn);
  end;
  Result:= True;
end;

procedure TQueryTempInfo.SetExportTemp(const Value: string);
begin
  FExportTemp := Value;
end;

procedure TQueryTempInfo.SetID(const Value: integer);
begin
  FID := Value;
  if FID = 0 then begin
    if Assigned(FParamList) then FreeAndNil(FParamList);
    if Assigned(FStyle) then FreeAndNil(FStyle);
  end;
end;

procedure TQueryTempInfo.SetSQLCondition(const Value: string);
begin
  FSQLCondition := Value;
end;

procedure TQueryTempInfo.SetSQLHeader(const Value: string);
begin
  FSQLHeader := Value;
end;

procedure TQueryTempInfo.SetSQLTrailler(const Value: string);
begin
  FSQLTrailler := Value;
end;

procedure TQueryTempInfo.SetTempName(const Value: string);
begin
  FTempName := Value;
end;

procedure TQueryTempInfo.SetTitle(const Value: string);
begin
  FTitle := Value;
end;

{ TDataDictionaryInfo }

function TDataDictionaryInfo.CheckInfo: boolean;
begin
  Result:= False;
  if TblName = '' then begin WriteInfo('表名称必须录入，数据无效。'); Exit; end;
  Result:= True;
end;

function TDataDictionaryInfo.LoadData(AConn: TADOConnection;
  AID: Integer): Boolean;
var
  DQData: TADOQuery;
begin
  DQData:= TADOQuery.Create(nil);
  try
    FConn:= AConn;
    DQData.Connection:= AConn;
    DQData.SQL.Text:= 'SELECT * FROM SYS_DataDictionary WHERE ID=' + IntToStr(AID);
    DQData.Open;
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

function TDataDictionaryInfo.LoadFromDB(DataSet: TCustomADODataSet): Boolean;
begin
  Result:= False;
  FConn:= DataSet.Connection;
  with DataSet do begin
    if IsEmpty then begin
      WriteInfo(RES_NO_DATA);
      Exit;
    end;
    FID:= FieldByName('ID').AsInteger;
    FTblName:= FieldByName('TblName').AsString;
    FFldName:= FieldByName('FldName').AsString;
    FFldCode:= FieldByName('FldCode').AsString;
    FFldType:= FieldByName('FldType').AsString;
    FLinkTbl:= FieldByName('LinkTbl').AsString;
    FLinkFld:= FieldByName('LinkFld').AsString;
  end;
  Result:= True;
end;

function TDataDictionaryInfo.SaveData(AConn: TADOConnection): Boolean;
begin
  Result:= False;
  FConn:= AConn;
  if not CheckInfo then Exit;
  if ID = 0 then
    FConn.Execute('INSERT SYS_DataDictionary([TblName],[FldName],[FldCode],[FldType],[LinkTbl],[LinkFld]' +
      ')VALUES(''' + FTblName + ''',''' + FFldName + ''',''' + FFldCode + ''',''' + FFldType + ''',''' + FLinkTbl + ''',''' + FLinkFld + ''')')
  else FConn.Execute('UPDATE ERP_DataDictionary SET [ID]=''' + IntToStr(FID) + ''',[TblName]=''' + FTblName + ''',[FldName]=''' + FFldName + ''',[FldCode]=''' + FFldCode + ''',[FldType]=''' + FFldType + ''',[LinkTbl]=''' + FLinkTbl + ''',[LinkFld]=''' + FLinkFld + '''');
  Result:= True;
end;

function TDataDictionaryInfo.SaveToDB(DataSet: TCustomADODataSet): Boolean;
begin
  Result:= False;
  FConn:= DataSet.Connection;
  if not CheckInfo then Exit;
  with DataSet do begin
    if ID = 0 then Append else Edit;
    FieldByName('TblName').AsString:= FTblName;
    FieldByName('FldName').AsString:= FFldName;
    FieldByName('FldCode').AsString:= FFldCode;
    FieldByName('FldType').AsString:= FFldType;
    FieldByName('LinkTbl').AsString:= FLinkTbl;
    FieldByName('LinkFld').AsString:= FLinkFld;
    Post;
    FID:= FieldByName('ID').AsInteger;
  end;
  Result:= True;
end;

procedure TDataDictionaryInfo.SetFldCode(const Value: string);
begin
  FFldCode := Value;
end;

procedure TDataDictionaryInfo.SetFldName(const Value: string);
begin
  FFldName := Value;
end;

procedure TDataDictionaryInfo.SetFldType(const Value: string);
begin
  FFldType := Value;
end;

procedure TDataDictionaryInfo.SetID(const Value: integer);
begin
  FID := Value;
end;

procedure TDataDictionaryInfo.SetLinkFld(const Value: string);
begin
  FLinkFld := Value;
end;

procedure TDataDictionaryInfo.SetLinkTbl(const Value: string);
begin
  FLinkTbl := Value;
end;

procedure TDataDictionaryInfo.SetTblName(const Value: string);
begin
  FTblName := Value;
end;

{ TTemplateInfo }

function TTemplateInfo.CheckInfo: boolean;
begin
  Result:= False;
  if FTempName = '' then begin WriteInfo('模板名称不能为空，数据无效。'); Exit; end;
  Result:= True;
end;

function TTemplateInfo.LoadFromDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    FID:= FieldByName('ID').AsInteger;
    FTempName:= FieldByName('TempName').AsString;
    FKind:= FieldByName('Kind').AsString;
    FFileType:= FieldByName('FileType').AsString;
    FOutType:= FieldByName('OutType').AsString;
    FVer:= FieldByName('Ver').AsInteger;
    Result:= true;
  end;
end;

procedure TTemplateInfo.LoadParams(Value: TStrings);
begin
  GetMemo(FID, 'SYS_Template', 'ExecuteParam', Value, FConn);
end;

function TTemplateInfo.SaveToDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    if FID = 0 then begin
      Append;
      FieldByName('Ver').AsInteger:= 1;
    end else begin
      Edit;
      FieldByName('Ver').AsInteger:= FVer + 1;
    end;
    FieldByName('TempName').AsString:= FTempName;
    FieldByName('Kind').AsString:= FKind;
    FieldByName('FileType').AsString:= FFileType;
    FieldByName('OutType').AsString:= FOutType;
    Post;
    FID:= FieldByName('ID').AsInteger;
  end;
end;

procedure TTemplateInfo.LoadContent(Value: TStream);
begin
  GetStream(FID, 'SYS_Template', 'Content', Value, FConn);
end;

function TTemplateInfo.LoadData(AConn: TADOConnection; AID: Integer): Boolean;
var
  DQData: TADOQuery;
begin
  DQData:= TADOQuery.Create(nil);
  try
    FConn:= AConn;
    DQData.Connection:= AConn;
    DQData.SQL.Text:= 'SELECT [ID],[TempName],[Kind],[FileType],[OutType],[Ver] ' +
      'FROM SYS_Template WHERE ID=' + IntToStr(FID);
    DQData.Open;
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

procedure TTemplateInfo.SaveContent(Value: TStream);
begin
  SetStream(FID, 'SYS_Template', 'Content', Value, FConn);
end;

function TTemplateInfo.SaveData(AConn: TADOConnection): Boolean;
begin
  FConn:= AConn;
  if FID = 0 then begin
    AConn.Execute('INSERT INTO SYS_Template([TempName],[Kind],[FileType],' +
      '[OutType],[Ver])VALUES(''' + FTempName + ''',''' + FKind +
      ''',''' + FFileType + ''',''' + FOutType + ''',' + IntToStr(FVer) +')');
    FID:= FCOnn.Execute('select SCOPE_IDENTITY()').Fields[0].Value;
  end else AConn.Execute('UPDATE SYS_Template SET [TempName]=''' + FTempName +
    ''',[Kind]=''' + FKind + ''',[FileType]=''' + FFileType + ''',[OutType]=''' +
    FOutType + ''',[Ver]=' + IntToStr(FVer) + ' WHERE ID=' + IntToStr(FID));
  Result:= FID > 0;
end;

procedure TTemplateInfo.SaveParams(Value: TStrings);
begin
  SetMemo(FID, 'SYS_Template', 'ExecuteParam', Value, FConn);
end;

procedure TTemplateInfo.SetFileType(const Value: string);
begin
  FFileType := Value;
end;

procedure TTemplateInfo.SetID(const Value: integer);
begin
  FID := Value;
end;

procedure TTemplateInfo.SetKind(const Value: string);
begin
  FKind := Value;
end;

procedure TTemplateInfo.SetOutType(const Value: string);
begin
  FOutType := Value;
end;

procedure TTemplateInfo.SetTempName(const Value: string);
begin
  FTempName := Value;
end;

procedure TTemplateInfo.SetVer(const Value: integer);
begin
  FVer := Value;
end;

{ TFollowerInfo }

function TFollowerInfo.CheckInfo: boolean;
begin
  Result:= False;
  Result:= True;
end;

function TFollowerInfo.LoadFromDB(DataSet : TCustomADODataSet): Boolean;
begin
  inherited;
  with DataSet do begin
    FID:= FieldByName('ID').AsInteger;
    FAppName:= FieldByName('AppName').AsString;
    FInStatus:= FieldByName('InStatus').AsString;
    FActionName:= FieldByName('ActionName').AsString;
    FCondition:= FieldByName('Condition').AsString;
    FOutStatus:= FieldByName('OutStatus').AsString;
    Result:= true;
  end;
end;

function TFollowerInfo.SaveToDB(DataSet : TCustomADODataSet): Boolean;
begin
  inherited;
  with DataSet do begin
    if FID = 0 then begin
      Append;
    end else begin
      Edit;
    end;
    FieldByName('AppName').AsString:= FAppName;
    FieldByName('InStatus').AsString:= FInStatus;
    FieldByName('ActionName').AsString:= FActionName;
    FieldByName('Condition').AsString:= FCondition;
    FieldByName('OutStatus').AsString:= FOutStatus;
    Post;
    FID:= FieldByName('ID').AsInteger;
  end;
end;

function TFollowerInfo.SaveData(AConn: TADOConnection): Boolean;
begin
  inherited;
  if FID = 0 then begin
    AConn.Execute('INSERT INTO ' + SYS_Prefix + 'Follower([AppName],' +
      '[InStatus],[ActionName],[Condition],[OutStatus])VALUES(''' +
      FAppName + ''',''' + FInStatus + ''',''' + FActionName + ''',''' +
      FCondition + ''',''' + FOutStatus + ''')');
    FID:= FConn.Execute('select SCOPE_IDENTITY()').Fields[0].Value;
  end else AConn.Execute('UPDATE ' + SYS_Prefix + 'Follower SET [AppName]=''' +
    FAppName + ''',[InStatus]=''' + FInStatus + ''',[ActionName]=''' +
    FActionName + ''',[Condition]=''' + FCondition + ''',[OutStatus]=''' +
    FOutStatus + ''' WHERE ID=' + IntToStr(FID));
  Result:= FID > 0;
end;

function TFollowerInfo.LoadData(AConn: TADOConnection; AID: Integer): Boolean;
var
  DQData: TADOQuery;
begin
  inherited;
  DQData:= TADOQuery.Create(nil);
  try
    DQData.Connection:= AConn;
    DQData.SQL.Text:= 'SELECT [ID],[AppName],[InStatus],[ActionName],' +
      '[Condition],[OutStatus] FROM ' + SYS_Prefix +
      'Follower WHERE ID=' + IntToStr(AID);
    DQData.Open;
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

{ TFollowerItemInfo }

destructor TFollowerItemInfo.Destroy;
begin
  if Assigned(FImg) then FImg.Free;
end;

function TFollowerItemInfo.CheckInfo: boolean;
begin
  Result:= False;
  Result:= True;
end;

function TFollowerItemInfo.LoadFromDB(DataSet : TCustomADODataSet): Boolean;
begin
  inherited;
  with DataSet do begin
    FID:= FieldByName('ID').AsInteger;
    FAppName:= FieldByName('AppName').AsString;
    FItemName:= FieldByName('ItemName').AsString;
    FKind:= FieldByName('Kind').AsInteger;
    FXPos:= FieldByName('XPos').AsInteger;
    FYPos:= FieldByName('YPos').AsInteger;
    Result:= true;
  end;
  if Assigned(FImg) then
    FreeAndNil(FImg);
end;

function TFollowerItemInfo.SaveToDB(DataSet : TCustomADODataSet): Boolean;
begin
  inherited;
  with DataSet do begin
    if FID = 0 then begin
      Append;
    end else begin
      Edit;
    end;
    FieldByName('AppName').AsString:= FAppName;
    FieldByName('ItemName').AsString:= FItemName;
    FieldByName('Kind').AsInteger:= FKind;
    FieldByName('XPos').AsInteger:= FXPos;
    FieldByName('YPos').AsInteger:= FYPos;
    Post;
    FID:= FieldByName('ID').AsInteger;
    if Assigned(FImg) then
      SetMemo(FID, 'SYS_FollowerItem', 'Img', FImg, FConn);
  end;
end;

function TFollowerItemInfo.SaveData(AConn: TADOConnection): Boolean;
begin
  inherited;
  if FID = 0 then begin
    AConn.Execute('INSERT INTO ' + SYS_Prefix + 'FollowerItem([AppName],' +
      '[ItemName],[Kind],[XPos],[YPos])VALUES(''' + FAppName + ''',''' +
      FItemName + ''',' + IntToStr(FKind) + ',' + IntToStr(FXPos) + ',' +
      IntToStr(FYPos) + ')');
    FID:= FConn.Execute('select SCOPE_IDENTITY()').Fields[0].Value;
  end else AConn.Execute('UPDATE ' + SYS_Prefix + 'FollowerItem SET [AppName]=''' +
    FAppName + ''',[ItemName]=''' + FItemName + ''',[Kind]=' + IntToStr(FKind) +
    ',[XPos]=' + IntToStr(FXPos) + ',[YPos]=' + IntToStr(FYPos) + ' WHERE ID=' +
    IntToStr(FID));
  Result:= FID > 0;
  if Assigned(FImg) then
    SetMemo(FID, 'SYS_FollowerItem', 'Img', FImg, FConn);
end;

function TFollowerItemInfo.LoadData(AConn: TADOConnection; AID: Integer): Boolean;
var
  DQData: TADOQuery;
begin
  inherited;
  DQData:= TADOQuery.Create(nil);
  try
    DQData.Connection:= AConn;
    DQData.SQL.Text:= 'SELECT [ID],[AppName],[ItemName],[Kind],[XPos],[YPos] ' +
      'FROM ' + SYS_Prefix + 'FollowerItem WHERE ID=' + IntToStr(AID);
    DQData.Open;
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

function TFollowerItemInfo.GetImg: TBitmap;
begin
  if not Assigned(FImg) then begin
    FImg:= TBitmap.Create();
    if FID > 0 then
      GetMemo(FID, 'SYS_FollowerItem', 'Img', FImg, FConn);
  end;
  Result:= FImg;
end;

{ TDimItemInfo }

function TDimItemInfo.CheckInfo: boolean;
begin
  Result := False;
  if FParentIdx < 0 then
  begin
    WriteInfo('父序号不能为空，数据无效。');
    Exit;
  end;
  Result := True;
end;

function TDimItemInfo.LoadData(AConn: TADOConnection; AID: integer): boolean;
var
  DQData: TADOQuery;
begin
  DQData := TADOQuery.Create(nil);
  try
    FConn := AConn;
    DQData.Connection := AConn;
    DQData.SQL.Text :=
      'SELECT [ID],[DimName],[ParentIdx],[ListOrder] FROM SYS_DimItem WHERE ID=' +
      IntToStr(AID);
    DQData.Open;
    Result := LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

function TDimItemInfo.LoadFromDB(DataSet: TCustomADODataSet): boolean;
begin
  with DataSet do
  begin
    FConn := Connection;
    FID := FieldByName('ID').AsInteger;
    FDimName := FieldByName('DimName').AsString;
    FParentIdx := FieldByName('ParentIdx').AsInteger;
    FListOrder := FieldByName('ListOrder').AsInteger;
    Result := True;
  end;
end;

function TDimItemInfo.SaveData(AConn: TADOConnection): boolean;
begin
  FConn := AConn;
  if FID = 0 then
  begin
    AConn.Execute
      ('INSERT INTO SYS_DimItem([DimName],[ParentIdx],[ListOrder])VALUES(''' +
      FDimName + ''',' + IntToStr(FParentIdx) + ',' +
      IntToStr(FListOrder) + ')');
    FID := FConn.Execute('select SCOPE_IDENTITY()').Fields[0].Value;
  end
  else
    AConn.Execute('UPDATE SYS_DimItem SET [DimName]=''' + FDimName +
      ''',[ParentIdx]=' + IntToStr(FParentIdx) + ',[ListOrder]=' +
      IntToStr(FListOrder) + ' WHERE ID=' + IntToStr(FID));
  Result := FID > 0;
end;

function TDimItemInfo.SaveToDB(DataSet: TCustomADODataSet): boolean;
begin
  with DataSet do
  begin
    FConn := Connection;
    if FID = 0 then
    begin
      Append;
    end
    else
    begin
      Edit;
    end;
    FieldByName('DimName').AsString := FDimName;
    FieldByName('ParentIdx').AsInteger := FParentIdx;
    FieldByName('ListOrder').AsInteger := FListOrder;
    Post;
    FID := FieldByName('ID').AsInteger;
  end;
end;

procedure TDimItemInfo.SetDimName(const Value: string);
begin
  FDimName:= Value;
end;

procedure TDimItemInfo.SetID(const Value: integer);
begin
  FID:= Value;
end;

procedure TDimItemInfo.SetListOrder(const Value: integer);
begin
  FListOrder:= Value;
end;

procedure TDimItemInfo.SetParentIdx(const Value: integer);
begin
  FParentIdx:= Value;
end;

{ TTaskFollowerInfo }

destructor TTaskFollowerInfo.Destroy;
begin
  if Assigned(FTaskData) then FTaskData.Free;
end;

function TTaskFollowerInfo.CheckInfo: boolean;
begin
  Result:= False;
  Result:= True;
end;

function TTaskFollowerInfo.LoadFromDB(DataSet : TCustomADODataSet): Boolean;
begin
  inherited;
  with DataSet do begin
    FID:= FieldByName('ID').AsInteger;
    FAppName:= FieldByName('AppName').AsString;
    FTaskName:= FieldByName('TaskName').AsString;
    FTaskStatus:= FieldByName('TaskStatus').AsString;
    FPre_TaskIdx:= FieldByName('Pre_TaskIdx').AsInteger;
    FPre_Src:= FieldByName('Pre_Src').AsString;
    FPre_ID:= FieldByName('Pre_ID').AsInteger;
    FTaskTime:= FieldByName('TaskTime').AsDatetime;
    FTaskInputor:= FieldByName('TaskInputor').AsString;
    FActionName:= FieldByName('ActionName').AsString;
    FDest:= FieldByName('Dest').AsString;
    FDestID:= FieldByName('DestID').AsInteger;
    FDestTaskIdx:= FieldByName('DestTaskIdx').AsInteger;
    FOverTime:= FieldByName('OverTime').AsDatetime;
    FExecutor:= FieldByName('Executor').AsString;
    Result:= true;
  end;
  if Assigned(FTaskData) then
    FreeAndNil(FTaskData);
end;

function TTaskFollowerInfo.SaveToDB(DataSet : TCustomADODataSet): Boolean;
begin
  inherited;
  with DataSet do begin
    if FID = 0 then begin
      Append;
    end else begin
      Edit;
    end;
    FieldByName('AppName').AsString:= FAppName;
    FieldByName('TaskName').AsString:= FTaskName;
    FieldByName('TaskStatus').AsString:= FTaskStatus;
    FieldByName('Pre_TaskIdx').AsInteger:= FPre_TaskIdx;
    FieldByName('Pre_Src').AsString:= FPre_Src;
    FieldByName('Pre_ID').AsInteger:= FPre_ID;
    FieldByName('TaskTime').AsDatetime:= FTaskTime;
    FieldByName('TaskInputor').AsString:= FTaskInputor;
    FieldByName('ActionName').AsString:= FActionName;
    FieldByName('Dest').AsString:= FDest;
    FieldByName('DestID').AsInteger:= FDestID;
    FieldByName('DestTaskIdx').AsInteger:= FDestTaskIdx;
    FieldByName('OverTime').AsDatetime:= FOverTime;
    FieldByName('Executor').AsString:= FExecutor;
    Post;
    FID:= FieldByName('ID').AsInteger;
    if Assigned(FTaskData) then
      SetMemo(FID, 'SYS_TaskFollower', 'TaskData', FTaskData, FConn);
  end;
end;

function TTaskFollowerInfo.SaveData(AConn: TADOConnection): Boolean;
begin
  inherited;
  if FID = 0 then begin
    AConn.Execute('INSERT INTO ' + SYS_Prefix + 'TaskFollower([AppName],[TaskName],[TaskStatus],[Pre_TaskIdx],[Pre_Src],[Pre_ID],[TaskTime],[TaskInputor],[ActionName],[Dest],[DestID],[DestTaskIdx],[OverTime],[Executor])VALUES(''' + FAppName + ''',''' + FTaskName + ''',''' + FTaskStatus + ''',' + IntToStr(FPre_TaskIdx) + ',''' + FPre_Src + ''',' + IntToStr(FPre_ID) + ',''' + DatetimeToStr(FTaskTime) + ''',''' + FTaskInputor + ''',''' + FActionName + ''',''' + FDest + ''',' + IntToStr(FDestID) + ',' + IntToStr(FDestTaskIdx) + ',''' + DatetimeToStr(FOverTime) + ''',''' + FExecutor + ''')');
    FID:= FCOnn.Execute('select SCOPE_IDENTITY()').Fields[0].Value;
  end else AConn.Execute('UPDATE ' + SYS_Prefix + 'TaskFollower SET [AppName]=''' + FAppName + ''',[TaskName]=''' + FTaskName + ''',[TaskStatus]=''' + FTaskStatus + ''',[Pre_TaskIdx]=' + IntToStr(FPre_TaskIdx) + ',[Pre_Src]=''' + FPre_Src + ''',[Pre_ID]=' + IntToStr(FPre_ID) + ',[TaskTime]=''' + DatetimeToStr(FTaskTime) + ''',[TaskInputor]=''' + FTaskInputor + ''',[ActionName]=''' + FActionName + ''',[Dest]=''' + FDest + ''',[DestID]=' + IntToStr(FDestID) + ',[DestTaskIdx]=' + IntToStr(FDestTaskIdx) + ',[OverTime]=''' + DatetimeToStr(FOverTime) + ''',[Executor]=''' + FExecutor + ''' WHERE ID=' + IntToStr(FID));
  Result:= FID > 0;
  if Assigned(FTaskData) then
    SetMemo(FID, 'SYS_TaskFollower', 'TaskData', FTaskData, FConn);
end;

function TTaskFollowerInfo.LoadData(AConn: TADOConnection; AID: Integer): Boolean;
var
  DQData: TADOQuery;
begin
  inherited;
  DQData:= TADOQuery.Create(nil);
  try
    DQData.Connection:= AConn;
    DQData.SQL.Text:= 'SELECT [ID],[AppName],[TaskName],[TaskStatus],[Pre_TaskIdx],[Pre_Src],[Pre_ID],[TaskTime],[TaskInputor],[ActionName],[Dest],[DestID],[DestTaskIdx],[OverTime],[Executor] FROM ' + SYS_Prefix + 'TaskFollower WHERE ID=' + IntToStr(AID);
    DQData.Open;
    Result:= LoadFromDB(DQData);
  finally
    DQData.Free;
  end;
end;

function TTaskFollowerInfo.GetTaskData: TStringList;
begin
  if not Assigned(FTaskData) then begin
    FTaskData:= TStringList.Create();
    if FID > 0 then
      GetMemo(FID, 'SYS_TaskFollower', 'TaskData', FTaskData, FConn);
  end;
  Result:= FTaskData;
end;

{ TFlowInfo }

function TFlowInfo.CheckInfo: boolean;
begin
  Result:= False;
  if FAppName = '' then begin WriteInfo('应用名称不能为空，数据无效。'); Exit; end;
  if FAppKind = '' then begin WriteInfo('应用类型不能为空，数据无效。'); Exit; end;
  Result:= True;
end;

procedure TFlowInfo.Clear;
var
  I: Integer;
begin
  if Assigned(FAppMap) then
    FreeAndNil(FAppMap);
end;

destructor TFlowInfo.Destroy;
begin
  if Assigned(FAppMap) then FAppMap.Free;
  inherited;
end;

function TFlowInfo.GetAppMap: TVonConfig;
begin
  if not Assigned(FAppMap) then begin
    FAppMap:= TVonConfig.Create;
    if FID > 0 then LoadMap(FConn);
  end;
  Result:= FAppMap;
end;

function TFlowInfo.LoadData(AConn: TADOConnection; AID: Integer): Boolean;
var
  DQData: TADOQuery;
begin
  Result:= False;
  DQData:= TADOQuery.Create(nil);
  with DQData do try
    Connection:= FConn;
    SQL.Text:= 'SELECT [ID],[AppName],[AppKind] FROM ' + SYS_Prefix + 'Flow WHERE ID=' + IntToStr(AID);
    Open;
    Result:= not EOF;
    if not Result then Exit;
    FID:= FieldByName('ID').AsInteger;
    FAppName:= FieldByName('AppName').AsString;
    FAppKind:= FieldByName('AppKind').AsString;
  finally
    Free;
  end;
  Clear;
  Result:= True;
end;

function TFlowInfo.LoadFromDB(DataSet: TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    FID:= FieldByName('ID').AsInteger;
    FAppName:= FieldByName('AppName').AsString;
    FAppKind:= FieldByName('AppKind').AsString;
    Result:= true;
  end;
  Clear;
end;

function TFlowInfo.LoadMap(AConn: TADOConnection): Boolean;
begin
  if FID < 1 then Exit;
  if not Assigned(FAppMap) then FAppMap:= TVonConfig.Create;
  GetMemo(FID, 'SYS_Flow', 'AppMap', FAppMap, FConn);
end;

function TFlowInfo.SaveData(AConn: TADOConnection): Boolean;
begin
  FConn:= AConn;
  if FID = 0 then
    FID:= AConn.Execute('INSERT INTO ' + SYS_Prefix + 'Flow([AppName],[AppKind])OUTPUT INSERTED.ID VALUES(''' + FAppName + ''',''' + FAppKind + ''')').Fields[0].Value
  else AConn.Execute('UPDATE ' + SYS_Prefix + 'Flow SET [AppName]=''' + FAppName + ''',[AppKind]=''' + FAppKind + ''' WHERE ID=' + IntToStr(FID));
  Result:= FID > 0;
  if Assigned(FAppMap) then
    SetMemo(FID, 'SYS_Flow', 'AppMap', FAppMap, FConn);
end;

function TFlowInfo.SaveMap(AConn: TADOConnection): Boolean;
begin
  if FID < 1 then Exit;
  if not Assigned(FAppMap) then Exit;
  SetMemo(FID, 'SYS_Flow', 'AppMap', FAppMap, FConn);
end;

function TFlowInfo.SaveToDB(DataSet: TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    if FID = 0 then begin
      Append;
    end else begin
      Edit;
    end;
    FieldByName('AppName').AsString:= FAppName;
    FieldByName('AppKind').AsString:= FAppKind;
    Post;
    FID:= FieldByName('ID').AsInteger;
    if Assigned(FAppMap) then
      SetMemo(FID, 'SYS_Flow', 'AppMap', FAppMap, FConn);
  end;
end;

{ TFlowSettingInfo }

function TFlowSettingInfo.CheckInfo: boolean;
begin
  Result:= False;
  if FFlowIdx <= 0 then begin WriteInfo('流程索引不能为空，数据无效。'); Exit; end;
  Result:= True;
end;

function TFlowSettingInfo.LoadFromDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    FID:= FieldByName('ID').AsInteger;
    FFlowIdx:= FieldByName('FlowIdx').AsInteger;
    FInStatus:= FieldByName('InStatus').AsString;
    FConditon:= FieldByName('Conditon').AsString;
    FMethodName:= FieldByName('MethodName').AsString;
    FOutStatus:= FieldByName('OutStatus').AsString;
    FUserOrRole:= FieldByName('UserOrRole').AsInteger;
    FLinker:= FieldByName('Linker').AsInteger;
    Result:= true;
  end;
end;

function TFlowSettingInfo.LoadData(AConn: TADOConnection; AID: Integer): Boolean;
var
  DQData: TADOQuery;
begin
  Result:= False;
  DQData:= TADOQuery.Create(nil);
  with DQData do try
    Connection:= FConn;
    SQL.Text:= 'SELECT [ID],[FlowIdx],[InStatus],[Conditon],[MethodName],[OutStatus],[UserOrRole],[Linker] FROM ' + SYS_Prefix + 'FlowSetting WHERE ID=' + IntToStr(AID);
    Open;
    Result:= not EOF;
    if not Result then Exit;
    FID:= FieldByName('ID').AsInteger;
    FFlowIdx:= FieldByName('FlowIdx').AsInteger;
    FInStatus:= FieldByName('InStatus').AsString;
    FConditon:= FieldByName('Conditon').AsString;
    FMethodName:= FieldByName('MethodName').AsString;
    FOutStatus:= FieldByName('OutStatus').AsString;
    FUserOrRole:= FieldByName('UserOrRole').AsInteger;
    FLinker:= FieldByName('Linker').AsInteger;
  finally
    Free;
  end;
  Result:= True;
end;

function TFlowSettingInfo.SaveToDB(DataSet : TCustomADODataSet): Boolean;
begin
  with DataSet do begin
    FConn:= Connection;
    if FID = 0 then begin
      Append;
    end else begin
      Edit;
    end;
    FieldByName('FlowIdx').AsInteger:= FFlowIdx;
    FieldByName('InStatus').AsString:= FInStatus;
    FieldByName('Conditon').AsString:= FConditon;
    FieldByName('MethodName').AsString:= FMethodName;
    FieldByName('OutStatus').AsString:= FOutStatus;
    FieldByName('UserOrRole').AsInteger:= FUserOrRole;
    FieldByName('Linker').AsInteger:= FLinker;
    Post;
    FID:= FieldByName('ID').AsInteger;
  end;
end;

function TFlowSettingInfo.SaveData(AConn: TADOConnection): Boolean;
begin
  FConn:= AConn;
  if FID = 0 then
    FID:= AConn.Execute('INSERT INTO ' + SYS_Prefix + 'FlowSetting([FlowIdx],[InStatus],[Conditon],[MethodName],[OutStatus],[UserOrRole],[Linker]))OUTPUT INSERTED.ID VALUES(' + IntToStr(FFlowIdx) + ',''' + FInStatus + ''',''' + FConditon + ''',''' + FMethodName + ''',''' + FOutStatus + ''',' + IntToStr(FUserOrRole) + ',' + IntToStr(FLinker) + ')').Fields[0].Value
  else AConn.Execute('UPDATE ' + SYS_Prefix + 'FlowSetting SET [FlowIdx]=' + IntToStr(FFlowIdx) + ',[InStatus]=''' + FInStatus + ''',[Conditon]=''' + FConditon + ''',[MethodName]=''' + FMethodName + ''',[OutStatus]=''' + FOutStatus + ''',[UserOrRole]=' + IntToStr(FUserOrRole) + ',[Linker]=' + IntToStr(FLinker) + ' WHERE ID=' + IntToStr(FID));
  Result:= FID > 0;
end;

{ TAttachmentBase }

procedure TAttachmentBase.DBToFile(TableName, Filename: string);
var
  fs: TFileStream;
begin
  fs:= TFileStream.Create(Filename, fmCreate);
  with TADOQuery.Create(nil) do
    try
      Connection := FConn;
      SQL.Text := 'SELECT Content FROM ' + TableName + ' WHERE ID=' + IntToStr(FID);
      Open;
      if not EOF then
        (FieldByName('Content') as TBlobField).SaveToStream(fs);
    finally
      Free;
    end;
  fs.Free;
end;

procedure TAttachmentBase.FileToDB(TableName, Filename: string);
var
  fs: TFileStream;
begin
  fs:= TFileStream.Create(Filename, fmOpenRead);
  with TADOQuery.Create(nil) do
    try
      Connection := FConn;
      SQL.Text := 'SELECT Content FROM ' + TableName + ' WHERE ID=' + IntToStr(FID);
      Open;
      if not EOF then
      begin
        Edit;
        (FieldByName('Content') as TBlobField).LoadFromStream(fs);
        Post;
      end;
    finally
      Free;
    end;
  fs.Free;
end;

{ TFlowCtrlBase }

constructor TFlowCtrlBase.Create(Conn: TADOConnection);
begin
  FConn:= Conn;
  FMethodList:= TStringList.Create;
end;

destructor TFlowCtrlBase.Destroy;
begin
  FMethodList.Free;
  inherited;
end;

function TFlowCtrlBase.DoOtherFunction(FunctionName: string; CurrentCalcObject: TCalc_Unit): string;
begin
  if functionName = 'SAMEDEPARTMENT' then
    Result:= BoolToStr(FInfo.DepartmentIdx = FPlatformDB.LoginInfo.DepartmentIdx)
  else if Assigned(FInfo) then Result:= FInfo.DoFunction(FunctionName, CurrentCalcObject);
end;

procedure TFlowCtrlBase.FlowMethod(MethodIdx: Integer; Info: TFlowBase);
var
  Q: TADOQuery;
  MethodFound: Boolean;
  S: string;
begin
  FInfo:= Info;
  Q:= TADOQuery.Create(nil);
  with Q do try
    Connection:= FConn;
    SQL.Text:= 'SELECT MethodName,[Conditon],OutStatus,UserOrRole,Linker FROM SYS_FlowSetting WHERE ID=:MID';
    Parameters.FindParam('MID').Value:= MethodIdx;
    Open;
    MethodFound:= False;
    while(not EOF)and(not MethodFound) do begin
      S:= CalcText(FieldByName('Conditon').AsString);
      MethodFound:= (S = '')or(S = '1')or(S = 'true');
      Next;
    end;
    if not MethodFound then
      raise Exception.Create('没有发现响应的工作流程设置，请联系管理员。');
    FInfo.Checker:= FPlatformDB.LoginInfo.LoginName;
    FInfo.CheckTime:= Now;
    FInfo.SaveToHistory(FieldByName('MethodName').AsString);
    FInfo.OrgStatus:= FInfo.CurrentStatus;
    FInfo.CurrentStatus:= FieldByName('OutStatus').AsString;
    if FInfo.CurrentStatus = '编辑' then BackToEditor(FInfo)
    else case FieldByName('UserOrRole').AsInteger of
    0: //UserOrRole=0，则为角色ID
      begin
        FInfo.RoleOrUser:= 0;
        FInfo.Linker:= FieldByName('Linker').AsInteger;
      end;
    1: //UserOrRole=1，则为人员ID
      begin
        FInfo.RoleOrUser:= 1;
        FInfo.Linker:= FieldByName('Linker').AsInteger;
      end;
    else //UserOrRole > 4
        DoOtherKindFlow(Q.FieldByName('MethodName').AsString, Q.FieldByName('OutStatus').AsString,
          Q.FieldByName('UserOrRole').AsInteger, Q.FieldByName('Linker').AsInteger, Info);
    end;
  finally
    Free;
  end;
end;

function TFlowCtrlBase.GetMethodList(AppName, KindName: string; info: TFlowBase): TStrings;
var
  Q: TADOQuery;
begin
  FInfo:= Info;
  FMethodList.Clear;
  Q:= TADOQuery.Create(nil);
  with Q do try
    Connection:= FConn;
    SQL.Text:= 'SELECT S.ID,S.InStatus,S.Conditon,S.MethodName ' +
      'FROM SYS_FlowSetting S JOIN SYS_Flow F ON F.ID=S.FlowIdx WHERE ' +
      'F.AppName=:App AND F.AppKind=:Kdn AND S.InStatus=:ST';
    Parameters.FindParam('App').Value:= AppName;
    Parameters.FindParam('Kdn').Value:= KindName;
    Parameters.FindParam('ST').Value:= FInfo.CurrentStatus;
    if FInfo.CurrentStatus='撤回' then Parameters.FindParam('ST').Value:='编辑'
      else Parameters.FindParam('ST').Value:= FInfo.CurrentStatus;

    Open;
    while not EOF do begin
      if(FieldByName('Conditon').AsString = '')or(CalcText(FieldByName('Conditon').AsString) = '1')then
        FMethodList.AddObject(FieldByName('MethodName').AsString, TObject(FieldByName('ID').AsInteger));
      Next;
    end;
  finally
    Free;
  end;
  Result:= FMethodList;
end;

end.
