unit UPlatformLogin;

interface

uses
  winapi.Windows, winapi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, jpeg, IniFiles, DB, UVonSystemFuns,
  data.win.ADODB, Math;

type
  TGetUserParams = procedure(ParamStr: string; Params: TStringList) of object;
  TUserLoginEvent = function(UserName, UserPwd: string): boolean of object;
  TDlgLoginEvent = function(Sender: TObject): boolean of object;

  TFPlatformLogin = class(TForm)
    plFace: TPanel;
    chkRemember: TCheckBox;
    Edit1: TEdit;
    Edit2: TEdit;
    DQUser: TADOQuery;
    lbTitle1: TLabel;
    lbTitle2: TLabel;
    lbPrompt: TLabel;
    lb1: TLabel;
    lb2: TLabel;
    imgBackground: TImage;
    imgLogin: TImage;
    imgCancel: TImage;
    imgOK: TImage;
    lbRemember: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure Edit1DblClick(Sender: TObject);
    procedure BtnOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Edit2KeyPress(Sender: TObject; var Key: Char);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Init(UserName: string);
  end;

/// <summary>登录</summary>
function Login(Event: TUserLoginEvent = nil): Boolean;
/// <summary>修改密码</summary>
procedure ChangePassword(UserName: string; Event: TUserLoginEvent = nil);

/// <summary>登录接口</summary>
function DBLogin(UserName, UserPwd: string): boolean;
/// <summary>修改密码接口</summary>
function DBChangePassword(UserName, UserPwd: string): boolean;

var
  FPlatformLogin: TFPlatformLogin;
  CurrentUser: string;

implementation

uses UPlatformDB, UVonClass;

type TVonControl = class(TControl) published property Font; end;

function DBLogin(UserName, UserPwd: string): boolean;
begin
  Result := FPlatformDB.LoginInfo.Login(UserName, UserPwd, FPlatformDB.ADOConn);
end;

function DBChangePassword(UserName, UserPwd: string): boolean;
begin
  FPlatformDB.LoginInfo.ChangePwd(UserPwd);
end;

function Login(Event: TUserLoginEvent): Boolean;
var
  LoginCount: Integer;
  DefaultUsername: string;
begin    // 登录
  Result := False;
  LoginCount := 3;
  with TFPlatformLogin.Create(nil) do
    try // Create a login dialog
      with TIniFile.Create(FPlatformDB.ConfigFilename)do try
        DefaultUsername := ReadString('Login', 'DefaultUsername', '');
      finally
        Free;
      end;
      Edit1.Text:= DefaultUsername;
      while (not Result) and (LoginCount > 0) do
      begin
        Dec(LoginCount);
        imgLogin.Visible := True;
        if ShowModal <> mrOk then Exit;
        if Assigned(Event) then
          Result := Event(Edit1.Text, Edit2.Text)
        else Result := DBLogin(Edit1.Text, Edit2.Text);
        if not Result then
          ShowMessage('您录入的用户名或口令不正确，请重新录入。')
        else begin
          if chkRemember.Checked then
            with TIniFile.Create(FPlatformDB.ConfigFilename) do
            try
              WriteString('SYSTEM', 'DefaultUsername', Edit1.Text);
            finally
              Free;
            end;
          Exit;
        end;
      end;
    finally
      Free;
    end;
  WriteInfo('登录失败！');
end;

procedure ChangePassword(UserName: string; Event: TUserLoginEvent);
var
  LoginCount: Integer;
begin
  LoginCount := 3;
  with TFPlatformLogin.Create(nil) do
    try
      lbPrompt.Caption := UserName + '更改用户口令';
      lb1.Caption := '登录口令';
      lb2.Caption := '验证口令';
      imgOK.Visible := True;
      chkRemember.Visible := False;
      lbRemember.Visible := chkRemember.Visible;
      imgOK.Visible := True;
      Edit1.Text:= '';
      Edit2.Text:= '';
      while LoginCount > 0 do
      begin
        if ShowModal <> mrOk then
          Exit; // Dialog
        if Edit1.Text = Edit2.Text then
        begin
          if Assigned(Event) then
            Event(UserName, Edit1.Text)
          else DBChangePassword(UserName, Edit1.Text);
          Exit;
        end;
        ShowMessage('密码设置不一致，请重新录入确认。');
        Dec(LoginCount);
      end;
    finally
      Free;
    end;
end;

{$R *.dfm}


procedure TFPlatformLogin.FormActivate(Sender: TObject);
begin
  Position:= poScreenCenter;
end;

type tempControl = class(TControl) published property Font; property Color; end;

procedure TFPlatformLogin.FormCreate(Sender: TObject);
var
  I: Integer;
  ms: TMemoryStream;
  FSysSetting: TVonSectionGroup;
  FLoginSetting: TVonSection;
begin
  FSysSetting:= TVonSectionGroup.Create;
  FSysSetting.LoadFromFile(FPlatformDB.AppPath + 'Styles\VonWindows.cfg');
  FLoginSetting:= FSysSetting.Sections['Login'];
  if not Assigned(FLoginSetting) then begin
    FLoginSetting:= TVonSection.Create;
    FLoginSetting.SectionName:= 'Login';
    FSysSetting.AddSection(FLoginSetting);
  end;
  for I := 0 to plFace.ControlCount - 1 do
    begin
      with TControl(plFace.Controls[I])do begin
        Top:= FLoginSetting.GetInteger(plFace.Controls[I].Name + '.Top');
        Left:= FLoginSetting.GetInteger(plFace.Controls[I].Name + '.Left');
        Width:= FLoginSetting.GetInteger(plFace.Controls[I].Name + '.Width');
        Height:= FLoginSetting.GetInteger(plFace.Controls[I].Name + '.Height');
      end;
      if plFace.Controls[I] is TImage then begin
        ms:= TMemoryStream.Create;
        FLoginSetting.GetStream(plFace.Controls[I].Name, ms);
        ms.Position:= 0;
        TImage(plFace.Controls[I]).Picture.LoadFromStream(ms);
        ms.Free;
      end else with TVonControl(plFace.Controls[I])do begin
        Caption:= FLoginSetting.GetString(plFace.Controls[I].Name + '.Caption');
        Font.Size:= FLoginSetting.GetInteger(plFace.Controls[I].Name + '.Font.Size');
        Font.Color:= TColor(FLoginSetting.GetInteger(plFace.Controls[I].Name + '.Font.Color'));
        Font.Name:= FLoginSetting.GetString(plFace.Controls[I].Name + '.Font.Name');
      end;
    end;
  FSysSetting.Free;
end;

procedure TFPlatformLogin.FormShow(Sender: TObject);
begin
  if chkRemember.Checked then
    Edit2.SetFocus
  else Edit1.SetFocus;
end;

procedure TFPlatformLogin.Init(UserName: string);
begin

end;

procedure TFPlatformLogin.BtnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TFPlatformLogin.Edit1DblClick(Sender: TObject);
begin
  if (Edit1.Text = '') and (Edit2.Text = '') then
    Edit1.Text := 'security';
  Edit2.Text := Edit1.Text;
end;

procedure TFPlatformLogin.Edit2KeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = 13 then
    BtnOKClick(nil);
end;

procedure TFPlatformLogin.BtnOKClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

end.
