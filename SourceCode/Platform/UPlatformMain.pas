(* ******************************************************************************
  * UPlatformMain v3.0 written by James Von (jamesvon@163.com) ******************
  ********************************************************************************
  * 平台之应用程序主程序
  *------------------------------------------------------------------------------*
  *     应用程序可继承本程序，增加自己的内容，本程序包含有：
  *     系统登录功能
  *     更改口令功能
  *     退出功能
  *     日志填写功能
  *     用户管理功能（用户可根据自己的实际情况，参照本功能重新）
  *     角色管理功能（用户可根据自己的实际情况，参照本功能重新）
  *     自动建立应用菜单（用户可以根据自己的情况向菜单区添加模块）
  *==============================================================================*
  * 向菜单区添加自己模块的方法
  *------------------------------------------------------------------------------*
  *     建立模块（需要从UPlatformDockForm单元的TFPlatformDockForm来继承），用
  * RegDockForm函数进行注册，使用以下两个函数来创建TFPlatformDockForm类：
  * TFPlatformDockForm.CreateDockForm(DockFormName, FormCaption: string)
  * TFPlatformDockForm.CreateDockForm(DockFormID: Integer; FormCaption: string)
  *     具体实现方法可以参照：
  *     UPlatformMenuBase.TFPlatformMenuBase
  *     UPlatformMenu1.TFPlatformMenu1
  *============================================================================= *)
unit UPlatformMain;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Tabs,
  Controls, Vcl.Forms, StdCtrls, ExtCtrls, DockTabSet, ImgList, ToolWin, ComCtrls,
  Dialogs, WinApi.ShellAPI, Buttons, Menus, jpeg, IniFiles, Contnrs, System.ImageList,
  UPlatformDB, UPlatformInfo, UVonMenuInfo, UFrameBarBase,
  UVonSystemFuns, UPlatformDockForm, UPlatformMenuBase, UVonLog, UVonClass,
  Data.DB, Vcl.Grids;

resourcestring
  WIN_HINT = 'The system will expire after %d days.';
  WIN_MULTI_MORE = '您打开的窗口太多，请关闭一些窗口后再打开。';
  RES_NO_RIGHT = '您的权限不能使用本功能';

type
  TFPlatformMain = class(TForm)
    plDockSite: TPanel;
    imgWinBar: TImageList;
    StatusBar1: TStatusBar;
    pmWinIcon: TPopupMenu;
    menuIconRestore: TMenuItem;
    menuIconMove: TMenuItem;
    menuIconSize: TMenuItem;
    menuIconMin: TMenuItem;
    menuIconMax: TMenuItem;
    N6: TMenuItem;
    menuIconClose: TMenuItem;
    menuIconHelp: TMenuItem;
    N2: TMenuItem;
    menuIconMenu: TMenuItem;
    mChangeUser: TMenuItem;
    menuIconAbout: TMenuItem;
//    procedure DockTabSet1DockDrop(Sender: TObject; Source: TDragDockObject;
//      X, Y: Integer);
//    procedure DockTabSet1TabRemoved(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure plDockSiteUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure plDockSiteDockDrop(Sender: TObject; Source: TDragDockObject;
      X, Y: Integer);
    procedure menuIconRestoreClick(Sender: TObject);
    procedure menuIconMoveClick(Sender: TObject);
    procedure menuIconSizeClick(Sender: TObject);
    procedure menuIconMinClick(Sender: TObject);
    procedure menuIconMaxClick(Sender: TObject);
    procedure menuIconCloseClick(Sender: TObject);
    procedure pmWinIconPopup(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure menuIconMenuClick(Sender: TObject);
    procedure mChangeUserClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FMainMenu: TFPlatformMenuBase;
    FCurrentBtn: TBitBtn;
    FCurrentMenu: TMenuItem;
    FTTaskMenu: TTaskMenu;
    FTTaskBtn: TTaskBtn;
    FOnCheckRight: TEventCheckRight;
    FBackground: TImage;
    procedure EventOfTaskClick(Sender: TObject);
    procedure EventOfResize(Sender: TObject);
    procedure EventOfActive(Sender: TObject);
    procedure EventOfChildFormClose(Sender: TObject; var Action: TCloseAction);
    procedure SetBackground(const Value: TImage);
    procedure SetFormStyle(ACtrl: TWinControl; HRate, VRate: Extended); virtual;
  protected
    FMainBar: TFrameBarBase;
    FDefaultTask, FDefaultTaskParams: string;
    FStyleFile: TVonSectionGroup;
    FStyleSetting: TVonSection;
    FDefaultTaskControl: Integer;
    FBackWin: TForm;
    FBackWinClass: TFormClass;
    //procedure CreateParams(var Params: TCreateParams); override;
    function CheckRight(TaskName: string; ControlID: Integer): Boolean; virtual;
    procedure EventOfMenuClick(MenuName, UsingName: string;
      ControlID, RunType: Integer; ExecParam: string);
    procedure ChangeUser(Sender: TObject); virtual;
  public
    { Public declarations }
    procedure LoadStyle; virtual;
    procedure LoadMenu(MenuType: string; MenuStyle: Integer; MenuCaption: string);
    procedure LoadTask;
    procedure ApplyStyle(AForm: TForm);
    function OpenAppModule(UsingName: string; UsingInfo: TAppCmdInfo;
      RunType: TRunType; ControlID: Integer = 0; TaskParam: string = ''; ExecuteParam: string = '')
      : TForm;
    function OpenModule(Caption: string; FormClass: TFormClass;
      RunType: TRunType; ControlID: Integer = 0; TaskParam: string = ''; ExecuteParam: string = '')
      : TForm;
    procedure RegBackWin(Form: TFormClass);
    procedure OpenASystemWindow(WinName, Param: string);
    procedure EventOnNewChildWin(AForm: TForm; ATitle: string);
  published
    property Background: TImage read FBackground write SetBackground;
  end;

var
  FPlatformMain: TFPlatformMain;

implementation

uses
  Vcl.Styles,
  Vcl.Themes,
  VCL.SysStyles, UPlatformLogin, UVonSinoVoiceApi,
  UFramebarSimple;

type
  TVonGrid = class(TCustomGrid)
  published property FixedColor;
  end;
  TVonInput = class(TCustomEdit)
  published property Color;
  end;
  TVonWin = class(TWinControl)
  published property Color; property Font; property ParentColor; property ParentFont;
  end;
  TVonButton = class(TButtonControl)
  published property Font; property Color;
  end;

{$R *.dfm}

procedure TFPlatformMain.ChangeUser(Sender: TObject);
var
  i: Integer;
begin
  if Login then
    with TFPlatformMain(Application.MainForm) do
    begin
      for i := FMainBar.FormCount - 1 downto 0 do // 关闭所有子窗口
        FMainBar.CloseForm(FMainBar.GetFormByIndex(i));
      //ClearDockForm;
      LoadStyle;
    end
  else DlgInfo('错误', '登录失败！');
  StatusBar1.Panels[0].Text := FPlatformDB.LoginInfo.DisplayName;
end;

function TFPlatformMain.CheckRight(TaskName: string;
  ControlID: Integer): Boolean;
begin
  Result := FPlatformDB.LoginInfo.CheckRight(TaskName, ControlID);
end;
//
//procedure TFPlatformMain.CreateParams(var Params: TCreateParams);
//begin
//  inherited CreateParams(Params);
//  Params.Style := Params.Style or WS_SIZEBOX;
//end;

procedure TFPlatformMain.FormCreate(Sender: TObject);
var
  szIcon: TIcon;
  i: Integer;
begin
  FPlatformMain := Self;
  FStyleFile:= TVonSectionGroup.Create;
  WriteLog(LOG_DEBUG, 'TFPlatformMain.FormCreate', 'Will to apply styles.');
  LoadStyle;
  StatusBar1.Panels[0].Text := FPlatformDB.LoginInfo.DisplayName;
  InitSinoVoice(Self.Handle);
  Caption := FPlatformDB.ApplicationTitle;
  WriteLog(LOG_DEBUG, 'TFPlatformMain.FormCreate', 'Created.');
end;

procedure TFPlatformMain.FormDestroy(Sender: TObject);
begin
  //ClearDockForm;
  FStyleSetting.Free;
end;

procedure TFPlatformMain.FormShow(Sender: TObject);
var
  tskIdx, appIdx: Integer;
begin
  setwindowlong(Handle, gwl_style, getwindowlong(handle, gwl_style) and not ws_caption);
  Self.Top:= - GetSystemMetrics(SM_CYCAPTION);
  Self.Left:= 0;
  Self.Width:= Screen.Width;
  Self.Height:= Screen.WorkAreaHeight;// + GetSystemMetrics(SM_CYCAPTION);

  menuIconMenuClick(nil);
  if FDefaultTask = '' then Exit;
  if FPlatformDB.LoginInfo.IsSystem then Exit;
  WriteLog(LOG_DEBUG, 'TFPlatformMain.FormShow', 'Display tasks..');

  tskIdx := FAppUsingList.IndexOfValue(0, FDefaultTask); // 功能->系统功能
  if tskIdx < 0 then
  begin
    if FDefaultTask = '更改口令' then
      ChangePassword(FPlatformDB.LoginInfo.DisplayName)
    else if FDefaultTask = '帮助命令' then
      OpenASystemWindow('帮助命令', 'HELP')
    else if FDefaultTask = '关于命令' then
      OpenASystemWindow('关于命令', 'ABOUT')
    else if FDefaultTask = '链接网站' then
      OpenASystemWindow('链接网站', 'WEB')
    else if FDefaultTask = '退出命令' then
      Close
  end
  else
  begin
    //FAppUsingList功能信息 <UsingName>|<CommandName>|<Param>|<HelpID>
    appIdx := FAppCommandList.IndexOf(FDefaultTask);//UsingName
    OpenAppModule(FDefaultTask,                     //TaskCaption
      TAppCmdInfo(FAppCommandList.Objects[appIdx]), //UsingInfo: TAppCmdInfo from FAppCommandList
      dtSingle,                                     //RunType
      FDefaultTaskControl,                          //Controller
      FAppUsingList[tskIdx, 2],                     //TaskParam: string from FAppUsingList
      FDefaultTaskParams);                          //ExecuteParam
  end;
end;

procedure TFPlatformMain.LoadMenu(MenuType: string; MenuStyle: Integer;
  MenuCaption: string);
var
  I: Integer;
begin
  if Assigned(FMainMenu) then FMainMenu.Free;
  FMainMenu := TFPlatformMenuBase.CreateDockForm(MenuType, MenuCaption) as TFPlatformMenuBase;
  if not Assigned(FMainMenu) then Exit;
  FMainMenu.LoginInfo:= FPlatformDB.LoginInfo;
  ApplyStyle(FMainMenu);
    FMainMenu.Parent := Self;
    case MenuStyle of
      0:
        FMainMenu.Align := alLeft;
      1:
        FMainMenu.Align := alRight;
      2:
        FMainMenu.Align := alTop;
      3:
        FMainMenu.Align := alBottom;
    end;
  FMainMenu.MenuStyle := MenuStyle;
  FMainMenu.DisplayMenu(FAppMenuList, FPlatformDB.imgLarge, FPlatformDB.ImgSmall);
  FMainMenu.OnClick := EventOfMenuClick;
end;

procedure TFPlatformMain.LoadStyle;
type
  PVonInt = ^VonInt;
  VonInt = packed record
    case integer of
    0: (bytes: array [0..3]of byte;);
    1: (Int : Integer;);
    end;
var
  S: string;
  Idx: Integer;
begin // 提取设置
  FStyleFile.LoadFromFile(FPlatformDB.AppPath + 'Styles\VonWindows.cfg');
  FStyleSetting:= FStyleFile.Sections['Style'];
  if not Assigned(FStyleSetting) then begin
    FStyleSetting:= TVonSection.Create;
    FStyleSetting.SectionName:= 'Style';
    FStyleFile.AddSection(FStyleSetting);
  end;
  with FStyleSetting do begin
    if TStyleManager.TrySetStyle(GetString('WinScheme', 'windows')) then
      TCustomStyleEngine.UnRegisterSysStyleHook('#32768', TSysPopupStyleHook);
    if Assigned(FMainBar) then FreeAndNil(FMainBar);
    FMainBar:= CreateBarComponent(GetString('WinBar', '普通标题'));
    if not Assigned(FMainBar) then FMainBar:= CreateBarComponent('普通标题');
    FMainBar.Parent:= FPlatformMain;
    FMainBar.MainForm:= Self;
    FMainBar.Align:= alTop;
    FMainBar.SetVersion('[' + FPlatformDB.CurrentVersion + ']');
    if Assigned(Background) then
    begin
      Background.Visible := GetBool('DisplayBackImage', false);
      if Background.Visible and FileExists(FPlatformDB.AppPath + 'Styles\Background.JPG') then
        Background.Picture.LoadFromFile(FPlatformDB.AppPath + 'Styles\Background.JPG');
      if Background.Visible then
      begin
        Background.AutoSize:= false;
        Background.Align:= alClient;
        Background.Center:= True;
        Background.Anchors := [];
        if GetBool('BackImgAnchorTop', false) then
          Background.Anchors := Background.Anchors + [akTop];
        if GetBool('BackImgAnchorLeft', false) then
          Background.Anchors := Background.Anchors + [akLeft];
        if GetBool('BackImgAnchorRight', false) then
          Background.Anchors := Background.Anchors + [akRight];
        if GetBool('BackImgAnchorBottom', false) then
          Background.Anchors := Background.Anchors + [akBottom];
      end;
    end;
    case GetInteger('MenuStyle', 0) of
      0: plDockSite.Align := alLeft;
      1: plDockSite.Align := alRight;
      2: plDockSite.Align := alTop;
      3: plDockSite.Align := alBottom;
    end;
    LoadMenu(GetString('MenuType', 'MENU_0'), GetInteger('MenuStyle', 0),
      GetString('MenuCaption', '系统菜单'));
    LoadTask;
    FDefaultTask := GetString('DefaultTask', '');
    FDefaultTaskControl := GetInteger('DefaultTaskControl', 0);
    FDefaultTaskParams := GetString('DefaultTaskParam', '');
  end;
end;

procedure TFPlatformMain.LoadTask;
var
  NewItem: TSpeedButton;
  i, cmdIdx: Integer;
begin
  // <TaskCaption><TaskImage><UsingName><Controller><Left><Top><Params><Hint>
  for i := 0 to FAppTaskList.Count - 1 do
  begin
    cmdIdx := FAppCommandList.IndexOf(FAppTaskList.Values[i, 2]);
    if cmdIdx < 0 then
      Exit;
    if not CheckRight(FAppTaskList[i, 2], StrToInt(FAppTaskList[i, 3])) then
      Exit; // 权限不足
    NewItem := TSpeedButton.Create(Self);
    NewItem.Caption := FAppTaskList[i, 0]; // <MenuName>
    NewItem.DesignInfo := i; // <TaskID>
    NewItem.Left := StrToInt(FAppTaskList[i, 4]); // <Left>
    NewItem.Top := StrToInt(FAppTaskList[i, 5]); // <Top>
    NewItem.Parent := Self;
    NewItem.Layout := blGlyphTop;
    NewItem.Height := 96;
    NewItem.Width := 96;
    NewItem.Hint := FAppTaskList[i, 8];
    NewItem.OnClick := EventOfTaskClick; // 添加相应事件
  end;
end;

{ Events of windows Icon }

procedure TFPlatformMain.mChangeUserClick(Sender: TObject);
begin
  ChangeUser(Sender);
end;

procedure TFPlatformMain.menuIconCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFPlatformMain.menuIconMaxClick(Sender: TObject);
begin
  Self.WindowState := wsMaximized;
end;

procedure TFPlatformMain.menuIconMenuClick(Sender: TObject);
var
  i, cnt: Integer;
begin
  if not Assigned(FMainMenu) then Exit;
  FMainMenu.Show;
  FMainMenu.Parent := Self;
end;

procedure TFPlatformMain.menuIconMinClick(Sender: TObject);
begin
  Self.WindowState := wsMinimized;
end;

procedure TFPlatformMain.menuIconMoveClick(Sender: TObject);
begin
  SendMessage(Handle, WM_SYSCOMMAND, SC_MOVE, 0);
end;

procedure TFPlatformMain.menuIconRestoreClick(Sender: TObject);
begin
  Self.WindowState := wsNormal;
end;

procedure TFPlatformMain.menuIconSizeClick(Sender: TObject);
begin
  SendMessage(Handle, WM_SYSCOMMAND, SC_SIZE, 0);
end;

procedure TFPlatformMain.pmWinIconPopup(Sender: TObject);
begin
  menuIconMax.Enabled := WindowState <> wsMaximized;
  menuIconMin.Enabled := WindowState <> wsMinimized;
  menuIconRestore.Enabled := WindowState <> wsNormal;
end;

procedure TFPlatformMain.RegBackWin(Form: TFormClass);
begin
  FBackWinClass := Form;
  Application.CreateForm(FBackWinClass, FBackWin);
  FBackWin.FormStyle := fsMDIChild;
  FBackWin.WindowState := wsMaximized;
  FBackWin.Show;
end;

procedure TFPlatformMain.SetBackground(const Value: TImage);
begin
  FBackground := Value;
end;

type
  TTempControl = class(TControl)
  published
    property ParentFont;
  end;
procedure TFPlatformMain.SetFormStyle(ACtrl: TWinControl; HRate, VRate: Extended);
const _FH : Integer = 12;
var
  I, K: Integer;

  procedure ChangeSize(ACtrl: TControl);
  begin
    case ACtrl.Align of
    alTop, alBottom: ACtrl.ClientHeight:= Round((ACtrl.ClientHeight - 4)* VRate + 4);
    alLeft, alRight: ACtrl.ClientWidth:= Round((ACtrl.ClientWidth - 4) * HRate + 4);
    alNone: begin
      ACtrl.ClientHeight:= Round((ACtrl.ClientHeight - 4) * VRate + 4);
      ACtrl.ClientWidth:= Round((ACtrl.ClientWidth - 4) * HRate + 4);
    end;
    end;
  end;

  procedure ChangePosition(ACtrl: TControl);
  begin
    case TVonWin(ACtrl).Align of
    alNone: begin
      ACtrl.Top:= Round(ACtrl.Top * VRate);
      ACtrl.Left:= Round(ACtrl.Left * HRate);
    end;
    end;
  end;

begin
  for I := 0 to ACtrl.ControlCount - 1 do begin
    //if not TTempControl(ACtrl.Controls[I]).ParentFont then Continue;          //不继承上级字体的不处理
    if ACtrl.Controls[I] is TButtonControl then begin                           //TButtonControl
      TVonButton(ACtrl.Controls[I]).ParentFont:= False;
      ChangeSize(ACtrl.Controls[I]);
      ChangePosition(ACtrl.Controls[I]);
    end else if ACtrl.Controls[I] is TPageControl then begin                    //TPageControl
      SetFormStyle(TWinControl(ACtrl.Controls[I]), HRate, VRate);
    end else if ACtrl.Controls[I] is TCustomPanel then begin                    //TCustomPanel
      SetFormStyle(TWinControl(ACtrl.Controls[I]), HRate, VRate);
    end else if(ACtrl.Controls[I] is TLabel)then begin                          //TLabel
      ChangePosition(ACtrl.Controls[I] as TControl);
      if not (ACtrl.Controls[I] as TLabel).AutoSize then
        ChangeSize(TWinControl(ACtrl.Controls[I]));
    end else if (ACtrl.Controls[I] is TCustomEdit)or(ACtrl.Controls[I] is TToolWindow) then begin      //TCustomEdit or TToolWindow
      ChangeSize(ACtrl.Controls[I] as TWinControl);
      ChangePosition(ACtrl.Controls[I] as TWinControl);
    end else if ACtrl.Controls[I] is TWinControl then begin                     //TWinControl
      SetFormStyle(TWinControl(ACtrl.Controls[I]), HRate, VRate);                             //
    end else if ACtrl.Controls[I] is TGraphicControl then begin                 //TGraphicControl
      ChangeSize(ACtrl.Controls[I]);
      ChangePosition(ACtrl.Controls[I]);
    end;
    if(ACtrl.Controls[I] is TCustomGrid)then begin
      TVonGrid(ACtrl.Controls[I]).DefaultRowHeight:=
        Round(TVonGrid(ACtrl.Controls[I]).DefaultRowHeight * VRate);
    end;
    with ACtrl.Controls[I] do
      if AlignWithMargins then begin
        if(Margins.Left = 0)then Margins.Left:= FStyleSetting.GetInteger('MarginLeft');
        if(Margins.Top = 0)then Margins.Top:= FStyleSetting.GetInteger('MarginTop');
        if(Margins.Right = 0)then Margins.Right:= FStyleSetting.GetInteger('MarginRight');
        if(Margins.Bottom = 0)then Margins.Bottom:= FStyleSetting.GetInteger('MarginBottom');
      end;
  end;
  ChangeSize(ACtrl);
  ChangePosition(ACtrl);
end;

procedure TFPlatformMain.plDockSiteDockDrop(Sender: TObject;
  Source: TDragDockObject; X, Y: Integer);
begin
  case plDockSite.Align of
    alLeft, alRight:
      if plDockSite.Width = 0 then
        plDockSite.Width := 200;
    alTop, alBottom:
      if plDockSite.Height = 0 then
        plDockSite.Height := 150;
  end;
end;

procedure TFPlatformMain.plDockSiteUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
  if plDockSite.DockClientCount = 1 then
  begin
    plDockSite.Width := 0;
  end;
end;

{ events of children window }

procedure TFPlatformMain.EventOfActive(Sender: TObject);
begin
  FMainBar.GetFormByName(TForm(Sender).Caption);
end;

procedure TFPlatformMain.EventOfChildFormClose(Sender: TObject;
  var Action: TCloseAction);

begin // 子窗口关闭事件，释放子窗口空间
  FMainBar.CloseForm(Sender as TForm);
  Action := caFree;
  FMainBar.DisplayWinChildrenBtns;
  if (FMainBar.FormCount = 0) and not Assigned(FBackWin) and
    Assigned(FBackWinClass) then
  begin
    Application.CreateForm(FBackWinClass, FBackWin);
    FBackWin.FormStyle := fsMDIChild;
    FBackWin.WindowState := wsMaximized;
    FBackWin.Show;
  end;
end;

procedure TFPlatformMain.OpenASystemWindow(WinName, Param: string);
var
  Idx: Integer;
begin
  Idx := FAppCommandList.IndexOf(WinName);
  if Idx < 0 then
    Exit;
  with FAppCommandList.Objects[Idx] as TAppCmdInfo do
    if UsingParams = '' then
      OpenModule(WinName, UsingClass, dtMulti, HelpID, Param, UsingParams)
    else OpenModule(WinName, UsingClass, dtMulti, HelpID, UsingParams, '');
end;

procedure TFPlatformMain.EventOfMenuClick(MenuName, UsingName: string;
  ControlID, RunType: Integer; ExecParam: string);
var
  tskIdx, appIdx: Integer;
  procedure OpenHelpHtml(Key: string);
  var URL: string;
  begin
    if FPlatformDB.WebUrl = '' then
      URL := 'File://' + FPlatformDB.AppPath + 'Help/' + Key + '.html'
    else URL:= FPlatformDB.WebUrl + 'Help/' + Key;
    ShellExecute(0, 'OPEN', PChar(URL), '', PChar(FPlatformDB.AppPath), 0)
  end;
begin
  //菜单信息 <MenuName><MemuParentName><ImageID><UsingName><Controller><ShortKey><RunType><Params><Hint>
  tskIdx := FAppUsingList.IndexOfValue(0, UsingName); // 功能->系统功能
  if tskIdx < 0 then
  begin
    if UsingName = '更改口令' then
      ChangePassword(FPlatformDB.LoginInfo.DisplayName)
    else if UsingName = '帮助命令' then OpenHelpHtml('Index')
    else if UsingName = '关于命令' then OpenHelpHtml('About')
    else if UsingName = '链接网站' then OpenHelpHtml(ExecParam)
    else if UsingName = '退出命令' then Close
  end
  else
  begin
    // if not CheckRight(UsingName, ControlID) then
    // raise Exception.Create(RES_NO_RIGHT);
    appIdx := FAppCommandList.IndexOf(FAppUsingList.Values[tskIdx, 1]);
    OpenAppModule(MenuName, TAppCmdInfo(FAppCommandList.Objects[appIdx]),
      TRunType(RunType), ControlID, FAppUsingList[tskIdx, 2], ExecParam);
  end;
end;

procedure TFPlatformMain.EventOfResize(Sender: TObject);
begin
  if Assigned(FMainBar) then
    FMainBar.ChangeFormState(TForm(Sender));
end;

procedure TFPlatformMain.EventOfTaskClick(Sender: TObject);
var
  tskIdx, appIdx, btnIdx: Integer;
  UsingName: string;
  procedure OpenHelpHtml(Key: string);
  var URL: string;
  begin
    if FPlatformDB.WebUrl = '' then
      URL := 'File://' + FPlatformDB.AppPath + 'Help/' + Key + '.html'
    else URL:= FPlatformDB.WebUrl + 'Help/' + Key;
    ShellExecute(0, 'OPEN', PChar(URL), '', PChar(FPlatformDB.AppPath), 0)
  end;
begin
  btnIdx:= TSpeedButton(Sender).DesignInfo;
  //按钮信息 <TaskCaption><TaskImage><UsingName><Controller><Left><Top><RunType><Params><Hint>
  UsingName:= FAppTaskList.Values[btnIdx, 2];
  tskIdx := FAppUsingList.IndexOfValue(0, UsingName); // 功能->系统功能
  if tskIdx < 0 then
  begin
    if UsingName = '更改口令' then
      ChangePassword(FPlatformDB.LoginInfo.DisplayName)
    else if UsingName = '帮助命令' then OpenHelpHtml('Index')
    else if UsingName = '关于命令' then OpenHelpHtml('About')
    else if UsingName = '链接网站' then OpenHelpHtml(FAppTaskList.Values[btnIdx, 7])
    else if UsingName = '退出命令' then Close
  end
  else
  begin
    //FAppUsingList功能信息 <UsingName>|<CommandName>|<Param>|<HelpID>
    appIdx := FAppCommandList.IndexOf(FAppUsingList.Values[tskIdx, 1]); //UsingName
    OpenAppModule(FAppTaskList.Values[btnIdx, 0],                       //TaskCaption
      TAppCmdInfo(FAppCommandList.Objects[appIdx]),                     //UsingInfo: TAppCmdInfo from FAppCommandList
      TRunType(StrToInt(FAppTaskList.Values[btnIdx, 6])),               //RunType
      StrToInt(FAppTaskList.Values[btnIdx, 3]),                         //Controller
      FAppUsingList[tskIdx, 2],                                         //TaskParam: string from FAppUsingList
      FAppTaskList.Values[btnIdx, 7]);                                  //ExecuteParam
  end;
end;

procedure TFPlatformMain.ApplyStyle(AForm: TForm);
var
  szOW, szOH: Integer;
begin
  szOH:= ABS(AForm.Font.Height);
  szOW:= AForm.Canvas.TextWidth('Jamesvon');
  AForm.Font.Size:= FStyleSetting.GetInteger('FontSize', 9);
  SetFormStyle(AForm, AForm.Canvas.TextWidth('Jamesvon') / szOW, ABS(AForm.Font.Height) / szOH);
end;

procedure TFPlatformMain.EventOnNewChildWin(AForm: TForm; ATitle: string);
begin

end;

function TFPlatformMain.OpenAppModule(UsingName: string; UsingInfo: TAppCmdInfo;
  RunType: TRunType; ControlID: Integer;
  TaskParam, ExecuteParam: string): TForm;
var
  Helper: IVonFormHelper;
  i: Integer;

  function CreateChildForm: TForm;
  var
    Idx: Integer;
  begin
    Result := FMainBar.GetFormByName(UsingName);
    if Result <> nil then Exit;
    Result := FMainBar.OpenedAForm(UsingName, UsingInfo.UsingClass);
    Result.Hide;
    Result.OnClose := EventOfChildFormClose;
    if Assigned(Result.OnActivate) then
      Result.OnActivate(Result);
    Result.OnActivate := EventOfActive;
    Result.OnResize := EventOfResize;
    Result.HelpFile := TaskParam;
    Result.Caption := UsingName;
    ApplyStyle(Result);
  end;

begin // TDisplayType = (dtCloseAll, dtCloseActive, dtMulti, dtModal);
  (* Memory params of a task window *)
  if Assigned(FBackWin) then
    FreeAndNil(FBackWin);
  FPlatformDB.TaskControlID := ControlID;
  FPlatformDB.TaskParams := TaskParam;
  FPlatformDB.ExecParams := ExecuteParam;
  {$region 'Close Other Windows'}
  case RunType of
    dtCloseAll:
      begin
        // Application.MainForm.
        for i := FMainBar.FormCount - 1 downto 0 do // 关闭所有子窗口
          with FMainBar.GetFormByIndex(i) do
            // 只有类一致且任务参数一致时才表示窗口时同一个任务
            if (ClassName = UsingInfo.UsingClass.ClassName) and
              (HelpFile = TaskParam) then
            begin
              Show;
              Exit;
            end
            else
              FMainBar.CloseForm(FMainBar.GetFormByIndex(i));
      end;
    dtCloseActive:
      begin
        if (ActiveMDIChild <> nil) and
          (ClassName = UsingInfo.UsingClass.ClassName) and
          (HelpFile = TaskParam) then
        begin
          ActiveMDIChild.Show;
          Exit;
        end
        else FMainBar.CloseForm(nil);
      end;
  end;
  {$endregion}
  {$region 'Create and Show a Window that you selected' }
  case RunType of
    dtModal:
      begin
        Result := CreateChildForm();
//        Result.Color:= Self.Color;
//        Result.Font:= Self.Font;
      end;
    dtCloseAll, dtCloseActive, dtMulti:
      begin
        Result := CreateChildForm();
        Result.FormStyle := fsMDIChild;
//        Result.Ctl3D := True;
//        Result.Designer.Modified;
        Result.WindowState := wsMaximized;
        FMainBar.DisplayWinChildrenBtns;
      end;
    dtSingle:
      if FMainBar.GetFormByName(UsingName) <> nil then
        raise Exception.Create('该窗口已打开。')
      else begin
        Result := CreateChildForm();
        //Result.FormStyle := fsMDIChild;
//        Result.Ctl3D := True;
//        Result.Color:= Self.Color;
//        Result.Font:= Self.Font;
//        Result.Designer.Modified;
        //Result.WindowState := wsMaximized;
        //FMainBanner.DisplayWinChildrenBtns;
      end;
  end;
  {$endregion}
  //SetFormStyle(Result);
  FPlatformDB.TaskControlID := 0;
  FPlatformDB.TaskParams := '';
  FPlatformDB.ExecParams := '';
  if RunType <> dtModal then begin
    Result.Show;
    if Result.GetInterface(GUID_VonFormHelper, Helper) then
      Helper.Init();
  end else Result.ShowModal;
end;

function TFPlatformMain.OpenModule(Caption: string; FormClass: TFormClass;
  RunType: TRunType; ControlID: Integer;
  TaskParam, ExecuteParam: string): TForm;
var
  Helper: IVonFormHelper;
  i, Idx: Integer;

  function CreateChildForm: TForm;
  begin
    Result := FMainBar.GetFormByName(Caption);
    if Result <> nil then Exit;
    Result := FMainBar.OpenedAForm(Caption, FormClass);
    Result.OnClose := EventOfChildFormClose;
    if Assigned(Result.OnActivate) then
      Result.OnActivate(Result);
    Result.OnActivate := EventOfActive;
    Result.OnResize := EventOfResize;
    Result.HelpFile := TaskParam;
    Result.Caption := Caption;
    ApplyStyle(Result);
  end;
begin
  (* Memory params of a task window *)
  if Assigned(FBackWin) then
    FreeAndNil(FBackWin);
  FPlatformDB.TaskControlID := ControlID;
  FPlatformDB.TaskParams := TaskParam;
  FPlatformDB.ExecParams := ExecuteParam;
  {$region 'Close Other Windows'}
  case RunType of
    dtCloseAll:
      begin
        for i := FMainBar.FormCount - 1 downto 0 do // 关闭所有子窗口
          with FMainBar.GetFormByIndex(i) do
            // 只有类一致且任务参数一致时才表示窗口时同一个任务
            if (ClassName = FormClass.ClassName) and
              (HelpFile = TaskParam) then
            begin
              Show;
              Exit;
            end
            else
              FMainBar.CloseForm(FMainBar.GetFormByIndex(i));
      end;
    dtCloseActive:
      begin
        if (ActiveMDIChild <> nil) and (ActiveMDIChild.ClassName = FormClass.ClassName) and
          (HelpFile = TaskParam) then
        begin
          ActiveMDIChild.Show;
          Exit;
        end
        else
        begin
          ActiveMDIChild.Close;
          FMainBar.DisplayWinChildrenBtns;
        end;
      end;
  end;
  {$endregion}
  if FMainBar.GetFormByName(Caption) <> nil then begin
    if RunType = dtSingle then
      raise Exception.Create('该窗口已打开。');
    Result := FMainBar.GetFormByName(Caption);
  end else
  {$region 'Create and Show a Window that you selected' }
  case RunType of
    dtModal:
      begin
        Result := CreateChildForm();
        Result.Color:= Self.Color;
        Result.Font:= Self.Font;
      end;
    dtCloseAll, dtCloseActive, dtMulti:
      begin
        Result := CreateChildForm();
        Result.FormStyle := fsMDIChild;
        Result.Ctl3D := True;
//        Result.Designer.Modified;
        Result.WindowState := wsMaximized;
        FMainBar.DisplayWinChildrenBtns;
      end;
    dtSingle:
      begin
        Result := CreateChildForm();
        Result.FormStyle := fsMDIChild;
        Result.Ctl3D := True;
        Result.Color:= Self.Color;
        Result.Font:= Self.Font;
        Result.WindowState := wsMaximized;
        FMainBar.DisplayWinChildrenBtns;
      end;
  end;
  {$endregion}
  FPlatformDB.TaskControlID := 0;
  FPlatformDB.TaskParams := '';
  FPlatformDB.ExecParams := '';
  //SetFormStyle(Result);
  if RunType <> dtModal then begin
    Result.Show;
    if Result.GetInterface(GUID_VonFormHelper, Helper) then
      Helper.Init();
  end else Result.Visible:= False;
end;

end.
