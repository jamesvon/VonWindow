unit UPlatformMenu1;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UPlatformMenuBase, ComCtrls, UPlatformDockForm, UVonClass, ExtCtrls, UVonLog;

type
  TFPlatformMenu1 = class(TFPlatformMenuBase)
    lvMenu: TListView;
    plParent: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lvMenuClick(Sender: TObject);
  private
    { Private declarations }
    FListParent: TVonNameValueData;
    FMenuValues: TVonArraySetting;
    FCurrentParent: string;
    procedure DisplayChildren(ParentMenu: string);
    procedure EventGroupClick(Sender: TObject);
  public
    { Public declarations }
    procedure DisplayMenu(MenuValues: TVonArraySetting;
      ImgLarge, ImgSmall: TImageList); override;
  end;

var
  FPlatformMenu1: TFPlatformMenu1;

implementation

{$R *.dfm}

{ TFPlatformMenu1 }

procedure TFPlatformMenu1.DisplayChildren(ParentMenu: string);
var
  i: Integer;
  szName: string;
  pMenu: TPanel;
begin
  plParent.Visible := ParentMenu <> '';
  FCurrentParent := ParentMenu;
  FListParent.Clear;
  for i := 0 to FMenuValues.Count - 1 do
    if SameText(FMenuValues[i, 1], ParentMenu) then
    begin
      pMenu := TPanel.Create(nil);
      pMenu.Top := 2000;
      pMenu.Height := plParent.Height;
      pMenu.Align := alTop;
      pMenu.OnClick := EventGroupClick;
      pMenu.Caption := FMenuValues[i, 0];
      pMenu.Hint := FMenuValues[i, 8];
      pMenu.Parent := Self;
      FListParent.Save(FMenuValues[i, 0], FMenuValues[i, 3], pMenu, Pointer(i));
    end;
  i:= 0;
  if FListParent.Count > 0 then begin
    repeat
      pMenu:= TPanel(FListParent.Objects[i]);
      EventGroupClick(pMenu);
      Inc(i);
    until pMenu.Visible or(i >= FListParent.Count);
  end;
end;

procedure TFPlatformMenu1.DisplayMenu(MenuValues: TVonArraySetting;
  ImgLarge, ImgSmall: TImageList);
begin // 菜单信息 <MenuName><MemuParentName><ImageID><UsingName><Controller><ShortKey><RunType><Params><Hint>
  inherited;
  FMenuValues := MenuValues;
  lvMenu.LargeImages := ImgLarge;
  DisplayChildren('');
end;

procedure TFPlatformMenu1.EventGroupClick(Sender: TObject);
var
  i, Idx: Integer;
  szS: string;
  newItem: TListItem;
begin
  Idx := FListParent.IndexOfObject(Sender);
  if Idx < 0 then
    Exit;
  for i := FListParent.Count - 1 downto Idx do
    TPanel(FListParent.Objects[i]).Align := alBottom;
  for i := 0 to Idx do
    TPanel(FListParent.Objects[i]).Align := alTop;
  szS := TPanel(Sender).Caption;
  lvMenu.Clear;
  for i := 0 to FMenuValues.Count - 1 do
    if SameText(FMenuValues[i, 1], szS) then
    begin
      if Assigned(LoginInfo)and not                        //权限判定
        LoginInfo.CheckRight(FMenuValues.Values[i, 3],
          StrToInt(FMenuValues.Values[i, 4]))then Continue;
      newItem := lvMenu.Items.Add;
      newItem.Caption := FMenuValues[i, 0];
      newItem.ImageIndex := StrToInt(FMenuValues[i, 2]);
      newItem.SubItems.Add(FMenuValues[i, 7]);
      newItem.StateIndex := i;
    end;
  TPanel(Sender).Visible:= lvMenu.Items.Count > 0;
end;

procedure TFPlatformMenu1.FormCreate(Sender: TObject);
begin
  inherited;
  FListParent := TVonNameValueData.Create;
  FListParent.AutoFreeObject := True;
end;

procedure TFPlatformMenu1.FormDestroy(Sender: TObject);
begin
  inherited;
  FListParent.Free;
end;

procedure TFPlatformMenu1.lvMenuClick(Sender: TObject);
begin
  inherited;
  if not Assigned(lvMenu.Selected) then
    Exit;
  if Assigned(OnClick) then
    OnClick(lvMenu.Selected.Caption,
      FMenuValues.Values[lvMenu.Selected.StateIndex, 3],
      StrToInt(FMenuValues.Values[lvMenu.Selected.StateIndex, 4]),
      StrToInt(FMenuValues.Values[lvMenu.Selected.StateIndex, 6]),
      FMenuValues.Values[lvMenu.Selected.StateIndex, 7]);
end;

initialization
  //WriteLog(LOG_DEBUG, 'TFPlatformMenu1', 'RegMenu 大图列表');
  RegMenu('MENU_0', '大图列表', TFPlatformMenu1);

finalization

end.
