unit UPlatformMenu2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UPlatformMenuBase, ComCtrls, UPlatformDockForm, UVonClass, ExtCtrls,
  Buttons, ImgList, ToolWin, Menus, UPlatformDB, Vcl.ButtonGroup, UVonLog;

type
  TFPlatformMenu2 = class(TFPlatformMenuBase)
    ButtonGroup1: TButtonGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MenuClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FMenuValues: TVonArraySetting;
    FMenu: TMainMenu;
    procedure DisplayWinMenu();
      overload;
    function CreateAMenuItem(IDX: Integer): TMenuItem;
  public
    { Public declarations }
    procedure DisplayMenu(MenuValues: TVonArraySetting;
      ImgLarge, ImgSmall: TImageList); override;
  end;

var
  FPlatformMenu2: TFPlatformMenu2;

implementation

{$R *.dfm}
{ TFPlatformMenu2 }

procedure TFPlatformMenu2.DisplayWinMenu;
  procedure AddSubMenu(item: TMenuItem; ParentName: string);
  var
    I: Integer;
    newItem: TMenuItem;
  begin
    for i := 0 to FMenuValues.Count - 1 do
      if SameText(FMenuValues[i, 1], ParentName) then
      begin
        newItem:= CreateAMenuItem(I);
        if not Assigned(item) then FMenu.Items.Add(newItem)
        else item.Add(newItem);
        AddSubMenu(newItem, FMenuValues[I, 0]);
      end;
//    if Assigned(newItem)and then

  end;
begin
  AddSubMenu(nil, '');
end;

function TFPlatformMenu2.CreateAMenuItem(IDX: Integer): TMenuItem;
begin
  Result := TMenuItem.Create(nil);
  Result.Caption := FMenuValues[IDX, 0];
  Result.Hint := FMenuValues[IDX, 8];
  Result.ImageIndex := StrToInt(FMenuValues[IDX, 2]);
  Result.DesignInfo := IDX;
  Result.OnClick := MenuClick;
end;

procedure TFPlatformMenu2.DisplayMenu(MenuValues: TVonArraySetting;
  ImgLarge, ImgSmall: TImageList);
begin // 菜单信息 <MenuName><MemuParentName><ImageID><UsingName><Controller><ShortKey><RunType><Params><Hint>
  inherited;
  FMenuValues := MenuValues;
  FMenu.Images := ImgSmall;
  //DisplayRootMenu;
  DisplayWinMenu;
end;

procedure TFPlatformMenu2.FormCreate(Sender: TObject);
begin
  inherited;
  FMenu:= TMainMenu.Create(nil);
end;

procedure TFPlatformMenu2.FormDestroy(Sender: TObject);
begin
  inherited;
  //FMenu.Free;
end;

procedure TFPlatformMenu2.FormShow(Sender: TObject);
begin
  inherited;
  if Assigned(Application.MainForm) then begin
    Application.MainForm.Menu:= FMenu;
    Self.Parent.Hide;
  end;
end;

procedure TFPlatformMenu2.MenuClick(Sender: TObject);
begin
  inherited;
  with TMenuItem(Sender) do
    if Assigned(OnClick) then
      self.OnClick(Caption, FMenuValues.Values[DesignInfo, 3],
        StrToInt(FMenuValues.Values[DesignInfo, 4]),
        StrToInt(FMenuValues.Values[DesignInfo, 6]),
        FMenuValues.Values[DesignInfo, 7]);
end;

initialization
  //WriteLog(LOG_DEBUG, 'TFPlatformMenu2', 'RegMenu 传统菜单');
  RegMenu('MENU_1', '传统菜单', TFPlatformMenu2);

finalization

end.
