unit UPlatformMenu3;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UPlatformMenuBase, ComCtrls, UPlatformDockForm, UVonClass, ExtCtrls,
  Buttons, ImgList, CategoryButtons, UVonLog;

type
  TFPlatformMenu3 = class(TFPlatformMenuBase)
    CategoryButtons1: TCategoryButtons;
    procedure CategoryButtons1ButtonClicked(Sender: TObject;
      const Button: TButtonItem);
    procedure CategoryButtons1SelectedCategoryChange(Sender: TObject;
      const Category: TButtonCategory);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FMenuValues: TVonArraySetting;
    FCurrentParent: string;
    FSmallImgs: TImageList;
    procedure DisplayRootMenu();
    procedure DisplayChildren(tmpMenu: TButtonCategory; ParentMenu: string);
  public
    { Public declarations }
    procedure DisplayMenu(MenuValues: TVonArraySetting;
      ImgLarge, ImgSmall: TImageList); override;
  end;

var
  FPlatformMenu3: TFPlatformMenu3;

implementation

uses UPlatformMain;

{$R *.dfm}

{ TFPlatformMenu3 }

procedure TFPlatformMenu3.DisplayRootMenu;
var
  i: Integer;
  pMenu: TButtonCategory;
begin
  CategoryButtons1.Categories.Clear;;
  for i := 0 to FMenuValues.Count - 1 do
    if SameText(FMenuValues[i, 1], '') then begin
      pMenu:= CategoryButtons1.Categories.Add;
      pMenu.Caption:= FMenuValues[i, 0];
      pMenu.Collapsed:= i > 0;
      DisplayChildren(pMenu, FMenuValues[i, 0]);
    end;
end;

procedure TFPlatformMenu3.FormCreate(Sender: TObject);
begin
  inherited;
  Color:= FPlatformMain.Color;
  Font:= FPlatformMain.Font;
end;

procedure TFPlatformMenu3.CategoryButtons1ButtonClicked(Sender: TObject;
  const Button: TButtonItem);
begin
  inherited;
  if Assigned(OnClick) then
    with Button do
      self.OnClick(Caption, FMenuValues.Values[Integer(Data), 3],
        StrToInt(FMenuValues.Values[Integer(Data), 4]),
        StrToInt(FMenuValues.Values[Integer(Data), 6]),
        FMenuValues.Values[Integer(Data), 7]);
end;

procedure TFPlatformMenu3.CategoryButtons1SelectedCategoryChange(
  Sender: TObject; const Category: TButtonCategory);
var
  I, Idx: Integer;
begin
  inherited;
  Idx:= CategoryButtons1.Categories.IndexOf(Category.Caption);
  for I := 0 to Idx - 1 do
    CategoryButtons1.Categories[I].Collapsed:= true;
  for I := Idx + 1 to CategoryButtons1.Categories.Count - 1 do
    CategoryButtons1.Categories[I].Collapsed:= true;
  CategoryButtons1.Categories[Idx].Collapsed:= False;
end;

procedure TFPlatformMenu3.DisplayChildren(tmpMenu: TButtonCategory;
  ParentMenu: string);
var
  i: Integer;
begin
  tmpMenu.Items.Clear;
  for i := 0 to FMenuValues.Count - 1 do
    if SameText(FMenuValues[i, 1], ParentMenu) then
      if Assigned(LoginInfo)and not                      //权限判定
        LoginInfo.CheckRight(FMenuValues.Values[i, 3],
          StrToInt(FMenuValues.Values[i, 4]))then Continue
      else with tmpMenu.Items.Add do begin
        Caption:= FMenuValues[i, 0];
        ImageIndex:= StrToInt(FMenuValues[i, 2]);
        Data:= TObject(I);
      end;
end;

procedure TFPlatformMenu3.DisplayMenu(MenuValues: TVonArraySetting;
  ImgLarge, ImgSmall: TImageList);
begin // 菜单信息 <MenuName><MemuParentName><ImageID><UsingName><Controller><ShortKey><RunType><Params><Hint>
  inherited;
  FMenuValues := MenuValues;
  FSmallImgs := ImgSmall;
  CategoryButtons1.Images := ImgSmall;
  DisplayRootMenu;
  Font.Color:= clBlack;
end;

initialization
  //WriteLog(LOG_DEBUG, 'TFPlatformMenu3', 'RegMenu 小图列表');
  RegMenu('MENU_2', '小图列表', TFPlatformMenu3);

finalization

end.
