unit UPlatformMenuBase;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UPlatformDockForm, ComCtrls, UVonClass, UPlatformInfo;

type
  TEventOfMenuClick = procedure(MenuName, UsingName: string;
    ControlID, RunType: Integer; ExecParam: string) of object;

  // <MenuName><MemuParentName><ImageID><UsingName><Controller><ShortKey><Params><Hint>
  TFPlatformMenuBase = class(TFPlatformDockForm)
  private
    { Private declarations }
    FOnClick: TEventOfMenuClick;
    FMenuStyle: Integer;
    FLoginInfo: TLoginInfo;
    procedure SetOnClick(const Value: TEventOfMenuClick);
    procedure SetMenuStyle(const Value: Integer);
    procedure SetLoginInfo(const Value: TLoginInfo);
  public
    { Public declarations }
    procedure DisplayMenu(MenuValues: TVonArraySetting;
      ImgLarge, ImgSmall: TImageList); virtual; abstract;
  published
    property Font;
    property Color;
    property MenuStyle: Integer read FMenuStyle write SetMenuStyle;
    property OnClick: TEventOfMenuClick read FOnClick write SetOnClick;
    property LoginInfo: TLoginInfo read FLoginInfo write SetLoginInfo;
  end;

function RegMenu(AName, ANote: string; AClass: TFormClass): Integer;

var
  FPlatformMenuBase: TFPlatformMenuBase;

implementation

function RegMenu(AName, ANote: string; AClass: TFormClass): Integer;
begin
  Result := RegDockForm(AName, ANote, AClass);
end;

{$R *.dfm}
{ TFPlatformMenuBase }

procedure TFPlatformMenuBase.SetLoginInfo(const Value: TLoginInfo);
begin
  FLoginInfo := Value;
end;

procedure TFPlatformMenuBase.SetMenuStyle(const Value: Integer);
begin
  FMenuStyle := Value;
end;

procedure TFPlatformMenuBase.SetOnClick(const Value: TEventOfMenuClick);
begin
  FOnClick := Value;
end;

end.
