unit UPlatformOptions;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, ExtCtrls, UPlatformDB, DB, Data.Win.ADODB, Grids, DBGrids, ComCtrls,
  StdCtrls, Spin, UFrameImageInput, ValEdit, UPlatformInfo, UVonLog,
  UFrameRichEditor, UVonSystemFuns, ImgList, ButtonGroup, System.ImageList;

resourcestring
  REL_OPTIONS_DEL = '删除系统选项%s';

type
  TFPlatformOptions = class(TForm)
    Panel1: TPanel;
    DSSysParam: TDataSource;
    Panel2: TPanel;
    BtnSave: TBitBtn;
    BtnDelete: TBitBtn;
    BitBtn3: TBitBtn;
    DQOptionNames: TADOQuery;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    EText: TMemo;
    EList: TValueListEditor;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    EOrgFileName: TEdit;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    EExportFilename: TEdit;
    SpeedButton2: TSpeedButton;
    Label2: TLabel;
    TabSheet5: TTabSheet;
    EOptionName: TEdit;
    Label3: TLabel;
    Panel3: TPanel;
    Label4: TLabel;
    ETextTitle: TEdit;
    Panel4: TPanel;
    Label5: TLabel;
    EListName: TEdit;
    FrameRichEditor1: TFrameRichEditor;
    Panel5: TPanel;
    Label6: TLabel;
    ERichName: TEdit;
    FrameImageInput1: TFrameImageInput;
    OpenDialog1: TOpenDialog;
    SpeedButton3: TSpeedButton;
    SaveDialog1: TSaveDialog;
    DQOption: TADOQuery;
    lstOption: TButtonGroup;
    imgOption: TImageList;
    btnExport: TBitBtn;
    btnImport: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure lstOptionButtonClicked(Sender: TObject; Index: Integer);
    procedure btnExportClick(Sender: TObject);
    procedure btnImportClick(Sender: TObject);
  private
    { Private declarations }
    FInfo: TOptionsInfo;
    FCurrentIdx: Integer;
  public
    { Public declarations }
  end;

var
  FPlatformOptions: TFPlatformOptions;

implementation

uses Masks;

{$R *.dfm}

procedure TFPlatformOptions.FormCreate(Sender: TObject);
begin // 初始化
  lstOption.Items.Clear;
  with DQOptionNames do
  begin
    Connection:= FPlatformDB.ADOConn;
    Open;
    while not EOF do
    begin
      with lstOption.Items.Add do
      begin
        Data := Pointer(FieldByName('ID').AsInteger);
        Caption := FieldByName('OptName').AsString;
        ImageIndex := FieldByName('OptKind').AsInteger;
      end;
      Next;
    end;
  end;
  FInfo := TOptionsInfo.Create;
  FInfo.Conn := FPlatformDB.ADOConn;
  FCurrentIdx := -1;
end;

procedure TFPlatformOptions.FormDestroy(Sender: TObject);
begin
  FInfo.Free;
end;

procedure TFPlatformOptions.btnExportClick(Sender: TObject);
begin
  if not SaveDialog1.Execute then Exit;
  case PageControl1.ActivePageIndex of
    0: EText.Lines.SaveToFile(SaveDialog1.FileName);
    1: EList.Strings.SaveToFile(SaveDialog1.FileName);
    2: FrameImageInput1.Bitmap.SaveToFile(SaveDialog1.FileName);
    3: CopyFile(PChar(EOrgFileName.Text), PChar(SaveDialog1.FileName), False);
    4: FrameRichEditor1.Editor.Lines.SaveToFile(SaveDialog1.FileName);
  end;
end;

procedure TFPlatformOptions.btnImportClick(Sender: TObject);
begin
  if not OpenDialog1.Execute then Exit;
  case PageControl1.ActivePageIndex of
    0: EText.Lines.LoadFromFile(OpenDialog1.FileName);
    1: EList.Strings.LoadFromFile(OpenDialog1.FileName);
    2: FrameImageInput1.Bitmap.LoadFromFile(OpenDialog1.FileName);
    4: FrameRichEditor1.Editor.Lines.LoadFromFile(OpenDialog1.FileName);
  end;
end;

procedure TFPlatformOptions.BtnSaveClick(Sender: TObject);
var
  FS: TFileStream;
begin // 保存参数
  FInfo.OptName := EOptionName.Text;
  FInfo.OptKind := PageControl1.ActivePageIndex;
  case FInfo.OptKind of
    0:
      FInfo.OptInfo := ETextTitle.Text;
    1:
      FInfo.OptInfo := EListName.Text;
    2:
      FInfo.OptInfo := FrameImageInput1.InputStr;
    3:
      FInfo.OptInfo := EOrgFileName.Text;
    4:
      FInfo.OptInfo := ERichName.Text;
  end;
  case FInfo.SaveToDB of
    - 1:
      raise Exception.Create(LastInfo);
    0:
      with lstOption.Items[FCurrentIdx] do
      begin
        Caption := EOptionName.Text;
        ImageIndex := FInfo.OptKind;
      end;
    1:
      with lstOption.Items.Add do
      begin
        Data := Pointer(ID);
        Caption := EOptionName.Text;
        ImageIndex := FInfo.OptKind;
        FCurrentIdx := Index;
      end;
  end;
  case FInfo.OptKind of
    0:
      FInfo.SetContent(EText.Lines);
    1:
      FInfo.SetContent(EList.Strings);
    2:
      FInfo.SetContent(FrameImageInput1.Bitmap);
    3:
      begin
        FS := TFileStream.Create(EOrgFileName.Text, fmOpenRead);
        FS.Position := 0;
        FInfo.SetContent(FS);
        FS.Free;
      end;
    4:
      FInfo.SetContent(FrameRichEditor1.Editor.Lines);
  end;
end;

procedure TFPlatformOptions.BtnDeleteClick(Sender: TObject);
begin // 删除参数
  if FCurrentIdx < 0 then
    raise Exception.Create(RES_NODE_NONESELECTED);
  if MessageDlg(RES_DLG_DEL_PROMPT, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  if MessageDlg(RES_DLG_DEL_WARNING, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  WriteLog(LOG_MAJOR, 'Options_Delete', Format(REL_OPTIONS_DEL,
    [FInfo.OptName]));
  with DQOption do
    try
      Connection:= FPlatformDB.ADOConn;
      Parameters[0].Value := FInfo.OptName;
      Open;
      if IsEmpty then
        Exit;
      Delete;
      lstOption.Items.Delete(FCurrentIdx);
    finally
      Close;
    end;
  FCurrentIdx := -1;
end;

procedure TFPlatformOptions.lstOptionButtonClicked(Sender: TObject;
  Index: Integer);
begin // 双击提取
  FCurrentIdx := Index;
  FInfo.LoadFromDB(lstOption.Items[Index].Caption);
  EOptionName.Text := FInfo.OptName;
  PageControl1.ActivePageIndex := FInfo.OptKind;
  case FInfo.OptKind of
    0:
      begin
        ETextTitle.Text := FInfo.OptInfo;
        FInfo.GetContent(EText.Lines);
      end;
    1:
      begin
        EListName.Text := FInfo.OptInfo;
        FInfo.GetContent(EList.Strings);
      end;
    2:
      begin
        FrameImageInput1.InputStr := FInfo.OptInfo;
        FInfo.GetContent(FrameImageInput1.Bitmap);
      end;
    3:
      EOrgFileName.Text := FInfo.OptInfo;
    4:
      begin
        ERichName.Text := FInfo.OptInfo;
        FInfo.GetContent(FrameRichEditor1.Editor.Lines);
      end;
  end;
end;

procedure TFPlatformOptions.SpeedButton1Click(Sender: TObject);
begin
  with OpenDialog1 do
    if Execute then
      EOrgFileName.Text := FileName;
end;

procedure TFPlatformOptions.SpeedButton3Click(Sender: TObject);
begin
  with SaveDialog1 do
    if Execute then
      EExportFilename.Text := FileName;
end;

procedure TFPlatformOptions.SpeedButton2Click(Sender: TObject);
var
  FS: TFileStream;
begin
  FS := TFileStream.Create(EExportFilename.Text, fmCreate);
  FInfo.GetContent(FS);
  FS.Free;
end;

end.
