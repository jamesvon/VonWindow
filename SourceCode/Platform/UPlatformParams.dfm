object FPlatformParams: TFPlatformParams
  Left = 0
  Top = 0
  Caption = 'FPlatformParams'
  ClientHeight = 508
  ClientWidth = 716
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pcKind: TPageControl
    Left = 0
    Top = 0
    Width = 716
    Height = 508
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    OnChange = pcKindChange
    object tsSystem: TTabSheet
      Caption = #31995#32479#21442#25968
      ImageIndex = 1
      ExplicitLeft = 8
      ExplicitTop = 28
      ExplicitWidth = 656
      ExplicitHeight = 362
      object lstSystem: TListBox
        Left = 0
        Top = 0
        Width = 233
        Height = 480
        Align = alLeft
        ItemHeight = 13
        TabOrder = 0
        OnClick = lstSystemClick
      end
      object Panel3: TPanel
        Left = 233
        Top = 0
        Width = 185
        Height = 480
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'Panel3'
        ShowCaption = False
        TabOrder = 1
        ExplicitLeft = 239
        ExplicitTop = 3
        ExplicitHeight = 362
        object lbSysIntValue: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 141
          Width = 48
          Height = 13
          Align = alTop
          Caption = #25972#25968#21442#25968
        end
        object lbSysHeader: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 49
          Width = 48
          Height = 13
          Align = alTop
          Caption = #21442#25968#21069#32512
        end
        object lbSysTrailler: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 234
          Width = 48
          Height = 13
          Align = alTop
          Caption = #21442#25968#21518#32512
        end
        object lbSysStrValue: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 95
          Width = 48
          Height = 13
          Align = alTop
          Caption = #25991#23383#21442#25968
        end
        object lbSysFloatValue: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 188
          Width = 48
          Height = 13
          Align = alTop
          Caption = #28014#28857#21442#25968
        end
        object lbSysName: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 48
          Height = 13
          Align = alTop
          Caption = #21442#25968#21517#31216
        end
        object ESysName: TEdit
          AlignWithMargins = True
          Left = 3
          Top = 22
          Width = 179
          Height = 21
          Align = alTop
          TabOrder = 0
          ExplicitLeft = 6
          ExplicitTop = 23
          ExplicitWidth = 177
        end
        object ESysStrValue: TEdit
          AlignWithMargins = True
          Left = 3
          Top = 114
          Width = 179
          Height = 21
          Align = alTop
          TabOrder = 1
          ExplicitLeft = 6
          ExplicitTop = 161
          ExplicitWidth = 177
        end
        object ESysIntValue: TSpinEdit
          AlignWithMargins = True
          Left = 3
          Top = 160
          Width = 179
          Height = 22
          Align = alTop
          MaxValue = 0
          MinValue = 0
          TabOrder = 2
          Value = 0
          ExplicitLeft = 6
          ExplicitTop = 198
          ExplicitWidth = 177
        end
        object btnSysSave: TButton
          AlignWithMargins = True
          Left = 3
          Top = 280
          Width = 179
          Height = 25
          Align = alTop
          Caption = #20445#23384
          ImageIndex = 29
          ImageMargins.Left = 5
          TabOrder = 3
          OnClick = btnSysSaveClick
          ExplicitLeft = 6
          ExplicitTop = 331
          ExplicitWidth = 177
        end
        object ESysHeader: TEdit
          AlignWithMargins = True
          Left = 3
          Top = 68
          Width = 179
          Height = 21
          Align = alTop
          TabOrder = 4
          ExplicitLeft = 6
          ExplicitTop = 115
          ExplicitWidth = 177
        end
        object ESysTrailler: TEdit
          AlignWithMargins = True
          Left = 3
          Top = 253
          Width = 179
          Height = 21
          Align = alTop
          TabOrder = 5
          ExplicitLeft = 6
          ExplicitTop = 299
          ExplicitWidth = 177
        end
        object ESysFloatValue: TEdit
          AlignWithMargins = True
          Left = 3
          Top = 207
          Width = 179
          Height = 21
          Align = alTop
          TabOrder = 6
          ExplicitLeft = 6
          ExplicitTop = 253
          ExplicitWidth = 177
        end
      end
    end
    object tsTicketNo: TTabSheet
      Caption = #32534#30721#31649#29702
      ExplicitLeft = 8
      ExplicitTop = 28
      ExplicitWidth = 1051
      ExplicitHeight = 362
      object lstTicket: TListBox
        Left = 0
        Top = 0
        Width = 233
        Height = 480
        Align = alLeft
        ItemHeight = 13
        TabOrder = 0
        OnClick = lstTicketClick
      end
      object GridPanel1: TGridPanel
        Left = 233
        Top = 0
        Width = 411
        Height = 480
        Align = alLeft
        Caption = 'GridPanel1'
        ColumnCollection = <
          item
            SizeStyle = ssAuto
            Value = 46.339326574668900000
          end
          item
            SizeStyle = ssAuto
            Value = 15.555856672006040000
          end
          item
            SizeStyle = ssAuto
            Value = 52.137218137746600000
          end
          item
            SizeStyle = ssAuto
            Value = 100.000000000000000000
          end>
        ControlCollection = <
          item
            Column = 0
            Control = Label2
            Row = 0
          end
          item
            Column = 0
            Control = Label7
            Row = 2
          end
          item
            Column = 0
            Control = Label8
            Row = 4
          end
          item
            Column = 0
            Control = Label9
            Row = 6
          end
          item
            Column = 0
            Control = Label10
            Row = 8
          end
          item
            Column = 0
            Control = Label5
            Row = 10
          end
          item
            Column = 2
            Control = Label3
            Row = 10
          end
          item
            Column = 3
            Control = Label4
            Row = 10
          end
          item
            Column = 0
            Control = ECodeFormat
            Row = 11
          end
          item
            Column = 1
            Control = SpeedButton1
            Row = 11
          end
          item
            Column = 2
            Control = EFmtDate
            Row = 11
          end
          item
            Column = 3
            Control = EFmtLen
            Row = 11
          end
          item
            Column = 0
            Control = ETicketTrailler
            Row = 9
          end
          item
            Column = 0
            Control = EKind
            Row = 7
          end
          item
            Column = 0
            Control = ETicketNo
            Row = 5
          end
          item
            Column = 0
            Control = ETicketName
            Row = 1
          end
          item
            Column = 0
            Control = ETicketHeader
            Row = 3
          end
          item
            Column = 0
            Control = btnSaveTicket
            Row = 12
          end
          item
            Column = 0
            Control = btnDel
            Row = 13
          end>
        RowCollection = <
          item
            SizeStyle = ssAuto
            Value = 6.086160918021251000
          end
          item
            SizeStyle = ssAbsolute
            Value = 28.000000000000000000
          end
          item
            SizeStyle = ssAuto
            Value = 6.286817572945048000
          end
          item
            SizeStyle = ssAbsolute
            Value = 28.000000000000000000
          end
          item
            SizeStyle = ssAuto
            Value = 7.675696994803357000
          end
          item
            SizeStyle = ssAbsolute
            Value = 28.000000000000000000
          end
          item
            SizeStyle = ssAuto
            Value = 9.585699008653647000
          end
          item
            SizeStyle = ssAbsolute
            Value = 28.000000000000000000
          end
          item
            SizeStyle = ssAuto
            Value = 12.381650647695490000
          end
          item
            SizeStyle = ssAbsolute
            Value = 28.000000000000000000
          end
          item
            SizeStyle = ssAuto
            Value = 16.899633681964950000
          end
          item
            SizeStyle = ssAbsolute
            Value = 32.000000000000000000
          end
          item
            SizeStyle = ssAbsolute
            Value = 28.000000000000000000
          end
          item
            SizeStyle = ssAbsolute
            Value = 32.000000000000000000
          end
          item
            SizeStyle = ssAbsolute
            Value = 28.000000000000000000
          end
          item
            Value = 100.000000000000000000
          end>
        ShowCaption = False
        TabOrder = 1
        ExplicitLeft = 637
        ExplicitHeight = 362
        DesignSize = (
          411
          480)
        object Label2: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 183
          Height = 17
          Align = alClient
          Caption = #32534#30721#21517#31216
          ExplicitWidth = 48
          ExplicitHeight = 13
        end
        object Label7: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 55
          Width = 183
          Height = 15
          Align = alClient
          Caption = #32534#30721#21069#32512
          ExplicitWidth = 48
          ExplicitHeight = 13
        end
        object Label8: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 104
          Width = 183
          Height = 17
          Align = alClient
          Caption = #24403#21069#24207#21495
          ExplicitWidth = 48
          ExplicitHeight = 13
        end
        object Label9: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 155
          Width = 183
          Height = 18
          Align = alClient
          Caption = #32534#30721#21407#21017
          ExplicitWidth = 48
          ExplicitHeight = 13
        end
        object Label10: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 207
          Width = 183
          Height = 18
          Align = alClient
          Caption = #32534#30721#21518#32512
          ExplicitWidth = 48
          ExplicitHeight = 13
        end
        object Label5: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 259
          Width = 183
          Height = 19
          Align = alClient
          Caption = #32534#30721#26684#24335
          ExplicitWidth = 48
          ExplicitHeight = 13
        end
        object Label3: TLabel
          AlignWithMargins = True
          Left = 216
          Top = 259
          Width = 96
          Height = 19
          Align = alClient
          Caption = #32534#30721#26684#24335
          ExplicitWidth = 48
          ExplicitHeight = 13
        end
        object Label4: TLabel
          AlignWithMargins = True
          Left = 318
          Top = 259
          Width = 89
          Height = 19
          Align = alClient
          Caption = #32534#30721#38271#24230
          ExplicitWidth = 48
          ExplicitHeight = 13
        end
        object ECodeFormat: TComboBox
          AlignWithMargins = True
          Left = 4
          Top = 284
          Width = 183
          Height = 26
          Align = alClient
          ItemIndex = 0
          TabOrder = 0
          Text = '%.6d'
          Items.Strings = (
            '%.6d'
            '%1:4d%.6d'
            '%2:2d%.6d'
            '%2:2d%3:2d%.6d')
        end
        object SpeedButton1: TSpeedButton
          Left = 190
          Top = 286
          Width = 23
          Height = 21
          Anchors = []
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF4CB122FF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFCC
            483F4CB122FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFCC483FBE92704CB122FF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFCC483FBE9270CC
            483F4CB1224CB1224CB1224CB1224CB1224CB1224CB122FF00FFFF00FFFF00FF
            FF00FFFF00FFCC483FBE9270BE9270CC483FCC483FCC483FCC483FCC483FCC48
            3FCC483F4CB1224CB122FF00FFFF00FFFF00FFCC483FBE9270BE9270BE9270BE
            9270BE9270BE9270BE9270BE9270BE9270CC483FCC483F4CB122FF00FFFF00FF
            CC483FBE9270EAD999EAD999EAD999EAD999EAD999EAD999EAD999EAD999EAD9
            9980FF8080FF804CB122FF00FFCC483FBE9270EAD99980FF8080FF8080FF8080
            FF8080FF8080FF8080FF8080FF8080FF8080FF8080FF804CB12280FF80FFFFFF
            EAD99980FF8080FF8080FF8080FF8080FF8080FF8080FF8080FF8080FF8080FF
            8080FF8080FF804CB122FF00FF80FF80FFFFFF80FF8080FF8080FF8080FF8080
            FF8080FF8080FF8080FF8080FF8080FF8080FF8080FF804CB122FF00FFFF00FF
            80FF80FFFFFF80FF8080FF8080FF8080FF8080FF8080FF8080FF8080FF8080FF
            8080FF8080FF804CB122FF00FFFF00FFFF00FF80FF80FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80FF80FF00FFFF00FFFF00FF
            FF00FFFF00FF80FF80FFFFFF80FF8080FF8080FF8080FF8080FF8080FF8080FF
            8080FF80FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF80FF80FFFFFF80
            FF804CB122FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FF80FF80FFFFFF4CB122FF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF80
            FF80FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
          OnClick = SpeedButton1Click
          ExplicitLeft = 158
          ExplicitTop = 1
        end
        object EFmtDate: TComboBox
          AlignWithMargins = True
          Left = 216
          Top = 284
          Width = 96
          Height = 26
          Align = alClient
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 1
          Text = #26080#24180#26376
          Items.Strings = (
            #26080#24180#26376
            #38271#24180#20998
            #30701#24180#20221
            #38271#24180#26376
            #30701#24180#26376
            #21547#26376#20221
            #24180#26376#26085
            #21547#26376#26085)
        end
        object EFmtLen: TComboBox
          AlignWithMargins = True
          Left = 318
          Top = 284
          Width = 89
          Height = 26
          Align = alClient
          ItemIndex = 4
          TabOrder = 2
          Text = '6'#20301
          Items.Strings = (
            '2'#20301
            '3'#20301
            '4'#20301
            '5'#20301
            '6'#20301
            '7'#20301
            '8'#20301
            '9'#20301
            '10'#20301
            '11'#20301
            '12'#20301)
        end
        object ETicketTrailler: TEdit
          AlignWithMargins = True
          Left = 4
          Top = 231
          Width = 183
          Height = 22
          Align = alClient
          TabOrder = 3
          ExplicitLeft = 117
          ExplicitTop = 1
          ExplicitWidth = 105
          ExplicitHeight = 21
        end
        object EKind: TComboBox
          AlignWithMargins = True
          Left = 4
          Top = 179
          Width = 183
          Height = 22
          Align = alClient
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 4
          Text = #36830#32493
          Items.Strings = (
            #36830#32493
            #24180#36830#32493
            #26376#36830#32493
            #21608#36830#32493
            #26085#36830#32493)
        end
        object ETicketNo: TSpinEdit
          AlignWithMargins = True
          Left = 4
          Top = 127
          Width = 183
          Height = 22
          Align = alClient
          MaxValue = 0
          MinValue = 0
          TabOrder = 5
          Value = 0
          ExplicitLeft = 117
          ExplicitTop = 1
          ExplicitWidth = 105
        end
        object ETicketName: TEdit
          AlignWithMargins = True
          Left = 4
          Top = 27
          Width = 183
          Height = 22
          Align = alClient
          TabOrder = 6
          ExplicitLeft = 117
          ExplicitTop = 1
          ExplicitWidth = 105
          ExplicitHeight = 21
        end
        object ETicketHeader: TEdit
          AlignWithMargins = True
          Left = 4
          Top = 76
          Width = 183
          Height = 22
          Align = alClient
          TabOrder = 7
          ExplicitLeft = 117
          ExplicitTop = 1
          ExplicitWidth = 105
          ExplicitHeight = 21
        end
        object btnSaveTicket: TButton
          AlignWithMargins = True
          Left = 80
          Top = 316
          Width = 107
          Height = 22
          Align = alRight
          Caption = #20445#23384
          ImageIndex = 29
          ImageMargins.Left = 5
          TabOrder = 8
          OnClick = btnSaveTicketClick
          ExplicitLeft = 4
          ExplicitTop = 295
          ExplicitHeight = 33
        end
        object btnDel: TButton
          AlignWithMargins = True
          Left = 80
          Top = 344
          Width = 107
          Height = 26
          Align = alRight
          Caption = #21024#38500
          ImageIndex = 20
          ImageMargins.Left = 5
          TabOrder = 9
          OnClick = btnDelClick
          ExplicitLeft = 4
          ExplicitTop = 320
          ExplicitHeight = 33
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = #24207#21015#31649#29702
      ImageIndex = 2
      ExplicitLeft = 8
      ExplicitTop = 48
      ExplicitWidth = 0
      ExplicitHeight = 0
      object lstSerial: TListBox
        Left = 0
        Top = 0
        Width = 233
        Height = 480
        Align = alLeft
        ItemHeight = 13
        TabOrder = 0
        OnClick = lstSerialClick
      end
      object vlSerial: TValueListEditor
        Left = 335
        Top = 0
        Width = 373
        Height = 480
        Align = alRight
        Anchors = [akLeft, akTop, akRight, akBottom]
        KeyOptions = [keyEdit, keyAdd, keyDelete, keyUnique]
        TabOrder = 1
        TitleCaptions.Strings = (
          #31995#21015#21517
          #31995#21015#20540)
        ColWidths = (
          150
          217)
        RowHeights = (
          18
          18)
      end
      object btnSerialAdd: TButton
        Left = 239
        Top = 67
        Width = 90
        Height = 25
        Caption = #20445#23384
        ImageIndex = 29
        ImageMargins.Left = 5
        TabOrder = 2
        OnClick = btnSerialAddClick
      end
      object btnSerialDel: TButton
        Left = 239
        Top = 98
        Width = 90
        Height = 25
        Caption = #21024#38500
        ImageIndex = 20
        ImageMargins.Left = 5
        TabOrder = 3
        OnClick = btnSerialDelClick
      end
      object ESerialName: TLabeledEdit
        Left = 239
        Top = 19
        Width = 90
        Height = 21
        EditLabel.Width = 48
        EditLabel.Height = 13
        EditLabel.Caption = #31995#21015#21517#31216
        TabOrder = 4
      end
    end
    object TabSheet2: TTabSheet
      Caption = #36816#34892#21442#25968
      ImageIndex = 3
      object Splitter1: TSplitter
        Left = 233
        Top = 0
        Height = 480
        ExplicitLeft = 264
        ExplicitTop = 136
        ExplicitHeight = 100
      end
      object lstRuntime: TListBox
        Left = 0
        Top = 0
        Width = 233
        Height = 480
        Align = alLeft
        ItemHeight = 13
        TabOrder = 0
        OnClick = lstRuntimeClick
      end
      object Panel2: TPanel
        Left = 236
        Top = 0
        Width = 472
        Height = 480
        Align = alClient
        Caption = 'Panel2'
        TabOrder = 1
        inline FrameEnterForm1: TFrameEnterForm
          Left = 1
          Top = 1
          Width = 470
          Height = 436
          Align = alClient
          TabOrder = 1
          ExplicitLeft = 1
          ExplicitTop = 1
          ExplicitWidth = 470
          ExplicitHeight = 436
          inherited ScrollBox1: TScrollBox
            Width = 470
            Height = 436
            ExplicitWidth = 470
            ExplicitHeight = 436
          end
        end
        object Panel1: TPanel
          Left = 1
          Top = 437
          Width = 470
          Height = 42
          Align = alBottom
          Caption = 'Panel1'
          ShowCaption = False
          TabOrder = 0
          object btnSaveRuntime: TButton
            Left = 15
            Top = 6
            Width = 90
            Height = 25
            Caption = #20445#23384
            ImageIndex = 29
            ImageMargins.Left = 5
            TabOrder = 0
            OnClick = btnSaveRuntimeClick
          end
        end
      end
    end
  end
  object DQParams: TADOQuery
    Connection = FPlatformDB.ADOConn
    Parameters = <
      item
        Name = 'KD'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM SYS_Params'
      'WHERE Kind=:KD')
    Left = 176
    Top = 192
  end
end
