unit UPlatformParams;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Spin, StdCtrls, Buttons, ExtCtrls, ComCtrls, UPlatformDB, DB, Data.Win.ADODB,
  UVonLog, UVonSystemFuns, Grids, ValEdit, UFrameEnterForm;

type
  TFPlatformParams = class(TForm)
    pcKind: TPageControl;
    tsTicketNo: TTabSheet;
    lstTicket: TListBox;
    DQParams: TADOQuery;
    tsSystem: TTabSheet;
    lstSystem: TListBox;
    TabSheet1: TTabSheet;
    lstSerial: TListBox;
    vlSerial: TValueListEditor;
    btnSerialAdd: TButton;
    btnSerialDel: TButton;
    ESerialName: TLabeledEdit;
    TabSheet2: TTabSheet;
    lstRuntime: TListBox;
    Panel2: TPanel;
    FrameEnterForm1: TFrameEnterForm;
    Panel1: TPanel;
    btnSaveRuntime: TButton;
    Splitter1: TSplitter;
    Panel3: TPanel;
    lbSysIntValue: TLabel;
    lbSysHeader: TLabel;
    ESysName: TEdit;
    lbSysTrailler: TLabel;
    ESysStrValue: TEdit;
    ESysIntValue: TSpinEdit;
    btnSysSave: TButton;
    lbSysStrValue: TLabel;
    ESysHeader: TEdit;
    lbSysFloatValue: TLabel;
    ESysTrailler: TEdit;
    lbSysName: TLabel;
    ESysFloatValue: TEdit;
    GridPanel1: TGridPanel;
    Label2: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    ECodeFormat: TComboBox;
    SpeedButton1: TSpeedButton;
    EFmtDate: TComboBox;
    EFmtLen: TComboBox;
    ETicketTrailler: TEdit;
    EKind: TComboBox;
    ETicketNo: TSpinEdit;
    ETicketName: TEdit;
    ETicketHeader: TEdit;
    btnSaveTicket: TButton;
    btnDel: TButton;
    procedure pcKindChange(Sender: TObject);
    procedure lstSystemClick(Sender: TObject);
    procedure lstTicketClick(Sender: TObject);
    procedure btnSaveTicketClick(Sender: TObject);
    procedure btnSysSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnSerialAddClick(Sender: TObject);
    procedure btnSerialDelClick(Sender: TObject);
    procedure lstSerialClick(Sender: TObject);
    procedure btnSaveRuntimeClick(Sender: TObject);
    procedure lstRuntimeClick(Sender: TObject);
  private
    { Private declarations }
    FCurrentRuntime: string;
    procedure LoadKind(KindName: string; lst: TListBox; multi: Boolean = True);
  public
    { Public declarations }
  end;

var
  FPlatformParams: TFPlatformParams;

implementation

uses DateUtils;

{$R *.dfm}

procedure TFPlatformParams.FormCreate(Sender: TObject);
begin
  DQParams.Connection:= FPlatformDB.ADOConn;
  pcKindChange(nil);
end;

procedure TFPlatformParams.pcKindChange(Sender: TObject);
var
  I: Integer;
begin
  case pcKind.ActivePageIndex of
  0: LoadKind('SYSTEM', lstSystem);
  1: LoadKind('TICKET', lstTicket);
  2: LoadKind('SERIAL', lstSerial, False);
  3: begin
      FCurrentRuntime:= '';
      lstRuntime.Clear;
      for I := 0 to system_runtime_settings.Count - 1 do
        lstRuntime.Items.Add(system_runtime_settings.Names[I]);
      with DQParams do begin
        Close;
        Parameters[0].Value:= 'RUNTIME';
        Open;
      end;
    end;
  end;
end;

procedure TFPlatformParams.LoadKind(KindName: string; lst: TListBox; multi: Boolean = True);
begin
  lst.Clear;
  with DQParams do begin
    Close;
    Parameters[0].Value:= KindName;
    Open;
    while not EOF do begin
      if multi then
        lst.Items.Add(FieldByName('Name').AsString)
      else if lst.Items.IndexOf(FieldByName('Name').AsString) < 0 then
        lst.Items.Add(FieldByName('Name').AsString);
      Next;
    end;
  end;
end;

(* 系统参数管理 *)

procedure TFPlatformParams.lstSystemClick(Sender: TObject);
begin
  if lstSystem.ItemIndex < 0 then Exit;
  DQParams.Locate('Name', lstSystem.Items[lstSystem.ItemIndex], []);
  ESysName.Text:= DQParams.FieldByName('Name').AsString;
  ESysHeader.Text:= DQParams.FieldByName('Header').AsString;
  ESysStrValue.Text:= DQParams.FieldByName('StrValue').AsString;
  ESysIntValue.Value:= DQParams.FieldByName('IntValue').AsInteger;
  ESysFloatValue.Text:= DQParams.FieldByName('FloatValue').AsString;
  ESysTrailler.Text:= DQParams.FieldByName('Trailler').AsString;
end;

procedure TFPlatformParams.btnSysSaveClick(Sender: TObject);
begin
  if DQParams.Locate('Name', ESysName.Text, [])then
    DQParams.Edit
  else begin
    DQParams.Append;
    lstSystem.AddItem(ESysName.Text, nil);
  end;
  DQParams.FieldByName('Kind').AsString:= 'SYSTEM';
  DQParams.FieldByName('Name').AsString:= ESysName.Text;
  DQParams.FieldByName('Header').AsString:= ESysHeader.Text;
  DQParams.FieldByName('StrValue').AsString:= ESysStrValue.Text;
  DQParams.FieldByName('IntValue').AsInteger:= ESysIntValue.Value;
  DQParams.FieldByName('FloatValue').AsString:= ESysFloatValue.Text;
  DQParams.FieldByName('Trailler').AsString:= ESysTrailler.Text;
  DQParams.Post;
end;

(* 编码规则管理 *)

procedure TFPlatformParams.SpeedButton1Click(Sender: TObject);
var
  S: string;
begin
  case EFmtDate.ItemIndex of
  0: S:= '';                  //无年月
  1: S:= '%1:.4d';            //长年分
  2: S:= '%2:.2d';            //短年份
  3: S:= '%1:.4d%3:.2d';      //长年月
  4: S:= '%2:.2d%3:.2d';      //短年月
  5: S:= '%3:.2d';            //含月份
  6: S:= '%2:.2d%3:.2d%4:.2d';//年月日
  7: S:= '%3:.2d%4:.2d';      //含月日
  end;
  case EFmtLen.ItemIndex of
  0: S:= S + '%0:.2d';        //2位
  1: S:= S + '%0:.3d';        //3位
  2: S:= S + '%0:.4d';        //4位
  3: S:= S + '%0:.5d';        //5位
  4: S:= S + '%0:.6d';        //6位
  5: S:= S + '%0:.7d';        //7位
  6: S:= S + '%0:.8d';        //8位
  7: S:= S + '%0:.9d';        //9位
  8: S:= S + '%0:.10d';       //10位
  9: S:= S + '%0:.11d';       //11位
  10:S:= S + '%0:.12d';       //12位
  end;
  ECodeFormat.Text:= S;
end;

procedure TFPlatformParams.lstTicketClick(Sender: TObject);
begin
  if lstTicket.ItemIndex < 0 then Exit;
  DQParams.Locate('Name', lstTicket.Items[lstTicket.ItemIndex], []);
  ETicketName.Text:= DQParams.FieldByName('Name').AsString;
  ETicketHeader.Text:= DQParams.FieldByName('Header').AsString;
  ETicketNo.Value:= DQParams.FieldByName('IntValue').AsInteger;
  EKind.ItemIndex:= Trunc(DQParams.FieldByName('FloatValue').AsFloat);
  ETicketTrailler.Text:= DQParams.FieldByName('Trailler').AsString;
  ECodeFormat.Text:= DQParams.FieldByName('Backup').AsString;
end;

procedure TFPlatformParams.btnDelClick(Sender: TObject);
begin     //删除一个编码规则
  if DQParams.FieldByName('Name').AsString <> ETicketName.Text then
    raise Exception.Create(RES_INFO_NOTSELECTED)
  else if MessageDlg(RES_DLG_DEL_PROMPT, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  if MessageDlg(RES_DLG_DEL_WARNING, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then Exit;
  lstTicket.Items.Delete(lstTicket.Items.IndexOf(DQParams.FieldByName('Name').AsString));
  WriteLog(LOG_MAJOR, 'Department_DELITEM',
    Format('项目“%s”被删除', [DQParams.FieldByName('Name').AsString]));
  DQParams.Delete;
end;

procedure TFPlatformParams.btnSaveTicketClick(Sender: TObject);
begin
  if DQParams.Locate('Name', ETicketName.Text, []) then
    DQParams.Edit
  else begin
    DQParams.Append;
    lstTicket.AddItem(ETicketName.Text, nil);
  end;
  DQParams.FieldByName('Kind').AsString:= 'TICKET';
  DQParams.FieldByName('Name').AsString:= ETicketName.Text;
  DQParams.FieldByName('Header').AsString:= ETicketHeader.Text;
  DQParams.FieldByName('IntValue').AsInteger:= ETicketNo.Value;
  case EKind.ItemIndex of
  0: DQParams.FieldByName('FloatValue').AsFloat:= 0;
  1: DQParams.FieldByName('FloatValue').AsFloat:= 1 + YearOf(Now) / 10000;
  2: DQParams.FieldByName('FloatValue').AsFloat:= 2 + MonthOf(Now) / 100;
  3: DQParams.FieldByName('FloatValue').AsFloat:= 3 + WeekOf(Now) / 10;
  4: DQParams.FieldByName('FloatValue').AsFloat:= 4 + DayOf(Now) / 100;
  end;
  DQParams.FieldByName('Trailler').AsString:= ETicketTrailler.Text;
  DQParams.FieldByName('Backup').AsString:= ECodeFormat.Text;
  DQParams.Post;
end;

(* 序列参数管理 *)

procedure TFPlatformParams.btnSerialAddClick(Sender: TObject);
var
  I, iv: Integer;
  fv: Extended;
begin
  while DQParams.Locate('Name', ESerialName.Text, []) do
    DQParams.Delete;
  if lstSerial.Items.IndexOf(ESerialName.Text) < 0 then
    lstSerial.Items.Add(ESerialName.Text);
  for I := 1 to vlSerial.RowCount - 1 do begin
    DQParams.Append;
    DQParams.FieldByName('Kind').AsString:= 'SERIAL';
    DQParams.FieldByName('Name').AsString:= ESerialName.Text;
    DQParams.FieldByName('StrValue').AsString:= vlSerial.Cells[0, I];
    if tryStrToInt(vlSerial.Cells[1, I], iv) then
      DQParams.FieldByName('IntValue').AsInteger:= iv
    else DQParams.FieldByName('IntValue').AsInteger:= 0;
    if TryStrToFloat(vlSerial.Cells[1, I], fv) then
      DQParams.FieldByName('FloatValue').AsExtended:= fv
    else DQParams.FieldByName('FloatValue').AsExtended:= 0;
    DQParams.FieldByName('Trailler').AsString:= ETicketTrailler.Text;
    DQParams.FieldByName('Backup').AsString:= ECodeFormat.Text;
    DQParams.Post;
  end;
end;

procedure TFPlatformParams.btnSerialDelClick(Sender: TObject);
begin
  if lstSerial.ItemIndex < 0 then Exit;
  if DQParams.FieldByName('Name').AsString <> ESerialName.Text then
    raise Exception.Create(RES_INFO_NOTSELECTED)
  else if MessageDlg(RES_DLG_DEL_PROMPT, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  if MessageDlg(RES_DLG_DEL_WARNING, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then Exit;
  while DQParams.Locate('Name', lstSerial.Items[lstSerial.ItemIndex], []) do
    DQParams.Delete;
  lstSerial.DeleteSelected;
end;

procedure TFPlatformParams.lstSerialClick(Sender: TObject);
var
  S: string;
begin
  if lstSerial.ItemIndex < 0 then Exit;
  if ESerialName.Text = lstSerial.Items[lstSerial.ItemIndex] then Exit;
  ESerialName.Text:= lstSerial.Items[lstSerial.ItemIndex];
  vlSerial.Strings.Clear;
  with DQParams do begin
    First;
    while not EOF do begin
      if DQParams.FieldByName('Name').AsString = ESerialName.Text then begin
        if DQParams.FieldByName('IntValue').AsInteger <> 0 then
          S:= DQParams.FieldByName('IntValue').AsString
        else S:= DQParams.FieldByName('FloatValue').AsString;
        vlSerial.InsertRow(DQParams.FieldByName('StrValue').AsString, S, True);
      end;
      Next;
    end;
  end;
end;

(* Runtime *)

procedure TFPlatformParams.lstRuntimeClick(Sender: TObject);
begin
  if lstRuntime.ItemIndex < 0 then Exit;
  FCurrentRuntime:= lstRuntime.Items[lstRuntime.ItemIndex];
  FrameEnterForm1.Xml:= '<template>' + system_runtime_settings.Values[FCurrentRuntime] + '</template>';
  FrameEnterForm1.ItemObj[0].NodeObj.Value:= system_runtime_value.Values[FCurrentRuntime];
end;

procedure TFPlatformParams.btnSaveRuntimeClick(Sender: TObject);
begin
  if DQParams.Locate('Name', FCurrentRuntime, []) then DQParams.Edit
  else DQParams.Append;
  DQParams.FieldByName('Kind').AsString:= 'RUNTIME';
  DQParams.FieldByName('Name').AsString:= FCurrentRuntime;
  DQParams.FieldByName('Header').AsString:= '';
  DQParams.FieldByName('IntValue').AsInteger:= 0;
  DQParams.FieldByName('FloatValue').AsFloat:= 0;
  DQParams.FieldByName('StrValue').AsString:= FrameEnterForm1.ItemObj[0].NodeObj.Value;
  DQParams.FieldByName('Trailler').AsString:= '';
  DQParams.FieldByName('Backup').AsString:= '';
  DQParams.Post;
  system_runtime_value.Values[FCurrentRuntime]:= FrameEnterForm1.ItemObj[0].NodeObj.Value;
end;

end.
