(*******************************************************************************
  角色管理    jamesvon 2/8 2015
================================================================================
  层级关系说明
  没关系  :独立角色，与上下级间无任何联系。
  继承上级:继承上级已有的角色权限，并增加本级设置的权限，是逐级扩大型
  包含下级:本级角色包含有下级角色的权限，并增加本级权限，是逐级减少型
*******************************************************************************)
unit UPlatformRole;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, CheckLst, ExtCtrls, DB, data.win.ADODB, ComCtrls, DBClient,
  UPlatformInfo, UFrameTree, ImgList, ToolWin, Spin, Grids, ValEdit,
  System.ImageList;

type
  TFPlatformRole = class(TForm)
    plRole: TPanel;
    Panel1: TPanel;
    Label10: TLabel;
    ERoleName: TEdit;
    ENote: TMemo;
    rgLinkType: TRadioGroup;
    DQARole: TADOQuery;
    ToolBar1: TToolBar;
    imgRight: TImageList;
    btnUsingExport: TToolButton;
    btnUsingImport: TToolButton;
    DQRights: TADOQuery;
    ToolButton1: TToolButton;
    FrameTree1: TFrameTree;
    rgUserGrade: TRadioGroup;
    EUsing: TComboBox;
    EControlID: TSpinEdit;
    btnUsingAdd: TToolButton;
    btnUsingDel: TToolButton;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    btnUsingSave: TToolButton;
    lstRight: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FrameTree1btnTreeAddClick(Sender: TObject);
    procedure FrameTree1btnTreeAddChildClick(Sender: TObject);
    procedure FrameTree1btnTreeEditClick(Sender: TObject);
    procedure btnUsingExportClick(Sender: TObject);
    procedure btnUsingImportClick(Sender: TObject);
    procedure FrameTree1btnTreeDelClick(Sender: TObject);
    procedure lstRightClick(Sender: TObject);
    procedure btnUsingSaveClick(Sender: TObject);
    procedure btnUsingAddClick(Sender: TObject);
    procedure btnUsingDelClick(Sender: TObject);
  private
    { Private declarations }
    FInfo: TRoleInfo;
    procedure EventOfLoadRole(Sender: TObject; Node: TTreeNode);
    procedure EnevtOfUpdateOrder(Sender: TObject; Node: TTreeNode);
    procedure LoadRight(RoleID: Integer);
    procedure SaveRight(Using: string; ControlId: Integer = -1);
    procedure DeleteRight(RoleID: Integer);
  public
    { Public declarations }
  end;

var
  FPlatformRole: TFPlatformRole;

implementation

uses Types, UPlatformDB, UVonSystemFuns;

{$R *.dfm}

procedure TFPlatformRole.FormCreate(Sender: TObject);
var
  i: Integer;
  szSpecial: string;
  D: TClientDataSet;
begin
  DQARole.Connection:= FPlatformDB.ADOConn;
  DQRights.Connection:= FPlatformDB.ADOConn;
  FInfo := TRoleInfo.Create;
  FrameTree1.DisplayCheckBox := True;
  if FPlatformDB.LoginInfo.IsSystem or FPlatformDB.LoginInfo.IsAdmin or
    FPlatformDB.LoginInfo.IsManager then
  begin
    for i := 0 to FAppUsingList.Count - 1 do
      EUsing.Items.Add(FAppUsingList[i, 0]);
  end
  else
  begin
    EUsing.Items.Text := FPlatformDB.LoginInfo.RightStr;
    EControlID.Visible := False;
  end;
  EUsing.ItemIndex := 0;
  FrameTree1.DisplayCheckBox := False;
  FrameTree1.DisplayTool := True;
  FrameTree1.DBConn := FPlatformDB.ADOConn;
  if FPlatformDB.LoginInfo.IsSystem then
    szSpecial := '500' // 系统管理员
  else if FPlatformDB.LoginInfo.IsAdmin then
    szSpecial := '200' // 应用管理员
  else if FPlatformDB.LoginInfo.IsManager then
    szSpecial := '100' // 管理员
  else
    szSpecial := '0';
  FrameTree1.FieldList := '*';
  FrameTree1.TableName:= 'SYS_Role';
  FrameTree1.ConditionSQL:= 'SpecialFlag<=' + szSpecial;
  FrameTree1.DisplayFieldName := 'RoleName';
  FrameTree1.DisplayOrder := 'ListOrder';
  FrameTree1.ImgFieldName := 'LinkType';
  FrameTree1.KeyFieldName := 'ID';
  FrameTree1.ParentFieldName := 'PID';
  FrameTree1.ImageOffset := 0;
  FrameTree1.SelectedOffset := 3;
  FrameTree1.OnSelectedNode := EventOfLoadRole;
  FrameTree1.OnUpdateOrder := EnevtOfUpdateOrder;
  FrameTree1.Load(nil, '0');
  if FPlatformDB.LoginInfo.IsSystem then
  begin
    rgUserGrade.Items.AddObject('系统管理', TObject(500));
    rgUserGrade.Items.AddObject('应用管理', TObject(200));
    rgUserGrade.Items.AddObject('业务管理', TObject(100));
    rgUserGrade.Columns := 2;
  end
  else if FPlatformDB.LoginInfo.IsAdmin then
  begin
    rgUserGrade.Items.AddObject('应用管理', TObject(200));
    rgUserGrade.Items.AddObject('业务管理', TObject(100));
    rgUserGrade.Columns := 3;
  end
  else if FPlatformDB.LoginInfo.IsManager then
    rgUserGrade.Items.AddObject('业务管理', TObject(100));
  rgUserGrade.Items.AddObject('普通用户', TObject(0));
  rgUserGrade.Visible := rgUserGrade.Items.Count > 1;
  rgUserGrade.ItemIndex := rgUserGrade.Items.Count - 1;
end;

procedure TFPlatformRole.FormDestroy(Sender: TObject);
begin
  FInfo.Free;
end;

procedure TFPlatformRole.FrameTree1btnTreeAddChildClick(Sender: TObject);
var
  newNode: TTreeNode;
begin
  if Assigned(FrameTree1.tree.Selected) then
    FInfo.PID := StrToInt(FrameTree1.ID[FrameTree1.tree.Selected])
  else
    FInfo.PID := 0;
  newNode := FrameTree1.AddChild(FrameTree1.tree.Selected, ERoleName.Text,
    rgLinkType.ItemIndex);
  FInfo.ID := 0;
  FInfo.RoleName := ERoleName.Text;
  FInfo.RoleInfo := ENote.Text;
  FInfo.LinkType := rgLinkType.ItemIndex;
  FInfo.SpecialFlag :=
    Integer(rgUserGrade.Items.Objects[rgUserGrade.ItemIndex]);
  FInfo.ListOrder := newNode.Index;
  try
    DQARole.Parameters[0].Value := 0;
    DQARole.Open;
    FInfo.SaveToDB(DQARole);
    FrameTree1.ID[newNode]:= IntToStr(FInfo.ID);
  finally
    DQARole.Close;
  end;
end;

procedure TFPlatformRole.FrameTree1btnTreeAddClick(Sender: TObject);
var
  newNode: TTreeNode;
begin
  if Assigned(FrameTree1.tree.Selected) and
    Assigned(FrameTree1.tree.Selected.Parent) then
    FInfo.PID := StrToInt(FrameTree1.ID[FrameTree1.tree.Selected.Parent])
  else
    FInfo.PID := 0;
  newNode := FrameTree1.AddNode(FrameTree1.tree.Selected, ERoleName.Text,
    rgLinkType.ItemIndex);

  FInfo.ID := 0;
  FInfo.RoleName := ERoleName.Text;
  FInfo.RoleInfo := ENote.Text;
  FInfo.LinkType := rgLinkType.ItemIndex;
  FInfo.SpecialFlag :=
    Integer(rgUserGrade.Items.Objects[rgUserGrade.ItemIndex]);
  FInfo.ListOrder := newNode.Index;
  try
    DQARole.Parameters[0].Value := 0;
    DQARole.Open;
    FInfo.SaveToDB(DQARole);
    FrameTree1.ID[newNode]:= IntToStr(FInfo.ID);
  finally
    DQARole.Close;
  end;
end;

procedure TFPlatformRole.FrameTree1btnTreeDelClick(Sender: TObject);
begin // 删除一个角色
  if not Assigned(FrameTree1.tree.Selected) then
    raise Exception.Create(RES_NODE_NONESELECTED);
  if FrameTree1.tree.Selected.HasChildren then
    raise Exception.Create(RES_NODE_HASCHILDREN);
  if MessageDlg(RES_DLG_DEL_PROMPT, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  if MessageDlg(RES_DLG_DEL_WARNING, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  DeleteRight(StrToInt(FrameTree1.ID[FrameTree1.tree.Selected]));
  try
    DQARole.Parameters[0].Value := StrToInt(FrameTree1.ID[FrameTree1.tree.Selected]);
    DQARole.Open;
    DQARole.Delete;
  finally
    DQARole.Close;
  end;
  FrameTree1.tree.Selected.Delete;
end;

procedure TFPlatformRole.FrameTree1btnTreeEditClick(Sender: TObject);
begin
  if not Assigned(FrameTree1.tree.Selected) then
    raise Exception.Create(RES_NODE_NONESELECTED);
  try
    DQARole.Parameters[0].Value := StrToInt(FrameTree1.ID[FrameTree1.tree.Selected]);
    DQARole.Open;
    FInfo.LoadFromDB(DQARole);
    FInfo.RoleName := ERoleName.Text;
    FInfo.RoleInfo := ENote.Text;
    FInfo.LinkType := rgLinkType.ItemIndex;
    FInfo.SpecialFlag :=
      Integer(rgUserGrade.Items.Objects[rgUserGrade.ItemIndex]);
    FInfo.SaveToDB(DQARole);
  finally
    DQARole.Close;
  end;
  FrameTree1.tree.Selected.ImageIndex := rgLinkType.ItemIndex;
  FrameTree1.tree.Selected.Text := ERoleName.Text;
end;

procedure TFPlatformRole.LoadRight(RoleID: Integer);
begin
  lstRight.Items.Clear;
  with DQRights do
  begin
    Close;
    Parameters[0].Value := RoleID;
    Open;
    while not EOF do
    begin
      lstRight.Items.Add(FieldByName('UsingName').AsString + ',' +
        FieldByName('ControlID').AsString);
      Next;
    end;
  end;
end;

procedure TFPlatformRole.lstRightClick(Sender: TObject);
begin
  if lstRight.ItemIndex < 0 then Exit;
  if EControlID.Visible then
    with SplitStr(lstRight.Items[lstRight.ItemIndex], ',') do
    begin
      EUsing.ItemIndex := EUsing.Items.IndexOf(Strings[0]);
      EControlID.Value := StrToInt(Strings[1]);
    end
  else
    EUsing.ItemIndex := EUsing.Items.IndexOf
      (lstRight.Items[lstRight.ItemIndex]);
end;

procedure TFPlatformRole.SaveRight(Using: string; ControlId: Integer);
var
  S: string;
begin
  if ControlId < 0 then
    S := Using
  else
    S := Using + ',' + IntToStr(ControlId);
  if lstRight.Items.IndexOf(S) > 0 then
    Exit;
  lstRight.Items.Add(S);
end;

procedure TFPlatformRole.btnUsingAddClick(Sender: TObject);
begin
  if not EControlID.Visible then
    SaveRight(EUsing.Text)
  else
    SaveRight(EUsing.Text, EControlID.Value);
end;

procedure TFPlatformRole.btnUsingDelClick(Sender: TObject);
begin
  if lstRight.ItemIndex < 0 then
    Exit;
  lstRight.DeleteSelected;
end;

procedure TFPlatformRole.btnUsingExportClick(Sender: TObject);
begin
  with SaveDialog1 do
    if Execute then
      lstRight.Items.SaveToFile(Filename);
end;

procedure TFPlatformRole.btnUsingImportClick(Sender: TObject);
begin
  with OpenDialog1 do
    if Execute then
      lstRight.Items.LoadFromFile(Filename);
end;

procedure TFPlatformRole.btnUsingSaveClick(Sender: TObject);
var
  i, mPos: Integer;
begin
  with DQRights do
  begin
    First;
    while not EOF do
      Delete;
    for i := 0 to lstRight.Count - 1 do
      with SplitStr(lstRight.Items[i], ',') do
      begin
        DQRights.Append;
        FieldByName('UsingName').AsString := Strings[0];
        FieldByName('ControlID').AsString := Strings[1];
        FieldByName('RoleIdx').AsInteger := FInfo.ID;
        Post;
      end;
  end;
end;

procedure TFPlatformRole.DeleteRight(RoleID: Integer);
begin
  with DQRights do
    try
      Parameters[0].Value := RoleID;
      Open;
      while not EOF do
        Delete;
    finally
      Close;
    end;
end;

procedure TFPlatformRole.EnevtOfUpdateOrder(Sender: TObject; Node: TTreeNode);
begin
  with DQARole do
    try
      Parameters[0].Value := StrToInt(FrameTree1.ID[Node]);
      Open;
      Edit;
      if Assigned(Node.Parent) then
        FieldByName('PID').AsInteger := StrToInt(FrameTree1.ID[Node])
      else
        FieldByName('PID').AsInteger := 0;
      FieldByName('ListOrder').AsInteger := Node.Index;
      Post;
    finally
      Close;
    end;
  if Assigned(Node.getNextSibling()) then
    EnevtOfUpdateOrder(Sender, Node.getNextSibling);
end;

procedure TFPlatformRole.EventOfLoadRole(Sender: TObject; Node: TTreeNode);
begin
  if not Assigned(Node) then
    Exit;
  try
    DQARole.Parameters[0].Value := StrToInt(FrameTree1.ID[FrameTree1.tree.Selected]);
    DQARole.Open;
    FInfo.LoadFromDB(DQARole);
  finally
    DQARole.Close;
  end;
  ERoleName.Text := FInfo.RoleName;
  ENote.Text := FInfo.RoleInfo;
  rgLinkType.ItemIndex := FInfo.LinkType;
  rgUserGrade.ItemIndex := rgUserGrade.Items.IndexOfObject
    (TObject(FInfo.SpecialFlag));
  LoadRight(StrToInt(FrameTree1.ID[FrameTree1.tree.Selected]));
end;

end.
