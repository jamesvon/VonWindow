unit UPlatformUpgrade;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, WinApi.ShellAPI, ZLib, Buttons, UPlatformDB, Data.win.ADODB,
  UVonLog, UVonSystemFuns, jpeg, IniFiles, UVonTransmissionApp;

resourcestring
  RES_UPGRADE_CONNECT_FAILED = '链接升级程序失败，无法完成升级工作。';

type
  TFPlatFormUpgrade = class(TForm)
    Panel1: TPanel;
    Image1: TImage;
    mLog: TMemo;
    btnLater: TBitBtn;
    TimerToUpgrade: TTimer;
    lbTitle2: TLabel;
    lbTitle1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btnLaterClick(Sender: TObject);
    procedure TimerToUpgradeTimer(Sender: TObject);
  private
    { Private declarations }
    FStatus: Integer;
    FOnUpgrade: TNotifyEvent;
    procedure SetOnUpgrade(const Value: TNotifyEvent);
  public
    { Public declarations }
    procedure UpgradeProcess(UrlType, UpgradeUrl: string; UrlPort: Integer);
    procedure DisplayLog(Info: string);
    property OnUpgrade: TNotifyEvent read FOnUpgrade write SetOnUpgrade;
  end;

var
  FPlatFormUpgrade: TFPlatFormUpgrade;

implementation

type
  TMyCtrl = class(TControl)
  published
    property Font;
    property Color;
  end;

{$R *.dfm}

procedure TFPlatFormUpgrade.DisplayLog(Info: string);
begin
  MLog.Lines.Add(Info);
  WriteLog(LOG_INFO, 'Upgrade', Info);
end;

type tempControl = class(TControl) published property Font; property Color; end;

procedure TFPlatFormUpgrade.FormCreate(Sender: TObject);
var
  IniFile: TIniFile;
  procedure SetComponent(Ctrl: TControl);
  begin
    Ctrl.Top:= IniFile.ReadInteger('Login', Ctrl.Name + '.Top', Ctrl.Top);
    Ctrl.Left:= IniFile.ReadInteger('Login', Ctrl.Name + '.Left', Ctrl.Left);
    Ctrl.Width:= IniFile.ReadInteger('Login', Ctrl.Name + '.Width', Ctrl.Width);
    Ctrl.Height:= IniFile.ReadInteger('Login', Ctrl.Name + '.Top', Ctrl.Height);
    tempControl(Ctrl).Font.Size:= IniFile.ReadInteger('Login', Ctrl.Name + '.Font.Size', tempControl(Ctrl).Font.Size);
    tempControl(Ctrl).Font.Color:= IniFile.ReadInteger('Login', Ctrl.Name + '.Font.Color', tempControl(Ctrl).Font.Color);
    tempControl(Ctrl).Color:= IniFile.ReadInteger('Login', Ctrl.Name + '.Color', tempControl(Ctrl).Color);
  end;
begin
  IniFile:= TIniFile.Create(FPlatformDB.AppPath + 'Styles\VonWindows.cfg');
  with IniFile do try
    Image1.Picture.LoadFromFile(ReadString('Login', 'background', ''));
    Panel1.Width := Image1.Picture.Width + 16;
    Panel1.Height := Image1.Picture.Height + 16;
    self.Width := Image1.Picture.Width + 16;
    self.Height := Image1.Picture.Height + 83;
    lbTitle1.Caption := FPlatformDB.ApplicationTitle;
    lbTitle2.Caption := lbTitle1.Caption;
    SetComponent(lbTitle1);
    SetComponent(lbTitle2);
  finally
    Free;
  end;
  FStatus := 0;
  TimerToUpgrade.Enabled := True;
end;

procedure TFPlatFormUpgrade.SetOnUpgrade(const Value: TNotifyEvent);
begin
  FOnUpgrade := Value;
end;

procedure TFPlatFormUpgrade.btnLaterClick(Sender: TObject);
begin
  ModalResult := mrYes;
end;

procedure TFPlatFormUpgrade.TimerToUpgradeTimer(Sender: TObject);
begin
  TimerToUpgrade.Enabled := False;
  btnLater.Enabled:= False;
  try
    if Assigned(FOnUpgrade) then FOnUpgrade(Self);
  except
    on E: Exception do begin
      mLog.Lines.Add(E.Message);
      mLog.Width:= btnLater.Left - 16;
      btnLater.Enabled:= true;
    end;
  end;
end;

procedure TFPlatFormUpgrade.UpgradeProcess(UrlType, UpgradeUrl: string; UrlPort: Integer);
var
  I: Integer;
begin // 链接网络，检查版本信息，获取下载列表，调用 EventToUpgradeFiles 下载     UrlType(TCP)UpgradeUrl(192.168.0.102)UrlPort(8080)
  MLog.Lines.Clear;
  DisplayLog('正在链接网络服务器检查版本 ... ... ');
  with TVonAppClient.Create(UrlType, SYS_NAME) do try
    HostIP:= UpgradeUrl;
    HostPort:= UrlPort;
    CheckVer(FPlatformDB.CurrentVersion);
    if Files.Count > 0 then begin
      DisplayLog('发现有升级文件..');
      for I := 0 to Files.Count - 1 do begin
        DisplayLog('已下载' + Files[I]);
        GetVerFile(Files.Names[I], FPlatformDB.AppPath + 'UPGRADE\' + Files.Names[I]);
      end;
      Files.SaveToFile(FPlatformDB.AppPath + 'UPGRADE\Upgrade.dat');
      ShellExecute(0, 'open', 'Upgrade.exe', PChar('/C:"' + CERT_KEY +
        '" /N:"' + SYS_NAME + '" /F:"' + CERT_FILE + '"'),
        PChar(FPlatformDB.AppPath + 'Upgrade\'), SW_NORMAL);
      ModalResult := mrNo;
    end else ModalResult := mrYes;
  finally
    Free;
  end;
end;

end.

