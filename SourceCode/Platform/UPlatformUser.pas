unit UPlatformUser;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, ToolWin, ImgList, CheckLst, Buttons,
  UPlatformDB, DB, Data.Win.ADODB, UFrameGridBar, UPlatformInfo, UFrameTree, DBGrids,
  System.ImageList;

resourcestring
  RES_USER_DOUBLICATION = '出现重名用户，不允许注册。';
  RES_USER_NOEDITLOGINNAME = '登录名不允许修改。';

type
  TFPlatformUser = class(TForm)
    Panel1: TPanel;
    ImageList1: TImageList;
    ToolBar1: TToolBar;
    Label1: TLabel;
    EUserGroup: TComboBox;
    btnGroupAdd: TToolButton;
    btnGroupEdit: TToolButton;
    ToolButton4: TToolButton;
    btnGroupDel: TToolButton;
    Panel2: TPanel;
    btnUserAdd: TBitBtn;
    btnUserEdit: TBitBtn;
    btnUserDel: TBitBtn;
    btnUserClose: TBitBtn;
    DBGroup: TADOTable;
    FrameUserGrid: TFrameGridBar;
    DQUser: TADOQuery;
    Splitter1: TSplitter;
    DQRoleOfUser: TADOQuery;
    FrameRole: TFrameTree;
    procedure FormCreate(Sender: TObject);
    procedure btnGroupAddClick(Sender: TObject);
    procedure btnGroupEditClick(Sender: TObject);
    procedure btnGroupDelClick(Sender: TObject);
    procedure btnUserAddClick(Sender: TObject);
    procedure EUserGroupSelect(Sender: TObject);
    procedure btnUserEditClick(Sender: TObject);
    procedure btnUserDelClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnUserCloseClick(Sender: TObject);
    procedure FrameUserGridGridCellClick(Column: TColumn);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    Info: TUserInfo;
    FShowed: Boolean;
    procedure LoadUsers(GroupId: Integer);
    procedure EventOfCheckNode(Sender: TObject; Node: TTreeNode);
  public
    { Public declarations }
  end;

var
  FPlatformUser: TFPlatformUser;

implementation

uses UVonSystemFuns, UDlgUser;

{$R *.dfm}

procedure TFPlatformUser.FormCreate(Sender: TObject);
var
  szSpecial: string;
begin
  Info := TUserInfo.Create;
  DBGroup.Connection:= FPlatformDB.ADOConn;
  DQUser.Connection:= FPlatformDB.ADOConn;
  DQRoleOfUser.Connection:= FPlatformDB.ADOConn;
  FrameUserGrid.LoadTitles('SystemUser');
{$REGION 'Load group names to EUserGroup'}
    with DBGroup do
    begin
      Open;
      while not EOF do
      begin
        EUserGroup.Items.AddObject(FieldByName('GroupName').AsString,
          TObject(FieldByName('ID').AsInteger));
        Next;
      end;
    end;
    EUserGroup.ItemIndex := 0;
    if EUserGroup.ItemIndex >= 0 then
      LoadUsers(Integer(EUserGroup.Items.Objects[EUserGroup.ItemIndex]));
{$ENDREGION}
{$REGION 'Load roles'}
  FrameRole.DisplayTool := false;
  FrameRole.AutoExpand := True;
  FrameRole.DBConn := FPlatformDB.ADOConn;
  FrameRole.FieldList := '*';
  FrameRole.TableName:= 'SYS_Role';
  if FPlatformDB.LoginInfo.IsSystem then
    szSpecial := '500' // 系统管理员
  else if FPlatformDB.LoginInfo.IsAdmin then
    szSpecial := '200' // 应用管理员
  else if FPlatformDB.LoginInfo.IsManager then
    szSpecial := '100' // 管理员
  else
    szSpecial := '0';
  FrameRole.ConditionSQL:= 'SpecialFlag<=' + szSpecial;
  FrameRole.DisplayFieldName := 'RoleName';
  FrameRole.DisplayOrder := 'ListOrder';
  FrameRole.ImgFieldName := 'LinkType';
  FrameRole.KeyFieldName := 'ID';
  FrameRole.ParentFieldName := 'PID';
  FrameRole.ImageOffset := 0;
  FrameRole.SelectedOffset := 3;
{$ENDREGION}
end;

procedure TFPlatformUser.FormDestroy(Sender: TObject);
begin
  Info.Free;
end;

procedure TFPlatformUser.FormShow(Sender: TObject);
begin
  FrameRole.DisplayCheckBox := True;
  FrameRole.Load(nil, '0');
  FrameRole.OnCheckNode := EventOfCheckNode;
  OnShow:= nil;
end;

{ TFPlatformUser.Group }

procedure TFPlatformUser.btnGroupAddClick(Sender: TObject);
var
  S: string;
begin
  if InputQuery('录入', '人员分组名称', S) then
  begin
    DBGroup.Append;
    DBGroup.FieldByName('GroupName').AsString := S;
    DBGroup.Post;
    EUserGroup.ItemIndex := EUserGroup.Items.AddObject(S,
      TObject(DBGroup.FieldByName('ID').AsInteger));
  end;
end;

procedure TFPlatformUser.btnGroupDelClick(Sender: TObject);
begin
  DBGroup.Locate('GroupName', EUserGroup.Text, []);
  if EUserGroup.ItemIndex < 0 then
    Exit;
  if MessageDlg(RES_DLG_DEL_PROMPT, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  if MessageDlg(RES_DLG_DEL_WARNING, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  DBGroup.Delete;
  EUserGroup.DeleteSelected;
  EUserGroup.ItemIndex := 0;
end;

procedure TFPlatformUser.btnGroupEditClick(Sender: TObject);
var
  S: string;
  Idx: Integer;
begin
  if EUserGroup.ItemIndex < 0 then
    Exit;
  S := EUserGroup.Text;
  if InputQuery('录入', '人员分组名称', S) and (S <> EUserGroup.Text) then
  begin
    DBGroup.Locate('GroupName', S, []);
    DBGroup.Edit;
    DBGroup.FieldByName('GroupName').AsString := S;
    DBGroup.Post;
    Idx := EUserGroup.ItemIndex;
    EUserGroup.Items[Idx] := S;
    EUserGroup.ItemIndex := Idx;
  end;
end;

procedure TFPlatformUser.EUserGroupSelect(Sender: TObject);
begin
  LoadUsers(Integer(EUserGroup.Items.Objects[EUserGroup.ItemIndex]));
end;

procedure TFPlatformUser.EventOfCheckNode(Sender: TObject; Node: TTreeNode);
begin
  with DQRoleOfUser do begin
    if FrameRole.IsChecked(Node)then begin
      if Locate('RoleIdx', StrToInt(FrameRole.ID[Node]), []) then
        raise Exception.Create(RES_INFO_DOUBLICATION);
      Append;
      FieldByName('UserIdx').AsInteger := Info.ID;
      FieldByName('RoleIdx').AsInteger := StrToInt(FrameRole.ID[Node]);
      Post;
    end else begin
      if not Locate('RoleIdx', StrToInt(FrameRole.ID[Node]), []) then
        raise Exception.Create(RES_NO_LOCATE);
      Delete;
    end;
  end;
end;

procedure TFPlatformUser.LoadUsers(GroupId: Integer);
begin
  DQUser.Close;
  DQUser.Parameters[0].Value := GroupId;
  DQUser.Open;
end;
     
{ TFPlatformUser.User }

procedure TFPlatformUser.btnUserAddClick(Sender: TObject);
begin
  with TFDlgUser.Create(nil)do try
    if ShowModal = mrOK then begin
      Info.ID:= 0;
      Info.GroupIdx:= Integer(EUserGroup.Items.Objects[EUserGroup.ItemIndex]);
      FormToInfo(Info);
      Info.SaveToDB(DQUser);
    end;
  finally
    Free;
  end;
end;

procedure TFPlatformUser.btnUserCloseClick(Sender: TObject);
begin
  FrameRole.DisplayCheckBox := not FrameRole.DisplayCheckBox;
end;

procedure TFPlatformUser.btnUserDelClick(Sender: TObject);
begin
  if DQUser.RecordCount < 1 then
    Exit;
  if MessageDlg(RES_DLG_DEL_PROMPT, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  if MessageDlg(RES_DLG_DEL_WARNING, mtInformation, [mbOK, mbCancel], 0) <>
    mrOK then
    Exit;
  DQUser.Delete;
end;

procedure TFPlatformUser.btnUserEditClick(Sender: TObject);
begin
  with TFDlgUser.Create(nil)do try
    Info.LoadFromDB(DQUser);
    InfoToForm(Info);
    if ShowModal = mrOK then begin
      FormToInfo(Info);
      Info.SaveToDB(DQUser);
    end;
  finally
    Free;
  end;
end;

procedure TFPlatformUser.FrameUserGridGridCellClick(Column: TColumn);
var
  szNode: TTreeNode;
begin
  with DQUser do
  begin
    if RecordCount < 1 then
      Exit;
    Info.LoadFromDB(DQUser);
  end;
  FrameRole.SelectAll(false, False);
  with DQRoleOfUser do
  begin
    Close;
    Parameters[0].Value := Info.ID;
    Open;
    while not EOF do
    begin
      szNode := FrameRole.FindNodeByID
        (FieldByName('RoleIdx').AsString);
      if Assigned(szNode) then
        FrameRole.SetChecked(szNode, True, false);
      Next;
    end;
  end;
end;

{ Department }


end.
