unit UPlatformZone;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Data.Win.ADODB, ComCtrls, UFrameLonLat, StdCtrls, ExtCtrls, UPlatformDB,
  ImgList, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  OleCtrls, SHDocVw, System.ImageList;

type
  TFPlatformZone = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    ECode: TEdit;
    EName: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    EWeatherCode: TEdit;
    btnAdd: TButton;
    btnAddChild: TButton;
    btnEdit: TButton;
    BtnDel: TButton;
    btnDownLoadGPS: TButton;
    FrameLon: TFrameLonLat;
    FrameLat: TFrameLonLat;
    tree: TTreeView;
    DQZone: TADOQuery;
    ImageList1: TImageList;
    DQItem: TADOQuery;
    DBZone: TADOTable;
    btnGetGPS: TButton;
    btnWeather: TButton;
    lbWeather: TLabel;
    btnImport: TButton;
    DQFind: TADOQuery;
    bhZone: TBalloonHint;
    procedure FormCreate(Sender: TObject);
    procedure treeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure treeChange(Sender: TObject; Node: TTreeNode);
    procedure btnAddChildClick(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure BtnDelClick(Sender: TObject);
    procedure btnDownLoadGPSClick(Sender: TObject);
    procedure btnGetGPSClick(Sender: TObject);
    procedure btnWeatherClick(Sender: TObject);
    procedure btnImportClick(Sender: TObject);
  private
    { Private declarations }
    FCurrentID, FCurrentPID: Integer;
    procedure ExpandItem(Node: TTreeNode);
    procedure AddNode(Sibling: TTreeNode; ID: Integer;
      const Code, ItemName: string; IsChild: Boolean = true);
    function GetGPS(city, address: string; var Lon, Lat: Extended): Boolean;
    procedure LoopToGetGPS(PID: Integer; PName: string);
  public
    { Public declarations }
  end;

var
  FPlatformZone: TFPlatformZone;

implementation

uses USuperObject, WinApi.wininet;

type
  TmyBtn = class(TCustomButton)
  public
    procedure UpdateImg;
  end;

procedure TmyBtn.UpdateImg;
begin
  Loaded;
end;

{$R *.dfm}

procedure TFPlatformZone.AddNode(Sibling: TTreeNode; ID: Integer;
  const Code, ItemName: string; IsChild: Boolean);
var
  newNode: TTreeNode;
begin
  if IsChild then
    newNode := tree.Items.AddChild(Sibling, ItemName + '(' + Code + ')')
  else
    newNode := tree.Items.Add(Sibling, ItemName + '(' + Code + ')');
  with newNode do
  begin
    Expanded := False;
    HasChildren := true;
    ImageIndex := Level * 2;
    SelectedIndex := Level * 2 + 1;
    StateIndex := ID;
  end;
end;

procedure TFPlatformZone.btnAddChildClick(Sender: TObject);
begin
  with DQItem do
  begin
    Parameters[0].Value := 0;
    Open;
    Append;
    FieldByName('PID').AsInteger := FCurrentID;
    FieldByName('Code').AsString := ECode.Text;
    FieldByName('ItemName').AsString := EName.Text;
    FieldByName('WeatherCode').AsString := EWeatherCode.Text;
    FieldByName('Lon').AsInteger := FrameLon.L;
    FieldByName('LonM').AsInteger := FrameLon.M;
    FieldByName('LonS').AsFloat := FrameLon.S;
    FieldByName('Lat').AsInteger := FrameLat.L;
    FieldByName('LatM').AsInteger := FrameLat.M;
    FieldByName('LatS').AsFloat := FrameLat.S;
    Post;
    AddNode(tree.Selected, FieldByName('ID').AsInteger, ECode.Text,
      EName.Text, False);
    Close;
  end;
end;

procedure TFPlatformZone.btnAddClick(Sender: TObject);
begin
  with DQItem do
  begin
    Parameters[0].Value := 0;
    Open;
    Append;
    FieldByName('PID').AsInteger := FCurrentPID;
    FieldByName('Code').AsString := ECode.Text;
    FieldByName('ItemName').AsString := EName.Text;
    FieldByName('WeatherCode').AsString := EWeatherCode.Text;
    FieldByName('Lon').AsInteger := FrameLon.L;
    FieldByName('LonM').AsInteger := FrameLon.M;
    FieldByName('LonS').AsFloat := FrameLon.S;
    FieldByName('Lat').AsInteger := FrameLat.L;
    FieldByName('LatM').AsInteger := FrameLat.M;
    FieldByName('LatS').AsFloat := FrameLat.S;
    Post;
    AddNode(tree.Selected, FieldByName('ID').AsInteger, ECode.Text, EName.Text);
    Close;
  end;
end;

procedure TFPlatformZone.BtnDelClick(Sender: TObject);
begin
  if not Assigned(tree.Selected) then
    Exit;
  with DQItem do
  begin
    Parameters[0].Value := FCurrentID;
    Open;
    Delete;
    tree.Items.Delete(tree.Selected);
    Close;
  end;
end;

procedure TFPlatformZone.btnDownLoadGPSClick(Sender: TObject);
begin
  with DQZone do
  begin
    Parameters[0].Value := 0;
    Open;
    while not EOF do
    begin
      LoopToGetGPS(FieldByName('ID').AsInteger, FieldByName('ItemName')
        .AsString);
      Next;
    end;
    Close;
  end;
end;

procedure TFPlatformZone.btnEditClick(Sender: TObject);
begin
  with DQItem do
  begin
    Parameters[0].Value := FCurrentID;
    Open;
    Edit;
    FieldByName('Code').AsString := ECode.Text;
    FieldByName('ItemName').AsString := EName.Text;
    FieldByName('WeatherCode').AsString := EWeatherCode.Text;
    FieldByName('Lon').AsInteger := FrameLon.L;
    FieldByName('LonM').AsInteger := FrameLon.M;
    FieldByName('LonS').AsFloat := FrameLon.S;
    FieldByName('Lat').AsInteger := FrameLat.L;
    FieldByName('LatM').AsInteger := FrameLat.M;
    FieldByName('LatS').AsFloat := FrameLat.S;
    Post;
    Close;
  end;
  if Assigned(tree.Selected) then
    tree.Selected.Text:= EName.Text;
end;

function UrlGetStr(const URL: string; ShowHeaders: Boolean = False): string;
const
  Agent = 'Internet Explorer 6.0';
var
  hFile, HInet: HINTERNET;
  Buffer: array [0 .. 32767] of Char;
  BufRead: Cardinal;
  BufSize: Cardinal;
  TempStream: TStringStream;
  dwIndex: dword;
begin
  HInet := InternetOpen(PChar(Agent), INTERNET_OPEN_TYPE_PRECONFIG,
    nil, nil, 0);
  if Assigned(HInet) then
    try
      hFile := InternetOpenUrl(HInet, PChar(URL), nil, 0, 0, 0);
      TempStream := TStringStream.Create('');
      dwIndex := 0;
      BufSize := SizeOf(Buffer);
      HttpQueryInfo(hFile, HTTP_QUERY_RAW_HEADERS_CRLF, @Buffer,
        BufSize, dwIndex);
      if ShowHeaders then
        TempStream.Write(Buffer, BufSize);
      if Assigned(hFile) then
        try
          with TempStream do
            try
              while InternetReadFile(hFile, @Buffer, BufSize, BufRead) and
                (BufRead > 0) do
                Write(Buffer, BufRead);
              Result := Encoding.UTF8.GetString(Bytes);
            finally
              Free;
            end;
        finally
          InternetCloseHandle(hFile);
        end;
    finally
      InternetCloseHandle(HInet);
    end;
  // Result := UTF8ToUnicodeString(Result);
end;

procedure TFPlatformZone.btnGetGPSClick(Sender: TObject);
var
  URL: Ansistring;
  vJson: ISuperObject;
  szNode: TTreeNode;

  function GetNodeValue(Node: TTreeNode): string;
  begin
    Result := Copy(Node.Text, 1, Pos('(', Node.Text) - 1);
  end;

begin
  if not Assigned(tree.Selected) then
    Exit;
  szNode := tree.Selected;
  while Assigned(szNode.Parent) do
    szNode := szNode.Parent;
  URL := 'http://api.map.baidu.com/geocoder/v2/?tmp=' + TimeToStr(now) +
    '&ak=E83cb11dbf2272a89ac819e8f0aad2e6&pois=0&output=json&city=' +
    GetNodeValue(szNode) + '&address=' + GetNodeValue(tree.Selected);
  // {"status":0,"result":{"location":{"lng":127.49111263245,"lat":44.772543560859},"precise":0,"confidence":10,"level":"\u533a\u53bf"}}
  vJson := SO(UrlGetStr(URL));
  if vJson.N['result.location'].DataType <> stNull then
  begin
    FrameLon.Value := vJson.N['result.location.lng'].AsDouble;
    FrameLat.Value := vJson.N['result.location.lat'].AsDouble;
  end;
end;

procedure TFPlatformZone.btnImportClick(Sender: TObject);
var
  F: TextFile;
  S, szCode: string;
  Idx: Integer;
  function FindPID(ACode: string): Integer;
  begin
    with DQFind do try
      repeat
        Close;
        ACode:= Copy(ACode, 1, Length(ACode) - 2);
        if ACode = '' then Break;
        Parameters[0].Value:= ACode;
        Open;
      until (not EOF);
      if EOF then Result:= 0
      else Result:= FieldByName('ID').AsInteger;
    finally
      Close;
    end;
  end;
begin
  DBZone.Open;
  with TOpenDialog.Create(nil) do try
    Filter:= '文本文件|*.txt|所有文件|*.*';
    if not Execute then Exit;
    AssignFile(F, Filename);
    Reset(F);
    repeat
      Readln(F, S);
      if S = '' then Continue;
      szCode:= Copy(S, 1, Pos(#09, S) - 1);
      if not DBZone.Locate('Code', szCode, []) then
        DBZone.Append
      else DBZone.Edit;
      DBZone.FieldByName('PID').AsInteger := FindPID(szCode);
      DBZone.FieldByName('Code').AsString := szCode;
      DBZone.FieldByName('ItemName').AsString := Copy(S, Length(szCode) + 2, MaxInt);
      DBZone.Post;
    until EOF(F);
  finally
    Free;
  end;
end;

procedure TFPlatformZone.btnWeatherClick(Sender: TObject);
var
  URL: Ansistring;
  vJson: ISuperObject;
  szNode: TTreeNode;

  function GetNodeValue(Node: TTreeNode): string;
  begin
    Result := Copy(Node.Text, 1, Pos('(', Node.Text) - 1);
  end;

begin
  if not Assigned(tree.Selected) then
    Exit;
  if EWeatherCode.Text = '' then
    Exit;
  URL := 'http://www.weather.com.cn/data/sk/' + EWeatherCode.Text + '.html';
  // {"weatherinfo":{"city":"望江","cityid":"101220607","temp":"11","WD":"东风","WS":"2级","SD":"76%","WSE":"2","time":"20:10","isRadar":"0","Radar":""}}
  vJson := SO(UrlGetStr(URL));
  if vJson.N['weatherinfo'].DataType <> stNull then
    lbWeather.Caption :=
      Format('城市:%s '#13#10'温度:%f℃ '#13#10'风向:%s '#13#10'风力:%s '#13#10'湿度:%s ',
      [vJson.N['weatherinfo.city'].AsString,
      vJson.N['weatherinfo.temp'].AsDouble, vJson.N['weatherinfo.WD'].AsString,
      vJson.N['weatherinfo.WS'].AsString, vJson.N['weatherinfo.SD'].AsString]);
end;

procedure TFPlatformZone.ExpandItem(Node: TTreeNode);
begin
  with DQZone do
  begin
    if not Assigned(Node) then
      Parameters[0].Value := 0
    else
      Parameters[0].Value := Node.StateIndex;
    Open;
    while not EOF do
    begin
      AddNode(Node, FieldByName('ID').AsInteger, FieldByName('Code').AsString,
        FieldByName('ItemName').AsString);
      Next;
    end;
    Close;
  end;
end;

procedure TFPlatformZone.FormCreate(Sender: TObject);
begin
  FCurrentPID := 0;
  DQZone.Connection:= FPlatformDB.ADOConn;
  DQItem.Connection:= FPlatformDB.ADOConn;
  DBZone.Connection:= FPlatformDB.ADOConn;
  ExpandItem(nil);
end;

function TFPlatformZone.GetGPS(city, address: string;
  var Lon, Lat: Extended): Boolean;
var
  vJson: ISuperObject;
  URL: string;
begin
  URL := 'http://api.map.baidu.com/geocoder/v2/?tmp=' + TimeToStr(now) +
    '&ak=E83cb11dbf2272a89ac819e8f0aad2e6&pois=0&output=json&city=' + city +
    '&address=' + address;
  // {"status":0,"result":{"location":{"lng":127.49111263245,"lat":44.772543560859},"precise":0,"confidence":10,"level":"\u533a\u53bf"}}
  vJson := SO(UrlGetStr(URL));
  Result := vJson.N['result.location'].DataType <> stNull;
  if Result then
  begin
    Lon := vJson.N['result.location.lng'].AsDouble;
    Lat := vJson.N['result.location.lat'].AsDouble;
  end;
end;

procedure TFPlatformZone.LoopToGetGPS(PID: Integer; PName: string);
var
  Lon, Lat: Extended;
begin
  with TADOQuery.Create(nil) do
    try
      Connection := FPlatformDB.ADOConn;
      SQL.Text := 'SELECT * FROM SYS_ZONE WHERE PID=' + IntToStr(PID);
      Open;
      while not EOF do
      begin
        if GetGPS(PName, FieldByName('ItemName').AsString, Lon, Lat) then
        begin;
          Edit;
          FieldByName('Lon').AsInteger := Trunc(Lon);
          Lon := (Lon - Trunc(Lon)) * 60;
          FieldByName('LonM').AsInteger := Trunc(Lon);
          Lon := (Lon - Trunc(Lon)) * 60;
          FieldByName('LonS').AsFloat := Lon;

          FieldByName('Lat').AsInteger := Trunc(Lat);
          Lat := (Lat - Trunc(Lat)) * 60;
          FieldByName('LatM').AsInteger := Trunc(Lat);
          Lat := (Lat - Trunc(Lat)) * 60;
          FieldByName('LatS').AsFloat := Lat;
          Post;
        end;
        LoopToGetGPS(FieldByName('ID').AsInteger, PName);
        Next;
      end;
    finally
      Free;
    end;
end;

procedure TFPlatformZone.treeChange(Sender: TObject; Node: TTreeNode);
begin
  with DQItem do
  begin
    Parameters[0].Value := Node.StateIndex;
    Open;
    FCurrentID := FieldByName('ID').AsInteger;
    FCurrentPID := FieldByName('PID').AsInteger;
    ECode.Text := FieldByName('Code').AsString;
    EName.Text := FieldByName('ItemName').AsString;
    EWeatherCode.Text := FieldByName('WeatherCode').AsString;
    FrameLon.L := FieldByName('Lon').AsInteger;
    FrameLon.M := FieldByName('LonM').AsInteger;
    FrameLon.S := FieldByName('LonS').AsFloat;
    FrameLat.L := FieldByName('Lat').AsInteger;
    FrameLat.M := FieldByName('LatM').AsInteger;
    FrameLat.S := FieldByName('LatS').AsFloat;
    Close;
  end;
end;

procedure TFPlatformZone.treeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  if Node.HasChildren and (Node.Count = 0) then
    ExpandItem(Node);
end;

end.
