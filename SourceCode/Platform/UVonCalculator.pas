unit UVonCalculator;

interface

uses SysUtils, Contnrs, Forms, Classes, Math, Dialogs, Controls, UVonSystemFuns,
  UVonLog;

const
  ERROR_CALC_NOFOUNDMARK = '标记‘%s’未能在文本‘%s’中发现，系统无法正常运行。';
  CRLF : string = #13 + #10;
  (* Boolean string *)
  STR_TRUE      : string = '1';
  STR_FALSE     : string = '0';

type
  TCalc_Unit = class;

  TCalc_DoUserFunction_Event = function (FunctionName: string; CurrentCalcObject: TCalc_Unit): string of object;
  TCalc_GetNameValue_Event = function (ValueName: string): string of object;

  TVonCalculator = class
  private
    FCurrentIDX: array[1..10]of Integer;
    FCurrentID: array[1..10]of Integer;
    FCurrentPChar: PChar;
    FCaleValue: string;
    FStrBuffer: TStringList;
  protected
    FOnDoUserFunction: TCalc_DoUserFunction_Event;
    FOnGetNameValue: TCalc_GetNameValue_Event;
    function DoOtherFunction(FunctionName: string; CurrentCalcObject: TCalc_Unit): string; virtual;
    function GetNameValue(ValueName: string): string; virtual;
  public
    constructor Create;
    destructor Destroy; virtual;
    function CalcText(Text: string): string;
    function CalcFormula(Text: string): string;
  published
    property OnDoUserFunction: TCalc_DoUserFunction_Event read FOnDoUserFunction write FOnDoUserFunction;
    property OnGetNameValue: TCalc_GetNameValue_Event read FOnGetNameValue write FOnGetNameValue;
    property CaleValue: string read FCaleValue write FCaleValue;
  end;

  TCalc_Unit = class
  private
    FValue: string;
    FUnits: TObjectList;
    FFunctionName: string;
    FOprator: Char;
    FCalc: TVonCalculator;
    FOnDoUserFunction: TCalc_DoUserFunction_Event;
    FOnGetNameValue: TCalc_GetNameValue_Event;
    function GetUnit(Index: Integer): TCalc_Unit;
    function GetCount: Integer;
    procedure SetFunctionName(const Value: string);
    procedure SetOprator(const Value: Char);
    procedure SetValue(const Value: string);
    function GetValue: string;
    function CalcTowValue(Value1: string; Opt: Char; Value2: string): string;
    function DoFunction(FunctionName: string): string;
    function Fun_IF(): string;
    function Fun_CASE(): string;
    function Fun_ABS(): string;
    function Fun_Round(): string;
    function Fun_Trunc(): string;
    function Fun_CRLF(): string;
    function Fun_Format(): string;
    function Fun_Trim(): string;
    function Fun_Len(): string;
    function Fun_ID(): string;
    function Fun_IDX(): string;
    function Fun_Loop(): string;
    function Fun_Now(): string;
    function Fun_Space(): string;
    function Fun_UpperCash(): string;       //金额大写
    function Fun_UpperNum(): string;        //数字大写
    function Fun_UpperDigit(): string;      //数字大写
    function Fun_DateStr(): string;
    function Fun_DataIn(): string;
    function Fun_Split(): string;
    function Fun_BufferValue(): string;
  public
    constructor Create(ACalc: TVonCalculator);
    destructor Destroy; override;
    function Analyze(FormulaText: PChar): PChar;
    function AddUnit(): TCalc_Unit;
    procedure GetParam(Index: Integer; var Value: string; var ValueType: TValueType);
    property Units[Index: Integer]: TCalc_Unit read GetUnit;
  published
    property Count: Integer read GetCount;
    property Value: string read GetValue write SetValue;
    property FunctionName: string read FFunctionName write SetFunctionName;
    property Oprator: Char read FOprator write SetOprator;
    property OnDoUserFunction: TCalc_DoUserFunction_Event read FOnDoUserFunction write FOnDoUserFunction;
    property OnGetNameValue: TCalc_GetNameValue_Event read FOnGetNameValue write FOnGetNameValue;
  end;

  function UpperCash(Value: Double): string;  //金额大写
  function UpperNum(Value: Double): string;   //数值大写
  function UpperDigit(Value: Double): string; //数字大写
  function AnalyzeCellName(CellName: string; var aPage, aCol, aRow: Integer): Boolean;
  function GetCellName(const aPage, aCol, aRow: Integer): string;
  function GetColumnName(const aCol: Integer): string;

implementation

function AnalyzeCellName(CellName: string; var aPage, aCol, aRow: Integer): Boolean;
var
  i, level: Integer;
  S1, S2, S3: string;
  IsNum: Boolean;

  procedure AddS(ch: char);
  begin
    case level of
    0: S1:= S1 + ch;
    1: S2:= S2 + ch;
    2: S3:= S3 + ch;
    end;
  end;

  function GetColID(): Integer;
  var
    k: Integer;
  begin
    Result:= 0;
    for k := 1 to Length(S2) do
      Result:= Result * 26 + Ord(S2[k]) - 65;
    Result:= Result + 1;
  end;

begin
  level:= 0; S1:= ''; S2:= ''; S3:= ''; IsNum:= True;
  Result:= False;
  if Length(CellName) < 2 then Exit;
  for I := 1 to Length(CellName) do begin
    if(Pos(CellName[i], '0123456789') > 0)then begin
      if level = 1 then level:= 2;
      Adds(CellName[i])
    end else if Pos(CellName[i], 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') > 0 then begin
      if level > 1 then Exit;
      level:= 1;
      Adds(CellName[i])
    end else Exit;
  end;
  if S1 <> '' then aPage:= StrToInt(S1);
  if S2 = '' then Exit; aCol:= GetColID;
  if S3 = '' then Exit; aRow:= StrToInt(S3);
  Result:= True;
end;

function GetCellName(const aPage, aCol, aRow: Integer): string;
const
  COLNAME : string[26] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
var
  szCol: Integer;
  S: string;
begin
  S:= '';  szCol:= aCol;
  repeat
    S:= COLNAME[szCol mod 26] + S;
    szCol:= szCol div 26;
  until szCol = 0;
  Result:= IntToStr(aPage) + S + IntToStr(aRow);
end;

function GetColumnName(const aCol: Integer): string;
const
  COLNAME : string[26] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
var
  szCol: Integer;
begin
  Result:= ''; szCol:= aCol;
  repeat
    Result:= COLNAME[szCol mod 26] + Result;
    szCol:= szCol div 26;
  until szCol = 0;
end;

function UpperNum(Value: Double): string;
const
  s1: string = '零壹贰叁肆伍陆柒捌玖';
  s2: string = '点拾佰仟万拾佰仟亿拾佰仟万';
var
  s, dx: string;
  i, Len, zeroPos: Integer;

  function StrTran(const S, S1, S2: String): String;
  begin
    Result := StringReplace(S, S1, S2, [rfReplaceAll]);
  end;
begin
  if Value < 0 then begin
    dx:= '负';
    Value:= -Value;
  end;
  s:= FloatToStr(Value);         //123.234
  zeroPos:= Pos('.', s);             //1234567
  Len:= Length(s);
  if zeroPos = 0 then zeroPos:= Len + 1;
  for i := 1 to zeroPos - 1 do
    dx := dx + Copy(s1, (Ord(s[i]) - Ord('0')) * 2 + 1, 2) + Copy(s2, (zeroPos - i) * 2 - 1, 2);
  dx := StrTran(StrTran(StrTran(dx, '零仟', '零'), '零佰', '零'), '零拾', '零');
  dx := StrTran(StrTran(StrTran(StrTran(dx, '零零', '零'), '零零', '零'), '零亿', '亿'), '零万', '万');
  for i := zeroPos + 1 to Len do
    dx := dx + Copy(s1, (Ord(s[i]) - Ord('0')) * 2 + 1, 2);
  if Copy(dx, 1, 4) = '壹拾' then Delete(dx, 1, 2);
  if dx = '零点' then dx:= '零' else dx:= StrTran(dx, '零点', '点');
  if Copy(dx, Length(dx) - 1, 2) = '点' then Delete(dx, Length(dx) - 1, 2);
  Result := StrTran(dx, '亿万', '亿');
end;

function UpperCash(Value: Double): string;
const
  s1: string = '零壹贰叁肆伍陆柒捌玖';
  s2: string = '分角元拾佰仟万拾佰仟亿拾佰仟万';
var
  s, dx: string;
  i, Len: Integer;

  function StrTran(const S, S1, S2: String): String;
  begin
    Result := StringReplace(S, S1, S2, [rfReplaceAll]);
  end;
begin
  if Value < 0 then begin
    dx:= '负';
    Value:= -Value;
  end;
  s:= Format('%.0f', [Value * 100]);
  Len:= Length(s);
  for i := 1 to Len do
    dx := dx + Copy(s1, (Ord(s[i]) - Ord('0')) * 2 + 1, 2) + Copy(s2, (Len - i) * 2 + 1, 2);
    dx := StrTran(StrTran(StrTran(StrTran(StrTran(dx, '零仟', '零'), '零佰', '零'), '零拾', '零'), '零角', '零'), '零分', '整');
    dx := StrTran(StrTran(StrTran(StrTran(StrTran(dx, '零零', '零'), '零零', '零'), '零亿', '亿'), '零万', '万'), '零元', '元');
  if dx = '整' then Result := '零元整'
  else Result := StrTran(StrTran(dx, '亿万', '亿'), '零整', '整');
end;

function UpperDigit(Value: Double): string;
const
  s1: string = '〇一二三四五六七八九';
var
  s, dx: string;
  i, Len: Integer;
begin
  if Value < 0 then begin
    dx:= '负';
    Value:= -Value;
  end;
  s:= FloatToStr(Value);
  Len:= Length(s);
  for i := 1 to Len do
    if s[i] = '.' then dx := dx + '点'
    else dx := dx + Copy(s1, (Ord(s[i]) - Ord('0')) * 2 + 1, 2);
  Result:= dx;
end;

function FindNextMark(var PMark: PChar): string;
var
  Mark: Char;
begin    //得到引号或双引号等标记字符之间的内容
  Mark:= PMark[0];
  Inc(PMark);
  Result:= '';
  while PMark[0] <> Char(0) do begin
    if(PMark[0] = Mark)then begin
      Inc(PMark);
      if(PMark[0] = Char(0))or(PMark[0] <> Mark) then Exit;  //Double = single
    end;
    Result:= Result + PMark[0];
    Inc(PMark);
  end;
  raise Exception.Create(Format(ERROR_CALC_NOFOUNDMARK, [Mark, string(PMark)]));
end;

function GetOperaterLevel(Operater: Char): word;
begin    //得到计算符的优先级
  case Operater of
  '+': Result:= 3;      //数值加法
  '-': Result:= 3;      //数值减法
  '*': Result:= 4;      //数值乘法
  '/': Result:= 4;      //数值除法
  '%': Result:= 5;      //数值取余
  '\': Result:= 5;      //数值整除
  '^': Result:= 5;      //指数运算
  '&': Result:= 3;      //字符合并
  '|': Result:= 2;      //逻辑或
  '@': Result:= 2;      //逻辑与
  '>': Result:= 1;      //逻辑大于
  '=': Result:= 1;      //逻辑等于
  '<': Result:= 1;      //逻辑小于
  ']': Result:= 1;      //逻辑大于等于
  '[': Result:= 1;      //逻辑小于等于
  '!': Result:= 0;      //逻辑不等于
  end;
end;

{ TCalc }

function TVonCalculator.CalcFormula(Text: string): string;
var
  FCalcUnit: TCalc_Unit;

  procedure DispalyUnit(AUnit: TCalc_Unit);
  var
    i: Integer;
    S: string;
  begin
    S:= '';
    for i:= 0 to AUnit.Count - 1 do begin
      if AUnit.Units[i].FFunctionName <> '' then
        S:= S + AUnit.Units[i].FFunctionName + ':' + AUnit.Units[i].FValue + AUnit.Units[i].Oprator
      else S:= S + AUnit.Units[i].FValue + AUnit.Units[i].Oprator;
      if AUnit.Units[i].Count > 0 then DispalyUnit(AUnit.Units[i]);
    end;
    WriteLog(LOG_DEBUG, 'CalcFormula', S);
  end;
begin    //计算公式内容
  Result:= '';
  if Text <> '' then FCurrentPChar:= PChar(Text);
  if(FCurrentPChar[0] = Char(0))or(FCurrentPChar[0] <> '=')then Exit;
  Inc(FCurrentPChar);
  FCalcUnit:= TCalc_Unit.Create(self);
  try
    FCalcUnit.OnDoUserFunction:= DoOtherFunction;
    FCalcUnit.OnGetNameValue:= GetNameValue;
    FCurrentPChar:= FCalcUnit.Analyze(FCurrentPChar);
//    DispalyUnit(FCalcUnit);
    Result:= FCalcUnit.Value;
    FCalcUnit.free;
  except  //错误保护，并提示系统是否继续运行
    on E: Exception do begin
      FCalcUnit.free;
      WriteLog(LOG_FAIL, 'CalcFormula', Text + CRLF + E.Message);
      if MessageDlg('计算"' + Text + '"' + CRLF + '发生错误：' + E.Message + CRLF +
        '是否继续?', mtWarning, mbOKCancel, 0) <> mrOK then
        raise Exception.Create('系统意外终止');
    end;
  end;
end;

function TVonCalculator.CalcText(Text: string): string;
var
  szChar: Char;
begin    //计算文本（内部可能含有公式计算）
  Result:= '';
  if Text = '' then Exit;
  WriteLog(LOG_DEBUG, 'CalcText', Text);
  FCurrentPChar:= PChar(Text);
  FCaleValue:= '';
  repeat
    szChar:= FCurrentPChar[0];
    case szChar of
    '''', '"': Result:= Result + szChar + FindNextMark(FCurrentPChar) + szChar;
    '\': begin Inc(FCurrentPChar); FCaleValue:= FCaleValue + FCurrentPChar[0]; Inc(FCurrentPChar); end;
    '=': FCaleValue:= FCaleValue + CalcFormula('');
    else begin Inc(FCurrentPChar); FCaleValue:= FCaleValue + szChar; end;
    end;
  until FCurrentPChar[0] = Char(0);
  Result:= FCaleValue;
  WriteLog(LOG_DEBUG, 'Calc result', Result);
end;

constructor TVonCalculator.Create;
begin
  FStrBuffer:= TStringList.Create;
end;

destructor TVonCalculator.Destroy;
begin
  FStrBuffer.Free;
end;

function TVonCalculator.DoOtherFunction(FunctionName: string; CurrentCalcObject: TCalc_Unit): string;
begin
  if Assigned(FOnDoUserFunction) then Result:= FOnDoUserFunction(FunctionName, CurrentCalcObject);
end;

function TVonCalculator.GetNameValue(ValueName: string): string;
begin
  if Assigned(FOnGetNameValue) then FOnGetNameValue(ValueName);
end;

{ TCalc_Unit }

function TCalc_Unit.AddUnit: TCalc_Unit;
begin
  Result:= TCalc_Unit.Create(FCalc);
  Result.OnDoUserFunction:= FOnDoUserFunction;
  Result.OnGetNameValue:= FOnGetNameValue;
  FUnits.Add(Result);
end;
//=32*(24-Month()*2)+2*if(3>4,12,2)
function TCalc_Unit.Analyze(FormulaText: PChar): PChar;
var
  currentUnit: TCalc_Unit;
  FunName: string;
  HasCalc: Boolean;
  S: string;

  function GetUnit: TCalc_Unit;
  begin
    if currentUnit = nil then
      currentUnit:= AddUnit;
    Result:= currentUnit;
  end;
begin
  currentUnit:= nil;
  Result:= FormulaText;
  HasCalc:= True;
  (* Create a unit to first formula *)
  //szUnit:= AddUnit;
  while FormulaText[0] <> Char(0) do
  case FormulaText[0] of
  '(': begin
      //szUnit.FunctionName:= FunName;
      GetUnit.FunctionName:= FunName;
      repeat
        Inc(FormulaText);
        //FormulaText:= szUnit.AddUnit.Analyze(FormulaText);
        FormulaText:= GetUnit.AddUnit.Analyze(FormulaText);
      until FormulaText[0] = ')';
      Inc(FormulaText);
      HasCalc:= False;
      FunName:= '';
    end;
  ')', ',': begin
      //if FunName <> '' then szUnit.Value:= FunName;
      if FunName <> '' then GetUnit.Value:= FunName;
      Result:= FormulaText;
      Exit;
    end;
  '+', '-', '*', '/', '%', '\', '^', '&', '@', '|': begin //^=Exp() %=Div @=Mod  '+','-','*','/','%','\','^','&','@','|','>','=','<',']','[','!',
      //if FunName <> '' then szUnit.Value:= FunName;
      if FunName <> '' then GetUnit.Value:= FunName;
      //szUnit.Oprator:= FormulaText[0];
      GetUnit.Oprator:= FormulaText[0];
      Inc(FormulaText);
  (* Create a unit to next formula *)
      //szUnit:= AddUnit;
      currentUnit:= nil;
      HasCalc:= True;
      FunName:= '';
    end;
  '>': begin
      Inc(FormulaText);
      //if FunName <> '' then szUnit.Value:= FunName;
      if FunName <> '' then GetUnit.Value:= FunName;
      if(FormulaText[0] <> Char(0))and(FormulaText[0] = '=')then begin
        Inc(FormulaText);
        //szUnit.Oprator:= ']';
        GetUnit.Oprator:= ']';
      //end else szUnit.Oprator:= '>';
      end else GetUnit.Oprator:= '>';
      //szUnit:= AddUnit;
      currentUnit:= nil;
      HasCalc:= True;
      FunName:= '';
    end;
  '<': begin
      Inc(FormulaText);
      //if FunName <> '' then szUnit.Value:= FunName;
      if FunName <> '' then GetUnit.Value:= FunName;
      if(FormulaText[0] <> Char(0))and(FormulaText[0] = '=')then begin
        Inc(FormulaText);
        //szUnit.Oprator:= '[';
        GetUnit.Oprator:= '[';
      end else if(FormulaText[0] <> Char(0))and(FormulaText[0] = '>')then begin
        Inc(FormulaText);
        //szUnit.Oprator:= '!';
        GetUnit.Oprator:= '!';
      //end else szUnit.Oprator:= '<';
      end else GetUnit.Oprator:= '<';
      //szUnit:= AddUnit;
      currentUnit:= nil;
      HasCalc:= True;
      FunName:= '';
    end;
  '!': begin
      Inc(FormulaText);
      //if FunName <> '' then szUnit.Value:= FunName;
      if FunName <> '' then GetUnit.Value:= FunName;
      if(FormulaText[0] <> Char(0))and(FormulaText[0] = '=')then
        Inc(FormulaText);
      //szUnit.Oprator:= '!';
      GetUnit.Oprator:= '!';
      //szUnit:= AddUnit;
      currentUnit:= nil;
      HasCalc:= True;
      FunName:= '';
    end;
  '=': begin
      //if FunName <> '' then szUnit.Value:= FunName;
      if FunName <> '' then GetUnit.Value:= FunName;
      if FormulaText[0] <> Char(0) then begin
        if FormulaText[0] = '=' then begin
          Inc(FormulaText);
          //szUnit.Oprator:= '=';
          GetUnit.Oprator:= '=';
        end;
        if FormulaText[0] = '>' then begin
          Inc(FormulaText);
          //szUnit.Oprator:= ']';
          GetUnit.Oprator:= ']';
        end;
        if FormulaText[0] = '<' then begin
          Inc(FormulaText);
          //szUnit.Oprator:= '[';
          GetUnit.Oprator:= '[';
        end;
      end;
      //szUnit:= AddUnit;
      currentUnit:= nil;
      HasCalc:= True;
      FunName:= '';
    end;
  '''', '"': begin
      S:= FindNextMark(FormulaText);
      if S = '' then with GetUnit.AddUnit do begin
        FunctionName:= 'SPACE';
        AddUnit.Value:= '0';
      end else FunName:= FunName + S;
    end;
  ' ': begin
      Inc(FormulaText);
      Continue;
    end;
  else begin
      if not HasCalc then begin
        //if FunName <> '' then szUnit.Value:= FunName;
        if FunName <> '' then GetUnit.Value:= FunName;
        Result:= FormulaText;
        Exit;
      end;
      if(FunName = '')and(Pos(FormulaText[0], '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ.') > 0)then
        while Pos(FormulaText[0], '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ.') > 0 do begin
          FunName:= FunName + FormulaText[0];
          Inc(FormulaText);
          HasCalc:= False;
      end else begin
        FunName:= FunName + FormulaText[0];
        Inc(FormulaText);
      end;
    end;
  end;
  //if FunName <> '' then szUnit.Value:= FunName;
  if FunName <> '' then GetUnit.Value:= FunName;
  Result:= FormulaText;
end;

function TCalc_Unit.CalcTowValue(Value1: string; Opt: Char;
  Value2: string): string;
var
  szV1, szV2, szV3: double;

  procedure CheckValue();
  begin
    if not TryStrToFloat(Trim(Value1), szV1) then szV1:= 0;
    if not TryStrToFloat(Trim(Value2), szV2) then szV2:= 0;
  end;
begin    //得到计算符的优先级
  Result:= '';
  try
    CheckValue;
    case Opt of
    '+': Result:= FloatToStr(szV1 + szV2);                        //数值加法
    '-': Result:= FloatToStr(szV1 - szV2);                        //数值减法
    '*': Result:= FloatToStr(szV1 * szV2);                        //数值乘法
    '/': if szV2 = 0 then Result:= '0'
        else begin
          szV3:= szV1 / szV2;
          Result:= FloatToStr(szV3);                              //数值除法
        end;
    '%': if szV2 = 0 then Result:= IntToStr(Trunc(szV1))
        else Result:= IntToStr(Trunc(szV1) mod Trunc(szV2));      //数值取余
    '\': if szV2 = 0 then Result:= '0'
        else Result:= IntToStr(Trunc(szV1) div Trunc(szV2));      //数值整除
    '^': Result:= FloatToStr(Power(szV1, szV2));                  //指数运算
    '&': Result:= Value1 + Value2;                                //字符合并
    '@': if((Trunc(szV1)and Trunc(szV2)) > 0) then Result:= '1' else Result:= '0';     //逻辑与
    '|': if((Trunc(szV1)or Trunc(szV2)) > 0) then Result:= '1' else Result:= '0';      //逻辑或
    '>': if szV1 >  szV2 then Result:= '1' else Result:= '0';     //逻辑大于
    '=': if Value1 =  Value2 then Result:= '1' else Result:= '0'; //逻辑等于
    '<': if szV1 <  szV2 then Result:= '1' else Result:= '0';     //逻辑小于
    ']': if szV1 >= szV2 then Result:= '1' else Result:= '0';     //逻辑大于等于
    '[': if szV1 <= szV2 then Result:= '1' else Result:= '0';     //逻辑小于等于
    '!': if Value1 <> Value2 then Result:= '1' else Result:= '0'; //逻辑不等于
    end;
  except
    Result:= '';
  end;
  WriteLog(LOG_DEBUG, 'CalcTowValue', Format('%s%s%s=%s', [Value1, Opt, Value2, Result]));
end;

constructor TCalc_Unit.Create(ACalc: TVonCalculator);
begin
  FUnits:= TObjectList.Create;
  FCalc:= ACalc;
end;

destructor TCalc_Unit.Destroy;
begin
  FUnits.Clear;
  FUnits.Free;
  inherited;
end;

function TCalc_Unit.DoFunction(FunctionName: string): string;
var
  FunName: string;
begin
  FunName:= UpperCase(FunctionName);
  if FunName = 'IF' then Result:= Fun_IF()
  else if FunName = 'CASE' then Result:= Fun_Case()
  else if FunName = 'FORMAT' then Result:= Fun_Format()
  else if FunName = 'ABS' then Result:= Fun_ABS()
  else if FunName = 'ROUND' then Result:= Fun_Round()
  else if FunName = 'TRUNC' then Result:= Fun_Trunc()
  else if FunName = 'CRLF' then Result:= Fun_CRLF()
  else if FunName = 'SPACE' then Result:= Fun_Space()
  else if FunName = 'TRIM' then Result:= Fun_Trim()
  else if FunName = 'LEN' then Result:= Fun_Len()
  else if FunName = 'NOW' then Value:= DateToStr(Now)
  else if FunName = 'DATAIN' then Result:= Fun_DataIn()
  else if FunName = 'ID' then Result:= Fun_ID()
  else if FunName = 'IDX' then Result:= Fun_IDX()
  else if FunName = 'LOOP' then Result:= Fun_Loop()
  else if FunName = 'UPPERCASH' then Result:= Fun_UpperCash()     //金额大写
  else if FunName = 'UPPERNUM' then Result:= Fun_UpperNum()       //数字大写
  else if FunName = 'UPPERDIGIT' then Result:= Fun_UpperDigit()   //数字大写
  else if FunName = 'SPLIT' then Result:= Fun_Split()
  else if FunName = 'BUFFERVALUE' then Result:= Fun_BufferValue()
  else if Assigned(FOnDoUserFunction) then
    Result:= FOnDoUserFunction(FunName, self);
  WriteLog(LOG_DEBUG, 'DoFunction', FunctionName + '=' + Result);
end;

function TCalc_Unit.Fun_ABS: string;
var
  sv: Double;
begin    //The ABS function
  if Count <> 1 then raise Exception.Create('The "ABS" Syntax is ABS(<value>).');
  if TryStrToFloat(Units[0].Value, sv) then
    Result:= FloatToStr(ABS(sv))
  else Result:= '-';
end;

function TCalc_Unit.Fun_BufferValue: string;
var
  sv: Integer;
begin
  if Count <> 1 then raise Exception.Create('The "BufferValue" Syntax is BufferValue(<index>).');
  if TryStrToInt(Units[0].Value, sv) then
    Result:= FCalc.FStrBuffer[sv]
  else Result:= '-';
end;

function TCalc_Unit.Fun_CASE: string;
var
  szValue: Double;
begin    //The Case function
  if Count < 4 then raise Exception.Create('The "case" Syntax is CASE(<value>, <positive_Result>, <zero>, <negative_Result>).');
  if not TryStrToFloat(Units[0].Value, szValue) then szValue:= 0;
  if szValue > 0 then Result:= Units[3].Value
  else if szValue = 0 then Result:= Units[2].Value
  else Result:= Units[1].Value;
end;

function TCalc_Unit.Fun_CRLF: string;
begin
  Result:= #13 + #10;
end;

function TCalc_Unit.Fun_DataIn: string;
var
  i: Integer;
begin
  Result:= STR_FALSE;
  if Count < 1 then raise Exception.Create('The "DateIn" Syntax is DataIn(<Data_Value>, <Const>[,<Const>[..]])');
  if Count = 2 then begin
    if Pos(Units[0].Value, Units[1].Value) > 0 then Result:= STR_TRUE
    else Result:= STR_FALSE;
  end else for I := 1 to Count - 1 do
    if UpperCase(Units[0].Value) = UpperCase(Units[I].Value) then begin
      Result:= STR_TRUE;
      Exit;
    end
end;

function TCalc_Unit.Fun_DateStr: string;
begin
  if Count <> 2 then raise Exception.Create('The "DateStr" Syntax is DateStr(<format_string>, <Date_Value>)');
  Result:= FormatDateTime(Units[0].Value, StrToDateTime(Units[1].Value));
end;

function TCalc_Unit.Fun_Format: string;
var
  Args: array of TVarRec;
  i: Integer;
  spCurr: PExtended;
  szCurr: Extended;
begin    //The Format function
  if Count < 2 then raise Exception.Create('The "format" Syntax is FORMAT(<format_string>, <value>[,<Value>[...]])');
  SetLength(Args, Count - 1);
  for i:= 1 to Count - 1 do begin
    New(spCurr);
    if not TextToFloat(PChar(Units[i].Value), szCurr, fvExtended) then
      szCurr:= 0;
    spCurr:= @szCurr;
    Args[i - 1].VExtended:= spCurr;
    Args[i - 1].VType := vtExtended;
//    Args[i - 1].VCurrency:= PCurrency(StrToCurr(Units[i].Value));
  end;
  Result:= Format(Units[0].Value, Args);
  SetLength(Args, 0);
end;

function TCalc_Unit.Fun_ID: string;
var
  idx: Integer;
begin    //The ID function
  Result:= '';
  if Count < 1 then raise Exception.Create('The "ID" Syntax is ID(<Index>, [<Init_Value>])');
  idx:= StrToInt(Units[0].Value);
  if(idx < 1)or(idx > 10)then
    raise Exception.Create('The <Index> value has advanced from 1 toward 10 of ID(<Index>, [<Init_Value>]). ');
  if Count = 2 then FCalc.FCurrentID[idx]:= StrToInt(Units[1].Value)
  else begin
    Result:= IntToStr(FCalc.FCurrentID[idx]);
    Inc(FCalc.FCurrentID[idx]);
  end;
end;

function TCalc_Unit.Fun_IDX: string;
begin    //The IDX function
  case Count of
  0: Result:= IntToStr(FCalc.FCurrentIDX[1]);
  1: Result:= IntToStr(FCalc.FCurrentIDX[StrToInt(Units[0].Value)]);
  else raise Exception.Create('The "IDX" Syntax is IDX([<Index>])');
  end;
end;

function TCalc_Unit.Fun_IF(): string;
begin    //The if function
  if Count < 3 then raise Exception.Create('The "if" Syntax is IF(<condition>, <True_Result>, <False_Result>).');
  if Units[0].Value = '1' then
    Result:= Units[1].Value
  else
    Result:= Units[2].Value;
end;

function TCalc_Unit.Fun_Len: string;
begin    //The if function
  if Count > 1 then raise Exception.Create('The "len" Syntax is LEN(<Value>).');
  Result:= IntToStr(Length(Units[0].Value));
end;

function TCalc_Unit.Fun_Loop: string;
var
  i, A, B, C: Integer;
begin    //The Loop function
  Result:= '';
  case Count of
  3: begin
    A:= StrToInt(Units[0].Value);
    B:= StrToInt(Units[1].Value);
    C:= 1;
  end;
  4: begin
    C:= StrToInt(Units[0].Value);
    A:= StrToInt(Units[1].Value);
    B:= StrToInt(Units[2].Value);
  end;
  else raise Exception.Create('The "Loop" Syntax is Loop([<Index>], <Start_Value>, <End_Value>, <Result>).');
  end;
  for i:= A to B do begin
    FCalc.FCurrentIDX[C]:= i;
    Result:= Result + Units[Count - 1].Value;
  end;
end;

function TCalc_Unit.Fun_Now: string;
begin
  if Count <> 1 then raise Exception.Create('The "trim" Syntax is NOW(<Format>).');
  Result:= FormatDateTime(Units[0].Value, Now);
end;

function TCalc_Unit.Fun_Round: string;
var
  sv: Double;
  rate: Integer;
begin
  if Count > 2 then raise Exception.Create('The "Round" Syntax is Round(<Value>,[<Digit>]).');
  if not TryStrToFloat(Units[0].Value, sv)then sv:= 0;
  if Count = 1 then Result:= IntToStr(Trunc(sv + 0.5000000001))
  else Result:= FloatToStr(Trunc(sv * Power(10, StrToInt(Units[1].Value)) + 0.5000000001) / Power(10, StrToInt(Units[1].Value)));
end;

function TCalc_Unit.Fun_Space: string;
begin
  if Count <> 1 then raise Exception.Create('The "Space" Syntax is SPACE(<Space_Count>).');
  if Units[0].Value = '0' then Result:= ''
  else FillChar(Result, StrToInt(Units[0].Value), ' ');
end;

function TCalc_Unit.Fun_Split: string;
begin
  if Count < 1 then raise Exception.Create('The "Split" Syntax is SPLIT(<Space_Count>).');
  if Count = 1 then begin
    FCalc.FStrBuffer.Delimiter:= ',';
    FCalc.FStrBuffer.DelimitedText:= Units[0].Value;
  end else begin
    if Units[0].Value = '' then FCalc.FStrBuffer.Delimiter:= ','
    else FCalc.FStrBuffer.Delimiter:= Units[0].Value[1];
    FCalc.FStrBuffer.DelimitedText:= Units[1].Value;
  end;
  Result:= IntToStr(FCalc.FStrBuffer.Count);
end;

function TCalc_Unit.Fun_Trim: string;
begin
  if Count <> 1 then raise Exception.Create('The "trim" Syntax is TRIM(<String_Value>).');
  Result:= Trim(Units[0].Value);
end;

function TCalc_Unit.Fun_Trunc: string;
var
  sv: Double;
  rate: Integer;
begin
  if Count > 1 then raise Exception.Create('The "Trunc" Syntax is Trunc(<Value>).');
  if not TryStrToFloat(Units[0].Value, sv)then sv:= 0;
  Result:= IntToStr(Trunc(sv));
end;

function TCalc_Unit.Fun_UpperCash: string;
begin
  if Count <> 1 then raise Exception.Create('The "UpperCash" Syntax is UPPERCASH(<Float_Value>).');
  Result:= UpperCash(StrToFloat(Units[0].Value));
end;

function TCalc_Unit.Fun_UpperDigit: string;
begin
  if Count <> 1 then raise Exception.Create('The "UpperDigit" Syntax is UPPERDIGIT(<Float_Value>).');
  Result:= UpperDigit(StrToFloat(Units[0].Value));
end;

function TCalc_Unit.Fun_UpperNum: string;
begin
  if Count <> 1 then raise Exception.Create('The "UpperNum" Syntax is UPPERNUM(<Float_Value>).');
  Result:= UpperNum(StrToFloat(Units[0].Value));
end;

function TCalc_Unit.GetCount: Integer;
begin
  Result:= FUnits.Count;
end;

procedure TCalc_Unit.GetParam(Index: Integer; var Value: string;
  var ValueType: TValueType);
begin
  if Index < Count then Value:= Units[Index].Value
  else raise Exception.Create('The index of parameter is overflow.');
end;

function TCalc_Unit.GetUnit(Index: Integer): TCalc_Unit;
begin
  if Index < Count then Result:= FUnits[Index] as TCalc_Unit
  else raise Exception.Create('The index of unit is overflow.');
end;

function TCalc_Unit.GetValue: string;
var
  idxUnit, aPage, aCol, aRow: Integer;
  tmpValue: string;
  tmpOpt: Char;

  procedure CalcNext(var Value: string; var Opt: Char);
  var
    szValue: string;
    szOpt: Char;
  begin
    Inc(idxUnit);
    szValue:= Units[idxUnit].Value;
    szOpt:= Units[idxUnit].Oprator;
    while(szOpt <> '')and(szOpt <> ',')and(GetOperaterLevel(Opt) < GetOperaterLevel(szOpt))do
      CalcNext(szValue, szOpt);
    Value:= CalcTowValue(Value, Opt, szValue);
    Opt:= szOpt;
  end;

begin    //计算单元值
  if FunctionName <> '' then Result:= DoFunction(FunctionName)
  else if FUnits.Count = 0 then begin
    if(Length(FValue) > 0)and(FValue[1] = '$')and Assigned(FOnGetNameValue) then
        Result:= FOnGetNameValue(FValue)
    else Result:= FValue;
  end else if FUnits.Count = 1 then Result:= Units[0].Value
  else if FUnits.Count = 2 then begin
    tmpValue:= Units[0].Value;
    tmpOpt:= Units[0].Oprator;
    Result:= CalcTowValue(tmpValue, tmpOpt, Units[1].Value);
  end else begin
    idxUnit:= 0;
    tmpValue:= Units[0].Value;
    tmpOpt:= Units[0].Oprator;
    while idxUnit < Count - 1 do
      CalcNext(tmpValue, tmpOpt);
    Result:= tmpValue;
  end;
end;

procedure TCalc_Unit.SetFunctionName(const Value: string);
begin
  FFunctionName := Value;
end;

procedure TCalc_Unit.SetOprator(const Value: Char);
begin
  FOprator := Value;
end;

procedure TCalc_Unit.SetValue(const Value: string);
begin
  FValue:= Value;
end;


end.
