(*******************************************************************************
* UVonConfig v2.0 written by James Von (jamesvon@163.com) **********************
********************************************************************************
* TVonConfig                                                                   *
*------------------------------------------------------------------------------*
*     配置文件格式内容管理类，基于 TPersistent 的实现，完全可替代TIniFiles功能 *
* 用于配置信息程序化的处理和使用。
*==============================================================================*
* TVonSetting
*------------------------------------------------------------------------------*
*     配置项分解和合成类，用于参数集合的分解与合成，支持三种格式
* Style = VSK_SEMICOLON 表示以函数方式间隔各个参数，例如 Rate(100)Height(1233)Width(12)
* Style = VSK_PARENTHESES 表示以分号方式间隔各个参数，例如 Rate=100;Height=1233;Width=12
* Style = VSK_COMMA 表示以逗号号方式间隔各个参数，例如 Rate=100,Height=1233,Width=12
* Style = VSK_SPEICAL 表示以#3作为数据分段，以#4作为等号，例如 Rate #4 100 #2 Height #4 1233 #3 Width #4 12 #3
* Style = VSK_BREAK 表示以"|"符号间隔各个参数例如 Rate=100|Height=1233|Width=12
*==============================================================================*
* TVonParams  caption-name-value-defaultvalue信息的存储
*------------------------------------------------------------------------------*
* Delimiter name-prompt-value-defaultvalue中间的间隔符，默认值是#3
* Paragraph 最后一个段落符，默认值是#4
*===============================================================================
* TVonNameValueData
*------------------------------------------------------------------------------*
*     Key - value - data - object 存储结构
*===============================================================================
* TVonList
*------------------------------------------------------------------------------*
*     系列值分解合成类，用于一组数据的分解和合成，数据间间隔符号是通过
* Delimiter 变量来进行控制的，通过 Text 属性进行读取。
*===============================================================================
* TVonArraySetting
*------------------------------------------------------------------------------*
*     二维文字存储结构。
****************************************************************************** *)
unit UVonClass;

// {$R-,T-,H+,X+}

{$R-,T-,X+,H+,B-}

interface

uses System.SysUtils, System.Classes, Forms, StdCtrls, XMLDoc, XMLIntf, DB, StrUtils, Winapi.Windows;

{ TConfigFile - loads an entire INI file into memory and allows all
  operations to be performed on the memory image.  The image can then
  be written out to the disk file }
resourcestring
  RES_OUT_OF_RANG = '索引溢出';

type
  /// <summary>配置文件读写类</summary>
  TVonConfig = class(TPersistent)
  private
    FFilename: string;
    FModified: Boolean;
    FSections: TStringList;
    function AddSection(const Section: string): TStrings;
    function GetCaseSensitive: Boolean;
    function GetSection(Index: Integer): string;
    function GetSectionCount: Integer;
    function GetSectionNames(Index: Integer): string;
    function GetSectionValues(Index: Integer): string;
    procedure LoadValues(const Text: string);
    procedure SetCaseSensitive(Value: Boolean);
    procedure SetFilename(const Value: string);
    procedure SetModified(const Value: Boolean);
    procedure SetSectionValues(Index: Integer; const Value: string);
  protected
    function GetText: string; virtual;
    procedure SetText(const Value: string); virtual;
  public
    constructor Create; overload;
    constructor Create(const ConfigText: string); overload;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure Clear;
    procedure DeleteKey(const Section, Ident: String);
    procedure EraseSection(const Section: string);
    function GetDirectionSection(SectionID: Integer): TStrings; overload;
    function GetDirectionSection(SectionName: string): TStrings; overload;
    procedure GetStrings(List: TStrings);
    function IdentExists(const Section, Ident: string): Boolean;
    procedure LoadFromFile(Filename: string);
    procedure Move(orgIdx, newIdx: Integer); overload;
    procedure Move(const Section: string; orgIdx, newIdx: Integer); overload;
    function ReadBinaryStream(const Section, Name: string; Value: TStream):
        Integer;
    function ReadBool(const Section, Ident: string; Default: Boolean): Boolean;
    function ReadDate(const Section, Name: string; Default: TDateTime):
        TDateTime;
    function ReadDateTime(const Section, Name: string; Default: TDateTime):
        TDateTime;
    function ReadFloat(const Section, Name: string; Default: Double): Double;
    function ReadInteger(const Section, Ident: string; Default: Longint):
        LongInt;
    procedure ReadSection(const Section: string; Strings: TStrings);
    function ReadSectionCount: Integer;
    function ReadSectionName(const SectionID: Integer): string;
    procedure ReadSections(Strings: TStrings);
    function ReadSectionText(const Section: string): string;
    procedure ReadSectionValues(const Section: string; Strings: TStrings);
    function ReadString(const Section, Ident, Default: string): string;
    function ReadTime(const Section, Name: string; Default: TDateTime):
        TDateTime;
    procedure Rename(const ConfigText: string; Reload: Boolean);
    procedure RenameSection(const OrgName, NewName: string);
    procedure SaveToFile(Filename: string);
    function SectionExists(const Section: string): Boolean;
    procedure SetStrings(List: TStrings);
    procedure UpdateFile;
    procedure WriteBinaryStream(const Section, Name: string; Value: TStream);
    procedure WriteBool(const Section, Ident: string; Value: Boolean);
    procedure WriteDate(const Section, Name: string; Value: TDateTime);
    procedure WriteDateTime(const Section, Name: string; Value: TDateTime);
    procedure WriteFloat(const Section, Name: string; Value: Double);
    procedure WriteInteger(const Section, Ident: string; Value: Longint);
    procedure WriteSection(const SectionID: Integer; StringsText: string);
        overload;
    procedure WriteSection(const SectionID: Integer; Strings: TStrings);
        overload;
    procedure WriteSection(const Section: string; StringsText: string);
        overload;
    procedure WriteSection(const Section: string; Strings: TStrings); overload;
    procedure WriteString(const Section, Ident, Value: String);
    procedure WriteTime(const Section, Name: string; Value: TDateTime);
    property SectionNames[Index: Integer]: string read GetSectionNames;
    property Sections[Index: Integer]: string read GetSection;
    property SectionValues[Index: Integer]: string read GetSectionValues write
        SetSectionValues;
  published
    property CaseSensitive: Boolean read GetCaseSensitive write
        SetCaseSensitive;
    property Filename: string read FFilename write SetFilename;
    property Modified: Boolean read FModified write SetModified;
    property SectionCount: Integer read GetSectionCount;
    property Text: string read GetText write SetText;
  end;

  /// <summary>文件信息存储方式</summary>
  /// <param name="VSK_PARENTHESES">表示以分号方式间隔各个参数，例如 Rate=100;Height=1233;Width=12</param>
  /// <param name="VSK_SEMICOLON">表示以函数方式间隔各个参数，例如 Rate(100)Height(1233)Width(12)</param>
  /// <param name="VSK_COMMA">表示以逗号方式间隔各个参数，例如 Rate=100,Height=1233,Width=12</param>
  /// <param name="VSK_SPEICAL">表示以#3作为数据分段，以#4作为等号，例如 Rate #4 100 #2 Height #4 1233 #3 Width #4 12 #3</param>
  /// <param name="VSK_BREAK">表示以"|"符号间隔各个参数例如 Rate=100|Height=1233|Width=12</param>
  EVonSettingKind = (VSK_PARENTHESES, VSK_SEMICOLON, VSK_COMMA, VSK_SPEICAL,
    VSK_BREAK);

  PWideCharList = ^TWideCharList;
  TWideCharList = array [0 .. MaxListSize - 1] of PWideChar;

  /// <summary>设置内容存储结构，即name-value信息的存储</summary>
  TVonSetting = class
  private
    FCapacity: Integer;
    FCount: Integer;
    FKeyList: PWideCharList;
    FValueList: PWideCharList;
    procedure FreeItem(const Index: Integer);
    function GetBreakText: string;
    function GetCommaText: string;
    function GetNames(Index: Integer): string;
    function GetNameValue(Name: string): string;
    function GetParamsText: string;
    function GetSettingText: string;
    function GetSpeicalText: string;
    function GetText(Style: EVonSettingKind): string;
    function GetValues(Index: Integer): string;
    procedure Grow;
    function NewItem(const Value: string): PWideChar;
    function NextChar(var P: PWideChar; ch: char): string;
    procedure SetBreakText(const Value: string);
    procedure SetCapacity(const Value: Integer);
    procedure SetCommaText(const Value: string);
    procedure SetCount(const Value: Integer);
    procedure SetNames(Index: Integer; const Value: string);
    procedure SetNameValue(Name: string; const Value: string);
    procedure SetParamsText(const Value: string);
    procedure SetSettingText(const Value: string);
    procedure SetSpeicalText(const Value: string);
    procedure SetText(Style: EVonSettingKind; const Value: string);
    procedure SetValues(Index: Integer; const Value: string);
  public
    constructor Create;
    destructor Destroy; override;
    function Add(const Name, Value: string): Integer;
    procedure Clear;
    procedure Delete(const Index: Integer); overload;
    procedure Delete(const Name: string); overload;
    function IndexOfName(const Name: string): Integer;
    function IndexOfValue(const Value: string): Integer;
    procedure Insert(Index: Integer; const Name, Value: string);
    procedure LoadFromFile(Filename: string; Style: EVonSettingKind);
    procedure LoadFromStream(aStream: TStream; Style: EVonSettingKind);
    procedure Move(const SourceIdx, DestIdx: Integer);
    function Rename(const oldName, NewName: string): Integer;
    procedure SaveToFile(Filename: string; Style: EVonSettingKind);
    procedure SaveToStream(aStream: TStream; Style: EVonSettingKind);
    property Names[Index: Integer]: string read GetNames write SetNames;
    property NameValue[Name: string]: string read GetNameValue write
        SetNameValue; default;
    property Text[Style: EVonSettingKind]: string read GetText write SetText;
    property Values[Index: Integer]: string read GetValues write SetValues;
  published
    property Count: Integer read FCount write SetCount;
  end;

  /// <summary>参数类型</summary>
  /// <param name="PT_Text">单行文字录入（TEdit）</param>
  /// <param name="PT_Int">整数录入(TSpinEdit)</param>
  /// <param name="PT_Float">浮点数据录入（TEdit）</param>
  /// <param name="PT_Conn">数据库链接录入(TButtonedEdit)</param>
  /// <param name="PT_Date">日期数据录入（TPickDatetime）</param>
  /// <param name="PT_Time">时间数据录入（TPickDatetime）</param>
  /// <param name="PT_DateTime">时间数据录入（TPickDatetime）</param>
  /// <param name="PT_Memo">多行数据录入(TMemo)</param>
  /// <param name="PT_RichText">富文本数据录入(FrameRichImputor)</param>
  /// <param name="PT_Selection">选择数据录入（TComBoBox）</param>
  /// <param name="PT_Choice">单选数据录入(TRadioGroup)</param>
  /// <param name="PT_Bool">布尔数据录入(TCheckBox)0：False，1：True</param>
  /// <param name="PT_CheckList">复选列表录入(TCheckList)</param>
  /// <param name="PT_Bitmap">图片录入(FrameImageImputor)</param>
  /// <param name="PT_GUID">GUID生成器(TButtonedEdit)</param>
  /// <param name="PT_TREE">树形结构(TButtonedEdit)</param>
  /// <param name="PT_GROUP">组合构件，通过多个项目组合出结果</param>
  /// <param name="PT_Dialog">...</param>
  TVonParamType = (PT_Text, PT_Int, PT_Float, PT_Conn, PT_MASK, PT_Date, PT_Time,
    PT_DateTime, PT_Memo, PT_RichText, PT_Selection, PT_Choice, PT_Bool,
    PT_CheckList, PT_Bitmap, PT_GUID, PT_TREE, PT_GROUP, PT_Dialog);
  /// <summary>参数内容存储结构，即name-prompt-value-defaultvalue-type-option信息的存储</summary>
  TVonParams = class(TObject)
  private
    FCapacity: Integer;
    FCount: Integer;
    FDefaultList: array of string;
    FDelimiter: Char;
    FKeyList: array of string;
    FOptionList: array of string;
    FParagraph: Char;
    FPromptList: array of string;
    FTypeList: array of TVonParamType;
    FValueList: array of string;
    function GetDefaultValue(Index: Integer): string;
    function GetNames(Index: Integer): string;
    function GetNameValue(Name: string): string;
    function GetOptions(Index: Integer): string;
    function GetParamsText: string;
    function GetPrompts(Index: Integer): string;
    function GetText: string;
    function GetTypes(Index: Integer): TVonParamType;
    function GetValues(Index: Integer): string;
    procedure Grow;
    procedure SetCapacity(const Value: Integer);
    procedure SetDefaultValue(Index: Integer; const Value: string);
    procedure SetNames(Index: Integer; const Value: string);
    procedure SetOptions(Index: Integer; const Value: string);
    procedure SetPrompts(Index: Integer; const Value: string);
    procedure SetText(const Value: string);
    procedure SetTypes(Index: Integer; const Value: TVonParamType);
    procedure SetValues(Index: Integer; const Value: string);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Assign(Source: TVonParams);
    procedure Clear;
    procedure Delete(const Index: Integer); overload;
    procedure Delete(const Name: string); overload;
    function IndexOfName(const Name: string): Integer;
    procedure Merge(Source: TVonParams);
    procedure Move(Org, Dest: Integer);
    function Save(const Name, Prompt, Value, DefaultValue: string; ValueType:
        TVonParamType; Option: string): Integer;
    property DefaultValue[Index: Integer]: string read GetDefaultValue write
        SetDefaultValue;
    property Names[Index: Integer]: string read GetNames write SetNames;
    property NameValue[Name: string]: string read GetNameValue;
    property Options[Index: Integer]: string read GetOptions write SetOptions;
    property Prompts[Index: Integer]: string read GetPrompts write SetPrompts;
    property Text: string read GetText write SetText;
    property Types[Index: Integer]: TVonParamType read GetTypes write SetTypes;
    property Values[Index: Integer]: string read GetValues write SetValues;
  published
    property Count: Integer read FCount;
    property Delimiter: Char read FDelimiter write FDelimiter default #3;
    property Paragraph: Char read FParagraph write FParagraph default #4;
  end;

  /// <summary>列表结构存储</summary>
  TVonList = class(TObject)
  private
    FCount: Integer;
    FDelimiter: Char;
    FList: array [0 .. 255] of string;
    function Get(Index: Integer): string;
    function GetText: string;
    procedure Put(Index: Integer; const Value: string);
    procedure SetDelimiter(const Value: Char);
    procedure SetText(const Value: string);
    function GetProtectedText: string;
    procedure SetProtectedText(const Value: string);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Add(Value: string);
    procedure Clear;
    procedure Delete(const Index: Integer); overload;
    procedure Delete(const Value: string); overload;
    function IndexOf(Value: string): Integer;
    property Strings[Index: Integer]: string read Get write Put; default;
  published
    property Count: Integer read FCount;
    property Delimiter: Char read FDelimiter write SetDelimiter;
    property Text: string read GetText write SetText;
    /// <summary>支持引号保护的文字内容</summary>
    property ProtectedText: string read GetProtectedText write SetProtectedText;
  end;

  /// <summary>XML配置信息存储结构</summary>
  TVonXmlCfg = class(TVonConfig)
  private
    function GetXmlText: string;
    procedure SetXmlText(const Value: string);
  public
    procedure Assign(Source: TPersistent); override;
    procedure AssignTo(Dest: TPersistent); override;
  published
    property XmlText: string read GetXmlText write SetXmlText;
  end;

  /// <summary>链接信息存储结构中每个链接点结构</summary>
  TVonLinker = class(TObject)
  private
    FData: TObject;
    FIndex: Integer;
    FKey: string;
    FNext: TVonLinker;
    FPrevious: TVonLinker;
    FValue: string;
    procedure SetData(const Value: TObject);
    procedure SetIndex(const Value: Integer);
    procedure SetKey(const Value: string);
    procedure SetNext(const Value: TVonLinker);
    procedure SetPrevious(const Value: TVonLinker);
    procedure SetValue(const Value: string);
  public
    procedure ChangeIndex;
  published
    property Data: TObject read FData write SetData;
    property Index: Integer read FIndex write SetIndex;
    property Key: string read FKey write SetKey;
    property Next: TVonLinker read FNext write SetNext;
    property Previous: TVonLinker read FPrevious write SetPrevious;
    property Value: string read FValue write SetValue;
  end;

  /// <summary>链接信息存储结构</summary>
  TVonLinkerCollection = class(TObject)
  private
    FCurrent: TVonLinker;
    function GetBOF: Boolean;
    function GetEOF: Boolean;
    function GetText: string;
    procedure SetCurrent(const Value: TVonLinker);
    procedure SetText(const Value: string);
  public
    First: TVonLinker;
    Laster: TVonLinker;
    destructor Destroy; override;
    procedure Append(AKey, AValue: string; AObj: TObject = nil);
    procedure Clear;
    procedure Delete;
    procedure Insert(AKey, AValue: string; AObj: TObject = nil);
    function Next: TVonLinker;
    function Previous: TVonLinker;
  published
    property BOF: Boolean read GetBOF;
    property Current: TVonLinker read FCurrent write SetCurrent;
    property EOF: Boolean read GetEOF;
    property Text: string read GetText write SetText;
  end;

  TFIFOItem = class
  private
    FObj: TObject;
    FData: Integer;
    FNext: TFIFOItem;
  public
    constructor Create(AObj: TObject; AData: Integer);
  published
    property Obj: TObject read FObj;
    property Data: Integer read FData write FData;
    property Next: TFIFOItem read FNext;
  end;

  /// <summary>先进先出队列</summary>
  TFIFO = class
  private
    FFirst: TFIFOItem;
    FLaster: TFIFOItem;
    lpCriticalSection: TRTLCriticalSection;
  public
    constructor Create();
    destructor Destroy; override;
    /// <summary>压入</summary>
    procedure Push(item: TFIFOItem);
    /// <summary>取出</summary>
    function Pull: TFIFOItem;
  end;

  /// <summary>Name-value-object存储结构</summary>
  TVonNameValueData = class(TObject)
  private
    FAutoFreeObject: Boolean;
    FCapacity: Integer;
    FColDelimiter: string;
    FCount: Integer;
    FDataList: array of Pointer;
    FNameList: array of string;
    FObjList: array of TObject;
    FRowDelimiter: string;
    FValueList: array of string;
    function GetDatas(Index: Integer): Pointer;
    function GetNameData(Name: string): Pointer;
    function GetNameObject(Name: string): TObject;
    function GetNames(Index: Integer): string;
    function GetNameValue(Name: string): string;
    function GetObjects(Index: Integer): TObject;
    function GetText: string;
    function GetValues(Index: Integer): string;
    procedure Grow;
    procedure SetAutoFreeObject(const Value: Boolean);
    procedure SetColDelimiter(const Value: string);
    procedure SetDatas(Index: Integer; const Value: Pointer);
    procedure SetNameData(Name: string; const Value: Pointer);
    procedure SetNameObject(Name: string; const Value: TObject);
    procedure SetNames(Index: Integer; const Value: string);
    procedure SetNameValue(Name: string; const Value: string);
    procedure SetObjects(Index: Integer; const Value: TObject);
    procedure SetRowDelimiter(const Value: string);
    procedure SetText(const Value: string);
    procedure SetValues(Index: Integer; const Value: string);
  public
    constructor Create;
    destructor Destroy; override;
    function Add(const Name, Value: string; Obj: TObject = nil; Data: Pointer =
        0): Integer;
    procedure Clear;
    procedure Delete(const Index: Integer); overload;
    procedure Delete(const Name: string); overload;
    function IndexOfData(Data: Pointer): Integer;
    function IndexOfName(Name: string): Integer;
    function IndexOfObject(Obj: TObject): Integer;
    function IndexOfValue(Value: string): Integer;
    procedure Move(OrgIdx, DestIdx: Integer);
    procedure Save(const Text: string); overload;
    function Save(const Name, Value: string; Obj: TObject = nil; Data: Pointer
        = 0): Integer; overload;
    property Datas[Index: Integer]: Pointer read GetDatas write SetDatas;
    property NameData[Name: string]: Pointer read GetNameData write SetNameData;
    property NameObject[Name: string]: TObject read GetNameObject write
        SetNameObject;
    property Names[Index: Integer]: string read GetNames write SetNames;
    property NameValue[Name: string]: string read GetNameValue write
        SetNameValue;
    property Objects[Index: Integer]: TObject read GetObjects write SetObjects;
    property Values[Index: Integer]: string read GetValues write SetValues;
  published
    property AutoFreeObject: Boolean read FAutoFreeObject write
        SetAutoFreeObject default false;
    property ColDelimiter: string read FColDelimiter write SetColDelimiter;
    property Count: Integer read FCount;
    property RowDelimiter: string read FRowDelimiter write SetRowDelimiter;
    property Text: string read GetText write SetText;
  end;

  TArrayOfString = array of string;
  PVonArrayString = ^TArrayOfString;

  TVonDataArrayString = record
    Data: Pointer;
    ArrayString: TArrayOfString;
  end;
  PVonDataArrayString = ^TVonDataArrayString;

  /// <summary>二维文字存储结构</summary>
  TVonArraySetting = class(TObject)
  private
    FCapacity: Integer;
    FColDelimiter: Char;
    FCount: Integer;
    FRowDelimiter: Char;
    FValueList: array of PVonDataArrayString;
    function GetColCount(const Row: Integer): Integer;
    function GetText: string;
    function GetValues(const Row, Col: Integer): string;
    procedure Grow;
    procedure SetColCount(const Row: Integer; const Value: Integer);
    procedure SetColDelimiter(const Value: Char);
    procedure SetPText(P: PChar; const endChar: string); overload;
    procedure SetRowDelimiter(const Value: Char);
    procedure SetText(const Value: string);
    procedure SetValues(const Row, Col: Integer; const Value: string);
    function GetData(const Row: Integer): Pointer;
    procedure SetData(const Row: Integer; const Value: Pointer);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Add(const Row, Col: Integer; Value: string);
    function AppendRow(Value: string; Data: Pointer = 0): Integer; overload;
    function AppendRow(Value: array of string; Data: Pointer = 0): Integer; overload;
    procedure Clear;
    procedure Delete(const Row: Integer);
    procedure DeleteCell(const Row, Col: Integer);
    function GetRow(const Row: Integer): TArrayOfString;
    function IndexOfValue(const Col: Integer; Value: string): Integer;
    procedure InsertRow(const Row: Integer; Value: array of string; Data: Pointer = 0);
    procedure LoadFromFile(Filename: string);
    procedure LoadFromStream(aStream: TStream; ASize: Int64 = 0);
    procedure Move(orgIdx, DestIdx: Integer);
    procedure SaveToFile(Filename: string);
    function SaveToStream(aStream: TStream): Int64;
    procedure UpdateRow(const Row: Integer; Value: array of string);
    property ColCount[const Row: Integer]: Integer read GetColCount write
        SetColCount;
    property Values[const Row, Col: Integer]: string read GetValues write
        SetValues; default;
    property Data[const Row: Integer]: Pointer read GetData write SetData;
  published
    property ColDelimiter: Char read FColDelimiter write SetColDelimiter
        default #3;
    property Count: Integer read FCount;
    property RowDelimiter: Char read FRowDelimiter write SetRowDelimiter
        default #4;
    property Text: string read GetText write SetText;
  end;

  /// <summary>二维文字存储结构</summary>
  TVonTable = class(TObject)
  private
    FColCapacity: Integer;
    FColCount: Integer;
    FColDelimiter: Char;
    FRowCapacity: Integer;
    FRowCount: Integer;
    FRowDelimiter: Char;
    FValueList: array of PVonArrayString;
    FCol: Integer;
    FRow: Integer;
    function GetCells(const Col, Row: Integer): string;
    function GetText: string;
    procedure GrowCol;
    procedure GrowRow;
    procedure SetCells(const Col, Row: Integer; const Value: string);
    procedure SetColCount(const Value: Integer);
    procedure SetRowCount(const Value: Integer);
    procedure SetText(const Value: string);
    function SplitText(P: PChar; endOfChar: char): PChar;
    procedure SetBOF(const Value: Boolean);
    procedure SetCol(const Value: Integer);
    procedure SetEOF(const Value: Boolean);
    procedure SetRow(const Value: Integer);
    function GetBOF: Boolean;
    function GetEOF: Boolean;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure DeleteCol(const Col: Integer);
    procedure DeleteRow(const Row: Integer);
    procedure InsertCol(const Col: Integer; Value: array of string);
    procedure InsertRow(const Row: Integer; Value: array of string);
    procedure UpdateRow(const Row: Integer; Value: array of string);
    procedure LoadFromFile(Filename: string; endOfChar: Char);
    procedure LoadFromStream(aStream: TStream; endOfChar: Char);
    procedure MoveCol(orgIdx, DestIdx: Integer);
    procedure MoveRow(orgIdx, DestIdx: Integer);
    procedure SaveToFile(Filename: string);
    function SaveToStream(aStream: TStream): Int64;
    property Cells[const Col, Row: Integer]: string read GetCells write SetCells;
  published
    property Col: Integer read FCol write SetCol;
    property Row: Integer read FRow write SetRow;
    property BOF: Boolean read GetBOF;
    property EOF: Boolean read GetEOF;
    property ColCount: Integer read FColCount write SetColCount;
    property RowCount: Integer read FRowCount write SetRowCount;
    property Text: string read GetText write SetText;
  end;

  /// <summary>支持多页存储的数据结构</summary>
  TVonBook = class(TObject)
  private
    FCapacity: Integer;
    FColDelimiter: Char;
    FPageCount: Integer;
    FPageDelimiter: Char;
    FPageList: array of TVonTable;
    FRowDelimiter: Char;
    function GetPages(Index: Integer): TVonTable;
    function GetText: string;
    procedure Grow;
    procedure SetText(const Value: string);
  public
    constructor Create;
    destructor Destroy; override;
    function Append: Integer;
    procedure Clear;
    procedure Delete(Index: Integer);
    procedure Insert(Index: Integer);
    procedure LoadFromFile(Filename: string);
    procedure LoadFromStream(aStream: TStream);
    procedure Move(orgIdx, DestIdx: Integer);
    procedure SaveToFile(Filename: string);
    function SaveToStream(aStream: TStream): Int64;
    property Pages[Index: Integer]: TVonTable read GetPages; default;
  published
    property ColDelimiter: Char read FColDelimiter write FColDelimiter default
        #3;
    property PageCount: Integer read FPageCount;
    property PageDelimiter: Char read FPageDelimiter write FPageDelimiter;
    property RowDelimiter: Char read FRowDelimiter write FRowDelimiter default
        #4;
    property Text: string read GetText write SetText;
  end;

  /// <summary>Key-Value的存储结构</summary>
  TVonIdentValue = class(TObject)
  private
    FCapacity: Integer;
    FCount: Integer;
    FEndDelimiter: Char;
    FIdentDelimiter: Char;
    FIdentList: array of string;
    FValueDelimiter: Char;
    FValueList: array of string;
    function GetText: string;
    function GetValueOfIndex(Index: Integer): string;
    function GetValues(Ident: string): string;
    procedure Grow;
    procedure SetEndDelimiter(const Value: Char);
    procedure SetIdentDelimiter(const Value: Char);
    procedure SetText(const Value: string);
    procedure SetValueDelimiter(const Value: Char);
    procedure SetValueOfIndex(Index: Integer; const Value: string);
    procedure SetValues(Ident: string; const Value: string);
  public
    constructor Create;
    destructor Destroy; override;
    procedure LoadFromStream(AStream: TStream);
    procedure SaveToStream(AStream: TStream);
    procedure Clear;
    procedure Delete(const Index: Integer); overload;
    procedure Delete(const Ident: string); overload;
    function IndexOf(Ident: string): Integer;
    procedure Save(const Ident, Value: string);
    function SetPText(PText: PChar): PChar;
    property ValueOfIndex[Index: Integer]: string read GetValueOfIndex write
        SetValueOfIndex;
    property Values[Ident: string]: string read GetValues write SetValues;
  published
    property Count: Integer read FCount;
    property EndDelimiter: Char read FEndDelimiter write SetEndDelimiter;
    property IdentDelimiter: Char read FIdentDelimiter write SetIdentDelimiter;
    property Text: string read GetText write SetText;
    property ValueDelimiter: Char read FValueDelimiter write SetValueDelimiter;
  end;

  /// <summary>含有键值的多行信息存储结构KEY=cell...cell</summary>
  TVonKeyArraySetting = class(TObject)
  private
    FCapacity: Integer;
    FColDelimiter: Char;
    FCount: Integer;
    FKeyDelimiter: Char;
    FKeyList: array of string;
    FRowDelimiter: Char;
    FValueDelimiter: Char;
    FValueList: array of TVonIdentValue;
    function GetColCount(const Idx: Integer): Integer;
    function GetNames(const Idx, Col: Integer): string;
    function GetValues(const Idx, Col: Integer): string;
    function GetText: string;
    function GetValue(const Idx: Integer; const Key: string): string;
    procedure Grow;
    function NewRow(Idx: Integer; Key: string): TVonIdentValue;
    procedure SetColDelimiter(const Value: Char);
    procedure SetValues(const Idx, Col: Integer; const Value: string);
    procedure SetKeyDelimiter(const Value: Char);
    procedure SetRowDelimiter(const Value: Char);
    procedure SetText(const Value: string);
    procedure SetValue(const Idx: Integer; const Key: string; const Value:
        string);
    procedure SetValueDelimiter(const Value: Char);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    procedure Delete(const Idx: Integer); overload;
    procedure Delete(const Key: string); overload;
    procedure DeleteData(const Idx: Integer; Inden: string); overload;
    procedure DeleteData(const Key, Inden: string); overload;
    function GetRow(const Idx: Integer): TVonIdentValue; overload;
    function GetRow(const Key: string): TVonIdentValue; overload;
    function IndexOfKey(Key: string): Integer;
    procedure InsertRow(const Idx: Integer; const Key: string; Value: array of
        string);
    procedure LoadFromFile(Filename: string);
    procedure LoadFromStream(aStream: TStream; ASize: Int64 = 0);
    procedure Move(orgIdx, DestIdx: Integer);
    procedure Save(const Idx: Integer; const Inden, Value: string); overload;
    procedure Save(const Key, Inden, Value: string); overload;
    procedure SaveRow(const Key: string; Value: array of string);
    procedure SaveToFile(Filename: string);
    function SaveToStream(aStream: TStream): Int64;
    property ColCount[const Idx: Integer]: Integer read GetColCount;
    property Names[const Idx, Col: Integer]: string read GetNames;
    property Values[const Idx, Col: Integer]: string read GetValues write
        SetValues;
    property Value[const Idx: Integer; const Key: string]: string read GetValue
        write SetValue;
  published
    property ColDelimiter: Char read FColDelimiter write SetColDelimiter
        default #3;
    property Count: Integer read FCount;
    property KeyDelimiter: Char read FKeyDelimiter write SetKeyDelimiter
        default #2;
    property RowDelimiter: Char read FRowDelimiter write SetRowDelimiter
        default #4;
    property Text: string read GetText write SetText;
    property ValueDelimiter: Char read FValueDelimiter write SetValueDelimiter
        default #5;
  end;

  /// <summary>对象列表</summary>
  TVonArrayObject = class(TObject)
  private
    FCapacity: Integer;
    FCount: Integer;
    FList: array of TObject;
    function GetItems(Index: Integer): TObject;
    procedure Grow;
    procedure SetItems(Index: Integer; const Value: TObject);
  public
    function Add(Item: TObject): Integer;
    procedure Delete(Index: Integer);
    procedure Insert(Index: Integer; Item: TObject);
    property Items[Index: Integer]: TObject read GetItems write SetItems;
  published
    property Count: Integer read FCount;
  end;

  PPVonHashItem = ^PVonHashItem;
  PVonHashItem = ^TVonHashItem;

  TVonHashItem = record
    Next: PVonHashItem;
    Key: string;
    Value: string;
    Data: Pointer;
    Obj: TObject;
  end;

  /// <summary>Hash列表，支持 key-value-object </summary>
  TVonHash = class(TObject)
  private
    Buckets: array of PVonHashItem;
  protected
    function Find(const Key: string): PPVonHashItem;
    function HashOf(const Key: string): Cardinal; virtual;
  public
    constructor Create(Size: Cardinal = 256);
    destructor Destroy; override;
    procedure Add(const Key, Value: string; Data: Pointer; Obj: TObject);
    procedure Clear;
    function DataOf(const Key: string): Pointer;
    function Modify(const Key, Value: string; Data: Pointer; Obj: TObject):
        Boolean;
    function ObjectOf(const Key: string): TObject;
    procedure Remove(const Key: string);
    function ValueOf(const Key: string): string;
  end;

  TVonTimeItem = class(TObject)
    Data: string;
    Datetime: TDateTime;
    Key: string;
  end;

  /// <summary>时间节点数据，存储是按时间顺序进行的，time-key-value</summary>
  TVonTimeList = class(TObject)
  private
    List: TList;
    function GetCount: Integer;
    function GetItems(Index: Integer): TVonTimeItem;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Append(Key: string; Data: string; Datetime: TDateTime);
    procedure Clear;
    procedure Delete(Index: Integer); overload;
    procedure Delete(Key: string); overload;
    property Items[Index: Integer]: TVonTimeItem read GetItems;
  published
    property Count: Integer read GetCount;
  end;

  TVonJSONtypes = (jsBase, jsNumber, jsString, jsBoolean, jsNull, jsList,
    jsObject);

  TVonFollowItem = class;

  /// <summary>操作流程控制</summary>
  TVonFollow = class(TObject)
  private
    FCapacity: Integer;
    FCount: Integer;
    FItemList: array of TVonFollowItem;
    FParent: TVonFollowItem;
    FCaseValue: string;
    function GetItems(Index: Integer): TVonFollowItem;
    procedure Grow;
    procedure SetCapacity(const Value: Integer);
    procedure SetCaseValue(const Value: string);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Add(Item: TVonFollowItem);
    procedure Clear;
    procedure Delete(const Index: Integer); overload;
    function IndexOfData(AData: Pointer): Integer;
    function IndexOfID(AID: Integer): Integer;
    function IndexOfKey(AKey: string): Integer;
    function IndexOfObj(AObj: TObject): Integer;
    function IndexOf(Item: TVonFollowItem): Integer;
    procedure Insert(Index: Integer; Item: TVonFollowItem);
    procedure Move(Org, Dest: Integer);
    property Items[Index: Integer]: TVonFollowItem read GetItems;
    property Parent: TVonFollowItem read FParent write FParent;
  published
    property CaseValue: string read FCaseValue write SetCaseValue;
    property Count: Integer read FCount;
  end;

  TVonFollowItem = class(TObject)
  private
    FCapacity: Integer;
    FCount: Integer;
    FData: Pointer;
    FFollowList: array of TVonFollow;
    FID: Integer;
    FKey: string;
    FObj: TObject;
    FParent: TVonFollow;
    function GetFollows(Index: Integer): TVonFollow;
    procedure Grow;
    procedure SetCapacity(const Value: Integer);
  public
    constructor Create;
    destructor Destroy; override;
    procedure Add(CaseValue: string; Item: TVonFollow);
    procedure Clear;
    procedure Delete(const Index: Integer); overload;
    procedure Insert(Index: Integer; CaseValue: string; Item: TVonFollow);
    procedure Move(Org, Dest: Integer);
    property Follows[Index: Integer]: TVonFollow read GetFollows;
    property Data: Pointer read FData write FData;
  published
    property ID: Integer read FID write FID;
    property Key: string read FKey write FKey;
    property Obj: TObject read FObj write FObj;
    property Parent: TVonFollow read FParent write FParent;
    property Count: Integer read FCount;
  end;

  TSerieItem = class
  private
    FKey: string;
    FObj: TObject;
    FPrevious: TSerieItem;
    FValue: string;
    FNext: TSerieItem;
  public
    constructor Create(Key, Value: string; Obj: TObject);
  published
    property Key: string read FKey write FKey;
    property Value: string read FValue write FValue;
    property Obj: TObject read FObj write FObj;
    property Previous: TSerieItem read FPrevious write FPrevious;
    property Next: TSerieItem read FNext write FNext;
  end;

  TSerieCollection = class
  private
    FLast: TSerieItem;
    FFirst: TSerieItem;
    FAutoFreeObject: Boolean;
  public
    constructor Create(AutoFreeObject: Boolean);
    destructor Destroy; override;
    procedure Append(AKey, AValue: string; AObj: TObject = nil);
    procedure Clear;
    procedure Delete(AKey: string); overload;
    procedure Delete(Item: TSerieItem); overload;
    function Find(AKey: string): TSerieItem;
  published
    property First: TSerieItem read FFirst write FFirst;
    property Last: TSerieItem read FLast write FLast;
    property AutoFreeObject: Boolean read FAutoFreeObject write FAutoFreeObject;
  end;

  TVonTreeItem = class
  private
    FChildren: TList;
    FInfo: string;
    function GetChildren(Index: Integer): TVonTreeItem;
    function GetChildCount: Integer;
  public
    constructor Create();
    destructor Destroy; override;
    function AddChild(AInfo: string): Integer;
    procedure Delete(Index: Integer); virtual;
    procedure Clear;
    procedure SaveToStream(S: TStream); virtual;
    procedure LoadFromStream(S: TStream); virtual;
    property Children[Index: Integer]: TVonTreeItem read GetChildren;
  published
    property Info: string read FInfo write FInfo;
    property ChildCount: Integer read GetChildCount;
  end;

  TVonTreeCollection<T: TVonTreeItem> = class
  private
    FRoot: TVonTreeItem;
    FItems: T;
    function GetItems(Index: Integer): T;
    procedure SetItems(Index: Integer; const Value: T);
  public
    constructor Create();
    destructor Destroy; override;
    procedure SaveToStream(S: TStream);
    procedure LoadFromStream(S: TStream);
    procedure ClearItems;
    property Items[Index: Integer]: T read GetItems write SetItems;
  published
    property Root: TVonTreeItem read FRoot;
  end;

  PX_Y = ^TX_Y;
  TX_Y = record
    X: Integer;
    Y: Integer;
  end;

  TVonSectionValueType = (vst_str, vst_int, vst_float, vst_datetime, vst_bool, vst_Guid, vst_bytes);
  TVonSectionValue = class
  private
    FName: string;
    FValueType: TVonSectionValueType;
    FValueSize: Integer;
    FData: TBytes;
    procedure SetValueSize(const Value: Integer);
    function GetText: string;
  public
    destructor Destroy; override;
  published
    property Name: string read FName write FName;
    property ValueType: TVonSectionValueType read FValueType write FValueType;
    property ValueSize: Integer read FValueSize write SetValueSize;
    property Data: TBytes read FData write FData;
    property Text: string read GetText;
  end;

  TVonSection = class
  private
    FList: TList;
    FSectionName: string;
    function GetValueCount: Integer;
    function GetValueByIndex(Index: Integer): TVonSectionValue;
    function GetValues(AName: string): TVonSectionValue;
  public
    constructor Create();
    destructor Destroy; override;
    function SetString(AName: string; Value: string): Integer;
    function SetInteger(AName: string; Value: Integer): Integer;
    function SetFloat(AName: string; Value: Extended): Integer;
    function SetDatetime(AName: string; Value: TDatetime): Integer;
    function SetBool(AName: string; Value: Bool): Integer;
    function SetGuid(AName: string; Value: TGuid): Integer;
    function SetBinary(AName: string; Value: TBytes): Integer;
    function SetStream(AName: string; Value: TStream): Integer;
    function SetValue(Value: TVonSectionValue): Integer;
    function GetString(AName: string; default: string = ''): string;
    function GetInteger(AName: string; default: Integer = 0): Integer;
    function GetFloat(AName: string; default: Extended = 0): Extended;
    function GetDatetime(AName: string; default: TDatetime = 0): TDatetime;
    function GetBool(AName: string; default: Bool = false): Bool;
    function GetGuid(AName: string; default: TGuid): TGuid;
    function GetBinary(AName: string): TBytes;
    procedure GetStream(AName: string; Value: TStream);
    procedure Delete(Index: Integer); overload;
    procedure Delete(AName: string); overload;
    procedure Clear;
    property Values[AName: string]: TVonSectionValue read GetValues;
    property ValueByIndex[Index: Integer]: TVonSectionValue read GetValueByIndex;
  published
    property SectionName: string read FSectionName write FSectionName;
    property ValueCount: Integer read GetValueCount;
  end;

  TVonSectionGroup = class
  private
    FList: TList;
    function GetSections(Name: string): TVonSection;
    function GetSectionByIndex(Index: Integer): TVonSection;
    function GetSectionCount: Integer;
  public
    constructor Create();
    destructor Destroy; override;
    procedure LoadFromFile(Filename: string);
    procedure SaveToFile(Filename: string);
    procedure LoadFromStream(AStream: TStream);
    procedure SaveToStream(AStream: TStream);
    procedure Clear;
    procedure Delete(Index: Integer); overload;
    procedure Delete(AName: string); overload;
    procedure AddSection(ASection: TVonSection);
    property Sections[Name: string]: TVonSection read GetSections;
    property SectionByIndex[Index: Integer]: TVonSection read GetSectionByIndex;
  published
    property SectionCount: Integer read GetSectionCount;
  end;

var
  FVonArray: TVonArraySetting;
  FVonSetting: TVonSetting;
  FVonList: TVonList;

implementation

uses RTLConsts, UVonSystemFuns;

{ TConfigFile }

{
********************************** TVonConfig **********************************
}
constructor TVonConfig.Create;
begin
  inherited Create; // (FileName);
  FSections := TStringList.Create;
{$IFDEF LINUX}
  FSections.CaseSensitive := True;
{$ENDIF}
  FModified := false;
end;

constructor TVonConfig.Create(const ConfigText: string);
begin
  Create;
  LoadValues(ConfigText);
  FModified := false;
end;

destructor TVonConfig.Destroy;
begin
  if FSections <> nil then
    Clear;
  FSections.Free;
  inherited Destroy;
end;

function TVonConfig.AddSection(const Section: string): TStrings;
begin
  FModified := True;
  // Result := THashedStringList.Create;
  Result := TStringList.Create;
  try
    TStringList(Result).CaseSensitive := CaseSensitive;
    FSections.AddObject(Section, Result);
  except
    Result.Free;
    raise;
  end;
end;

procedure TVonConfig.Assign(Source: TPersistent);
begin
  if Source is TVonConfig then
    Text := TVonConfig(Source).Text
  else if Source is TField then
    Text := TField(Source).AsString
  else if Source is TMemoField then
    Text := TField(Source).AsString
  else if Source is TMemo then
    Text := TMemo(Source).Text;
  // inherited;
end;

procedure TVonConfig.AssignTo(Dest: TPersistent);
begin
  // inherited;
  if Dest is TVonConfig then
    TVonConfig(Dest).Text := Text
  else if Dest is TField then
    TField(Dest).AsString := Text
  else if Dest is TMemoField then
    TField(Dest).AsString := Text;
end;

procedure TVonConfig.Clear;
var
  I: Integer;
begin
  FModified := True;
  for I := 0 to FSections.Count - 1 do
    TObject(FSections.Objects[I]).Free;
  FSections.Clear;
end;

procedure TVonConfig.DeleteKey(const Section, Ident: String);
var
  I, J: Integer;
  Strings: TStrings;
begin
  I := FSections.IndexOf(Section);
  if I >= 0 then
  begin
    FModified := True;
    Strings := TStrings(FSections.Objects[I]);
    J := Strings.IndexOfName(Ident);
    if J >= 0 then
      Strings.Delete(J);
  end;
end;

procedure TVonConfig.EraseSection(const Section: string);
var
  I: Integer;
begin
  I := FSections.IndexOf(Section);
  if I >= 0 then
  begin
    FModified := True;
    TStrings(FSections.Objects[I]).Free;
    FSections.Delete(I);
  end;
end;

function TVonConfig.GetCaseSensitive: Boolean;
begin
  Result := FSections.CaseSensitive;
end;

function TVonConfig.GetDirectionSection(SectionID: Integer): TStrings;
begin
  if SectionID < FSections.Count then
    Result := TStrings(FSections.Objects[SectionID])
  else
    Result := nil;
end;

function TVonConfig.GetDirectionSection(SectionName: string): TStrings;
var
  I: Integer;
begin
  I := FSections.IndexOf(SectionName);
  Result := nil;
  if I >= 0 then
    Result := TStrings(FSections.Objects[I]);
end;

function TVonConfig.GetSection(Index: Integer): string;
begin
  Result := FSections[Index];
end;

function TVonConfig.GetSectionCount: Integer;
begin
  Result := FSections.Count;
end;

function TVonConfig.GetSectionNames(Index: Integer): string;
begin
  Result := FSections[Index];
end;

function TVonConfig.GetSectionValues(Index: Integer): string;
begin
  Result := FSections.ValueFromIndex[Index];
end;

procedure TVonConfig.GetStrings(List: TStrings);
var
  I: Integer;
  Strings: TStrings;
begin
  List.BeginUpdate;
  try
    for I := 0 to FSections.Count - 1 do
    begin
      List.Add('[' + FSections[I] + ']');
      Strings := TStrings(FSections.Objects[I]);
      List.AddStrings(Strings);
      List.Add('');
    end;
  finally
    List.EndUpdate;
  end;
end;

function TVonConfig.GetText: string;
var
  List: TStringList;
begin
  List := TStringList.Create;
  try
    GetStrings(List);
    Result := List.Text;
  finally
    List.Free;
  end;
end;

function TVonConfig.IdentExists(const Section, Ident: string): Boolean;
var
  I: Integer;
begin
  I := FSections.IndexOf(Section);
  if I >= 0 then
    Result := TStrings(FSections.Objects[I]).IndexOfName(Ident) >= 0
  else
    Result := false;
end;

procedure TVonConfig.LoadFromFile(Filename: string);
var
  List: TStringList;
begin
  FFileName := Filename;
  if (FFileName <> '') and FileExists(FFileName) then
  begin
    List := TStringList.Create;
    try
      List.LoadFromFile(FFileName);
      SetStrings(List);
    finally
      List.Free;
    end;
  end
  else
    Clear;
end;

procedure TVonConfig.LoadValues(const Text: string);
var
  List: TStringList;
begin
  List := TStringList.Create;
  try
    List.Text := Text;
    SetStrings(List);
  finally
    List.Free;
  end;
end;

procedure TVonConfig.Move(orgIdx, newIdx: Integer);
begin
  FSections.Move(orgIdx, newIdx);
end;

procedure TVonConfig.Move(const Section: string; orgIdx, newIdx: Integer);
var
  I: Integer;
begin
  I := FSections.IndexOf(Section);
  if I >= 0 then
  begin
    FModified := True;
    TStrings(FSections.Objects[I]).Move(orgIdx, newIdx);
  end;
end;

function TVonConfig.ReadBinaryStream(const Section, Name: string; Value:
    TStream): Integer;
var
  Text: string;
  Stream: TMemoryStream;
  Pos: Integer;
  dataLen: Integer;
  DataBytes: TBytes;
begin
  Text := ReadString(Section, Name, '');
  if Text <> '' then
  begin
    if Value is TMemoryStream then
      Stream := TMemoryStream(Value)
    else
      Stream := TMemoryStream.Create;

    try
      dataLen := Length(Text) div 2;
      SetLength(DataBytes, dataLen);
      Pos := Stream.Position;
      System.Classes.HexToBin(BytesOf(Text), 0, DataBytes, 0, dataLen);
      Stream.Write(DataBytes[0], dataLen);
      Stream.Position := Pos;
      if Value <> Stream then
        Value.CopyFrom(Stream, dataLen);
      Result := Stream.Size - Pos;
    finally
      if Value <> Stream then
        Stream.Free;
    end;
  end
  else
    Result := 0;
end;

function TVonConfig.ReadBool(const Section, Ident: string; Default: Boolean):
    Boolean;
begin
  Result := ReadInteger(Section, Ident, Ord(Default)) <> 0;
end;

function TVonConfig.ReadDate(const Section, Name: string; Default: TDateTime):
    TDateTime;
var
  DateStr: string;
begin
  DateStr := ReadString(Section, Name, '');
  Result := Default;
  if DateStr <> '' then // yyyy-mm-dd
    try
      Result := EncodeDate(StrToInt(Copy(DateStr, 1, 4)),
        StrToInt(Copy(DateStr, 6, 2)), StrToInt(Copy(DateStr, 9, 2)));
    except
      on EConvertError do
        // Ignore EConvertError exceptions
      else
        raise;
    end;
end;

function TVonConfig.ReadDateTime(const Section, Name: string; Default:
    TDateTime): TDateTime;
var
  DateStr: string;
begin
  DateStr := ReadString(Section, Name, '');
  Result := Default;
  if DateStr <> '' then // yyyy-mm-dd hh:nn:ss
    try
      Result := EncodeDate(StrToInt(Copy(DateStr, 1, 4)),
        StrToInt(Copy(DateStr, 6, 2)), StrToInt(Copy(DateStr, 9, 2))) +
        EncodeTime(StrToInt(Copy(DateStr, 12, 2)), StrToInt(Copy(DateStr, 15, 2)
        ), StrToInt(Copy(DateStr, 18, 2)), 0);
    except
      on EConvertError do
        // Ignore EConvertError exceptions
      else
        raise;
    end;
end;

function TVonConfig.ReadFloat(const Section, Name: string; Default: Double):
    Double;
var
  FloatStr: string;
begin
  FloatStr := ReadString(Section, Name, '');
  Result := Default;
  if FloatStr <> '' then
    try
      Result := StrToFloat(FloatStr);
    except
      on EConvertError do
        // Ignore EConvertError exceptions
      else
        raise;
    end;
end;

function TVonConfig.ReadInteger(const Section, Ident: string; Default:
    Longint): LongInt;
var
  IntStr: string;
begin
  IntStr := ReadString(Section, Ident, '');
  if (Length(IntStr) > 2) and (IntStr[1] = '0') and
    ((IntStr[2] = 'X') or (IntStr[2] = 'x')) then
    IntStr := '$' + Copy(IntStr, 3, Maxint);
  Result := StrToIntDef(IntStr, Default);
end;

procedure TVonConfig.ReadSection(const Section: string; Strings: TStrings);
var
  I, J: Integer;
  SectionStrings: TStrings;
begin
  Strings.BeginUpdate;
  try
    Strings.Clear;
    I := FSections.IndexOf(Section);
    if I >= 0 then
    begin
      SectionStrings := TStrings(FSections.Objects[I]);
      for J := 0 to SectionStrings.Count - 1 do
        Strings.Add(SectionStrings.Names[J]);
    end;
  finally
    Strings.EndUpdate;
  end;
end;

function TVonConfig.ReadSectionCount: Integer;
begin
  Result := FSections.Count;
end;

function TVonConfig.ReadSectionName(const SectionID: Integer): string;
begin
  if (SectionID >= 0) and (SectionID < FSections.Count) then
    Result := FSections.Strings[SectionID];
end;

procedure TVonConfig.ReadSections(Strings: TStrings);
begin
  Strings.Assign(FSections);
end;

function TVonConfig.ReadSectionText(const Section: string): string;
var
  I: Integer;
begin
  I := FSections.IndexOf(Section);
  if I >= 0 then
    Result := TStrings(FSections.Objects[I]).Text
  else
    Result := '';
end;

procedure TVonConfig.ReadSectionValues(const Section: string; Strings:
    TStrings);
var
  I: Integer;
begin
  Strings.BeginUpdate;
  try
    Strings.Clear;
    I := FSections.IndexOf(Section);
    if I >= 0 then
      Strings.Assign(TStrings(FSections.Objects[I]));
  finally
    Strings.EndUpdate;
  end;
end;

function TVonConfig.ReadString(const Section, Ident, Default: string): string;
var
  I: Integer;
  Strings: TStrings;
begin
  I := FSections.IndexOf(Section);
  if I >= 0 then
  begin
    Strings := TStrings(FSections.Objects[I]);
    I := Strings.IndexOfName(Ident);
    if I >= 0 then
    begin
      Result := Copy(Strings[I], Length(Ident) + 2, Maxint);
      Exit;
    end;
  end;
  Result := Default;
end;

function TVonConfig.ReadTime(const Section, Name: string; Default: TDateTime):
    TDateTime;
var
  TimeStr: string;
begin
  TimeStr := ReadString(Section, Name, '');
  Result := Default;
  if TimeStr <> '' then // hh:nn:ss
    try
      Result := EncodeTime(StrToInt(Copy(TimeStr, 1, 2)),
        StrToInt(Copy(TimeStr, 4, 2)), StrToInt(Copy(TimeStr, 7, 2)), 0);
    except
      on EConvertError do
        // Ignore EConvertError exceptions
      else
        raise;
    end;
end;

procedure TVonConfig.Rename(const ConfigText: string; Reload: Boolean);
begin
  if Reload then
    LoadValues(ConfigText);
end;

procedure TVonConfig.RenameSection(const OrgName, NewName: string);
var
  I: Integer;
begin
  I := FSections.IndexOf(OrgName);
  if I >= 0 then
    FSections[I] := NewName;
end;

procedure TVonConfig.SaveToFile(Filename: string);
var
  List: TStringList;
begin
  List := TStringList.Create;
  try
    GetStrings(List);
    List.SaveToFile(Filename);
  finally
    List.Free;
  end;
end;

function TVonConfig.SectionExists(const Section: string): Boolean;
begin
  Result := FSections.IndexOf(Section) >= 0;
end;

procedure TVonConfig.SetCaseSensitive(Value: Boolean);
var
  I: Integer;
begin
  if Value <> FSections.CaseSensitive then
  begin
    FSections.CaseSensitive := Value;
    for I := 0 to FSections.Count - 1 do
      with TStringList(FSections.Objects[I]) do
      begin
        CaseSensitive := Value;
        // Changed;
      end;
    // THashedStringList(FSections).Changed;
  end;
end;

procedure TVonConfig.SetFilename(const Value: string);
begin
  FFileName := Value;
end;

procedure TVonConfig.SetModified(const Value: Boolean);
begin
  FModified := Value;
end;

procedure TVonConfig.SetSectionValues(Index: Integer; const Value: string);
begin
  if Index < FSections.Count then
  begin
    FSections[Index] := FSections.Names[Index] + '=' + Value;
    FModified := True;
  end;
end;

procedure TVonConfig.SetStrings(List: TStrings);
var
  I, J: Integer;
  S: string;
  Strings: TStrings;
begin
  FModified := True;
  Clear;
  Strings := nil;
  for I := 0 to List.Count - 1 do
  begin
    S := List[I];
    if (S <> '') and (S[1] <> ';') then
      if (S[1] = '[') and (S[Length(S)] = ']') then
      begin
        Delete(S, 1, 1);
        SetLength(S, Length(S) - 1);
        Strings := AddSection(Trim(S));
      end
      else if Strings <> nil then
      begin
        J := Pos('=', S);
        if J > 0 then // remove spaces before and after '='
          Strings.Add(Trim(Copy(S, 1, J - 1)) + '=' + Copy(S, J + 1, Maxint))
        else
          Strings.Add(S);
      end;
  end;
end;

procedure TVonConfig.SetText(const Value: string);
begin
  LoadValues(Value);
end;

procedure TVonConfig.UpdateFile;
var
  List: TStringList;
begin
  List := TStringList.Create;
  try
    GetStrings(List);
    List.SaveToFile(FFileName);
  finally
    List.Free;
  end;
end;

procedure TVonConfig.WriteBinaryStream(const Section, Name: string; Value:
    TStream);
var
  Text: string;
  Stream: TMemoryStream;
begin
  SetLength(Text, (Value.Size - Value.Position) * 2);
  if Length(Text) > 0 then
  begin
    if Value is TMemoryStream then
      Stream := TMemoryStream(Value)
    else
      Stream := TMemoryStream.Create;
    try
      if Stream <> Value then
      begin
        Stream.CopyFrom(Value, Value.Size - Value.Position);
        Stream.Position := 0;
      end;
      BinToHex(PChar(Integer(Stream.Memory) + Stream.Position), PChar(Text),
        Stream.Size - Stream.Position);
    finally
      if Value <> Stream then
        Stream.Free;
    end;
  end;
  WriteString(Section, Name, Text);
end;

procedure TVonConfig.WriteBool(const Section, Ident: string; Value: Boolean);
const
  Values: array [Boolean] of string = ('0', '1');
begin
  WriteString(Section, Ident, Values[Value]);
end;

procedure TVonConfig.WriteDate(const Section, Name: string; Value: TDateTime);
var
  S: string;
begin
  DateTimeToString(S, 'yyyy-mm-dd', Value);
  WriteString(Section, Name, S);
end;

procedure TVonConfig.WriteDateTime(const Section, Name: string; Value:
    TDateTime);
var
  S: string;
begin
  DateTimeToString(S, 'yyyy-mm-dd hh:nn:ss', Value);
  WriteString(Section, Name, S);
end;

procedure TVonConfig.WriteFloat(const Section, Name: string; Value: Double);
begin
  WriteString(Section, Name, FloatToStr(Value));
end;

procedure TVonConfig.WriteInteger(const Section, Ident: string; Value: Longint);
begin
  WriteString(Section, Ident, IntToStr(Value));
end;

procedure TVonConfig.WriteSection(const SectionID: Integer; StringsText:
    string);
begin
  FModified := True;
  if (SectionID >= 0) and (SectionID < FSections.Count) then
  begin
    TStrings(FSections.Objects[SectionID]).Text := StringsText;
    FModified := True;
  end;
end;

procedure TVonConfig.WriteSection(const SectionID: Integer; Strings: TStrings);
begin
  FModified := True;
  if (SectionID >= 0) and (SectionID < FSections.Count) then
  begin
    TStrings(FSections.Objects[SectionID]).Assign(Strings);
    FModified := True;
  end;
end;

procedure TVonConfig.WriteSection(const Section: string; StringsText: string);
var
  I: Integer;
  Strings: TStrings;
begin
  FModified := True;
  I := FSections.IndexOf(Section);
  if I >= 0 then
    Strings := TStrings(FSections.Objects[I])
  else
    Strings := AddSection(Section);
  Strings.Text := StringsText;
end;

procedure TVonConfig.WriteSection(const Section: string; Strings: TStrings);
var
  I: Integer;
  szStrings: TStrings;
begin
  FModified := True;
  I := FSections.IndexOf(Section);
  if I >= 0 then
    szStrings := TStrings(FSections.Objects[I])
  else
    szStrings := AddSection(Section);
  szStrings.Assign(Strings);
end;

procedure TVonConfig.WriteString(const Section, Ident, Value: String);
var
  I: Integer;
  S: string;
  Strings: TStrings;
begin
  FModified := True;
  I := FSections.IndexOf(Section);
  if I >= 0 then
    Strings := TStrings(FSections.Objects[I])
  else
    Strings := AddSection(Section);
  S := Ident + '=' + Value;
  I := Strings.IndexOfName(Ident);
  if I >= 0 then
    Strings[I] := S
  else
    Strings.Add(S);
end;

procedure TVonConfig.WriteTime(const Section, Name: string; Value: TDateTime);
var
  S: string;
begin
  DateTimeToString(S, 'hh:nn:ss', Value);
  WriteString(Section, Name, S);
end;

{ TSettingValue }

{
********************************** TVonParams **********************************
}
constructor TVonParams.Create;
begin
            // 构造函数
  FDelimiter:= #3;
  FParagraph:= #4;
  FCount:= 0;
  SetCapacity(0);
end;

destructor TVonParams.Destroy;
begin
  FCount:= 0;
  SetCapacity(0);
end;

procedure TVonParams.Assign(Source: TVonParams);
var
  I: Integer;
begin
  if FCapacity < Source.FCapacity then SetCapacity(Source.FCapacity);
  FCount:= Source.FCount;
  for I := 0 to Source.FCount - 1 do begin
    FKeyList[I]:= Source.FKeyList[I];
    FValueList[I]:= Source.FValueList[I];
    FPromptList[I]:= Source.FPromptList[I];
    FDefaultList[I]:= Source.FDefaultList[I];
    FTypeList[I]:= Source.FTypeList[I];
    FOptionList[I]:= Source.FOptionList[I];
  end;
end;

procedure TVonParams.Clear;
begin
  FCount:= 0;
end;

procedure TVonParams.Delete(const Index: Integer);
var
  I: Integer;
begin
  for I := Index to FCount - 2 do begin
    FKeyList[I]:= FKeyList[I + 1];
    FValueList[I]:= FValueList[I + 1];
    FPromptList[I]:= FPromptList[I + 1];
    FDefaultList[I]:= FDefaultList[I + 1];
    FTypeList[I]:= FTypeList[I + 1];
    FOptionList[I]:= FOptionList[I + 1];
  end;
  Dec(FCount);
end;

procedure TVonParams.Delete(const Name: string);
var
  Idx: Integer;
begin
  Idx := IndexOfName(Name);
  if Idx >= 0 then Delete(Idx);
end;

function TVonParams.GetDefaultValue(Index: Integer): string;
begin
  if(Index < 0)or(Index > FCount)then raise Exception.Create(RES_OUT_OF_RANG);
  Result:= FDefaultList[Index];
end;

function TVonParams.GetNames(Index: Integer): string;
begin
  if(Index < 0)or(Index > FCount)then raise Exception.Create(RES_OUT_OF_RANG);
  Result:= FKeyList[Index];
end;

function TVonParams.GetNameValue(Name: string): string;
var
  Idx: Integer;
begin
  Idx := IndexOfName(Name);
  if Idx < 0 then Result:= ''
  else Result:= FValueList[Idx];
end;

function TVonParams.GetOptions(Index: Integer): string;
begin
  if(Index < 0)or(Index > FCount)then raise Exception.Create(RES_OUT_OF_RANG);
  Result:= FOptionList[Index];
end;

function TVonParams.GetParamsText: string;
begin
end;

function TVonParams.GetPrompts(Index: Integer): string;
begin
  if(Index < 0)or(Index > FCount)then raise Exception.Create(RES_OUT_OF_RANG);
  Result:= FPromptList[Index];
end;

function TVonParams.GetText: string;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to FCount - 1 do
    Result := Result + FKeyList[I] + FDelimiter + FPromptList[I] + FDelimiter +
      FValueList[I] + FDelimiter + FDefaultList[I] + FParagraph;
end;

function TVonParams.GetTypes(Index: Integer): TVonParamType;
begin
  if(Index < 0)or(Index > FCount)then raise Exception.Create(RES_OUT_OF_RANG);
  Result:= FTypeList[Index];
end;

function TVonParams.GetValues(Index: Integer): string;
begin
  if(Index < 0)or(Index > FCount)then raise Exception.Create(RES_OUT_OF_RANG);
  Result:= FValueList[Index];
end;

procedure TVonParams.Grow;
var
  Delta: Integer;
begin
          // 成长一下系统实际开创的记录空间
  if FCapacity > 64 then
    Delta := FCapacity div 4
  else if FCapacity > 8 then
    Delta := 16
  else
    Delta := 4;
  SetCapacity(FCapacity + Delta);
end;

function TVonParams.IndexOfName(const Name: string): Integer;
var
  I: Integer;
begin
  Result:= -1;
  for I := 0 to FCount - 1 do
    if SameText(FKeyList[I], Name) then begin
      Result:= I;
      Exit;
    end;
end;

procedure TVonParams.Merge(Source: TVonParams);
var
  I: Integer;
begin
  for I := 0 to Source.Count - 1 do
    save(Source.Names[I], Source.Prompts[I], Source.Values[I], Source.DefaultValue[I],
      Source.Types[I], Source.Options[I]);
end;

procedure TVonParams.Move(Org, Dest: Integer);
var
  szN, szP, szV, szD, szO: string;
  offset: Integer;
  szT: TVonParamType;
begin
  if Org = Dest then Exit;
  if Org > Dest then offset:= -1 else offset:= 1;
  szN:= FKeyList[Org];
  szP:= FPromptList[Org];
  szV:= FValueList[Org];
  szD:= FDefaultList[Org];
  szT:= FTypeList[Org];
  szO:= FOptionList[Org];
  repeat
    FKeyList[Org]:= FKeyList[Org + offset];
    FPromptList[Org]:= FPromptList[Org + offset];
    FValueList[Org]:= FValueList[Org + offset];
    FDefaultList[Org]:= FDefaultList[Org + offset];
    FTypeList[Org]:= FTypeList[Org + offset];
    FOptionList[Org]:= FOptionList[Org + offset];
    Inc(Org, offset);
  until Org = dest;
  FKeyList[Dest]:= szN;
  FPromptList[Dest]:= szP;
  FValueList[Dest]:= szV;
  FDefaultList[Dest]:= szD;
  FTypeList[Dest]:= szT;
  FOptionList[Dest]:= szO;
end;

function TVonParams.Save(const Name, Prompt, Value, DefaultValue: string;
    ValueType: TVonParamType; Option: string): Integer;
begin
  if Name = '' then Exit;
  Result:= IndexOfName(Name);
  if Result < 0 then begin
    if FCount = FCapacity then Grow;
    FKeyList[FCount]:= PChar(Name);
    FValueList[FCount]:= Value;
    FPromptList[FCount]:= Prompt;
    FDefaultList[FCount]:= DefaultValue;
    FOptionList[FCount]:= Option;
    FTypeList[FCount]:= ValueType;
    Inc(FCount);
    Result:= FCount - 1;
  end else begin
    FValueList[Result]:= Value;
    FPromptList[Result]:= Prompt;
    FDefaultList[Result]:= DefaultValue;
    FOptionList[Result]:= Option;
    FTypeList[Result]:= ValueType;
  end;
end;

procedure TVonParams.SetCapacity(const Value: Integer);
begin
          // 设定当前实际空间数量
  if (Value < FCount) or (Value > MaxListSize) then
    raise EListError.CreateFmt(SListCapacityError, [Value]);
  if Value <> FCapacity then
  begin
    SetLength(FKeyList, Value);
    SetLength(FValueList, Value);
    SetLength(FPromptList, Value);
    SetLength(FDefaultList, Value);
    SetLength(FTypeList, Value);
    SetLength(FOptionList, Value);
    FCapacity := Value;
  end;
end;

procedure TVonParams.SetDefaultValue(Index: Integer; const Value: string);
begin
  if(Index > 0)or(Index > FCount)then raise Exception.Create(RES_OUT_OF_RANG);
  FDefaultList[Index]:= Value;
end;

procedure TVonParams.SetNames(Index: Integer; const Value: string);
begin
  if(Index > 0)or(Index > FCount)then raise Exception.Create(RES_OUT_OF_RANG);
  FKeyList[Index]:= Value;
end;

procedure TVonParams.SetOptions(Index: Integer; const Value: string);
begin
  if(Index > 0)or(Index > FCount)then raise Exception.Create(RES_OUT_OF_RANG);
  FOptionList[Index]:= Value;
end;

procedure TVonParams.SetPrompts(Index: Integer; const Value: string);
begin
  if(Index > 0)or(Index > FCount)then raise Exception.Create(RES_OUT_OF_RANG);
  FPromptList[Index]:= Value;
end;

procedure TVonParams.SetText(const Value: string);
var
  P: PChar;
  S: string;
  vIdx: Integer;
begin
  P:= PChar(Value);
  vIdx:= 0;
  Clear;
  while P^ <> #0 do begin
    if(P^ = FDelimiter)or(P^ = FParagraph)then begin
      case vIdx of
      0: begin
          if FCount = FCapacity then Grow;
          FKeyList[FCount]:= S;
          Inc(FCount);
      end;
      1: FPromptList[FCount - 1]:= S;
      2: FValueList[FCount - 1]:= S;
      3: begin FDefaultList[FCount - 1]:= S; vIdx:= -1; end;
      end;
      Inc(vIdx);
      S:= '';
    end else S:= S + P^;
    Inc(P);
  end;
end;

procedure TVonParams.SetTypes(Index: Integer; const Value: TVonParamType);
begin
  if(Index > 0)or(Index > FCount)then raise Exception.Create(RES_OUT_OF_RANG);
  FTypeList[Index]:= Value;
end;

procedure TVonParams.SetValues(Index: Integer; const Value: string);
begin
  if(Index > 0)or(Index > FCount)then raise Exception.Create(RES_OUT_OF_RANG);
  FValueList[Index]:= Value;
end;

{ TVonList }

{
*********************************** TVonList ***********************************
}
constructor TVonList.Create;
begin
  // New(FList);
  FDelimiter := ',';
  FCount := 0;
end;

destructor TVonList.Destroy;
begin
  // Dispose(FList);
  inherited;
end;

procedure TVonList.Add(Value: string);
begin
  FList[FCount] := Value;
  Inc(FCount);
end;

procedure TVonList.Clear;
begin
  FCount := 0;
end;

procedure TVonList.Delete(const Index: Integer);
var
  I: Integer;
begin
  for I := Index to FCount - 2 do
    FList[I] := FList[I + 1];
  Dec(FCount);
end;

procedure TVonList.Delete(const Value: string);
var
  I: Integer;
begin
  for I := 0 to FCount - 1 do
    if FList[I] = Value then
    begin
      Delete(I);
      Exit;
    end;
end;

function TVonList.Get(Index: Integer): string;
begin
  if Index > FCount then
    Result := ''
  else Result := FList[Index];
end;

function TVonList.GetProtectedText: string;
var
  I: Integer;
begin
  Result := FList[0];
  for I := 1 to FCount - 1 do
    if Pos(FDelimiter, FList[I]) > 0 then
      Result := Result + FDelimiter + '"' + FList[I] + '"'
    else Result := Result + FDelimiter + FList[I];
end;

function TVonList.GetText: string;
var
  I: Integer;
begin
  Result := FList[0];
  for I := 1 to FCount - 1 do
    Result := Result + FDelimiter + FList[I];
end;

function TVonList.IndexOf(Value: string): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to FCount - 1 do
    if FList[I] = Value then
    begin
      Result := I;
      Exit;
    end;
end;

procedure TVonList.Put(Index: Integer; const Value: string);
begin
  FList[Index] := Value;
end;

procedure TVonList.SetDelimiter(const Value: Char);
begin
  FDelimiter := Value;
end;

procedure TVonList.SetProtectedText(const Value: string);
var
  P: PChar;
  S: string;
  function GetQuotation: string;
  var
    QuotationMark: Char;
  begin
    QuotationMark:= P^;
    Result:= '';
    while P^ = QuotationMark do begin
      Result := Result + P^;
      Inc(P);
      while P^ <> QuotationMark do begin
        Result := Result + P^;
        Inc(P);
      end;
      Inc(P);
    end;
    System.Delete(Result, 1, 1);
  end;
begin
  Clear;
  if Value = '' then
    Exit;
  P := PChar(Value);
  S := '';
  repeat
    if P^ = FDelimiter then
    begin
      Add(S);
      S := '';
      Inc(P);
    end
    else if(S = '')and((P^ = '"')or(P^ = ''''))then
      S := GetQuotation
    else begin
      S := S + P[0];
      Inc(P);
    end
  until P[0] = #0;
  if S <> '' then
    Add(S);
end;

procedure TVonList.SetText(const Value: string);
var
  P: PChar;
  S: string;
begin
  Clear;
  if Value = '' then
    Exit;
  P := PChar(Value);
  S := '';
  repeat
    if P[0] = FDelimiter then
    begin
      Add(S);
      S := '';
    end
    else
      S := S + P[0];
    Inc(P);
  until P[0] = #0;
  if S <> '' then
    Add(S);
end;

{ TVonXmlCfg }

{
********************************** TVonXmlCfg **********************************
}
procedure TVonXmlCfg.Assign(Source: TPersistent);
begin
  if Source is TVonXmlCfg then
    XmlText := TVonXmlCfg(Source).XmlText
  else if Source is TVonConfig then
    Text := TVonConfig(Source).Text
  else if Source is TField then
    XmlText := TField(Source).AsString
  else if Source is TMemoField then
    XmlText := TField(Source).AsString;
  // inherited;
end;

procedure TVonXmlCfg.AssignTo(Dest: TPersistent);
begin
  // inherited;
  if Dest is TVonConfig then
    TVonConfig(Dest).Text := Text
  else if Dest is TVonXmlCfg then
    TVonXmlCfg(Dest).Text := Text
  else if Dest is TField then
    TField(Dest).AsString := XmlText
  else if Dest is TMemoField then
    TField(Dest).AsString := XmlText;
end;

function TVonXmlCfg.GetXmlText: string;
var
  XMLDoc: TXMLDocument;
  rootNode, SectionNode: IXMLNode;
  I, J: Integer;
begin
  XMLDoc := TXMLDocument.Create(Application);
  XMLDoc.Active := True;
  XMLDoc.Encoding := 'UTF-8';
  rootNode := XMLDoc.AddChild('CONFIG');
  for I := 0 to ReadSectionCount - 1 do
  begin
    SectionNode := rootNode.AddChild(SectionNames[I]);
    with GetDirectionSection(I) do
      for J := 0 to Count - 1 do
        SectionNode.AddChild(Names[J]).Text := ValueFromIndex[J];
  end;
  Result := XMLDoc.XML.Text;
  XMLDoc.Active := false;
end;

procedure TVonXmlCfg.SetXmlText(const Value: string);
var
  XMLDoc: TXMLDocument;
  rootNode, SectionNode, ValueNode: IXMLNode;
  SectionName: string;
  I, J: Integer;
begin
  XMLDoc := TXMLDocument.Create(Application);
  XMLDoc.XML.Clear;
  XMLDoc.Active := True;
  XMLDoc.LoadFromXML(Value);
  rootNode := XMLDoc.ChildNodes.FindNode('CONFIG');
  if not Assigned(rootNode) then
    Exit;
  for I := 0 to rootNode.ChildNodes.Count - 1 do
  begin
    SectionNode := rootNode.ChildNodes[I];
    SectionName := SectionNode.NodeName;
    if SectionNode.ChildNodes.Count = 0 then
      Continue;
    ValueNode := SectionNode.ChildNodes[0];
    while Assigned(ValueNode) do
    begin
      WriteString(SectionName, ValueNode.NodeName, ValueNode.Text);
      ValueNode := ValueNode.NextSibling;
    end;
  end;
  XMLDoc.Active := false;
end;

{ TVonSetting }

{
********************************* TVonSetting **********************************
}
constructor TVonSetting.Create;
begin       // 构造函数
  SetCount(0);
  SetCapacity(0);
end;

destructor TVonSetting.Destroy;
begin      // 析构函数
  Clear;
  inherited;
end;

function TVonSetting.Add(const Name, Value: string): Integer;
var      // 添加一个内容
  Idx: Integer;
  szKey, szValue: PWideChar;
begin
  Result := IndexOfName(Name);
  if Result >= 0 then
  begin  // 信息已经存在，替换掉原有数据
    FreeMem(FValueList^[Result]);
    FValueList^[Result] := NewItem(Value);
  end
  else
  begin
    if FCount = FCapacity then
      Grow;
    Result := FCount;
    FKeyList^[Result] := NewItem(Name);
    FValueList^[Result] := NewItem(Value);
    Inc(FCount);
  end;
end;

procedure TVonSetting.Clear;
var       // 清除所有缓存
  szKey, szValue: PWideChar;
begin
  SetCount(0);
  SetCapacity(0);
end;

procedure TVonSetting.Delete(const Index: Integer);
var       // 删除一个指定位置的数据
  szKey, szValue: PWideChar;
begin
  if (Index < 0) or (Index >= FCount) then
    raise EListError.CreateFmt(SListIndexError, [Index]);
  Move(Index, FCount - 1);
  FreeItem(FCount - 1);
  Dec(FCount);
end;

procedure TVonSetting.Delete(const Name: string);
var
  Idx: Integer;
begin
      // 删除一个内容
  Idx := IndexOfName(Name);
  if Idx >= 0 then
    Delete(Idx);
end;

procedure TVonSetting.FreeItem(const Index: Integer);
begin
      // 释放一个节点单元空间
  FreeMem(FKeyList^[Index]);
  FreeMem(FValueList^[Index]);
end;

function TVonSetting.GetBreakText: string;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to FCount - 1 do
    Result := Result + '|' + string(FKeyList^[I]) + '=' +
      string(FValueList^[I]);
  if FCount > 0 then
    System.Delete(Result, 1, 1);
end;

function TVonSetting.GetCommaText: string;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to FCount - 1 do
    Result := Result + ',' + string(FKeyList^[I]) + '=' +
      string(FValueList^[I]);
  if FCount > 0 then
    System.Delete(Result, 1, 1);
end;

function TVonSetting.GetNames(Index: Integer): string;
begin
      // 根据序号得到键值
  if (Index < 0) or (Index >= FCount) then
    raise EListError.CreateFmt(SListIndexError, [Index]);
  Result := string(FKeyList^[Index]);
end;

function TVonSetting.GetNameValue(Name: string): string;
var
  Idx: Integer;
begin
      // 根据键值得到内容
  Idx := IndexOfName(Name);
  Result := '';
  if Idx >= 0 then
    Result := GetValues(Idx);
end;

function TVonSetting.GetParamsText: string;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to FCount - 1 do
    Result := Result + ';' + string(FKeyList^[I]) + '=' +
      string(FValueList^[I]);
  if FCount > 0 then
    System.Delete(Result, 1, 1);
end;

function TVonSetting.GetSettingText: string;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to FCount - 1 do
    Result := Result + string(FKeyList^[I]) + '(' +
      string(FValueList^[I]) + ')';
end;

function TVonSetting.GetSpeicalText: string;
var
  I: Integer;
begin
      // 得到Specail设置内容
  Result := '';
  for I := 0 to FCount - 1 do
    Result := Result + string(FKeyList^[I]) + #4 + string(FValueList^[I]) + #2;
end;

function TVonSetting.GetText(Style: EVonSettingKind): string;
begin
      // 得到设置内容
  case Style of
    VSK_PARENTHESES:
      Result := GetParamsText;
    VSK_SEMICOLON:
      Result := GetSettingText;
    VSK_COMMA:
      Result := GetCommaText;
    VSK_SPEICAL:
      Result := GetSpeicalText;
    VSK_BREAK:
      Result := GetBreakText;
  end;
end;

function TVonSetting.GetValues(Index: Integer): string;
begin
      // 根据序号得到设置内容
  if (Index < 0) or (Index >= FCount) then
    raise EListError.CreateFmt(SListIndexError, [Index]);
  Result := string(FValueList^[Index]);
end;

procedure TVonSetting.Grow;
var
  Delta: Integer;
begin
      // 成长一下系统实际开创的记录空间
  if FCapacity > 64 then
    Delta := FCapacity div 4
  else if FCapacity > 8 then
    Delta := 16
  else
    Delta := 4;
  SetCapacity(FCapacity + Delta);
end;

function TVonSetting.IndexOfName(const Name: string): Integer;
var
  I: Integer;
begin
      // 得到键值的索引
  Result := -1;
  for I := 0 to FCount - 1 do
  begin
    if StrIComp(FKeyList^[I], PWideChar(Name)) = 0 then
    begin
      Result := I;
      Exit;
    end;
  end;
end;

function TVonSetting.IndexOfValue(const Value: string): Integer;
var
  I: Integer;
begin
      // 得到内容的索引
  Result := -1;
  for I := 0 to FCount - 1 do
  begin
    if StrComp(FValueList^[I], PWideChar(Value)) = 0 then
    begin
      Result := I;
      Exit;
    end;
  end;
end;

procedure TVonSetting.Insert(Index: Integer; const Name, Value: string);
var
  Idx: Integer;
begin
  Idx := Add(Name, Value);
  Move(Idx, Index);
end;

procedure TVonSetting.LoadFromFile(Filename: string; Style: EVonSettingKind);
var
  aStream: TFileStream;
begin
  aStream := TFileStream.Create(Filename, fmOpenRead);
  LoadFromStream(aStream, Style);
  aStream.Free;
end;

procedure TVonSetting.LoadFromStream(aStream: TStream; Style: EVonSettingKind);
var
  arrData: TBytes;
begin
  aStream.Position := 0;
  SetLength(arrData, aStream.Size);
  aStream.Read(arrData[0], aStream.Size);
  Text[Style] := TEncoding.Unicode.GetString(arrData);
  SetLength(arrData, 0);
end;

procedure TVonSetting.Move(const SourceIdx, DestIdx: Integer);
var
  I: Integer;
  Pn, Pv: PWideChar;
begin
      // 移动一个节点
  if SourceIdx = DestIdx then
    Exit;
  if (SourceIdx < 0) or (SourceIdx >= FCount) then
    raise EListError.CreateFmt(SListIndexError, [SourceIdx]);
  if (DestIdx < 0) or (DestIdx >= FCount) then
    raise EListError.CreateFmt(SListIndexError, [DestIdx]);
  Pn := FKeyList^[SourceIdx];
  Pv := FValueList^[SourceIdx];
  if SourceIdx < DestIdx then
    for I := SourceIdx to DestIdx - 1 do
    begin // 1-4,
      FKeyList^[I] := FKeyList^[I + 1];
      FValueList^[I] := FValueList^[I + 1];
    end
  else
    for I := SourceIdx downto DestIdx - 1 do
    begin // 4-1
      FKeyList^[I] := FKeyList^[I - 1];
      FValueList^[I] := FValueList^[I - 1];
    end;
  FKeyList^[DestIdx] := Pn;
  FValueList^[DestIdx] := Pv;
end;

function TVonSetting.NewItem(const Value: string): PWideChar;
begin
      // 创建一个新的节点内容
  GetMem(Result, Length(Value) * SizeOf(WideChar) + SizeOf(WideChar));
  Result := StrLCopy(Result, PWideChar(Value), Length(Value));
  Result[Length(Value)] := #0;
end;

function TVonSetting.NextChar(var P: PWideChar; ch: char): string;
begin
  Result := '';
  Inc(P);
  while P^ <> ch do
  begin
    Result := Result + P^;
    Inc(P);
  end;
end;

function TVonSetting.Rename(const oldName, NewName: string): Integer;
var
  Idx: Integer;
begin
      // 更换某一特定键值内容
  if oldName = NewName then
    Exit;
  if IndexOfName(NewName) >= 0 then
    raise Exception.Create('发现重复键值，不能完成更名操作。');
  Idx := IndexOfName(oldName);
  if Idx < 0 then
    raise Exception.Create('未发现要修改的键值，不能完成更名操作。');
  FreeMem(FKeyList^[Idx]);
  FKeyList^[Idx] := NewItem(NewName);
end;

procedure TVonSetting.SaveToFile(Filename: string; Style: EVonSettingKind);
var
  aStream: TFileStream;
begin
  aStream := TFileStream.Create(Filename, fmOpenRead);
  SaveToStream(aStream, Style);
end;

procedure TVonSetting.SaveToStream(aStream: TStream; Style: EVonSettingKind);
var
  arr: TBytes;
begin
  arr := TEncoding.Unicode.GetBytes(Text[Style]);
  aStream.Write(arr[0], Length(arr));
end;

procedure TVonSetting.SetBreakText(const Value: string);
var
  P, Q: PChar;
  szName, szValue: string;
begin
  P := PChar(Value);
  szName := '';
  szValue := '';
  while P[0] <> #0 do
  begin
    case P[0] of
      '=':
        begin
          szName := szValue;
          szValue := '';
        end;
      '(':
        begin
          szValue := szValue + '(' + NextChar(P, Q[0]) + ')';
        end;
      '''', '"':
        begin
          Q := P;
          szValue := szValue + Q[0] + NextChar(P, Q[0]) + Q[0];
        end;
      '|':
        begin
          Add(szName, szValue);
          szName := '';
          szValue := '';
        end;
    else
      szValue := szValue + P[0];
    end;
    Inc(P);
  end;
  if szValue <> '' then
    Add(szName, szValue);
end;

procedure TVonSetting.SetCapacity(const Value: Integer);
begin
      // 设定当前实际空间数量
  if (Value < FCount) or (Value > MaxListSize) then
    raise EListError.CreateFmt(SListCapacityError, [Value]);
  if Value <> FCapacity then
  begin
    ReallocMem(FKeyList, Value * SizeOf(PWideChar));
    ReallocMem(FValueList, Value * SizeOf(PWideChar));
    FCapacity := Value;
  end;
end;

procedure TVonSetting.SetCommaText(const Value: string);
var
  P, Q: PChar;
  szName, szValue: string;
begin
  P := PChar(Value);
  szName := '';
  szValue := '';
  while P[0] <> #0 do
  begin
    case P[0] of
      '=':
        begin
          szName := szValue;
          szValue := '';
        end;
      '(':
        begin
          szValue := szValue + '(' + NextChar(P, Q[0]) + ')';
        end;
      '''', '"':
        begin
          Q := P;
          szValue := szValue + Q[0] + NextChar(P, Q[0]) + Q[0];
        end;
      ',':
        begin
          Add(szName, szValue);
          szName := '';
          szValue := '';
        end;
    else
      szValue := szValue + P[0];
    end;
    Inc(P);
  end;
  if szValue <> '' then
    Add(szName, szValue);
end;

procedure TVonSetting.SetCount(const Value: Integer);
var
  I: Integer;
begin
      // 设定当前记录数量
  if (Value < 0) or (Value > MaxListSize) then
    raise EListError.CreateFmt(SListCountError, [Value]);
  if Value = FCount then
    Exit;
  if Value > FCapacity then
    Grow; // SetCapacity(Value);
  // if Value > FCount then begin
  // FillChar(FKeyList^[FCount], (Value - FCount) * SizeOf(PWideChar), 0);
  // FillChar(FValueList^[FCount], (Value - FCount) * SizeOf(PWideChar), 0);
  // end;
  for I := FCount - 1 downto Value do
    FreeItem(I);
  FCount := Value;
end;

procedure TVonSetting.SetNames(Index: Integer; const Value: string);
begin
      // 设置名称
  if (Index < 0) or (Index >= FCount) then
    raise EListError.CreateFmt(SListIndexError, [Index]);
  FKeyList^[Index] := NewItem(Value);
end;

procedure TVonSetting.SetNameValue(Name: string; const Value: string);
var
  Idx: Integer;
begin
      // 设置键值的内容
  Idx := IndexOfName(Name);
  if Idx >= 0 then
    Values[Idx] := Value // 发现原有定义，则替换
  else
    Add(Name, Value); // 未发现原有定义，则添加
end;

procedure TVonSetting.SetParamsText(const Value: string);
var
  P, Q: PChar;
  szName, szValue: string;
begin
      // * Style = VSK_PARENTHESES 表示以分号方式间隔各个参数，例如 Rate=100;Height=1233;Width=12
  P := PChar(Value);
  szName := '';
  szValue := '';
  while P[0] <> #0 do
  begin
    case P[0] of
      '=':
        begin
          szName := szValue;
          szValue := '';
        end;
      '(':
        begin
          szValue := szValue + '(' + NextChar(P, ')') + ')';
        end;
      '''', '"':
        begin
          Q := P;
          szValue := szValue + Q[0] + NextChar(P, Q[0]) + Q[0];
        end;
      ';':
        begin
          Add(szName, szValue);
          szName := '';
          szValue := '';
        end;
    else
      szValue := szValue + P[0];
    end;
    Inc(P);
  end;
  if szValue <> '' then
    Add(szName, szValue);
end;

procedure TVonSetting.SetSettingText(const Value: string);
var
  P, Q: PChar;
  szName, szValue: string;
begin
      // * Style = VSK_SEMICOLON 表示以函数方式间隔各个参数，例如 Rate(100)Height(1233)Width(12)
  Clear;
  P := PChar(Value);
  szName := '';
  szValue := '';
  while P[0] <> #0 do
  begin
    case P[0] of
      '(':
        begin
          if szName <> '' then
            szValue := szValue + '(' + NextChar(P, ')') + ')'
          else
          begin
            szName := szValue;
            szValue := '';
          end;
        end;
      '''', '"':
        begin
          Q := P;
          szValue := szValue + Q[0] + NextChar(P, Q[0]) + Q[0];
        end;
      ')':
        begin
          Add(szName, szValue);
          szName := '';
          szValue := '';
        end;
    else
      szValue := szValue + P[0];
    end;
    Inc(P);
  end;
  if szValue <> '' then
    Add(szName, szValue);
end;

procedure TVonSetting.SetSpeicalText(const Value: string);
var
  P: PChar;
  szName, szValue: string;
begin
      // 设定设置内容，名称与内容是#4间隔，段落是#3间隔
  Clear;
  P := PChar(Value);
  szName := '';
  szValue := '';
  while P^ <> #0 do
  begin
    case P^ of
      #4:
        begin
          szName := szValue;
          szValue := '';
        end;
      #2:
        begin
          Add(szName, szValue);
          szName := '';
          szValue := '';
        end;
    else
      szValue := szValue + P^;
    end;
    Inc(P);
  end;
  if szValue <> '' then
    Add(szName, szValue);
end;

procedure TVonSetting.SetText(Style: EVonSettingKind; const Value: string);
begin
  Clear;
  case Style of
    VSK_PARENTHESES:
      SetParamsText(Value);
    VSK_SEMICOLON:
      SetSettingText(Value);
    VSK_COMMA:
      SetCommaText(Value);
    VSK_SPEICAL:
      SetSpeicalText(Value);
    VSK_BREAK:
      SetBreakText(Value);
  end;
end;

procedure TVonSetting.SetValues(Index: Integer; const Value: string);
begin
      // 根据索引设定某一设置内容
  if (Index < 0) or (Index >= FCount) then
    raise EListError.CreateFmt(SListIndexError, [Index]);
  FValueList^[Index] := NewItem(Value);
end;

{ TVonNameValueData }

{
****************************** TVonNameValueData *******************************
}
constructor TVonNameValueData.Create;
begin
      // 初始化
  FCapacity := 20;
  SetLength(FNameList, FCapacity);
  SetLength(FValueList, FCapacity);
  SetLength(FObjList, FCapacity);
  SetLength(FDataList, FCapacity);
  FRowDelimiter := #13#10;
  FColDelimiter := '=';
  FCount := 0;
end;

destructor TVonNameValueData.Destroy;
var
  I: Integer;
begin
  if FAutoFreeObject then
    for I := 0 to FCount - 1 do
      if Assigned(FObjList[I]) then
        FObjList[I].Free;
  SetLength(FNameList, 0);
  SetLength(FValueList, 0);
  SetLength(FObjList, 0);
  SetLength(FDataList, 0);
  inherited;
end;

function TVonNameValueData.Add(const Name, Value: string; Obj: TObject = nil;
    Data: Pointer = 0): Integer;
begin
      // 添加一个内容
  Result := FCount;
  Inc(FCount);
  if FCount > FCapacity then
    Grow;
  FNameList[Result] := Name;
  FValueList[Result] := Value;
  FObjList[Result] := Obj;
  FDataList[Result] := Data;
end;

procedure TVonNameValueData.Clear;
var
  I: Integer;
begin
      // 清除所有内容
  for I := 0 to FCount - 1 do
  begin
    FNameList[I] := '';
    FValueList[I] := '';
    if FAutoFreeObject and Assigned(FObjList[I]) then
      FObjList[I].Free;
    FObjList[I] := nil;
    FDataList[I] := 0;
  end;
  FCount := 0;
end;

procedure TVonNameValueData.Delete(const Index: Integer);
var
  I: Integer;
begin
  if (Index < 0) or (Index >= FCount) then
    Exit;
  if FAutoFreeObject and Assigned(FDataList[Index]) then
    FObjList[Index].Free;
  for I := Index to FCount - 2 do
  begin
    FNameList[I] := FNameList[I + 1];
    FValueList[I] := FValueList[I + 1];
    FObjList[I] := FObjList[I + 1];
    FDataList[I] := FDataList[I + 1];
  end;
  Dec(FCount);
end;

procedure TVonNameValueData.Delete(const Name: string);
var
  I: Integer;
begin
  for I := 0 to FCount - 1 do
    if SameText(FNameList[I], Name) then
      Delete(I);
end;

function TVonNameValueData.GetDatas(Index: Integer): Pointer;
begin
  if (Index < 0) or (Index >= FCount) then
    Exit;
  Result := FDataList[Index];
end;

function TVonNameValueData.GetNameData(Name: string): Pointer;
begin
  Result := GetDatas(IndexOfName(Name));
end;

function TVonNameValueData.GetNameObject(Name: string): TObject;
begin
  Result := GetObjects(IndexOfName(Name));
end;

function TVonNameValueData.GetNames(Index: Integer): string;
begin
  if (Index < 0) or (Index >= FCount) then
    Exit;
  Result := FNameList[Index];
end;

function TVonNameValueData.GetNameValue(Name: string): string;
begin
  Result := GetValues(IndexOfName(Name));
end;

function TVonNameValueData.GetObjects(Index: Integer): TObject;
begin
  if (Index < 0) or (Index >= FCount) then
    Exit;
  Result := FObjList[Index];
end;

function TVonNameValueData.GetText: string;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to FCount - 1 do
    Result := Result + FRowDelimiter + FNameList[I] + FColDelimiter +
      FValueList[I];
  if Result <> '' then
    System.Delete(Result, 1, 2);
end;

function TVonNameValueData.GetValues(Index: Integer): string;
begin
  if (Index < 0) or (Index >= FCount) then
    Exit;
  Result := FValueList[Index];
end;

procedure TVonNameValueData.Grow;
var
  Delta: Integer;
begin
      // 成长一下系统实际开创的记录空间
  if FCapacity > 64 then
    Delta := FCapacity div 4
  else if FCapacity > 8 then
    Delta := 16
  else
    Delta := 4;
  FCapacity := FCapacity + Delta;
  SetLength(FNameList, FCapacity);
  SetLength(FValueList, FCapacity);
  SetLength(FObjList, FCapacity);
  SetLength(FDataList, FCapacity);
end;

function TVonNameValueData.IndexOfData(Data: Pointer): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to FCount - 1 do
    if FDataList[I] = Data then
    begin
      Result := I;
      Exit;
    end;
end;

function TVonNameValueData.IndexOfName(Name: string): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to FCount - 1 do
    if SameText(FNameList[I], Name) then
    begin
      Result := I;
      Exit;
    end;
end;

function TVonNameValueData.IndexOfObject(Obj: TObject): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to FCount - 1 do
    if FObjList[I] = Obj then
    begin
      Result := I;
      Exit;
    end;
end;

function TVonNameValueData.IndexOfValue(Value: string): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to FCount - 1 do
    if SameText(FValueList[I], Value) then
    begin
      Result := I;
      Exit;
    end;
end;

procedure TVonNameValueData.Move(OrgIdx, DestIdx: Integer);
var
  szDataList: Pointer;
  szNameList: string;
  szObjList: TObject;
  szValueList: string;
  I: Integer;
begin
  if(OrgIdx < 0)or(OrgIdx >= FCount)then Exit;
  if DestIdx < 0 then DestIdx:= 0;
  if DestIdx >= FCount then DestIdx:= FCount - 1;
  if OrgIdx = DestIdx then Exit;
  szDataList:= FDataList[OrgIdx];
  szNameList:= FNameList[OrgIdx];
  szObjList:= FObjList[OrgIdx];
  szValueList:= FValueList[OrgIdx];
  if OrgIdx > DestIdx then
    for I := OrgIdx downto DestIdx + 1 do begin
      FDataList[I]:= FDataList[I - 1];
      FNameList[I]:= FNameList[I - 1];
      FObjList[I]:= FObjList[I - 1];
      FValueList[I]:= FValueList[I - 1];
    end
  else
    for I := OrgIdx to DestIdx - 1 do begin
      FDataList[I]:= FDataList[I + 1];
      FNameList[I]:= FNameList[I + 1];
      FObjList[I]:= FObjList[I + 1];
      FValueList[I]:= FValueList[I + 1];
    end;
  FDataList[DestIdx]:= szDataList;
  FNameList[DestIdx]:= szNameList;
  FObjList[DestIdx]:= szObjList;
  FValueList[DestIdx]:= szValueList;
end;

procedure TVonNameValueData.Save(const Text: string);
var
  P, Q, szP: PChar;
  szName, szValue: string;
begin
  P := PChar(Text);
  new(szP);
  while P[0] <> #0 do
  begin
    Q := StrPos(P, PChar(FColDelimiter));
    szName := StrPas(StrLCopy(szP, P, Q - P));
    P := Q;
    Inc(P, Length(FColDelimiter));
    Q := StrPos(P, PChar(FRowDelimiter));
    szValue := StrPas(StrLCopy(szP, P, Q - P));
    P := Q;
    Inc(P, Length(FRowDelimiter));
    Save(szName, szValue, nil);
  end;
end;

function TVonNameValueData.Save(const Name, Value: string; Obj: TObject = nil;
    Data: Pointer = 0): Integer;
begin
      // 添加一个内容
  Result := IndexOfName(Name);
  if Result < 0 then
  begin
    Result := FCount;
    Inc(FCount);
  end;
  if FCount > FCapacity then
    Grow;
  FNameList[Result] := Name;
  FValueList[Result] := Value;
  FObjList[Result] := Obj;
  FDataList[Result] := Data;
end;

procedure TVonNameValueData.SetAutoFreeObject(const Value: Boolean);
begin
  FAutoFreeObject := Value;
end;

procedure TVonNameValueData.SetColDelimiter(const Value: string);
begin
  FColDelimiter := Value;
end;

procedure TVonNameValueData.SetDatas(Index: Integer; const Value: Pointer);
begin
  if (Index < 0) or (Index >= FCount) then
    Exit;
  FDataList[Index] := Value;
end;

procedure TVonNameValueData.SetNameData(Name: string; const Value: Pointer);
begin
  SetDatas(IndexOfName(Name), Value);
end;

procedure TVonNameValueData.SetNameObject(Name: string; const Value: TObject);
begin
  SetObjects(IndexOfName(Name), Value);
end;

procedure TVonNameValueData.SetNames(Index: Integer; const Value: string);
begin
  if (Index < 0) or (Index >= FCount) then
    Exit;
  FNameList[Index] := Value;
end;

procedure TVonNameValueData.SetNameValue(Name: string; const Value: string);
begin
  SetNames(IndexOfName(Name), Value);
end;

procedure TVonNameValueData.SetObjects(Index: Integer; const Value: TObject);
begin
  if (Index < 0) or (Index >= FCount) then
    Exit;
  FObjList[Index] := Value;
end;

procedure TVonNameValueData.SetRowDelimiter(const Value: string);
begin
  FRowDelimiter := Value;
end;

procedure TVonNameValueData.SetText(const Value: string);
begin
  Clear;
  Save(Value);
end;

procedure TVonNameValueData.SetValues(Index: Integer; const Value: string);
begin
  if (Index < 0) or (Index >= FCount) then
    Exit;
  FValueList[Index] := Value;
end;

{ TVonArraySetting }

{
******************************* TVonArraySetting *******************************
}
constructor TVonArraySetting.Create;
begin
  ColDelimiter := #3;
  RowDelimiter := #4;
end;

destructor TVonArraySetting.Destroy;
begin
  Clear;
  SetLength(FValueList, 0);
  inherited;
end;

procedure TVonArraySetting.Add(const Row, Col: Integer; Value: string);
var
  currentRow: PVonDataArrayString;
begin
  if (Row < 0) or (Row > Count) then
    raise Exception.Create('Row value is error.');
  if (Col < 0) then
    raise Exception.Create('Col value is error.');
  if Row = Count then
  begin
    new(currentRow);
    Inc(FCount);
    if FCount >= FCapacity then
      Grow;
    FValueList[FCount - 1] := currentRow;
  end
  else
    currentRow := FValueList[Row];
  if Col >= Length(currentRow.ArrayString) then
    SetLength(currentRow.ArrayString, Col + 1);
  currentRow.ArrayString[Col] := Value;
end;

function TVonArraySetting.AppendRow(Value: string; Data: Pointer): Integer;
var
  P: PChar;
  I, cap, cnt: Integer;
  S: string;
  NewRow: PVonDataArrayString;

  procedure GrowArray;
  var
    Delta: Integer;
  begin // 成长一下系统实际开创的记录空间
    if cap > 64 then
      Delta := cap div 4
    else if cap > 8 then
      Delta := 16
    else
      Delta := 4;
    cap := cap + Delta;
    SetLength(NewRow.ArrayString, cap);
  end;

  procedure AddValue;
  begin
    Inc(cnt);
    if cnt > cap then
      GrowArray;
    NewRow.ArrayString[cnt - 1] := S;
    S := '';
  end;

begin
  P := PChar(Value);
  S := '';
  cnt := 0;
  cap := 0;
  new(NewRow);
  Inc(FCount);
  if FCount >= FCapacity then Grow;
  FValueList[FCount - 1] := NewRow;
  Result:= FCount - 1;
  while P[0] <> #0 do
  begin
    if P[0] = ColDelimiter then
      AddValue
    else if P[0] = RowDelimiter then
    begin
      AddValue;
      NewRow.Data:= Data;
      Exit;
    end
    else
      S := S + P[0];
    Inc(P);
  end;
end;

function TVonArraySetting.AppendRow(Value: array of string;
  Data: Pointer): Integer;
var
  currentRow: PVonDataArrayString;
  Col, I: Integer;
begin
  Col := Length(Value);
  new(currentRow);
  Inc(FCount);
  if FCount >= FCapacity then
    Grow;
  FValueList[FCount - 1] := currentRow;
  SetLength(currentRow.ArrayString, Col);
  for I := 0 to Col - 1 do
    currentRow.ArrayString[I] := Value[I];
  currentRow.Data:= Data;
  Result:= FCount - 1;
end;

procedure TVonArraySetting.Clear;
var
  Row: Integer;
begin
  for Row := 0 to Count - 1 do
  begin
    SetLength(FValueList[Row].ArrayString, 0);
    Dispose(FValueList[Row]);
  end;
  FCount := 0;
end;

procedure TVonArraySetting.Delete(const Row: Integer);
var
  Idx: Integer;
begin
  if (Row < 0) or (Row >= Count) then
    raise Exception.Create('Row value is error.');
  SetLength(FValueList[Row].ArrayString, 0);
  Dispose(FValueList[Row]);
  for Idx := Row to FCount - 2 do
    FValueList[Idx] := FValueList[Idx + 1];
  Dec(FCount);
end;

procedure TVonArraySetting.DeleteCell(const Row, Col: Integer);
var
  Idx, cnt: Integer;
begin
  if (Row < 0) or (Row >= Count) then
    raise Exception.Create('Row value is error.');
  cnt := Length(FValueList[Row].ArrayString);
  if (Col < 0) and (Col >= cnt) then
    raise Exception.Create('Col value is error.');
  for Idx := Col to Length(FValueList[Row].ArrayString) - 2 do
    FValueList[Row].ArrayString[Idx] := FValueList[Row].ArrayString[Idx + 1];
  SetLength(FValueList[Row].ArrayString, cnt - 1);
end;

function TVonArraySetting.GetColCount(const Row: Integer): Integer;
begin
  if (Row < 0) or (Row >= Count) then
    raise Exception.Create('Row value is error.');
  Result := Length(FValueList[Row].ArrayString);
end;

function TVonArraySetting.GetData(const Row: Integer): Pointer;
begin
  if (Row < 0) or (Row >= Count) then
    raise Exception.Create('Row value is error.');
  Result:= FValueList[Row].Data;
end;

function TVonArraySetting.GetRow(const Row: Integer): TArrayOfString;
begin
  if (Row < 0) or (Row >= Count) then
    raise Exception.Create('Row value is error.');
  Result := FValueList[Row].ArrayString;
end;

function TVonArraySetting.GetText: string;
var
  Row, Col: Integer;
  currentRow: PVonDataArrayString;
begin
  Result := '';
  for Row := 0 to FCount - 1 do
  begin
    currentRow := FValueList[Row];
    if Length(currentRow.ArrayString) > 0 then
    begin
      Result := Result + currentRow.ArrayString[0];
      for Col := 1 to Length(currentRow.ArrayString) - 1 do
        Result := Result + ColDelimiter + currentRow.ArrayString[Col];
    end;
    Result := Result + RowDelimiter;
  end;
end;

function TVonArraySetting.GetValues(const Row, Col: Integer): string;
begin
  if (Row < 0) or (Row >= Count) then
    raise Exception.Create('Row value is error.');
  if (Col < 0) and (Col >= Length(FValueList[Row].ArrayString)) then
    raise Exception.Create('Col value is error.');
  Result := FValueList[Row].ArrayString[Col];
end;

procedure TVonArraySetting.Grow;
var
  Delta: Integer;
begin
      // 成长一下系统实际开创的记录空间
  if FCapacity > 64 then
    Delta := FCapacity div 4
  else if FCapacity > 8 then
    Delta := 16
  else
    Delta := 4;
  FCapacity := FCapacity + Delta;
  SetLength(FValueList, FCapacity);
end;

function TVonArraySetting.IndexOfValue(const Col: Integer; Value: string):
    Integer;
var
  Row: Integer;
begin
  Result := -1;
  for Row := 0 to FCount - 1 do
    if (Col < Length(FValueList[Row].ArrayString)) and
      SameText(Value, FValueList[Row].ArrayString[Col]) then
    begin
      Result := Row;
      Exit;
    end;
end;

procedure TVonArraySetting.InsertRow(const Row: Integer; Value: array of
    string; Data: Pointer);
var
  currentRow: PVonDataArrayString;
  Col, I: Integer;
begin
  Col := Length(Value);
  new(currentRow);
  Inc(FCount);
  currentRow.Data:= Data;
  if FCount >= FCapacity then
    Grow;
  for I := FCount - 1 downto Row + 1 do
    currentRow.ArrayString[I] := currentRow.ArrayString[I - 1];
  FValueList[Row] := currentRow;
  SetLength(currentRow.ArrayString, Length(Value));
  for I := 0 to Col - 1 do
    currentRow.ArrayString[I] := Value[I];
end;

procedure TVonArraySetting.LoadFromFile(Filename: string);
var
  fs: TFileStream;
begin
  fs := TFileStream.Create(Filename, fmOpenRead);
  LoadFromStream(fs);
  fs.Free;
end;

procedure TVonArraySetting.LoadFromStream(aStream: TStream; ASize: Int64 = 0);
var
  ss: TStringStream;
begin
  ss := TStringStream.Create;
  if ASize = 0 then
    ss.CopyFrom(aStream, aStream.Size)
  else
    ss.CopyFrom(aStream, ASize);
  ss.Position := 0;
  // TEncoding.Unicode.GetBytes()
  SetText(ss.DataString);
  ss.Free;
end;

procedure TVonArraySetting.Move(orgIdx, DestIdx: Integer);
var
  P: PVonDataArrayString;
  Idx: Integer;
begin
  P := FValueList[orgIdx];
  Idx := orgIdx;
  if orgIdx > DestIdx then
    while Idx > DestIdx do
    begin
      FValueList[Idx] := FValueList[Idx - 1];
      Dec(Idx);
    end
  else
    while Idx < DestIdx do
    begin
      FValueList[Idx] := FValueList[Idx + 1];
      Inc(Idx);
    end;
  FValueList[Idx] := P;
end;

procedure TVonArraySetting.SaveToFile(Filename: string);
var
  fs: TFileStream;
begin
  fs := TFileStream.Create(Filename, fmCreate);
  SaveToStream(fs);
  fs.Free;
end;

function TVonArraySetting.SaveToStream(aStream: TStream): Int64;
var
  ss: TStringStream;
begin
  ss := TStringStream.Create(GetText);
  Result := ss.Size;
  ss.Position := 0;
  aStream.CopyFrom(ss, Result);
  ss.Free;
end;

procedure TVonArraySetting.SetColCount(const Row: Integer; const Value:
    Integer);
begin
  if (Row < 0) or (Row >= Count) then
    raise Exception.Create('Row value is error.');
  SetLength(FValueList[Row].ArrayString, Value);
end;

procedure TVonArraySetting.SetColDelimiter(const Value: Char);
begin
  FColDelimiter := Value;
end;

procedure TVonArraySetting.SetData(const Row: Integer; const Value: Pointer);
begin
  if (Row < 0) or (Row >= Count) then
    raise Exception.Create('Row value is error.');
  FValueList[Row].Data:= Value;
end;

procedure TVonArraySetting.SetPText(P: PChar; const endChar: string);
var
  I, Row, cap, cnt: Integer;
  arr: array of string;
  S: string;
  NewRow: PVonDataArrayString;

  procedure GrowArray;
  var
    Delta: Integer;
  begin // 成长一下系统实际开创的记录空间
    if cap > 64 then
      Delta := cap div 4
    else if cap > 8 then
      Delta := 16
    else
      Delta := 4;
    cap := cap + Delta;
    SetLength(arr, cap);
  end;

  procedure AddValue;
  begin
    Inc(cnt);
    if cnt > cap then
      GrowArray;
    arr[cnt - 1] := S;
    S := '';
  end;
begin
  Clear;
  S := '';
  cnt := 0;
  cap := 0;
  Row := 0;
  GrowArray; // 初始化变量
  while(P[0] <> #0)and(P[0] <> endChar)do
  begin
    if P[0] = ColDelimiter then
      AddValue
    else if P[0] = RowDelimiter then
    begin
      AddValue;
      if Row >= FCount then
        Inc(FCount);
      if FCount > FCapacity then
        Grow;
      new(NewRow);
      FValueList[Row] := NewRow;
      SetLength(FValueList[Row].ArrayString, cnt); // 设定列数
      for I := 0 to cnt - 1 do // 复制列内容
        FValueList[Row].ArrayString[I] := arr[I];
      Inc(Row);
      cnt := 0; // 下一行
    end
    else
      S := S + P[0];
    Inc(P);
  end;
end;

procedure TVonArraySetting.SetRowDelimiter(const Value: Char);
begin
  FRowDelimiter := Value;
end;

procedure TVonArraySetting.SetText(const Value: string);
var
  P: PChar;
begin
  P := PChar(Value);
  SetPText(P, #0);
end;

procedure TVonArraySetting.SetValues(const Row, Col: Integer; const Value:
    string);
begin
  if (Row < 0) or (Row >= Count) then
    raise Exception.Create('Row value is error.');
  if (Col < 0) and (Col >= Length(FValueList[Row].ArrayString)) then
    raise Exception.Create('Col value is error.');
  FValueList[Row].ArrayString[Col] := Value;
end;

procedure TVonArraySetting.UpdateRow(const Row: Integer; Value: array of
    string);
var
  I: Integer;
begin
  if (Row < 0) or (Row >= Count) then
    raise Exception.Create('Row value is error.');
  if (Length(Value) > Length(FValueList[Row].ArrayString)) then
    raise Exception.Create('The value is longer.');
  for I := 0 to Length(Value) - 1 do
    FValueList[Row].ArrayString[I] := Value[I];
end;

{
*********************************** TVonBook ***********************************
}
constructor TVonBook.Create;
begin
  PageDelimiter := #2;
  ColDelimiter := #3;
  RowDelimiter := #4;
end;

destructor TVonBook.Destroy;
begin
  Clear;
  SetLength(FPageList, 0);
  inherited;
end;

function TVonBook.Append: Integer;
begin
  Inc(FPageCount);
  if FPageCount > FCapacity then
    Grow;
  Result := FPageCount - 1;
  FPageList[Result] := TVonTable.Create;
  FPageList[Result].RowCount:= 1;
  FPageList[Result].ColCount:= 1;
end;

procedure TVonBook.Clear;
var
  I: Integer;
begin
  for I := 0 to FPageCount - 1 do
    FPageList[I].Clear;
  FPageCount := 0;
end;

procedure TVonBook.Delete(Index: Integer);
var
  I: Integer;
  tmp: TVonTable;
begin
  FPageList[Index].Clear;
  tmp:= FPageList[Index];
  for I := Index to FPageCount - 2 do
    FPageList[I] := FPageList[I + 1];
  FPageList[FPageCount - 1]:= tmp;
  Dec(FPageCount);
end;

function TVonBook.GetPages(Index: Integer): TVonTable;
begin
  Result:= FPageList[Index];
end;

function TVonBook.GetText: string;
var
  I: Integer;
begin
  Result:= '';
  for I := 0 to FPageCount - 1 do
    Result:= Result + Pages[I].Text + FPageDelimiter;
end;

procedure TVonBook.Grow;
var
  Delta, I: Integer;
begin
      // 成长一下系统实际开创的记录空间
  if FCapacity > 64 then
    Delta := FCapacity div 4
  else if FCapacity > 8 then
    Delta := 16
  else
    Delta := 4;
  SetLength(FPageList, FCapacity + Delta);
  for I := FCapacity to FCapacity + Delta - 1 do begin
    FPageList[I]:= TVonTable.Create;
    FPageList[I].RowCount:= 1;
    FPageList[I].ColCount:= 1;
  end;
  FCapacity := FCapacity + Delta;
end;

procedure TVonBook.Insert(Index: Integer);
var
  I: Integer;
  tmp: TVonTable;
begin
  Inc(FPageCount);
  if FPageCount > FCapacity then
    Grow;
  tmp:= FPageList[FPageCount - 1];
  for I := FPageCount - 1 downto Index + 1 do
    FPageList[I] := FPageList[I - 1];
  FPageList[Index] := tmp;
  FPageList[Index].Clear;
end;

procedure TVonBook.LoadFromFile(Filename: string);
var
  F: TFileStream;
begin
  F:= TFileStream.Create(Filename, fmOpenRead);
  LoadFromStream(F);
  F.Free;
end;

procedure TVonBook.LoadFromStream(aStream: TStream);
var
  ch: char;
  S: string;
begin
  Clear;
  if aStream.Size = 0 then Exit;
  while aStream.Position < aStream.Size do begin
    Append;
    Pages[FPageCount - 1].LoadFromStream(aStream, FPageDelimiter);
  end;
end;

procedure TVonBook.Move(orgIdx, DestIdx: Integer);
var
  P: TVonTable;
  Idx: Integer;
begin
  P := FPageList[orgIdx];
  Idx := orgIdx;
  if orgIdx > DestIdx then
    while Idx > DestIdx do
    begin
      FPageList[Idx] := FPageList[Idx - 1];
      Dec(Idx);
    end
  else
    while Idx < DestIdx do
    begin
      FPageList[Idx] := FPageList[Idx + 1];
      Inc(Idx);
    end;
  FPageList[Idx] := P;
end;

procedure TVonBook.SaveToFile(Filename: string);
var
  F: TFileStream;
begin
  F:= TFileStream.Create(Filename, fmCreate);
  SaveToStream(F);
  F.Free;
end;

function TVonBook.SaveToStream(aStream: TStream): Int64;
var
  P, R, C: Integer;
  procedure WriteStr(S: string);
  begin
    aStream.WriteBuffer(S[1], Length(S) * SizeOf(Char));
  end;
begin
  for P := 0 to FPageCount - 1 do
    with Pages[P] do begin
      for R := 0 to RowCount - 1 do begin
        WriteStr(Cells[0, R]);
        for C := 1 to ColCount - 1 do
          WriteStr(FColDelimiter + Cells[C, R]);
        if R < RowCount - 1 then WriteStr(FRowDelimiter)
        else WriteStr(FPageDelimiter);
      end;
    end;
end;

procedure TVonBook.SetText(const Value: string);
var
  P: PChar;
begin
  P:= PChar(Value);
  Clear;
  while P^ <> #0 do begin
    Append;
    Pages[FPageCount - 1].FRowDelimiter:= FRowDelimiter;
    Pages[FPageCount - 1].FColDelimiter:= FColDelimiter;
    P:= Pages[FPageCount - 1].SplitText(P, FPageDelimiter);
    if P^ = FPageDelimiter then
      Inc(P);
  end;
end;

{ TVonLinker }

{
********************************** TVonLinker **********************************
}
procedure TVonLinker.ChangeIndex;
begin
  if Assigned(Next) then
  begin
    Next.Index := Index + 1;
    Next.ChangeIndex;
  end;
end;

procedure TVonLinker.SetData(const Value: TObject);
begin
  FData := Value;
end;

procedure TVonLinker.SetIndex(const Value: Integer);
begin
  FIndex := Value;
end;

procedure TVonLinker.SetKey(const Value: string);
begin
  FKey := Value;
end;

procedure TVonLinker.SetNext(const Value: TVonLinker);
begin
  FNext := Value;
end;

procedure TVonLinker.SetPrevious(const Value: TVonLinker);
begin
  FPrevious := Value;
end;

procedure TVonLinker.SetValue(const Value: string);
begin
  FValue := Value;
end;

{ TVonLinkerCollection }

{
***************************** TVonLinkerCollection *****************************
}
destructor TVonLinkerCollection.Destroy;
begin
  Clear;
  inherited;
end;

procedure TVonLinkerCollection.Append(AKey, AValue: string; AObj: TObject =
    nil);
var
  newLinker: TVonLinker;
begin
  newLinker := TVonLinker.Create;
  newLinker.Key := AKey;
  newLinker.Value := AValue;
  newLinker.Data := AObj;
  if Assigned(FCurrent) then
  begin
    if not Assigned(FCurrent.Next) then
      Laster := newLinker;
    newLinker.Next := FCurrent.Next;
    newLinker.Previous := FCurrent;
    FCurrent.Next := newLinker;
    FCurrent.ChangeIndex;
  end
  else
  begin
    FCurrent := newLinker;
    First := newLinker;
    Laster := newLinker;
  end;
end;

procedure TVonLinkerCollection.Clear;
  procedure ClearIt(ALinker: TVonLinker);
  begin
    if not Assigned(ALinker) then
      Exit;
    if Assigned(ALinker.Next) then
      ClearIt(ALinker.Next);
    ALinker.Free;
  end;

begin
  ClearIt(First);
  First := nil;
  FCurrent := nil;
  Laster := nil;
end;

procedure TVonLinkerCollection.Delete;
var
  szLinker: TVonLinker;
begin
  if not Assigned(FCurrent) then
    Exit;
  if not Assigned(FCurrent.Previous) then
    First := FCurrent.Next;
  szLinker := FCurrent.Next;
end;

function TVonLinkerCollection.GetBOF: Boolean;
begin
  Result := First = FCurrent;
end;

function TVonLinkerCollection.GetEOF: Boolean;
begin
  Result := Laster = FCurrent;
end;

function TVonLinkerCollection.GetText: string;
  procedure GetLinkerText(ALinker: TVonLinker);
  begin
    Result := Result + ALinker.Key + #3 + ALinker.Value + #4;
    if Assigned(ALinker.Next) then
      GetLinkerText(ALinker.Next);
  end;

begin
  if not Assigned(First) then
    Result := ''
  else
    GetLinkerText(First);
end;

procedure TVonLinkerCollection.Insert(AKey, AValue: string; AObj: TObject =
    nil);
var
  newLinker: TVonLinker;
begin
  newLinker := TVonLinker.Create;
  newLinker.Key := AKey;
  newLinker.Value := AValue;
  newLinker.Data := AObj;
  if Assigned(FCurrent) then
  begin
    if not Assigned(FCurrent.Previous) then
      First := newLinker;
    newLinker.Previous := FCurrent.Previous;
    FCurrent.Previous := newLinker;
    newLinker.Next := FCurrent;
    newLinker.Index := FCurrent.Index;
    newLinker.ChangeIndex;
  end
  else
  begin
    FCurrent := newLinker;
    First := newLinker;
    Laster := newLinker;
  end;
end;

function TVonLinkerCollection.Next: TVonLinker;
begin
  if not Assigned(FCurrent) then
    Result := nil;
  Result := FCurrent.Next;
  if Assigned(Result) then
    FCurrent := Result;
end;

function TVonLinkerCollection.Previous: TVonLinker;
begin
  if not Assigned(FCurrent) then
    Result := nil;
  Result := FCurrent.Previous;
  if Assigned(Result) then
    FCurrent := Result;
end;

procedure TVonLinkerCollection.SetCurrent(const Value: TVonLinker);
begin
  FCurrent := Value;
end;

procedure TVonLinkerCollection.SetText(const Value: string);
  procedure SetLinkerValue(P: PChar);
  var
    szKey, szValue: string;
    newLinker: TVonLinker;
  begin
    while P[0] <> #0 do
    begin
      case P[0] of
        #3:
          begin
            szKey := szValue;
            szValue := '';
          end;
        #4:
          begin
            Append(szKey, szValue);
            Next;
            Inc(P);
            SetLinkerValue(P);
            Exit;
          end;
      else
        szValue := szValue + P[0];
      end;
      Inc(P);
    end;
  end;

begin
  Clear;
  SetLinkerValue(PChar(Value));
end;

{ TVonArrayObject }

{
******************************* TVonArrayObject ********************************
}
function TVonArrayObject.Add(Item: TObject): Integer;
begin
  Inc(FCount);
  if FCount > FCapacity then
    Grow;
  Result := FCount - 1;
  FList[Result] := Item;
end;

procedure TVonArrayObject.Delete(Index: Integer);
var
  I: Integer;
begin
  FList[Index].Free;
  for I := Index to FCount - 2 do
    FList[Index] := FList[Index + 1];
  Dec(FCount);
end;

function TVonArrayObject.GetItems(Index: Integer): TObject;
begin
  Result := FList[Index];
end;

procedure TVonArrayObject.Grow;
var
  Delta: Integer;
begin
      // 成长一下系统实际开创的记录空间
  if FCapacity > 64 then
    Delta := FCapacity div 4
  else if FCapacity > 8 then
    Delta := 16
  else
    Delta := 4;
  FCapacity := FCapacity + Delta;
  SetLength(FList, FCapacity);
end;

procedure TVonArrayObject.Insert(Index: Integer; Item: TObject);
var
  I: Integer;
begin
  Inc(FCount);
  if FCount > FCapacity then
    Grow;
  for I := FCount - 1 downto Index + 1 do
    FList[I] := FList[I - 1];
  FList[Index] := Item;
end;

procedure TVonArrayObject.SetItems(Index: Integer; const Value: TObject);
begin
  if Index < FCount then
  begin
    FList[Index].Free;
    FList[Index] := Value;
  end
  else
    Add(Value);
end;

{ TStringHash }

{
*********************************** TVonHash ***********************************
}
constructor TVonHash.Create(Size: Cardinal = 256);
begin
  inherited Create;
  SetLength(Buckets, Size);
end;

destructor TVonHash.Destroy;
begin
  Clear;
  inherited Destroy;
end;

procedure TVonHash.Add(const Key, Value: string; Data: Pointer; Obj: TObject);
var
  Hash: Integer;
  Bucket: PVonHashItem;
begin
  Hash := HashOf(Key) mod Cardinal(Length(Buckets));
  new(Bucket);
  Bucket^.Key := Key;
  Bucket^.Value := Value;
  Bucket^.Data := Data;
  Bucket^.Obj := Obj;
  Bucket^.Next := Buckets[Hash];
  Buckets[Hash] := Bucket;
end;

procedure TVonHash.Clear;
var
  I: Integer;
  P, N: PVonHashItem;
begin
  for I := 0 to Length(Buckets) - 1 do
  begin
    P := Buckets[I];
    while P <> nil do
    begin
      N := P^.Next;
      Dispose(P);
      P := N;
    end;
    Buckets[I] := nil;
  end;
end;

function TVonHash.DataOf(const Key: string): Pointer;
var
  P: PVonHashItem;
begin
  P := Find(Key)^;
  if P <> nil then
    Result := P^.Data
  else
    Result := nil;
end;

function TVonHash.Find(const Key: string): PPVonHashItem;
var
  Hash: Integer;
begin
  Hash := HashOf(Key) mod Cardinal(Length(Buckets));
  Result := @Buckets[Hash];
  while Result^ <> nil do
  begin
    if Result^.Key = Key then
      Exit
    else
      Result := @Result^.Next;
  end;
end;

function TVonHash.HashOf(const Key: string): Cardinal;
var
  I: Integer;
begin
  Result := 0;
  for I := 1 to Length(Key) do
    Result := ((Result shl 2) or (Result shr (SizeOf(Result) * 8 - 2)))
      xor Ord(Key[I]);
end;

function TVonHash.Modify(const Key, Value: string; Data: Pointer; Obj:
    TObject): Boolean;
var
  P: PVonHashItem;
begin
  P := Find(Key)^;
  if P <> nil then
  begin
    Result := True;
    P^.Value := Value;
    P^.Data := Data;
    P^.Obj := Obj;
  end
  else
    Result := false;
end;

function TVonHash.ObjectOf(const Key: string): TObject;
var
  P: PVonHashItem;
begin
  P := Find(Key)^;
  if P <> nil then
    Result := P^.Obj
  else
    Result := nil;
end;

procedure TVonHash.Remove(const Key: string);
var
  P: PVonHashItem;
  Prev: PPVonHashItem;
begin
  Prev := Find(Key);
  P := Prev^;
  if P <> nil then
  begin
    Prev^ := P^.Next;
    Dispose(P);
  end;
end;

function TVonHash.ValueOf(const Key: string): string;
var
  P: PVonHashItem;
begin
  P := Find(Key)^;
  if P <> nil then
    Result := P^.Value
  else
    Result := '';
end;

{ TVonKeyArraySetting }

{
***************************** TVonKeyArraySetting ******************************
}
constructor TVonKeyArraySetting.Create;
begin
  Grow;
end;

destructor TVonKeyArraySetting.Destroy;
begin
  Clear;
  SetLength(FKeyList, 0);
  SetLength(FKeyList, 0);
  inherited;
end;

procedure TVonKeyArraySetting.Clear;
var
  I: Integer;
begin
  for I := 0 to FCount - 1 do
    FValueList[I].Free;
  FCount := 0;
end;

procedure TVonKeyArraySetting.Delete(const Idx: Integer);
begin
  if Idx < 0 then
    raise Exception.Create(RES_OUT_OF_RANG);
  if Idx >= FCount then
    raise Exception.Create(RES_OUT_OF_RANG);
  FValueList[Idx].Free;
  Move(Idx, FCount - 1);
  Dec(FCount);
end;

procedure TVonKeyArraySetting.Delete(const Key: string);
begin
  Delete(IndexOfKey(Key));
end;

procedure TVonKeyArraySetting.DeleteData(const Idx: Integer; Inden: string);
begin
  if Idx < 0 then
    raise Exception.Create(RES_OUT_OF_RANG);
  if Idx >= FCount then
    raise Exception.Create(RES_OUT_OF_RANG);
  FValueList[Idx].Delete(Inden);
end;

procedure TVonKeyArraySetting.DeleteData(const Key, Inden: string);
begin
  DeleteData(IndexOfKey(Key), Inden);
end;

function TVonKeyArraySetting.GetColCount(const Idx: Integer): Integer;
begin
  if Idx < 0 then
    raise Exception.Create(RES_OUT_OF_RANG);
  if Idx >= FCount then
    raise Exception.Create(RES_OUT_OF_RANG);
  Result := FValueList[Idx].Count;
end;

function TVonKeyArraySetting.GetNames(const Idx, Col: Integer): string;
begin
  if Idx < 0 then
    raise Exception.Create(RES_OUT_OF_RANG);
  if Idx >= FCount then
    raise Exception.Create(RES_OUT_OF_RANG);
  with FValueList[Idx] do
  begin
    if Col < 0 then
      raise Exception.Create(RES_OUT_OF_RANG);
    if Col >= Count then
      raise Exception.Create(RES_OUT_OF_RANG);
    Result := FIdentList[Col];
  end;
end;

function TVonKeyArraySetting.GetValues(const Idx, Col: Integer): string;
begin
  if Idx < 0 then
    raise Exception.Create(RES_OUT_OF_RANG);
  if Idx >= FCount then
    raise Exception.Create(RES_OUT_OF_RANG);
  with FValueList[Idx] do
  begin
    if Col < 0 then
      raise Exception.Create(RES_OUT_OF_RANG);
    if Col >= Count then
      raise Exception.Create(RES_OUT_OF_RANG);
    Result := FValueList[Col];
  end;
end;

function TVonKeyArraySetting.GetRow(const Idx: Integer): TVonIdentValue;
begin
  if Idx < 0 then
    raise Exception.Create(RES_OUT_OF_RANG);
  if Idx >= FCount then
    raise Exception.Create(RES_OUT_OF_RANG);
  Result := FValueList[Idx];
end;

function TVonKeyArraySetting.GetRow(const Key: string): TVonIdentValue;
begin
  Result := GetRow(IndexOfKey(Key));
end;

function TVonKeyArraySetting.GetText: string;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to FCount - 1 do
    Result := Result + FKeyList[I] + FKeyDelimiter + FValueList[I].Text +
      FRowDelimiter;
end;

function TVonKeyArraySetting.GetValue(const Idx: Integer; const Key: string):
    string;
begin
  Result := FValueList[Idx].Values[Key];
end;

procedure TVonKeyArraySetting.Grow;
var
  Delta: Integer;
begin
      // 成长一下系统实际开创的记录空间
  if FCapacity > 64 then
    Delta := FCapacity div 4
  else if FCapacity > 8 then
    Delta := 16
  else
    Delta := 4;
  FCapacity := FCapacity + Delta;
  SetLength(FValueList, FCapacity);
  SetLength(FKeyList, FCapacity);
end;

function TVonKeyArraySetting.IndexOfKey(Key: string): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to FCount - 1 do
    if SameText(FKeyList[I], Key) then
    begin
      Result := I;
      Exit;
    end;
end;

procedure TVonKeyArraySetting.InsertRow(const Idx: Integer; const Key: string;
    Value: array of string);
var
  I, mPos: Integer;
  tmpValue: TVonIdentValue;
begin
  if Idx < 0 then
    raise Exception.Create(RES_OUT_OF_RANG);
  if Idx >= FCount then
    raise Exception.Create(RES_OUT_OF_RANG);
  if FCount = FCapacity then
    Grow;
  for I := FCount - 1 downto Idx + 1 do
  begin
    FKeyList[I] := FKeyList[I - 1];
    FValueList[I] := FValueList[I - 1];
  end;
  tmpValue := NewRow(Idx, Key);
  for I := 0 to Length(Value) - 1 do
  begin
    mPos := Pos('=', Value[I]);
    if mPos > 0 then
      tmpValue.Save(Copy(Value[I], 1, mPos - 1),
        Copy(Value[I], mPos + 1, Maxint))
    else
      tmpValue.Save(Value[I], '');
  end;
end;

procedure TVonKeyArraySetting.LoadFromFile(Filename: string);
var
  fs: TFileStream;
begin
  fs := TFileStream.Create(Filename, fmOpenRead);
  LoadFromStream(fs);
end;

procedure TVonKeyArraySetting.LoadFromStream(aStream: TStream; ASize: Int64 =
    0);
var
  ss: TStringStream;
begin
  ss := TStringStream.Create;
  if ASize = 0 then
    ss.CopyFrom(aStream, aStream.Size)
  else
    ss.CopyFrom(aStream, ASize);
  ss.Position := 0;
  // TEncoding.Unicode.GetBytes()
  SetText(ss.DataString);
  ss.Free;
end;

procedure TVonKeyArraySetting.Move(orgIdx, DestIdx: Integer);
var
  I: Integer;
  S: string;
  szData: TVonIdentValue;
begin
  S := FKeyList[orgIdx];
  szData := FValueList[orgIdx];
  if orgIdx > DestIdx then
  begin
    for I := orgIdx downto DestIdx + 1 do
    begin
      FValueList[I] := FValueList[I - 1];
      FKeyList[I] := FKeyList[I - 1];
    end;
  end
  else
  begin
    for I := orgIdx to DestIdx - 1 do
    begin
      FValueList[I] := FValueList[I + 1];
      FKeyList[I] := FKeyList[I + 1];
    end;
  end;
  FValueList[orgIdx] := szData;
  FKeyList[orgIdx] := S;
end;

function TVonKeyArraySetting.NewRow(Idx: Integer; Key: string): TVonIdentValue;
begin
  if FCount = FCapacity then
    Grow;
  Inc(FCount);
  Result := TVonIdentValue.Create;
  Result.IdentDelimiter := FValueDelimiter;
  Result.ValueDelimiter := FColDelimiter;
  Result.EndDelimiter := #13;
  FValueList[Idx] := Result;
  FKeyList[Idx] := Key;
end;

procedure TVonKeyArraySetting.Save(const Idx: Integer; const Inden, Value:
    string);
begin
  if (Idx < 0) or (Idx >= FCount) then
    Exit;
  FValueList[Idx].Save(Inden, Value);
end;

procedure TVonKeyArraySetting.Save(const Key, Inden, Value: string);
var
  Idx: Integer;
begin
  Idx := IndexOfKey(Key);
  if Idx < 0 then
    NewRow(FCount, Key).Save(Inden, Value)
  else
    Save(Idx, Inden, Value);
end;

procedure TVonKeyArraySetting.SaveRow(const Key: string; Value: array of
    string);
var
  Idx, mPos: Integer;
  tmpValue: TVonIdentValue;
begin
  Idx := IndexOfKey(Key);
  if Idx < 0 then
  begin
    tmpValue := NewRow(FCount, Key);
  end
  else
    tmpValue := FValueList[Idx];
  for Idx := 0 to Length(Value) - 1 do
  begin
    mPos := Pos('=', Value[Idx]);
    if mPos > 0 then
      tmpValue.Save(Copy(Value[Idx], 1, mPos - 1),
        Copy(Value[Idx], mPos + 1, Maxint))
    else
      tmpValue.Save(Value[Idx], '');
  end;
end;

procedure TVonKeyArraySetting.SaveToFile(Filename: string);
var
  f: TFileStream;
begin
  f := TFileStream.Create(Filename, fmCreate);
  SaveToStream(f);
  f.Free;
end;

function TVonKeyArraySetting.SaveToStream(aStream: TStream): Int64;
begin
  // aStream.W
end;

procedure TVonKeyArraySetting.SetColDelimiter(const Value: Char);
var
  I: Integer;
begin
  FColDelimiter := Value;
  for I := 0 to FCount - 1 do
    FValueList[I].ValueDelimiter:= Value;
end;

procedure TVonKeyArraySetting.SetValues(const Idx, Col: Integer; const
    Value: string);
begin
  FValueList[Idx].ValueOfIndex[Col] := Value;
end;

procedure TVonKeyArraySetting.SetKeyDelimiter(const Value: Char);
begin
  FKeyDelimiter := Value;
end;

procedure TVonKeyArraySetting.SetRowDelimiter(const Value: Char);
begin
  FRowDelimiter := Value;
end;

procedure TVonKeyArraySetting.SetText(const Value: string);
var
  P0, P1: PChar;
  K, N, V: string;
begin
  P0 := PChar(Value);
  P1 := P0;
  while P1[0] <> #0 do
  begin
    if P1[0] = FRowDelimiter then
    begin
      K := '';
      N := '';
      V := '';
      Inc(P1);
      P0 := P1;
    end
    else if P1[0] = FColDelimiter then
    begin
      V := string(Copy(P0, 0, P1 - P0));
      Save(K, N, V);
      Inc(P1);
      P0 := P1;
    end
    else if P1[0] = FValueDelimiter then
    begin
      N := string(Copy(P0, 0, P1 - P0));
      Inc(P1);
      P0 := P1;
    end
    else if P1[0] = FKeyDelimiter then
    begin
      K := Trim(string(Copy(P0, 0, P1 - P0)));
      Inc(P1);
      P0 := P1;
    end
    else
      Inc(P1);
  end;
end;

procedure TVonKeyArraySetting.SetValue(const Idx: Integer; const Key: string;
    const Value: string);
begin
  FValueList[Idx].Values[Key] := Value;
end;

procedure TVonKeyArraySetting.SetValueDelimiter(const Value: Char);
var
  I: Integer;
begin
  FValueDelimiter := Value;
  for I := 0 to FCount - 1 do
    FValueList[I].IdentDelimiter:= Value;
end;

{ TVonIdentValue }

{
******************************** TVonIdentValue ********************************
}
constructor TVonIdentValue.Create;
begin
  Grow;
end;

destructor TVonIdentValue.Destroy;
begin
  SetLength(FIdentList, 0);
  SetLength(FValueList, 0);
  inherited;
end;

procedure TVonIdentValue.Clear;
begin
  FCount := 0;
end;

procedure TVonIdentValue.Delete(const Index: Integer);
var
  I: Integer;
begin
  if (Index < 0) or (Index >= FCount) then
    raise Exception.Create(RES_OUT_OF_RANG);
  for I := Index to FCount - 2 do
  begin
    FIdentList[I] := FIdentList[I + 1];
    FValueList[I] := FValueList[I + 1];
  end;
  Dec(FCount);
end;

procedure TVonIdentValue.Delete(const Ident: string);
begin
  Delete(IndexOf(Ident));
end;

function TVonIdentValue.GetText: string;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to FCount - 1 do
    Result := Result + FIdentList[I] + FIdentDelimiter + FValueList[I] +
      FValueDelimiter;
  Result := Result + FEndDelimiter;
end;

function TVonIdentValue.GetValueOfIndex(Index: Integer): string;
begin
  if (Index < 0) or (Index >= FCount) then
    Result:= ''
  else Result := FValueList[Index];
end;

function TVonIdentValue.GetValues(Ident: string): string;
begin
  Result := GetValueOfIndex(IndexOf(Ident));
end;

procedure TVonIdentValue.Grow;
var
  Delta: Integer;
begin
      // 成长一下系统实际开创的记录空间
  if FCapacity > 64 then
    Delta := FCapacity div 4
  else if FCapacity > 8 then
    Delta := 16
  else
    Delta := 4;
  FCapacity := FCapacity + Delta;
  SetLength(FIdentList, FCapacity);
  SetLength(FValueList, FCapacity);
end;

function TVonIdentValue.IndexOf(Ident: string): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to FCount - 1 do
    if SameText(Ident, FIdentList[I]) then
    begin
      Result := I;
      Exit;
    end;
end;

procedure TVonIdentValue.LoadFromStream(AStream: TStream);
var
  szIdent, szValue: string;
  ch: char;
begin
  while AStream.Position < AStream.Size do
  begin
    AStream.Read(ch, sizeof(char));
    if ch = FEndDelimiter then Exit;
    if ch = FIdentDelimiter then
    begin
      szIdent := szValue;
      szValue := '';
    end
    else if ch = FValueDelimiter then
    begin
      Save(szIdent, szValue);
      szIdent := '';
      szValue := '';
    end
    else
      szValue := szValue + ch;
  end;
end;

procedure TVonIdentValue.Save(const Ident, Value: string);
var
  Idx: Integer;
begin
  Idx := IndexOf(Ident);
  if Idx < 0 then
  begin
    Inc(FCount);
    if FCount > FCapacity then
      Grow;
    FIdentList[FCount - 1] := Ident;
    FValueList[FCount - 1] := Value;
  end
  else
  begin
    FIdentList[Idx] := Ident;
    FValueList[Idx] := Value;
  end;
end;

procedure TVonIdentValue.SaveToStream(AStream: TStream);
var
  I: Integer;
begin
  for I := 0 to FCount - 1 do
  begin
    AStream.Write(FIdentList[I][1], Length(FIdentList[I]) * SizeOf(Char));
    AStream.Write(FIdentDelimiter, SizeOf(Char));
    AStream.Write(FValueList[I][1], Length(FValueList[I]) * SizeOf(Char));
    AStream.Write(FValueDelimiter, SizeOf(Char));
  end;
  AStream.Write(FEndDelimiter, SizeOf(Char));
end;

procedure TVonIdentValue.SetEndDelimiter(const Value: Char);
begin
  FEndDelimiter := Value;
end;

procedure TVonIdentValue.SetIdentDelimiter(const Value: Char);
begin
  FIdentDelimiter := Value;
end;

function TVonIdentValue.SetPText(PText: PChar): PChar;
var
  szIdent, szValue: string;
begin
  while (PText[0] <> #0) and (PText[0] <> FEndDelimiter) do
  begin
    if PText[0] = FIdentDelimiter then
    begin
      szIdent := szValue;
      szValue := '';
    end
    else if PText[0] = FValueDelimiter then
    begin
      Save(szIdent, szValue);
      szIdent := '';
      szValue := '';
    end
    else
      szValue := szValue + PText[0];
    Inc(PText);
  end;
end;

procedure TVonIdentValue.SetText(const Value: string);
var
  P: PChar;
begin
  P := PChar(Value);
  SetPText(P);
end;

procedure TVonIdentValue.SetValueDelimiter(const Value: Char);
begin
  FValueDelimiter := Value;
end;

procedure TVonIdentValue.SetValueOfIndex(Index: Integer; const Value: string);
begin
  FValueList[Index] := Value;
end;

procedure TVonIdentValue.SetValues(Ident: string; const Value: string);
var
  Idx: Integer;
begin
  Save(Ident, Value);
end;

{ TVonTimeList }

{
********************************* TVonTimeList *********************************
}
constructor TVonTimeList.Create;
begin
  List := TList.Create;
end;

destructor TVonTimeList.Destroy;
begin
  while List.Count > 0 do
    Delete(0);
  List.Free;
  inherited;
end;

procedure TVonTimeList.Append(Key: string; Data: string; Datetime: TDateTime);
var
  szItem: TVonTimeItem;
  I: Integer;
begin
  szItem := TVonTimeItem.Create;
  szItem.Key := Key;
  szItem.Data := Data;
  szItem.Datetime := Datetime;
  for I := 0 to List.Count - 1 do
    if TVonTimeItem(List[I]).Datetime > Datetime then
    begin
      List.Insert(I, Pointer(szItem));
      Exit;
    end;
  List.Add(Pointer(szItem));
end;

procedure TVonTimeList.Clear;
begin
  while List.Count > 0 do
  begin
    TVonTimeItem(List[0]).Free;
    List.Delete(0);
  end;
end;

procedure TVonTimeList.Delete(Index: Integer);
begin
  TVonTimeItem(List[Index]).Free;
  List.Delete(Index);
end;

procedure TVonTimeList.Delete(Key: string);
var
  I: Integer;
begin
  for I := 0 to List.Count - 1 do
    if TVonTimeItem(List[I]).Key = Key then
    begin
      Delete(I);
      Exit;
    end;
end;

function TVonTimeList.GetCount: Integer;
begin
  Result := List.Count;
end;

function TVonTimeList.GetItems(Index: Integer): TVonTimeItem;
begin
  Result := TVonTimeItem(List[Index]);
end;

{
******************************** TVonFollowItem ********************************
}
constructor TVonFollowItem.Create;
begin       // 构造函数
  FCount:= 0;
  SetCapacity(0);
end;

destructor TVonFollowItem.Destroy;
begin
  FCount:= 0;
  SetCapacity(0);
end;

procedure TVonFollowItem.Add(CaseValue: string; Item: TVonFollow);
begin
  if FCount = FCapacity then Grow;
  FFollowList[FCount]:= Item;
  FFollowList[FCount].CaseValue:= CaseValue;
  Item.Parent:= Self;
  Inc(FCount);
end;

procedure TVonFollowItem.Clear;
begin
  while FCount > 0 do begin
    FFollowList[FCount - 1].Clear;
    FFollowList[FCount - 1].Free;
    Dec(FCount);
  end;
end;

procedure TVonFollowItem.Delete(const Index: Integer);
var
  I: Integer;
begin
  FFollowList[Index].Clear;
  FFollowList[Index].Free;
  for I := Index to FCount - 2 do
    FFollowList[I]:= FFollowList[I + 1];
  Dec(FCount);
end;

function TVonFollowItem.GetFollows(Index: Integer): TVonFollow;
begin
  Result:= FFollowList[Index];
end;

procedure TVonFollowItem.Grow;
var
  Delta: Integer;
begin     // 成长一下系统实际开创的记录空间
  if FCapacity > 64 then
    Delta := FCapacity div 4
  else if FCapacity > 8 then
    Delta := 16
  else
    Delta := 4;
  SetCapacity(FCapacity + Delta);
end;

procedure TVonFollowItem.Insert(Index: Integer; CaseValue: string; Item: TVonFollow);
var
  I: Integer;
begin
  if FCount = FCapacity then Grow;
  for I := FCount - 1 to Index do
    FFollowList[I + 1]:= FFollowList[I];
  FFollowList[Index]:= Item;
  FFollowList[Index].CaseValue:= CaseValue;
  Item.Parent:= Self;
  Inc(FCount);
end;

procedure TVonFollowItem.Move(Org, Dest: Integer);
var
  offset: Integer;
  szF: TVonFollow;
begin
  if Org = Dest then Exit;
  if Org > Dest then offset:= -1 else offset:= 1;
  szF:= FFollowList[Org];
  repeat
    FFollowList[Org]:= FFollowList[Org + offset];
    Inc(Org, offset);
  until Org = dest;
  FFollowList[Dest]:= szF;
end;

procedure TVonFollowItem.SetCapacity(const Value: Integer);
begin     // 设定当前实际空间数量
  if (Value < FCount) or (Value > MaxListSize) then
    raise EListError.CreateFmt(SListCapacityError, [Value]);
  if Value <> FCapacity then
  begin
    SetLength(FFollowList, Value);
    FCapacity := Value;
  end;
end;

{
********************************** TVonFollow **********************************
}
constructor TVonFollow.Create;
begin       // 构造函数
  FCount:= 0;
  SetCapacity(0);
end;

destructor TVonFollow.Destroy;
begin
  FCount:= 0;
  SetCapacity(0);
end;

procedure TVonFollow.Add(Item: TVonFollowItem);
begin
  if FCount = FCapacity then Grow;
  FItemList[FCount]:= Item;
  Item.Parent:= Self;
  Inc(FCount);
end;

procedure TVonFollow.Clear;
begin
  while FCount > 0 do begin
    FItemList[FCount - 1].Clear;
    FItemList[FCount - 1].Obj.Free;
    FItemList[FCount - 1].Free;
    Dec(FCount);
  end;
end;

procedure TVonFollow.Delete(const Index: Integer);
var
  I: Integer;
begin
  FItemList[Index].Clear;
  FItemList[Index].Obj.Free;
  FItemList[Index].Free;
  for I := Index to FCount - 2 do
    FItemList[I]:= FItemList[I + 1];
  Dec(FCount);
end;

function TVonFollow.GetItems(Index: Integer): TVonFollowItem;
begin
  Result:= FItemList[Index];
end;

procedure TVonFollow.Grow;
var
  Delta: Integer;
begin     // 成长一下系统实际开创的记录空间
  if FCapacity > 64 then
    Delta := FCapacity div 4
  else if FCapacity > 8 then
    Delta := 16
  else
    Delta := 4;
  SetCapacity(FCapacity + Delta);
end;

function TVonFollow.IndexOf(Item: TVonFollowItem): Integer;
var
  I: Integer;
begin
  Result:= -1;
  for I := 0 to FCount - 1 do
    if FItemList[I] = Item then begin
      Result:= I;
      Exit;
    end;
end;

function TVonFollow.IndexOfData(AData: Pointer): Integer;
var
  I: Integer;
begin
  Result:= -1;
  for I := 0 to FCount - 1 do
    if FItemList[I].Data = AData then begin
      Result:= I;
      Exit;
    end;
end;

function TVonFollow.IndexOfID(AID: Integer): Integer;
var
  I: Integer;
begin
  Result:= -1;
  for I := 0 to FCount - 1 do
    if FItemList[I].ID = AID then begin
      Result:= I;
      Exit;
    end;
end;

function TVonFollow.IndexOfKey(AKey: string): Integer;
var
  I: Integer;
begin
  Result:= -1;
  for I := 0 to FCount - 1 do
    if FItemList[I].Key = AKey then begin
      Result:= I;
      Exit;
    end;
end;

function TVonFollow.IndexOfObj(AObj: TObject): Integer;
var
  I: Integer;
begin
  Result:= -1;
  for I := 0 to FCount - 1 do
    if FItemList[I].Obj = AObj then begin
      Result:= I;
      Exit;
    end;
end;

procedure TVonFollow.Insert(Index: Integer; Item: TVonFollowItem);
var
  I: Integer;
begin
  if FCount = FCapacity then Grow;
  for I := FCount - 1 to Index do
    FItemList[I + 1]:= FItemList[I];
  FItemList[Index]:= Item;
  Item.Parent:= Self;
  Inc(FCount);
end;

procedure TVonFollow.Move(Org, Dest: Integer);
var
  offset: Integer;
  item: TVonFollowItem;
begin
  if Org = Dest then Exit;
  if(Org < 0)or(Org >= FCount)or(Dest < 0)or(Dest >= FCount) then Exit;
  if Org > Dest then offset:= -1 else offset:= 1;
  item:= FItemList[Org];
  repeat
    FItemList[Org]:= FItemList[Org + offset];
    Inc(Org, offset);
  until Org = dest;
  FItemList[Dest]:= item;
end;

procedure TVonFollow.SetCapacity(const Value: Integer);
begin     // 设定当前实际空间数量
  if (Value < FCount) or (Value > MaxListSize) then
    raise EListError.CreateFmt(SListCapacityError, [Value]);
  if Value <> FCapacity then
  begin
    SetLength(FItemList, Value);
    FCapacity := Value;
  end;
end;

procedure TVonFollow.SetCaseValue(const Value: string);
begin
  FCaseValue := Value;
end;

{ TVonTable }

{
********************************** TVonTable ***********************************
}
constructor TVonTable.Create;
begin
  FColDelimiter := #3;
  FRowDelimiter := #4;
  FColCapacity:= 0;
  FRowCapacity:= 0;
  FColCount:= 0;
  FRowCount:= 0;
end;

destructor TVonTable.Destroy;
var
  R: Integer;
begin
  for R := 0 to FRowCount - 1 do
    SetLength(FValueList[R]^, 0);
  SetLength(FValueList, 0);
  inherited;
end;

procedure TVonTable.Clear;
var
  R, C: Integer;
begin
  for R := 0 to FRowCount - 1 do
    for C := 0 to FColCount - 1 do
      FValueList[R]^[C]:= '';
end;

procedure TVonTable.DeleteCol(const Col: Integer);
var
  R, C: Integer;
begin
  for R := 0 to FRowCount - 1 do begin
    for C := Col to FColCount - 2 do
      FValueList[R]^[C]:= FValueList[R]^[C + 1];
    FValueList[R]^[FColCount - 1]:= '';
  end;
  FColCount:= FColCount - 1;
end;

procedure TVonTable.DeleteRow(const Row: Integer);
var
  R, C: Integer;
  P: PVonArrayString;
begin
  P:= FValueList[R];
  for R := Row to FRowCount - 2 do
    FValueList[R]:= FValueList[R + 1];
  FValueList[FRowCount - 1]:= P;
  for C := 0 to FColCount - 1 do
    P^[C]:= '';
  FRowCount:= FRowCount - 1;
end;

function TVonTable.GetBOF: Boolean;
begin
  Result:= FRow = 0;
end;

function TVonTable.GetCells(const Col, Row: Integer): string;
begin
  if Row >= FRowCount then Result:= ''//raise Exception.Create('行索引溢出');
  else if Col >= FColCount then Result:= ''//raise Exception.Create('列索引溢出');
  else Result:= FValueList[Row]^[Col];
end;

function TVonTable.GetEOF: Boolean;
begin

end;

function TVonTable.GetText: string;
var
  R, C: Integer;
begin
  Result:= '';
  for R := 0 to FRowCount - 1 do begin
    Result:= Result + FValueList[R]^[0];
    for C := 1 to FColCount - 1 do
      Result:= Result + FColDelimiter + FValueList[R]^[C];
    Result:= Result + FRowDelimiter;
  end;
end;

procedure TVonTable.GrowCol;
var
  Delta, R, C: Integer;
begin     // 成长一下系统实际开创的记录空间
  Delta:= 0;
  while FColCapacity + Delta < FColCount do
    if FColCapacity + Delta > 64 then Inc(Delta, (FColCapacity + Delta) div 4)
    else if FColCapacity + Delta > 8 then Inc(Delta, 16)
    else Inc(Delta, 4);
  for R := 0 to FRowCapacity - 1 do begin
    SetLength(FValueList[R]^, FColCapacity + Delta);
    for C := 0 to Delta - 1 do
      FValueList[R]^[FColCapacity + C]:= '';
  end;
  FColCapacity := FColCapacity + Delta;
end;

procedure TVonTable.GrowRow;
var
  Delta, R, C: Integer;
begin     // 成长一下系统实际开创的记录空间
  Delta:= 0;
  while FRowCapacity + Delta < FRowCount do
    if FRowCapacity + Delta > 64 then Inc(Delta, (FRowCapacity + Delta) div 4)
    else if FRowCapacity + Delta > 8 then Inc(Delta, 16)
    else Inc(Delta, 4);
  SetLength(FValueList, FRowCapacity + Delta);
  for R := FRowCapacity to FRowCapacity + Delta do begin
    New(FValueList[R]);
    SetLength(FValueList[R]^, FColCapacity);
    for C := 0 to FColCapacity - 1 do
      FValueList[R]^[C]:= '';
  end;
  FRowCapacity := FRowCapacity + Delta;
end;

procedure TVonTable.InsertCol(const Col: Integer; Value: array of string);
var
  R, C: Integer;
begin
  ColCount:= ColCount + 1;
  for R := 0 to FRowCount - 1 do begin
    for C := FColCount - 1 downto Col do
      FValueList[R]^[C]:= FValueList[R]^[C - 1];
    if Length(Value) > R then FValueList[R]^[Col]:= Value[R]
    else FValueList[R]^[Col]:= '';
  end;
end;

procedure TVonTable.InsertRow(const Row: Integer; Value: array of string);
var
  R, C: Integer;
  P: PVonArrayString;
begin
  RowCount:= RowCount + 1;
  for R := FRowCount - 1 downto Row + 1 do
    FValueList[R]:= FValueList[R - 1];
  if ColCount < Length(Value) then ColCount:= Length(Value);
  for C := 0 to ColCount - 1 do
    if Length(Value) > C then FValueList[Row]^[C]:= Value[C]
    else FValueList[Row]^[C]:= '';
end;

procedure TVonTable.LoadFromFile(Filename: string; endOfChar: Char);
var
  F: TFileStream;
begin
  F:= TFileStream.Create(Filename, fmOpenRead);
  LoadFromStream(F, endOfChar);
  F.Free;
end;

procedure TVonTable.LoadFromStream(aStream: TStream; endOfChar: Char);
var
  ch: Char;
  C: Integer;
  S: string;
begin
  RowCount:= 1;
  ColCount:= 0;
  C:= 0;
  S:= '';
  while aStream.Read(ch, SizeOf(Char)) = SizeOf(Char) do begin
    if ch = endOfChar then begin
      FValueList[RowCount - 1]^[C]:= S;
      Exit;
    end;
    if ch = FColDelimiter then begin
      FValueList[RowCount - 1]^[C]:= S;
      S:= '';
      Inc(C);
      if C >= FColCount then ColCount:= ColCount + 1;
    end else if ch = FRowDelimiter then begin
      FValueList[RowCount - 1]^[C]:= S;
      RowCount:= RowCount + 1;
      C:= 0;
      S:= '';
    end else S:= S + ch;
  end;
end;

procedure TVonTable.MoveCol(orgIdx, DestIdx: Integer);
var
  S: String;
  R, C, step: Integer;
begin
  if orgIdx >= FColCount then raise Exception.Create(RES_OUT_OF_RANG);
  if DestIdx >= FColCount then raise Exception.Create(RES_OUT_OF_RANG);
  if orgIdx = DestIdx then Exit;
  if orgIdx > DestIdx then step:= -1 else step:= 1;
  for R := 0 to FRowCount - 1 do begin
    C:= orgIdx;
    S:= FValueList[R]^[C];
    while C <> DestIdx do begin
      FValueList[R]^[C]:= FValueList[R]^[C + step];
      Inc(C);
    end;
    FValueList[R]^[DestIdx]:= S;
  end;
end;

procedure TVonTable.MoveRow(orgIdx, DestIdx: Integer);
var
  P: PVonArrayString;
  R, step: Integer;
begin
  if orgIdx >= FRowCount then raise Exception.Create(RES_OUT_OF_RANG);
  if DestIdx >= FRowCount then raise Exception.Create(RES_OUT_OF_RANG);
  if orgIdx = DestIdx then Exit;
  if orgIdx > DestIdx then step:= -1 else step:= 1;
  P:= FValueList[R];
  R:= orgIdx;
  while R <> DestIdx do begin
    FValueList[R]:= FValueList[R + step];
    Inc(R);
  end;
  FValueList[DestIdx]:= P;
end;

procedure TVonTable.SaveToFile(Filename: string);
var
  F: TFileStream;
begin
  F:= TFileStream.Create(Filename, fmCreate);
  SaveToStream(F);
  F.Free;
end;

function TVonTable.SaveToStream(aStream: TStream): Int64;
var
  R, C: Integer;
  procedure WriteStr(S: string);
  begin
    aStream.WriteBuffer(S[1], Length(S) * SizeOf(Char));
  end;
begin
  for R := 0 to FRowCount - 1 do begin
    WriteStr(FValueList[R]^[0]);
    for C := 1 to FColCount - 1 do
      WriteStr(FColDelimiter + FValueList[R]^[C]);
    if R < FRowCount -1 then WriteStr(FRowDelimiter);
  end;
end;

procedure TVonTable.SetBOF(const Value: Boolean);
begin

end;

procedure TVonTable.SetCells(const Col, Row: Integer; const Value: string);
begin
  if Row >= FRowCount then raise Exception.Create(RES_OUT_OF_RANG);
  if Col >= FColCount then raise Exception.Create(RES_OUT_OF_RANG);
  FValueList[Row]^[Col]:= Value;
end;

procedure TVonTable.SetCol(const Value: Integer);
begin
  FCol := Value;
end;

procedure TVonTable.SetColCount(const Value: Integer);
var
  R, C: Integer;
begin
  FColCount := Value;
  if FColCount > FColCapacity then GrowCol;
end;

procedure TVonTable.SetEOF(const Value: Boolean);
begin

end;

procedure TVonTable.SetRow(const Value: Integer);
begin
  FRow := Value;
end;

procedure TVonTable.UpdateRow(const Row: Integer; Value: array of string);
var
  C: Integer;
begin
  for C := 0 to ColCount - 1 do
    if Length(Value) > C then FValueList[Row]^[C]:= Value[C]
    else FValueList[Row]^[C]:= '';
end;

procedure TVonTable.SetRowCount(const Value: Integer);
begin
  FRowCount := Value;
  if FRowCount > FRowCapacity then GrowRow;
end;

procedure TVonTable.SetText(const Value: string);
begin
  SplitText(PChar(Value), #0);
end;

function TVonTable.SplitText(P: PChar; endOfChar: char): PChar;
var
  C, R: Integer;
  S: string;
begin
  if P^ = #0 then Exit;
  RowCount:= 1;
  R:= 0;
  ColCount:= 1;
  Result:= P;
  C:= 0;
  S:= '';
  while(Result^ <> #0)and(Result^ <> endOfChar) do begin
    if R = RowCount then RowCount:= R + 1;
    if Result^ = FColDelimiter then begin
      FValueList[R]^[C]:= S;
      S:= '';
      Inc(C);
      if C >= FColCount then ColCount:= ColCount + 1;
    end else if Result^ = FRowDelimiter then begin
      FValueList[R]^[C]:= S;
      S:= '';
      R:= R + 1;
      C:= 0;
    end else S:= S + Result^;
    Inc(Result);
  end;
end;

{ TSerieItem }

constructor TSerieItem.Create(Key, Value: string; Obj: TObject);
begin
  FKey:= Key; FValue:= Value; FObj:= Obj;
  FPrevious:= nil;
  FNext:= nil;
end;

{ TSerieCollection }

procedure TSerieCollection.Append(AKey, AValue: string; AObj: TObject);
var
  item: TSerieItem;
begin
  if not Assigned(FFirst) then begin
    FFirst:= TSerieItem.Create(AKey, AValue, AObj);
    FLast:= FFirst;
  end else begin
    item:= FFirst;
    while Assigned(item) do
      if MatchText(item.Key, AKey) then Exit
      else item:= item.Next;
    FLast.Next:= TSerieItem.Create(AKey, AValue, AObj);
    FLast.Next.Previous:= FLast;
    FLast:= FLast.Next;
  end;
end;

procedure TSerieCollection.Clear;
var
  item: TSerieItem;
begin
  while Assigned(FFirst) do begin
    item:= FFirst; FFirst:= FFirst.Next;
    if FAutoFreeObject and Assigned(Item.Obj) then Item.Obj.Free;
    FreeAndNil(Item);
  end;
end;

constructor TSerieCollection.Create(AutoFreeObject: Boolean);
begin
  FAutoFreeObject:= AutoFreeObject;
end;

procedure TSerieCollection.Delete(AKey: string);
var
  item, temp: TSerieItem;
begin
  item:= FFirst;
  while Assigned(item) do begin
    if MatchText(item.Key, AKey)  then begin
      Delete(item);
      Exit;
    end else item:= item.Next;
  end;
end;

procedure TSerieCollection.Delete(Item: TSerieItem);
begin
  if item = FFirst then begin
    FFirst:= FFirst.Next;
    if Assigned(FFirst) then FFirst.Previous:= nil;
  end;
  if item = FLast then begin
    FLast:= FLast.Previous;
    if Assigned(FLast) then FLast.Next:= nil;
  end;
  if FAutoFreeObject and Assigned(Item.Obj) then begin Item.Obj.Free; Item.Obj:= nil; end;
  if Assigned(item.Previous) then item.Previous.Next:= item.Next;
  if Assigned(item.Next) then item.Next.Previous:= item.Previous;
  FreeAndNil(item);
end;

destructor TSerieCollection.Destroy;
begin
  Clear;
  inherited;
end;

function TSerieCollection.Find(AKey: string): TSerieItem;
var
  item, temp: TSerieItem;
begin
  Result:= nil;
  item:= FFirst;
  while Assigned(item) do begin
    if MatchText(item.Key, AKey)  then begin
      Result:= item;
      Exit;
    end else item:= item.Next;
  end;
end;

{ TVonTreeItem }

function TVonTreeItem.AddChild(AInfo: string): Integer;
var
  AItem: TVonTreeItem;
begin
  AItem:= TVonTreeItem.Create;
  AItem.Info:= AInfo;
  Result:= FChildren.Add(AItem);
end;

procedure TVonTreeItem.Clear();
var
  I: Integer;
begin
  for I := FChildren.Count - 1 downto 0 do
    Delete(I);
end;

constructor TVonTreeItem.Create;
begin
  FChildren:= TList.Create;
end;

procedure TVonTreeItem.Delete(Index: Integer);
begin
  if(Index >= FChildren.Count)or(Index < 0)then Exit;
  TVonTreeItem(FChildren[Index]).Clear();
  TVonTreeItem(FChildren[Index]).Free;
  FChildren.Delete(Index);
end;

destructor TVonTreeItem.Destroy;
begin
  Clear;
  FChildren.Free;
  inherited;
end;

function TVonTreeItem.GetChildCount: Integer;
begin
  Result:= FChildren.Count;
end;

function TVonTreeItem.GetChildren(Index: Integer): TVonTreeItem;
begin
  if(Index >= FChildren.Count)or(Index < 0)then Result:= nil
  else Result:= TVonTreeItem(FChildren[Index]);
end;

procedure TVonTreeItem.LoadFromStream(S: TStream);
var
  I, Cnt, Idx: Integer;
begin
  Cnt:= UVonSystemFuns.ReadIntFromStream(S);
  for I := 0 to Cnt - 1 do begin
    Idx:= AddChild(ReadStringFromStream(S));
    Children[Idx].LoadFromStream(S);
  end;
end;

procedure TVonTreeItem.SaveToStream(S: TStream);
var
  I: Integer;
begin
  WriteStringToStream(Info, S);
  WriteIntToStream(FChildren.Count, S);
  for I := 0 to ChildCount - 1 do
    Children[I].SaveToStream(S);
end;

{ TVonTreeCollection<T> }

procedure TVonTreeCollection<T>.ClearItems;
begin
  FRoot.Clear();
end;

constructor TVonTreeCollection<T>.Create;
begin
  FRoot:= TVonTreeItem.Create;
  FRoot.Info:= 'ROOT';
end;

destructor TVonTreeCollection<T>.Destroy;
begin
  FRoot.Clear();
  FRoot.Free;
  inherited;
end;

function TVonTreeCollection<T>.GetItems(Index: Integer): T;
begin
  Result:= FRoot.Children[Index] as T
end;

procedure TVonTreeCollection<T>.LoadFromStream(S: TStream);
begin
  FRoot.LoadFromStream(S);
end;

procedure TVonTreeCollection<T>.SaveToStream(S: TStream);
begin
  FRoot.SaveToStream(S);
end;

procedure TVonTreeCollection<T>.SetItems(Index: Integer; const Value: T);
begin
  FRoot.Children[Index].Free;
  FRoot.FChildren[Index]:= Pointer(T);
end;

{ TFIFOItem }

constructor TFIFOItem.Create(AObj: TObject; AData: Integer);
begin
  FObj:= AObj;
  FData:= AData;
end;

{ TFIFO }

constructor TFIFO.Create;
begin
  InitializeCriticalSection(lpCriticalSection);
  FFirst:= nil;
  FLaster:= nil;
end;

destructor TFIFO.Destroy;
var
  item: TFIFOItem;
begin
  while Assigned(FFirst) do begin
    item:= FFirst;
    FFirst:= FFirst.Next;
    if Assigned(item.obj) then item.obj.Free;
    item.free
  end;
  DeleteCriticalSection(lpCriticalSection);
  inherited;
end;

function TFIFO.Pull: TFIFOItem;
var
  item: TFIFOItem;
begin
  Result := FFirst;
  EnterCriticalSection(lpCriticalSection);
  if Assigned(FFirst) then FFirst:= FFirst.Next;
  if not Assigned(FFirst) then FLaster:= nil;
  LeaveCriticalSection(lpCriticalSection);
end;

procedure TFIFO.Push(item: TFIFOItem);
begin
  item.FNext:= nil;
  EnterCriticalSection(lpCriticalSection);
  if not Assigned(FFirst) then begin
    FFirst:= item;
    FLaster:= item;
  end else begin
    FLaster.FNext:= item;
    FLaster:= item;
  end;
  LeaveCriticalSection(lpCriticalSection);
end;

{ TVonSectionValue }

destructor TVonSectionValue.Destroy;
begin
  SetLength(FData, 0);
  inherited;
end;

function TVonSectionValue.GetText: string;
var
  szI: Integer;
  szFloat: Extended;
  szDate: TDateTime;
  szByte: Byte;
  szGuid: TGuid;
begin
  case ValueType of
  vst_str: Result:= TEncoding.Default.GetString(FData);
  vst_int: begin Move(FData[0], szI, 4); Result:= IntToStr(szI); end;
  vst_float: begin Move(FData[0], szFloat, 10); Result:= FloatToStr(szFloat); end;
  vst_datetime: begin Move(FData[0], szDate, SizeOf(TDateTime)); Result:= DatetimeToStr(szDate); end;
  vst_bool: Result:= BoolToStr(FData[0] = 1, false);
  vst_Guid: begin Move(FData[0], szGuid, SizeOf(TGuid)); Result:= GuidToString(szGuid); end;
  vst_bytes: Result:= IntToHex(FData[0], 2) + ' ... ... ' + IntToHex(FData[ValueSize - 1], 2);
  end;
end;

procedure TVonSectionValue.SetValueSize(const Value: Integer);
begin
  FValueSize := Value;
  SetLength(FData, FValueSize);
end;

{ TVonSection }

function TVonSection.SetBinary(AName: string; Value: TBytes): Integer;
var
  item: TVonSectionValue;
begin
  Delete(AName);
  item:= TVonSectionValue.Create;
  item.Name:= AName;
  item.ValueType:= vst_bytes;
  item.ValueSize:= Length(Value);
  Move(Value[0], item.FData[0], item.ValueSize);
  Result:= FList.Add(item);
end;

function TVonSection.SetBool(AName: string; Value: Bool): Integer;
var
  item: TVonSectionValue;
begin
  Delete(AName);
  item:= TVonSectionValue.Create;
  item.Name:= AName;
  item.ValueType:= vst_bool;
  item.ValueSize:= 1;
  if Value then item.FData[0]:= 1 else item.FData[0]:= 0;
  Result:= FList.Add(item);
end;

function TVonSection.SetDatetime(AName: string; Value: TDatetime): Integer;
var
  item: TVonSectionValue;
begin
  Delete(AName);
  item:= TVonSectionValue.Create;
  item.Name:= AName;
  item.ValueType:= vst_datetime;
  item.ValueSize:= SizeOf(TDatetime);
  Move(Value, item.FData[0], item.ValueSize);
  Result:= FList.Add(item);
end;

function TVonSection.SetFloat(AName: string; Value: Extended): Integer;
var
  item: TVonSectionValue;
begin
  Delete(AName);
  item:= TVonSectionValue.Create;
  item.Name:= AName;
  item.ValueType:= vst_float;
  item.ValueSize:= SizeOf(Extended);
  Move(Value, item.FData[0], item.ValueSize);
  Result:= FList.Add(item);
end;

function TVonSection.SetGuid(AName: string; Value: TGuid): Integer;
var
  item: TVonSectionValue;
begin
  Delete(AName);
  item:= TVonSectionValue.Create;
  item.Name:= AName;
  item.ValueType:= vst_Guid;
  item.ValueSize:= SizeOf(TGuid);
  Move(Value, item.FData[0], item.ValueSize);
  Result:= FList.Add(item);
end;

function TVonSection.SetInteger(AName: string; Value: Integer): Integer;
var
  item: TVonSectionValue;
begin
  Delete(AName);
  item:= TVonSectionValue.Create;
  item.Name:= AName;
  item.ValueType:= vst_int;
  item.ValueSize:= SizeOf(Integer);
  Move(Value, item.FData[0], item.ValueSize);
  Result:= FList.Add(item);
end;

function TVonSection.SetStream(AName: string; Value: TStream): Integer;
var
  item: TVonSectionValue;
  buff: TBytes;
begin
  Delete(AName);
  SetLength(buff, Value.Size);
  Value.Read(buff, Value.Size);
  item:= TVonSectionValue.Create;
  item.Name:= AName;
  item.ValueType:= vst_bytes;
  item.ValueSize:= Value.Size;
  Move(buff[0], item.FData[0], item.ValueSize);
  Result:= FList.Add(item);
  SetLength(buff, 0);
end;

function TVonSection.SetString(AName, Value: string): Integer;
var
  item: TVonSectionValue;
  data: TBytes;
begin
  Delete(AName);
  item:= TVonSectionValue.Create;
  item.Name:= AName;
  item.ValueType:= vst_str;
  data:= TEncoding.Default.GetBytes(Value);
  item.ValueSize:= Length(data);
  Move(data[0], item.FData[0], item.ValueSize);
  Result:= FList.Add(item);
end;

function TVonSection.SetValue(Value: TVonSectionValue): Integer;
begin
  Delete(Value.Name);
  Result:= FList.Add(Value);
end;

constructor TVonSection.Create;
begin
  FList:= TList.Create;
end;

procedure TVonSection.Delete(Index: Integer);
var
  item: TVonSectionValue;
begin
  item:= TVonSectionValue(FList[Index]);
  item.Free;
  FList.Delete(Index);
end;

procedure TVonSection.Delete(AName: string);
var
  I: Integer;
begin
  for I:= 0 to FList.Count - 1 do
    if SameText(TVonSectionValue(FList[I]).Name, AName) then begin
      Delete(I);
      Exit;
    end;
end;

destructor TVonSection.Destroy;
begin
  Clear;
  inherited;
end;

function TVonSection.GetBinary(AName: string): TBytes;
var
  item: TVonSectionValue;
begin
  item:= GetValues(AName);
  if item = nil then Result:= nil
  else if item.ValueType = vst_bytes then Result:= item.Data
  else Result:= nil;
end;

function TVonSection.GetBool(AName: string; default: Bool): Bool;
var
  item: TVonSectionValue;
begin
  item:= GetValues(AName);
  if item = nil then Result:= default
  else if item.ValueType = vst_bool then Result:= item.Data[0] = 1
  else Result:= default;
end;

function TVonSection.GetDatetime(AName: string; default: TDatetime): TDatetime;
var
  item: TVonSectionValue;
begin
  item:= GetValues(AName);
  if item = nil then Result:= default
  else if item.ValueType = vst_datetime then
    Move(item.Data[0], Result, item.ValueSize)
  else Result:= default;
end;

function TVonSection.GetFloat(AName: string; default: Extended): Extended;
var
  item: TVonSectionValue;
begin
  item:= GetValues(AName);
  if item = nil then Result:= default
  else if item.ValueType = vst_float then
    Move(item.Data[0], Result, item.ValueSize)
  else Result:= default;
end;

function TVonSection.GetGuid(AName: string; default: TGuid): TGuid;
var
  item: TVonSectionValue;
begin
  item:= GetValues(AName);
  if item = nil then Result:= default
  else if item.ValueType = vst_Guid then
    Move(item.Data[0], Result, item.ValueSize)
  else Result:= default;
end;

function TVonSection.GetInteger(AName: string; default: Integer): Integer;
var
  item: TVonSectionValue;
begin
  item:= GetValues(AName);
  if item = nil then Result:= default
  else if item.ValueType = vst_int then
    Move(item.Data[0], Result, item.ValueSize)
  else Result:= default;
end;

procedure TVonSection.GetStream(AName: string; Value: TStream);
var
  item: TVonSectionValue;
begin
  item:= GetValues(AName);
  if item.ValueType = vst_bytes then
    Value.Write(item.Data, item.ValueSize);
end;

function TVonSection.GetString(AName: string; default: string): string;
var
  item: TVonSectionValue;
begin
  item:= GetValues(AName);
  if item = nil then Result:= default
  else if item.ValueType = vst_str then
    Result:= TEncoding.Default.GetString(item.Data)
  else Result:= default;
end;

function TVonSection.GetValueByIndex(Index: Integer): TVonSectionValue;
begin
  Result:= TVonSectionValue(FList[Index]);
end;

function TVonSection.GetValueCount: Integer;
begin
  Result:= FList.Count;
end;

function TVonSection.GetValues(AName: string): TVonSectionValue;
var
  I: Integer;
begin
  Result:= nil;
  for I:= 0 to FList.Count - 1 do
    if SameText(TVonSectionValue(FList[I]).Name, AName) then begin
      Result:= TVonSectionValue(FList[I]);
      Exit;
    end;
end;

procedure TVonSection.Clear;
var
  I: Integer;
begin
  for I:= 0 to FList.Count - 1 do
    TVonSectionValue(FList[I]).Free;
  FList.Clear;
end;

{ TVonSectionGroup }

procedure TVonSectionGroup.AddSection(ASection: TVonSection);
begin
  Delete(ASection.SectionName);
  FList.Add(ASection);
end;

procedure TVonSectionGroup.Clear;
var
  I: Integer;
begin
  for I:= 0 to FList.Count - 1 do
    TVonSection(FList[I]).Free;
  FList.Clear;
end;

constructor TVonSectionGroup.Create;
begin
  FList:= TList.Create;
end;

procedure TVonSectionGroup.Delete(AName: string);
var
  I: Integer;
begin
  for I:= 0 to FList.Count - 1 do
    if SameText(TVonSection(FList[I]).SectionName, AName) then begin
      Delete(I);
      Exit;
    end;
end;

procedure TVonSectionGroup.Delete(Index: Integer);
var
  item: TVonSection;
begin
  item:= TVonSection(FList[Index]);
  item.Free;
  FList.Delete(Index);
end;

destructor TVonSectionGroup.Destroy;
begin
  Clear;
  inherited;
end;

function TVonSectionGroup.GetSectionByIndex(Index: Integer): TVonSection;
begin
  Result:= TVonSection(FList[Index]);
end;

function TVonSectionGroup.GetSectionCount: Integer;
begin
  Result:= FList.Count;
end;

function TVonSectionGroup.GetSections(Name: string): TVonSection;
var
  I: Integer;
begin
  Result:= nil;
  for I:= 0 to FList.Count - 1 do
    if SameText(TVonSection(FList[I]).SectionName, Name) then begin
      Result:= TVonSection(FList[I]);
      Exit;
    end;
end;

procedure TVonSectionGroup.LoadFromFile(Filename: string);
var
  fs: TFileStream;
begin
  fs:= TFileStream.Create(Filename, fmOpenRead);
  LoadFromStream(fs);
  fs.Free;
end;

procedure TVonSectionGroup.LoadFromStream(AStream: TStream);
var
  I, J: Integer;
  SectionName: string;
  flag: Byte;
  b: TBytes;
  section: TVonSection;
  value: TVonSectionValue;
begin     //fs ->[int,sectionName],{[1,valueType],[int,ValueName],[int,ValueBytes]}0..n
  Clear;
  while AStream.Position < AStream.Size do begin
    section:= TVonSection.Create;
    section.SectionName:= ReadStringFromStream(AStream);
    AStream.Read(flag, 1);
    while flag <> 255 do begin
      value:= TVonSectionValue.Create;
      value.Name := ReadStringFromStream(AStream);
      Value.ValueType:= TVonSectionValueType(flag);
      Value.ValueSize:= ReadIntFromStream(AStream);
      AStream.Read(Value.Data[0], Value.ValueSize);
      section.SetValue(Value);
      AStream.Read(flag, 1);
    end;
    AddSection(section);
  end;
end;

procedure TVonSectionGroup.SaveToFile(Filename: string);
var
  fs: TFileStream;
begin
  fs:= TFileStream.Create(Filename, fmCreate);
  SaveToStream(fs);
  fs.Free;
end;

procedure TVonSectionGroup.SaveToStream(AStream: TStream);
const flag : byte = 255;
var
  I, J: Integer;
  b: TBytes;
  section: TVonSection;
  value: TVonSectionValue;
begin     //fs ->[int,sectionName],{[1,valueType],[int,ValueName],[int,ValueBytes]}0..n
  for I:= 0 to FList.Count - 1 do begin
    section:= TVonSection(FList[I]);
    //[int,sectionName]
    WriteStringToStream(TVonSection(FList[I]).SectionName, AStream);
    for J:= 0 to section.ValueCount - 1 do begin
      value:= TVonSectionValue(section.ValueByIndex[J]);
      //[1,valueType]
      AStream.Write(value.ValueType, 1);
      //[int,ValueName]
      WriteStringToStream(value.Name, AStream);
      //[int,ValueBytes]
      AStream.Write(value.ValueSize, SizeOf(Integer));
      AStream.Write(value.Data[0], value.ValueSize);
    end;
    AStream.Write(flag, 1);
  end;
end;

initialization
  //WriteLog(LOG_DEBUG, 'UVonConfig', 'Create any memory.');

FVonArray := TVonArraySetting.Create;
FVonSetting := TVonSetting.Create;
FVonList := TVonList.Create;

finalization

FVonList.Free;
FVonArray.Free;
FVonSetting.Free;

end.
