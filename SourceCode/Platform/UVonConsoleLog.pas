unit UVonConsoleLog;

interface

uses SysUtils, Forms, stdctrls, Controls, Graphics, DateUtils, UVonLog;

type
  TVonConsoleFile = class(TVonLog)
  private
    FLogger: TForm;
    FNote: TMemo;
  public
    constructor Create(ApplicationName, LogPath: string); override;
    destructor Destory; override;
    procedure WriteLog(LogClass: ELOG_Class; Process, Info: string); override;
  end;

implementation

{ TVonEventFile }

constructor TVonConsoleFile.Create(ApplicationName, LogPath: string);
begin
  inherited;
  FLogger:= TForm.Create(nil);
  FNote:= TMemo.Create(FLogger);
  FNote.Parent:= FLogger;
  FNote.Align:= alClient;
  FLogger.Show;
  FLogger.Width:= 800;
  FLogger.Height:= 600;
  FNote.Color:= clBlack;
  FNote.Font.Color:= clWhite;
end;

destructor TVonConsoleFile.Destory;
begin
  FLogger.Close;
  FLogger.Free;
  inherited
end;

procedure TVonConsoleFile.WriteLog(LogClass: ELOG_Class; Process, Info: string);
var
  szS: string;
begin
  if LogClass > FLogClass then Exit;
  case LogClass of
    LOG_MAJOR:    szS := 'M';
    LOG_FAIL:     szS := 'E';
    LOG_WARNING:  szS := '!';
    LOG_INFO:     szS := ' ';
    LOG_DEBUG:    szS := '?';
  end;
  FNote.Lines.Insert(0, szS + FormatDatetime('[yyyy-mm-dd hh:nn:ss:zzz]', Now) +
    Process + #9 + Info);
  while FNote.Lines.Count > 40 do
    FNote.Lines.Delete(FNote.Lines.Count - 1);
end;

initialization

RegistLog('CONSOLELOG', TVonConsoleFile);

finalization


end.
