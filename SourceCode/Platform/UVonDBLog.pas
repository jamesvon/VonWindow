unit UVonDBLog;

interface

uses SysUtils, DateUtils, UVonLog, Data.Win.ADODB;

type
  TVonDBLog = class(TVonLog)
  private
    ADOConn: TADOConnection;
    FCurrentDate: TDate;
  public
    constructor Create(ApplicationName, LogParam: string); override;
    destructor Destory; override;
    procedure WriteLog(LogClass: ELOG_Class; Process, Info: string); override;
  end;

implementation

uses UPlatformDB;

{ TVonLogFile }

constructor TVonDBLog.Create(ApplicationName, LogParam: string);
begin
  inherited;
  ADOConn:= TADOConnection.Create(nil);
  ADOConn.ConnectionString:= LogParam;
  ADOConn.LoginPrompt:= False;
  ADOConn.Open();
end;

destructor TVonDBLog.Destory;
begin
  ADOConn.Free;
  inherited
end;

procedure TVonDBLog.WriteLog(LogClass: ELOG_Class; Process, Info: string);
var
  szS: string;
begin
  if LogClass > FLogClass then Exit;
  if ADOConn.Provider = 'Microsoft.Jet.OLEDB.4.0' then
    ADOConn.Execute('INSERT INTO [SYS_Log]([LogTime],[UserName],[LogClass]'
    + ',[Processor],[Content])VALUES(Date()+Time(),''' + FPlatformDB.LoginInfo.LoginName
    + ''',' + IntToStr(Ord(LogClass)) + ',''' + Process + ''',''' + Info + ''')')
  else
    ADOConn.Execute('INSERT INTO [SYS_Log]([LogTime],[UserName],[LogClass]'
    + ',[Processor],[Content])VALUES(GetDate(),''' + FPlatformDB.LoginInfo.LoginName
    + ''',' + IntToStr(Ord(LogClass)) + ',''' + Process + ''',''' + Info + ''')');
end;

initialization

RegistLog('DBLOG', TVonDBLog);

finalization

end.
