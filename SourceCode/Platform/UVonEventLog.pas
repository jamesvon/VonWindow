unit UVonEventLog;

interface

uses SysUtils, DateUtils, SvcMgr, UVonLog;

type
  TVonEventFile = class(TVonLog)
  private
    FEventLogger: TEventLogger;
  public
    constructor Create(ApplicationName, LogPath: string); override;
    destructor Destory; override;
    procedure WriteLog(LogClass: ELOG_Class; Process, Info: string); override;
  end;

implementation

{ TVonEventFile }

constructor TVonEventFile.Create(ApplicationName, LogPath: string);
begin
  inherited;
  FEventLogger:= TEventLogger.Create(ApplicationName);
end;

destructor TVonEventFile.Destory;
begin
  FEventLogger.Free;
  inherited
end;

procedure TVonEventFile.WriteLog(LogClass: ELOG_Class; Process, Info: string);
var
  szID: Integer;
begin
  if LogClass > FLogClass then Exit;
  case LogClass of
    LOG_MAJOR:    szID := 0;
    LOG_FAIL:     szID := 1;
    LOG_WARNING:  szID := 2;
    LOG_INFO:     szID := 3;
    LOG_DEBUG:    szID := 4;
  end;
  FEventLogger.LogMessage(FormatDatetime('[yyyy-mm-dd hh:nn:ss:zzz]', Now) +
    Process + #9 + Info, szID, 0, 0);
end;

initialization

RegistLog('WINLOG', TVonEventFile);

finalization

end.
