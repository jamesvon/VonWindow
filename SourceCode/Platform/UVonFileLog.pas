unit UVonFileLog;

interface

uses WinApi.windows, SysUtils, Forms, DateUtils, UVonLog;

type
  TVonLogFile = class(TVonLog)
  private
    FLog_File: TextFile;
    FCurrentDate: TDate;
    FCritical: TRTLCriticalSection;
  public
    constructor Create(ApplicationName, LogPath: string); override;
    destructor Destory; override;
    procedure WriteLog(LogClass: ELOG_Class; Process, Info: string); override;
  end;

implementation

{ TVonLogFile }

constructor TVonLogFile.Create(ApplicationName, LogPath: string);
var
  searchRec: TSearchRec;
  logDate: TDatetime;
  y,m,d: Integer;
begin
  inherited;
  InitializeCriticalSection(FCritical);
  if FindFirst(LogPath + '*.LOG', faAnyfile, searchRec) = 0 then try
    repeat
      Application.ProcessMessages;
      if ((searchRec.Name = '.') or (searchRec.Name = '..')) then Continue;
      if Length(searchRec.Name) < 8 then Continue;
      if not TryStrToInt(Copy(searchRec.Name, 1, 4), y) then Continue;
      if not TryStrToInt(Copy(searchRec.Name, 5, 2), m) then Continue;
      if not TryStrToInt(Copy(searchRec.Name, 7, 2), d) then Continue;
      logDate:= EncodeDate(y, m, d);
      if IncMonth(logDate, 2) < now then
        DeleteFile(LogPath + searchRec.Name);
    until FindNext(searchRec) <> 0;
  except
    on E: Exception do begin
      WriteLog(LOG_FAIL, 'TVonLogFile.Create', 'Cannote remove ' + searchRec.Name);
    end;
  end;
  FCurrentDate:= 0;
end;

destructor TVonLogFile.Destory;
begin
  CloseFile(FLog_File);
  DeleteCriticalSection(FCritical);//删除临界区
  inherited
end;

procedure TVonLogFile.WriteLog(LogClass: ELOG_Class; Process, Info: string);
var
  szS: string;
begin
//  if LogClass > FLogClass then Exit;
  EnterCriticalSection(FCritical); //进入临界区
  if FCurrentDate < DateOf(Now) then begin
    if FCurrentDate > 0 then
      CloseFile(FLog_File);
    FCurrentDate:= DateOf(Now);
    szS:= LogParam + FormatDatetime('yyyymmdd', FCurrentDate) + '.LOG';
    AssignFile(FLog_File, szS);
    if FileExists(szS) then Append(FLog_File)
    else Rewrite(FLog_File);
  end;
  szS := 'M';
  case LogClass of
    LOG_MAJOR:    szS := 'M';
    LOG_FAIL:     szS := 'E';
    LOG_WARNING:  szS := '!';
    LOG_INFO:     szS := ' ';
    LOG_DEBUG:    szS := '?';
  end;
  szS := FormatDatetime('[yyyy-mm-dd hh:nn:ss:zzz]', Now) + Process + #9 + Info;
  WriteLn(FLog_File, szS);
  Flush(FLog_File);
  LeaveCriticalSection(FCritical); //离开临界区
end;

initialization

RegistLog('FILELOG', TVonLogFile);

finalization

end.
