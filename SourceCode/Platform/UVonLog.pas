unit UVonLog;

interface

uses
  SysUtils, Classes, winapi.Windows, forms, SyncObjs, System.win.Registry, SvcMgr;

const
  CONST_PIPEFORMAT = '\\%s\pipe\%s';
  VON_LOGSERVICE_NAME = 'VONLOGSERVICE';

resourcestring
  RES_LOG_NO_OPEN = 'The log is not opened.';
  RES_LOG_CONNECT_FAILED = '连接日志管理器失败';
  RES_LOG_WRITE_ERROR = '写日志失败';

type
  { LOG 日志管理部分 }
  /// <summary>日志级别，从大到小排列</summary>
  /// <param name="LOG_MAJOR">重要信息</param>
  /// <param name="LOG_FAIL">错误或失败信息</param>
  /// <param name="LOG_WARNING">警告信息</param>
  /// <param name="LOG_INFO">一般信息</param>
  /// <param name="LOG_DEBUG">调试信息</param>
  ELOG_Class = (LOG_MAJOR, LOG_FAIL, LOG_WARNING, LOG_INFO, LOG_DEBUG);

  TVonLogClass = class of TVonLog;

  TVonLog = class
  protected
    FLogClass: ELOG_Class;
    FAppName: string;
    FLogParam: string;
    procedure SetLogClass(const Value: ELOG_Class);
  public
    constructor Create(ApplicationName, Param: string); virtual;
    destructor Destory; virtual;
    procedure WriteLog(LogClass: ELOG_Class; Process, Info: string);
      virtual; abstract;
  published
    property AppName: string read FAppName;
    property LogParam: string read FLogParam;
    property LogClass: ELOG_Class read FLogClass write SetLogClass;
  end;

procedure RegistLog(ALogType: string; ALogClass: TVonLogClass);
/// <summary>注册日志文件</summary>
procedure OpenLog(ALogType, AppName, Params: string; AvailClass: ELOG_Class);
/// <summary>写日志</summary>
/// <param name="LogClass">日志等级</param>
/// <param name="Process">进程名称</param>
/// <param name="Info">日志内容</param>
procedure WriteLog(LogClass: ELOG_Class; Process, Info: string); overload;
procedure WriteLog(LogClass: ELOG_Class; Process: string; buff: array of byte; buffSize: Integer); overload;
procedure WriteStream(LogClass: ELOG_Class; Process: string; Stream: TStream);
/// <summary>关闭日志</summary>
procedure CloseLog(ALogType: string = '');

implementation

var
  FLogClassList: TStringList;
  FLogList: TStringList;

  { LOG }

procedure RegistLog(ALogType: string; ALogClass: TVonLogClass);
var
  Idx: Integer;
begin
  Idx := FLogClassList.IndexOf(ALogType);
  if Idx < 0 then
    FLogClassList.AddObject(ALogType, TObject(ALogClass))
  else
    FLogClassList.Objects[Idx] := TObject(ALogClass);
end;

procedure OpenLog(ALogType, AppName, Params: string; AvailClass: ELOG_Class);
var
  Idx: Integer;
  function newLog: TVonLog;
  begin
    Result := TVonLogClass(FLogClassList.Objects[FLogClassList.IndexOf(ALogType)
      ]).Create(AppName, Params);
    Result.LogClass := AvailClass;
  end;

begin
  Idx := FLogList.IndexOf(ALogType);
  if Idx < 0 then
    FLogList.AddObject(ALogType, newLog)
  else
  begin
    FLogList.Objects[Idx].Free;
    FLogList.Objects[Idx] := newLog;
  end;
end;

procedure WriteLog(LogClass: ELOG_Class; Process, Info: string);
var
  I: Integer;
begin
  for I := 0 to FLogList.Count - 1 do
    TVonLog(FLogList.Objects[I]).WriteLog(LogClass, Process, Info);
end;

procedure WriteLog(LogClass: ELOG_Class; Process: string; buff: array of byte; buffSize: Integer);
var
  I: Integer;
  s, c: string;
begin
  S:= ''; c:= '';
  for I := 0 to buffSize - 1 do begin
    if I mod 16 = 0 then begin
      S:= S + '         ' + c + #13#10;
      c:= '';
    end else if I mod 8 = 0 then
      S:= S + '  ';
    S:= S + ' ' + IntToHex(buff[I], 2);
    c:= c + Char(buff[I]);
  end;
  WriteLog(LogClass, Process, S + '         ' + c);
end;

procedure WriteStream(LogClass: ELOG_Class; Process: string; Stream: TStream);
var
  I: Integer;
  szS: TStringStream;
  b: Byte;
begin
  I:= 0;
  szS:= TStringStream.Create;
  szS.WriteString(#13#10);
  while Stream.Position < Stream.Size do begin
    Stream.Read(b, 1);
    szS.WriteString(IntToHex(b, 2) + ' ');
    inc(I);
    case I of
    8: szS.WriteString(' ');
    16: begin szS.WriteString(#13#10); I:= 0; end;
    end;
  end;
  WriteLog(LogClass, Process, szS.DataString);
  szS.Free;
end;

procedure CloseLog(ALogType: string);
var
  Idx: Integer;
begin
  if ALogType = '' then
  begin
    for Idx := 0 to FLogList.Count - 1 do
      FLogList.Objects[Idx].Free;
    FLogList.Clear;
  end
  else
  begin
    Idx := FLogList.IndexOf(ALogType);
    if Idx >= 0 then
    begin
      FLogList.Objects[Idx].Free;
      FLogList.Delete(Idx);
    end;
  end;
end;

{ TVonFile }

constructor TVonLog.Create(ApplicationName, Param: string);
begin
  FAppName := ApplicationName;
  FLogParam := Param;
  FLogClass := LOG_DEBUG;
end;

destructor TVonLog.Destory;
begin

end;

procedure TVonLog.SetLogClass(const Value: ELOG_Class);
begin
  FLogClass := Value;
end;

initialization

FLogClassList := TStringList.Create;
FLogList := TStringList.Create;

finalization

CloseLog;
FLogList.Free;
FLogClassList.Free;

end.
