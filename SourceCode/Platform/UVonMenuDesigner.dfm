object FVonMenuDesigner: TFVonMenuDesigner
  AlignWithMargins = True
  Left = 411
  Top = -50
  Caption = 'FVonMenuDesigner'
  ClientHeight = 779
  ClientWidth = 1000
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pcSettings: TPageControl
    Left = 0
    Top = 0
    Width = 1000
    Height = 747
    ActivePage = tsMenu
    Align = alClient
    DoubleBuffered = True
    Images = FPlatformDB.ImgSmall
    MultiLine = True
    ParentDoubleBuffered = False
    TabOrder = 0
    OnResize = pcSettingsResize
    object tsUsing: TTabSheet
      Caption = #21151#33021#35774#32622
      object lstUsing: TListBox
        Left = 0
        Top = 0
        Width = 839
        Height = 699
        Align = alClient
        Columns = 10
        ItemHeight = 13
        TabOrder = 0
        OnDblClick = lstUsingDblClick
      end
      object PlUsing: TPanel
        Left = 839
        Top = 0
        Width = 153
        Height = 699
        Align = alRight
        BevelOuter = bvLowered
        DoubleBuffered = True
        ParentBackground = False
        ParentDoubleBuffered = False
        TabOrder = 1
        object Label12: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 145
          Height = 13
          Align = alTop
          Caption = #21151#33021#21517#31216
          ExplicitWidth = 48
        end
        object Label13: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 50
          Width = 145
          Height = 13
          Align = alTop
          Caption = #31995#32479#21151#33021
          ExplicitWidth = 48
        end
        object Label14: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 96
          Width = 145
          Height = 13
          Align = alTop
          Caption = #21151#33021#21442#25968
          ExplicitWidth = 48
        end
        object Label15: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 142
          Width = 145
          Height = 13
          Align = alTop
          Caption = #24110#21161#24207#21495
          ExplicitWidth = 48
        end
        object EUsingName: TEdit
          AlignWithMargins = True
          Left = 4
          Top = 23
          Width = 145
          Height = 21
          Align = alTop
          TabOrder = 0
        end
        object EUsing: TComboBox
          AlignWithMargins = True
          Left = 4
          Top = 69
          Width = 145
          Height = 21
          Align = alTop
          Style = csDropDownList
          Sorted = True
          TabOrder = 1
          OnChange = EUsingChange
        end
        object EParams: TComboBox
          AlignWithMargins = True
          Left = 4
          Top = 115
          Width = 145
          Height = 21
          Align = alTop
          TabOrder = 2
        end
        object btnRegeist: TBitBtn
          AlignWithMargins = True
          Left = 4
          Top = 189
          Width = 145
          Height = 25
          Align = alTop
          Caption = #27880#20876'(&R)'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555555FFFFFFFFFF55555000000000055555577777777775F55500B8B8B8B8
            B05555775F555555575F550F0B8B8B8B8B05557F75F555555575550BF0B8B8B8
            B8B0557F575FFFFFFFF7550FBF0000000000557F557777777777500BFBFBFBFB
            0555577F555555557F550B0FBFBFBFBF05557F7F555555FF75550F0BFBFBF000
            55557F75F555577755550BF0BFBF0B0555557F575FFF757F55550FB700007F05
            55557F557777557F55550BFBFBFBFB0555557F555555557F55550FBFBFBFBF05
            55557FFFFFFFFF7555550000000000555555777777777755555550FBFB055555
            5555575FFF755555555557000075555555555577775555555555}
          NumGlyphs = 2
          TabOrder = 3
          OnClick = btnRegeistClick
        end
        object btnUnregeist: TBitBtn
          AlignWithMargins = True
          Left = 4
          Top = 220
          Width = 145
          Height = 25
          Align = alTop
          Caption = #27880#38144'(&U)'
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333333333000033338833333333333333333F333333333333
            0000333911833333983333333388F333333F3333000033391118333911833333
            38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
            911118111118333338F3338F833338F3000033333911111111833333338F3338
            3333F8330000333333911111183333333338F333333F83330000333333311111
            8333333333338F3333383333000033333339111183333333333338F333833333
            00003333339111118333333333333833338F3333000033333911181118333333
            33338333338F333300003333911183911183333333383338F338F33300003333
            9118333911183333338F33838F338F33000033333913333391113333338FF833
            38F338F300003333333333333919333333388333338FFF830000333333333333
            3333333333333333333888330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
          TabOrder = 4
          OnClick = btnUnregeistClick
        end
        object EHelpID: TSpinEdit
          AlignWithMargins = True
          Left = 4
          Top = 161
          Width = 145
          Height = 22
          Align = alTop
          MaxValue = 0
          MinValue = 0
          TabOrder = 5
          Value = 0
        end
        object ToolBar1: TToolBar
          AlignWithMargins = True
          Left = 4
          Top = 251
          Width = 145
          Height = 24
          AutoSize = True
          ButtonHeight = 24
          ButtonWidth = 24
          Caption = 'ToolBar4'
          DrawingStyle = dsGradient
          Images = ImgButton
          TabOrder = 6
          object btnUsingTop: TToolButton
            Left = 0
            Top = 0
            Caption = 'btnManuTop'
            ImageIndex = 4
            OnClick = btnUsingTopClick
          end
          object btnUsingUp: TToolButton
            Left = 24
            Top = 0
            Caption = 'btnManuUp'
            ImageIndex = 5
            OnClick = btnUsingUpClick
          end
          object btnUsingDown: TToolButton
            Left = 48
            Top = 0
            Caption = 'btnManuDown'
            ImageIndex = 6
            OnClick = btnUsingDownClick
          end
          object btnUsingBottom: TToolButton
            Left = 72
            Top = 0
            Caption = 'btnManuBotom'
            ImageIndex = 7
            OnClick = btnUsingBottomClick
          end
        end
        object btnAutoOrder: TButton
          AlignWithMargins = True
          Left = 4
          Top = 281
          Width = 145
          Height = 25
          Align = alTop
          Caption = #33258#21160#25490#24207
          ImageIndex = 74
          ImageMargins.Left = 5
          TabOrder = 7
          OnClick = btnAutoOrderClick
        end
      end
      object EUsingHint: TStatusBar
        Left = 0
        Top = 699
        Width = 992
        Height = 19
        Panels = <
          item
            Width = 50
          end>
        ExplicitTop = 700
      end
    end
    object tsMenu: TTabSheet
      Caption = #33756#21333#31649#29702
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 24
      ExplicitWidth = 0
      ExplicitHeight = 719
      object Panel5: TPanel
        Left = 831
        Top = 0
        Width = 161
        Height = 718
        Align = alRight
        Caption = 'Panel5'
        ShowCaption = False
        TabOrder = 0
        ExplicitHeight = 719
        object Label2: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 50
          Width = 153
          Height = 13
          Align = alTop
          Caption = #33756#21333#21517#31216
          ExplicitWidth = 48
        end
        object Label3: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 96
          Width = 153
          Height = 13
          Align = alTop
          Caption = #23545#24212#21151#33021
          ExplicitWidth = 48
        end
        object Label9: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 153
          Height = 13
          Align = alTop
          Caption = #29238#32423#33756#21333
          ExplicitWidth = 48
        end
        object Label7: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 290
          Width = 153
          Height = 13
          Align = alTop
          Caption = #25171#24320#26041#24335
          ExplicitWidth = 48
        end
        object Label22: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 188
          Width = 153
          Height = 13
          Align = alTop
          Caption = #21151#33021#25552#31034
          ExplicitWidth = 48
        end
        object Label23: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 142
          Width = 153
          Height = 13
          Align = alTop
          Caption = #25191#34892#21442#25968
          ExplicitWidth = 48
        end
        object EMenuTask: TComboBox
          AlignWithMargins = True
          Left = 4
          Top = 115
          Width = 153
          Height = 21
          Align = alTop
          Style = csDropDownList
          TabOrder = 0
          OnChange = EMenuTaskChange
        end
        object EParentMenuName: TEdit
          AlignWithMargins = True
          Left = 4
          Top = 23
          Width = 153
          Height = 21
          Align = alTop
          TabOrder = 1
        end
        object EMenuName: TEdit
          AlignWithMargins = True
          Left = 4
          Top = 69
          Width = 153
          Height = 21
          Align = alTop
          TabOrder = 2
          Text = #31995#32479
        end
        object EMenuImage: TComboBoxEx
          Left = 1
          Top = 402
          Width = 159
          Height = 22
          Align = alTop
          ItemsEx = <>
          Style = csExDropDownList
          TabOrder = 3
          Images = FPlatformDB.ImgSmall
        end
        object rgMenuImgLS: TRadioGroup
          Left = 1
          Top = 358
          Width = 159
          Height = 44
          Align = alTop
          Caption = #28436#31034#22270#29255
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            #22823#22270
            #23567#22270)
          TabOrder = 4
          OnClick = rgMenuImgLSClick
        end
        object EMenuRunType: TComboBox
          AlignWithMargins = True
          Left = 4
          Top = 309
          Width = 153
          Height = 21
          Align = alTop
          Style = csDropDownList
          ItemIndex = 2
          TabOrder = 5
          Text = #30452#25509#25171#24320
          Items.Strings = (
            #20851#38381#25152#26377#21518#25171#24320
            #20851#38381#24403#21069#21518#25171#24320
            #30452#25509#25171#24320
            #23545#35805#26041#24335)
        end
        object ToolBar4: TToolBar
          Left = 1
          Top = 333
          Width = 159
          Height = 25
          Caption = 'ToolBar4'
          Images = ImgButton
          TabOrder = 6
          object btnManuTop: TToolButton
            Left = 0
            Top = 0
            Caption = 'btnManuTop'
            ImageIndex = 4
            OnClick = btnManuTopClick
          end
          object btnManuUp: TToolButton
            Left = 23
            Top = 0
            Caption = 'btnManuUp'
            ImageIndex = 5
            OnClick = btnManuUpClick
          end
          object btnManuDown: TToolButton
            Left = 46
            Top = 0
            Caption = 'btnManuDown'
            ImageIndex = 6
            OnClick = btnManuDownClick
          end
          object btnManuBotom: TToolButton
            Left = 69
            Top = 0
            Caption = 'btnManuBotom'
            ImageIndex = 7
            OnClick = btnManuBotomClick
          end
          object btnExport: TToolButton
            Left = 92
            Top = 0
            Caption = 'btnExport'
            ImageIndex = 8
            OnClick = btnExportClick
          end
        end
        object btnAddMenu: TBitBtn
          Left = 1
          Top = 642
          Width = 159
          Height = 25
          Align = alBottom
          Caption = #28155#21152'(&J)'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555555FFFFFFFFFF55555000000000055555577777777775F55500B8B8B8B8
            B05555775F555555575F550F0B8B8B8B8B05557F75F555555575550BF0B8B8B8
            B8B0557F575FFFFFFFF7550FBF0000000000557F557777777777500BFBFBFBFB
            0555577F555555557F550B0FBFBFBFBF05557F7F555555FF75550F0BFBFBF000
            55557F75F555577755550BF0BFBF0B0555557F575FFF757F55550FB700007F05
            55557F557777557F55550BFBFBFBFB0555557F555555557F55550FBFBFBFBF05
            55557FFFFFFFFF7555550000000000555555777777777755555550FBFB055555
            5555575FFF755555555557000075555555555577775555555555}
          NumGlyphs = 2
          TabOrder = 7
          OnClick = btnAddMenuClick
          ExplicitTop = 643
        end
        object btnEditMenu: TBitBtn
          Left = 1
          Top = 667
          Width = 159
          Height = 25
          Align = alBottom
          Caption = #20462#25913'(&T)'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
          TabOrder = 8
          OnClick = btnEditMenuClick
          ExplicitTop = 668
        end
        object btnDelMenu: TBitBtn
          Left = 1
          Top = 692
          Width = 159
          Height = 25
          Align = alBottom
          Caption = #21024#38500'(&L)'
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333333333000033338833333333333333333F333333333333
            0000333911833333983333333388F333333F3333000033391118333911833333
            38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
            911118111118333338F3338F833338F3000033333911111111833333338F3338
            3333F8330000333333911111183333333338F333333F83330000333333311111
            8333333333338F3333383333000033333339111183333333333338F333833333
            00003333339111118333333333333833338F3333000033333911181118333333
            33338333338F333300003333911183911183333333383338F338F33300003333
            9118333911183333338F33838F338F33000033333913333391113333338FF833
            38F338F300003333333333333919333333388333338FFF830000333333333333
            3333333333333333333888330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
          TabOrder = 9
          OnClick = btnDelMenuClick
          ExplicitTop = 693
        end
        object Panel1: TPanel
          Left = 1
          Top = 257
          Width = 159
          Height = 30
          Align = alTop
          BevelOuter = bvNone
          Caption = 'Panel1'
          ShowCaption = False
          TabOrder = 10
          object Label10: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 48
            Height = 13
            Align = alLeft
            Caption = #29305#27530#26631#35782
            Layout = tlCenter
          end
          object EMenuCtrlValue: TSpinEdit
            AlignWithMargins = True
            Left = 57
            Top = 3
            Width = 99
            Height = 24
            Align = alClient
            MaxValue = 0
            MinValue = 0
            TabOrder = 0
            Value = 0
          end
        end
        object EMenuHint: TEdit
          AlignWithMargins = True
          Left = 4
          Top = 207
          Width = 153
          Height = 21
          Align = alTop
          TabOrder = 11
        end
        object EMenuParams: TEdit
          AlignWithMargins = True
          Left = 4
          Top = 161
          Width = 153
          Height = 21
          Align = alTop
          TabOrder = 12
        end
        object Panel4: TPanel
          Left = 1
          Top = 231
          Width = 159
          Height = 26
          Align = alTop
          BevelOuter = bvNone
          Caption = 'Panel4'
          ShowCaption = False
          TabOrder = 13
          object Label4: TLabel
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 36
            Height = 20
            Align = alLeft
            Caption = #24555#25463#38190
            Layout = tlCenter
            ExplicitHeight = 13
          end
          object EMenuShortKey: TComboBox
            AlignWithMargins = True
            Left = 45
            Top = 3
            Width = 111
            Height = 21
            Align = alClient
            ItemIndex = 0
            TabOrder = 0
            Text = 'CTRL+A'
            Items.Strings = (
              'CTRL+A'
              'CTRL+B'
              'CTRL+C'
              'CTRL+D'
              'CTRL+E'
              'CTRL+F'
              'CTRL+G'
              'CTRL+H'
              'CTRL+I'
              'CTRL+G'
              'CTRL+K'
              'CTRL+L'
              'CTRL+M'
              'CTRL+N'
              'CTRL+O'
              'CTRL+P'
              'CTRL+Q'
              'CTRL+R'
              'CTRL+S'
              'CTRL+T'
              'CTRL+U'
              'CTRL+V'
              'CTRL+W'
              'CTRL+X'
              'CTRL+Y'
              'CTRL+Z'
              'ALT+A'
              'ALT+B'
              'ALT+C'
              'ALT+D'
              'ALT+E'
              'ALT+F'
              'ALT+G'
              'ALT+H'
              'ALT+I'
              'ALT+G'
              'ALT+K'
              'ALT+L'
              'ALT+M'
              'ALT+N'
              'ALT+O'
              'ALT+P'
              'ALT+Q'
              'ALT+R'
              'ALT+S'
              'ALT+T'
              'ALT+U'
              'ALT+V'
              'ALT+W'
              'ALT+X'
              'ALT+Y'
              'ALT+Z'
              'ALT+SHIFT+A'
              'ALT+SHIFT+B'
              'ALT+SHIFT+C'
              'ALT+SHIFT+D'
              'ALT+SHIFT+E'
              'ALT+SHIFT+F'
              'ALT+SHIFT+G'
              'ALT+SHIFT+H'
              'ALT+SHIFT+I'
              'ALT+SHIFT+G'
              'ALT+SHIFT+K'
              'ALT+SHIFT+L'
              'ALT+SHIFT+M'
              'ALT+SHIFT+N'
              'ALT+SHIFT+O'
              'ALT+SHIFT+P'
              'ALT+SHIFT+Q'
              'ALT+SHIFT+R'
              'ALT+SHIFT+S'
              'ALT+SHIFT+T'
              'ALT+SHIFT+U'
              'ALT+SHIFT+V'
              'ALT+SHIFT+W'
              'ALT+SHIFT+X'
              'ALT+SHIFT+Y'
              'ALT+SHIFT+Z')
          end
        end
      end
      object TreeMenu: TTreeView
        Left = 0
        Top = 0
        Width = 831
        Height = 718
        Align = alClient
        DragMode = dmAutomatic
        Images = FPlatformDB.ImgSmall
        Indent = 19
        TabOrder = 1
        OnChange = TreeMenuChange
        OnDragDrop = TreeMenuDragDrop
        OnDragOver = TreeMenuDragOver
        OnEdited = TreeMenuEdited
        ExplicitLeft = -2
        ExplicitTop = 1
        ExplicitHeight = 719
      end
    end
    object tsTask: TTabSheet
      Caption = #24555#25463#25353#38062
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 24
      ExplicitWidth = 0
      ExplicitHeight = 719
      object Panel3: TPanel
        Left = 855
        Top = 0
        Width = 137
        Height = 718
        Align = alRight
        Caption = 'Panel3'
        ParentShowHint = False
        ShowCaption = False
        ShowHint = False
        TabOrder = 0
        ExplicitHeight = 719
        object Label6: TLabel
          Left = 8
          Top = 48
          Width = 48
          Height = 13
          Caption = #23545#24212#21151#33021
        end
        object Label11: TLabel
          Left = 8
          Top = 88
          Width = 48
          Height = 13
          Caption = #29305#27530#26631#35782
        end
        object Label8: TLabel
          Left = 8
          Top = 221
          Width = 48
          Height = 13
          Caption = #25171#24320#26041#24335
        end
        object EBtnName: TLabeledEdit
          Left = 8
          Top = 19
          Width = 121
          Height = 21
          EditLabel.Width = 48
          EditLabel.Height = 13
          EditLabel.Caption = #21151#33021#21517#31216
          TabOrder = 0
        end
        object EBtnTask: TComboBox
          Left = 8
          Top = 65
          Width = 121
          Height = 21
          Style = csDropDownList
          TabOrder = 1
        end
        object EBtnCtrlValue: TSpinEdit
          Left = 8
          Top = 104
          Width = 121
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 2
          Value = 0
        end
        object EBtnHint: TLabeledEdit
          Left = 8
          Top = 149
          Width = 121
          Height = 21
          EditLabel.Width = 48
          EditLabel.Height = 13
          EditLabel.Caption = #21151#33021#25552#31034
          TabOrder = 3
        end
        object EBtnParam: TLabeledEdit
          Left = 8
          Top = 192
          Width = 121
          Height = 21
          EditLabel.Width = 48
          EditLabel.Height = 13
          EditLabel.Caption = #25191#34892#21442#25968
          TabOrder = 4
        end
        object rgTaskImgLS: TRadioGroup
          Left = 8
          Top = 267
          Width = 121
          Height = 44
          Caption = #28436#31034#22270#29255
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            #22823#22270
            #23567#22270)
          TabOrder = 5
          OnClick = rgTaskImgLSClick
        end
        object ETaskImage: TComboBoxEx
          Left = 8
          Top = 317
          Width = 121
          Height = 22
          ItemsEx = <>
          Style = csExDropDownList
          TabOrder = 6
          Images = FPlatformDB.ImgSmall
        end
        object EBtnRunType: TComboBox
          Left = 8
          Top = 238
          Width = 121
          Height = 21
          Style = csDropDownList
          ItemIndex = 2
          TabOrder = 7
          Text = #30452#25509#25171#24320
          Items.Strings = (
            #20851#38381#25152#26377#21518#25171#24320
            #20851#38381#24403#21069#21518#25171#24320
            #30452#25509#25171#24320
            #23545#35805#26041#24335)
        end
        object btnAddBtn: TBitBtn
          Left = 11
          Top = 419
          Width = 72
          Height = 25
          Caption = #28155#21152'(&A)'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
            0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
            0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
            33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
            B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
            3BB33773333773333773B333333B3333333B7333333733333337}
          NumGlyphs = 2
          TabOrder = 8
          OnClick = btnAddBtnClick
        end
        object btnEditBtn: TBitBtn
          Left = 11
          Top = 450
          Width = 72
          Height = 25
          Caption = #20462#25913'(&F)'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
          TabOrder = 9
          OnClick = btnEditBtnClick
        end
        object btnDelBtn: TBitBtn
          Left = 11
          Top = 481
          Width = 72
          Height = 25
          Caption = #21024#38500'(&D)'
          Glyph.Data = {
            DE010000424DDE01000000000000760000002800000024000000120000000100
            0400000000006801000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333333333000033338833333333333333333F333333333333
            0000333911833333983333333388F333333F3333000033391118333911833333
            38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
            911118111118333338F3338F833338F3000033333911111111833333338F3338
            3333F8330000333333911111183333333338F333333F83330000333333311111
            8333333333338F3333383333000033333339111183333333333338F333833333
            00003333339111118333333333333833338F3333000033333911181118333333
            33338333338F333300003333911183911183333333383338F338F33300003333
            9118333911183333338F33838F338F33000033333913333391113333338FF833
            38F338F300003333333333333919333333388333338FFF830000333333333333
            3333333333333333333888330000333333333333333333333333333333333333
            0000}
          NumGlyphs = 2
          TabOrder = 10
          OnClick = btnDelBtnClick
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 747
    Width = 1000
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 48
      Height = 13
      Align = alLeft
      Caption = #32593#26684#38388#36317
      Layout = tlCenter
    end
    object btnSave: TBitBtn
      AlignWithMargins = True
      Left = 168
      Top = 3
      Width = 75
      Height = 26
      Align = alLeft
      Caption = #20445#23384'(&S)'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333FFFFFFFFFFFFF33000077777770033377777777777773F000007888888
        00037F3337F3FF37F37F00000780088800037F3337F77F37F37F000007800888
        00037F3337F77FF7F37F00000788888800037F3337777777337F000000000000
        00037F3FFFFFFFFFFF7F00000000000000037F77777777777F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF00037F7F333333337F7F000FFFFFFFFF
        00037F7F333333337F7F000FFFFFFFFF07037F7F33333333777F000FFFFFFFFF
        0003737FFFFFFFFF7F7330099999999900333777777777777733}
      NumGlyphs = 2
      TabOrder = 0
      OnClick = btnSaveClick
    end
    object btnClose: TBitBtn
      AlignWithMargins = True
      Left = 330
      Top = 3
      Width = 75
      Height = 26
      Align = alLeft
      Caption = #36864#20986'(&C)'
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      ModalResult = 8
      NumGlyphs = 2
      TabOrder = 1
      OnClick = btnCloseClick
    end
    object btnRefrensh: TBitBtn
      AlignWithMargins = True
      Left = 249
      Top = 3
      Width = 75
      Height = 26
      Align = alLeft
      Caption = #21047#26032'(&R)'
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
        33333333333F8888883F33330000324334222222443333388F3833333388F333
        000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
        F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
        223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
        3338888300003AAAAAAA33333333333888888833333333330000333333333333
        333333333333333333FFFFFF000033333333333344444433FFFF333333888888
        00003A444333333A22222438888F333338F3333800003A2243333333A2222438
        F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
        22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
        33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
        3333333333338888883333330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      TabOrder = 2
    end
    object SpinEdit1: TSpinEdit
      AlignWithMargins = True
      Left = 57
      Top = 3
      Width = 105
      Height = 26
      Align = alLeft
      MaxValue = 0
      MinValue = 0
      TabOrder = 3
      Value = 15
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 272
    Top = 232
  end
  object FontDialog1: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Left = 208
    Top = 232
  end
  object ImgButton: TImageList
    Left = 96
    Top = 240
    Bitmap = {
      494C010108002300040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001002000000000000030
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000080000000800080808000000000000000000000000000000000000000
      00000000FF008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080008080
      8000808080008080800080808000808080008080800080808000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000080000000800000008000808080000000000000000000000000000000
      FF00000080000000800080808000000000000000000000000000000000000000
      0000000000008080800080808000808080008080800080808000808080008080
      800000000000000000000000000000000000000000000000000080808000C0C0
      C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C00080808000000000000000
      0000000000000000FF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      FF000000800000008000000080000000800080808000000000000000FF000000
      8000000080000000800000008000808080000000000000000000000000000000
      00000000000080808000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C00000000000000000000000000000000000000000000000000080808000C0C0
      C000C0C0C00080808000C0C0C00080808000C0C0C00080808000000000000000
      0000000000000000FF00000000000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000FFFFFF00000000000000000000000000FFFF
      FF000000000000000000FFFFFF00000000000000000000000000000000000000
      00000000FF000000800000008000000080000000800080808000000080000000
      8000000080000000800000008000808080000000000000000000000000000000
      00000000000080808000C0C0C0000000000000000000C0C0C000C0C0C000C0C0
      C00000000000000000000000000000000000000000000000000080808000C0C0
      C00080808000C0C0C00080808000C0C0C000C0C0C00080808000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000FF0000008000000080000000800000008000000080000000
      8000000080000000800080808000000000000000000000000000000000000000
      00000000000080808000C0C0C0000000000000000000C0C0C000C0C0C000C0C0
      C00000000000000000000000000000000000000000000000000080808000C0C0
      C000C0C0C000C0C0C000C0C0C000808080008080800080808000808080008080
      80008080800080808000808080000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      000000000000000000000000FF00000080000000800000008000000080000000
      8000000080008080800000000000000000000000000000000000000000000000
      00000000000080808000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C00000000000000000000000000000000000000000000000000080808000C0C0
      C00000800000008000000080000080808000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00808080000000000000000000FFFFFF00FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFFFF00FFFFFF0000000000FFFFFF000000
      FF00FFFFFF00FF000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000080000000800000008000000080000000
      8000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000080808000C0C0
      C00000800000C0C0C000C0C0C00080808000FFFFFF00FFFFFF0000000000FFFF
      FF0000000000FFFFFF00808080000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000000
      FF00FFFFFF00FF000000FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000FF000000800000008000000080000000
      8000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000080808000C0C0
      C00000800000008000000080000080808000FFFFFF0000000000FFFFFF000000
      0000FFFFFF00FFFFFF00808080000000000000000000FFFFFF00000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FF000000FFFFFF00000000000000000000000000000000000000
      000000000000000000000000FF00000080000000800000008000000080000000
      800080808000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000000000000000000080808000C0C0
      C000C0C0C000C0C0C000C0C0C00080808000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00808080000000000000000000FFFFFF00000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000FF0000008000000080000000800080808000000080000000
      800000008000808080000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000808080008080
      800080808000808080008080800080808000FFFFFF00FF000000FF000000FF00
      0000FF000000FFFFFF00808080000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000FF0000008000000080000000800080808000000000000000FF000000
      800000008000000080008080800000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000080808000FFFFFF00FF0000000000FF000000
      FF00FF000000FFFFFF00808080000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000000000000000000000000000000000000000
      00000000FF000000800000008000808080000000000000000000000000000000
      FF0000008000000080000000800080808000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      FF0000000000000000000000000080808000FFFFFF00FF000000FF000000FF00
      0000FF000000FFFFFF00808080000000000000000000C0C0C000C0C0C000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000C0C0C000C0C0C000000000000000000000000000000000000000
      0000000000000000FF0000008000000000000000000000000000000000000000
      00000000FF00000080000000800000008000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      FF0000000000000000000000000080808000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF00000080000000FF00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000008080800000000000000000000000000000000000000000000000
      00000000FF000000FF0000000000808080008080800080808000808080008080
      8000808080008080800080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF0000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FC7FFC7FFEFFF01FFC7FFC7FFEFFFEFF
      FC7FFC7FFC7FFC7FFC7FFC7FFC7FFC7FFC7FFC7FF83FF83FFC7FFC7FF83FF83F
      E00FE00FF01FF01FE00FE00FF01FF01FF01FF01FE00FE00FF01FF01FE00FE00F
      F83FF83FFC7FFC7FF83FF83FFC7FFC7FFC7FFC7FFC7FFC7FFC7FFC7FFC7FFC7F
      FEFFFEFFFC7FFC7FF01FFEFFFC7FFC7FFFFFFFFFE1F3FFFFC0270000E0E18003
      C03B0000E0400001C03B0000F0000001C03F0000F8010001C0010000FC030001
      C0010000FE070001C0010000FE070001C0010000FC070001C0010000F8030001
      C0010000F0410001FE010000F0E00001EE010000F9F00001EE010000FFF80001
      F201FFFFFFFF0001FFFFFFFFFFFF800300000000000000000000000000000000
      000000000000}
  end
  object SaveDialog1: TSaveDialog
    Left = 336
    Top = 232
  end
  object ClientDataSet1: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'Name'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'Value'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <>
    Params = <>
    ProviderName = 'DataSetProvider1'
    StoreDefs = True
    Left = 95
    Top = 140
  end
  object DataSource1: TDataSource
    DataSet = ClientDataSet1
    Left = 95
    Top = 188
  end
  object ColorDialog1: TColorDialog
    Left = 404
    Top = 234
  end
end
