(* *****************************************************************************
* UVonMenuDesigner v3.0 written by James Von (jamesvon@163.com) 2/2 2015
********************************************************************************
* TFVonMenuDesigner                                                            *
*------------------------------------------------------------------------------*
*     TFVonMenuDesigner 是一个支持动态建立菜单及任务按钮的一个设计软件，同时支 *
* 持创建样式参数，生成样式文件目录。
*==============================================================================*
* 辅助类
*------------------------------------------------------------------------------*
* TEventCheckRight 检查权限事件函数类
* TTaskMenuInfo    菜单项目类信息
* TTaskMenu        菜单项目类
* TTaskBtnInfo     任务按钮信息类
* TTaskBtn         任务按钮类
*===============================================================================
* 部分信息存储内容说明
*------------------------------------------------------------------------------*
* 功能信息 <UsingName>|<CommandName>|<Param>|<HelpID>
* 菜单信息 <MenuName><MemuParentName><ImageID><UsingName><Controller><ShortKey><RunType><Params><Hint>
* 按钮信息 <TaskCaption><TaskImage><UsingName><Controller><Left><Top><RunType><Params><Hint>
***************************************************************************** *)
unit UVonMenuDesigner;

interface

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, jpeg, ExtCtrls, StdCtrls, StrUtils, Spin, FileCtrl,
  UVonClass, ComCtrls, Menus, Contnrs, WinApi.CommCtrl, ToolWin, ImgList, IniFiles,
  UVonSystemFuns, UVonLog, UVonMenuInfo, UPlatformDockForm, System.ImageList,
  Vcl.Grids, Data.DB, Vcl.DBGrids, Datasnap.DBClient, MidasLib;

resourcestring
  RES_DESIGN_DELTASK = 'Task %d will delete.';
  RES_DESIGN_DELMENU = 'Menu %s will delete.';

type
  TFVonMenuDesigner = class(TForm)
    OpenDialog1: TOpenDialog;
    FontDialog1: TFontDialog;
    ImgButton: TImageList;
    SaveDialog1: TSaveDialog;
    pcSettings: TPageControl;
    tsUsing: TTabSheet;
    lstUsing: TListBox;
    PlUsing: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    EUsingName: TEdit;
    EUsing: TComboBox;
    EParams: TComboBox;
    btnRegeist: TBitBtn;
    btnUnregeist: TBitBtn;
    EHelpID: TSpinEdit;
    tsMenu: TTabSheet;
    tsTask: TTabSheet;
    Panel2: TPanel;
    Label1: TLabel;
    btnSave: TBitBtn;
    btnClose: TBitBtn;
    btnRefrensh: TBitBtn;
    SpinEdit1: TSpinEdit;
    Panel3: TPanel;
    Label6: TLabel;
    Label11: TLabel;
    Label8: TLabel;
    EBtnName: TLabeledEdit;
    EBtnTask: TComboBox;
    EBtnCtrlValue: TSpinEdit;
    EBtnHint: TLabeledEdit;
    EBtnParam: TLabeledEdit;
    rgTaskImgLS: TRadioGroup;
    ETaskImage: TComboBoxEx;
    EBtnRunType: TComboBox;
    btnAddBtn: TBitBtn;
    btnEditBtn: TBitBtn;
    btnDelBtn: TBitBtn;
    Panel5: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label9: TLabel;
    EMenuTask: TComboBox;
    EParentMenuName: TEdit;
    EMenuName: TEdit;
    EMenuImage: TComboBoxEx;
    rgMenuImgLS: TRadioGroup;
    Label7: TLabel;
    EMenuRunType: TComboBox;
    ToolBar4: TToolBar;
    btnManuTop: TToolButton;
    btnManuUp: TToolButton;
    btnManuDown: TToolButton;
    btnManuBotom: TToolButton;
    btnAddMenu: TBitBtn;
    btnEditMenu: TBitBtn;
    btnDelMenu: TBitBtn;
    TreeMenu: TTreeView;
    ToolBar1: TToolBar;
    btnUsingTop: TToolButton;
    btnUsingUp: TToolButton;
    btnUsingDown: TToolButton;
    btnUsingBottom: TToolButton;
    btnExport: TToolButton;
    btnAutoOrder: TButton;
    EUsingHint: TStatusBar;
    ClientDataSet1: TClientDataSet;
    DataSource1: TDataSource;
    ColorDialog1: TColorDialog;
    Panel1: TPanel;
    Label10: TLabel;
    EMenuCtrlValue: TSpinEdit;
    EMenuHint: TEdit;
    Label22: TLabel;
    Label23: TLabel;
    EMenuParams: TEdit;
    Panel4: TPanel;
    Label4: TLabel;
    EMenuShortKey: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnAddMenuClick(Sender: TObject);
    procedure btnEditMenuClick(Sender: TObject);
    procedure btnDelMenuClick(Sender: TObject);
    procedure btnAddBtnClick(Sender: TObject);
    procedure btnEditBtnClick(Sender: TObject);
    procedure btnDelBtnClick(Sender: TObject);
    procedure EUsingChange(Sender: TObject);
    procedure btnRegeistClick(Sender: TObject);
    procedure btnUnregeistClick(Sender: TObject);
    procedure lstUsingDblClick(Sender: TObject);
    procedure EMenuTaskChange(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure rgMenuImgLSClick(Sender: TObject);
    procedure rgTaskImgLSClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnManuUpClick(Sender: TObject);
    procedure btnManuBotomClick(Sender: TObject);
    procedure btnManuTopClick(Sender: TObject);
    procedure btnManuDownClick(Sender: TObject);
    procedure btnExportClick(Sender: TObject);
    procedure btnImportClick(Sender: TObject);
    procedure TreeMenuChange(Sender: TObject; Node: TTreeNode);
    procedure TreeMenuEdited(Sender: TObject; Node: TTreeNode; var S: string);
    procedure TreeMenuDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure TreeMenuDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure btnUsingTopClick(Sender: TObject);
    procedure btnUsingUpClick(Sender: TObject);
    procedure btnUsingDownClick(Sender: TObject);
    procedure btnUsingBottomClick(Sender: TObject);
    procedure btnAutoOrderClick(Sender: TObject);
    procedure tsStyleShow(Sender: TObject);
    procedure pcSettingsResize(Sender: TObject);
  private
    { Private declarations }
    FCurrentBtn: TBitBtn;
    FMenuSetting: TVonArraySetting;
    FTTaskBtn: TTaskBtn;
    FOnCheckRight: TEventCheckRight;
    FModified: Boolean;
    procedure MoveBtn(Sender: TObject; Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer); // TMouseEvent
    procedure btnClick(Sender: TObject);
    procedure MenuClick(Sender: TObject);
    procedure SetOnCheckRight(const Value: TEventCheckRight);
    procedure DisplayMenu(PName: string; PNode: TTreeNode; MenuValues: TVonArraySetting);
  public
    { Public declarations }
    procedure Init;
    property OnCheckRight: TEventCheckRight read FOnCheckRight write SetOnCheckRight;
  end;

var
  FVonMenuDesigner: TFVonMenuDesigner;
  FOnApplyStyle: TNotifyEvent;

implementation

uses UPlatformInfo, UFrameBarBase, UPlatformDB, ListActns;

{$R *.dfm}

{ TFVonMenuDesigner }

procedure TFVonMenuDesigner.FormCreate(Sender: TObject);
begin // 加载设计参数
  FMenuSetting:= TVonArraySetting.Create;
  FTTaskBtn := TTaskBtn.Create;
  FTTaskBtn.IsDesigner := True;
  FTTaskBtn.OnTaskClick := btnClick;
  FTTaskBtn.OnMove := MoveBtn;
  FTTaskBtn.ImgList := FPlatformDB.ImgLarge;
  TreeMenu.Images:= FPlatformDB.ImgSmall;
  Init;
end;

procedure TFVonMenuDesigner.FormDestroy(Sender: TObject);
begin
  FMenuSetting.Free;
end;

procedure TFVonMenuDesigner.btnSaveClick(Sender: TObject);
var
  fs: TFileStream;
  ms: TMemoryStream;
  vInt: RVonInt;
  procedure SaveNode(ANode: TTreeNode);
  var
    Idx: Integer;
    szPName: string;
  begin
    Idx := FMenuSetting.IndexOfValue(0, StripHotkey(ANode.Text));
    if Assigned(ANode.Parent) then szPName:= ANode.Parent.Text
    else szPName:= '';
    FAppMenuList.AppendRow([FMenuSetting.Values[Idx, 0], szPName,
      FMenuSetting.Values[Idx, 2], FMenuSetting.Values[Idx, 3],
      FMenuSetting.Values[Idx, 4], FMenuSetting.Values[Idx, 5],
      FMenuSetting.Values[Idx, 6], FMenuSetting.Values[Idx, 7],
      FMenuSetting.Values[Idx, 8]]);
    if Assigned(ANode.getFirstChild()) then
      SaveNode(ANode.getFirstChild());
    if Assigned(ANode.getNextSibling()) then
      SaveNode(ANode.getNextSibling());
  end;
begin
  FAppMenuList.Clear;
  SaveNode(TreeMenu.Items[0]);
  fs := TFileStream.Create(ExtractFilePath(Application.ExeName) + MENU_FILE,
    fmCreate);
  ms := TMemoryStream.Create;
  vInt.Int := FAppUsingList.SaveToStream(ms);
  ms.Position := 0;
  fs.Write(vInt.Bytes[0], 4);
  fs.CopyFrom(ms, vInt.Int);
  ms.Size := 0;
  vInt.Int := FAppMenuList.SaveToStream(ms);
  ms.Position := 0;
  fs.Write(vInt.Bytes[0], 4);
  fs.CopyFrom(ms, vInt.Int);
  ms.Size := 0;
  vInt.Int := FAppTaskList.SaveToStream(ms);
  ms.Position := 0;
  fs.Write(vInt.Bytes[0], 4);
  fs.CopyFrom(ms, vInt.Int);
  ms.Free;
  fs.Free;
end;

procedure TFVonMenuDesigner.Init;
var
  i: Integer;
  sch: TSearchrec;
begin
  FMenuSetting.Text:= FAppMenuList.Text;
  DisplayMenu('', nil, FMenuSetting);
  FTTaskBtn.Display(tsTask, FAppTaskList);
  // 设置应用窗口列表<WindowName><ExecParam><UsingNote>
  EUsing.Clear;
  for i := 0 to FAppCommandList.Count - 1 do
    EUsing.Items.Add(FAppCommandList[i]);
  EUsing.ItemIndex := 0;
  // 加载功能列表<UsingName>|<WindowName>|<Param>
  lstUsing.Clear;
  for i := 0 to FAppUsingList.Count - 1 do
    lstUsing.Items.Add(FAppUsingList.Values[i, 0]);
  EBtnTask.Items.Assign(lstUsing.Items);
  EBtnTask.Items.InsertObject(0, '退出命令', TObject(-4));
  EBtnTask.Items.InsertObject(0, '链接网站', TObject(-3));
  EBtnTask.Items.InsertObject(0, '关于命令', TObject(-3));
  EBtnTask.Items.InsertObject(0, '帮助命令', TObject(-3));
  EBtnTask.Items.InsertObject(0, '更改口令', TObject(-2));
  EBtnTask.Items.InsertObject(0, '无', TObject(-1));
  EMenuTask.Items.Assign(EBtnTask.Items);
end;

procedure TFVonMenuDesigner.SetOnCheckRight(const Value: TEventCheckRight);
begin // 设置权限检查属性
  FOnCheckRight := Value;
end;

procedure TFVonMenuDesigner.btnManuBotomClick(Sender: TObject);
var
  szNode: TTreeNode;
begin
  if not Assigned(TreeMenu.Selected) then
    Exit;
  szNode := TreeMenu.Selected.getNextSibling;
  if not Assigned(szNode) then
    Exit;
  TreeMenu.Selected.MoveTo(szNode, naAdd);
  FModified := True;
end;

procedure TFVonMenuDesigner.btnManuDownClick(Sender: TObject);
var
  szNode1, szNode2: TTreeNode;
begin
  if not Assigned(TreeMenu.Selected) then
    Exit;
  szNode1 := TreeMenu.Selected.getNextSibling;
  if not Assigned(szNode1) then
    Exit;
  szNode2 := szNode1.getNextSibling;
  if not Assigned(szNode2) then begin
    TreeMenu.Selected.MoveTo(szNode1, naAdd);
  end else begin
    TreeMenu.Selected.MoveTo(szNode2, naInsert);
  end;
  FModified := True;
end;

procedure TFVonMenuDesigner.btnManuTopClick(Sender: TObject);
var
  szNode: TTreeNode;
begin
  if not Assigned(TreeMenu.Selected) then
    Exit;
  szNode := TreeMenu.Selected.getPrevSibling();
  if not Assigned(szNode) then
    Exit;
  TreeMenu.Selected.MoveTo(szNode, naAddFirst);
  FModified := True;
end;

procedure TFVonMenuDesigner.btnManuUpClick(Sender: TObject);
var
  szNode: TTreeNode;
begin
  if not Assigned(TreeMenu.Selected) then
    Exit;
  if TreeMenu.Selected.Index = 0 then
    Exit;
  szNode := TreeMenu.Selected.getPrevSibling();
  TreeMenu.Selected.MoveTo(szNode, naInsert);
  FModified := True;
end;

{ TFFaceDesigner.Usings }

procedure TFVonMenuDesigner.EUsingChange(Sender: TObject);
var
  Idx: Integer;
begin // 提取任务所拥有的默认参数
  if EUsingName.Text = '' then
    EUsingName.Text := EUsing.Text;
  EParams.Items.Delimiter := ',';
  Idx := FAppCommandList.IndexOf(EUsing.Text);
  if Idx < 0 then
    raise Exception.Create('该功能已经被注销，请重新选择。');
  EParams.Items.DelimitedText := TAppCmdInfo(FAppCommandList.Objects[Idx])
    .UsingParams;
  EUsingHint.Panels[0].Text:= TAppCmdInfo(FAppCommandList.Objects[Idx])
    .UsingInfo;
end;

procedure TFVonMenuDesigner.lstUsingDblClick(Sender: TObject);
begin // 提取功能信息 <UsingName>|<CommandName>|<Param>|<HelpID>
  EUsingName.Text := FAppUsingList.Values[lstUsing.ItemIndex, 0];
  EUsing.ItemIndex := EUsing.Items.IndexOf
    (FAppUsingList.Values[lstUsing.ItemIndex, 1]);
  EUsingChange(nil);
  EParams.Text := FAppUsingList.Values[lstUsing.ItemIndex, 2];
  EHelpID.Value := StrToInt(FAppUsingList.Values[lstUsing.ItemIndex, 3]);
end;

procedure TFVonMenuDesigner.btnRegeistClick(Sender: TObject);
var
  Idx: Integer;
begin // 注册功能   功能信息 <UsingName>|<CommandName>|<Param>|<HelpID>
  Idx := lstUsing.Items.IndexOf(EUsingName.Text);
  if Idx >= 0 then
  begin
    Idx := FAppUsingList.IndexOfValue(0, EUsingName.Text);
    FAppUsingList.Values[Idx, 1] := EUsing.Text;
    FAppUsingList.Values[Idx, 2] := EParams.Text;
    FAppUsingList.Values[Idx, 3] := IntToStr(EHelpID.Value);
  end
  else
  begin
    FAppUsingList.AppendRow([EUsingName.Text, EUsing.Text, EParams.Text,
      IntToStr(EHelpID.Value)]);
    lstUsing.Items.Add(EUsingName.Text);
    EBtnTask.Items.Add(EUsingName.Text);
    EMenuTask.Items.Add(EUsingName.Text);
  end;
  EUsingName.Text := '';
end;

procedure TFVonMenuDesigner.btnUnregeistClick(Sender: TObject);
begin // 注销功能
  if lstUsing.ItemIndex < 0 then
    raise Exception.Create('尚未提取或选择功能节点。');
  if lstUsing.Items[lstUsing.ItemIndex] <> EUsingName.Text then
    raise Exception.Create('当前选择的节点与设定内容不一致，不允许注销。');
  FAppUsingList.Delete(lstUsing.ItemIndex);
  lstUsing.DeleteSelected;
  EBtnTask.Items.Delete(EBtnTask.Items.IndexOf(EUsingName.Text));
  EMenuTask.Items.Delete(EMenuTask.Items.IndexOf(EUsingName.Text));
end;

procedure TFVonMenuDesigner.btnUsingBottomClick(Sender: TObject);
var
  Idx: Integer;
begin
  Idx := lstUsing.ItemIndex;
  if Idx = lstUsing.Count - 1 then Exit;
  lstUsing.Items.Move(Idx, lstUsing.Count - 1);
  FAppUsingList.Move(Idx, lstUsing.Count - 1);
  lstUsing.ItemIndex:= lstUsing.Count - 1;
end;

procedure TFVonMenuDesigner.btnUsingDownClick(Sender: TObject);
var
  Idx: Integer;
begin
  Idx := lstUsing.ItemIndex;
  if Idx = lstUsing.Count - 1 then Exit;
  lstUsing.Items.Move(Idx, Idx + 1);
  FAppUsingList.Move(Idx, Idx + 1);
  lstUsing.ItemIndex:= Idx + 1;
end;

procedure TFVonMenuDesigner.btnUsingTopClick(Sender: TObject);
var
  Idx: Integer;
begin
  Idx := lstUsing.ItemIndex;
  if Idx = 0 then Exit;
  lstUsing.Items.Move(Idx, 0);
  FAppUsingList.Move(Idx, 0);
  lstUsing.ItemIndex:= 0;
end;

procedure TFVonMenuDesigner.btnUsingUpClick(Sender: TObject);
var
  Idx: Integer;
begin
  Idx := lstUsing.ItemIndex;
  if Idx <= 0 then Exit;
  lstUsing.Items.Move(Idx, Idx - 1);
  FAppUsingList.Move(Idx, Idx - 1);
  lstUsing.ItemIndex:= Idx - 1;
end;

procedure TFVonMenuDesigner.btnAutoOrderClick(Sender: TObject);
var
  I, Idx: Integer;
  function WhereToMove: Integer;
  begin
    Result:= I;
    while(Result > 0)and(lstUsing.Items[Result - 1] > lstUsing.Items[I + 1])do
      Dec(Result);
  end;
begin
  I := 0;
  while I < lstUsing.Items.Count - 1 do begin
    if lstUsing.Items[I] > lstUsing.Items[I + 1] then begin
      Idx:= WhereToMove;
      lstUsing.Items.Move(I + 1, Idx);
      FAppUsingList.Move(I + 1, Idx);
    end;
    Inc(I);
  end;
end;

{ TFFaceDesigner.Menu }

procedure TFVonMenuDesigner.EMenuTaskChange(Sender: TObject);
begin
  if EMenuName.Text = '' then
    EMenuName.Text := EMenuTask.Text;
end;

procedure SetImg(ImgList: TImageList; Dest: TComboBoxEx);
var
  i: Integer;
begin
  Dest.ItemsEx.Clear;
  Dest.Images := ImgList;
  for i := 0 to ImgList.Count - 1 do
    Dest.ItemsEx.AddItem(IntToStr(i + 1), i, i, i, 0, nil);
//  Dest.ItemsEx.BeginUpdate;
//  Dest.ItemsEx.EndUpdate;
end;

procedure TFVonMenuDesigner.rgMenuImgLSClick(Sender: TObject);
begin
  case rgMenuImgLS.ItemIndex of
    0:
      SetImg(FPlatformDB.ImgLarge, EMenuImage);
    1:
      SetImg(FPlatformDB.imgSmall, EMenuImage);
  end;
end;

procedure TFVonMenuDesigner.rgTaskImgLSClick(Sender: TObject);
begin
  case rgTaskImgLS.ItemIndex of
    0:
      SetImg(FPlatformDB.ImgLarge, ETaskImage);
    1:
      SetImg(FPlatformDB.imgSmall, ETaskImage);
  end;
end;

procedure TFVonMenuDesigner.btnAddMenuClick(Sender: TObject);
var
  Idx: Integer;
  newNode: TTreeNode;
begin // 添加菜单
  EMenuName.Text := StripHotkey(Trim(EMenuName.Text));
  if EMenuName.Text = '' then
    raise Exception.Create('菜单名称不能为空。');
  Idx := FMenuSetting.IndexOfValue(0, EMenuName.Text);
  if Idx >= 0 then
    raise Exception.Create('该菜单已经存在，请更换菜单名称。');
  EParentMenuName.Text := Trim(EParentMenuName.Text);
  if EParentMenuName.Text <> '' then
  begin
    Idx := FMenuSetting.IndexOfValue(0, EParentMenuName.Text);
    if Idx < 0 then
      raise Exception.Create('该菜单的父级菜单不存在，请更换父级菜单名称。');
  end;
  // <MenuName><MemuParentName><ImageID><UsingName><Controller><ShortKey><RunType><Params><Hint>
  FMenuSetting.AppendRow([EMenuName.Text, EParentMenuName.Text,
    IntToStr(EMenuImage.ItemIndex), EMenuTask.Text,
    IntToStr(EMenuCtrlValue.Value), EMenuShortKey.Text,
    IntToStr(EMenuRunType.ItemIndex), EMenuParams.Text, EMenuHint.Text]);
  if EParentMenuName.Text = '' then
    newNode:= TreeMenu.Items.AddChild(nil, EMenuName.Text)
  else for Idx := 0 to TreeMenu.Items.Count - 1 do
    if SameText(TreeMenu.Items[Idx].Text, EParentMenuName.Text) then begin
      newNode:= TreeMenu.Items.AddChild(TreeMenu.Items[Idx], EMenuName.Text);
      Break;
    end;
  newNode.ImageIndex:= EMenuImage.ItemIndex;
  newNode.SelectedIndex:= newNode.ImageIndex;
  EMenuName.Text := '';
  FModified := True;
end;

procedure TFVonMenuDesigner.btnEditMenuClick(Sender: TObject);
var
  I, Idx: Integer;
begin // 修改菜单
  if not Assigned(TreeMenu.Selected) then
    raise Exception.Create('尚未选中菜单项，无法进行修改。');
  EMenuName.Text := Trim(EMenuName.Text);
  if EMenuName.Text = '' then
    raise Exception.Create('菜单名称不能为空。');
  Idx := FMenuSetting.IndexOfValue(0, TreeMenu.Selected.Text);
  if (not SameText(TreeMenu.Selected.Text, EMenuName.Text)) and
    (FMenuSetting.IndexOfValue(0, EMenuName.Text) >= 0) then
    raise Exception.Create('该菜单已经存在，不能进行修改。');
  if Assigned(TreeMenu.Selected.Parent) then
    EParentMenuName.Text := StripHotkey(TreeMenu.Selected.Parent.Text)
  else
    EParentMenuName.Text := '';
  // <MenuName><MemuParentName><ImageID><UsingName><Controller><ShortKey><RunType><Params><Hint>
  if TreeMenu.Selected.Level = 0 then begin
    EParentMenuName.Text:= '';
    for I:= 0 to FMenuSetting.Count - 1 do
      if FMenuSetting.Values[I, 1] = TreeMenu.Selected.Text then
        FMenuSetting.Values[I, 1]:= EMenuName.Text;
  end;
  FMenuSetting.UpdateRow(Idx, [EMenuName.Text, EParentMenuName.Text,
    IntToStr(EMenuImage.ItemIndex), EMenuTask.Text,
    IntToStr(EMenuCtrlValue.Value), EMenuShortKey.Text,
    IntToStr(EMenuRunType.ItemIndex), EMenuParams.Text, EMenuHint.Text]);
  TreeMenu.Selected.Text:= EMenuName.Text;
  EMenuName.Text := '';
  FModified := True;
end;

procedure TFVonMenuDesigner.btnExportClick(Sender: TObject);
begin
  FMenuSetting.SaveToFile(ExtractFilePath(Application.ExeName) + 'TMP.DAT');
end;

procedure TFVonMenuDesigner.btnImportClick(Sender: TObject);
begin
  FMenuSetting.LoadFromFile(ExtractFilePath(Application.ExeName) + 'TMP.DAT');
end;

procedure TFVonMenuDesigner.btnDelMenuClick(Sender: TObject);
var // 删除一个菜单项目
  Idx: Integer;
begin
  if not Assigned(TreeMenu.Selected) then
    Exit;
  EMenuName.Text := Trim(EMenuName.Text);
  if not SameText(TreeMenu.Selected.Text, EMenuName.Text) then
    raise Exception.Create('必须先提取,然后才可以删除！');
  if TreeMenu.Selected.Count > 0 then
    raise Exception.Create('该菜单含有子菜单不能删除！');
  if MessageDlg('真的要删除这个菜单吗?', mtInformation, [mbOK, mbCancel], 0) <> mrOK then
    Exit;
  if MessageDlg('一旦删除该菜单，系统将无法恢复，是否确认?', mtInformation, [mbOK, mbCancel], 0)
    <> mrOK then
    Exit;
  Idx := FMenuSetting.IndexOfValue(0, TreeMenu.Selected.Text);
  FMenuSetting.Delete(Idx);
  WriteLog(LOG_MAJOR, 'DESIGN_DelMenu', Format(RES_DESIGN_DELMENU,
    [TreeMenu.Selected.Text]));
  TreeMenu.Selected.Delete;
end;

procedure TFVonMenuDesigner.MenuClick(Sender: TObject);
begin
end;

procedure TFVonMenuDesigner.DisplayMenu(PName: string; PNode: TTreeNode; MenuValues: TVonArraySetting);
var
  i: Integer;
  ANode: TTreeNode;
begin
  // 菜单信息 <MenuName><MemuParentName><ImageID><UsingName><Controller><ShortKey><RunType><Params><Hint>
  for i := 0 to MenuValues.Count - 1 do
    if SameText(MenuValues[i, 1], PName) then begin
      ANode:= TreeMenu.Items.AddChild(PNode, MenuValues[i, 0]);
      ANode.ImageIndex:= StrToInt(MenuValues[i, 2]);
      ANode.SelectedIndex:= ANode.ImageIndex;
      DisplayMenu(MenuValues[i, 0], ANode, MenuValues);
    end;
end;

procedure TFVonMenuDesigner.TreeMenuChange(Sender: TObject; Node: TTreeNode);
var
  Idx: Integer;
begin     // 点击菜单相当于提取该菜单信息
  if not Assigned(Node) then Exit;
  with Node do
  begin
    // <MenuName>,<MemuParentName>,<ImageID>,<UsingName>,<Controller>,<ShortKey><Params><Hint>
    Idx := FMenuSetting.IndexOfValue(0, StripHotkey(Text));
    EParentMenuName.Text := FMenuSetting.Values[Idx, 1];
    EMenuName.Text := FMenuSetting.Values[Idx, 0];
    EMenuImage.ItemIndex := StrToInt(FMenuSetting.Values[Idx, 2]);
    EMenuTask.ItemIndex := EMenuTask.Items.IndexOf
      (FMenuSetting.Values[Idx, 3]);
    EMenuCtrlValue.Value := StrToInt(FMenuSetting.Values[Idx, 4]);
    EMenuShortKey.Text := FMenuSetting.Values[Idx, 5];
    EMenuRunType.ItemIndex := StrToInt(FMenuSetting.Values[Idx, 6]);
    EMenuParams.Text := FMenuSetting.Values[Idx, 7];
    EMenuHint.Text := FMenuSetting.Values[Idx, 8];
    pcSettings.ActivePageIndex := 1;
  end;
end;

procedure TFVonMenuDesigner.TreeMenuDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  sNode, dNode, aNode: TTreeNode;
begin
  sNode:= (Source as TTreeView).Selected;
  if not Assigned(sNode)then Exit;
  dNode:= (Sender as TTreeView).GetNodeAt(X, Y);
  if not Assigned(dNode)then Exit;
  case sNode.Level of
  0: case dNode.Level of
    0: sNode.MoveTo(dNode, naInsert);
    1: if Assigned(dNode.Parent.getNextSibling()) then
        sNode.MoveTo(dNode.Parent.getNextSibling(), naInsert)
      else sNode.MoveTo(dNode.Parent, naAdd);
    end;
  1: case dNode.Level of
    0: sNode.MoveTo(dNode, naAddChild);
    1: sNode.MoveTo(dNode, naInsert);
    end;
  end;
end;

procedure TFVonMenuDesigner.TreeMenuDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin     //

end;

procedure TFVonMenuDesigner.TreeMenuEdited(Sender: TObject; Node: TTreeNode; var S: string);
var
  Idx: Integer;
begin
  EMenuName.Text:= S;
  Idx := FMenuSetting.IndexOfValue(0, StripHotkey(Node.Text));
  FMenuSetting.Values[Idx, 0]:= S;
end;

procedure TFVonMenuDesigner.tsStyleShow(Sender: TObject);
begin
  if ClientDataSet1.Active then Exit;
  //创建演示临时表
  with ClientDataSet1 do begin
    FieldDefs.Clear;   //创建字段名表
    with FieldDefs.AddFieldDef do begin Name := 'ID'; Size := 0; DataType := ftInteger; end;
    with FieldDefs.AddFieldDef do begin Name := 'Name'; Size := 10; DataType := ftString; end;
    with FieldDefs.AddFieldDef do begin Name := 'Value'; Size := 10; DataType := ftString; end;
    CreateDataSet;     //动态创建数据集
    Append; Fields[0].AsInteger:= 1; Fields[1].AsString:= 'James'; Fields[2].AsString:= 'Von'; Post;
    Append; Fields[0].AsInteger:= 2; Fields[1].AsString:= 'Harry'; Fields[2].AsString:= 'ben'; Post;
    Append; Fields[0].AsInteger:= 3; Fields[1].AsString:= 'Daniel'; Fields[2].AsString:= 'Adam'; Post;
    Append; Fields[0].AsInteger:= 4; Fields[1].AsString:= 'David'; Fields[2].AsString:= 'Alva'; Post;
    Append; Fields[0].AsInteger:= 5; Fields[1].AsString:= 'Edgar'; Fields[2].AsString:= 'Andy'; Post;
    Append; Fields[0].AsInteger:= 6; Fields[1].AsString:= 'George'; Fields[2].AsString:= 'Bob'; Post;
    Append; Fields[0].AsInteger:= 7; Fields[1].AsString:= 'Henry'; Fields[2].AsString:= 'Bill'; Post;
    Open;              //激活和打开该数据集
  end;
end;

{ TFFaceDesigner.Task }

procedure TFVonMenuDesigner.MoveBtn(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var // 任务按钮拖动
  SC_MANIPULATE: Word;
  Idx: Integer;
begin
  SC_MANIPULATE := $F009;
  ReleaseCapture;
  with Sender as TBitBtn do
  begin
    Perform(WM_SYSCOMMAND, SC_MANIPULATE, 0);
    Left := Left div SpinEdit1.Value * SpinEdit1.Value;
    Top := Top div SpinEdit1.Value * SpinEdit1.Value;
    // <TaskCaption>|<TaskImage>|<TaskTaskID>|<IsSpecail>|<Left>|<Top>|<ShortKey>|<Params>
    Idx := FTTaskBtn.FCertTask.IndexOfValue(0, Caption);
    FTTaskBtn.FCertTask.Values[Idx, 4] := IntToStr(Left);
    FTTaskBtn.FCertTask.Values[Idx, 5] := IntToStr(Top);
  end;
end;

procedure TFVonMenuDesigner.pcSettingsResize(Sender: TObject);
begin
  lstUsing.Columns:= lstUsing.Width div (ABS(lstUsing.Font.Height) * 12);
end;

procedure TFVonMenuDesigner.btnClick(Sender: TObject);
var // 点击任务相当于提取该任务信息
  Idx: Integer;
begin
  // 按钮信息 <TaskCaption><TaskImage><UsingName><Controller><Left><Top><Params><Hint>
  FCurrentBtn := TBitBtn(Sender);
  with FCurrentBtn do
  begin
    Idx := FTTaskBtn.FCertTask.IndexOfValue(0, Caption);
    EBtnName.Text := Caption;
    ETaskImage.ItemIndex := StrToInt(FTTaskBtn.FCertTask.Values[Idx, 1]);
    EBtnTask.ItemIndex := EBtnTask.Items.IndexOf
      (FTTaskBtn.FCertTask.Values[Idx, 2]);
    EBtnCtrlValue.Value := StrToInt(FTTaskBtn.FCertTask.Values[Idx, 3]);
    EBtnRunType.ItemIndex := StrToInt(FTTaskBtn.FCertTask.Values[Idx, 6]);
    EBtnParam.Text := FTTaskBtn.FCertTask.Values[Idx, 7];
    EBtnHint.Text := FTTaskBtn.FCertTask.Values[Idx, 8];
    pcSettings.ActivePageIndex := 2;
  end;
end;

procedure TFVonMenuDesigner.btnCloseClick(Sender: TObject);
begin
  if FModified then
    if Assigned(FOnApplyStyle) then FOnApplyStyle(self);
  Close;
end;

procedure TFVonMenuDesigner.btnAddBtnClick(Sender: TObject);
var
  Idx: Integer;
begin // 添加任务按钮
  EBtnName.Text := Trim(EBtnName.Text);
  if EBtnName.Text = '' then
    raise Exception.Create('任务按钮名称不能为空。');
  Idx := FTTaskBtn.FCertTask.IndexOfValue(0, EBtnName.Text);
  if Idx >= 0 then
    raise Exception.Create('该任务按钮已经存在，请更换任务按钮名称。');
  // <TaskCaption><TaskImage><UsingName><Controller><Left><Top><RunType><Params><Hint>
  FTTaskBtn.FCertTask.AppendRow([EBtnName.Text, IntToStr(ETaskImage.ItemIndex),
    EBtnTask.Text, IntToStr(EBtnCtrlValue.Value), '300', '300',
    IntToStr(EBtnRunType.ItemIndex), EBtnParam.Text, EBtnHint.Text]);
  FTTaskBtn.AddTask(FTTaskBtn.FCertTask.Count - 1, True);
  EBtnName.Text := '';
  FModified := True;
end;

procedure TFVonMenuDesigner.btnEditBtnClick(Sender: TObject);
var // 修改任务内容
  Idx: Integer;
begin
  EBtnName.Text := Trim(EBtnName.Text);
  if Trim(EBtnName.Text) = '' then
    Exit;
  if FCurrentBtn = nil then
    Exit;
  Idx := FTTaskBtn.FCertTask.IndexOfValue(0, EBtnName.Text);
  if FCurrentBtn.Caption <> EBtnName.Text then
  begin
    if FTTaskBtn.FCertTask.IndexOfValue(0, EBtnName.Text) >= 0 then
      raise Exception.Create('该任务已经存在，请核实后再进行修改！');
  end;
  // <TaskCaption><TaskImage><UsingName><Controller><Left><Top><RunType><Params><Hint>
  FTTaskBtn.FCertTask.UpdateRow(Idx,
    [EBtnName.Text, IntToStr(ETaskImage.ItemIndex), EBtnTask.Text,
    IntToStr(EBtnCtrlValue.Value), IntToStr(FCurrentBtn.Left),
    IntToStr(FCurrentBtn.Top), IntToStr(EBtnRunType.ItemIndex), EBtnParam.Text,
    EBtnHint.Text]);
  FCurrentBtn.Caption := EBtnName.Text;
  FCurrentBtn.ImageIndex := ETaskImage.ItemIndex;
  FCurrentBtn.Hint := EBtnHint.Text;
  FModified := True;
end;

procedure TFVonMenuDesigner.btnDelBtnClick(Sender: TObject);
var // 删除一个任务项目
  Idx: Integer;
begin
  EBtnName.Text := Trim(EBtnName.Text);
  if FCurrentBtn = nil then
    Exit;
  if FCurrentBtn.Caption <> EBtnName.Text then
    raise Exception.Create('必须先提示提取后才可以删除！');
  if MessageDlg('真的要删除这个任务吗?', mtInformation, [mbOK, mbCancel], 0) <> mrOK then
    Exit;
  if MessageDlg('一旦删除该任务，系统将无法恢复，是否确认?', mtInformation, [mbOK, mbCancel], 0)
    <> mrOK then
    Exit;
  Idx := FTTaskBtn.FCertTask.IndexOfValue(0, EBtnName.Text);
  FTTaskBtn.FCertTask.Delete(Idx);
  FCurrentBtn.Free;
  WriteLog(LOG_MAJOR, 'DESIGN_DelTask', Format(RES_DESIGN_DELMENU,
    [FCurrentBtn.Caption]));
end;

end.

