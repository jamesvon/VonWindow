unit UVonMenuInfo;

interface

uses
  winapi.Windows, winapi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, jpeg, ExtCtrls, StdCtrls, StrUtils, Spin, FileCtrl,
  UVonClass, ComCtrls, Menus, Contnrs, winapi.CommCtrl, ToolWin, ImgList, IniFiles;

type

  /// <summary>检查权限事件函数类</summary>
  /// <param name="SpecailValue">特殊功能值</param>
  /// <param name="Using">功能名称</param>
  TEventCheckRight = function(SpecailValue, UsingIdx: Integer)
    : boolean of object;

  /// <summary>菜单项目类</summary>
  TTaskMenu = class
  private
    FMenu: TMenu;
    FOnMenuClose: TNotifyEvent;
    FOnMenuChangePWD: TNotifyEvent;
    FOnMenuClick: TNotifyEvent;
    FOnMenuHelp: TNotifyEvent;
    FIsDesigner: boolean;
    FOnCheckRight: TEventCheckRight;
    procedure SetOnMenuChangePWD(const Value: TNotifyEvent);
    procedure SetOnMenuClick(const Value: TNotifyEvent);
    procedure SetOnMenuClose(const Value: TNotifyEvent);
    procedure SetOnMenuHelp(const Value: TNotifyEvent);
    procedure SetIsDesigner(const Value: boolean);
    procedure SetOnCheckRight(const Value: TEventCheckRight);
    function CompMenu(AMenuItem: TMenuItem; Idx: Integer): boolean;
  public
    FCertMenu: TVonArraySetting;
    procedure Display(AMenu: TMenu; CertMenu: TVonArraySetting);
    procedure AddMenu(Idx: Integer; Checked: boolean = true);
    procedure SetMenu(Idx: Integer; MenuItem: TMenuItem);
  published
    property IsDesigner: boolean read FIsDesigner write SetIsDesigner;
    property OnMenuClose: TNotifyEvent read FOnMenuClose write SetOnMenuClose;
    property OnMenuChangePWD: TNotifyEvent read FOnMenuChangePWD
      write SetOnMenuChangePWD;
    property OnMenuHelp: TNotifyEvent read FOnMenuHelp write SetOnMenuHelp;
    property OnMenuClick: TNotifyEvent read FOnMenuClick write SetOnMenuClick;
    property OnCheckRight: TEventCheckRight read FOnCheckRight
      write SetOnCheckRight;
  end;

  /// <summary>任务按钮类</summary>
  TTaskBtn = class
  private
    FCurrentBtn: TBitBtn;
    FTaskForm: TWinControl;
    FOnTaskClick: TNotifyEvent;
    FOnDragOver: TDragOverEvent;
    FOnEndDrag: TEndDragEvent;
    FOnMove: TMouseEvent;
    FIsDesigner: boolean;
    FOnTaskClose: TNotifyEvent;
    FOnTaskChangePWD: TNotifyEvent;
    FOnTaskHelp: TNotifyEvent;
    FOnCheckRight: TEventCheckRight;
    FImgList: TImageList;
    procedure SetOnTaskClick(const Value: TNotifyEvent);
    procedure SetOnMove(const Value: TMouseEvent);
    procedure SetIsDesigner(const Value: boolean);
    procedure SetOnTaskChangePWD(const Value: TNotifyEvent);
    procedure SetOnTaskClose(const Value: TNotifyEvent);
    procedure SetOnTaskHelp(const Value: TNotifyEvent);
    procedure SetOnCheckRight(const Value: TEventCheckRight);
    procedure SetImgList(const Value: TImageList);
  public
    FCertTask: TVonArraySetting;
    procedure Display(AForm: TWinControl; CertTask: TVonArraySetting);
    procedure AddTask(Idx: Integer; Checked: boolean = true);
    procedure AddBtn(Idx: Integer; Checked: boolean = true);
  published
    property IsDesigner: boolean read FIsDesigner write SetIsDesigner;
    property OnTaskClick: TNotifyEvent read FOnTaskClick write SetOnTaskClick;
    property OnMove: TMouseEvent read FOnMove write SetOnMove;
    property OnDragOver: TDragOverEvent read FOnDragOver write FOnDragOver;
    property OnEndDrag: TEndDragEvent read FOnEndDrag write FOnEndDrag;
    property OnTaskClose: TNotifyEvent read FOnTaskClose write SetOnTaskClose;
    property OnTaskChangePWD: TNotifyEvent read FOnTaskChangePWD
      write SetOnTaskChangePWD;
    property OnTaskHelp: TNotifyEvent read FOnTaskHelp write SetOnTaskHelp;
    property OnCheckRight: TEventCheckRight read FOnCheckRight
      write SetOnCheckRight;
    property ImgList: TImageList read FImgList write SetImgList;
  end;

implementation

uses UPlatformDB;

{ TTaskBtn }

procedure TTaskBtn.AddBtn(Idx: Integer; Checked: boolean = true);
var
  NewItem: TBitBtn;
  cmdIdx: Integer;
  szCommand: string;
begin // <TaskCaption><TaskImage><UsingName><Controller><Left><Top><RunType><Params><Hint>
  cmdIdx := FAppUsingList.IndexOfValue(0, FCertTask.Values[Idx, 2]);
  if (not IsDesigner) and (cmdIdx >= 0) and Assigned(FOnCheckRight) and
    (not FOnCheckRight(StrToInt(FCertTask.Values[Idx, 3]), cmdIdx)) then
    Exit; // 权限不足
  NewItem := TBitBtn.Create(FTaskForm);
  NewItem.Caption := FCertTask.Values[Idx, 0]; // <MenuName>
  NewItem.DesignInfo := cmdIdx; // <TaskID>
  NewItem.Left := StrToInt(FCertTask.Values[Idx, 4]); // <Left>
  NewItem.Top := StrToInt(FCertTask.Values[Idx, 5]); // <Top>
  NewItem.Parent := FTaskForm;
  NewItem.Layout := blGlyphTop;
  NewItem.Height := 96;
  NewItem.Width := 96;
  NewItem.OnMouseDown := FOnMove;
  NewItem.Hint := FCertTask.Values[Idx, 7];
  NewItem.OnEnter := FOnTaskClick; // 权限控制
  if cmdIdx < ImgList.Count then
    ImgList.GetBitmap(StrToInt(FAppTaskList[Idx, 1]), NewItem.Glyph); // <ImageID>
end;

procedure TTaskBtn.AddTask(Idx: Integer; Checked: boolean = true);
var
  NewItem: TSpeedButton;
  cmdIdx: Integer;
begin // <TaskCaption><TaskImage><UsingName><Controller><Left><Top><Params><Hint>
  cmdIdx := FAppUsingList.IndexOfValue(0, FCertTask.Values[Idx, 2]);
  if (not IsDesigner) and (cmdIdx >= 0) and Assigned(FOnCheckRight) and
    (not FOnCheckRight(StrToInt(FCertTask.Values[Idx, 3]), cmdIdx)) then
    Exit; // 权限不足
  NewItem := TSpeedButton.Create(FTaskForm);
  NewItem.Caption := FCertTask.Values[Idx, 0]; // <MenuName>
  NewItem.DesignInfo := Idx; // <TaskID>
  NewItem.Left := StrToInt(FCertTask.Values[Idx, 4]); // <Left>
  NewItem.Top := StrToInt(FCertTask.Values[Idx, 5]); // <Top>
  NewItem.Parent := FTaskForm;
  NewItem.Layout := blGlyphTop;
  NewItem.Height := 96;
  NewItem.Width := 96;
  NewItem.OnMouseDown := FOnMove;
  NewItem.Hint := FCertTask.Values[Idx, 7];
  NewItem.OnClick := FOnTaskClick; // 添加相应事件
  cmdIdx := StrToInt(FCertTask.Values[Idx, 1]);
  if cmdIdx < ImgList.Count then
    ImgList.GetBitmap(StrToInt(FAppTaskList[Idx, 1]), NewItem.Glyph); // <ImageID>
end;

procedure TTaskBtn.Display(AForm: TWinControl; CertTask: TVonArraySetting);
var
  i: Integer;
begin
  FTaskForm := AForm;
  FCertTask := CertTask;
  for i := AForm.ControlCount - 1 downto 0 do
    if (IsDesigner and (AForm.Controls[i].ClassName = 'TBitBtn')) or
      (IsDesigner and (AForm.Controls[i].ClassName = 'TSpeedButton')) then
      AForm.Controls[i].Free;
  for i := 0 to FCertTask.Count - 1 do
  begin
    if IsDesigner then
      AddBtn(i, true)
    else
      AddTask(i, true);
  end;
end;

procedure TTaskBtn.SetImgList(const Value: TImageList);
begin
  FImgList := Value;
end;

procedure TTaskBtn.SetIsDesigner(const Value: boolean);
begin
  FIsDesigner := Value;
end;

procedure TTaskBtn.SetOnCheckRight(const Value: TEventCheckRight);
begin
  FOnCheckRight := Value;
end;

procedure TTaskBtn.SetOnMove(const Value: TMouseEvent);
begin
  FOnMove := Value;
end;

procedure TTaskBtn.SetOnTaskChangePWD(const Value: TNotifyEvent);
begin
  FOnTaskChangePWD := Value;
end;

procedure TTaskBtn.SetOnTaskClick(const Value: TNotifyEvent);
begin
  FOnTaskClick := Value;
end;

procedure TTaskBtn.SetOnTaskClose(const Value: TNotifyEvent);
begin
  FOnTaskClose := Value;
end;

procedure TTaskBtn.SetOnTaskHelp(const Value: TNotifyEvent);
begin
  FOnTaskHelp := Value;
end;

{ TTaskMenu }

procedure TTaskMenu.AddMenu(Idx: Integer; Checked: boolean = true);
var // 添加一个菜单  Checked: 有效性检查标志
  NewItem: TMenuItem;
begin // <MenuName><MemuParentName><ImageID><UsingName><Controller><ShortKey><Params><Hint>
  if (not IsDesigner) and Assigned(FOnCheckRight) and
    (not FOnCheckRight(StrToInt(FCertMenu.Values[Idx, 4]),
    FAppUsingList.IndexOfValue(0, FCertMenu.Values[Idx, 3]))) then
    Exit; // 权限不足
  NewItem := TMenuItem.Create(FMenu);
  if FCertMenu.Values[Idx, 1] <> '' then
    with FMenu.Items.Find(FCertMenu.Values[Idx, 1]) do
    begin
      Add(NewItem);
    end
  else
    FMenu.Items.Add(NewItem);
  SetMenu(Idx, NewItem);
  FCertMenu.Values[Idx, 0] := NewItem.Caption;
  if (NewItem.Enabled) and (NewItem.Parent <> nil) then
  begin
    NewItem.Parent.Visible := true;
    NewItem.Parent.Enabled := true;
  end;
end;

procedure TTaskMenu.SetMenu(Idx: Integer; MenuItem: TMenuItem);
begin // <MenuName><MemuParentName><ImageID><UsingName><Controller><ShortKey><Params><Hint>
  MenuItem.Caption := FCertMenu.Values[Idx, 0]; // <MenuName>
  MenuItem.DesignInfo := Idx; // <TaskID>
  MenuItem.ImageIndex := StrToInt(FCertMenu.Values[Idx, 2]); // <ImageID>
  MenuItem.ShortCut := TextToShortCut(FCertMenu.Values[Idx, 5]); // <ShortKey>
  MenuItem.Hint := FCertMenu.Values[Idx, 7]; // <Hint>
  MenuItem.OnClick := FOnMenuClick;
end;

function TTaskMenu.CompMenu(AMenuItem: TMenuItem; Idx: Integer): boolean;
var
  mPos: Integer;
  S: string;
begin
  S := AMenuItem.Caption;
  mPos := Pos('(&', S);
  if mPos > 0 then
    Delete(S, mPos, 4);
  Result := SameText(S, FCertMenu.Values[Idx, 0]);
end;

procedure TTaskMenu.Display(AMenu: TMenu; CertMenu: TVonArraySetting);
var
  i: Integer;
begin // 根据配置文件展示菜单
  FCertMenu := CertMenu;
  FMenu := AMenu;
  AMenu.Items.Clear;
  for i := 0 to FCertMenu.Count - 1 do
  begin
    AddMenu(i, false);
  end;
end;

procedure TTaskMenu.SetIsDesigner(const Value: boolean);
begin
  FIsDesigner := Value;
end;

procedure TTaskMenu.SetOnCheckRight(const Value: TEventCheckRight);
begin
  FOnCheckRight := Value;
end;

procedure TTaskMenu.SetOnMenuChangePWD(const Value: TNotifyEvent);
begin
  FOnMenuChangePWD := Value;
end;

procedure TTaskMenu.SetOnMenuClick(const Value: TNotifyEvent);
begin
  FOnMenuClick := Value;
end;

procedure TTaskMenu.SetOnMenuClose(const Value: TNotifyEvent);
begin
  FOnMenuClose := Value;
end;

procedure TTaskMenu.SetOnMenuHelp(const Value: TNotifyEvent);
begin
  FOnMenuHelp := Value;
end;

end.
