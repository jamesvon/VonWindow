(*******************************************************************************
* UVonTransmissionApp v2.0 written by James Von (jamesvon@163.com) *************
********************************************************************************
* 通讯命令集说明
*===============================================================================
*  CMD=ADDAPP 添加应用
*         PARAMS=<appName>
*         --> ERECEIVED_RESULT
*  CMD=DELAPP 删除应用
*         PARAMS=<appName>
*         --> ERECEIVED_RESULT
*  CMD=LSTAPP 得到应用列表
*         PARAMS=
*         --> ERECEIVED_RESULT,<AppList>
*  CMD=GETVER 得到当前版本号
*         PARAMS=
*         --> ERECEIVED_RESULT,<version>
*  CMD=CHKVER 检查版本号，放回版本号列表
*         PARAMS=<version>
*         --> ERECEIVED_RESULT,<versionList>
*  CMD=DELVER 删除其中一个版本号
*         PARAMS=<VerName>
*         --> ERECEIVED_RESULT
*  CMD=NEWVER 添加一个新版本
*         PARAMS=<kind><varName><version><realfilename> DATA=<filestream>
*         --> ERECEIVED_RESULT
*  CMD=DOWVER 下载版本文件
*         PARAMS=<VerName>
*         --> ERECEIVED_RESULT,<kind><version><realfilename>,<filestream>
********************************************************************************
* TVonApplicationVersionServer = class(TVonApplicationServerBase) 应用服务器管理
*===============================================================================
*  CMD=ADDAPP PARAMS=<appName>
********************************************************************************
*  TVonAppClient = class(TVonApplicationClient)                   应用客户端管理
*===============================================================================
*  CMD=ADDAPP PARAMS=<appName>
*******************************************************************************)
unit UVonTransmissionApp;

interface

uses
  SysUtils, Classes, Variants, UVonSystemFuns, UVonLog, UVonClass, System.ZLib,
  UVonTransmissionBase;

type
{--$IFDEF TRANS_SERVER}
{$region '服务器段落'}

  TVonApplicationVersionServer = class(TVonApplicationServerBase)
  private
  public
    /// <summary>向服务器注册本服务模块的功能</summary>
    procedure RegistMethod; override;
    /// <summary>添加一个应用</summary>
    /// <remarks>CMD=ADDAPP PARAMS=&lt;appName&gt; --&gt; ERECEIVED_RESULT</remarks>
    procedure AddApplication(clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
    /// <summary>删除一个应用</summary>
    /// <remarks>CMD=DELAPP PARAMS=&lt;appName&gt; --&gt; ERECEIVED_RESULT</remarks>
    procedure DelApplication(clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
    /// <summary>得到应用列表</summary>
    /// <remarks>CMD=LSTAPP PARAMS= --&gt; ERECEIVED_RESULT,&lt;AppList&gt;</remarks>
    procedure ListApplication(clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
    /// <summary>得到应用最新版本</summary>
    /// <remarks>CMD=CMD=GETVER PARAMS= --&gt; ERECEIVED_RESULT,&lt;version&gt;</remarks>
    procedure GetVersion(clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
    /// <summary>检查应用版本，得到文件列表</summary>
    /// <remarks>CMD=CHKVER PARAMS=&lt;version&gt; --&gt; ERECEIVED_RESULT,&lt;versionList&gt;</remarks>
    procedure CheckVersion(clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
    /// <summary>检查应用版本，得到文件列表</summary>
    /// <remarks>CMD=DELVER PARAMS=&lt;VerName&gt; --&gt; ERECEIVED_RESULT</remarks>
    procedure DeleteVersion(clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
    /// <summary>设置服务器文件</summary>
    /// <remarks>CMD=NEWVER PARAMS=&lt;kind&gt;&lt;verName&gt;&lt;version&gt;&lt;realfilename&gt; DATA=&lt;filestream&gt; --&gt; ERECEIVED_RESULT</remarks>
    procedure UploadVersion(clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
    /// <summary>提取服务器文件</summary>
    /// <remarks>CMD=DOWVER PARAMS=&lt;VerName&gt; --&gt; ERECEIVED_RESULT,&lt;kind&gt;&lt;version&gt;&lt;realfilename&gt;,&lt;filestream&gt;</remarks>
    procedure DownVersion(clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
    procedure DownFile(clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
  end;

{$endregion '服务器段落'}
{--$ELSE}
{$region '客户端段落'}

  TVonAppClient = class(TVonApplicationClientBase)
  public
    Files : TStringList;
    constructor Create(TransmissionType, ApplicationName: string); override;
    destructor Destroy; override;
    /// <summary>添加一个应用</summary>
    /// <remarks>CMD=ADDAPP PARAMS=&lt;appName&gt; --&gt; ERECEIVED_RESULT</remarks>
    procedure AddApplication(AppName: string);
    /// <summary>删除一个应用</summary>
    /// <param name="AppName: string">应用名称</param>
    /// <remarks>CMD=DELAPP PARAMS=&lt;appName&gt; --&gt; ERECEIVED_RESULT</remarks>
    procedure DelApplication(AppName: string);
    /// <summary>得到应用列表</summary>
    /// <remarks>CMD=LSTAPP PARAMS= --&gt; ERECEIVED_RESULT,&lt;AppList&gt;</remarks>
    function ListApplication(): Integer;
    /// <summary>得到应用最新版本</summary>
    /// <returns>当前应用版本</returns>
    /// <remarks>CMD=CMD=GETVER PARAMS= --&gt; ERECEIVED_RESULT,&lt;version&gt;</remarks>
    function GetVer: string;
    /// <summary>检查应用版本，得到文件列表</summary>
    /// <param name="AVer: string">当前系统版本</param>
    /// <remarks>CMD=CHKVER PARAMS=&lt;version&gt; --&gt; ERECEIVED_RESULT,&lt;versionList&gt;</remarks>
    function CheckVer(AVer: string): Integer;
    /// <summary>删除服务器文件</summary>
    /// <param name="srcFilename: string">服务器版本名称</param>
    /// <remarks>CMD=DELVER PARAMS=&lt;VerName&gt; --&gt; ERECEIVED_RESULT</remarks>
    procedure DelVerFile(srcFilename: string);
    /// <summary>设置服务器文件</summary>
    /// <param name="AFilename: string">文件名</param>
    /// <param name="AKind: string">版本类型，U:替换文件，E:执行文件</param>
    /// <param name="VersionName: string">版本名称</param>
    /// <param name="AVersion: string">版本号</param>
    /// <param name="ARealName: string">实际文件</param>
    /// <remarks>CMD=NEWVER PARAMS=&lt;kind&gt;&lt;verName&gt;&lt;version&gt;&lt;realfilename&gt; DATA=&lt;filestream&gt; --&gt; ERECEIVED_RESULT</remarks>
    procedure SetVerFile(AFilename, AKind, VersionName, AVersion, ARealName: string);
    /// <summary>提取服务器文件</summary>
    /// <param name="srcFilename: string">服务器文件名</param>
    /// <param name="destFilename: string">另存为本地文件名</param>
    /// <remarks>CMD=DOWVER PARAMS=&lt;VerName&gt; --&gt; ERECEIVED_RESULT,&lt;kind&gt;&lt;version&gt;&lt;realfilename&gt;,&lt;filestream&gt;</remarks>
    procedure GetVerFile(VersionName, destFilename: string);
  end;
{$endregion '客户端段落'}
{--$ENDIF}

implementation

{--$IFDEF TRANS_SERVER}
{$region '服务器段落'}

{ TVonApplicationVersionServer }

procedure TVonApplicationVersionServer.RegistMethod;
begin
  inherited;
  FServer.AddMethod('ADDAPP', AddApplication);
  FServer.AddMethod('DELAPP', DelApplication);
  FServer.AddMethod('LSTAPP', ListApplication);
  FServer.AddMethod('GETVER', GetVersion);
  FServer.AddMethod('CHKVER', CheckVersion);
  FServer.AddMethod('DELVER', DeleteVersion);
  FServer.AddMethod('NEWVER', UploadVersion);
  FServer.AddMethod('DOWVER', DownVersion);
  FServer.AddMethod('DOWFIL', DownFile);
end;

procedure TVonApplicationVersionServer.ListApplication(
  clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
begin  // CMD=LSTAPP PARAMS=  --> ERECEIVED_RESULT,<AppList>
  WriteLog(LOG_INFO, 'ListApplication', Format('Found %d applications to send.', [FServer.AppCount]));
  if FServer.AppCount > 0 then begin
    FServer.AppListSaveToStream(serverMsg.DataStream);
    serverMsg.ReturnValue:= E_RETURN;
  end else serverMsg.ReturnValue:= E_SUCCESS;
end;

procedure TVonApplicationVersionServer.AddApplication(
  clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
var
  appName: string;
begin    // CMD=ADDAPP PARAMS=<appName> --> ERECEIVED_RESULT
  appName:= Copy(clientMsg.Params[0] + '________', 1, 8);
  if FServer.AppNames[appName] = nil then begin
    CreateDir(FServer.ServiceWorkFolder + appName);
    FServer.AddApplication(TVonTransmissionApp.Create(appName,
      FServer.ServiceWorkFolder + appName + '\'));
  end;
  WriteLog(LOG_MAJOR, 'AddApplication', Format('Add a application %s', [appName]));
  serverMsg.ReturnValue:= E_SUCCESS;
end;

procedure TVonApplicationVersionServer.DelApplication(
  clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
var
  appName: string;
begin    // CMD=DELAPP PARAMS=<appName> --> ERECEIVED_RESULT
  appName:= Copy(clientMsg.Params[0] + '________', 1, 8);
  FServer.RemoveAppllication(appName);
  serverMsg.ReturnValue:= E_SUCCESS;
  WriteLog(LOG_MAJOR, 'DelApplication', Format('Remove the %s application', [appName]));
end;

procedure TVonApplicationVersionServer.GetVersion(
  clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
var
  app: TVonTransmissionApp;
begin     //CMD=GETVER PARAMS=<appName> --> ERECEIVED_RESULT,<version>
  WriteLog(LOG_MAJOR, 'GetVersion', Format('Get version from %s application', [clientMsg.AppName]));
  app:= FServer.AppNames[clientMsg.AppName];
  if not Assigned(app) then begin
    serverMsg.ReturnValue:= E_FAILD;
    serverMsg.ErrorInfo:= 'Can not found application.';
  end else begin
    WriteStringToStream(app.CurrentVersion.Text, serverMsg.DataStream);
    serverMsg.ReturnValue:= E_RETURN;
  end;
end;

procedure TVonApplicationVersionServer.DeleteVersion(
  clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
var
  app: TVonTransmissionApp;
begin     //CMD=DELVER PARAMS=<VerName> --> ERECEIVED_RESULT
  app:= FServer.AppNames[clientMsg.AppName];
  if not Assigned(app) then begin
    serverMsg.ReturnValue:= E_FAILD;
    serverMsg.ErrorInfo:= 'Can not found application.';
  end else begin
    app.DelVersion(clientMsg.Params[0]);
    serverMsg.ReturnValue:= E_SUCCESS;
  end;
end;

procedure TVonApplicationVersionServer.CheckVersion(
  clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
var
  app: TVonTransmissionApp;
  I: Integer;
  AppVer, lastVer: TVonVersion;
begin     //CMD=CHKVER PARAMS=<appName>,<version> --> ERECEIVED_RESULT,<versionList>
  app:= FServer.AppNames[clientMsg.AppName];
  if not Assigned(app) then begin
    serverMsg.ReturnValue:= E_FAILD;
    serverMsg.ErrorInfo:= 'Can not found application.';
  end else begin
    AppVer.Text:= clientMsg.Params[0];
    lastVer:= AppVer;
    clientMsg.Params.Clear;
    for I := 0 to app.VersionFileCount - 1 do
      if AppVer.Check(app.VersionData[I].Version) < 0 then
        clientMsg.Params.Values[app.SysFileFlag[I]]:= app.VersionData[I].Text;
    if clientMsg.Params.Count > 0 then begin
      clientMsg.Params.SaveToStream(serverMsg.DataStream);
      serverMsg.ReturnValue:= E_RETURN;
    end else serverMsg.ReturnValue:= E_SUCCESS;
  end;
end;

procedure TVonApplicationVersionServer.UploadVersion(
  clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
var
  app: TVonTransmissionApp;
  fs: TFileStream;
  AVer: TVonTransmissionVersion;
begin    //CMD=NEWVER PARAMS=<kind><vername><version><realfilename> DATA=<filestream> --> ERECEIVED_RESULT
  WriteLog(LOG_DEBUG, 'UploadVersion', Format('Receive a file,App=%s,Kind=%s,SysFilename=%s,Version=%s,RealFilename=%s',
    [clientMsg.AppName, clientMsg.Params[0], clientMsg.Params[1], clientMsg.Params[2], clientMsg.Params[3]]));
  app:= FServer.AppNames[clientMsg.AppName];
  if not Assigned(app) then begin
    serverMsg.ReturnValue:= E_FAILD;
    serverMsg.ErrorInfo:= 'Can not found application.';
    WriteLog(LOG_DEBUG, 'UploadVersion', Format('No found %s', [clientMsg.AppName]));
  end else begin
    AVer:= TVonTransmissionVersion.Create;
    AVer.Kind:= clientMsg.Params[0];
    AVer.SysFilename:= clientMsg.Params[1];
    AVer.Version.Text:= clientMsg.Params[2];
    AVer.RealFilename:= clientMsg.Params[3];
    fs:= TFileStream.Create(app.WorkFolder + AVer.SysFilename, fmCreate);
    fs.CopyFrom(clientMsg.DataStream, clientMsg.DataStream.Size);
    fs.Free;
  WriteLog(LOG_DEBUG, 'UploadVersion', Format('App will add a file, cfg=%s', [AVer.Text]));
    app.AddVersion(AVer);
    FServer.AppListSaveToFile(FServer.ServiceWorkFolder + 'VonService.CFG');
    serverMsg.ReturnValue:= E_SUCCESS;
  end;
end;

procedure TVonApplicationVersionServer.DownFile(
  clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
var
  fs: TFileStream;
  app: TVonTransmissionApp;
begin     //CMD=DOWVER PARAMS=<VerName> --> ERECEIVED_RESULT,<kind><version><realfilename>,<filestream>
  app:= FServer.AppNames[clientMsg.AppName];
  if not Assigned(app) then begin
    serverMsg.ReturnValue:= E_FAILD;
    serverMsg.ErrorInfo:= 'Can not found application.';
  end else begin
    if FileExists(app.WorkFolder + clientMsg.Params[0]) then begin
      fs:= TFileStream.Create(app.WorkFolder + clientMsg.Params[0], fmOpenRead);
      serverMsg.DataStream.CopyFrom(fs, fs.Size);
      fs.Free;
      serverMsg.ReturnValue:= E_RETURN;
    end else begin
      serverMsg.ReturnValue:= E_FAILD;
      serverMsg.ErrorInfo:= 'Can not found file ' + clientMsg.Params[0];
    end;
  end;
end;

procedure TVonApplicationVersionServer.DownVersion(
  clientMsg: TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
var
  fs: TFileStream;
  app: TVonTransmissionApp;
begin     //CMD=DOWVER PARAMS=<VerName> --> ERECEIVED_RESULT,<kind><version><realfilename>,<filestream>
  app:= FServer.AppNames[clientMsg.AppName];
  if not Assigned(app) then begin
    serverMsg.ReturnValue:= E_FAILD;
    serverMsg.ErrorInfo:= 'Can not found application.';
  end else begin
    if FileExists(app.WorkFolder + clientMsg.Params[0]) then begin
      fs:= TFileStream.Create(app.WorkFolder + clientMsg.Params[0], fmOpenRead);
      serverMsg.DataStream.CopyFrom(fs, fs.Size);
      fs.Free;
      serverMsg.ReturnValue:= E_RETURN;
    end else begin
      serverMsg.ReturnValue:= E_FAILD;
      serverMsg.ErrorInfo:= 'Can not found file ' + clientMsg.Params[0];
    end;
  end;
end;

{$endregion '服务器段落'}
{--$ELSE}
{$region '客户端段落'}

{ TVonAppClient }

constructor TVonAppClient.Create(TransmissionType, ApplicationName: string);
begin
  inherited;
  Files:= TStringList.Create;
end;

destructor TVonAppClient.Destroy;
begin
  Files.Free;
  inherited;
end;

function TVonAppClient.ListApplication: Integer;
begin    //CMD=LSTAPP PARAMS= --> ERECEIVED_RESULT,<AppList>
  WriteLog(LOG_INFO, 'ListApplication', 'LSTAPP');
  ClientMsg.ClearMsg;
  ClientMsg.AppName:= AppName;
  ClientMsg.CommandString:= 'LSTAPP';
  FTransmissionClient.SendIt(ClientMsg, ServerMsg);
  if ServerMsg.ReturnValue = E_RETURN then
    Files.LoadFromStream(ServerMsg.DataStream)
  else if ServerMsg.ReturnValue = E_SUCCESS then
    Files.Clear
  else raise Exception.Create(ServerMsg.ErrorInfo);
  Result:= Files.Count;
end;

procedure TVonAppClient.AddApplication(AppName: string);
begin     //CMD=ADDAPP PARAMS=<appName> --> ERECEIVED_RESULT
  ClientMsg.ClearMsg;
  ClientMsg.AppName:= AppName;
  ClientMsg.CommandString:= 'ADDAPP';
  ClientMsg.Params.Add(AppName);
  FTransmissionClient.SendIt(ClientMsg, ServerMsg);
  if ServerMsg.ReturnValue = E_SUCCESS then
    WriteLog(LOG_INFO, 'AddApplication', 'SUCCESS')
  else raise Exception.Create(ServerMsg.ErrorInfo);
end;

procedure TVonAppClient.DelApplication(AppName: string);
begin     //CMD=DELAPP PARAMS=<appName> --> ERECEIVED_RESULT
  ClientMsg.ClearMsg;
  ClientMsg.AppName:= AppName;
  ClientMsg.CommandString:= 'DELAPP';
  ClientMsg.Params.Add(AppName);
  FTransmissionClient.SendIt(ClientMsg, ServerMsg);
  if ServerMsg.ReturnValue = E_SUCCESS then
    WriteLog(LOG_INFO, 'AddApplication', 'SUCCESS')
  else raise Exception.Create(ServerMsg.ErrorInfo);
end;

function TVonAppClient.GetVer: string;
begin    //CMD=GETVER PARAMS= --> ERECEIVED_RESULT,<version>
  ClientMsg.ClearMsg;
  ClientMsg.AppName:= AppName;
  ClientMsg.CommandString:= 'GETVER';
  FTransmissionClient.SendIt(ClientMsg, ServerMsg);
  if ServerMsg.ReturnValue = E_RETURN then
    Result:= ReadStringFromStream(ServerMsg.DataStream)
  else raise Exception.Create(ServerMsg.ErrorInfo);
end;

procedure TVonAppClient.DelVerFile(srcFilename: string);
begin     //CMD=DELVER PARAMS=<VerName> --> ERECEIVED_RESULT
  ClientMsg.ClearMsg;
  ClientMsg.AppName:= AppName;
  ClientMsg.CommandString:= 'DELVER';
  ClientMsg.Params.Add(srcFilename);
  FTransmissionClient.SendIt(ClientMsg, ServerMsg);
  if ServerMsg.ReturnValue = E_SUCCESS then
    WriteLog(LOG_INFO, 'DelVerFile', 'SUCCESS')
  else raise Exception.Create(ServerMsg.ErrorInfo);
end;

//版本检查，检查后自动下载差异版本文件列表
function TVonAppClient.CheckVer(AVer: string): Integer;
begin    //CMD=CHKVER PARAMS=<version> --> ERECEIVED_RESULT,<versionList>
  ClientMsg.ClearMsg;
  ClientMsg.AppName:= AppName;
  ClientMsg.CommandString:= 'CHKVER';
  ClientMsg.Params.Add(AVer);
  FTransmissionClient.SendIt(ClientMsg, ServerMsg);
  if ServerMsg.ReturnValue = E_RETURN then
    Files.LoadFromStream(ServerMsg.DataStream)
  else if ServerMsg.ReturnValue = E_SUCCESS then
    Files.Clear
  else raise Exception.Create(ServerMsg.ErrorInfo);
  Result:= Files.Count;
end;

procedure TVonAppClient.GetVerFile(VersionName, destFilename: string);
var
  fs: TFileStream;
begin     //CMD=DOWVER PARAMS=<VerName> --> ERECEIVED_RESULT,<kind><version><realfilename>,<filestream>
  ClientMsg.ClearMsg;
  ClientMsg.AppName:= AppName;
  ClientMsg.CommandString:= 'DOWVER';
  ClientMsg.Params.Add(VersionName);
  FTransmissionClient.SendIt(ClientMsg, ServerMsg);
  if ServerMsg.ReturnValue = E_RETURN then begin
    fs:= TFileStream.Create(destFilename, fmCreate);
    ZDecompressStream(ServerMsg.DataStream, fs);
    //fs.CopyFrom(, ServerMsg.DataStream.Size);
    fs.Free;
  end else raise Exception.Create(ServerMsg.ErrorInfo);
end;

procedure TVonAppClient.SetVerFile(AFilename, AKind, VersionName, AVersion, ARealName: string);
var
  fs: TFileStream;
begin     //CMD=NEWVER PARAMS=<kind><verName><version><realfilename> DATA=<filestream> --> ERECEIVED_RESULT
  ClientMsg.ClearMsg;
  ClientMsg.AppName:= AppName;
  ClientMsg.CommandString:= 'NEWVER';
  ClientMsg.Params.Add(AKind);
  ClientMsg.Params.Add(VersionName);
  ClientMsg.Params.Add(AVersion);
  ClientMsg.Params.Add(ARealName);
  fs:= TFileStream.Create(AFilename, fmOpenRead);
  try
    ZCompressStream(fs, ClientMsg.DataStream, zcDefault);
    FTransmissionClient.SendIt(ClientMsg, ServerMsg);
  finally
    fs.Free;
  end;
  if ServerMsg.ReturnValue = E_SUCCESS then begin
  end else raise Exception.Create(ServerMsg.ErrorInfo);
end;

{$endregion '客户端段落'}
{--$ENDIF}

initialization
{--$IFDEF TRANS_SERVER}
  TVonTransmissionServer.RegistModuleClass('应用管理', TVonApplicationVersionServer);
{--$ENDIF}

end.
