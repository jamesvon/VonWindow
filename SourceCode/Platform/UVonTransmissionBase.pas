/// <summary>服务器与客户端通讯基类及简单运用</summary>
(*======================================
* UVonTransmissionBase v3.0 written by James Von (jamesvon@163.com) ************
* v1.0 2012 完成了通讯基本功能
* v2.0 2014 规范了通讯基本架构，构建了第一个应用模块版本管理
* v3.0 2015 在2.0的基础上规范了命名和架构类结构
======================================*)(*======================================
* TVonTransmissionVersion 版本文件信息
--------------------------------------------------------------------------------
  SysFilename   文件唯一标识
  Kind          版本文件类型，U:替换文件类型，B:批处理执行类型，S:SQL执行类型
  Version       文件版本(十六进制存储的，占8个字节FFFFFFFF)
  RealFilename  实际文件名，相对路径的文件名，含子路径信息
  ------------------------------------------------------------------------------
  <fileflag>=<kind><version><realfilename>  缓存文件格式
======================================*)(*======================================
* TVonTransmissionApp 应用基本信息，存储应用名称和版本
--------------------------------------------------------------------------------
  FVersionList: TStringList  <fileflag>=<kind><version><realfilename>   缓存格式
======================================*)(*======================================
* TVonTransmissionInfo  通讯信息
--------------------------------------------------------------------------------
  通讯协议：通讯的基本格式 <CMD><application name><Params_Size><Params><Data>
    <CMD>               通讯命令（6个标准字符）
    <application name>  应用名称（8个标准字符）
    <Params_Size>       参数长度（以整型值直接存储）
    <Params>            参数内容
    <Data>              数据，以二进制方式读取
  ------------------------------------------------------------------------------
    使用时可以通过 TVonTransmissionInfo 的 MerageStream 函数完成信息写入，通过
  ReadFromStream 函数完成信息的读取；
======================================*)(*======================================
* TVonTransmissionServer  通讯服务，支持多个应用共同启用一个后台服务，应用信息缓
*   存在 FAppList[应用名，应用信息类] 中，通过静态函数 CreateServer 来创建实例。
--------------------------------------------------------------------------------
  TVonTransmissionServer.FAppList              服务器的应用信息存储结构
    <AppName>=<version>,Object                 应用名称=当前版本，App实例
  ------------------------------------------------------------------------------
  可通过静态函数 CreateServer(ATransmissionType: string) 来创建这个服务
    TransmissionType 通讯类型，目前支持 TCP UDP HTTP
  通讯类型是通过静态函数 RegistTransmission
    RegistTransmission(ATransmissionType: string; AClass: TVonTransmissionServerClass);
  可处理的命令是通过 RegistCommand 来进行注册使用的
    RegistCommand(ACommand: string; AMethod: TVonTransmissionMethodBase);
======================================*)(*======================================
* TVonApplicationServerBase 应用服务类基类
--------------------------------------------------------------------------------
  procedure RegistMethod; virtual; abstract;
  服务命令处理函数注册函数，子类只需实现这个函数即可，在这个函数内完成功能注册
 +------------------------------------------------------------------------------
 |TVonTransmissionReceiverAPP = class(TVonTransmissionModuleBase)
 |private
 |public
 |  procedure RegistMethod; override;
 |  procedure ...(clientMsg: TVonTransmissionClientMsg; serverMsg:
 |    TVonTransmissionServerMsg);
 |end;
 |
 |... ...
 |
 |procedure TVonTransmissionReceiverAPP.RegistMethod;
 |begin
 |  inherited;
 |  FServer.AddMethod('%command%', ...);
 |  FServer.AddMethod('...', ...);
 |end;
======================================*)(*======================================
*  TVonTransmissionClientClass = class of TVonTransmissionClient;
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*  TVonTransmissionClient   客户端通讯基类，继承类，只需实现下面这个方法就可以了
*     procedure SendIt(Msg: TVonTransmissionInfo); virtual; abstract;
*-------------------------------------------------------------------------------
*  TVonApplicationClientBase    客户端访问基类，
======================================*)
unit UVonTransmissionBase;

interface
// Forms, Classes, SysUtils, Math, Zlib, SvcMgr, UVonSystemFuns, ActiveX,
//  IniFiles, TypInfo, UVonLog, Types, Registry, Windows;

uses
  WinApi.Windows, WinApi.Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UVonSystemFuns, UVonLog, WinApi.ActiveX, DB, Data.win.ADODB, UVonClass;

const
  MAX_BUFFER_SIZE = 1073741824;
  RESULT_SUCC = 'SUCCESS';
  RESULT_CONT = 'CONTINUE';
  RESULT_FAIL = 'FAILD';

resourcestring
  RES_COMM_ConnectionInfo = 'The %s is connected on %d';
  // The <IP> is connected on <Port>
  RES_COMM_DisconnectionInfo = 'The %s is disconnected on %d';
  // The <IP> is disconnected on <Port>
  RES_COMM_ConnectToInfo = 'Begin to connect server %s on %d';
  // Begin to connect server <IP> on <Port>
  RES_COMM_DisconnectToInfo = 'Disconnect a server %s on %d';
  // Disconnect a server <IP> on <Port>
  RES_COMM_ReceiveInfo = 'Receive a %s command of %s';
  // Receive a <commandName> command of <ApplicationName>
  RES_COMM_MessageInfo = 'Received %d bytes from %s on %d';
  // Received <number> bytes from <IP> on <Port>
  RES_COMM_MaxErrorInfo = 'Received %d bytes, very large.';

type
  /// <summary>客户端通讯信息</summary>
  TVonTransmissionClientMsg = class(TObject)
  private
    FTempStream: TMemoryStream;
  public
    AppName: string;
    CommandString: string;
    DataStream: TStream;
    Flag: Integer;
    Params: TStringList;
    constructor Create;
    destructor Destroy; override;
    procedure ClearMsg;
    function ReadFromStream(ReceiveStream: TStream): Boolean;
    function SendStream: TStream;
  end;

  /// <summary>接收信息处理后结果</summary>
  ERECEIVED_RESULT = (E_FAILD, E_SUCCESS, E_RETURN);

  /// <summary>服务端通讯信息</summary>
  TVonTransmissionServerMsg = class(TObject)
  private
    FTempStream: TMemoryStream;
  public
    AppName: string;
    DataStream: TStream;
    ErrorInfo: string;
    Flag: Integer;
    ReturnValue: ERECEIVED_RESULT;
    constructor Create(ApplicationName: string; AFlag: Integer);
    destructor Destroy; override;
    function ReadFromStream(ReceiveStream: TStream): Boolean;
    function SendStream: TStream;
  end;

{--$IFDEF TRANS_SERVER}
{$region '服务器段落'}

  /// <summary>服务器文件信息</summary>
  TVonTransmissionVersion = class(TObject)
  private
    FKind: string;
    FRealFilename: string;
    FSysFilename: string;
    FVersion: TVonVersion;
    function GetText: string;
    procedure SetKind(const Value: string);
    procedure SetRealFilename(const Value: string);
    procedure SetSysFilename(const Value: string);
    procedure SetText(const Value: string);
  published
    property Kind: string read FKind write SetKind;
    property RealFilename: string read FRealFilename write SetRealFilename;
    property SysFilename: string read FSysFilename write SetSysFilename;
    property Text: string read GetText write SetText;
    property Version: TVonVersion read FVersion;
  end;

  /// <summary>应用信息</summary>
  TVonTransmissionApp = class(TObject)
  private
    FAppName: string;
    FCurrentVersion: TVonVersion;
    FVersionList: TStringList;
    FWorkFolder: string;
    function GetSysFileFlag(Index: Integer): string;
    function GetSysFileVer(Index: Integer): string;
    function GetVersionData(Index: Integer): TVonTransmissionVersion;
    function GetVersionFileCount: Integer;
  public
    /// <summary>构造函数</summary>
    /// <param name='ApplicationName'>应用名称</param>
    /// <param name='WorkDirectory'>工作目录</param>
    constructor Create(ApplicationName, WorkDirectory: string);
    /// <summary>析构函数</summary>
    destructor Destroy; override;
    /// <summary>添加一个新版本</summary>
    procedure AddVersion(AVer: TVonTransmissionVersion);
    /// <summary>删除一个版本文件</summary>
    procedure DelVersion(AVerName: string);
    /// <summary>第n个版本文件标识</summary>
    property SysFileFlag[Index: Integer]: string read GetSysFileFlag;
    /// <summary>第n个版本的版本号</summary>
    property SysFileVer[Index: Integer]: string read GetSysFileVer;
    /// <summary>第n个版本的版本信息对象</summary>
    property VersionData[Index: Integer]: TVonTransmissionVersion read
        GetVersionData;
  published
    /// <summary>应用名称</summary>
    property AppName: string read FAppName;
    /// <summary>当前应用最新版本</summary>
    property CurrentVersion: TVonVersion read FCurrentVersion;
    /// <summary>版本文件数量</summary>
    property VersionFileCount: Integer read GetVersionFileCount;
    /// <summary>当前应用工作目录</summary>
    property WorkFolder: string read FWorkFolder;
  end;

  /// <summary>服务端收到信息后处理方法声明</summary>
  TVonTransmissionMethod = procedure (clientMsg: TVonTransmissionClientMsg;
      serverMsg: TVonTransmissionServerMsg) of object;
  TVonTransmissionServer = class;

  /// <summary>服务端收到信息后处理模块基类声明</summary>
  TVonTransmissionModuleBaseClass = class of TVonApplicationServerBase;
  /// <summary>应用服务响应基类，所有应用通讯均从此模块继承，系统在加载时会调用</summary>
  TVonApplicationServerBase = class(TObject)
  protected
    FConn: TADOConnection;
    FServer: TVonTransmissionServer;
    procedure RegistMethod; virtual; abstract;
  public
    /// <summary>构造函数，在这里面需要通过实现基类的RegistMethod函数完成命令执行的注册</summary>、
    /// <code>
    ///   TVonTransmissionReceiverAPP = class(TVonTransmissionModuleBase)
    ///   private
    ///   public
    ///     procedure RegistMethod; override;
    ///     procedure ...(clientMsg: TVonTransmissionClientMsg; serverMsg:
    ///     TVonTransmissionServerMsg);
    ///   end;
    /// 
    ///   ... ...
    /// 
    /// procedure TVonTransmissionReceiverAPP.RegistMethod;
    /// begin
    ///   inherited;
    ///   FServer.AddMethod('%command%', ...);
    /// end;
    /// </code>
    constructor Create(Server: TVonTransmissionServer; Conn: TADOConnection);
  end;

  /// <summary>通讯服务基类的声明</summary>
  TVonTransmissionServerClass = class of TVonTransmissionServer;
  /// <summary>通讯服务基类，通过静态函数 CreateServer 创建实例</summary>
  TVonTransmissionServer = class(TObject)
  private
    /// <summary>应用信息对象集合，[应用名称,应用对象]</summary>
    FAppList: TStringList;
    FCommunicationType: string;
    FConn: TADOConnection;
    FPort: Integer;
    /// <summary>服务命令处理信息对象集合，[命令名称,处理事件对象]</summary>
    FServerMethodList: TStringList;
    /// <summary>服务模块对象集合，[模块名称,模块对象]</summary>
    FServerModuleList: TList;
    FServiceWorkFolder: string;
    function GetAppNames(AppName: string): TVonTransmissionApp;
    function GetApps(Index: Integer): TVonTransmissionApp;
    procedure SetConn(const Value: TADOConnection);
    procedure SetServiceWorkFolder(const Value: string);
    function GetAppCount: Integer;
  protected
    procedure ReceivedIt(clientMsg: TVonTransmissionClientMsg; serverMsg:
        TVonTransmissionServerMsg);
  public
    /// <summary>构造函数</summary>
    constructor Create; virtual;
    /// <summary>析构函数</summary>
    destructor Destroy; override;
    /// <summary>添加一个应用模块</summary>
    procedure AddApplication(App: TVonTransmissionApp);
    /// <summary>注册一个服务命令执行方法</summary>
    procedure AddMethod(Cmd: string; Method: TVonTransmissionMethod);
    /// <summary>创建一个通讯服务</summary>
    class function CreateServer(TransmissionType, ServicePath, ConnStr: string):
        TVonTransmissionServer; static;
    /// <summary>注册一个服务模块</summary>
    class procedure RegistModuleClass(ModuleName: string; Module:
        TVonTransmissionModuleBaseClass);
    /// <summary>注册一种通讯类型</summary>
    class procedure RegistTransmission(TransmissionType: string; Transmission:
        TVonTransmissionServerClass); static;
    /// <summary>删除一个应用模块</summary>
    procedure RemoveAppllication(Idx: Integer); overload;
    /// <summary>删除一个应用模块</summary>
    procedure RemoveAppllication(AppName: string); overload;
    /// <summary>启动服务</summary>
    procedure Start(Port: Integer); virtual; abstract;
    /// <summary>停止服务</summary>
    procedure Stop; virtual; abstract;
    /// <summary>将应用列表写入流中</summary>
    procedure AppListSaveToStream(s: TStream);
    /// <summary>将应用列表写入文件</summary>
    procedure AppListSaveToFile(filename: TFilename);
    /// <summary>根据应用名称得到应用信息对象</summary>
    property AppNames[AppName: string]: TVonTransmissionApp read GetAppNames;
    /// <summary>按照序号德大应用信息对象</summary>
    property Apps[Index: Integer]: TVonTransmissionApp read GetApps;
  published
    /// <summary>通讯类型</summary>
    property CommunicationType: string read FCommunicationType;
    /// <summary>数据库连接器</summary>
    property Conn: TADOConnection read FConn write SetConn;
    /// <summary>通信端口</summary>
    property Port: Integer read FPort;
    /// <summary>应用模块数量</summary>
    property AppCount: Integer read GetAppCount;
    /// <summary>服务器工作目录</summary>
    property ServiceWorkFolder: string read FServiceWorkFolder write
        SetServiceWorkFolder;
  end;

{$endregion '服务器段落'}
{--$ELSE}
{$region '客户端段落'}
  /// <summary>接收数据后处理数据事件</summary>
  TEventOnPrompt = procedure (Cmd, PromptInfo: string) of object;
  TVonApplicationClientBase = class;

  /// <summary>通讯客户端控件基类</summary>
  TVonTransmissionClientClass = class of TVonTransmissionClient;
  /// <summary>通讯客户端基类</summary>
  TVonTransmissionClient = class(TObject)
  protected
    FAppClient: TVonApplicationClientBase;
  public
    constructor Create(AppClient: TVonApplicationClientBase); virtual;
    procedure SendIt(clientMsg: TVonTransmissionClientMsg; serverMsg:
        TVonTransmissionServerMsg); virtual; abstract;
  end;

  /// <summary>客户端应用基类</summary>
  TVonApplicationClientBase = class(TObject)
  private
    FAppName: string;
    FClientMsg: TVonTransmissionClientMsg;
    FHostIP: string;
    FHostPort: Integer;
    FOnPrompt: TEventOnPrompt;
    FServerMsg: TVonTransmissionServerMsg;
    FWorkFolder: string;
    procedure SetHostIP(const Value: string);
    procedure SetHostPort(const Value: Integer);
    procedure SetOnPrompt(const Value: TEventOnPrompt);
    procedure SetWorkFolder(const Value: string);
  protected
    FTransmissionClient: TVonTransmissionClient;
  public
    constructor Create(TransmissionType, ApplicationName: string); virtual;
    destructor Destroy; override;
    class procedure RegistTransmission(TransmissionType: string; Transmission:
        TVonTransmissionClientClass); static;
  published
    /// <summary>应用模块名称</summary>
    property AppName: string read FAppName;
    property ClientMsg: TVonTransmissionClientMsg read FClientMsg;
    property HostIP: string read FHostIP write SetHostIP;
    property HostPort: Integer read FHostPort write SetHostPort;
    property ServerMsg: TVonTransmissionServerMsg read FServerMsg;
    property WorkFolder: string read FWorkFolder write SetWorkFolder;
    property OnPrompt: TEventOnPrompt read FOnPrompt write SetOnPrompt;
  end;

{$endregion '客户端段落'}
{--$ENDIF}

var
  FSvcPath: string;

implementation

var
  /// <summary>通讯方式及通讯类列表</summary>
  FTransList: TStringList;

{ TVonTransmissionClientMsg }

{
************************** TVonTransmissionClientMsg ***************************
}
constructor TVonTransmissionClientMsg.Create;
begin
  Params:= TStringList.Create;
  DataStream:= TMemoryStream.Create;
  FTempStream:= TMemoryStream.Create;
end;

destructor TVonTransmissionClientMsg.Destroy;
begin
  FTempStream.Free;
  DataStream.Free;
  Params.Free;
  inherited;
end;

procedure TVonTransmissionClientMsg.ClearMsg;
begin
  Flag:= Random(MaxInt);
  CommandString:= '';
  Params.Clear;
  DataStream.Size:= 0;
  DataStream.Position:= 0;
end;

function TVonTransmissionClientMsg.ReadFromStream(ReceiveStream: TStream):
    Boolean;
var
  szStr: string;
  szSize: Integer;
  b: Byte;
  errInfo: string;
begin
         //<FLAG><CMD><APPNAME><SIZE_PARAM><PARAM><DATA>   4+4+(6+8)+4+n+m
  Result := False;
  try
    ReceiveStream.Position := 0;
    if ReceiveStream.Size < 22 then begin       //4+4+(6+8)+4
      ErrInfo:= 'The message size is very shorter.';
      Exit;
    end;
    ReceiveStream.Read(Flag, 4);
    szStr:= ReadStringFromStream(ReceiveStream);//4+(6+8) CommandString + AppName
    if Length(szStr) <> 14 then begin
      ErrInfo:= 'Command and application name error.';
      Exit;
    end;
    CommandString := Copy(szStr, 1, 6);         //Read Command name(1-6,6)
    AppName := Copy(szStr, 7, 8);               //Read application name(7-14,8)
    Params.Text:= ReadStringFromStream(ReceiveStream);//读取参数(15-n)
    DataStream.Size := 0;                       //读取数据(n...)
    DataStream.CopyFrom(ReceiveStream, ReceiveStream.Size -
      ReceiveStream.Position);
    DataStream.Position := 0;
  except
    on E: Exception do
    begin //将接收到的信息（十六进制形式）写入日志
      ReceiveStream.Position := 0;
      szSize := 0; errInfo:= '';
      while ReceiveStream.Position < ReceiveStream.Size do
      begin
        ReceiveStream.Read(b, 1);
        errInfo := errInfo + ' ' + IntToHex(b, 2);
        Inc(szSize);
        if szSize mod 16 = 0 then
          errInfo := errInfo + #13#10;
      end;
      WriteLog(LOG_FAIL, 'StreamToMsg',
        'Receiver a message from client. The error is raised. '#13#10 +
        E.Message + errInfo);
    end;
  end;
  Result := True;
end;

function TVonTransmissionClientMsg.SendStream: TStream;
var
  szSize: Integer;
begin
         //<FLAG><CMD><APPNAME><SIZE_PARAM><PARAM><DATA>   4+4+(6+8)+4+n+m
  FTempStream.SetSize(0);
  if Length(CommandString) <> 6 then
    raise Exception.Create('发送命令长度（6字节）不正确！');
  if Length(AppName) <> 8 then
    raise Exception.Create('应用名称长度（8字节）不正确！');
  FTempStream.Write(Flag, 4);                                // 写入通讯标识       4
  WriteStringToStream(CommandString + AppName, FTempStream); // 写入命令及应用名称 4+6+8
  szSize := Length(Params.Text) * SizeOf(Char);              // 写入参数信息       4+n
  FTempStream.Write(szSize, 4);
  if szSize > 0 then
    FTempStream.Write(Params.Text[1], szSize);
  DataStream.Position := 0;
  if DataStream.Size > 0 then
    FTempStream.CopyFrom(DataStream, DataStream.Size);       // 写入数据
  FTempStream.Position := 0;
  Result := FTempStream;
end;

{ TVonTransmissionServerMsg }

{
************************** TVonTransmissionServerMsg ***************************
}
constructor TVonTransmissionServerMsg.Create(ApplicationName: string; AFlag:
    Integer);
begin
  DataStream:= TMemoryStream.Create;
  FTempStream:= TMemoryStream.Create;
  Flag:= AFlag;
  AppName:= ApplicationName;
  ReturnValue:= E_FAILD;
  ErrorInfo:= '';
  DataStream.Size:= 0;
  DataStream.Position:= 0;
end;

destructor TVonTransmissionServerMsg.Destroy;
begin
  FTempStream.Free;
  DataStream.Free;
  inherited;
end;

function TVonTransmissionServerMsg.ReadFromStream(ReceiveStream: TStream):
    Boolean;
var
  szStr: string;
  szSize: Integer;
  b: Byte;
  errInfo: string;
begin
         //<FLAG><APPNAME><RESULT><DATA>      4+4+(8)+4+m
  Result := False;
  try
    ReceiveStream.Position := 0;
    if ReceiveStream.Size < 8 then begin                    //4+4+(8)+4
      ErrInfo:= 'The message size is very shorter.';
      Exit;
    end;
    ReceiveStream.Read(Flag, 4);
    AppName:= ReadStringFromStream(ReceiveStream);          //4+(8) AppName
    if Length(AppName) <> 8 then begin
      ErrInfo:= 'Command and application name error.';
      Exit;
    end;
    ReceiveStream.Read(szSize, 4);
    ReturnValue := ERECEIVED_RESULT(szSize);                //Read ReturnValue 4
    DataStream.Size := 0;                       //读取数据(n...)
    case ReturnValue of
    E_FAILD: ErrorInfo:= ReadStringFromStream(ReceiveStream);
    E_SUCCESS: DataStream.CopyFrom(ReceiveStream, ReceiveStream.Size -
      ReceiveStream.Position);
    E_RETURN: DataStream.CopyFrom(ReceiveStream, ReceiveStream.Size -
      ReceiveStream.Position);
    end;
    DataStream.Position := 0;                       //读取数据(n...)
  except
    on E: Exception do
    begin //将接收到的信息（十六进制形式）写入日志
      ReceiveStream.Position := 0;
      szSize := 0; errInfo:= '';
      while ReceiveStream.Position < ReceiveStream.Size do
      begin
        ReceiveStream.Read(b, 1);
        errInfo := errInfo + ' ' + IntToHex(b, 2);
        Inc(szSize);
        if szSize mod 16 = 0 then
          errInfo := errInfo + #13#10;
      end;
      WriteLog(LOG_FAIL, 'StreamToMsg',
        'Receiver a message from client. The error is raised. '#13#10 +
        E.Message + errInfo);
    end;
  end;
  Result := True;
end;

function TVonTransmissionServerMsg.SendStream: TStream;
begin
         //<FLAG><APPNAME><RESULT><DATA>
  FTempStream.SetSize(0);
  FTempStream.Write(Flag, 4);
  if Length(AppName) <> 8 then
    raise Exception.Create('应用名称长度（8字节）不正确！');
  WriteStringToStream(AppName, FTempStream); // 写入命令及应用名称 4+6+8
  FTempStream.Write(ReturnValue, 4);
  DataStream.Position := 0;
  if DataStream.Size > 0 then
    FTempStream.CopyFrom(DataStream, DataStream.Size);       // 写入数据
  FTempStream.Position := 0;
  Result := FTempStream;
end;

{--$IFDEF TRANS_SERVER}
{$region '服务器段落'}

type
  PVonMethod = ^TVonMethod;
  TVonMethod = record
    Method: TVonTransmissionMethod;
  end;

var
  FModuleList: TStringList;

{ TVonTransmissionVersion }

{
*************************** TVonTransmissionVersion ****************************
}
function TVonTransmissionVersion.GetText: string;
begin
  Result:= Format('%s%s%s', [FKind, Version.Hex, FRealFilename]);
end;

procedure TVonTransmissionVersion.SetKind(const Value: string);
begin
  FKind := Value;
end;

procedure TVonTransmissionVersion.SetRealFilename(const Value: string);
begin
  FRealFilename := Value;
end;

procedure TVonTransmissionVersion.SetSysFilename(const Value: string);
begin
  FSysFilename := Value;
end;

procedure TVonTransmissionVersion.SetText(const Value: string);
begin     //<flag(1)><version(hex,4,4,4,4)><realfilename>
  FKind:= Value[1];
  FVersion.Hex:= Copy(Value, 2, 16);
  RealFilename:= Copy(Value, 18, MaxInt);
end;

{ TVonTransmissionApp }

// 加载该应用路径下的 APPFILE.CFG 文件，读取该应用的版本信息
{
***************************** TVonTransmissionApp ******************************
}
constructor TVonTransmissionApp.Create(ApplicationName, WorkDirectory: string);
var
  aVer: TVonTransmissionVersion;
  I: Integer;
begin
  FVersionList:= TStringList.Create;
  FAppName:= ApplicationName;
  FCurrentVersion.Text:= '0.0.0.0';
  if WorkDirectory[Length(WorkDirectory)] = '\' then
    FWorkFolder:= WorkDirectory
  else FWorkFolder:= WorkDirectory + '\';
  if FileExists(WorkFolder + 'APPFILE.CFG') then
    FVersionList.LoadFromFile(WorkFolder + 'APPFILE.CFG');
  for I := 0 to FVersionList.Count - 1 do begin
    if FVersionList[I] <> ''  then begin
      aVer:= TVonTransmissionVersion.Create;
      WriteLog(LOG_DEBUG, 'TVonTransmissionApp.Create',
        Format('App set config %s', [FVersionList.ValueFromIndex[I]]));
      aVer.Text:= FVersionList.ValueFromIndex[I];
      WriteLog(LOG_DEBUG, 'TVonTransmissionApp.Create',
        Format('%s While check %s and %s', [FVersionList.ValueFromIndex[I], FCurrentVersion.Text, aVer.Version.Text]));
      if FCurrentVersion.Check(aVer.Version) < 0 then
        FCurrentVersion:= aVer.Version;
      FVersionList.Objects[I]:= aVer;
    end;
  end;
end;

destructor TVonTransmissionApp.Destroy;
begin
  while FVersionList.Count > 0 do begin
    FVersionList.Objects[FVersionList.Count - 1].Free;
    FVersionList.Delete(FVersionList.Count - 1);
  end;
  FVersionList.Free;
  inherited;
end;

procedure TVonTransmissionApp.AddVersion(AVer: TVonTransmissionVersion);
var
  Idx: Integer;
begin
  Idx:= FVersionList.IndexOfName(aVer.FSysFilename);
  //if AVer.Version.Text = '0.0.0.0' then AVer.Version.Int:= CurrentVersion.Int + 1;

  if Idx < 0 then begin
    FVersionList.AddObject(aVer.FSysFilename + '=' + aVer.Text, aVer);
    WriteLog(LOG_DEBUG, 'FVersionList', 'AddObject -> ' + aVer.Text);
  end else begin
    FVersionList.ValueFromIndex[Idx]:= aVer.Text;
    FVersionList.Objects[Idx].Free;
    FVersionList.Objects[Idx]:= AVer;
    WriteLog(LOG_DEBUG, 'FVersionList', 'EditObject -> ' + aVer.Text);
  end;
  FCurrentVersion:= AVer.Version;
  FVersionList.SaveToFile(WorkFolder + 'APPFILE.CFG');
end;

procedure TVonTransmissionApp.DelVersion(AVerName: string);
var
  Idx: Integer;
begin
  Idx:= FVersionList.IndexOfName(AVerName);
  if Idx >= 0 then
    FVersionList.Delete(Idx);
end;

function TVonTransmissionApp.GetSysFileFlag(Index: Integer): string;
begin
  Result:= FVersionList.Names[Index];
end;

function TVonTransmissionApp.GetSysFileVer(Index: Integer): string;
begin
  Result:= FVersionList.ValueFromIndex[Index];
end;

function TVonTransmissionApp.GetVersionData(Index: Integer):
    TVonTransmissionVersion;
begin
  Result:= TVonTransmissionVersion(FVersionList.Objects[Index]);
end;

function TVonTransmissionApp.GetVersionFileCount: Integer;
begin
  Result:= FVersionList.Count;
end;

{ TVonTransmissionServer }

(* 静态类处理函数 *)

{
**************************** TVonTransmissionServer ****************************
}
constructor TVonTransmissionServer.Create;
begin
  FAppList:= TStringList.Create;
  FServerModuleList:= TList.Create;
  FServerMethodList:= TStringList.Create;
end;

destructor TVonTransmissionServer.Destroy;
begin
  while FServerMethodList.Count > 0 do begin
    Dispose(PVonMethod(FServerMethodList.Objects[FServerMethodList.Count - 1]));
    FServerMethodList.Delete(FServerMethodList.Count - 1);
  end;
  FServerMethodList.Free;
  while FServerModuleList.Count > 0 do begin
    TObject(FServerModuleList[FServerModuleList.Count - 1]).Free;
    FServerModuleList.Delete(FServerModuleList.Count - 1);
  end;
  FServerModuleList.Free;
  with FAppList do
    while Count > 0 do begin
      Objects[Count - 1].Free;
      Delete(Count - 1);
    end;
  FAppList.Free;
end;

procedure TVonTransmissionServer.AddApplication(App: TVonTransmissionApp);
begin
  FAppList.AddObject(App.AppName + '=' + App.CurrentVersion.Text, App);
  FAppList.SaveToFile(FServiceWorkFolder + 'VonService.CFG');
end;

procedure TVonTransmissionServer.AddMethod(Cmd: string; Method:
    TVonTransmissionMethod);
var
  I: Integer;
  P: PVonMethod;
begin
  Cmd:= UpperCase(Cmd);
  for I := 0 to FServerMethodList.Count - 1 do
    if FServerMethodList[I] = Cmd then begin
      P:= PVonMethod(FServerMethodList.Objects[I]);
      P.Method:= Method;
      Exit;
    end;
  New(P);
  P.Method:= Method;
  FServerMethodList.AddObject(Cmd, TObject(P));
end;

procedure TVonTransmissionServer.AppListSaveToFile(filename: TFilename);
begin
  FAppList.SaveToFile(filename);
end;

procedure TVonTransmissionServer.AppListSaveToStream(s: TStream);
begin
  FAppList.SaveToStream(s);
end;

class function TVonTransmissionServer.CreateServer(TransmissionType,
    ServicePath, ConnStr: string): TVonTransmissionServer;
var
  I, J: Integer;
begin
  Result:= nil;
  WriteLog(LOG_DEBUG, 'CreateServer', Format('TransmissionType=%s,ServicePath=%s,TransList=%s', [TransmissionType, ServicePath,FTransList.Text]));
  if TransmissionType = '' then TransmissionType:= 'TCP';
  I:= FTransList.IndexOf('SVR_' + TransmissionType);
  if I < 0 then Exit;
  Result:= TVonTransmissionServerClass(FTransList.Objects[I]).Create;
  if not Assigned(Result) then
    WriteLog(LOG_DEBUG, 'CreateServer', Format('Failed to create %s service', [TransmissionType]));
  if ConnStr <> '' then begin
    CoInitialize(nil); // 手动调用 CoInitialize()
    Result.Conn:= TADOConnection.Create(nil);
    Result.Conn.ConnectionString:= ConnStr;
    Result.Conn.LoginPrompt:= False;
    Result.Conn.Open;
    WriteLog(LOG_DEBUG, 'CreateServer', 'Connected database.');
  end;
  WriteLog(LOG_DEBUG, 'CreateServer', Format('Set ServiceWorkFolder %s', [ServicePath]));
  Result.ServiceWorkFolder:= ServicePath;
end;

function TVonTransmissionServer.GetAppCount: Integer;
begin
  Result:= FAppList.Count;
end;

function TVonTransmissionServer.GetAppNames(AppName: string):
    TVonTransmissionApp;
var
  Idx: Integer;
begin
  Idx:= FAppList.IndexOfName(AppName);
  if Idx < 0 then Result:= nil
  else Result:= TVonTransmissionApp(FAppList.Objects[Idx]);
end;

function TVonTransmissionServer.GetApps(Index: Integer): TVonTransmissionApp;
begin
  Result:= TVonTransmissionApp(FAppList.Objects[Index]);
end;

procedure TVonTransmissionServer.ReceivedIt(clientMsg:
    TVonTransmissionClientMsg; serverMsg: TVonTransmissionServerMsg);
var
  I: Integer;
begin
  for I := 0 to FServerMethodList.Count - 1 do
    if clientMsg.CommandString = FServerMethodList[I] then begin
      PVonMethod(FServerMethodList.Objects[I]).Method(clientMsg, serverMsg);
      Exit;
    end;
  WriteLog(LOG_FAIL, 'SVR_Received_MSG', 'Can not found method ' + clientMsg.CommandString);
end;

class procedure TVonTransmissionServer.RegistModuleClass(ModuleName: string;
    Module: TVonTransmissionModuleBaseClass);
var
  Idx: Integer;
begin
  Idx:= FModuleList.IndexOf(ModuleName);
  if Idx < 0 then FModuleList.AddObject(UpperCase(ModuleName), TObject(Module))
  else FModuleList.Objects[Idx]:= TObject(Module);
end;

class procedure TVonTransmissionServer.RegistTransmission(TransmissionType:
    string; Transmission: TVonTransmissionServerClass);
begin
  if FTransList.IndexOf('SVR_' + TransmissionType) >= 0 then Exit;
  FTransList.AddObject('SVR_' + TransmissionType, TObject(Transmission));
end;

procedure TVonTransmissionServer.RemoveAppllication(AppName: string);
begin
  RemoveAppllication(FTransList.IndexOfName(AppName));
end;

procedure TVonTransmissionServer.RemoveAppllication(Idx: Integer);
begin
  if Idx >= 0 then begin
    FAppList.Objects[Idx].Free;
    FAppList.Delete(Idx);
    FAppList.SaveToFile(FServiceWorkFolder + 'VonService.CFG');
  end;
end;

procedure TVonTransmissionServer.SetConn(const Value: TADOConnection);
begin
  FConn := Value;
end;

procedure TVonTransmissionServer.SetServiceWorkFolder(const Value: string);
var
  I: Integer;
  App: TVonTransmissionApp;
  szModule: TVonApplicationServerBase;
begin
  WriteLog(LOG_Info, 'Init', 'Service work folder is ' + Value);
  FServiceWorkFolder := Value;
  if Not assigned(FModuleList) then
    WriteLog(LOG_Info, 'Init', 'Can not found any modules to load.');
  WriteLog(LOG_Info, 'Init', Format('Find %d modules to load.', [FModuleList.Count]));
  for I := 0 to FModuleList.Count - 1 do begin
    szModule:= TVonTransmissionModuleBaseClass(FModuleList.Objects[I]).Create(Self, FConn);
    FServerModuleList.Add(szModule);
  end;
  FAppList.LoadFromFile(FServiceWorkFolder + 'VonService.CFG');
  WriteLog(LOG_Info, 'Init', Format('Load %d applications', [FAppList.Count]));
  for I := 0 to FAppList.Count - 1 do begin
    WriteLog(LOG_Info, 'Init', Format('init %s application', [FAppList.Names[I]]));
    App:= TVonTransmissionApp.Create(FAppList.Names[I], FServiceWorkFolder + FAppList.Names[I]);
    FAppList.Objects[I]:= App;
  end;
  WriteLog(LOG_Info, 'Init', 'Service is repaired.');
end;

(* 构造及析构函数 *)

(* 主要注册及添加函数 *)

{ TVonApplicationServerBase }

{
************************** TVonApplicationServerBase ***************************
}
constructor TVonApplicationServerBase.Create(Server: TVonTransmissionServer;
    Conn: TADOConnection);
begin
  FServer:= Server;
  FConn:= Conn;
  RegistMethod;
end;

{$endregion '服务器段落'}{--$ELSE}
{$region '客户端段落'}

{ TVonTransmissionClient }

{
**************************** TVonTransmissionClient ****************************
}
constructor TVonTransmissionClient.Create(AppClient: TVonApplicationClientBase);
begin
  FAppClient:= AppClient;
end;

{ TVonApplicationClientBase }

{
************************** TVonApplicationClientBase ***************************
}
constructor TVonApplicationClientBase.Create(TransmissionType, ApplicationName:
    string);
var
  I: Integer;
begin
  FAppName := Copy(ApplicationName + '________', 1, 8);
  FClientMsg:= TVonTransmissionClientMsg.Create;
  FServerMsg:= TVonTransmissionServerMsg.Create(FAppName, 0);
  I:= FTransList.IndexOf('CLT_' + TransmissionType);
  if I < 0 then
    raise Exception.Create('Can not found class ' + TransmissionType);
  FTransmissionClient:= TVonTransmissionClientClass(FTransList.Objects[I]).Create(Self);
end;

destructor TVonApplicationClientBase.Destroy;
begin
  FServerMsg.Free;
  FClientMsg.Free;
  inherited;
end;

class procedure TVonApplicationClientBase.RegistTransmission(TransmissionType:
    string; Transmission: TVonTransmissionClientClass);
begin
  if FTransList.IndexOf('CLT_' + TransmissionType) >= 0 then Exit;
  FTransList.AddObject('CLT_' + TransmissionType, TObject(Transmission));
end;

procedure TVonApplicationClientBase.SetHostIP(const Value: string);
begin
  FHostIP:= Value;
end;

procedure TVonApplicationClientBase.SetHostPort(const Value: Integer);
begin
  FHostPort := Value;
end;

procedure TVonApplicationClientBase.SetOnPrompt(const Value: TEventOnPrompt);
begin
  FOnPrompt := Value;
end;

procedure TVonApplicationClientBase.SetWorkFolder(const Value: string);
begin
  FWorkFolder := Value;
end;

{$endregion '客户端段落'}{--$ENDIF}

initialization
  WriteLog(LOG_DEBUG, 'UVonTransmissionBase', 'initialization ... ...');
  Randomize;
  CoInitialize(nil); // 手动调用 CoInitialize()
  FTransList:= TStringList.Create;
  FModuleList:= TStringList.Create;

finalization
  FModuleList.Free;
  FTransList.Free;
  CoUninitialize; // 释放内存

end.
