object FVonTools: TFVonTools
  Left = 0
  Top = 0
  Caption = 'VonWindows'#24037#20855
  ClientHeight = 747
  ClientWidth = 1194
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 1194
    Height = 747
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #35777#20070#29983#25104#22120
      ExplicitWidth = 1004
      ExplicitHeight = 608
      object Splitter1: TSplitter
        Left = 549
        Top = 0
        Height = 719
        Align = alRight
        ExplicitLeft = 496
        ExplicitTop = 264
        ExplicitHeight = 100
      end
      object Panel2: TPanel
        Left = 1030
        Top = 0
        Width = 156
        Height = 719
        Align = alRight
        Caption = 'Panel2'
        ShowCaption = False
        TabOrder = 0
        ExplicitLeft = 848
        ExplicitHeight = 608
        object Label1: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 148
          Height = 13
          Align = alTop
          Caption = #21152#23494#31639#27861
          ExplicitTop = 50
          ExplicitWidth = 48
        end
        object Label3: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 50
          Width = 148
          Height = 13
          Align = alTop
          Caption = #26680#25276#31639#27861
          ExplicitTop = 96
          ExplicitWidth = 48
        end
        object Label2: TLabel
          AlignWithMargins = True
          Left = 4
          Top = 96
          Width = 148
          Height = 13
          Align = alTop
          Caption = #35777#20070#31192#38053
          ExplicitTop = 142
          ExplicitWidth = 48
        end
        object ECertCipherName: TComboBox
          AlignWithMargins = True
          Left = 4
          Top = 23
          Width = 148
          Height = 21
          Align = alTop
          Style = csDropDownList
          TabOrder = 0
          ExplicitTop = 69
        end
        object ECertHashName: TComboBox
          AlignWithMargins = True
          Left = 4
          Top = 69
          Width = 148
          Height = 21
          Align = alTop
          Style = csDropDownList
          TabOrder = 1
          ExplicitTop = 115
        end
        object ECertCipherKey: TEdit
          AlignWithMargins = True
          Left = 4
          Top = 115
          Width = 148
          Height = 21
          Align = alTop
          TabOrder = 2
          ExplicitTop = 161
        end
        object btnCertRead: TButton
          AlignWithMargins = True
          Left = 4
          Top = 142
          Width = 148
          Height = 25
          Align = alTop
          Caption = #35835#21462
          ImageIndex = 11
          ImageMargins.Left = 5
          Images = imgBtn
          TabOrder = 3
          OnClick = btnCertReadClick
          ExplicitTop = 188
        end
        object btnCertPublish: TButton
          AlignWithMargins = True
          Left = 4
          Top = 173
          Width = 148
          Height = 25
          Align = alTop
          Caption = #21457#24067
          ImageIndex = 32
          ImageMargins.Left = 5
          Images = imgBtn
          TabOrder = 4
          OnClick = btnCertPublishClick
          ExplicitTop = 219
        end
      end
      object GroupBox5: TGroupBox
        Left = 552
        Top = 0
        Width = 478
        Height = 719
        Align = alRight
        Caption = #24212#29992#21442#25968
        TabOrder = 1
        object lstAppCert: TValueListEditor
          Left = 2
          Top = 15
          Width = 474
          Height = 546
          Align = alClient
          KeyOptions = [keyEdit, keyAdd, keyDelete, keyUnique]
          Strings.Strings = (
            '')
          TabOrder = 0
          TitleCaptions.Strings = (
            #39033#30446
            #20869#23481)
          ExplicitWidth = 181
          ExplicitHeight = 702
          ColWidths = (
            150
            318)
        end
        object Panel4: TPanel
          Left = 2
          Top = 561
          Width = 474
          Height = 156
          Align = alBottom
          Caption = 'Panel4'
          ShowCaption = False
          TabOrder = 1
          object lstAppAttachment: TListView
            Left = 1
            Top = 1
            Width = 343
            Height = 154
            Align = alClient
            Columns = <>
            LargeImages = ImgIcon
            SmallImages = imgBtn
            TabOrder = 0
            OnClick = lstAppAttachmentClick
            ExplicitLeft = 0
            ExplicitTop = 6
            ExplicitHeight = 125
          end
          object GroupBox2: TGroupBox
            Left = 344
            Top = 1
            Width = 129
            Height = 154
            Align = alRight
            Caption = #38468#20214
            TabOrder = 1
            ExplicitLeft = 51
            ExplicitHeight = 125
            object Label4: TLabel
              AlignWithMargins = True
              Left = 5
              Top = 18
              Width = 119
              Height = 13
              Align = alTop
              Caption = #38468#20214#21517#31216
              ExplicitWidth = 48
            end
            object EAppCertAttaName: TEdit
              AlignWithMargins = True
              Left = 5
              Top = 37
              Width = 119
              Height = 21
              Align = alTop
              TabOrder = 0
            end
            object btnAppCertAttaAdd: TButton
              AlignWithMargins = True
              Left = 5
              Top = 95
              Width = 119
              Height = 25
              Align = alTop
              Caption = #20445#23384#38468#20214
              ImageIndex = 9
              ImageMargins.Left = 5
              Images = imgBtn
              TabOrder = 1
              OnClick = btnAppCertAttaAddClick
              ExplicitTop = 64
            end
            object btnAppCertAttaDel: TButton
              AlignWithMargins = True
              Left = 5
              Top = 126
              Width = 119
              Height = 25
              Align = alTop
              Caption = #21024#38500#38468#20214
              ImageIndex = 2
              ImageMargins.Left = 5
              Images = imgBtn
              TabOrder = 2
              OnClick = btnAppCertAttaDelClick
              ExplicitLeft = 3
              ExplicitTop = 95
            end
            object btnAppCertAttaRename: TButton
              AlignWithMargins = True
              Left = 5
              Top = 64
              Width = 119
              Height = 25
              Align = alTop
              Caption = #26356#25913#21517#31216
              ImageIndex = 48
              ImageMargins.Left = 5
              Images = imgBtn
              TabOrder = 3
              OnClick = btnAppCertAttaRenameClick
              ExplicitLeft = 10
              ExplicitTop = 72
            end
          end
        end
      end
      object GroupBox6: TGroupBox
        Left = 0
        Top = 0
        Width = 549
        Height = 719
        Align = alClient
        Caption = #31995#32479#21442#25968
        TabOrder = 2
        ExplicitLeft = 8
        ExplicitWidth = 185
        object lstSysCert: TValueListEditor
          Left = 2
          Top = 15
          Width = 545
          Height = 545
          Align = alClient
          KeyOptions = [keyEdit, keyAdd, keyDelete, keyUnique]
          Strings.Strings = (
            'SYS_NAME=VON_WIN_'
            'ApplicationTitle=VonWindows'#24320#28304#31995#32479
            'SupportVersion=6.0.*.*'
            'CurrentVersion=6.0.0'
            'AutoUpgrade=false'
            'UpgradeUrl=TCP,127.0.0.1,9001'
            'CheckNetMac=true'
            
              'DBConnectString=Provider=SQLOLEDB.1;Integrated Security=SSPI;Per' +
              'sist Security Info=False;Initial Catalog=VonWindows;Data Source=' +
              '.')
          TabOrder = 0
          TitleCaptions.Strings = (
            #39033#30446
            #20869#23481)
          ExplicitWidth = 181
          ExplicitHeight = 575
          ColWidths = (
            150
            389)
        end
        object Panel3: TPanel
          Left = 2
          Top = 560
          Width = 545
          Height = 157
          Align = alBottom
          Caption = 'Panel4'
          ShowCaption = False
          TabOrder = 1
          object lstSysAttachment: TListView
            Left = 1
            Top = 1
            Width = 414
            Height = 155
            Align = alClient
            Columns = <>
            LargeImages = ImgIcon
            SmallImages = imgBtn
            TabOrder = 0
            OnClick = lstSysAttachmentClick
            ExplicitLeft = -5
            ExplicitTop = 6
          end
          object GroupBox7: TGroupBox
            Left = 415
            Top = 1
            Width = 129
            Height = 155
            Align = alRight
            Caption = #38468#20214
            TabOrder = 1
            ExplicitLeft = 51
            ExplicitHeight = 125
            object Label16: TLabel
              AlignWithMargins = True
              Left = 5
              Top = 18
              Width = 119
              Height = 13
              Align = alTop
              Caption = #38468#20214#21517#31216
              ExplicitWidth = 48
            end
            object ESysCertAttaName: TEdit
              AlignWithMargins = True
              Left = 5
              Top = 37
              Width = 119
              Height = 21
              Align = alTop
              TabOrder = 0
            end
            object btnSysCertAttaAdd: TButton
              AlignWithMargins = True
              Left = 5
              Top = 95
              Width = 119
              Height = 25
              Align = alTop
              Caption = #20445#23384#38468#20214
              ImageIndex = 9
              ImageMargins.Left = 5
              Images = imgBtn
              TabOrder = 1
              OnClick = btnSysCertAttaAddClick
              ExplicitTop = 64
            end
            object btnSysCertAttaDel: TButton
              AlignWithMargins = True
              Left = 5
              Top = 126
              Width = 119
              Height = 25
              Align = alTop
              Caption = #21024#38500#38468#20214
              ImageIndex = 2
              ImageMargins.Left = 5
              Images = imgBtn
              TabOrder = 2
              OnClick = btnSysCertAttaDelClick
              ExplicitTop = 95
            end
            object btnSysCertAttaRename: TButton
              AlignWithMargins = True
              Left = 5
              Top = 64
              Width = 119
              Height = 25
              Align = alTop
              Caption = #26356#25913#21517#31216
              ImageIndex = 48
              ImageMargins.Left = 5
              Images = imgBtn
              TabOrder = 3
              OnClick = btnSysCertAttaRenameClick
              ExplicitLeft = 10
              ExplicitTop = 72
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #39029#38754#39118#26684
      ImageIndex = 1
      ExplicitWidth = 1004
      ExplicitHeight = 608
      object Panel1: TPanel
        Left = 1038
        Top = 0
        Width = 148
        Height = 719
        Align = alRight
        Caption = 'Panel1'
        ShowCaption = False
        TabOrder = 0
        ExplicitLeft = 1035
        object btnLoginLoad: TButton
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 140
          Height = 25
          Align = alTop
          Caption = #21152#36733
          TabOrder = 0
          OnClick = btnLoginLoadClick
        end
        object btnLoginPublished: TButton
          AlignWithMargins = True
          Left = 4
          Top = 690
          Width = 140
          Height = 25
          Align = alBottom
          Caption = #21457#24067
          TabOrder = 1
          OnClick = btnLoginPublishedClick
          ExplicitTop = 579
        end
        object GroupBox1: TGroupBox
          Left = 1
          Top = 264
          Width = 146
          Height = 399
          Align = alTop
          Caption = #30331#24405#39029#38754#30028#38754#35774#35745
          TabOrder = 2
          ExplicitTop = 258
          object Label14: TLabel
            AlignWithMargins = True
            Left = 5
            Top = 142
            Width = 136
            Height = 13
            Align = alTop
            Caption = #24403#21069#39029#38754#32452#20214
            ExplicitWidth = 72
          end
          object chkLoginHide: TCheckBox
            AlignWithMargins = True
            Left = 5
            Top = 188
            Width = 136
            Height = 17
            Align = alTop
            Caption = #26159#21542#38544#21547
            TabOrder = 0
            OnClick = chkLoginHideClick
            ExplicitLeft = 2
            ExplicitTop = 28
            ExplicitWidth = 142
          end
          object GridPanel1: TGridPanel
            AlignWithMargins = True
            Left = 5
            Top = 211
            Width = 136
            Height = 183
            Align = alClient
            Caption = 'GridPanel1'
            ColumnCollection = <
              item
                Value = 32.432432432432440000
              end
              item
                Value = 67.567567567567560000
              end>
            ControlCollection = <
              item
                Column = 0
                Control = Label5
                Row = 0
              end
              item
                Column = 1
                Control = ELoginItemX
                Row = 0
              end
              item
                Column = 0
                Control = Label6
                Row = 1
              end
              item
                Column = 1
                Control = ELoginItemY
                Row = 1
              end
              item
                Column = 0
                Control = Label7
                Row = 2
              end
              item
                Column = 1
                Control = ELoginItemW
                Row = 2
              end
              item
                Column = 0
                Control = Label8
                Row = 3
              end
              item
                Column = 1
                Control = ELoginItemH
                Row = 3
              end
              item
                Column = 0
                Control = Label9
                Row = 4
              end
              item
                Column = 1
                Control = ELoginItemFontName
                Row = 4
              end
              item
                Column = 0
                Control = Label10
                Row = 5
              end
              item
                Column = 1
                Control = ELoginItemFontSize
                Row = 5
              end
              item
                Column = 0
                Control = Label11
                Row = 6
              end
              item
                Column = 1
                Control = ELoginItemFontColor
                Row = 6
              end>
            RowCollection = <
              item
                Value = 14.280000000000000000
              end
              item
                Value = 14.280000000000000000
              end
              item
                Value = 14.280000000000000000
              end
              item
                Value = 14.280000000000000000
              end
              item
                Value = 14.280000000000000000
              end
              item
                Value = 14.280000000000000000
              end
              item
                Value = 14.320000000000000000
              end>
            ShowCaption = False
            TabOrder = 1
            ExplicitLeft = 2
            ExplicitTop = 45
            ExplicitWidth = 142
            ExplicitHeight = 166
            object Label5: TLabel
              Left = 1
              Top = 1
              Width = 43
              Height = 26
              Align = alClient
              Caption = #22352#26631'X'
              Layout = tlCenter
              ExplicitWidth = 30
              ExplicitHeight = 13
            end
            object ELoginItemX: TSpinEdit
              Left = 44
              Top = 1
              Width = 91
              Height = 26
              Align = alClient
              MaxValue = 0
              MinValue = 0
              TabOrder = 0
              Value = 0
              OnChange = ELoginItemXChange
              ExplicitLeft = 46
              ExplicitWidth = 95
              ExplicitHeight = 23
            end
            object Label6: TLabel
              Left = 1
              Top = 27
              Width = 43
              Height = 26
              Align = alClient
              Caption = #22352#26631'Y'
              Layout = tlCenter
              ExplicitTop = 24
              ExplicitWidth = 30
              ExplicitHeight = 13
            end
            object ELoginItemY: TSpinEdit
              Left = 44
              Top = 27
              Width = 91
              Height = 26
              Align = alClient
              MaxValue = 0
              MinValue = 0
              TabOrder = 1
              Value = 0
              OnChange = ELoginItemYChange
              ExplicitLeft = 46
              ExplicitTop = 24
              ExplicitWidth = 95
              ExplicitHeight = 24
            end
            object Label7: TLabel
              Left = 1
              Top = 53
              Width = 43
              Height = 26
              Align = alClient
              Caption = #23485#24230
              Layout = tlCenter
              ExplicitTop = 48
              ExplicitWidth = 24
              ExplicitHeight = 13
            end
            object ELoginItemW: TSpinEdit
              Left = 44
              Top = 53
              Width = 91
              Height = 26
              Align = alClient
              MaxValue = 0
              MinValue = 0
              TabOrder = 2
              Value = 0
              OnChange = ELoginItemWChange
              ExplicitLeft = 46
              ExplicitTop = 48
              ExplicitWidth = 95
              ExplicitHeight = 23
            end
            object Label8: TLabel
              Left = 1
              Top = 79
              Width = 43
              Height = 25
              Align = alClient
              Caption = #39640#24230
              Layout = tlCenter
              ExplicitTop = 71
              ExplicitWidth = 24
              ExplicitHeight = 13
            end
            object ELoginItemH: TSpinEdit
              Left = 44
              Top = 79
              Width = 91
              Height = 25
              Align = alClient
              MaxValue = 0
              MinValue = 0
              TabOrder = 3
              Value = 0
              OnChange = ELoginItemHChange
              ExplicitLeft = 46
              ExplicitTop = 71
              ExplicitWidth = 95
              ExplicitHeight = 24
            end
            object Label9: TLabel
              Left = 1
              Top = 104
              Width = 43
              Height = 26
              Align = alClient
              Caption = #23383#20307
              Layout = tlCenter
              ExplicitTop = 95
              ExplicitWidth = 24
              ExplicitHeight = 13
            end
            object ELoginItemFontName: TEdit
              Left = 44
              Top = 104
              Width = 91
              Height = 26
              Align = alClient
              TabOrder = 4
              Text = #23435#20307
              OnExit = ELoginItemFontNameExit
              ExplicitLeft = 46
              ExplicitTop = 95
              ExplicitWidth = 95
              ExplicitHeight = 21
            end
            object Label10: TLabel
              Left = 1
              Top = 130
              Width = 43
              Height = 26
              Align = alClient
              Caption = #23383#21495
              Layout = tlCenter
              ExplicitTop = 118
              ExplicitWidth = 24
              ExplicitHeight = 13
            end
            object ELoginItemFontSize: TSpinEdit
              Left = 44
              Top = 130
              Width = 91
              Height = 26
              Align = alClient
              MaxValue = 0
              MinValue = 0
              TabOrder = 5
              Value = 0
              OnChange = ELoginItemFontSizeChange
              ExplicitLeft = 46
              ExplicitTop = 118
              ExplicitWidth = 95
              ExplicitHeight = 24
            end
            object Label11: TLabel
              Left = 1
              Top = 156
              Width = 43
              Height = 26
              Align = alClient
              Caption = #39068#33394
              Layout = tlCenter
              ExplicitTop = 142
              ExplicitWidth = 24
              ExplicitHeight = 13
            end
            object ELoginItemFontColor: TColorBox
              Left = 44
              Top = 156
              Width = 91
              Height = 22
              Align = alClient
              TabOrder = 6
              OnChange = ELoginItemFontColorChange
              ExplicitLeft = 46
              ExplicitTop = 142
              ExplicitWidth = 95
            end
          end
          object ELoginComName: TComboBox
            AlignWithMargins = True
            Left = 5
            Top = 161
            Width = 136
            Height = 21
            Align = alTop
            Style = csDropDownList
            TabOrder = 2
            ExplicitLeft = 2
            ExplicitTop = 15
            ExplicitWidth = 142
          end
          object btnLoginOkBtn: TButton
            AlignWithMargins = True
            Left = 5
            Top = 18
            Width = 136
            Height = 25
            Align = alTop
            Caption = #26356#25442#30830#23450#25353#38062
            TabOrder = 3
            OnClick = btnLoginOkBtnClick
            ExplicitTop = 5
          end
          object btnLoginLoginBtn: TButton
            AlignWithMargins = True
            Left = 5
            Top = 49
            Width = 136
            Height = 25
            Align = alTop
            Caption = #26356#25442#30331#24405#25353#38062
            TabOrder = 4
            OnClick = btnLoginLoginBtnClick
            ExplicitTop = 36
          end
          object btnLoginCancelBtn: TButton
            AlignWithMargins = True
            Left = 5
            Top = 80
            Width = 136
            Height = 25
            Align = alTop
            Caption = #26356#25442#25918#24323#25353#38062
            TabOrder = 5
            OnClick = btnLoginCancelBtnClick
            ExplicitTop = 67
          end
          object btnLoginBackground: TButton
            AlignWithMargins = True
            Left = 5
            Top = 111
            Width = 136
            Height = 25
            Align = alTop
            Caption = #26356#25442#32972#26223
            TabOrder = 6
            OnClick = btnLoginBackgroundClick
            ExplicitTop = 98
          end
        end
        object GroupBox3: TGroupBox
          Left = 1
          Top = 32
          Width = 146
          Height = 232
          Align = alTop
          Caption = #31995#32479#30028#38754#21442#25968
          TabOrder = 3
          object Label12: TLabel
            AlignWithMargins = True
            Left = 5
            Top = 18
            Width = 136
            Height = 13
            Align = alTop
            Caption = #31995#32479#23383#20307
            ExplicitLeft = 6
            ExplicitTop = 9
            ExplicitWidth = 48
          end
          object Label25: TLabel
            AlignWithMargins = True
            Left = 5
            Top = 64
            Width = 136
            Height = 13
            Align = alTop
            Caption = #31995#32479#23383#21495
            ExplicitLeft = 6
            ExplicitTop = 56
            ExplicitWidth = 48
          end
          object Label13: TLabel
            AlignWithMargins = True
            Left = 5
            Top = 111
            Width = 136
            Height = 13
            Align = alTop
            Caption = #23383#20307#39068#33394
            ExplicitLeft = 6
            ExplicitTop = 104
            ExplicitWidth = 48
          end
          object Label20: TLabel
            AlignWithMargins = True
            Left = 5
            Top = 158
            Width = 136
            Height = 13
            Align = alTop
            Caption = #32452#20214#38388#38548
            ExplicitTop = 201
          end
          object ESysFontName: TComboBox
            AlignWithMargins = True
            Left = 5
            Top = 37
            Width = 136
            Height = 21
            Align = alTop
            TabOrder = 0
            Text = 'Default'
            ExplicitLeft = -24
            ExplicitTop = 28
            ExplicitWidth = 170
          end
          object ESysFontSize: TSpinEdit
            AlignWithMargins = True
            Left = 5
            Top = 83
            Width = 136
            Height = 22
            Align = alTop
            MaxValue = 0
            MinValue = 0
            TabOrder = 1
            Value = 8
            ExplicitLeft = 6
            ExplicitTop = 75
            ExplicitWidth = 91
          end
          object ESysFontColor: TColorBox
            AlignWithMargins = True
            Left = 5
            Top = 130
            Width = 136
            Height = 22
            Align = alTop
            TabOrder = 2
            ExplicitLeft = 6
            ExplicitTop = 123
            ExplicitWidth = 91
          end
          object Panel6: TPanel
            Left = 2
            Top = 174
            Width = 142
            Height = 51
            Align = alTop
            BevelOuter = bvNone
            Caption = 'Panel6'
            ShowCaption = False
            TabOrder = 3
            ExplicitTop = 228
            object Bevel2: TBevel
              AlignWithMargins = True
              Left = 24
              Top = 5
              Width = 97
              Height = 36
              Shape = bsFrame
            end
            object ESysMarginTop: TSpinEdit
              Left = 50
              Top = 1
              Width = 45
              Height = 22
              MaxValue = 0
              MinValue = 0
              TabOrder = 0
              Value = 3
            end
            object ESysMarginLeft: TSpinEdit
              Left = 3
              Top = 11
              Width = 45
              Height = 22
              MaxValue = 0
              MinValue = 0
              TabOrder = 1
              Value = 3
            end
            object ESysMarginRight: TSpinEdit
              Left = 97
              Top = 11
              Width = 45
              Height = 22
              MaxValue = 0
              MinValue = 0
              TabOrder = 2
              Value = 3
            end
            object ESysMarginBottom: TSpinEdit
              Left = 50
              Top = 29
              Width = 45
              Height = 22
              MaxValue = 0
              MinValue = 0
              TabOrder = 3
              Value = 3
            end
          end
        end
      end
      object ScrollBox1: TScrollBox
        Left = 0
        Top = 0
        Width = 1038
        Height = 719
        Align = alClient
        TabOrder = 1
        ExplicitWidth = 856
        ExplicitHeight = 608
        object imgBackground: TImage
          Left = -2
          Top = 0
          Width = 105
          Height = 105
          AutoSize = True
        end
        object lbTitle2: TLabel
          AlignWithMargins = True
          Left = 122
          Top = 52
          Width = 304
          Height = 37
          Alignment = taCenter
          Caption = #26234#33021#36130#21153#20998#26512#31995#32479
          Font.Charset = DEFAULT_CHARSET
          Font.Color = cl3DDkShadow
          Font.Height = -37
          Font.Name = #38582#20070
          Font.Style = []
          ParentFont = False
          Transparent = True
          Layout = tlCenter
          OnMouseDown = lbTitle1MouseDown
          OnMouseMove = lbTitle1MouseMove
        end
        object lbTitle1: TLabel
          Left = 122
          Top = 48
          Width = 304
          Height = 37
          Alignment = taCenter
          Caption = #26234#33021#36130#21153#20998#26512#31995#32479
          DragCursor = crHandPoint
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -37
          Font.Name = #38582#20070
          Font.Style = []
          ParentFont = False
          Transparent = True
          Layout = tlCenter
          OnMouseDown = lbTitle1MouseDown
          OnMouseMove = lbTitle1MouseMove
        end
        object lbPrompt: TLabel
          Left = 7
          Top = 169
          Width = 48
          Height = 13
          Caption = #29992#25143#30331#24405
          DragCursor = crHandPoint
          Transparent = True
          OnMouseDown = lbTitle1MouseDown
          OnMouseMove = lbTitle1MouseMove
        end
        object lb1: TLabel
          Left = 223
          Top = 279
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = #29992#25143#21517
          DragCursor = crHandPoint
          Transparent = True
          OnMouseDown = lbTitle1MouseDown
          OnMouseMove = lbTitle1MouseMove
        end
        object lb2: TLabel
          Left = 211
          Top = 304
          Width = 48
          Height = 13
          Alignment = taRightJustify
          Caption = #30331#24405#21475#20196
          DragCursor = crHandPoint
          Transparent = True
          OnMouseDown = lbTitle1MouseDown
          OnMouseMove = lbTitle1MouseMove
        end
        object imgLogin: TImage
          Left = 400
          Top = 271
          Width = 86
          Height = 24
          Cursor = crHandPoint
          AutoSize = True
          DragCursor = crHandPoint
          Picture.Data = {
            07544269746D617096180000424D961800000000000036000000280000005600
            000018000000010018000000000060180000120B0000120B0000000000000000
            0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FF0000FF00FFFF00FFFF00FF0E8E4E068244037E40037E40037E40037E
            40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E4003
            7E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40
            037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E
            40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E4003
            7E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40
            037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E
            40037E40037E40037E40037E40037E40037E40037E40037E400682440E8E4EFF
            00FFFF00FFFF00FF0000FF00FFFF00FF06824421A8652AB5702FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762AB570
            21A865068244FF00FFFF00FF0000FF00FF0E8E4E21A8652FBC762AB570129453
            068244037E40037E41037E41037E41037E41037E41037E40037E41037E40037E
            41037E40037E41037E40037E40037E40037E40037E41037E41037E40037E4003
            7E41037E41037E40037E40037E41037E41037E40037E40037E40037E40037E40
            037E41037E40037E41037E40037E40037E40037E41037E41037E40037E41037E
            41037E41037E41037E40037E40037E41037E40037E40037E41037E40037E4003
            7E40037E40037E41037E41037E40037E41037E40037E40037E41037E41037E40
            037E40037E41037E41037E41037E41037E41037E41037E41037E400682441294
            532AB5702FBC7621A8650E8E4EFF00FF0000FF00FF0682442DBA7427B06C0682
            44037E41037E41037E40037E41037E40037E41037E40037E40037E40037E4103
            7E41037E41037E40037E40037E41037E40037E41037E40037E40037E40037E41
            037E40037E40037E40037E41037E40037E41037E40037E40037E40037E41037E
            41037E41037E40037E40037E41037E40037E40037E40037E41037E40037E4103
            7E40037E40037E40037E41037E40037E41037E40037E40037E40037E41037E41
            037E41037E40037E40037E41037E40037E41037E41037E40037E40037E40037E
            40037E41037E40037E41037E41037E41037E41037E40037E41037E41037E4003
            7E41037E4006824427B06C2CB772068244FF00FF0000FF00FF037E402FBC7612
            9453037E41037E40037E40037E40037E40037E41037E40037E40037E41037E41
            037E41037E41037E41037E40037E41037E40037E41037E40037E41037E41037E
            41037E40037E40037E41037E41037E41037E41037E40037E40037E41037E4103
            7E41037E41037E41037E40037E41037E40037E40037E41037E41037E41037E41
            037E40037E40037E41037E41037E41037E41037E40037E40037E41037E41037E
            41037E41037E41037E40037E41037E40037E41037E40037E40037E40037E4103
            7E41037E40037E41037E41037E40037E41037E40037E41037E40037E40037E40
            037E41037E40037E41037E411294532FBC76037E40FF00FF0000FF00FF037E40
            2FBC76068244068244037E41068244037E41068244037E41068244037E410682
            44037E41068244037E41068244037E41068244037E41037E41068244037E4106
            8244037E41068244037E41068244037E41068244037E41068244037E41068244
            037E41068244037E41068244037E41037E41068244037E41068244037E410682
            44037E41068244037E41068244037E41068244037E41068244037E4106824403
            7E41068244037E41068244037E41068244037E41037E41068244037E41068244
            037E41037E41068244037E41068244037E41068244068244037E41068244037E
            41068244037E41068244037E410682440682442FBC76037E40FF00FF0000FF00
            FF037E402FBC760A88490A88490A88490A88490A88490A88490A88490682440A
            88490A88490A88490A88490A88490A88490A88490A88490A8849000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            000A88490A88490A88490A88490A88490A88490A88490682440A88490A88490A
            88490A88490A88490682440A88490A88490A88490A88490A88490682440A8849
            0000000000000000000A88490A88490A88490000000000000A88490A88490A88
            490A88490682440A88490A88490A88490A88490A88490682440A88490A88490A
            88490A88490A88490682440A88490A88490A88490A88492FBC76037E40FF00FF
            0000FF00FF037E402FBC76129453129453129453129453129453129453129453
            1294531294530E8E4E1294531294531294531294531294531294531294531294
            5312945312945312945300000000000012945300000000000012945312945312
            94531294530E8E4E129453129453129453129453129453129453129453129453
            0E8E4E1294531294531294531294531294530E8E4E1294530000000000000000
            001294530E8E4E00000000000012945312945300000000000000000000000012
            9453129453129453129453129453129453129453129453129453129453129453
            1294531294531294531294531294531294531294531294531294532FBC76037E
            40FF00FF0000FF00FF037E402FBC76199D5B199D5B199D5B199D5B199D5B199D
            5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B19
            9D5B199D5B199D5B199D5B000000000000199D5B199D5B199D5B000000000000
            199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D
            5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B19
            9D5B000000000000000000000000000000199D5B000000000000199D5B199D5B
            199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D
            5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B2F
            BC76037E40FF00FF0000FF00FF037E402FBC7621A86521A865199D5B21A86521
            A865199D5B21A86521A865199D5B21A86521A865199D5B21A86521A865199D5B
            21A86521A865199D5B21A86521A8650000000000000000000000000000000000
            0000000021A86521A865199D5B21A86521A865199D5B21A86521A865199D5B21
            A86521A865199D5B21A86521A865199D5B21A86521A865199D5B21A86521A865
            199D5B21A86521A865199D5B000000000000000000000000000000199D5B21A8
            6521A865199D5B21A865199D5B21A86521A865199D5B21A86521A865199D5B21
            A86521A865199D5B21A86521A865199D5B21A86521A865199D5B21A86521A865
            199D5B2FBC76037E40FF00FF0000FF00FF037E402FBC7621A86521A86521A865
            21A86521A86521A86521A86521A86521A86521A86521A86521A86521A86521A8
            6521A86521A86521A86521A86521A86521A86500000000000021A86521A86521
            A86500000000000021A86521A86521A86521A86521A86521A86521A86521A865
            21A86521A86521A86521A86521A86521A86521A86521A86521A86521A86521A8
            6521A86521A86521A86521A86500000000000000000000000000000000000000
            000021A86521A86521A86521A86521A86521A86521A86521A86521A86521A865
            21A86521A86521A86521A86521A86521A86521A86521A86521A86521A86521A8
            6521A86521A8652FBC76037E40FF00FF0000FF00FF037E402FBC7624AC6824AC
            6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824
            AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC68000000000000000000
            00000000000000000000000024AC6824AC6824AC6824AC6824AC6824AC6824AC
            6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824
            AC6824AC6824AC6824AC6824AC6800000000000024AC6800000000000024AC68
            24AC6800000000000024AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC
            6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824
            AC6824AC6824AC6824AC682FBC76037E40FF00FF0000FF00FF037E402FBC7627
            B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C
            27B06C27B06C27B06C27B06C27B06C27B06C00000000000000000027B06C27B0
            6C27B06C27B06C27B06C27B06C27B06C00000000000000000027B06C27B06C27
            B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C
            27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C0000000000
            0027B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27
            B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C
            27B06C27B06C27B06C27B06C27B06C2FBC76037E40FF00FF0000FF00FF037E40
            2FBC7629B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B4
            6F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F00000000
            000000000000000000000000000000000000000000000029B46F29B46F29B46F
            29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B4
            6F29B46F29B46F29B46F29B46F29B46F00000000000000000000000000000000
            000000000000000000000000000000000000000000000029B46F29B46F29B46F
            29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B4
            6F29B46F29B46F29B46F29B46F29B46F29B46F2FBC76037E40FF00FF0000FF00
            FF037E402FBC762CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722C
            B7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB772
            2CB7720000000000002CB7722CB7722CB7720000000000002CB7722CB7722CB7
            722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722C
            B7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB772
            2CB7722CB7722CB7722CB7720000000000002CB7722CB7722CB7722CB7722CB7
            722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722C
            B7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722FBC76037E40FF00FF
            0000FF00FF037E402FBC762DBA742DBA742DBA742DBA742DBA742DBA742DBA74
            2DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA
            742DBA740000000000000000000000002DBA7400000000000000000000000000
            00002DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA74
            2DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA
            740000000000000000000000000000000000000000002DBA742DBA742DBA742D
            BA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA74
            2DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742FBC76037E
            40FF00FF0000FF00FF037E402FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC76000000000000000000000000000000000000000000000000000000
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC760000000000002FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC76037E40FF00FF0000FF00FF037E402FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC760000000000002FBC
            760000000000002FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC760000000000000000000000000000000000000000000000002FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC76037E40FF00FF0000FF00FF0682442CB7722FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762CB772068244FF00FF0000FF00FF0E8E4E21A8652FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC7621A8650E8E4EFF00FF0000FF00FFFF00FF06824421
            A8652AB5702FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762AB57021A865068244FF00FFFF00FF0000FF00FFFF00FF
            FF00FF0E8E4E068244037E40037E40037E40037E40037E40037E40037E40037E
            40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E4003
            7E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40
            037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E
            40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E4003
            7E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40
            037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E
            40037E40037E40037E40037E400682440E8E4EFF00FFFF00FFFF00FF0000FF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            0000}
          Transparent = True
          OnMouseDown = lbTitle1MouseDown
          OnMouseMove = lbTitle1MouseMove
        end
        object imgCancel: TImage
          Left = 400
          Top = 301
          Width = 86
          Height = 24
          Cursor = crHandPoint
          AutoSize = True
          DragCursor = crHandPoint
          Picture.Data = {
            07544269746D617096180000424D961800000000000036000000280000005600
            000018000000010018000000000060180000120B0000120B0000000000000000
            0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FF0000FF00FFFF00FFFF00FF0E8E4E068244037E40037E40037E40037E
            40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E4003
            7E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40
            037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E
            40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E4003
            7E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40
            037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E
            40037E40037E40037E40037E40037E40037E40037E40037E400682440E8E4EFF
            00FFFF00FFFF00FF0000FF00FFFF00FF06824421A8652AB5702FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762AB570
            21A865068244FF00FFFF00FF0000FF00FF0E8E4E21A8652FBC762AB570129453
            068244037E40037E41037E41037E41037E41037E41037E40037E41037E40037E
            41037E40037E41037E40037E40037E40037E40037E41037E41037E40037E4003
            7E41037E41037E40037E40037E41037E41037E40037E40037E40037E40037E40
            037E41037E40037E41037E40037E40037E40037E41037E41037E40037E41037E
            41037E41037E41037E40037E40037E41037E40037E40037E41037E40037E4003
            7E40037E40037E41037E41037E40037E41037E40037E40037E41037E41037E40
            037E40037E41037E41037E41037E41037E41037E41037E41037E400682441294
            532AB5702FBC7621A8650E8E4EFF00FF0000FF00FF0682442DBA7427B06C0682
            44037E41037E41037E40037E41037E40037E41037E40037E40037E40037E4103
            7E41037E41037E40037E40037E41037E40037E41037E40037E40037E40037E41
            037E40037E40037E40037E41037E40037E41037E40037E40037E40037E41037E
            41037E41037E40037E40037E41037E40037E40037E40037E41037E40037E4103
            7E40037E40037E40037E41037E40037E41037E40037E40037E40037E41037E41
            037E41037E40037E40037E41037E40037E41037E41037E40037E40037E40037E
            40037E41037E40037E41037E41037E41037E41037E40037E41037E41037E4003
            7E41037E4006824427B06C2CB772068244FF00FF0000FF00FF037E402FBC7612
            9453037E41037E40037E40037E40037E40037E41037E40037E40037E41037E41
            037E41037E41037E41037E40037E41037E40037E41037E40037E41037E41037E
            41037E40037E40037E41037E41037E41037E41037E40037E40037E41037E4103
            7E41037E41037E41037E40037E41037E40037E40037E41037E41037E41037E41
            037E40037E40037E41037E41037E41037E41037E40037E40037E41037E41037E
            41037E41037E41037E40037E41037E40037E41037E40037E40037E40037E4103
            7E41037E40037E41037E41037E40037E41037E40037E41037E40037E40037E40
            037E41037E40037E41037E411294532FBC76037E40FF00FF0000FF00FF037E40
            2FBC76068244068244037E41068244037E41068244037E41068244037E410682
            44037E41068244037E41068244037E41068244037E41037E41068244037E4106
            8244037E41068244037E41068244037E41068244037E41068244037E41068244
            037E41068244037E41068244037E41037E41068244037E41068244037E410682
            44037E41068244037E41068244037E41068244037E41068244037E4106824403
            7E41068244037E41068244037E41068244037E41037E41068244037E41068244
            037E41037E41068244037E41068244037E41068244068244037E41068244037E
            41068244037E41068244037E410682440682442FBC76037E40FF00FF0000FF00
            FF037E402FBC760A88490A88490A88490A88490A88490A88490A88490682440A
            88490A88490A88490A88490A88490A88490A88490A88490A88490A8849000000
            0000000A88490000000000000000000000000A88490A88490A88490682440000
            000000000A88490A88490A88490A88490A88490A88490682440A88490A88490A
            88490A88490A88490682440A88490A88490A88490A88490A8849000000000000
            0000000A88490A88490A88490A88490000000000000A88490A88490A88490A88
            490A88490682440A88490A88490A88490A88490A88490682440A88490A88490A
            88490A88490A88490682440A88490A88490A88490A88492FBC76037E40FF00FF
            0000FF00FF037E402FBC76129453129453129453129453129453129453129453
            1294531294530E8E4E1294531294531294531294531294531294531294531294
            5312945300000000000012945300000000000000000000000012945312945300
            00000000000E8E4E129453129453129453129453129453129453129453129453
            0E8E4E1294531294531294531294531294530E8E4E1294531294531294531294
            5312945300000000000012945312945312945300000000000012945312945312
            9453129453129453129453129453129453129453129453129453129453129453
            1294531294531294531294531294531294531294531294531294532FBC76037E
            40FF00FF0000FF00FF037E402FBC76199D5B199D5B199D5B199D5B199D5B199D
            5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B19
            9D5B199D5B199D5B000000000000199D5B000000000000199D5B000000000000
            000000000000199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D
            5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B19
            9D5B199D5B199D5B199D5B000000000000199D5B199D5B000000000000199D5B
            199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D
            5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B2F
            BC76037E40FF00FF0000FF00FF037E402FBC7621A86521A865199D5B21A86521
            A865199D5B21A86521A865199D5B21A86521A865199D5B21A86521A865199D5B
            21A86521A865199D5B21A86521A86500000000000000000000000021A86521A8
            6500000000000021A865199D5B21A86521A865199D5B21A86521A865199D5B21
            A86521A865199D5B21A86521A865199D5B21A86521A865199D5B21A86521A865
            199D5B21A86521A865199D5B21A86500000000000021A86521A8650000000000
            0021A865199D5B21A865199D5B21A86521A865199D5B21A86521A865199D5B21
            A86521A865199D5B21A86521A865199D5B21A86521A865199D5B21A86521A865
            199D5B2FBC76037E40FF00FF0000FF00FF037E402FBC7621A86521A86521A865
            21A86521A86521A86521A86521A86521A86521A86521A86521A86521A86521A8
            6521A86521A86521A86521A86521A86521A86500000000000000000000000021
            A86500000000000000000000000021A86521A86521A86521A86521A86521A865
            21A86521A86521A86521A86521A86521A86521A86521A86521A86521A86521A8
            6521A86521A86500000000000000000000000000000000000000000000000000
            000000000000000000000000000021A86521A86521A86521A86521A86521A865
            21A86521A86521A86521A86521A86521A86521A86521A86521A86521A86521A8
            6521A86521A8652FBC76037E40FF00FF0000FF00FF037E402FBC7624AC6824AC
            6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824
            AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC68000000000000000000
            00000024AC6800000000000000000000000024AC6824AC6824AC6824AC6824AC
            6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824
            AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6800000000000024AC68
            24AC6800000000000024AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC
            6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824
            AC6824AC6824AC6824AC682FBC76037E40FF00FF0000FF00FF037E402FBC7627
            B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C
            27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C0000000000
            0000000000000027B06C00000000000027B06C00000000000027B06C27B06C27
            B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C
            27B06C27B06C27B06C27B06C27B06C27B06C27B06C0000000000000000000000
            0000000000000000000000000000000027B06C27B06C27B06C27B06C27B06C27
            B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C
            27B06C27B06C27B06C27B06C27B06C2FBC76037E40FF00FF0000FF00FF037E40
            2FBC7629B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B4
            6F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F00
            000000000029B46F29B46F00000000000029B46F29B46F00000000000029B46F
            29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B4
            6F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F00000000
            000029B46F29B46F29B46F00000000000029B46F29B46F29B46F29B46F29B46F
            29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B4
            6F29B46F29B46F29B46F29B46F29B46F29B46F2FBC76037E40FF00FF0000FF00
            FF037E402FBC762CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722C
            B7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB772
            2CB7720000000000002CB7722CB7720000000000002CB7722CB7720000000000
            002CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722C
            B7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB772
            2CB7720000000000002CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7
            722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722C
            B7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722FBC76037E40FF00FF
            0000FF00FF037E402FBC762DBA742DBA742DBA742DBA742DBA742DBA742DBA74
            2DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA
            7400000000000000000000000000000000000000000000000000000000000000
            00000000000000002DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA74
            2DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA740000
            000000000000000000000000000000000000000000000000000000000000002D
            BA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA74
            2DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742FBC76037E
            40FF00FF0000FF00FF037E402FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC760000000000002FBC762FBC76000000000000
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC760000000000002FBC76000000000000
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC76037E40FF00FF0000FF00FF037E402FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC760000000000002FBC762FBC762FBC760000
            000000002FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC760000000000002FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC76037E40FF00FF0000FF00FF0682442CB7722FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762CB772068244FF00FF0000FF00FF0E8E4E21A8652FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC7621A8650E8E4EFF00FF0000FF00FFFF00FF06824421
            A8652AB5702FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762AB57021A865068244FF00FFFF00FF0000FF00FFFF00FF
            FF00FF0E8E4E068244037E40037E40037E40037E40037E40037E40037E40037E
            40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E4003
            7E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40
            037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E
            40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E4003
            7E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40
            037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E
            40037E40037E40037E40037E400682440E8E4EFF00FFFF00FFFF00FF0000FF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            0000}
          Transparent = True
          OnMouseDown = lbTitle1MouseDown
          OnMouseMove = lbTitle1MouseMove
        end
        object imgOK: TImage
          Left = 400
          Top = 241
          Width = 86
          Height = 24
          Cursor = crHandPoint
          AutoSize = True
          DragCursor = crHandPoint
          Picture.Data = {
            07544269746D617096180000424D961800000000000036000000280000005600
            000018000000010018000000000060180000120B0000120B0000000000000000
            0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FF0000FF00FFFF00FFFF00FF0E8E4E068244037E40037E40037E40037E
            40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E4003
            7E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40
            037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E
            40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E4003
            7E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40
            037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E
            40037E40037E40037E40037E40037E40037E40037E40037E400682440E8E4EFF
            00FFFF00FFFF00FF0000FF00FFFF00FF06824421A8652AB5702FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762AB570
            21A865068244FF00FFFF00FF0000FF00FF0E8E4E21A8652FBC762AB570129453
            068244037E40037E41037E41037E41037E41037E41037E40037E41037E40037E
            41037E40037E41037E40037E40037E40037E40037E41037E41037E40037E4003
            7E41037E41037E40037E40037E41037E41037E40037E40037E40037E40037E40
            037E41037E40037E41037E40037E40037E40037E41037E41037E40037E41037E
            41037E41037E41037E40037E40037E41037E40037E40037E41037E40037E4003
            7E40037E40037E41037E41037E40037E41037E40037E40037E41037E41037E40
            037E40037E41037E41037E41037E41037E41037E41037E41037E400682441294
            532AB5702FBC7621A8650E8E4EFF00FF0000FF00FF0682442DBA7427B06C0682
            44037E41037E41037E40037E41037E40037E41037E40037E40037E40037E4103
            7E41037E41037E40037E40037E41037E40037E41037E40037E40037E40037E41
            037E40037E40037E40037E41037E40037E41037E40037E40037E40037E41037E
            41037E41037E40037E40037E41037E40037E40037E40037E41037E40037E4103
            7E40037E40037E40037E41037E40037E41037E40037E40037E40037E41037E41
            037E41037E40037E40037E41037E40037E41037E41037E40037E40037E40037E
            40037E41037E40037E41037E41037E41037E41037E40037E41037E41037E4003
            7E41037E4006824427B06C2CB772068244FF00FF0000FF00FF037E402FBC7612
            9453037E41037E40037E40037E40037E40037E41037E40037E40037E41037E41
            037E41037E41037E41037E40037E41037E40037E41037E40037E41037E41037E
            41037E40037E40037E41037E41037E41037E41037E40037E40037E41037E4103
            7E41037E41037E41037E40037E41037E40037E40037E41037E41037E41037E41
            037E40037E40037E41037E41037E41037E41037E40037E40037E41037E41037E
            41037E41037E41037E40037E41037E40037E41037E40037E40037E40037E4103
            7E41037E40037E41037E41037E40037E41037E40037E41037E40037E40037E40
            037E41037E40037E41037E411294532FBC76037E40FF00FF0000FF00FF037E40
            2FBC76068244068244037E41068244037E41068244037E41068244037E410682
            44037E41068244037E41068244037E41068244037E41037E41068244037E4106
            8244037E41068244037E41068244037E41068244037E41068244037E41068244
            037E41068244037E41068244037E41037E41068244037E41068244037E410682
            44037E41068244037E41068244037E41068244037E41068244037E4106824403
            7E41068244037E41068244037E41068244037E41037E41068244037E41068244
            037E41037E41068244037E41068244037E41068244068244037E41068244037E
            41068244037E41068244037E410682440682442FBC76037E40FF00FF0000FF00
            FF037E402FBC760A88490A88490A88490A88490A88490A88490A88490682440A
            88490A88490A88490A88490A88490A88490A88490A88490A88490A88490A8849
            0A88490A88490A88490000000000000A88490A88490000000000000000000000
            000000000A88490A88490A88490A88490A88490A88490682440A88490A88490A
            88490A88490A88490682440A88490A88490A88490A88490000000000000A8849
            0A88490A88490A88490000000000000000000000000000000000000000000A88
            490A88490682440A88490A88490A88490A88490A88490682440A88490A88490A
            88490A88490A88490682440A88490A88490A88490A88492FBC76037E40FF00FF
            0000FF00FF037E402FBC76129453129453129453129453129453129453129453
            1294531294530E8E4E1294531294531294531294531294531294531294531294
            5312945300000000000012945312945300000000000012945300000000000012
            9453000000000000129453129453129453129453129453129453129453129453
            0E8E4E1294531294531294531294531294530E8E4E1294531294531294530000
            000000000E8E4E12945300000000000000000012945312945312945312945312
            9453129453129453129453129453129453129453129453129453129453129453
            1294531294531294531294531294531294531294531294531294532FBC76037E
            40FF00FF0000FF00FF037E402FBC76199D5B199D5B199D5B199D5B199D5B199D
            5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B19
            9D5B199D5B199D5B000000000000000000000000000000000000000000000000
            000000199D5B000000000000199D5B199D5B199D5B199D5B199D5B199D5B199D
            5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B19
            9D5B199D5B000000000000000000000000000000000000199D5B199D5B199D5B
            199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D
            5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B199D5B2F
            BC76037E40FF00FF0000FF00FF037E402FBC7621A86521A865199D5B21A86521
            A865199D5B21A86521A865199D5B21A86521A865199D5B21A86521A865199D5B
            21A86521A865199D5B21A86500000000000021A8650000000000000000000000
            0000000000000000000000000000000021A865199D5B21A86521A865199D5B21
            A86521A865199D5B21A86521A865199D5B21A86521A865199D5B21A86521A865
            199D5B21A86521A865199D5B000000000000199D5B000000000000199D5B21A8
            6521A865199D5B21A865199D5B21A86521A865199D5B21A86521A865199D5B21
            A86521A865199D5B21A86521A865199D5B21A86521A865199D5B21A86521A865
            199D5B2FBC76037E40FF00FF0000FF00FF037E402FBC7621A86521A86521A865
            21A86521A86521A86521A86521A86521A86521A86521A86521A86521A86521A8
            6521A86521A86521A86521A86521A86500000000000021A86500000000000000
            000000000000000000000021A86500000000000021A86521A86521A86521A865
            21A86521A86521A86521A86521A86521A86521A86521A86521A86521A86521A8
            6521A86521A86521A86521A86521A86500000000000021A86500000000000000
            000000000000000021A86521A86521A86521A86521A86521A86521A86521A865
            21A86521A86521A86521A86521A86521A86521A86521A86521A86521A86521A8
            6521A86521A8652FBC76037E40FF00FF0000FF00FF037E402FBC7624AC6824AC
            6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824
            AC6824AC6824AC6824AC6824AC6824AC6800000000000000000024AC68000000
            00000000000000000000000000000000000000000000000024AC6824AC6824AC
            6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824
            AC6824AC6824AC6824AC6824AC6824AC6824AC6800000000000024AC68000000
            00000024AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC
            6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824AC6824
            AC6824AC6824AC6824AC682FBC76037E40FF00FF0000FF00FF037E402FBC7627
            B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C
            27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C0000000000000000
            0000000000000000000000000000000000000027B06C00000000000027B06C27
            B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C
            27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B0
            6C00000000000027B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27
            B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C27B06C
            27B06C27B06C27B06C27B06C27B06C2FBC76037E40FF00FF0000FF00FF037E40
            2FBC7629B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B4
            6F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F00000000
            000029B46F29B46F29B46F00000000000000000000000029B46F000000000000
            29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B4
            6F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F00000000000000
            000000000000000000000000000000000000000029B46F29B46F29B46F29B46F
            29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B46F29B4
            6F29B46F29B46F29B46F29B46F29B46F29B46F2FBC76037E40FF00FF0000FF00
            FF037E402FBC762CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722C
            B7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB772
            2CB7720000000000002CB7720000000000000000000000000000000000000000
            000000002CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722C
            B7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB772000000000000
            2CB7722CB7722CB7722CB7722CB7722CB7722CB7720000000000002CB7722CB7
            722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722C
            B7722CB7722CB7722CB7722CB7722CB7722CB7722CB7722FBC76037E40FF00FF
            0000FF00FF037E402FBC762DBA742DBA742DBA742DBA742DBA742DBA742DBA74
            2DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA
            742DBA742DBA740000000000002DBA742DBA740000000000002DBA7400000000
            00002DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA74
            2DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA740000
            000000000000000000000000000000000000000000000000000000000000002D
            BA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA74
            2DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742DBA742FBC76037E
            40FF00FF0000FF00FF037E402FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC760000000000000000000000000000000000002FBC76000000000000
            0000000000000000002FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC760000000000002FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC76037E40FF00FF0000FF00FF037E402FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC760000
            000000002FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC760000000000002FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC76037E40FF00FF0000FF00FF0682442CB7722FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762CB772068244FF00FF0000FF00FF0E8E4E21A8652FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC7621A8650E8E4EFF00FF0000FF00FFFF00FF06824421
            A8652AB5702FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC
            762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762F
            BC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC762FBC76
            2FBC762FBC762FBC762AB57021A865068244FF00FFFF00FF0000FF00FFFF00FF
            FF00FF0E8E4E068244037E40037E40037E40037E40037E40037E40037E40037E
            40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E4003
            7E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40
            037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E
            40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E4003
            7E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40
            037E40037E40037E40037E40037E40037E40037E40037E40037E40037E40037E
            40037E40037E40037E40037E400682440E8E4EFF00FFFF00FFFF00FF0000FF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            0000}
          Transparent = True
          OnMouseDown = lbTitle1MouseDown
          OnMouseMove = lbTitle1MouseMove
        end
        object lbRemember: TLabel
          Left = 273
          Top = 333
          Width = 72
          Height = 13
          Caption = #35760#24518#29992#25143#20449#24687
          DragCursor = crHandPoint
          Transparent = True
          OnMouseDown = lbTitle1MouseDown
          OnMouseMove = lbTitle1MouseMove
        end
        object Edit1: TEdit
          Left = 273
          Top = 271
          Width = 121
          Height = 21
          DragCursor = crHandPoint
          TabOrder = 0
          OnMouseDown = lbTitle1MouseDown
          OnMouseMove = lbTitle1MouseMove
        end
        object Edit2: TEdit
          Left = 273
          Top = 301
          Width = 121
          Height = 21
          PasswordChar = '*'
          TabOrder = 1
          OnMouseDown = lbTitle1MouseDown
          OnMouseMove = lbTitle1MouseMove
        end
        object chkRemember: TCheckBox
          Left = 253
          Top = 334
          Width = 14
          Height = 14
          DragCursor = crHandPoint
          TabOrder = 2
          OnMouseDown = lbTitle1MouseDown
          OnMouseMove = lbTitle1MouseMove
        end
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 740
    Top = 176
  end
  object SaveDialog1: TSaveDialog
    Left = 740
    Top = 224
  end
  object ImgIcon: TImageList
    Height = 64
    Width = 64
    Left = 444
    Top = 384
    Bitmap = {
      494C010103005800040040004000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000000100004000000001002000000000000000
      0100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F3F2F000DDD9D600D3CEC900D2CD
      C800D2CDC800D2CDC800D2CDC800D2CDC800D2CDC800D2CDC800D2CDC800D2CD
      C800D2CDC800D2CDC800D2CDC800D2CDC800D2CDC800D2CDC800D2CDC800D2CD
      C800D2CDC800D2CDC800D2CDC800D2CDC800D3CECA00DEDBD700F4F2F1000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EEEEEE00DADA
      DA00D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6
      D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6
      D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6
      D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6
      D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6D600D6D6
      D600D6D6D600D6D6D600D6D6D600E2E2E200FBFBFB0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FBFBFA00EDEBE900DEDAD700D9D5D100D9D5
      D100D9D5D100D9D5D100D9D5D100D9D5D100D9D5D100D9D5D100D9D5D100D9D5
      D100D9D5D100D9D5D100D9D5D100D9D5D100D9D5D100D9D5D100D9D5D100D9D5
      D100D9D5D100D9D5D100D9D5D100D9D5D100D9D5D100D9D5D100D9D5D100D9D5
      D100D9D5D100D9D5D100DEDAD700EDEBE900FBFBFA0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FDFDFD00F9F9F900F6F6F600F5F5F500F4F4F400F3F3
      F300F3F3F300F2F2F200F2F2F200F2F2F200B7B3AF0096918A00918C8600918C
      8600918C8600918C8600918C8600918C8600918C8600918C8600918C8600918C
      8600918C8600918C8600918C8600918C8600918C8600918C8600918C8600918C
      8600918C8600918C8600918C8600918C8600918C860098928C00BBB7B300F2F2
      F200F2F2F200F2F2F200F3F3F300F3F3F300F4F4F400F5F5F500F6F6F600F8F8
      F800FCFCFC00FEFEFE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F3F3F300CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCC
      CC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00D7D7D70000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FEFE
      FE00FBFBFB00F9F9F900F8F8F800F7F7F700F6F6F600F6F6F600F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500ECEBEA00B5B0AB0098928D0097928B009792
      8B0097928B0097928B0097928B0097928B0097928B0097928B0097928B009792
      8B0097928B0097928B0097928B0097928B0097928B0097928B0097928B009792
      8B0097928B0097928B0097928B0097928B0097928B0097928B0097928B009792
      8B0097928B0097928B0098928D00B5B0AB00ECEBEA00F5F5F500F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F6F6F600F6F6F600F7F7F700F8F8F800FAFA
      FA00FDFDFD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FEFEFE00F9F9F900EDEDED00E8E8E800E6E6E600E4E4E400E3E3
      E300E3E3E300E2E2E200E2E2E200E2E2E20098948E00F2F2F100FDFDFD00FCFB
      FB00FCFBFB00FCFBFB00FCFBFB00FCFBFB00FCFBFB00FCFBFB00FCFBFB00FCFB
      FB00FCFBFB00FCFBFB00FCFBFB00FCFBFB00FCFBFB00FCFBFB00FCFBFB00FCFB
      FB00FCFBFB00FCFBFB00FCFBFB00FCFBFB00FDFDFD00EDECEB009C979200E2E2
      E200E2E2E200E2E2E200E3E3E300E3E3E300E4E4E400E5E5E500E7E7E700EAEA
      EA00F4F4F400FDFDFD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DCDCDC00CCCCCC00CCCC
      CC0085858500313131002525250000000000000000000000000017242F003A58
      74003A5874003A5874003A5874003A5874003A5874003A5874003A5874003A58
      74003A5874003A5874003A5874003A5874003A5874003A5874003A5874003A58
      74003A5874003A5874003A5874003A5874003A5874003A5874003A5874003A58
      74003A5874003A5874003A5874003A5874003A5874003A5874003A5874003A58
      7400425F7A007E8E9D00CCCCCC00CCCCCC00CCCCCC00F5F5F500000000000000
      000000000000000000000000000000000000000000000000000000000000FBFB
      FB00F1F1F100EBEBEB00E9E9E900E8E8E800E7E7E700E6E6E600E5E5E500E5E5
      E500E4E4E400E4E4E400E4E4E400C8C5C300B1AEA900E9E8E700E8E7E600E8E7
      E600E8E7E600E8E7E600E8E7E600E8E7E600E8E7E600E8E7E600E8E7E600E8E7
      E600E8E7E600E8E7E600E8E7E600E8E7E600E8E7E600E8E7E600E8E7E600E8E7
      E600E8E7E600E8E7E600E8E7E600E8E7E600E8E7E600E8E7E600E8E7E600E8E7
      E600E8E7E600E8E7E600E9E8E700B1AEA900C8C5C300E4E4E400E4E4E400E4E4
      E400E5E5E500E5E5E500E5E5E500E6E6E600E7E7E700E8E8E800EAEAEA00EEEE
      EE00F7F7F700FEFEFE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FEFEFE00F6F6F600EAEAEA00E5E5E500E3E3E300E2E2E200E1E1
      E100E1E1E100E0E0E000E0E0E000E0E0E000B6B3B000C8C4C100F9F9F900E9E8
      E700E7E5E400E7E5E400E7E5E400E7E5E400E7E5E400E7E5E400E7E5E400E7E5
      E400E7E5E400E7E5E400E7E5E400E7E5E400E7E5E400E7E5E400E7E5E400E7E5
      E400E7E5E400E7E5E400E7E5E400EAE8E700F9F9F900C3C0BD00B9B6B200E0E0
      E000E0E0E000E0E0E000E1E1E100E1E1E100E2E2E200E3E3E300E4E4E400E8E8
      E800F1F1F100FCFCFC0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D5D5D500CCCCCC005353
      53001616160042424200393939002C2C2C001212120000000000000000005879
      970058799700597997005A7A98005A7A98005B7B99005C7C99005C7C9A005D7D
      9A005E7D9B005F7E9B005F7E9B00607F9C00617F9C0061809D0062809D006381
      9D0065829F006584A0006885A0006986A2006B88A3006D89A4006E8AA500708C
      A700728EA700748FA8007590AA007792AB007993AC007B95AD007D97AE007E97
      AF007B93AB00526F8A008795A200CCCCCC00CCCCCC00EEEEEE00000000000000
      000000000000000000000000000000000000000000000000000000000000F9F9
      F900EBEBEB00E7E7E700E5E5E500E3E3E300E2E2E200E2E2E200E1E1E100E1E1
      E100E1E1E100E1E1E100E1E1E100D6D5D4009F9B9600FBFBFB00F4F2F200EFEE
      EE00EFEEEE00EFEEEE00EFEEEE00EFEEEE00EFEEEE00EFEEEE00EFEEEE00EFEE
      EE00EFEEEE00EFEEEE00EFEEEE00EFEEEE00EFEEEE00EFEEEE00EFEEEE00EFEE
      EE00EFEEEE00EFEEEE00EFEEEE00EFEEEE00EFEEEE00EFEEEE00EFEEEE00EFEE
      EE00EFEEEE00F4F2F200FBFBFB009F9B9600D6D5D400E1E1E100E1E1E100E1E1
      E100E1E1E100E1E1E100E1E1E100E2E2E200E3E3E300E4E4E400E6E6E600E9E9
      E900F2F2F200FDFDFD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F8F8F800EBEBEB00E6E6E600E4E4E400E3E3E300E2E2
      E200E2E2E200E1E1E100E1E1E100E1E1E100DADADA00948F8900EAEAE800FAF9
      F900F5F4F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4
      F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4
      F300F5F4F300F5F4F300F5F4F300FAF9F900E8E7E600928D8700DDDDDC00E1E1
      E100E1E1E100E1E1E100E1E1E100E2E2E200E3E3E300E4E4E400E6E6E600E9E9
      E900F3F3F300FDFDFD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D5D5D5008F8F8F000D0D
      0D00484848007070700056565600333333001212120000000000000000005979
      9700597A98005A7A98005B7B99005C7B99005C7C9A005D7D9A005E7D9B005F7E
      9B005F7E9C00607F9C0061809D0061809D0062819D0063819E0064829E006482
      9F0065839F0065839F006684A0006684A0006784A0006785A1006785A1006785
      A1006885A1006885A1006886A1006886A1006886A1006885A1006885A1006B89
      A4008EAFC1007A94AC004B667E00CCCCCC00CCCCCC00EDEDED00000000000000
      000000000000000000000000000000000000000000000000000000000000F9F9
      F900ECECEC00E7E7E700E5E5E500E3E3E300E3E3E300E2E2E200E1E1E100E1E1
      E100E1E1E100E1E1E100E1E1E100E0E0E000B2AEAB00C7C3C000FCFCFC00F6F5
      F400F5F4F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4
      F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4
      F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4F300F5F4
      F300F6F5F400FCFCFC00C7C3C000B2AEAB00E0E0E000E0E0E000E1E1E100E1E1
      E100E1E1E100E1E1E100E1E1E100E2E2E200E3E3E300E4E4E400E6E6E600E9E9
      E900F2F2F200FDFDFD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FBFBFB00F0F0F000EAEAEA00E7E7E700E6E6E600E4E4
      E400E4E4E400E3E3E300E3E3E300E3E3E300E3E3E300C1BEBC00A6A19D00FCFC
      FC0000000000000000000000000000000000F0EEED00EBE9E700EBE9E700EBE9
      E700EBE9E700EBE9E700EBE9E700EBE9E700EBE9E700EBE9E700EDEBE9000000
      0000000000000000000000000000FCFCFC00A39F9A00C3C1BE00E3E3E300E3E3
      E300E3E3E300E3E3E300E4E4E400E4E4E400E5E5E500E7E7E700E9E9E900EDED
      ED00F7F7F700FEFEFE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D7D7D7003D3D3D001E1E
      1E005555550058585800464646002C2C2C001212120000000000000000005A7A
      98005A7A98005B7B99005C7C99005D7C9A005E7D9A005E7E9B005F7E9B00607F
      9C0061809D0062809D0062819E0063829E0064829F0065839F006583A0006684
      A0006785A0006785A1006885A1006886A1006986A2006986A2006987A2006A87
      A2006A87A2006A87A3006A87A3006A87A3006A87A3006A87A3006A87A2006A87
      A2007A99B000829AB100425F7A00CCCCCC00CCCCCC00F1F1F100000000000000
      000000000000000000000000000000000000000000000000000000000000FAFA
      FA00EFEFEF00E9E9E900E6E6E600E5E5E500E4E4E400E3E3E300E2E2E200E2E2
      E200E2E2E200E1E1E100E1E1E100E1E1E100DBDBDB00928D8700E9E8E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E9E8E700928D8700DBDBDB00E1E1E100E1E1E100E1E1E100E1E1
      E100E2E2E200E2E2E200E3E3E300E3E3E300E4E4E400E6E6E600E7E7E700EBEB
      EB00F5F5F500FEFEFE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FDFDFD00F6F6F600EFEFEF00ECECEC00EAEAEA00E9E9
      E900E8E8E800E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700B3B0AC00CAC8
      C400000000000000000000000000D2CDC800AAA29800B0A89F00B0A89F00B0A8
      9F00B0A89F00B0A89F00B0A89F00B0A89F00B0A89F00B0A89F00AEA69D00C7C0
      B900000000000000000000000000C7C3C000B4B1AD00E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E8E8E800E9E9E900EAEAEA00EBEBEB00EEEEEE00F3F3
      F300FBFBFB000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E5E5E500151515002727
      27005959590058585800464646002C2C2C001212120000000000000000005B7B
      98005B7B99005C7C9A005D7D9A005E7D9B005F7E9B00607F9C0061809C006280
      9D0062819E0063829E0064829F0065839F006684A0006784A0006785A1006886
      A1006986A2006987A2006A87A2006A88A3006B88A3006B88A3006B88A4006C89
      A4006C89A4006C89A4006C89A4006C89A4006C89A4006C89A4006C89A4006C89
      A4006F8CA7008BA2B7003A587400CCCCCC00CECECE00FBFBFB00000000000000
      000000000000000000000000000000000000000000000000000000000000FCFC
      FC00F3F3F300ECECEC00E9E9E900E7E7E700E6E6E600E5E5E500E4E4E400E4E4
      E400E3E3E300E3E3E300E3E3E300E3E3E300E3E3E300C1BFBD00A4A09B00FCFC
      FC000000000000000000000000000000000000000000EEECEA00C4BEB800B0A8
      9F00B0A89F00B0A89F00B0A89F00B0A89F00B0A89F00B0A89F00B0A89F00B0A8
      9F00B0A89F00B4ADA400DEDAD600000000000000000000000000000000000000
      0000FCFCFC00A4A09B00C1BFBD00E3E3E300E3E3E300E3E3E300E3E3E300E3E3
      E300E4E4E400E4E4E400E5E5E500E5E5E500E6E6E600E8E8E800EAEAEA00EEEE
      EE00F9F9F900FEFEFE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FEFEFE00FBFBFB00F5F5F500F2F2F200F1F1F100EFEF
      EF00EEEEEE00EEEEEE00EEEEEE00EEEEEE00EDEDED00EDEDED00E3E2E200948F
      8900C2BFBC00D3D1CF00D3D1CF00A39A9000F0EEED0000000000000000000000
      0000000000000000000000000000000000000000000000000000FDFDFD009D93
      8800D2D0CE00D3D1CF00C0BEBA00928D8700E6E6E500EDEDED00EDEDED00EDED
      ED00EEEEEE00EEEEEE00EFEFEF00EFEFEF00F0F0F000F2F2F200F4F4F400F9F9
      F900FEFEFE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FDFDFD00050505002B2B
      2B004E4E4E0058585800464646002C2C2C001212120000000000000000005C7B
      99005D7C9A005D7D9A005E7E9B005F7F9C00607F9C0061809D0062819D006382
      9E0064829F0065839F006684A0006785A1006885A1006986A2006987A2006A87
      A3006B88A3006B88A3006C89A4006C89A4006D8AA5006D8AA5006E8AA5006E8B
      A5006E8BA5006E8BA5006F8BA6006F8BA6006F8BA6006E8BA5006E8BA5006E8B
      A5006E8AA5008EA4B9003A5874008F8F8F008F8F8F00B5B5B500E4E4E4000000
      000000000000000000000000000000000000000000000000000000000000FEFE
      FE00F8F8F800F0F0F000ECECEC00EBEBEB00E9E9E900E8E8E800E7E7E700E7E7
      E700E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600B3B0AC00C8C4
      C1000000000000000000000000000000000000000000C4BEB800D3CECA00EBE9
      E700EBE9E700EBE9E700EBE9E700EBE9E700EBE9E700EBE9E700EBE9E700EBE9
      E700EBE9E700E4E1DF00B4ADA400000000000000000000000000000000000000
      0000C8C4C100B3B0AC00E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6
      E600E7E7E700E7E7E700E8E8E800E9E9E900EAEAEA00EBEBEB00EEEEEE00F3F3
      F300FCFCFC000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FEFEFE00FBFBFB00FAFAFA00F8F8F800F7F7
      F700F7F7F700F6F6F600F6F6F600F6F6F600F6F6F600F6F6F600F6F6F600F2F2
      F200C9C7C300B9B6B200B9B5B1009E948900F1F1F100F3F3F300F3F3F300F3F3
      F300F3F3F300F3F3F300F3F3F300F3F3F300F3F3F300F3F3F300F3F3F300A59C
      9300B5B1AD00B9B6B200CBC8C500F3F3F300F6F6F600F6F6F600F6F6F600F6F6
      F600F6F6F600F6F6F600F7F7F700F8F8F800F8F8F800F9F9F900FBFBFB00FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004848480058585800464646002C2C2C001212120000000000000000005D7C
      9A005E7D9B005F7E9B00607F9C0061809D0062819D0063819E0064829F006583
      9F006684A0006785A1006886A1006986A2006A87A2006B88A3006B88A3006C89
      A4006D8AA4006E8AA5006E8BA5006F8BA6006F8CA600708CA600708CA700708D
      A700718DA700718DA700718DA700718DA700718DA700718DA700718DA700708D
      A700708CA70092A7BB003A587400F1F1F100F5F5F500D8D8D800B5B5B5000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FCFCFC00F5F5F500F1F1F100EFEFEF00EEEEEE00EDEDED00ECECEC00EBEB
      EB00EBEBEB00EBEBEB00EBEBEB00EAEAEA00EAEAEA00EAEAEA00E3E2E200948F
      8900E3E1E00000000000000000000000000000000000ACA49B00EBE9E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FDFDFD00A0978D00F9F9F800000000000000000000000000E3E1
      E000948F8900E3E2E200EAEAEA00EAEAEA00EAEAEA00EBEBEB00EBEBEB00EBEB
      EB00EBEBEB00ECECEC00ECECEC00EDEDED00EFEFEF00F0F0F000F3F3F300F9F9
      F900FEFEFE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FEFEFE00FEFEFE00FEFEFE00FDFD
      FD00FDFDFD00FDFDFD00FDFDFD00FDFDFD00FDFDFD00FDFDFD00FDFDFD00FDFD
      FD00FDFDFD00FDFDFD00EEEDEB00A39B9100D3D3D300D3D3D300D3D3D300D3D3
      D300D3D3D300D3D3D300D3D3D300D3D3D300D3D3D300D3D3D300D4D4D400AAA2
      9A00E2E0DC00FDFDFD00FDFDFD00FDFDFD00FDFDFD00FDFDFD00FDFDFD00FDFD
      FD00FDFDFD00FDFDFD00FDFDFD00FDFDFD00FEFEFE00FEFEFE00FEFEFE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000005E7D
      9B005F7E9B00607F9C0061809D0062819D0063829E0064839F006684A0006785
      A0006885A1006986A2006A87A2006B88A3006C89A4006D89A4006D8AA5006E8B
      A5006F8BA600708CA600708DA700718DA700728EA800728EA800728EA800738E
      A800738FA900738FA900738FA900738FA900738FA900738FA900738FA900738E
      A800728EA80095AABE003A587400C8C8C800CECECE00F7F7F7008F8F8F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FEFEFE00FBFBFB00F7F7F700F5F5F500F4F4F400F3F3F300F2F2F200F2F2
      F200F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100D2D0
      CE00A9A5A100918C8600918C8600918C8600918C86009C928700FBFBFA00FEFE
      FE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFE
      FE00FEFEFE0000000000ABA39A00938D8600918C8600918C8600918C8600A9A5
      A100D1CFCE00F0F0F000F0F0F000F1F1F100F1F1F100F1F1F100F1F1F100F1F1
      F100F1F1F100F2F2F200F3F3F300F3F3F300F4F4F400F6F6F600F8F8F800FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6D3D000A19B940081796F0081796F0081796F008179
      6F0081796F0081796F0081796F0081796F0081796F0081796F0081796F008179
      6F0081796F0081796F0081796F0081796F0081796F0081796F0081796F008179
      6F0081796F0081796F0081796F0081796F0081796F0081796F0081796F008179
      6F0081796F0081796F0081796F0081796F0081796F0081796F0081796F008179
      6F0081796F0081796F0081796F0081796F0081796F0081796F0081796F00A49F
      9700D9D6D3000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000005F7E
      9B00607F9C0061809D0063819E0064829E0065839F006684A0006785A1006886
      A2006A87A2006B88A3006C89A4006D8AA4006E8AA5006F8BA600708CA600718D
      A700718DA700728EA800738EA800738FA900748FA9007490A9007590AA007591
      AA007691AA007691AA007691AA007691AA007691AA007691AA007691AA007591
      AA007590AA0098ACBF003A587400D3D3D300D3D3D300F6F6F6008F8F8F000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FEFEFE00FCFCFC00FBFBFB00FAFAFA00F9F9F900F9F9F900F9F9
      F900F8F8F800F8F8F800F8F8F800F8F8F800F8F8F800F8F8F800F8F8F800F8F8
      F800F8F8F800F7F7F700F8F8F800F8F8F800EFEEEC00A39A9100F3F3F300F2F2
      F200F2F2F200F2F2F200F2F2F200F2F2F200F2F2F200F2F2F200F2F2F200F2F2
      F200F2F2F200F3F3F300B2ABA400DBD8D500F7F7F700F8F8F800F8F8F800F7F7
      F700F8F8F800F8F8F800F8F8F800F8F8F800F8F8F800F8F8F800F8F8F800F8F8
      F800F8F8F800F9F9F900F9F9F900F9F9F900FAFAFA00FBFBFB00FDFDFD00FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D1CECB00938C8300DFDDDA000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DCD9
      D70090898000D9D6D30000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C00121212000000000000000000617F
      9C0062809D0063819E0064829F006583A0006785A0006886A1006987A2006A88
      A3006B88A4006D89A4006E8AA5006F8BA600708CA600718DA700728EA800738E
      A800748FA9007490A9007590AA007691AA007691AB007792AB007792AB007893
      AC007893AC007893AC007893AC007893AC007893AC007893AC007893AC007893
      AC007792AB009CAFC1003A587400DEDEDE00DEDEDE00F8F8F8008F8F8F000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFE
      FE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFE
      FE00FDFDFD00FDFDFD00FDFDFD00FEFEFE00E7E5E200A79F9600D4D4D400D2D2
      D200D2D2D200D2D2D200D2D2D200D2D2D200D2D2D200D2D2D200D2D2D200D2D2
      D200D2D2D200D4D4D400B1ACA500D4CFCA00FDFDFD00FDFDFD00FDFDFD00FDFD
      FD00FDFDFD00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFE
      FE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00FEFEFE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009D978F00E3E1DF00FCFBFB00F6F5F300F4F3F100F5F3F100F5F4
      F300F6F5F300F7F6F400F7F7F500F8F8F600F8F8F700F9F8F700FAF9F800FAFA
      F900FBFAF900FCFBFA00FCFCFB00FCFCFC00FDFDFC00FDFDFD00FEFEFD00FEFE
      FE000000000000000000000000000000000000000000FEFEFE00FEFEFD00FDFD
      FD00FDFDFC00FCFCFC00FCFCFB00FCFBFA00FBFAF900FAFAF900FAF9F800F9F8
      F700F8F8F700DFF0E500F7F7F500F7F6F400F6F5F300F5F4F300F7F5F400FDFC
      FC00DCD9D700A49F970000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000006281
      9D0063829E0064839F006684A0006785A1006886A1006A87A2006B88A3006C89
      A4006D8AA5006F8BA600708CA600718DA700728EA800738FA9007490A9007590
      AA007691AA007792AB007892AB007893AC007994AC007994AD007A94AD007A95
      AD007B95AD007B95AE007B95AE007B95AE007B95AE007B95AE007B95AD007A95
      AD007A94AD009EB2C4003A587400E8E8E800E8E8E800FAFAFA008F8F8F000000
      000000000000000000000000000000000000000000000000000000000000FDFD
      FD00C7C3BF00948F88007A736A007A736A007A736A007A736A007A736A007A73
      6A007A736A007A736A007A736A007A736A007A736A007A736A007A736A007A73
      6A007A736A007A736A007A736A007A736A007A736A007A736A007A736A007A73
      6A007A736A007A736A007A736A007A736A007A736A007A736A007A736A007A73
      6A007A736A007A736A007A736A007A736A007A736A007A736A007A736A007A73
      6A007A736A007A736A007A736A007A736A007A736A007A736A007A736A007A73
      6A007A736A007A736A007A736A007A736A007A736A007A736A00938D8600C7C3
      BF00FDFDFD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000827B7100FDFCFC00EEECE900E8E5E100E8E5E100E9E6E200EAE8
      E500EBE9E600EDECE800EEEDE900F0EFEC00F1F0ED00F2F1EE00F4F3F100F5F4
      F200F6F5F300F8F7F500F8F8F700F9F9F800FAFAF900FBFBFA00FCFCFB00FDFD
      FD00FEFEFE00FEFEFE0000000000FEFEFE00FEFEFE00FDFDFD00FCFCFB00FBFB
      FA00FAFAF900F9F9F800F8F8F700F8F7F500F6F5F300F5F4F200F4F3F100F2F1
      EE00BFE1CE0044BB7F00CEE4D500EDECE800EBE9E600EAE8E500EAE7E300F2F0
      EE00F5F5F4008A82790000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000006382
      9E0065839F006684A0006785A1006986A2006A87A3006B88A3006D8AA4006E8B
      A5006F8CA600718DA700728EA800738FA9007490A9007591AA007691AB007792
      AB007893AC007994AD007A94AD007B95AE007B96AE007C96AE007C96AF007D97
      AF007D97AF007D97AF007D97AF007E97AF007D97AF007D97AF007D97AF007D97
      AF007C96AF00A2B4C5003A587400F2F2F200F2F2F200FCFCFC008F8F8F000000
      0000000000000000000000000000000000000000000000000000F6F5F500B0AC
      A700B2AEA900E5E3E10000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E6E5E300B2AE
      A900B0ACA700F4F4F30000000000000000000000000000000000000000000000
      0000FDFDFD00F9F9F900F6F6F600F5F5F500F4F4F400F3F3F300F3F3F300F2F2
      F200F2F2F20081796F00FDFCFC00E6E3DF00E7E4E000E8E5E100E9E6E200EAE8
      E500EBE9E600EDECE800EEEDE900F0EFEC00F1F0ED00F2F1EE00F4F3F100F5F4
      F200F6F5F300F8F7F500F8F8F700F9F9F800FAFAF900FBFBFA00FCFCFB00FDFD
      FD00FEFEFE00FEFEFE0000000000FEFEFE00FEFEFE00FDFDFD00FCFCFB00FBFB
      FA00FAFAF900F9F9F800F8F8F700F8F7F500F6F5F300F5F4F200F4F3F100F2F1
      EE00EBEEE900B7DEC800EAECE700EDECE800EBE9E600EAE8E500E9E6E200ECE9
      E500F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000006583
      9F006684A0006785A1006986A2006A88A3006C89A4006D8AA5006F8BA600708C
      A600718DA700738EA800748FA9007591AA007692AB007892AC007993AC007A94
      AD007B95AE007C96AE007C96AF007D97AF007E98B0007E98B0007F99B0007F99
      B1008099B1008099B1008099B100809AB1008099B1008099B1008099B1007F99
      B1007F99B000A4B6C8003A587400F9F9F900F9F9F900FEFEFE008F8F8F000000
      0000000000000000000000000000000000000000000000000000BBB8B300BAB6
      B100FEFEFE00F8F8F800F3F3F200F2F1F100F3F2F100F3F3F200F4F4F300F5F4
      F400F6F5F500F6F6F500F7F6F600F7F7F700F8F8F700F9F8F800F9F9F900FAF9
      F900FAFAFA00FBFBFA00FCFBFB00FCFBFB00FCFCFC00FDFDFC00FDFDFD00FEFE
      FE00FEFEFE00FFFEFE00000000000000000000000000FFFEFE00FEFEFE00FEFE
      FE00FDFDFD00FDFDFC00FCFCFC00FCFBFB00FCFBFB00FBFBFA00FAFAFA00FAF9
      F900F9F9F900F9F8F800F8F8F700F7F7F700F7F6F600F6F6F500F6F5F500F5F4
      F400F4F4F300F3F3F200F3F2F100F2F1F100F1F1F100F3F2F200F8F8F700FEFE
      FE00BBB8B300BDB9B5000000000000000000000000000000000000000000FEFE
      FE00F9F9F900EDEDED00E8E8E800E6E6E600E4E4E400E3E3E300E3E3E300E2E2
      E200E2E2E20081796F00FDFCFC00E5E2DE00E7E4E000E8E5E100E9E6E200EAE8
      E500EBE9E600EDECE800EEEDE900F0EFEC00F1F0ED00F2F1EE00F4F3F100F5F4
      F200F6F5F300F8F7F500F8F8F700F9F9F800FAFAF900FBFBFA00FCFCFB00FDFD
      FD00FEFEFE00FEFEFE0000000000FEFEFE00FEFEFE00FDFDFD00FCFCFB00FBFB
      FA00FAFAF900F9F9F800F8F8F700F8F7F500F6F5F300F5F4F200F4F3F100F2F1
      EE00F1F0ED00F0EFEC00EEEDE900EDECE800EBE9E600EAE8E500E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000006684
      A0006785A1006986A2006A88A3006C89A4006D8AA5006F8BA600708DA700728E
      A800738FA9007590AA007691AB007792AB007993AC007A94AD007B95AE007C96
      AE007D97AF007E98B0007F99B0008099B100809AB100819AB200829BB200829B
      B200829BB300839CB300839CB300839CB300839CB300839CB300829BB300829B
      B200829BB200A8B9CB003A5874000000000000000000000000008F8F8F000000
      0000000000000000000000000000000000000000000000000000857E7600F4F4
      F300F2F2F100E6E5E300E3E2E100E4E2E100E5E4E200E6E5E400E8E7E600E9E8
      E700EBEAE900ECEBEA00EDECEB00EEEDED00EFEFEE00F2F1F000F3F2F200F4F3
      F300F5F5F400F6F6F500F8F7F700F8F7F700F9F8F800FAFAF900FBFBFB00FCFC
      FC00FCFCFC00FEFDFD00000000000000000000000000FEFDFD00FCFCFC00FCFC
      FC00FBFBFB00FAFAF900F9F8F800F8F7F700F8F7F700F6F6F500F5F5F400F4F3
      F300F3F2F200F2F1F000EFEFEE00EEEDED00EDECEB00ECEBEA00E9E9E800A6D6
      BD00CDE0D500E6E5E400E5E4E200E4E2E100E2E1E000E2E1DF00E5E4E200F2F2
      F100EFEFEE008A837B000000000000000000000000000000000000000000FEFE
      FE00F6F6F600EAEAEA00E5E5E500E3E3E300E2E2E200E1E1E100E1E1E100E0E0
      E000E0E0E00081796F00FDFCFC00E5E2DE00E7E4E000C4D0BE0064986100065F
      0500005C0000005C0000005C0000005C0000005C0000005C0000005C0000005C
      0000005C0000005C0000005C0000005C0000005C0000005C0000005C0000005C
      0000005C0000005C0000005C0000005C0000005C0000005C0000005C0000005C
      0000005C0000005C0000005C0000005C0000005C0000005C0000005C0000005C
      0000005C0000005C0000005C0000005C000060965E00C0CFBC00E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000006785
      A1006986A2006B88A3006C89A4006E8AA5006F8CA600718DA700728EA800748F
      A9007591AA007792AB007893AC007A94AD007C96AF007E98B000829BB200839C
      B300859DB400859DB300839DB400839CB300839CB300849CB300849DB400859D
      B400859EB400859EB500859EB500869EB500859EB500859EB500859EB400859D
      B400849DB400ABBBCC003A5874000000000000000000000000008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FDFD
      FD00E4E3E200E1E0DE00E2E1E000E4E2E100E5E4E200E6E5E400E8E7E600E9E8
      E700EBEAE900ECEBEA00EDECEB00EEEDED00EFEFEE00F2F1F000F3F2F200F4F3
      F300F5F5F400F6F6F500F8F7F700F8F7F700F9F8F800FAFAF900FBFBFB00FCFC
      FC00FCFCFC00FEFDFD00000000000000000000000000FEFDFD00FCFCFC00FCFC
      FC00FBFBFB00FAFAF900F9F8F800F8F7F700F8F7F700F6F6F500F5F5F400F4F3
      F300F3F2F200F2F1F000EFEFEE00EEEDED00EDECEB00ECEBEA00CEE2D60044BB
      7F007ACAA100E6E5E400E5E4E200E4E2E100E2E1E000E1E0DE00E0DFDD00E4E3
      E100FDFDFD007A736A0000000000000000000000000000000000000000000000
      0000F8F8F800EBEBEB00E6E6E600E4E4E400E3E3E300E2E2E200E2E2E200E1E1
      E100E1E1E10081796F00FDFCFC00E5E2DE00E7E4E0006497610031A15B004EC5
      8D0059C28C005EC38E0063C490006AC592006DC7940073C7960077C899007AC9
      9A007CCA9B007FCA9D0082CA9E0083CA9F0085CBA00086CCA20087CCA30087CC
      A30089CDA30089CDA30089CDA30089CDA30089CDA30087CCA30087CCA30086CC
      A20085CBA00083CA9F0082CA9E007FCA9D007CCA9B007AC99A0077C8990073C7
      96006DC794006AC5920063C4900064C9940043A465005F955D00E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000006986
      A2006A88A3006C89A4006E8AA5006F8CA600718DA700738EA8007490A9007691
      AA007792AB007994AC007C97AF00869FB50093AABE009BB1C3009FB3C700A0B4
      C800A1B5C800A1B6C8009EB3C70097ACC0008AA1B700869FB500879FB600879F
      B60088A0B60088A0B60088A0B60088A0B60088A0B60088A0B60088A0B600879F
      B600879FB600AEBECE003A5874000000000000000000000000008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E100E5E4E200E6E5E400E8E7E600E9E8
      E700EBEAE900ECEBEA00EDECEB00EEEDED00EFEFEE00F2F1F000F3F2F200F4F3
      F300F5F5F400F6F6F500F8F7F700F8F7F700F9F8F800FAFAF900FBFBFB00FCFC
      FC00FCFCFC00FEFDFD00000000000000000000000000FEFDFD00FCFCFC00FCFC
      FC00FBFBFB00FAFAF900F9F8F800F8F7F700F8F7F700F6F6F500F5F5F400F4F3
      F300F3F2F200F2F1F000EFEFEE00EEEDED00EDECEB00ECEBEA00EBEAE900AAD7
      BF00CDE0D500E6E5E400E5E4E200E4E2E100E2E1E000E1E0DE00E0DFDD00E0DF
      DD00FCFCFC007A736A0000000000000000000000000000000000000000000000
      0000FBFBFB00F0F0F000EAEAEA00E7E7E700E6E6E600E4E4E400E4E4E400E3E3
      E300E3E3E30081796F00FDFCFC00E5E2DE00E7E4E0000E640D0052BE8500259E
      560021964C0025965000289852002E995500319B5700349C5A00389E5C003B9F
      5E003EA0610041A1630043A3640045A4660047A4680049A569004AA56A004BA6
      6B004BA66B004BA76C004CA76C004BA76C004BA66B004BA66B004AA56A0049A5
      690047A4680045A4660043A3640041A163003EA061003B9F5E00389E5C00349C
      5A00319B57002E995500289852002B9D570064C59100005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000006A88
      A3006C89A4006E8AA5006F8CA600718DA700738EA8007490A9007691AB007893
      AC007994AD00819AB10095ABC00094AABC00798FA100677C8F00586D8000596E
      8100596E8100596E8100738799008296A8009FB4C5008BA3B90089A1B7008AA2
      B8008AA2B8008BA2B8008BA2B8008BA2B8008BA2B8008BA2B8008AA2B8008AA2
      B80089A1B700B0C0D0003A5874000000000000000000D7D7D700B7B7B7000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E100E5E4E200E6E5E400E8E7E600E9E8
      E700EBEAE900ECEBEA00EDECEB00EEEDED00EFEFEE00F2F1F000F3F2F200F4F3
      F300F5F5F400F6F6F500F8F7F700F8F7F700F9F8F800FAFAF900FBFBFB00FCFC
      FC00FCFCFC00FEFDFD00000000000000000000000000FEFDFD00FCFCFC00FCFC
      FC00FBFBFB00FAFAF900F9F8F800F8F7F700F8F7F700F6F6F500F5F5F400F4F3
      F300F3F2F200F2F1F000EFEFEE00EEEDED00EDECEB00ECEBEA00EBEAE900E9E8
      E700E8E7E600E6E5E400E5E4E200E4E2E100E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000000000
      0000FDFDFD00F6F6F600EFEFEF00ECECEC00EAEAEA00E9E9E900E8E8E800E7E7
      E700E7E7E70081796F00FDFCFC00E5E2DE00E7E4E0000E640D0052B57C001883
      3A00127A2E00137C3100167E3400198037001B8239001E833C0020853F002287
      410024884300278A4600288B48002A8C49002C8E4B002D8F4C002E8F4D002F90
      4E002F904F0030914F003091500030914F002F904F002F904E002E8F4D002D8F
      4C002C8E4B002A8C4900288B4800278A4600248843002287410020853F001E83
      3C001B82390019803700167E34001680350060BB8500005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000006C89
      A4006D8AA5006F8CA600718DA700738EA8007590A9007691AB007893AC007A94
      AD00819CB20098AEC10070859700586D8000586D8000596E8100596E8100596E
      8100596E81005A6E81005A6F81005A6F82005F7486008DA5BB008CA3B9008DA4
      B9008DA4BA008DA4BA008DA4BA008EA5BA008DA4BA008DA4BA008DA4BA008DA4
      B9008CA3B900B3C2D2003A5874008F8F8F008F8F8F00B7B7B700E6E6E6000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E100E5E4E200E6E5E400E8E7E600E9E8
      E700EBEAE900ECEBEA00EDECEB00EEEDED00EFEFEE00F2F1F000F3F2F200F4F3
      F300F5F5F400F6F6F500F8F7F700F8F7F700F9F8F800FAFAF900FBFBFB00FCFC
      FC00FCFCFC00FEFDFD00000000000000000000000000FEFDFD00FCFCFC00FCFC
      FC00FBFBFB00FAFAF900F9F8F800F8F7F700F8F7F700F6F6F500F5F5F400F4F3
      F300F3F2F200F2F1F000EFEFEE00EEEDED00EDECEB00ECEBEA00EBEAE900E9E8
      E700E8E7E600E6E5E400E5E4E200E4E2E100E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000000000
      0000FEFEFE00FBFBFB00F5F5F500F2F2F200F1F1F100EFEFEF00EEEEEE00EEEE
      EE00EEEEEE0081796F00FDFCFC00E5E2DE00E7E4E0000E640D0057B77F001B81
      3900147C3200177E35001A8038001C823B001F843E002286410024884300278A
      4600298C48002B8D4B002D8F4D002F904F003191500032925200339353003494
      5400359455003595550035955500359555003594550034945400339353003292
      5200319150002F904F002D8F4D002B8D4B00298C4800278A4600248843002286
      41001F843E001C823B001A803800177E350067BB8900005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000006D8A
      A5006F8BA600718DA700738EA8007490A9007691AB007893AC007A94AD007F99
      B00093A9BD00667B8E00586D8000586D81006B819600738BA000879FB50088A0
      B60089A1B7008AA2B800758B9F006F8598005E7286008EA5BA008FA6BB008FA6
      BB0090A6BB0090A7BC0090A7BC0090A7BC0090A7BC0090A7BC0090A6BB008FA6
      BB008FA6BB00B6C5D3003A587400000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E100E4E2DF00AB987500795717006C46
      00006C4600006C4600006C4600006C4600006C4600006C4600006C4600006C46
      00006C4600006C4600006C4600006C4600006C4600006C4600006C4600006C46
      00006C4600006C4600006C4600006C4600006C4600006C4600006C4600006C46
      00006C4600006C4600006C4600006C4600006C4600006C4600006C4600006C46
      00006C4600006C4600006C4600006C4600006C4600006C4600006C4600006C46
      00006C46000079571700AA977400E1DEDC00E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000000000
      000000000000FEFEFE00FBFBFB00FAFAFA00F8F8F800F7F7F700F7F7F700F6F6
      F600F6F6F60081796F00FDFCFC00E5E2DE00E7E4E0000E640D0063B985001F84
      3D00177F35001A8139001D833C0020853F0023874200268A4500298C48002B8D
      4B002E8F4D003091500032935200349454003695560037965800399759003A98
      5A003A985B003B995B003B995B003B995B003A985B003A985A00399759003796
      5800369556003494540032935200309150002E8F4D002B8D4B00298C4800268A
      45002387420020853F001D833C001A81390072BF9000005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000006F8B
      A600708DA700728EA8007490A9007691AB007893AC007A94AD007D97AF0092A8
      BD006B7F9300586D800063788C007F97AD00879FB6008BA2B8008DA4B9008CA4
      BA008DA3B9008DA4B9008FA5BA0091A8BD0094A9BD0094A9BE0092A9BD0092A8
      BD0092A8BD0093A9BD0093A9BD0093A9BD0093A9BD0093A9BD0092A8BD0092A8
      BD0091A8BC00B9C8D5003A5874008F8F8F008F8F8F00B5B5B500E4E4E4000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E100A28C64009A7C3400C2A76400C8AB
      6800C8AA6B00C8AB7000C9AC7300C9AD7700CAAF7A00CAB07C00CAB27F00CBB2
      8200CCB28300CCB38500CDB48600CDB58900CDB58A00CDB68B00CEB68C00CEB7
      8C00CFB78C00CFB78E00D0B78E00D0B88E00D0B88E00D0B88E00D0B78E00CFB7
      8E00CFB78C00CEB78C00CEB68C00CDB68B00CDB58A00CDB58900CDB48600CCB3
      8500CCB28300CBB28200CAB27F00CAB07C00CAAF7A00C9AD7700C9AC7300C8AB
      7000C9AC6D00C3A867009C7D3800A0896100E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000000000
      00000000000000000000FEFEFE00FEFEFE00FEFEFE00FDFDFD00FDFDFD00FDFD
      FD00FDFDFD0081796F00FDFCFC00E5E2DE00E7E4E0000E640D006CBD8C002387
      43001B8139001E843C002186400024884300278B47002A8D4A002D8F4D003091
      50003393530035955500379658003A985A003B995C003D9A5D003E9B5F003F9C
      6000409C6100409D6100419D6100409D6100409C61003F9C60003E9B5F003D9A
      5D003B995C003A985A00379658003595550033935300309150002D8F4D002A8D
      4A00278B470024884300218640001E843C007CC39700005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C00121212000000000000000000708C
      A600728EA800748FA9007691AA007893AC007A94AD007C96AE00829AB2007D91
      A400586D8000647A8D00839BB20089A1B60099AEC100A5B8CA00A8BBCC00A7BA
      CC009CB0C30093A9BD009EB2C500ABBECF00AEBFD000ADBFD000A5B8CA0097AC
      C10095ABBF0095ABBF0095ABBF0096ABBF0095ABBF0095ABBF0095ABBF0095AA
      BF0094AABE00BBCAD7003A587400F1F1F100F5F5F500D8D8D800B5B5B5000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E100704C0800C9AD6900A68037009C74
      2D009C732E009D7431009E7534009E773700A0793A00A17A3D00A27C3F00A37D
      4100A47E4300A57F4600A6804700A7814A00A7824B00A8834C00A9834D00A984
      4E00A9845000A9855000AA855100AA855100AA855100AA855100AA855100A985
      5000A9845000A9844E00A9834D00A8834C00A7824B00A7814A00A6804700A57F
      4600A47E4300A37D4100A27C3F00A17A3D00A0793A009E7737009E7534009D74
      31009E743000A6813A00CAAF6F006F4A0500E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000000000
      0000D6D3D000A19B940081796F0081796F0081796F0081796F0081796F008179
      6F0081796F0081796F00FDFCFC00E5E2DE00E7E4E0000E640D0074C191002889
      46001E843D002186400025894400288B47002C8E4B002F904E00329252003594
      5500389658003A985B003D9A5D003F9C5F00419D6100429E6300449F650045A0
      660046A1670046A1670046A1670046A1670046A1670045A06600449F6500429E
      6300419D61003F9C5F003D9A5D003A985B003896580035945500329252002F90
      4E002C8E4B00288B4700258944002186400084C89E00005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C00121212000000000000000000718D
      A700738FA9007591AA007792AB007994AD007B96AE007D97AF008CA4BA006277
      8A00596E81007E96AB0088A0B70099AEC100889CAE00607487005B6F8200657A
      8D0090A3B500A9BBCC0093A5B500627688005C718300627688008A9CAD00A9BA
      CC009AAFC20098ADC10098ADC10098ADC10098ADC10098ADC10098ADC10097AC
      C00097ACC000BECCD8003A587400C8C8C800CECECE00F7F7F7008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000BEA0630086591A008153
      17008153180083551A0084571C0086591E00875A2000885C22008A5E24008B5F
      26008C6128008D622A008E632B008F652D0090662E0091673000926831009268
      32009369320093693300946A3300946A3400946A3400946A3400946A33009369
      33009369320092683200926831009167300090662E008F652D008E632B008D62
      2A008C6128008B5F26008A5E2400885C2200875A200086591E0084571C008355
      1A0082541900875A1C00BFA268006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A000000000000000000000000000000000000000000D1CE
      CB00938C8300DFDDDA0000000000000000000000000000000000000000000000
      00000000000081796F00FDFCFC00E5E2DE00E7E4E0000E640D007FC397002D8D
      4B002186400025894400298B48002C8E4C0030914F0033935300369556003A98
      5A003C9A5D003F9C6000429E6300449F650046A1670048A2690049A36B004AA4
      6C004BA56D004CA56D004CA56D004CA56D004BA56D004AA46C0049A36B0048A2
      690046A16700449F6500429E63003F9C60003C9A5D003A985A00369556003393
      530030914F002C8E4C00298B4800268A450090CDA700005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C00121212000000000000000000738E
      A8007590AA007792AB007994AC007B95AE007D97AF00809AB100859BB000596E
      81006D839600889FB6008CA3B900879BAD005A6F82005B6F82005B7082005B70
      82005C7083009AACBC00627689005C7183005D7183005D7183005D7183008598
      A800A5B9CA009AAFC2009BAFC3009BAFC3009BAFC3009AAFC2009AAFC2009AAE
      C20099AEC100C0CEDA003A587400D3D3D300D3D3D300F6F6F6008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000BC9E6400805216008254
      180083561B0085581D00865A1F00885B2200895D24008B5F26008C6128008E62
      2A008F642C0090652E00916730009268310093693300946A3400956B3500956C
      3600966D3700966D3800976E3800976E3900976E3900976E3900976E3800966D
      3800966D3700956C3600956B3500946A34009369330092683100916730009065
      2E008F642C008E622A008C6128008B5F2600895D2400885B2200865A1F008558
      1D0083561B0082541800BD9F67006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000009D97
      8F00E3E1DF00FCFBFB00F6F5F300F4F3F100F5F3F100F5F4F300F6F5F300F7F6
      F400F7F7F50081796F00FDFCFC00E5E2DE00E7E4E0000E640D0085C79E003190
      4F0025894400298B48002C8E4C003091500034945400379658003B995B003E9B
      5F00419D620044A0650047A1680049A36B004BA56D004DA66F004FA7710050A8
      720051A9730051A9730052A9740051A9730051A9730050A872004FA771004DA6
      6F004BA56D0049A36B0047A1680044A06500419D62003E9B5F003B995B003796
      580036955500379556003B9658003D965900A0D4B200005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C00121212000000000000000000748F
      A9007691AB007893AC007A95AD007C96AF007F98B000839CB3007D92A500596E
      81008197AC0089A1B70092A8BD00657A8B005B6F82007A8FA30092A8BD008FA5
      BA006D819400687B8E005D718300728597009BB0C30094A7BA006D8092005E72
      84009BADBD009FB3C5009DB1C4009DB2C4009DB1C4009DB1C4009DB1C4009CB1
      C4009CB0C300C3D0DC003A587400DEDEDE00DEDEDE00F8F8F8008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000BFA16D00825419008456
      1B0085581E00875A2000895C23008A5E25008C6028008D622A008F642C009066
      2E009267300093693200946A3400956C3600966D3700976E3900986F3A009970
      3B0099713C009A713D009A723D009A723D009A723E009A723D009A723D009A71
      3D0099713C0099703B00986F3A00976E3900966D3700956C3600946A34009369
      32009267300090662E008F642C008D622A008C6028008A5E2500895C2300875A
      200085581E0084561B00BFA370006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A000000000000000000000000000000000000000000827B
      7100FDFCFC00EEECE900E8E5E100E8E5E100E9E6E200EAE8E500EBE9E600EDEC
      E800EEEDE90081796F00FDFCFC00E5E2DE00E7E4E0000E640D008DCAA3003592
      5300288B47002C8E4C003091500034945400389758003C995C003F9C6000439F
      640046A1670049A36A004CA56D004FA7700051A9730053AA750054AB760056AC
      780057AD790057AD790057AE7A0057AD790057AD790056AC780054AB760053AA
      750051A973004FA770004CA56D0049A36A0046A1670045A0650047A067004BA1
      68004DA26A004AA06700479D6400469C6200A9D8BA00005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000007591
      AA007792AB007A94AD007C96AE007E98B0008099B100849DB500708598005A6F
      8100889EB4008CA3B90090A5B8005B6F82006F84960094A9BE0094AABE0096AB
      BF0098ADC100677C8D005D7184008B9EB0009DB1C4009DB1C40093A7B9005E72
      840075899A00A4B6C900A0B4C700A0B4C700A0B4C700A0B3C7009FB3C7009FB3
      C5009EB2C500C5D1DD003A587400E8E8E800E8E8E800FAFAFA008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000C1A7750084571C008659
      1E00875B2100895D24008B5F26008D6129008E632B0090652E00916730009369
      3200946B3500966C3700976E3900986F3A0099713C009A723E009B733F009C74
      40009C7541009D7542009D7642009D7642009D7642009D7642009D7642009D75
      42009C7541009C7440009B733F009A723E0099713C00986F3A00976E3900966C
      3700946B3500936932009167300090652E008E632B008D6129008B5F2600895D
      2400875B210086591E00C2A878006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E6E3DF00E7E4E000E8E5E100E9E6E200EAE8E500EBE9E600EDEC
      E800EEEDE90081796F00FDFCFC00E5E2DE00E7E4E0000E640D0094CDA8003A96
      57002C8E4B0030914F0034945400389758003C9A5C00409C6100449F650047A2
      69004BA46C004EA7700051A9730054AB760056AD780058AE7A005AAF7C005BB0
      7E005CB17F005DB17F005DB280005DB17F005CB17F005BB07E005AAF7C0058AE
      7A0056AD780054AB760053AA740056AB76005AAB78005DAD7B005BAB780058A8
      750056A7720053A56F0051A36C004FA16900B3DCC200005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000007692
      AB007993AC007B95AE007D97AF007F99B100829BB200869FB600687C8F005F74
      87008FA6BB008EA6BA008DA1B6005E7385008295A60096ACBF0096ABBF0098AD
      C10099AEC2008598A9005E72840093A5B5009EB2C5009FB3C700A1B5C8007588
      990065788A00ABBCCE00A2B6C900A2B6C900A2B6C900A2B5C800A2B5C800A1B5
      C800A0B4C700C7D3DE003A587400F2F2F200F2F2F200FCFCFC008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000C4AA7C0086591F00885B
      21008A5D24008B6027008D622A008F642C0091662F0092683200946A3400966C
      3700976E390099703B009A723D009B733F009C7441009D7642009E7744009F78
      4500A0794600A0794700A17A4700A17A4700A17A4800A17A4700A17A4700A079
      4700A07946009F7845009E7744009D7642009C7441009B733F009A723D009970
      3B00976E3900966C3700946A34009268320091662F008F642C008D622A008B60
      27008A5D2400885B2100C5AB80006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000E8E5E100E9E6E200EAE8E500EBE9E600EDEC
      E800EEEDE90081796F00FDFCFC00E5E2DE00E7E4E0000E640D009BD1AE003E98
      5B002F904E0033935300379658003C995C00409C6100449F650048A269004CA5
      6D004FA8710053AA750056AC780059AE7B005BB07E005DB280005FB3820061B4
      840062B5850062B6850063B6860062B6850062B5850061B484005FB382005FB3
      820063B4850068B687006CB789006AB5870067B4840066B2820063B07F0061AE
      7D005FAC7A005DAA770059A8750057A67100BCE0C900005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000007892
      AC007A94AD007C96AE007E98B000819AB200839CB30087A0B700697E8F006074
      870094A8BD0090A7BC0090A5B800617587008295A5009DB2C5009AAFC2009BAE
      C2009BB0C30093A7B8005E72840090A1B200A2B5C900A2B5C800A4B7C9008699
      A9005F7385009FB1C100A5B8CA00A5B8CA00A5B8CA00A4B7CA00A4B7CA00A3B7
      C900A3B6C900CAD5DF003A587400F9F9F900F9F9F900FEFEFE008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000C8AE8400885B21008A5E
      24008C6027008E622A008F652D009167300093693300956B3600976E38009870
      3B009A723D009C7340009D7542009E7744009F784500A17A4700A17B4900A27C
      4A00A37D4B00A37D4C00A47E4C00A47E4D00A47E4D00A47E4D00A47E4C00A37D
      4C00A37D4B00A27C4A00A17B4900A17A47009F7845009E7744009D7542009C73
      40009A723D0098703B00976E3800956B360093693300916730008F652D008E62
      2A008C6027008A5E2400C9AF86006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000D5CDBE00A1895F006D4803006C4600006C46
      00006C46000081796F00FDFCFC00E5E2DE00E7E4E0000E640D00A1D3B200429B
      5F0032925200369556003B995B003F9C6000449F650048A269004CA56E0050A8
      720054AB760057AE7A005BB07D005EB2800060B4830063B6860065B7880066B8
      890067B98A0068BA8B0068BA8B0068BA8B006BBB8D0070BC900076BF950079C0
      970077BF950076BE930075BC910073BB900071B98D006FB78B006CB588006AB3
      840068B1820065B0800063AE7C0060AB7A00C3E4D000005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000007993
      AC007B95AE007D97AF008099B100829BB200849DB40089A0B6006D8295006176
      87009AAFC20093A8BD0097ADC000687A8C006F819200AFBFCF00A2B5C800A1B4
      C700A1B3C7009DAFBF005E7284008C9EAE00A4B7CA00A4B7CA00A6B9CB008E9F
      B0006073850096A7B800A7B9CC00A7BACC00A7B9CC00A7B9CB00A6B9CB00A6B8
      CB00A5B8CA00CCD7E1003A5874000000000000000000000000008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000CBB28A008A5E24008C60
      27008E632B0090652E0092683100946A3400966C3700986F3A0099713C009B73
      3F009D7542009E774400A0794600A17B4800A37C4A00A47E4C00A57F4E00A680
      4F00A6815000A7815100A7825100A8825200A8825200A8825200A7825100A781
      5100A6815000A6804F00A57F4E00A47E4C00A37C4A00A17B4800A07946009E77
      44009D7542009B733F0099713C00986F3A00966C3700946A3400926831009065
      2E008E632B008C602700CCB48D006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000A28B6100A98B4500CAAF6D00C8AB6B00C8AC
      7000C9AD760081796F00FDFCFC00E5E2DE00E7E4E0000E640D00A8D6B800469D
      6300359455003A985A003E9B5F00439F640047A269004CA56D0050A8720054AB
      760058AE7A005CB17E005FB3820062B6850065B8880068B98B006ABB8D006BBC
      8F006EBE920074C096007AC49B0081C7A00082C8A10083C8A10082C7A00082C5
      9F0080C49D007FC39B007DC199007BC0970079BE940077BC920075BA900073B9
      8D0070B78A006EB588006BB284006AB18200CCE8D700005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000007A94
      AD007C96AE007E98B000819AB200839CB300869EB5008AA1B70075899C005D71
      840097AABC0095ABBF0097ACC0007A8C9D00657889009EADBC00B3C2D200AABD
      CD00B0C1D100A5B6C40064778900889AAB00A6B9CB00A6B9CB00A9BBCD0091A1
      B100607486009BACBC00A9BBCD00A9BBCD00A9BBCD00A9BBCD00A9BBCD00A8BA
      CC00A7BACC00CED8E2003A5874000000000000000000000000008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000CEB691008C6027008E63
      2B0090652E0092683100946A3400966D3700986F3A009A723E009C7440009E77
      4300A0794600A17B4800A37D4B00A47E4D00A6804F00A7825100A8835300A984
      5400AA855500AA865600AB865700AB865700AB865700AB865700AB865700AA86
      5600AA855500A9845400A8835300A7825100A6804F00A47E4D00A37D4B00A17B
      4800A07946009E7743009C7440009A723E00986F3A00966D3700946A34009268
      310090652E0090662F00D1BB98006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00C4A86500A47C37009C732E009D74
      32009E76360081796F00FDFCFC00E5E2DE00E7E4E0000E640D00ADD9BC004BA0
      6700389658003C9A5D00419D620046A167004BA46C004FA8710054AB760058AE
      7A005CB17F0060B4830064B6870067B98A006FBD910075C197007DC59D0083CA
      A30089CCA7008ACCA8008BCDA9008BCDA8008CCDA9008BCDA8008BCCA7008ACB
      A60089CAA40088C9A30086C7A10084C59F0081C39C0080C19A007EC098007BBE
      940079BB920076B98F0074B78C0072B68A00D3EADC00005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000007B95
      AE007D97AF008099B100829BB200849DB400879FB6008AA2B8007E93A7005E72
      8400899CAC009BB0C20099AEC1008EA2B40065788A006C7F90009AABBA00BFCD
      DA00C0CFDC008595A4006E8091008B9BAB00ABBBCE00A8BACC00ADBECF008798
      A70061748600A4B5C500ACBDCF00ACBDCF00ACBDCF00ABBDCE00ABBDCE00AABC
      CE00A9BBCD00CFD9E3003A5874000000000000000000000000008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000D0BB98008E622A009065
      2E0092683100946B3400966D380099703B009B723E009D7541009F784500A17A
      4700A37C4A00A47E4D00A6804F00A8825200A9845400AA855600AB875700AC88
      5900AD895A00AE8A5B00AE8A5C00AE8B5C00AE8B5C00AE8B5C00AE8A5C00AE8A
      5B00AD895A00AC885900AB875700AA855600A9845400A8825200A6804F00A47E
      4D00A37C4A00A17A47009F7845009D7541009B723E0099703B0098703B009871
      3C009A723E0098703D00D6C1A3006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00BB9E6100895E2100825419008456
      1B0085581E0081796F00FDFCFC00E5E2DE00E7E4E0000E640D00B3DBC0004EA2
      6A003A985B003F9C600044A0650049A36A004EA7700053AA750058AF7B0061B3
      820069B88A0072BE92007BC3990083C8A10086CAA4008ACCA8008CCEAA008ECF
      AC0091D0AE0092D1B00093D2B00094D2B10094D2B00094D1B00093D1AE0092D0
      AE0090CEAC008FCDAA008ECCA8008CCAA6008AC9A40088C7A10085C49F0084C2
      9C0082C19A007FBE96007DBD94007BBB9200D9EDE100005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000007C96
      AE007E98B000819AB100839CB300869EB50089A1B7008BA3B9008DA4B7006377
      8A006B7E9000AABDCE009CB1C4009EB2C4008295A600687B8C006A7C8D006C7E
      8F006E8090006F819100768898008C9CAB00B2C3D300AFC0D100B9C9D7006E80
      9100677A8C00AEC0D100AEBFD000AEBFD000AEBFD000ADBFD000ADBED000ACBE
      CF00ABBDCE00D1DBE4003A5874000000000000000000D7D7D700B7B7B7000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000D3BF9F008F652D009268
      3100946A3400966D380099703B009B733F009D7642009F784500A17B4900A37D
      4C00A5804F00A7825100A9845400AB865600AC885900AD895B00AF8B5C00B08C
      5E00B08D5F00B18E6000B18E6100B28F6100B28F6200B28F6100B18E6100B18E
      6000B08D5F00B08C5E00AF8B5C00AD895B00AC885900AB865600A9845400A782
      5100A5804F00A37D4C00A17B4900A17A4800A27C4A00A27D4E00A27C4C009F7A
      4A009E7847009D774500D8C7AA006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00BC9E6500885D220084571C008659
      1F00885C220081796F00FDFCFC00E5E2DE00E7E4E0000E640D00B8DEC50051A4
      6D003E9B5E0047A1670050A670005AAC780063B282006EB78B0076BD92007AC0
      97007FC39C0084C8A00087C9A4008ACCA8008FCEAC0092D1AF0094D2B10097D4
      B30098D5B5009AD5B7009BD6B7009CD6B8009CD7B8009CD6B6009BD5B6009AD4
      B50099D3B30097D2B10096D0AF0094CFAD0093CDAB0090CBA8008EC9A5008CC7
      A3008AC5A10087C39D0085C19B0084C09900DFF0E500005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000007C96
      AF007F99B000819BB200849DB400869FB5008AA2B8008DA4BA0090A7BB007488
      9A00617487008EA0B000ABBCCF00A0B4C700A2B6C9008EA0B100738596006D7F
      90007385950097A8B7007F90A0008192A100B7C8D600BECDDA00A4B2BF006578
      89008396A600AFC0D100B0C1D100B0C1D100B0C1D100AFC0D100AFC0D100AEBF
      D000ADBFD000D3DCE5003A5874008F8F8F008F8F8F00B7B7B700E6E6E6000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000D6C2A40091673000946A
      3400966D370099703B009B733F009D764200A0794600A27B4900A47E4D00A681
      5000A8835300AA855600AC885900AE8A5B00AF8C5D00B18D5F00B28F6100B390
      6300B4916400B4926500B5936600B5936600B5936700B5936600B5936600B492
      6500B4916400B3906300B28F6100B18D5F00AF8C5D00AE8A5B00AC885900AA85
      5600A9855600AA875800AB895C00AB885B00AA875900A7845600A6825400A480
      5100A27E4E00A07B4C00DCCBB2006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00BFA36F008B602600865A1F00895C
      23008B5F260081796F00FDFCFC00E5E2DE00E7E4E0000E640D00C5E4D0006EB3
      85005EAC790063AF7F0068B384006EB6890073BA8F0079BD94007DC1990082C5
      9E0086C8A2008BCBA7008ECEAA0093D1AE0096D2B2009AD5B5009CD6B8009ED8
      BA00A1D9BC00A2DABD00A3DBBE00A4DBBF00A4DABE00A3DABD00A3D9BD00A2D9
      BB00A1D7BA009FD6B8009ED5B6009BD3B4009AD1B10098D0AF0096CEAC0094CC
      AA0092CAA80090C8A5008DC5A2008BC4A000E4F3E900005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000007D97
      AF008099B100829BB300859DB40088A0B7008BA3B9008EA6BA0092A8BD008EA5
      B800667A8C0063778800A1B2C000B1C2D100A6B9CB00A8BACB00AABBCD00ACBE
      CF00AFBFD000B2C2D200B3C3D300B8C8D600C0D0DB00C0CDD8008695A4007586
      9600A7B7C700B1C2D200B2C2D300B2C2D300B2C2D300B1C2D200B1C1D200B0C1
      D200AFC0D100D4DDE6003A587400000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000D9C7AA0093693300966C
      3700986F3A009B723E009D764200A0794600A27C4900A47E4D00A7815100A984
      5400AB865700AD895A00AF8B5D00B18D6000B28F6200B4916400B5936600B694
      6800B7956900B8966A00B8976B00B9976C00B9976C00B9976C00B8976B00B896
      6A00B7956900B6946800B5936600B4916400B28F6200B28F6200B3916500B593
      6800B4936900B3926700B1906500AF8E6300AE8C6000AC8A5E00AA885B00A985
      5800A6835600A5815400DFD0B8006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00C1A777008E642C00895C23008B5F
      26008D622A0081796F00FDFCFC00E5E2DE00E7E4E0000E640D00CFE8D80076B8
      8D0065B07F006BB4850070B78B0076BB90007ABE95007FC19B0084C59F0089C9
      A4008DCCA80092CFAD0096D2B1009AD4B5009DD7B800A0D9BB00A3DABE00A6DC
      C100A8DDC200A9DEC300ABDEC400ABDFC400ABDEC400ABDEC400AADDC300A9DC
      C100A8DBC000A7D9BE00A5D8BC00A3D6B900A1D5B7009FD4B5009DD2B3009CD0
      B10099CEAE0098CDAC0095CBA90094C9A700E8F5ED00005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002C2C
      2C004646460058585800464646002C2C2C001212120000000000000000007E98
      B000809AB100839CB300869EB50089A1B7008DA3B9008FA6BB0093A8BD0096AC
      BF008499AB00657889006B7E8F009AABBA00BACAD700B6C7D600B1C2D200B1C2
      D100B4C3D300B8C8D700C2D0DB00C9D6E000BCC9D4008595A3007C8C9B00A1B0
      BE00BFCEDA00BAC9D800B4C4D400B4C4D400B4C4D400B3C3D400B3C3D300B2C2
      D300B1C1D200D5DEE7003A5874008F8F8F008F8F8F00B5B5B500E4E4E4000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000DBCAB000956B3600986F
      3A009A723E009D7541009F784500A27B4900A47E4D00A7815100A9845400AC87
      5800AE8A5B00B08C5E00B28F6100B4916400B5936700B7956900B8976B00B998
      6D00BA996E00BB9A6F00BB9B7000BC9B7100BC9B7100BC9B7100BB9B7000BB9A
      6F00BA996E00B9986D00B9996E00BB9B7100BD9E7600BD9E7700BC9D7500BA9B
      7300B9997100B7976F00B5956C00B4936A00B1916700B08F6500AE8D6200AD8B
      5F00AA895E00A9875B00E2D5BF006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00C4AB7E00916731008B5F27008D62
      2A0090652E0081796F00FDFCFC00E5E2DE00E7E4E0000E640D00D3EBDC007EBC
      93006CB3860072B78B0077BB91007DBE960082C29B0086C5A0008CCAA60090CC
      AA0094D0AE0099D3B3009DD6B700A1D8BA00A4DABE00A8DCC100AADEC400ADE0
      C700AFE1C900B0E2CA00B2E2CB00B2E2CC00B2E2CC00B1E2CB00B1E1C900B0E0
      C800AFDEC500ADDDC300ACDCC200AADABF00A8D9BD00A7D7BB00A5D5B900A2D4
      B600A1D2B4009FD1B2009DCFAF009CCDAD00ECF6F000005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002D2D
      2D004646460058585800464646002C2C2C001212120000000000000000007E98
      B000819AB200849CB30087A0B6008AA2B8008DA5BA0091A7BC0094AABE0097AC
      C1009BAFC2008598AA0066798B00697B8C008092A20099A9B800C2D1DD00C5D2
      DE00C8D3DE00CAD6E000A4B1BE0094A2B000798A98007B8B9A0098A7B500C1D0
      DC00C3D1DD00C4D2DE00BDCDDA00B6C7D600B5C5D500B5C5D500B4C4D400B3C4
      D400B2C3D300D7E0E8003A587400F1F1F100F5F5F500D8D8D800B5B5B5000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000DECEB500976E38009971
      3C009C7440009F784500A17B4900A47E4D00A7815100A9845400AC875800AE8A
      5C00B08D5F00B2906300B5926600B6956900B8976B00BA996E00BB9A7000BC9C
      7200BD9D7300BE9E7400BF9F7500BF9F7600BF9F7600BF9F7600BF9F7500C0A1
      7800C1A37C00C4A78100C4A78300C3A78200C2A68000C0A47F00C0A27D00BEA1
      7B00BC9F7800BB9D7600BA9B7400B8997100B6977000B4956D00B2936A00B191
      6800AF8F6500AD8D6200E6D9C7006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00C8B08700946B35008D622A009065
      2E009268320081796F00FDFCFC00E5E2DE00E7E4E0000E640D00D8EDE00085C0
      990074B88D0079BB92007EBF970083C29C0088C5A1008DCAA60093CDAB0097D0
      AF009BD4B3009FD6B800A4D9BC00A7DBC000AADDC300AEE0C700B1E1CA00B3E3
      CC00B6E4CE00B7E5D000B8E5D000B8E6D100B9E5D000B8E5D000B8E4CE00B6E3
      CD00B5E1CB00B4E0CA00B2DFC800B1DEC500AFDCC200ADDAC000ACD9BF00AAD7
      BC00A8D6BA00A6D4B800A5D3B600A3D1B400F0F8F300005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002E2E
      2E004646460058585800464646002C2C2C001212120000000000000000007F99
      B000829BB200849DB40088A0B7008BA3B8008EA5BB0092A8BD0095ABBE0098AE
      C1009CB0C3009FB2C40090A1B300728495006B7D8E006D8090006F8191007183
      9200738494007586950076889600788998007F909D00A5B3C100C1D0DC00C3D1
      DD00C5D2DE00C5D3DF00C8D4DF00C1D0DC00B9CAD700B6C7D600B6C7D500B5C5
      D500B4C4D400D8E0E8003A587400C8C8C800CECECE00F7F7F7008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000E0D2BB0098703B009B73
      3F009E774300A17A4700A37D4C00A6815000A9845400AC875800AE8A5C00B18D
      5F00B3906300B5936600B7966A00B9986D00BB9A7000BD9C7200BE9E7400BFA0
      7600C0A17800C1A27900C2A37A00C3A47C00C5A78100C8AB8700CAAF8C00CBAF
      8C00CAAF8C00CAAE8B00C9AD8A00C9AC8900C7AB8800C5AA8600C4A88400C2A6
      8100C1A48000BFA27D00BDA07B00BC9F7900BA9D7700B89B7400B6987200B596
      6E00B3956C00B2936A00E8DDCD006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00CBB38D00966E3A0090652E009268
      3200956C360081796F00FDFCFC00E5E2DE00E7E4E0000E640D00DDEEE3008CC3
      9F007BBC930080BF980085C29D008BC7A2008FCAA70094CDAC0099D0B0009DD4
      B500A1D6B900A5D9BD00AADCC100ADDEC400B1E0C900B4E2CC00B7E4CF00BAE6
      D100BBE7D300BDE8D500BEE9D600BFE9D600BEE8D500BEE7D400BDE6D300BCE6
      D200BBE4D000B9E3CE00B8E2CC00B7E0CA00B5DFC900B4DEC700B2DCC400B0DA
      C200AFD9C000AED8BE00ACD7BC00ABD5BA00F3F9F500005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002F2F
      2F004646460058585800464646002C2C2C001212120000000000000000007F99
      B100829BB200869EB50088A0B7008CA4B90090A6BB0093A9BD0096ABC0009AAE
      C2009DB0C300A0B3C700A3B6C900A6B9CA008D9FB0008395A400708192007284
      93007385950076869600899AA8009DADBB00BFCDDA00C0CFDC00C3D1DD00C4D2
      DE00C7D4DF00C8D4E000C9D6E000CBD6E100C3D2DE00BBCAD900B7C8D600B6C7
      D600B5C5D500D9E1E9003A587400D3D3D300D3D3D300F6F6F6008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000E2D5C0009A723D009D75
      4200A0794600A37C4A00A5804F00A8835300AB865700AE8A5B00B08D5F00B390
      6300B5936700B8966A00BA996E00BC9B7100BE9E7400C0A07600C1A27900C3A4
      7D00C8AA8200CAAE8900CEB18F00CFB59300D0B59400CFB59400D0B59400CFB5
      9400CFB59300CEB39300CDB39200CDB29000CCB18F00CAAF8D00C9AE8B00C8AC
      8900C4AA8700C3A88400C1A68300C0A48000BEA27E00BCA07B00BA9E7900B99C
      7600B89A7400B6987200EAE1D3006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00CEB8940099723E0092683100956B
      3600986F3A0081796F00FDFCFC00E5E2DE00E7E4E0000E640D00E1F1E70092C8
      A50083C0990087C39D008CC7A20091CAA70096CDAC009AD0B1009FD3B500A3D6
      B900A8D9BE00ABDCC100AFDEC500B3E1CA00B6E3CD00BAE5D100BCE7D300BFE8
      D500C1EAD800C3EBD900C4EBDA00C5ECDA00C4EBDA00C4EAD900C3E9D700C1E8
      D600C0E7D400BFE6D300BEE5D100BDE3CF00BBE2CD00B9E0CB00B8DFC900B7DE
      C800B6DDC500B4DBC300B3DAC100B1D9C000F5FBF700005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003030
      30004646460058585800464646002C2C2C001212120000000000000000008099
      B100839CB400869FB5008AA2B7008DA4BA0090A7BC0094A9BD0096ACC0009AAF
      C2009EB2C500A1B4C700A4B7C900A7B9CC00AABCCD00ADBECF00B0C1D100B3C3
      D300B6C7D400B8C8D700BBCAD800BDCCD900C0CEDB00C1D0DC00C4D2DD00C5D3
      DE00C8D5E000CAD5E100CBD7E100CCD7E200CDD8E200C5D2DF00BBCBD900B7C8
      D600B6C7D500DAE2E9003A587400DEDEDE00DEDEDE00F8F8F8008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000E5D8C5009C7340009E77
      4400A17B4800A47E4D00A7825100AA855600AD895A00B08C5E00B2906300B593
      6600B8966A00BA996E00BC9C7200C1A17900C4A78100C9AC8900CDB29000CFB5
      9400D1B79600D1B89700D3B99900D4BA9A00D4BB9B00D4BB9B00D4BA9B00D3BA
      9B00D3BA9A00D2B99900D1B89800D0B79800CFB69600CEB49400CCB39200CBB1
      9000CAAF8E00C8AD8C00C5AC8A00C4A98700C2A88500C0A58300BFA48000BDA1
      7E00BB9F7B00B99E7900EDE5D8006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00D1BC9B009C754200946B3500976E
      39009A723E0081796F00FDFCFC00E5E2DE00E7E4E0000E640D00E4F3EA0099CC
      AA0089C39E008EC7A30092CAA70097CDAC009CD0B100A0D4B500A5D7BA00A9D9
      BE00ADDCC200B1DEC700B5E1CA00B8E3CE00BBE5D100BEE7D400C1E9D700C4EA
      D900C8ECDC00C9EDDD00CAEEDE00CBEDDE00CBEDDD00CAECDC00C8EBDB00C7EA
      DA00C7E9D800C5E8D700C3E7D500C2E6D300C1E4D100BFE3D000BEE2CE00BDE1
      CC00BCDFCB00BBDEC900B9DDC800B9DCC700F8FCF900005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003131
      31004646460058585800464646002C2C2C001212120000000000000000008099
      B100849DB400879FB6008AA2B8008EA4BA0091A7BC0094AABF0098ADC0009BAF
      C2009EB2C500A1B5C700A5B7CA00A8BACC00ABBCCD00AEBFD000B1C1D100B4C4
      D300B6C7D500B9C9D700BBCBD800BECDDA00C0CFDC00C3D1DD00C4D2DE00C8D4
      DF00C9D5E100CBD6E100CCD8E200CDD8E300CED9E300CED9E300C5D3DF00BACA
      D800B6C7D600DAE2EA003A587400E8E8E800E8E8E800FAFAFA008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000E7DCCA009D754200A079
      4600A37D4B00A6804F00A9845400AC885900AF8B5D00B3906300B8966C00BC9D
      7400C1A37C00C5A98400CBAF8C00CDB19000CEB49300D1B69600D2B89900D3BA
      9B00D4BC9D00D5BD9F00D6BEA000D7BFA200D8C0A200D7C0A300D7C0A200D7BF
      A200D6BFA100D6BEA000D5BD9F00D4BC9E00D3BB9D00D2B99B00D1B89900CFB6
      9700CDB49500CBB39300CBB09100C9AF8E00C7AC8C00C4AB8A00C3A98700C1A7
      8500BFA58200BEA38000EFE7DC006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00D4C0A1009F794800976E38009A71
      3D009D75410081796F00FDFCFC00E5E2DE00E7E4E00010661000E8F5ED00A2D0
      B20090C7A30095CAA80099CDAD009ED1B200A2D3B600A6D6BA00AAD9BE00AEDC
      C200B3DEC700B6E0CA00BAE3CE00BDE5D100C0E7D400C3E9D700C7EBDA00C9EC
      DC00CBEDDE00CEEFE000D0F0E200CFEFE100CEEEE000CEEEDF00CDEDDE00CDEC
      DD00CCEBDC00CAEADA00C9E9D800C8E8D700C7E7D600C5E6D400C4E5D200C3E4
      D100C2E3D000C1E2CF00C0E0CD00C3E2CF00FAFDFB00005C0000E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003333
      33004646460058585800464646002C2C2C00121212000000000000000000819A
      B200849DB40087A0B6008BA2B8008EA5BA0091A7BD0095ABBF0098ADC0009BB0
      C3009FB2C500A2B5C900A5B7CA00A9BBCC00ABBDCF00AFC0D000B1C1D200B4C4
      D400B8C7D600BACAD700BDCCD900BFCDDB00C2D0DC00C3D1DD00C7D3DF00C8D5
      E000CAD6E100CCD7E200CDD9E300CFD9E400CFDAE400CFDAE400CFDAE400C4D2
      DE00B7C8D600DBE3EA003A587400F2F2F200F2F2F200FCFCFC008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000E9DFCF009E774400A27C
      4A00A7825200AD895C00B2906400B7976E00BC9E7800C1A47F00C3A88400C7AB
      8800C9AE8C00CCB08F00CEB39300D0B69600D2B99900D4BB9C00D5BE9F00D7BF
      A100D8C1A300D9C2A500DAC3A700DAC4A800DBC4A800DBC5A900DBC4A900DBC4
      A900DAC4A800DAC3A700D8C2A600D7C1A500D6C0A300D5BEA100D4BCA000D3BB
      9E00D1B99C00CFB79900CEB69800CDB49500CBB19300C9B09000C8AE8E00C5AC
      8C00C3AA8900C1A98800F1EAE1006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00D7C3A800A17C4C0099703B009C74
      40009F78450081796F00FDFCFC00E5E2DE00E7E4E0006F9E6C00C0DCC500F1F8
      F400F7FBF800F7FBF800F7FBF900F7FCF900F8FCFA00F8FCFA00F9FCFA00F9FC
      FA00F9FCFB00FAFDFB00FAFDFC00FAFDFC00FAFDFC00FBFEFC00FBFEFC00FBFE
      FC00FBFEFD00FBFEFD00FCFEFD00FCFEFD00FCFEFD00FBFEFD00FBFEFD00FBFE
      FD00FBFDFC00FBFDFC00FBFDFC00FBFEFC00FBFDFC00FBFDFC00FBFDFC00FBFD
      FC00FBFDFC00FAFDFB00FAFDFB00F9FCFA00EBF4EC0057905500E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003434
      34004646460058585800464646002C2C2C00121212000000000001010100819B
      B200859DB40088A0B7008BA2B8008FA5BA0092A9BD0095ABBF0099AEC1009DB0
      C3009FB3C500A3B5C900A6B9CA00A9BBCD00ACBECF00AFC0D000B2C3D300B5C4
      D400B8C8D600BBCAD800BDCCD900C0CEDB00C2D0DC00C4D1DD00C7D4DF00C9D5
      E000CBD6E100CCD8E200CFD9E400CFDAE400D0DAE400CFDAE400CFDAE400CFDA
      E400C1D0DC00DBE3EA003A587400F9F9F900F9F9F900FEFEFE008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000EDE5D800AE8E6300B393
      6A00B6976F00B99A7300BB9E7800BEA27C00C1A58100C4A98600C8AC8A00CAAF
      8E00CDB39200CFB59500D1B89900D3BB9C00D6BEA000D7C0A300D9C2A500DAC4
      A700DCC5AA00DDC8AC00DEC9AD00DEC9AE00DECAAF00DFCAAF00DFCBAF00DECA
      AF00DEC9AE00DDC9AD00DCC8AC00DBC5AB00DAC4AA00D8C3A700D7C1A600D5C0
      A400D4BEA200D3BCA000D1BB9E00CFB89C00CEB69900CCB59600CBB39500C9B1
      9300C8AF9000C7AD8E00F3EDE5006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00D9C9AE00A47F50009B733F009E77
      4400A17B490081796F00FDFCFC00E5E2DE00E7E4E000D2D8CC006D9C69000E64
      0D00005C0000005C0000005C0000005C0000005C0000005C0000005C0000005C
      0000005C0000005C0000005C0000005C0000005C0000005C0000005C0000005C
      0000005C0000005C0000005C0000005C0000005C0000005C0000005C0000005C
      0000005C0000005C0000005C0000005C0000005C0000005C0000005C0000005C
      0000005C0000005C0000005C00000B630B0057915600B7CBB400E9E6E200EBE8
      E400F6F5F5008881770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003535
      35004646460058585800464646002C2C2C00121212000000000001010100819A
      B200859EB40088A0B7008BA3B8008FA5BB0092A8BD0096ACBF0099AEC1009CB1
      C300A0B3C700A3B6C900A6B8CA00AABCCD00ACBECF00B0C1D100B3C2D300B5C5
      D400B8C8D600BBCAD800BECDD900C0CEDB00C2D0DD00C4D2DE00C7D4DF00C9D5
      E000CBD7E100CDD8E300CED9E400D0DAE400D0DBE500D0DAE400D0DAE400CFDA
      E400CCD8E200DFE6EC003A5874000000000000000000000000008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000F0E9DF00B4946C00B698
      7000B99C7500BC9F7A00BEA27E00C2A68200C5AA8700C8AD8B00CBB19000CEB3
      9300D0B79700D2BA9C00D5BC9F00D6BFA200D9C2A600DBC4A900DCC7AB00DEC9
      AE00DFCBB000E0CDB200E1CEB300E2CEB400E2CFB500E2CFB600E1CFB500E1CF
      B500E1CEB400E0CDB400DFCCB200DECBB000DDC9AF00DBC8AD00DAC5AC00D9C4
      AA00D8C2A800D6C0A600D5BFA400D3BDA200D1BB9F00D0B99D00CEB79C00CDB5
      9900CBB59800CAB39500F4F0E8006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00DCCCB300A58253009D754200A079
      4700A47D4C0081796F00FCFBFB00E7E4E100E7E4E000E8E5E100E9E6E200EAE8
      E500EBE9E600EDECE800EEEDE900F0EFEC00F1F0ED00F2F1EE00F4F3F100F5F4
      F200F6F5F300F8F7F500F8F8F700F9F9F800FAFAF900FBFBFA00FCFCFB00FDFD
      FD00FEFEFE00FEFEFE0000000000FEFEFE00FEFEFE00FDFDFD00FCFCFB00FBFB
      FA00FAFAF900F9F9F800F8F8F700F8F7F500F6F5F300F5F4F200F4F3F100F2F1
      EE00F1F0ED00F0EFEC00EEEDE900EDECE800EBE9E600EAE8E500E9E6E200EDEA
      E700F5F5F3008A82790000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003737
      37004646460058585800464646002C2C2C00121212000101010002020200829A
      B200859EB50088A1B7008CA3B90090A6BB0092A8BD0096ACC0009AAEC1009CB1
      C400A0B4C700A3B6C800A7B9CB00AABBCD00ADBECF00B0C0D100B3C3D200B6C5
      D500B8C8D600BBCBD800BDCCD900C0CFDB00C2D0DD00C5D2DE00C7D4DF00C9D5
      E100CBD6E200CCD8E200CED9E300CFDAE400CFDAE400D0DAE400CFDAE400D0DA
      E400CFDAE400E4EAEF003A5874000000000000000000000000008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000F2ECE200B6997200BA9C
      7700BCA07B00BFA37F00C3A78400C5AB8800C9AE8D00CBB29100CEB59600D0B8
      9900D3BB9E00D5BEA100D8C1A500D9C4A800DCC7AB00DDC9AE00DFCCB100E1CD
      B300E1CFB600E3D1B700E4D2B900E4D3BA00E4D3BB00E5D4BB00E5D3BB00E4D3
      BA00E3D2BA00E3D1B900E2D0B700E1CFB600E0CEB500DECCB300DDCAB200DCC9
      B000DAC7AE00D9C5AC00D7C3AA00D6C1A800D5C0A600D3BEA300D2BCA100D0BB
      A000CFB99E00CDB79C00F6F2EC006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00DED0B800A88557009F774400A27C
      4A00A6804F008A7B6900E3E1DF00F2F0EE00E8E5E200E8E5E100E9E6E200EAE8
      E500EBE9E600EDECE800EEEDE900F0EFEC00F1F0ED00F2F1EE00F4F3F100F5F4
      F200F6F5F300F8F7F500F8F8F700F9F9F800FAFAF900FBFBFA00FCFCFB00FDFD
      FD00FEFEFE00FEFEFE0000000000FEFEFE00FEFEFE00FDFDFD00FCFCFB00FBFB
      FA00FAFAF900F9F9F800F8F8F700F8F7F500F6F5F300F5F4F200F4F3F100F2F1
      EE00F1F0ED00F0EFEC00EEEDE900EDECE800EBE9E600EAE8E500EAE8E400F5F3
      F100DCD9D700A49F970000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003838
      38004646460058585800464646002C2C2C00121212000202020003030300829B
      B200859DB50089A1B6008CA4B9008FA6BB0093A9BE0097ABBF0099AEC1009DB1
      C400A1B4C700A3B7C900A7B9CB00AABCCE00ADBECF00B0C1D100B3C3D300B6C5
      D500B9C9D600BBCAD800BECDDA00C0CEDB00C3D0DC00C4D2DE00C7D4DF00C9D5
      E000CBD7E100CCD7E200CED9E300CED9E300CFDAE400CFDAE400CFDAE400CFDA
      E400CFDAE300E7ECF2003A5874000000000000000000000000008F8F8F000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000F4EFE600B99D7800BDA1
      7D00C0A58100C2A88600C5AB8A00C9AF8E00CCB29300CEB69700D0B99B00D3BC
      9F00D5BFA300D8C2A600DAC4AA00DCC8AD00DECBB000E0CDB400E2CFB600E3D1
      B900E4D3BB00E6D4BD00E7D6BE00E7D6BF00E7D7C000E8D7C100E7D7C000E7D6
      C000E6D6C000E5D4BE00E4D4BD00E4D2BC00E2D1BA00E1CFB800E0CEB700DECD
      B500DDCBB300DBCAB100DAC8AF00D9C7AE00D7C4AC00D6C2AA00D5C1A800D3BF
      A600D2BDA400D1BCA300F7F4EE006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00E0D2BD00AA875A00A17A4800A682
      5100AC895B009F886800968F8700E8E7E500FBFBFA00FAFAF900FBFAF900FBFA
      FA00FBFBFA00FBFBFA00FCFBFB00FCFCFB00FCFCFB00FCFCFC00FDFDFC00FDFD
      FC00FDFDFD00FEFDFD00FEFEFD00FEFEFE00FEFEFE00FEFEFE00FEFEFE000000
      0000000000000000000000000000000000000000000000000000FEFEFE00FEFE
      FE00FEFEFE00FEFEFE00FEFEFD00FEFDFD00FDFDFD00FDFDFC00FDFDFC00FCFC
      FC00FCFCFB00FCFCFB00FCFBFB00FBFBFA00FBFBFA00FBFAFA00FBFBFA00E5E4
      E200918A8200D9D6D30000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003A3A
      3A004646460058585800464646002C2C2C00131313000202020004040400829B
      B300859EB40089A0B7008CA3B90090A7BC0093A9BD0096ACBF009AAEC2009DB1
      C400A0B3C700A4B6C900A7B9CB00AABCCD00ADBECF00B0C1D100B3C3D300B6C5
      D400B8C8D600BBCBD800BDCCD900C0CEDB00C2D0DC00C4D2DE00C7D4DF00C9D5
      E000CAD6E100CCD7E200CDD8E200CED9E300CED9E300CFD9E400CFDAE300CFDA
      E400CFDAE400E7ECF2003A5874000000000000000000D7D7D700B7B7B7000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000F6F1EA00BDA27E00C0A5
      8200C3A98700C5AC8C00C9B09000CCB39400CFB69800D1B99C00D3BDA000D5C0
      A400D9C3A800DBC5AC00DCC9AF00DFCCB200E1CEB500E2D0B800E4D3BC00E5D5
      BE00E7D6C000E8D8C200E9D9C400EADAC500EADAC700EADBC700EADAC700E9DA
      C500E8D9C400E7D8C300E7D7C200E6D6C000E5D4BF00E3D3BD00E2D2BC00E1D0
      BA00E0CFB800DECDB700DDCCB500DCCAB300DAC9B100D9C7AF00D8C5AE00D6C3
      AC00D5C2AA00D4C1A900F9F6F1006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00E6DBC900B89B7600B1906600B595
      6D00B89A7200BC9E7800A59177008C81720081796F0081796F0081796F008179
      6F0081796F0081796F0081796F0081796F0081796F0081796F0081796F008179
      6F0081796F0081796F0081796F0081796F0081796F0081796F0081796F008179
      6F0081796F0081796F0081796F0081796F0081796F0081796F0081796F008179
      6F0081796F0081796F0081796F0081796F0081796F0081796F0081796F008179
      6F0081796F0081796F0081796F0081796F0081796F0081796F0081796F009A94
      8C00CECBC7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F6F6F600000000003B3B
      3B004646460058585800464646002C2C2C00131313000303030004040400829B
      B200869EB40089A1B7008CA3B9008FA6BB0093A9BD0097ACC00099AFC1009DB1
      C400A1B4C700A4B6C800A7B9CB00AABBCD00ADBECF00B0C0D100B3C3D200B5C5
      D500B8C8D600BBCAD700BDCCD900C0CEDB00C2CFDC00C4D2DD00C7D3DF00C8D4
      E000CAD6E000CBD7E100CCD8E200CDD8E200CED9E300CED9E300CFDAE300CFD9
      E300CFD9E300E7EDF1003A5874008F8F8F008F8F8F00B7B7B700E6E6E6000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000F7F3ED00C0A68500C3A9
      8900C5AD8D00CAB19100CCB49500CEB79900D1BA9E00D4BDA100D6C0A500D8C4
      AA00DAC7AD00DCCAB000DFCDB400E1CFB700E3D2BA00E4D4BD00E6D6C000E8D8
      C200E9DAC500EADBC700EBDCC900ECDDCB00ECDECB00ECDECC00ECDECB00EBDD
      CA00EBDCCA00EADBC900E9DAC700E8D9C500E7D8C400E5D6C200E4D5C100E3D4
      BF00E2D2BD00E1D1BC00DFD0BA00DECEB800DDCDB700DCCBB500DACAB300DAC9
      B200D8C7B000D7C5AF00FAF7F4006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00EAE1D100BCA17D00B5966E00B89B
      7400BC9F7A00BFA37E00C3A78400C7AC8A00CAB08F00CEB39300D0B79800D3BB
      9D00D5BEA100D8C1A500DBC5A900DDC8AC00DECAAF00E0CCB200E1CDB400E2CF
      B600E2D0B600E3D0B700E2CFB700E2CFB600E1CFB500E0CDB400DFCDB300DDCA
      B100DCC9B000DAC7AD00D9C4AB00D8C3A900D6C1A800D5C0A600D3BEA300D1BC
      A100D0BA9F00CEB89D00F6F1EB006C460000E9E6E200EBE8E400F6F5F5008881
      7700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F0F0F000000000003C3C
      3C004646460058585800464646002D2D2D00131313000404040005050500829B
      B200859EB50089A0B6008CA4B90090A6BB0092A9BE0096ACBF009AAEC2009DB1
      C400A0B3C500A3B6C900A7B9CB00AABBCD00ACBECF00B0C0D000B3C3D300B5C5
      D400B8C7D500BACAD800BDCCD900BFCEDA00C1CFDC00C3D1DD00C5D3DE00C8D4
      DF00C9D5E000CBD6E100CBD7E100CCD7E200CDD8E200CED9E300CED9E300CED9
      E300D0DAE400E7ECF1003A587400000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000F8F5F000C3AB8A00C7AE
      8E00CAB19300CCB49700CEB79B00D1BB9F00D4BEA300D6C1A700D8C4AA00DBC8
      AE00DDCAB200DFCDB500E1D0B900E3D2BB00E5D5BE00E6D7C100E8D9C400EADB
      C700EBDCCA00ECDECC00EDDFCE00EEE1CF00EEE1D000EEE1D000EEE1D000EDE0
      CE00EDDFCE00ECDECD00EBDDCB00EADCCA00E9DBC900E8DAC800E6D8C500E5D7
      C300E4D6C200E3D5C000E2D3BF00E1D2BD00DFD1BC00DECFBA00DDCEB900DCCD
      B700DCCCB700DACAB500FBF9F6006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00ECE4D600C0A68400B89B7500BCA0
      7B00C0A48000C3A88600C8AD8B00CBB19100CEB59500D1B99B00D4BD9F00D7C0
      A400D9C3A800DBC7AC00DECAAF00E0CDB300E2CFB600E3D1B800E4D3BA00E5D4
      BC00E6D4BD00E5D5BD00E6D5BE00E5D4BD00E5D3BC00E3D2BB00E2D0B900E1CF
      B800DFCEB600DECDB400DDCBB200DBC9B100DAC7AE00D8C5AD00D7C3AA00D5C1
      A800D4C0A700D2BEA500F7F4EE006C460000E9E6E200EBE8E400F6F5F5008881
      7700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F0F0F000000000003F3F
      3F004646460058585800474747002D2D2D00141414000505050006060600829B
      B300859EB40089A0B6008CA3B9008FA6BA0093A8BD0096ABBF009AAEC1009CB1
      C300A0B4C700A3B6C800A7B9CB00A9BCCD00ACBDCE00AFC0D100B2C2D200B5C4
      D300B7C7D600BAC9D700BDCCD800BECDDA00C1CFDB00C2D0DC00C5D2DE00C7D3
      DF00C9D4DF00CAD5E000CBD6E100CCD7E100CDD8E200CDD8E200CED8E200CED8
      E300D9E1E900E1E7EE0046627C00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000F9F7F300C7AF9000CAB2
      9400CDB59800CFB99C00D1BBA000D4BFA400D6C1A800D8C4AC00DBC8B000DDCB
      B300DFCEB600E1D0BA00E3D2BD00E4D5C000E6D7C300E8D9C500EADCC900EBDD
      CB00EDDFCE00EEE0D000EFE2D100F0E3D300F0E4D400F0E4D400EFE3D300EFE2
      D300EEE2D200EDE0D000ECE0CF00EBDECE00EADECD00E9DCCC00E8DBCA00E8DA
      C900E6D9C700E5D7C500E4D6C400E3D5C200E2D4C100E1D3BF00E0D2BE00DFD1
      BD00DECFBC00DDCEBB00FCFAF8006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00EEE7DB00C4AB8B00BCA17C00C0A5
      8200C3A98700C8AE8D00CBB29200CFB69800D1BA9D00D4BDA100D7C1A500DAC5
      AA00DCC9AE00DFCCB200E1CFB600E3D1B900E5D4BC00E6D5BF00E7D7C100E8D8
      C200E9D9C400E9D9C400E8D9C400E8D8C300E7D8C200E6D6C100E5D5BF00E4D4
      BE00E2D3BC00E1D1BB00E0D0B900DECEB700DDCCB600DCCAB300DAC9B100D9C7
      AF00D7C5AE00D6C3AC00F9F6F1006C460000E9E6E200EBE8E400F6F5F5008881
      7700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F0F0F000000000004141
      41004646460058585800474747002E2E2E00151515000505050007070700829B
      B200859DB40088A0B7008CA3B8008FA5BB0093A9BD0095ABBF0099AEC1009CB1
      C400A0B3C500A2B6C900A6B9CA00A9BBCC00ACBECE00AFBFD000B2C2D200B4C4
      D400B7C7D500BAC9D700BBCBD800BECDD900C0CEDB00C2D0DC00C4D1DD00C5D3
      DE00C8D3DF00C9D5DF00CAD5E100CBD6E100CCD7E100CCD7E100CDD7E200D5DD
      E600EAEFF300D0D9E100687F9400000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E1006C460000FAF8F500CAB39600CDB6
      9A00CFB99E00D1BCA100D4BFA600D6C2A900D9C5AD00DBC9B100DDCBB400DFCE
      B700E1D1BB00E3D3BD00E5D6C100E7D8C400E8D9C700EADCCA00EBDECC00EDE0
      CF00EEE1D100EFE3D300F0E4D500F1E5D700F2E7D800F1E6D700F1E5D700F0E5
      D600EFE4D500EFE3D400EDE2D300EDE1D200ECE0D000EBDFCF00EADECE00E9DD
      CD00E8DCCC00E7DBCB00E6D9C900E5D8C800E4D7C700E3D6C500E2D5C300E2D5
      C200E1D3C100E0D2C000FDFBF9006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00F0E9DF00C8B09200C0A68400C4AA
      8900C8AE8F00CBB29400CEB79900D2BB9D00D4BEA200D7C2A700DAC5AC00DDCA
      B000DFCCB400E2D0B800E4D3BC00E5D5BF00E8D7C200E9DAC500EADBC700EBDC
      C900ECDDCA00ECDDCA00EBDDCA00EBDCCA00EADBC900E9DAC800E8D9C500E7D8
      C400E6D6C200E4D5C100E3D4BF00E1D2BD00E0D0BB00DFCFB900DDCEB800DCCC
      B600DBCBB500DAC9B400FAF8F4006C460000E9E6E200EBE8E400F6F5F5008881
      7700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F0F0F000000000004242
      42004646460063636300646464004E4E4E003C3C3C0032323200373737009BAF
      C200A0B3C400A3B6C800A8BACB00ABBDCD00B0C0CF00B3C3D200B8C7D500BBC9
      D700BECDD800C2CFDA00C5D2DD00C9D5DF00CCD7E100CED9E200D1DBE300D3DD
      E500D6DEE600D8E0E800DAE2E900DCE3EA00DDE5EC00DFE6ED00E0E7ED00E2E8
      EE00E3E9EF00E3E9EF00E4EAEF00E5EBF000E5EBF000E5EBF000E6EBF000E6EB
      F100CED8E00096A7B600B9C3CE00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E100704C0800FBF9F500D7C5AE00D0BA
      A000D2BDA300D4C0A700D6C3AA00D8C5AE00DBC9B200DDCCB500DFCEB800E1D1
      BC00E2D3BE00E5D6C200E6D8C500E8DAC800E9DCCB00EBDECD00ECE0CF00EEE1
      D200EFE3D400F0E4D600F1E6D700F2E7D900F2E7DA00F2E7DA00F1E7D900F1E6
      D900F0E6D800F0E5D700EFE4D600EEE3D500EDE2D400ECE1D300EBE0D200EBDF
      D100EADFD000E9DDCF00E8DCCD00E7DCCD00E7DBCB00E6DACA00E5D9C900E4D8
      C800E3D7C800E6DBCD00FDFCFB006C460000E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00F2ECE300CDB59900C4AB8A00C8AF
      8F00CCB39500CFB79A00D2BB9F00D4BFA400D7C3A800DAC7AD00DDCAB100DFCD
      B500E2D1B900E4D4BD00E6D6C100E8D9C400EADBC800EBDDCA00ECDFCD00EEE0
      CE00EEE1D000EEE1D000EDE0CF00EDE0CF00ECDFCE00EBDECD00EADCCB00E9DC
      CA00E8DAC800E6D9C700E6D7C500E4D6C200E3D4C100E1D3C000E1D2BE00E0D1
      BD00DED0BC00DDCFBA00FBF9F7006C460000E9E6E200EBE8E400F6F5F5008881
      7700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F9F9F900000000004444
      44005B5B5B0076767600505050001A1A1A00050505000101010017242F003A58
      74003A5874003A5874003A5874003A5874003A5874003A5874003A5874003A58
      74003A5874003A5874003A5874003A5874003A5874003A5874003A5874003A58
      74003A5874003A5874003A5874003A5874003A5874003A5874003A5874003A58
      74003A5874003A5874003A5874003A5874003A5874003A5874003A5874003A58
      74003A587500B3BFCB0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E100A28C6400DAD0BB00F8F5F000FCFA
      F800FCFAF800FCFAF800FCFAF900FCFBF900FDFBF900FDFBFA00FDFCFA00FDFC
      FA00FDFCFA00FDFCFA00FDFCFB00FDFCFB00FEFCFB00FEFDFB00FEFDFC00FEFD
      FC00FEFDFC00FEFDFC00FEFDFC00FEFDFC00FEFDFC00FEFDFC00FEFDFC00FEFD
      FC00FEFDFC00FEFDFC00FEFDFC00FEFDFC00FEFDFC00FEFDFC00FEFDFC00FEFD
      FC00FEFDFC00FDFDFC00FDFDFB00FDFCFB00FDFCFB00FDFCFB00FDFCFB00FDFC
      FB00FDFCFB00FBFAF700F3EFE8009C845900E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00F3EFE700CFBA9F00C8B09100CCB4
      9700CFB89C00D2BCA000D4BFA500D7C3A900DAC7AE00DCCBB200DFCEB600E2D1
      BB00E4D4BE00E6D7C200E8D9C700EADCCA00ECDECC00EEE0CF00EFE2D100EFE3
      D300F0E4D500F0E4D500F0E4D400EFE3D300EEE2D200EDE1D100ECE0D000EBDE
      CF00EADDCD00E9DCCC00E8DBCA00E6DAC900E6D9C800E4D7C500E3D6C400E2D5
      C300E2D4C200E1D3C100FCFBF9006C460000E9E6E200EBE8E400F6F5F5008881
      7700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004646
      460065656500323232000101010086868600CCCCCC00DBDBDB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BDCB
      D7003C5A7700F3F5F70000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E0DFDD00E1E0DE00E2E1E000E4E2E100E5E4E200AC9A78007A591A006C46
      00006C4600006C4600006C4600006C4600006C4600006C4600006C4600006C46
      00006C4600006C4600006C4600006C4600006C4600006C4600006C4600006C46
      00006C4600006C4600006C4600006C4600006C4600006C4600006C4600006C46
      00006C4600006C4600006C4600006C4600006C4600006C4600006C4600006C46
      00006C4600006C4600006C4600006C4600006C4600006C4600006C4600006C46
      00006C460000795717009C855900CEC4B600E2E1E000E1E0DE00E0DFDD00DFDE
      DC00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000734F0D00F6F2EB00D4C1A800CCB59800CFB9
      9D00D2BDA200D4BFA600D7C3AA00DAC8AF00DDCBB300DFCFB800E1D1BC00E4D4
      BF00E6D7C300E8DAC700EADDCB00ECDFCE00EDE0D000EFE3D300F0E4D500F1E6
      D700F2E7D900F2E7D900F1E6D800F0E6D700EFE4D600EFE4D500EEE3D400EDE2
      D300ECE1D100EBDFD000EADECF00E9DDCE00E8DCCD00E7DBCC00E6DACB00E5D9
      CA00E4D9C900E5DACC00FDFCFA006C460000E9E6E200EBE8E400F6F5F5008881
      7700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004747
      47002828280058585800E4E4E400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FDFEFE00A5B7
      C8003C5A7700F3F5F70000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007A736A00FCFC
      FC00E1E0DE00E1E0DE00E2E1E000E4E2E100E5E4E200E6E5E400E8E7E600E9E8
      E700EBEAE900ECEBEA00EDECEB00EEEDED00EFEFEE00F2F1F000F3F2F200F4F3
      F300F5F5F400F6F6F500F8F7F700F8F7F700F9F8F800FAFAF900FBFBFB00FCFC
      FC00FCFCFC00FEFDFD00000000000000000000000000FEFDFD00FCFCFC00FCFC
      FC00FBFBFB00FAFAF900F9F8F800F8F7F700F8F7F700F6F6F500F5F5F400F4F3
      F300F3F2F200F2F1F000EFEFEE00EEEDED00EDECEB00ECEBEA00EBEAE900E9E8
      E700E8E7E600E6E5E400E5E4E200E4E2E100E2E1E000E1E0DE00E0DFDD00E0DF
      DD00FCFCFC007A736A0000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000A48E6600E1D7C500F9F6F200FBFAF700FBFA
      F700FCFAF800FCFAF800FCFBF900FCFBF900FCFBF900FDFCFA00FDFCFA00FDFC
      FA00FDFCFB00FDFCFB00FEFCFB00FEFDFB00FEFDFB00FEFDFC00FEFDFC00FEFD
      FC00FEFDFC00FEFDFC00FEFDFC00FEFDFC00FEFDFC00FEFDFC00FEFDFC00FEFD
      FC00FEFDFC00FEFDFC00FEFDFC00FDFDFB00FDFCFB00FDFCFB00FDFCFB00FDFC
      FB00FDFCFB00FCFBF900F5F2EC009B835500E9E6E200EBE8E400F6F5F5008881
      7700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000B0B0B003939
      39000101010094949400FBFBFB00FBFBFB00FBFBFB00FBFBFB00FBFBFB00FBFB
      FB00FBFBFB00FBFBFB00FBFBFB00FBFBFB00FBFBFB00FBFBFB00FBFBFB00FBFB
      FB00FBFBFB00FBFBFB00FBFBFB00FBFBFB00FBFBFB00FBFBFB00FBFBFB00FBFB
      FB00FBFBFB00FBFBFB00FBFBFB00FBFBFB00FBFBFB00FBFBFB00FBFBFB00FBFB
      FB00FBFBFB00FBFBFB00FBFBFB00FBFBFB00FBFBFB00FBFBFB00E8EBEF00A2B4
      C5003A587500FDFDFD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000857E7600F3F3
      F100E6E5E400E1E0DE00E2E1E000E4E2E100E5E4E200E6E5E400E8E7E600E9E8
      E700EBEAE900ECEBEA00EDECEB00EEEDED00EFEFEE00F2F1F000F3F2F200F4F3
      F300F5F5F400F6F6F500F8F7F700F8F7F700F9F8F800FAFAF900FBFBFB00FCFC
      FC00FCFCFC00FEFDFD00000000000000000000000000FEFDFD00FCFCFC00FCFC
      FC00FBFBFB00FAFAF900F9F8F800F8F7F700F8F7F700F6F6F500F5F5F400F4F3
      F300F3F2F200F2F1F000EFEFEE00EEEDED00EDECEB00ECEBEA00EBEAE900E9E8
      E700E8E7E600E6E5E400E5E4E200E4E2E100E2E1E000E1E0DE00E0DFDD00E5E5
      E300F3F2F100857E760000000000000000000000000000000000000000008179
      6F00FDFCFC00E5E2DE00E7E4E000D8D1C300A58F6700724E0B006C4600006C46
      00006C4600006C4600006C4600006C4600006C4600006C4600006C4600006C46
      00006C4600006C4600006C4600006C4600006C4600006C4600006C4600006C46
      00006C4600006C4600006C4600006C4600006C4600006C4600006C4600006C46
      00006C4600006C4600006C4600006C4600006C4600006C4600006C4600006C46
      00006C460000724E0B009B835600D0C5B400E9E6E200EBE8E400F6F5F5008881
      7700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005E5E5E001313
      13001515150048484800EBEBEB00F5F5F500F5F5F500F5F5F500F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500D9DEE400A0B2
      C4003C5A76000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B8B4B000C0BD
      B800F7F6F600E7E7E500E3E2E100E4E2E100E5E4E200E6E5E400E8E7E600E9E8
      E700EBEAE900ECEBEA00EDECEB00EEEDED00EFEFEE00F2F1F000F3F2F200F4F3
      F300F5F5F400F6F6F500F8F7F700F8F7F700F9F8F800FAFAF900FBFBFB00FCFC
      FC00FCFCFC00FEFDFD00000000000000000000000000FEFDFD00FCFCFC00FCFC
      FC00FBFBFB00FAFAF900F9F8F800F8F7F700F8F7F700F6F6F500F5F5F400F4F3
      F300F3F2F200F2F1F000EFEFEE00EEEDED00EDECEB00ECEBEA00EBEAE900E9E8
      E700E8E7E600E6E5E400E5E4E200E4E2E100E2E1E000E2E1DF00E7E6E400F6F6
      F600C0BDB800B8B4B0000000000000000000000000000000000000000000827B
      7100FCFBFB00E7E4E100E7E4E000E8E5E100E9E6E200EAE8E500EBE9E600EDEC
      E800EEEDE900F0EFEC00F1F0ED00F2F1EE00F4F3F100F5F4F200F6F5F300F8F7
      F500F8F8F700F9F9F800FAFAF900FBFBFA00FCFCFB00FDFDFD00FEFEFE00FEFE
      FE0000000000FEFEFE00FEFEFE00FDFDFD00FCFCFB00FBFBFA00FAFAF900F9F9
      F800F8F8F700F8F7F500F6F5F300F5F4F200F4F3F100F2F1EE00F1F0ED00F0EF
      EC00EEEDE900EDECE800EBE9E600EAE8E500E9E6E200EDEAE700F5F5F3008A82
      7900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F6F6F6000202
      020014141400313131006F6F6F00C2C2C200E3E3E300EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00DBDFE4008DA2
      B700566F87000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFEFEE007D76
      6D00BEBBB700EEEEED00FAFAFA00FAF9F900FAFAF900FAFAFA00FAFAFA00FBFA
      FA00FBFBFB00FBFBFB00FBFBFB00FCFBFB00FCFCFC00FCFCFC00FDFCFC00FDFD
      FD00FDFDFD00FDFDFD00FEFDFD00FEFDFD00FEFEFE00FEFEFE00FEFEFE00FEFE
      FE00FEFEFE000000000000000000000000000000000000000000FEFEFE00FEFE
      FE00FEFEFE00FEFEFE00FEFEFE00FEFDFD00FEFDFD00FDFDFD00FDFDFD00FDFD
      FD00FDFCFC00FCFCFC00FCFCFC00FCFBFB00FBFBFB00FBFBFB00FBFBFB00FBFA
      FA00FAFAFA00FAFAFA00FAFAF900FAF9F900F9F9F900FAFAF900EEEEED00BEBB
      B7007D766D00EFEFEE0000000000000000000000000000000000000000009D97
      8F00E3E1DF00F2F0EE00E8E5E200E8E5E100E9E6E200EAE8E500EBE9E600EDEC
      E800EEEDE900F0EFEC00F1F0ED00F2F1EE00F4F3F100F5F4F200F6F5F300F8F7
      F500F8F8F700F9F9F800FAFAF900FBFBFA00FCFCFB00FDFDFD00FEFEFE00FEFE
      FE0000000000FEFEFE00FEFEFE00FDFDFD00FCFCFB00FBFBFA00FAFAF900F9F9
      F800F8F8F700F8F7F500F6F5F300F5F4F200F4F3F100F2F1EE00F1F0ED00F0EF
      EC00EEEDE900EDECE800EBE9E600EAE8E500EAE8E400F5F3F100DCD9D700A49F
      9700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D0D0
      D000686868000B0B0B00232323003E3E3E004B4B4B0032373C007E97AF008098
      B000829AB100849BB200859DB300879EB50089A0B6008AA1B7008CA2B8008EA4
      B90090A5BA0091A6BB0093A8BC0094A9BD0096AABE0097ABBF0099ACBF009AAE
      C0009BAEC1009CB0C2009EB0C3009FB1C300A0B2C400A1B3C500A1B4C500A2B4
      C500A2B4C500A2B4C500A2B4C500A2B4C500A2B4C500A2B4C50099ADBF004F6B
      8700A6B4C0000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F1F0
      EF00BAB6B10088827A007A736A007A736A007A736A007A736A007A736A007A73
      6A007A736A007A736A007A736A007A736A007A736A007A736A007A736A007A73
      6A007A736A007A736A007A736A007A736A007A736A007A736A007A736A007A73
      6A007A736A007A736A007A736A007A736A007A736A007A736A007A736A007A73
      6A007A736A007A736A007A736A007A736A007A736A007A736A007A736A007A73
      6A007A736A007A736A007A736A007A736A007A736A007A736A007A736A007A73
      6A007A736A007A736A007A736A007A736A007A736A007A736A0088827A00BAB6
      B100F1F0EF00000000000000000000000000000000000000000000000000D1CE
      CB00968F8700E8E7E500FBFBFA00FAFAF900FBFAF900FBFAFA00FBFBFA00FBFB
      FA00FCFBFB00FCFCFB00FCFCFB00FCFCFC00FDFDFC00FDFDFC00FDFDFD00FEFD
      FD00FEFEFD00FEFEFE00FEFEFE00FEFEFE00FEFEFE0000000000000000000000
      000000000000000000000000000000000000FEFEFE00FEFEFE00FEFEFE00FEFE
      FE00FEFEFD00FEFDFD00FDFDFD00FDFDFC00FDFDFC00FCFCFC00FCFCFB00FCFC
      FB00FCFBFB00FBFBFA00FBFBFA00FBFAFA00FBFBFA00E5E4E200918A8200D9D6
      D300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C9C9C9007D7D7D00414141001D2024002A3B4C003A5874003A58
      74003A5874003A5874003A5874003A5874003A5874003A5874003A5874003A58
      74003A5874003A5874003A5874003A5874003A5874003A5874003A5874003A58
      74003A5874003A5874003A5874003A5874003A5874003A5874003A5874003A58
      74003A5874003A5874003A5874003A5874003A5874003A58740046627C0096A6
      B500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CCC8C3009790880081796F0081796F0081796F0081796F0081796F008179
      6F0081796F0081796F0081796F0081796F0081796F0081796F0081796F008179
      6F0081796F0081796F0081796F0081796F0081796F0081796F0081796F008179
      6F0081796F0081796F0081796F0081796F0081796F0081796F0081796F008179
      6F0081796F0081796F0081796F0081796F0081796F0081796F0081796F008179
      6F0081796F0081796F0081796F0081796F0081796F009A948C00CECBC7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000000010000400000000100010000000000000800000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFF0000001FFF0000000000000000FFC000000000007FFFFE000000007FFF
      FFFC0000000000030000000000000000FF8000000000007FE000000000000007
      FFF80000000000030000000000000000FF8000000000003FE000000000000003
      FFF80000000000030000000000000000FF8000000000003FE000000000000003
      FFFC0000000000030000000000000000FF8000000000003FE000000000000003
      FFFC000F001E00030000000000000000FF8000000000003FE0001FFFFFF80003
      FFFC000E000E00070000000000000000FF8000000000003FE0000F8001F00003
      FFFC00007FC000070000000000000000FF8000000000001FE0000F8001F00007
      FFFE00000000000F0000000000000000FFC000000000001FF000079FF8E00007
      FFFF00000000001F0000000000000000FFC000000000001FF00000000400000F
      FFFC0000000000070000000000000000FFC000000000001FF80000000000000F
      FFF8FFFFFFFFFFE30000000000000000FFC000000000001FFC0000000000003F
      FFF800000F8000030000000000000000FFC000000000001FE000000000000007
      FFF80000020000030000000000000000FFC000000000001FC3FFFFFFFFFFFFC3
      F0000000020000030000000000000000FFC000000000001FC000000380000003
      E0000000020000030000000000000000FFC00000000001DFC000000380000003
      E0000000000000030000000000000000FFC00000000001DFC000000380000003
      F0000000000000030000000000000000FFC00000000001DFC000000380000003
      F0000000000000030000000000000000FFC000000000019FC000000380000003
      F0000000000000030000000000000000FFC000000000001FC000000380000003
      F0000000000000030000000000000000FFC00000000001FFC000000000000003
      F8000000000000030000000000000000FFC000000000001FC000000000000003
      FC000000000000030000000000000000FFC000000000001FC000000000000003
      F0000000000000030000000000000000FFC000000000001FC000000000000003
      E3F80000000000030000000000000000FFC000000000001FC000000000000003
      E0000000000000030000000000000000FFC000000000001FC000000000000003
      E0000000000000030000000000000000FFC000000000001FC000000000000003
      E0000000000000030000000000000000FFC000000000001FC000000000000003
      E0000000000000030000000000000000FFC000000000001FC000000000000003
      E0000000000000030000000000000000FFC00000000001DFC000000000000003
      E0000000000000030000000000000000FFC00000000001DFC000000000000003
      E0000000000000030000000000000000FFC00000000001DFC000000000000003
      E0000000000000030000000000000000FFC000000000019FC000000000000003
      E0000000000000030000000000000000FFC000000000001FC000000000000003
      E0000000000000030000000000000000FFC00000000001FFC000000000000003
      E0000000000000030000000000000000FFC000000000001FC000000000000003
      E0000000000000030000000000000000FFC000000000001FC000000000000003
      E0000000000000030000000000000000FFC000000000001FC000000000000003
      E0000000000000030000000000000000FFC000000000001FC000000000000003
      E0000000000000030000000000000000FFC000000000001FC000000000000003
      E0000000000000030000000000000000FFC000000000001FC000000000000003
      E0000000000000030000000000000000FFC000000000001FC000000000000003
      E0000000000000030000000000000000FFC000000000001FC000000000000003
      E0000000000000030000000000000000FFC00000000001DFC000000000000003
      E0000000020000030000000000000000FFC00000000001DFC000000000000003
      E0000000020000030000000000000000FFC00000000001DFC000000000000003
      E00000001FC000030000000000000000FFC000000000019FC000000000000003
      E0000000000000070000000000000000FF8000000000001FC000000000000003
      E000000000000FFF0000000000000000FF800000000001FFC000000000000003
      E000000000000FFF0000000000000000FF800000000001FFC000000000000003
      E000000000000FFF0000000000000000FF800000000001FFC000000000000003
      E000000000000FFF0000000000000000FF800000000001FFC000000000000003
      E000000000000FFF0000000000000000FF800000000003FFC000000000000003
      E000000000000FFF0000000000000000FFC03FFFFFFFE3FFC000000000000003
      E000000000000FFF0000000000000000FFC1FFFFFFFFC3FFC000000380000003
      E000000000000FFF0000000000000000FFC00000000003FFC000000380000003
      E000000000000FFF0000000000000000FFC00000000007FFC000000380000003
      E000000800000FFF0000000000000000FFC00000000007FFC0000007C0000003
      E000000800000FFF0000000000000000FFE00000000007FFE000000000000007
      E000007F00000FFF0000000000000000FFF8000000000FFFFFFFFFFFFFFFFFFF
      F000000000001FFF0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF000000000000000000000000000000000000000000000000
      000000000000}
  end
  object imgBtn: TImageList
    Left = 380
    Top = 384
    Bitmap = {
      494C010136008800040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000E0000000010020000000000000E0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007058400070504000604830007050400070504000705040007050
      40007050400087625A00977D7000B29C91000000000000000000000000000000
      00000000000000000000CC918A00D0B8B000E0E8E000C89D9D00B06870000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000080584000C07870007050400060403000B0686000B0687000C0707000C070
      7000C0788000C0808000C0808000A07060000000000000000000000000000000
      000000000000C0808000D0B8B000F0F8F000F0F8F000F0F8F000CC918A00B068
      7000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008058
      4000C0787000E0D8D000A078600090786000A0807000D0B8B000D0D0C000E0D8
      D000F0E0E000E0E0E000C0788000B0686000000000000000000000000000C88F
      8F00C0989000E0E8E000E0E8E000C0989000C0787000C0C0C000F0F8F000C888
      8100B06870000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000080584000C078
      7000D098A000E0D8D000A0786000E0D0C000E0D0C000A0786000B0908000E0D0
      D000F0E0E000F0E0E000C0808000B07070000000000000000000C0788000D0C8
      C000F0F8F000D0C8C000C0808000C0808000C0787000C0707000C0D0D000F0F8
      F000C8888100B068700000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080584000B0707000D098
      9000F0E8F000E0D8D000A0786000E0C8C000F0E0E000F0E8E000D0C0B000A078
      6000E0D8D000F0E8E000D0889000C0787000AC8F8F00C0909000E0F0E000E0E8
      E000C0909000C0888000D0808000D0808000C0808000C0788000C0707000C0D0
      D000F0F8F000C8888100B0687000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AC818100C0788000FFFF
      FF00F0E8F000E0D8D000A0806000E0C8C000F0E0E000F0F8F000FFFFFF00FFFF
      FF0090786000F0E8F000D0909000C0788000C0787000E0E8E000D0C0C000C078
      8000C0808000C0808000D0808000C0808000C0808000C0808000C0788000C070
      7000C0D0D000F0F8F000C8888100B06870000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B5807800C0788000FFFF
      FF00F0E8F000E0D8D000B0807000E0C8C000F0E0E000F0F8F000FFFFFF00FFFF
      FF0090786000F0E8E000D0909000D0888000C0808000D0A8B000C0808000D090
      9000D0909000D0889000D0889000D0888000D0808000D0808000C0808000C078
      8000C0707000C0D0D000F0F8F000C88F8F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0888000C0808000FFFF
      FF00F0E8F000E0D8D000B0887000E0C8C000F0E0E000F0F8F000FFFFFF00FFFF
      FF00A0786000F0E8E000D098A000D0889000D0A0A000D0A8B000D0909000E0A0
      A000E098A000D0989000D0909000D0909000D0909000D0889000D0889000D080
      8000C0787000C0687000C0D0D000E0E0E0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0888000C0808000FFFF
      FF00F0E8F000E0D8D000C0988000E0C8C000F0E0E000F0F8F000FFFFFF00FFFF
      FF00A0806000F0E8E000D098A000D090900000000000D0A0A000F0D0D000D098
      9000E0A8B000E0A8B000E0A8A000E0A0A000E098A000D0989000D0909000D088
      9000D0808000C0707000C0687000C0B0B0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0909000C0808000FFFF
      FF00F0E8F000E0D8D000D0988000E0C8C000F0E0E000F0F8F000FFFFFF00FFFF
      FF00B0807000F0E0E000D098A000D098A0000000000000000000D0A0A000F0C8
      D000D0989000E0B0B000F0B8B000E0B0B000E0A8B000E0A0A000E0A0A000D098
      9000D0909000D0888000C0707000C07070000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0989000C0888000FFFF
      FF00F0E8F000E0D0C000E0B8B000E0C8B000F0E0E000F0F8F000FFFFFF00FFFF
      FF00C0887000E098A000E0A8B00000000000000000000000000000000000D0A0
      A000F0C8D000D0989000E0B0B000F0C0C000F0B8B000F0B0B000E0B0B000E0A8
      B000E0A0A000D098A000D0909000D6968F000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0A0A000C0888000FFFF
      FF00F0E8E000E0C0B0000000000000000000D0B0A000D0A09000F0F0F000FFFF
      FF00C09070000000000000000000000000000000000000000000000000000000
      0000D0A0A000F0C8D000D0989000F0B8B000F0C8C000F0C0C000F0C0C000F0B8
      C000E0B0B000D0A8A000E4B7B700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E0B0B000D0908000FFFF
      FF00F0E8E000E2B5970000000000000000000000000000000000C0B0A000D0A0
      9000D0A890000000000000000000000000000000000000000000000000000000
      000000000000D0A0A000F0C8D000D0989000F0C0C000E0C0C000E0C0C000D0A8
      B000D7AFAF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D0A09000FFFF
      FF00E4BA9D000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D0A0A000F0D0D000E0A0A000D0A8A000D7AFAF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E0B0
      9000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D9A4A400D7AFAF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000003088F0003080E0003080E0002078E0002070
      D0002068D0002060D0002058C0002050C0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000087B1C400739DAC005080A0005070900040708000407090004981
      9D006399B7000000000000000000000000000000000000000000000000000000
      000000000000000000000000000010A0F00070D8FF0070D8FF0040B0F00040C0
      FF0000A0FF001088F00040B8FF000090FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000509050004080
      4000306830003050300020382000202820000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005090B00060A0C00080C0D00090D8E00080D8F00050C8F00020A8E0001090
      C0002080B00030689000659CBD00000000000000000000000000000000000000
      000000000000000000000000000020A8F00090E8FF0090E8FF0030A8F00080E0
      FF0050C0FF000068E00060D8FF000090FF000000000000000000000000000000
      000000000000CEC0A40091977B0097714B00B5714B00BD917B00C7B2A4000000
      000000000000000000000000000000000000000000007098700080E0A00080E8
      A00080E0A0004090500050B07000578865003040300000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006088
      A00040C0E00060D0F00090E0F000A0E8F00080E0F00050C8F00020B8F00000A8
      F00000A0E0001098D000406080000000000000000000C0A89000E0C8C000E0C8
      C000E0D0C000E0D0C000E0D0D00020A8F000A0F0FF0090E8FF0030A8F000A0E8
      FF0050C0FF001070E00070D8FF000090FF000000000000000000000000000000
      0000EAA7860080703000607020005060400090604000C0684000B0604000B28C
      7000000000000000000000000000000000000000000070A0800090E8B00080E8
      A00080E0A0004090500050B070005090600030403000B0504000A0403000A030
      2000872D1E000000000000000000000000000000000000000000000000006088
      A00040C8F00070D8F00090E0F000A0E8F00080E0F00050C8F00020B8F00000A8
      F00000A0E00000A0E000406080000000000000000000C0A8A00090786000D0B8
      B000D0B8B000D0B8B000D0B8B00030B0F000A0F0FF00A0F0FF0040B0F000A0E8
      FF0060C8FF001078E00080E0FF000098FF00000000000000000000000000EAAD
      9100E0885000D080400060882000308820005068300070604000B0684000B060
      4000B28C70000000000000000000000000000000000080B08000C0E8D00090F0
      B00080B08000C0D8C0004090500050A8700040604000F0988000B0584000F058
      4000C04020009028100000000000000000000000000000000000000000006090
      A00040C8F00070D8F00090E0F000A0E8F00080E0F00050C8F00020B8F00000A8
      F00000A0E0000098E000406880000000000000000000C0A8A00060503000FFF0
      F000FFE8F000FFE8E000FFE0E00030B0F000A0F8FF00A0F0FF0040B8F000A0F0
      FF0060C8FF001080E00090E8FF0010A0FF00000000000000000000000000F090
      6000F0906000C080500050784000309820003088200070683000B06840008060
      400060604000B2B9AB000000000000000000000000000000000080B8900070B0
      7000F0F0F000FFF8FF0070A0700040784000D0887000FF989000C0585000F058
      4000E05030009028100000000000000000000000000000000000000000006090
      A00040C8F00070D8F00090E0F000A0E8F00080E0F00050C8F00020B8F00000A8
      F00000A0E00000A0E000406880000000000000000000C0B0A00070504000FFF8
      FF00FFF8F000FFF0F000FFE8E00030B8F000A0F8FF00A0F0FF0040B8F000A0F0
      FF0060D0FF001080E00090E8FF0020A8FF000000000000000000F4B19300FF98
      6000E09060006078500050A0300050A0300080902000D0784000706840005060
      400030781000919C700000000000000000000000000000000000000000005050
      5000505050006060600040705000C0908000FFA09000E0908000F0B8B000C058
      5000E0584000A03820000000000000000000000000002D506500103850006090
      B00040C8F00070D8F00090E0F000A0E8F00080E0F000E0907000B0583000A050
      3000A0503000A0482000904820009048200000000000C0B0A00070584000FFFF
      FF00FFF8FF00FFF8F000FFF0F00050C0F000B0F8FF00A0F8FF0050C8F000B0F0
      FF0070D0FF002088F000A0E8FF0030B0FF000000000000000000FFA67800F098
      7000607050006098400060B0400090A04000E0886000D0805000506840003080
      2000309010003C872D000000000000000000000000000000000000000000A0A0
      A000A0A8A000808880005058500080686000C0686000F0F8F000FFF8FF00C058
      5000B04830000000000000000000000000003C80970060D0E00040B0D0002048
      600040C8F00070D8F00090E0F000A0E8F00080E0F000E0907000F0B8A000F0A0
      8000D0886000E0886000D07850009048200000000000D0B0A00070584000FFFF
      FF00FFFFFF00FFF8FF00FFF8F00030B0FF0030A8FF0030A0FF003098F0003090
      F0003088F0003080F0003078F0003070E0000000000000000000FFA678008080
      600040A0400050B0400090906000E0987000E0906000E088500060A020004098
      2000409820004B8F2D000000000000000000000000004038400030303000D0D8
      D000C0C0C000A0A0A0008088800060686000606060005058500060606000B048
      3000000000000000000000000000000000003C80970070E0F00070E8FF0050C0
      E000305070009098A00090E0F000A0E8F00080E0F000E0987000FFC0A000F0A8
      8000B0604000F0987000E0906000A050300000000000D0B8A00080605000FFFF
      FF00FFFFFF00FFFFFF00FFF8FF0030B0FF0080F0FF0080E8FF0070D8FF0060D0
      FF0050C0FF0040B0FF0030A0FF003098F0000000000000000000E8B793006098
      500040C8600040C8700070A07000B0A07000D0986000E0906000A090300040A0
      200040A020007BB865000000000000000000000000004B534B0050485000B0B8
      B000D0D8D000C0C0C000A0A0A00070787000D0D8D000B0B8B000808880005058
      500091919100000000000000000000000000000000003C80970070E8F00080F0
      FF0060C8E00020507000405060006070800080E0F000E0987000FFC8B000F0B0
      8000A0502000D0886000F0987000A050300000000000D0B8B00080605000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0030B0FF0030A8FF0030A0FF003098F0003090
      F0003088F0003080F0003078F0003070E0000000000000000000C7C7A70050B8
      600060D8800090E0A00080E0A000A0D09000B0A06000F0986000E088500050A8
      200040A02000ABD59D000000000000000000000000007D7D7D00606060006060
      60007070700060586000C0C0C00070707000F0E8F000D0D8D000B0B8B0008088
      80006068600000000000000000000000000000000000000000003C87A60080E8
      F00080F8FF0080F0F00070D0E0005090A00030587000A0807000FFC8B000FFB0
      9000FFB08000F0A88000FFA88000B058300000000000D0C0B00090706000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8FF00FFF8F000FFF8F000FFF0
      F000FFF0F000D0B8B000E0D0C000000000000000000000000000000000006FD6
      930070E09000C0F0B000D0F0D000F0F8D000B0E8A00080986000D0984000B090
      300086BD65000000000000000000000000000000000000000000707070008080
      8000A0A0A000909090005058500030303000C0C8C000E0E8E000D0D8D000B0B8
      B0007078700000000000000000000000000000000000000000000000000057A5
      C80080F0FF0080FFFF0090FFFF0080F8FF0080C8D00020506000FFD0B000FFC8
      B000FFC8B000FFC0A000FFB8A000C060400000000000E0C0B000907060009070
      6000907050008068500080605000806050007058400070584000705040006050
      300060503000A0908000E0D0C000000000000000000000000000000000000000
      00009FE2AB00C0F0B000E0F8D00080A8700070D8900040B0600080804000EAAD
      8600000000000000000000000000000000000000000000000000000000008A8A
      8A0090889000878787005757570040384000403840005048500030303000C0C0
      C0008780870000000000000000000000000000000000000000000000000070A7
      BD0070E0F00090F0F0005080900070D0E00090F0F00030709000E0A07000E098
      7000E0907000D0886000D0886000D078500000000000E0C0B000E0C0B000D0C0
      B000D0C0B000D0C0B000D0B8B000D0B8B000D0B8A000D0B8A000C0B0A000C0B0
      A000C0B0A000C0B0A000C0A8A000000000000000000000000000000000000000
      000000000000D7EFCF00D0EEC40097E2A6005ADA870063D08700CECBA4000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000404840006060600080808000706870003030
      300000000000000000000000000000000000000000000000000000000000739B
      AF0070C8E00090F0F000406870005070800090E8F0004080A00060606000B0D8
      E00090C8D00080A8C00040384000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000706A700060686000696969000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004B9EB50080D8E000B0F0F000B0E8F00090D8E00091B4C3008787870078AD
      B5009FC4D0000000000065656500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000003088A0003090B0004098B00086B8C80000000000000000008780
      8700606860007077700000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006953
      3C00605040006048300070504000706050007050400060483000604830006048
      30006048300060483000B1B1B700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B0A09000706050007058
      4000605040006048300060483000604830006048300060483000604830006048
      3000604830006048300060483000604830006048300000000000C0A8A000F0F0
      F000E0D8D000E0D0C000E0C8C0009090B000D0C0B000E0B8A000D0B0A000D0B0
      A000D0A89000D0A0900060483000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF000000840000008400FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00000000000000000000000000B0A09000F0E0D000F0D8
      D000E0D0C000E0C8B000E0C0B000E0C0B000E0C0B000E0C0A000E0B8A000E0B8
      A000E0B8A000E0B09000E0B09000E0B090006048300000000000C0A8A000FFF0
      F000F0F0F000F0E8E0003050C0001038B0007078C000E0D0D000F0D0C000E0D0
      C000E0C8B000D0A8900060483000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00000084000000FF000000FF000000
      8400FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00000000000000000000000000C0A89000FFF0F000B0B8
      C0001058C0001050B0000040900020509000B0A0A000FFE0D000F0E0D000F0D8
      C000F0D8C000F0D8C000F0D8C000E0B090006048300000000000C0A8A000FFF0
      F000D0D0E0003050C0003058F0002048E0001038B000A098C000F0D0C000F0D0
      C000E0C8B000D0A8900060483000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF000000FF000000FF000000FF000000FF000000
      8400FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00000000000000000000000000C0A8A000E0E8F0002060
      C00070A8F0004088E0000048A0001068D00000409000FFE8E000E0B8B000C0A0
      9000C0988000C0988000C0908000E0B8A0006048300000000000C0B0A000FFF8
      F0002040C0003058F0006080FF005078F0004060F0002040B000D0C0C000F0D8
      D000E0C8C000D0B0A00060483000000000000000000000000000000000000000
      0000FFFFFF00000000000000FF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF0000008400FFFFFF0000000000000000000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00000000000000000000000000C0B0A000F0F0F0003070
      C00070A8E0002058B0002070E0001050B0002058A000FFF0E000FFE0D000FFE0
      D000F0E0D000F0D8C000F0D8C000E0B8A0006048300000000000C0B0A000FFF8
      F00080A0FF008098FF008090F000D0D0E0008098F0004060E0004058B000F0D8
      D000F0D8D000D0B8A00060504000000000000000000000000000FFFFFF000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF0000008400FFFFFF0000000000000000000000000000000000FFFFFF000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00000000000000000000000000C0B0A000FFF8FF00E0E0
      E00030509000A09890008088A00030509000D0D8E000FFF0F000F0D0C000C0A0
      9000C0988000C0988000C0908000D0C0B0006048300000000000D0B0A000FFF8
      FF00E0E8FF00C0C8F000F0F0F000F0F0E000E0D8E0008090F0003058E0005068
      B000F0E0D000E0C8B00070584000000000000000000000000000FFFFFF000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000FF000000840000000000000000000000000000000000FFFFFF000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00000000000000000000000000C0B8A000FFFFFF006068
      600030282000B0B8B0008080800060687000FFF8FF00FFF8F000FFF0F000FFF0
      E000FFE8E000FFE8E000FFE0D000D0C0B0006048300000000000D0B8A000FFFF
      FF00FFF8FF00FFF8F000FFF8F000FFF0F000F0F0E000F0E0E0007088F0002050
      D0009090C000E0D0C00080706000000000000000000000000000FFFFFF000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000FF0000000000000000000000000000000000FFFFFF000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00000000000000000000000000D0B8B000FFF8FF005050
      500050485000E0E0E000C0C8C00080787000E0D8E000FFF8F000FFC8A000FFB8
      9000FFB08000FFA88000F0A07000D0C0B0006048300000000000D0B8B000FFFF
      FF00FFFFFF00FFF8FF00FFF8F000FFF8F000F0F0F000F0E0E000F0E8E0008090
      F0002048D000A098C000A0908000000000000000000000000000FFFFFF000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFFFF000000
      0000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00000000000000000000000000D0B8B000FFFFFF007068
      700070787000A098A0009098900080788000E0E0E000FFFFFF00FFF8F000FFF8
      FF00FFF8F000FFF8F000FFF8F000D0C8C0006050400000000000D0C0B000FFFF
      FF00FFFFFF0080A0B000608890006088900060789000607880007080900090A0
      B00090A0F0003050D000B0989000000000000000000000000000FFFFFF000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0C0B000FFFFFF00E0E0
      E000707870008088800070787000C0C8C000FFF8FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFF8F000FFF8F000D0C8C0007058400000000000D0C0B000FFFF
      FF00FFFFFF0080A8B00090D8E00090E8F00080D8F00060C8E0005098B0007080
      9000F0E8E000E0D8D000A0989000000000000000000000000000FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000000000D0C0B000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFF8FF00FFF8FF00FFFFFF008068500000000000D3C4B500FFFF
      FF00FFFFFF00F0F8FF0080A8B000A0A8A0009787780080C8D00050708000F0F0
      F000F0E0E000F0E0E00080706000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0C0B000D0C0B000D0C0
      B000D0C0B000D0B8B000D0B8B000C0B8A000C0B0A000C0B0A000C0A8A000C0A8
      A000C0A89000B0A09000B0A09000B0A09000B0A090000000000000000000D3C4
      B500D0C0B000D0C0B00070A8B000A0E8F000A0E8F00090D0E00040687000C0A8
      A000C0A8A000C0A8900000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000080B0C00080A0B0007090A000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0A09000604830006048
      3000604830006048300060483000604830006048300060483000604830006048
      3000604830006048300060483000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0A8A000604830006048
      3000604830006048300060483000604830006048300060483000604830006048
      3000604830006048300060483000604830000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0A09000FFFFF000E0D8
      D000E0D0D000D0C8C000D0C0B000D0B8B000C0B0A000C0A8A000C0A09000B098
      9000B0908000A08880006048300000000000B0A0900060483000604830006048
      3000604840006048300060483000604830006048300060483000604830006048
      30006048300060483000604830006048300000000000C0A8A000FFF8F000F0D8
      D000F0D8D000E0D0C000E0C8C000E0C0B000E0C0B000E0B8A000D0B0A000D0A8
      A000D0A89000D0A08000D0988000604830001098F0003078E0003070E0003068
      E0003060D0003058D0002058C0002050C0002050C0002048C0002048C0002048
      C0002048B0001048B0001048B0001048B00000000000C0A09000FFFFF000B0D0
      E00030708000A0A8B000FFE0C000FFD0C000F0C8B000F0C0A000E0B09000E0A8
      9000D0A08000B09080006048300000000000C0A8A000D0B8A000C0A08000B088
      7000E0B8A000D0A89000B0907000E0B8A000C0A08000B0887000D0B8A000C0A0
      8000B0887000D0A89000C09880006048300000000000C0A8A000FFF0F000FFF0
      F000FFF0E000FFE8E000F0E8E000F0E0E0009088800060606000404840004048
      40004048400030303000D0A090006048300010A0F00070D8FF000000000060D0
      FF000000000040C0FF000000000010B0FF000000000000A0FF00000000000098
      FF00000000000098FF000090FF001048B00000000000C0A09000FFFFF00000B0
      FF00C0FFFF0030688000FFE8D000B0A0A000B0A09000B0989000B0988000B090
      8000E0A89000B09890006048300000000000C0A8A000FFFFFF00FFFFFF00C098
      8000FFF8FF00FFF8F000C0988000FFF0F000FFF0F000C0988000FFE8E000FFE8
      E000C0A08000FFE0D000D0C0B0006048300000000000C0A8A000FFF8F000FFF0
      F000FFF0F000FFF0E000F0E8E000F0E8E000A0A0A000FFF8F000FFF0F000FFF0
      F000FFF0F00040404000D0B0A0006048300020A0F00090E8FF0090E0FF0080E0
      FF0080D8FF0070D0FF0060D0FF0060C8FF0050C0FF0050C0FF0040B8FF0040B0
      FF0030B0FF0030A8FF000090FF001048B00000000000C0A89000FFFFF000B0E0
      F00000B0FF00B0D0E000FFF0E000FFE0D000FFD8C000FFD0B000F0C0A000F0C0
      A000E0B09000C0A090006048300000000000C0A8A000FFFFFF00FFFFFF00D0A8
      9000FFFFFF00FFF8FF00D0A08000FFF8F000FFF0F000D0A08000FFE8E000FFE8
      E000D0A08000FFE0D000D0C0B0006048300000000000C0A8A000FFF8F0009090
      9000808880007070700060606000F0E8E000C0B8B000A0989000908880008080
      80008078700050505000E0B8A0006048300020A8F00090E8FF000000000080E0
      FF00D0A89000A0685000A06840009060400090604000905030008048300040B8
      FF000000000030B0FF000090FF002048B00000000000C0A8A000FFFFF000FFFF
      F000FFFFF000FFFFF000FFF8F000FFE8E000FFE0D000FFD8C000FFD0B000F0C0
      A000F0C0A000C0A8A0006048300000000000C0B0A000D0B09000C0988000B088
      7000D0A08000C0988000B0887000D0A08000C09880001040E0001038E0001038
      C0000030B000D0A08000C09880006048300000000000C0A8A000FFF8F000FFF8
      F000FFF0F000FFF0F000FFF0E000FFE8E000F0E8E000F0E0E000F0E0D000F0E0
      D000F0D8D000F0D8C000E0C0B0006048300020A8F000A0F0FF0090E8FF0090E8
      FF00D0A89000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A068500050B8
      FF0040B8FF0040B0FF000098FF002048B00000000000C0A8A000FFFFF00090A0
      FF003038A0009098D000FFF8F000FFF0E000FFE8D000FFE0C000FFD0C000FFC8
      B000F0C0A000C0B0A0006048300000000000C0B0A000FFFFFF00FFFFFF00C098
      8000FFFFFF00FFFFFF00C0988000FFF8FF00FFF8F0003060F000FFF8F000FFF8
      F0001038C000FFE8E000D0C0B0006048300000000000C0A8A000FFF8F000FFF8
      F000FFF8F000FFF0F000FFF0F000FFF0E0009088800060606000404840004048
      40004048400030303000E0C8C0006048300030B0F000A0F0FF000000000090E8
      FF00D0A89000D0A89000D0A89000D0A89000D0A89000D0A89000D0A8900050C0
      FF000000000040B8FF0010A0FF002048C00000000000C0A8A000FFFFF0004060
      FF0090B0FF003038B000FFFFF000C0A8A000B0A09000B0A09000B0989000B098
      9000F0C8B000D0B8B0006048300000000000C0B0A000FFFFFF00FFFFFF00D0A0
      8000FFFFFF00FFFFFF00D0A08000FFFFFF00FFF8FF006080FF00FFFFFF00FFF8
      F0001038E000FFE8E000D0C8C0006048300000000000C0A8A000FFF8F000FFF8
      F000FFF8F000FFF0F000FFF0F000FFF0E000A0A0A000FFF8F000FFF0F000FFF0
      F000FFF0F00040404000F0D8D0006048300040B0F000A0F8FF00A0F0FF0090F0
      FF0090E8FF0090E0FF0080D8FF0070D8FF0070D0FF0070D0FF0060C8FF0050C0
      FF0050C0FF0040B8FF0020A8FF002050C00000000000C0B0A000FFFFF00090A0
      FF004060FF0090A0FF00FFFFF000FFFFF000FFF8F000FFF0E000FFE0D000FFD8
      C000FFD0B000D0B8B0006048300000000000D0B0A000D0B09000C0988000B088
      7000D0A08000C0988000B0887000D0A08000C09880008098FF006080FF003060
      F0001040E000D0A08000C09880006048300000000000C0A8A000FFF8F0009090
      9000808880007070700060606000FFF0F000C0B8B000A0989000908880008080
      80008078700050505000F0E0E0006048300050B8F000A0F8FF0000000000A0F0
      FF00D0A89000A0685000A06840009060400090604000905030008048300060C8
      FF000000000050C0FF0030B0FF002050C00000000000C0B0A000FFFFF000FFFF
      F000FFFFF000FFFFF000FFFFF000FFFFF000FFFFF000FFF8E000FFE8E000FFE0
      D000FFD8C000D0B8B0006048300000000000D0B8A000FFFFFF00FFFFFF00C098
      8000FFFFFF00FFFFFF00C0988000FFFFFF00FFFFFF00C0988000FFF8FF00FFF8
      F000C0988000FFF0F000E0D8D0006050400000000000C0A8A000FFF8F000FFF8
      F000FFF8F000FFF8F000FFF8F000FFF0F000FFF0F000FFF0E000F0E8E000F0E8
      E000F0E0D000F0E0D000D0B8A0006048300050C0F000B0F8FF00A0F8FF00A0F0
      FF00D0A89000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A068500060C8
      FF0060C8FF0050C0FF0040B8FF002058D00000000000C0B0A000FFFFF000FF98
      A000C0303000FF907000FFFFF000FFFFF000FFFFF000FFF8F000FFF0E000FFE8
      D000FFE0C000D0C0B0006048300000000000D0B8A000FFFFFF00FFFFFF00D0A0
      8000FFFFFF00FFFFFF00D0A08000FFFFFF00FFFFFF00D0A08000FFFFFF00FFF8
      FF00D0A08000FFF8F000FFF0F0006048300000000000F0A89000F0A89000F0A8
      8000E0A08000E0987000E0907000E0906000E0806000D0805000D0784000D070
      4000D0704000D0704000D0704000D068300060C0F000B0F8FF0000000000A0F8
      FF00D0A89000D0A89000D0A89000D0A89000D0A89000D0A89000D0A8900070D0
      FF000000000050C0FF0050C0FF003060D00000000000D0B0A000FFFFF000FF80
      8000FFC0C000B0282000FFFFF000C0A8A000C0A8A000B0A09000B0A09000B098
      9000FFE8D000D0C8C0006048300000000000F0A89000F0A89000F0A88000F0A8
      8000F0A08000F0A07000E0987000E0907000E0906000E0886000E0805000E080
      5000E0784000E0784000E0704000C4693C0000000000F0A89000FFC0A000FFC0
      A000FFC0A000FFC0A000FFB8A000FFB89000FFB09000FFB08000F0A88000F0A0
      7000F0A07000F0987000F0986000D068300070C8F000B0F8FF00B0F8FF00A0F8
      FF00A0F0FF0090F0FF0090E8FF0090E8FF0080E0FF0080D8FF0070D0FF0070D0
      FF0060C8FF0050C0FF0050C0FF003068E00000000000D0B8A000FFFFF000FFA8
      B000FF808000FF989000FFFFF000FFFFF000FFFFF000FFFFF000FFFFF000FFF8
      F000FFF0E000D0D0C0006048300000000000F0A89000FFE8E000FFE8E000FFE8
      E000FFE8E000FFE8E000FFE8E000FFB09000FFA88000F0A88000F0A88000F0A0
      8000F0A08000F0A08000F0A07000D3693C0000000000F0A89000F0A89000F0A8
      8000E0A08000E0987000E0907000E0906000E0886000D0805000D0784000D070
      4000D0704000D0704000D0704000D068300070C8F000B0FFFF0000000000A0F8
      FF0000000000A0F0FF000000000090E8FF000000000080E0FF000000000070D0
      FF000000000060C8FF0050C0FF004068E00000000000D0B8A000FFFFF000FFFF
      F000FFFFF000FFFFF000FFFFF000FFFFF000FFFFF000FFFFF000FFFFF000FFFF
      F000FFF8E000FFE8E0006048300000000000F0A89000F0A89000F0A88000E0A0
      8000E0A08000E0987000E0907000E0906000E0886000E0805000D0805000D078
      5000D0704000D0704000D0704000D06830000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000070C8F00070C8F00070C8F00060C0
      F00050C0F00040B8F00030B8F00030B0F00030B0F00020B0F00020A8F00020A8
      F00020A8F00010A0F00010A0F00020A0F00000000000D0B8A000D0B8A000D0B8
      A000D0B0A000D0B0A000C0B0A000C0B0A000C0A8A000C0A8A000C0A89000C0A8
      9000C0A09000C0A09000C0A0900000000000A4A4A40000000000909090000000
      00008F8F8F000000000080808000000000008080800000000000707070000000
      0000606060000000000060586000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0B8A000604830006048
      3000604830006048300060483000604830006048300060483000604830006048
      30006048300060483000604830000000000000000000B0A09000604830006048
      3000604830006048300060483000604830006048300060483000604830006048
      300060483000604830006048300000000000B0A0900060483000604830006048
      3000604830006048300060483000604830006048300060483000604830006048
      300060483000604830006048300000000000B0A0900060483000604830006048
      3000604830006048300060483000604830006048300060483000604830006048
      30006048300060483000604830000000000000000000D0B8A000FFFFFF00F0E0
      D000E0D0C000E0C8C000E0C0B000E0C0B000E0B8A000D0B0A000D0A8A000D0A8
      9000D0A08000D0988000604830000000000000000000B0A09000FFFFFF00F0E0
      D000E0D0C000E0C8C000E0C0B000E0C0B000E0B8A000D0B0A000D0A8A000D0A8
      9000D0A08000D09880006048300000000000B0A09000FFF8F000F0D8D000F0D8
      D000E0D0C000E0C8C000E0C0B000E0C0B000E0B8A000D0B0A000D0A8A000D0A8
      9000D0A08000D09880006048300000000000B0A09000FFF8F000F0E8E000F0E0
      D000F0D8D000F0D8C000F0D0C000E0C8C000E0C0B000E0C0B000E0B8A000E0B0
      A000D0A89000D0A89000604830000000000000000000D0B8A000FFFFFF00FFF8
      FF00FFF8F000F0F0F000F0E8E000F0E0E000606050008078700090888000A098
      9000A0A0A000D0A09000604830000000000000000000B0A09000FFFFFF00A0A8
      A000908880008078700070707000706860006060600060586000605860006068
      600090808000D0A090006048300000000000B0A09000FFFFFF00D0C0B000D0C0
      B000D0B8B000D0B0A000D0A8A000C0A89000C0A09000C0988000C0908000C090
      8000C0887000D0A090006048300000000000B0A09000FFFFFF00B0989000FFF0
      F000B0908000F0E8E000B0888000F0D8D000A0807000E0C8C000A0786000E0C0
      B000A0706000E0B09000604830000000000000000000D0B8A000FFFFFF00A0A0
      A0008088800080808000A0989000F0E8E00030303000FFFFFF00FFFFFF00FFFF
      FF0090909000D0A89000604830000000000000000000B0A09000FFFFFF00FFFF
      FF00FFF8F000FFF0F000F0F0F000F0E8E000F0E0E000F0D8D000F0D8D000F0D0
      C000E0C8C000D0A890006048300000000000B0A09000FFFFFF00FFFFFF00FFFF
      FF00FFF8F000FFF0F000F0F0F000F0E8E000F0E0D000F0D8D000F0D0C000E0C8
      C000E0C0B000D0A890006048300000000000B0A09000FFFFFF00B0A09000FFFF
      FF00B0989000FFF0F000B0908000F0E8E000B0887000F0D8D000A0807000E0C8
      C000A0786000E0B8A000604830000000000000000000D0B8A000FFFFFF00FFFF
      FF00FFFFFF00FFF8F000FFF0F000F0F0F0001018100030282000403830005050
      500070605000D0A89000604830000000000000000000B0A09000FFFFFF006058
      50006060600070707000808080008078700070707000F0E0E000606060006060
      600080787000D0A890006048300000000000B0A09000FFFFFF00E0C8C000E0C8
      C000D0C0B000D0B8B000D0B8A000D0B0A000D0A8A000C0A09000C0A09000C098
      8000C0908000D0B0A0006048300000000000B0A09000FFFFFF00C0A09000FFFF
      FF00B0989000FFF8FF00B0908000FFF0F000B0888000F0E8E000A0807000F0D8
      D000A0786000E0C0B000604830000000000000000000D0B8A000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFF8F000FFF0F000F0F0F000F0E8E000F0E0E000F0D8
      D000F0D8D000D0B0A000604830000000000000000000B0A09000FFFFFF005048
      4000FFFFFF00FFFFFF00FFF8F000FFF0F00070686000F0E8E000F0E0E000F0D8
      D000F0D8D000D0B0A0006048300000000000C0A89000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFF8FF00FFF8F000F0F0F000F0E8E000F0E0
      E000F0E0D000E0C0B0006048300000000000C0A89000FFFFFF00C0A8A000FFFF
      FF00B0A09000FFFFFF00B0989000FFF8FF00FFF8F000F0F0F000F0E8E000F0E0
      E000F0E0D000E0C8C000604830000000000000000000D0B8A000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFF8F000606050008078700090888000A098
      9000A0A0A000E0B8B000604830000000000000000000B0A09000FFFFFF004040
      40003028200030282000303030005048500050505000F0F0F000707070007070
      700080808000E0B8B0006048300000000000C0A8A000FFFFFF0030303000A0A8
      A000FFFFFF00B0B0B00020202000FFFFFF00D0B0A000D0B0A000D0A89000C0A0
      9000C0989000E0C8C0006048300000000000C0A8A000FFFFFF00C0A8A000FFFF
      FF00C0A09000FFFFFF00B0989000FFFFFF0050505000B0B0B000FFF0F000C0B8
      C00040484000F0D0C000604830000000000000000000D0B8A000FFFFFF00A0A0
      A0009090900090888000A0989000FFFFFF0030303000FFFFFF00FFFFFF00FFFF
      FF0090909000E0C0B000604830000000000000000000C0A89000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8F000FFF0F000F0F0F000F0E8
      E000F0E0E000E0C0B0006048300000000000C0A8A000FFFFFF00908890002018
      2000302830003028300090889000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8
      F000FFF0F000E0D0C0006048300000000000C0A8A000FFFFFF00C0B0A000FFFF
      FF00C0A8A000FFFFFF00B0A09000FFFFFF00A0A0A00040404000505050005050
      5000A0A0A000F0E0D000604830000000000000000000D0B8A000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001018100030282000403830005050
      500070605000E0C8C000604830000000000000000000C0A8A000FFFFFF00B0B0
      B00090888000FFFFFF0050485000606060007068700080787000808080008078
      700080787000E0C8C0006048300000000000C0B0A000FFFFFF00F0E8F0006058
      5000F0F0F00030282000F0E8F000FFFFFF00D0C0B000D0B8B000D0B0A000D0B0
      A000C0A89000F0D8D0006048300000000000C0B0A000FFFFFF00C0B0A000FFFF
      FF00C0A8A000FFFFFF00C0A09000FFFFFF00F0F0F00070707000F0F0F0005048
      4000F0F0F000F0E8E000604830000000000000000000D0B8A000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8F000FFF0
      F000F0F0F000E0D0C000604830000000000000000000C0A8A000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0040404000FFFFFF00FFFFFF00FFFFFF00FFF8F000FFF0
      F00060606000E0D0C0006048300000000000D0B0A000FFFFFF00FFFFFF00D0D0
      D00080707000D0D0D000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00F0E0E0006048300000000000D0B0A000FFFFFF00C0B0A000FFFF
      FF00C0B0A000FFFFFF00C0A8A000FFFFFF00FFFFFF00D0D8D00090888000D0D8
      D000FFFFFF00FFF0E000604830000000000000000000D0B8A000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00606050008078700090888000A098
      9000A0A0A000F0D8D000604830000000000000000000C0B0A000FFFFFF00B0B0
      B00090989000FFFFFF0030303000202020002020200030383000504850005058
      500050505000F0D8D0006048300000000000D0B8A000FFFFFF00FFFFFF00FFF8
      FF0080808000FFF8FF00FFFFFF00FFFFFF00E0C8C000D0C0B000D0C0B000D0B8
      B000D0B0A000F0E0E0006048300000000000D0B8A000FFFFFF00C0B0A000FFFF
      FF00C0B0A000FFFFFF00C0A8A000FFFFFF00FFFFFF00FFFFFF0090989000FFFF
      FF00FFFFFF00FFF0E000604830000000000000000000D0B8A000FFFFFF00A0A8
      A0008088800080808000A0989000FFFFFF0030303000FFFFFF00FFFFFF00FFFF
      FF0090909000F0E0D000604830000000000000000000D0B0A000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFF8F000F0E0D0006048300000000000D0B0A000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF006048300000000000D0B0A000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00604830000000000000000000D0B8A000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF001018100030282000403830005050
      500070605000F0E8E000604830000000000000000000D0B8A000FFFFFF00B0B0
      B00090989000A098900090989000908890009088800080808000808080008080
      800090909000F0E8E0006048300000000000C0B0A000D0B8A000D0B8A000D0B8
      A000D0B0A000C0B0A000C0B0A000C0B0A000C0B0A000C0A8A000C0A8A000C0A8
      9000C0A89000C0A09000B0A0900000000000C0B0A000D0B8A000D0B8A000D0B8
      A000D0B0A000C0B0A000C0B0A000C0B0A000C0B0A000C0A8A000C0A8A000C0A8
      9000C0A89000C0A09000B0A090000000000000000000D0B8A000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00604830000000000000000000D0B0A000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0060483000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0B8A000D0B8A000D0B0
      A000C0B0A000C0B0A000C0B0A000C0B0A000C0A8A000C0A8A000C0A89000C0A8
      9000C0A09000B0A09000000000000000000000000000D0B8A000D0B8A000D0B0
      A000C0B0A000C0B0A000C0B0A000C0B0A000C0A8A000C0A8A000C0A89000C0A8
      9000C0A09000B0A0900000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000050786000607060005060
      5000405840003040300020302000202020001018100010181000101810001018
      1000101810001018100010181000000000000000000050786000607060005060
      5000405840003040300020302000202020001018100010181000101810001018
      1000101810001018100010181000000000000000000050786000607060005060
      5000405840003040300020302000202020001018100010181000101810001018
      1000101810001018100010181000000000000000000050786000607060005060
      5000405840003040300020302000202020001018100010181000101810001018
      1000101810001018100010181000000000005090500080B8800060A87000B0A0
      9000604830006048300060483000604830006048300060483000604830006048
      3000604830004070400030603000101810005090500080B8800060A8700060A0
      6000609860005098600050905000509050005088500050805000408050004078
      4000407040004070400030603000101810005090500080B8800060A8700060A0
      6000609860005098600050905000509050005088500050805000408050004078
      4000407040004070400030603000101810005090500080B8800060A8700060A0
      6000609860005098600050905000509050005088500050805000408050004078
      4000407040004070400030603000101810006098600090C8900080C08000C0A8
      9000FFF8F000FFF8F000FFF0E000FFF0E000FFE8D000FFE0D000FFE0D000FFE0
      D000604830005080500040704000101810006098600090C8900080C0800080B8
      8000B0A090006048300060483000604830006048300060483000604830006048
      3000508850005080500040704000101810006098600090C89000A0C0A00090C0
      900090B8900080B0800070A8800070A070006098600060906000509060005088
      5000508850005088500040704000101810006098600090C8900080C08000B0A0
      900080706000706050006048300060A0700060A06000B0A09000807060007060
      5000604830005080500040704000101810006098600090D0A00090C89000C0A8
      9000FFFFFF00FFF8F000FFF0E000FFF0E000FFE8D000FFE0D000FFE0D000FFE0
      D000604830005088500040704000101810006098600090D0A00090C8900080C0
      8000C0A89000FFF8F000FFF0E000FFF0E000FFE8D000FFE0D000FFE0D0006048
      3000508860005088500040704000101810006098600090D0A000A0C8A000A0C0
      A00090C0900090B8900080B0800080B0800070A8700070A07000609860006090
      6000508850005088500040704000101810006098600090D0A00090C89000C0A8
      A000FFF0E000FFE0D0006048300070A8700060A07000C0A8A000FFF0E000FFE0
      D0006048300050885000407040001018100060A06000A0D0A00090C89000C0A8
      A000FFFFFF00C0A09000C0A09000C0A09000C0988000C0908000C0908000FFE8
      D0006048300050886000407840001018100060A06000A0D0A00090C8900090C8
      9000C0A8A000FFFFFF00C0A09000C0A09000C0988000C0908000FFE0D0006048
      30006090600050886000407840001018100060A06000A0D0A000B09890006048
      300060483000604830006048300080B0800080B08000B0989000604830006048
      30006048300060483000407840001018100060A06000A0D0A00090C89000D0B0
      A000FFF8F000FFE8E0007060500070B0700070A87000D0B0A000FFF8F000FFE8
      E0007060500050886000407840001018100060A07000A0D8B00090D0A000C0B0
      A000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8F000FFF0E000FFE8D000FFE8
      E0006048300060906000408050001018100060A07000A0D8B00090D0A00090C8
      9000C0B0A000FFFFFF00FFFFFF00FFFFFF00FFF8F000FFF0E000FFE8D0006048
      30006098600060906000408050001018100060A07000A0D8B000C0A8A000FFE8
      E000FFD8D000E0C0B0006048300080B8900080B08000C0A8A000FFE8E000FFD8
      D000E0C0B00060483000408050001018100060A07000A0D8B00090D0A000D0C0
      B000FFFFFF00FFFFFF008070600070B8700070B07000D0C0B000FFFFFF00FFFF
      FF008070600060906000408050001018100070A87000B0D8B000A0D0A000D0B0
      A000FFFFFF00D0B0A000D0B0A000D0B0A000D0A8A000C0A09000C0A09000FFF0
      E0006048300060986000508050001018100070A87000B0D8B000A0D0A00090D0
      A000D0B0A000FFFFFF00D0B0A000D0B0A000D0A8A000C0A09000FFF0E0006048
      300060A0600060986000508050001018100070A87000B0D8B000C0B0A000FFF0
      E000FFE8E000FFE0D0006048300090B8900080B89000C0B0A000FFF0E000D0A8
      A000C098900060483000508050001018100070A87000B0D8B000A0D0A000E0C0
      B000D0C0B000D0B0A000C0A8A00080B8800070B87000E0C0B000D0C0B000D0B0
      A000C0A8A00060986000508050001018100070A88000B0D8B000A0D8B000D0B8
      A000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8F000FFF8
      F0006048300060A06000508850001018100070A88000B0D8B000A0D8B000A0D0
      A000D0B8A000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8F0006048
      300060A0700060A06000508850001018100070A88000B0D8B000D0B0A000C0A0
      9000FFF0F000FFE8E0006048300090C0900090B89000D0B0A000FFF0F000FFF0
      F000FFE8E00060483000508850001018100070A88000B0D8B000A0D8B000A0D0
      A00090D0A00090C8900090C8900080C0800080B8800070B8700070B0700070A8
      700060A0700060A06000508850001018100070B08000C0E0C000B0D8B000D0C0
      B000FFFFFF00E0C8C000E0C8C000E0C8C000D0C0B000D0B8B000D0B8B000FFF8
      F0006048300060A07000509050002020200070B08000C0E0C000B0D8B000A0D8
      B000D0C0B000FFFFFF00E0C8C000E0C8C000D0C0B000D0B8B000FFFFFF006048
      300070A8700060A07000509050002020200070B08000C0E0C000D0B8A000FFF8
      F000FFF8F000FFF0F00060483000A0C8A00090C0A000D0B8A000E0C8C000D0B8
      B000FFF0F00060483000509050002020200070B08000C0E0C000B0D8B000B0A0
      900080706000706050006048300090C8900080C08000B0A09000807060006048
      30006048300060A07000509050002020200080B08000C0E8D000C0E0C000D0B8
      A000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8F000FFFF
      FF006048300070A87000509050002030200080B08000C0E8D000C0E0C000B0D8
      B000D0B8A000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8F0006048
      300070B0700070A87000509050002030200080B08000C0E8D000D0C0B000D0B8
      B000D0A8A000FFF8F00060483000A0C8A000A0C8A000D0C0B000FFFFFF00FFF8
      F000FFF8F00060483000509050002030200080B08000C0E8D000C0E0C000C0A8
      A000FFF0E000FFE0D0006048300090C8900090C89000C0A8A000FFF0E000FFE0
      D0006048300070A87000509050002030200080B89000D0E8D000C0E0C000D0C0
      B000FFFFFF00E0C8C000E0C8C000E0C8C000D0C0B000D0B8B000D0B8B000FFFF
      FF006048300070B07000509860003040300080B89000D0E8D000C0E0C000C0E0
      C000D0C0B000FFFFFF00E0C8C000E0C8C000D0C0B000D0B8B000FFFFFF006048
      300070B8700070B07000509860003040300080B89000D0E8D000D0C0B000FFFF
      FF00FFFFFF00FFFFFF0060483000B0D0B000A0C8A000D0C0B000FFFFFF00FFFF
      FF00FFFFFF0060483000509860003040300080B89000D0E8D000C0E0C000D0B0
      A000FFF8F000FFE8E0007060500090D0A00090C89000D0B0A000FFF8F000FFE8
      E0007060500070B07000509860003040300080B89000D0E8D000D0E8D000D0C0
      B000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF006048300070B87000609860004058400080B89000D0E8D000D0E8D000C0E0
      C000D0C0B000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF006048
      300080B8800070B87000609860004058400080B89000D0E8D000E0C8C000D0C0
      B000D0B8A000C0B0A000B0989000B0D8B000B0D0B000E0C8C000D0C0B000D0B8
      A000C0B0A000B0989000609860004058400080B89000D0E8D000D0E8D000D0C0
      B000FFFFFF00FFF8F00080706000A0D0A00090D0A000D0C0B000FFFFFF00FFF8
      F0008070600070B87000609860004058400080B89000D0F0E000D0E8D000E0C0
      B000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF006048300080B8800060A060005060500080B89000D0F0E000D0E8D000D0E8
      D000E0C0B000E0C0B000D0C0B000D0C0B000D0B8B000D0B0A000C0A8A000C0A8
      900080C0800080B8800060A060005060500080B89000D0F0E000D0E8D000D0E8
      D000C0E8C000C0E0C000C0E0C000B0D8B000B0D0B000A0D0B000A0C8A000A0C8
      A00090C0900090B8900060A060005060500080B89000D0F0E000D0E8D000E0C0
      B000D0C0B000D0B0A000C0A8A000A0D8B000A0D0A000E0C0B000D0C0B000D0B0
      A000C0A8A00080B8800060A060005060500080C09000C0E0D000D0F0E000E0C0
      B000E0C0B000E0C0B000D0C0B000D0C0B000D0B8B000D0B0A000C0A8A000C0A8
      A000C0A8900090C8900080B880006070600080C09000C0E0D000D0F0E000D0E8
      D000D0E8D000C0E8D000C0E0C000B0D8B000B0D8B000A0D8B000A0D0A00090D0
      A00090C8900090C8900080B880006070600080C09000C0E0D000D0F0E000D0E8
      D000D0E8D000C0E8D000C0E0C000B0D8B000B0D8B000A0D8B000A0D0A00090D0
      A00090C8900090C8900080B880006070600080C09000C0E0D000D0F0E000D0E8
      D000D0E8D000C0E8D000C0E0C000B0D8B000B0D8B000A0D8B000A0D0A00090D0
      A00090C8900090C8900080B88000607060000000000080C0900080B8900080B8
      900080B8900080B0800070B0800070A8800070A8700070A0700060A0700060A0
      6000609860006098600050905000000000000000000080C0900080B8900080B8
      900080B8900080B0800070B0800070A8800070A8700070A0700060A0700060A0
      6000609860006098600050905000000000000000000080C0900080B8900080B8
      900080B8900080B0800070B0800070A8800070A8700070A0700060A0700060A0
      6000609860006098600050905000000000000000000080C0900080B8900080B8
      900080B8900080B0800070B0800070A8800070A8700070A0700060A0700060A0
      6000609860006098600050905000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008078700050382000503820005038
      20005038200050382000503820000000000000000000785A4B00604830006048
      3000604830006048300060483000604830006048300060483000604830006048
      30006048300069533C0000000000000000000000000060504000605040006048
      3000604830006048300060483000604830006048300060483000604830006048
      3000604830006048300000000000000000000000000000000000000000000000
      0000000000000000000080787000503820005038200050382000503820005038
      2000503820005038200000000000000000000000000000000000000000000000
      00000000000000000000000000000000000090807000B0B0B000A0A0A000A098
      900090889000808080005038200000000000C0A89000FFE8E000F0D8D000E0C8
      C000D0C8C000D0C8C000D0C0C000E0C8C000E0C8B000E0C0B000E0B8A000D0B8
      A000D0B8A000D0B8A00069533C0000000000A0887000FFFFFF00F0F0F000F0E8
      F000F0E8E000E0E0D000E0D8D000E0D8D000E0D8D000E0D0D000E0C8C000E0C0
      B000E0C0B000D0A8A00060483000000000000000000000000000000000000000
      0000000000000000000090807000B5B5B500B5ADA600A69EA60097979700978F
      87008F8F8F005038200000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A0908000C0C0C000C0B8B000B0B0
      B000B0A8A000909090005038200000000000C0A89000FFE8E000FFE8E000FFE8
      E00090888000907870008070700070686000705850006050500060484000FFE8
      E000FFE8E000D0B8B0006048300000000000B0887000FFFFFF00807060008068
      6000706860007060600070585000705850006058500060505000605040006048
      400060484000D0C0B00060483000000000000000000000000000000000000000
      00000000000000000000A0908000C0C0C000B0B8B000B0B0B000B0A8A000A098
      A000978F87005038200000000000000000000000000000000000000000007088
      900060708000606870005060600040506000A0989000D0C8C000C0C0C000C0B8
      B000B0B0B000A09890005038200000000000C0A8A000FFF0E000FFE8E000FFE8
      E000FFFFFF00FFFFFF00FFF8F000FFF0F000FFF0E000FFE8E000FFE8D000FFF0
      E000FFE8E000D0C0B0006048300000000000B0887000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFF8FF00FFF8F000FFF0F000FFF0E000FFE8E000FFE8D000FFE0
      D000FFD8C000E0C0B00060483000000000007088900060708000606870006068
      70005060600040506000A0989000C0C8C000C0C0C000C0B8B000B0B8B000B0A8
      A000979797005038200000000000000000000000000000000000000000008088
      900080D0F00060B0D00050A8D00040A0D000B0A0A000D0D8D000C0C0C000C0C0
      C000C0B8B000B0A8A0005038200000000000C0B0A000FFF0E000FFF0E000FFF0
      E000A0888000908080009078700080706000706860007058500060505000FFF0
      E000FFF0E000E0C8C0006050400000000000B0907000FFFFFF00807870008070
      7000807060008068600070686000706060007060500070585000605850006050
      500060504000E0C8C00060483000000000008088900080D0F00060B0D00060B0
      D00050A8D00040A0D000B0A0A000D0D0D000C0C0C000C0B8C000C0B8B000B0B0
      B000A6A6A6005038200000000000000000000000000000000000000000008090
      A00090D8F00070D0F00060C8F00050B8F000B0B0B000E0E0E000E0D8D000D0C8
      D000C0C0C000C0B8B0005038200000000000C0B0A000FFF0F000E0D0C0009058
      4000FFFFFF00FFFFFF00FFFFFF00FFF8FF00FFF8F000FFF0F000FFF0E000B080
      6000F0D8D000E0D0D0008060500000000000B0908000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFF8FF00FFF8F000FFF0F000FFF0E000FFE8
      E000FFE0D000E0D0C00060504000000000008090A00090D8F00070D0F00070D0
      F00060C8F00050B8F000B0B0B000E0D8E000D0D0D000C0C8C000C0C0C000C0B8
      B000B5ADB5005038200000000000000000000000000000000000000000008098
      A000A0E0FF0080D8F00070D0F00060C8F000B0B0B000B0B0B000B0A0A000A098
      9000A0908000908070008078700000000000D0B8A000F0D8D000A05840009058
      4000A0908000A08880009080800080787000807060007060600070585000A060
      4000B0806000D0C8C0008068500000000000B0988000FFFFFF00908070009078
      7000807870008070700080706000806860007068600070606000705850007058
      500060585000E0D8D00060483000000000008098A000A0E0FF0080D8F00080D8
      F00070D0F00060C8F000B0B0B000E0E0E000E0E0E000D0D0D000C0C8C000C0C0
      C000B5BCB5005038200000000000000000009080700060403000604030006040
      300060403000604030006040300070D0F00060C0F00050B8F0004098C0005060
      700000000000000000000000000000000000D0B8B000C0684000D07850009058
      4000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8FF00FFF8F000E080
      5000B0684000B08060009080700000000000C0988000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8FF00FFF8F000FFF0
      F000FFE8E000E0D8D00060504000000000008098A000A0E8FF0090D8F00090D8
      F00080D8F00070D0F000B0B0B000B0B0B000B0B0B000B0A0A000A0989000A090
      800090807000807870000000000000000000A0888000C0B8C000B0B0B000A0A0
      A000A0989000908880006040300080D8F00070D0F00060C0F00040A0D0006070
      800000000000000000000000000000000000D0B8B000F0D0C000F0805000D078
      5000A0908000A09080009088800090808000807870008070600070606000F088
      5000E0805000F0E0D0008070600000000000C0A08000FFFFFF00908880009080
      8000908070009078700080787000807070008070600080686000706860007060
      600070605000F0E0D000604830000000000090A0A000B0E8FF00A0E0F00090D8
      F00090D8F00080D8F00070D0F00060C0F00040A0D00060708000000000000000
      000000000000000000000000000000000000A0989000D0C8D000C0C0C000C0B8
      B000B0B0B000A09890006040300090D8F00080D8F00070C8F00050A8D0006070
      800000000000000000000000000000000000D0C0B000FFF8FF00FFE8E000F0C0
      A000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F090
      6000FFF0E000F0F0F0007058400000000000C0A08000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF8
      FF00FFF8F000F0E8E000605040000000000090A0B000C0F0FF00B0E8FF00A0E0
      F000A0E0F00090D8F00080D8F00070C8F00050A8D00060708000000000000000
      000000000000000000000000000000000000B0A0A000D0D8D000D0C8C000C0C0
      C000C0B8B000A0A0A00060403000A0E8FF00A0E0FF0090D8F00080D0F0006070
      800000000000000000000000000000000000D0C0B000FFFFFF00FFFFFF00FFFF
      FF00A0908000A0908000A090800090888000908070008078700080686000FFFF
      FF00FFFFFF00F0F0F0006048300000000000C0A08000FFFFFF00A0908000A088
      8000908880009080800090807000907870008078700080707000807060008068
      600070686000F0F0F000705040000000000090A0B000C0F0FF00B0E8FF00B0E8
      FF00A0E0F00090D8F00080D8F00070C8F00050A8D00060708000000000000000
      000000000000000000000000000000000000B0B0B000E0E0E000D0D0D000D0C8
      C000C0C0C000B0B0B0006040300090A0A0008098A0008098A0008090A0008090
      900000000000000000000000000000000000E2C4B500FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00735E490000000000C4A69700FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00605040000000000090A8B000C0F0FF00C0F0FF00C0F0
      FF00B0E8FF00A0E8FF00A0E0FF0090D8F00080D0F00060708000000000000000
      000000000000000000000000000000000000C0B8B000F0F0F000E0E0E000D0D8
      D000D0C8D000C0C0C00060403000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E0C0B000D0C0B000D0C0
      B000D0C0B000D0B8B000D0B8A000D0B8A000C0B0A000C0B0A000C0A8A000C0A8
      A000C0A89000BFB2A400000000000000000000000000C0A09000C0A09000C0A0
      9000C0A08000C0988000C0988000B0988000B0908000B0908000B0907000B090
      7000B0887000B29E8A00000000000000000090A8B00090A8B00090A0B00090A0
      B00090A0B00090A0A0008098A0008098A0008090A00080909000000000000000
      000000000000000000000000000000000000C0C0C000C0B8B000B0B0B000B0A0
      A000A0989000A088800090807000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000008078700070585000605040006048
      3000504030006040300050403000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000000000000000000000000
      00000000000000000000000000000000000090807000B0B0B000A0A0A000A0A0
      A000908880008078700050403000000000000000000000000000000000000000
      0000000000000000000080787000503820005038200050382000503820005038
      2000503820005038200000000000000000000000000000000000000000000000
      000000000000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084848400008484008484
      8400008484008484840084000000FFFFFF000000000000000000000000000000
      00000000000000000000FFFFFF00840000000000000000000000000000000000
      000000000000000000000000000000000000A0909000C0C0C000C0B8B000B0B0
      B000B0A8B0009090900050403000000000000000000000000000000000000000
      0000000000000000000090807000B5B5B500B5ADA600A69EA60097979700978F
      87008F8F8F005038200000000000000000000000000000000000000000000000
      000000000000000000000000000084000000FFFFFF0000000000000000000000
      00000000000000000000FFFFFF00840000000000000000848400848484000084
      8400848484000084840084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000000000000000000007088
      9000607080006068700050606000405060004048500030384000202830002020
      2000B0B0B000A0A0A00060483000000000000000000000000000000000000000
      00000000000000000000A0908000C0C0C000B0B8B000B0B0B000B0A8A000A098
      A000978F87005038200000000000000000000000000000000000000000000000
      000000000000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084848400008484008484
      8400008484008484840084000000FFFFFF00000000000000000000000000FFFF
      FF00840000008400000084000000840000000000000000000000000000008088
      900080D0F00060B0D00050A8D00040A0D0004098C0003090C0002088C0002030
      3000C0B8B000B0A8A00060504000000000007088900060708000606870006068
      7000506060004050600040485000303840002028300020202000B0B8B000B0A8
      A000979797005038200000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF0000000000000000000000
      00000000000000000000FFFFFF00840000000000000000848400848484000084
      8400848484000084840084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0084000000FFFFFF0084000000000000000000000000000000000000008090
      A00090D8F00070D0F00060C8F00050B8F00040B0F00030A8E0003088C0003040
      4000C0C0C000C0B8B00060584000000000008088900080D0F00060B0D00060B0
      D00050A8D00040A0D0004098C0003090C0002088C00020303000C0B8B000B0B0
      B000A6A6A6005038200000000000000000000000000000000000FFFFFF000000
      000000000000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084848400008484008484
      8400008484008484840084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00840000008400000000000000000000000000000000000000000000008098
      A000A0E0FF0080D8F00070D0F00060C8F00050B8F00040B0F0003090C0004050
      6000A09090009080700080787000000000008090A00090D8F00070D0F00070D0
      F00060C8F00050B8F00040B0F00030A8E0003088C00030404000C0C0C000C0B8
      B000B5ADB5005038200000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF000000000000000000FFFF
      FF00840000008400000084000000840000000000000000848400848484000084
      8400848484000084840084000000840000008400000084000000840000008400
      0000840000000000000000000000000000009080700070584000706050008098
      A000A0E8FF0090D8F00080D8F00070D0F00060C0F00050B8F0004098C0005060
      7000000000000000000000000000000000008098A000A0E0FF0080D8F00080D8
      F00070D0F00060C8F00050B8F00040B0F0003090C00040506000C0C8C000C0C0
      C000B5BCB5005038200000000000000000000000000000000000FFFFFF000000
      000000000000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0084000000FFFFFF0084000000000000000000000084848400008484008484
      8400008484008484840000848400848484000084840084848400008484008484
      840000848400000000000000000000000000A0888000C0B8C000B0B0B00090A0
      A000B0E8FF00A0E0F00090D8F00080D8F00070D0F00060C0F00040A0D0006070
      8000000000000000000000000000000000008098A000A0E8FF0090D8F00090D8
      F00080D8F00070D0F00060C0F00050B8F0004098C00050607000A0989000A090
      8000908070008078700000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0084000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00840000008400000000000000000000000000000000848400848484000000
      0000000000000000000000000000000000000000000000000000000000008484
      840084848400000000000000000000000000A0989000D0C8D000C0B8B00090A0
      B000C0F0FF00B0E8FF00A0E0F00090D8F00080D8F00070C8F00050A8D0006070
      80000000000000000000000000000000000090A0A000B0E8FF00A0E0F00090D8
      F00090D8F00080D8F00070D0F00060C0F00040A0D00060708000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      000000000000FFFFFF0000000000840000008400000084000000840000008400
      0000840000000000000000000000000000000000000084848400848484000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000008484
      840000848400000000000000000000000000B0A0A000D0D8D000C0C0C00090A8
      B000C0F0FF00C0F0FF00B0E8FF00A0E8FF00A0E0FF0090D8F00080D0F0006070
      80000000000000000000000000000000000090A0B000C0F0FF00B0E8FF00A0E0
      F000A0E0F00090D8F00080D8F00070C8F00050A8D00060708000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000848400848484000084
      84000000000000FFFF00000000000000000000FFFF0000000000848484000084
      840084848400000000000000000000000000B0B0B000E0E0E000D0D0D00090A8
      B00090A8B00090A0B00090A0B00090A0A0008098A0008098A0008090A0008090
      90000000000000000000000000000000000090A0B000C0F0FF00B0E8FF00B0E8
      FF00A0E0F00090D8F00080D8F00070C8F00050A8D00060708000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF000000000000000000000000000000
      000000000000000000000000000000000000C0B8B000F0F0F000E0E0E000D0D0
      D000C0C0C000C0B8C00070605000000000000000000000000000000000000000
      00000000000000000000000000000000000090A8B000C0F0FF00C0F0FF00C0F0
      FF00B0E8FF00A0E8FF00A0E0FF0090D8F00080D0F00060708000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0C0C000C0B8B000B0B0B000B0A0
      A000A0989000A088800090807000000000000000000000000000000000000000
      00000000000000000000000000000000000090A8B00090A8B00090A0B00090A0
      B00090A0B00090A0A0008098A0008098A0008090A00080909000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000084000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000000000000000000000840000000000000000000000840000008400
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000000000000000000000000000FFFF0000000000000000000000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000000000000000000000840000000000000084000000000000000000
      0000840000000000000000000000000000000000000000000000FFFFFF008484
      8400848484008484840084848400848484008484840084848400FFFFFF00FFFF
      FF0000000000000000000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000000000000000000000840000000000000084000000000000000000
      0000840000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000008400000084000000840000000000000084000000000000000000
      0000840000000000000000000000000000000000000000000000FFFFFF008484
      8400848484008484840084848400848484008484840084848400848484008484
      840084848400FFFFFF000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000008400
      0000840000008400000084000000000000000000000000000000000000000000
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840000000000000084000000840000008400
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
      FF0000000000000000000000FF000000FF000000000000000000840000008400
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000840000000000000084000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF008484
      8400848484008484840084848400848484008484840084848400848484008484
      840084848400FFFFFF000000000000000000FFFF00000000000000FFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000000000000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF0000008400FFFFFF0000008400FFFFFF0000008400FFFFFF0000008400FFFF
      FF00FFFFFF0000008400000000000000000000000000000000000000000000FF
      FF00FFFFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000008400
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      FF00000084000000FF00000084000000FF00000084000000FF00000084000000
      84000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000000000
      0000000000000000000084000000840000000000000000000000000000000000
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      FF000000FF00000084000000FF000000FF00000084000000FF000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008400000084000000840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      FF00FFFFFF000000FF00FFFFFF000000FF00FFFFFF000000FF00FFFFFF00FFFF
      FF000000FF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000424242004242
      4200424242004242420042424200424242004242420042424200424242004242
      4200424242004242420042424200000000000000000000000000525252002121
      2100212121002121210042424200424242004242420042424200212121002121
      2100212121005252520000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000042424200E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70042424200000000000000000052525200DEDEDE005252
      52005252520052525200DEDEDE00DEDEDE00DEDEDE00DEDEDE00525252005252
      520052525200DEDEDE00525252000000000000000000BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00000000004A4A420084636B00BD949C000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000042424200E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70042424200000000000000000052525200DEDEDE00DEDE
      DE00DEDEDE00DEDEDE0052525200525252005252520052525200DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00525252000000000000000000BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD0000000000426B7B003973C60084636B00BD94
      9C0000000000AD848400AD848400AD848400AD848400AD848400AD848400AD84
      8400AD848400AD848400AD848400B5847300000000000000000042424200E7E7
      E70094949400E7E7E700949494009494940094949400E7E7E700949494009494
      940094949400E7E7E70042424200000000000000000052525200525252000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005252520052525200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000052A5DE0052C6FF003973C6008463
      6B00BD949C00DEAD8400F7EFE700FFF7EF00FFF7E700FFF7E700FFF7E700FFEF
      E700FFEFE700FFF7E700FFF7DE00B5847300000000000000000042424200E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700424242000000000052525200DEDEDE00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE005252520000000000FFFFFF00BDBDBD00FFFF
      FF00BDBDBD00FFFFFF00BDBDBD00FFFFFF00BDBDBD00FFFFFF00BDBDBD00FFFF
      FF00BDBDBD00FFFFFF00BDBDBD00000000000000000052A5DE0052C6FF003973
      C60084636B00BD949C00E7DED600EFDEDE00E7DEDE00E7DED600EFEFE700F7F7
      EF00F7F7EF00F7F7F700FFF7D600B5847300000000000000000042424200E7E7
      E70094949400E7E7E700949494009494940094949400E7E7E700949494009494
      940094949400E7E7E700424242000000000052525200DEDEDE00DEDEDE00DEDE
      DE00DEDEDE0073737300DEDEDE007373730073737300DEDEDE0073737300DEDE
      DE00DEDEDE00DEDEDE00DEDEDE005252520000000000BDBDBD00FFFFFF00BDBD
      BD00FFFFFF00BDBDBD00FFFFFF00BDBDBD00FFFFFF00BDBDBD00FFFFFF00BDBD
      BD00FFFFFF000000FF00FFFFFF0000000000000000000000000052A5DE0052C6
      FF003973C60084636B00AD737B00AD737B00AD737B00AD737B00AD737B00F7EF
      E700FFF7EF00F7EFEF00FFEFD600B5847300000000000000000042424200E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700424242000000000052525200DEDEDE00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE005252520000000000FFFFFF00BDBDBD00FFFF
      FF00BDBDBD00FFFFFF00BDBDBD00FFFFFF00BDBDBD00FFFFFF00BDBDBD00FFFF
      FF00BDBDBD00FFFFFF00BDBDBD000000000000000000000000000000000052A5
      DE0052C6FF00AD737B00EFCEA500FFFFCE00FFFFE700FFFFE700DEC6BD00AD73
      7B00F7E7DE00F7F7EF00FFEFD600B5847300000000000000000042424200E7E7
      E70094949400E7E7E700949494009494940094949400E7E7E700949494009494
      940094949400E7E7E70042424200000000005252520073737300737373007373
      7300737373007373730073737300737373007373730073737300737373007373
      7300737373007373730073737300525252000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000AD737B00F7DEB500FFFFCE00FFFFD600FFFFEF00FFFFFF00FFFFFF00CEAD
      A500AD737B00F7F7EF00FFEFD600B5847300000000000000000042424200E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700424242000000000052525200DEDEDE00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE008484840084848400DEDEDE00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00525252000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000AD737B00FFFFC600FFE7AD00FFFFD600FFFFEF00FFFFF700FFFFF700EFDE
      CE00AD737B00F7F7EF00FFEFD600B5847300000000000000000042424200E7E7
      E70094949400E7E7E700949494009494940094949400E7E7E700949494009494
      940094949400E7E7E70042424200000000000000000052525200525252005252
      5200525252005252520052525200DEDEDE00DEDEDE0052525200525252005252
      5200525252005252520052525200000000000000000000000000000000000000
      0000FFFFFF0000000000000000000000000000000000FFFFFF0000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000AD737B00FFFFCE00FFDEAD00FFF7C600FFFFDE00FFFFDE00FFFFDE00F7E7
      C600AD737B00F7F7F700FFEFD600B5847300000000000000000042424200E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E7004242420000000000000000000000000042424200C6C6
      C60084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      8400C6C6C6004242420000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000AD737B00FFFFE700FFFFE700FFE7B500FFEFB500FFF7BD00FFFFD600DEB5
      9C00AD737B00F7F7EF00FFE7D600B5847300000000000000000042424200E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70042424200000000000000000000000000000000004242
      420084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      8400424242000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AD737B00F7F7FF00FFFFE700FFF7BD00FFFFCE00EFD6AD00AD73
      7B00EFDECE00E7C6BD00E7A5A500B58473000000000000000000424242009494
      9400EFD6C600EFD6C600EFD6C60094949400FFFFFF00F7F7F700FFFFFF00F7F7
      F700FFFFFF00FFFFFF0042424200000000000000000000000000000000000000
      000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DEAD8400AD737B00AD737B00AD737B00AD737B00AD737B00EFE7
      EF00EFDED600B5847300B5847300B58473000000000000000000424242009494
      940094949400949494009494940094949400FFFFFF00E7E7E700F7F7F700FFFF
      FF00FFFFFF00FFFFFF0042424200000000000000000000000000000000000000
      000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000BDBDBD00FFFFFF0000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DEAD8400EFE7EF00DEC6CE00DEC6CE00D6C6CE00EFEFEF00FFFF
      FF00E7D6DE00B5847300E7BD940000000000000000000000000042424200E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70042424200000000000000000000000000000000000000
      0000848484008484840084848400848484008484840084848400848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DEAD8400DEAD8400DEAD8400DEAD8400DEAD8400DEAD8400DEAD
      8400DEAD8400B584730000000000000000000000000000000000424242004242
      4200424242004242420042424200424242004242420042424200424242004242
      4200424242004242420042424200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000800000008000000080000000800000008000000080000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003070
      4000307040003070400030684000306840003068400020603000206030002060
      3000205030002050300020503000205030000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000080000080000000000000008000
      0000008000000080000000800000008000000080000000800000800000008000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000070A8800060A0700060A0
      7000609870006098700050987000509870005090600050906000509060004090
      5000409050003090500030905000205030000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000080000000800000800000000080
      0000008000000080000000800000008000000080000000800000008000000080
      0000800000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004078500070A88000F0F0F000F0F0
      F000E0F0E000E0F0E000E0F0E000E0F0E000E0E8E000E0E8E000E0E8E000E0E8
      E000E0E8E000E0E8E00030905000205030000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000080000000800000008000000080
      00000080000000FF000000FF000000FF000000FF000000FF0000008000000080
      0000800000000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000004078500070A88000F0F8F000F0F0
      F000F0F0F000E0F0E000E0F0E000E0F0E000E0F0E000E0E8E000E0E8E000E0E8
      E000E0E8E000E0E8E00040905000205030000000000000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000080000000800000008000000080
      000000FF0000000000000000000000000000000000000000000000FF00000080
      0000008000008000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000004080600070B08000F0F8F000F0F8
      F000F0F0F000F0F0F000F0F0F000E0F0E00080A8900030503000305030003050
      300030503000E0E8E00040905000205030000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000080000000800000008000000080
      00000000000000000000000000000000000000000000000000000000000000FF
      0000008000008000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005088600080B89000F0F8F00050A8
      6000305030003050300030503000305030002078200070B87000509060005098
      500020603000E0E8E00050906000206030000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000080000000800000008000000080
      00000080000000000000000000000000000000000000000000000000000000FF
      000080000000800000000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000005090600080C09000F0F8F000F0F8
      F00050A8600060B0700050A860002078200080C0800050906000509850002060
      300080A89000E0E8E00050906000206030000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000FFFFFF000000000000000000FFFFFF000000
      0000000000000000000000000000000000005098700090B89000F0F8F000F0F8
      F000F0F8F00050A860003088300080C090005090600050985000206030006098
      600060986000E0F0E00050906000206030000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000800000008000
      000080000000800000008000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000FFFFFF000000000000000000FFFFFF000000
      0000000000000000000000000000000000006098700090C8A000FFF8FF00F0F8
      F000F0F8F0003088300090C8A00060A8600050A860003070400000680000E0F0
      E000E0F0E000E0F0E00050987000306840000000000000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000080000000800000000000
      0000000000000000000000000000000000000000000000FF0000008000000080
      0000008000000080000080000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000060A0700090C8A000FFFFFF00FFF8
      FF0040904000A0D0A00070B8700060A860004088500050A86000308830000068
      0000E0F0E000E0F0E00050987000306840000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000800000800000000000
      000000000000000000000000000000000000000000000000000000FF00000080
      0000008000000080000080000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000FFFFFF000000
      00000000000000000000000000000000000070A88000A0D0A000FFFFFF005098
      5000B0D8B00070B8800070B88000509060005080600060B0700050A860003088
      300000680000E0F0E00060987000307040000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF0000008000008000
      0000000000000000000000000000000000000000000080000000800000000080
      0000008000000080000080000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000070A88000A0D0A000FFFFFF0060B0
      700060B0700060B0700050986000F0F8F000F0F8F00050806000508060005080
      600050806000F0F0F00060987000307040000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF0000008000000080
      0000800000008000000080000000800000008000000000800000008000000080
      0000008000000080000080000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000070B08000B0D8B000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFF8FF00FFF8FF00F0F8F000F0F8F000F0F8F000F0F8
      F000F0F8F000F0F0F00060A07000000000000000000000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008000000080
      0000008000000080000000800000008000000080000000800000008000000080
      000000FF00000080000080000000000000000000000000000000000000000000
      000000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF000000000000000000000000000000000080B89000B0D8B000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFF8FF00FFF8FF00F0F8F000F0F8F000F0F8
      F000F0F8F000F0F8F00060A07000000000000000000000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FF000000FF
      000000800000008000000080000000800000008000000080000000FF000000FF
      00000000000000FF000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000080B89000B0D8B000B0D8B000A0D0
      A000A0D0A00090C8A00090C8A00090C8A00090B8900080C0900080B8900070B0
      800070A8800070A8800070A88000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF000000FF000000FF000000FF000000FF000000FF0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000080C0900080B8900070B0800070A8
      800070A8800060A0700060987000509870005090600050886000508860004080
      6000407850004078500000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000033
      3300003333000033330000333300003333000033330000333300003333000033
      3300003333000033330000333300003333000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000033
      3300003333000033330000333300003333000033330000333300003333000033
      3300003333000033330000333300003333000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000009999000099
      9900009999000099990000999900009999000099990000999900009999000099
      9900009999000099990000333300003333000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000084840000000000000000000000000000000000009999000099
      9900009999000099990000999900009999000099990000999900009999000099
      9900009999000099990000333300003333000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000000
      000000000000000000000000000000000000000000000000000000999900F8F8
      F80099CCFF0099FFFF0099CCFF0099FFFF0099CCFF0099CCFF0099CCFF0099CC
      FF0066CCCC000099990000333300003333000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF0000000000008484000000000000000000000000000000000000999900F8F8
      F80099CCFF0099FFFF0099CCFF0099FFFF0099CCFF0099CCFF0099CCFF0099CC
      FF0066CCCC000099990000333300003333000000000000FFFF00000000000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400000000000000000000000000000000000000000000999900F8F8F80099FF
      FF0099FFFF0099CCFF0099FFFF0099CCFF0099FFFF0099CCFF0099CCFF0099CC
      FF0066CCCC000033330000999900003333000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000084840000000000000000000000000000999900F8F8F80099FF
      FF0099FFFF0099CCFF0099FFFF0099CCFF0099FFFF0099CCFF0099CCFF0099CC
      FF0066CCCC0000333300009999000033330000000000FFFFFF0000FFFF000000
      0000008484000084840000848400008484000084840000848400008484000084
      8400008484000000000000000000000000000000000000999900F8F8F80099FF
      FF0099FFFF0099FFFF0099FFFF0099FFFF0099CCFF0099FFFF0099CCFF0099CC
      FF00009999000033330000999900003333000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000999900F8F8F80099FF
      FF0099FFFF0099FFFF0099FFFF0099FFFF0099CCFF0099FFFF0099CCFF0099CC
      FF00009999000033330000999900003333000000000000FFFF00FFFFFF0000FF
      FF00000000000084840000848400008484000084840000848400008484000084
      84000084840000848400000000000000000000999900F8F8F80099FFFF0099FF
      FF0099FFFF0099FFFF0099CCFF0099FFFF0099FFFF0099CCFF0099FFFF0066CC
      CC0000333300FF99990000000000003333000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000084
      84000084840000848400000000000000000000999900F8F8F80099FFFF0099FF
      FF0099FFFF0099FFFF0099CCFF0099FFFF0099FFFF0099CCFF0099FFFF0066CC
      CC000033330066CCCC0066CCCC000033330000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000999900F8F8F80099FFFF0099FF
      FF0099FFFF0099FFFF0099FFFF0099FFFF0099CCFF0099FFFF0099CCFF0066CC
      CC0000333300FFCCCC00FFCCCC00000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      00000084840000848400000000000000000000999900F8F8F80099FFFF0099FF
      FF0099FFFF0099FFFF0099FFFF0099FFFF0099CCFF0099FFFF0099CCFF0066CC
      CC00003333000000000000000000000000000000000000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00000000000000
      0000000000000000000000000000000000000099990000999900009999000099
      9900009999000099990000999900009999000099990000999900009999000099
      9900FF999900FFFFCC00FFCCCC00000000000000000000000000008484000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000084840000000000000000000099990000999900009999000099
      9900009999000099990000999900009999000099990000999900009999000099
      9900FF999900FFFFFF00000000000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF00000000000000
      000000000000000000000000000000000000000000000080800099666600FFFF
      CC00FFCCCC0099003300FF999900FFCCCC0099003300FF999900FFCCCC00FFCC
      CC00FFCCCC00FFCCCC0000000000003333000000000000000000008484000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000008484000000000000000000000000000080800066CCCC0066CC
      CC0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000003333000000000000FFFF00FFFFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000080800066CCCC009966
      6600FFFFCC00FFFFFF00CC333300FFFF9900FFFFFF00CC333300FFFFFF00FFFF
      CC00FFCCCC000000000066CCCC00003333000000000000000000008484000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000008484000000000000000000000000000080800066CCCC0066CC
      CC0000000000FFFFFF00000000000000000000000000FFFFFF00000000000000
      FF00FF000000FFFFFF0000000000003333000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000080800066CCCC0066CC
      CC0099666600FFFFCC00FFFFFF00FF660000FFFF9900FFFFFF00FFFFCC00FFCC
      CC00000000000099990000999900000000000000000000000000008484000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000008484000000000000000000000000000080800066CCCC0066CC
      CC0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      FF00FF000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000080800066CC
      CC0066CCCC0099666600FFFFCC00FFFFFF00FF993300FFFFCC00FFCCCC000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000000000000000000080800066CC
      CC0000000000FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FF000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000080
      8000008080000080800099666600FFFFCC00FFFFFF00FFCCCC00000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000080
      800000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000099666600FFFFCC0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009966660000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF000000
      00007F7F7F007F7F7F007F7F7F0000FFFF0000FFFF007F7F7F007F7F7F007F7F
      7F007F7F7F0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF000000000000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000007F7F7F0000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000000000FFFF0000000000000000000000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000007F7F7F0000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000000000FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000007F7F7F0000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF00000000000000000000000000FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFFFF000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000007F7F7F0000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF00000000000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF00000000000000000000000000000000000000000000FF
      FF0000000000FFFFFF00FFFFFF000000000000FFFF0000FFFF0000FFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF000000000000FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000FFFF0000FFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFF00000000000000FFFF00FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF00FFFFFF0000FFFF00000000000000000000FFFF0000000000FFFFFF00FFFF
      FF000000000000000000FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000008000000080000000800000008000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000008000000080000000800000008000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000000000000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000008000000080000000800000008000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000008000000080000000800000008000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000008000000080000000800000008000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000008000000080000000800000008000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000FFFFFF00FFFFFF000000000000000000FFFF
      FF0000000000FFFFFF00FFFFFF0000000000000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000008000000080000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000008000000080000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000FFFF0000FFFF000000
      000000000000000000000000000000FFFF0000FFFF0000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000008000000080000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000008000000080000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000FFFF0000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000800000008000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008000
      0000008000000080000080000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000080000000
      80008080800000000000000000000000000000000000000000000000FF008080
      8000000000000000000000000000000000000000000000000000C0C0C0008080
      8000C0C0C000FFFFFF0000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000800000000080
      0000008000000080000000800000800000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000080000000
      800000008000808080000000000000000000000000000000FF00000080000000
      8000808080000000000000000000000000000000000000000000808080000000
      000080808000FFFFFF0000000000000000000000000000000000000000000000
      000080808000C0C0C00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080000000008000000080
      0000008000000080000000800000008000008000000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000080000000
      8000000080000000800080808000000000000000FF0000008000000080000000
      8000000080008080800000000000000000000000000000000000C0C0C0000000
      00000000000080808000FFFFFF0000000000000000000000000000000000FFFF
      FF0000800000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000008000000000800000008000000080
      000000FF00000080000000800000008000000080000080000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      8000000080000000800000008000808080000000800000008000000080000000
      8000000080008080800000000000000000000000000000000000FFFFFF008080
      80000000000080808000C0C0C0000000000000000000FFFFFF00FFFFFF008080
      800080808000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000800000008000000080000000FF
      00000000000000FF000000800000008000000080000080000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000080000000800000008000000080000000800000008000000080000000
      800080808000000000000000000000000000000000000000000000000000FFFF
      FF00808080000000000080808000FFFFFF00FFFFFF00FFFFFF00808080000000
      0000FFFFFF00000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000FF00000080000000FF00000000
      0000000000000000000000FF0000008000000080000000800000800000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000800000008000000080000000800000008000000080008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00808080000000000080808000C0C0C0008080800000000000C0C0
      C00000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000FF0000000000000000
      000000000000000000000000000000FF00000080000000800000008000008000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000800000008000000080000000800000008000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0080808000000000000000000000000000808080000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000800000008000000080
      0000800000000000000000000000000000000000000000000000000000000000
      0000000000000000FF0000008000000080000000800000008000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C0C0C000000000000000000080808000FFFFFF000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF0000008000000080
      0000008000008000000000000000000000000000000000000000000000000000
      00000000FF000000800000008000000080000000800000008000808080000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C0C0C0008080800000000000000000000000000080808000FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FF00000080
      0000008000000080000080000000000000000000000000000000000000000000
      FF00000080000000800000008000808080000000800000008000000080008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000C0C0C000000000000000000080808000C0C0C00080808000000000008080
      8000FFFFFF00000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      00000080000000800000008000008000000000000000000000000000FF000000
      8000000080000000800080808000000000000000FF0000008000000080000000
      80008080800000000000000000000000000000000000FFFFFF00C0C0C0008080
      8000000000000000000080808000FFFFFF0000000000FFFFFF00808080000000
      000080808000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF000000800000008000008000000000000000000000000000FF000000
      800000008000808080000000000000000000000000000000FF00000080000000
      80000000800080808000000000000000000000000000C0C0C000808080000000
      000000000000C0C0C00000000000000000000000000000000000FFFFFF00C0C0
      C00080808000808080000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF000000800000008000000000000000000000000000000000
      FF000000800000000000000000000000000000000000000000000000FF000000
      80000000800000008000000000000000000000000000C0C0C000000000008080
      8000C0C0C0000000000000000000000000000000000000000000000000000000
      0000FFFFFF00C0C0C0000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000080000000FF00000000000000000000000000C0C0C00080808000C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000E00000000100010000000000000700000000000000000000
      000000000000000000000000FFFFFF00F800FC1F00000000F000F80F00000000
      E000E00700000000C000C0030000000080000001000000008000000000000000
      8000000000000000800000000000000080008000000000008000C00000000000
      8001E000000000008307F0010000000083C7F80700000000C7FFFC1F00000000
      EFFFFE7F00000000FFFFFFFF00000000FE00FFFFFFFFF807FE00FFFFC0FFF001
      FE00F81F807FE0018000F00F8007E0018000E0078003E0018000E003C003E001
      8000C003E00380008000C003C00700008000C003800F00008000C00380078000
      8000C0038007C0008001E007C007E0008001F00FE007E0008001F81FFE0FE001
      FFFFFFFFFF1FF005FFFFFFFFFFFFF863FFFFFFFFFFFFFFFFFFFFE001F801F801
      80004001F801F80180004001E001E00180004001E001E0018000400180018001
      8000400180018001800040018001800180004001800180018000400180018001
      80004001800180018000400180078007800040018007800780006003801F801F
      FFFFFE3F801F801FFFFFFFFFFFFFFFFFFFFFFFFF8001FFFF8000FFFF80010000
      8000000080010000800000008001000080000000800100008000000080010000
      8000000080010000800000008001000080000000800100008000000080010000
      8000000080010000800000008001000080000000800100008000000080010000
      FFFF000080015555FFFFFFFFFFFFFFFFFFFFFFFF800180010001000180018001
      0001000180018001000100018001800100010001800180010001000180018001
      0001000180018001000100018001800100010001800180010001000180018001
      0001000180018001000100018001800100010001800180010001000180018001
      FFFFFFFF80038003FFFFFFFFFFFFFFFF80018001800180010000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      8001800180018001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0180038003
      FC03FF0100010001FC03FF0100010001FC03E001000100010003E00100010001
      0003E001000100010003E001000100010003000F000100010003000F00010001
      003F000F00010001003F000F00010001003F000F00010001003F01FF80038003
      003F01FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00FF01FFFF
      FE008000FF01FC03FE000000FF01FC03FE000000E001FC0380000000E0010003
      80000001E001000380000003E001000380000003000F000380010003000F0003
      80030003000F003F80070003000F003F807F0003000F003F80FF800701FF003F
      81FFF87F01FF003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80FFFFF8FFFFF9FF
      803F20F8FFFFF6CF800F007FFFFFF6B78003007CFFFFF6B78001003CFFF7F8B7
      8001000FC1F7FE8F80010004C3FBFE3F8001000CC7FBFF7F800101FFCBFBFE3F
      8001E3FCDCF7FEBF8001FFFCFF0FFC9F8001FFFFFFFFFDDF8001FFF8FFFFFDDF
      8001FFF8FFFFFDDFFFFFFFFFFFFFFFFFC001C0038001FFFFC001800100001FFF
      C001800100000800C001800100000000C001000000008000C00100000000C000
      C00100000000E000C00100000000F000C0010000E007F000C0018001E007F000
      C001C003E007F000C001E007E007F800C001F00FE00FF800C001F00FE01FF801
      C001F00FE03FF803C001FFFFE07FFFFFBFFFF03FFFFFE000BFFF200FFFFF8000
      B049000783E00000807F000783E00000B07F07C383E00000B9FF0FE380800000
      BFFF07E3C0000000B04903FF80000000807FFFC180000000B07F1F81C0010000
      B9FF1FC1E0830000BFFF8F81E0830000048F8001F1C7000107FFC001F1C70001
      07FFC00BF1C700019FFFF03FFFFF0003FFFFFFFFFFFFFFFFFFFFE000C001E000
      001FC0008001C000000FC0008001C00000078000800180000003800080018000
      00010000800100000000000080010000001F000080010000001F800080018000
      001F8000800180008FF1800180018001FFF9C00F8001C001FF75E01F8001E001
      FF8FFE3F8001F001FFFFFF7FFFFFF001FF7EFFFFFFFFFC009001DC0FDFC0FC00
      C003DC0FDFC02000E003DC0FDFC00000E003C00FDF000000E003DC0FDF400000
      E003DC1FDF4100000001DC3FDF4300008000DFFFDF7F0000E007DC0FDC0F0000
      E00FDC0FDC0FE000E00FDC0FDC0FF800E027DC0FDC0FF000C073C00FC00FE001
      9E79DC1FDC1FC4037EFEDC3FDC3FEC07F3FFFFFFFFFFFFFFE1FF87CFC3FBFFFF
      C0FF8387C3F3C007807F8103C1E3C007003FC003C187C007083FE007E007C007
      1C1FF00FF00FC007BE0FF81FF81FC007FF07F81FFC1FC007FF83F01FF80FC007
      FFC1E00FF007C007FFE0C1078083C007FFF0C38383C3C00FFFF8E7C387F3C01F
      FFFDFFE38FFFC03FFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
end
