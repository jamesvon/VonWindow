unit UCertTools;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls,
  Vcl.ValEdit, Vcl.Grids, Vcl.Samples.Spin, Vcl.Imaging.jpeg, UVonConfig,
  Vcl.CheckLst, System.ImageList, Vcl.ImgList;

type
  TFVonTools = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    Panel2: TPanel;
    Label1: TLabel;
    ECertCipherName: TComboBox;
    Label3: TLabel;
    ECertHashName: TComboBox;
    Label2: TLabel;
    ECertCipherKey: TEdit;
    btnCertRead: TButton;
    btnCertPublish: TButton;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    ScrollBox1: TScrollBox;
    imgBackground: TImage;
    lbTitle2: TLabel;
    lbTitle1: TLabel;
    lbPrompt: TLabel;
    lb1: TLabel;
    lb2: TLabel;
    imgLogin: TImage;
    imgCancel: TImage;
    imgOK: TImage;
    Edit1: TEdit;
    Edit2: TEdit;
    lbRemember: TLabel;
    chkRemember: TCheckBox;
    btnLoginLoad: TButton;
    btnLoginPublished: TButton;
    GroupBox1: TGroupBox;
    chkLoginHide: TCheckBox;
    GridPanel1: TGridPanel;
    Label5: TLabel;
    ELoginItemX: TSpinEdit;
    Label6: TLabel;
    ELoginItemY: TSpinEdit;
    Label7: TLabel;
    ELoginItemW: TSpinEdit;
    Label8: TLabel;
    ELoginItemH: TSpinEdit;
    Label9: TLabel;
    ELoginItemFontName: TEdit;
    Label10: TLabel;
    ELoginItemFontSize: TSpinEdit;
    Label11: TLabel;
    ELoginItemFontColor: TColorBox;
    ELoginComName: TComboBox;
    GroupBox3: TGroupBox;
    Label12: TLabel;
    ESysFontName: TComboBox;
    Label25: TLabel;
    ESysFontSize: TSpinEdit;
    Label13: TLabel;
    ESysFontColor: TColorBox;
    Label20: TLabel;
    Panel6: TPanel;
    Bevel2: TBevel;
    ESysMarginTop: TSpinEdit;
    ESysMarginLeft: TSpinEdit;
    ESysMarginRight: TSpinEdit;
    ESysMarginBottom: TSpinEdit;
    btnLoginOkBtn: TButton;
    btnLoginLoginBtn: TButton;
    btnLoginCancelBtn: TButton;
    btnLoginBackground: TButton;
    Label14: TLabel;
    GroupBox5: TGroupBox;
    lstAppCert: TValueListEditor;
    Panel4: TPanel;
    lstAppAttachment: TListView;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    EAppCertAttaName: TEdit;
    btnAppCertAttaAdd: TButton;
    btnAppCertAttaDel: TButton;
    GroupBox6: TGroupBox;
    lstSysCert: TValueListEditor;
    Panel3: TPanel;
    lstSysAttachment: TListView;
    GroupBox7: TGroupBox;
    Label16: TLabel;
    ESysCertAttaName: TEdit;
    btnSysCertAttaAdd: TButton;
    btnSysCertAttaDel: TButton;
    Splitter1: TSplitter;
    btnSysCertAttaRename: TButton;
    btnAppCertAttaRename: TButton;
    ImgIcon: TImageList;
    imgBtn: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure btnCertPublishClick(Sender: TObject);
    procedure btnCertReadClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lbTitle1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ELoginItemXChange(Sender: TObject);
    procedure ELoginItemYChange(Sender: TObject);
    procedure ELoginItemWChange(Sender: TObject);
    procedure ELoginItemHChange(Sender: TObject);
    procedure ELoginItemFontSizeChange(Sender: TObject);
    procedure ELoginItemFontNameExit(Sender: TObject);
    procedure ELoginItemFontColorChange(Sender: TObject);
    procedure btnLoginLoadClick(Sender: TObject);
    procedure btnLoginOkBtnClick(Sender: TObject);
    procedure btnLoginLoginBtnClick(Sender: TObject);
    procedure btnLoginCancelBtnClick(Sender: TObject);
    procedure btnLoginBackgroundClick(Sender: TObject);
    procedure btnLoginPublishedClick(Sender: TObject);
    procedure lbTitle1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure chkLoginHideClick(Sender: TObject);
    procedure btnSysCertAttaAddClick(Sender: TObject);
    procedure btnSysCertAttaDelClick(Sender: TObject);
    procedure lstSysAttachmentClick(Sender: TObject);
    procedure btnAppCertAttaAddClick(Sender: TObject);
    procedure btnAppCertAttaDelClick(Sender: TObject);
    procedure lstAppAttachmentClick(Sender: TObject);
    procedure btnSysCertAttaRenameClick(Sender: TObject);
    procedure btnAppCertAttaRenameClick(Sender: TObject);
  private
    { Private declarations }
    FCipherName, FHashName, FCertKey, FCertFilename: string;
    FStyleFile: TVonSectionGroup;
    FLoginSetting: TVonSection;
    FStyleSetting: TVonSection;
    FCertFile: TVonSectionGroup;
    FCertSystem: TVonSection;
    FCertApplication: TVonSection;
    FCurrentMover: TObject;
    procedure WriteImageData(Key: string; img: TImage);
  public
    { Public declarations }
  end;

var
  FVonTools: TFVonTools;

implementation

uses IniFiles, DCPBase64, UVonCrypt;

type TVonControl = class(TControl) published property Font; end;

{$R *.dfm}

procedure TFVonTools.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  // ======= Certification =======
  FCertFile:= TVonSectionGroup.Create;
  FCertSystem:= TVonSection.Create;
  FCertSystem.SectionName:= 'SYSTEM';
  FCertFile.AddSection(FCertSystem);
  FCertApplication:= TVonSection.Create;
  FCertApplication.SectionName:= 'APP';
  FCertFile.AddSection(FCertApplication);
  // ======= Form style =======
  FStyleFile:= TVonSectionGroup.Create;
  FLoginSetting:= TVonSection.Create;
  FLoginSetting.SectionName:= 'Login';
  FStyleFile.AddSection(FLoginSetting);
  FStyleSetting:= TVonSection.Create;
  FStyleSetting.SectionName:= 'Style';
  FStyleFile.AddSection(FStyleSetting);
  // ======= Cipher and hash =======
  GetCipherList(ECertCipherName.Items);
  ECertCipherName.ItemIndex:= 0;
  GetHashList(ECertHashName.Items);
  ECertHashName.ItemIndex:= 0;
  // ======= Load controls to combobox =======
  for I := 0 to ScrollBox1.ControlCount - 1 do
    ELoginComName.Items.Add(ScrollBox1.Controls[I].Name);
end;

procedure TFVonTools.FormDestroy(Sender: TObject);
begin
  FStyleSetting.Free;
  FCertFile.Free;
end;

{$region 'Certification'}
procedure TFVonTools.btnCertReadClick(Sender: TObject);
var
  sIntput: TFileStream;
  szStream: TMemoryStream;
  I: Integer;
begin
  with OpenDialog1 do
    if Execute then begin
      // Load certification file and decrypt
      with GetCipher(ECertCipherName.Text) do try
        InitStr(ECertCipherKey.Text, GetHashClass(ECertHashName.Text));
        sIntput := TFileStream.Create(Filename, fmOpenRead);
        szStream:= TMemoryStream.Create;
        DecryptStream(sIntput, szStream, sIntput.Size);
        szStream.Position:= 0;
        FCertFile.LoadFromStream(szStream);
        FCertSystem:= FCertFile.Sections['SYSTEM'];
        FCertApplication:= FCertFile.Sections['APP'];
        szStream.Free;
        sIntput.Free;
      finally
        Free;
      end;
      // Load system settings from certification file
      lstSysCert.Strings.Clear;
      lstSysAttachment.Clear;
      for I := 0 to FCertSystem.ValueCount - 1 do
        with FCertSystem.ValueByIndex[I] do
          case ValueType of
          vst_str, vst_int, vst_float, vst_datetime, vst_bool, vst_Guid:
            lstSysCert.InsertRow(Name, Text, true);
          vst_bytes: with lstSysAttachment.Items.Add do begin
              Caption:= Name;
              ImageIndex:= 0;
            end;
          end;
      // Load application settings from certification file
      lstAppCert.Strings.Clear;
      lstAppAttachment.Clear;
      for I := 0 to FCertApplication.ValueCount - 1 do
        with FCertApplication.ValueByIndex[I] do
          case ValueType of
          vst_str, vst_int, vst_float, vst_datetime, vst_bool, vst_Guid:
            lstAppCert.InsertRow(Name, Text, true);
          vst_bytes: with lstAppAttachment.Items.Add do begin
              Caption:= Name;
              ImageIndex:= 1;
            end;
          end;
    end;
end;

procedure TFVonTools.btnCertPublishClick(Sender: TObject);
var
  sOutput: TFileStream;
  szStream: TMemoryStream;
  I: Integer;
begin
  with SaveDialog1 do
    if Execute then begin //Save certification to file
      for I := 1 to lstSysCert.RowCount - 1 do
        FCertSystem.SetString(lstSysCert.Cells[0, I], lstSysCert.Cells[1, I]);
      for I := 1 to lstAppCert.RowCount - 1 do
        FCertApplication.SetString(lstAppCert.Cells[0, I], lstAppCert.Cells[1, I]);
      with GetCipher(ECertCipherName.Text) do try
        InitStr(ECertCipherKey.Text, GetHashClass(ECertHashName.Text));
        szStream:= TMemoryStream.Create;
        FCertFile.SaveToStream(szStream);
        szStream.Position:= 0;
        sOutput := TFileStream.Create(Filename, fmCreate);
        EncryptStream(szStream, sOutput, szStream.Size);
        szStream.Free;
        sOutput.Free;
      finally
        Free;
      end;

    end;
end;
{$endregion}

{$region 'System certification'}
procedure TFVonTools.btnSysCertAttaAddClick(Sender: TObject);
var
  fs: TFileStream;
  item: TListItem;
  b: TBytes;
begin
  with OpenDialog1 do
    if Execute then begin
      fs:= TFileStream.Create(Filename, fmOpenRead);
      SetLength(b, fs.Size);
      fs.Read(b, fs.Size);
      FCertSystem.SetBinary(ESysCertAttaName.Text, b);
      fs.Free;
      item:= lstSysAttachment.FindCaption(0, ESysCertAttaName.Text,True,True,True);
      if not Assigned(item) then
        with lstSysAttachment.Items.Add do begin
          Caption:= ESysCertAttaName.Text;
          ImageIndex:= 1;
        end;
    end;
end;

procedure TFVonTools.btnAppCertAttaAddClick(Sender: TObject);
var
  fs: TFileStream;
  item: TListItem;
  b: TBytes;
begin
  with OpenDialog1 do
    if Execute then begin
      fs:= TFileStream.Create(Filename, fmOpenRead);
      SetLength(b, fs.Size);
      fs.Read(b, fs.Size);
      FCertApplication.SetBinary(EAppCertAttaName.Text, b);
      fs.Free;
      item:= lstAppAttachment.FindCaption(0, EAppCertAttaName.Text,True,True,True);
      if not Assigned(item) then
        with lstAppAttachment.Items.Add do begin
          Caption:= EAppCertAttaName.Text;
          ImageIndex:= 0;
        end;
    end;
end;

procedure TFVonTools.btnSysCertAttaDelClick(Sender: TObject);
begin
  if not Assigned(lstSysAttachment.Selected) then Exit;
  if lstSysAttachment.Selected.Caption <> ESysCertAttaName.Text then
    raise Exception.Create('尚未选择系统参数附件');
  FCertSystem.Delete(ESysCertAttaName.Text);
  lstSysAttachment.DeleteSelected;
end;

procedure TFVonTools.btnSysCertAttaRenameClick(Sender: TObject);
begin
  if not Assigned(lstSysAttachment.Selected) then Exit;
  FCertSystem.Values[lstSysAttachment.Selected.Caption].Name:= ESysCertAttaName.Text;
  lstSysAttachment.Selected.Caption:= ESysCertAttaName.Text;
end;

procedure TFVonTools.btnAppCertAttaDelClick(Sender: TObject);
begin
  if not Assigned(lstAppAttachment.Selected) then Exit;
  if lstAppAttachment.Selected.Caption <> EAppCertAttaName.Text then
    raise Exception.Create('尚未选择系统参数附件');
  FCertApplication.Delete(EAppCertAttaName.Text);
  lstAppAttachment.DeleteSelected;
end;

procedure TFVonTools.btnAppCertAttaRenameClick(Sender: TObject);
begin
  if not Assigned(lstAppAttachment.Selected) then Exit;
  FCertApplication.Values[lstAppAttachment.Selected.Caption].Name:= EAppCertAttaName.Text;
  lstAppAttachment.Selected.Caption:= EAppCertAttaName.Text;
end;

procedure TFVonTools.lstAppAttachmentClick(Sender: TObject);
begin
  if not Assigned(lstAppAttachment.Selected) then Exit;
  EAppCertAttaName.Text:= lstAppAttachment.Selected.Caption;
end;

procedure TFVonTools.lstSysAttachmentClick(Sender: TObject);
begin
  if not Assigned(lstSysAttachment.Selected) then Exit;
  ESysCertAttaName.Text:= lstSysAttachment.Selected.Caption;
end;
{$endregion}
{$region 'Face designer'}

procedure TFVonTools.chkLoginHideClick(Sender: TObject);
begin
  FLoginSetting.SetBool(ELoginComName.Text + '.Hide', chkLoginHide.Checked);
end;

procedure TFVonTools.WriteImageData(Key: string; img: TImage);
var
  ms: TMemoryStream;
begin
  ms:= TMemoryStream.Create;
  img.Picture.SaveToStream(ms);
  ms.Position:= 0;
  FLoginSetting.SetStream(Key, ms);
  ms.Free;
end;

procedure TFVonTools.btnLoginLoginBtnClick(Sender: TObject);
begin
  with OpenDialog1 do
    if Execute then begin
      imgLogin.Picture.LoadFromFile(Filename);
      WriteImageData('imgLogin', imgLogin);
    end;
end;

procedure TFVonTools.btnLoginOkBtnClick(Sender: TObject);
begin
  with OpenDialog1 do
    if Execute then begin
      imgOK.Picture.LoadFromFile(Filename);
      WriteImageData('imgOK', imgOK);
    end;
end;

procedure TFVonTools.btnLoginPublishedClick(Sender: TObject);
begin
  with SaveDialog1 do
    if Execute then
      FStyleFile.SaveToFile(Filename);
end;

procedure TFVonTools.btnLoginBackgroundClick(Sender: TObject);
var
  ms: TMemoryStream;
begin
  with OpenDialog1 do
    if Execute then begin
      imgBackground.Picture.LoadFromFile(Filename);
      ms:= TMemoryStream.Create;
      imgBackground.Picture.SaveToStream(ms);
      ms.Position:= 0;
      FLoginSetting.SetStream('imgBackground', ms);
      ms.Free;
    end;
end;

procedure TFVonTools.btnLoginCancelBtnClick(Sender: TObject);
begin
  with OpenDialog1 do
    if Execute then begin
      imgCancel.Picture.LoadFromFile(Filename);
      WriteImageData('imgCancel', imgCancel);
    end;
end;

procedure TFVonTools.btnLoginLoadClick(Sender: TObject);
var
  I: Integer;
  ms: TMemoryStream;
begin
  with OpenDialog1 do
    if Execute then begin
      FStyleFile.LoadFromFile(Filename);
      FLoginSetting:= FStyleFile.Sections['Login'];
      if not Assigned(FLoginSetting) then begin
        FLoginSetting:= TVonSection.Create;
        FLoginSetting.SectionName:= 'Login';
        FStyleFile.AddSection(FLoginSetting);
      end;
      for I := 0 to ScrollBox1.ControlCount - 1 do
        begin
          with TControl(ScrollBox1.Controls[I])do begin
            Top:= FLoginSetting.GetInteger(ScrollBox1.Controls[I].Name + '.Top');
            Left:= FLoginSetting.GetInteger(ScrollBox1.Controls[I].Name + '.Left');
            Width:= FLoginSetting.GetInteger(ScrollBox1.Controls[I].Name + '.Width');
            Height:= FLoginSetting.GetInteger(ScrollBox1.Controls[I].Name + '.Height');
          end;
          if ScrollBox1.Controls[I] is TImage then begin
            ms:= TMemoryStream.Create;
            FLoginSetting.GetStream(ScrollBox1.Controls[I].Name, ms);
            ms.Position:= 0;
            TImage(ScrollBox1.Controls[I]).Picture.LoadFromStream(ms);
            ms.Free;
          end else with TVonControl(ScrollBox1.Controls[I])do begin
            Font.Size:= FLoginSetting.GetInteger(ScrollBox1.Controls[I].Name + '.Font.Size');
            Font.Color:= TColor(FLoginSetting.GetInteger(ScrollBox1.Controls[I].Name + '.Font.Color'));
            Font.Name:= FLoginSetting.GetString(ScrollBox1.Controls[I].Name + '.Font.Name');
          end;
        end;
      FStyleSetting:= FStyleFile.Sections['Style'];
      if not Assigned(FStyleSetting) then begin
        FStyleSetting:= TVonSection.Create;
        FStyleSetting.SectionName:= 'Style';
        FStyleFile.AddSection(FStyleSetting);
      end;
    end;
end;

procedure TFVonTools.ELoginItemFontColorChange(Sender: TObject);
begin
  if(not Assigned(FCurrentMover))or(Sender is TImage)then Exit
  else begin
    TVonControl(FCurrentMover).Font.Color:= ELoginItemFontColor.Selected;
    FLoginSetting.SetInteger(ELoginComName.Text + '.Font.Color', ELoginItemFontColor.Selected);
  end;
end;

procedure TFVonTools.ELoginItemFontNameExit(Sender: TObject);
begin
  if(not Assigned(FCurrentMover))or(Sender is TImage)then Exit
  else begin
    TVonControl(FCurrentMover).Font.Name:= ELoginItemFontName.Text;
    FLoginSetting.SetString(ELoginComName.Text + '.Font.Name', ELoginItemFontName.Text);
  end;
end;

procedure TFVonTools.ELoginItemFontSizeChange(Sender: TObject);
begin
  if(not Assigned(FCurrentMover))or(Sender is TImage)then Exit
  else begin
    TVonControl(FCurrentMover).Font.Size:= ELoginItemFontSize.Value;
    FLoginSetting.SetInteger(ELoginComName.Text + '.Font.Size', ELoginItemFontSize.Value);
  end;
end;

procedure TFVonTools.ELoginItemHChange(Sender: TObject);
begin
  (FCurrentMover as TControl).Height:= ELoginItemH.Value;
  FLoginSetting.SetInteger(ELoginComName.Text + '.Height', ELoginItemH.Value);
end;

procedure TFVonTools.ELoginItemWChange(Sender: TObject);
begin
  (FCurrentMover as TControl).Width:= ELoginItemW.Value;
  FLoginSetting.SetInteger(ELoginComName.Text + '.Width', ELoginItemW.Value);
end;

procedure TFVonTools.ELoginItemXChange(Sender: TObject);
begin
  (FCurrentMover as TControl).Left:= ELoginItemX.Value;
  FLoginSetting.SetInteger(ELoginComName.Text + '.Left', ELoginItemX.Value);
end;

procedure TFVonTools.ELoginItemYChange(Sender: TObject);
begin
  (FCurrentMover as TControl).Top:= ELoginItemY.Value;
  FLoginSetting.SetInteger(ELoginComName.Text + '.Top', ELoginItemY.Value);
end;

procedure TFVonTools.lbTitle1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if FCurrentMover = Sender then Exit;
  FCurrentMover:= Sender;
  ELoginComName.ItemIndex:= ELoginComName.Items.IndexOf(TControl(FCurrentMover).Name);
  chkLoginHide.Checked:= FLoginSetting.GetBool(ELoginComName.Text + '.Hide');
  ELoginItemX.Value:= TControl(FCurrentMover).Left;
  ELoginItemY.Value:= TControl(FCurrentMover).Top;
  ELoginItemW.Value:= TControl(FCurrentMover).Width;
  ELoginItemH.Value:= TControl(FCurrentMover).Height;
  if Sender is TImage then begin
    ELoginItemFontName.Enabled:= False;
    ELoginItemFontSize.Enabled:= False;
    ELoginItemFontColor.Enabled:= False;
  end else begin
    ELoginItemFontName.Text:= TVonControl(FCurrentMover).Font.Name;
    ELoginItemFontSize.Value:= TVonControl(FCurrentMover).Font.Size;
    ELoginItemFontColor.Selected:= TVonControl(FCurrentMover).Font.Color;
    ELoginItemFontName.Enabled:= true;
    ELoginItemFontSize.Enabled:= true;
    ELoginItemFontColor.Enabled:= true;
  end;
end;

procedure TFVonTools.lbTitle1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if FCurrentMover <> Sender then Exit;
  if ssLeft in Shift then
    with TControl(FCurrentMover) do begin
      Left:= Left + X - width div 2;
      Top:= Top + Y - Height div 2;
      ELoginItemX.Value:= Left;
      ELoginItemY.Value:= Top;
      ELoginItemW.Value:= Width;
      ELoginItemH.Value:= Height;
      FLoginSetting.SetInteger(ELoginComName.Text + '.Left', Left);
      FLoginSetting.SetInteger(ELoginComName.Text + '.Top', Top);
      FLoginSetting.SetInteger(ELoginComName.Text + '.Width', Width);
      FLoginSetting.SetInteger(ELoginComName.Text + '.Height', Height);
    end;
end;

{$endregion}

end.
