(*******************************************************************************
 Properties list Frame控件
 Author: james von  Create date: 2022/04/09
================================================================================
 Setting: TStringList  属性项目设置  <项目名称>=<项目类型>(类型参数=PPropertyType)
   如果项目类型是 string 或 Enum 则 Object 为 nil，否则，Object = PPropertyType
---------------------- 属性项目类型说明 ----------------------------------------
 string(默认值)        字符型数值
 enum(<值集合>,...)    选择值，用户值必须在本列表里面
  自定义类型
  RegPropertyType('File', EventToGetFile);            文件类型
  RegPropertyType('Date', EventToGetDate);            日期类型
  RegPropertyType('Time', EventToGetTime);            时间类型
  RegPropertyType('Guid', EventToGetGuid);            Guid类型
  RegPropertyType('Code', EventToGetRegCode);         注册码
  RegPropertyType('Color', EventToGetColor);          颜色类型
  RegPropertyType('Image', EventToGetImage);          图片旋转
  RegPropertyType('DBConn', EventToGetDBConnection);  数据库连接串
*******************************************************************************)
unit UPropertyList;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.ValEdit,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.ComCtrls, Vcl.ExtDlgs, Vcl.ExtCtrls;

type
  TData = class
  private
    FLength: Integer;
    procedure SetLength(const Value: Integer);
  public
    Datas: array of Byte;
    destructor Destroy; override;
  published
    property Length: Integer read FLength write SetLength;
  end;

  TFramePropertyList = class(TFrame)
    lstValues: TValueListEditor;
    procedure lstValuesEditButtonClick(Sender: TObject);
  private
    { Private declarations }
    FSettings: TStringList;
    function GetCount: Integer;
    procedure ClearData;
    function GetData(Index: Integer): TData;
    function GetDatas(Name: string): TData;
    function GetValue(Index: Integer): string;
    function GetValues(Name: string): string;
    procedure SetData(Index: Integer; const Value: TData);
    procedure SetDatas(Name: string; const Value: TData);
    procedure SetValue(Index: Integer; const Value: string);
    procedure SetValues(Name: string; const Value: string);
    function GetNames(Index: Integer): string;
    procedure SetSettings(const Value: TStringList);
    function AddSetting(key, val: string): TObject;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Refresh;
    procedure Clear;
    property Names[Index: Integer]: string read GetNames;
    property Datas[Name: string]: TData read GetDatas write SetDatas;
    property DataByIndex[Index: Integer]: TData read GetData write SetData;
    property Values[Name: string]: string read GetValues write SetValues;
    property ValueByIndex[Index: Integer]: string read GetValue write SetValue;
  published
    property Settings: TStringList read FSettings write SetSettings;
    property Count: Integer read GetCount;
  end;

  TVonDatetimeOptions = (vdoDatetime, vdoDate, vdoTime);
  TVonDatetimeDialog = class
  private
    FOwner: TComponent;
    FDatetime: TDatetime;
    FOptions: TVonDatetimeOptions;
    function GetDatetime: TDatetime;
    procedure SetDatetime(const Value: TDatetime);
{$IF DEFINED(CLR)}
  protected
    function LaunchDialog(DialogData: IntPtr): Bool; override;
{$ENDIF}
  public
    constructor Create(AOwner: TComponent);
    destructor Destroy; override;
    function Execute: Boolean;
  published
    property Options: TVonDatetimeOptions read FOptions write FOptions default TVonDatetimeOptions.vdoDatetime;
    property Datetime: TDatetime read GetDatetime write SetDatetime;
  end;

  TPictureEditorDlg = class
    procedure LoadClick(Sender: TObject);
    procedure SaveClick(Sender: TObject);
    procedure ClearClick(Sender: TObject);
    procedure ImagePaintBoxPaint(Sender: TObject);
  private
    Pic: TPicture;
    ImagePaintBox: TPaintBox;
    btnSave: TButton;
    btnClear: TButton;
    FForm: TForm;
  public
    constructor Create;
    destructor Destroy;
    function Execute: Boolean;
  end;

  TFileEditorDlg = class
    procedure LoadClick(Sender: TObject);
    procedure SaveClick(Sender: TObject);
    procedure ClearClick(Sender: TObject);
  private
    EFileExt: TEdit;
    FileData: TBytes;
    ImagePaintBox: TPaintBox;
    btnSave: TButton;
    btnClear: TButton;
    FForm: TForm;
  public
    constructor Create;
    destructor Destroy;
    function Execute: Boolean;
  end;

  TEventToGetValue = function (Lst: TFramePropertyList; Key: string): Boolean;

  procedure RegPropertyType(TypeName: string; Event: TEventToGetValue);
  procedure GetPropertyTypes(List: TStrings);

implementation

uses System.DateUtils, DCPbase64, UVonSystemFuns, Vcl.Consts;

type
  PPropertyType = ^TPropertyType;
  TPropertyType = record
    TypeName: string;
    Event: TEventToGetValue;
  end;

var
  FPropertyTypeList : TList;

procedure RegPropertyType(TypeName: string; Event: TEventToGetValue);
var
  P: PPropertyType;
begin
  new(P);
  P.TypeName:= TypeName;
  P.Event:= Event;
  FPropertyTypeList.Add(P);
end;

procedure GetPropertyTypes(List: TStrings);
var
  I: Integer;
begin
  for I := 0 to FPropertyTypeList.Count - 1 do
    List.Add(PPropertyType(FPropertyTypeList[I]).TypeName);
end;

procedure FreePropertyList();
var
  I: Integer;
begin
  for I := 0 to FPropertyTypeList.Count - 1 do
    FreeMem(PPropertyType(FPropertyTypeList[I]));
  FPropertyTypeList.Free;
end;

{ TData }

destructor TData.Destroy;
begin
  Length:= 0;
  inherited;
end;

procedure TData.SetLength(const Value: Integer);
begin
  System.SetLength(Datas, Value);
  FLength := Value;
end;


{$R *.dfm}

{ TFramePropertyList }

constructor TFramePropertyList.Create(AOwner: TComponent);
begin
  inherited;
  FSettings:= TStringList.Create;
end;

destructor TFramePropertyList.Destroy;
begin
  ClearData;
  FSettings.Free;
  inherited;
end;

{$region '处理页面'}
procedure TFramePropertyList.ClearData;
var
  I: Integer;
  obj: TObject;
begin
  for I := 0 to lstValues.Strings.Count - 1 do begin
    obj:= lstValues.Strings.Objects[I];
    if Assigned(obj) then Obj.Free;
  end;
  lstValues.Strings.Clear;
end;

procedure TFramePropertyList.Clear;
begin
  ClearData;
  FSettings.Clear;
end;

procedure TFramePropertyList.Refresh;
var
  I: Integer;
begin
  ClearData;
  for I := 0 to FSettings.Count - 1 do
    FSettings.Objects[I]:= AddSetting(FSettings.Names[I], FSettings.ValueFromIndex[I]);
end;

procedure TFramePropertyList.SetSettings(const Value: TStringList);
begin
  FSettings.Assign(Value);
  Refresh;
end;

function TFramePropertyList.AddSetting(key, val: string): TObject;
var
  I, I1, I2, ARow: Integer;
  param: string;
  procedure AppendPickListValue(key, data: string);
  var ARow: Integer; szList: TStringList;
  begin
    szList:= TStringList.Create;
    szList.Delimiter:= ',';
    szList.DelimitedText:= data;
    ARow:= lstValues.InsertRow(key, szList[0], true);
    with lstValues.ItemProps[key] do begin
      EditStyle:= esPickList;
      PickList:= szList;
    end;
    szList.Free;
  end;
begin
  Result:= nil;
  I:= Pos('(', val);
  if I < 1 then begin
    ARow:= lstValues.InsertRow(key, val, true);
    lstValues.ItemProps[key].EditStyle:= TEditStyle.esSimple;
    Exit;
  end;
  param:= Copy(val, I + 1, Length(val) - I - 1);
  val:= Copy(val, 1, I - 1);
  if SameText(val, 'string') then begin
    ARow:= lstValues.InsertRow(key, param, true);
    lstValues.ItemProps[key].EditStyle:= TEditStyle.esSimple;
  end else if SameText(val, 'enum') then
    AppendPickListValue(key, param)
  else for I := 0 to FPropertyTypeList.Count - 1 do
    if SameText(val, PPropertyType(FPropertyTypeList[I]).TypeName) then begin
      Result:= TObject(FPropertyTypeList[I]);
      ARow:= lstValues.InsertRow(key, param, true);
      lstValues.ItemProps[key].EditStyle:= TEditStyle.esEllipsis;
    end;
end;
{$endregion}
{$region '数据处理'}
function TFramePropertyList.GetCount: Integer;
begin
  Result:= lstValues.Strings.Count;
end;

function TFramePropertyList.GetData(Index: Integer): TData;
begin
  Result:= TData(lstValues.Strings.Objects[Index]);
end;

function TFramePropertyList.GetDatas(Name: string): TData;
begin
  Result:= GetData(lstValues.Strings.IndexOfName(Name));
end;

function TFramePropertyList.GetNames(Index: Integer): string;
begin
  Result:= lstValues.Strings.Names[Index];
end;

function TFramePropertyList.GetValue(Index: Integer): string;
begin
  Result:= lstValues.Strings.Values[GetNames(Index)];
end;

function TFramePropertyList.GetValues(Name: string): string;
begin
  Result:= lstValues.Strings.Values[Name];
end;

procedure TFramePropertyList.lstValuesEditButtonClick(Sender: TObject);
var
  P: PPropertyType;
  key, val, data: string;
begin
  key:= lstValues.Cells[0, lstValues.Row];
  val:= lstValues.Cells[1, lstValues.Row];
  P:= PPropertyType(FSettings.Objects[FSettings.IndexOfName(Key)]);
  if P = nil then Exit;
  P.Event(Self, key);
end;

procedure TFramePropertyList.SetData(Index: Integer; const Value: TData);
begin
  if Assigned(lstValues.Strings.Objects[Index]) then
    lstValues.Strings.Objects[Index].Free;
  lstValues.Strings.Objects[Index]:= TObject(Value);
end;

procedure TFramePropertyList.SetDatas(Name: string; const Value: TData);
var
  Idx: Integer;
begin
  Idx:= lstValues.Strings.IndexOfName(Name);
  if Idx >= 0 then SetData(Idx, Value);
end;

procedure TFramePropertyList.SetValue(Index: Integer; const Value: string);
begin
  lstValues.Cells[1, Index + 1]:= Value;
end;

procedure TFramePropertyList.SetValues(Name: string; const Value: string);
var
  Idx: Integer;
begin
  Idx:= lstValues.Strings.IndexOfName(Name);
  if Idx >= 0 then SetValue(Idx, Value);
end;
{$endregion}

{ TVonDialog }

constructor TVonDatetimeDialog.Create(AOwner: TComponent);
begin
  FOwner:= AOwner;
  FDatetime:= Now;
end;

destructor TVonDatetimeDialog.Destroy;
begin
  inherited;
end;

function TVonDatetimeDialog.Execute: Boolean;
var
  FForm: TForm;
  FDateTimePicker: TDateTimePicker;
  FMonthCalendar: TMonthCalendar;
  lpPoint: TPoint;
  procedure CreateCalendar;
  begin
    FMonthCalendar:= TMonthCalendar.Create(FForm);
    FMonthCalendar.Parent:= FForm;
    FMonthCalendar.Align:= alTop;
    FMonthCalendar.Height:= 190;
    FMonthCalendar.Date:= DateOf(FDatetime);
  end;
  procedure CreateTime;
  begin
    FDateTimePicker:= TDateTimePicker.Create(FForm);
    FDateTimePicker.Parent:= FForm;
    FDateTimePicker.Align:= alTop;
    FDateTimePicker.Kind:= TDateTimeKind.dtkTime;
    FDateTimePicker.DateTime:= TimeOf(FDatetime);
  end;
  procedure SetSize;
  begin
    if GetCursorPos(lpPoint)then
      case FOptions of
      vdoDatetime: FForm.SetBounds(lpPoint.X, lpPoint.Y, 222, 254);
      vdoDate: FForm.SetBounds(lpPoint.X, lpPoint.Y, 222, 222);
      vdoTime: FForm.SetBounds(lpPoint.X, lpPoint.Y, 222, 64);
      end;
  end;
begin
  FForm:= TForm.Create(FOwner);
  try
    SetSize;
    case FOptions of
    vdoDatetime: begin
        CreateCalendar;
        CreateTime;
        FDateTimePicker.Top:= FMonthCalendar.Height;
        FDateTimePicker.Width:= FMonthCalendar.Width;
      end;
    vdoDate: CreateCalendar;
    vdoTime: CreateTime;
    end;
    with TBitBtn.Create(FForm) do begin
      Parent:= FForm;
      Top:= FForm.Height - 32;
      Left:= 60;
      Kind:= TBitBtnKind.bkOK;
      Caption:= '确定';
      ModalResult:= mrOK;
    end;
    with TBitBtn.Create(FForm) do begin
      Parent:= FForm;
      Top:= FForm.Height - 32;
      Left:= 140;
      Kind:= TBitBtnKind.bkCancel;
      Caption:= '放弃';
      ModalResult:= mrCancel;
    end;
    FForm.BorderStyle := bsNone;
    FForm.Position := poDesigned;
    Result:= FForm.ShowModal = mrOK;
    if Result then
      FDatetime:= DateOf(FMonthCalendar.Date) + TimeOf(FDateTimePicker.Time);
  finally
    FForm.Free;
  end;
end;

function TVonDatetimeDialog.GetDatetime: TDatetime;
begin
  case FOptions of
  vdoDatetime: Result:= FDatetime;
  vdoDate: Result:= DateOf(FDatetime);
  vdoTime: Result:= TimeOf(FDatetime);
  end;
end;

procedure TVonDatetimeDialog.SetDatetime(const Value: TDatetime);
begin
  FDatetime:= Value;
end;

{ TPictureEditorDlg }

constructor TPictureEditorDlg.Create;
var
  GroupBox1: TGroupBox;
  btnPanel: TPanel;
  procedure InitControl(Ctrl: TWinControl; Left, Top, Width, Height: Integer);
  begin
    Ctrl.Left:= Left;
    Ctrl.Top:= Top;
    Ctrl.Width:= Width;
    Ctrl.Height:= Height;
  end;
  function AddBtn(Left, Top, Width, Height: Integer; Caption: string; Parent: TWinControl): TButton;
  begin
    Result:= TButton.Create(FForm);
    Result.Parent:= Parent;
    Result.Caption:= Caption;
    InitControl(Result, Left, Top, Width, Height);
  end;
begin
  Pic := TPicture.Create;
  {$region 'Create a form'}
  FForm:= TForm.Create(nil);
  with FForm do begin
    BorderIcons:= [biSystemMenu];
    BorderStyle:= bsDialog;
    Caption:= 'Picture Editor';
    ClientHeight:= 306;
    ClientWidth:= 357;
    Position:= poScreenCenter;
  end;
  {$endregion}
  {$region 'Create any components on the form'}
  with AddBtn(274, 12, 75, 25, 'OK', FForm) do begin
    Default:= True; ModalResult:= 1;
  end;
  with AddBtn(274, 41, 75, 25, 'Cancel', FForm) do begin
    ModalResult:= 2; Cancel:= True;
  end;
  GroupBox1:= TGroupBox.Create(FForm);
  GroupBox1.Parent:= FForm;
  InitControl(GroupBox1, 10, 7, 255, 288);
  ImagePaintBox:= TPaintBox.Create(FForm);
  ImagePaintBox.Parent:= GroupBox1;
  ImagePaintBox.Align:= alClient;
  ImagePaintBox.OnPaint:= ImagePaintBoxPaint;
  btnPanel:= TPanel.Create(FForm);
  btnPanel.Parent:= GroupBox1;
  btnPanel.Align:= alBottom;
  btnPanel.Height:= 32;
  with AddBtn(0, 0, 75, 25, '&Load...', btnPanel) do begin
    Align:= alLeft;
    AlignWithMargins:= true;
    OnClick:= LoadClick;
  end;
  btnSave:= AddBtn(78, 0, 75, 25, '&Save...', btnPanel);
  with btnSave do begin
    Align:= alLeft;
    AlignWithMargins:= true;
    Enabled := False;
    OnClick:= SaveClick;
  end;
  btnClear:= AddBtn(156, 0, 75, 25, '&Clear...', btnPanel);
  with btnClear do begin
    Align:= alLeft;
    AlignWithMargins:= true;
    OnClick:= ClearClick;
  end;
  {$endregion}
end;

destructor TPictureEditorDlg.Destroy;
begin
  Pic.Free;
  FForm.Free;
end;

function TPictureEditorDlg.Execute: Boolean;
begin
  Result:= FForm.ShowModal = mrOK;
end;

procedure TPictureEditorDlg.ClearClick(Sender: TObject);
begin
  Pic.Graphic := nil;
  ImagePaintBox.Invalidate;
  btnSave.Enabled := False;
  btnClear.Enabled := False;
end;

procedure TPictureEditorDlg.ImagePaintBoxPaint(Sender: TObject);
var
  DrawRect: TRect;
  SNone: string;
begin
  with TPaintBox(Sender) do
  begin
    Canvas.Brush.Color := {Self.}Color;
    DrawRect := ClientRect;//Rect(Left, Top, Left + Width, Top + Height);
    if Pic.Width > 0 then
    begin
      with DrawRect do
        if (Pic.Width > Right - Left) or (Pic.Height > Bottom - Top) then
        begin
          if Pic.Width > Pic.Height then
            Bottom := Top + MulDiv(Pic.Height, Right - Left, Pic.Width)
          else
            Right := Left + MulDiv(Pic.Width, Bottom - Top, Pic.Height);
          Canvas.StretchDraw(DrawRect, Pic.Graphic);
        end
        else
          with DrawRect do
            Canvas.Draw(Left + (Right - Left - Pic.Width) div 2, Top + (Bottom - Top -
              Pic.Height) div 2, Pic.Graphic);
    end
    else
      with DrawRect, Canvas do
      begin
        SNone := srNone;
        TextOut(Left + (Right - Left - TextWidth(SNone)) div 2, Top + (Bottom -
          Top - TextHeight(SNone)) div 2, SNone);
      end;
  end;
end;

procedure TPictureEditorDlg.LoadClick(Sender: TObject);
begin
  with TOpenDialog.Create(nil) do try
    Filter:=
      'All (*.bmp;*.ico;*.emf;*.wmf)|*.bmp;*.ico;*.emf;*.wmf|Bitmaps (*' +
      '.bmp)|*.bmp|Icons (*.ico)|*.ico|Enhanced Metafiles (*.emf)|*.emf' +
      '|Metafiles (*.wmf)|*.wmf';
    if Execute then
    begin
      Pic.LoadFromFile(Filename);
      ImagePaintBox.Invalidate;
      btnSave.Enabled := (Pic.Graphic <> nil) and not Pic.Graphic.Empty;
      btnClear.Enabled := (Pic.Graphic <> nil) and not Pic.Graphic.Empty;
    end;
  finally
    Free;
  end;
end;

procedure TPictureEditorDlg.SaveClick(Sender: TObject);
begin
  if Pic.Graphic <> nil then
  with TSaveDialog.Create(nil) do try
    Filter:=
      'All (*.bmp;*.ico;*.emf;*.wmf)|*.bmp;*.ico;*.emf;*.wmf|Bitmaps (*' +
      '.bmp)|*.bmp|Icons (*.ico)|*.ico|Enhanced Metafiles (*.emf)|*.emf' +
      '|Metafiles (*.wmf)|*.wmf';
    Options:= [ofOverwritePrompt, ofEnableSizing];
    DefaultExt := GraphicExtension(TGraphicClass(Pic.Graphic.ClassType));
    Filter := GraphicFilter(TGraphicClass(Pic.Graphic.ClassType));
    if Execute then Pic.SaveToFile(Filename);
  finally
    Free;
  end;
end;

{ TFileEditorDlg }

constructor TFileEditorDlg.Create;
var
  GroupBox1: TGroupBox;
  btnPanel: TPanel;
  procedure InitControl(Ctrl: TWinControl; Left, Top, Width, Height: Integer);
  begin
    Ctrl.Left:= Left;
    Ctrl.Top:= Top;
    Ctrl.Width:= Width;
    Ctrl.Height:= Height;
  end;
  function AddBtn(Left, Top, Width, Height: Integer; Caption: string; Parent: TWinControl): TButton;
  begin
    Result:= TButton.Create(FForm);
    Result.Parent:= Parent;
    Result.Caption:= Caption;
    InitControl(Result, Left, Top, Width, Height);
  end;
begin
  {$region 'Create a form'}
  FForm:= TForm.Create(nil);
  with FForm do begin
    BorderIcons:= [biSystemMenu];
    BorderStyle:= bsDialog;
    Caption:= 'File Editor';
    ClientHeight:= 88;
    ClientWidth:= 357;
    Position:= poScreenCenter;
  end;
  {$endregion}
  {$region 'Create any components on the form'}
  with AddBtn(274, 12, 75, 25, 'OK', FForm) do begin
    Default:= True; ModalResult:= 1;
  end;
  with AddBtn(274, 41, 75, 25, 'Cancel', FForm) do begin
    ModalResult:= 2; Cancel:= True;
  end;
  GroupBox1:= TGroupBox.Create(FForm);
  GroupBox1.Parent:= FForm;
  InitControl(GroupBox1, 10, 7, 255, 69);
  EFileExt:= TEdit.Create(FForm);
  EFileExt.Parent:= GroupBox1;
  EFileExt.Align:= alClient;
  btnPanel:= TPanel.Create(FForm);
  btnPanel.Parent:= GroupBox1;
  btnPanel.Align:= alBottom;
  btnPanel.Height:= 32;
  with AddBtn(0, 0, 75, 25, '&Load...', btnPanel) do begin
    Align:= alLeft;
    AlignWithMargins:= true;
    OnClick:= LoadClick;
  end;
  btnSave:= AddBtn(78, 0, 75, 25, '&Save...', btnPanel);
  with btnSave do begin
    Align:= alLeft;
    AlignWithMargins:= true;
    Enabled := False;
    OnClick:= SaveClick;
  end;
  btnClear:= AddBtn(156, 0, 75, 25, '&Clear...', btnPanel);
  with btnClear do begin
    Align:= alLeft;
    AlignWithMargins:= true;
    OnClick:= ClearClick;
  end;
  {$endregion}
end;

destructor TFileEditorDlg.Destroy;
begin
  SetLength(FileData, 0);
end;

procedure TFileEditorDlg.ClearClick(Sender: TObject);
begin
  EFileExt.Text:= '';
  SetLength(FileData, 0);
end;

function TFileEditorDlg.Execute: Boolean;
begin
  Result:= FForm.ShowModal = mrOK;
end;

procedure TFileEditorDlg.LoadClick(Sender: TObject);
var
  fs: TFileStream;
begin
  with TOpenDialog.Create(nil) do try
    if Execute then
    begin
      fs:= TFileStream.Create(Filename, fmOpenRead);
      SetLength(FileData, fs.Size);
      fs.Read(FileData, fs.Size);
      EFileExt.Text:= 'data:file/' + Copy(ExtractFileExt(Filename), 2, MaxInt);
      btnSave.Enabled := fs.Size > 0;
      btnClear.Enabled := fs.Size > 0;
      fs.Free;
    end;
  finally
    Free;
  end;
end;

procedure TFileEditorDlg.SaveClick(Sender: TObject);
var
  fs: TFileStream;
begin
  if Length(FileData) > 0 then
  with TSaveDialog.Create(nil) do try
    DefaultExt:= Copy(EFileExt.Text, 11, MaxInt);
    Options:= [ofOverwritePrompt, ofEnableSizing];
    if Execute then begin
      fs:= TFileStream.Create(Filename, fmCreate);
      fs.Write(FileData, Length(FileData));
      fs.Free;
    end;
  finally
    Free;
  end;
end;
{$region 'Event of list button'}
function EventToGetFile(Lst: TFramePropertyList; Key: string): Boolean;
var
  bs: TData;
begin
  {$region 'read a file and save it to B64 data.'}
  with TFileEditorDlg.Create do try
    EFileExt.Text:= Lst.Values[Key];
    bs:= Lst.Datas[Key];
    if Assigned(bs) then begin
      SetLength(FileData, bs.Length);
      Move(bs.Datas[0], FileData[0], bs.Length);
      btnSave.Enabled:= True;
    end;
    if Execute then begin
      Lst.Values[Key]:= EFileExt.Text;
      bs:= TData.Create;
      bs.Length:= Length(FileData);
      Move(FileData[0], bs.Datas[0], bs.Length);
      Lst.Datas[Key]:= bs;
      Result:= True;
    end;
  finally
    Free;
  end;
  {$endregion}
end;

function EventToGetImage(Lst: TFramePropertyList; Key: string): Boolean;
var
  fs: TMemoryStream;
  bs: TData;
  index: Integer;
  S: string;
begin
  {$region 'read a file and save it to B64 data.'}
  with TPictureEditorDlg.Create do try
    bs:= Lst.Datas[Key];
    if Assigned(bs) then try
      fs:= TMemoryStream.Create;
      fs.Write(bs.Datas[0], bs.Length);
      fs.Position:= 0;
      Pic.LoadFromStream(fs);
    finally
      fs.Free;
    end;
    if Execute then begin
      fs:= TMemoryStream.Create();
      pic.SaveToStream(fs);
      bs:= TData.Create;
      bs.Length:= fs.Size;
      fs.Position:= 0;
      fs.Read(bs.Datas[0], fs.Size);
      if fs.Size = 0 then Lst.Values[Key]:= ''
      else Lst.Values[Key]:= 'data:image';
      lst.Datas[Key]:= bs;
      Result:= True;
      Fs.Free;
    end;
  finally
    Free;
  end;
  {$endregion}
end;

function EventToGetDate(Lst: TFramePropertyList; Key: string): Boolean;
begin
  {$region 'Get datetime with TVonDialog.'}
  with TVonDatetimeDialog.Create(nil) do try
    Options:= TVonDatetimeOptions.vdoDate;
    if Execute then begin
      Lst.Values[Key]:= DateToStr(Datetime);
      Result:= True;
    end;
  finally
    Free;
  end
  {$endregion}
end;

function EventToGetTime(Lst: TFramePropertyList; Key: string): Boolean;
begin
  {$region 'Get datetime with TVonDialog.'}
  with TVonDatetimeDialog.Create(nil) do try
    Options:= TVonDatetimeOptions.vdoTime;
    if Execute then begin
      Lst.Values[Key]:= TimeToStr(Datetime);
      Result:= True;
    end;
  finally
    Free;
  end
  {$endregion}
end;

function EventToGetGuid(Lst: TFramePropertyList; Key: string): Boolean;
var
  G: TGuid;
begin
  CreateGUID(G);
  Lst.Values[Key]:= GUIDToString(G);
  Result:= True;
end;

function EventToGetRegCode(Lst: TFramePropertyList; Key: string): Boolean;
var
  newCode: TGUID;
  szBit: PByteArray;
  index: Integer;
  S: string;

  function CVByte(aBit: byte): Char;
  const
    chars = 'W0Z4VNSYF8I9ELH6UMD1T3OBQGAK7X25';
  begin // 12345678901234567890123456789012
    Result := chars[aBit div 16 + aBit mod 16 + 1];
  end;

begin
  CreateGUID(newCode);
  with newCode do
  begin
    szBit := @D1;
    S := CVByte(szBit[0]) + CVByte(szBit[1]) + CVByte(szBit[2]) +
      CVByte(szBit[3]) + CVByte(szBit[4]) + '-';
    S := S + CVByte(szBit[5]) + CVByte(szBit[6]) + CVByte(szBit[7]);
    szBit := @D2;
    S := S + CVByte(szBit[0]) + CVByte(szBit[1]) + '-';
    szBit := @D3;
    S := S + CVByte(szBit[0]) + CVByte(szBit[1]);
    szBit := @D4;
    Lst.Values[Key]:= S + CVByte(szBit[0]) + CVByte(szBit[1]) + CVByte(szBit[2]) +
      '-' + CVByte(szBit[3]) + CVByte(szBit[4]) + CVByte(szBit[5]) +
      CVByte(szBit[6]) + CVByte(szBit[7]);
    Lst.Datas[Key]:= nil;
    Result:= True;
  end;
end;

function EventToGetColor(Lst: TFramePropertyList; Key: string): Boolean;
begin
  with TColorDialog.Create(nil) do try
    if Execute then begin
      Lst.Values[Key]:= IntToStr(Color);
      Result:= True;
    end;
  finally
    Free;
  end;
end;

function EventToGetDBConnection(Lst: TFramePropertyList; Key: string): Boolean;
begin
  Lst.Values[Key]:= DlgConnectionString(Lst.Values[Key]);
  Result:= True;
end;
{$endregion}

initialization
  FPropertyTypeList := TList.Create;
  RegPropertyType('File', EventToGetFile);
  RegPropertyType('Date', EventToGetDate);
  RegPropertyType('Time', EventToGetTime);
  RegPropertyType('Guid', EventToGetGuid);
  RegPropertyType('Code', EventToGetRegCode);
  RegPropertyType('Color', EventToGetColor);
  RegPropertyType('Image', EventToGetImage);
  RegPropertyType('DBConn', EventToGetDBConnection);

finalization
  FreePropertyList();

end.
