unit UVonTools;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls,
  Vcl.ValEdit, Vcl.Grids, Vcl.Samples.Spin, Vcl.Imaging.jpeg, UVonClass,
  Vcl.CheckLst, System.ImageList, Vcl.ImgList, Vcl.ToolWin, Vcl.Menus;

type
  TFVonTools = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    Panel2: TPanel;
    Label1: TLabel;
    ECertCipherName: TComboBox;
    Label3: TLabel;
    ECertHashName: TComboBox;
    Label2: TLabel;
    ECertCipherKey: TEdit;
    btnCertRead: TButton;
    btnCertPublish: TButton;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    btnStyleLoad: TButton;
    btnStylePublished: TButton;
    GroupBox1: TGroupBox;
    chkLoginHide: TCheckBox;
    GridPanel1: TGridPanel;
    Label5: TLabel;
    ELoginItemX: TSpinEdit;
    Label6: TLabel;
    ELoginItemY: TSpinEdit;
    Label7: TLabel;
    ELoginItemW: TSpinEdit;
    Label8: TLabel;
    ELoginItemH: TSpinEdit;
    Label9: TLabel;
    ELoginItemFontName: TEdit;
    Label10: TLabel;
    ELoginItemFontSize: TSpinEdit;
    Label11: TLabel;
    ELoginItemFontColor: TColorBox;
    ELoginComName: TComboBox;
    btnFaceOkBtn: TButton;
    btnFaceLoginBtn: TButton;
    btnFaceCancelBtn: TButton;
    btnFaceBackground: TButton;
    Label14: TLabel;
    ImgIcon: TImageList;
    imgBtn: TImageList;
    lstSysCert: TValueListEditor;
    Panel4: TPanel;
    ScrollBox1: TScrollBox;
    imgBackground: TImage;
    lbTitle2: TLabel;
    lbTitle1: TLabel;
    lbPrompt: TLabel;
    lb1: TLabel;
    lb2: TLabel;
    imgLogin: TImage;
    imgCancel: TImage;
    imgOK: TImage;
    lbRemember: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    chkRemember: TCheckBox;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Label12: TLabel;
    ESysFontName: TComboBox;
    Label25: TLabel;
    ESysFontSize: TSpinEdit;
    Label13: TLabel;
    ESysFontColor: TColorBox;
    Bevel2: TBevel;
    ESysMarginTop: TSpinEdit;
    ESysMarginLeft: TSpinEdit;
    ESysMarginRight: TSpinEdit;
    ESysMarginBottom: TSpinEdit;
    Label20: TLabel;
    lstSysAttachment: TListView;
    Label16: TLabel;
    ESysCertAttaName: TEdit;
    btnSysCertAttaAdd: TButton;
    btnSysCertAttaDel: TButton;
    btnSysCertAttaRename: TButton;
    Panel3: TPanel;
    Label4: TLabel;
    ESysBarName: TComboBox;
    ESysMenuName: TComboBox;
    Label15: TLabel;
    Label17: TLabel;
    ESysWinScheme: TComboBox;
    Label18: TLabel;
    EMenuPosition: TComboBox;
    TabSheet3: TTabSheet;
    ImgSmall: TImageList;
    Panel11: TPanel;
    ViewLarge: TListView;
    viewSmall: TListView;
    Splitter1: TSplitter;
    Panel12: TPanel;
    ToolBar1: TToolBar;
    btnLargeTop: TToolButton;
    btnLargeUp: TToolButton;
    btnLargeDown: TToolButton;
    btnLargeBottom: TToolButton;
    btnLargePublish: TButton;
    btnLargeAdd: TButton;
    btnLargeLoad: TButton;
    btnLargeDel: TButton;
    Panel13: TPanel;
    ToolBar2: TToolBar;
    btnSmallTop: TToolButton;
    btnSmallUp: TToolButton;
    btnSmallDown: TToolButton;
    btnSmallBottom: TToolButton;
    btnSmallPublish: TButton;
    btnSmallAdd: TButton;
    btnSmallLoad: TButton;
    btnSmallDel: TButton;
    imgLarge: TImageList;
    Panel15: TPanel;
    Panel6: TPanel;
    Label19: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    EParentMenuName: TEdit;
    EMenuName: TEdit;
    EMenuRunType: TComboBox;
    ToolBar4: TToolBar;
    btnManuTop: TToolButton;
    btnManuUp: TToolButton;
    btnManuDown: TToolButton;
    btnManuBotom: TToolButton;
    Panel9: TPanel;
    Label27: TLabel;
    EMenuCtrlValue: TSpinEdit;
    EMenuHint: TEdit;
    EMenuParams: TEdit;
    Panel10: TPanel;
    Label28: TLabel;
    EMenuShortKey: TComboBox;
    EMenuTask: TEdit;
    btnMenuPublish: TButton;
    btnMenuDel: TButton;
    btnMenuEdit: TButton;
    btnMenuAdd: TButton;
    btnMenuLoad: TButton;
    Panel14: TPanel;
    Label29: TLabel;
    EMenuImageIdx: TSpinEdit;
    TreeMenu: TTreeView;
    Splitter2: TSplitter;
    Label30: TLabel;
    ELoginItemCaption: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure btnCertPublishClick(Sender: TObject);
    procedure btnCertReadClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lbTitle1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ELoginItemXChange(Sender: TObject);
    procedure ELoginItemYChange(Sender: TObject);
    procedure ELoginItemWChange(Sender: TObject);
    procedure ELoginItemHChange(Sender: TObject);
    procedure ELoginItemFontSizeChange(Sender: TObject);
    procedure ELoginItemFontNameExit(Sender: TObject);
    procedure ELoginItemFontColorChange(Sender: TObject);
    procedure btnStyleLoadClick(Sender: TObject);
    procedure btnFaceOkBtnClick(Sender: TObject);
    procedure btnFaceLoginBtnClick(Sender: TObject);
    procedure btnFaceCancelBtnClick(Sender: TObject);
    procedure btnFaceBackgroundClick(Sender: TObject);
    procedure btnStylePublishedClick(Sender: TObject);
    procedure lbTitle1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure chkLoginHideClick(Sender: TObject);
    procedure btnSysCertAttaAddClick(Sender: TObject);
    procedure btnSysCertAttaRenameClick(Sender: TObject);
    procedure btnSysCertAttaDelClick(Sender: TObject);
    procedure btnSmallLoadClick(Sender: TObject);
    procedure btnLargeLoadClick(Sender: TObject);
    procedure btnSmallAddClick(Sender: TObject);
    procedure btnLargeAddClick(Sender: TObject);
    procedure btnSmallDelClick(Sender: TObject);
    procedure btnLargeDelClick(Sender: TObject);
    procedure btnSmallPublishClick(Sender: TObject);
    procedure btnLargePublishClick(Sender: TObject);
    procedure btnSmallTopClick(Sender: TObject);
    procedure btnSmallUpClick(Sender: TObject);
    procedure btnSmallDownClick(Sender: TObject);
    procedure btnSmallBottomClick(Sender: TObject);
    procedure btnLargeTopClick(Sender: TObject);
    procedure btnLargeUpClick(Sender: TObject);
    procedure btnLargeDownClick(Sender: TObject);
    procedure btnLargeBottomClick(Sender: TObject);
    procedure btnMenuPublishClick(Sender: TObject);
    procedure btnMenuLoadClick(Sender: TObject);
    procedure btnMenuAddClick(Sender: TObject);
    procedure ViewLargeChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure viewSmallChange(Sender: TObject; Item: TListItem;
      Change: TItemChange);
    procedure ELoginItemCaptionExit(Sender: TObject);
  private
    { Private declarations }
    FCipherName, FHashName, FCertKey, FCertFilename: string;
    FStyleFile: TVonSectionGroup;
    FLoginSetting: TVonSection;
    FStyleSetting: TVonSection;
    FCurrentMover: TObject;
    /// <summary>应用系统设定的功能列表</summary>
    FAppUsingList,
    /// <summary>应用系统设定的菜单列表</summary>
    FAppMenuList,
    /// <summary>应用系统设定的按钮列表</summary>
    FAppTaskList: TVonArraySetting;
    procedure WriteImageData(Key: string; img: TImage);
    procedure DisplayMenu(PName: string; PNode: TTreeNode;
      MenuValues: TVonArraySetting);
    procedure InitEnvironment;
  public
    { Public declarations }
  end;

var
  FVonTools: TFVonTools;

implementation

uses DCPBase64, UVonCrypt, UVonSystemFuns;

type TVonControl = class(TControl) published property Font; end;

{$R *.dfm}

procedure TFVonTools.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  InitEnvironment;
  // ======= Form style =======
  FStyleFile:= TVonSectionGroup.Create;
  FLoginSetting:= TVonSection.Create;
  FLoginSetting.SectionName:= 'Login';
  FStyleFile.AddSection(FLoginSetting);
  FStyleSetting:= TVonSection.Create;
  FStyleSetting.SectionName:= 'Style';
  FStyleFile.AddSection(FStyleSetting);
  // ======= Cipher and hash =======
  GetCipherList(ECertCipherName.Items);
  ECertCipherName.ItemIndex:= 0;
  GetHashList(ECertHashName.Items);
  ECertHashName.ItemIndex:= 0;
  // ======= Load controls to combobox =======
  for I := 0 to ScrollBox1.ControlCount - 1 do
    ELoginComName.Items.Add(ScrollBox1.Controls[I].Name);
  ESysFontName.Items.Assign(Screen.Fonts);
  ESysFontName.ItemIndex:= ESysFontName.Items.IndexOf(Self.Font.Name);
  // ======= Menu var =======
  FAppUsingList := TVonArraySetting.Create;// 应用系统设定的功能列表
  FAppMenuList := TVonArraySetting.Create; // 应用系统设定的菜单列表
  FAppTaskList := TVonArraySetting.Create; // 应用系统设定的按钮列表
end;

procedure TFVonTools.FormDestroy(Sender: TObject);
begin
  FAppTaskList.Free;
  FAppMenuList.Free;
  FAppUsingList.Free;
  FStyleSetting.Free;
end;

{$region 'Certification'}
procedure TFVonTools.btnCertReadClick(Sender: TObject);
var
  sIntput: TFileStream;
  szStream: TMemoryStream;
  I: Integer;
begin
  with OpenDialog1 do
    if Execute then begin
      // Load certification file and decrypt
      with GetCipher(ECertCipherName.Text) do try
        InitStr(ECertCipherKey.Text, GetHashClass(ECertHashName.Text));
        sIntput := TFileStream.Create(Filename, fmOpenRead);
        szStream:= TMemoryStream.Create;
        DecryptStream(sIntput, szStream, sIntput.Size);
        szStream.Position:= 0;
        lstSysCert.Strings.LoadFromStream(szStream);
        szStream.Free;
        sIntput.Free;
      finally
        Free;
      end;
    end;
end;

procedure TFVonTools.btnCertPublishClick(Sender: TObject);
var
  sOutput: TFileStream;
  szStream: TMemoryStream;
  I: Integer;
begin
  with SaveDialog1 do
    if Execute then begin //Save certification to file
      with GetCipher(ECertCipherName.Text) do try
        InitStr(ECertCipherKey.Text, GetHashClass(ECertHashName.Text));
        szStream:= TMemoryStream.Create;
        lstSysCert.Strings.Values['Publishedate']:= DateTimeToStr(Now);
        lstSysCert.Strings.SaveToStream(szStream);
        szStream.Position:= 0;
        sOutput := TFileStream.Create(Filename, fmCreate);
        EncryptStream(szStream, sOutput, szStream.Size);
        szStream.Free;
        sOutput.Free;
      finally
        Free;
      end;

    end;
end;

procedure TFVonTools.btnSysCertAttaAddClick(Sender: TObject);
var
  fs: TFileStream;
  item: TListItem;
  b: TBytes;
begin
  with OpenDialog1 do
    if Execute then begin
      fs:= TFileStream.Create(Filename, fmOpenRead);
      SetLength(b, fs.Size);
      fs.Read(b, fs.Size);
      FStyleSetting.SetBinary(ESysCertAttaName.Text, b);
      fs.Free;
      item:= lstSysAttachment.FindCaption(0, ESysCertAttaName.Text,True,True,True);
      if not Assigned(item) then
        with lstSysAttachment.Items.Add do begin
          Caption:= ESysCertAttaName.Text;
          ImageIndex:= 1;
        end;
    end;
end;

procedure TFVonTools.btnSysCertAttaDelClick(Sender: TObject);
begin
  if not Assigned(lstSysAttachment.Selected) then Exit;
  if lstSysAttachment.Selected.Caption <> ESysCertAttaName.Text then
    raise Exception.Create('尚未选择系统参数附件');
  FStyleSetting.Delete(ESysCertAttaName.Text);
  lstSysAttachment.DeleteSelected;
end;

procedure TFVonTools.btnSysCertAttaRenameClick(Sender: TObject);
begin
  if not Assigned(lstSysAttachment.Selected) then Exit;
  FStyleSetting.Values[lstSysAttachment.Selected.Caption].Name:= ESysCertAttaName.Text;
  lstSysAttachment.Selected.Caption:= ESysCertAttaName.Text;
end;
{$endregion}
{$region 'Face designer'}

procedure TFVonTools.chkLoginHideClick(Sender: TObject);
begin
  FLoginSetting.SetBool(ELoginComName.Text + '.Hide', chkLoginHide.Checked);
end;

procedure TFVonTools.WriteImageData(Key: string; img: TImage);
var
  ms: TMemoryStream;
begin
  ms:= TMemoryStream.Create;
  img.Picture.SaveToStream(ms);
  ms.Position:= 0;
  FLoginSetting.SetStream(Key, ms);
  ms.Free;
end;

procedure TFVonTools.btnFaceLoginBtnClick(Sender: TObject);
begin
  with OpenDialog1 do
    if Execute then begin
      imgLogin.Picture.LoadFromFile(Filename);
      WriteImageData('imgLogin', imgLogin);
    end;
end;

procedure TFVonTools.btnFaceOkBtnClick(Sender: TObject);
begin
  with OpenDialog1 do
    if Execute then begin
      imgOK.Picture.LoadFromFile(Filename);
      WriteImageData('imgOK', imgOK);
    end;
end;

procedure TFVonTools.btnStylePublishedClick(Sender: TObject);
begin
  with SaveDialog1 do
    if Execute then begin
      FStyleSetting.SetString('Font.Name', ESysFontName.Text);
      FStyleSetting.SetInteger('Font.Size', ESysFontSize.Value);
      FStyleSetting.SetInteger('Font.Color', ESysFontColor.Selected);
      FStyleSetting.SetInteger('Margin.Left', ESysMarginLeft.Value);
      FStyleSetting.SetInteger('Margin.Top', ESysMarginTop.Value);
      FStyleSetting.SetInteger('Margin.Right', ESysMarginRight.Value);
      FStyleSetting.SetInteger('Margin.Bottom', ESysMarginBottom.Value);
      FStyleSetting.SetString('WinScheme', ESysWinScheme.Text);
      FStyleSetting.SetString('WinBar', ESysBarName.Text);
      FStyleSetting.SetString('MenuName', ESysMenuName.Text);
      FStyleFile.SaveToFile(Filename);
    end;
end;

procedure TFVonTools.btnFaceBackgroundClick(Sender: TObject);
var
  ms: TMemoryStream;
begin
  with OpenDialog1 do
    if Execute then begin
      imgBackground.Picture.LoadFromFile(Filename);
      ms:= TMemoryStream.Create;
      imgBackground.Picture.SaveToStream(ms);
      ms.Position:= 0;
      FLoginSetting.SetStream('imgBackground', ms);
      ms.Free;
    end;
end;

procedure TFVonTools.btnFaceCancelBtnClick(Sender: TObject);
begin
  with OpenDialog1 do
    if Execute then begin
      imgCancel.Picture.LoadFromFile(Filename);
      WriteImageData('imgCancel', imgCancel);
    end;
end;

procedure TFVonTools.btnStyleLoadClick(Sender: TObject);
var
  I: Integer;
  ms: TMemoryStream;
begin
  with OpenDialog1 do
    if Execute then begin
      FStyleFile.LoadFromFile(Filename);
      FLoginSetting:= FStyleFile.Sections['Login'];
      if not Assigned(FLoginSetting) then begin
        FLoginSetting:= TVonSection.Create;
        FLoginSetting.SectionName:= 'Login';
        FStyleFile.AddSection(FLoginSetting);
      end;
      for I := 0 to ScrollBox1.ControlCount - 1 do
        begin
          with TControl(ScrollBox1.Controls[I])do begin
            Top:= FLoginSetting.GetInteger(ScrollBox1.Controls[I].Name + '.Top');
            Left:= FLoginSetting.GetInteger(ScrollBox1.Controls[I].Name + '.Left');
            Width:= FLoginSetting.GetInteger(ScrollBox1.Controls[I].Name + '.Width');
            Height:= FLoginSetting.GetInteger(ScrollBox1.Controls[I].Name + '.Height');
          end;
          if ScrollBox1.Controls[I] is TImage then begin
            ms:= TMemoryStream.Create;
            FLoginSetting.GetStream(ScrollBox1.Controls[I].Name, ms);
            ms.Position:= 0;
            TImage(ScrollBox1.Controls[I]).Picture.LoadFromStream(ms);
            ms.Free;
          end else with TVonControl(ScrollBox1.Controls[I])do begin
            Caption:= FLoginSetting.GetString(ScrollBox1.Controls[I].Name + '.Caption', Caption);
            Font.Size:= FLoginSetting.GetInteger(ScrollBox1.Controls[I].Name + '.Font.Size');
            Font.Color:= TColor(FLoginSetting.GetInteger(ScrollBox1.Controls[I].Name + '.Font.Color'));
            Font.Name:= FLoginSetting.GetString(ScrollBox1.Controls[I].Name + '.Font.Name');
          end;
        end;
      FStyleSetting:= FStyleFile.Sections['Style'];
      lstSysAttachment.Clear;
      if not Assigned(FStyleSetting) then begin
        FStyleSetting:= TVonSection.Create;
        FStyleSetting.SectionName:= 'Style';
        FStyleFile.AddSection(FStyleSetting);
      end else begin
        ESysFontName.Text:= FStyleSetting.GetString('Font.Name');
        ESysFontSize.Value:= FStyleSetting.GetInteger('Font.Size');
        ESysFontColor.Selected:= FStyleSetting.GetInteger('Font.Color');
        ESysMarginLeft.Value:= FStyleSetting.GetInteger('Font.Left');
        ESysMarginTop.Value:= FStyleSetting.GetInteger('Font.Top');
        ESysMarginRight.Value:= FStyleSetting.GetInteger('Font.Right');
        ESysMarginBottom.Value:= FStyleSetting.GetInteger('Font.Bottom');
        ESysWinScheme.ItemIndex:= ESysWinScheme.Items.IndexOf(FStyleSetting.GetString('WinScheme'));
        ESysBarName.ItemIndex:= ESysBarName.Items.IndexOf(FStyleSetting.GetString('WinBar'));
        ESysMenuName.ItemIndex:= ESysMenuName.Items.IndexOf(FStyleSetting.GetString('MenuName'));
        for I := 0 to FStyleSetting.ValueCount - 1 do
          if FStyleSetting.ValueByIndex[I].ValueType = vst_bytes then
            with lstSysAttachment.Items.Add do begin
              Caption:= FStyleSetting.ValueByIndex[I].Name;
              ImageIndex:= 1;
            end;
      end;
    end;
end;

procedure TFVonTools.ELoginItemCaptionExit(Sender: TObject);
begin
  if(not Assigned(FCurrentMover))or(Sender is TImage)then Exit
  else begin
    TVonControl(FCurrentMover).Caption:= ELoginItemCaption.Text;
    FLoginSetting.SetString(ELoginComName.Text + '.Caption', ELoginItemCaption.Text);
  end;
end;

procedure TFVonTools.ELoginItemFontColorChange(Sender: TObject);
begin
  if(not Assigned(FCurrentMover))or(Sender is TImage)then Exit
  else begin
    TVonControl(FCurrentMover).Font.Color:= ELoginItemFontColor.Selected;
    FLoginSetting.SetInteger(ELoginComName.Text + '.Font.Color', ELoginItemFontColor.Selected);
  end;
end;

procedure TFVonTools.ELoginItemFontNameExit(Sender: TObject);
begin
  if(not Assigned(FCurrentMover))or(Sender is TImage)then Exit
  else begin
    TVonControl(FCurrentMover).Font.Name:= ELoginItemFontName.Text;
    FLoginSetting.SetString(ELoginComName.Text + '.Font.Name', ELoginItemFontName.Text);
  end;
end;

procedure TFVonTools.ELoginItemFontSizeChange(Sender: TObject);
begin
  if(not Assigned(FCurrentMover))or(Sender is TImage)then Exit
  else begin
    TVonControl(FCurrentMover).Font.Size:= ELoginItemFontSize.Value;
    FLoginSetting.SetInteger(ELoginComName.Text + '.Font.Size', ELoginItemFontSize.Value);
  end;
end;

procedure TFVonTools.ELoginItemHChange(Sender: TObject);
begin
  (FCurrentMover as TControl).Height:= ELoginItemH.Value;
  FLoginSetting.SetInteger(ELoginComName.Text + '.Height', ELoginItemH.Value);
end;

procedure TFVonTools.ELoginItemWChange(Sender: TObject);
begin
  (FCurrentMover as TControl).Width:= ELoginItemW.Value;
  FLoginSetting.SetInteger(ELoginComName.Text + '.Width', ELoginItemW.Value);
end;

procedure TFVonTools.ELoginItemXChange(Sender: TObject);
begin
  (FCurrentMover as TControl).Left:= ELoginItemX.Value;
  FLoginSetting.SetInteger(ELoginComName.Text + '.Left', ELoginItemX.Value);
end;

procedure TFVonTools.ELoginItemYChange(Sender: TObject);
begin
  (FCurrentMover as TControl).Top:= ELoginItemY.Value;
  FLoginSetting.SetInteger(ELoginComName.Text + '.Top', ELoginItemY.Value);
end;

procedure TFVonTools.lbTitle1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if FCurrentMover = Sender then Exit;
  FCurrentMover:= Sender;
  ELoginComName.ItemIndex:= ELoginComName.Items.IndexOf(TControl(FCurrentMover).Name);
  chkLoginHide.Checked:= FLoginSetting.GetBool(ELoginComName.Text + '.Hide');
  ELoginItemX.Value:= TControl(FCurrentMover).Left;
  ELoginItemY.Value:= TControl(FCurrentMover).Top;
  ELoginItemW.Value:= TControl(FCurrentMover).Width;
  ELoginItemH.Value:= TControl(FCurrentMover).Height;
  if Sender is TImage then begin
    ELoginItemFontName.Enabled:= False;
    ELoginItemFontSize.Enabled:= False;
    ELoginItemFontColor.Enabled:= False;
    ELoginItemCaption.Enabled:= False;
  end else begin
    ELoginItemCaption.Text:= TVonControl(FCurrentMover).Caption;
    ELoginItemFontName.Text:= TVonControl(FCurrentMover).Font.Name;
    ELoginItemFontSize.Value:= TVonControl(FCurrentMover).Font.Size;
    ELoginItemFontColor.Selected:= TVonControl(FCurrentMover).Font.Color;
    ELoginItemFontName.Enabled:= true;
    ELoginItemFontSize.Enabled:= true;
    ELoginItemFontColor.Enabled:= true;
    ELoginItemCaption.Enabled:= true;
  end;
end;

procedure TFVonTools.lbTitle1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if FCurrentMover <> Sender then Exit;
  if ssLeft in Shift then
    with TControl(FCurrentMover) do begin
      Left:= Left + X - width div 2;
      Top:= Top + Y - Height div 2;
      ELoginItemX.Value:= Left;
      ELoginItemY.Value:= Top;
      ELoginItemW.Value:= Width;
      ELoginItemH.Value:= Height;
      FLoginSetting.SetInteger(ELoginComName.Text + '.Left', Left);
      FLoginSetting.SetInteger(ELoginComName.Text + '.Top', Top);
      FLoginSetting.SetInteger(ELoginComName.Text + '.Width', Width);
      FLoginSetting.SetInteger(ELoginComName.Text + '.Height', Height);
    end;
end;

procedure TFVonTools.ViewLargeChange(Sender: TObject; Item: TListItem;
  Change: TItemChange);
begin
  if Assigned(ViewLarge.Selected) then
    EMenuImageIdx.Value:= ViewLarge.Selected.ImageIndex;
end;

procedure TFVonTools.viewSmallChange(Sender: TObject; Item: TListItem;
  Change: TItemChange);
begin
  if Assigned(viewSmall.Selected) then
    EMenuImageIdx.Value:= viewSmall.Selected.ImageIndex;
end;

{$endregion}
{$region 'Methods of menu'}
procedure TFVonTools.btnMenuAddClick(Sender: TObject);
var
  Idx: Integer;
  newNode: TTreeNode;
begin // 添加菜单
  EMenuName.Text := StripHotkey(Trim(EMenuName.Text));
  if EMenuName.Text = '' then
    raise Exception.Create('菜单名称不能为空。');
  Idx := FAppMenuList.IndexOfValue(0, EMenuName.Text);
  if Idx >= 0 then
    raise Exception.Create('该菜单已经存在，请更换菜单名称。');
  EParentMenuName.Text := Trim(EParentMenuName.Text);
  if EParentMenuName.Text <> '' then
  begin
    Idx := FAppMenuList.IndexOfValue(0, EParentMenuName.Text);
    if Idx < 0 then
      raise Exception.Create('该菜单的父级菜单不存在，请更换父级菜单名称。');
  end;
  // <MenuName><MemuParentName><ImageID><UsingName><Controller><ShortKey><RunType><Params><Hint>
  FAppMenuList.AppendRow([EMenuName.Text, EParentMenuName.Text,
    IntToStr(EMenuImageIdx.Value), EMenuTask.Text,
    IntToStr(EMenuCtrlValue.Value), EMenuShortKey.Text,
    IntToStr(EMenuRunType.ItemIndex), EMenuParams.Text, EMenuHint.Text]);
  if EParentMenuName.Text = '' then
    newNode:= TreeMenu.Items.AddChild(nil, EMenuName.Text)
  else for Idx := 0 to TreeMenu.Items.Count - 1 do
    if SameText(TreeMenu.Items[Idx].Text, EParentMenuName.Text) then begin
      newNode:= TreeMenu.Items.AddChild(TreeMenu.Items[Idx], EMenuName.Text);
      Break;
    end;
  newNode.ImageIndex:= EMenuImageIdx.Value;
  newNode.SelectedIndex:= EMenuImageIdx.Value;
  EMenuName.Text := '';
end;

procedure TFVonTools.btnMenuLoadClick(Sender: TObject);
var
  fs: TFileStream;
  vInt: RVonInt;
begin
  with OpenDialog1 do
    if Execute then begin
      FAppMenuList.LoadFromFile(Filename);
      DisplayMenu('', nil, FAppMenuList);
    end;
//      begin
//        fs := TFileStream.Create(Filename, fmOpenRead);
//        fs.Read(vInt.Bytes[0], 4);
//        if vInt.Int > 0 then
//          FAppUsingList.LoadFromStream(fs, vInt.Int);
//        fs.Read(vInt.Bytes[0], 4);
//        if vInt.Int > 0 then
//          FAppMenuList.LoadFromStream(fs, vInt.Int);
//        fs.Read(vInt.Bytes[0], 4);
//        if vInt.Int > 0 then
//          FAppTaskList.LoadFromStream(fs, vInt.Int);
//        fs.Free;
//        DisplayMenu('', nil, FAppMenuList);
//      end;
end;

procedure TFVonTools.DisplayMenu(PName: string; PNode: TTreeNode; MenuValues: TVonArraySetting);
var
  i: Integer;
  ANode: TTreeNode;
begin
  // 菜单信息 <MenuName><MemuParentName><ImageID><UsingName><Controller><ShortKey><RunType><Params><Hint>
  for i := 0 to MenuValues.Count - 1 do
    if SameText(MenuValues[i, 1], PName) then begin
      ANode:= TreeMenu.Items.AddChild(PNode, MenuValues[i, 0]);
      ANode.ImageIndex:= StrToInt(MenuValues[i, 2]);
      ANode.SelectedIndex:= ANode.ImageIndex;
      DisplayMenu(MenuValues[i, 0], ANode, MenuValues);
    end;
end;

procedure TFVonTools.btnMenuPublishClick(Sender: TObject);

var
  fs: TFileStream;
  ms: TMemoryStream;
  vInt: RVonInt;
//  procedure SaveNode(ANode: TTreeNode);
//  var
//    Idx: Integer;
//    szPName: string;
//  begin
//    Idx := FMenuSetting.IndexOfValue(0, StripHotkey(ANode.Text));
//    if Assigned(ANode.Parent) then szPName:= ANode.Parent.Text
//    else szPName:= '';
//    FAppMenuList.AppendRow([FMenuSetting.Values[Idx, 0], szPName,
//      FMenuSetting.Values[Idx, 2], FMenuSetting.Values[Idx, 3],
//      FMenuSetting.Values[Idx, 4], FMenuSetting.Values[Idx, 5],
//      FMenuSetting.Values[Idx, 6], FMenuSetting.Values[Idx, 7],
//      FMenuSetting.Values[Idx, 8]]);
//    if Assigned(ANode.getFirstChild()) then
//      SaveNode(ANode.getFirstChild());
//    if Assigned(ANode.getNextSibling()) then
//      SaveNode(ANode.getNextSibling());
//  end;
begin
//  FAppMenuList.Clear;
//  SaveNode(TreeMenu.Items[0]);
  with SaveDialog1 do
    if Execute then
      FAppMenuList.SaveToFile(Filename);
//    begin
//      fs := TFileStream.Create(Filename, fmCreate);
//      ms := TMemoryStream.Create;
//      vInt.Int := FAppUsingList.SaveToStream(ms);
//      ms.Position := 0;
//      fs.Write(vInt.Bytes[0], 4);
//      fs.CopyFrom(ms, vInt.Int);
//      ms.Size := 0;
//      vInt.Int := FAppMenuList.SaveToStream(ms);
//      ms.Position := 0;
//      fs.Write(vInt.Bytes[0], 4);
//      fs.CopyFrom(ms, vInt.Int);
//      ms.Size := 0;
//      vInt.Int := FAppTaskList.SaveToStream(ms);
//      ms.Position := 0;
//      fs.Write(vInt.Bytes[0], 4);
//      fs.CopyFrom(ms, vInt.Int);
//      ms.Free;
//      fs.Free;
//    end;
end;
{$endregion}
{$region 'Methods of image'}
procedure TFVonTools.btnSmallAddClick(Sender: TObject);
var
  bmp: TBitmap;
begin
  with OpenDialog1 do
    if Execute then begin
      bmp:= TBitmap.Create;
      bmp.LoadFromFile(Filename);
      ImgSmall.AddMasked(bmp, bmp.Canvas.Pixels[0,0]);
      ImgSmall.Masked:= True;
      with viewSmall.Items.Add do begin
        Caption:= IntToStr(ImgSmall.Count);
        ImageIndex:= ImgSmall.Count - 1;
      end;
      bmp.Free;
    end;
end;

procedure TFVonTools.btnSmallBottomClick(Sender: TObject);
begin
  if not Assigned(viewSmall.Selected) then Exit;
  ImgSmall.Move(viewSmall.Selected.ImageIndex, imgSmall.Count - 1);
end;

procedure TFVonTools.btnSmallDelClick(Sender: TObject);
begin
  ImgSmall.Delete(viewSmall.Selected.ImageIndex);
  viewSmall.Items.Delete(viewSmall.Items.Count - 1);
end;

procedure TFVonTools.btnSmallDownClick(Sender: TObject);
begin
  if not Assigned(viewSmall.Selected) then Exit;
  ImgSmall.Move(viewSmall.Selected.ImageIndex, viewSmall.Selected.ImageIndex + 1);
end;

procedure TFVonTools.btnSmallLoadClick(Sender: TObject);
var
  I: Integer;
begin
  with OpenDialog1 do
    if Execute then begin
      LoadImages(Filename, imgSmall);
      viewSmall.Clear;
      for I := 0 to imgSmall.Count - 1 do
        with viewSmall.Items.Add do begin
          Caption:= IntToStr(I + 1);
          ImageIndex:= I;
        end;
    end;
end;

procedure TFVonTools.btnSmallPublishClick(Sender: TObject);
begin
  with SaveDialog1 do
    if Execute then
      SaveImages(Filename, imgSmall);
end;

procedure TFVonTools.btnSmallTopClick(Sender: TObject);
begin
  if not Assigned(viewSmall.Selected) then Exit;
  ImgSmall.Move(viewSmall.Selected.ImageIndex, 0);
end;

procedure TFVonTools.btnSmallUpClick(Sender: TObject);
begin
  if not Assigned(viewSmall.Selected) then Exit;
  ImgSmall.Move(viewSmall.Selected.ImageIndex, viewSmall.Selected.ImageIndex - 1);
end;

procedure TFVonTools.btnLargeAddClick(Sender: TObject);
var
  bmp: TBitmap;
begin
  with OpenDialog1 do
    if Execute then begin
      bmp:= TBitmap.Create;
      bmp.LoadFromFile(Filename);
      imgLarge.AddMasked(bmp, bmp.Canvas.Pixels[0,0]);
      imgLarge.Masked:= True;
      with viewLarge.Items.Add do begin
        Caption:= IntToStr(imgLarge.Count);
        ImageIndex:= imgLarge.Count - 1;
      end;
      bmp.Free;
    end;
end;

procedure TFVonTools.btnLargeBottomClick(Sender: TObject);
begin
  if not Assigned(viewLarge.Selected) then Exit;
  ImgLarge.Move(viewLarge.Selected.ImageIndex, ImgLarge.Count - 1);
end;

procedure TFVonTools.btnLargeDelClick(Sender: TObject);
begin
  imgLarge.Delete(viewLarge.Selected.ImageIndex);
  viewLarge.Items.Delete(viewLarge.Items.Count - 1);
end;

procedure TFVonTools.btnLargeDownClick(Sender: TObject);
begin
  if not Assigned(viewLarge.Selected) then Exit;
  ImgLarge.Move(viewLarge.Selected.ImageIndex, viewLarge.Selected.ImageIndex + 1);
end;

procedure TFVonTools.btnLargeLoadClick(Sender: TObject);
var
  I: Integer;
begin
  with OpenDialog1 do
    if Execute then begin
      LoadImages(Filename, imgLarge);
      viewLarge.Clear;
      for I := 0 to imgLarge.Count - 1 do
        with viewLarge.Items.Add do begin
          Caption:= IntToStr(I + 1);
          ImageIndex:= I;
        end;
    end;
end;

procedure TFVonTools.btnLargePublishClick(Sender: TObject);
begin
  with SaveDialog1 do
    if Execute then
      SaveImages(Filename, imgLarge);
end;

procedure TFVonTools.btnLargeTopClick(Sender: TObject);
begin
  if not Assigned(viewLarge.Selected) then Exit;
  ImgLarge.Move(viewLarge.Selected.ImageIndex, 0);
end;

procedure TFVonTools.btnLargeUpClick(Sender: TObject);
begin
  if not Assigned(viewLarge.Selected) then Exit;
  ImgLarge.Move(viewLarge.Selected.ImageIndex, viewLarge.Selected.ImageIndex - 1);
end;
{$endregion}

procedure TFVonTools.InitEnvironment;
var
  p: DWORD;
begin
  with FormatSettings do
  begin
    CurrencyString := '￥';
    CurrencyFormat := 0;
    CurrencyDecimals := 2;
    DateSeparator := '-';
    TimeSeparator := ':';
    ListSeparator := ',';
    ShortDateFormat := 'yyyy-M-d';
    LongDateFormat := 'yyyy年M月d日';
    TimeAMString := '上午';
    TimePMString := '下午';
    ShortTimeFormat := 'h:mm';
    LongTimeFormat := 'h:mm:ss';
    ShortMonthNames[1] := '一月';
    ShortMonthNames[2] := '二月';
    ShortMonthNames[3] := '三月';
    ShortMonthNames[4] := '四月';
    ShortMonthNames[5] := '五月';
    ShortMonthNames[6] := '六月';
    ShortMonthNames[7] := '七月';
    ShortMonthNames[8] := '八月';
    ShortMonthNames[9] := '九月';
    ShortMonthNames[10] := '十月';
    ShortMonthNames[11] := '十一月';
    ShortMonthNames[12] := '十二月';
    LongMonthNames[1] := '一月';
    LongMonthNames[2] := '二月';
    LongMonthNames[3] := '三月';
    LongMonthNames[4] := '四月';
    LongMonthNames[5] := '五月';
    LongMonthNames[6] := '六月';
    LongMonthNames[7] := '七月';
    LongMonthNames[8] := '八月';
    LongMonthNames[9] := '九月';
    LongMonthNames[10] := '十月';
    LongMonthNames[11] := '十一月';
    LongMonthNames[12] := '十二月';
    ShortDayNames[1] := '星期日';
    ShortDayNames[2] := '星期一';
    ShortDayNames[3] := '星期二';
    ShortDayNames[4] := '星期三';
    ShortDayNames[5] := '星期四';
    ShortDayNames[6] := '星期五';
    ShortDayNames[7] := '星期六';
    LongDayNames[1] := '星期日';
    LongDayNames[2] := '星期一';
    LongDayNames[3] := '星期二';
    LongDayNames[4] := '星期三';
    LongDayNames[5] := '星期四';
    LongDayNames[6] := '星期五';
    LongDayNames[7] := '星期六';
    ThousandSeparator := ',';
    DecimalSeparator := '.';
    TwoDigitYearCenturyWindow := $32;
    NegCurrFormat := $02;
  end;
  SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_SSHORTDATE,pchar('yyyy/MM/dd')); //设置短日期格式
  SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_SLONGDATE,pchar('yyyy''年''M''月''d''日''')); //设置长日期格式为 yyyy'年'M'月'd'日'，“年月日”字符必须用单引号括起来。Delphi字符串里必须用两个单引号。
  SetLocaleInfo(LOCALE_USER_DEFAULT,LOCALE_STIMEFORMAT,pchar('HH:mm:ss')); //设置时间格式，24小时制
  SendMessageTimeOut(HWND_BROADCAST,WM_SETTINGCHANGE,0,0,SMTO_ABORTIFHUNG,10,p);//设置完成后必须调用，通知其他程序格式已经更改，否则即使是程序自身也不能使用新设置的格式
end;

end.
