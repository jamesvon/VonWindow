unit UVonCrypt;

interface

uses
  System.SysUtils, System.Classes,
  DCPcrypt2, DCPblockciphers, DCPconst, DCPblowfish, DCPcast128, DCPcast256,
  DCPdes, DCPgost, DCPice, DCPidea, DCPmars, DCPmisty1, DCPrc2, DCPrc4, DCPrc5,
  DCPrc6, DCPrijndael, DCPserpent, DCPtea, DCPtwofish,
  DCPhaval, DCPmd4, DCPmd5, DCPripemd128, DCPripemd160, DCPsha1, DCPsha256,
  DCPsha512, DCPtiger;

type
  TCryptItem = class
  private
    FName: string;
  public
    constructor Create(); virtual;
    procedure ReadFromStream(Stream: TStream); virtual; abstract;
    procedure WriteToStream(Stream: TStream); virtual; abstract;
  published
    property Name: string read FName write FName;
  end;

  TCryptCollection<T: TCryptItem> = class(TList)
  private
    FAutoFree: Boolean;
    function GetItem(Name: string): T;
    function GetItemByIndex(Index: Integer): T;
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;
  public
    constructor Create(AutoFree: Boolean = true);
    procedure LoadFromFile(Filename, Key, Cipher, hash: string);
    procedure SaveToFile(Filename, Key, Cipher, hash: string);
    procedure LoadFromStream(AStream: TStream); virtual;
    procedure SaveToStream(AStream: TStream); virtual;
    procedure SetItem(item: TCryptItem);
    property Items[Name: string]: T read GetItem;
    property ItemByIndex[Index: Integer]: T read GetItemByIndex;
  end;

/// <summary>得到CRC校验值</summary>
function Get_CRCCode(RegString: string): string;
/// <summary>得到加密算法名称集合</summary>
procedure GetCipherList(List: TStrings);
/// <summary>得到摘要算法名称集合</summary>
procedure GetHashList(List: TStrings);
/// <summary>根据名称得到加密算法类</summary>
function GetCipherClass(CipherName: string): TDCP_cipherclass;
/// <summary>根据名称得到摘要算法类</summary>
function GetHashClass(HashName: string): TDCP_hashclass;
/// <summary>根据名称得到加密算法</summary>
function GetCipher(CipherName: string): TDCP_cipher; overload;
/// <summary>根据名称得到摘要算法</summary>
function GetHash(HashName: string): TDCP_hash; overload;
/// <summary>根据序号得到加密算法</summary>
function GetCipher(CipherIdx: Integer): TDCP_cipher; overload;
/// <summary>根据序号得到摘要算法</summary>
function GetHash(HashIdx: Integer): TDCP_hash; overload;

implementation

type
  TCryptRegister = class
    /// <summary>加密算法名称</summary>
    CryptName: string;
    /// <summary>加密类</summary>
    CipherClass: TDCP_cipherclass;
  end;

  THashRegister = class
    /// <summary>摘要算法名称</summary>
    HashName: string;
    /// <summary>摘要类</summary>
    HashClass: TDCP_hashclass;
  end;

var
  FCipherList, FHashList: TList;

procedure RegCipher(CryptName: string; CipherClass: TDCP_cipherclass);
var
  newCryptRegister: TCryptRegister;
begin
  newCryptRegister:= TCryptRegister.Create;
  newCryptRegister.CryptName := CryptName;
  newCryptRegister.CipherClass := CipherClass;
  FCipherList.Add(newCryptRegister);
end;

procedure RegHash(HashName: string; HashClass: TDCP_hashclass);
var
  newHashRegister: THashRegister;
begin
  newHashRegister:= THashRegister.Create;
  newHashRegister.HashName := HashName;
  newHashRegister.HashClass := HashClass;
  FHashList.Add(newHashRegister);
end;

/// <summary>得到CRC校验值</summary>
function Get_CRCCode(RegString: string): string;
const
  chars = '\03[YJ$Q9Zu#~maWElw(=*oP"5V}R;d6U!Sf4Hvx{.)>|IK^%nt_BpMO@ADqe-k`8FG+ji1:]/N,7syXcCzgrbL&h2<T?';
var
  TestHash: TDCP_sha1;
  szBit: array [0 .. 19] of byte;
  szRtn: array [0 .. 19] of Char; // 256/93
  i: Integer;
begin
  TestHash := TDCP_sha1.Create(nil);
  TestHash.Init;
  TestHash.UpdateStr(RegString);
  TestHash.Final(szBit);
  TestHash.Free;
  for i := 0 to 19 do
    szRtn[i] := chars[szBit[i] div 90 + szBit[i] mod 90 + 1];
  Result := szRtn;
end;
/// <summary>根据名称得到加密算法类</summary>
function GetCipherClass(CipherName: string): TDCP_cipherclass;
var I: Integer;
begin
  for I := 0 to FCipherList.Count - 1 do begin
    Result:= TCryptRegister(FCipherList[I]).CipherClass;
    Exit;
  end;
end;
/// <summary>根据名称得到摘要算法类</summary>
function GetHashClass(HashName: string): TDCP_hashclass;
var I: Integer;
begin
  for I := 0 to FHashList.Count - 1 do begin
    Result:= THashRegister(FHashList[I]).HashClass;
    Exit;
  end;
end;
/// <summary>得到加密算法名称集合</summary>
procedure GetCipherList(List: TStrings);
var I: Integer;
begin
  for I := 0 to FCipherList.Count - 1 do
    List.Add(TCryptRegister(FCipherList[I]).CryptName);
end;
/// <summary>得到摘要算法名称集合</summary>
procedure GetHashList(List: TStrings);
var I: Integer;
begin
  for I := 0 to FHashList.Count - 1 do
    List.Add(THashRegister(FHashList[I]).HashName);
end;
/// <summary>根据名称得到加密算法</summary>
function GetCipher(CipherName: string): TDCP_cipher;
var szS: string; I: Integer;
begin
  szS:= LowerCase(CipherName);
  for I := 0 to FCipherList.Count - 1 do
    if TCryptRegister(FCipherList[I]).CryptName = szS then begin
      Result:= TCryptRegister(FCipherList[I]).CipherClass.Create(nil);
      Exit;
    end;
end;
/// <summary>根据名称得到摘要算法</summary>
function GetHash(HashName: string): TDCP_hash;
var szS: string; I: Integer;
begin
  szS:= LowerCase(HashName);
  for I := 0 to FHashList.Count - 1 do
    if THashRegister(FHashList[I]).HashName = szS then begin
      Result:= THashRegister(FHashList[I]).HashClass.Create(nil);
      Exit;
    end;
end;
/// <summary>根据序号得到加密算法</summary>
function GetCipher(CipherIdx: Integer): TDCP_cipher;
begin
  Result:= TCryptRegister(FCipherList[CipherIdx]).CipherClass.Create(nil);
end;
/// <summary>根据序号得到摘要算法</summary>
function GetHash(HashIdx: Integer): TDCP_hash;
begin
  Result:= THashRegister(FHashList[HashIdx]).HashClass.Create(nil);
end;

{ TCryptCollection<T> }

constructor TCryptCollection<T>.Create(AutoFree: Boolean);
begin
  FAutoFree:= AutoFree;
end;

function TCryptCollection<T>.GetItem(Name: string): T;
var
  I : Integer;
begin
  for I := 0 to Count - 1 do
    if SameText(T(Get(I)).Name, Name) then begin
      Result:= T(Get(I));
      Exit;
    end;
end;

function TCryptCollection<T>.GetItemByIndex(Index: Integer): T;
begin
  Result:= T(Get(Index));
end;

procedure TCryptCollection<T>.LoadFromFile(Filename, Key, Cipher, hash: string);
var
  sIntput: TFileStream;
  szStream: TMemoryStream;
begin
  with GetCipher(Cipher) do try
    InitStr(Key, GetHashClass(hash));
    sIntput := TFileStream.Create(Filename, fmOpenRead);
    szStream:= TMemoryStream.Create;
    DecryptStream(sIntput, szStream, sIntput.Size);
    szStream.Position:= 0;
    LoadFromStream(szStream);
    szStream.Free;
    sIntput.Free;
  finally
    Free;
  end;
end;

procedure TCryptCollection<T>.LoadFromStream(AStream: TStream);
var
  item: T;
begin
  Clear;
  while AStream.Position < AStream.Size do begin
    item:= T(TClass(T).Create);
    item.ReadFromStream(AStream);
    Add(Pointer(item));
  end;
end;

procedure TCryptCollection<T>.Notify(Ptr: Pointer; Action: TListNotification);
begin
  inherited;
  case Action of
  lnDeleted: if FAutoFree then T(Ptr).Free;

  end;
end;

procedure TCryptCollection<T>.SaveToFile(Filename, Key, Cipher, hash: string);
var
  sOutput: TFileStream;
  szStream: TMemoryStream;
begin
  szStream:= TMemoryStream.Create;
  SaveToStream(szStream);
  with GetCipher(Cipher) do try
    //SetLength(CipherIV, BlockSize div 8);
    InitStr(Key, GetHashClass(hash));
    sOutput := TFileStream.Create(Filename, fmCreate);
    szStream.Position:= 0;
    EncryptStream(szStream, sOutput, szStream.Size);
    szStream.Free;
    sOutput.Free;
  finally
    Free;
  end;
end;

procedure TCryptCollection<T>.SaveToStream(AStream: TStream);
var
  I: Integer;
begin
  for I := 0 to Count - 1 do
    T(Get(I)).WriteToStream(AStream);
end;

procedure TCryptCollection<T>.SetItem(item: TCryptItem);
var
  I: Integer;
  A: T;
begin
  for I := 0 to Count - 1 do
    if SameText(T(Get(I)).Name, item.Name) then begin
      A:= T(Get(I));
      inherited Items[I]:= item;
      A.Free;
      Exit;
    end;
  Add(item);
end;

{ TCryptItem }

constructor TCryptItem.Create;
begin

end;

initialization

  FCipherList:= TList.Create;
  FHashList:= TList.Create;
  (* Regist cipher *)
  RegCipher('blowfish', TDCP_blowfish);
  RegCipher('cast128', TDCP_cast128);
  RegCipher('cast256', TDCP_cast256);
  RegCipher('des', TDCP_des);
  RegCipher('3des', TDCP_3des);
  RegCipher('ice', TDCP_ice);
  RegCipher('thinice', TDCP_thinice);
  RegCipher('ice2', TDCP_ice2);
  RegCipher('idea', TDCP_idea);
  RegCipher('mars', TDCP_mars);
  RegCipher('misty1', TDCP_misty1);
  RegCipher('rc2', TDCP_rc2);
  RegCipher('rc4', TDCP_rc4);
  RegCipher('rc5', TDCP_rc5);
  RegCipher('rc6', TDCP_rc6);
  RegCipher('rijndael', TDCP_rijndael);
  RegCipher('serpent', TDCP_serpent);
  RegCipher('tea', TDCP_tea);
  RegCipher('twofish', TDCP_twofish);
  (* Regist hash *)
  RegHash('haval', TDCP_haval);
  RegHash('md4', TDCP_md4);
  RegHash('md5', TDCP_md5);
  RegHash('ripemd128', TDCP_ripemd128);
  RegHash('ripemd160', TDCP_ripemd160);
  RegHash('sha1', TDCP_sha1);
  RegHash('sha256', TDCP_sha256);
  RegHash('sha384', TDCP_sha384);
  RegHash('sha512', TDCP_sha512);
  RegHash('tiger', TDCP_tiger);

finalization

  FCipherList.Free;
  FHashList.Free;

end.
